(define-module (crates-io mi t- mit-hook-test-helper) #:use-module (crates-io))

(define-public crate-mit-hook-test-helper-3.98.71 (c (n "mit-hook-test-helper") (v "3.98.71") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1lzpikdpg7qivx3na86jcsjhvy7zyizla3zc16bzn43yy093hzpx")))

(define-public crate-mit-hook-test-helper-3.98.70 (c (n "mit-hook-test-helper") (v "3.98.70") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1n4ca18q3cyigkqb0wdr1zvhabby5lyyax4dwvak5asvqp64p6vm")))

(define-public crate-mit-hook-test-helper-3.98.73 (c (n "mit-hook-test-helper") (v "3.98.73") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qnparmj0c0pan10ski7ayzxlwbb686jxf3ryx7kffjm4yndis3q")))

(define-public crate-mit-hook-test-helper-3.98.74 (c (n "mit-hook-test-helper") (v "3.98.74") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1mryyqc8m1wm4az4w49jk15p1rn5dbksdwlcdpj8mlcb51vmvg78")))

(define-public crate-mit-hook-test-helper-3.98.76 (c (n "mit-hook-test-helper") (v "3.98.76") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0yaj0acw71rrjkqfcabxkqi11s5z94p1kympac0jgsgrif2iy6va")))

(define-public crate-mit-hook-test-helper-3.98.77 (c (n "mit-hook-test-helper") (v "3.98.77") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qlihb9zjf7bhf58q31cb0z0v0nj4mh6zavva8byqyhgzdpgmiga")))

(define-public crate-mit-hook-test-helper-3.98.78 (c (n "mit-hook-test-helper") (v "3.98.78") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0lg882g3rf22rbmrx301inyqiz5f0rv8wwigmdg7ix7cf5i8j7x0")))

(define-public crate-mit-hook-test-helper-3.99.0 (c (n "mit-hook-test-helper") (v "3.99.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0fgjfxyh83nc7hhjyfhbi3pri4pgvf0hhlkis09l6i06nsdi0swn")))

(define-public crate-mit-hook-test-helper-3.99.1 (c (n "mit-hook-test-helper") (v "3.99.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1sa5jn3qabbw9km59j37wsgkrm92sin91q5ks4s7lg07lx9004rh")))

(define-public crate-mit-hook-test-helper-3.99.2 (c (n "mit-hook-test-helper") (v "3.99.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1a6b2bmqqypswx0vfhh86g52j49b27bjvi2s47iqhwqzryi5lndq")))

(define-public crate-mit-hook-test-helper-3.99.3 (c (n "mit-hook-test-helper") (v "3.99.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "08aggh1rw7s9prqhb06qiq75i09nxcnwn1c215imriq32hfxg61j")))

(define-public crate-mit-hook-test-helper-3.99.4 (c (n "mit-hook-test-helper") (v "3.99.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1987lnzrvbyhfkpnrmr3drspqsavhia68gc85zl8a3hlyd3r73dj")))

(define-public crate-mit-hook-test-helper-3.99.5 (c (n "mit-hook-test-helper") (v "3.99.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "07ijxsp9rhd0mrqkm55d06cxzsjy741vggzwnkxxan2byhnyag97")))

(define-public crate-mit-hook-test-helper-3.99.6 (c (n "mit-hook-test-helper") (v "3.99.6") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "031qrl9dc39m1243mcz8g5m929lfnyky1dh7nd63qwdzcd0r18yg")))

(define-public crate-mit-hook-test-helper-3.99.7 (c (n "mit-hook-test-helper") (v "3.99.7") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "07sj282ild894jay8s7jw2j54llsdnhn0r702sfra6llfz4jnb6v")))

(define-public crate-mit-hook-test-helper-3.99.8 (c (n "mit-hook-test-helper") (v "3.99.8") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1y9ibzpip5fclzwcfzrgb1gwykcx0wdx46f8ds1z2p8468cjzf1s") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-3.100.0 (c (n "mit-hook-test-helper") (v "3.100.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1v8mddar9scfkxp1w4z67ak386k9h154l31j3ki3bnaa2j82pk5j") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-3.100.1 (c (n "mit-hook-test-helper") (v "3.100.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qci582s32nzrp2wcylygvyw8lh98ncq9v14aqa5mimf7dsd3lb3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-3.100.2 (c (n "mit-hook-test-helper") (v "3.100.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0wm28dnkbp69lv3lz5fk19rb4ybh76lbd0c71rwrh9spjzlrai7y") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.0 (c (n "mit-hook-test-helper") (v "4.0.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0p807wbrjg525nsk1l9snka3w1nnjc8y6bsxx6nblv2vhxw5bzf4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.1 (c (n "mit-hook-test-helper") (v "4.0.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0abkc1jl2a25gw91vkbk0qkg9r23mj17lbbdsfib8znrpgdyf7ly") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.2 (c (n "mit-hook-test-helper") (v "4.0.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "16l5p2mmc4qwa8agxdikllrw1hsz85cssp4iy4bdj44ziq20p329") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.3 (c (n "mit-hook-test-helper") (v "4.0.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0c0mmcb5r7ksnk1fa3q4jnprw6axfr8yjz2ql9121iwahg1fm5sj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.4 (c (n "mit-hook-test-helper") (v "4.0.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qhymxizbk8yyspp1qxcpfzj4ghj8smdp8r4gpac0164313rvs54") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.5 (c (n "mit-hook-test-helper") (v "4.0.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1r25p354wp1g3rmqnchwh5x3w63nfngyalggvfkkfyi6b69g5dkk") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.6 (c (n "mit-hook-test-helper") (v "4.0.6") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qj85va2ck2a2m8rdh8wdhdr98cxvn11jdxxinff4raci2nh1j54") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.7 (c (n "mit-hook-test-helper") (v "4.0.7") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0fl67h610cywvkinhfg7hkvf2b33w0xkz27iy9zjdyc0qbv81siv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.8 (c (n "mit-hook-test-helper") (v "4.0.8") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18h1dgm2bip3vab16362clxrx6yhh5483ap7wgjin5ygfhvryd94") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.9 (c (n "mit-hook-test-helper") (v "4.0.9") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "04k80lmfazv0sqpkpd46f9hvqky4gw4sb0cls3q8k8rp3w86iqy7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.10 (c (n "mit-hook-test-helper") (v "4.0.10") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18m8g6alkkqdndjhm7w682xmq8gdm0b90dzqfdj84754d4pnifrq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.11 (c (n "mit-hook-test-helper") (v "4.0.11") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1bspfqvalzd65jjnj3f8cmwg7crflpvxls819amy0x2kykrf9n8l") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.12 (c (n "mit-hook-test-helper") (v "4.0.12") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0n8c164h91i4ds61y0mn3d8kj5ljq82rpbwlm44w0fvxpkc67v42") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.13 (c (n "mit-hook-test-helper") (v "4.0.13") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "00slndg5ivjznqsjxx309qqmmi5d9lrn0zhnx3sridqglqq99k4h") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.14 (c (n "mit-hook-test-helper") (v "4.0.14") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1g9j5gf59brvhc6jn29bjh9kzylx4pr49qprsl2bdsgg1cym56y1") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.15 (c (n "mit-hook-test-helper") (v "4.0.15") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0jqfih03wgr3z0l8dbgcj4xvwyv21kw7cbv3a4wafr03hj2qbia4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.16 (c (n "mit-hook-test-helper") (v "4.0.16") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "02y5sprnm8s77f8sk4x5gfmpl1dvybqz2xz1cvalgiwmq7m229yv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.17 (c (n "mit-hook-test-helper") (v "4.0.17") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0dlc83fc4nr4c79hb3gy532ffzmm9jnpmnsk3dfsmvp6mx2vynk5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.18 (c (n "mit-hook-test-helper") (v "4.0.18") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0yyj9pbslm2is1ixwbfnplx2kv75qbh2l1v51w43llkyshjkfgwh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.19 (c (n "mit-hook-test-helper") (v "4.0.19") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0c91h7lahfzwqvcl95krs1w4ys0vxw0nw1j27y94qlyw0cxlrl6w") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.20 (c (n "mit-hook-test-helper") (v "4.0.20") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0fdjx1cna9v0lnpbhswcdp5y37l6y73n7hdb6dg0svfdccq7b352") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.21 (c (n "mit-hook-test-helper") (v "4.0.21") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "03jg2rn5vncn47mi8hhmbnir9rnx4qbjv7sf4mhzfswr6qdpv25k") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.22 (c (n "mit-hook-test-helper") (v "4.0.22") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "10xi6y29wg9577szd8jwsjxnh3dh3vv62hn048wlp6hfar8zs3dv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.23 (c (n "mit-hook-test-helper") (v "4.0.23") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1dh80y89wx65q7fbfwl350v73h0kig5p2giranbs647pv8hyjl8k") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.24 (c (n "mit-hook-test-helper") (v "4.0.24") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "13vphf80bdlwzd4dmikmrx0z7ib0ifjkf2vm02gi33grmbjb8ci7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.25 (c (n "mit-hook-test-helper") (v "4.0.25") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1x715df5r69q9ca8lmjzpsq5nvqx20n3vcjw1lpzjiiqhbs7012w") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-4.0.26 (c (n "mit-hook-test-helper") (v "4.0.26") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1ywmcfydnhnc24z12i981h01hs4mi4bbh3bwd5l1pr5pkbd0gmx7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.0.0 (c (n "mit-hook-test-helper") (v "5.0.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1rs61xaip70gqf7pspy755bhv6jcv5lsbbikpy2c68gvmfy8dmbg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.0.1 (c (n "mit-hook-test-helper") (v "5.0.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1y9njnw592ixdbbsw0cjkx00kqs6m4y4c0xwl61hzwl0xyn8gnlh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.1.0 (c (n "mit-hook-test-helper") (v "5.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "06id3pi7z34vd94rgaacnqf7zrvh468g6jymja85d5sp1aaas8l5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.1.1 (c (n "mit-hook-test-helper") (v "5.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "17p68m0zpizz3w0n8hvdsaddz2riig9q6cg27vb5wc7nc9q2zbzc") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.1.2 (c (n "mit-hook-test-helper") (v "5.1.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1gx4l3n8rj9qxbxbcf72gnbmbdna8c0wscf1dnmabjcn1m0x1kil") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.2.0 (c (n "mit-hook-test-helper") (v "5.2.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "05frjp0pahz8rzbjg3jdllr7mzz59wxf17nk48hfy9lfk7dr1ks5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.2.1 (c (n "mit-hook-test-helper") (v "5.2.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0bwh4xf91i4xk516bybr1pkjcbp9ksnc0b6kbndibjwrln5qm8ws") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.3.0 (c (n "mit-hook-test-helper") (v "5.3.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0wah5lgi8wy2aivnf6h6azzzdgsgs9wiz92k3wvclx41xygp0hjl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.3.1 (c (n "mit-hook-test-helper") (v "5.3.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0cpb4jvf9iazp67hl6wypbpyii7101fz7q4g0s0nwfdy58aayv9s") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.3.2 (c (n "mit-hook-test-helper") (v "5.3.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0ml73vsq83bvlh644mj87ap0l4hyhy1ckg6abj427g1wwz34qkyz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.4.0 (c (n "mit-hook-test-helper") (v "5.4.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1804a5w7c9prgj0zkv4m3mqx3z5hilnszr38s20vhvxr2n6c738f") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.5.0 (c (n "mit-hook-test-helper") (v "5.5.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "08f1nj431hs3cij8578irm1gi6rjplv8vapgybzaz3pprg7ggq79") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.0 (c (n "mit-hook-test-helper") (v "5.6.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "16j4p573crpr0k96nvr0b0jia3y4j2f3a8idlzc2hv7h2ha2s5f7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.1 (c (n "mit-hook-test-helper") (v "5.6.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "124rhpxn6jhdfwy7fmsv9n33s3115b720mckgxcmvah4s095b0v9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.2 (c (n "mit-hook-test-helper") (v "5.6.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "02vxwj9jsifrblm7y6ryg4n277c4n9mzbllp13x0lj47apm7r5wb") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.3 (c (n "mit-hook-test-helper") (v "5.6.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1gkr84v9295sm8dfng5wcgnzi9jfg7dck9crimna467jfhrj320j") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.4 (c (n "mit-hook-test-helper") (v "5.6.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0w6m9rq0q0lm13mb9ldq2w8fr2nz0c584pwfiiz5xxvvqgcm81a5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.6.5 (c (n "mit-hook-test-helper") (v "5.6.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0zp8akhab3fy0y8zl3s7xy60mbfmw44wikw81z9ybnixzjad83dy") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.0 (c (n "mit-hook-test-helper") (v "5.7.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18j1zg8pn11r3z9sys8db53wkq5s8085nb86i1f54v261m412nbi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.1 (c (n "mit-hook-test-helper") (v "5.7.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "057bxzf6bpf972zgp3q66y22f7z9nhxqwfpy4qmlq2pf1qiksvzz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.2 (c (n "mit-hook-test-helper") (v "5.7.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "10z6yrqz602g2n9rf8xxqkw7pg8r97643848mrkandd812kapsr4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.3 (c (n "mit-hook-test-helper") (v "5.7.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1vy9rpx6lly6cf09nnzwm62gqk7wn33820k4d154hzr4s15vx07m") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.4 (c (n "mit-hook-test-helper") (v "5.7.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "01pgscs4bc04d1wdj972p474k3r9v9sp4689629fjh13hpkbjnaz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.5 (c (n "mit-hook-test-helper") (v "5.7.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1ah518bg50x029y00cbif93p8idp6l7019jxq04qlzjm6h9bm6hs") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.7.6 (c (n "mit-hook-test-helper") (v "5.7.6") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0a31y53ia2mi4nwp0m0l52wyhg9dy8z5iqn6ia8p8mh7na5xndij") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.0 (c (n "mit-hook-test-helper") (v "5.8.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0apzyq7yn4jkd4v32f49s85dhd5cys34r78zhjpdisyv9j0p43ff") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.1 (c (n "mit-hook-test-helper") (v "5.8.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18aj71z6fqr78k20b461k1clp0wafqvysddsc40zx1gncfgwwr32") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.2 (c (n "mit-hook-test-helper") (v "5.8.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1mghbhv6i5rpfv4cvx112z3zzd0x03dar7h2gnwicfzh7ci1z981") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.3 (c (n "mit-hook-test-helper") (v "5.8.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1472sjf1h5nzrvz3b9v9hsv118qzx5n8vpshfsx60mlvdnfmzgj4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.4 (c (n "mit-hook-test-helper") (v "5.8.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "08pg3vx7amawwshk54hiaymn6zd80z7vgs4rarw3gl7f0avkaidl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.5 (c (n "mit-hook-test-helper") (v "5.8.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1qv9v4ilx6x7adk5ydsjs6r6yni3yynsk8biszd2qvvyg2qnvr28") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.6 (c (n "mit-hook-test-helper") (v "5.8.6") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1v9ccj3k1jawxmzyrybqrcmimc8p610p2bmn1pfpmyxh6mnvc6xq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.8.7 (c (n "mit-hook-test-helper") (v "5.8.7") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1vn1hffw2gvfwd6spv7mya3s5v49k03p8a0gd71fw4a6xk4d1jc4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.0 (c (n "mit-hook-test-helper") (v "5.9.0") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1ahd5apg3207mm30ikh9407kvjraddp2lbpn2a66lda132jnyrs7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.1 (c (n "mit-hook-test-helper") (v "5.9.1") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1aj8b1dxl7j8kdmwmlb9phvaa34canpy0q2vd0xa4m0nj21azb5l") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.2 (c (n "mit-hook-test-helper") (v "5.9.2") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18cgw5wqwxgg89jaflphjz204gf3jxaa0zaav1w0j7vfi507h4xr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.3 (c (n "mit-hook-test-helper") (v "5.9.3") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0n252nmcxj3gyh4pqpyk4xw88spq4xjn0ly72x9niw88w9j8zrrv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.4 (c (n "mit-hook-test-helper") (v "5.9.4") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "055makh44f4w9445d92r2qpncnrymp8hhdxds59446lksi665qm9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.5 (c (n "mit-hook-test-helper") (v "5.9.5") (d (list (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "121qrh0jmbhz9pnddr3lplzcqjzip1c02wgh5p1ypr9hghj3xh1i") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.6 (c (n "mit-hook-test-helper") (v "5.9.6") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1p6bh4c2cdn7hmxw461fyd5xybiz8vk7mrnwkhphpliyw45zjfcj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.7 (c (n "mit-hook-test-helper") (v "5.9.7") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0m084lms8jjp1hn22npdgcglwg9s41x4x4x6s87bmvqhqnzvla0z") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.8 (c (n "mit-hook-test-helper") (v "5.9.8") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1y5040jqvcfv6vx8643n92xlz4c7l16j21qa283hcp1pfj9xblfq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.9 (c (n "mit-hook-test-helper") (v "5.9.9") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1fbfxc5hjls3z958mih0lpp9wi97qi3qg2sddwggixkq4plr748l") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.10 (c (n "mit-hook-test-helper") (v "5.9.10") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1fnyv3g5aqmd3sfbbcncdr9gpl6jd7h90jz5sbf7gbikzgs0apv8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.9.11 (c (n "mit-hook-test-helper") (v "5.9.11") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1215x2n3bv36vn20l1qswkzkkwr6a10pdr2s75iqc1smm3w89lba") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.0 (c (n "mit-hook-test-helper") (v "5.10.0") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0y5s0bzmw6d5ni9jx8593nn6azyvlbay50fmfpca6gspjxish0c6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.1 (c (n "mit-hook-test-helper") (v "5.10.1") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0n2bimvjv40yibnw40v92kxl0c09ypbkh9y186w093p0hr2q58vg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.2 (c (n "mit-hook-test-helper") (v "5.10.2") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1jxwjify6byvi6a1ipwwcn33yg6wq859w9k0h1hl2zy02n8ckbf8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.3 (c (n "mit-hook-test-helper") (v "5.10.3") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "10xxw15qhngxa8m2aqfyy407garxlcpr5ln68wwk3xw741f77ibg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.4 (c (n "mit-hook-test-helper") (v "5.10.4") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1vh75ihd5w7k1qj92xwgwg6l7wby30b4cl043szkji0w1dvd9kn9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.5 (c (n "mit-hook-test-helper") (v "5.10.5") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1z4bzimrx5j1if1p1m8wkyhs53cj80r6d3zaircb2y1c4c6syznf") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.6 (c (n "mit-hook-test-helper") (v "5.10.6") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "13pbm0w81g0fdgg4hhrb4yhgjm6a54f87zabrwrgg1k4s0kylliw") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.7 (c (n "mit-hook-test-helper") (v "5.10.7") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0ha64mlcl2gw1fvf66s9xvrcnvi4igvg0clln0hwp2cy0jx31i61") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.10.8 (c (n "mit-hook-test-helper") (v "5.10.8") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1jg5n2sv6wxxjp008qm012sbbpskw9hmbn8rcpqkiyb23r0c12jv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.0 (c (n "mit-hook-test-helper") (v "5.11.0") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "03pqbwas02qqg8zmkli1d2fhla2ggzx17aspcqf8ld7rl1k07q4b") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.1 (c (n "mit-hook-test-helper") (v "5.11.1") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "18jd4jk0pkx0xvd3aaq8lxd8a4baj8z2n7zf4q4jqjxn7pz5ydyd") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.2 (c (n "mit-hook-test-helper") (v "5.11.2") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0kmb7p3x1jpxs0qig95m5mq5d3hmbhfv1vbkf3yic2hw173l752m") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.3 (c (n "mit-hook-test-helper") (v "5.11.3") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0d1mn986w2paa3r55f86vrqlhdsilb91xxwj43smlq25zp049rdw") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.4 (c (n "mit-hook-test-helper") (v "5.11.4") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0ihqp75lzmz6rblk362k60brgprh3awbp2n0056al06al6yhahrf") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.5 (c (n "mit-hook-test-helper") (v "5.11.5") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0421sdj4l9z4csjwm9z8m87h14issw6kk3bm7cd8n71w2715196k") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.6 (c (n "mit-hook-test-helper") (v "5.11.6") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0d6md2daibdhgmaiwsc85sxfz458ddpzicwn8pyvyqxacrwr6a9j") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.7 (c (n "mit-hook-test-helper") (v "5.11.7") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.37") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0ixlqnyiyz1bhbkm7p715q8sqmda6s4bzpsgl4h8wci2yvspjhy3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.8 (c (n "mit-hook-test-helper") (v "5.11.8") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1mpkwaly4paya28l4h4y1y5gs5xf0ga0wv5r2pa30kjhgzj8kb2a") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.11.9 (c (n "mit-hook-test-helper") (v "5.11.9") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0wwwcidbz6vb3id1s7lhx57jar4c1z2lij7z6in4pv206fr2lhiv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.0 (c (n "mit-hook-test-helper") (v "5.12.0") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0pgdamdkifqb6zv8rml0awj4c06k6ir26j4mv4r228jag4rq2myd") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.1 (c (n "mit-hook-test-helper") (v "5.12.1") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "038wivjbq13bs6f81vqxajiwk4i6any6kmllc6rgaip1rk4ppky5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.2 (c (n "mit-hook-test-helper") (v "5.12.2") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0bv60hdbhlssd48ncc2894kblay679glwnsd13y352847jwbkwap") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.3 (c (n "mit-hook-test-helper") (v "5.12.3") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0sn2yjdqkn8fkln57p00zfsh6p4r82zkhb9fis1ldibs1mkjy40m") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.4 (c (n "mit-hook-test-helper") (v "5.12.4") (d (list (d (n "git2") (r "^0.13.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1ysq9x8jyciw02gci6y579pm52f0ml9dkjcr5kbag7j515g6l2rz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.5 (c (n "mit-hook-test-helper") (v "5.12.5") (d (list (d (n "git2") (r "^0.13.24") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0f4cys6p4dgmf9rs8m2m41f02y8s8f5f5g8h3p8iacq7k46x67gy") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.6 (c (n "mit-hook-test-helper") (v "5.12.6") (d (list (d (n "git2") (r "^0.13.24") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "05sgb0jqrh4ij6hqk6jq6cjx20yv1cqv2q6g0nnc8qn0mbs55l3k") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.7 (c (n "mit-hook-test-helper") (v "5.12.7") (d (list (d (n "git2") (r "^0.13.24") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0vwa4bbcq6x3zvn862gsfjix9qcd4ifz0jlwgr6jh8p54hvhjaam") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.8 (c (n "mit-hook-test-helper") (v "5.12.8") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1yh008b93m1d456by1s65xs1a3jmzhvp6sn11k73nchffxvcq99i") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.9 (c (n "mit-hook-test-helper") (v "5.12.9") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0faksiy20ybc0crz3gak3v758nxid6bsxa471hw1573v8pjlkdzw") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.10 (c (n "mit-hook-test-helper") (v "5.12.10") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "05yl0g88x2l48y9ffis5knhhzycx0m6wav00myii62wgaqsx6kg5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.11 (c (n "mit-hook-test-helper") (v "5.12.11") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0mssi893smwaf9zqx3rnnd78znshvgn0z1win3bi3g4hz7j437ql") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.12 (c (n "mit-hook-test-helper") (v "5.12.12") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0gx6r93hyc77qzmxndhpmdrivhlf176wn62ajcyvy0cf3454khgh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.13 (c (n "mit-hook-test-helper") (v "5.12.13") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0xdqa1nsysx43mv6bqizb5kk5sm063wghppag91z33284ynw43f1") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.14 (c (n "mit-hook-test-helper") (v "5.12.14") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "174kq1xv390y9n5823rkaxxvvns68k91qdr5k8dwhgvnxai76bw9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.15 (c (n "mit-hook-test-helper") (v "5.12.15") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "114lpzvwkmx6piapcjmkahvh869y80xwg604fdvfmq1bih2ibvmn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.16 (c (n "mit-hook-test-helper") (v "5.12.16") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1sayhybj6r7v9ma6gp9bnvk38zr59fdvz25j3s993d0wnr8zs2a7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.17 (c (n "mit-hook-test-helper") (v "5.12.17") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1k1vsyz6835il39ggz7gj9ndrngzzfv7z52m274i0g922fvmzv3a") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.18 (c (n "mit-hook-test-helper") (v "5.12.18") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "18c07gmwkphgli3v12w2cal30a3yrp0xxd014f2a12p0gijspnfn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.19 (c (n "mit-hook-test-helper") (v "5.12.19") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0fswjfaszgnv6lwjldnb93zd25rn16s7f8m4pgd5nixl9msdrks5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.20 (c (n "mit-hook-test-helper") (v "5.12.20") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0prj4x8y2a746vq4wxmf8k1l1l5j61cjjshknbdpkmnidw0c3cbw") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.21 (c (n "mit-hook-test-helper") (v "5.12.21") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1l6lizyaz6llrh2y1swshmlsznhldzgwym057h2gscl4pg3rlncg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.22 (c (n "mit-hook-test-helper") (v "5.12.22") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "06qyf3s7xr8p9703yw52cwdlsqcn5jb4l9awl1y2h1a61mc7iqmg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.23 (c (n "mit-hook-test-helper") (v "5.12.23") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "15hd1hwmaay89b6vgfhi9wy8ncvkgcgjn9sfxwgkjbklj0anjjnx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.24 (c (n "mit-hook-test-helper") (v "5.12.24") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0k789dh56pd6rm3gdcpy9g0sczjgz885jp931znn2bjivdis68xz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.25 (c (n "mit-hook-test-helper") (v "5.12.25") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1zydv103s2i2qgvlrcqds2zar8v21pl46scj4ccf3rdky9imjjxp") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.26 (c (n "mit-hook-test-helper") (v "5.12.26") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1c85fgrmp55n5f90xszvaw1dlvp638vqx5bqyswh8a5lc2hk00c2") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.27 (c (n "mit-hook-test-helper") (v "5.12.27") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1l9hmyapg233q9gi3r5w26zjcai53jhkq84j5w166v9hxa3y7rg8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.28 (c (n "mit-hook-test-helper") (v "5.12.28") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0jx85gdyypjnqsc6apbzv774vzb7bs4nmrj29bhsd5f92k2x7s5r") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.29 (c (n "mit-hook-test-helper") (v "5.12.29") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "00hb88b3g7p2rzl0jls37h429k86q4y1qcp7xhgz42l4n8zkrzbv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.30 (c (n "mit-hook-test-helper") (v "5.12.30") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0w0yikpf82rw20sm2xq7nxsdyx7csnfxc0v2hrk097y1nagx6cha") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.31 (c (n "mit-hook-test-helper") (v "5.12.31") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "007c3cbivrw84y28x25czmrp29fgnngq60nkjalxclnsbnrkyycr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.32 (c (n "mit-hook-test-helper") (v "5.12.32") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ijc8j6akrza5hja2ml0yirlss87vc6vya80gqimfqb6b3j3rqry") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.33 (c (n "mit-hook-test-helper") (v "5.12.33") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1123rkc5rimgp1alx1hixr6x15v8sg20lvhypjdcgdhq5dkhhvyx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.34 (c (n "mit-hook-test-helper") (v "5.12.34") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "09ajz3fmqg5jbr320qzygqbp1ks6dql2h67l1kzz2bi2vcx2xis7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.35 (c (n "mit-hook-test-helper") (v "5.12.35") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0nfq3nwplz5isq65psf7smiq72d257320g7csw68gc158c5nnbwr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.36 (c (n "mit-hook-test-helper") (v "5.12.36") (d (list (d (n "git2") (r "^0.14.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0970l63dzxz612zc9rpv1y5lfc11njw0cwlccbcvsa50sg0wzjqs") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.37 (c (n "mit-hook-test-helper") (v "5.12.37") (d (list (d (n "git2") (r "^0.14.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "16k1lkav4pvaw8kismkg58zjpnmjkpy0h7hpwhzv79jz19kfyj4h") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.38 (c (n "mit-hook-test-helper") (v "5.12.38") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "19bbv1wjibwzb5xi5f464279vc7803852fbgvp1n5bq5dd03wc91") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.39 (c (n "mit-hook-test-helper") (v "5.12.39") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0n1nacfglg211476a6hnqd201bipg7b1qqllmgjqa9q6940c5y23") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.40 (c (n "mit-hook-test-helper") (v "5.12.40") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0wkjr0v76w1kk2a3xz0d54hyc4gm70dd1bbqh9r1ajcb8vihhdbs") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.41 (c (n "mit-hook-test-helper") (v "5.12.41") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "14vmxzacsld7794sgafqhcqk19i051h8dgq2yar0rvz5ik09f0nd") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.42 (c (n "mit-hook-test-helper") (v "5.12.42") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1aliv3527gaarqyczzw0czdx5nznn6gi3wpq8qv2gm1rnz8xs5g8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.43 (c (n "mit-hook-test-helper") (v "5.12.43") (d (list (d (n "git2") (r "^0.14.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1vhr8cc1gnib1drr41952jqzrgrcyr7f26qg8zpwcl1b7c6iqind") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.44 (c (n "mit-hook-test-helper") (v "5.12.44") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "16v0bf8jl7whshr6h02ryqzk8ha0g11a2pxgw13alzh8anv8mb90") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.45 (c (n "mit-hook-test-helper") (v "5.12.45") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "00bdhxnzvwp3is5as297aivljag6l93ry1qb19m251daivddynwm") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.46 (c (n "mit-hook-test-helper") (v "5.12.46") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "06vjkag4l7mv6fjjywzfrh6qsgi5j071gxfg4iv06k0a3h5zk381") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.47 (c (n "mit-hook-test-helper") (v "5.12.47") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1cxrci5k2zd6yggv85w3n9dc8kjkdkl4yq6sr3a2wacnw2jsk4sq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.48 (c (n "mit-hook-test-helper") (v "5.12.48") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "12ndslmaq0144a1v2yf3qlr8ff7il9hf4gdiw41b7zsxp47gcsid") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.49 (c (n "mit-hook-test-helper") (v "5.12.49") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "119x4c8klgnv2qzn6g5ly76y2k49hd2l7881h8hqsa6zv6kxh1i5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.50 (c (n "mit-hook-test-helper") (v "5.12.50") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "03rxwbz9va602ypsb15ksj2fv8i26xj3fhmwx0l3kg0dl6xpfjdj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.51 (c (n "mit-hook-test-helper") (v "5.12.51") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "10bj1zc0x9b79qp2pwn72ykfi298k49q46a3rag3flims60l15yc") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.52 (c (n "mit-hook-test-helper") (v "5.12.52") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "14zxwjw08651c9apirh68paiq16sp34jm4iqhhxmckgh1mm4lx3k") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.53 (c (n "mit-hook-test-helper") (v "5.12.53") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "10mwkc498ijvg1ib9szqf6iy4qkz5jpksx7chch0ks9fx1p8z12h") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.54 (c (n "mit-hook-test-helper") (v "5.12.54") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0sh22a96s1kz1kl4cz9g0qhy2wj1z5cwfdb29l6nw28r3j7isf1y") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.55 (c (n "mit-hook-test-helper") (v "5.12.55") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0mqm9vg6qsh2fwvh4f8zjhvzyfa2cydb36dqfb04rkfn4fm9jlsi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.56 (c (n "mit-hook-test-helper") (v "5.12.56") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0pw97hxwwkj87asqmap5pc92aaspjy1v2jiwrj4i72c115l5qja9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.57 (c (n "mit-hook-test-helper") (v "5.12.57") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ly41v6f63mc358gj4nnnr8gxz0whf07bkv7k6d7dpf15mw61dip") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.58 (c (n "mit-hook-test-helper") (v "5.12.58") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1pzsn6sq898x75jzbq36v5j3a2lynqj3pv001rcq8pyv4s11gam8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.59 (c (n "mit-hook-test-helper") (v "5.12.59") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1czcb7c5k76zrpg9nms372wz6lym5ijd7jixwv66ffy8cm07plng") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.60 (c (n "mit-hook-test-helper") (v "5.12.60") (d (list (d (n "git2") (r "^0.14.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0c0m5vz50va0k2f2nnvsw1ndwjbzzhs0jd8ny1gw863wjv3qjhqg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.61 (c (n "mit-hook-test-helper") (v "5.12.61") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "171nc46lrvy4if9ria2y3kn9310y1n5wnbcg3w6hry3qhhi83h67") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.62 (c (n "mit-hook-test-helper") (v "5.12.62") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1r6qlpkcsf4j1dnmdd5dbf3j5z3i1vl99v3gj1p326qb98zrfsvk") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.63 (c (n "mit-hook-test-helper") (v "5.12.63") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1zg8305bvm9i01xcnc54ar71rv126c7a9fprrr30davv18bbqidz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.64 (c (n "mit-hook-test-helper") (v "5.12.64") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1m5bpfcqik5gpg44r6fwriplnrq82kgwl8kxwi34pwkp4s64jgzx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.65 (c (n "mit-hook-test-helper") (v "5.12.65") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.40") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1klq6ycxf1z1cd1mly8w7cnqnghjknpj6a797f8pg93j4bv9k0zx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.66 (c (n "mit-hook-test-helper") (v "5.12.66") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "065dlijnld1rm04yh3w2r1i5f8cv57b1nj1482pgnxxhlgxv94y9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.67 (c (n "mit-hook-test-helper") (v "5.12.67") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1jry49v0kj3k4km87hvqqxbmkyr54gfimcyf91zkrx9j8dd3mgf7") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.68 (c (n "mit-hook-test-helper") (v "5.12.68") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0qcq18jxbglsrag3qab2nhy1k2mw82rigdv1hpj8f7mz7mx17h8m") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.69 (c (n "mit-hook-test-helper") (v "5.12.69") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "05fajnrsaqxchcb73q794n6s7s6gjl0ahdxfyg5g08jm3rf8chnl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.70 (c (n "mit-hook-test-helper") (v "5.12.70") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0dkr6zm6p9sfkv2lf9n8q8bdcg4wlsd42l2v9ngjwkh03h936s15") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.71 (c (n "mit-hook-test-helper") (v "5.12.71") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "03b9p9zyd53867vzb64m286rzs55p9rb7176rpzy857aqfgr0914") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.72 (c (n "mit-hook-test-helper") (v "5.12.72") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0mpzvki3qx8w8cpg1qk54ziv62i4xay8vmq76jmrvniwnmc4yds8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.73 (c (n "mit-hook-test-helper") (v "5.12.73") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1cmcgld64895bdyljrlwxd2rmx1zdarmy0q8j63cbsmyarafimr8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.74 (c (n "mit-hook-test-helper") (v "5.12.74") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0wzavq314n92r84i2i5nff96jnblh4j1bd55cas81fsnzdwyc9dg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.75 (c (n "mit-hook-test-helper") (v "5.12.75") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "08mr6ydfaa03bkl9lcqwm0ns7vqmbc8bncgqj3rjz5xip6z7zynl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.76 (c (n "mit-hook-test-helper") (v "5.12.76") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0lcas9y5zhp6zplj05kcdprcazqhab3588a3gcin5jm749zghyqy") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.77 (c (n "mit-hook-test-helper") (v "5.12.77") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ly2rgymc1isava7vs2fx9v13dkcdx0wkj5mj1mm1g7g3cr09r8p") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.78 (c (n "mit-hook-test-helper") (v "5.12.78") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1zkzv9rbv4lndw3fc0r698lnlgsk8k9ipfihvxkmria22l0jhm70") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.79 (c (n "mit-hook-test-helper") (v "5.12.79") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0fx39mz6ypa956s2ba16s2vb4m17sxwdmp6qgnafkllsf07hvzc3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.80 (c (n "mit-hook-test-helper") (v "5.12.80") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0hm3j6ll281a2mx1qlq3x5phakds8p8w5iyknmpx73vnzj392w5s") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.81 (c (n "mit-hook-test-helper") (v "5.12.81") (d (list (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0cac88ym6xhffc0l98s57k7hjl38hmaaiydmy6pywrymyrm6l25j") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.82 (c (n "mit-hook-test-helper") (v "5.12.82") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "01p7ldjh1l7y4v4ivm15rvhfyhmqi8h59cyc0bryhz0alxygii75") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.83 (c (n "mit-hook-test-helper") (v "5.12.83") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1bkjnkskpjazwll4kr4bbzi37n39rz1qf61v32hr1ijw4xqnlih6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.84 (c (n "mit-hook-test-helper") (v "5.12.84") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ajaym26r89bvdgprnq1krj0dj7gs0173rn31lcdifmx7517w6fc") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.85 (c (n "mit-hook-test-helper") (v "5.12.85") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ppv4lsssk9lq4csmfbs80vnnxhv2p7vdjr66z0gy31q1gs8jsyj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.86 (c (n "mit-hook-test-helper") (v "5.12.86") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0wh16rbhjl46mkdxqmgmnmn6lppac9nsnxfgvn915lwcim7ml8zj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.87 (c (n "mit-hook-test-helper") (v "5.12.87") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1myygyv2bzs2zyapyh7l7c8gcm1jdn8g7bydc3z91lay12ailsjz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.88 (c (n "mit-hook-test-helper") (v "5.12.88") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1yp74nvzzwxb4y63vakw2g8ik35n35f2krfgi2c8fjdpsb85cxwx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.89 (c (n "mit-hook-test-helper") (v "5.12.89") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0pl7f39kbi5wmf5gm1pqfjvbjnxlznzd6hjvqr3m8f6bvsviirfl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.90 (c (n "mit-hook-test-helper") (v "5.12.90") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "089z0s26mz9q1qslql7ir4jr37h5znnviazwfx2h0s4p2wg7pgfy") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.91 (c (n "mit-hook-test-helper") (v "5.12.91") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1g0l57ysiapdhvzhqddrj0f3mg3wlwybm2l0wmjkr9mm8x5nq211") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.92 (c (n "mit-hook-test-helper") (v "5.12.92") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1z4ngfa0f262nzgk5ghhk64n3sy4zqlv82qlfkqfn6b8qngcn97z") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.93 (c (n "mit-hook-test-helper") (v "5.12.93") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1nfwvfyacvm0wzmpv1ydax8m6k8sq5vy541i3y0634hvd72xvp3a") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.94 (c (n "mit-hook-test-helper") (v "5.12.94") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1dk7qvvx6acwg936xwdbri64nfgiz6fkz7absiyg53lmilr9d8jj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.95 (c (n "mit-hook-test-helper") (v "5.12.95") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0z9xa9ci00jrj8ixjdhv5xyhzbayi99a27kql50d0d6q3lg7832p") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.96 (c (n "mit-hook-test-helper") (v "5.12.96") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1xyr0g83cv0divbhwqw5n4jydz3s1n12vv3s89z99nh1jky3rm4g") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.97 (c (n "mit-hook-test-helper") (v "5.12.97") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0nnsqr73dv4idymyil0a68nnfk14p4cnsa3jy1f5lwcv79l2bwa2") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.98 (c (n "mit-hook-test-helper") (v "5.12.98") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "11xgq1xj58qrmpr5s38jkrww7l4q4jl1ffdhh81zflmqf5kdc2pi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.99 (c (n "mit-hook-test-helper") (v "5.12.99") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "094inb1vwswwd98xk68ci0122r0fpw63a9zy38706b662sh36yi4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.100 (c (n "mit-hook-test-helper") (v "5.12.100") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0bws5gvvasdscpma9r2nwbqdqjz7j4mpync6gqki4xns5bx45ky6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.101 (c (n "mit-hook-test-helper") (v "5.12.101") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "02ndjz6yn7pnil2rp1wp25b47jvaiwqkixad4qba92gz93dkj595") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.102 (c (n "mit-hook-test-helper") (v "5.12.102") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "051345p5lz48iflc98c5q906fba7vaz22kg5j3594m9likxd71lv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.103 (c (n "mit-hook-test-helper") (v "5.12.103") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0g0x8fzvwyffqxf1qyil6hzwbkvisvlws0qgdxk5ssfyjy9aca79") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.104 (c (n "mit-hook-test-helper") (v "5.12.104") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "105d8lbv225p68rr7w1i9yr3cynxlgk5nh71daraxxifq88066vn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.105 (c (n "mit-hook-test-helper") (v "5.12.105") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0zla9q2z2w5pnrdia1fw4pjx34skmc5v11c0f96rdzcj5y18v891") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.106 (c (n "mit-hook-test-helper") (v "5.12.106") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1bi3rzm4z98iiwq9648vbcm2y9s8z7s5mqfv38jd8z306yq5i8gk") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.107 (c (n "mit-hook-test-helper") (v "5.12.107") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1vr3vgx8xprpr8mpbri9s06m6125zij0568vqya5mw8wp626nnvn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.108 (c (n "mit-hook-test-helper") (v "5.12.108") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1yka50zzdcanpiwmasdlz94qxdpp7m73kpdm5vlib4d6zndn4ggi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.109 (c (n "mit-hook-test-helper") (v "5.12.109") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0sc5sw8yl1n6fwng2i75rjxd0pwnm7q1xrpx15v1mgncixwkw9kn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.110 (c (n "mit-hook-test-helper") (v "5.12.110") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "11bmi5x5k3a6j1xxrglv83fyrbjgq8m8kripahvqyaip8irdin1x") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.111 (c (n "mit-hook-test-helper") (v "5.12.111") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1dvax43x4imfqv7yha1h542b0sx5gbwf3ym7ziayii8aq7b665zy") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.112 (c (n "mit-hook-test-helper") (v "5.12.112") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0irvvq27zlhh2zhq5as90fq00q9lrb92mfg40m9vd618972sb4sh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.113 (c (n "mit-hook-test-helper") (v "5.12.113") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1dyb4z0pxhzbar01cfbwqsaifm3jxl7fkxnawlm29l8pm5a868k3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.114 (c (n "mit-hook-test-helper") (v "5.12.114") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.43") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1fikymjrlm7fqk0crkavzy72rczxkpv1cc02rqqr9hjsff1sfw10") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.115 (c (n "mit-hook-test-helper") (v "5.12.115") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.43") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1n599617r4lvlbilm22zwrx6ngz2gjpliw81lskhr3asfyzj7v6c") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.116 (c (n "mit-hook-test-helper") (v "5.12.116") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.44") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "17p2hd22zmbzcpcid0xm7xkc68sgr8pr2qvv3xn1a7vaxmd59467") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.117 (c (n "mit-hook-test-helper") (v "5.12.117") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.44") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "09bbldcqvglfwf0bpn3sdy9fxvzqynqii013qfj20m984z1x7xf0") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.118 (c (n "mit-hook-test-helper") (v "5.12.118") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.44") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0mbpk9kvn6g32g6sppglj19nbq1j42p9jyanmkic5qk8nd6crmld") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.119 (c (n "mit-hook-test-helper") (v "5.12.119") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1gvrh30xlzq9i2q77ncbmphccmwrdya6s0b790c3kd9zxjj7hs1f") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.120 (c (n "mit-hook-test-helper") (v "5.12.120") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1bxxlyp8mas5khvi8ja30v2327jfligrbjlaf2c2g0mhmp4ljm15") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.121 (c (n "mit-hook-test-helper") (v "5.12.121") (d (list (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1k7phfmvv0pidhwawkpjm6zkszr7chvpg2xj6gdhr0x3qnx0kajx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.122 (c (n "mit-hook-test-helper") (v "5.12.122") (d (list (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0xwlx1fxc95nzqy40qm5pgn30nl9rc79m7di015fkmh115sj4b3n") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.123 (c (n "mit-hook-test-helper") (v "5.12.123") (d (list (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0vswrw4p9rvnfh75axc47s04kfhcrwxvzbmixy3zmkbjy6w4yh53") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.124 (c (n "mit-hook-test-helper") (v "5.12.124") (d (list (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "197labyl57c4ikk5abnqd7i10h1jnv39454rdam04367psrjldhl") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.125 (c (n "mit-hook-test-helper") (v "5.12.125") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "151x20q31gixq6pf916m6yni7cg0glkga5af7d6asxrqqiiga3b1") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.126 (c (n "mit-hook-test-helper") (v "5.12.126") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1v9xn6yrqm791zn95x7n40nlkjg3y07mdirbs5vizrhvywydlajs") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.127 (c (n "mit-hook-test-helper") (v "5.12.127") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "16mzd0yf4haqf79a5lnr6srfi7zcs2xfwrc1ng66csj342vcg8mx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.128 (c (n "mit-hook-test-helper") (v "5.12.128") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "07f8ixwpjj7db58hbbh3m6a11ls3bb4ficrq9d7l9hr4rm6a063a") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.129 (c (n "mit-hook-test-helper") (v "5.12.129") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0qi50r78qwicvbvpvyf75p9i7qfbzs81sp8343l3hwsiy71gws6i") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.130 (c (n "mit-hook-test-helper") (v "5.12.130") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1xci1gz56973k4vc6mps6h72jmi7jdd543najwj2603nvwvmc5q6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.131 (c (n "mit-hook-test-helper") (v "5.12.131") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1483lpys4f9afq2dikdz7ssq8hwqqxllx14i9114bmpngirpzpjj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.132 (c (n "mit-hook-test-helper") (v "5.12.132") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1m2xqchdcx84iw91fkmq1vvfpdq3f3x2lxfsxil14knaxw5r4cm5") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.133 (c (n "mit-hook-test-helper") (v "5.12.133") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0br9bfg21xl7lc3d2aiknbn2p3b0ls65vh9l5ha7c4pxfmyl92ym") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.134 (c (n "mit-hook-test-helper") (v "5.12.134") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1ldcgj6xh70il1kprlx4ndq8dsppvm2xshz4p5m115708xx34jg2") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.135 (c (n "mit-hook-test-helper") (v "5.12.135") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0zmfc2az42z26kx2vpv74na1sjns9dkmr8c7nbf59ra34z2awcg2") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.136 (c (n "mit-hook-test-helper") (v "5.12.136") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0dnfiikfvjd2f5sr1x5y7df8ln0v9f6mjxx4qcxgy93lr7z7d8bn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.137 (c (n "mit-hook-test-helper") (v "5.12.137") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1mqr29z0xnp1ps8pd10hq7y0qi42jf0ikhqk58l9hp5vglgfrh48") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.138 (c (n "mit-hook-test-helper") (v "5.12.138") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1mvcdiq4sgsmvwy23fbhcz4n1b6fbmr4bb4ydc6n8wndpa5qajmm") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.139 (c (n "mit-hook-test-helper") (v "5.12.139") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0znxs2c1vql9g2pvlbzzi4a66lksd1mk66i2v9185nsg9yv3f4r9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.140 (c (n "mit-hook-test-helper") (v "5.12.140") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1rzjvwvw16s3i0pdidc29q47bq92a98ajbb97cpf16xkn5gzdwd3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.141 (c (n "mit-hook-test-helper") (v "5.12.141") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1w3r63267absm442zhpxsbm6izqsr7l3nfi3656v9n1hc2s0cq0r") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.142 (c (n "mit-hook-test-helper") (v "5.12.142") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1xjpxxz0p6s5f1lw9w4w8mzc0g3y821vw1lvylrhqamhx6jsp4z0") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.143 (c (n "mit-hook-test-helper") (v "5.12.143") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0ap3xr718k19pxa3gq9jjjqz97y1a15gdy81s98mfj7c89gf45xm") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.144 (c (n "mit-hook-test-helper") (v "5.12.144") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1i3mdl8b2b9l8lmz0n7v0snplvv1gs8cgl994691zjgd0c9dwc3x") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.145 (c (n "mit-hook-test-helper") (v "5.12.145") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1m6539b9p0nr33nf1krinzl1p6najslfxs3rc5s1a42bamp5z4gs") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.146 (c (n "mit-hook-test-helper") (v "5.12.146") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0f278ky43y1556pp6p8w85frm1hdg5aqzys1v4wz9ksfsgy8821h") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.147 (c (n "mit-hook-test-helper") (v "5.12.147") (d (list (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "02w6gycyk31y3rar207pqwwx666jxkq84r6blb193v3sxhh8iidz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.148 (c (n "mit-hook-test-helper") (v "5.12.148") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "16lv8xx5cxwk1ywagfi9h4dsy7wl2azdapiv1lvwkx503i1iqbss") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.149 (c (n "mit-hook-test-helper") (v "5.12.149") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "18bb82srikifhfprdazfzxlldyav6qr1j5jw2akknvrs2cq3rhvh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.150 (c (n "mit-hook-test-helper") (v "5.12.150") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0738y26n19lc6im9qmk408kclf792bsc9la9mmqkd1rdficr6yhi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.151 (c (n "mit-hook-test-helper") (v "5.12.151") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0qr9wbzdjsjn2cr951d3k3iz1592nan70blvhgimdgyp08g8q3kr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.152 (c (n "mit-hook-test-helper") (v "5.12.152") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0lysvzplpvdvhxcirkpk8ph8lnikbryy9l115cx8h8mxnx44v9x8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.153 (c (n "mit-hook-test-helper") (v "5.12.153") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0hhsqb24cj994jhcrdanna6254hysr9fxzqi742rj03zwlil68wv") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.154 (c (n "mit-hook-test-helper") (v "5.12.154") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "009px8212yj597001vjzj0lydvs5r54glvg5pmpr8yjpz8aw2784") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.155 (c (n "mit-hook-test-helper") (v "5.12.155") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1zyqskrd7wb6dc5p30yxaa783k09k4icivrgyixalv18nprjr00j") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.156 (c (n "mit-hook-test-helper") (v "5.12.156") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "19xbrbjfy0d5jk8b9ss5j117rg2j6rzcik2gl3j8gx4p203lvfxf") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.157 (c (n "mit-hook-test-helper") (v "5.12.157") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1mw0dy4bvcjgxxzcggx6nbmriizx4s5q3nrrkhzslm62hvzdlc9r") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.158 (c (n "mit-hook-test-helper") (v "5.12.158") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1jspjvsya6dib3p33v4acf3rc9imaarkpk7caxajbv4ab65mx24v") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.159 (c (n "mit-hook-test-helper") (v "5.12.159") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1gc88byfcgsm0xmn6c0a95812yvcfxk5g20k8pjgz9h8ak3psbm9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.160 (c (n "mit-hook-test-helper") (v "5.12.160") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "08p7ip42qrm0d232z6a0l6v1wv1dn4kafnax2hzvx2inlcms2ap8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.161 (c (n "mit-hook-test-helper") (v "5.12.161") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1mimjmdqfhck7pb193gbs9vg78g62p0l3c16czamwssi9xaxnpv8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.162 (c (n "mit-hook-test-helper") (v "5.12.162") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1f2ifnaibfpai0xsnixrn47cix7c2w7gjpa616mrazr443h8lkz1") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.163 (c (n "mit-hook-test-helper") (v "5.12.163") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "04m15ynvpjsqhy12vxb25cahx6d7s7kq0dc3ymmn48c1yr6vyhnh") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.164 (c (n "mit-hook-test-helper") (v "5.12.164") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0i1bg5k0fqf6ab791jnj84w2bn1zy04rcpdbg8hjaakv177qgivq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.165 (c (n "mit-hook-test-helper") (v "5.12.165") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "185bbmfbjcaacvziwf8gyxxmb51qfvwzl8jbw608n4qm7par65a4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.166 (c (n "mit-hook-test-helper") (v "5.12.166") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "06j228mw7jmxbnilfpxn7idw6db6x6kq5g9dzqi3m14hzg0q84f2") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.167 (c (n "mit-hook-test-helper") (v "5.12.167") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "199wk8i3b0kjzi3sp6wcddgi0vfvaql39c0d9pj0zfyx60p5q94a") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.168 (c (n "mit-hook-test-helper") (v "5.12.168") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0k704jjm2p2xv8klbzafmdlbka4s31fg76xl4ckvm8ni1ygqvwsa") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.169 (c (n "mit-hook-test-helper") (v "5.12.169") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0gagja20vg1nq3m1afgyx3gzc3w26zkr1vlxvc8q855hm53nq0qr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.170 (c (n "mit-hook-test-helper") (v "5.12.170") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.58") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0122ywl88m48zk36ky4yk991ab7qlflp3sid8wybkr52mb6w2cdr") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.171 (c (n "mit-hook-test-helper") (v "5.12.171") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "054sabfxrxhvxw7v2x3m2d8ga1gsh2if93k0qx8a5rk4a211l2g3") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.172 (c (n "mit-hook-test-helper") (v "5.12.172") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0cmy2jzbrwpxh3fzqzk8hwq29f81ga2v3ks88fdjcqdjg32ri30i") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.173 (c (n "mit-hook-test-helper") (v "5.12.173") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0fx9n5kkq0f01mj463zbng8rs4cllwf0yamqhj5rvm6d40j9x9a9") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.174 (c (n "mit-hook-test-helper") (v "5.12.174") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "1wnhk0hmxcy9qbifqhw4mww8f5bwn570882xfc2nhzk3k6l3hvy6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.175 (c (n "mit-hook-test-helper") (v "5.12.175") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "1bshh9a5y0f51k9n3v9mmylrxjpb3hmhsh7amgki11la9wnx688l") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.176 (c (n "mit-hook-test-helper") (v "5.12.176") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.59") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0lsrlq96zkf8km6c8b8j9c2g7mxn6ab73f5wwjcvs6i4yxlq22as") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.177 (c (n "mit-hook-test-helper") (v "5.12.177") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.60") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "04513l1b7d40r6kyrxyixlxf9kngwgq7g8vkrv8fpr6frml7vm60") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.178 (c (n "mit-hook-test-helper") (v "5.12.178") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.60") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0jbcm41ikiknj0fvdj8ha07pw9ia5v0yihj1lnib82s9jqznd4d8") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.179 (c (n "mit-hook-test-helper") (v "5.12.179") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.61") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "09ny1lcvb1w9n5919f4zlbm4n8namm16klibv5a28ivmw7aa41jg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.180 (c (n "mit-hook-test-helper") (v "5.12.180") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.61") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0gsv9d7f51yvjbm0w5x33rs5gx2qf4csq7cnmlic2cs9dsv87v4b") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.181 (c (n "mit-hook-test-helper") (v "5.12.181") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.61") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "1afmli7hdaqy8pzj7xdrgsh31wx0bqwjmlpprza07awwshyggwm4") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.182 (c (n "mit-hook-test-helper") (v "5.12.182") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.62") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "097gi2rq83bap7syrzjfizzd7dlwdqn3w5pqn69hjhdjpldmh93z") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.183 (c (n "mit-hook-test-helper") (v "5.12.183") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.62") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1bjjbji3jhbq2zm9ihj62mgwr2ln86a1z0p725lipf01yw947gvq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.184 (c (n "mit-hook-test-helper") (v "5.12.184") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.62") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "07wwbpkbgk7jjsr4zdzd7vrg9nzwppzfmj6a4irmy92aacrsmpwn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.185 (c (n "mit-hook-test-helper") (v "5.12.185") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.62") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1drg9zhyapmw0xnw5jziczm1rcdw4krll5aq1q46n0k2spvpp3gg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.186 (c (n "mit-hook-test-helper") (v "5.12.186") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "19q01b1scdk1gnnr1q9rx0qh2b8863sxvl55a359afaawppl3x6r") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.187 (c (n "mit-hook-test-helper") (v "5.12.187") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1qrr3lj8yfpl5m6lcqm4c4q3xpk2kn40kl27q98dl3hy8b5icfyb") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.188 (c (n "mit-hook-test-helper") (v "5.12.188") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1r9m4cflvihn28l5i1khrppdxl5n1ip75xkxkx7ff4pjxz1qmq9i") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.189 (c (n "mit-hook-test-helper") (v "5.12.189") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1shisiqmja5rhlyz1qxw63ivx362rky6h695nxwwcbgny74akrvn") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.190 (c (n "mit-hook-test-helper") (v "5.12.190") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "006lbs3zc8r9vfnk6v14ggkrl1fd43d0cicqmhla534a8jy54cpq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.191 (c (n "mit-hook-test-helper") (v "5.12.191") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1gjf99g7hdgyikbraifwxm76nb2lkm7jzby6zdy81mjmz52jlvdj") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.192 (c (n "mit-hook-test-helper") (v "5.12.192") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1kg3xh026241pa8nnvq4yriccfkmj3cyqhxpgpjfh6d2bx1kpvyd") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.193 (c (n "mit-hook-test-helper") (v "5.12.193") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0lhygmqvnb9syrk95c157xn8wwlvl6pv09hkjk8kfnwd3wb5w6vk") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.194 (c (n "mit-hook-test-helper") (v "5.12.194") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0a1vyhgdf8akjhbn1hsa88ky38cy4d0ln70ls7b0m9pf58i2qr5v") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.195 (c (n "mit-hook-test-helper") (v "5.12.195") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1yhrfqapd4pkbp6awqvvq648yq65d85xy67a1qvk8vvkjcjj23fi") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.196 (c (n "mit-hook-test-helper") (v "5.12.196") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0l6pnrxi7p3c835abifslmcr1x8x599nl9fifnhdp94b291wkwlg") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.197 (c (n "mit-hook-test-helper") (v "5.12.197") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0slv5rccl79xj0vc1cg2nz11h1yd6m0fafy400012skdxp6wgx25") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.198 (c (n "mit-hook-test-helper") (v "5.12.198") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1sy16jcd1a4g93plh3hf16qznnnjgjqk78qpipd25hn4pjaqaiv6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.199 (c (n "mit-hook-test-helper") (v "5.12.199") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "03cd8r668g0if2dsv5nw20f8rpymgzq5l3gqnji35qd0q5cx03mx") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.200 (c (n "mit-hook-test-helper") (v "5.12.200") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "1z2z20mhpajcf1598rl235qpj94l69pd84yz2x5xp4zm38hnsmbq") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-mit-hook-test-helper-5.12.201 (c (n "mit-hook-test-helper") (v "5.12.201") (d (list (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)))) (h "0bgpnivzxm8dn4vlq94a1jblagrkdzb1vgsi0nv7hk23ra37r498") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

