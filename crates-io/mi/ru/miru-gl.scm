(define-module (crates-io mi ru miru-gl) #:use-module (crates-io))

(define-public crate-miru-gl-0.1.0 (c (n "miru-gl") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14") (k 1)))) (h "04yizjw7py23wyyvvlngnwly2yf9pfpfwgrpjzrllab53msrg3si")))

(define-public crate-miru-gl-0.1.1 (c (n "miru-gl") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.14") (k 1)))) (h "1ap95lszw0prmng6qvdj5c224ashzxh8jh0n43nv5s646z7kgb0s")))

