(define-module (crates-io mi de miden-hir-type) #:use-module (crates-io))

(define-public crate-miden-hir-type-0.0.0 (c (n "miden-hir-type") (v "0.0.0") (h "1lr962lqwfwcv18cjn6rp5w7y435dhr34fiqzszjzcy9hm3ya7yz") (r "1.78")))

(define-public crate-miden-hir-type-0.1.0 (c (n "miden-hir-type") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.9") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "1w4ycmqq6b75s9maxjmx1n54dfci9pf70zlh6g0cf2nf3rv9kha0") (r "1.71")))

