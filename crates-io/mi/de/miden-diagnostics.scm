(define-module (crates-io mi de miden-diagnostics) #:use-module (crates-io))

(define-public crate-miden-diagnostics-0.1.0 (c (n "miden-diagnostics") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "codespan") (r "^0.11") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "flurry") (r "^0.4") (d #t) (k 0)) (d (n "miden-diagnostics-macros") (r "^0.1") (d #t) (k 0) (p "miden-diagnostics-macros")) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1w8cab12yn5n7c44m59rwgwam5ha2fakla69yk7sp6ragicq4flz") (r "1.67")))

