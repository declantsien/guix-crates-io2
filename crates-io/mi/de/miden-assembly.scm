(define-module (crates-io mi de miden-assembly) #:use-module (crates-io))

(define-public crate-miden-assembly-0.1.0 (c (n "miden-assembly") (v "0.1.0") (d (list (d (n "vm-core") (r "^0.1") (k 0) (p "miden-core")) (d (n "winter-utils") (r "^0.2") (k 0) (p "winter-utils")))) (h "1fl03nrc2whsp6bg2pb0mb0kk94fwjxanpz1pfqimfgkfx8vwq63") (f (quote (("std" "vm-core/std" "winter-utils/std") ("default" "std"))))))

(define-public crate-miden-assembly-0.2.0 (c (n "miden-assembly") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "vm-core") (r "^0.2") (k 0) (p "miden-core")) (d (n "vm-stdlib") (r "^0.1") (k 0) (p "miden-stdlib")))) (h "1akvrsvc5yppj004iqrrmrn3z461cpkak1x0ky6fw2rv2r5i2n5f") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.62")))

(define-public crate-miden-assembly-0.3.0 (c (n "miden-assembly") (v "0.3.0") (d (list (d (n "crypto") (r "^0.4") (k 0) (p "winter-crypto")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "vm-core") (r "^0.3") (k 0) (p "miden-core")))) (h "1p2rj8q6x7q6vhh92d6kvgnlirl37m628sni382z6p5x1hbfqkm0") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.62")))

(define-public crate-miden-assembly-0.4.0 (c (n "miden-assembly") (v "0.4.0") (d (list (d (n "num_enum") (r "^0.5.10") (d #t) (k 0)) (d (n "vm-core") (r "^0.4") (k 0) (p "miden-core")))) (h "1xav8r44c998hp0jb4q13kzajdlx1a8x9bnpx43323f8m4n6fna6") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.67")))

(define-public crate-miden-assembly-0.5.0 (c (n "miden-assembly") (v "0.5.0") (d (list (d (n "num_enum") (r "^0.5.10") (d #t) (k 0)) (d (n "vm-core") (r "^0.5") (k 0) (p "miden-core")))) (h "00qv50w4dvfa15p8c39vgdmxcvq8kkbmihir7adf8fw5jqcwh32i") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.67")))

(define-public crate-miden-assembly-0.6.0 (c (n "miden-assembly") (v "0.6.0") (d (list (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "vm-core") (r "^0.6") (k 0) (p "miden-core")))) (h "114pflwzgh7hch2bwrxbdvgfkrwc43pahcr6ypfhxpmrzcnkkrsg") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.67")))

(define-public crate-miden-assembly-0.6.1 (c (n "miden-assembly") (v "0.6.1") (d (list (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "vm-core") (r "^0.6") (k 0) (p "miden-core")))) (h "01w94dmrxc4w4jh27sbwzjx4rc2ajim90422h0d8dsd4qalzql7k") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.67")))

(define-public crate-miden-assembly-0.7.0 (c (n "miden-assembly") (v "0.7.0") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "vm-core") (r "^0.7") (k 0) (p "miden-core")))) (h "1sngnjcf4h1sd5nykq4nvyv5qm6hjqixx69cka35ydz77pg3lj3c") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.73")))

(define-public crate-miden-assembly-0.8.0 (c (n "miden-assembly") (v "0.8.0") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "vm-core") (r "^0.8") (k 0) (p "miden-core")))) (h "1xcvs18npb1vl2x4syf3sh3cy6a4w2s8xzyx0h058gjrnb062hkz") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.75")))

(define-public crate-miden-assembly-0.9.0 (c (n "miden-assembly") (v "0.9.0") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "vm-core") (r "^0.9") (k 0) (p "miden-core")))) (h "00pbdixk23qs546dnivy0i5r7vj24vvi7i5kk3iw1kcqhv9myg1f") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.75")))

(define-public crate-miden-assembly-0.9.1 (c (n "miden-assembly") (v "0.9.1") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "vm-core") (r "^0.9") (k 0) (p "miden-core")))) (h "121r98gayv9b17fk7zb2qrl38875g4jf1gf58md2bcc3l29vynh1") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.75")))

(define-public crate-miden-assembly-0.9.2 (c (n "miden-assembly") (v "0.9.2") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "vm-core") (r "^0.9") (k 0) (p "miden-core")))) (h "09avsdq4anzv0zqay9099ycqdxsp85603gd8brji9m0q7h5c49jv") (f (quote (("std" "vm-core/std") ("default" "std")))) (r "1.75")))

