(define-module (crates-io mi de miden-gpu) #:use-module (crates-io))

(define-public crate-miden-gpu-0.1.0 (c (n "miden-gpu") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "metal") (r "^0.27") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"macos\"))") (k 0)) (d (n "miden-crypto") (r "^0.9") (d #t) (k 2)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "winter-crypto") (r "^0.8") (d #t) (k 2)) (d (n "winter-math") (r "^0.8") (d #t) (k 0)))) (h "1lxvmsri1alvlmczv4jgsjwpcs1y5wwr4irnfpszvkxjvbg3f5v9") (r "1.75")))

