(define-module (crates-io mi de miden-formatting) #:use-module (crates-io))

(define-public crate-miden-formatting-0.1.0 (c (n "miden-formatting") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1s85jnggnpqj90vvqivrgmnb4jkl56284fj0wlv852almcbx0ra9") (f (quote (("std") ("default" "std")))) (r "1.75")))

(define-public crate-miden-formatting-0.1.1 (c (n "miden-formatting") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0fr6dv6biy4pkcksr2hg9zqpp658bzirshrb05qjdcrlih52wfby") (f (quote (("std") ("default" "std")))) (r "1.75")))

