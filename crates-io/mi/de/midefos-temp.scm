(define-module (crates-io mi de midefos-temp) #:use-module (crates-io))

(define-public crate-midefos-temp-0.1.0 (c (n "midefos-temp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "05p2dlw5c71lrwxajnh8lm1gqq2wn1lfi7v6rc5i0j3ndm8a2pay") (y #t)))

