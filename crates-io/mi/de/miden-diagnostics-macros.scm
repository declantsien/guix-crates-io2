(define-module (crates-io mi de miden-diagnostics-macros) #:use-module (crates-io))

(define-public crate-miden-diagnostics-macros-0.1.0 (c (n "miden-diagnostics-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0qpzxllp3jyccj14dvz9anpjqrnmz5vvysfzriksf6r0xfq107a9") (r "1.67")))

