(define-module (crates-io mi de miden-parsing) #:use-module (crates-io))

(define-public crate-miden-parsing-0.1.0 (c (n "miden-parsing") (v "0.1.0") (d (list (d (n "miden-diagnostics") (r "^0.1") (d #t) (k 0) (p "miden-diagnostics")) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "120srmhay2cbdv44h0fqprvj08d0ja7k29rnm1rkg6riq31gwvfk") (r "1.67")))

