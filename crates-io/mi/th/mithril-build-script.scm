(define-module (crates-io mi th mithril-build-script) #:use-module (crates-io))

(define-public crate-mithril-build-script-0.1.4 (c (n "mithril-build-script") (v "0.1.4") (d (list (d (n "semver") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)))) (h "0my3kl1zwjpddxkz9b1d0qh4j3dyzn0qapcswxgb02j1h3sk6wr6")))

(define-public crate-mithril-build-script-0.2.1 (c (n "mithril-build-script") (v "0.2.1") (d (list (d (n "semver") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)))) (h "0qzs6z2rjqa54rlhg4dznqg5psirswg5ppsxfwd0048scxci5y1f")))

