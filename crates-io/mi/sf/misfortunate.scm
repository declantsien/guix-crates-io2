(define-module (crates-io mi sf misfortunate) #:use-module (crates-io))

(define-public crate-misfortunate-0.1.0 (c (n "misfortunate") (v "0.1.0") (h "1h1sz3z511yjab8nisv3q9bb5ng446vbqr815smf8cmyv1vnvn8s")))

(define-public crate-misfortunate-0.2.0 (c (n "misfortunate") (v "0.2.0") (h "1kyzh2mxv2g1fv8xjlmzjrvdjjwdrba3nvlgmxmwqy59z87hjva3")))

(define-public crate-misfortunate-1.0.0 (c (n "misfortunate") (v "1.0.0") (h "17ql332a8srqxmlfk67xb05z8vlljpli7d92yjplqj2lvxcfaf3h")))

