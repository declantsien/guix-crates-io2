(define-module (crates-io mi mo mimo) #:use-module (crates-io))

(define-public crate-mimo-0.1.0 (c (n "mimo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)))) (h "1w9pmcn72c18l47hrnrqf710i6fihh8kx9920762bqh6xjny68w3")))

