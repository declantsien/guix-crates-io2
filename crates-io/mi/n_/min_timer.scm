(define-module (crates-io mi n_ min_timer) #:use-module (crates-io))

(define-public crate-min_timer-0.1.0 (c (n "min_timer") (v "0.1.0") (h "11fljivs4sj53ylqj9hg9rzn3dwi2mnid1wn37zbqy9v8h9kpb5m")))

(define-public crate-min_timer-0.2.0 (c (n "min_timer") (v "0.2.0") (h "0gij0f0la6wq2gjl2ll0ricfxzzzgk4j7viyljq57wlpkgq886iv")))

(define-public crate-min_timer-0.3.0 (c (n "min_timer") (v "0.3.0") (h "0pzzmpli1nrvb6ihjhkhwihany3k73f01jhkaxnrl0afwbgb7p0c")))

(define-public crate-min_timer-0.4.0 (c (n "min_timer") (v "0.4.0") (h "0dasrvz5ic141a9sijrwk3b4zkd4757p2nj8ww6sbir65dhhi661") (f (quote (("stat") ("prf" "stat") ("hrt" "prf"))))))

