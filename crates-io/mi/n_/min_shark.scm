(define-module (crates-io mi n_ min_shark) #:use-module (crates-io))

(define-public crate-min_shark-0.1.0 (c (n "min_shark") (v "0.1.0") (d (list (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "17j7r7fh72a0dvc1zldafkkfa85p115dkhgn4rnlcihm88rgvw0q")))

(define-public crate-min_shark-0.2.0 (c (n "min_shark") (v "0.2.0") (d (list (d (n "bstr") (r "^1.6") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0zfjl0f6ajy3hbca0zx2naqnbw8yigks6170f05vymhxpbcj4z43")))

(define-public crate-min_shark-0.4.0 (c (n "min_shark") (v "0.4.0") (d (list (d (n "bstr") (r "^1.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipnet") (r "^2.9") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1idvyhj1yzc8cz2nik4ndx41kz3hx99igca3qcc2ci6g0fmn1n70")))

(define-public crate-min_shark-0.5.0 (c (n "min_shark") (v "0.5.0") (d (list (d (n "bstr") (r "^1.9") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ipnet") (r "^2.9") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1pvp4r74brmk21c9sjhb41np54m57r8qipb8malywcdjv4a16jb6")))

