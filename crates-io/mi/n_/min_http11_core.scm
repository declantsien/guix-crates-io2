(define-module (crates-io mi n_ min_http11_core) #:use-module (crates-io))

(define-public crate-min_http11_core-0.0.1 (c (n "min_http11_core") (v "0.0.1") (d (list (d (n "crc32fast") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1xfq8pssfcd7ak6gv571y6sqd9j2p4rmhr8rib4h3lbyxr50c9pj") (f (quote (("default") ("_minimal"))))))

(define-public crate-min_http11_core-0.1.0 (c (n "min_http11_core") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "09cx7fh3wzppjaysykl8v9lw2dgb899gc4si2r98d8si7358ql7c") (f (quote (("default") ("_minimal"))))))

(define-public crate-min_http11_core-0.1.1 (c (n "min_http11_core") (v "0.1.1") (d (list (d (n "crc32fast") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1axlzpqg9dwy39ndc97wcfpqb0dw76hcvp704vc16dn96i5q0xdh") (f (quote (("default") ("_minimal"))))))

