(define-module (crates-io mi n_ min_max_macros) #:use-module (crates-io))

(define-public crate-min_max_macros-0.1.0 (c (n "min_max_macros") (v "0.1.0") (h "0skj9g6a0a28sfw40ryjbbiq4p2rqggkpgr3wl3zklx1yhagn390")))

(define-public crate-min_max_macros-0.1.1 (c (n "min_max_macros") (v "0.1.1") (h "1wmr2p2byh8kz0mqadjr1sy1ksqf9jps7qs6jv7649jy23z786hk")))

