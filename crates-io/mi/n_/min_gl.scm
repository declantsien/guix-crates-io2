(define-module (crates-io mi n_ min_gl) #:use-module (crates-io))

(define-public crate-min_gl-0.1.0 (c (n "min_gl") (v "0.1.0") (d (list (d (n "glfw") (r "^0.43.0") (d #t) (k 0)))) (h "103y6dwqmprcaz78wbw2amn95nyr64gkig5zrjypzlqsliyf0p0j")))

(define-public crate-min_gl-0.1.1 (c (n "min_gl") (v "0.1.1") (d (list (d (n "glfw") (r "^0.43.0") (d #t) (k 0)))) (h "06a4l0h58vkc07857i18wpissgwpb652crxm380sl0kklfv4kf1p")))

(define-public crate-min_gl-0.2.0 (c (n "min_gl") (v "0.2.0") (d (list (d (n "glfw") (r "^0.43.0") (d #t) (k 0)))) (h "1g4jhkn71q2ais0zma2gimdy109n0d2sbrj8xqf6dddhbg6cpz2n")))

(define-public crate-min_gl-0.3.0 (c (n "min_gl") (v "0.3.0") (d (list (d (n "glfw") (r "^0.43.0") (d #t) (k 0)) (d (n "min_timer") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "049c97vzvhf4mwlr8aaj0picimynm3s3hj14cwr52m4fwxia820n")))

