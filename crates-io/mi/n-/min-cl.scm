(define-module (crates-io mi n- min-cl) #:use-module (crates-io))

(define-public crate-min-cl-0.1.0 (c (n "min-cl") (v "0.1.0") (h "0l0699axg1xz9cjxbar84ssv71d5glci5hlxaik4g7akb9k09vxl")))

(define-public crate-min-cl-0.1.1 (c (n "min-cl") (v "0.1.1") (h "13pgisvx5jy7qjxpx0awx0p0qxqs6n934k7b6nr94yp7smr4lwbb")))

(define-public crate-min-cl-0.1.2 (c (n "min-cl") (v "0.1.2") (h "0rh45s3rd7lld6j46jg1yi91kc9a05qbi908zvwbqxl22ny506w3")))

(define-public crate-min-cl-0.1.3 (c (n "min-cl") (v "0.1.3") (h "1hwq2xb3y23mpb77wj8ym1fk4gcha1w0d9gqadfh4a052amz9in4")))

(define-public crate-min-cl-0.2.0 (c (n "min-cl") (v "0.2.0") (h "0cr5bpph7ag1a59nkp0zl6xw8pmnf2yxgdf2fcqvgfmrhzk5wfb5")))

(define-public crate-min-cl-0.3.0 (c (n "min-cl") (v "0.3.0") (h "00m89az1ncxi0vc7f6icy508adjqcs3f9hcj5gwr8wzabwcpp1i7")))

