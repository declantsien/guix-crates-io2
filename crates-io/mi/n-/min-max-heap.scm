(define-module (crates-io mi n- min-max-heap) #:use-module (crates-io))

(define-public crate-min-max-heap-0.1.0 (c (n "min-max-heap") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "16a85qvz9qxh2vy7izy696fhlf8z5sk5wqrxr5lsz9w6d6rzxz1m")))

(define-public crate-min-max-heap-0.1.1 (c (n "min-max-heap") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0jskyj7pi7xcch3l8yvk2gdjgi4hzgmi1lrrs814disyyvvv6m71")))

(define-public crate-min-max-heap-0.1.2 (c (n "min-max-heap") (v "0.1.2") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14libwbc7fl6l9lyfj31whxhqxcrjbq880z4v42zgl8mvlcmvjcz")))

(define-public crate-min-max-heap-0.2.0 (c (n "min-max-heap") (v "0.2.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1bzzpqf09bswyy0pd8ic4pmb9pr0srszaa9ngi3qy7hp7i7s08g2")))

(define-public crate-min-max-heap-0.2.1 (c (n "min-max-heap") (v "0.2.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "19pda89a49jl8rgm83walyg7l3pwpvhrww611x8b5smydzqln8l2")))

(define-public crate-min-max-heap-1.0.0 (c (n "min-max-heap") (v "1.0.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1wfs3kihny2c0vwb8j44smlq3z02lgs56jxncjgl8fy1zxzb7y0x")))

(define-public crate-min-max-heap-1.0.1 (c (n "min-max-heap") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1mil6w57474ldsphjqm7b0w0ybb27x331g1j2r4727agylj18jhc")))

(define-public crate-min-max-heap-1.0.2 (c (n "min-max-heap") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0kfn4l7jz9wbfgzmh5h1wr6lslmqbcn3c95d9xc6s9dwxh4wvpq2")))

(define-public crate-min-max-heap-1.0.3 (c (n "min-max-heap") (v "1.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0wzpkfsvcd4v41675qq7sh6q3hhdcca7i7d9a28l8zz14ic9pxv0")))

(define-public crate-min-max-heap-1.0.4 (c (n "min-max-heap") (v "1.0.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0c91snb8nl4znbjg7di0lvfpq91rmkyhgg3wv6mcrjba82v7pk57")))

(define-public crate-min-max-heap-1.1.0 (c (n "min-max-heap") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "118n9yprr25jmvbq51rkzd8r3lcgwjnj4v6kp5m5sy9wr8dsnn17")))

(define-public crate-min-max-heap-1.1.1 (c (n "min-max-heap") (v "1.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k3pkyjj8br072n15mvaqrj3p0h2ph18zh5z5hvqil5q6r9561ji")))

(define-public crate-min-max-heap-1.2.0 (c (n "min-max-heap") (v "1.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zpprggyvbfkia5c5dqbk9x3k3ib6k18fm8nlwb4zzqd5izyywlf")))

(define-public crate-min-max-heap-1.2.1 (c (n "min-max-heap") (v "1.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05n790jr9bsy7568cz6naq77wknz162al55rz8b9arw7knsawk3b")))

(define-public crate-min-max-heap-1.2.2 (c (n "min-max-heap") (v "1.2.2") (d (list (d (n "quickcheck") (r "^0.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rgdjsw7j6n99bfw4sap5zgzpn5ps05x39yrbp32fc7das7cki7l")))

(define-public crate-min-max-heap-1.3.0 (c (n "min-max-heap") (v "1.3.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "060bwpg7zw65yjfvyfklqwyd0hgk5rgx37yghj98xx00kk7yd1r6")))

