(define-module (crates-io mi n- min-sqlite3-sys) #:use-module (crates-io))

(define-public crate-min-sqlite3-sys-0.1.0 (c (n "min-sqlite3-sys") (v "0.1.0") (d (list (d (n "sqlite3-builder") (r "^3.38.1") (d #t) (k 0)))) (h "0bg4j1kxsvll97p1fn1appxpk0k6zpy2xhrpkr077akp1j68njj1") (y #t)))

(define-public crate-min-sqlite3-sys-0.1.1 (c (n "min-sqlite3-sys") (v "0.1.1") (d (list (d (n "sqlite3-builder") (r "^3.38.1") (d #t) (k 0)))) (h "0v97p3613jd516y19n399ca98i0dikn74mxf3h2fq1n0gz0klk0w") (y #t)))

(define-public crate-min-sqlite3-sys-1.0.0 (c (n "min-sqlite3-sys") (v "1.0.0") (d (list (d (n "sqlite3-builder") (r "^3.38.2") (d #t) (k 0)))) (h "1bw6y110w116qw50kkl5gijhs6ljrdnvq9v600289xnqlfbyaig4") (y #t)))

(define-public crate-min-sqlite3-sys-1.0.1 (c (n "min-sqlite3-sys") (v "1.0.1") (d (list (d (n "sqlite3-builder") (r "^3.38.3") (d #t) (k 0)))) (h "0188pq6sy1423qj0f48qvrlmlmhzb0shwmg4qz0hm7sa9hzmln2w") (y #t)))

(define-public crate-min-sqlite3-sys-1.1.0 (c (n "min-sqlite3-sys") (v "1.1.0") (d (list (d (n "sqlite3-builder") (r "^3.38.4") (d #t) (k 0)))) (h "0pp1mhla04hsr735z9hr8caym4ndrd52xh5x1wpv154xmgfyvldq") (y #t)))

(define-public crate-min-sqlite3-sys-1.1.1 (c (n "min-sqlite3-sys") (v "1.1.1") (d (list (d (n "sqlite3-builder") (r "^3.38.5") (d #t) (k 0)))) (h "10mmfsdzl4asm7qfj2nv5zk4hjv5md35yv41zv4kmk7ysfncffmr")))

(define-public crate-min-sqlite3-sys-1.2.0 (c (n "min-sqlite3-sys") (v "1.2.0") (d (list (d (n "sqlite3-builder") (r "^3.38.6") (d #t) (k 0)))) (h "0k5fj0rxiy6c98gmwpgnq8fkvigjnp1msgqaq86vq7hjanzsr7c4")))

(define-public crate-min-sqlite3-sys-1.3.0 (c (n "min-sqlite3-sys") (v "1.3.0") (d (list (d (n "sqlite3-builder") (r "^3.38.6") (d #t) (k 0)))) (h "1xp7qnfih9s3sgqp7mrd2awcarwzzvc45jnp3nj3c7l75w1785f0")))

(define-public crate-min-sqlite3-sys-1.3.1 (c (n "min-sqlite3-sys") (v "1.3.1") (d (list (d (n "sqlite3-builder") (r "^3.38.6") (d #t) (k 0)))) (h "0mzvxbsjmkz544mq3r8kgqk0daprv8hkbfy8cjr3qykl9qs9p79c")))

(define-public crate-min-sqlite3-sys-1.3.2 (c (n "min-sqlite3-sys") (v "1.3.2") (d (list (d (n "sqlite3-builder") (r "^3.38.6") (d #t) (k 0)))) (h "1jjrw3hvc3kgbnv23fv5hzxzfyfgqpcyzdjiavkyclc0avgri7v1")))

(define-public crate-min-sqlite3-sys-1.4.0 (c (n "min-sqlite3-sys") (v "1.4.0") (d (list (d (n "sqlite3-builder") (r "^3.39.4") (d #t) (k 0)))) (h "0mp9ykpsig3vjkv9dnv3q9cqln0liry9bli23dkpcg0aqagc10bz")))

(define-public crate-min-sqlite3-sys-1.4.1 (c (n "min-sqlite3-sys") (v "1.4.1") (d (list (d (n "sqlite3-builder") (r "^3.39.4") (d #t) (k 0)))) (h "1mbprzmclhkf8f42c1ixzyvy6zqci808l3h858ad8hfyvlghqi5r")))

(define-public crate-min-sqlite3-sys-1.4.2 (c (n "min-sqlite3-sys") (v "1.4.2") (d (list (d (n "sqlite3-builder") (r "^3.39.4") (d #t) (k 0)))) (h "0iby1bypjr88in8skja5rhk8gss218gix6880iyz878cmpr4a2a3")))

(define-public crate-min-sqlite3-sys-1.4.3 (c (n "min-sqlite3-sys") (v "1.4.3") (d (list (d (n "sqlite3-builder") (r "^3.39.4") (d #t) (k 0)))) (h "1q1lvnb12j42i4vkn049jkr8nin4jrbncqcfcyhqfk6q25lycsky")))

