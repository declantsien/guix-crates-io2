(define-module (crates-io mi n- min-tun) #:use-module (crates-io))

(define-public crate-min-tun-0.1.0 (c (n "min-tun") (v "0.1.0") (h "040mprw7ndnhqx5z710znga4qk74a887g9alngl1kw1qi56afn6z")))

(define-public crate-min-tun-0.1.1 (c (n "min-tun") (v "0.1.1") (h "0smjny1y84c8pi32p9z8g7d1qbqb4b1bg1ydn848iw8ynbh88lpb")))

(define-public crate-min-tun-0.1.2 (c (n "min-tun") (v "0.1.2") (h "1qrv39acsf4h81704dhz5ipxdxn2vp2ggyhp9h5rzmxl2mpnjf7f")))

