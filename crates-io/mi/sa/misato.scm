(define-module (crates-io mi sa misato) #:use-module (crates-io))

(define-public crate-misato-0.1.0 (c (n "misato") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0xvxbqf54a123gn85bv7ngr8g0az85f0vb1l79lm81z7h7ryr41f")))

