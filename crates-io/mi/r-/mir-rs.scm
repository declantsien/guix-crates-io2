(define-module (crates-io mi r- mir-rs) #:use-module (crates-io))

(define-public crate-mir-rs-0.1.0 (c (n "mir-rs") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.2.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "mir-sys") (r "^0.1") (d #t) (k 0)))) (h "1jrqvffhlslah6svarc504i9gs25rj72lyfjp0msbii5hpzhmryp")))

