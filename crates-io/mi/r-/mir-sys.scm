(define-module (crates-io mi r- mir-sys) #:use-module (crates-io))

(define-public crate-mir-sys-0.1.0+mir.0.1.1 (c (n "mir-sys") (v "0.1.0+mir.0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "0l5lgrc8rqjxh29f7yw3ln6iia1ifb6gv7gdily3mrz09db0gaq4") (f (quote (("parallel-gen") ("default")))) (l "mir")))

