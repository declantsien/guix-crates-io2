(define-module (crates-io mi nn minnow) #:use-module (crates-io))

(define-public crate-minnow-0.1.0 (c (n "minnow") (v "0.1.0") (d (list (d (n "arithmetic-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "minnow-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zl7rjgrxgjra4pfmhambcxfnvngfszafbr6qsyvz08qq67g4590")))

