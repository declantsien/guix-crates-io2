(define-module (crates-io mi nn minnow-derive) #:use-module (crates-io))

(define-public crate-minnow-derive-0.1.0 (c (n "minnow-derive") (v "0.1.0") (d (list (d (n "arithmetic-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.3.0") (d #t) (k 2)) (d (n "darling") (r "^0.13.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 2)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)))) (h "134abxw633ckriav5an48g055r22jna7g8hc94cfmh2bb2ww87k5")))

