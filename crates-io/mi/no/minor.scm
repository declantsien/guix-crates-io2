(define-module (crates-io mi no minor) #:use-module (crates-io))

(define-public crate-minor-0.1.0 (c (n "minor") (v "0.1.0") (h "1ld8lzhpgk7bq90arafa7vb7acczxgrnw5j96j4xv43y1zpxw0vc")))

(define-public crate-minor-0.1.1 (c (n "minor") (v "0.1.1") (h "08x0cfbzysm702w701bj3ixlklizdzdyfwd97r6l4fj8nxn52554")))

(define-public crate-minor-0.1.2 (c (n "minor") (v "0.1.2") (d (list (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)))) (h "0nnqgfqlrjmcvsdzidmvzz23cphyhaxb927igxiyqyidsq80gigw")))

