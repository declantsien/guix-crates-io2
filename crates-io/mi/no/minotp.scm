(define-module (crates-io mi no minotp) #:use-module (crates-io))

(define-public crate-minotp-0.1.0 (c (n "minotp") (v "0.1.0") (d (list (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "sha3") (r "^0.10") (k 2)))) (h "1v6qsiaq404yif42dvj3s0rh5vclrkgb3l58gwjr6mkj46c5aa1c") (y #t)))

(define-public crate-minotp-0.1.1 (c (n "minotp") (v "0.1.1") (d (list (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "sha3") (r "^0.10") (k 2)))) (h "0ddq2qld6jy6ybf1z1vgkb9mqr38a9p9pggddfcpqf8lwlcdbhvn") (y #t)))

(define-public crate-minotp-1.0.0 (c (n "minotp") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 2)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "sha3") (r "^0.10") (k 2)))) (h "052sh4npzky888znamczsxgs9s19q0cdm4wi71sril0bmbjf43w6")))

(define-public crate-minotp-1.1.0 (c (n "minotp") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "sha3") (r "^0.10") (k 2)))) (h "1lx96m6p50mdayqr09n9ymbws3knj02a1mhfm3x83ynjnxjll11i") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

