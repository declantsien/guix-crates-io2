(define-module (crates-io mi no minotaur) #:use-module (crates-io))

(define-public crate-minotaur-0.1.0 (c (n "minotaur") (v "0.1.0") (h "08a2nfqywsi8ij21daz0k2qbr9zb3pdyas7mmg9ic900rfvbwx01")))

(define-public crate-minotaur-0.2.0 (c (n "minotaur") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "16h517hl39vnqcbyf23zwg75sz5lr1msn7bzl6df8whmnxwl0vpa")))

(define-public crate-minotaur-0.3.0 (c (n "minotaur") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0cmxsch9glcbxq049ddbaxrxn5ykby1r969q73wr34f35givc6z2")))

