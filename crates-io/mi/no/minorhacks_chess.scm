(define-module (crates-io mi no minorhacks_chess) #:use-module (crates-io))

(define-public crate-minorhacks_chess-0.1.0 (c (n "minorhacks_chess") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "nodrop") (r "^0.1.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (k 1)))) (h "0b6shrjf1j9nviyir8sbm6w2q1lxrhg13sg1qmjgjn04as93vjsm")))

(define-public crate-minorhacks_chess-0.1.1 (c (n "minorhacks_chess") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "nodrop") (r "^0.1.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (k 1)))) (h "01r09hbd083kl6l6qjib148iv3l8c89il2ff1g9jhkl7divim567")))

(define-public crate-minorhacks_chess-0.1.3 (c (n "minorhacks_chess") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "nodrop") (r "^0.1.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (k 1)))) (h "16fhbkszmphnmh9yg6bm7bjc3sbfzy58a5wqbhl7799vkggwy8xd")))

