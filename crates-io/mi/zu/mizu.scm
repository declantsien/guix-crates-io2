(define-module (crates-io mi zu mizu) #:use-module (crates-io))

(define-public crate-mizu-0.1.0 (c (n "mizu") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "mizu-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15.1") (d #t) (k 0)))) (h "1pd04j3k6d12w9hagby975bn11hcfwpigmmi8h5isfswldyxxcxk")))

(define-public crate-mizu-0.1.1 (c (n "mizu") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "mizu-core") (r "^0.1.1") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15.1") (d #t) (k 0)))) (h "1y0k9ldxlzxrn8bimi6qh3q609rnlbawbvfpvvg3n5pwvb0dr9xq")))

(define-public crate-mizu-0.1.2 (c (n "mizu") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "mizu-core") (r "^0.1.2") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5.4") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml") (r "^0.15.1") (d #t) (k 0)))) (h "1irxc7wjb8mdx2kvhdwr783v3i3ngrrjlqgblzvfpbhwl52bn1ii")))

(define-public crate-mizu-0.2.0 (c (n "mizu") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "mizu-core") (r "^0.2.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5.4") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (f (quote ("graphics"))) (k 0)))) (h "0xrvb9g64f1danfr24fgffzfzvn0r4cz62n9ngarc7b121bc2qyw")))

(define-public crate-mizu-1.0.0 (c (n "mizu") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.12") (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "mizu-core") (r "^1.0.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (f (quote ("graphics"))) (k 0)))) (h "1d32g9lzk689v3fc5i2ws4ddafccbgrqrs3qwmhgx14gza6a7nxb")))

(define-public crate-mizu-1.0.1 (c (n "mizu") (v "1.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("string"))) (d #t) (k 0)) (d (n "directories-next") (r "^2.0") (d #t) (k 0)) (d (n "dynwave") (r "^0.1.0") (d #t) (k 0)) (d (n "mizu-core") (r "^1.0.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.7") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3") (d #t) (k 0)) (d (n "sfml") (r "^0.21") (f (quote ("graphics"))) (k 0)))) (h "0xrf160am2ai2p9lldh6nnpyn9jcb61jdxnzx3zskdj192700g7d")))

