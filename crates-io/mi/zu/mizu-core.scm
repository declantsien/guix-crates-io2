(define-module (crates-io mi zu mizu-core) #:use-module (crates-io))

(define-public crate-mizu-core-0.1.0 (c (n "mizu-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "fixed-vec-deque") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0bli5s1miyywcbncbz42ip83nx8fnbp2bbdl6cixdyanzz259i0v")))

(define-public crate-mizu-core-0.1.1 (c (n "mizu-core") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "fixed-vec-deque") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0mv6vn7kmrnvc75mnj55c2b3pm3b138cd1mgrkk6y0p03nqmy9d2")))

(define-public crate-mizu-core-0.1.2 (c (n "mizu-core") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "fixed-vec-deque") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0xc6rrvv77l6qdiih84mppa1yixfy9c8jygg79pavhlr9vllf8fy")))

(define-public crate-mizu-core-0.2.0 (c (n "mizu-core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "fixed-vec-deque") (r "^0.1.9") (d #t) (k 0)) (d (n "save_state") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "zstd") (r "^0.7.0") (d #t) (k 0)))) (h "0686q57ffgdab95yfff7r0cj8vsl3axxc6wvazz7sr2dyrih9ms6")))

(define-public crate-mizu-core-1.0.0 (c (n "mizu-core") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 2)) (d (n "fixed-vec-deque") (r "^0.1.9") (d #t) (k 0)) (d (n "save_state") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "zstd") (r "^0.11.0") (d #t) (k 0)))) (h "1rn2smjkqsln1jc17gszjqw8iqjlmr4v82795ygyrx7c2hj66hps")))

