(define-module (crates-io mi zu mizui) #:use-module (crates-io))

(define-public crate-mizui-0.0.1 (c (n "mizui") (v "0.0.1") (h "12vx1isndr24kcgx68w9lv8dh5z0a3ni2ybvkffmyjyzag2daqvp")))

(define-public crate-mizui-0.1.0 (c (n "mizui") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "v8") (r "^0.48.0") (d #t) (k 0)))) (h "0p2r98a2ssbki6qq3rzn9r5cimkhh2mhg3m8i3fc4ryw456drbgq") (y #t)))

