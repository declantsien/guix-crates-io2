(define-module (crates-io mi l_ mil_std_1553b_derive) #:use-module (crates-io))

(define-public crate-mil_std_1553b_derive-0.5.0 (c (n "mil_std_1553b_derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1npq7nm02f2bhj6z0m2zgxwv8wmdx4vdngd7pcnmiqgm9j9jqk51")))

