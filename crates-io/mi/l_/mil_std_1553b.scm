(define-module (crates-io mi l_ mil_std_1553b) #:use-module (crates-io))

(define-public crate-mil_std_1553b-0.2.0 (c (n "mil_std_1553b") (v "0.2.0") (h "02w94cdskmdds2vli6ggkmbls7mcdz8nrhi12dldlzrbmqhpxrbd")))

(define-public crate-mil_std_1553b-0.3.0 (c (n "mil_std_1553b") (v "0.3.0") (h "1qrjffm3vrs6lp2wbzd5c7w4i380xi16c6ap147xjxg07qjjszb9")))

(define-public crate-mil_std_1553b-0.3.1 (c (n "mil_std_1553b") (v "0.3.1") (h "06z5r3xxj65wx1wzlwkx65zjxfs6ajnp1796sn5ky7qza9glbkfr")))

(define-public crate-mil_std_1553b-0.4.0 (c (n "mil_std_1553b") (v "0.4.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "01rdb3mmzm7c64wj9kiq3inggyyxkhv4lkak87dfn69y8acb9jka")))

(define-public crate-mil_std_1553b-0.5.0 (c (n "mil_std_1553b") (v "0.5.0") (d (list (d (n "mil_std_1553b_derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0gdach764rxv014pb1vzxhjaynzrndms21yjlvhxg8wfcds323lp") (s 2) (e (quote (("derive" "dep:mil_std_1553b_derive"))))))

