(define-module (crates-io mi co micon) #:use-module (crates-io))

(define-public crate-micon-0.1.0 (c (n "micon") (v "0.1.0") (d (list (d (n "divider") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "svg") (r "^0.14") (d #t) (k 0)) (d (n "tessor") (r "^0.1") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.1") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "17md844fkyry4vn1yfwkxnkckykal0yqy690vbs9y6fl1npqw2xx")))

(define-public crate-micon-0.2.0 (c (n "micon") (v "0.2.0") (d (list (d (n "divider") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "svg") (r "^0.14") (d #t) (k 0)) (d (n "tessor") (r "^0.1") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.1") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "1g0dccfap2fvqgbm65k552pz0xbkz6ldysfwjndy0s4i1wfjldzw")))

(define-public crate-micon-0.3.0 (c (n "micon") (v "0.3.0") (d (list (d (n "divider") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "svg") (r "^0.14") (d #t) (k 0)) (d (n "tessor") (r "^0.1") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.1") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "0gldsxymyrc08fgkq42ibh8p645kqgyj5vz7lqva2dpcwqkcjqsp")))

(define-public crate-micon-0.4.0 (c (n "micon") (v "0.4.0") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "svg") (r "^0.14") (d #t) (k 0)) (d (n "tessor") (r "^0.2") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.2") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "0n2zvxczm27ss7mvnp4zmikvfdp9al22ch97np3hfl7yj0jzxmss")))

