(define-module (crates-io mi co mico) #:use-module (crates-io))

(define-public crate-mico-0.1.0 (c (n "mico") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kh81yna10k9zvixvdkr26inqv1f2803ls7mrkl039vxpnvrrz3a")))

(define-public crate-mico-0.1.1 (c (n "mico") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yp1aaaalkqkfi1kwkk06i2mfkgr7qb14kn8cgk4v3bd44kbzalp")))

(define-public crate-mico-0.1.2 (c (n "mico") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ckfv5ajm8kjsh2646hq6vjwvbd5s56b77745f5iw80b61bydzfq")))

(define-public crate-mico-0.1.3 (c (n "mico") (v "0.1.3") (h "175hpfggwsc77izsgfrkyfahdqq8vpjsik23lvd4hdp1sa9wdrp1")))

(define-public crate-mico-0.1.4 (c (n "mico") (v "0.1.4") (h "05jr1qcfa0wyjsw16agfgl1j9j2cr6d4fz143iydq32hcybffgdl")))

