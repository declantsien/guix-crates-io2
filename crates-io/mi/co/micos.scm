(define-module (crates-io mi co micos) #:use-module (crates-io))

(define-public crate-micos-0.0.0 (c (n "micos") (v "0.0.0") (h "0fj4kgaa7hxpp93g9fmxf5glan9b6x3j4311c4h811m1cfiknzq6") (y #t)))

(define-public crate-micos-0.0.1 (c (n "micos") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l4wwv58q5f095b2nrwf1q6wvczhll6xvbxyvmngfdsbf8vh3n3b") (y #t)))

