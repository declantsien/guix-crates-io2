(define-module (crates-io mi sc misc-conf) #:use-module (crates-io))

(define-public crate-misc-conf-0.1.0 (c (n "misc-conf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0v5qk7nz2dcdjbz9nk58sq5j26hxzaz7f6gp1wxqybl937i9wc5a")))

(define-public crate-misc-conf-0.1.1 (c (n "misc-conf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sg8ixiq8102j0fa0z6qwkzwp57jxcqf2552n7ghmlgfjycw4bs0")))

(define-public crate-misc-conf-0.1.2 (c (n "misc-conf") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "luaparse") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l59625d62nf9f478yplgf2gnsqnrpipk6phd6yvdnwjgvmn220l")))

