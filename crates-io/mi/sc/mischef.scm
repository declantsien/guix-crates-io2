(define-module (crates-io mi sc mischef) #:use-module (crates-io))

(define-public crate-mischef-0.1.0 (c (n "mischef") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "00jwz3n7ykpcsiriff4f6vdy7x1543wap9kmp2m36vqzxsg8lvyc")))

(define-public crate-mischef-0.1.1 (c (n "mischef") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1zm4n4sj93y72giazf61kz17ph9nksnr69balix66qmxw2yafzpv")))

(define-public crate-mischef-0.1.2 (c (n "mischef") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1rx8klim8pc99byrz1ajaffc8g3bk0284hhklgmbwsn15r9xxxq1")))

(define-public crate-mischef-0.1.3 (c (n "mischef") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "08w6v5vzzjrs3ril6cdzc8ds8gjmq64zgx6n60g5lsn9yjrnqgiq")))

(define-public crate-mischef-0.1.4 (c (n "mischef") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "19jaw8fjbawjhrq17904db0q5mrskr4c4g6ay73nkmy82zl8dhlg")))

(define-public crate-mischef-0.1.5 (c (n "mischef") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "08cfzx5k4wksqvszxhg6wxmp5qs0lxk1msk8pz2gzyyhnvpdby8a")))

(define-public crate-mischef-0.1.6 (c (n "mischef") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1dga04ccrxqn54mkn66wd8mvmxih2lsjbj34khspdcnwg8r15r7f")))

(define-public crate-mischef-0.1.7 (c (n "mischef") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1r55m0x0d5vpcbk740s7lkw41aiz9cyv09sv40z3mk26wh3653zr")))

(define-public crate-mischef-0.1.8 (c (n "mischef") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "150pzp989algrhc4b5xjshiigczy7kc928b72vnd6d8f787bqbg1")))

(define-public crate-mischef-0.1.9 (c (n "mischef") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1p1wgcqpirx45l6v3brranlk7775xmm2x83vilblnx16h5m67mii")))

(define-public crate-mischef-0.1.11 (c (n "mischef") (v "0.1.11") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0lhd1xis0bqfg0a27ahrkpbf4giswxd9in7x1s3qr9gwwh2112r3")))

(define-public crate-mischef-0.1.12 (c (n "mischef") (v "0.1.12") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1nz8lm43yd0a72xd0nmx3sv7lbgd99nlarxx5bc2kbjxq55vrjfa")))

(define-public crate-mischef-0.1.14 (c (n "mischef") (v "0.1.14") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1i0rhvfwd6adz0sbin64xca08vinj3613xlvmnpph0ya37d7crig")))

(define-public crate-mischef-0.1.15 (c (n "mischef") (v "0.1.15") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "10zrvk0gdbhibyq5x9r5za05h2gf2cjhq3dj9l0iwcmcizxvrmwy")))

(define-public crate-mischef-0.1.16 (c (n "mischef") (v "0.1.16") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "05y2r68hi6xi8hbc208m4g38x27y620y2n3mldq2ibynbngl7c0n")))

