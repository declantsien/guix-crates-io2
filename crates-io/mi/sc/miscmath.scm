(define-module (crates-io mi sc miscmath) #:use-module (crates-io))

(define-public crate-miscmath-0.1.0 (c (n "miscmath") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zq095b94wxmxrm9ykn733cfij2qb8ni2cjaixwxh0g6ikk6v91a") (y #t)))

(define-public crate-miscmath-0.1.1 (c (n "miscmath") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xwd64arbjv874hx76kr8ny0llx6j4fk1rvjchhqdyd6jqiif4g4")))

(define-public crate-miscmath-0.1.2 (c (n "miscmath") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cdvc93clgq652qc6d4cyr9lnk9x9ivcf8dmksdxngk1s5mx98hx")))

(define-public crate-miscmath-0.1.3 (c (n "miscmath") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fyni6jcl6yvixqgprcrn586l7rhcphi7ahssh7rqfnyzl2zb2f7")))

(define-public crate-miscmath-0.1.4 (c (n "miscmath") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wh9y204d29gynz44bv1mn60krab9ng1ydyxq7db5dv7kv7qysdq")))

(define-public crate-miscmath-0.1.5 (c (n "miscmath") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g0f4fxqh0n0zhb5j7k72qmiz9db89fvc0p7gig6wx7lxj243kif")))

(define-public crate-miscmath-0.1.6 (c (n "miscmath") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ksim3nkzadc6k8b7m0rycl0ff6al74fywl40cffrq6bqv804msq")))

(define-public crate-miscmath-0.1.7 (c (n "miscmath") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n8dyfh077hg3df0g4li46fbpy1bxi22z6kkwas3fncpvkkf7p7x")))

(define-public crate-miscmath-0.1.8 (c (n "miscmath") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f5z304gqmihkc58swazvk3r6nhfj3n3pwy32j8nlglqxi0yx83h")))

(define-public crate-miscmath-0.1.9 (c (n "miscmath") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kq5z66dfgx225diyf67a6hhsvkmym4acwqpcikfv2hjzhjcb9nn")))

(define-public crate-miscmath-0.1.10 (c (n "miscmath") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1r0gxny9hww6c06wsdbch588v2s07hln7hv0p0scp7wlyxmwh33v")))

(define-public crate-miscmath-0.1.11 (c (n "miscmath") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0973av0xjqcvinnvk589j3lj3r77sxsf34sdbz58aw24jg4slxr5")))

(define-public crate-miscmath-0.1.12 (c (n "miscmath") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kkrj607a8d1xnzzhapjq9nf388y89fpz7pny0rwgkgn8jwijmcy")))

(define-public crate-miscmath-0.1.13 (c (n "miscmath") (v "0.1.13") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s43nh6k1ag34a0ym2dg257nfyhpnjl46szh205zi2rfnzrr7ymy")))

(define-public crate-miscmath-0.1.14 (c (n "miscmath") (v "0.1.14") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m1z75846567v2fvdvzpy88l2p5mszb613qy5kb8j1gpyx788krl")))

(define-public crate-miscmath-0.1.15 (c (n "miscmath") (v "0.1.15") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ag70178w2hfcfv0fx9pwmpdhl9pj2dhmqkmwwsr1wagzck5cnin")))

(define-public crate-miscmath-0.1.16 (c (n "miscmath") (v "0.1.16") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pxqjjc4gz3f8n8r1gwq6b3ghzgmkcw8yynby8ygys6jr4pgayl1")))

(define-public crate-miscmath-0.1.17 (c (n "miscmath") (v "0.1.17") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19xs33cyqzmhi00z548qkylipkpn64579gxv9vb5b6djjm221l40")))

(define-public crate-miscmath-0.2.0 (c (n "miscmath") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kchad4zlwmc3lir0sh5km9mayqq2ixyz49k33yxqjd4zlk6p6ak")))

(define-public crate-miscmath-0.2.1 (c (n "miscmath") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sbq11x5b5y54gs84s7ag8082pv3yx6l4rqcdq0pfqh2xwf8bhxb")))

(define-public crate-miscmath-0.2.2 (c (n "miscmath") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07qzn1yzzxpis9zzpzq6r97bzd64jb3qkcl6fjvis0xdd347k5dq")))

(define-public crate-miscmath-0.2.3 (c (n "miscmath") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mymsdbynvlfxkf165438s997lvjb84z0amninfzhg8d9whrpya9")))

(define-public crate-miscmath-0.3.0 (c (n "miscmath") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m9gqcn5y7cxndpjkki93s0wihh84y9hs245wlh4bi7q4694hy0s")))

(define-public crate-miscmath-0.3.1 (c (n "miscmath") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "144w3cs3gmkh61y20zx0s5f5dqlrdidq9p0aypfggvnayd7dqkfr")))

(define-public crate-miscmath-0.3.2 (c (n "miscmath") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03c24xwx9428n4zn6ik8dph4hyd89bzlxm2746h6p3mb4c5izm4x")))

