(define-module (crates-io mi sc miscreant-aes-v05) #:use-module (crates-io))

(define-public crate-miscreant-aes-v05-0.5.2 (c (n "miscreant-aes-v05") (v "0.5.2") (d (list (d (n "aes") (r "^0.5") (k 0)) (d (n "aes-siv") (r "^0.4") (k 0)) (d (n "cmac") (r "^0.4") (k 0)) (d (n "crypto-mac") (r "^0.9") (k 0)) (d (n "ctr") (r "^0.5") (k 0)) (d (n "pmac_crate") (r "^0.4") (o #t) (k 0) (p "pmac")) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.7") (k 0)) (d (n "subtle-encoding") (r "^0.5") (d #t) (k 2)))) (h "07pr0z146jsxgkkyqs1l0zyzcmzy1k5bzlfliy2kfs9df1n1d05n") (f (quote (("stream") ("std" "alloc") ("pmac" "pmac_crate" "aes-siv/pmac") ("default" "std" "pmac" "stream") ("alloc" "aes-siv/alloc")))) (y #t)))

