(define-module (crates-io mi d- mid-compression) #:use-module (crates-io))

(define-public crate-mid-compression-1.0.0 (c (n "mid-compression") (v "1.0.0") (d (list (d (n "enum_dispatch") (r "^0.3.11") (d #t) (k 0)) (d (n "integral-enum") (r "^2.1.1") (d #t) (k 0)) (d (n "libdeflate-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.7") (f (quote ("thin" "zdict_builder"))) (k 0)))) (h "07h2mhqrnjhng0ad84j8b1k843dg27gx6j9srixdx2r70azif4v7")))

