(define-module (crates-io mi d- mid-net) #:use-module (crates-io))

(define-public crate-mid-net-1.0.0 (c (n "mid-net") (v "1.0.0") (d (list (d (n "integral-enum") (r "^2.1.1") (d #t) (k 0)) (d (n "mid-compression") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt" "rt-multi-thread" "net" "sync" "macros" "io-util" "net"))) (k 0)) (d (n "rstest") (r "^0.16.0") (k 2)))) (h "06rbfggmjd0vr74l2sgi9r26mlvmaram30jw8siqw9ivg8gjg6ij") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

