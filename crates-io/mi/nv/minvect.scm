(define-module (crates-io mi nv minvect) #:use-module (crates-io))

(define-public crate-minvect-0.1.0 (c (n "minvect") (v "0.1.0") (h "0x3vwd5kbbf9m59d88dg05lf350qgs3mwb81nkfv5hfp96lfxzcc")))

(define-public crate-minvect-0.1.1 (c (n "minvect") (v "0.1.1") (h "0dxx67xv8na4ksb2ycqnrc2qdsdjyvxdg2i2ssibf0r99xgwkqb4")))

(define-public crate-minvect-0.1.2 (c (n "minvect") (v "0.1.2") (h "1zglx2b0mbpismj535zbkpkg48g4hbkpjy9mj3ga07wxyyirck20")))

(define-public crate-minvect-0.1.3 (c (n "minvect") (v "0.1.3") (h "0wv4z4am0aws1xs7q9gaxdxn0maldd1r2mb84v9lbs5cj77h47c6")))

(define-public crate-minvect-0.1.4 (c (n "minvect") (v "0.1.4") (h "1hpsbk8m0p9x2vhwmry3c2mcn0na2241qriqg3b70d5aprina0sj")))

(define-public crate-minvect-0.1.5 (c (n "minvect") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s4hp78njsyax24nds7a85i6xkjcznd08dj8gbnhmi1ay24c576r")))

(define-public crate-minvect-0.1.6 (c (n "minvect") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pyfnqbvjyrllchpiyngavsvaqv2lsmjmckc7g6pj19s0lzkczj8")))

(define-public crate-minvect-0.1.7 (c (n "minvect") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1npv1hah6w5g3khhr1lxj0izqvq8fgsp5csad3vx1pj0jlya88vm")))

