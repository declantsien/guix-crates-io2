(define-module (crates-io mi mx mimxrt500-bootstub) #:use-module (crates-io))

(define-public crate-mimxrt500-bootstub-0.1.0 (c (n "mimxrt500-bootstub") (v "0.1.0") (h "14rbw2lrlhrb4y11p4rdq8psdc6zv34fzzvq524rsgmxw539shh1")))

(define-public crate-mimxrt500-bootstub-0.2.0 (c (n "mimxrt500-bootstub") (v "0.2.0") (h "1a45rmyvm84bll7rzl4ycc079rdzq6xj3sb9y82hdc0rxnqhmkvn")))

(define-public crate-mimxrt500-bootstub-0.3.0 (c (n "mimxrt500-bootstub") (v "0.3.0") (h "1gfqp448rl31vcldwbrb3yrxb0r353szfn286d3bfg507cmri0bm")))

