(define-module (crates-io mi mx mimxrt1062) #:use-module (crates-io))

(define-public crate-mimxrt1062-0.1.0 (c (n "mimxrt1062") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0bbmckhqlcb2dlrkagk1d60z81lmmq9azg77zsb9h04wpp85gbgj") (f (quote (("rt" "cortex-m-rt/device"))))))

