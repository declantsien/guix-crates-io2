(define-module (crates-io mi mx mimxrt595s) #:use-module (crates-io))

(define-public crate-mimxrt595s-0.1.0 (c (n "mimxrt595s") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pi683vnnba508w7pyvj92ygq0kpmbx3jnp3qpml14zqwa1vg4c9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mimxrt595s-0.2.0 (c (n "mimxrt595s") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "mimxrt500-rt") (r "^0.1.0") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1m36mz83s3c6jclpkqilk39vsafwz29nh7wv9zs7571ypr4zbik9") (s 2) (e (quote (("rt" "dep:mimxrt500-rt"))))))

(define-public crate-mimxrt595s-0.3.0 (c (n "mimxrt595s") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05b3zx0a2mhajh4idp843m0kvkv4xni6vqivwlb6yblpflf62s55") (s 2) (e (quote (("rt" "dep:cortex-m-rt"))))))

(define-public crate-mimxrt595s-0.4.0 (c (n "mimxrt595s") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1x3ns374fiff3wh9d2wgqgc9qixppj1vb20pv4np0rna6x6blck3") (s 2) (e (quote (("rt" "dep:cortex-m-rt"))))))

(define-public crate-mimxrt595s-0.5.0 (c (n "mimxrt595s") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (f (quote ("device"))) (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "04lzy8nxdsrh3m1hzqkpqpx1vh2rcnk5i9r5lyh5yhy75r2kag7h") (s 2) (e (quote (("rt" "dep:cortex-m-rt"))))))

