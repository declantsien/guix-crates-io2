(define-module (crates-io mi mx mimxrt595-evk) #:use-module (crates-io))

(define-public crate-mimxrt595-evk-0.1.0 (c (n "mimxrt595-evk") (v "0.1.0") (d (list (d (n "mimxrt500-hal") (r "^0.2.0") (f (quote ("mimxrt595s"))) (d #t) (k 0)) (d (n "mimxrt595s") (r "^0.2.0") (d #t) (k 0)))) (h "0fszm4x8awz3ffi05zcf0wxm4bdc835fyzax94ahzzmf6p9mxlxr")))

(define-public crate-mimxrt595-evk-0.2.0 (c (n "mimxrt595-evk") (v "0.2.0") (d (list (d (n "mimxrt500-hal") (r "^0.3.0") (f (quote ("mimxrt595s"))) (d #t) (k 0)) (d (n "mimxrt595s") (r "^0.2.0") (d #t) (k 0)))) (h "0b6zm1xc8d89slh3jn70rm8xb5m9z0dc95pp9hj7kd78bbbr83l3")))

(define-public crate-mimxrt595-evk-0.3.0 (c (n "mimxrt595-evk") (v "0.3.0") (d (list (d (n "mimxrt500-bootstub") (r "^0.1.0") (d #t) (k 0)) (d (n "mimxrt500-hal") (r "^0.4.0") (f (quote ("mimxrt595s"))) (d #t) (k 0)) (d (n "mimxrt595s") (r "^0.3.0") (d #t) (k 0)))) (h "0bxhzlq3qa6ppw057sqxgvdffrwxhkiaj6z9f8r77qhz5jpx0wkr") (f (quote (("bootstub-standalone-bin"))))))

(define-public crate-mimxrt595-evk-0.5.0 (c (n "mimxrt595-evk") (v "0.5.0") (d (list (d (n "mimxrt500-bootstub") (r "^0.3.0") (d #t) (k 0)) (d (n "mimxrt500-hal") (r "^0.6.0") (f (quote ("mimxrt595s"))) (d #t) (k 0)) (d (n "mimxrt595s") (r "^0.5.0") (d #t) (k 0)))) (h "1lbccnanpmf22l2gjizwq7yddpx5rq69swhhnzzl7gxzfzqhhlrs") (f (quote (("bootstub-standalone-bin"))))))

