(define-module (crates-io mi li milim_vulkan) #:use-module (crates-io))

(define-public crate-milim_vulkan-0.1.0 (c (n "milim_vulkan") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1xi14g0hdfj8cfwkridazjy6apipfw990nh494aa6gwjvc4nb3m5")))

(define-public crate-milim_vulkan-0.1.1 (c (n "milim_vulkan") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rcs89dq6rwzvkpbrlpjkjlghlvy8vcy6fc5ah1xc417ynhxlms8")))

