(define-module (crates-io mi ro miro) #:use-module (crates-io))

(define-public crate-miro-0.1.0 (c (n "miro") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "freetype") (r "^0.7.0") (d #t) (k 0)) (d (n "harfbuzz-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "servo-fontconfig") (r "^0.5.1") (d #t) (k 0)) (d (n "vte") (r "^0.10.0") (d #t) (k 0)))) (h "0335xma2pcxvslp2xfai6ihg4hzsd013gvpwrfvy2qb6fraqnddm")))

