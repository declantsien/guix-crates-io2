(define-module (crates-io mi lv milvus) #:use-module (crates-io))

(define-public crate-milvus-0.1.0 (c (n "milvus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "0zjgkrhhpgbjcqmipddn8xw78gbmkxdf6ngzrpq8i8chzqq6azzx")))

(define-public crate-milvus-0.1.1 (c (n "milvus") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "09n8c5pvh96izyg7p6q4qk1dg0xxzxz7ax9yx8icv9q0fqbp4qd3")))

(define-public crate-milvus-0.1.2 (c (n "milvus") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "1yq559swfnl6a3c2f0l36kdqxkywrhqds70wcy2pgcia997fiqmj")))

(define-public crate-milvus-0.2.0 (c (n "milvus") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0jx31prnmjny3jsznxpr6b0gnjip0hbw7wyxy8wyzqlb7y2847yg")))

