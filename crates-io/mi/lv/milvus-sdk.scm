(define-module (crates-io mi lv milvus-sdk) #:use-module (crates-io))

(define-public crate-milvus-sdk-0.0.1-pre-alpha (c (n "milvus-sdk") (v "0.0.1-pre-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "0g5k2b2rfg334idnfz2gyi5p6shwf0xbfbimrihwqmgd1fq0fr9k") (y #t)))

