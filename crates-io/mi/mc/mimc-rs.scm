(define-module (crates-io mi mc mimc-rs) #:use-module (crates-io))

(define-public crate-mimc-rs-0.0.1 (c (n "mimc-rs") (v "0.0.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "01sr6xs4v1vi3xpz864p9jpvd7nk24pic4534y4w03wbfw92h717")))

(define-public crate-mimc-rs-0.0.2 (c (n "mimc-rs") (v "0.0.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "0lnq0f0nzwmy7h6mhm2crh8gwsnam99jf505awx2qk0c0jsxj9hi")))

