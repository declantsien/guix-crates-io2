(define-module (crates-io mi kk mikktspace-sys) #:use-module (crates-io))

(define-public crate-mikktspace-sys-0.1.0 (c (n "mikktspace-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1pdi019g17yx9s7wqqb17kr2grr1r918m7c99mdvnyxkx8iary4v")))

(define-public crate-mikktspace-sys-0.1.1 (c (n "mikktspace-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p13hvvrpzv7jvh3n0sm8gan8k75k3kgh2xwbi46h754ylif9s2g")))

