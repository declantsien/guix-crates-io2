(define-module (crates-io mi kk mikktspace) #:use-module (crates-io))

(define-public crate-mikktspace-0.1.0 (c (n "mikktspace") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mxddd86d7kj05amv2kpv4dwqh4dzsm1az56hf22lbn21fa1zy13")))

(define-public crate-mikktspace-0.1.1 (c (n "mikktspace") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cgmath") (r "^0.15") (d #t) (k 2)))) (h "1aim03rs5rq0xqsz4p2ji85sq6hmhlw1hqm47x27p3rz3si1jnsw")))

(define-public crate-mikktspace-0.2.0 (c (n "mikktspace") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "1mp1lriqkzfhfsc549pfbrbjcmx0gjjm6y5ilhbrwdjw3dgmllp7")))

(define-public crate-mikktspace-0.3.0 (c (n "mikktspace") (v "0.3.0") (d (list (d (n "glam") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.26.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (d #t) (k 2)))) (h "1c7qcan08qxgqifrba5f3f2d9456q0nda9m65jcqy6l70fs5c2vx") (f (quote (("default" "nalgebra"))))))

