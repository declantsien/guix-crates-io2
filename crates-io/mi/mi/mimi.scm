(define-module (crates-io mi mi mimi) #:use-module (crates-io))

(define-public crate-mimi-0.1.0 (c (n "mimi") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1g97m2khg9jfvbv1yi23sv7kdn0if171b6w14iavf3nw12fpn70y") (f (quote (("to_tui" "tui") ("default"))))))

(define-public crate-mimi-0.1.1 (c (n "mimi") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ljsap0qmbam67c3dmmwad0l1qk4xm3db85p0whdqhg7vva3lakk") (f (quote (("to_tui" "tui") ("default"))))))

(define-public crate-mimi-0.1.2 (c (n "mimi") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.3") (o #t) (d #t) (k 0)))) (h "022d8lbyax3d0wxy872j10s2xm2y1hlwsw8zdpb8yhns24fb9naw") (f (quote (("to_tui" "tui") ("default"))))))

