(define-module (crates-io mi mi mimicry) #:use-module (crates-io))

(define-public crate-mimicry-0.1.0 (c (n "mimicry") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "mimicry-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "049kypdyxrr6gs79ajglva2xg01xf5bzpqvzbkmyqczq6kdasw6h") (f (quote (("shared" "parking_lot" "ouroboros") ("default")))) (r "1.57")))

