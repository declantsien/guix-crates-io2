(define-module (crates-io mi mi mimicry-derive) #:use-module (crates-io))

(define-public crate-mimicry-derive-0.1.0 (c (n "mimicry-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1farpyi2k5svdskm4ljp93kbiirdjcqb42jw2m4kapvjgr3j6gp4") (r "1.57")))

