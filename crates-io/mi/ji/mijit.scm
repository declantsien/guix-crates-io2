(define-module (crates-io mi ji mijit) #:use-module (crates-io))

(define-public crate-mijit-0.1.0 (c (n "mijit") (v "0.1.0") (d (list (d (n "bad64") (r "^0.3.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.2.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "14za5b9m2ckq2sl6r71sdjil3ilava3i7gs8k1nv0pk775jis6xq")))

(define-public crate-mijit-0.1.1 (c (n "mijit") (v "0.1.1") (d (list (d (n "bad64") (r "^0.3.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.2.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0f3qy3s8vhpcyavc201pmvpn6fpbdymw1x01i5bhryfw13nkmb6s")))

(define-public crate-mijit-0.1.2 (c (n "mijit") (v "0.1.2") (d (list (d (n "bad64") (r "^0.3.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.2.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "11yf43yrh4lip1zn6rmgwimk93rncz9w6bfhfsmkp4dw4j48w90q") (r "1.53")))

(define-public crate-mijit-0.1.3 (c (n "mijit") (v "0.1.3") (d (list (d (n "bad64") (r "^0.3.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.2.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17kmn3zm95izia29qnm95v3c6cf4409q71zpxxlzqhr1h3y01cgk") (r "1.53")))

(define-public crate-mijit-0.1.4 (c (n "mijit") (v "0.1.4") (d (list (d (n "bad64") (r "^0.3.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.2.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1rscyfpy2yyidcaba04lqs1847dqz83119nmkk6zqs5bgcw3vnrl") (r "1.53")))

(define-public crate-mijit-0.1.5 (c (n "mijit") (v "0.1.5") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "0ic778ji9yvx9x9a4bg8i10h6cjn88zm4qm53m46ka4152yyh006") (r "1.53")))

(define-public crate-mijit-0.1.6 (c (n "mijit") (v "0.1.6") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "15gi153yykf8yxcngjmviqxl9d1bzfi601177mf16rn9y274d0xp") (r "1.53")))

(define-public crate-mijit-0.1.7 (c (n "mijit") (v "0.1.7") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "0miwb9lg9xs4kq591wlj0n3lv6j380y2p7ghflrjqj17brq134wf") (r "1.56")))

(define-public crate-mijit-0.2.0 (c (n "mijit") (v "0.2.0") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "0040qiw16r1fr98yww45d378r19x0xah8pj8pr55ihqpcyad4yny") (r "1.56")))

(define-public crate-mijit-0.1.8 (c (n "mijit") (v "0.1.8") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "0shpzsy2bz9yjb1wdydz2x6g2x7mib7ndbpm0p72c6j7b6a0wawr") (r "1.56")))

(define-public crate-mijit-0.2.1 (c (n "mijit") (v "0.2.1") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "0v6xs4mks7spf8ggk53m75iga6y2bx3a2g9kccw6cmkrwbhxycn7") (r "1.56")))

(define-public crate-mijit-0.2.2 (c (n "mijit") (v "0.2.2") (d (list (d (n "bad64") (r "^0.4.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.4") (d #t) (k 0)))) (h "01jph9n0vxy674mfivk43mk6jxdc36bpg0irdiijc4hb2fyp0n49") (r "1.56")))

(define-public crate-mijit-0.2.3 (c (n "mijit") (v "0.2.3") (d (list (d (n "bad64") (r "^0.6.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.14.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1jpip5zybg7ijl37mzkj6mzl57ds6m3qix93l52a6h29ajg42hp3") (r "1.56")))

(define-public crate-mijit-0.2.4 (c (n "mijit") (v "0.2.4") (d (list (d (n "bad64") (r "^0.6.0") (d #t) (k 2)) (d (n "iced-x86") (r "^1.18.0") (f (quote ("std" "decoder" "nasm"))) (k 2)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0pasf2pz0y743zs2nwdd3dcp3856r5kwybjihry2ngyx18da5jdl") (r "1.56")))

