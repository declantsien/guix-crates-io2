(define-module (crates-io mi ta mitata) #:use-module (crates-io))

(define-public crate-mitata-0.0.1 (c (n "mitata") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)))) (h "00iyknidw0r1blcbf5iz62bazihfzz0z7nhcyv07881023kka9xi")))

(define-public crate-mitata-0.0.2 (c (n "mitata") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)))) (h "0cymlx4ibd5a7jaa90j5q9gaqygfrr9f4pfwm2ms8744lg6xn610")))

(define-public crate-mitata-0.0.3 (c (n "mitata") (v "0.0.3") (d (list (d (n "clap") (r "^3") (f (quote ("std" "env" "cargo" "color"))) (k 0)))) (h "1d8va0c96ijg3x084r2dqcb48zpsi6v1s2ql5w973i39jdk7nj3w")))

(define-public crate-mitata-0.0.4 (c (n "mitata") (v "0.0.4") (d (list (d (n "clap") (r "^3") (f (quote ("std" "env" "cargo" "color"))) (k 0)))) (h "0bahpki2pwpip5g06yrhjnl522sx1v3rhdpdsnh8hghnqrkf1899")))

(define-public crate-mitata-0.0.5 (c (n "mitata") (v "0.0.5") (d (list (d (n "clap") (r "^3") (f (quote ("std" "env" "cargo" "color"))) (k 0)))) (h "1v2zzhdyjk7g4qgq1dc8vw7cssd6j4v14sjjkk8djzkh2r29vdpl")))

(define-public crate-mitata-0.0.6 (c (n "mitata") (v "0.0.6") (d (list (d (n "clap") (r "^3") (f (quote ("std" "env" "cargo" "color"))) (k 0)))) (h "0xqgv1rn235yk1pfs8zr7hnnilbibhc6ds41ijqyhs4f13j46pcp")))

(define-public crate-mitata-0.0.7 (c (n "mitata") (v "0.0.7") (d (list (d (n "clap") (r "^3") (f (quote ("std" "env" "cargo" "color"))) (k 0)))) (h "0ygsyxlhssjr16ng7jg290xik6gdjllrx2915ifvxib9pnilkz91")))

