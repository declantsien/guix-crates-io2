(define-module (crates-io mi ny miny) #:use-module (crates-io))

(define-public crate-miny-1.0.0 (c (n "miny") (v "1.0.0") (h "1i374ydxzaxnc915hlx0jh44qns782f8px1dbcjjnvbd7hm9ffid")))

(define-public crate-miny-2.0.0 (c (n "miny") (v "2.0.0") (h "1yzmldxsa1428g8zry0bm672pv2r7223qs8wy2vijrsk2fa8lh9b")))

(define-public crate-miny-2.0.1 (c (n "miny") (v "2.0.1") (h "0zs50fdy935x04f3d5fmlwl7cw95y0w4786m7m5b5mz3465m3z4x")))

