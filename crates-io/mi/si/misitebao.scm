(define-module (crates-io mi si misitebao) #:use-module (crates-io))

(define-public crate-misitebao-0.1.0 (c (n "misitebao") (v "0.1.0") (d (list (d (n "open") (r "^3") (d #t) (k 0)))) (h "1w4gzz1xl281aplny4iz9zi1m9d0m9inh3r1091zq0r4jjxj1hpv")))

(define-public crate-misitebao-1.0.2 (c (n "misitebao") (v "1.0.2") (d (list (d (n "open") (r "^3") (d #t) (k 0)))) (h "0pir6vrmhc06y3jlzc1mcfzp547bjxdp2nimqlw4aacvq85xcf6c")))

(define-public crate-misitebao-1.0.3 (c (n "misitebao") (v "1.0.3") (d (list (d (n "open") (r "^3") (d #t) (k 0)))) (h "1mys3ipw5kspihp58zmq79x1z4rxbvl3fz0m9l313qrwhrlbw93p")))

(define-public crate-misitebao-1.0.4 (c (n "misitebao") (v "1.0.4") (d (list (d (n "open") (r "^3") (d #t) (k 0)))) (h "1z0slfqrcs44xfr11g4rsg85a7n1x6bg5jfi8aa5xpw56kmlv26v")))

(define-public crate-misitebao-1.1.0 (c (n "misitebao") (v "1.1.0") (d (list (d (n "open") (r "^3") (d #t) (k 0)))) (h "1jh9q35s0yfbrk8i3hqas5rf15xi31ianmccigjhcpvda2rs35pb")))

