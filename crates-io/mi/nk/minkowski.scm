(define-module (crates-io mi nk minkowski) #:use-module (crates-io))

(define-public crate-minkowski-0.1.0 (c (n "minkowski") (v "0.1.0") (h "0fgr0g6fjh79pvb36r8zmviq6f1xrn5ll5p26n0nz1sq2lbawx33")))

(define-public crate-minkowski-0.2.0 (c (n "minkowski") (v "0.2.0") (h "0kd01zbgnj063fm1ccw12r45dlbybza7m43zdpyi0kpf08k519sk")))

(define-public crate-minkowski-0.2.1 (c (n "minkowski") (v "0.2.1") (h "0gxnzdwggaabw5k2x3kmbhhl6ypbbhsfl1hfbmj8cxwaillqrqls")))

