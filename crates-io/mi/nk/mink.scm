(define-module (crates-io mi nk mink) #:use-module (crates-io))

(define-public crate-mink-0.1.0 (c (n "mink") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1ssx1j6a5bx278z8bi112czfxz2yldy1xwmsk744j2l0689ybf8y") (f (quote (("minkstd") ("default" "minkstd"))))))

(define-public crate-mink-0.1.1 (c (n "mink") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0xdkdnaa0k3f53l11jb0a1jdr47cn527r53li39y9xp3p9zfafhr") (f (quote (("minkstd") ("default" "minkstd"))))))

