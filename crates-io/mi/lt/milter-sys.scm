(define-module (crates-io mi lt milter-sys) #:use-module (crates-io))

(define-public crate-milter-sys-0.1.0 (c (n "milter-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1x24gwypxdl4myl6g7g1kr13wmwj2lvh4cckysf2mj24zkhcbfcf") (l "milter")))

(define-public crate-milter-sys-0.1.1 (c (n "milter-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1gz5lgh1y25ciq4jmx8gar576dhfaw0x4zy08qglfj3zcfis3an3") (l "milter")))

(define-public crate-milter-sys-0.1.2 (c (n "milter-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wxyqp9ags6glaryv74989kiznr8k9x2lbg9qq1jbw6k9hql8v2l") (l "milter")))

(define-public crate-milter-sys-0.1.3 (c (n "milter-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a0pch8aifixqmdaiiysbkqxdlrg077hvxvf2w17ab7nirk6h7sm") (l "milter")))

(define-public crate-milter-sys-0.1.4 (c (n "milter-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xhzpb7vbc3h5wfzxpp7xp7vyylamc89jflwc4igy4y8ddimkdd1") (l "milter")))

(define-public crate-milter-sys-0.2.0 (c (n "milter-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04icva83q9322s6sv4wdafg1r59r0cml8sm15pjxn3zdylrd1axs") (l "milter")))

(define-public crate-milter-sys-0.2.1 (c (n "milter-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06y4wcbvaf75ryqf563cfnsj6f5n7j7wkmzji039af97ah5pw8av") (l "milter")))

(define-public crate-milter-sys-0.2.2 (c (n "milter-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "09fdjgx80djis7ib2hv0ihf16jj4d7jw1xbpir1gbg6y2bzad9f6") (l "milter")))

(define-public crate-milter-sys-0.2.3 (c (n "milter-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0k7kzbklsb96rlawly4zq621anmxal2wjyww1l93p378153xbl26") (l "milter") (r "1.42.0")))

