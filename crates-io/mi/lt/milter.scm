(define-module (crates-io mi lt milter) #:use-module (crates-io))

(define-public crate-milter-0.1.0 (c (n "milter") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.0") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1bdpg74lrqa571bcs2jaxbxh4asd9nggx37sklr0qcz6z9p5av1c")))

(define-public crate-milter-0.1.1 (c (n "milter") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.1") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1pjzjadc19sxy7z6akfcfhhf4wgnpkm2980hhlbddn3jqdxvjc0n")))

(define-public crate-milter-0.1.2 (c (n "milter") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.2") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1a8rzfb1dk46yrwvsj5zbxp5fng4kkaw0bs11nrcvgmzij5srmny")))

(define-public crate-milter-0.1.3 (c (n "milter") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.3") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1y3znwcs578qpclvf9bwz5pzklw4gm1clvlb6md8pm5aq3mqdk9h")))

(define-public crate-milter-0.1.4 (c (n "milter") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.4") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1") (d #t) (k 0)))) (h "1ldlm1zm19dwk4s36ixgsv4chnwb2d6k5f4s88pg5aajvz69xgp3")))

(define-public crate-milter-0.1.5 (c (n "milter") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.5") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1") (d #t) (k 0)))) (h "0br8lpss6bdkd8f45586m7xf3ahmcrvn6gkr9kzckid395fdq564")))

(define-public crate-milter-0.1.6 (c (n "milter") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.6") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1") (d #t) (k 0)))) (h "0nxw6x78fvivg25zwkscjl3y8h47zghnv9b9svlw1fs29fgmypic")))

(define-public crate-milter-0.1.7 (c (n "milter") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.7") (d #t) (k 0)) (d (n "milter-sys") (r "^0.1") (d #t) (k 0)))) (h "1ghisqxji9642khgcs3s2l998l8cacz2i71yxfvw7vjm0nfq3dp6")))

(define-public crate-milter-0.1.8 (c (n "milter") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.1.8") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0sqfah5k4fiv6abxzp897xa9gw0ps3g11wgfn71d2x2l234b2dxz")))

(define-public crate-milter-0.2.0 (c (n "milter") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "^0.2.0") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "113nmf1l14kd909d6f0nwfs7080liqld3sgxhh4pzf44g5ym7xdm")))

(define-public crate-milter-0.2.1 (c (n "milter") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "= 0.2.1") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0dr2lkg4yagl8gmsa59fsi4dlwdpcb5bhs7l5dhq9zc842vvc968")))

(define-public crate-milter-0.2.2 (c (n "milter") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "milter-callback") (r "=0.2.2") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0r8xmxc2jgx4vybxpp75xj14l2zi1plw44iq9qgydcn9kfmk0rwf")))

(define-public crate-milter-0.2.3 (c (n "milter") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "milter-callback") (r "=0.2.3") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "03g2a8kiw61dg547mh2nwbv8mr0fhvv93j12li92j69524m3vrlj")))

(define-public crate-milter-0.2.4 (c (n "milter") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "milter-callback") (r "=0.2.4") (d #t) (k 0)) (d (n "milter-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1wxb3fd4mvmdc9wid3k219ldqdnkbzrsjkm2n4n9cl3vmjk070vg") (r "1.42.0")))

