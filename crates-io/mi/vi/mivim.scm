(define-module (crates-io mi vi mivim) #:use-module (crates-io))

(define-public crate-mivim-0.1.0 (c (n "mivim") (v "0.1.0") (h "09n8p20hrsw8053945a09zsqpj59rj2d3b871rayb64ll9dxz3w6")))

(define-public crate-mivim-0.2.0 (c (n "mivim") (v "0.2.0") (h "1sfhc03k6cisii7y353v7f9q6iqnifkns94fqpgmfcb2ajgj8q3p")))

(define-public crate-mivim-0.3.0 (c (n "mivim") (v "0.3.0") (h "1bhkn4q4mvzxvaab86b6by2fak3gvga3ijdzacp04n5pg72la2ma")))

