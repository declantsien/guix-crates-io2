(define-module (crates-io mi dd middleware-template) #:use-module (crates-io))

(define-public crate-middleware-template-0.0.0 (c (n "middleware-template") (v "0.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gotham") (r "^0.4.0") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q6xfspz66nlplx1ykm16wfs4q2r99flpw4ljx1fhhs6ipnlvwr0")))

