(define-module (crates-io mi dd middleman) #:use-module (crates-io))

(define-public crate-middleman-0.1.0 (c (n "middleman") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "084hzfjilw2hxdpy5x0m8dc0ahggjm8qyznqjry0p66kr6dpn1q1")))

(define-public crate-middleman-0.2.0 (c (n "middleman") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1n444iyi93bb4i3acdw1sh9i9bd7l1icrz553741q0hqrczprgqf")))

(define-public crate-middleman-0.2.1 (c (n "middleman") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "05njbb8wpdgdkvq0mwfp1bhjns12gf3igp5zzshwqd72zalp1yh6")))

(define-public crate-middleman-0.3.0 (c (n "middleman") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "02v7pr4m249i78pr8h0fgyqn2g37cdcq2pfw4ciwxvg4p0nkyfls")))

(define-public crate-middleman-0.3.1 (c (n "middleman") (v "0.3.1") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1gg076fl44vfqncg7qjvvcnnx74x35lvmdp9dp7yfy032w5rnxdc")))

