(define-module (crates-io mi ef mief) #:use-module (crates-io))

(define-public crate-mief-0.1.0 (c (n "mief") (v "0.1.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "fps_counter") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "piston_window") (r "^0.65") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xrfvixqyxf16dykla4w3dqfk5b9jqmssaqawhdcf7dpfsrz1475") (f (quote (("display-fps" "fps_counter"))))))

(define-public crate-mief-0.1.1 (c (n "mief") (v "0.1.1") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "fps_counter") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "piston_window") (r "^0.73") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1aszscas8cm5zh6swylj45mipysh5a9aim0wal482yys3sji3l73") (f (quote (("display-fps" "fps_counter"))))))

