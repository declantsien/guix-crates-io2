(define-module (crates-io mi gh mighty-mancala) #:use-module (crates-io))

(define-public crate-mighty-mancala-0.1.0 (c (n "mighty-mancala") (v "0.1.0") (d (list (d (n "cursive") (r "^0.20") (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fvx2yw0k386fnhpikc8m69r0s29ag0kgri5l828k2iqw1q59bvw") (f (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1.1 (c (n "mighty-mancala") (v "0.1.1") (d (list (d (n "cursive") (r "^0.20") (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qq5457frr6aakpjx5cijn33d3awn2ml4rnix21pdhl46pjykr24") (f (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1.2 (c (n "mighty-mancala") (v "0.1.2") (d (list (d (n "cursive") (r "^0.20") (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xgfp68szqnrhs396jjmrgb9jlkc1i8x8cphjkv4vcxbawh9h5rd") (f (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1.3 (c (n "mighty-mancala") (v "0.1.3") (d (list (d (n "cursive") (r "^0.20") (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vkm4irr06h3ypy152pq75hnq5smc4zax5rsmkpmgd44dcg4icdg") (f (quote (("default" "cursive/crossterm-backend"))))))

