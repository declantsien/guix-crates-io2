(define-module (crates-io mi gh might_sleep) #:use-module (crates-io))

(define-public crate-might_sleep-0.1.0 (c (n "might_sleep") (v "0.1.0") (h "0fxajm1vafg1v1g8s7d2qb3hs0g6qir4abmv7g0jxaha923iiqzk")))

(define-public crate-might_sleep-0.2.0 (c (n "might_sleep") (v "0.2.0") (h "04nrgqv9x9514f3hm6p3aa1bkbck567j8mil24rljj82g9lbml8y")))

