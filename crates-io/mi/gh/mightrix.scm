(define-module (crates-io mi gh mightrix) #:use-module (crates-io))

(define-public crate-mightrix-0.1.0 (c (n "mightrix") (v "0.1.0") (h "15s48j7ilba95hs7aps5rny6hrskx75fyqpy0fqhdnj9b5rf39ag") (y #t)))

(define-public crate-mightrix-0.2.0 (c (n "mightrix") (v "0.2.0") (h "1sr3hlqpk4185zdhr4g9l6yr6cq27nrshdb1cip6rwl8s4vqipn5")))

(define-public crate-mightrix-0.2.1 (c (n "mightrix") (v "0.2.1") (h "0zxlcnb35plmm7dc6bi462kklfy60z9s1335yj52isxd04gj1aim")))

(define-public crate-mightrix-0.3.0 (c (n "mightrix") (v "0.3.0") (h "1wlxb4clmgikil15jyqin2l918fxajpfyd5p8pr6j4d53ax35nfa")))

(define-public crate-mightrix-0.3.1 (c (n "mightrix") (v "0.3.1") (h "16pgk49ckjs17b6li1a4dkgwvhw27k765dlfcpm2yfw96ixjchvc")))

(define-public crate-mightrix-0.3.2 (c (n "mightrix") (v "0.3.2") (h "1wvqmzy5sdlsmgw94szg7xyg1b4npp7fnzvn9rd57w6v5dxk1j3d")))

