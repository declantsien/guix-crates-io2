(define-module (crates-io mi gh might-be-minified) #:use-module (crates-io))

(define-public crate-might-be-minified-0.1.0 (c (n "might-be-minified") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "132pssxqf96n367f07z9fynz99zb2yin0pav5vnvfx1wnqcnyvil")))

(define-public crate-might-be-minified-0.1.1 (c (n "might-be-minified") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0impvrb0lm8pnyxlr194kbh5xd068cskmrrb5vs364yw5d5icylc")))

(define-public crate-might-be-minified-0.2.0 (c (n "might-be-minified") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0whp2lqf4wslmsc4rzj1sc52lylg1lb605qncbdk7qcz0ws94j8f")))

(define-public crate-might-be-minified-0.2.1 (c (n "might-be-minified") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0l07gc8ag5046pj2gmkl0zp0ivrv453pb4r3spqmamrc2y1gffch")))

(define-public crate-might-be-minified-0.3.0 (c (n "might-be-minified") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0sdpskdzsmphwb5yhbmxlq8znki4yzaj3mb3ir8pa5h90zqvqpw8")))

