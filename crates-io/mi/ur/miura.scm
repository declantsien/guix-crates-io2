(define-module (crates-io mi ur miura) #:use-module (crates-io))

(define-public crate-miura-0.1.0 (c (n "miura") (v "0.1.0") (h "0crgzywwgd53cqv9z2wasv1y011imzbc8142rhnpf44qzgccxgj4") (y #t)))

(define-public crate-miura-0.1.1 (c (n "miura") (v "0.1.1") (h "1z6dghsblay9djkjzpjkqc94qidsyqa6pawznkzbd28z2cgbxqyg")))

