(define-module (crates-io mi x- mix-distribution) #:use-module (crates-io))

(define-public crate-mix-distribution-0.1.0 (c (n "mix-distribution") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.0-pre.0") (d #t) (k 0)))) (h "1cqaddm1w7mff94z0ncns58rn99l8mm4s2pjc1230aszrf0pzqkh")))

(define-public crate-mix-distribution-0.1.1 (c (n "mix-distribution") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0hgpkm9qhz31rhil9pawppcp16xavwc3hvzkv64q0772zflqb0pa")))

(define-public crate-mix-distribution-0.2.0 (c (n "mix-distribution") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0r01xvxyxpyv038xjiw9mly1jvjnksd0li57g29jlrlnpasrik98")))

(define-public crate-mix-distribution-0.2.1 (c (n "mix-distribution") (v "0.2.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0axwjq51zlh16rai7vlspdiqqhi5kzdc9mxlic30h7fbrrj42dvl")))

(define-public crate-mix-distribution-0.3.0 (c (n "mix-distribution") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.1") (d #t) (k 0)))) (h "1wlljvshn694kpii197dbj3pk5b72vwrgwbb9qm429ryfh1znqzx")))

