(define-module (crates-io mi dg midgard) #:use-module (crates-io))

(define-public crate-midgard-0.1.0 (c (n "midgard") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.18") (d #t) (k 0)))) (h "1yb1kqy9djz6lnydprchw04cc3377frn8hlly1jk7yfygrd2im6a") (y #t)))

