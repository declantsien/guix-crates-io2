(define-module (crates-io mi nb minbpe) #:use-module (crates-io))

(define-public crate-minbpe-0.1.0 (c (n "minbpe") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tiktoken-rs") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "0rq86qk6gjvxszsndh8ss6j3k332maxvrald76nhgsq93pfmi2g5") (f (quote (("tiktoken_tests" "gpt4" "tiktoken-rs") ("regex") ("gpt4" "regex") ("default" "basic" "regex") ("basic"))))))

