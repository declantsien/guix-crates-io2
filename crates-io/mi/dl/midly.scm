(define-module (crates-io mi dl midly) #:use-module (crates-io))

(define-public crate-midly-0.1.0 (c (n "midly") (v "0.1.0") (d (list (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "0vwdlri1is9akp4xdcnl2nx9nz1bmpw84r5cfx1ry8x2zfirmfd0")))

(define-public crate-midly-0.1.1 (c (n "midly") (v "0.1.1") (d (list (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "0j7irm46xy5qj8yrmvdcpqykypgxng74iaj0zffrr2z90qkz5iz7")))

(define-public crate-midly-0.1.2 (c (n "midly") (v "0.1.2") (d (list (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "1gb3aca1sw8zrl8kczfy65wj9d73agdxigc4j2dg0az668g2lygc")))

(define-public crate-midly-0.1.3 (c (n "midly") (v "0.1.3") (d (list (d (n "bit") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "0mls9q5ghm7vwvc0phxgcqv9sqb6i88h2jj11j6xv9y5bdhr8hsy")))

(define-public crate-midly-0.2.0 (c (n "midly") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1j4m53jf8v6fs3z0cakdj2lblj7k3nc9qmc1clhpdyhl45n3rzqn") (f (quote (("strict") ("std" "rayon" "failure/std") ("lenient") ("default" "std"))))))

(define-public crate-midly-0.2.1 (c (n "midly") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1zf758399xakk30al2bsay2ndbl83xnyyghc1kj8rcsgj35jydjj") (f (quote (("strict") ("std" "rayon" "failure/std") ("lenient") ("default" "std"))))))

(define-public crate-midly-0.2.2 (c (n "midly") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)))) (h "123gjh9z08rbrq96jlqg8s1r35hk80mzvb4bnps5khj0yw5kx7dv") (f (quote (("strict") ("std" "rayon" "failure/std") ("lenient") ("default" "std"))))))

(define-public crate-midly-0.3.0 (c (n "midly") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0ix2m9b44a6imm20pnvs9qvm54sjkh5z0pjziaq7dw5bvjzb8zll") (f (quote (("strict") ("std" "rayon" "failure/std") ("lenient") ("default" "std"))))))

(define-public crate-midly-0.4.0 (c (n "midly") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1qr9lkw0dk13f8l7lvllgcipq73y0mkgry8dk8p1ngfsscbjg3vg") (f (quote (("strict") ("std" "rayon" "failure/std") ("default" "std"))))))

(define-public crate-midly-0.4.1 (c (n "midly") (v "0.4.1") (d (list (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "13c9jp0kcqv97pmh6g6ri3h7jwnzz951gcyz9xip7x2g5mk8njph") (f (quote (("strict") ("std" "rayon" "failure/std") ("default" "std"))))))

(define-public crate-midly-0.5.0 (c (n "midly") (v "0.5.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1wrhwarwl5xwl61i90apxs3qhpijicfr7zl9ipg142vyhvvs31b2") (f (quote (("strict") ("std" "alloc") ("parallel" "std" "rayon") ("default" "alloc" "std" "parallel") ("alloc"))))))

(define-public crate-midly-0.5.1 (c (n "midly") (v "0.5.1") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0xvma5mw443ld0qvb5fy1qd98fy8ynfdsacb0k734vdyxindi9fb") (f (quote (("strict") ("std" "alloc") ("parallel" "std" "rayon") ("default" "alloc" "std" "parallel") ("alloc"))))))

(define-public crate-midly-0.5.2 (c (n "midly") (v "0.5.2") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1bpha274ljjl72gv9y7dxkc6nsmxs1ycdm3yrf6akjnprrjcdwz5") (f (quote (("strict") ("std" "alloc") ("parallel" "std" "rayon") ("default" "alloc" "std" "parallel") ("alloc"))))))

(define-public crate-midly-0.5.3 (c (n "midly") (v "0.5.3") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0yib6qv8x6r99xx6a7q6qndhq2hkm5y713d59l6d50mq9igpaz90") (f (quote (("strict") ("std" "alloc") ("parallel" "std" "rayon") ("default" "alloc" "std" "parallel") ("alloc"))))))

