(define-module (crates-io mi dl midl) #:use-module (crates-io))

(define-public crate-midl-0.1.0 (c (n "midl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf-schema") (r "^0.1") (d #t) (k 0)) (d (n "midl-parser") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04krqfcf3kf2lrryp4djqzya303r287v600fpr547fwd58bmfhbp")))

(define-public crate-midl-0.1.2 (c (n "midl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf-schema") (r "^0.1") (d #t) (k 0)) (d (n "midl-parser") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fyh2m42322vkgbblpnbjzxhp1nj72499w587nmfncmvjkh28wjv")))

