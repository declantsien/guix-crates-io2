(define-module (crates-io mi dl midl-parser) #:use-module (crates-io))

(define-public crate-midl-parser-0.1.0 (c (n "midl-parser") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "frodobuf-schema") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0hs3m9wf74hdxsbmqgp1hcnvrz40yyx373xv0zfqsrd24542isjg")))

(define-public crate-midl-parser-0.1.1 (c (n "midl-parser") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "frodobuf-schema") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1vkag7k4cwf8zv062dk5icjrvk86ibqk91cq5902sir9w82yy9hw")))

