(define-module (crates-io mi gr migratour) #:use-module (crates-io))

(define-public crate-migratour-0.1.0 (c (n "migratour") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio" "tls-rustls" "postgres" "mysql" "sqlite"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "08c49ayybgvgwf6gcfigda95d5n336i076rmxjffjd591r8zjmli")))

(define-public crate-migratour-0.1.1 (c (n "migratour") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio" "tls-rustls" "postgres" "mysql" "sqlite"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0a6hxiaf48y98lqg1zb5igfslz44bqfihsa6l7dz5nhlrq2997c9")))

