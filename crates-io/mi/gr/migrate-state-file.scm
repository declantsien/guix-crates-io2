(define-module (crates-io mi gr migrate-state-file) #:use-module (crates-io))

(define-public crate-migrate-state-file-0.1.0 (c (n "migrate-state-file") (v "0.1.0") (d (list (d (n "advisory-lock") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.6") (d #t) (k 0)) (d (n "migrate-core") (r "^0.1") (d #t) (k 2)) (d (n "migrate-state") (r "^0.1") (d #t) (k 0)) (d (n "migrate-state-test") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 0)))) (h "0493yrmr0054p53nzm239fwzfrsdyvrvr42rmg5yz6ajiskaa07s")))

