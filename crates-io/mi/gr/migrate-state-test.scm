(define-module (crates-io mi gr migrate-state-test) #:use-module (crates-io))

(define-public crate-migrate-state-test-0.1.0 (c (n "migrate-state-test") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "migrate-state") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 0)))) (h "11krh8p5gdm0x9xjmqis5s0gdy4n2zmwy3qqfp46iknl8bhys22d")))

