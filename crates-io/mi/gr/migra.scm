(define-module (crates-io mi gr migra) #:use-module (crates-io))

(define-public crate-migra-1.0.0 (c (n "migra") (v "1.0.0") (d (list (d (n "mysql") (r "^20.1") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0y9cdsgq664lvkm0riad9cl9f6ynhiz3mldlws92fjpsbfihgs91") (f (quote (("sqlite" "rusqlite") ("default" "postgres"))))))

