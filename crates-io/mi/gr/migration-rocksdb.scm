(define-module (crates-io mi gr migration-rocksdb) #:use-module (crates-io))

(define-public crate-migration-rocksdb-0.1.0 (c (n "migration-rocksdb") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-kvdb") (r "^0.3.2") (d #t) (k 0)) (d (n "tetsy-kvdb-rocksdb") (r "^0.4.3") (d #t) (k 0)) (d (n "tetsy-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ibzlycqa710l4z53lf9y07qj5lq1pvk60h30mpa9z8byvyzzm7f")))

