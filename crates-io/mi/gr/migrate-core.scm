(define-module (crates-io mi gr migrate-core) #:use-module (crates-io))

(define-public crate-migrate-core-0.1.0 (c (n "migrate-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "migrate-state") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)))) (h "0wid8q2imaig4ms5hdq404davw4avc2aavbk3g4w9n0s7vzm6ahp")))

