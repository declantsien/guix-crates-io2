(define-module (crates-io mi sd misdreavus-test) #:use-module (crates-io))

(define-public crate-misdreavus-test-0.1.0 (c (n "misdreavus-test") (v "0.1.0") (h "07qvqf48igz9hl2iixp331lb1fpwf1qk3sspq22hpi6g3j4az3il")))

(define-public crate-misdreavus-test-0.2.0 (c (n "misdreavus-test") (v "0.2.0") (h "0q37kfw7pf64ahpc6q7dbp6aqpj2wzyxdh40dg37nc4x34d2rnxm") (y #t)))

(define-public crate-misdreavus-test-0.3.0 (c (n "misdreavus-test") (v "0.3.0") (h "028v27gbganmhnjxc8yfxyqkkdw5ggdspq3qv436l17r0s5kjy7v") (y #t)))

(define-public crate-misdreavus-test-0.4.0 (c (n "misdreavus-test") (v "0.4.0") (h "0ma3f2mbghdya1y1qrzp4i3fj5l8pmpinhvy2j37bhl76bp942i2")))

(define-public crate-misdreavus-test-0.5.0 (c (n "misdreavus-test") (v "0.5.0") (h "0bscnfrgrwny6jxjrnznk5qnpwm461076cma8wnyac3v9zdcdxf2")))

(define-public crate-misdreavus-test-0.6.0 (c (n "misdreavus-test") (v "0.6.0") (h "07d011p9d1i858q2ifbj1plx7v57z1m32380hh9nv3x4chx2yxhf")))

(define-public crate-misdreavus-test-0.6.1 (c (n "misdreavus-test") (v "0.6.1") (h "16hffqffcm9azs558lpdrymg3hbyhdhmllghh7yja1zghcm9q52b")))

(define-public crate-misdreavus-test-0.6.2 (c (n "misdreavus-test") (v "0.6.2") (h "1wqmhn2c37r5hfim92amq1ain06cpw6pngi5d2b5knmxra1bspbr")))

(define-public crate-misdreavus-test-0.7.0 (c (n "misdreavus-test") (v "0.7.0") (h "0vv86m109gf6if6wvavsv2dkrfa0vcfmhdg1pppmwjz8rh5akm90")))

(define-public crate-misdreavus-test-0.7.1 (c (n "misdreavus-test") (v "0.7.1") (h "1xdc1vz82153kk8a93if6rqsw7p06sjf5nx0dlfkq48daq19ahgq")))

(define-public crate-misdreavus-test-0.7.2 (c (n "misdreavus-test") (v "0.7.2") (h "1msdglmmgdkq12fh9g6h8r1slfqdfnvbq6rbd4f0fmp5vi5fw3p5")))

