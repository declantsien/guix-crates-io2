(define-module (crates-io mi ko miko) #:use-module (crates-io))

(define-public crate-miko-0.0.0 (c (n "miko") (v "0.0.0") (d (list (d (n "diagnostic") (r "^0.4.1") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "voml-collection") (r "^0.3.0") (d #t) (k 0)))) (h "1v2w05c1kphhlmkplxmqgp6zhi3rbhw1y68n1za57x1a1abknv77") (f (quote (("default"))))))

