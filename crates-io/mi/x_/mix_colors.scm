(define-module (crates-io mi x_ mix_colors) #:use-module (crates-io))

(define-public crate-mix_colors-0.1.0 (c (n "mix_colors") (v "0.1.0") (h "0lcjhzgbf5c45m2nmy1shckyd22mbvpcs0aqw9yb9bijvsvk4csk")))

(define-public crate-mix_colors-0.1.1 (c (n "mix_colors") (v "0.1.1") (h "1r45bjcfyrbhlf9j8zp4yb0fzmrz6fx56671gycafx9045lgbaa1")))

