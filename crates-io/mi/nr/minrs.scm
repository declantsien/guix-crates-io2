(define-module (crates-io mi nr minrs) #:use-module (crates-io))

(define-public crate-minrs-0.1.0 (c (n "minrs") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0pf4qg9wfrvikvdj6djs2rljdm23jpb9wdjfzl12n6m8m04jvl7n")))

(define-public crate-minrs-0.1.1 (c (n "minrs") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0rr0a1akmk6yndma4r0vk2f0vxlb5iwckdf70zwf9avfswinkm1w")))

(define-public crate-minrs-0.1.2 (c (n "minrs") (v "0.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0r6jl1vy4rp6z94c44y10mygnmfg00h808b2nzqgp704af10pl2y")))

(define-public crate-minrs-0.2.0 (c (n "minrs") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v0wv0syr95w5247vszp3dmkw1aw6gdhq3lc3kiwffxv01a7v60c")))

(define-public crate-minrs-0.2.1 (c (n "minrs") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1gsx5q49almrq5n1rvp3cldzwdazwskqjgr6c58w7wjsg2kw1g0m")))

(define-public crate-minrs-0.2.2 (c (n "minrs") (v "0.2.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qh3s4hb5dx0rx2ldpsxqd7vw8bvjb4w29wz8gqlz00qb6fb04ff")))

(define-public crate-minrs-0.4.0 (c (n "minrs") (v "0.4.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hn3slgf486nai3381a04x3rxa54z9lcrwqdcn384mb550ysbcvi")))

(define-public crate-minrs-0.4.1 (c (n "minrs") (v "0.4.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "glium") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "118x8lcr6q5lsqki1npykcrzw791qx4niykf3r1x87li7lmakymi")))

