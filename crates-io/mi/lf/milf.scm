(define-module (crates-io mi lf milf) #:use-module (crates-io))

(define-public crate-milf-0.5.8 (c (n "milf") (v "0.5.8") (d (list (d (n "indexmap") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nn004f2kk2wsmd5flnszf5qp85aymlsjchr21qnrgicbf16h1r6") (f (quote (("preserve_order" "indexmap") ("default"))))))

