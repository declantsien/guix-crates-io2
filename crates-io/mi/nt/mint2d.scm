(define-module (crates-io mi nt mint2d) #:use-module (crates-io))

(define-public crate-mint2d-0.1.0 (c (n "mint2d") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)))) (h "19ylax0b5dm3q748rqrhw3izbk0cmbz6aszss8j128s03ahq5qca")))

(define-public crate-mint2d-0.1.1 (c (n "mint2d") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)))) (h "09s40xj4vgi6hv6cariffn82pnqci9v8p7rdp1f68p271qvxwrgf")))

(define-public crate-mint2d-0.1.2 (c (n "mint2d") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)))) (h "179ww3jfm1vcagkfyyrc40qnaxjh9wjjdb7a5fh1qpasd9s24xds")))

