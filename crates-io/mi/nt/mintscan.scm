(define-module (crates-io mi nt mintscan) #:use-module (crates-io))

(define-public crate-mintscan-0.0.0 (c (n "mintscan") (v "0.0.0") (h "09zw9c0nnr6zg8lx9hcrkhgmmh02x3cvlx04wk8vxaxzgd0gn57d")))

(define-public crate-mintscan-0.0.1 (c (n "mintscan") (v "0.0.1") (d (list (d (n "iqhttp") (r "^0.0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "tendermint") (r "^0.19") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "092q7rcq1ihfxhin3j8jsk7075528a4w7c159rwja3p5j6g0sd1x")))

(define-public crate-mintscan-0.1.0 (c (n "mintscan") (v "0.1.0") (d (list (d (n "iqhttp") (r "^0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "tendermint") (r "^0.19") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1d8z2q2kkhk116rpb6r85xvwxjsn32n6krp3760jwxabydfjndls")))

(define-public crate-mintscan-0.2.0 (c (n "mintscan") (v "0.2.0") (d (list (d (n "iqhttp") (r "^0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "tendermint") (r "^0.22") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1hm2v3vp6avmmjbfgkb0ks3g3y0ryag2b25ln7hbb8ddicw5qal5") (r "1.56")))

(define-public crate-mintscan-0.3.0 (c (n "mintscan") (v "0.3.0") (d (list (d (n "iqhttp") (r "^0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "tendermint") (r "^0.23") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0mkys20sx8frbmdzsbpa67xqd3dddzczcj4cfvlfdp2v0k9hjs4v") (r "1.56")))

(define-public crate-mintscan-0.4.0 (c (n "mintscan") (v "0.4.0") (d (list (d (n "iqhttp") (r "^0.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "tendermint") (r "^0.23") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0sjgwqymyz09n4bm0v605hy2d420qz7nhxymmnaaimf2hghdapn5") (r "1.56")))

