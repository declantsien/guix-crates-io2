(define-module (crates-io mi nt mintaka-hal) #:use-module (crates-io))

(define-public crate-mintaka-hal-0.0.2 (c (n "mintaka-hal") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sysinfo") (r "^0.29.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1wkcclmjcjjgwicmbz5iqhjnm080l6syyrnn2plmyn72kdarnxi2")))

(define-public crate-mintaka-hal-0.0.3 (c (n "mintaka-hal") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sysinfo") (r "^0.29.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16jpdm2pr2ba089qpjw3zywbd2w9m9i9mdlkk668l5vnbwvqiljc")))

