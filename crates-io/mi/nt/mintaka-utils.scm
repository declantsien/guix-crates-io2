(define-module (crates-io mi nt mintaka-utils) #:use-module (crates-io))

(define-public crate-mintaka-utils-0.0.2 (c (n "mintaka-utils") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "09w6wkwn5i30kladmlzi6j1vrnckgk67aj6zapls94ijxk07xnwx")))

(define-public crate-mintaka-utils-0.0.3 (c (n "mintaka-utils") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1kqg24llrl881rmjg4b6di6dvalb9b18lj2vcdb3lspwlbrisqi5")))

