(define-module (crates-io mi nt mintaka-log) #:use-module (crates-io))

(define-public crate-mintaka-log-0.0.2 (c (n "mintaka-log") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^0.2") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v3kxbpydm12267km0yacvslnahxbwcvh12qc6lpb9idrr5j9855")))

(define-public crate-mintaka-log-0.0.3 (c (n "mintaka-log") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^0.2") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lmyxqjssw1xji7qvh6ibd4khvvz3g4nw6yj7khi3n568xlia8y6")))

(define-public crate-mintaka-log-0.0.4 (c (n "mintaka-log") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p8mi45q54lk8jrfvldva6jdg5zpmvl9fsigha4dysw2dgxmmnnd")))

(define-public crate-mintaka-log-0.0.5 (c (n "mintaka-log") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v4xkva5qm4smczk22wqc3imqjqwng6dr8hkvjvnmrm8xz8ql81g")))

