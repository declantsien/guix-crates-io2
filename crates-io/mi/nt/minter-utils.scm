(define-module (crates-io mi nt minter-utils) #:use-module (crates-io))

(define-public crate-minter-utils-0.1.0 (c (n "minter-utils") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nglfsdqnk5mpl535msxzlz41qrxvj8jdanl15jhcr14pgihv8jm")))

