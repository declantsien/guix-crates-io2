(define-module (crates-io mi nt mintaka-types) #:use-module (crates-io))

(define-public crate-mintaka-types-0.0.2 (c (n "mintaka-types") (v "0.0.2") (d (list (d (n "mintaka-consts") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("js" "v1" "v3" "v4" "v5"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1jr6q9rmgjfahbrf0w4x1c2z2xir0yl7qnmjws4l8a0nzylv4ld3")))

(define-public crate-mintaka-types-0.0.3 (c (n "mintaka-types") (v "0.0.3") (d (list (d (n "mintaka-consts") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("serde" "v1" "v3" "v4" "v5"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("js" "v1" "v3" "v4" "v5"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1025sz9083xvm1caqm3f7jfwvp9nr8sbm0mz4p2y8n5s9cs3vj0x")))

(define-public crate-mintaka-types-0.0.4 (c (n "mintaka-types") (v "0.0.4") (d (list (d (n "mintaka-consts") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "type-uuid") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("serde" "v1" "v3" "v4" "v5" "v6" "v7" "v8"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("js" "v1" "v3" "v4" "v5" "v6" "v7" "v8"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "02yddyfyyyw2m7z7az8b6cmqd0kvrpwwg4rgvnvkhd2g4z85jpl5")))

