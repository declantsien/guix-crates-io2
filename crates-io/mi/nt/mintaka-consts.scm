(define-module (crates-io mi nt mintaka-consts) #:use-module (crates-io))

(define-public crate-mintaka-consts-0.0.2 (c (n "mintaka-consts") (v "0.0.2") (d (list (d (n "vergen") (r "=5.1.5") (f (quote ("build" "cargo" "rustc"))) (k 1)))) (h "0sr390agv8x3zndl6r0sfhfgxs5mah6hzpqvzz90yx4nsgsza2rz")))

(define-public crate-mintaka-consts-0.0.3 (c (n "mintaka-consts") (v "0.0.3") (d (list (d (n "vergen") (r "=5.1.5") (f (quote ("build" "cargo" "rustc"))) (k 1)))) (h "0ipjc2zqkgs4y390ynj1fkvw4mfiap7hxcccmk8h9wwbyk6p4wr3")))

(define-public crate-mintaka-consts-0.0.4 (c (n "mintaka-consts") (v "0.0.4") (d (list (d (n "vergen") (r "=5.1.5") (f (quote ("build" "cargo" "rustc"))) (k 1)))) (h "0323b57m351lawvafavq0hm3lc6iwxmawpl68ybxxs0x3fq5k8zc")))

