(define-module (crates-io mi nt mint) #:use-module (crates-io))

(define-public crate-mint-0.1.0 (c (n "mint") (v "0.1.0") (h "0blzmmxiawfi29ck8lxv0v34fv19c33na0gkzvlwxiq8a4fc80ys")))

(define-public crate-mint-0.2.0 (c (n "mint") (v "0.2.0") (h "15r24hv9r5d70g4c76bl97qrj7wfsjz0gkfy5a4fp9mw5amswsv6")))

(define-public crate-mint-0.3.0 (c (n "mint") (v "0.3.0") (h "1nb57fzj0aj9pv2bxfvbc1cgql3iib3hnbjs4i9qjhngyc00w75y")))

(define-public crate-mint-0.4.0 (c (n "mint") (v "0.4.0") (h "0h4w49cmwshw35glmg2ly78ds2kvv7jkng1l5h821gqz0bd8rxwj") (y #t)))

(define-public crate-mint-0.4.1 (c (n "mint") (v "0.4.1") (h "1w8iszc1c0xdhlap8y1lj6x9wmwinr2w7xypx9ckn3amvskizj44")))

(define-public crate-mint-0.4.2 (c (n "mint") (v "0.4.2") (h "0z17ji3micz1lwjhsgfp6vbrxmk4dd2ia161ljhkdcvz107hxzwv")))

(define-public crate-mint-0.5.0 (c (n "mint") (v "0.5.0") (h "1sn8wcqplq6vgzw3i9g3ryz8hx73iyk0rka5mj50y6nqvh0gffv6")))

(define-public crate-mint-0.4.9 (c (n "mint") (v "0.4.9") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1kpr1ncyamhwpckkzsbyg1vyl3vdn8shxdxh9rzwksmizq1fgn49")))

(define-public crate-mint-0.5.1 (c (n "mint") (v "0.5.1") (h "0z2akkbail867wr8f0hvwwz6fxf90q254q8sx8bm25dh9fdw5rn9")))

(define-public crate-mint-0.5.2 (c (n "mint") (v "0.5.2") (h "03n6hd7lf52dcgi7k1xq510yiaqdmdbgj2z8bfd8bvjl4hny42xq")))

(define-public crate-mint-0.5.3 (c (n "mint") (v "0.5.3") (h "01j14izcqvrcsxrhi53f0cw5k92k8q1zai0nbv2vy7bksrq5h4dr")))

(define-public crate-mint-0.5.4 (c (n "mint") (v "0.5.4") (h "0c4190gr348fkfijij7vm19iagwl36mssj1irc9f6m448hbhgn68")))

(define-public crate-mint-0.5.5 (c (n "mint") (v "0.5.5") (h "1q4ivadb1lw7d5sxls2kclvmny27hwpdnwihiqixb6m3ckrm8bnl")))

(define-public crate-mint-0.5.6 (c (n "mint") (v "0.5.6") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11a1cryxgrfab435jcn03w2vx0cg81xp6iwra504pkbdhpbgi7ai")))

(define-public crate-mint-0.5.7 (c (n "mint") (v "0.5.7") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v0jfmns6kazpyzfmjpgcbw78diy5an7y2bmai522qws7zzq8gkr")))

(define-public crate-mint-0.5.8 (c (n "mint") (v "0.5.8") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1p10zjfdhm6di2kpd3ah6hrxqksqmc3vyvm13jggxf5lhha5jbhn")))

(define-public crate-mint-0.5.9 (c (n "mint") (v "0.5.9") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zw5glv8z2d99c82jy2za97hh9p6377xmf4rbwz7jynsdfxfngg5")))

