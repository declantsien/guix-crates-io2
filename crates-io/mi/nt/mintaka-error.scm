(define-module (crates-io mi nt mintaka-error) #:use-module (crates-io))

(define-public crate-mintaka-error-0.0.1 (c (n "mintaka-error") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0m73140k0ild4bpb4pqvdxricbdw12qqwlq838gkl4hfm0pr239j")))

