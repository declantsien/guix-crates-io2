(define-module (crates-io mi nt mintyml-cli) #:use-module (crates-io))

(define-public crate-mintyml-cli-0.1.0 (c (n "mintyml-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "17jlavlf50kahpb6c00xg4c2qwblafg6bsgwacb6mi239m3azrwh")))

(define-public crate-mintyml-cli-0.1.2 (c (n "mintyml-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1ps3njbxrbh3ncxi65lxjqyn4bq1b43cmhkxbabfgbzhz3qszg1c")))

(define-public crate-mintyml-cli-0.1.6 (c (n "mintyml-cli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0wm2gvgvymh814sxprm2dhnzimy9x8dmvrm6bcwwsb6j94vsgy7b")))

(define-public crate-mintyml-cli-0.1.7 (c (n "mintyml-cli") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1h60hfrqrfbblh3k6sayk72vwrns6liw4l0rfdslbn6zdam1yqvw")))

(define-public crate-mintyml-cli-0.1.8 (c (n "mintyml-cli") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1qfr1msyk5xy8cins4q694f2m7jdahi7zff9ylm5g0zwhmdsc3pc")))

(define-public crate-mintyml-cli-0.1.9 (c (n "mintyml-cli") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1nlpns2hr87i6xdmhn83m8amds2g2y9qnavj9mfyw45rbhk2bw0w")))

(define-public crate-mintyml-cli-0.1.10 (c (n "mintyml-cli") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0jggfff192m9rj0laa90cpn7zqaa5a4xd38mhlb6hqalawihfhff")))

(define-public crate-mintyml-cli-0.1.11 (c (n "mintyml-cli") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1d6h7jpmak050qjy9g2hiky0b0zqa9f1wkzcqrz0qa897qk29hiv")))

(define-public crate-mintyml-cli-0.1.13 (c (n "mintyml-cli") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1i7lnr3ghc935vrajf13hgnh22962ray6kzzm7v4pggkac5k2wpg")))

(define-public crate-mintyml-cli-0.1.15 (c (n "mintyml-cli") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1wimx19h44d2y965kl8w497wnds0pyib9bhnw6c5ria2bpnflxrv")))

(define-public crate-mintyml-cli-0.1.16 (c (n "mintyml-cli") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0f956zaqy988f9qc1k4zabhg8idq4gddcsz9h825rdg5qnr2cgyn")))

(define-public crate-mintyml-cli-0.1.17 (c (n "mintyml-cli") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "13vrx525gj78cmcsasiykaq6nglhi2zslcxnxspg9kkz6lxdw7sj")))

(define-public crate-mintyml-cli-0.1.18 (c (n "mintyml-cli") (v "0.1.18") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "string" "wrap_help"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "either") (r "^1.12.0") (d #t) (k 0)) (d (n "mintyml") (r "^0.1") (f (quote ("error-trait"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 2)))) (h "0s5yl1i3sji32rh0m6v6x1vw0b9r8nfgg8wmxw880f6d0d6rvc6m")))

