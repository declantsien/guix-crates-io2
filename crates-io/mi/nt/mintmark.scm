(define-module (crates-io mi nt mintmark) #:use-module (crates-io))

(define-public crate-mintmark-0.1.0 (c (n "mintmark") (v "0.1.0") (d (list (d (n "barcoders") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.11") (k 0)))) (h "0mhcqjh5nr71n3dlvclnfkd9rh19zj0gvxh6v62l0p7zpfaxa3wv")))

(define-public crate-mintmark-0.2.0 (c (n "mintmark") (v "0.2.0") (d (list (d (n "barcoders") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (k 0)))) (h "03f0fhnkrlpdxg33hq5d7qkkhwd1h460p0rjjik2b7pgzlc04v9x")))

