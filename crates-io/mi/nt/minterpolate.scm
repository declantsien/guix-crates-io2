(define-module (crates-io mi nt minterpolate) #:use-module (crates-io))

(define-public crate-minterpolate-0.1.0 (c (n "minterpolate") (v "0.1.0") (d (list (d (n "mint") (r "^0.4") (d #t) (k 0)))) (h "0cz6kfqdyfhanbdyv55w2v30fdvhk71hkq8cwg53nzvl696wxk2x")))

(define-public crate-minterpolate-0.2.0 (c (n "minterpolate") (v "0.2.0") (d (list (d (n "mint") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)))) (h "0hzyf2r5mqyicp7vcinayzbcv8drk75rc5ppldsnif07p5vwgjzm")))

(define-public crate-minterpolate-0.2.1 (c (n "minterpolate") (v "0.2.1") (d (list (d (n "mint") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)))) (h "146jmah2pgj89xg7y64gkmfqflxkq1jav3ay64fbjd0605dqikhx")))

(define-public crate-minterpolate-0.2.2 (c (n "minterpolate") (v "0.2.2") (d (list (d (n "mint") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0122g3d0rfg8m5xrii19wjhbs5vin9z1m310wry0zznjwb9lg91h")))

(define-public crate-minterpolate-0.3.0 (c (n "minterpolate") (v "0.3.0") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pm8nk443bh819qkmf3xdp0833qaikk12ickf0s0z1bqwr6b0ia1")))

(define-public crate-minterpolate-0.4.0 (c (n "minterpolate") (v "0.4.0") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vll6l5z3q7nsyig1dsq1hsi2kmbfpi5yhdahgvcpyd2air1clcx")))

