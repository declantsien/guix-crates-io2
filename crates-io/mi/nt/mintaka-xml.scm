(define-module (crates-io mi nt mintaka-xml) #:use-module (crates-io))

(define-public crate-mintaka-xml-0.0.4 (c (n "mintaka-xml") (v "0.0.4") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bk7hrb57bak856czmacjl63vrrsd49lpy907pn5mv8j86af2sak")))

