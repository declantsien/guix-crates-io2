(define-module (crates-io mi nt mintaka-bincode) #:use-module (crates-io))

(define-public crate-mintaka-bincode-0.0.4 (c (n "mintaka-bincode") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h063hi5pxm315vpnmvlac2yhpy9cf0dz95rpiwvs2915z1hva9q")))

