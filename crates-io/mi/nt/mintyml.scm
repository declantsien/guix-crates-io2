(define-module (crates-io mi nt mintyml) #:use-module (crates-io))

(define-public crate-mintyml-0.1.0 (c (n "mintyml") (v "0.1.0") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "0sx8q3bpds0fp9j9vll82pb56bkj5igcssz32gbzbrzif4fai60r") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "gramma/std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.1 (c (n "mintyml") (v "0.1.1") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "19ii8z5l9qh82lkmlr2h70c8fbb2k8p5ilfkga42nw0pzlxyv2g9") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "gramma/std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.2 (c (n "mintyml") (v "0.1.2") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "1sdggd8mlpy5dkfsypw0xi4qba7k3xfl91a8vvjgm0mp4shhf4by") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "gramma/std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.3 (c (n "mintyml") (v "0.1.3") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "07akfcprqq3q3mbi312bzyymzjz9q9syzn7cdba4j7paqg6aij3j") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "gramma/std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.6 (c (n "mintyml") (v "0.1.6") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "1r54jix4ylva0gn6cy5dkxk4zbm37bdhi8nxv79c9lqpi0ycvpfm") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "gramma/std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.9 (c (n "mintyml") (v "0.1.9") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "0a96njw2fl24lxh3m72s658w72a608d71k4zjxkqifbkkxdbklxa") (f (quote (("std" "gramma/std") ("default")))) (s 2) (e (quote (("error-trait" "std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.10 (c (n "mintyml") (v "0.1.10") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "1m1f559k5y8xj97hjm4izqviqgzv08jg1bwg1vmpavs2r0qwnviz") (f (quote (("std" "gramma/std") ("default")))) (s 2) (e (quote (("error-trait" "std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.11 (c (n "mintyml") (v "0.1.11") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "0ffnqgi74jrry68bcrmzj72ywai727k6z8rhls99n3vavwxcpqj0") (f (quote (("std" "gramma/std") ("default")))) (s 2) (e (quote (("error-trait" "std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.15 (c (n "mintyml") (v "0.1.15") (d (list (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0.57") (o #t) (k 0)))) (h "060mj4r4bfdhivw25q7r9s1p85yvvs2p49r2p897g61r0nlnrj6v") (f (quote (("std" "gramma/std") ("default")))) (s 2) (e (quote (("error-trait" "std" "dep:thiserror"))))))

(define-public crate-mintyml-0.1.16 (c (n "mintyml") (v "0.1.16") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "add"))) (k 0)) (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)))) (h "1jdfzr6swshz24z5k3dvlc3h3xfik1wmhiv732rhivdp2ma12r8r") (f (quote (("std" "gramma/std") ("error-trait" "std" "derive_more/error") ("default"))))))

(define-public crate-mintyml-0.1.17 (c (n "mintyml") (v "0.1.17") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "add"))) (k 0)) (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)))) (h "041bdd9l0ixfbnx8xg86lz21ffnpmy6ms9cl4sw7cnm1n3vr952v") (f (quote (("std" "gramma/std") ("error-trait" "std" "derive_more/error") ("default"))))))

(define-public crate-mintyml-0.1.18 (c (n "mintyml") (v "0.1.18") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "add"))) (k 0)) (d (n "either") (r "^1.9") (k 0)) (d (n "gramma") (r "^0.2") (k 0)))) (h "0p51splmici6rxglslg58jbcgrn2j93gar779rmba6wk3dvafla4") (f (quote (("std" "gramma/std") ("error-trait" "std" "derive_more/error") ("default"))))))

