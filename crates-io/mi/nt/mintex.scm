(define-module (crates-io mi nt mintex) #:use-module (crates-io))

(define-public crate-mintex-0.1.0 (c (n "mintex") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1fswaka30775w39cmq604p27axb7lgcs81hv9lwcgm11mr8qp9kx")))

(define-public crate-mintex-0.1.1 (c (n "mintex") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0x3dlqnqsd2jffb4wnb88alr98pz6qyfljagpki4h8hsbcivmffy")))

(define-public crate-mintex-0.1.2 (c (n "mintex") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1jsrslivgmm83lijj8804l0nm56lqdqqrydvswc398mmqfhmnz7x")))

(define-public crate-mintex-0.1.3 (c (n "mintex") (v "0.1.3") (h "01ydy8pvyy96cjvjh4hgfqmjalr6hnbyc6c8a9xwq4yvznc4bv4v")))

