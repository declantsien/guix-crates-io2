(define-module (crates-io mi nt mints) #:use-module (crates-io))

(define-public crate-mints-0.1.0 (c (n "mints") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal-fonts") (r "^0.1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)) (d (n "webster") (r "^0.3.0") (d #t) (k 0)))) (h "1n5nhl9lh3gna73gx4l73ywy190rlla2w39v703r1vhc3vblvh0s") (y #t)))

(define-public crate-mints-0.1.1 (c (n "mints") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal-fonts") (r "^0.1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)) (d (n "webster") (r "^0.3.0") (d #t) (k 0)))) (h "1nci7cihrp83x62yy28bshq79s22h2vm7alxhpd6hglm1nx775q7")))

