(define-module (crates-io mi nt mint-std140) #:use-module (crates-io))

(define-public crate-mint-std140-0.1.0 (c (n "mint-std140") (v "0.1.0") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "std140") (r "^0.2") (d #t) (k 0)))) (h "1zzg8kim66ncz38i6g13qc13swfv62x8gbvi5ib1jhinff7hsijp")))

(define-public crate-mint-std140-0.1.1 (c (n "mint-std140") (v "0.1.1") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "std140") (r "^0.2") (d #t) (k 0)))) (h "1c8qbdrz90fgk3bkjpx3rp0d84f5drv1i1sgfxsmqyxd0p59p9md")))

