(define-module (crates-io mi nt mintaka) #:use-module (crates-io))

(define-public crate-mintaka-0.0.1 (c (n "mintaka") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvrxhcrs8cyvb1a6xddcz28f1x4gzp7yrvdnqn94r6mqcy9khgv")))

(define-public crate-mintaka-0.0.2 (c (n "mintaka") (v "0.0.2") (d (list (d (n "mintaka-consts") (r "^0.0.2") (d #t) (k 0)) (d (n "mintaka-hal") (r "^0.0.2") (d #t) (k 0)) (d (n "mintaka-log") (r "^0.0.2") (d #t) (k 0)) (d (n "mintaka-types") (r "^0.0.2") (d #t) (k 0)) (d (n "mintaka-utils") (r "^0.0.2") (d #t) (k 0)))) (h "10b5d8i093zwzrlxpz6qa38xh5sg944xk15n4kb6n5zjk298px9c")))

(define-public crate-mintaka-0.0.3 (c (n "mintaka") (v "0.0.3") (d (list (d (n "mintaka-consts") (r "^0.0.3") (d #t) (k 0)) (d (n "mintaka-hal") (r "^0.0.3") (d #t) (k 0)) (d (n "mintaka-log") (r "^0.0.3") (d #t) (k 0)) (d (n "mintaka-types") (r "^0.0.3") (d #t) (k 0)) (d (n "mintaka-utils") (r "^0.0.3") (d #t) (k 0)))) (h "0m9gyinwnxnfl4nmjyns3ai205v30xhcly4ngxjfgrhg0dcgw7i7")))

(define-public crate-mintaka-0.0.4 (c (n "mintaka") (v "0.0.4") (d (list (d (n "mintaka-consts") (r "^0.0.4") (d #t) (k 0)) (d (n "mintaka-types") (r "^0.0.4") (d #t) (k 0)))) (h "0fz5mfqfz8rwmk8nz8c70jkqb5mx82v974qmdd9k26f1glac67pd")))

