(define-module (crates-io mi np minpixwin) #:use-module (crates-io))

(define-public crate-minpixwin-0.1.0 (c (n "minpixwin") (v "0.1.0") (d (list (d (n "pixels") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 0)))) (h "0jr7rb5pxk3pk3r2xic816qnmnd31x9sqc5lsl1xq2s2gdb0bxrb") (y #t)))

(define-public crate-minpixwin-0.1.1 (c (n "minpixwin") (v "0.1.1") (d (list (d (n "pixels") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 0)))) (h "1na9rmyn3wv7b8dy7m8lxysh1gchkqd3v4gkxjaglwjdf3bqs1c6") (y #t)))

(define-public crate-minpixwin-0.1.2 (c (n "minpixwin") (v "0.1.2") (d (list (d (n "pixels") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 0)))) (h "1v5yzn1psqsghnrknz199a0s36b3v6ilw6dkj6z8cyi9yfj81ri8")))

