(define-module (crates-io mi pi mipidsi-async) #:use-module (crates-io))

(define-public crate-mipidsi-async-0.1.0 (c (n "mipidsi-async") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "14s4hrmy50ab7savqd1brpqgzph4iha83f4mnqd9g5lrr8ws4skv") (f (quote (("default" "batch") ("batch" "heapless")))) (r "1.61")))

