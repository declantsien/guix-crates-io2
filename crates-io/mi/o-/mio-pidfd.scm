(define-module (crates-io mi o- mio-pidfd) #:use-module (crates-io))

(define-public crate-mio-pidfd-0.1.0 (c (n "mio-pidfd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1c1vpygjz10yig6yac6k6qpz4h7rs7dw4wfz71ry0l9f837k87wq")))

(define-public crate-mio-pidfd-0.1.1 (c (n "mio-pidfd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1vywiamp20cj6gmh138jw7z9h042swb27wc59kbji74iqd5vgpxx")))

(define-public crate-mio-pidfd-0.2.0 (c (n "mio-pidfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "10i0rzkdzj3qcq2k8jzvz768c2f78z5zwkyw539yvhndvylcbazc")))

(define-public crate-mio-pidfd-0.3.0 (c (n "mio-pidfd") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "1nx7lkyx2v503k5gz8wxmm6qv70v96kd20l8wmxiha3v2v40r2h9")))

