(define-module (crates-io mi o- mio-signals) #:use-module (crates-io))

(define-public crate-mio-signals-0.0.0 (c (n "mio-signals") (v "0.0.0") (h "0dwzlqz21k4y2brymn377cmdwf3l973g3k4va5mrv9vx8y00x6r5")))

(define-public crate-mio-signals-0.1.0 (c (n "mio-signals") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-util"))) (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-poll"))) (d #t) (k 2)))) (h "0yy3wsdqfbjyk7giky2gqqhbsy8v34yavnrpbdsgsxdvpjm6a63v")))

(define-public crate-mio-signals-0.1.1 (c (n "mio-signals") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-util"))) (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-poll"))) (d #t) (k 2)))) (h "174ypxh2pd4vj254c9kmwqf77vls6ivwg8pk05cbglq6wkczn6b8")))

(define-public crate-mio-signals-0.1.2 (c (n "mio-signals") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-util"))) (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-poll"))) (d #t) (k 2)))) (h "1vha8h3ckzi2cfpfs52ny9h19z9bm40v79jc3jpig20gsm7zd5hf")))

(define-public crate-mio-signals-0.1.3 (c (n "mio-signals") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mio") (r "^0.7.6") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "0kzf8324y25am9wshlp28wfxxj7hwk2slgsb28xwn0yhnxslpcxx")))

(define-public crate-mio-signals-0.1.4 (c (n "mio-signals") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mio") (r "^0.7.6") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "07c1j3qswwpkdv116fyzb0rpggx50aafabw9w8c2g475zwm49al1")))

(define-public crate-mio-signals-0.1.5 (c (n "mio-signals") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mio") (r "^0.7.6") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "0ycbiqsc1ykcz6kgv43s87k9n0b4mq4jpwyg8x0nyw4796b035qi")))

(define-public crate-mio-signals-0.2.0 (c (n "mio-signals") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext"))) (d #t) (k 0)))) (h "0s1kr43i3xxmcpaadvv2b3aj2nv9iav844hachj9hx684r755s91")))

