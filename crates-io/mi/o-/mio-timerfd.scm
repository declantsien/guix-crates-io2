(define-module (crates-io mi o- mio-timerfd) #:use-module (crates-io))

(define-public crate-mio-timerfd-0.1.0 (c (n "mio-timerfd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "13phvyshhawlmc93w0cd85z0jjnbkarij4wsl9aasc91jw6zmq7h")))

(define-public crate-mio-timerfd-0.1.1 (c (n "mio-timerfd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "1h5z4r40a1nb8ccv25vwwqp3b26qk0gxkr8j7kymzn2h9b99pcmg")))

(define-public crate-mio-timerfd-0.1.2 (c (n "mio-timerfd") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "19l7wbj9ybrai1g62g4nng26h21rmd7c5jirjcmxn3fia5j4gp0z")))

(define-public crate-mio-timerfd-0.2.0 (c (n "mio-timerfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-util" "os-poll"))) (d #t) (k 0)))) (h "0p1n3j0bhricr4m1adv04ninvl96fqdhjcc5f9pnms1rncwh7131")))

