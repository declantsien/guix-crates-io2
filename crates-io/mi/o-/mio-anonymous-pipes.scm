(define-module (crates-io mi o- mio-anonymous-pipes) #:use-module (crates-io))

(define-public crate-mio-anonymous-pipes-0.1.0 (c (n "mio-anonymous-pipes") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "miow") (r "^0.3") (d #t) (k 0)) (d (n "spsc-buffer") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("ioapiset"))) (d #t) (k 0)))) (h "1bqs8wncd73q4pnbiwskhgds57hyr8g89vfpqmw1vk9dqp1p9hpq")))

(define-public crate-mio-anonymous-pipes-0.2.0 (c (n "mio-anonymous-pipes") (v "0.2.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "miow") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "spsc-buffer") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("ioapiset"))) (d #t) (k 0)))) (h "0sqsr9ifvacarlmf02l0hh5ianns5kdhzdb1llx5l075bw117ibb")))

