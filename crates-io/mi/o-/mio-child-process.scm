(define-module (crates-io mi o- mio-child-process) #:use-module (crates-io))

(define-public crate-mio-child-process-0.1.0 (c (n "mio-child-process") (v "0.1.0") (d (list (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.4") (d #t) (k 0)))) (h "05lg0a94imlsqkf7xcm5hq2zf1bwb4jr85mk6aclav9m4h3vwblm")))

(define-public crate-mio-child-process-0.1.1 (c (n "mio-child-process") (v "0.1.1") (d (list (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.4") (d #t) (k 0)))) (h "1a4ljrrav8svsa617f23k98yc14sazyk0sg1q8v2x7gdfhznmij0")))

(define-public crate-mio-child-process-0.1.2 (c (n "mio-child-process") (v "0.1.2") (d (list (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.4") (d #t) (k 0)))) (h "05dgwy532lgsf0p15kfdc3fldmdwrc441ldw94wk8i98sqg5wj5w")))

(define-public crate-mio-child-process-0.2.0 (c (n "mio-child-process") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mio") (r "^0.6.15") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "06as1appnm4v29nln6wc64nj7ghqzrirw0v5f85cx4mcz470phw5")))

(define-public crate-mio-child-process-0.2.1 (c (n "mio-child-process") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mio") (r "^0.6.15") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("tlhelp32"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1x0bps0frvc60rk188mkfmddd79mgsqj76qw0flfwpyvianw5qlk")))

