(define-module (crates-io mi o- mio-pipe) #:use-module (crates-io))

(define-public crate-mio-pipe-0.1.0 (c (n "mio-pipe") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-util"))) (d #t) (k 0)) (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("os-poll"))) (d #t) (k 2)))) (h "00434wr6vwmqz24hd6wgqhhcd3lr37na23hgklm3bp3jvaqldddr")))

(define-public crate-mio-pipe-0.1.1 (c (n "mio-pipe") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-util"))) (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-poll"))) (d #t) (k 2)))) (h "1ybn7sz9krpmcjd5acv7x90wbxbvjrf6i64sj6lrcgfgmsbb9vpv")))

