(define-module (crates-io mi o- mio-more) #:use-module (crates-io))

(define-public crate-mio-more-0.1.0 (c (n "mio-more") (v "0.1.0") (d (list (d (n "lazycell") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "1q6k8kcbz50r5iq3gcw2k2d7qsyrypfl0x93mmvz38q7gnncaqq0")))

