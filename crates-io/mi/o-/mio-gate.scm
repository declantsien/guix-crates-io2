(define-module (crates-io mi o- mio-gate) #:use-module (crates-io))

(define-public crate-mio-gate-0.1.1 (c (n "mio-gate") (v "0.1.1") (d (list (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0gh1rsvga7vq4yb2hyyxwy9p3fg0xvy127h0x4agpxfcw5mzsd93")))

(define-public crate-mio-gate-0.1.2 (c (n "mio-gate") (v "0.1.2") (d (list (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "11v19csalq0k2xdwrvaq6xgvyn0kcgq30fpf691vd1zf54p1q12l")))

