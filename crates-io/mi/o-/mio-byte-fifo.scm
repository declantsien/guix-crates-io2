(define-module (crates-io mi o- mio-byte-fifo) #:use-module (crates-io))

(define-public crate-mio-byte-fifo-0.1.0 (c (n "mio-byte-fifo") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0") (d #t) (k 0)) (d (n "rb") (r "^0.3") (d #t) (k 0)))) (h "0m7w90x0n28rwc7pz6sg3drhv7zrsqvr6n1dl891l7syz951w4aa")))

(define-public crate-mio-byte-fifo-0.1.1 (c (n "mio-byte-fifo") (v "0.1.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "rb") (r "^0.3") (d #t) (k 0)))) (h "183ml21cl29a01g45gnbdv5832fq43scaldqhlzg1niflr9vqvp7")))

(define-public crate-mio-byte-fifo-0.1.2 (c (n "mio-byte-fifo") (v "0.1.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "ringbuf") (r "^0.1") (d #t) (k 0)))) (h "0lrq5jc5kjykddcmrpimxy7mpiaax32pm7i0b57si5dj88f87rg3")))

(define-public crate-mio-byte-fifo-0.1.3 (c (n "mio-byte-fifo") (v "0.1.3") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "ringbuf") (r "^0.1.4") (d #t) (k 0)))) (h "1c7hcdvbid0xfkjj0avlckx6sf1m2cgzx6aycihwv1hc2yarhjrq")))

