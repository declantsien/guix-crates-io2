(define-module (crates-io mi o- mio-pool) #:use-module (crates-io))

(define-public crate-mio-pool-0.1.0 (c (n "mio-pool") (v "0.1.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0n4v97hmyrqvprsym3g04g5aym6p1qqf7cxx0srqkkyjqif4qifh")))

(define-public crate-mio-pool-0.1.1 (c (n "mio-pool") (v "0.1.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1xb827h413vn6nh64yrkxi3qdi3hwkk9071547z75cbxlx8rhrjy")))

(define-public crate-mio-pool-0.2.0 (c (n "mio-pool") (v "0.2.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0l3chgzaci917s7r7mx4vm5cjlfhj2rdva9ic0lcwc5w78is6m55")))

(define-public crate-mio-pool-0.2.1 (c (n "mio-pool") (v "0.2.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "11j97rlkyhq7pzsj7q6y296mjda2v4dd2ld2bfp7zc5glgbgd0qf")))

(define-public crate-mio-pool-0.2.2 (c (n "mio-pool") (v "0.2.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "07a00z2x88cn0ysfcxgpm2hqqpx5q1lj7c5mkllzd401g8sgck2w")))

(define-public crate-mio-pool-0.2.3 (c (n "mio-pool") (v "0.2.3") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0fgrgiffn9fg5plqfx7p5m7x66cgrcqdsn821fi9li9mrrd9n76a")))

(define-public crate-mio-pool-0.3.0 (c (n "mio-pool") (v "0.3.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0h0mas48lwn8bcynspga96sfwcl0rldvikb8is1n1p1mmkqa389m")))

(define-public crate-mio-pool-0.4.0 (c (n "mio-pool") (v "0.4.0") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1xqv4iwx4f577c7s4f87p2rypnz2xmvnklqrkzc6kc3yy6vlryjh")))

(define-public crate-mio-pool-0.5.0 (c (n "mio-pool") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1chwmxr7f5yn45y3pa5d508psjqss2gx4qyg4lakllq0iwncrdgy")))

(define-public crate-mio-pool-0.5.1 (c (n "mio-pool") (v "0.5.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1dcndjnbjcfqc872yzdp055ykib513qbsb3l79y36zlmg8rzz84m")))

(define-public crate-mio-pool-0.5.2 (c (n "mio-pool") (v "0.5.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1d8f59jlypzbbk7605c4qp4hzds5z7q88f8d23a97dlngzjjnhsn")))

(define-public crate-mio-pool-0.5.3 (c (n "mio-pool") (v "0.5.3") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "05dafyfm2pzj1y776c5k2qlsrs3b95mwmgycgfz4nq1bnxc3kxdx")))

(define-public crate-mio-pool-0.5.4 (c (n "mio-pool") (v "0.5.4") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1c8p1b79js2r7bwp93zdhibw95lskpx82bc2pnq5zh0rvmdad4q6")))

(define-public crate-mio-pool-0.5.5 (c (n "mio-pool") (v "0.5.5") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0bi1xmzr8569av5zzzpc06bb75g0z31wcg9n2j3pdx7rsjzgl345")))

(define-public crate-mio-pool-0.5.6 (c (n "mio-pool") (v "0.5.6") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0ydv1g5b4svbwb6cp1zgkga2wlpx4cxx0ayhq0v5pd6acdp4jiw1")))

(define-public crate-mio-pool-0.5.7 (c (n "mio-pool") (v "0.5.7") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "01w9dkjhzg8kfcbwa545cyqpz176b5d142j135ggqvbjkds1jxkn")))

