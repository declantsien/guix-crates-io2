(define-module (crates-io mi o- mio-st) #:use-module (crates-io))

(define-public crate-mio-st-0.1.0 (c (n "mio-st") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.42") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "net2") (r "^0.2.33") (d #t) (k 0)))) (h "0jn1dqa3xsssqy9bwixdvqp3vi6a33vqziphyx42s2zczfzpz2gf")))

(define-public crate-mio-st-0.2.0 (c (n "mio-st") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1x0a3396mqbf2rs7zciwb16bjp2arj14n5kn5fz8ylp9bqa8i20m") (f (quote (("disable_test_ipv6") ("disable_test_deadline"))))))

(define-public crate-mio-st-0.2.1 (c (n "mio-st") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0h8nb49k6fmc5wkynanlisqk6vf8mzdqc8v27fl1ysgfw9zp3p87") (f (quote (("disable_test_ipv6") ("disable_test_deadline"))))))

(define-public crate-mio-st-0.2.2 (c (n "mio-st") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0s0l7m8rdr264a7p957pvh316mycbgrj3cmjy4sikg7bwn1ywzhj") (f (quote (("disable_test_ipv6") ("disable_test_deadline"))))))

(define-public crate-mio-st-0.2.3 (c (n "mio-st") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1253775ksc24qznsw4f503b10bs8j4mlhydqiik0h5l2ipyrycc1") (f (quote (("disable_test_ipv6") ("disable_test_deadline"))))))

