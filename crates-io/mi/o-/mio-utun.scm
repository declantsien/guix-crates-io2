(define-module (crates-io mi o- mio-utun) #:use-module (crates-io))

(define-public crate-mio-utun-0.1.0 (c (n "mio-utun") (v "0.1.0") (d (list (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ij6kncpafpfv137mlhhiygi8z9ifgqha54wk28z1vilwwan1d33")))

(define-public crate-mio-utun-0.1.1 (c (n "mio-utun") (v "0.1.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "12r13wzch8f6g3fkvdhm4wxfb9aa0qcn3lkz7w68fda3m0l95rk8")))

(define-public crate-mio-utun-0.6.1 (c (n "mio-utun") (v "0.6.1") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0ndmpk1h5qrs47y64j82dwpc2qvmriy2jaav0syqwpzdyg4r4ci9")))

(define-public crate-mio-utun-0.6.2 (c (n "mio-utun") (v "0.6.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1rd312zcsamvv0p7r4cqmc0phys8j0yxxvy297fwx1a9h953plwf")))

(define-public crate-mio-utun-0.6.3 (c (n "mio-utun") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0b7im2j5jclvghys7samq6ka1m94fgj038ajzzjn4lqaxbdq2k3a")))

(define-public crate-mio-utun-0.6.4 (c (n "mio-utun") (v "0.6.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "10hfq9mlckc135kf0vshfyaf59p6sj3jkg1gv313f0d3pssva630")))

(define-public crate-mio-utun-0.6.5 (c (n "mio-utun") (v "0.6.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1j7x5rkjns68gcwjx7lbhawgp60j9xc5kb49ny43j2cv7ya0adv1")))

(define-public crate-mio-utun-0.6.6 (c (n "mio-utun") (v "0.6.6") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0a7w844804n34lxkgnf7hpchp0riswgqvq9hqn05pr5pn6rdkb80")))

(define-public crate-mio-utun-0.6.7 (c (n "mio-utun") (v "0.6.7") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "094w7vixra6xq9a4hdx23bp246kskm4mmswp5vwgs67m4mp02yir")))

(define-public crate-mio-utun-0.6.8 (c (n "mio-utun") (v "0.6.8") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0bcvz7dnh60dr0ifdqyiw82gj5dq894gc879ijji1k6m5pr8l1by")))

(define-public crate-mio-utun-0.6.9 (c (n "mio-utun") (v "0.6.9") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1gav8kbngmk0dc0m0yk6ggsfmi5f6amkiwkqxpsgbcdcpxxdiwys")))

(define-public crate-mio-utun-0.6.10 (c (n "mio-utun") (v "0.6.10") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0nwnq74x1p5mhz1h171fqjghg5xvcls9p76fcbiqd7q15yardhm4")))

(define-public crate-mio-utun-0.6.11 (c (n "mio-utun") (v "0.6.11") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0nc8xpi53mqa7s726baz90lg8sff5nlphd51n5f8dyjvp63w25jl")))

(define-public crate-mio-utun-0.6.12 (c (n "mio-utun") (v "0.6.12") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0ifxvlyyjns94lf2hbyilm03ai2p6v1dqkygfjclggkhkly9ysi2")))

(define-public crate-mio-utun-0.6.13 (c (n "mio-utun") (v "0.6.13") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "18gaa3jhxlg9cc56c93pk27fcdky7kqlfjrm0w25vfsjygk4ql0f")))

(define-public crate-mio-utun-0.6.14 (c (n "mio-utun") (v "0.6.14") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "1pp61ppm6s1svnrnknm47prn88xnznmx2rjxwvr5crapnfqxswh4")))

(define-public crate-mio-utun-0.6.15 (c (n "mio-utun") (v "0.6.15") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "15qn413alrzi45p68mkqbdhrq8b2l0b4c8r05xd2gcq1zgk280a8")))

(define-public crate-mio-utun-0.6.16 (c (n "mio-utun") (v "0.6.16") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "04yifhxqvx1kg3ggzmsyh8rivdgrxpkqrpjxd09p0j0yqnl6nl4s")))

(define-public crate-mio-utun-0.6.17 (c (n "mio-utun") (v "0.6.17") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "1yxm3w6alf8rqmvr7kh8frxzlva5sxix1rwyqxgd4q0lq2gfcgad")))

(define-public crate-mio-utun-0.6.18 (c (n "mio-utun") (v "0.6.18") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "1pnw63gskvi7jsb9jqv2xbhkl95pwjs52hc3k70w6q5pcsc100lg")))

(define-public crate-mio-utun-0.6.19 (c (n "mio-utun") (v "0.6.19") (d (list (d (n "byteorder") (r "^1.2") (d #t) (t "cfg(all(target_family = \"unix\", not(any(target_os = \"macos\", target_os = \"ios\"))))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "0ljr4l3k1yh8bsqx902znbj1lcwblnha1jblh05vrs82yw04rima")))

