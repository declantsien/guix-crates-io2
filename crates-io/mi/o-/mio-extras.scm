(define-module (crates-io mi o- mio-extras) #:use-module (crates-io))

(define-public crate-mio-extras-1.0.0 (c (n "mio-extras") (v "1.0.0") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "132llvh8h8zslsjl5y73hf0na3yhd5adrpf2r2c8j37z448063rz")))

(define-public crate-mio-extras-2.0.0 (c (n "mio-extras") (v "2.0.0") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1ww8jncfg04d3kps2n0g53dpcff3a62zj0pkgrr1mbg6kkwghb6i")))

(define-public crate-mio-extras-2.0.1 (c (n "mio-extras") (v "2.0.1") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1kw1vxjkcwhl4swf6y16q8gb333njn242xwldk8h0qwizjs64cr7")))

(define-public crate-mio-extras-2.0.2 (c (n "mio-extras") (v "2.0.2") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0512kqfbcawd8hrjq3aszd3vi790g1xwjy74jdhy1qgdix52vs69")))

(define-public crate-mio-extras-2.0.3 (c (n "mio-extras") (v "2.0.3") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1snddcwwczwwiqxdaaydpyn45hpm8c1mkyfww3ndzsmvppmh7cx1")))

(define-public crate-mio-extras-2.0.4 (c (n "mio-extras") (v "2.0.4") (d (list (d (n "lazycell") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0x7j78s75lxfl3bh4m2kbshp1hkjkyv5wiykkpxx9lycy8c4k6vy")))

(define-public crate-mio-extras-2.0.5 (c (n "mio-extras") (v "2.0.5") (d (list (d (n "lazycell") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0h6fc7pmvsh5r1vpg5xz48lyraalsmb4s4q2v2w50qpsq823mrs6")))

(define-public crate-mio-extras-2.0.6 (c (n "mio-extras") (v "2.0.6") (d (list (d (n "lazycell") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "069gfhlv0wlwfx1k2sriwfws490kjp490rv2qivyfb01j3i3yh2j")))

