(define-module (crates-io mi kr mikrotik-rs) #:use-module (crates-io))

(define-public crate-mikrotik-rs-0.1.0 (c (n "mikrotik-rs") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "sync" "signal" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "0fqf9w7v3vmslv948kbpgpj6l34wnn0ynjbp1npjr4ncz4d5i92d")))

(define-public crate-mikrotik-rs-0.2.0 (c (n "mikrotik-rs") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "sync" "signal" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "0sjpskdjmb672py5qd3m6fhvi76qarcy1sj33f15d0f76s761rrw")))

(define-public crate-mikrotik-rs-0.3.0 (c (n "mikrotik-rs") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "sync" "signal" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "0q2svy2w84rrjwm2wm79v9rys6wjfnk3cwy73v793qjppngyyygw")))

