(define-module (crates-io mi kr mikrus-cli) #:use-module (crates-io))

(define-public crate-mikrus-cli-0.1.0 (c (n "mikrus-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jky5sb5v1finp22y79pmsg22g6v12cisyaq2rgb96wzaj6yhyr4")))

(define-public crate-mikrus-cli-0.1.1 (c (n "mikrus-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "ron") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c031gwp77vr2xj92qv8fdw9azcjyl17qshk247mpmn4iw0b3z67")))

