(define-module (crates-io mi kr mikrotik_lite) #:use-module (crates-io))

(define-public crate-mikrotik_lite-0.1.0 (c (n "mikrotik_lite") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde-aux") (r "^4.2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1fll08b0jcyjhbszgdj925lllw6fj5ay1bm8hn6y9mrdyflfdv28")))

