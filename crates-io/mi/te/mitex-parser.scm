(define-module (crates-io mi te mitex-parser) #:use-module (crates-io))

(define-public crate-mitex-parser-0.2.2-rc1 (c (n "mitex-parser") (v "0.2.2-rc1") (d (list (d (n "divan") (r "^0.1.7") (d #t) (k 2)) (d (n "insta") (r "^1.34") (d #t) (k 2)) (d (n "mitex-glob") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "mitex-lexer") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "mitex-spec-gen") (r "^0.2.2-rc1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rowan") (r "^0.15.15") (d #t) (k 0)))) (h "1sypyhbibqm3s81kkxpvm2644m1dqsmzwp4m1wamz7jkda22izl8")))

(define-public crate-mitex-parser-0.2.2-rc2 (c (n "mitex-parser") (v "0.2.2-rc2") (d (list (d (n "divan") (r "^0.1.7") (d #t) (k 2)) (d (n "insta") (r "^1.34") (d #t) (k 2)) (d (n "mitex-glob") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "mitex-lexer") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "mitex-spec-gen") (r "^0.2.2-rc2") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rowan") (r "^0.15.15") (d #t) (k 0)))) (h "033nla0haqs5hqik903r45nsd4xwjlprpg5dywp6ija1bki9c9qx")))

