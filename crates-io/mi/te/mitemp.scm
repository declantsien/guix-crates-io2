(define-module (crates-io mi te mitemp) #:use-module (crates-io))

(define-public crate-mitemp-0.3.0 (c (n "mitemp") (v "0.3.0") (d (list (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "main_error") (r "^0.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0vrcjbwnk3grmbxzjqvvyjd1nk24h046pllbxvc9r42dhvmiafcy")))

(define-public crate-mitemp-0.3.1 (c (n "mitemp") (v "0.3.1") (d (list (d (n "btleplug") (r "^0.9.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "main_error") (r "^0.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0ky2cqaa5hcj5d6kznwj5nm9qpfp5zibrcxbzhrk1hh5pmjzqlvi")))

