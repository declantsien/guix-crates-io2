(define-module (crates-io mi te miter) #:use-module (crates-io))

(define-public crate-miter-0.1.0 (c (n "miter") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0snxxf4jzf960b00dqm951yi86jpgxnn4s1fqzz4yns7yp10cfk3")))

(define-public crate-miter-0.2.0 (c (n "miter") (v "0.2.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1djidjw678gdzh113xca317ddr7qz8pjzqlcg731b96ajwnjh9bk")))

(define-public crate-miter-0.3.0 (c (n "miter") (v "0.3.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1ry1jbpk77cw9xf491baj5vqqa924nfg49xj1yrs5p4s8pqmkp9m")))

(define-public crate-miter-0.5.0 (c (n "miter") (v "0.5.0") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1rjbl5jhmq26sabv9h1dvym7ppdnylgh5n78w663rnksmk33xa8k")))

