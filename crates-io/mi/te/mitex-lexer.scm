(define-module (crates-io mi te mitex-lexer) #:use-module (crates-io))

(define-public crate-mitex-lexer-0.2.2-rc1 (c (n "mitex-lexer") (v "0.2.2-rc1") (d (list (d (n "divan") (r "^0.1.7") (d #t) (k 2)) (d (n "ecow") (r "^0.2.0") (d #t) (k 0)) (d (n "ena") (r "^0.14.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.34") (d #t) (k 2)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "mitex-spec-gen") (r "^0.2.2-rc1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0j78fjwk2s1cl3xgmb6lgfh18rqnlfd4n8a0136smban3psxjsz5")))

(define-public crate-mitex-lexer-0.2.2-rc2 (c (n "mitex-lexer") (v "0.2.2-rc2") (d (list (d (n "divan") (r "^0.1.7") (d #t) (k 2)) (d (n "ecow") (r "^0.2.0") (d #t) (k 0)) (d (n "ena") (r "^0.14.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.34") (d #t) (k 2)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "mitex-spec-gen") (r "^0.2.2-rc2") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "14d0hl3x00h7dx93pncalinbhj9h15d8m4a3f5kjvp2rlvm4svv3")))

