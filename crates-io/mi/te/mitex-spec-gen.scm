(define-module (crates-io mi te mitex-spec-gen) #:use-module (crates-io))

(define-public crate-mitex-spec-gen-0.2.2-rc1 (c (n "mitex-spec-gen") (v "0.2.2-rc1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "mitex-spec") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 1)) (d (n "which") (r "^5.0.0") (d #t) (k 1)))) (h "0s2sfvymwv6vwc5hwmh1w82nakk4lhb1s46i9adqc6678zws7l24") (f (quote (("prebuilt") ("generate"))))))

(define-public crate-mitex-spec-gen-0.2.2-rc2 (c (n "mitex-spec-gen") (v "0.2.2-rc2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "mitex-spec") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "mitex-spec") (r "^0.2.2-rc2") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 1)) (d (n "which") (r "^5.0.0") (d #t) (k 1)))) (h "1p7a95n6digscw8nhs7y48cs596qh54yra0260kqpp87qj6mb8n2") (f (quote (("prebuilt") ("generate"))))))

