(define-module (crates-io mi te mitex-glob) #:use-module (crates-io))

(define-public crate-mitex-glob-0.2.2-rc1 (c (n "mitex-glob") (v "0.2.2-rc1") (h "03rw9ynxjqk7ai93gfi72nr6f3rnfnhs0iqarhynii4sgr3xdzaa")))

(define-public crate-mitex-glob-0.2.2 (c (n "mitex-glob") (v "0.2.2") (h "1hy3xlj7qi32l1k3n0jk1r1svr1g5vbbl9r9mx73k5xvvynsf0b6")))

(define-public crate-mitex-glob-0.2.2-rc2 (c (n "mitex-glob") (v "0.2.2-rc2") (h "1hrvbxqv93cimpwscrgdaq2101n6lhzv7jma4plq1a93r0k20l56")))

