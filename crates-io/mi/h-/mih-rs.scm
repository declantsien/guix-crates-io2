(define-module (crates-io mi h- mih-rs) #:use-module (crates-io))

(define-public crate-mih-rs-0.1.0 (c (n "mih-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0hzh71v7lxhm0siwpq29xi43daq86vp59z4b27nrnclic7jlw4jg")))

(define-public crate-mih-rs-0.1.1 (c (n "mih-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "193qadw9108daka84pjf6pjrpwvwlcwqqk0k9j2dbycgf4fjsvs8")))

(define-public crate-mih-rs-0.2.0 (c (n "mih-rs") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ycc4c08xqhr5m7kxhakr335h70filvnzn8frkalqsj9q2fga3yd")))

(define-public crate-mih-rs-0.2.1 (c (n "mih-rs") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pw5amz0h4ki4w4nm7zisl8xd07d9zpqkrhi6mglbdvnyfmnq28i")))

(define-public crate-mih-rs-0.3.0 (c (n "mih-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0i4wxglg2gf79q5adq7208gd9c0mhgmvfacqrn1056sp4j62yl4r")))

(define-public crate-mih-rs-0.3.1 (c (n "mih-rs") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "08gaqi8bizymhr46cskf6yfsb3a3qqlsn0cjiq3622fx9adnvcj8")))

