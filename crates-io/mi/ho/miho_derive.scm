(define-module (crates-io mi ho miho_derive) #:use-module (crates-io))

(define-public crate-miho_derive-3.7.1 (c (n "miho_derive") (v "3.7.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jir5p6v0xzkkgxvbwfwfxzg1w70l0w0q8yd9iinlpy1w8z719g2")))

(define-public crate-miho_derive-3.7.2 (c (n "miho_derive") (v "3.7.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0d2jzm0dvihi71hnc2sdsa3ry2z5ig5arq1brisjdq7blgmx4cc1")))

(define-public crate-miho_derive-3.7.3 (c (n "miho_derive") (v "3.7.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05c57fm7gnawd37fsvnq9kgrp2gf0narcnrlgignnfdiqyviy8nz")))

(define-public crate-miho_derive-3.7.4 (c (n "miho_derive") (v "3.7.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bvkv4qr7sxw3jrk5n5hn0kn8gaahfzj2y93sls27z26gci28jvj")))

(define-public crate-miho_derive-3.7.5 (c (n "miho_derive") (v "3.7.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ihxrixjd2kxfsca62ax0ivxxa28riw301hbs257qga7qj57svrg")))

(define-public crate-miho_derive-3.7.6 (c (n "miho_derive") (v "3.7.6") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0d66syr2nplxk58al0kwbr9hhbl2ji17ki3x2xz9rlzvf26xmf30")))

(define-public crate-miho_derive-3.7.7 (c (n "miho_derive") (v "3.7.7") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p15645yb8xdjghcyklza9cmdj318d9x6c8lyyx3wag8ph07w902")))

(define-public crate-miho_derive-3.7.8 (c (n "miho_derive") (v "3.7.8") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wxswqcshrjf3zk59dykbly6nl7851hlg36lkvjbnsizbdlxp31z")))

(define-public crate-miho_derive-3.7.9 (c (n "miho_derive") (v "3.7.9") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nwh9i5n6yqa7wlpmm0iyaw6kh2h35ssyhwj6821srqhn8zsxb98")))

(define-public crate-miho_derive-3.7.10 (c (n "miho_derive") (v "3.7.10") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "055ijncf9ajq0wd5y085kpsl47cnk3npmr506skjz402449h5al8")))

(define-public crate-miho_derive-3.7.11 (c (n "miho_derive") (v "3.7.11") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0pl4zijw39kl2mdqyjxfsys258jz8xskyj092skl06i34ggpb95b")))

(define-public crate-miho_derive-3.7.12 (c (n "miho_derive") (v "3.7.12") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kr2za2smfgrg8jbssrapvpyz6amz30k9nb3ig1c5pyxmnxv8krw")))

(define-public crate-miho_derive-3.7.13 (c (n "miho_derive") (v "3.7.13") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xa008szlpw06rgjlzdpxmcp0z9dpfkdmnp65n1bnnq2z0v1v8dq")))

(define-public crate-miho_derive-3.7.15 (c (n "miho_derive") (v "3.7.15") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13qw3i96vkxpwhc8gc0bbfgyyir0j6p5n4pipvvm7v009gib76yj")))

(define-public crate-miho_derive-3.7.17 (c (n "miho_derive") (v "3.7.17") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bh74rbwb01f34hhx0ha7gkmzd1z165z7sykd8csjxk97ygk290y")))

(define-public crate-miho_derive-3.7.19 (c (n "miho_derive") (v "3.7.19") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1whxf1zc8y85z4nzsrp8n4lncyk9njbaz9pv02zvkvr9layx1vbx")))

(define-public crate-miho_derive-3.7.20 (c (n "miho_derive") (v "3.7.20") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18bpxy99ivk7c864ck3n2hq50vgzqcz916pbccxmh9i06wasjs3m")))

(define-public crate-miho_derive-3.7.21 (c (n "miho_derive") (v "3.7.21") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kb323yfkxl14cqr2hbjj9kcc56acr2vnwpqfn37fk6b81qjcgz7")))

(define-public crate-miho_derive-4.0.0 (c (n "miho_derive") (v "4.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0h9zs2rzz52naxwax0z17ji2ym0iqrll354vpjji3hxx6mj4xjf4")))

(define-public crate-miho_derive-4.1.0 (c (n "miho_derive") (v "4.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0vhnx0dn01sim1w5c8b69fbc7hpkpibcgjw8c3f39qabcv5vg8k3") (r "1.76")))

(define-public crate-miho_derive-4.1.1 (c (n "miho_derive") (v "4.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0ilniyljxinbdmflm7vdbxrm62fmqyy5462vy0f37jiwg9kklisp") (r "1.76")))

(define-public crate-miho_derive-4.1.2 (c (n "miho_derive") (v "4.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "1vb438vvr9z7bil78l6n26aq4ih5lnlg6a8kb4clw6vhwa400fh0") (r "1.76")))

(define-public crate-miho_derive-4.1.3 (c (n "miho_derive") (v "4.1.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0ay0ik154nzbfw3zjh6xrd1wksxd768wal94m75fppl2n5rj83yx") (r "1.76")))

(define-public crate-miho_derive-4.1.4 (c (n "miho_derive") (v "4.1.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0ffzc1049n6id5mvgvhnj3l8d9g3r2rgfv0m1y4k54g8w37k9pkx") (r "1.76")))

(define-public crate-miho_derive-4.1.5 (c (n "miho_derive") (v "4.1.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0qljrlxlk0kq9dmrmzljn6fc4imnsdymvvdwj24jdfmq3f3pygvm") (r "1.76")))

(define-public crate-miho_derive-4.1.6 (c (n "miho_derive") (v "4.1.6") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "19a2wvzha8nl505ip41ylvxkb1hg9kllzscz8xq6n2r4awgf7m0b") (r "1.76")))

(define-public crate-miho_derive-4.2.0 (c (n "miho_derive") (v "4.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0hxpnf1qmsnqhv3cbq8p62l92dwk2rrh7p3cfld14x9nl0078bdw") (r "1.76")))

(define-public crate-miho_derive-4.2.1 (c (n "miho_derive") (v "4.2.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "107k39f7j91a1ycxjm0i73mczx55acjy5knnrkmw7n1i5hcs4sw7") (r "1.76")))

(define-public crate-miho_derive-4.3.0 (c (n "miho_derive") (v "4.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0qn44jx976h24cgw0kvrcsaqw43byddgb87af06amycl58q0sz0g") (r "1.76")))

(define-public crate-miho_derive-4.3.1 (c (n "miho_derive") (v "4.3.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1s3isffwgnbm0w6va3ywy7007s8xyr4045ihzf7hpmz13pqk2cqr") (r "1.76")))

(define-public crate-miho_derive-4.3.2 (c (n "miho_derive") (v "4.3.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0g54f3f2ykkp2f33crg7jran1dx8703gnjigmafajclxvvicvlhi") (r "1.76")))

(define-public crate-miho_derive-5.0.0 (c (n "miho_derive") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "14jjy1lh4bfrgdgm4r3knh38g7y4fb6laj19q0g617gamhgm1il2") (r "1.76")))

(define-public crate-miho_derive-5.0.1 (c (n "miho_derive") (v "5.0.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1hlx4m2f9lwmj9ysz0wm0mq0qpwra8ak8cb6hymv5f1ijva8pw7j") (r "1.76")))

(define-public crate-miho_derive-5.0.2 (c (n "miho_derive") (v "5.0.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0cz6isdvfaprhbbdj61zqxs1ygh4pasg93ywiqbmgw2wcb77dcmb") (r "1.76")))

(define-public crate-miho_derive-5.0.3 (c (n "miho_derive") (v "5.0.3") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1vjjwygbjwqzn4ymfg21f6gjni63rp8aq0bii173xlx52g5d0phx") (r "1.76")))

(define-public crate-miho_derive-5.0.4 (c (n "miho_derive") (v "5.0.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0vff69qpczz7mf5imj9r6d7278smni8vjnp658ymgxp6jpfimm5x") (r "1.78")))

(define-public crate-miho_derive-5.0.5 (c (n "miho_derive") (v "5.0.5") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "0bg34ksspf8xk4s3qg82nac5m9xk7c0qf9np7fpzsr8igdz99q6b") (r "1.78")))

(define-public crate-miho_derive-5.0.6 (c (n "miho_derive") (v "5.0.6") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "03ady4aa5f7k46zad531qzy58kvs4i0gsb7in06khssp26bf37hr") (r "1.78")))

