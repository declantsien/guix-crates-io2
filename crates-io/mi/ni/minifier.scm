(define-module (crates-io mi ni minifier) #:use-module (crates-io))

(define-public crate-minifier-0.0.1 (c (n "minifier") (v "0.0.1") (h "17m39infxydha9m9hy3rihz8fdyy08y3hqp28zgacxsrclmlkc8n")))

(define-public crate-minifier-0.0.2 (c (n "minifier") (v "0.0.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1agrfl12dg9q1q7x26b0kgf728n0in3avqw62lrriv9013crr3nb")))

(define-public crate-minifier-0.0.3 (c (n "minifier") (v "0.0.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "182yv1yrx08xl66r7db1v62hv9vgjng0nqwrizfrf2j9qy00hbpr")))

(define-public crate-minifier-0.0.4 (c (n "minifier") (v "0.0.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1kasfzxg3kc1w8adicjxy005pklmskakh52gzszfwr4xwx7h5wpw")))

(define-public crate-minifier-0.0.5 (c (n "minifier") (v "0.0.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16yd9v8p9k2nfrx2vj0lhr3jlgw4qlbdbb0645byahf0lvdlwadz")))

(define-public crate-minifier-0.0.6 (c (n "minifier") (v "0.0.6") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0pw63m086bxpz1w4bb5jsb8kd25zj69dir1j4i3ls1dfqvqsl0cf")))

(define-public crate-minifier-0.0.7 (c (n "minifier") (v "0.0.7") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0wlcp0dryixdxf05msbz95kmjrnb3m5i47ps5idvn92x2m8hvlrk")))

(define-public crate-minifier-0.0.8 (c (n "minifier") (v "0.0.8") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0pdbgywwc95ifq3afbam0wccpw1777x37nq56bc6j3bpdsv3nnhg")))

(define-public crate-minifier-0.0.9 (c (n "minifier") (v "0.0.9") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0c716w1fqhwlfaqka1qhdxaxgjd17nxhp47v8pwhzxlwql8p1rcn")))

(define-public crate-minifier-0.0.10 (c (n "minifier") (v "0.0.10") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1sc9m1yynfxc5x9zabpmq608pwr7d3nxfxvcyrzdzqqdmfm1419g")))

(define-public crate-minifier-0.0.11 (c (n "minifier") (v "0.0.11") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1pabnzwnnr8zf1lcigahp74inr63vpbbyjkyaqb1p65i9mmf7wr6")))

(define-public crate-minifier-0.0.12 (c (n "minifier") (v "0.0.12") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0gdgkb19a8g00bkkvyqp72xvwyzcqnv4w1dxzdys2i14xcgw4rgk")))

(define-public crate-minifier-0.0.13 (c (n "minifier") (v "0.0.13") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0pzwc3q03sw15vah57vc2k1slrdplfws7dl3y8ygac4kcj8y9mdr")))

(define-public crate-minifier-0.0.14 (c (n "minifier") (v "0.0.14") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1p9mclsy20q86nfbpl7zl2vpid2hh086wzyny9h0slw5lgwmgjvq")))

(define-public crate-minifier-0.0.15 (c (n "minifier") (v "0.0.15") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "12wypbj5r6h0gdcgzzpzvw29v3xcvq9x7gpn088irs2aqsix6j1y")))

(define-public crate-minifier-0.0.16 (c (n "minifier") (v "0.0.16") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1jrvpw7cdcva66qavy9nvimg6xicxv32iawhmi538gmz5ci4qma4")))

(define-public crate-minifier-0.0.17 (c (n "minifier") (v "0.0.17") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0sv7zsay70zh72vnm9f9wl3qjjiwhg6kjpflm6hw50j851vvkkzq")))

(define-public crate-minifier-0.0.18 (c (n "minifier") (v "0.0.18") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0s5l2crlk32rdjasd0xaj62kj2b3ywfjan91jhxx41g7ydr6scjq")))

(define-public crate-minifier-0.0.19 (c (n "minifier") (v "0.0.19") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04dw4xz3jyvjvv22k1wxlq6xlfjahqxabp0znhdc547rc9yfs24r") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.20 (c (n "minifier") (v "0.0.20") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0x7lndcfsng9n2316nb6av34mcv02zbqmcdjj8rk76y38nxnkhln") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.21 (c (n "minifier") (v "0.0.21") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kxz66hm18xy8cih807p8syljvzbvk1xa9xvqh4vm3423gzx01wp") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.22 (c (n "minifier") (v "0.0.22") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13fak09x5zlcq9g4fxg1anxj4cdp4lzny1nl1w79pzplahmvym3k") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.23 (c (n "minifier") (v "0.0.23") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cppyn9zcc6ir4x78naapk0b8gvkbdyky15xirhkd4qcdsx5fqgy") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.24 (c (n "minifier") (v "0.0.24") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hi737kjjwkz6cygi2x01f4qd341pvbyfj77rp4c480mrj2cr6vm") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.25 (c (n "minifier") (v "0.0.25") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wssi5cypkwys6f9p0xfp4v8f49zhrv3q7czr64liw72b64bil3a") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.26 (c (n "minifier") (v "0.0.26") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11br59j97firyfmhj533ic6cjkx82v41gxx99q234cypmx2xz6gj") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.27 (c (n "minifier") (v "0.0.27") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0irm57c8ys6khs281akywnij7p9cdskxp2nq6dxwhrfi48n86agh") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.28 (c (n "minifier") (v "0.0.28") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jp4z0km0vrr2lss7qc3brh93k33rzrmgzvgdgbckp2i4x89ha1s") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.29 (c (n "minifier") (v "0.0.29") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02r17fkfcikck6bj5xrgq8i6l3gy9n36wi505lz97c8p4v5m0j8z") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.30 (c (n "minifier") (v "0.0.30") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1h687qjj0gfad23j9gsb0hll6c6zihbdm1i0vnh3l7znxmw9x42c") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.31 (c (n "minifier") (v "0.0.31") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "030v25536568lll9s95nq991w8qmi33pl1k48in0hjxsxczqb0g9") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.32 (c (n "minifier") (v "0.0.32") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cf7kfbsli41khgis7a3pjf20f5ja1kjzfvy99fwi1jr2awglwng") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.33 (c (n "minifier") (v "0.0.33") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ckkbvdywqn6id5i7x297w399hikzr9clxyshxvn4pjz8yr0vgvh") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.34 (c (n "minifier") (v "0.0.34") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17l4fz61i2fwrclf761fa4jccvjn4zcwkxp9v2dxbxz9wh29bg1x") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.35 (c (n "minifier") (v "0.0.35") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ill1c3hqkdhnr9slh8jlxs4hg45yn3m3kzc7ywa4ac04i7g52zs") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.36 (c (n "minifier") (v "0.0.36") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wwnwz0djmfclkgamp7bz0fy2l4lig94f7avya4gsix7d2ks0lhi") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.37 (c (n "minifier") (v "0.0.37") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1a1aa2baq2yv3akay2vxk9a9sldcjkqnb9qfnszkvvc3bii0v3gk") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.38 (c (n "minifier") (v "0.0.38") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "070ah2immfiah35lqlqp4h8vr96idnvgiv4szm0r4j402gwc507p") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.39 (c (n "minifier") (v "0.0.39") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "164ds8jiqqh3n85h957xdhcijapq8hg1y39fgd58vjf9wn6n3pvc") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.40 (c (n "minifier") (v "0.0.40") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1j9j9gqhzdyxmqibamhjpb73ivnh5c8afbp7lsbmfgd2cfl053zd") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.41 (c (n "minifier") (v "0.0.41") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "026c63xq384kbppjkp6akl8s4rqwrzhbhpsy9ybjnkw340nm952m") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.42 (c (n "minifier") (v "0.0.42") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16bxxzx2k49ll2sp29fq6dd0azlr4q66afr4fn4ada7d2y2ki8am") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.0.43 (c (n "minifier") (v "0.0.43") (d (list (d (n "macro-utils") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pk99snc18b1ckwc34zxmxnf2rjg0miagyhafbqlml7llsym44yq") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.1.0 (c (n "minifier") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0whsf0ld1y8d7pgn9s9rqxbxzkm0xz6l6kv2pz539qcq51zd2wbh") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.2.0 (c (n "minifier") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "1c5838kip18n3gpngp53s30x30g9iqsakcx79i1a9diax39nkyyd") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.2.1 (c (n "minifier") (v "0.2.1") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "17zzx5i0dkfjnadrrz6njxycg40nizggdzssz51n882zlvkx35mc") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.2.2 (c (n "minifier") (v "0.2.2") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0aap8ga2yy82r9gkjv8pacd9wb3fzfgbzrjl2ac4dx7j98vj5c4f") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.2.3 (c (n "minifier") (v "0.2.3") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0vgx8b8d0pvqbmykvr4qmr2cakkycp5wzn9gq2vb5d12chvsm52k") (f (quote (("html" "regex"))))))

(define-public crate-minifier-0.3.0 (c (n "minifier") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "10j2x1k91sj20ysg7aszphci71gd2mxbcl2llb184d5cp6bbzfwm") (f (quote (("html" "regex"))))))

