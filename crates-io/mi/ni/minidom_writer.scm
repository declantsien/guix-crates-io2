(define-module (crates-io mi ni minidom_writer) #:use-module (crates-io))

(define-public crate-minidom_writer-1.0.0 (c (n "minidom_writer") (v "1.0.0") (d (list (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07pm92qignddcb5vny919f6n8rrqk1a8cyalvblsiibaqcxwn1a8")))

