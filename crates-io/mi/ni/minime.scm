(define-module (crates-io mi ni minime) #:use-module (crates-io))

(define-public crate-minime-0.1.0 (c (n "minime") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qli0mvhlyq402h27dspq7ca0b6vjrpsbz32g871qcn5s8dlg4av") (f (quote (("bin" "clap"))))))

(define-public crate-minime-0.1.1 (c (n "minime") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09227501fb1bxzm1agjnp3hx1sw2fdl8ixfd5y98yc0ay3zv8pyx") (f (quote (("bin" "clap"))))))

(define-public crate-minime-0.2.0 (c (n "minime") (v "0.2.0") (d (list (d (n "arboard") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "ropey") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "010gw6i7xwnanhy17fshmwknr8nfdgank0gd4ka89fnvhpjrk22p") (f (quote (("unstable" "arboard") ("bin" "clap"))))))

