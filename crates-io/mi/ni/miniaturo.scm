(define-module (crates-io mi ni miniaturo) #:use-module (crates-io))

(define-public crate-miniaturo-0.6.0 (c (n "miniaturo") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("std" "derive" "help" "usage" "cargo" "error-context" "wrap_help"))) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg" "tiff"))) (k 0)) (d (n "libopenraw") (r "^0.1.1") (d #t) (k 0)))) (h "0ixaw8rmam4xrrfx8ddma18crk232fkaj8wqkh81nxgq6c39zx10")))

