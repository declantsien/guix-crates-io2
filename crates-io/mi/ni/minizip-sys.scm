(define-module (crates-io mi ni minizip-sys) #:use-module (crates-io))

(define-public crate-minizip-sys-0.1.0 (c (n "minizip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0vf57casdazi9h5fiziappwhd51y70qwlpj71xzslr3i39g96zp6") (l "minizip")))

(define-public crate-minizip-sys-0.1.1 (c (n "minizip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0ma4vphlss4r2d0hf52wxxfd85jhgbvillwn3k1hbx4jgzjhiapl") (l "minizip")))

(define-public crate-minizip-sys-0.1.2 (c (n "minizip-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0w769cvg6k58d4aypka2c5kiqz3ah5rnxs09m6c4fsx08q3ddna8")))

