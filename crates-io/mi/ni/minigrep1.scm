(define-module (crates-io mi ni minigrep1) #:use-module (crates-io))

(define-public crate-minigrep1-0.1.0 (c (n "minigrep1") (v "0.1.0") (h "077y7rb3wkczb8dzs2437rgiqwsdi0dzklx13jdhjsnf8ickcvd8")))

(define-public crate-minigrep1-0.1.1 (c (n "minigrep1") (v "0.1.1") (h "09r6j1mamp3d7nj7kjqxrj713rd5p32jvjjk7walva9x1ffi21a0")))

