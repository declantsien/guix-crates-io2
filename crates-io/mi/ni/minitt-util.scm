(define-module (crates-io mi ni minitt-util) #:use-module (crates-io))

(define-public crate-minitt-util-0.1.0 (c (n "minitt-util") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0y449m0szczibfxcnqkjcn93hc57hypwazvpv0a96k2x7xdzi8cl") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.1.1 (c (n "minitt-util") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "00i190v9wyj559b1smb9mwvfrap88x80hfyycbmpf7wcg3sbx7rf") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.2.0 (c (n "minitt-util") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0clwj3v3g3058gimmbbd5xbpak5vi13akfa6kvddnc4fa85iwjb2") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.2.1 (c (n "minitt-util") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "19pl1h1gbg31gwv56fy8z2m0670bcbmm1fc2da75y1zr1aqy8rzw") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.2.2 (c (n "minitt-util") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0a8nphln595qsml5a0jvpfvynccc3lhrg5mb8jq7j8lq69y5fz5y") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.2.3 (c (n "minitt-util") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1mmkgaqp2q83qz06a0nij9wl018h1rv946dr7jp8x88s81zmlv6n") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

(define-public crate-minitt-util-0.2.4 (c (n "minitt-util") (v "0.2.4") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rustyline") (r "=6.3") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "12raxx748pmx8h5fz57bw3h3lvh2hvkzdv2dxnrini77bfn9fmg0") (f (quote (("repl" "rustyline") ("cli" "clap" "structopt"))))))

