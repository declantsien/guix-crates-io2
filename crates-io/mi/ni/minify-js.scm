(define-module (crates-io mi ni minify-js) #:use-module (crates-io))

(define-public crate-minify-js-0.0.1 (c (n "minify-js") (v "0.0.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "03184cl7cgzzx5j7bbdnjxwz4dd7lxw59dv5c692a03k63zz2qs8")))

(define-public crate-minify-js-0.0.2 (c (n "minify-js") (v "0.0.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "0qpc9q9z8y7djvhygyhgshqhg5lw795mf8p0v62lvq1k34r4f364")))

(define-public crate-minify-js-0.1.0 (c (n "minify-js") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "0bpk11ix0nivxjyxp6ni4i2137miyq5985s7h7az9mjv7iyk6r1a")))

(define-public crate-minify-js-0.1.1 (c (n "minify-js") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "0kjgikr3ys31kpy6nvcydbvzzwhlm96jc59j6gmip2w3r53q7fpx")))

(define-public crate-minify-js-0.1.2 (c (n "minify-js") (v "0.1.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "1mzkf4cp77kw4izyr3q4bb6kjb4w82kmncfrsw38i0w8cp1lw9kh")))

(define-public crate-minify-js-0.1.3 (c (n "minify-js") (v "0.1.3") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "1zjj2xkhxyn1591b6zq35qdmd21f3fs0x2d9divvkq33qw7br32r")))

(define-public crate-minify-js-0.1.4 (c (n "minify-js") (v "0.1.4") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "16cnmxvvr2h4p7s24il6yh0mqg0lqmrqmayvwkzgadxpq7xapvf0")))

(define-public crate-minify-js-0.1.5 (c (n "minify-js") (v "0.1.5") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "0ldj34pgpdzh9agn4pkw7mdz2m121lb7bvls8x3riy7sr30231a2")))

(define-public crate-minify-js-0.2.0 (c (n "minify-js") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "091dda528qcvncvjb0v2vindaxc8lr6p9znzrjvvqlhk4gwrxr1q")))

(define-public crate-minify-js-0.2.1 (c (n "minify-js") (v "0.2.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "1k922i9y4k8hig55fqbagyh2qj05s7q8xxa4zgkwgzs0p2wv473a")))

(define-public crate-minify-js-0.2.2 (c (n "minify-js") (v "0.2.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "12vih7qgrp66wk5w3sgpjgdhir48l6h32b4s2yf0gjkvkjpi83yq")))

(define-public crate-minify-js-0.2.3 (c (n "minify-js") (v "0.2.3") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "1i2rya6xrxzb7r5h4swxhjws2lp56kca0292fcpjzlhkiplx2bza")))

(define-public crate-minify-js-0.2.4 (c (n "minify-js") (v "0.2.4") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "similar") (r "^2.1.0") (d #t) (k 2)))) (h "1b2w9cz260ffhgh3x5ap9rgnhqk83dyimpn9yrshd4jvgvg5gq1m")))

(define-public crate-minify-js-0.2.5 (c (n "minify-js") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.2.6") (d #t) (k 0)))) (h "0p4l3p7j90l0qvbvw6ygs216bnavh4g7i9dm5f69qfm9ga4k5xyr")))

(define-public crate-minify-js-0.2.6 (c (n "minify-js") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.2.6") (d #t) (k 0)))) (h "1lmj5cg9qiqh7liz3xgh2r3nm26nqfnss3r2sdwj0drz72y91n7r")))

(define-public crate-minify-js-0.2.7 (c (n "minify-js") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.3.0") (d #t) (k 0)))) (h "1ggywz0p1pqvwkix9k2lz0l8iilhbwzv4w6abqkrk326iryxdxcb")))

(define-public crate-minify-js-0.2.8 (c (n "minify-js") (v "0.2.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.3.0") (d #t) (k 0)))) (h "0yi68zlwnzqrxdsmmsmr0p24d5smshwry36znk7bydxnw409qw5p")))

(define-public crate-minify-js-0.2.9 (c (n "minify-js") (v "0.2.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.3.0") (d #t) (k 0)))) (h "0dlcqqwbpj7ca96jd9pk4sppbywga542gqppdirmj4askxq37q3z")))

(define-public crate-minify-js-0.2.10 (c (n "minify-js") (v "0.2.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.3.2") (d #t) (k 0)))) (h "10db1g9shdj308wbkx6hcjs0k1m0icdirvmhs1cb0ik194j5m0hv")))

(define-public crate-minify-js-0.2.11 (c (n "minify-js") (v "0.2.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.3.4") (d #t) (k 0)))) (h "0z97382l9mrbph96cwvyk3qy04yzpimwr2mcmxfw1vkd9xvybkw5")))

(define-public crate-minify-js-0.3.0 (c (n "minify-js") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.4.0") (d #t) (k 0)))) (h "0h5rm4qp63plnpcdzwswh32pj38az0ciy6d9hgxirl6skil89srq")))

(define-public crate-minify-js-0.4.0 (c (n "minify-js") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.8.0") (d #t) (k 0)))) (h "16qv5wwbkg1w4r500isjf5q8s8fs91vlg4nqm6jdacl7cbq202r1")))

(define-public crate-minify-js-0.4.1 (c (n "minify-js") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.10.0") (d #t) (k 0)))) (h "1dcd5yrpjqc0gv4v15fx9xl665gb6b3q9vqfwdgdihdsbfcblv9i")))

(define-public crate-minify-js-0.4.2 (c (n "minify-js") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.10.3") (d #t) (k 0)))) (h "15gm5qhb5l7i2i9q9sg800fvkvpphfnlfv4z00py610mndn5d8aq")))

(define-public crate-minify-js-0.4.3 (c (n "minify-js") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.10.3") (d #t) (k 0)))) (h "1r1v46y4f9ia6qwgcb62m84bhrywkgf4352xmxfmr2qkl45zj063")))

(define-public crate-minify-js-0.5.0 (c (n "minify-js") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.13.1") (d #t) (k 0)))) (h "0j30b939zd44i4k4g4hv6binzm72p2gbq247i60rr22w5p3pbibg") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.1 (c (n "minify-js") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.13.1") (d #t) (k 0)))) (h "1qxrabgp6cqqxw1m0jnh78y8zdgf3n3kgnsk2mj5wnk3bar4s45j") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.2 (c (n "minify-js") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.15") (d #t) (k 0)))) (h "1b26fjdvcm33nv8vf3zxl61smxp95dgdaqs1by6p969l36symzs2") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.3 (c (n "minify-js") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.16") (d #t) (k 0)))) (h "09gvvq0rpvgw2yg5gg5543mh9606lg25xmnyfqpnv0wnh5lz7c7f") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.4 (c (n "minify-js") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.16") (d #t) (k 0)))) (h "1vj7qdqi1v4qa2jbhrj91klyvbnipfj7c02pzfgsp4sgjdzb2xfx") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.5 (c (n "minify-js") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.17") (d #t) (k 0)))) (h "02fd0lvnzw36l1w76v617whsvj7vpg9fjmn8b20h9i5bg90wlcmx") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.5.6 (c (n "minify-js") (v "0.5.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.17") (d #t) (k 0)))) (h "12zw73j8a72ydr6m7zxgqzi1pw7g5g5hj1mp2fydpg9am09cbmi2") (f (quote (("serialize" "parse-js/serialize"))))))

(define-public crate-minify-js-0.6.0 (c (n "minify-js") (v "0.6.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parse-js") (r "^0.20.1") (d #t) (k 0)))) (h "0bm9vlhn1dji43m0ck09b9ipc3i3wjmnql1y24j61mlbxr35bymi") (f (quote (("serialize" "parse-js/serialize"))))))

