(define-module (crates-io mi ni mini-forum) #:use-module (crates-io))

(define-public crate-mini-forum-0.2.0 (c (n "mini-forum") (v "0.2.0") (d (list (d (n "anansi") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "0x3pgsazddk66w2751hxbiclcl24zd71dn8k111ib0hibaj4f50s")))

(define-public crate-mini-forum-0.3.0 (c (n "mini-forum") (v "0.3.0") (d (list (d (n "anansi") (r "^0.3.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "10alx214755mqsjykdrmrcc57p5m8x9jyhf2k6xz3l8gji9j2qnb")))

(define-public crate-mini-forum-0.4.0 (c (n "mini-forum") (v "0.4.0") (d (list (d (n "anansi") (r "^0.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "19q46f8rb04304xdbvfizw885gzvkrvk590pyx51ij7y9yadx3mi")))

(define-public crate-mini-forum-0.4.1 (c (n "mini-forum") (v "0.4.1") (d (list (d (n "anansi") (r "^0.4.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "0xfv7b7kq1q0p10gxhjkqqrv0fkd22pz3gbm42b4gqz6cgbjd6pn")))

(define-public crate-mini-forum-0.5.0 (c (n "mini-forum") (v "0.5.0") (d (list (d (n "anansi") (r "^0.5.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "0i025z63dzm1m2r740c8p7q322ryn5a1mxnypvcccxg02z78654r")))

(define-public crate-mini-forum-0.6.0 (c (n "mini-forum") (v "0.6.0") (d (list (d (n "anansi") (r "^0.6.0") (f (quote ("sqlite" "redis"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0plm6diw3ckm6bfhi6j8a7d751cm0lk8h4nmdrbp33a8mlh670zd")))

