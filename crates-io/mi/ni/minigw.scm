(define-module (crates-io mi ni minigw) #:use-module (crates-io))

(define-public crate-minigw-0.0.1 (c (n "minigw") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "07vy6sp7dlz0lnmf06rixak87hwwri3zpl1fmrpwydaax3cp6r59")))

(define-public crate-minigw-0.0.2 (c (n "minigw") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "0rz28fcx4xy454i5n8c76wyzy5xycnnaqglx2hdsq7b4zygxb0qb")))

(define-public crate-minigw-0.0.3 (c (n "minigw") (v "0.0.3") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "04d3rr1rhbr2a4vvmifxa0g6w5n826qi0sd5g00i4rp2hi2m4nji")))

(define-public crate-minigw-0.0.4 (c (n "minigw") (v "0.0.4") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "116kd87kq73szvkl9vrv3d323dpvppdhssnxi0zgia8nmyjfyczv") (y #t)))

(define-public crate-minigw-0.0.5 (c (n "minigw") (v "0.0.5") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "14h6s8m38gfzfnpjilp17a0lyp22w25ygkb0cv3xhmxv5z3qw39w")))

(define-public crate-minigw-0.0.6 (c (n "minigw") (v "0.0.6") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "0v9zzkqq4w3vq1nfs20fh7rqrh3j6x5975hi7vq9pxslnrlz5maz")))

