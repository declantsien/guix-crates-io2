(define-module (crates-io mi ni mini-v8) #:use-module (crates-io))

(define-public crate-mini-v8-0.1.0 (c (n "mini-v8") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)))) (h "0krks2ngpvy2pp6vs1slg10x8gpbqxfifv54jkamw3f0i2sndi18")))

(define-public crate-mini-v8-0.2.0 (c (n "mini-v8") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)))) (h "1y1lmlfbaih9mvw0kk8g1ndhfydxcsk78rbnc6baa5f7sc0s2szj")))

(define-public crate-mini-v8-0.2.1 (c (n "mini-v8") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)))) (h "10wqhwa9hkm203qgfyms93ick0727ky2f13nykmz7qvsyj52n4cl")))

(define-public crate-mini-v8-0.3.0 (c (n "mini-v8") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)))) (h "0sxqvddj4fp0kd50wb8jdi6vh51yv44ka59b6r99ahwqx5a3r8c3")))

(define-public crate-mini-v8-0.4.0 (c (n "mini-v8") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)) (d (n "v8") (r "^0.61.0") (d #t) (k 0)))) (h "1n0h5scdcr3zscr5d2cnsx9x23jmpxgjxl7nmv1ry6m0h9n1nwba")))

(define-public crate-mini-v8-0.4.1 (c (n "mini-v8") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "rustyline") (r "^5.0") (d #t) (k 2)) (d (n "v8") (r "^0.75.1") (d #t) (k 0)))) (h "1qh85l1smznnwj4w0f2hn78rqf8kxmla6ppjlnnxkkr1d4llyhhr")))

