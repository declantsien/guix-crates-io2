(define-module (crates-io mi ni mini-config) #:use-module (crates-io))

(define-public crate-mini-config-0.1.0 (c (n "mini-config") (v "0.1.0") (d (list (d (n "mini-config-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "15prc2g00106gdk8flksf2im54cwnl7gf4q6d061f9ky5p1y60bz") (f (quote (("derive" "mini-config-derive") ("default"))))))

(define-public crate-mini-config-0.1.1 (c (n "mini-config") (v "0.1.1") (d (list (d (n "mini-config-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1hcnph1nfaj907yx2h27pw7yq7rf6mqjar3kpq5mxb8ijlannhrf") (f (quote (("derive" "mini-config-derive") ("default"))))))

(define-public crate-mini-config-0.1.2 (c (n "mini-config") (v "0.1.2") (d (list (d (n "mini-config-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1ka1m44gk0cgqrikmd9g8i74lx2dwxqnhwazgcjj23rk8wzqg3c4") (f (quote (("derive" "mini-config-derive") ("default"))))))

(define-public crate-mini-config-0.1.3 (c (n "mini-config") (v "0.1.3") (d (list (d (n "mini-config-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "03agpmgar53lklbbvkcfdsb2vkq1zf12w1bqlc29d4srk0m0a8rr") (f (quote (("derive" "mini-config-derive") ("default"))))))

