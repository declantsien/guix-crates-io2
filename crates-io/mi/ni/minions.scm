(define-module (crates-io mi ni minions) #:use-module (crates-io))

(define-public crate-minions-0.0.1 (c (n "minions") (v "0.0.1") (d (list (d (n "lossyq") (r "^0.1.5") (d #t) (k 0)))) (h "11n569fv66dfww92k23f20bvq4pmp9b6wp6ixcb6x9n5m5rk35nn")))

(define-public crate-minions-0.1.2 (c (n "minions") (v "0.1.2") (d (list (d (n "lossyq") (r "^0.1.6") (d #t) (k 0)))) (h "1d7i944s22zzxb92r181yw6nsbq3qjbyn689q0fa5byyh8dh0sp7")))

(define-public crate-minions-0.1.3 (c (n "minions") (v "0.1.3") (d (list (d (n "lossyq") (r "^0.1.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0wz2sxfgc4lci70hmbxrrc2qi0yby6n40cwwwm51krs2v14970a8")))

(define-public crate-minions-0.1.4 (c (n "minions") (v "0.1.4") (d (list (d (n "lossyq") (r "^0.1.9") (d #t) (k 0)))) (h "0rkr01x83vda8bg8qsfgznycb09yvs7hljs0sm6f0lcnx2lzmlca")))

(define-public crate-minions-0.1.5 (c (n "minions") (v "0.1.5") (d (list (d (n "lossyq") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "0j45gb386zzkvjhy43vakxbz55cynyzf2h84cfw8qdnva7h002qg")))

(define-public crate-minions-0.1.6 (c (n "minions") (v "0.1.6") (d (list (d (n "lossyq") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "0p540imwh2bwni1qdc4dq39vlkwxfw8vls8lakqmj1j3fnlqc6k2")))

(define-public crate-minions-0.1.7 (c (n "minions") (v "0.1.7") (d (list (d (n "lossyq") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1vmlwy5y60g4ap7w95b0c0ixq82s9fdjrcj6flvk23diai8r90ng")))

(define-public crate-minions-0.1.8 (c (n "minions") (v "0.1.8") (d (list (d (n "lossyq") (r "^0.1.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0zgdqjjsmf51rpiyv8pwkgajh0yxvn03vjmzlqj90ha6ld8sczb9")))

(define-public crate-minions-0.1.9 (c (n "minions") (v "0.1.9") (d (list (d (n "lossyq") (r "^0.1.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mz6czp14nylq85rwg48hgyynvn8zpkcsrcywk1hylwkwm9kk6rx")))

(define-public crate-minions-0.1.10 (c (n "minions") (v "0.1.10") (d (list (d (n "lossyq") (r "^0.1.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zr3wdz9cmbbacpll4qrs4v83pd5d2cd7ks3ccsv9kawlsn8w1rj")))

(define-public crate-minions-0.1.11 (c (n "minions") (v "0.1.11") (d (list (d (n "lossyq") (r "^0.1.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1cj7ykvss4fvncgvgb085h3fxhvaf7fw3cr116w7ghb611mdwrpp")))

(define-public crate-minions-0.1.12 (c (n "minions") (v "0.1.12") (d (list (d (n "lossyq") (r "^0.1.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0aczj0nfmbkr2fpcd0738c6s6h1qifzrhlw7spv9axd8clarkpzh")))

(define-public crate-minions-0.2.1 (c (n "minions") (v "0.2.1") (d (list (d (n "lossyq") (r "^0.1.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1x6qxhnwl2lkbgr7ba73dbgvfvkr216lp54cvi11f4y61yi8qj7m")))

(define-public crate-minions-0.2.2 (c (n "minions") (v "0.2.2") (d (list (d (n "lossyq") (r "^0.1.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0g7fqdccm6z6dkf39ybdvyhhs92g1gwlbg0w19lqpayjgbc8cv25")))

(define-public crate-minions-0.2.4 (c (n "minions") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0z0vi08p0gzzky8fl1qrhikvc6h2ciigmbb1k2qa4r8vdyaxw2vh")))

(define-public crate-minions-0.2.5 (c (n "minions") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16nq1v4rqba66jsh28czxhqf986r54kxwbp7c4hmlf355y63sx7i")))

(define-public crate-minions-0.2.6 (c (n "minions") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "136z6cld1jhpwg0wr3s5696zzad45qihl01xcj6cimjabvlc2ghk")))

(define-public crate-minions-0.2.7 (c (n "minions") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vxk4ma7p2x56inns9bjm7l7w11pcvmyd0ri0fnrig79hhgvyf4v")))

(define-public crate-minions-0.2.8 (c (n "minions") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l0c1w94ya783g87mxzfp3y9wj5spsxvg6whisrya8iiyh3hxbxh")))

(define-public crate-minions-0.2.9 (c (n "minions") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zimqijn5hsmhm1wa0hmxsw93n4wj7gy1y5dfxkxc8qhpwa3jwb7")))

