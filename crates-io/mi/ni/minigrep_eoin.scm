(define-module (crates-io mi ni minigrep_eoin) #:use-module (crates-io))

(define-public crate-minigrep_eoin-0.1.0 (c (n "minigrep_eoin") (v "0.1.0") (h "09lz73g1w5s009q901bfkj5iwy09bilbd81p8rgf4a4j7ldwhnhs") (y #t)))

(define-public crate-minigrep_eoin-0.1.1 (c (n "minigrep_eoin") (v "0.1.1") (h "0vd4xzf8i8n9zvyqb79cx0rnaz225d9m2ipvxbvxzb33ahmwbkyk")))

