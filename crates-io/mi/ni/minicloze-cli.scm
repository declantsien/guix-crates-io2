(define-module (crates-io mi ni minicloze-cli) #:use-module (crates-io))

(define-public crate-minicloze-cli-1.4.0 (c (n "minicloze-cli") (v "1.4.0") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1yksx76zpq0ly4ch985l2zmq1i96pahmz96k6av3fgk4vvp13xpf")))

