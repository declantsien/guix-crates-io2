(define-module (crates-io mi ni mini-c-ares) #:use-module (crates-io))

(define-public crate-mini-c-ares-0.1.0 (c (n "mini-c-ares") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-ares-sys") (r "^0.1.0") (d #t) (k 0) (p "mini-c-ares-sys")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bdqnpix6qbvsbhhxcck965j1gifbxjjhfnbq394ms67wr6wcmly")))

(define-public crate-mini-c-ares-0.1.1 (c (n "mini-c-ares") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-ares-sys") (r "^0.1.0") (d #t) (k 0) (p "mini-c-ares-sys")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19sal3a8c1wczs3s6pncdihpzk8p06z7l7h9qwkv3c1rsj1bhbp2")))

(define-public crate-mini-c-ares-0.2.0 (c (n "mini-c-ares") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-ares-sys") (r "^0.2.0") (d #t) (k 0) (p "mini-c-ares-sys")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00q06rw3zyj6c1kgl2s3ddkc7wj2m1d3qm3xsplwzgkr4rs8spyl") (f (quote (("vendored" "c-ares-sys/vendored") ("default"))))))

(define-public crate-mini-c-ares-0.2.1 (c (n "mini-c-ares") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-ares-sys") (r "^0.2.1") (d #t) (k 0) (p "mini-c-ares-sys")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kv0z1y4wvz2yxh26qb3mrbz5lxh9ngkpm9khmvz9cbhjdb2b4zb") (f (quote (("vendored" "c-ares-sys/vendored") ("default"))))))

(define-public crate-mini-c-ares-0.2.2 (c (n "mini-c-ares") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "c-ares-sys") (r "^0.2.1") (d #t) (k 0) (p "mini-c-ares-sys")) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0898344m97qb0l8gi2n5g9ydnlzm7qb1rgv4qailxn0ywd8m5ck8") (f (quote (("vendored" "c-ares-sys/vendored") ("default"))))))

