(define-module (crates-io mi ni mini_markdown) #:use-module (crates-io))

(define-public crate-mini_markdown-0.1.0 (c (n "mini_markdown") (v "0.1.0") (h "1d4xscjxsyyca9q512frhxw5x4wbm6sjvvcqg16f8g3d2cwdcvxp")))

(define-public crate-mini_markdown-0.1.1 (c (n "mini_markdown") (v "0.1.1") (h "178150pv257n718gzs70n37d9mjpa53zjvsndn1wjm0dj6hhikdb")))

(define-public crate-mini_markdown-0.1.2 (c (n "mini_markdown") (v "0.1.2") (h "148msknrmd145g1ncfqyjx5c9433x0gv65irnjia2l5i1w94dmms")))

(define-public crate-mini_markdown-0.1.3 (c (n "mini_markdown") (v "0.1.3") (h "17mz93s0y9f9br1wznk51lgnyb2d05618lzq7s3082glzs5qp5ha")))

(define-public crate-mini_markdown-0.1.4 (c (n "mini_markdown") (v "0.1.4") (h "1f99xkvs1h4h5rvaq6fry88cizhank6296zjlxmmgs77dfh85ay9")))

(define-public crate-mini_markdown-0.1.5 (c (n "mini_markdown") (v "0.1.5") (h "0llh2yq9kc2q7c2npngq1yq21s8d4dssdmjqkqq1gqkhgycrypnb")))

(define-public crate-mini_markdown-0.1.6 (c (n "mini_markdown") (v "0.1.6") (h "04p4s29ixz6d5ja176frqr72mv1fam76n35v5w291v0sbgasw8dn")))

(define-public crate-mini_markdown-0.1.7 (c (n "mini_markdown") (v "0.1.7") (h "16r2ksy48vi7k158k9j7qy8g2jymf04rw14v0zdv5yi99n0j5san")))

(define-public crate-mini_markdown-0.1.8 (c (n "mini_markdown") (v "0.1.8") (h "0cb2cfd6xfqh1ylzylbxhcs4j38gn1cmfpz0c50pkrk93cwa5ckr")))

(define-public crate-mini_markdown-0.2.0 (c (n "mini_markdown") (v "0.2.0") (h "1kacd6za2gy0v2n9mp9g0k6gvmvhg8s71lnkqr9fg76x9yj84ji5")))

(define-public crate-mini_markdown-0.2.1 (c (n "mini_markdown") (v "0.2.1") (h "09igpcy3pxkbawk89jj1y47wki69bj0bas825v56aa6q1hr1nm04")))

(define-public crate-mini_markdown-0.2.2 (c (n "mini_markdown") (v "0.2.2") (h "180iwh9p0q5g1wjmqxzbf53gryz5wdsv9lkbcxbsbahw28j6hpkl")))

(define-public crate-mini_markdown-0.2.3 (c (n "mini_markdown") (v "0.2.3") (h "0r5qliwwxdmq6yd3z2647kgs7xa88zmnnx07ycx12ppza3pflmhk")))

(define-public crate-mini_markdown-0.2.4 (c (n "mini_markdown") (v "0.2.4") (h "0d5hpk0lyq3ybg2z4gjq5sjy4z6qnryqsxc7522klf909mbqvsxa")))

(define-public crate-mini_markdown-0.2.5 (c (n "mini_markdown") (v "0.2.5") (h "0vfb056c57ffj0626lhx4nw56q7h1mhr4gq5aahwscq73q8gbxwl")))

(define-public crate-mini_markdown-0.2.6 (c (n "mini_markdown") (v "0.2.6") (h "1z0f49285sk86sm2zhxvjsr1dmwj63wp3fv8487f8a5z1vvxz4kk")))

(define-public crate-mini_markdown-0.2.7 (c (n "mini_markdown") (v "0.2.7") (h "1m4gn4b3v7wipmna66fz1ii1dlnrxnhw74wq2srxwcn08ss2n9qp")))

(define-public crate-mini_markdown-0.2.8 (c (n "mini_markdown") (v "0.2.8") (h "0gbfxlij6mcd0f37msgqmhyddwhy3z391lhzmayvr1pdzjxcjdc3")))

(define-public crate-mini_markdown-0.2.9 (c (n "mini_markdown") (v "0.2.9") (h "1d0bm22axb8h4lp3nkar1kjyvymg7yb33vbbl21ys8yndx6jl95z")))

(define-public crate-mini_markdown-0.3.0 (c (n "mini_markdown") (v "0.3.0") (h "00ig1qq6mykzkr2x2q0k7d2v32bnzxwkn0p5lb5yhs6k0spza1lp")))

(define-public crate-mini_markdown-0.3.1 (c (n "mini_markdown") (v "0.3.1") (h "0wclhi2gfr777y7irzw8rwd0f5spvy55lr785prb4h89xxsgbskf")))

(define-public crate-mini_markdown-0.3.2 (c (n "mini_markdown") (v "0.3.2") (h "1xl3hq34zk2pqmcppzx264vj7sqaj6rnmy7iry548b5xldh9lyw5")))

(define-public crate-mini_markdown-0.3.3 (c (n "mini_markdown") (v "0.3.3") (h "1iqs3gkrmi1hci84x3nis317saw06ckhipdd9i8llwypabpvap4k")))

(define-public crate-mini_markdown-0.3.4 (c (n "mini_markdown") (v "0.3.4") (h "1sp6difd90zkvlfb5nmng64za2xq52j5fvc41jw6qx7ry300h1pq")))

(define-public crate-mini_markdown-0.3.5 (c (n "mini_markdown") (v "0.3.5") (h "0z2f8w48fxibfclmca0px91ygjcbjbvkfnrri7a9x44hcsndabks")))

