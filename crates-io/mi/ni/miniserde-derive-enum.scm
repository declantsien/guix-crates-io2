(define-module (crates-io mi ni miniserde-derive-enum) #:use-module (crates-io))

(define-public crate-miniserde-derive-enum-0.1.0 (c (n "miniserde-derive-enum") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nqzifpz8ifbc83ff3j7yw8wapw05473vr61l55lrpxz5hfnc3y3")))

(define-public crate-miniserde-derive-enum-0.1.1 (c (n "miniserde-derive-enum") (v "0.1.1") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12zg42yxphwzaamxlrw21ky6d4wyfc38wbn2ck7lws3wbyb6jkli")))

