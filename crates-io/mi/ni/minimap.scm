(define-module (crates-io mi ni minimap) #:use-module (crates-io))

(define-public crate-minimap-0.1.0 (c (n "minimap") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "syntect") (r "^3.2") (d #t) (k 0)))) (h "1lq1qpmrzpby6k66hc717xzfjm40q947q8h16nh1s1cf2r2jg5xn")))

