(define-module (crates-io mi ni mini-query-derive) #:use-module (crates-io))

(define-public crate-mini-query-derive-0.1.5 (c (n "mini-query-derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08w0ar88yl2acf8m5ahvr1qbzb8f3lhj2qd5mfp9m489spg2h316")))

