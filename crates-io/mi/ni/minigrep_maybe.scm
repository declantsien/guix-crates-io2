(define-module (crates-io mi ni minigrep_maybe) #:use-module (crates-io))

(define-public crate-minigrep_maybe-0.1.0 (c (n "minigrep_maybe") (v "0.1.0") (h "1lh4rnfrcwrd1ri88cci1i167qza85vpdhyp8zwqm2gqa5ja2k6n")))

(define-public crate-minigrep_maybe-0.1.1 (c (n "minigrep_maybe") (v "0.1.1") (h "06bvlw9x381p2jmjh1p6fj5nhxwjcw68g3alqjpyvj42flfgk4ly")))

(define-public crate-minigrep_maybe-0.1.2 (c (n "minigrep_maybe") (v "0.1.2") (h "10sy8rrh1f23fac2v0by9yndqg3vh31fpkl27zlfip36l66cxall")))

