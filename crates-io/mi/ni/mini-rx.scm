(define-module (crates-io mi ni mini-rx) #:use-module (crates-io))

(define-public crate-mini-rx-0.1.0 (c (n "mini-rx") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.10") (d #t) (k 2)))) (h "0yx1jrkv7bcj6bb3y6cby0p6cjaxrbf7jiqjgii4h7kfdqr23n5q")))

(define-public crate-mini-rx-0.1.1 (c (n "mini-rx") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.10") (d #t) (k 2)))) (h "1ynmb2ahfhgvq4lr4hcgpwah1b6ji5i821w83sw6chxsj82xsp8m")))

