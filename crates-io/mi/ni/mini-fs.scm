(define-module (crates-io mi ni mini-fs) #:use-module (crates-io))

(define-public crate-mini-fs-0.1.0 (c (n "mini-fs") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "1g6kvwzdjc3gzsw73hw77f7g02xbi9gy3nd0dr32k21ypax711ng") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.1.1 (c (n "mini-fs") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "0j7g361vpg22138vqrkd0iapljwcmvrcv7bi6j296sffic6v903s") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.1.2 (c (n "mini-fs") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "037myhgxml0r3p2wl9wi2m7dbj2by4z3bl2b2pnbk5zdhmsbqavn") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.1.3 (c (n "mini-fs") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "08nf8p439q58v243h7mnr1pwy6ad7ycfpbja2fy6krqcfjkws45j") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.2.0 (c (n "mini-fs") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "1dql81n6dzw2fm5da5yfk3cfg06z749y498s78m3jq9qqqan3j1q") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.2.1 (c (n "mini-fs") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "0a841yqkq60n69rc8j7s64k4h3h6bwzy9jp5wplpn93kp1w0sl9g") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

(define-public crate-mini-fs-0.2.2 (c (n "mini-fs") (v "0.2.2") (d (list (d (n "flate2") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "tar_") (r "^0.4.23") (o #t) (d #t) (k 0) (p "tar")) (d (n "zip_") (r "^0.5.2") (o #t) (d #t) (k 0) (p "zip")))) (h "0fkv1sgicv3szr6iqd42s742w190392354lamkrmhl42pv3nxyfy") (f (quote (("zip" "zip_") ("tar" "tar_" "flate2") ("default" "tar" "zip"))))))

