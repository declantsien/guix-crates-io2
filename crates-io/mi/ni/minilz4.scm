(define-module (crates-io mi ni minilz4) #:use-module (crates-io))

(define-public crate-minilz4-0.1.0 (c (n "minilz4") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19lgmipmx20g08h4dlg3s7sbfjcwzn6smyy5qcv7vlzhrms01y8w") (l "lz4")))

(define-public crate-minilz4-0.2.0 (c (n "minilz4") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ckblgcxf9ly72m4wiba9mf9lsx8pva12sj7pv9p6m0dhxdhmafp") (l "lz4")))

(define-public crate-minilz4-0.3.0 (c (n "minilz4") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1776i5zk3n7pcy1zs30h28qb0i3nzsadi47im647xpj8clrasqyg") (l "lz4")))

(define-public crate-minilz4-0.4.0 (c (n "minilz4") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vhgrycj8ikk1c48yczfsbqdj9glsha8lf2dhn7s7iklnahcdznj") (l "lz4")))

(define-public crate-minilz4-0.5.0 (c (n "minilz4") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w65ajbwpjwyis4hmb477lcr7i77cscmmf5hfsz7mxb8df1ks8aq") (l "lz4")))

(define-public crate-minilz4-0.6.0 (c (n "minilz4") (v "0.6.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03nmqd2w742zp9fy89hxxx8cd3yk78hvqqa9wxiiby42grq3mlrv") (l "lz4")))

(define-public crate-minilz4-0.6.1 (c (n "minilz4") (v "0.6.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p6zmbdm42pbiark9kz7nmkwjfywm48kjrd14574s4gmm6ignn9q") (l "lz4")))

