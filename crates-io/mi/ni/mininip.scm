(define-module (crates-io mi ni mininip) #:use-module (crates-io))

(define-public crate-mininip-1.1.0 (c (n "mininip") (v "1.1.0") (h "0ggk2czijh39vfa2dyfcsyxzvmsczx7n9xz417apc2syxg591ig3")))

(define-public crate-mininip-1.2.0 (c (n "mininip") (v "1.2.0") (h "0ha9q5wz7f915vm55amd8r0s63n67zmqym2n1i7xx1jxv2wwb5vi")))

(define-public crate-mininip-1.2.1 (c (n "mininip") (v "1.2.1") (h "09rrzpf04a5k9r0pm4s5cfh54fncraj68vvb1magi6cmmh7wr8mz")))

(define-public crate-mininip-1.2.2 (c (n "mininip") (v "1.2.2") (h "1jyz034r5a4sij2qhkalwbrli2v0rmy65vazfcrp5fg8g4x0b2j9")))

(define-public crate-mininip-1.2.3 (c (n "mininip") (v "1.2.3") (h "1sqzbyczhg42kf69ypx1nfsqpawzlh0afna6p7sxmszhhpdgzvn9")))

(define-public crate-mininip-1.3.0 (c (n "mininip") (v "1.3.0") (h "09agh7dika13ri5axwv5p5db431hn7pxgvxirazxw1vimaxkf7nq")))

(define-public crate-mininip-1.3.1 (c (n "mininip") (v "1.3.1") (h "1hcaig7cg6cd9fpv3gzpvfiv4n6rncc1bxgf0iiz58c89sgb4khh")))

