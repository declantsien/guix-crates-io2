(define-module (crates-io mi ni minigrep_npm) #:use-module (crates-io))

(define-public crate-minigrep_npm-1.0.0 (c (n "minigrep_npm") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1jm8z27n0cpwvgghx98vq1n660z59ash0hb3mp091aja0hvwp08l")))

(define-public crate-minigrep_npm-1.0.1 (c (n "minigrep_npm") (v "1.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1cm7pwj3bqzmdrxj7jnyyyn01kigyqk3mcbgccqhk8xlry89m6ik")))

