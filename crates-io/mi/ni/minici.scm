(define-module (crates-io mi ni minici) #:use-module (crates-io))

(define-public crate-minici-0.1.0 (c (n "minici") (v "0.1.0") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "03hzvmx4iiggxkgwapm73bsyp5ha9myfd5mzbzd8csk2q5wlaxgl")))

