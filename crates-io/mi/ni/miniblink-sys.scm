(define-module (crates-io mi ni miniblink-sys) #:use-module (crates-io))

(define-public crate-miniblink-sys-0.1.0 (c (n "miniblink-sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "192shcpcic8w5ssby6ikk7ghkr674f0kpzvhbdn5rhgifsfk6kwi")))

(define-public crate-miniblink-sys-0.1.1 (c (n "miniblink-sys") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1xy40l1pyr1b46jqm922ss1p1949cq9wp9nm845v3ivrplbkvhxh")))

(define-public crate-miniblink-sys-0.1.2 (c (n "miniblink-sys") (v "0.1.2") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1sc0g73q2n6dqbyxiiky3ryirfkkliz0yc2933q4i1pxvyim5s41")))

(define-public crate-miniblink-sys-0.2.0 (c (n "miniblink-sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r ">=0.54") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "0nnjwdjaz9hik4lfldx14ik1gvkp4mb5xx898f2cwcnkk73fafc5")))

(define-public crate-miniblink-sys-0.3.0 (c (n "miniblink-sys") (v "0.3.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "136dafnr8xxv0l96jiba51v44ajadg3h2qy6n2iv2vlgl2yj5ypj")))

