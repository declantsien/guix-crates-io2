(define-module (crates-io mi ni minicount) #:use-module (crates-io))

(define-public crate-minicount-0.1.0 (c (n "minicount") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0p429n7dwlhx5ichbhmdj5792p5386l69j1k5k8ih2r0sj4nj60v")))

(define-public crate-minicount-0.2.0 (c (n "minicount") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0wvd3x38s3xigrqndd10q9mjxqw6y7kmwd9n0k0z7v4rn33snswq")))

(define-public crate-minicount-0.2.1 (c (n "minicount") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0mk0yhasl46xw9dl6kz1v744x0b1n8a92w810v4a5rq10zbdscfr")))

(define-public crate-minicount-0.2.2 (c (n "minicount") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "03khcnhlc977q8rirbsbddra114m9xxhvcw3qgvdxzkqz3p7abq7")))

(define-public crate-minicount-0.2.3 (c (n "minicount") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yml") (r "^0.0.6") (d #t) (k 0)))) (h "1nsb0psbfzqhq54n9g3v2ak5831mlv8y6k08qa620jm8rzz50dr5")))

