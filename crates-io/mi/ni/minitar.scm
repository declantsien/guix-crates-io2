(define-module (crates-io mi ni minitar) #:use-module (crates-io))

(define-public crate-minitar-0.1.0 (c (n "minitar") (v "0.1.0") (h "1w33rqll2yzxapn7lwss4c8p9h03hrm55pmxff4a1v1b9h2lm8m1")))

(define-public crate-minitar-0.2.0 (c (n "minitar") (v "0.2.0") (d (list (d (n "deku") (r "^0.13") (d #t) (k 0)))) (h "0mdkhj6k34f6x83cx6m2snhv9n329pp7ir5n6bzkqryw1lfrf7hk")))

(define-public crate-minitar-0.2.1 (c (n "minitar") (v "0.2.1") (d (list (d (n "deku") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v0j0x5nsdwspv6bcr8hc3j1i99g9a7l6jgi0gp41rysj23a2igi")))

