(define-module (crates-io mi ni minicloze-lib) #:use-module (crates-io))

(define-public crate-minicloze-lib-0.1.0 (c (n "minicloze-lib") (v "0.1.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qpih1l99kqzq7xpb3qq50mf2z0glrqncmff06asfvfy9aar37vc")))

(define-public crate-minicloze-lib-0.2.0 (c (n "minicloze-lib") (v "0.2.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w0ls1v2kx0v98jz8v6a7l71rggc7bc42nng92w5zlapwh6b0daz")))

(define-public crate-minicloze-lib-0.3.0 (c (n "minicloze-lib") (v "0.3.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kkwy69k4bb4lw9xmgmfihin4823m7n41s0nv10gqv51vymkxjai")))

(define-public crate-minicloze-lib-0.4.0 (c (n "minicloze-lib") (v "0.4.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01bz4l9zdi739ksyjqnigjgnw37dmd4lcqz3sn3qc6w6rm6m2dvv")))

(define-public crate-minicloze-lib-0.4.1 (c (n "minicloze-lib") (v "0.4.1") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bd0si5d98bjy9hkd9li518pvri1qycyss0h8kwmnbb9bwcgpfzw")))

