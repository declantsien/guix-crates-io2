(define-module (crates-io mi ni minigrep_ag) #:use-module (crates-io))

(define-public crate-minigrep_ag-0.1.0 (c (n "minigrep_ag") (v "0.1.0") (h "0j4hjn26gbyvw7mg2z0haspi28s6gzn3kkkybhpgrdfkm5z7fvi1")))

(define-public crate-minigrep_ag-0.1.1 (c (n "minigrep_ag") (v "0.1.1") (h "14iwigqmrz24kf2vcik5bfxp2irvybqiaxajywz3dlgz8ppvfcqv")))

(define-public crate-minigrep_ag-0.1.2 (c (n "minigrep_ag") (v "0.1.2") (h "06cwpi4rivjizdp799gpyf82adl66m2f0gvybzh8n4ndsnxrqy0f")))

