(define-module (crates-io mi ni miniscript-compiler) #:use-module (crates-io))

(define-public crate-miniscript-compiler-0.1.0 (c (n "miniscript-compiler") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.31.0") (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "16ck0h23aqznm92bs60ghjzlvx66hq8b2cmp305vsk5hdvd6ggzb")))

(define-public crate-miniscript-compiler-0.1.1 (c (n "miniscript-compiler") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.31.0") (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "14kn2wdviam8kby9cj1raqid4wf38z2jpr1d81pycqa62fijwkdw")))

