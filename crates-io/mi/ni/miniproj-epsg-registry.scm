(define-module (crates-io mi ni miniproj-epsg-registry) #:use-module (crates-io))

(define-public crate-miniproj-epsg-registry-0.1.0 (c (n "miniproj-epsg-registry") (v "0.1.0") (d (list (d (n "miniproj-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "14p31sd24qghbbd0yyix0ij2qmn10fipsq773khbh1nl1da63vaa")))

(define-public crate-miniproj-epsg-registry-0.1.1 (c (n "miniproj-epsg-registry") (v "0.1.1") (d (list (d (n "miniproj-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1zf31sn5va070x4dmyw6x5svg5as2xx3261j1axj14q3ls33cl3v")))

(define-public crate-miniproj-epsg-registry-0.2.0 (c (n "miniproj-epsg-registry") (v "0.2.0") (d (list (d (n "miniproj-ops") (r "^0.2.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1hqkpzd1ri5l2i7n72s67ar4fiq9b222xafc78mx4iwvi1jhy8k6")))

(define-public crate-miniproj-epsg-registry-0.3.0 (c (n "miniproj-epsg-registry") (v "0.3.0") (d (list (d (n "miniproj-ops") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0b7wyzx7ajgx3n05dxs6ypqlnl592qpjyaspl6xi1dj1whahkbiy")))

(define-public crate-miniproj-epsg-registry-0.4.0 (c (n "miniproj-epsg-registry") (v "0.4.0") (d (list (d (n "miniproj-ops") (r "^0.4.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "10p158n7gyjl3crbgxw0884pp4z4vjrmzd4pp3khsi4mmfy4cwxb")))

(define-public crate-miniproj-epsg-registry-0.5.0 (c (n "miniproj-epsg-registry") (v "0.5.0") (d (list (d (n "miniproj-ops") (r "^0.5.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "19bqylnyqymip1rfzq5xfb5drx200yix8rma7rjs47wl4rj039jv")))

(define-public crate-miniproj-epsg-registry-0.6.0 (c (n "miniproj-epsg-registry") (v "0.6.0") (d (list (d (n "miniproj-ops") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "17laklrs5gjk4klwkxgz4r6l93vzj5c6yh25d83wv60qb45ncn29")))

(define-public crate-miniproj-epsg-registry-0.7.0 (c (n "miniproj-epsg-registry") (v "0.7.0") (d (list (d (n "miniproj-ops") (r "^0.7.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1f9hjknfkkyxyx11gfkhr8lhsqxp85h5naz2v1hm8aaqci51c3hg")))

(define-public crate-miniproj-epsg-registry-0.8.0 (c (n "miniproj-epsg-registry") (v "0.8.0") (d (list (d (n "miniproj-ops") (r "^0.8.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0x57w2hbkszg1wwamkhgby0634yvxc0pdknnrd2azy5awkw8vswa")))

(define-public crate-miniproj-epsg-registry-0.9.0 (c (n "miniproj-epsg-registry") (v "0.9.0") (d (list (d (n "miniproj-ops") (r "^0.9.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.33.0") (d #t) (k 0)))) (h "0i1ind6n0jf8b8p0viamn2b5bfzar9vsx9k34n2s1ayshpq2ir3g")))

(define-public crate-miniproj-epsg-registry-0.10.0 (c (n "miniproj-epsg-registry") (v "0.10.0") (d (list (d (n "miniproj-ops") (r "^0.10.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.33.0") (d #t) (k 0)))) (h "0qw9pk2d1nbvkx70j3isnkdgz9hw8myrgx2mg8xj5j8jpsq7qq5s")))

(define-public crate-miniproj-epsg-registry-0.10.1 (c (n "miniproj-epsg-registry") (v "0.10.1") (d (list (d (n "miniproj-ops") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)) (d (n "phf_codegen") (r "~0.11.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.33.0") (d #t) (k 0)))) (h "0gykhgpn16kk5dn54fasjqysp8gd5bp5kbggr3ssj3jqam4yn4qi")))

(define-public crate-miniproj-epsg-registry-0.10.2 (c (n "miniproj-epsg-registry") (v "0.10.2") (d (list (d (n "miniproj-ops") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "~0.11.2") (k 0)) (d (n "phf_codegen") (r "~0.11.2") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)))) (h "03fw5g5yh1ks0pkkyzsz7ggc2ml39gc5jrwjfmfknjcg8wld1zzq")))

