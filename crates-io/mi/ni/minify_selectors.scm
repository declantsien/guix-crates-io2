(define-module (crates-io mi ni minify_selectors) #:use-module (crates-io))

(define-public crate-minify_selectors-0.11.3 (c (n "minify_selectors") (v "0.11.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.3.3") (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "parse_selectors") (r "^0.9.2") (d #t) (k 0)))) (h "0gsnm4kg5agvsgn3m3x66gxcscmxfaw1kq2i3jlqyxxxhwsyp8ld")))

(define-public crate-minify_selectors-1.1.0 (c (n "minify_selectors") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "encode_selector") (r "^0.3.3") (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "minify_selectors_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "parse_selectors") (r "^1.0.0") (d #t) (k 0)))) (h "1px96vfn1gp0w8s4grbjy0iba28r4hx3mjym51z5a0wk330rpg49")))

(define-public crate-minify_selectors-2.1.2 (c (n "minify_selectors") (v "2.1.2") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "minify_selectors_utils") (r "^2.1.3") (d #t) (k 0)) (d (n "parse_selectors") (r "^2.0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0hm8rg31pv55ggs36daj1nlwfdv26h684idxik55q3gpwxv511fi")))

(define-public crate-minify_selectors-2.3.3 (c (n "minify_selectors") (v "2.3.3") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "minify_selectors_utils") (r "^2.3.3") (d #t) (k 0)) (d (n "parse_selectors") (r "^2.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "106p444k5dk1hkx4a86j5lprgwz5d0i3krnpfvrnyvfpzy133psc")))

(define-public crate-minify_selectors-2.4.0 (c (n "minify_selectors") (v "2.4.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "minify_selectors_utils") (r "^2.4.0") (d #t) (k 0)) (d (n "parse_selectors") (r "^2.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0fnyfia2fs65l2wj96n5fn6fq1jbimah7rbq6g9cczpwwblapz3m")))

(define-public crate-minify_selectors-2.6.0 (c (n "minify_selectors") (v "2.6.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "minify_selectors_utils") (r "^2.6.0") (d #t) (k 0)) (d (n "parse_selectors") (r "^2.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13213162jg4gilag2kp360hfsfrwzjhx8wirm6bv14095vr9qb2r")))

