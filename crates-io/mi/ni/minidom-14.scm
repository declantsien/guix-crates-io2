(define-module (crates-io mi ni minidom-14) #:use-module (crates-io))

(define-public crate-minidom-14-0.14.0 (c (n "minidom-14") (v "0.14.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1y9xahs0gvn4slk325szbasz8bv4v0nl9aqyiwishn0v9n5rxhzp")))

(define-public crate-minidom-14-0.15.0 (c (n "minidom-14") (v "0.15.0") (d (list (d (n "quick-xml") (r "^0.27") (d #t) (k 0)))) (h "1fbjg8z7aa1gz02d47ib6dm668qscdimsahriaa3ll3z9sf1i33n")))

(define-public crate-minidom-14-0.16.0 (c (n "minidom-14") (v "0.16.0") (d (list (d (n "quick-xml") (r "^0.28") (d #t) (k 0)))) (h "18139k5zhydabb0f25c9hk4q6353jbd4vs2q1j28jrn1ff6s0rvf")))

