(define-module (crates-io mi ni minikalman) #:use-module (crates-io))

(define-public crate-minikalman-0.0.1 (c (n "minikalman") (v "0.0.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "micromath") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "stdint") (r "^0.2.0") (k 0)))) (h "0ibb9lnxq03kkafndj78wghslabza5qm1hckz46y8bci1sms2nhd") (f (quote (("std" "stdint/std") ("no_std" "micromath") ("default" "no_std")))) (y #t)))

(define-public crate-minikalman-0.0.2 (c (n "minikalman") (v "0.0.2") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "micromath") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "stdint") (r "^0.2.0") (k 0)))) (h "13prynmlnyf7xl8xya478pf273myzw3akgfr72g8dlp467sa1zvw") (f (quote (("std" "stdint/std") ("no_std" "micromath") ("default" "no_std"))))))

