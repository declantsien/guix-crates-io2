(define-module (crates-io mi ni minimal-id) #:use-module (crates-io))

(define-public crate-minimal-id-0.1.0 (c (n "minimal-id") (v "0.1.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)))) (h "09zpjfpqz51dx4k3jx1ps3wr8ksqhi3x45smqdi5har8pc2vqfgp")))

(define-public crate-minimal-id-0.2.0 (c (n "minimal-id") (v "0.2.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "1q73cm16n4683gir77bs968d9pn4ni9vwx0a95vk9bwhp2g2b84n")))

(define-public crate-minimal-id-0.2.1 (c (n "minimal-id") (v "0.2.1") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "1v2zzq5wxxc245jf0msk7wdpq19ydwd8isrd9abg20mxq0118vi4")))

(define-public crate-minimal-id-0.2.2-alpha.0 (c (n "minimal-id") (v "0.2.2-alpha.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "0w2ngzlcmjnwzw5q8ihwi4r7z87g10b1l4y116pfwwn75lmhznwx")))

(define-public crate-minimal-id-0.3.0 (c (n "minimal-id") (v "0.3.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "0hjizk7wk8xf4x854jv54kcarfq5h7zix0wzb5g0747dgsfd7vpz")))

(define-public crate-minimal-id-0.3.1 (c (n "minimal-id") (v "0.3.1") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "1hzaw0b24k39llp4b3mbknl50v3bxlg4nzblgji8s57qc3baklm0")))

(define-public crate-minimal-id-0.4.0 (c (n "minimal-id") (v "0.4.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "juniper") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)))) (h "093aszncb4q2a0mv65kgh37rfnsr8y1fc2acysn7vfsx0x71qlf0") (f (quote (("graphql" "juniper") ("default"))))))

(define-public crate-minimal-id-0.5.0 (c (n "minimal-id") (v "0.5.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "juniper") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)) (d (n "serde") (r "~1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1") (o #t) (d #t) (k 0)))) (h "0hibcgdn5gcbk4k5pfalzw24jgwzjkxzk38bbj5v3dq8pass5pjy") (f (quote (("json" "serde" "serde_json") ("graphql" "juniper") ("default"))))))

(define-public crate-minimal-id-0.6.0 (c (n "minimal-id") (v "0.6.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "juniper") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)) (d (n "serde") (r "~1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1") (o #t) (d #t) (k 0)))) (h "0b033w63li298dxjjr0362rdcrlp16rdp2j8fcdxji50fb3bzr25") (f (quote (("json" "serde" "serde_json") ("graphql" "juniper") ("default"))))))

(define-public crate-minimal-id-0.7.0 (c (n "minimal-id") (v "0.7.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "juniper") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)) (d (n "serde") (r "~1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1") (o #t) (d #t) (k 0)))) (h "0m4q0dakpg1kvbplyix2nqgvwnar19c1jvwpqj48hscg8r27lv5x") (f (quote (("json" "serde" "serde_json") ("graphql" "juniper") ("default"))))))

(define-public crate-minimal-id-0.8.0 (c (n "minimal-id") (v "0.8.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "data-encoding") (r "~2") (d #t) (k 0)) (d (n "juniper") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0") (d #t) (k 0)) (d (n "rusty-hook") (r "~0") (d #t) (k 2)) (d (n "serde") (r "~1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1") (o #t) (d #t) (k 0)))) (h "1bcykg78m0i061fd8x2dzyvcw39d4f4f433k9zdx9y5l34bvldzc") (f (quote (("json" "serde" "serde_json") ("graphql" "juniper") ("default"))))))

