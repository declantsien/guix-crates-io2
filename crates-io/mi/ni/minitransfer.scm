(define-module (crates-io mi ni minitransfer) #:use-module (crates-io))

(define-public crate-minitransfer-0.1.0 (c (n "minitransfer") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "minipac") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ly9vfny2nlg4fwp380w29wqgglp4zvbaq1jh1ixmy14v4sgyjk2")))

(define-public crate-minitransfer-0.2.1 (c (n "minitransfer") (v "0.2.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minipac") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17lvd53bcasqcjpsc7vfflm15byv6wlkq2qfnl28g1b0ifasfk0g")))

