(define-module (crates-io mi ni minimal-yaml) #:use-module (crates-io))

(define-public crate-minimal-yaml-0.1.0 (c (n "minimal-yaml") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "0k7dc1f88bcd2bknikxfyais1509cfrwccfgdx0x684d8zw6gh5s")))

(define-public crate-minimal-yaml-0.1.1 (c (n "minimal-yaml") (v "0.1.1") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "0sa1bnyssczlqnm1wxiixjljlskv4bdxcsl1cw114h31bgdlzyji")))

(define-public crate-minimal-yaml-0.1.2 (c (n "minimal-yaml") (v "0.1.2") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "1isfvsh7w0plyvdhgc1g3s27f5fw2714bf7lzcpp3qx8zmb5f011")))

(define-public crate-minimal-yaml-0.1.3 (c (n "minimal-yaml") (v "0.1.3") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qixlga0n0xzns7nciaajklj6vpmz961jbhkirykvk30lrgmh5mj")))

(define-public crate-minimal-yaml-0.1.4 (c (n "minimal-yaml") (v "0.1.4") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vprshzbgk2bgkzkknkcvqzppq382alpxgrb8c77y3gp1gw9an99")))

(define-public crate-minimal-yaml-0.1.5 (c (n "minimal-yaml") (v "0.1.5") (d (list (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)))) (h "1scp50a67p5za9naa9jc574hnaw1x9nal5qg25kngqhfs80c9m6c")))

