(define-module (crates-io mi ni minipre) #:use-module (crates-io))

(define-public crate-minipre-0.1.0 (c (n "minipre") (v "0.1.0") (h "1qimp23wacgis7dd3igwk90gi1cdjbafdi8c39i9xrp9pzqxwv4v")))

(define-public crate-minipre-0.2.0 (c (n "minipre") (v "0.2.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0f85sarnnlx9qkbwdn1spslx1jkwyqm5z37p20lb7myzb6krci6m")))

