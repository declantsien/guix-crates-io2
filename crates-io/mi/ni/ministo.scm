(define-module (crates-io mi ni ministo) #:use-module (crates-io))

(define-public crate-ministo-0.1.0 (c (n "ministo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rust-randomx") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "watch") (r "^0.2.3") (d #t) (k 0)))) (h "0bk8kidm7hqqg893rqasqbarlvm0dppx3k6c9ma62wmapf9slxvy")))

