(define-module (crates-io mi ni minihttpse) #:use-module (crates-io))

(define-public crate-minihttpse-0.1.0 (c (n "minihttpse") (v "0.1.0") (h "1h0bymcsirizb3qr7j0xvd06ra8vwrawadfg5w6m3wra0k2kp44h")))

(define-public crate-minihttpse-0.1.1 (c (n "minihttpse") (v "0.1.1") (h "1nlghnp0mgxqglxz0qxygqqzysrgmiqzgixh97lr5g5swg8s5h6z")))

(define-public crate-minihttpse-0.1.2 (c (n "minihttpse") (v "0.1.2") (h "1ynw5p55ipclj513v1z27dd58dndxnkpmcf709ai5prxh15r8z70")))

(define-public crate-minihttpse-0.1.3 (c (n "minihttpse") (v "0.1.3") (h "1n2w54dbx53m5fhwks6h7ipf3072g79psa6wcwr6ri95yxpy0dnz")))

(define-public crate-minihttpse-0.1.4 (c (n "minihttpse") (v "0.1.4") (h "0rzd0v6dv0q9s19vmvbr49b854g20h4rp7zzzzpsc8ymv3w52ad0")))

(define-public crate-minihttpse-0.1.5 (c (n "minihttpse") (v "0.1.5") (h "09lmyijp3k3kaziac8lg21g4y8srha7d9yd2bm5scaab16w1wl8j")))

(define-public crate-minihttpse-0.1.6 (c (n "minihttpse") (v "0.1.6") (h "0mdzgv895m1pp9pips4lmd6x2fgqlmp0v4srqy733d1nwk7fhl4f")))

