(define-module (crates-io mi ni miniring_nightly) #:use-module (crates-io))

(define-public crate-miniring_nightly-1.0.0+58ec7c1 (c (n "miniring_nightly") (v "1.0.0+58ec7c1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1ywh6slk5h7d9l7aqz639zs40v4688g1y94xznq9xmmx7995sw9p") (y #t)))

(define-public crate-miniring_nightly-1.0.1+58ec7c1 (c (n "miniring_nightly") (v "1.0.1+58ec7c1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "08vyvw473zhjhh2l468wx0wv7f457qq3ad45lpnjysr3fq0yswp9") (y #t)))

(define-public crate-miniring_nightly-1.0.2+58ec7c1 (c (n "miniring_nightly") (v "1.0.2+58ec7c1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1cb431fkll3m22mg8m9kx9mjpksn43jhbvwl15ccax299nwk8fc5") (y #t)))

(define-public crate-miniring_nightly-1.0.3+58ec7c1 (c (n "miniring_nightly") (v "1.0.3+58ec7c1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "12rlwc422qif7jc2g4bgawzqlkqw0z9r37j65cyf2gpwphgkn0x1") (y #t)))

