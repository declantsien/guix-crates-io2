(define-module (crates-io mi ni minigrepsada) #:use-module (crates-io))

(define-public crate-minigrepsada-0.1.0 (c (n "minigrepsada") (v "0.1.0") (h "0xjwpli0yw5hy4mg6syg6706dvwpxc41p4gfifqfh7541nm0i7pz")))

(define-public crate-minigrepsada-0.1.2 (c (n "minigrepsada") (v "0.1.2") (h "0cyppdfmzaj37kac8wcvrx58gknancn5ynp2is8vprds75l1cnb7")))

(define-public crate-minigrepsada-0.1.3 (c (n "minigrepsada") (v "0.1.3") (h "1dkmk9fi4syrs7ffkvdilkql5c998xqqwvjmdpbsdizi8cmkvja5") (y #t)))

