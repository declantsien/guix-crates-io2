(define-module (crates-io mi ni minigrep_boypaper) #:use-module (crates-io))

(define-public crate-minigrep_boypaper-0.1.1 (c (n "minigrep_boypaper") (v "0.1.1") (h "1nhfag8wn1fjjk781jgwry6ax9v0ri7g27i91ija0yvzlcask5i7")))

(define-public crate-minigrep_boypaper-0.1.2 (c (n "minigrep_boypaper") (v "0.1.2") (h "09yrd4i0n396scb1fj069p18sbgy1k1z4s2yxii8i95f1fxwwn9h")))

