(define-module (crates-io mi ni miniblink) #:use-module (crates-io))

(define-public crate-miniblink-0.1.0 (c (n "miniblink") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "miniblink-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s0l7wva6jw7xv9j0wsb8jvrbgncn2kdrd6d1crkl79r2dr9llj4")))

(define-public crate-miniblink-0.1.1 (c (n "miniblink") (v "0.1.1") (d (list (d (n "miniblink-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vsj79lnyqmhqxlzqj6zvqrlm18qm84gzhjm337dm9wy2pnxb2w4")))

(define-public crate-miniblink-0.1.2 (c (n "miniblink") (v "0.1.2") (d (list (d (n "miniblink-sys") (r ">=0.1.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0bg6lmx3qg8hyfl2sklkssipmh1j3nfq3dwlxqx9hx3ilz2grvih")))

(define-public crate-miniblink-0.1.3 (c (n "miniblink") (v "0.1.3") (d (list (d (n "miniblink-sys") (r ">=0.1.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "1fjr05ly27ckbbpxvs6d1g0c6bckpdhpzvjrh8l1lybzpgyagbzk") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-miniblink-0.1.4 (c (n "miniblink") (v "0.1.4") (d (list (d (n "miniblink-sys") (r ">=0.1.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "11g15js94rqjfcqckgawjxaflm3mnnm46rdcf1ja8xkr7ng8nkm6") (f (quote (("default" "serde" "rwh_06")))) (s 2) (e (quote (("serde" "dep:serde") ("rwh_06" "dep:raw-window-handle"))))))

(define-public crate-miniblink-0.1.5 (c (n "miniblink") (v "0.1.5") (d (list (d (n "miniblink-sys") (r ">=0.1.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "05f5l16ga12bcvppi7p9ichwz082wrisirjpw19fvi7z1c4a8np5") (f (quote (("default" "serde" "rwh_06")))) (s 2) (e (quote (("serde" "dep:serde") ("rwh_06" "dep:raw-window-handle"))))))

(define-public crate-miniblink-0.1.6 (c (n "miniblink") (v "0.1.6") (d (list (d (n "miniblink-sys") (r ">=0.1.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "1ap5pwsrldwy6xmsgr0zq01qlv2av4isl3anfjkv48xlq3kz2wzs") (f (quote (("default" "serde" "rwh_06")))) (s 2) (e (quote (("serde" "dep:serde") ("rwh_06" "dep:raw-window-handle"))))))

(define-public crate-miniblink-0.2.0 (c (n "miniblink") (v "0.2.0") (d (list (d (n "miniblink-sys") (r "=0.2") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0l0xv1dggmbl4pscpnw0fmahzpar6r1mic9fh0d7n4p2gm5wadfc") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("rwh_06" "dep:raw-window-handle"))))))

(define-public crate-miniblink-0.3.0 (c (n "miniblink") (v "0.3.0") (d (list (d (n "miniblink-sys") (r "=0.3") (d #t) (k 0)))) (h "0kjs9alkqalrj0zazr4j8017h4p9ydswh5axz9azwzc2vannsw46")))

