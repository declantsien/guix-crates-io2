(define-module (crates-io mi ni miniarg_derive) #:use-module (crates-io))

(define-public crate-miniarg_derive-0.1.0 (c (n "miniarg_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11v1h557rw3zn2zm8pjqwmhh8184i0g5rar4iz1b7awb3qgamq3d")))

(define-public crate-miniarg_derive-0.2.0 (c (n "miniarg_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0z2nkwxw11hnvm9kavdddp0cb99fz22m52yla33xfihps6l79084")))

(define-public crate-miniarg_derive-0.3.0 (c (n "miniarg_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1sfh9qb3i18a6qgjy2z6qvqna0dr2gwjwfc43bcm2385r47lbwdz")))

(define-public crate-miniarg_derive-0.3.1 (c (n "miniarg_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0zcim0amibqbfx9n7hx524baqkc5pa9pls4427hlnmzyabz9w9b0")))

