(define-module (crates-io mi ni minilog) #:use-module (crates-io))

(define-public crate-minilog-0.1.0 (c (n "minilog") (v "0.1.0") (h "1zjxzx538574bnklpjc96qrf6ak2k12f3jw32165qy68qn9bkyv0") (f (quote (("warn") ("strip") ("info") ("error"))))))

