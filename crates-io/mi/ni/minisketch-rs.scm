(define-module (crates-io mi ni minisketch-rs) #:use-module (crates-io))

(define-public crate-minisketch-rs-0.1.0 (c (n "minisketch-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "019wjzimmki1s3c0129m975dgc562b7awb4z05hka8jkf8qjb8fw") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.1 (c (n "minisketch-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1vhzdcgwchgblw4x15rr1gpnvwhnw1hbhk4wangnyqmkgjbg3c34") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.2 (c (n "minisketch-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1ayzdcyjawxb40nz2bp2ygib6sm3d46f1zbgqrxp763r5ag66r44") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.3 (c (n "minisketch-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w6c9axi8zwy0d19klr0xj14h4bmpkpsfycarj56kz43pb1z7pbl") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.4 (c (n "minisketch-rs") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qsnvwp9cv2y2k4h2d88cfyxc35b1077anpr6ksk06qs8lx5dwd8") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.5 (c (n "minisketch-rs") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dbyagmxf486591vys88jwcl4d7ahkx1wv1i3m9xji1nbivk8717") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.6 (c (n "minisketch-rs") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cxry2zjlkk880gh41nj2956pchk5n2bmlvx8in9cld5pm3619m0") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.7 (c (n "minisketch-rs") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "176x1i7z9lq619rbmd2a68sx1iqh4d65md7jshqmrfg24cqgacnv") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.8 (c (n "minisketch-rs") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z48vpj0bmvdrhppldnma27xcxia7mnj3g20mhg27lnv55xbxaws") (l "minisketch")))

(define-public crate-minisketch-rs-0.1.9 (c (n "minisketch-rs") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ggzqh3m27sj9il4damq5irild10zwvg5gc8sl0cglxl4p6lpbg2") (l "minisketch")))

