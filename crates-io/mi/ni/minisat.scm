(define-module (crates-io mi ni minisat) #:use-module (crates-io))

(define-public crate-minisat-0.1.0 (c (n "minisat") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0pp5h7d2rxxz9gsqv1ckwp8xiyfdp4ss9izsymbqfbdvvifa9n9j")))

(define-public crate-minisat-0.1.1 (c (n "minisat") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1h96bnxmajcfwk16s2f3zpbazba2hp335nalk0m8919wpm685828")))

(define-public crate-minisat-0.2.0 (c (n "minisat") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1j2y3ig52s67j021fmj2dpkcz3v88ffzv56c1wd37d3hhpmvxyfr")))

(define-public crate-minisat-0.2.1 (c (n "minisat") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1lk10w5klswry0vim8hm1hs2gb1a6xr12q1k20q9q994ajw3p083")))

(define-public crate-minisat-0.3.0 (c (n "minisat") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1k7x1xy50hcwr233qjc8yhnmaqkrkn6804jiy8mc1c9iwgvv3kid")))

(define-public crate-minisat-0.3.1 (c (n "minisat") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1ahsmaxw5r792j48v1js2aa37vxp6prijhc6shhvdqc7k7446dzn")))

(define-public crate-minisat-0.3.2 (c (n "minisat") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "0ni0ld89p8mlyjqlsqly0s44cldb7s0lm7rkiad5fn7har9knjp1")))

(define-public crate-minisat-0.3.3 (c (n "minisat") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0vriy0fhc3qbl26x5zm7bi6gms2r13lw20bwjbx7x667zgbf4g1z")))

(define-public crate-minisat-0.3.4 (c (n "minisat") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0ybd2zhw5nyh8sgdg08mgg1zdgh1sf06xzjmjwg7m3cisp4p27bv")))

(define-public crate-minisat-0.4.0 (c (n "minisat") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "16fmmvc1b6wrgrhda8ybzr5mc9p2gplasrm7nljxr3h2bbkmwzkg")))

(define-public crate-minisat-0.4.1 (c (n "minisat") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1p1npkshq2ihq7knq4iywy7rvmcidf6l3h15xy1hk3dfgvyl5nvy")))

(define-public crate-minisat-0.4.2 (c (n "minisat") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "05pgx4j508i6cgngf116zihwqi3s0aidb8784hap7kcr2q46fdzp")))

(define-public crate-minisat-0.4.3 (c (n "minisat") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "086yllpfdlw8k9hh3waixf1xhi3mr2knpr2vg4f968w4b3vh3958") (f (quote (("glucose"))))))

(define-public crate-minisat-0.4.4 (c (n "minisat") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "sudoku") (r "^0.7") (d #t) (k 2)))) (h "06605q813x58nih8z6d6bk2dsqxlpw5fj6acgmn1ic0ryg3cid40") (f (quote (("glucose"))))))

