(define-module (crates-io mi ni minigrep_123) #:use-module (crates-io))

(define-public crate-minigrep_123-0.1.0 (c (n "minigrep_123") (v "0.1.0") (h "1vdd7gpffgf6f177hxkdrslwh23d96z5cf543i6xhics7c3kr5b1") (y #t)))

(define-public crate-minigrep_123-0.1.1 (c (n "minigrep_123") (v "0.1.1") (h "18xn7434nljw4cbw2j07glrifz1spgm0fcqn6m08n1nx2sn9cyz1")))

