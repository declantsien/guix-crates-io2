(define-module (crates-io mi ni miniaudio-sys) #:use-module (crates-io))

(define-public crate-miniaudio-sys-0.0.0 (c (n "miniaudio-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1slq66zbkbgyj39af5iqyc59pwywsls0zx58xjr60lbm7ym17aqs") (f (quote (("ma_wav_decoder") ("ma_vorbis_decoder") ("ma_mp3_decoder") ("ma_flac_decoder") ("ma_debug") ("default" "ma_flac_decoder" "ma_mp3_decoder" "ma_vorbis_decoder" "ma_wav_decoder")))) (l "miniaudio")))

