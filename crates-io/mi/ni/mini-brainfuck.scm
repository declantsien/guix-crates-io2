(define-module (crates-io mi ni mini-brainfuck) #:use-module (crates-io))

(define-public crate-mini-brainfuck-0.1.0 (c (n "mini-brainfuck") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "162hggbyvc94hcb0ak47x94rvy1r7avilhqsjywsqhmcsza0hnxw")))

(define-public crate-mini-brainfuck-0.1.1 (c (n "mini-brainfuck") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "044s2grpf94a38k6kagikv9n5gfi5xsiy7wiz1nh62lb109zsrlk")))

(define-public crate-mini-brainfuck-0.1.2 (c (n "mini-brainfuck") (v "0.1.2") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w3j26nk7gzvn2ajl7dd4hd0b3h2nn01khkd2bckx864z0v0bqrn")))

(define-public crate-mini-brainfuck-0.1.3 (c (n "mini-brainfuck") (v "0.1.3") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "12ww6m2kmd1mixyb1i6dxxasi8wn1lh11l4i7d1dky5gxclzx599")))

(define-public crate-mini-brainfuck-0.1.4 (c (n "mini-brainfuck") (v "0.1.4") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16vw2vb1z35l1xi58mv3091ljkjsfyy2zgz6fmswcsdpqkc3i1nz")))

(define-public crate-mini-brainfuck-1.0.0 (c (n "mini-brainfuck") (v "1.0.0") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0j9bqnxs1nhjblniqwcbqk984ibff3j6zv5vkhv80yh3wrzz5ard")))

(define-public crate-mini-brainfuck-1.0.1 (c (n "mini-brainfuck") (v "1.0.1") (d (list (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0g2qg4h45vvdkfhhp64vlxx0yfsgk0hfy2sgqry3a210lz30pcf5")))

