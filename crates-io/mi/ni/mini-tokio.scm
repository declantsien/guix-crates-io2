(define-module (crates-io mi ni mini-tokio) #:use-module (crates-io))

(define-public crate-mini-tokio-0.1.0 (c (n "mini-tokio") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)))) (h "0iaspp03w0yvqak0xlnzhb6g2pxpph2b9g5bw12sl3g0a0pylin6")))

