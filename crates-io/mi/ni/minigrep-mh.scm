(define-module (crates-io mi ni minigrep-mh) #:use-module (crates-io))

(define-public crate-minigrep-mh-0.1.0 (c (n "minigrep-mh") (v "0.1.0") (h "0lri97dr9kbqxadmbb6z69fs7zh11afbc2cpyaz8xmayg867wysw") (y #t)))

(define-public crate-minigrep-mh-0.1.1 (c (n "minigrep-mh") (v "0.1.1") (h "067mpib9dn80cqy0nkb7wsynndc240maj7spxlixqczwshw095rm") (y #t)))

