(define-module (crates-io mi ni minigrepsv1) #:use-module (crates-io))

(define-public crate-minigrepsv1-0.1.0 (c (n "minigrepsv1") (v "0.1.0") (h "0njqn0h8pb6gfx91v566h2p62wdklab4f1xdqabjnr2qgap0h6va")))

(define-public crate-minigrepsv1-0.1.1 (c (n "minigrepsv1") (v "0.1.1") (h "08jswrg739ih3h64h4vh8inmynp4sx9gzrpdvfz99kjsx7rgx12v")))

