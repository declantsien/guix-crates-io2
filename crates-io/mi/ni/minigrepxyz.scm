(define-module (crates-io mi ni minigrepxyz) #:use-module (crates-io))

(define-public crate-minigrepxyz-0.1.0 (c (n "minigrepxyz") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0qi5pzhbg9m29xc7q777qmb3784z14w82pnmfi5afvmhrmy00j8y") (y #t)))

(define-public crate-minigrepxyz-0.1.1 (c (n "minigrepxyz") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "14jd270lc5r955xw3b7yrsav4iiqc8q9wg9qa5sr0fcajlc756f4") (y #t)))

