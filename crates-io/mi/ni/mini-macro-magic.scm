(define-module (crates-io mi ni mini-macro-magic) #:use-module (crates-io))

(define-public crate-mini-macro-magic-1.0.0 (c (n "mini-macro-magic") (v "1.0.0") (h "0h6h61yyyy2f63vhn48d5psnzv2gpw8vw3q5hwi0p6wid50v0pzh")))

(define-public crate-mini-macro-magic-2.0.0 (c (n "mini-macro-magic") (v "2.0.0") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)))) (h "0m7l8llssivambvcl7clicqbzvq3i6rrg9k1gp383in9qdchhgg2")))

(define-public crate-mini-macro-magic-2.0.1 (c (n "mini-macro-magic") (v "2.0.1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)))) (h "0g698391w7z2dgvzp9c8wgak0scy05s76f09657pcaxgji5km8d5")))

(define-public crate-mini-macro-magic-2.0.2 (c (n "mini-macro-magic") (v "2.0.2") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)))) (h "042jq95v0942xqzz4zdsdbfs6qi9rw8zhk2fl7zb9888sm16p4zn")))

