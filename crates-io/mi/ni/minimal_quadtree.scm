(define-module (crates-io mi ni minimal_quadtree) #:use-module (crates-io))

(define-public crate-minimal_quadtree-0.1.0 (c (n "minimal_quadtree") (v "0.1.0") (d (list (d (n "noisy_float") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0glncbw26k3p827zwcvlyh7i85rwxsaiiipw2fnsgakh9sni2mx7") (y #t)))

(define-public crate-minimal_quadtree-0.1.1 (c (n "minimal_quadtree") (v "0.1.1") (d (list (d (n "noisy_float") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0gymy5zd1521mfyjjv76zdqyhrcqd7v407b4nr2qm6pvn9h6paw5") (y #t)))

