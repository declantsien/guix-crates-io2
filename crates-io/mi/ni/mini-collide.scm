(define-module (crates-io mi ni mini-collide) #:use-module (crates-io))

(define-public crate-mini-collide-0.1.0 (c (n "mini-collide") (v "0.1.0") (d (list (d (n "mini-math") (r "^0.1.2") (d #t) (k 0)))) (h "0lxg8lv9jvx8fhgc52a8ry0zmhs3kvzna8khp6yhzfglwc8zcb6r")))

(define-public crate-mini-collide-0.1.1 (c (n "mini-collide") (v "0.1.1") (d (list (d (n "mini-math") (r "^0.1.3") (d #t) (k 0)))) (h "0iaa3fgl8m649qzxx31kgcgjknbxbmdbbksqhq6kqa5iavanjfz1")))

(define-public crate-mini-collide-0.2.0 (c (n "mini-collide") (v "0.2.0") (d (list (d (n "mini-math") (r "^0.2") (d #t) (k 0)))) (h "18x4m4h06d0z4pi3d0bgl503mpbak2nbmryhiv3ywaqp5y98hkhv")))

(define-public crate-mini-collide-0.2.1 (c (n "mini-collide") (v "0.2.1") (d (list (d (n "mini-math") (r "^0.2") (d #t) (k 0)))) (h "1lapw6hjkxcg9fawqgxdcs971lcdsgizl950sk1wq9p77kd4ardi")))

(define-public crate-mini-collide-0.3.0 (c (n "mini-collide") (v "0.3.0") (d (list (d (n "mini-math") (r "^0.2") (d #t) (k 0)))) (h "1cfc31gy6ik371w2nhlz1iis4shf70v0azjpnxpbpagg5pnnhhll")))

(define-public crate-mini-collide-0.3.1 (c (n "mini-collide") (v "0.3.1") (d (list (d (n "mini-math") (r "^0.2") (d #t) (k 0)))) (h "0hww6bvw19g994kl8mz23z7swmq39h4a9p94fx0344cwbkznl4m7")))

(define-public crate-mini-collide-0.3.2 (c (n "mini-collide") (v "0.3.2") (d (list (d (n "mini-math") (r "^0.2") (d #t) (k 0)))) (h "00q1c5vqqm7smjr2m983xni8l3i237dbm9iklf62631h4526q6f9")))

