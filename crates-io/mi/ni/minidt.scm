(define-module (crates-io mi ni minidt) #:use-module (crates-io))

(define-public crate-minidt-0.1.0 (c (n "minidt") (v "0.1.0") (d (list (d (n "anstyle") (r "^1.0.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minijinja") (r "^1.0.15") (f (quote ("loader"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1jj66c918achmasbcnlipp007fi5hm7s74s8b5qngh01y7nvq66j")))

