(define-module (crates-io mi ni minimp3_ex-sys) #:use-module (crates-io))

(define-public crate-minimp3_ex-sys-0.1.0 (c (n "minimp3_ex-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jbfb3938z692pn70srch5ixs30axx2msfkxdlqg3c5s4gxzwv4k") (f (quote (("float-output"))))))

(define-public crate-minimp3_ex-sys-0.1.1 (c (n "minimp3_ex-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "115w6w4ihybwm6wphljqcffj8swkp165h954gs75gsy6jsjyawd1") (f (quote (("float-output"))))))

