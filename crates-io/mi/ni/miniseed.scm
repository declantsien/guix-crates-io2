(define-module (crates-io mi ni miniseed) #:use-module (crates-io))

(define-public crate-miniseed-1.0.0 (c (n "miniseed") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "17kkp4n4gh5khjk94nw4sqs7vhda05bbmm0p0ihqkv71wir8qkdd")))

(define-public crate-miniseed-1.0.1 (c (n "miniseed") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "11njfz4qnxks90r74khlm0sm8jq4ca4x04yjb21xd81damh7zfk8")))

