(define-module (crates-io mi ni miniupload) #:use-module (crates-io))

(define-public crate-miniupload-0.1.0 (c (n "miniupload") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x7j623jwn1cwckyl7lwrfxvwwhcn19a362vxxriz4h2c5arr1jr")))

