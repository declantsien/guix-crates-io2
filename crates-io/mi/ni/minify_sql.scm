(define-module (crates-io mi ni minify_sql) #:use-module (crates-io))

(define-public crate-minify_sql-0.1.2 (c (n "minify_sql") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1rw1f59zadfcy4j02f6adxdfyzhvka2gd81jyydqi3fgjb9h11py")))

(define-public crate-minify_sql-0.1.3 (c (n "minify_sql") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "15c6c8w2a0v6lpvycdl62g40mi8b4nw7qzasz0wip3l82ms2hym0")))

(define-public crate-minify_sql-0.1.4 (c (n "minify_sql") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1f6wmv1zawi9jvlbm51i0hs3qg24h29469grxdzk7zx7y358n3w6")))

