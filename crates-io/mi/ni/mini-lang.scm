(define-module (crates-io mi ni mini-lang) #:use-module (crates-io))

(define-public crate-mini-lang-0.1.0 (c (n "mini-lang") (v "0.1.0") (d (list (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lp7l8xy8s8k9d9avwm2bp09rjpf6mvb6dbf5s7fqjccf2v0vknh") (f (quote (("default") ("bin" "structopt"))))))

(define-public crate-mini-lang-0.1.1 (c (n "mini-lang") (v "0.1.1") (d (list (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zhg6c6kys12xm13x3gnzdc6yghasm32m423626hgjmxjpddgfjc") (f (quote (("default") ("bin" "structopt"))))))

(define-public crate-mini-lang-0.1.2 (c (n "mini-lang") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k21s75g5bc01g54l61m6grlmlz3m6hbc0b3vad419p1f0z2lcd0") (f (quote (("default") ("bin" "structopt"))))))

