(define-module (crates-io mi ni minicoroutine) #:use-module (crates-io))

(define-public crate-minicoroutine-0.1.0 (c (n "minicoroutine") (v "0.1.0") (d (list (d (n "minicoro-sys") (r "^0.8") (d #t) (k 0)))) (h "0q9im2h1xpdyl0b1v5696ldq9j0naq32fdkr9xkdx2kk8glpf49s") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-minicoroutine-0.1.2 (c (n "minicoroutine") (v "0.1.2") (d (list (d (n "minicoro-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0klrp2ry7zcwj3qg74nbqyhypggmk36c4i1f8lwan5x9h0hqbdzx") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-minicoroutine-0.1.3 (c (n "minicoroutine") (v "0.1.3") (d (list (d (n "minicoro-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1ypkgafz0jxwk4ix3zgfsda6piv56h252ir4sv6a1s1swwmin99a") (f (quote (("std") ("default" "std"))))))

