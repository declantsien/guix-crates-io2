(define-module (crates-io mi ni miniball) #:use-module (crates-io))

(define-public crate-miniball-0.1.0 (c (n "miniball") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "stacker") (r "^0.1.14") (d #t) (k 0)))) (h "04axs6ysjr4lvy57vd0v91df3lpz1i3s6yqsclyknqz1ypn53mlc")))

(define-public crate-miniball-0.1.1 (c (n "miniball") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "stacker") (r "^0.1.14") (d #t) (k 0)))) (h "0fxc7f6wgdsbj9jqc95v7mm3lx6fvr2ls8v71c96p8zgscjg7mqx")))

(define-public crate-miniball-0.2.0 (c (n "miniball") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)))) (h "01lqqj50rq53zsacvlrrsj3zrxw9sq7w51d056vfxmnl9hy42v3v")))

(define-public crate-miniball-0.3.0 (c (n "miniball") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)))) (h "0489kjbbkg8r2xmr2pqlrpvv9l85nwqbjzqny6m6vrpm1whipj38") (r "1.61.0")))

(define-public crate-miniball-0.4.0 (c (n "miniball") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.4") (f (quote ("alloc"))) (k 0)) (d (n "nalgebra") (r "^0.32.4") (f (quote ("rand"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (o #t) (d #t) (k 0)))) (h "19s98p9d6yw0w7wpww7b76chn1gwkvjhzvm05rzv0ldsv9lq6w6q") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:stacker")))) (r "1.61.0")))

(define-public crate-miniball-0.5.0 (c (n "miniball") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32.5") (f (quote ("alloc"))) (k 0)) (d (n "nalgebra") (r "^0.32.5") (f (quote ("alloc" "rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 2)) (d (n "rand_distr") (r "^0.4.3") (k 2)) (d (n "stacker") (r "^0.1.15") (o #t) (d #t) (k 0)))) (h "0pq972mh6fbics1lv6f1iqpz1w7jjhxw01k0qiv27wmgbj8lf5i6") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:stacker")))) (r "1.61.0")))

