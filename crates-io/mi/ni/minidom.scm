(define-module (crates-io mi ni minidom) #:use-module (crates-io))

(define-public crate-minidom-0.1.0 (c (n "minidom") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "1nhgzrwrpl8sgw3j83v5ai93gjv5j5iijzxbi6qgl4ifpm2adj5k")))

(define-public crate-minidom-0.1.1 (c (n "minidom") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "0p4jc9q0zbk0lc3x3id5fishc8g926067k9pkdcrra8ysn5k6191")))

(define-public crate-minidom-0.2.0 (c (n "minidom") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "1xnhv584sxbrcn8agra3ghkz40a5070m22xqxqy1c3bskikv9b3i")))

(define-public crate-minidom-0.3.0 (c (n "minidom") (v "0.3.0") (d (list (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1mqalbdpni3lnvm4hp1jyh3hg6k1xvs8syryh42llmx1psl5plws")))

(define-public crate-minidom-0.3.1 (c (n "minidom") (v "0.3.1") (d (list (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1a81vbw36cb05m8bwrnzf45j3bjzd5z09sk7zrjrs84fi87lin87")))

(define-public crate-minidom-0.3.2 (c (n "minidom") (v "0.3.2") (d (list (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0qwcj1rzqrjn6hqjr4s6q86ipfaysm3lgb8jgp8caykxw5i0q1fv")))

(define-public crate-minidom-0.4.0 (c (n "minidom") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1sh889c9ailwj3ipc1v5ms01lfkdbr8ha69f67fj6i3rspm52j4c")))

(define-public crate-minidom-0.4.1 (c (n "minidom") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0841mgv8i45qmg1ng07cjm2smyxf0584y1p9nk7bjridipx5p43r")))

(define-public crate-minidom-0.4.2 (c (n "minidom") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0mbbbhizwg657q5xkpfwllkbhrhpcpj1q52simdxg99bfxp4sdnw")))

(define-public crate-minidom-0.4.3 (c (n "minidom") (v "0.4.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "1w82s5wmaryy7qmjhvh0ply50isb8wzlasfkk769knqycry4g3s0")))

(define-public crate-minidom-0.5.0 (c (n "minidom") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.7.3") (d #t) (k 0)))) (h "1qrcyhmxmn1saza4nbpk9pj7glz5vpl1kcr20rahm43cbbs01xcz")))

(define-public crate-minidom-0.4.4 (c (n "minidom") (v "0.4.4") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "17yfnzi0y2gc0b5qbl9wbaw5hb13hbsmvcmck06fnyg124c7vnjx")))

(define-public crate-minidom-0.6.0 (c (n "minidom") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.7.3") (d #t) (k 0)))) (h "118x69ax1ka803q1km422g4r0fy8i1y8cx52zixwcaiarkyxhzi8")))

(define-public crate-minidom-0.6.1 (c (n "minidom") (v "0.6.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.0") (d #t) (k 0)))) (h "11kj4x89q3gk93ww8y0f0jajb975knx3xng9hawk94z03rw1f85h")))

(define-public crate-minidom-0.6.2 (c (n "minidom") (v "0.6.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.0") (d #t) (k 0)))) (h "01hhr800891l0lq4qgrjzhcz44i2ifrxk84ni6a1435d4rqx6j0x")))

(define-public crate-minidom-0.7.0 (c (n "minidom") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10.0") (d #t) (k 0)))) (h "1xawv0qac19flj3zxrn251vh0png9fhgas0z2h6q7vba3vmdc5zb")))

(define-public crate-minidom-0.8.0 (c (n "minidom") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.11.0") (d #t) (k 0)))) (h "0vaz88qim3w9gsfvaw4y5sy7d07bp0fcc5rczxvw1mqk2vczijvy")))

(define-public crate-minidom-0.9.0 (c (n "minidom") (v "0.9.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)))) (h "15rx8ysv0rzf8a9lxp1k3ns5qc49ki50k0z2vfkzb67cnqn63f74")))

(define-public crate-minidom-0.9.1 (c (n "minidom") (v "0.9.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.1") (d #t) (k 0)))) (h "1bjh3fwcw1bzr0naxcy787w37gw4xvil9f2cdr59xb6hz5mjk9f0")))

(define-public crate-minidom-0.10.0 (c (n "minidom") (v "0.10.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1yclryc8hssf3bwwzhgfi197i0qihd1hhxgj4b74mzy6lvp28l17")))

(define-public crate-minidom-0.11.0 (c (n "minidom") (v "0.11.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.14") (d #t) (k 0)))) (h "0kqwbynlngwq0mkld4z1g9bb42xjjznqib29fkzbpkcb9i77jwn1")))

(define-public crate-minidom-0.10.1 (c (n "minidom") (v "0.10.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1yxwh119k4nf0kjwk6g4ypwy29qdapg5cn0rzrrad218g0b4jr3k")))

(define-public crate-minidom-0.11.1 (c (n "minidom") (v "0.11.1") (d (list (d (n "quick-xml") (r "^0.16") (d #t) (k 0)))) (h "16pjxh0c302am9q3j1lp5p8bw378fs4jngakdf6k5i0mg34jj7yz") (f (quote (("default" "comments") ("comments"))))))

(define-public crate-minidom-0.12.0 (c (n "minidom") (v "0.12.0") (d (list (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "06nbqscsv2clc4mvdzzl1syn89plsqvmxn2lqxjfrxbllqar2m7y") (f (quote (("default" "comments") ("comments"))))))

(define-public crate-minidom-0.12.1 (c (n "minidom") (v "0.12.1") (d (list (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "1i3qs0m0z2299krazbqd82nzb7fd5jlkgnw0n10xic271bfqbd47") (y #t)))

(define-public crate-minidom-0.13.0 (c (n "minidom") (v "0.13.0") (d (list (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "13k0ngkwgj0zgn0zkkklnj274q351mpyzjaglr0dviwz2k19499k")))

(define-public crate-minidom-0.14.0 (c (n "minidom") (v "0.14.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0cfhzm9qxbg85llijvaqjhamvzif4b0bv8bvw292wi9g7p3msmih")))

(define-public crate-minidom-0.15.0 (c (n "minidom") (v "0.15.0") (d (list (d (n "rxml") (r "^0.8.0") (d #t) (k 0)))) (h "1igzgc84myv3spwmq6769fcgzmjwkg5rc45xs876139zhqhzxpcx")))

(define-public crate-minidom-0.15.1 (c (n "minidom") (v "0.15.1") (d (list (d (n "rxml") (r "^0.8.0") (d #t) (k 0)))) (h "0yrrvafh4qlfhgl3gwkmifwglk5fyngn1rw5la88fdcy8mfy971f")))

(define-public crate-minidom-0.15.2 (c (n "minidom") (v "0.15.2") (d (list (d (n "rxml") (r "^0.9.1") (f (quote ("mt"))) (k 0)))) (h "0y32595a7f055890jgmhf5cm49q21jk152bnl5vipkiqaw3i8mpl")))

