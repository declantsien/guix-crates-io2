(define-module (crates-io mi ni minigrep_tupelo) #:use-module (crates-io))

(define-public crate-minigrep_tupelo-0.1.0 (c (n "minigrep_tupelo") (v "0.1.0") (h "1jcar9pv08srrv8w35hf5p9sq39sq90b2mxhgq3wrcqvzsarnrjh")))

(define-public crate-minigrep_tupelo-0.2.0 (c (n "minigrep_tupelo") (v "0.2.0") (h "1gy206hwyxs070r9h6p4ihlzvxxhq0wqz19zhp5sx59c6nj5bv42")))

