(define-module (crates-io mi ni miniconf_derive) #:use-module (crates-io))

(define-public crate-miniconf_derive-0.6.2 (c (n "miniconf_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dgsm3v57w2n0nbgrahmgw0i0pdkhvwfc0s9x10bkmzy3abg8hp1")))

(define-public crate-miniconf_derive-0.8.0 (c (n "miniconf_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0isxh0r30zx3s8as5nghc1fc3259mify5av6n2263j0l1da7ycn2")))

(define-public crate-miniconf_derive-0.9.0 (c (n "miniconf_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qxlwv7lb7jrnx13yp4ajyzkwjgy4pibk3kbsx9gah8fyhjnvx49")))

(define-public crate-miniconf_derive-0.10.0 (c (n "miniconf_derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dygl1kdd1rvc70fg9lk8g2gdi6lkw0lcn3q7mag9d3inr00vr9p") (r "1.70.0")))

(define-public crate-miniconf_derive-0.10.1 (c (n "miniconf_derive") (v "0.10.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rl10zgyifzdn08ajji4hhly9ils4l337xwqn18686yiiy7da5mn") (r "1.70.0")))

(define-public crate-miniconf_derive-0.11.0 (c (n "miniconf_derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0pla42mjml63djy4mlswxdswiv8s96413v6q8cy52v7z18bb1ga7") (r "1.70.0")))

