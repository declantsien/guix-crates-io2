(define-module (crates-io mi ni minigrep_luke_yang) #:use-module (crates-io))

(define-public crate-minigrep_luke_yang-0.1.0 (c (n "minigrep_luke_yang") (v "0.1.0") (h "1rkfmqv0mwlapxcz69w5wcvszy8rpndq9pcrm1myzlqszs3q95vb") (y #t)))

(define-public crate-minigrep_luke_yang-0.1.1 (c (n "minigrep_luke_yang") (v "0.1.1") (h "106nj01xnanhqmspf166b8v8zl5y9di4fhcwi33vbg74ipyr67yp") (y #t)))

(define-public crate-minigrep_luke_yang-0.1.2 (c (n "minigrep_luke_yang") (v "0.1.2") (h "10ysj5ayylva8p7q57kb1aq0v31n06dv7gq4xp5rza138fdavzq5") (y #t)))

