(define-module (crates-io mi ni minidump-synth) #:use-module (crates-io))

(define-public crate-minidump-synth-0.10.1 (c (n "minidump-synth") (v "0.10.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.10.1") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "00740jnyld2hgq0xg2m4b8qf6dggyxc9rqrv7vrbag3sc03kwwd8")))

(define-public crate-minidump-synth-0.10.2 (c (n "minidump-synth") (v "0.10.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.10.2") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1p80b9n708ffjy0rl633i2f3l7zvjizjsgb9x8460h1z7dgg71dj")))

(define-public crate-minidump-synth-0.10.3 (c (n "minidump-synth") (v "0.10.3") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.10.3") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0a01ng0x5bd22c16fka45kzn06jlhx6br8f8wvy1mqwi5flq52l4")))

(define-public crate-minidump-synth-0.10.4 (c (n "minidump-synth") (v "0.10.4") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.10.3") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1qxsx0r93pvx7kkmgx9przkfvqljfr2rpr2xkvhl6v7w0rbivkg1")))

(define-public crate-minidump-synth-0.11.0 (c (n "minidump-synth") (v "0.11.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.11.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0rz5ji05m0x9c5v6ymlnm0ghh93kqz3cs798y0vy73900ad4vs6b")))

(define-public crate-minidump-synth-0.12.0 (c (n "minidump-synth") (v "0.12.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.12.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1if787325ndiaz3fhd0pr8c2vp2a2vw7kfmm9as6i18q28w3wama")))

(define-public crate-minidump-synth-0.13.0 (c (n "minidump-synth") (v "0.13.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.13.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "115n9ai2s7yzms1bnl0dchnyk5ivjh78c6s494y84rjxfzpldfzw")))

(define-public crate-minidump-synth-0.14.0 (c (n "minidump-synth") (v "0.14.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.14.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "09spilnq6pc6xg8kybx8y5knlxq031b7xf0zk1q4pd8rkrkc32zj")))

(define-public crate-minidump-synth-0.15.0 (c (n "minidump-synth") (v "0.15.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.15.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "04z3ddbkjr5hakv7b8sql0dg2vblyamdlkx95my5gynx12h5cwni")))

(define-public crate-minidump-synth-0.15.1 (c (n "minidump-synth") (v "0.15.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.15.1") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0n1ik0q1fryzsw8f53rqmw4j7c10aidxhnax1lkfsi4kg6jjdi86")))

(define-public crate-minidump-synth-0.15.2 (c (n "minidump-synth") (v "0.15.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "minidump-common") (r "^0.15.2") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0zscsm9x8ic4f1xlkxkvz7mhj0ydvsi9ygvpa0mhrfp9hwzrprd9")))

(define-public crate-minidump-synth-0.16.0 (c (n "minidump-synth") (v "0.16.0") (d (list (d (n "minidump-common") (r "^0.16.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0jgvv575q0m079l18piszyajav7nwf0696p3b0cn6s4sqkmh7zjy")))

(define-public crate-minidump-synth-0.17.0 (c (n "minidump-synth") (v "0.17.0") (d (list (d (n "minidump-common") (r "^0.17.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1svn1pcymgy0kbibnsc3g35kvmspnnskxlydnaszp2i7nxhn0310")))

(define-public crate-minidump-synth-0.18.0 (c (n "minidump-synth") (v "0.18.0") (d (list (d (n "minidump-common") (r "^0.18.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1b9hyf7l766q4ag7s5a7cfgwc718jbqffvd7mmhq8mwk2ds2089p")))

(define-public crate-minidump-synth-0.19.0 (c (n "minidump-synth") (v "0.19.0") (d (list (d (n "minidump-common") (r "^0.19.0") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "12qdc02qhy3jvbnaic8bnwglakhqkws88q6hzcgd1ryzzynzfiw1")))

(define-public crate-minidump-synth-0.19.1 (c (n "minidump-synth") (v "0.19.1") (d (list (d (n "minidump-common") (r "^0.19.1") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0jkj091jm1qmxd3c3daa92csvqj2gchrb172p6k45i9yywbz992w")))

(define-public crate-minidump-synth-0.20.0 (c (n "minidump-synth") (v "0.20.0") (d (list (d (n "minidump-common") (r "^0.20.0") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "1ivarnh2j6jnyf9624b2np8hcnkhb9ah648k6pzagq9nrcm0jyl2")))

(define-public crate-minidump-synth-0.21.0 (c (n "minidump-synth") (v "0.21.0") (d (list (d (n "minidump-common") (r "^0.21.0") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0kw7287i1wf3vpmh16369jgla32hpk1dnyk5fgbc5lmkh1k4nngz")))

(define-public crate-minidump-synth-0.21.1 (c (n "minidump-synth") (v "0.21.1") (d (list (d (n "minidump-common") (r "^0.21.1") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (d #t) (k 0)) (d (n "test-assembler") (r "^0.1.5") (d #t) (k 0)))) (h "0z2129fs0ypv3f7apnxvcz30gvacry1d62qc2mpqdswbj2krdrz2")))

