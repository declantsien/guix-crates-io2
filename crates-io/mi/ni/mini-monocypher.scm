(define-module (crates-io mi ni mini-monocypher) #:use-module (crates-io))

(define-public crate-mini-monocypher-0.1.0 (c (n "mini-monocypher") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "mini-monocypher-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)))) (h "1329102xarrmj0w5c0a0vgr82zj4vlhqh3jxk7xznajn91bg5cw2")))

