(define-module (crates-io mi ni miniurl) #:use-module (crates-io))

(define-public crate-miniurl-0.1.0 (c (n "miniurl") (v "0.1.0") (h "1qdr9d256dn5kzn7ika36x134c9zxfwf668chdfipza6y5w715fm")))

(define-public crate-miniurl-0.1.1 (c (n "miniurl") (v "0.1.1") (h "1rvvy9j8kqz547z9ihcky324hg2140x8nli78v3qhs1lqv78fish")))

(define-public crate-miniurl-0.1.2 (c (n "miniurl") (v "0.1.2") (h "121avdls9p82w61jki5rqrbfsmvcak8winh9nlg8mhyd2jg970f8")))

(define-public crate-miniurl-0.1.3 (c (n "miniurl") (v "0.1.3") (h "1q03wgz4ifag4z1vl20lg35rwgxc9j3lkz7qlxplwmd4725y4ihk")))

