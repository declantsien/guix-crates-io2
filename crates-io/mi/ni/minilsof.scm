(define-module (crates-io mi ni minilsof) #:use-module (crates-io))

(define-public crate-minilsof-0.1.0 (c (n "minilsof") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1bm0wqpy843h8hdlgphv7aavz7yrpzgnhycd541lfixykqfz3yrw") (y #t)))

(define-public crate-minilsof-0.1.1 (c (n "minilsof") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1mlqbvzbak5nrgsyr06cz9wjx55bwrph3ma0wil345mpdbpjf7rb") (y #t)))

(define-public crate-minilsof-0.1.2 (c (n "minilsof") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0f0cs215v5mb90q2y3chxfzd2xc16y208scf8dfjy287r2nlzi03")))

