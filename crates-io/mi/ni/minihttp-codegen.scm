(define-module (crates-io mi ni minihttp-codegen) #:use-module (crates-io))

(define-public crate-minihttp-codegen-0.1.0 (c (n "minihttp-codegen") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1aqm4kpycpp3nrgf5k04vdkw60dv7d6014hfjc5k1k86rcnfhbk8")))

