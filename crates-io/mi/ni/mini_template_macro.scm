(define-module (crates-io mi ni mini_template_macro) #:use-module (crates-io))

(define-public crate-mini_template_macro-0.1.0 (c (n "mini_template_macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6vvprqqcsx2pjf7p6wzyzj4sm44gwg8hnw4v7m30b1857im3rx")))

