(define-module (crates-io mi ni minix) #:use-module (crates-io))

(define-public crate-minix-0.1.0 (c (n "minix") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hq696idlx1f6yqv2mpfx3plzzg00kmnyl69xbkmy692r4lny5wv")))

(define-public crate-minix-0.1.1 (c (n "minix") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11sjqclj3xmqhxy6frpxs62yz4fjnnr63y7gd587adqyzkw768mi")))

(define-public crate-minix-0.1.2 (c (n "minix") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7rc63jrh0m4f6pg8lcm33w04m52k5cll2plfb2s6w1wwfg3rpf")))

(define-public crate-minix-0.1.3 (c (n "minix") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13sdxdzxnwy969zn127nhb21afamaybdq75yq149azw6yrsf9pmh")))

