(define-module (crates-io mi ni minimap2-paf-io) #:use-module (crates-io))

(define-public crate-minimap2-paf-io-0.1.0 (c (n "minimap2-paf-io") (v "0.1.0") (h "19pydkjiv8ifgyjnbf0h9cq5pj3dh9j55jkczp9w00gxjyz6564v") (r "1.58.1")))

(define-public crate-minimap2-paf-io-1.0.0 (c (n "minimap2-paf-io") (v "1.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ixvzw2gsr3lq7ci8q1hnh1j406dzfjqrid8g71nwmgnblmhykjc") (r "1.58.1")))

(define-public crate-minimap2-paf-io-2.0.0 (c (n "minimap2-paf-io") (v "2.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ns9kdbkgbw67s5fj1cg99azv3fxd0c8zzfjy14wnnqxy0s85prj") (r "1.58.1")))

(define-public crate-minimap2-paf-io-3.0.0 (c (n "minimap2-paf-io") (v "3.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1a1r6xyxa8gvc5mbn42c9vyf3rb1wq8a2qjiypvnh3pq6cgw38nz") (r "1.58.1")))

