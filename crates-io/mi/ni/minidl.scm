(define-module (crates-io mi ni minidl) #:use-module (crates-io))

(define-public crate-minidl-0.1.0 (c (n "minidl") (v "0.1.0") (h "1sdrmp7i9rn11xs21098xmaz7wxngzjf1zxwx43vg3j3km4a4zb4") (f (quote (("nightly"))))))

(define-public crate-minidl-0.1.1 (c (n "minidl") (v "0.1.1") (h "1lid7mmfbmmrmp9089p6v0nask988nxw423033vr111yvhbiymbl") (f (quote (("nightly"))))))

(define-public crate-minidl-0.1.2 (c (n "minidl") (v "0.1.2") (h "1md70s98may6splrf598a37j6bhwld0ai9bxjksfxvnijk23m7wa") (r "1.54")))

(define-public crate-minidl-0.1.3 (c (n "minidl") (v "0.1.3") (h "1cg4dmgmsf5wrq35jw522dx1lmzp4zqwzr43n8la5a3ryns3mbfr") (r "1.54")))

(define-public crate-minidl-0.1.4 (c (n "minidl") (v "0.1.4") (h "1jd5vkzy6gpnzr17hcypizg2xbya4fxx5097jqisyzr9flmc99ay") (r "1.54")))

(define-public crate-minidl-0.1.5 (c (n "minidl") (v "0.1.5") (h "00fq4q6z8pgyp7hnjn4pwaqhay89yz6dqvh9q0q25ljydrkdilw7") (r "1.54")))

(define-public crate-minidl-0.1.6 (c (n "minidl") (v "0.1.6") (h "0rlkqqc1zdmxy1mvjfd5b0hwkv1xm95imydp2n54n5hm4wx042pw") (r "1.54")))

