(define-module (crates-io mi ni mini_sbc) #:use-module (crates-io))

(define-public crate-mini_sbc-0.1.0 (c (n "mini_sbc") (v "0.1.0") (h "1ns43r1fqwm60y3dw3nf07nwm48yrivxv2wvshxpx7qnfhjb1dv5")))

(define-public crate-mini_sbc-0.1.1 (c (n "mini_sbc") (v "0.1.1") (h "18s9jiqpdxwvkvpl28xvs5r181bhm4rgvkda7jxzjc4x2qf7hfhv")))

(define-public crate-mini_sbc-0.1.2 (c (n "mini_sbc") (v "0.1.2") (h "1l5sah7rwhbz3rzzdyv0hbcx6xrwb6jl7za2k93lhfjnxbkij8ap")))

(define-public crate-mini_sbc-0.1.3 (c (n "mini_sbc") (v "0.1.3") (h "199v9jxvyanmbl3ykmrd5bl0nxq2z7qmfg8fk7yagwamqfm4wp03")))

(define-public crate-mini_sbc-0.1.4 (c (n "mini_sbc") (v "0.1.4") (h "19brr0xwn1x9gsry9cp7kj6c4zwfgfls89xyxynw311zgh3sxqwx")))

(define-public crate-mini_sbc-0.1.5 (c (n "mini_sbc") (v "0.1.5") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "0jam7zhxhqyqls26f87a2bqiywpd0qh207vpqd5z2237i3gkgrh1")))

(define-public crate-mini_sbc-0.1.6 (c (n "mini_sbc") (v "0.1.6") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "04qvmb6kfm7z8hbbxaskd66y6ww7gnwwik411yhapbhfggn5dp82")))

(define-public crate-mini_sbc-0.1.7 (c (n "mini_sbc") (v "0.1.7") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "0c5qb1sbnvmryz2marhi7hbm6hwijpskygwdxzh2j6qjk40yn8h5")))

