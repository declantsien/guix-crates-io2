(define-module (crates-io mi ni mini-math) #:use-module (crates-io))

(define-public crate-mini-math-0.1.0 (c (n "mini-math") (v "0.1.0") (h "0cxxplx8dla4a6h4dsbig35b2c5jlnwcipr7kih200580bkdzmza") (y #t)))

(define-public crate-mini-math-0.1.1 (c (n "mini-math") (v "0.1.1") (h "1hy9f4sr9jnyjrk87c8cqvnamvb0fdixin1b1dzw2gqc8kyic3dm")))

(define-public crate-mini-math-0.1.2 (c (n "mini-math") (v "0.1.2") (d (list (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "097v3y1wjc5x8v1nvvxpjy4halnwfd4n3p1zj16q854szh2jy76f")))

(define-public crate-mini-math-0.1.3 (c (n "mini-math") (v "0.1.3") (d (list (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "019pamikq4vxqic1kbs3s1ly14vp7lw6b1ci7zb7kfwwg04lsvyb")))

(define-public crate-mini-math-0.1.4 (c (n "mini-math") (v "0.1.4") (d (list (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "11j28n3sav1mhayzrwv4fgx3fsjzpb01c6ls3lvsal0cljg7drxv")))

(define-public crate-mini-math-0.2.0 (c (n "mini-math") (v "0.2.0") (h "0da7fgp9yg7s3bzkl8j6nrbngz94bdyn5zq88nss1zawbwcggwfb")))

(define-public crate-mini-math-0.2.1 (c (n "mini-math") (v "0.2.1") (h "1pbx6r0nxk3fjav54yi2z7cpfva9krvagiki4ghc2zx8ya7v5jg0")))

(define-public crate-mini-math-0.2.2 (c (n "mini-math") (v "0.2.2") (h "1svglcrig9j7s0j0xh0s67dkrjsxznk1wc0pvhnd5ksiakjhp0yn")))

(define-public crate-mini-math-0.2.3 (c (n "mini-math") (v "0.2.3") (h "1hg8x2zfbxmpjhcj20b2xg8r06l3ivd901irnw801h5ixcmxwc7x")))

(define-public crate-mini-math-0.2.4 (c (n "mini-math") (v "0.2.4") (h "0x0vqkmw71x799pnmj9wgv1197qw11qkwjyklmh1qb4ywfzm7p0q")))

(define-public crate-mini-math-0.2.5 (c (n "mini-math") (v "0.2.5") (h "1rsf7d8anr5b6mx5qi44sk62x57fi9jwkmrw7dx8ybfyhlyc57pf")))

(define-public crate-mini-math-0.2.6 (c (n "mini-math") (v "0.2.6") (h "092nq9y1nbacvljdxxvl8gzqcs7h58f1s3j72blyx2f50a53xdi4")))

(define-public crate-mini-math-0.2.7 (c (n "mini-math") (v "0.2.7") (h "1fvq4f9idx19jhcknmhwbxlry5zbq5kihj86k61jf03gvgnpq0sa")))

