(define-module (crates-io mi ni mining_sv2) #:use-module (crates-io))

(define-public crate-mining_sv2-0.1.1 (c (n "mining_sv2") (v "0.1.1") (d (list (d (n "binary_sv2") (r "^0.1.6") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "0dfl9yahmvndx1nc748jfbw5gy7rgvvys0m39ppa335lpl5vwif3") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-mining_sv2-1.0.0 (c (n "mining_sv2") (v "1.0.0") (d (list (d (n "binary_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "const_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1k04kfvwf3mj2yrbngbvg03lxh5g2wyvr1200x8wc6va4b6n593g") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

