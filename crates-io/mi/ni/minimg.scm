(define-module (crates-io mi ni minimg) #:use-module (crates-io))

(define-public crate-minimg-0.1.0 (c (n "minimg") (v "0.1.0") (d (list (d (n "minvect") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1x2m874wb7z5c924yvy0apal8q22jbhnrc54mg4lph97f9zlwr5r")))

(define-public crate-minimg-0.1.1 (c (n "minimg") (v "0.1.1") (d (list (d (n "minvect") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "10lw8gd1pmvan5jj33cqsi8lpsd6igwy6ppgj9jqf5n8gy95qcfb")))

(define-public crate-minimg-0.1.2 (c (n "minimg") (v "0.1.2") (d (list (d (n "minvect") (r "^0.1.3") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1k2i0hvg2ckzd0n7rv01p84767dl8l8yg579a3qpgzhgi3nwa7ad")))

(define-public crate-minimg-0.1.3 (c (n "minimg") (v "0.1.3") (d (list (d (n "minvect") (r "^0.1.3") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1slh4bng5pnkpwzqwsll0wi41nzw9xp7vhy030y1ayjvp9bv7h43")))

(define-public crate-minimg-0.1.4 (c (n "minimg") (v "0.1.4") (d (list (d (n "minvect") (r "^0.1.6") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "0zpv1xip09jnsdh6h371j8nlfqw1kricl3ddk502cna1sbwkhq15")))

