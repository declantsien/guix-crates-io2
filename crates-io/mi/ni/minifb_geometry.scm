(define-module (crates-io mi ni minifb_geometry) #:use-module (crates-io))

(define-public crate-minifb_geometry-0.1.0 (c (n "minifb_geometry") (v "0.1.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)))) (h "1m1626lxd1f1rani93b4cdxsls7s07g21k25hc9w7lbgkvn9i6i9")))

(define-public crate-minifb_geometry-0.1.1 (c (n "minifb_geometry") (v "0.1.1") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)))) (h "06q9a4199rsgjh112jwbs2d57dyqff56df0k3jyj7cjnax52iish")))

