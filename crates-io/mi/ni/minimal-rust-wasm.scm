(define-module (crates-io mi ni minimal-rust-wasm) #:use-module (crates-io))

(define-public crate-minimal-rust-wasm-0.2.0 (c (n "minimal-rust-wasm") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.40") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "00yx4r0vj3dwsixy9l82v73n66gs8x8gqalavzibcbj23pv8hs6k")))

