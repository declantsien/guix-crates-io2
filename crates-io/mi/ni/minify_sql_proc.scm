(define-module (crates-io mi ni minify_sql_proc) #:use-module (crates-io))

(define-public crate-minify_sql_proc-0.1.3 (c (n "minify_sql_proc") (v "0.1.3") (d (list (d (n "minify_sql") (r "^0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "1jf0hiz0cdsvhghk4jhswpvfzrgs8h54hrqdv23mq3191gxd8dyv")))

(define-public crate-minify_sql_proc-0.1.4 (c (n "minify_sql_proc") (v "0.1.4") (d (list (d (n "minify_sql") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "09wigj6mh7qk18s0y6y6y3mjplfqr5by1hab3ds5dyihn685diqv")))

