(define-module (crates-io mi ni mini-grep) #:use-module (crates-io))

(define-public crate-mini-grep-0.1.0 (c (n "mini-grep") (v "0.1.0") (h "1yvaw0l7kdqr6vrz8q1z8k5f74l4pxp70idkjfd6i0qhj6m5qc4s") (y #t)))

(define-public crate-mini-grep-0.1.1 (c (n "mini-grep") (v "0.1.1") (h "0vbcv968z7a0inr5i3c9zqp4g8sjp90nn44p0960fc7gnl3v2x1b")))

(define-public crate-mini-grep-0.1.2 (c (n "mini-grep") (v "0.1.2") (h "01zvj44g1wqna2ji3gin5jin04vpr8mjxyrh6hzn1i7xzgm72b76")))

(define-public crate-mini-grep-0.1.3 (c (n "mini-grep") (v "0.1.3") (h "0fakprgv3c7k2a0zh8f4gz1vigid7dsh3ihbrpp4gpavg8v11wz7")))

