(define-module (crates-io mi ni mini-builder-rs) #:use-module (crates-io))

(define-public crate-mini-builder-rs-0.1.0 (c (n "mini-builder-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 2)))) (h "12wsqwi5h4z6y613nc80m9h4ggwmqxd3jfsngaiqscsc2nf0v5qc")))

(define-public crate-mini-builder-rs-0.1.1 (c (n "mini-builder-rs") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 2)))) (h "1xi2hp176dcgpf0cc35ilj0400fhmg0vgspgz6f2yf3f1fc7z7ay")))

(define-public crate-mini-builder-rs-0.1.2 (c (n "mini-builder-rs") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 2)))) (h "1x3xkvcq7zx819ham11aw509krjm9c51l116if4nmr28fyh7m7a0")))

(define-public crate-mini-builder-rs-0.1.3 (c (n "mini-builder-rs") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 2)))) (h "0v2cr9x4l7gkpr0h14vkkqisrylc4w3xli9jdafhaicp4y8ccwq4")))

