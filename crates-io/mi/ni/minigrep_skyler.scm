(define-module (crates-io mi ni minigrep_skyler) #:use-module (crates-io))

(define-public crate-minigrep_skyler-0.1.0 (c (n "minigrep_skyler") (v "0.1.0") (h "1g0qd3aamibkf28sqycffn75889f38sv6dcljki79fz3zhnwfxqb")))

(define-public crate-minigrep_skyler-0.1.1 (c (n "minigrep_skyler") (v "0.1.1") (h "04lc80xn78806h2qhnk0fpcl9q9h1bgmj1ccga5x045zq67rdddm")))

