(define-module (crates-io mi ni minigrep-yogie) #:use-module (crates-io))

(define-public crate-minigrep-yogie-0.1.0 (c (n "minigrep-yogie") (v "0.1.0") (h "01f56xigcwhw4inj57yvl3ypaglhk608yalisf6yg25kl9bnsczx") (y #t)))

(define-public crate-minigrep-yogie-0.1.1 (c (n "minigrep-yogie") (v "0.1.1") (h "18ldv2zsv0c43rgvh20wcq50fbw58mmwmdv0qccsk72h0a8d25h4")))

