(define-module (crates-io mi ni mini-kanren) #:use-module (crates-io))

(define-public crate-mini-kanren-0.1.0 (c (n "mini-kanren") (v "0.1.0") (h "1jzzs1b5vs3f1vbaaip6pm65pllds1zqb5bm2sy43rkqn8nc8b29")))

(define-public crate-mini-kanren-0.2.0 (c (n "mini-kanren") (v "0.2.0") (h "1mdqxj0j4865m5jsalya9jkbaf1lpyhb4kgv82mznsni8w62dssf")))

(define-public crate-mini-kanren-0.2.1 (c (n "mini-kanren") (v "0.2.1") (h "1a81qmh9plixzzjnwslnwsd2wqqp88yqdxjasr2qandv9q1i7qnj") (y #t)))

(define-public crate-mini-kanren-0.2.2 (c (n "mini-kanren") (v "0.2.2") (h "03j45kmhvpw6fwqzkw62pdlp2c1jzcsbpf98vq2riajjknd7x04b")))

(define-public crate-mini-kanren-0.3.0 (c (n "mini-kanren") (v "0.3.0") (h "1fwykwdk5p28zk8w320n4ydp7a2qz2g1lqdphagvw811q4mrnv9s")))

(define-public crate-mini-kanren-0.3.1 (c (n "mini-kanren") (v "0.3.1") (h "139nysqhyf3lirw3sy38kr9fspcf4fs7dmhixllwizbjh7wppix3")))

(define-public crate-mini-kanren-0.3.2 (c (n "mini-kanren") (v "0.3.2") (h "1h91mkrzxqgrsxg332wr51kpw9i33iip0mjv24j7drkk78lf9l7v")))

(define-public crate-mini-kanren-0.3.3 (c (n "mini-kanren") (v "0.3.3") (h "037wrcrn5bhg5lrjsima9d6cgjligfkpqcr3wkzi3c4gzx468w85")))

(define-public crate-mini-kanren-0.3.4 (c (n "mini-kanren") (v "0.3.4") (h "17pcyig2v96hf3d1xl2vrav9cpp92scr2j191cydwgb8gjwacmjw")))

(define-public crate-mini-kanren-0.3.5 (c (n "mini-kanren") (v "0.3.5") (h "09mzlqclwizangivw24ql77cilh4md5hfa20v1izry3nj12x2c2n")))

(define-public crate-mini-kanren-0.4.0 (c (n "mini-kanren") (v "0.4.0") (h "16c9wrn16xdkv11brs3f1qh0js9cx89fn017ps1azfvi3glr6428")))

