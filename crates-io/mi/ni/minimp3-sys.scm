(define-module (crates-io mi ni minimp3-sys) #:use-module (crates-io))

(define-public crate-minimp3-sys-0.2.0 (c (n "minimp3-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jy0idly9maw8p3nadx0rafxc5lxp8ivq53jn55ymrmmfg5cnqns")))

(define-public crate-minimp3-sys-0.3.0 (c (n "minimp3-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qvf6gbahy0c7wwpvyv73wsx1ln7n4r9yn3gipb31cgxfmpa41my")))

(define-public crate-minimp3-sys-0.3.1 (c (n "minimp3-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05vqh5qazvsmjcwzvzzh238bsia54cpiw45b7yjy7mhaq02sw2f1")))

(define-public crate-minimp3-sys-0.3.2 (c (n "minimp3-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "144vmf3s89kad0smjprzigcp2c9r5dm95n4ydilrbp399irp6772")))

