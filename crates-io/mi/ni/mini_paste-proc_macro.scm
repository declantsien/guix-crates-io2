(define-module (crates-io mi ni mini_paste-proc_macro) #:use-module (crates-io))

(define-public crate-mini_paste-proc_macro-0.1.10 (c (n "mini_paste-proc_macro") (v "0.1.10") (h "154s2nafghvn87caw0lqambcnjzpl91h917lh47vkmk6s6kk1iri")))

(define-public crate-mini_paste-proc_macro-0.1.11 (c (n "mini_paste-proc_macro") (v "0.1.11") (h "0ryv978qz4g1mxfkkg878rad8wwinbqm9d7l7bkjidrr5vsz3ig6")))

