(define-module (crates-io mi ni minidom_ext) #:use-module (crates-io))

(define-public crate-minidom_ext-1.0.0 (c (n "minidom_ext") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "minidom") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n66fhb3ip7z18f3xc576dv0rn53i3hhjddqamqndh19bxnndj4j")))

(define-public crate-minidom_ext-1.0.1 (c (n "minidom_ext") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "minidom") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yx307ah2df0d9z78q5vyhb8g09hqwi0q2jxd73r592cwj482ahc")))

(define-public crate-minidom_ext-1.1.0 (c (n "minidom_ext") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "008ppnh364vg5b6a3mj2jm26dkir88g07v495dvricnr92n8aj6d")))

(define-public crate-minidom_ext-1.1.1 (c (n "minidom_ext") (v "1.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "minidom") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1674bi9x56ki8gbdir0qy24fq3gl4gi1lldis5hi8051idda42r8")))

