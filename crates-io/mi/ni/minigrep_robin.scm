(define-module (crates-io mi ni minigrep_robin) #:use-module (crates-io))

(define-public crate-minigrep_robin-0.1.0 (c (n "minigrep_robin") (v "0.1.0") (h "12y9a2gzjq4y1g6v2m39cz1x58nskb57s00iph8m2qgig3f76xiz") (y #t)))

(define-public crate-minigrep_robin-0.1.1 (c (n "minigrep_robin") (v "0.1.1") (h "031y76n6pv08faf03l8zryvpwj40lr1qccy62xcmz8irvrxcvfzb")))

