(define-module (crates-io mi ni minicov) #:use-module (crates-io))

(define-public crate-minicov-0.1.0 (c (n "minicov") (v "0.1.0") (d (list (d (n "cstr_core") (r "^0.2.0") (d #t) (k 0)) (d (n "postcard") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("alloc"))) (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1lqs8nmp6131kdqhrpkm0lgmwk12mwx268xaqpn6njpmj2iaqwd1")))

(define-public crate-minicov-0.1.1 (c (n "minicov") (v "0.1.1") (d (list (d (n "cstr_core") (r "^0.2.0") (d #t) (k 0)) (d (n "postcard") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("alloc"))) (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1p5gcdjr1frcv2glp3krxd57gk9vk0asgysfqrnibmvkv2faw75c")))

(define-public crate-minicov-0.1.2 (c (n "minicov") (v "0.1.2") (d (list (d (n "cstr_core") (r "^0.2.2") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("alloc"))) (k 0)) (d (n "spinning_top") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0vl2qbknkw3ki9x6ygi8adh9zcj7kp2diimix7ymwz3kwivbmnx2")))

(define-public crate-minicov-0.2.0 (c (n "minicov") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "14qay9qrxjwmdjdbgrwvm7if6kwzmizrw9qzpwd9lmwlfhy643hm")))

(define-public crate-minicov-0.2.1 (c (n "minicov") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "08rdhn6br196r8xsffkcw4a95ak4npban5djh53vjkkcq785g8np") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.2.2 (c (n "minicov") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "1lpw2j3d5a55mlddfdw4p81v4z0x3qsv2x0fxg2zfp2i0ikdr0j2") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.2.3 (c (n "minicov") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "0p0q8nhhz7g4vi3q0vld2rcm673blbqf121i3yfq3in9czahgv51") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.2.4 (c (n "minicov") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "1i488ii5dk1v7vi4y6i7saz4r1xv4dd2phlpbj2bk4vy2drlsys1") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.3.0 (c (n "minicov") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0qxm3jknxyp23k39j6c1zy1qfbsgnsg97dmh4h914smq0c5l17bx") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.3.1 (c (n "minicov") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1wdqsxik9kdldj5k52kpsqg3hshsm1rkr9phxrw6f94mm1bw7xfz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.3.2 (c (n "minicov") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "01qdxbrmmwsg06k0v8h265lp8ydz1p4f0rryynd6fv441s786wpa") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-minicov-0.3.3 (c (n "minicov") (v "0.3.3") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0n1qyarx6djjcw3j12s4874lb226msflxinkvmm7vdp1wq9m9y1r") (f (quote (("default" "alloc") ("alloc"))))))

