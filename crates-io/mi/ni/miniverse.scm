(define-module (crates-io mi ni miniverse) #:use-module (crates-io))

(define-public crate-miniverse-0.1.0 (c (n "miniverse") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "15pxmz5zwdw72kbpdah6psqd6g5bd044cks7iijslcg1xi266pr9")))

(define-public crate-miniverse-0.1.1 (c (n "miniverse") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "05zmjf7c6gc79y4ll6rgnghbwd7shs0bwbpmfwdkzazgl869nyy2")))

(define-public crate-miniverse-0.1.2 (c (n "miniverse") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "smooth-bevy-cameras") (r "^0.4.0") (d #t) (k 0)))) (h "1kng2w8126bcc1qg7m9pvymfkyvnv301p05dv6y00r2xwj9mlwpj")))

