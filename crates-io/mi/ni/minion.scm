(define-module (crates-io mi ni minion) #:use-module (crates-io))

(define-public crate-minion-0.1.0 (c (n "minion") (v "0.1.0") (h "13ajgiqjgswss3d0n66svx4m3zv3b4pnyw52bqwrpwirjblra9y3")))

(define-public crate-minion-0.1.1 (c (n "minion") (v "0.1.1") (h "1qjz7p73hzphd3liw4l1pda9z2qqlkc1w7ipljggkgblqkjlvn4r")))

(define-public crate-minion-0.1.2 (c (n "minion") (v "0.1.2") (h "0v8mz1iaa8f41vxmkmzbvkcg3xhjqlvxmwzx0x8wxy5pss6z6ryw")))

(define-public crate-minion-0.1.3 (c (n "minion") (v "0.1.3") (h "0ja1jxiihnagwvywpmj3dq3p237i60sd001knhin1nxn9vrr43a5")))

