(define-module (crates-io mi ni minigps) #:use-module (crates-io))

(define-public crate-minigps-0.1.0 (c (n "minigps") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0x3n8clinrkljc0l5rm72rncymw6r0yb1ki7a0iz6zc94l61dwic")))

(define-public crate-minigps-0.1.1 (c (n "minigps") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gpx") (r "^0.8.6") (d #t) (k 0)))) (h "0vl3dyxnj58d04y97si2w5v16h4iav1agmky9n85nv3ca0b92lri")))

(define-public crate-minigps-0.1.3 (c (n "minigps") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.5") (d #t) (k 0)) (d (n "gpx") (r "^0.9.1") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "10gjhssn626aw39353krgym2afgmimaisl15rx76bhnk7pb0yww5")))

