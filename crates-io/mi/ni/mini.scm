(define-module (crates-io mi ni mini) #:use-module (crates-io))

(define-public crate-mini-0.0.1 (c (n "mini") (v "0.0.1") (h "07y6pmbf3k715hvk4bq05fvzan77w2bf7826dzq2n6xl3d1ql5kb")))

(define-public crate-mini-0.0.2 (c (n "mini") (v "0.0.2") (h "02w4qb0wwxv8y1h5wxfb9ly7xaxjfy76rdln7ak5v32xy98h0xgj") (f (quote (("nightly_test"))))))

(define-public crate-mini-0.0.3 (c (n "mini") (v "0.0.3") (h "1dgpsh9wvny1kxa5z3ijlkqhcq0msy8rqg6ihdf03dnk4mks9879") (f (quote (("nightly_test"))))))

(define-public crate-mini-0.0.4 (c (n "mini") (v "0.0.4") (h "0gqbglz4413ssnfz7skz7gpzzc2prldc0blsbir5zkamry5dvyvy") (f (quote (("nightly_test"))))))

