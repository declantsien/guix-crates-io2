(define-module (crates-io mi ni miniz_oxide) #:use-module (crates-io))

(define-public crate-miniz_oxide-0.1.0 (c (n "miniz_oxide") (v "0.1.0") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "1y34rlfvw3mk4naa5j7fxsv6fdlmxsn5s4w7ijlvha2w0x8f9lyy")))

(define-public crate-miniz_oxide-0.1.1 (c (n "miniz_oxide") (v "0.1.1") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "1ldnmy0lqgja9wcr29pydihcccjk4rd3hlvxr271mlzq6f0qvdqr")))

(define-public crate-miniz_oxide-0.1.2 (c (n "miniz_oxide") (v "0.1.2") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "165k30abqh0q9v20ai02x7zqq3dv43ma4g3xppzqyhhg0ynx78ma")))

(define-public crate-miniz_oxide-0.1.3 (c (n "miniz_oxide") (v "0.1.3") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "1d8fhwx6gmaxdij8d46l1kb5gz1j3jfx9lmw52pfsv4x3hlk194v")))

(define-public crate-miniz_oxide-0.2.0 (c (n "miniz_oxide") (v "0.2.0") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "172h9sqxy0vl8mw13xmmhf80ra02hbcza503ipjws5lw653hmlss")))

(define-public crate-miniz_oxide-0.2.1 (c (n "miniz_oxide") (v "0.2.1") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "07ppnzf6win6qp845zkf8vand2j8z1wr0b5vs2jm3mh7kwvg4s64")))

(define-public crate-miniz_oxide-0.2.2 (c (n "miniz_oxide") (v "0.2.2") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "17f92krv9hhsyc38prpfyn99m2hqhr4fgszpsla66a6gcrnpbhxn")))

(define-public crate-miniz_oxide-0.2.3 (c (n "miniz_oxide") (v "0.2.3") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "1m4c86wv0yr3hyln8d039pgwmzrr8g3rvazprsvhvdqlv4ypnvaz")))

(define-public crate-miniz_oxide-0.3.0 (c (n "miniz_oxide") (v "0.3.0") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "05h5hbslgl3h9cj1q4fy06i4h13ssya8ps3chxfv73m8fkpfsqf0")))

(define-public crate-miniz_oxide-0.3.1 (c (n "miniz_oxide") (v "0.3.1") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "0nyf7qf3p9yxvxy19z9c8mfxlbgx5wjlqi5larx8sykll32mjagy")))

(define-public crate-miniz_oxide-0.3.2 (c (n "miniz_oxide") (v "0.3.2") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "041s41l5w7z8pkp93pdzn8rngxr93q4wxp034pr0cvc7bgway23i")))

(define-public crate-miniz_oxide-0.3.3 (c (n "miniz_oxide") (v "0.3.3") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "1bmanbbcdmssfbgik3fs323g7vljc5wkjz7s61jsbbz2kg0nckrh")))

(define-public crate-miniz_oxide-0.3.4 (c (n "miniz_oxide") (v "0.3.4") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "0ax8a4klp8w2k51lxrsihlnjgqmig0160pd955y2d08dbklbdd4a") (y #t)))

(define-public crate-miniz_oxide-0.3.5 (c (n "miniz_oxide") (v "0.3.5") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)))) (h "09bnfn4bn3hcp912v5syphm4kjd0fdkwq023a4zmr4xf4vvp8gvg")))

(define-public crate-miniz_oxide-0.3.6 (c (n "miniz_oxide") (v "0.3.6") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)))) (h "198n4hfpq0qcxf275l6fpzh7b9cl7ck2xs6pjgpds74bazv9yrxa")))

(define-public crate-miniz_oxide-0.3.7 (c (n "miniz_oxide") (v "0.3.7") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)))) (h "0dblrhgbm0wa8jjl8cjp81akaj36yna92df4z1h9b26n3spal7br")))

(define-public crate-miniz_oxide-0.4.0 (c (n "miniz_oxide") (v "0.4.0") (d (list (d (n "adler") (r "^0.2.1") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0zqwm810ff1c1af0y10rdrimbydd2h0y8020q3izlv0z5j9pa3xy") (f (quote (("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std"))))))

(define-public crate-miniz_oxide-0.4.1 (c (n "miniz_oxide") (v "0.4.1") (d (list (d (n "adler") (r "^0.2.1") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "08mp4c1r3qzxd2gy8ckmnrd1r2zpk3v20cpaxphrf3qdljl5jxad") (f (quote (("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("no_extern_crate_alloc"))))))

(define-public crate-miniz_oxide-0.4.2 (c (n "miniz_oxide") (v "0.4.2") (d (list (d (n "adler") (r "^0.2.1") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1fd7slw7pgy7gvm008y90a1zallcaf1zrn5s8jhl62y16bz0s366") (f (quote (("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("no_extern_crate_alloc"))))))

(define-public crate-miniz_oxide-0.4.3 (c (n "miniz_oxide") (v "0.4.3") (d (list (d (n "adler") (r "^0.2.3") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "17d1xp29v5xgh4vahxld14w1c1hgh38qmxpv7i18wy096gn2cb8g") (f (quote (("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("no_extern_crate_alloc"))))))

(define-public crate-miniz_oxide-0.4.4 (c (n "miniz_oxide") (v "0.4.4") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0jsfv00hl5rmx1nijn59sr9jmjd4rjnjhh4kdjy8d187iklih9d9") (f (quote (("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("no_extern_crate_alloc"))))))

(define-public crate-miniz_oxide-0.5.0 (c (n "miniz_oxide") (v "0.5.0") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1zzb0r513b43xqd5ssy0fw8p6nxn3hhl3b94hcmbsswljm4nfblp") (f (quote (("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std" "simd-adler32/std") ("default"))))))

(define-public crate-miniz_oxide-0.5.1 (c (n "miniz_oxide") (v "0.5.1") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "10phz3ppw4p8pz4rwniy3qkw95wiq64kbvpb0l8kjcrzpka9pcnj") (f (quote (("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std" "simd-adler32/std") ("default"))))))

(define-public crate-miniz_oxide-0.5.3 (c (n "miniz_oxide") (v "0.5.3") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1k1wfxb35v129mhqy14yqhrj3wvknafrwygiq7zvi0m5iml7ap3g") (f (quote (("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default"))))))

(define-public crate-miniz_oxide-0.6.0 (c (n "miniz_oxide") (v "0.6.0") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "0msi1d88ryj8isxlmyrsmjbq6dc9g6f07whm61xkan6f65ibpnqi") (f (quote (("with-alloc") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

(define-public crate-miniz_oxide-0.6.1 (c (n "miniz_oxide") (v "0.6.1") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1l64y7wqihkyk7cxlwwmc367q8sqzwlf4525dd26rj0h3pq8z38j") (f (quote (("with-alloc") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

(define-public crate-miniz_oxide-0.5.4 (c (n "miniz_oxide") (v "0.5.4") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "0d2xcypr8s0skd81dhlrylas1j794qyz74snm11jc8kmy6l0nncn") (f (quote (("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default"))))))

(define-public crate-miniz_oxide-0.6.2 (c (n "miniz_oxide") (v "0.6.2") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1yp8z6yll5ypz1ldmgnv7zi0r78kbvmqmn2mii77jzmk5069axdj") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

(define-public crate-miniz_oxide-0.7.0 (c (n "miniz_oxide") (v "0.7.0") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "0w35s2v9nmz00k0y5ff3h1hqxq0qfp96yacxnh6lisa22j62f2hw") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc")))) (y #t)))

(define-public crate-miniz_oxide-0.6.3 (c (n "miniz_oxide") (v "0.6.3") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1s4lzplqllgw4wv4vdprl5867lhnqv976wlbrbg0ppzpn1i8lgli") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc")))) (y #t)))

(define-public crate-miniz_oxide-0.6.4 (c (n "miniz_oxide") (v "0.6.4") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "08j82769wkgbzfpg0k2qa744w3sg79vx1vsmjw88p1yy5rc15qpj") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc")))) (y #t)))

(define-public crate-miniz_oxide-0.7.1 (c (n "miniz_oxide") (v "0.7.1") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "1ivl3rbbdm53bzscrd01g60l46lz5krl270487d8lhjvwl5hx0g7") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

(define-public crate-miniz_oxide-0.7.2 (c (n "miniz_oxide") (v "0.7.2") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3") (o #t) (k 0)))) (h "19qlxb21s6kabgqq61mk7kd1qk2invyygj076jz6i1gj2lz1z0cx") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

(define-public crate-miniz_oxide-0.7.3 (c (n "miniz_oxide") (v "0.7.3") (d (list (d (n "adler") (r "^1.0") (k 0)) (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "simd-adler32") (r "^0.3.3") (o #t) (k 0)))) (h "1bndap8kj8ihlaz23a5cq0ihc09xh3c1m4ip5dbnpilmw4gx1pw7") (f (quote (("with-alloc") ("std") ("simd" "simd-adler32") ("rustc-dep-of-std" "core" "alloc" "compiler_builtins" "adler/rustc-dep-of-std") ("default" "with-alloc"))))))

