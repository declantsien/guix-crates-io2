(define-module (crates-io mi ni minigrep_learning) #:use-module (crates-io))

(define-public crate-minigrep_learning-0.1.0 (c (n "minigrep_learning") (v "0.1.0") (h "07hhbqpgx84aiw3vbjp7abjrw8ywpr8gds47llnx7yf9lm5h2044") (y #t)))

(define-public crate-minigrep_learning-0.1.1 (c (n "minigrep_learning") (v "0.1.1") (h "0f54s29vkq2hhzgc9bp4czhv1g9i40g1i7x1dcs4sgpsdaxzlk89") (y #t)))

