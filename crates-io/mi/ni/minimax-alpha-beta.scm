(define-module (crates-io mi ni minimax-alpha-beta) #:use-module (crates-io))

(define-public crate-minimax-alpha-beta-0.1.0 (c (n "minimax-alpha-beta") (v "0.1.0") (h "13p0y0y05ppmfx138qmk1714yzq23crh8mlac2638sk897ndihyl")))

(define-public crate-minimax-alpha-beta-0.1.1 (c (n "minimax-alpha-beta") (v "0.1.1") (h "0n3z62n0d4mvk5z9pyjrr1bs1cdqm1jgw58lxg58sld7ydbxzzmx")))

(define-public crate-minimax-alpha-beta-0.1.2 (c (n "minimax-alpha-beta") (v "0.1.2") (h "1gga185p1701g5nyx730lx9ridc6bbg7qy06yybkqc84bvhh8ij2")))

(define-public crate-minimax-alpha-beta-0.1.3 (c (n "minimax-alpha-beta") (v "0.1.3") (h "0xsdvxh36psi8v8yfp4zv3s5zi1a4flh483xf6d1i1bmwsa5087f")))

(define-public crate-minimax-alpha-beta-0.1.4 (c (n "minimax-alpha-beta") (v "0.1.4") (h "1m2d3svyqp56fzqpa2wyb8ynqzbdcd57a4i45yn38ry6z9j3g8c0")))

(define-public crate-minimax-alpha-beta-0.1.5 (c (n "minimax-alpha-beta") (v "0.1.5") (h "02382mxc73216c6kbi8gbgbz7jwzcvchmjkh667v6zy44nxr3l6s")))

(define-public crate-minimax-alpha-beta-0.1.6 (c (n "minimax-alpha-beta") (v "0.1.6") (h "0sma0rmna0cb17jsbc16hadw9khild0rj3qv97b5f49sqmdfjmnp")))

(define-public crate-minimax-alpha-beta-0.1.7 (c (n "minimax-alpha-beta") (v "0.1.7") (h "03kwlc41kymq00fk0zhqh4l2ph7szyypd7qcrbbarw41a13rwgwy")))

(define-public crate-minimax-alpha-beta-0.2.0 (c (n "minimax-alpha-beta") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shakmaty") (r "^0.21.3") (o #t) (d #t) (k 0)))) (h "0a6n9g1bvf853hgn9f4awsnqn9216ardi3jk1x4nprwylxmhaxgn") (f (quote (("tictactoe") ("default" "tictactoe")))) (s 2) (e (quote (("chess" "dep:shakmaty"))))))

