(define-module (crates-io mi ni minidumper-child) #:use-module (crates-io))

(define-public crate-minidumper-child-0.1.0 (c (n "minidumper-child") (v "0.1.0") (d (list (d (n "crash-handler") (r "^0.4") (d #t) (k 0)) (d (n "minidumper") (r "^0.6") (d #t) (k 0)) (d (n "sadness-generator") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j4v8csfy3mpzjwzxck06nqav9qllvjkal9kz8hxsr9xlv0sgxn5")))

(define-public crate-minidumper-child-0.2.0 (c (n "minidumper-child") (v "0.2.0") (d (list (d (n "crash-handler") (r "^0.6") (d #t) (k 0)) (d (n "minidumper") (r "^0.8") (d #t) (k 0)) (d (n "sadness-generator") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jz5lzfpvri3wcpxnfcq7gdj2rjnh9ib33m0chz1y6ya62hgd7a0")))

(define-public crate-minidumper-child-0.2.1 (c (n "minidumper-child") (v "0.2.1") (d (list (d (n "crash-handler") (r "^0.6") (d #t) (k 0)) (d (n "minidumper") (r "^0.8") (d #t) (k 0)) (d (n "sadness-generator") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v4xrs0zrqknnzr5ilb5yy4r0wm7fcznnbzqfqmxcv2lprsizjrv")))

(define-public crate-minidumper-child-0.2.2 (c (n "minidumper-child") (v "0.2.2") (d (list (d (n "crash-handler") (r "^0.6") (d #t) (k 0)) (d (n "minidumper") (r "^0.8") (d #t) (k 0)) (d (n "sadness-generator") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l94cg4i76hfsyghxxlbjjjrqm7z08w4v26z9pj6ggjxhczz5i6p")))

