(define-module (crates-io mi ni minifb_wrapper) #:use-module (crates-io))

(define-public crate-minifb_wrapper-0.1.0 (c (n "minifb_wrapper") (v "0.1.0") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "0cixic9l19hb6jvp980ym2292x48jaz3jr0a85iwv4b6brs83v96") (y #t)))

(define-public crate-minifb_wrapper-0.1.1 (c (n "minifb_wrapper") (v "0.1.1") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "1131az5jf339jqxzzgn067i4f16dmdr7h4apc23y7sn1x5wzgyr0") (y #t)))

(define-public crate-minifb_wrapper-0.1.2 (c (n "minifb_wrapper") (v "0.1.2") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "11xcrsaisqfi9ykqwnf878a6dd1naaf8ds4vxzm1kdlaa1bzkv06")))

(define-public crate-minifb_wrapper-0.1.3 (c (n "minifb_wrapper") (v "0.1.3") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "0svqy82v50fsrdbrhip9qbwj1wp4daa5f6w9c87y10qc87jvn0f2")))

