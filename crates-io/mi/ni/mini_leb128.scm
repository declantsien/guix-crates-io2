(define-module (crates-io mi ni mini_leb128) #:use-module (crates-io))

(define-public crate-mini_leb128-0.1.0 (c (n "mini_leb128") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "zc_io") (r "^0.1") (k 0)))) (h "063qqh37yjm74d8qradrjbcpk69wlc5np3v6z9fvvlnraxin3gm8")))

(define-public crate-mini_leb128-0.1.1 (c (n "mini_leb128") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "zc_io") (r "^0.2") (k 0)))) (h "1r3k00bilzzg4n1zbd6mq6h7vkhir8jcqh7z7jqhb7qcd0f5xng6")))

