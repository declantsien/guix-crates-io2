(define-module (crates-io mi ni minigames) #:use-module (crates-io))

(define-public crate-minigames-0.0.0 (c (n "minigames") (v "0.0.0") (h "0lpr67b7yclsm0d9si7hx8i7bnfcdximcam9mkgg0fmgjb0d0637")))

(define-public crate-minigames-0.0.1 (c (n "minigames") (v "0.0.1") (h "0lyv1pjh1ms8d5037bjp8j5jpmsvb4ln9n6gigdqznp0ciw2p5in")))

