(define-module (crates-io mi ni minigrep_necimye) #:use-module (crates-io))

(define-public crate-minigrep_necimye-0.1.0 (c (n "minigrep_necimye") (v "0.1.0") (d (list (d (n "rust_programming_book") (r "^0.1.1") (d #t) (k 0)))) (h "00sz4qrphp3b8hlrnrz5h0crkdpva1r6y4zjgnap8fcnwhjd7w6z")))

(define-public crate-minigrep_necimye-0.1.1 (c (n "minigrep_necimye") (v "0.1.1") (d (list (d (n "rust_programming_book") (r "^0.1.1") (d #t) (k 0)))) (h "07y1cara1pvlbc9wsmx02wx2hyrrfa72dvx4d2qsqbv991cf0a1l")))

