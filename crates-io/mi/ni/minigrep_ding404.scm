(define-module (crates-io mi ni minigrep_ding404) #:use-module (crates-io))

(define-public crate-minigrep_ding404-0.1.0 (c (n "minigrep_ding404") (v "0.1.0") (h "0r0nnbc3i7k5qb7cdldr7f6zvbj4k8f8j3r4mnyyamsxs5kbhba5")))

(define-public crate-minigrep_ding404-0.1.1 (c (n "minigrep_ding404") (v "0.1.1") (h "02sb6pybfn67n9i9ayhsvxzyv3vyx5rbv9bafy3kpn49pbcly5ms")))

