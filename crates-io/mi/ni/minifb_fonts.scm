(define-module (crates-io mi ni minifb_fonts) #:use-module (crates-io))

(define-public crate-minifb_fonts-0.1.0 (c (n "minifb_fonts") (v "0.1.0") (d (list (d (n "minifb") (r "^0.25.0") (d #t) (k 2)))) (h "1pg9n6hd1551zkj0r0d24rlavs02b59h7s252bpsc3yviw5zcn8a") (y #t)))

(define-public crate-minifb_fonts-0.1.1 (c (n "minifb_fonts") (v "0.1.1") (d (list (d (n "minifb") (r "^0.25.0") (d #t) (k 2)))) (h "01qdvfl4bx2lz6fhi8iacjbm63lxm1xzn8zpb9wa6p2ad4ibwcy3") (y #t)))

(define-public crate-minifb_fonts-0.1.3 (c (n "minifb_fonts") (v "0.1.3") (d (list (d (n "minifb") (r "^0.25.0") (d #t) (k 2)))) (h "13i05jcz8n3qzd3p7dgd825n7glyanrfnhibjnjmplhmwb5v32ab")))

