(define-module (crates-io mi ni minimult_cortex-m) #:use-module (crates-io))

(define-public crate-minimult_cortex-m-0.1.0 (c (n "minimult_cortex-m") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)))) (h "01dpgg5nl2xrszcd14wd9d4ara64fhc8gzav00mg0lfdvp9piljf")))

(define-public crate-minimult_cortex-m-0.2.0 (c (n "minimult_cortex-m") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)))) (h "1b4hqpy3n11qi88ilmjrqw5p5iqrf1hflkz4wwy9xxrkn87nadg4")))

(define-public crate-minimult_cortex-m-0.2.1 (c (n "minimult_cortex-m") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)))) (h "0kfab9ysvxvb8z01zmjysbsx3gcbdd6rz5qi6f3xr4ndacf4vms1")))

(define-public crate-minimult_cortex-m-0.3.0 (c (n "minimult_cortex-m") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)))) (h "0yg7636189sgdnlvzj9qsds75lys21zjgg6pjs1b342jr8chcp0g")))

(define-public crate-minimult_cortex-m-0.3.1 (c (n "minimult_cortex-m") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.41") (k 0)))) (h "1aspl9a51i0km9sx6sm08mcmmdw4nhss1l0xnprnq71xfyh278mx")))

(define-public crate-minimult_cortex-m-0.3.2 (c (n "minimult_cortex-m") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (k 0)) (d (n "panic-semihosting") (r "^0.5.3") (f (quote ("exit"))) (d #t) (k 2)))) (h "0dp1pvhmb07k5lja9n85537miv2vlwjzd1m126vi3agy3lr4y8wp")))

(define-public crate-minimult_cortex-m-0.3.3 (c (n "minimult_cortex-m") (v "0.3.3") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (k 0)) (d (n "panic-semihosting") (r "^0.5.3") (f (quote ("exit"))) (d #t) (k 2)))) (h "0qr4fh5p4nkib5sxzcrvm1k0w4p1ib79b37pvzil75wc6wrvaslf")))

