(define-module (crates-io mi ni mini_paste) #:use-module (crates-io))

(define-public crate-mini_paste-0.1.10 (c (n "mini_paste") (v "0.1.10") (d (list (d (n "proc_macro") (r "^0.1.10") (d #t) (k 0) (p "mini_paste-proc_macro")))) (h "1jakins4l7jpq9jz0hkb84yqjklss7hv9801a1mqi5gi8plfi7j2") (f (quote (("nightly"))))))

(define-public crate-mini_paste-0.1.11 (c (n "mini_paste") (v "0.1.11") (d (list (d (n "proc_macro") (r "^0.1.11") (d #t) (k 0) (p "mini_paste-proc_macro")))) (h "0qrbx1kdxqmffavpcckzznk0n0jrpsbdvi6g4jzp0hl3v5xrnjfj") (f (quote (("nightly"))))))

