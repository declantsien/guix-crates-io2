(define-module (crates-io mi ni minigrepzzz) #:use-module (crates-io))

(define-public crate-minigrepzzz-0.1.0 (c (n "minigrepzzz") (v "0.1.0") (h "1nj5khnx65lp6y5yp6nnq81d2a8h17a6yjd78vnixiiqc7hqwmwj")))

(define-public crate-minigrepzzz-0.1.1 (c (n "minigrepzzz") (v "0.1.1") (h "1hvgxjxgcqvv9g2r33453m3pk1davazlh5dk93cpxfiah61l2mnv")))

