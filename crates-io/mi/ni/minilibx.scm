(define-module (crates-io mi ni minilibx) #:use-module (crates-io))

(define-public crate-minilibx-0.1.0 (c (n "minilibx") (v "0.1.0") (h "0950ns8xvr6vlf32z6wc8brcgkbhp8855nj1xn8jxspj961ad1il")))

(define-public crate-minilibx-0.1.1 (c (n "minilibx") (v "0.1.1") (h "1fv97amz2g4s8w8pxivl77h7wvhhsbxsavmwlfggkcnf6j10jp6l")))

(define-public crate-minilibx-0.2.0 (c (n "minilibx") (v "0.2.0") (h "0xs5akvp06gjwcbmck7cs36428jsc4r8rs6rg4q6fqydz46f6kq7")))

(define-public crate-minilibx-0.2.1 (c (n "minilibx") (v "0.2.1") (h "112hpcxjhkny5dibs6qj1nvgc7n1a9capd5p0jr5180h7bpiv4jk")))

