(define-module (crates-io mi ni miniorm-macros) #:use-module (crates-io))

(define-public crate-miniorm-macros-0.1.0 (c (n "miniorm-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "14129jykjqyvwpij20vv9rbg4x4i31v7am31fg9ppj64f56v528n")))

(define-public crate-miniorm-macros-0.2.0 (c (n "miniorm-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1zlp59zj0fpys7mnzmjzxkd83fa2anyv5y3d1qrdiw4037pbxpwp")))

(define-public crate-miniorm-macros-0.3.0 (c (n "miniorm-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0nyk386j87hzb79nq93f7zcq56fn9mxblmikr87q28snibndj6ri")))

(define-public crate-miniorm-macros-0.4.0 (c (n "miniorm-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("sqlite" "postgres"))) (d #t) (k 2)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1fyrkwbf234w2f67h24p2x05m7n199ndqzgj628swd5kpa1k682b")))

