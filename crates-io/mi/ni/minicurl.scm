(define-module (crates-io mi ni minicurl) #:use-module (crates-io))

(define-public crate-minicurl-0.1.0 (c (n "minicurl") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "blocking" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jbnmqajcy8xw03hllkzbh9kzh6rlkh2az5ncn54h3wvbq340l5f")))

