(define-module (crates-io mi ni miniproj) #:use-module (crates-io))

(define-public crate-miniproj-0.1.0 (c (n "miniproj") (v "0.1.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.1.1") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "0q9gw1f6c4ldiparnxxj5sh3rxk04cn8l8ipxiis993y0i6lqmik")))

(define-public crate-miniproj-0.1.1 (c (n "miniproj") (v "0.1.1") (d (list (d (n "miniproj-epsg-registry") (r "^0.1.1") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "00fbk7dr18ny7m5j58s3jiqzx4avq19mlc955mpyfdbhmkpi61mp")))

(define-public crate-miniproj-0.2.0 (c (n "miniproj") (v "0.2.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.2.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.2.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "1sb4l85iklxb27d6dlkw6kx2lydb4g7ykk3bbsxl2m03shwcqpi9")))

(define-public crate-miniproj-0.3.0 (c (n "miniproj") (v "0.3.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.3.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "0p6n1fblpiy15dvd2744q2a17ja029jaxm4fl50l23zd5llyhcwv")))

(define-public crate-miniproj-0.4.0 (c (n "miniproj") (v "0.4.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.4.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.4.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "1paf8rccg7npcm12hi244gxm2yw1y4ssybgd8w70hy3pggrl4c69")))

(define-public crate-miniproj-0.5.0 (c (n "miniproj") (v "0.5.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.5.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.5.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "0wlibzfcn3bqbjj7c41al0pg8a654dwrd2lb37ky6irrd299j2gb")))

(define-public crate-miniproj-0.6.0 (c (n "miniproj") (v "0.6.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.6.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "0g8d94hlzn4l76zc7g87w2c4rjiis1pikm32793vab9mvlrm5a55")))

(define-public crate-miniproj-0.7.0 (c (n "miniproj") (v "0.7.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.7.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.7.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "1manm69nr3ccjhwr5grg7vxzsrc37f0476w26jxypb37kgccrj0y")))

(define-public crate-miniproj-0.7.1 (c (n "miniproj") (v "0.7.1") (d (list (d (n "miniproj-epsg-registry") (r "^0.7.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.7.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "19f6prmf091p25shb18scklrb4yksd5gsvb206lilx1pvk67v2z7")))

(define-public crate-miniproj-0.8.0 (c (n "miniproj") (v "0.8.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.8.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.8.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "16a5zcbskzh7jnam7kg87zqad2pw0bd44hhmzidx6y9g632232rh")))

(define-public crate-miniproj-0.9.0 (c (n "miniproj") (v "0.9.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.9.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.9.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "1j11169x06ngsx8nb9vb08p8fg22dhggv8wqkwyzzwpl8b4346w7")))

(define-public crate-miniproj-0.10.0 (c (n "miniproj") (v "0.10.0") (d (list (d (n "miniproj-epsg-registry") (r "^0.10.0") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.10.0") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "17y8g46y7pj5laljzjwhv9wjxkwh6g5783ddjs4yz3vxynkgnj7x")))

(define-public crate-miniproj-0.10.1 (c (n "miniproj") (v "0.10.1") (d (list (d (n "miniproj-epsg-registry") (r "^0.10.1") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "~0.11.1") (k 0)))) (h "0fl16rjrqr0az679rdpkr2ddvdrfwmjrxfpb1qpf3zgd1fd5nvbb")))

(define-public crate-miniproj-0.10.2 (c (n "miniproj") (v "0.10.2") (d (list (d (n "miniproj-epsg-registry") (r "^0.10.2") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "~0.11.2") (k 0)))) (h "1r3bi89fi9nnwaqfl56i0fnxfgvckljvqczihrf93ql33fnljg7s") (y #t)))

(define-public crate-miniproj-0.10.3 (c (n "miniproj") (v "0.10.3") (d (list (d (n "miniproj-epsg-registry") (r "^0.10.2") (d #t) (k 1)) (d (n "miniproj-ops") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "~0.11.2") (k 0)))) (h "0mpq0isiqjy98kzw32fi4pckbhvs24fbxf6h4vpiyvjd8s2pf7mi")))

