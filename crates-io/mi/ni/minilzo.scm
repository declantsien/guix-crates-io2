(define-module (crates-io mi ni minilzo) #:use-module (crates-io))

(define-public crate-minilzo-0.1.0 (c (n "minilzo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "minilzo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "101s18kngwqxx9dn4nm1dvgrk9p3n8zj1af5fr958rj34ln5lkbv")))

(define-public crate-minilzo-0.1.1 (c (n "minilzo") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "minilzo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0fkcqf57k47zbvhyarvpaglwpjljf0nn7sfsn1zq1nr4b54krd6k")))

(define-public crate-minilzo-0.1.2 (c (n "minilzo") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "minilzo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1a8x4zps7pwd95vldx5wsp8ap8hrcnw637c103qgmiqv44ms4wgf")))

(define-public crate-minilzo-0.2.0 (c (n "minilzo") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "minilzo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1x1hk7rhqhwdl87xcqgg78g0dpiislkklk19h980srvg18ghfjar")))

