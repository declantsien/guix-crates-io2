(define-module (crates-io mi ni minify_selectors_utils) #:use-module (crates-io))

(define-public crate-minify_selectors_utils-0.2.0 (c (n "minify_selectors_utils") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.3.3") (d #t) (k 0)))) (h "19l4aw1agnwvr6f72v7bj4084dpcd13cdsid99a9klacvxikyab6")))

(define-public crate-minify_selectors_utils-2.1.3 (c (n "minify_selectors_utils") (v "2.1.3") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "005rp995avz4szwcc36fpk3vgax8zvdh244925ki15d1ymp7hlnz")))

(define-public crate-minify_selectors_utils-2.3.3 (c (n "minify_selectors_utils") (v "2.3.3") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "181abip1gxwhbsn0646jkyvsyx29swwq82cyh6zzwkb7hd4gq83v")))

(define-public crate-minify_selectors_utils-2.4.0 (c (n "minify_selectors_utils") (v "2.4.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "08cvh4lr6g4lr461ndb2r5rmg6r3y126kd9fyhz7mb3p1ilzgpjd")))

(define-public crate-minify_selectors_utils-2.6.0 (c (n "minify_selectors_utils") (v "2.6.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encode_selector") (r "^0.4.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i1wp8rg9xar9wj8bwbcr79zjnqhis97vhndmsl2czddxp4dg3a2")))

