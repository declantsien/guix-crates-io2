(define-module (crates-io mi ni mini2react) #:use-module (crates-io))

(define-public crate-mini2react-0.1.0 (c (n "mini2react") (v "0.1.0") (h "0inrcfv1qjhc9cpzjzy238ylzpz60phpcrshnrzryg3kdd0ia97l")))

(define-public crate-mini2react-0.1.1 (c (n "mini2react") (v "0.1.1") (h "17bqlx3nmh8pdzj2hm3nc6rfp90rgsk41s205mg3lyw239cf74iv")))

