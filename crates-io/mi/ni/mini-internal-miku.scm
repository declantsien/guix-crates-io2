(define-module (crates-io mi ni mini-internal-miku) #:use-module (crates-io))

(define-public crate-mini-internal-miku-0.1.23 (c (n "mini-internal-miku") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p8s2sjmmm72fx3pmjyncg03pgf4h3cmmby9chrhf4c0hwfpij8m") (r "1.36")))

