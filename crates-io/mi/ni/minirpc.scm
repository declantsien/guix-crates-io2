(define-module (crates-io mi ni minirpc) #:use-module (crates-io))

(define-public crate-minirpc-0.1.0 (c (n "minirpc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1f3czvrvakf2mrc7il94gsv02456ii30sdrkj0crd5s9km7gagi1")))

(define-public crate-minirpc-0.1.1 (c (n "minirpc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1vc19464m0qb8anxfivrqd7wr2y6d1rkafjs1gxr0ld6gr9ig2qs")))

