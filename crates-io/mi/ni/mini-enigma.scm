(define-module (crates-io mi ni mini-enigma) #:use-module (crates-io))

(define-public crate-mini-enigma-0.1.0 (c (n "mini-enigma") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "02mbv9xpjzpbjfl81bw8q6lp2m9qi6xhqcj6izc3s4h8yyygff6q")))

(define-public crate-mini-enigma-0.2.0 (c (n "mini-enigma") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "1ba96mddv8b5a6ni31mfypgahlw46dr0rr6rrlkf07s2cmn1yjgh") (f (quote (("M4"))))))

(define-public crate-mini-enigma-0.3.0 (c (n "mini-enigma") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "1h52724wj40967z15mfvlm7dhhi27m05qbd8sibyl3iq32d6y6qv")))

