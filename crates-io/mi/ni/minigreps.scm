(define-module (crates-io mi ni minigreps) #:use-module (crates-io))

(define-public crate-minigreps-0.1.0 (c (n "minigreps") (v "0.1.0") (h "0qc04kfbx9mcr4lh2sdxlrzmslyz8ds6jqp0djf48zhljc96ny1n") (y #t)))

(define-public crate-minigreps-0.1.1 (c (n "minigreps") (v "0.1.1") (h "08dipbr99fydgl1k2vd5wp1dx5sg06b5g5qaiqjsw2j54ss715pr")))

