(define-module (crates-io mi ni minigrep_sopesto) #:use-module (crates-io))

(define-public crate-minigrep_sopesto-0.1.0 (c (n "minigrep_sopesto") (v "0.1.0") (h "02slag1yvbp9iym4l0ddqnn2xkjxa7ilrc2ga1gj2wxjyn1xlvg2")))

(define-public crate-minigrep_sopesto-0.1.1 (c (n "minigrep_sopesto") (v "0.1.1") (h "1cr2iza7nmpy26dbwbp4c674459ljwbcjqf0fwi5r3i4q0fdn4yy")))

