(define-module (crates-io mi ni mini-backtrace) #:use-module (crates-io))

(define-public crate-mini-backtrace-0.1.0 (c (n "mini-backtrace") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1lmchd8m5zpd3156nvyiz5rxqffdbw290wkwcm0sqxy9v1clbvcx")))

(define-public crate-mini-backtrace-0.1.1 (c (n "mini-backtrace") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0882mpaiq6za54wxrx6g5dr58gqvx60p6py890ipnj4a6virzy0q")))

(define-public crate-mini-backtrace-0.1.2 (c (n "mini-backtrace") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "05ldhh509xfsc4bzqdxz0v3hkw669l652bqvdq9v4bbj9zsd94jz")))

(define-public crate-mini-backtrace-0.1.3 (c (n "mini-backtrace") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0i2js15ip1v0291k7mksw999scwsri64f9njljm6gfdl12p8b74c")))

(define-public crate-mini-backtrace-0.1.4 (c (n "mini-backtrace") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1ifp76dg5jxhz5p13vmcmxf0a8353kwba3bdbd71kss0y576bf97")))

