(define-module (crates-io mi ni minibar) #:use-module (crates-io))

(define-public crate-minibar-0.1.0 (c (n "minibar") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1b39b2pq8a9bhdypihw7ck1x776z4ycihjg5nl5icy292ysbfcji")))

