(define-module (crates-io mi ni minigrep_desonglll) #:use-module (crates-io))

(define-public crate-minigrep_desonglll-0.1.0 (c (n "minigrep_desonglll") (v "0.1.0") (h "1jwa9rz32rhyrfjvdnk3ipahgzlmrcqw3gjfffybrcva32sfqkza")))

(define-public crate-minigrep_desonglll-0.1.1 (c (n "minigrep_desonglll") (v "0.1.1") (h "1663f8a1a7n2ss347n6iwazp38kvfpvnfa9cgzjwnv9001nvx2gl")))

(define-public crate-minigrep_desonglll-0.1.2 (c (n "minigrep_desonglll") (v "0.1.2") (h "0qpfxsn00nrdx6lh2gjpcy50d34655ywp3wa5nlwvd4rycffanh2")))

