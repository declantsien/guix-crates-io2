(define-module (crates-io mi ni minimum) #:use-module (crates-io))

(define-public crate-minimum-0.1.0 (c (n "minimum") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mopa") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "135rvpa7wbr78dyiqwfk5pq1xr1fngqzb6il9qmmafkr3dfbn8yc")))

