(define-module (crates-io mi ni mini-c-ares-src) #:use-module (crates-io))

(define-public crate-mini-c-ares-src-0.1.0 (c (n "mini-c-ares-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "09qz91w334zmi3c3l47zmzbnmm0xpjahjvrqd62j9yrbvcn3flvh")))

(define-public crate-mini-c-ares-src-0.2.0 (c (n "mini-c-ares-src") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "18nfqrilbp473s8y6a8pz21mprhk14173d6xpipzzxcz03zn9pm0")))

(define-public crate-mini-c-ares-src-0.2.1 (c (n "mini-c-ares-src") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0qg7kr710mz9nh8l1cfd9d4rf50igl4m7rb6xzfx7ys9nnknnf1w")))

