(define-module (crates-io mi ni mini-c-ares-sys) #:use-module (crates-io))

(define-public crate-mini-c-ares-sys-0.1.0 (c (n "mini-c-ares-sys") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.3") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0fhsjrqb23y2pjdwicifrh9rmz8il9q2rkm7cpgfhpzqyg2ws8ds") (l "cares")))

(define-public crate-mini-c-ares-sys-0.2.0 (c (n "mini-c-ares-sys") (v "0.2.0") (d (list (d (n "c-ares-src") (r "^0.1") (o #t) (d #t) (k 1) (p "mini-c-ares-src")) (d (n "jni-sys") (r "^0.3") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0j432dajccdf3jscay017avsr9ik6sf7p8kf4ph5ysk4pmj52smb") (f (quote (("vendored" "c-ares-src") ("default")))) (l "cares")))

(define-public crate-mini-c-ares-sys-0.2.1 (c (n "mini-c-ares-sys") (v "0.2.1") (d (list (d (n "c-ares-src") (r "^0.2") (o #t) (d #t) (k 1) (p "mini-c-ares-src")) (d (n "jni-sys") (r "^0.3") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0jqg7ybn5ra9nk5kpqmc955dwv4dl15m4lgy5phkpfhq7dnrmd4v") (f (quote (("vendored" "c-ares-src") ("default")))) (l "cares")))

(define-public crate-mini-c-ares-sys-0.2.2 (c (n "mini-c-ares-sys") (v "0.2.2") (d (list (d (n "c-ares-src") (r "^0.2") (o #t) (d #t) (k 1) (p "mini-c-ares-src")) (d (n "jni-sys") (r "^0.4") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1d7x57d80brvzf7z6dqcnjnvxi8mcmi6w118w333r9fz0f6friqi") (f (quote (("vendored" "c-ares-src") ("default")))) (l "cares")))

(define-public crate-mini-c-ares-sys-0.2.3 (c (n "mini-c-ares-sys") (v "0.2.3") (d (list (d (n "c-ares-src") (r "^0.2") (o #t) (d #t) (k 1) (p "mini-c-ares-src")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1krapg6bgmp9vnbkmk1rby3k5jkd68p7alvrym1vj931nwibwgqd") (f (quote (("vendored" "c-ares-src") ("default")))) (l "cares")))

