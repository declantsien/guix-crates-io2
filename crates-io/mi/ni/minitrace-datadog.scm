(define-module (crates-io mi ni minitrace-datadog) #:use-module (crates-io))

(define-public crate-minitrace-datadog-0.3.0 (c (n "minitrace-datadog") (v "0.3.0") (d (list (d (n "minitrace") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jb7h233rma2nx12hdg4631q92qifsv7xjcllirrhxz4ldiyzvq0")))

(define-public crate-minitrace-datadog-0.3.1 (c (n "minitrace-datadog") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "02rjslyx3g4y44xszhrv7ip2r5yjmh1aqzmcl8c4jd383zz27jg8")))

(define-public crate-minitrace-datadog-0.4.0 (c (n "minitrace-datadog") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "05b0x8z2y84jzqfyc5qa3y94wg2cnc6l1p56zhlvca8i1h6ysgi7")))

(define-public crate-minitrace-datadog-0.4.1 (c (n "minitrace-datadog") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fsxlbp4ixl3gvm71hygnvnkjqfiynrph0qh7299a7aravhhziah")))

(define-public crate-minitrace-datadog-0.5.0 (c (n "minitrace-datadog") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y32kiflqvmsb0079vdzywlviizbnjqiw6zm0ipdb1sqch5ngjhi")))

(define-public crate-minitrace-datadog-0.5.1 (c (n "minitrace-datadog") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aqyxjn0bj4z3i3vk5zcxr0ha7w2v4qx10pac8q5c4f3pyrnzf39")))

(define-public crate-minitrace-datadog-0.6.0 (c (n "minitrace-datadog") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p73ghpviwy69gjjrrplsv2iim61mr5jg72w7pbh91lj20is96g0")))

(define-public crate-minitrace-datadog-0.6.1 (c (n "minitrace-datadog") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.6.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nhnd1nhv90ljmqj7zmjxxwd87iapaksi0zhfdf7cq1x760xmvix")))

(define-public crate-minitrace-datadog-0.6.2 (c (n "minitrace-datadog") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1krssb2md2z8mqlrz0wi53pw0vl1hbk0fj4m4jpi5hw9rzd2krgq")))

(define-public crate-minitrace-datadog-0.6.3 (c (n "minitrace-datadog") (v "0.6.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.6.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hw1d7wwkpphcd8qmvahpqzqk6n0l5nlar8lkkzq5xzwyfqahfh5")))

(define-public crate-minitrace-datadog-0.6.4 (c (n "minitrace-datadog") (v "0.6.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08767x2a4zisric9vd8wplfsils1y24j4wqqc7a3h047jb50204p")))

(define-public crate-minitrace-datadog-0.6.5 (c (n "minitrace-datadog") (v "0.6.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "117l1qk07vk6qlzwwp1wxr3hy67abnzngnqw9x23sdhrasbzs202")))

