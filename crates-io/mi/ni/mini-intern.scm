(define-module (crates-io mi ni mini-intern) #:use-module (crates-io))

(define-public crate-mini-intern-1.0.0 (c (n "mini-intern") (v "1.0.0") (h "0y39kfsfgvjpnq67sn27jjx7g62w2bgxkq6x4510zhn8wykh86wz")))

(define-public crate-mini-intern-1.1.0 (c (n "mini-intern") (v "1.1.0") (h "1yfgn3k69n31a3zmdhjb9g1wy285sdmpycgq468q665ppbrc7vc0")))

