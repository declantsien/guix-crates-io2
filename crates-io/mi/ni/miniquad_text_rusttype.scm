(define-module (crates-io mi ni miniquad_text_rusttype) #:use-module (crates-io))

(define-public crate-miniquad_text_rusttype-0.1.1 (c (n "miniquad_text_rusttype") (v "0.1.1") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 2)) (d (n "miniquad") (r "0.2.*") (o #t) (d #t) (k 0)) (d (n "rusttype") (r "^0.7.7") (d #t) (k 0)))) (h "0gwhfw0hilp4fk0svrni1v221wlhab97xss147bsqknzdp91iwzz") (f (quote (("render" "miniquad") ("default" "render"))))))

(define-public crate-miniquad_text_rusttype-0.1.2 (c (n "miniquad_text_rusttype") (v "0.1.2") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 2)) (d (n "miniquad") (r "0.2.*") (o #t) (d #t) (k 0)) (d (n "rusttype") (r "^0.7.7") (d #t) (k 0)))) (h "19xfdh68jw483f7rlib593b8csksbaclyih9h8i438j3ph6vs8rj") (f (quote (("render" "miniquad") ("default" "render"))))))

