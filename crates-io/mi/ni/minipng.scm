(define-module (crates-io mi ni minipng) #:use-module (crates-io))

(define-public crate-minipng-0.1.0 (c (n "minipng") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "png-decoder") (r "^0.1.1") (d #t) (k 2)))) (h "0ddvjy9lhb0d4vd4rh0kwl6rd3625d61as1d01qq1ynmq1x2lrg3") (f (quote (("default") ("adler"))))))

(define-public crate-minipng-0.1.1 (c (n "minipng") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "png-decoder") (r "^0.1.1") (d #t) (k 2)))) (h "1a3n6y1y6xvf8ai7aivrslrac02jxm7rcrpxp158mk6vycjgiaj4") (f (quote (("default") ("adler"))))))

