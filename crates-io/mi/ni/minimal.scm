(define-module (crates-io mi ni minimal) #:use-module (crates-io))

(define-public crate-Minimal-0.1.0 (c (n "Minimal") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Document" "HtmlElement" "Window" "NodeList" "Element"))) (d #t) (k 0)))) (h "1jhdmfhadq23q6xa8ymx5839v19r2x48m5i7bfhw9wdi3iypgp3s") (y #t)))

(define-public crate-Minimal-0.1.1 (c (n "Minimal") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Document" "HtmlElement" "Window" "NodeList" "Element"))) (d #t) (k 0)))) (h "0c48pfcq49i72202rh9qmbcp4g0xb336jabj4q6r1v73asmgns5r") (y #t)))

(define-public crate-Minimal-0.1.2 (c (n "Minimal") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Document" "HtmlElement" "Window" "NodeList" "Element" "ShadowRoot" "HtmlSlotElement" "Attr" "MouseEvent" "DomRect" "CssStyleDeclaration" "Location"))) (d #t) (k 0)))) (h "1kl7fzyri91g49vmw8ll1rwjr2c2qx8nfgd38xgzgzr9h9sv8479") (y #t)))

(define-public crate-Minimal-0.1.3 (c (n "Minimal") (v "0.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Document" "HtmlElement" "Window" "NodeList" "Element" "ShadowRoot" "HtmlSlotElement" "Attr" "MouseEvent" "DomRect" "CssStyleDeclaration" "Location" "Node"))) (d #t) (k 0)))) (h "0afcsf1afm87cqias9zcqs28x4z8piybhysxzsn7151lyks223sp") (y #t)))

(define-public crate-Minimal-0.1.4 (c (n "Minimal") (v "0.1.4") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "1pzwffpk063ribb6vkkd1yj2kliqgvglrrrmnv83lhmdkgldb76f") (y #t)))

(define-public crate-Minimal-0.1.5 (c (n "Minimal") (v "0.1.5") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "04csgyak1nhqp2rqqqscn3wm5ykgaxcv7zqnxcv5lkd4hkaxqgya") (y #t)))

(define-public crate-Minimal-0.1.6 (c (n "Minimal") (v "0.1.6") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "1jbz6vd0zwbz40fk7ga050lzg8y3lyazxk051p15zhw37gcxkfm9") (y #t)))

