(define-module (crates-io mi ni minipac) #:use-module (crates-io))

(define-public crate-minipac-0.1.1 (c (n "minipac") (v "0.1.1") (d (list (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "kiss-tnc") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "02qzysznvf3kkg39x5a8svc2sl358z35flxw6ndjakzl0h7jk8x3")))

(define-public crate-minipac-0.1.2 (c (n "minipac") (v "0.1.2") (d (list (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "kiss-tnc") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1j1r2q5279i85ahg58k63w12ba2a6hh7imjhf341m3j4fc0pf6a3")))

(define-public crate-minipac-0.2.0 (c (n "minipac") (v "0.2.0") (d (list (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "kiss-tnc") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1j4a086l960szxlajhiyhdjvq9paz83gh5p440k8zzvsb0q37gyl")))

(define-public crate-minipac-0.3.0 (c (n "minipac") (v "0.3.0") (d (list (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "kiss-tnc") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync" "tracing"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 2)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0b0d9an6kr8pzl612mppb1v8saizgqcy9vrmx5qpcyg8ls3rcliq")))

(define-public crate-minipac-0.3.1 (c (n "minipac") (v "0.3.1") (d (list (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "kiss-tnc") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync" "tracing" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 2)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0ajda9fbcdyns7ylqn5zriraqbz6s6m5rnb65p47xcnqkmzvxm98")))

