(define-module (crates-io mi ni mini-c-ares-resolver) #:use-module (crates-io))

(define-public crate-mini-c-ares-resolver-0.1.0 (c (n "mini-c-ares-resolver") (v "0.1.0") (d (list (d (n "c-ares") (r "^0.1.0") (d #t) (k 0) (p "mini-c-ares")) (d (n "futures-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.9") (d #t) (k 2)) (d (n "polling") (r "^2.0.1") (d #t) (k 0)))) (h "1c0myv6kbdyb81rw3k5dkzpa7ssxp1pcrha6qcj0jgx8bli83c0w") (f (quote (("default"))))))

(define-public crate-mini-c-ares-resolver-0.2.0 (c (n "mini-c-ares-resolver") (v "0.2.0") (d (list (d (n "c-ares") (r "^0.2.0") (d #t) (k 0) (p "mini-c-ares")) (d (n "futures-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.9") (d #t) (k 2)) (d (n "polling") (r "^2.0.1") (d #t) (k 0)))) (h "17q29qxx4h4qp45hmvyv32g9cq2qchfmd9vw3yxmp0abfsijsmx3") (f (quote (("vendored" "c-ares/vendored") ("default"))))))

(define-public crate-mini-c-ares-resolver-0.2.1 (c (n "mini-c-ares-resolver") (v "0.2.1") (d (list (d (n "c-ares") (r "^0.2.0") (d #t) (k 0) (p "mini-c-ares")) (d (n "futures-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.9") (d #t) (k 2)) (d (n "polling") (r "^3.0.0") (d #t) (k 0)))) (h "1ijg04vd0zqaqyys4alz9jpg7nxxpnq8gsnq9a1054lvlczm3l7f") (f (quote (("vendored" "c-ares/vendored") ("default"))))))

(define-public crate-mini-c-ares-resolver-0.2.2 (c (n "mini-c-ares-resolver") (v "0.2.2") (d (list (d (n "c-ares") (r "^0.2.0") (d #t) (k 0) (p "mini-c-ares")) (d (n "futures-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.9") (d #t) (k 2)) (d (n "polling") (r "^3.1.0") (d #t) (k 0)))) (h "1hyfqn4c5101cllirikpn1yz1zsdnm6w16r7arc9ffa7iyj6wn69") (f (quote (("vendored" "c-ares/vendored") ("default"))))))

