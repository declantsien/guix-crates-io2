(define-module (crates-io mi ni minigrepf) #:use-module (crates-io))

(define-public crate-minigrepf-0.1.0 (c (n "minigrepf") (v "0.1.0") (h "03asxlxs7rcjp29j58ajrw07iznd4wq300ikbx8zahp87by8q9z7")))

(define-public crate-minigrepf-0.1.1 (c (n "minigrepf") (v "0.1.1") (h "02mrnvfjlk7jwcyjk1q768nff7698rz24y1jdn936szg12j4pvm5")))

(define-public crate-minigrepf-0.1.3 (c (n "minigrepf") (v "0.1.3") (h "0rdxqk64rc0cb70865fliasfdrbg7srn9xi2ribhapd0qj7g6h6h")))

