(define-module (crates-io mi ni minimal-socks5) #:use-module (crates-io))

(define-public crate-minimal-socks5-0.1.0 (c (n "minimal-socks5") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util" "net" "time" "macros" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("parking_lot" "env-filter"))) (d #t) (k 0)))) (h "19bhn94hag2yizwfxpzdqw5vprknbffpxmrlh03aclrpcjnfjp18") (r "1.72")))

(define-public crate-minimal-socks5-0.1.1 (c (n "minimal-socks5") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util" "net" "time" "macros" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("parking_lot" "env-filter"))) (d #t) (k 0)))) (h "0yc89nvzql6j522i3waz1lixazjl0ky0al09ram4fsqqwszk76fp") (r "1.72")))

