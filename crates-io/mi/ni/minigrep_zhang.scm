(define-module (crates-io mi ni minigrep_zhang) #:use-module (crates-io))

(define-public crate-minigrep_zhang-0.1.0 (c (n "minigrep_zhang") (v "0.1.0") (h "0djqrk52wwsi3pq54b85fpbkw6fqfbhj7ha1mv8wxambfnxniqh8") (y #t)))

(define-public crate-minigrep_zhang-0.1.1 (c (n "minigrep_zhang") (v "0.1.1") (h "0ykw2m2xm50nx23n1vaadsbihvw0901xc917gx3f14p26j54qjrm") (y #t)))

(define-public crate-minigrep_zhang-0.1.2 (c (n "minigrep_zhang") (v "0.1.2") (h "0qkiggk0j12gnk6b2ysi2953sa16khfc331dlzb9qrpssx0nckw3") (y #t)))

(define-public crate-minigrep_zhang-0.1.3 (c (n "minigrep_zhang") (v "0.1.3") (h "1vyj2g1s13lvc65jx08qyd212k9sd1213i4hv50aqjb0cqmxsvar") (y #t)))

