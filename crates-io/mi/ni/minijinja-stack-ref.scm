(define-module (crates-io mi ni minijinja-stack-ref) #:use-module (crates-io))

(define-public crate-minijinja-stack-ref-0.27.0 (c (n "minijinja-stack-ref") (v "0.27.0") (d (list (d (n "minijinja") (r "^0.27.0") (k 0)))) (h "04qlzzfb3rwxjx6b6gyp33cq3y2iqcpljhzi733225p8djk7hnah") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.28.0 (c (n "minijinja-stack-ref") (v "0.28.0") (d (list (d (n "minijinja") (r "^0.28.0") (k 0)))) (h "0bif1azgn5by96sixq6lvg1yph0scwmzqlr7534w2xq665dz3rcc") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.29.0 (c (n "minijinja-stack-ref") (v "0.29.0") (d (list (d (n "minijinja") (r "^0.29.0") (k 0)))) (h "17kjcg354d0v9fkb9zvwdnznnyjivamcpxh90jw7v20m0820i9xh") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.0 (c (n "minijinja-stack-ref") (v "0.30.0") (d (list (d (n "minijinja") (r "^0.30.0") (k 0)))) (h "0q48bwnz8zzyz6xyiypfybmxcidppxkbrpvb2xv1i0mmgl8bgama") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.1 (c (n "minijinja-stack-ref") (v "0.30.1") (d (list (d (n "minijinja") (r "^0.30.1") (k 0)))) (h "19pd85bs34i91bsqssajnkq93g05699f17xlvcpaz8cw3rf7hyli") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.2 (c (n "minijinja-stack-ref") (v "0.30.2") (d (list (d (n "minijinja") (r "^0.30.2") (k 0)))) (h "1gknqh1mdjsj18nwn1mc7r4dk00a66ybx2xymqr2n6n3c79l7v5j") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.3 (c (n "minijinja-stack-ref") (v "0.30.3") (d (list (d (n "minijinja") (r "^0.30.3") (k 0)))) (h "0kwhqal92wn0snp24mk3yh5jn24y6vsm9fs030ljfhyr3g3bjclh") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.4 (c (n "minijinja-stack-ref") (v "0.30.4") (d (list (d (n "minijinja") (r "^0.30.4") (k 0)))) (h "1hdh2cy45l7v4alchzvhk7l7zaffpj461710f06ybsnd0l5i71hf") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.5 (c (n "minijinja-stack-ref") (v "0.30.5") (d (list (d (n "minijinja") (r "^0.30.5") (k 0)))) (h "1d943jj09gphsbs49lxcjd3yd731qd10nzyfww4k46nd0aqk5m93") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.6 (c (n "minijinja-stack-ref") (v "0.30.6") (d (list (d (n "minijinja") (r "^0.30.6") (k 0)))) (h "1lfhi53ia4d0fi4w3p28nfk30va9miwyg0whyvrkkaglbgydw4ks") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.30.7 (c (n "minijinja-stack-ref") (v "0.30.7") (d (list (d (n "minijinja") (r "^0.30.7") (k 0)))) (h "1k9nwd06h76vzmdyb3pra15wxnaic9bdpjq00nk30vnj91jdmdks") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.31.0 (c (n "minijinja-stack-ref") (v "0.31.0") (d (list (d (n "minijinja") (r "^0.31.0") (k 0)))) (h "1dqk727plyd56xa0dsdgpaxvmdwdmvl0ffjmaqi5k42bzl9jcrw3") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.31.1 (c (n "minijinja-stack-ref") (v "0.31.1") (d (list (d (n "minijinja") (r "^0.31.1") (k 0)))) (h "0h97iiq2qgnzabbwr7diah1jjdra0ya2rn3zl1v2wzya2ljr5knz") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.32.0 (c (n "minijinja-stack-ref") (v "0.32.0") (d (list (d (n "minijinja") (r "^0.32.0") (k 0)))) (h "0ir2lnv6srhg2qpggmvhrga49ldk22ska9cmsa2sm8l8w9j0ka91") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.32.1 (c (n "minijinja-stack-ref") (v "0.32.1") (d (list (d (n "minijinja") (r "^0.32.1") (k 0)))) (h "0azgrhd7xvzk9llh60mi43xj5hqvh18yajaisjmhy7yfs06vbf1i") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.33.0 (c (n "minijinja-stack-ref") (v "0.33.0") (d (list (d (n "minijinja") (r "^0.33.0") (k 0)))) (h "18hfkw2ypi40w9lsgigvy74iyz9x1ibs0ry8lp7f0z41x7z03rly") (r "1.61")))

(define-public crate-minijinja-stack-ref-0.34.0 (c (n "minijinja-stack-ref") (v "0.34.0") (d (list (d (n "minijinja") (r "^0.34.0") (k 0)))) (h "1mrm379ivgh2pld2ljj8xbkl8gacsx6amy6w943fxwh2l19rcv3h") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.0-alpha.1 (c (n "minijinja-stack-ref") (v "1.0.0-alpha.1") (d (list (d (n "minijinja") (r "^1.0.0-alpha.1") (k 0)))) (h "1x2inqlqdzgmd49p76cdf97qslq5dsxvafbsfhg4nc8h70jzfzlz") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.0-alpha.2 (c (n "minijinja-stack-ref") (v "1.0.0-alpha.2") (d (list (d (n "minijinja") (r "^1.0.0-alpha.2") (k 0)))) (h "1z401ws26x35lpznrf548pycg82mxz5pd7r7vzzcxlc2k365fnhb") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.0-alpha.3 (c (n "minijinja-stack-ref") (v "1.0.0-alpha.3") (d (list (d (n "minijinja") (r "^1.0.0-alpha.3") (k 0)))) (h "01g448mnyhmy45xq72fq52fi5p4iarn1b5zhaddpk9b4qifhlhlx") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.0-alpha.4 (c (n "minijinja-stack-ref") (v "1.0.0-alpha.4") (d (list (d (n "minijinja") (r "^1.0.0-alpha.4") (k 0)))) (h "0yghhavpimssqv67c9ddm6b52w9ii0ih5hnww95v11rdfnmdlbhf") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.0 (c (n "minijinja-stack-ref") (v "1.0.0") (d (list (d (n "minijinja") (r "^1.0.0") (k 0)))) (h "0i447lh0hi5yigpsfbxija55vli7y4y4s3wdnihgqblwnvmh0n9g") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.1 (c (n "minijinja-stack-ref") (v "1.0.1") (d (list (d (n "minijinja") (r "^1.0.1") (k 0)))) (h "0smvc1mmff97v8nxqiv119dlzia0nbzcybs2znh6ac66jvvrnxbd") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.2 (c (n "minijinja-stack-ref") (v "1.0.2") (d (list (d (n "minijinja") (r "^1.0.2") (k 0)))) (h "10znbaqkx400czv7c04cgw0cic32zqgmz8a5wa9ixvh8s5x7mz14") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.3 (c (n "minijinja-stack-ref") (v "1.0.3") (d (list (d (n "minijinja") (r "^1.0.3") (k 0)))) (h "1xvzj4vh7zl49g6m0xax9qjshd4ic5gng9myfalvqnhnqpr04zkv") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.4 (c (n "minijinja-stack-ref") (v "1.0.4") (d (list (d (n "minijinja") (r "^1.0.4") (k 0)))) (h "0aqsharwx7rn11mzpz2gq673ycfws2a3bswgjgfbkdgz5k6684f7") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.5 (c (n "minijinja-stack-ref") (v "1.0.5") (d (list (d (n "minijinja") (r "^1.0.5") (k 0)))) (h "0whl7f6xa1zj9swx0xl6vhpc70zr6s7hf1w9a61473vj8cbxn5jx") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.6 (c (n "minijinja-stack-ref") (v "1.0.6") (d (list (d (n "minijinja") (r "^1.0.6") (k 0)))) (h "1mvrmwz14adxp6pphjcknqhnvm37ym7sp762r4q2ppvkpisbcciv") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.7 (c (n "minijinja-stack-ref") (v "1.0.7") (d (list (d (n "minijinja") (r "^1.0.7") (k 0)))) (h "14idfv3srlgwcylkgrvkxz5nj0v23jamfkap0d01d08n27qw65fq") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.8 (c (n "minijinja-stack-ref") (v "1.0.8") (d (list (d (n "minijinja") (r "^1.0.8") (k 0)))) (h "0wb8a99ms5hrqq2rs08f7gjyd30k5fhcdwark4asf72hjgf9bndm") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.9 (c (n "minijinja-stack-ref") (v "1.0.9") (d (list (d (n "minijinja") (r "^1.0.9") (k 0)))) (h "0gzrmyy07f6gawmr9nvx0mys5ww6wl5vy461xclhgnp5iqph8sfy") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.10 (c (n "minijinja-stack-ref") (v "1.0.10") (d (list (d (n "minijinja") (r "^1.0.10") (k 0)))) (h "0yw8q3d0jv5pcg96gwpn1ii7ajksxl0dkfql2q2kaapgcaqis1bc") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.11 (c (n "minijinja-stack-ref") (v "1.0.11") (d (list (d (n "minijinja") (r "^1.0.11") (k 0)))) (h "19zajm8f7vphwk6mkgwazisr0ds11gc14mq0m9njyb5qz4zjnicj") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.12 (c (n "minijinja-stack-ref") (v "1.0.12") (d (list (d (n "minijinja") (r "^1.0.12") (k 0)))) (h "13yvyc0pxn8xwf3sp7lan41b6c49i02wn50i734l33arq045dvrp") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.13 (c (n "minijinja-stack-ref") (v "1.0.13") (d (list (d (n "minijinja") (r "^1.0.13") (k 0)))) (h "1izd2f88aggcbjjbfciykk2l87h08qvbyjbqg5ki4z2glz8pbzg7") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.14 (c (n "minijinja-stack-ref") (v "1.0.14") (d (list (d (n "minijinja") (r "^1.0.14") (k 0)))) (h "1qixkrw1g34mgvgrqcj5pdqcc6viab98hcnmphqrav3zhws967m9") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.15 (c (n "minijinja-stack-ref") (v "1.0.15") (d (list (d (n "minijinja") (r "^1.0.15") (k 0)))) (h "0zrc3ccdqj79jfgn33487g9wbk419iwxssdf96kisj0xzv1rkpy8") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.16 (c (n "minijinja-stack-ref") (v "1.0.16") (d (list (d (n "minijinja") (r "^1.0.16") (k 0)))) (h "0xg1s7h90j32g85v979dzvkkhv5nx1qb4q74bfqz0x40baxddfxh") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.17 (c (n "minijinja-stack-ref") (v "1.0.17") (d (list (d (n "minijinja") (r "^1.0.17") (k 0)))) (h "062v7wrgynrpjjd6aabgkxvfrrspnpy82rxwhi5xbkfql0x5qw4m") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.18 (c (n "minijinja-stack-ref") (v "1.0.18") (d (list (d (n "minijinja") (r "^1.0.18") (k 0)))) (h "1vy527745nhi81dl51zxka2d2ckiyb7jxk8pj41cindy9rfarmy9") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.20 (c (n "minijinja-stack-ref") (v "1.0.20") (d (list (d (n "minijinja") (r "^1.0.20") (k 0)))) (h "179llf4fclb4k3a5sz6p24gcf5w4s9glbvhm4d9g63x7zq9ag122") (r "1.61")))

(define-public crate-minijinja-stack-ref-1.0.21 (c (n "minijinja-stack-ref") (v "1.0.21") (d (list (d (n "minijinja") (r "^1.0.21") (k 0)))) (h "0jgx8csdzp9dnjrq50ssjhnm1bjaaj3ch4mrvg9iwqbjz5yw9ysb") (r "1.61")))

