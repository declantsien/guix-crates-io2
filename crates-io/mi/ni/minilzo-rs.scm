(define-module (crates-io mi ni minilzo-rs) #:use-module (crates-io))

(define-public crate-minilzo-rs-0.1.0 (c (n "minilzo-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0zp20pjxqiaklwigbf8xfj9z8alv29xrri6d944g1yr7k81x5yvr") (y #t)))

(define-public crate-minilzo-rs-0.2.0 (c (n "minilzo-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0lah9iwbipxw8p2433jsi1vwf1yafqsbnwbhjnx8lks64by84y8i") (y #t)))

(define-public crate-minilzo-rs-0.2.1 (c (n "minilzo-rs") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1c31diw19i7cbp4if0qxyvn8rhid6jwf47zgjj4ckq3waaifmsf9") (y #t)))

(define-public crate-minilzo-rs-0.3.0 (c (n "minilzo-rs") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0a5s45xspg3li7si140wxwm2pfxiwhmi29jis1n6hgricchynlkd")))

(define-public crate-minilzo-rs-0.5.0 (c (n "minilzo-rs") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "12ldprnjyp78xmqrfzmd5q44xqr4sw3may1ng2jly46w831gv3k5")))

(define-public crate-minilzo-rs-0.6.0 (c (n "minilzo-rs") (v "0.6.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1z3i24ckk45gc3rbpxamgyv91nbg36d5ilrp0yznllcprlksbkxi")))

(define-public crate-minilzo-rs-0.6.1 (c (n "minilzo-rs") (v "0.6.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "02ffrwgragi4cfp0c1q02qp2bn7ik2lsflbfjjki4xvxnnj1j6n7")))

