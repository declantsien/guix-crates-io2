(define-module (crates-io mi ni mini-prop) #:use-module (crates-io))

(define-public crate-mini-prop-0.1.0 (c (n "mini-prop") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prop_tune") (r "^0.1") (d #t) (k 0)))) (h "0r8yardd52ylvs8wjrarabf75r0w6fs7pdxgcjmwfrdwhjmca2if")))

(define-public crate-mini-prop-0.1.1 (c (n "mini-prop") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prop_tune") (r "^0.1") (d #t) (k 0)))) (h "0mgfgk6gin79c8q4kw7qa4blis52nwpyh40vh1m9b46ac757n96c")))

(define-public crate-mini-prop-0.2.0 (c (n "mini-prop") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prop_tune") (r "^0.2") (d #t) (k 0)))) (h "1x13vsw0jvkz1a91wc8kkhmx1k0vn1b38rdg4zxsxiqj51jxx0r6")))

