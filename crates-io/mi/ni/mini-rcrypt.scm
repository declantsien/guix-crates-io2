(define-module (crates-io mi ni mini-rcrypt) #:use-module (crates-io))

(define-public crate-mini-rcrypt-0.1.0 (c (n "mini-rcrypt") (v "0.1.0") (h "1spkxbinr5lyp8dy30iga47n76yhlkm3f773im19nb1mky43d399")))

(define-public crate-mini-rcrypt-0.1.1 (c (n "mini-rcrypt") (v "0.1.1") (h "1d79nmkircpp5zsld11570n7s7582m16f4w7vybwzlhmw9pflp0j")))

