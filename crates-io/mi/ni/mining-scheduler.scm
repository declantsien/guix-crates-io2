(define-module (crates-io mi ni mining-scheduler) #:use-module (crates-io))

(define-public crate-mining-scheduler-0.1.0 (c (n "mining-scheduler") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06n62mrcpb7qjy49xbgcaifxnmndal8275kikbx9hrd1a41f0i22") (y #t)))

(define-public crate-mining-scheduler-0.2.0 (c (n "mining-scheduler") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "00kwrv3a5f5jv47hd1g3rrpyapyplfiv9n9vgmrdphs6q4f0m1qm")))

