(define-module (crates-io mi ni minigrep_improved) #:use-module (crates-io))

(define-public crate-minigrep_improved-0.1.0 (c (n "minigrep_improved") (v "0.1.0") (h "162xn9g2p34h7v9hfylspglgwlxzzbdr6vw2841fm0dczvl96ml8")))

(define-public crate-minigrep_improved-0.1.1 (c (n "minigrep_improved") (v "0.1.1") (h "1mad4lmwhsqa8d47wzxfkpg1sqrmcbdpyhwpgg4x4ybhw1pxi5lh")))

(define-public crate-minigrep_improved-0.1.2 (c (n "minigrep_improved") (v "0.1.2") (h "0wpl422db5jr6zn3jkv8jspypdwyrq2818sb03kqlc7z5dq2z9hi")))

