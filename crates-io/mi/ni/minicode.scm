(define-module (crates-io mi ni minicode) #:use-module (crates-io))

(define-public crate-minicode-1.4.3 (c (n "minicode") (v "1.4.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "self_update") (r "^0.39.0") (f (quote ("archive-tar" "archive-zip" "compression-flate2"))) (d #t) (k 0)))) (h "1r89jfsnhblxxk4481babxqzgw5c0xq8p0czyjpfr0ll584llbv9")))

