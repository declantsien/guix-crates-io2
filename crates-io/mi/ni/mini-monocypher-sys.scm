(define-module (crates-io mi ni mini-monocypher-sys) #:use-module (crates-io))

(define-public crate-mini-monocypher-sys-0.1.0 (c (n "mini-monocypher-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "05mw2s13hf7illm8r3x45jr580r0fg7c6kravdxqsaax4wxksz2v")))

(define-public crate-mini-monocypher-sys-0.1.1 (c (n "mini-monocypher-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "093qpg629ha80vh715v31810l8yky1j1msbkb82jcks1jwgbsrcr")))

