(define-module (crates-io mi ni minigltf) #:use-module (crates-io))

(define-public crate-minigltf-0.1.0 (c (n "minigltf") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zql01q8ix8pxw77i0p86ipc4nzjb2hpaidprd8ylnxlvwmipilf")))

