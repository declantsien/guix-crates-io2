(define-module (crates-io mi ni minimal-executor) #:use-module (crates-io))

(define-public crate-minimal-executor-0.1.0 (c (n "minimal-executor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1wz5a6m79wpi15rv36ai3881h8mrdb68kaprmny5gk34wn8mhf6l")))

(define-public crate-minimal-executor-0.1.1 (c (n "minimal-executor") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "04py10fzh19y6p74z0d9blhrwi0jsh38m660j1i6inh2z9s48gy8")))

(define-public crate-minimal-executor-0.1.2 (c (n "minimal-executor") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0gjnsbnanawyy8w7ahl83zxx5wnff7gkfd49ll7cfh3w1pl850hx")))

(define-public crate-minimal-executor-0.1.3 (c (n "minimal-executor") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "02vd8v6s6ylwzcgj4arq3ablf4lhglpkbkwa3i27aprq5vga13db")))

(define-public crate-minimal-executor-0.1.4 (c (n "minimal-executor") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "04dr9f0za8gf2q8mpkg98dynqjcwnfmj090ylxv2w99y4ywvxxxa")))

(define-public crate-minimal-executor-0.1.5 (c (n "minimal-executor") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1r2fyjzqhwpl184dh8fg9l80y2js1m5g5d1s1zhs8xvx1z826fi0")))

(define-public crate-minimal-executor-0.1.6 (c (n "minimal-executor") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0sjjc8656ayc7rb3cpv92vf669y93m10r9aykygh0p87lq0k9n5i")))

(define-public crate-minimal-executor-0.1.7 (c (n "minimal-executor") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0h3rlw5r1bwg4f9pcshbypdbh7lh9ijay9mhj6nwsvfkzv4n041l")))

(define-public crate-minimal-executor-0.1.8 (c (n "minimal-executor") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1xn548vh84g8gpvknd5m0ms2snmms7cwwpvckbqnddhqrskpzfhn")))

(define-public crate-minimal-executor-0.1.9 (c (n "minimal-executor") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1j2hjd8m3by2lqs09jlgm9fdfg0zw6a6hfbywxrhzlij1hdp2zmk")))

(define-public crate-minimal-executor-0.2.0 (c (n "minimal-executor") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "07z7jlfmk7423vzcnspv5bjm131ybxffmsz57lq8pkqz9byfq24l")))

(define-public crate-minimal-executor-0.3.0 (c (n "minimal-executor") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("alloc" "crossbeam-channel"))) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)))) (h "191iikbbmcqv9836111zcp16rcnjgqp2c5r023l5gyc9shih31b9") (f (quote (("std" "crossbeam/std") ("default" "std"))))))

(define-public crate-minimal-executor-0.4.0 (c (n "minimal-executor") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("alloc" "crossbeam-channel"))) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "kanal") (r "^0.1.0-pre7") (d #t) (k 0)))) (h "1k8qd2x38gw7jya8w8aqky9wg2sixgkj63lxyc67m1nhaigxbgr2")))

(define-public crate-minimal-executor-0.4.1 (c (n "minimal-executor") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("alloc" "crossbeam-channel"))) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "kanal") (r "^0.1.0-pre7") (d #t) (k 0)))) (h "1rn9c762p9l9q0b4hvfwma31dlq90iqw247027vl5m8s1kvw0rh9")))

