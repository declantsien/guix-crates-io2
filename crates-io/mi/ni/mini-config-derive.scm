(define-module (crates-io mi ni mini-config-derive) #:use-module (crates-io))

(define-public crate-mini-config-derive-0.1.0 (c (n "mini-config-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "146hsd0jchymc4apr59dwcs9s44ld7gbqrsrys3gxvz2fvmac98k")))

(define-public crate-mini-config-derive-0.1.1 (c (n "mini-config-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1jhdqkndmz8f3xqdj1vf97i7njyifwwpplrm42f25xhj1nfazv9i")))

(define-public crate-mini-config-derive-0.1.2 (c (n "mini-config-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1q6v900rsjd3cx714mx9gql7zjnhb49q96pim2jidlgs06894041")))

