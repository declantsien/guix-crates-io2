(define-module (crates-io mi ni mini_gl_fb) #:use-module (crates-io))

(define-public crate-mini_gl_fb-0.2.4 (c (n "mini_gl_fb") (v "0.2.4") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0p7k7xnx6yrmj7n4jvn5ywd5sbk8k138wqx85ldapifjp5r6zylf")))

(define-public crate-mini_gl_fb-0.3.2 (c (n "mini_gl_fb") (v "0.3.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "1mrrpm01wpils8an4gnbj9wbn8zs9l7m6chnzncrrbxk0yycanz2")))

(define-public crate-mini_gl_fb-0.4.0 (c (n "mini_gl_fb") (v "0.4.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0h54m2lmq9x6cadkhndg1ssc66g7razbh05al5hs3rx8rvm0c9qy")))

(define-public crate-mini_gl_fb-0.4.1 (c (n "mini_gl_fb") (v "0.4.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0ma2dmp5973pvijma0n72xv674nzp5dg358h0qp7pyapirwn9x5b")))

(define-public crate-mini_gl_fb-0.5.0 (c (n "mini_gl_fb") (v "0.5.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0g9p0gpxh3xldcf1q0v5rj2dbkf1k6xaz7x49854p0lshf1i28gh")))

(define-public crate-mini_gl_fb-0.5.1 (c (n "mini_gl_fb") (v "0.5.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "04l0jjc9cj4mapggfilrmvwzfa422fz62pq1fk3v1353iyjfb2di")))

(define-public crate-mini_gl_fb-0.6.0 (c (n "mini_gl_fb") (v "0.6.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "1vci7rpwp1lja05r0qihmgwiyl1gagp01w8ri8kwvcllqhsrv2vr")))

(define-public crate-mini_gl_fb-0.7.0 (c (n "mini_gl_fb") (v "0.7.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.19.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0kfz9c6rb6wqilwy539m119zfgx81m3hapnf7kg7ma4mw4ggp8s1")))

(define-public crate-mini_gl_fb-0.8.0 (c (n "mini_gl_fb") (v "0.8.0") (d (list (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.26.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "0r5xzi0lly5jl0pwwz94npfbr34xyar6564q6dawmv80k9vl7bbz")))

(define-public crate-mini_gl_fb-0.9.0 (c (n "mini_gl_fb") (v "0.9.0") (d (list (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.26.0") (d #t) (k 0)) (d (n "rustic_gl") (r "^0.3.2") (d #t) (k 0)))) (h "16a3ky6vjfwn590js8idi8n36kap0rky6hsiqs688ffh0nzlljwb")))

