(define-module (crates-io mi ni minigrepr) #:use-module (crates-io))

(define-public crate-minigrepr-0.1.1 (c (n "minigrepr") (v "0.1.1") (h "0zrpidrmhn6sh67l9ms8nm8r5nkmhrd10cfs2pav7db42r7sk8lf")))

(define-public crate-minigrepr-0.1.2 (c (n "minigrepr") (v "0.1.2") (h "03677gxxvqcmzk646mbpq5cz2wwnb93dv9karzbhgzdvff9xl5b9")))

(define-public crate-minigrepr-0.1.3 (c (n "minigrepr") (v "0.1.3") (h "1337k4dp4v6zhj8w01gdbx5lg69hd4mgnc0rqdmabj7nfmjgn101")))

