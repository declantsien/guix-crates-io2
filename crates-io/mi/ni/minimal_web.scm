(define-module (crates-io mi ni minimal_web) #:use-module (crates-io))

(define-public crate-minimal_web-0.1.0 (c (n "minimal_web") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "0dibkk4cxik9syl35x5c227msssc5iim40bbfqg9441vkgljvb8r")))

(define-public crate-minimal_web-0.1.1 (c (n "minimal_web") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "043v6b10hn3iiczx5i67wqw1zf0kh6crj52fg1kv4k4pl2r4aclv")))

(define-public crate-minimal_web-0.1.2 (c (n "minimal_web") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Location" "Attr" "Document" "Element" "HtmlElement" "HtmlSlotElement" "NodeList" "Window" "Node" "CssStyleDeclaration" "DomRect"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "0z46d2lqvh9mjjq4kr38iajnjyqg32a98b87xq7vqpq5mk63wa5k")))

