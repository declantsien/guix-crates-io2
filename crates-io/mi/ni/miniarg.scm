(define-module (crates-io mi ni miniarg) #:use-module (crates-io))

(define-public crate-miniarg-0.1.0 (c (n "miniarg") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "miniarg_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0g13qrxhnf9dd530kw803ss1kmn67r3ngw9i3bhyj0addn9bxxi3") (f (quote (("std") ("derive" "miniarg_derive") ("default" "std") ("alloc"))))))

(define-public crate-miniarg-0.2.0 (c (n "miniarg") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "miniarg_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0flylrqnzpky07kzan37g7kqpdnsq9l32xn3qb0m4abwm4v749sb") (f (quote (("std" "alloc") ("derive" "miniarg_derive") ("default" "std") ("alloc"))))))

(define-public crate-miniarg-0.3.0 (c (n "miniarg") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "miniarg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0115287agn5b7p3wyqdqncwbkzb20jgfwp4c1jwnlf124scbvip6") (f (quote (("std" "alloc") ("derive" "miniarg_derive") ("default" "std") ("alloc"))))))

(define-public crate-miniarg-0.3.1 (c (n "miniarg") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "miniarg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0lz8kxhj597nj331s85av91wi52w0xnyd4akw7f8zwh6pp7aw2lz") (f (quote (("std" "alloc") ("derive" "miniarg_derive") ("default" "std") ("alloc"))))))

