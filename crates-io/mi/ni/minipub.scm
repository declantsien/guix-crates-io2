(define-module (crates-io mi ni minipub) #:use-module (crates-io))

(define-public crate-minipub-0.1.0 (c (n "minipub") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "minijinja") (r "^1.0.10") (f (quote ("loader" "builtins" "debug"))) (d #t) (k 0)) (d (n "minijinja-contrib") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (f (quote ("compression"))) (k 0)))) (h "03k8f2492b00bg67yjrhlkk24iwz7h2s9likg9b6j0hq66gczhv3")))

