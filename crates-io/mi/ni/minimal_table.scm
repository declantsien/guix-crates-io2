(define-module (crates-io mi ni minimal_table) #:use-module (crates-io))

(define-public crate-minimal_table-0.1.0 (c (n "minimal_table") (v "0.1.0") (h "0y2h95pqlqmp059x9sqqwmwhdccrrx56b12j3275yq6smgsfmya7") (y #t)))

(define-public crate-minimal_table-0.2.0 (c (n "minimal_table") (v "0.2.0") (h "16hk57w7wqqd98y7hghq05biac3zgib09aig39lm2a0r0n7amq8h")))

