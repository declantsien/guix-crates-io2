(define-module (crates-io mi ni minimp3) #:use-module (crates-io))

(define-public crate-minimp3-0.1.0 (c (n "minimp3") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0iag5vx1laklw7254rbjiky19lzzspvmmvxxvjw5q84s15hym408")))

(define-public crate-minimp3-0.2.0 (c (n "minimp3") (v "0.2.0") (d (list (d (n "minimp3-sys") (r "^0.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.8") (d #t) (k 0)))) (h "0p19szjs1digx8rhhk6yahwk0jwnkqjn67js6javpw6ysi9lvngv")))

(define-public crate-minimp3-0.3.0 (c (n "minimp3") (v "0.3.0") (d (list (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.8") (d #t) (k 0)))) (h "1ggp81kla4amwkpxasbpbcgprji5825fz84rnbmlrir1h30n5lws")))

(define-public crate-minimp3-0.3.1 (c (n "minimp3") (v "0.3.1") (d (list (d (n "minimp3-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.16") (d #t) (k 0)))) (h "0inbwczij98lpqbvhlysfqiabjwxrcaikinj4harnjy868jhdll3")))

(define-public crate-minimp3-0.3.2 (c (n "minimp3") (v "0.3.2") (d (list (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.16") (d #t) (k 0)))) (h "0i42lpda3r6g58lj28wxqm4f1c8552m5n51gwkw7xwbdqkfrc5pl")))

(define-public crate-minimp3-0.3.3 (c (n "minimp3") (v "0.3.3") (d (list (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2.0") (d #t) (k 0)))) (h "0fbj07klf2b2dpsxs103yrrqyvyzwbhfwfcrl1q50346avnrnbjl")))

(define-public crate-minimp3-0.3.4 (c (n "minimp3") (v "0.3.4") (d (list (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2.4") (d #t) (k 0)))) (h "0f5wq982mlr8rn282wraz13lzb78lkjdjmw8sa2zyacpdahzlhwr")))

(define-public crate-minimp3-0.3.5 (c (n "minimp3") (v "0.3.5") (d (list (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)))) (h "1mfj2vg9vx0gvn5qhlsyqifccfynvnxij21mnavgilxzl3vczq6w")))

(define-public crate-minimp3-0.4.0 (c (n "minimp3") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0l8gv4gjdarwyncmll778xj1fjhjc7s21sb7ww10q5xp164wyx67") (f (quote (("default") ("async_tokio" "tokio"))))))

(define-public crate-minimp3-0.5.0 (c (n "minimp3") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1nrqszxbrlakzra8ljicsgkzccrv2skdgdarn7d2ycimb72si14n") (f (quote (("default") ("async_tokio" "tokio"))))))

(define-public crate-minimp3-0.5.1 (c (n "minimp3") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "minimp3-sys") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wj3nzj1swnvwsk3a4a3hkfj1d21jsi7babi40wlrxzbbzvkhm4q") (f (quote (("default") ("async_tokio" "tokio"))))))

