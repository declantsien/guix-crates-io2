(define-module (crates-io mi ni mini-io-queue) #:use-module (crates-io))

(define-public crate-mini-io-queue-0.1.0 (c (n "mini-io-queue") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "1apfzivwnjxxm6dw7zyhisgjcayiq02sgdrfjsy6p01826aybvjh") (f (quote (("std-io" "std" "futures/std") ("std") ("stack-buffer" "array-init") ("nonblocking" "alloc") ("heap-buffer" "alloc") ("default" "blocking" "nonblocking" "heap-buffer" "std-io") ("blocking" "std") ("asyncio" "futures" "alloc") ("alloc"))))))

(define-public crate-mini-io-queue-0.2.0 (c (n "mini-io-queue") (v "0.2.0") (d (list (d (n "array-init") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "1ihd1qnqkdnmpn2ip04zdxjck42kxxahar8ylxx0pmj9hdvwcaqn") (f (quote (("std-io" "std" "futures/std") ("std") ("stack-buffer" "array-init") ("nonblocking" "alloc") ("heap-buffer" "alloc") ("default" "blocking" "nonblocking" "heap-buffer" "std-io") ("blocking" "std") ("asyncio" "futures" "alloc") ("alloc"))))))

