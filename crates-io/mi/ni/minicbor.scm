(define-module (crates-io mi ni minicbor) #:use-module (crates-io))

(define-public crate-minicbor-0.1.0 (c (n "minicbor") (v "0.1.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (k 0)))) (h "0ap3ia88svrn0i15x51gnvfd38dx7cy12f9zab1nzhasj3gcysdm") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.1.1 (c (n "minicbor") (v "0.1.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (k 0)))) (h "0fvhka51240ij53kk014g805bxrmf76b9d7fyk2pxjpznkf73swk") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.2.0 (c (n "minicbor") (v "0.2.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0g7330klfcd2xm8pqp3r749xk7s760q6a743vr6fyqw20limi6hm") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.3.0 (c (n "minicbor") (v "0.3.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0nn1xx1cyk57ncamhns4k5v6wcyhxd80svxnmx23iyszx2vss2a7") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.4.0 (c (n "minicbor") (v "0.4.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "10vhc6nnb8rb1i0wlzgdwpp30xc8bnxsyhhlzcdpl535wflip99b") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.4.1 (c (n "minicbor") (v "0.4.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1r4iw7fyczpkxndyvsm4kslnil632zp7r3vz6gh5jipsybsma806") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.5.0 (c (n "minicbor") (v "0.5.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0zbm4g0whmww0lrrvr5s9hx62rhnp2w5fjibl7b9q9gkkygwhzr8") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.5.1 (c (n "minicbor") (v "0.5.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0hfhbhj0xk5s32zx4rzgwgjclbiljrpmmzx5jiqxnj7mz3b3mh1g") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.6.0 (c (n "minicbor") (v "0.6.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "10d7iw0ir28pwqdn21r88jhaig9p2dfzl1l2bn6529lphsmgcbls") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.7.0 (c (n "minicbor") (v "0.7.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0bg13zms25ckx6nhm4zvkp6japfmgq2mn1s26y64bcbi2w6ijr01") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.7.1 (c (n "minicbor") (v "0.7.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0arky3c1j3lvi1qhx33ljp6j2lzz1ap5di7r3vw2ddqb47ssjr9j") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.7.2 (c (n "minicbor") (v "0.7.2") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1sm960d09j7nvm8sqb7qr35iy1ymfhs2nzllafmwq3v4z5rjqaqw") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.8.0 (c (n "minicbor") (v "0.8.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1ncv3l1axv0y916awz7sw3fwja8amj824fl3f5myqiglp55cwyga") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.8.1 (c (n "minicbor") (v "0.8.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "03fahp9aa4jk6alpza0q4cdsv07q0ysjg8lnlnn5sh92raq5paji") (f (quote (("std") ("derive" "minicbor-derive"))))))

(define-public crate-minicbor-0.9.0 (c (n "minicbor") (v "0.9.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "0pgdnmh4c078iyskh89mi2699vqjmk9zh5daycfcb2z4nkz8ll61") (f (quote (("std" "alloc") ("derive" "minicbor-derive") ("alloc"))))))

(define-public crate-minicbor-0.9.1 (c (n "minicbor") (v "0.9.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "0mydiig3qnighl292pk9mwzyap24v8k6374bj5fcq6bnh5wil6jr") (f (quote (("std" "alloc") ("derive" "minicbor-derive") ("alloc"))))))

(define-public crate-minicbor-0.10.0 (c (n "minicbor") (v "0.10.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "0ii4nmcmbgj8b40sdi7xb16ng72pvnzrrzd7kk538c44rfc5n7sg") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.10.1 (c (n "minicbor") (v "0.10.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1djwmc6sk3pjh1jcblmf0z5v9hgwgp5zppqzsfmsx6cxjqqlkdrz") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.0 (c (n "minicbor") (v "0.11.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1akcnsm62w23yqzal25kbb791qxkkhlgz8qk966iabzi872g1mf6") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.1 (c (n "minicbor") (v "0.11.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "0d2z4ai6c7nhnp39vf5a759w4zlgi9c27wbk8vsyywfgnhnyvp7n") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.2 (c (n "minicbor") (v "0.11.2") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "03l9038qskvb3qgx6vlya9ckypv163hxp24r3624vbr658kpycx8") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.3 (c (n "minicbor") (v "0.11.3") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "0hd5gi1amyihjlwlxq01gka47vrmgiqi8kfxa5v7vd4741axyqcp") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.4 (c (n "minicbor") (v "0.11.4") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "1sad0ljhjr16xffl5ny6cmhra7v9azkl7ghypn99fwnzxq1cjvhd") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.11.5 (c (n "minicbor") (v "0.11.5") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "0n8cnzhl9zmzrssm76bdd9d67vx666np9xbgg4gii97fjc5jn8j8") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.12.0 (c (n "minicbor") (v "0.12.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1n9vblyfxci4yf9awbxdx04g7wls13bh07mh51262xv1hfkr6mz6") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.12.1 (c (n "minicbor") (v "0.12.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0yq49k7ffl0wh5p5ygbl71z1yzhpd1r17pmid1b6k8qvwqqkwzzq") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.13.0 (c (n "minicbor") (v "0.13.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0njgrnc7f6c0pc1fkb0wh9nfina43x7brly0xdfjx6shp7rxnnn7") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.13.1 (c (n "minicbor") (v "0.13.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0drh72zmbrm7wyq122rnj88cfx34qzrvk2rzdas3xa4mqb62cy4v") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.13.2 (c (n "minicbor") (v "0.13.2") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "08a5249jnxz5f00apzal3bniwf2afrjingdcpmli82rgp1y8hk8j") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.14.0 (c (n "minicbor") (v "0.14.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1iy2r3sw58pbsmszdm5ykdgh1c5yygvmnn7jqp8xjgx32rpym9b1") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.14.1 (c (n "minicbor") (v "0.14.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "062asbflrpw3sd2a3li5kwbhmg8r7a801mrbd547gabd4q9ji20d") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.14.2 (c (n "minicbor") (v "0.14.2") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1pwl88pgcy3mj728cw6sikk7vx4ls3fxg1s2wjkmglzm1bmsbwsy") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.15.0 (c (n "minicbor") (v "0.15.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0avx8d520y32jhyf3hrkfz8d2cbl3wag9f6l9n1gbk4qpmk9wh9b") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("legacy" "std") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.16.0-rc.1 (c (n "minicbor") (v "0.16.0-rc.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.10.0-rc.1") (o #t) (d #t) (k 0)))) (h "1gn2nrclbf3ly7rj08rf2sra9q7zzcwj3snm8d42ww6fd1dm8pkl") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.16.0 (c (n "minicbor") (v "0.16.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0jvpmb8df6ychg7hjf7bryzm9f9fayagqwrv7ammkn8mgyjp5ph8") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.16.1 (c (n "minicbor") (v "0.16.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1cxcgj8s15fb20nnym887zkdhlw8gdjrfl9ibiylp7qk8hbf7n7v") (f (quote (("std" "alloc") ("partial-skip-support") ("partial-derive-support" "minicbor-derive" "partial-skip-support") ("derive" "minicbor-derive" "alloc") ("alloc") ("__test-partial-skip-support"))))))

(define-public crate-minicbor-0.17.0 (c (n "minicbor") (v "0.17.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "12kaq4xjasnyfxhnc7nrdxq99rd8i7r5sv8n8xl6ibamn713z0j3") (f (quote (("std" "alloc") ("partial-derive-support" "minicbor-derive") ("derive" "minicbor-derive" "alloc") ("alloc"))))))

(define-public crate-minicbor-0.17.1 (c (n "minicbor") (v "0.17.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0sykvwxpybm95f7cgpyihkzlp57y0wli8pnznw6imck30y8pbrd5") (f (quote (("std" "alloc") ("partial-derive-support" "minicbor-derive") ("derive" "minicbor-derive" "alloc") ("alloc"))))))

(define-public crate-minicbor-0.18.0 (c (n "minicbor") (v "0.18.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1jwmas8whib9f24k1kj7k7dz36gznldh2r3gfgc8261diq70481a") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.19.0 (c (n "minicbor") (v "0.19.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0lmfq9vfzg70z0i1mm6i9rwa0ggahbfrnqhwic9yar7id1sd86fk") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.19.1 (c (n "minicbor") (v "0.19.1") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0fb1782d95yxhmzzykskhqas4s68ir9xbabiwi6zynbs4npml06p") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.20.0 (c (n "minicbor") (v "0.20.0") (d (list (d (n "half") (r "^1") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "046agb739dbki941ick77hv7mjba89anwsf20c4zkzbi7lhg85cx") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.21.0 (c (n "minicbor") (v "0.21.0") (d (list (d (n "half") (r "^2.4.0") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "015bamc1faqh27297xwa8a9bzngl5wc9hc9mxb2xffk9rk735mx3") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.21.1 (c (n "minicbor") (v "0.21.1") (d (list (d (n "half") (r "^2.4.0") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "11iyq50k3pgw1x5pmskf1rrk63bjwpvgd5n4m1gv5zh3bwhpy4iy") (f (quote (("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.22.0 (c (n "minicbor") (v "0.22.0") (d (list (d (n "half") (r "^2.4.0") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1f7qmdbiha0z15fn48sc847bg6rnfd18kzrkz2b7x37pnnqifqh7") (f (quote (("full" "std" "derive" "half") ("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.23.0 (c (n "minicbor") (v "0.23.0") (d (list (d (n "half") (r "^2.4.0") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1cwh4nfprdlknn60mwk6sbhgcj55hg47myczd1kxf6nh8ss7xv8d") (f (quote (("full" "std" "derive" "half") ("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

(define-public crate-minicbor-0.24.0 (c (n "minicbor") (v "0.24.0") (d (list (d (n "half") (r "^2.4.0") (o #t) (k 0)) (d (n "minicbor-derive") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "0s99wwxwf513ys89a1bkl6amv5hh1s62qf243vf6qy7cnga5j4vs") (f (quote (("full" "std" "derive" "half") ("derive" "minicbor-derive")))) (s 2) (e (quote (("std" "alloc" "minicbor-derive?/std") ("alloc" "minicbor-derive?/alloc"))))))

