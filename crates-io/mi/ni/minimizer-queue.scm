(define-module (crates-io mi ni minimizer-queue) #:use-module (crates-io))

(define-public crate-minimizer-queue-1.0.0 (c (n "minimizer-queue") (v "1.0.0") (d (list (d (n "ahash") (r "^0.8") (f (quote ("no-rng"))) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 2)) (d (n "strength_reduce") (r "^0.2") (d #t) (k 0)))) (h "1sgk5l1nv96jd92yclw5fyf2qflx7zd428m2gkjgmxcxfcwql3i8")))

