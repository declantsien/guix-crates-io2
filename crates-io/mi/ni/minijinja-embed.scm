(define-module (crates-io mi ni minijinja-embed) #:use-module (crates-io))

(define-public crate-minijinja-embed-1.0.11 (c (n "minijinja-embed") (v "1.0.11") (h "1l9b76g4hb6c4j7lv5kind07pac0ngpz48kgdlsrgs7fgwn4i5bs") (r "1.61")))

(define-public crate-minijinja-embed-1.0.12 (c (n "minijinja-embed") (v "1.0.12") (h "135178ss7npxbp39wndrigmhpjlnrszccpccbkxy4mjvab9px92x") (r "1.61")))

(define-public crate-minijinja-embed-1.0.13 (c (n "minijinja-embed") (v "1.0.13") (h "0zm7hz4ipbj22ppr3rm37rvn67pmj7prx7k6kmamqlqvnp8p11kx") (r "1.61")))

(define-public crate-minijinja-embed-1.0.14 (c (n "minijinja-embed") (v "1.0.14") (h "02214wnz279yb46ynhds45spylg03mh1p3pjjhcimw85bfyhgx57") (r "1.61")))

(define-public crate-minijinja-embed-1.0.15 (c (n "minijinja-embed") (v "1.0.15") (h "0af3cg9az2w6n629n6wz7n51nc7v7i6rkwkjibjchbmcp6q780pw") (r "1.61")))

(define-public crate-minijinja-embed-1.0.16 (c (n "minijinja-embed") (v "1.0.16") (h "1b0cnxid7x4fghbmbic0bc7b4j0rg55x845yqn70lkibdaf05ixx") (r "1.61")))

(define-public crate-minijinja-embed-1.0.17 (c (n "minijinja-embed") (v "1.0.17") (h "191i95m5r5z38h6hbrr28j812g9hjskagdw29rkrfy9k7i5b3m3g") (r "1.61")))

(define-public crate-minijinja-embed-1.0.18 (c (n "minijinja-embed") (v "1.0.18") (h "0p46mnbj421kc7qpybg0bijds644dfnnzn2sa8bbnz1d0dk3ziri") (r "1.61")))

(define-public crate-minijinja-embed-1.0.20 (c (n "minijinja-embed") (v "1.0.20") (h "0fd8abxhz67jpy0bqx58s28k95k80rp22vhxyh2avxnfr36krfdi") (r "1.61")))

(define-public crate-minijinja-embed-2.0.0-alpha.0 (c (n "minijinja-embed") (v "2.0.0-alpha.0") (h "1lylpgnbfllhdy8p81cybhi349wz45yqn25z2aa9xd698bk6cdq6") (r "1.61")))

(define-public crate-minijinja-embed-1.0.21 (c (n "minijinja-embed") (v "1.0.21") (h "1phsr4g0mnq3amqmsrrsgd5shj62v4fhdwyl4830vvyjm8d9sh3l") (r "1.61")))

(define-public crate-minijinja-embed-2.0.0 (c (n "minijinja-embed") (v "2.0.0") (h "0j81hd3dmss66rswk4079ai4a3d8vkd62nx1027lq0rracv33mqh") (r "1.61")))

(define-public crate-minijinja-embed-2.0.1 (c (n "minijinja-embed") (v "2.0.1") (h "05iindxnv7dfxg62sz81fcki4c4cxiygchrxm1dxgwkayq47iafh") (r "1.61")))

