(define-module (crates-io mi ni minigrep_awm) #:use-module (crates-io))

(define-public crate-minigrep_awm-0.1.0 (c (n "minigrep_awm") (v "0.1.0") (h "182y0kfynn4licffc4dk5qgfmh8vzdv4f9wsbsv9imx4m3djf47f") (y #t)))

(define-public crate-minigrep_awm-0.2.0 (c (n "minigrep_awm") (v "0.2.0") (h "1wz5hlf9y50cnzjqr1qijca04ww83v2j0k45z306kvfbmagyhbzj")))

