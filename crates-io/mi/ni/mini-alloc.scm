(define-module (crates-io mi ni mini-alloc) #:use-module (crates-io))

(define-public crate-mini-alloc-0.4.2 (c (n "mini-alloc") (v "0.4.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1bzmx12bks3m9pc55284a2hczvcp2b43ssm01nywsl1qdmar76dx")))

