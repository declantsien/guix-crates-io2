(define-module (crates-io mi ni mini-http-test) #:use-module (crates-io))

(define-public crate-mini-http-test-0.1.0 (c (n "mini-http-test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.1") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.1") (f (quote ("http1" "server"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0sxxvs03glia86fxyyhmglxikpzid1kafri9pgh8hn46jlv0gjlb")))

(define-public crate-mini-http-test-0.2.0 (c (n "mini-http-test") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.2") (f (quote ("http1" "server"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)))) (h "1vdhh08ynd5lxmdwjxhhki4xwazghciwhnvlh3w76ab4avlyqvxb")))

