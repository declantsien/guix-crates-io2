(define-module (crates-io mi ni minisign-verify) #:use-module (crates-io))

(define-public crate-minisign-verify-0.1.0 (c (n "minisign-verify") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre") (d #t) (k 0)))) (h "023r4vvvqqml737xnmysyacnhjvqkfnv1mqa7ibgsizwhdb4yagb")))

(define-public crate-minisign-verify-0.1.1 (c (n "minisign-verify") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre") (d #t) (k 0)))) (h "15lb5drblxxxkkdbjzd1sjpxpp43264bvv5i2i0lywnl2vaiq16p")))

(define-public crate-minisign-verify-0.1.2 (c (n "minisign-verify") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)))) (h "1kj1s7g5n3z06l11h9lwk5055cfkxp93m5l83sbvnmr9ixcnx6zb")))

(define-public crate-minisign-verify-0.1.3 (c (n "minisign-verify") (v "0.1.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)))) (h "0hi1m4r6mj2wg4wqk6wlax3m8a1l3findqk1ar940j5f067ag0nb")))

(define-public crate-minisign-verify-0.1.4 (c (n "minisign-verify") (v "0.1.4") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)))) (h "0wz05fakl30ads9ra3hdq0qhsx7hhnjv80f7yr14z0i4lxh7xbln")))

(define-public crate-minisign-verify-0.1.5 (c (n "minisign-verify") (v "0.1.5") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1z1qvm99iscrm8h3zb1aic0yday1mdzm9hspfa2gajal99g6pnyl")))

(define-public crate-minisign-verify-0.1.6 (c (n "minisign-verify") (v "0.1.6") (d (list (d (n "ct-codecs") (r "^0.1.1") (d #t) (k 0)))) (h "1dwjx37vkdabalivg96rpnxz5k2x2f5zn031glq3gnin2q0k2f29")))

(define-public crate-minisign-verify-0.1.7 (c (n "minisign-verify") (v "0.1.7") (d (list (d (n "ct-codecs") (r "^0.1.1") (d #t) (k 0)))) (h "0whh171x25l75qx33vhj0zgx70dp3qd7i45fqph3rgh28h4rmbxv")))

(define-public crate-minisign-verify-0.1.8 (c (n "minisign-verify") (v "0.1.8") (h "04jw9q0rvym3li872njisf2dh89bdizziy8wjricss1wivz0f1fv")))

(define-public crate-minisign-verify-0.2.0 (c (n "minisign-verify") (v "0.2.0") (h "1nfzrzdvffv0kq6by7333mhkcxrfpfnxmmham2rw8w24i28z1k4m")))

(define-public crate-minisign-verify-0.2.1 (c (n "minisign-verify") (v "0.2.1") (h "10cql6vfmwcb319n00vzz1qxmx9g18w3vdyhanrm7pawsr2clgck")))

