(define-module (crates-io mi ni miniserver) #:use-module (crates-io))

(define-public crate-miniserver-0.1.5 (c (n "miniserver") (v "0.1.5") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "actix-web") (r "^0.7") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "1hb03iq43b14m640jrpv70f5lmj22d8n65v95s04i6xv92mfp729")))

