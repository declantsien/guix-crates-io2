(define-module (crates-io mi ni minitrace-jaeger) #:use-module (crates-io))

(define-public crate-minitrace-jaeger-0.3.0 (c (n "minitrace-jaeger") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.3.0") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.1") (d #t) (k 0)))) (h "1mi6d407zr9vg670wipv2swizs1sa2w402v115wx5w36p5cmawy6")))

(define-public crate-minitrace-jaeger-0.3.1 (c (n "minitrace-jaeger") (v "0.3.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.3.1") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.1") (d #t) (k 0)))) (h "0nvngj9s07zdwnzhrs3rlg5maifzf32mj47vm03l33wji9gn3nxi")))

(define-public crate-minitrace-jaeger-0.4.0 (c (n "minitrace-jaeger") (v "0.4.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.4.0") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.1") (d #t) (k 0)))) (h "0nygcpx8k7qwzm81q0nf0hi2gjv0qcr4sbdlbdagyhhpj7dwfc71")))

(define-public crate-minitrace-jaeger-0.4.1 (c (n "minitrace-jaeger") (v "0.4.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "minitrace") (r "^0.4.1") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "1b132picfc9sb8l0xis4ffaqwpyzjlap498rn5blnlndfcrbwnks")))

(define-public crate-minitrace-jaeger-0.5.0 (c (n "minitrace-jaeger") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.5.0") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "19kx16lh5v5syag6hg2mhz6wm8krd3rq8kpnnanb8vy1dbszzl7k")))

(define-public crate-minitrace-jaeger-0.5.1 (c (n "minitrace-jaeger") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.5.1") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "0rd8r0fwh9sj258ivb378x3av76dh1ynhaqcymf2s8qcs91y2w2z")))

(define-public crate-minitrace-jaeger-0.6.0 (c (n "minitrace-jaeger") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.0") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "08mcr9mjsbzac251nbyzhixzw4bh1a6h3aadg6q8zw7g7a7dp30s")))

(define-public crate-minitrace-jaeger-0.6.1 (c (n "minitrace-jaeger") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.1") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "07pd7imlcmxfin9pai9hndwry2w2ipngzq0yjp1088x8s9byiglq")))

(define-public crate-minitrace-jaeger-0.6.2 (c (n "minitrace-jaeger") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.2") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "0wjrxcxv7i7jrpdqsz722sm6sijzknmgxjh3a1cw37z8xjkkkhbf")))

(define-public crate-minitrace-jaeger-0.6.3 (c (n "minitrace-jaeger") (v "0.6.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.3") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "0km5drnxs67lhghf55l0ypb5fpq4d5g94jsbh1iacld7wz9j4v8p")))

(define-public crate-minitrace-jaeger-0.6.4 (c (n "minitrace-jaeger") (v "0.6.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.4") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "0x2fbkr2wbqnk2akcfllpkwshzh9rbpmj6il35qw3cb0jzddgd3p")))

(define-public crate-minitrace-jaeger-0.6.5 (c (n "minitrace-jaeger") (v "0.6.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minitrace") (r "^0.6.5") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.2") (d #t) (k 0)))) (h "0lq1iyaz6q82wzd8qls2j54afx1v33agd58bs6gh3j4jkaam74ri")))

