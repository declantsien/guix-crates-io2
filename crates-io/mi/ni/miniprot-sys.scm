(define-module (crates-io mi ni miniprot-sys) #:use-module (crates-io))

(define-public crate-miniprot-sys-0.1.0 (c (n "miniprot-sys") (v "0.1.0") (d (list (d (n "libz-sys") (r "^1.1.8") (f (quote ("libc"))) (k 0)) (d (n "bindgen") (r "^0.63") (f (quote ("runtime" "which-rustfmt"))) (o #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0m4frvjhxkbmjbzc4l7546l33ds1mv9yri5ls6zhcwcn7mxd6whi") (f (quote (("vendored" "bindgen") ("default")))) (l "libminiprot")))

