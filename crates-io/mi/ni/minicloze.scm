(define-module (crates-io mi ni minicloze) #:use-module (crates-io))

(define-public crate-minicloze-0.1.0 (c (n "minicloze") (v "0.1.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0klw4ssck646dsqid4dp7ynfnhr2c6d1gfjqq5r4lalz86abd3hb")))

(define-public crate-minicloze-1.1.0 (c (n "minicloze") (v "1.1.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "022hfiic7m4pq0im5gcrh3b773bzxsrk0ykcg2vzcdisgnpfic9m")))

(define-public crate-minicloze-1.1.1 (c (n "minicloze") (v "1.1.1") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07pi9x52cj8vw9s6sxiadrhkjmclvv59pac1hmvbw7xyhzf307ak")))

(define-public crate-minicloze-1.2.0 (c (n "minicloze") (v "1.2.0") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01i81gq10h6hqp3m6gr3p8mik4wiq86jpggd1xqcl7aaqxbq9qsk")))

(define-public crate-minicloze-1.2.1 (c (n "minicloze") (v "1.2.1") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r5f5v81mk5pj40ybqkw0j5sv9zy2sfbjfvvdczmrcqpsf87w6yv")))

(define-public crate-minicloze-1.2.2 (c (n "minicloze") (v "1.2.2") (d (list (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bcyks1b4nbn13qxnn91krwy6d2bpcybjv07vxbwcg99p9gvir3z")))

(define-public crate-minicloze-1.3.0 (c (n "minicloze") (v "1.3.0") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1035s4a4wrfm65rf6pw6nzpbirk7ffb3lx18qsxfni8n4fciyi2y")))

(define-public crate-minicloze-1.3.1 (c (n "minicloze") (v "1.3.1") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03flcpbgcsjm350fj25q9gjffmq410l1r4k4zw7awwj7dflxr2mc")))

(define-public crate-minicloze-1.4.0 (c (n "minicloze") (v "1.4.0") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "open") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1vakfiiad6csnzrjh8mwsh2dzfwwlh2spfkv5ssy44jza56cg5d4")))

(define-public crate-minicloze-1.4.1 (c (n "minicloze") (v "1.4.1") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.2.0") (d #t) (k 0)))) (h "1f7yhg76ij3y0nlh44i8781va7kk3vvpklbg01nmh2w8cwj77bhz")))

(define-public crate-minicloze-1.4.2 (c (n "minicloze") (v "1.4.2") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.3.0") (d #t) (k 0)))) (h "0wyj9axwz3n4ix491589696kq03pdlb0fbfpgwikphxf15kppv0i")))

(define-public crate-minicloze-1.5.0 (c (n "minicloze") (v "1.5.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.3.0") (d #t) (k 0)))) (h "0m2bccw75jcdw0sjxxzxzfmrxc7fglpv29mb9zc5fj0nkx1b7wwg")))

(define-public crate-minicloze-1.6.0 (c (n "minicloze") (v "1.6.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.4.0") (d #t) (k 0)))) (h "1vy1n38i47cb0hpyisskniv5yxjawx7w1mp0x3qs0kxqgywfw2j6")))

(define-public crate-minicloze-1.6.1 (c (n "minicloze") (v "1.6.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.4.0") (d #t) (k 0)))) (h "1yfzaygq2p77q25rv26n6yv8svjd6grhh59qncqampnf97l4csnc")))

(define-public crate-minicloze-1.6.2 (c (n "minicloze") (v "1.6.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 0)) (d (n "minicloze-lib") (r "^0.4.0") (d #t) (k 0)))) (h "0y091z5yhlmp6b5v9ip2inz3x5jjkvj0pl5nyizdw38a76dq7cph")))

