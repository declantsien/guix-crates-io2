(define-module (crates-io mi ni mini-grep-rs) #:use-module (crates-io))

(define-public crate-mini-grep-rs-0.1.0 (c (n "mini-grep-rs") (v "0.1.0") (d (list (d (n "jdks") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "15pr4r826srny8a4k06ss3qswspwi9ql3vk6bqaxm9zp4zmvm0ba")))

