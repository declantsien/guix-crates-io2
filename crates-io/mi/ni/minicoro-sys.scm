(define-module (crates-io mi ni minicoro-sys) #:use-module (crates-io))

(define-public crate-minicoro-sys-0.8.0 (c (n "minicoro-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05fyq1xb8ap2fa1d656frwmfkl4ic0clypzfhx4kydcly27ad6z6")))

(define-public crate-minicoro-sys-0.8.1 (c (n "minicoro-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0fz35ybaw0r15gjn2jky08by2rbpfb37q4jqhxlcln7h5ksfx02m")))

