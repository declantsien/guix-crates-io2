(define-module (crates-io mi ni minify) #:use-module (crates-io))

(define-public crate-minify-1.0.0 (c (n "minify") (v "1.0.0") (h "075ny8m7fv1arhjhpp3l47vjdfs5v36xqp84j3rg9gf9mcg4kwla")))

(define-public crate-minify-1.1.0 (c (n "minify") (v "1.1.0") (h "0xsdv00h8p9713b4x7jxb282j98v7kgafzkqjndkjqn5bln3sgl5")))

(define-public crate-minify-1.1.1 (c (n "minify") (v "1.1.1") (h "0nw8db98r49agw9iyx4br7sz50kyi6ag5shlg64dm1vmpn1cbxrf")))

(define-public crate-minify-1.2.0 (c (n "minify") (v "1.2.0") (h "0niq6ylsj859qs386hlawd964qyl8mqrvni1nfi78z8d17wgjlsm")))

(define-public crate-minify-1.3.0 (c (n "minify") (v "1.3.0") (h "0d96wkzdhy3zj2b16p0hip5ggqch8281b52dv90kxkz0dkyaqfz9")))

