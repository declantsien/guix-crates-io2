(define-module (crates-io mi ni minigreprfoxkendo) #:use-module (crates-io))

(define-public crate-minigreprfoxkendo-0.1.0 (c (n "minigreprfoxkendo") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1iqbv005im8xm0bldhayzbn55ias30h4imp20yr9a1zkj1c11yvg") (y #t)))

(define-public crate-minigreprfoxkendo-0.1.1 (c (n "minigreprfoxkendo") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rf9xykji4bballf6qfdkavakiqb50zi8ib1ricgcr88bm57gv78")))

