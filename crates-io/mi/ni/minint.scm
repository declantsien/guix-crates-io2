(define-module (crates-io mi ni minint) #:use-module (crates-io))

(define-public crate-minint-0.1.0 (c (n "minint") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (f (quote ("tokio-io" "sink"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rmp") (r "^0.8.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "net" "time" "macros"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "0lahvf78b0wj9g2bdlkcyjbynz9im98cmmgwh8shxb4qahf2d995")))

