(define-module (crates-io mi ni minitime) #:use-module (crates-io))

(define-public crate-minitime-0.1.0 (c (n "minitime") (v "0.1.0") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "09q971gm6pfzdqg703n377l2i39n106p6ayrg0ad7y7my9jgf2is")))

