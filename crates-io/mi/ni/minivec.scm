(define-module (crates-io mi ni minivec) #:use-module (crates-io))

(define-public crate-minivec-0.1.0 (c (n "minivec") (v "0.1.0") (h "09m33mr75f9amlmyg3yvvx5lz4c2l1vmhkb85b1v4ik2alss4x78")))

(define-public crate-minivec-0.1.1 (c (n "minivec") (v "0.1.1") (h "0bnpdpc4r00l0l3zqkmg2kylzamv0waj7ba5fbqdqfsqj4b5w8w6")))

(define-public crate-minivec-0.1.2 (c (n "minivec") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1pb46z31ljs4fhj6k6xzh658ib22bwby9ynghxldp05869x6s40z")))

(define-public crate-minivec-0.1.3 (c (n "minivec") (v "0.1.3") (d (list (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (k 0)))) (h "0lpw0b5sx59692bna8h4inxq24w69ska37vk8z1jd0cwdzvn3ay6")))

(define-public crate-minivec-0.2.0 (c (n "minivec") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0givy04zipjpcv373w4a7dj38dzdq5ifhz6c59rry845vf2i05y9")))

(define-public crate-minivec-0.2.1 (c (n "minivec") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1azijjydyqrims2vi5wc5kbf8ai4y0ssx8pzz9fm6gq1q66dyml0")))

(define-public crate-minivec-0.2.2 (c (n "minivec") (v "0.2.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1jh5icji0wjnavrh951k48wjhx8gqd4w8lwssvks8ws6m9i9vh21") (y #t)))

(define-public crate-minivec-0.2.3 (c (n "minivec") (v "0.2.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "04dl7ij6ivj12sr9hs2pq4inbcx7bfz6l2j209w8vnnbb0srvvy0")))

(define-public crate-minivec-0.3.0 (c (n "minivec") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "12yk0sb4brlhil7nyhkghz4w22bq0np6dcisw40xbrfm1frz3gri")))

(define-public crate-minivec-0.3.1 (c (n "minivec") (v "0.3.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "00zik5x28q3lf8kz6x0c9w5m0y8hxmrlgsr7f7r3inkmka3sdf0r")))

(define-public crate-minivec-0.4.0 (c (n "minivec") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "153x8d7kjgm2sfmygkanm3v7mcp1viwpfyw9r3p24s04x1fxp194")))

