(define-module (crates-io mi ni minifloat) #:use-module (crates-io))

(define-public crate-minifloat-0.1.0 (c (n "minifloat") (v "0.1.0") (h "0216lbdixvr7yz1rhdg9ppbyasqa0bagxj9y3arprjlhgm6v192i")))

(define-public crate-minifloat-0.1.1 (c (n "minifloat") (v "0.1.1") (h "0s9c88csb7vpkm0286qk02pk36aw7y5j1vq4q9m8iw2lmayn983x")))

(define-public crate-minifloat-0.1.2 (c (n "minifloat") (v "0.1.2") (h "1jb948xnxx36905dchiwfjd45f62qh1xqkwvvhkrxqkmhbw2vbjs")))

(define-public crate-minifloat-0.1.3 (c (n "minifloat") (v "0.1.3") (h "160y0n0w4px4glp6a85xzbp5xsnyhgi9dzlhdijqhfb5j8vipr78")))

(define-public crate-minifloat-0.1.4 (c (n "minifloat") (v "0.1.4") (h "1hy5zcnj2djjmngyids0rbzi14f7jjmjpij0csj3g3nxpkf0w63i")))

(define-public crate-minifloat-0.1.5 (c (n "minifloat") (v "0.1.5") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 2)))) (h "0fnipn55pjk1xvbam8j39i1wj2bh2ib5rrlsq30hhkwxksl6v6d7")))

(define-public crate-minifloat-0.1.6 (c (n "minifloat") (v "0.1.6") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)))) (h "1r9h90d7y1msdn8jqz2mbbj9mvpprway9gdwqmwr78rnw120f7ha")))

