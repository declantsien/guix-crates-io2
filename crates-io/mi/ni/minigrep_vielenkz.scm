(define-module (crates-io mi ni minigrep_vielenkz) #:use-module (crates-io))

(define-public crate-minigrep_vielenkz-0.1.0 (c (n "minigrep_vielenkz") (v "0.1.0") (h "06dahfgaysgmlwc45by76pksl6j8sb4xak0dl8h6ddn4s1p8qmr0") (y #t)))

(define-public crate-minigrep_vielenkz-0.1.1 (c (n "minigrep_vielenkz") (v "0.1.1") (h "0mhkszjsr4b68b1ck85jbvkalwan1xzj7brsxg7vazczdfnd39d5") (y #t)))

(define-public crate-minigrep_vielenkz-0.1.2 (c (n "minigrep_vielenkz") (v "0.1.2") (h "0fh4pcihr5ffq5sp7lv508wqbmid8d23gag3x41yzld1bvwjyv2r")))

