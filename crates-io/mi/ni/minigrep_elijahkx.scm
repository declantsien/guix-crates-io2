(define-module (crates-io mi ni minigrep_elijahkx) #:use-module (crates-io))

(define-public crate-minigrep_elijahkx-0.1.0 (c (n "minigrep_elijahkx") (v "0.1.0") (h "113lpc7lzass5zycwq7vw88z5rwaliahr0rhpvxgkh738l4w7fyn") (r "1.72.0")))

(define-public crate-minigrep_elijahkx-0.1.1 (c (n "minigrep_elijahkx") (v "0.1.1") (h "09w3vif8m70x77vkvs95miki8vhh6ycx4llppsa17xa5v1fqczq5") (r "1.72.0")))

(define-public crate-minigrep_elijahkx-0.1.2 (c (n "minigrep_elijahkx") (v "0.1.2") (h "00xba5qym4dv585rnllz26w1ciybqza16yfzdzjl5d3npa0g7542") (r "1.72.0")))

