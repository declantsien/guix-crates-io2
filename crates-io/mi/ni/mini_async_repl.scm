(define-module (crates-io mi ni mini_async_repl) #:use-module (crates-io))

(define-public crate-mini_async_repl-0.2.1 (c (n "mini_async_repl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "1j98s97sqnh35dbj7k5fi9l4iz8kvs7k7cwsmqm5ql5c7zxrm4zw")))

