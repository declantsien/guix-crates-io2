(define-module (crates-io mi ni miniserde-enum) #:use-module (crates-io))

(define-public crate-miniserde-enum-0.1.0 (c (n "miniserde-enum") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0i03z9jh9vzm96r6y7l7icxhwpghvvdbv3qnb7gb8rhm1s1kp6nb")))

(define-public crate-miniserde-enum-0.1.1 (c (n "miniserde-enum") (v "0.1.1") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "152agxzrghv3izm9givy7z8r4bcfqsfmp8hz48zk60ygss07k8xx")))

(define-public crate-miniserde-enum-0.1.2 (c (n "miniserde-enum") (v "0.1.2") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "09rgx9bm1w901v2ya8iz47f9n41m614qjzng7mxwhjzhwj3n13y3")))

(define-public crate-miniserde-enum-0.1.3 (c (n "miniserde-enum") (v "0.1.3") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0f58h5mpy6w1w5h8j94f7wl5q6cn28kqinl7ss39x24ghg0lpnf3")))

