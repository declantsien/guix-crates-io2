(define-module (crates-io mi ni mini_async_http) #:use-module (crates-io))

(define-public crate-mini_async_http-0.0.1 (c (n "mini_async_http") (v "0.0.1") (d (list (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "tcp" "os-util"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0r7pzmx7g1ma94vkjzamyg7zf2y1gcw9qjx27bihzcqcmwb2z5xr")))

(define-public crate-mini_async_http-0.1.0 (c (n "mini_async_http") (v "0.1.0") (d (list (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "tcp" "os-util"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1zz6azcxnsjhfzly23djmk0w3cml45hmk2bx3xj2n5hcryrmndph")))

