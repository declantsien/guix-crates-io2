(define-module (crates-io mi ni miniring) #:use-module (crates-io))

(define-public crate-miniring-0.1.0 (c (n "miniring") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "05lyknvj3r6ck2gvjsgf2rargn4s3qhd2w10spiw6vjfdy3irv3b") (y #t)))

(define-public crate-miniring-1.0.0 (c (n "miniring") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1kamid0w3xgzw41ri9h9a1ih7g4rz1pw2acarrpgwvdgv2lazw0z") (y #t)))

(define-public crate-miniring-2.0.0 (c (n "miniring") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0yym7j7k9pgvjvb8g9430kqm4xrm3fqmazdl9h466wsyijvbqvyv") (y #t)))

(define-public crate-miniring-3.0.0 (c (n "miniring") (v "3.0.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0xb1w4mjkhpzanv92gsggykrf26n7nky6qqgcgh3xk9bj0w2fjzq") (y #t)))

(define-public crate-miniring-24.0.0+298c083 (c (n "miniring") (v "24.0.0+298c083") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0aarpdfj38hdvz70a25skw6agg9w9had3j9j7cc5g1bdwnigklvp") (y #t)))

(define-public crate-miniring-24.0.1+298c083 (c (n "miniring") (v "24.0.1+298c083") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1vzkyh7skvi9x8dmci53y1r9j5ia9r0dp3aydzpiq5rc0669dfk4")))

