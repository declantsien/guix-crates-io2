(define-module (crates-io mi ni miniproj-ops) #:use-module (crates-io))

(define-public crate-miniproj-ops-0.1.0 (c (n "miniproj-ops") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)))) (h "1zyq28cijpinlxbwb30ydxnrfj1k6yhpkxqiy5v6fl5vynzz9r94")))

(define-public crate-miniproj-ops-0.2.0 (c (n "miniproj-ops") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)))) (h "0f5d4rbjhczvxwabqr80j4v9hkxz7jcgkjv2h4ha8935xvxgfhnh") (y #t)))

(define-public crate-miniproj-ops-0.3.0 (c (n "miniproj-ops") (v "0.3.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)))) (h "1cjzrs9p7cwgabyphf1hnj7gwjs3p2z5srkz6m9axy8m725aq054")))

(define-public crate-miniproj-ops-0.4.0 (c (n "miniproj-ops") (v "0.4.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)))) (h "0z89hkggvl9i6kqck37zywpg5knpmgf10klc9g0bqaqn450vkda6")))

(define-public crate-miniproj-ops-0.5.0 (c (n "miniproj-ops") (v "0.5.0") (h "0bf21p07lmzgkxh39hayk2zgc0mvsxznx8jwgcl6bk1k5fqsrf2i")))

(define-public crate-miniproj-ops-0.6.0 (c (n "miniproj-ops") (v "0.6.0") (h "103cswgc3cqsm94ld7jdwvrgvyzr4zj0i9jqa5nv74rigvdfcv2h")))

(define-public crate-miniproj-ops-0.7.0 (c (n "miniproj-ops") (v "0.7.0") (h "04p3xdyn5cs5pyfk4ipl6dkql6355jnqfhsrwpfz4ig78v8d6i30")))

(define-public crate-miniproj-ops-0.8.0 (c (n "miniproj-ops") (v "0.8.0") (h "0hzzr3r6ndjdi08rc6i2wh3i0865zcabcq893z324wnnszay03rn")))

(define-public crate-miniproj-ops-0.9.0 (c (n "miniproj-ops") (v "0.9.0") (h "046mszc6ihchqb6hlfzbfpanv5m467rvpps5wqsjw36x47skcibg")))

(define-public crate-miniproj-ops-0.10.0 (c (n "miniproj-ops") (v "0.10.0") (h "1fpfd2h6s2vc90432v3882ny1fyhj4xfrinzx4wvr4lnnsmgz5ln")))

(define-public crate-miniproj-ops-0.10.1 (c (n "miniproj-ops") (v "0.10.1") (h "0c6jyw890v0da63j0mw2piv46zr95im7p47lx0xyqfbnhyncy6r6")))

