(define-module (crates-io mi ni minicbor-ser) #:use-module (crates-io))

(define-public crate-minicbor-ser-0.1.0 (c (n "minicbor-ser") (v "0.1.0") (d (list (d (n "minicbor") (r "0.11.*") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "0mvpwc0572vsvfryip77v0i5q1i7icxv45shacbyh0vx9bapnhd0") (f (quote (("std" "serde/std" "minicbor/std" "minicbor/alloc") ("default" "std")))) (y #t)))

(define-public crate-minicbor-ser-0.1.1 (c (n "minicbor-ser") (v "0.1.1") (d (list (d (n "minicbor") (r "~0.11") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "00fpbp14j5r8a45azp6yzfz33aqs3mhy1kphsksz6iij0lhb1krd") (f (quote (("std" "serde/std" "minicbor/std" "minicbor/alloc") ("default" "std")))) (y #t)))

(define-public crate-minicbor-ser-0.1.2 (c (n "minicbor-ser") (v "0.1.2") (d (list (d (n "minicbor") (r "~0.11") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "1sbhmiz7ygg165qqhl4dr3hzv4ljb61zgzvv3s1c7q33md7sqxs3") (f (quote (("std" "serde/std" "minicbor/std" "minicbor/alloc") ("default" "std"))))))

(define-public crate-minicbor-ser-0.1.3 (c (n "minicbor-ser") (v "0.1.3") (d (list (d (n "minicbor") (r "~0.11") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "14yljlclynrm02p6bhagzvhy2xmzyxmc6ajms3jz997czp022zrj") (f (quote (("std" "serde/std" "minicbor/std" "minicbor/alloc") ("default" "std"))))))

(define-public crate-minicbor-ser-0.1.4 (c (n "minicbor-ser") (v "0.1.4") (d (list (d (n "minicbor") (r "~0.18") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (k 2)))) (h "03rv9kvk2gr734x28yqbjjfla31v1cahh9cnp9wi5mk5pikdac8d") (f (quote (("std" "serde/std" "minicbor/std" "alloc") ("default" "std") ("alloc" "serde/alloc" "minicbor/alloc")))) (y #t)))

(define-public crate-minicbor-ser-0.2.0 (c (n "minicbor-ser") (v "0.2.0") (d (list (d (n "minicbor") (r "~0.18") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (k 2)))) (h "1c8fvh70z2fcc4b14qfz0fl6s2vz1mj6zm8k35ki2qy5m634p0y0") (f (quote (("std" "serde/std" "minicbor/std" "alloc") ("default" "std") ("alloc" "serde/alloc" "minicbor/alloc"))))))

