(define-module (crates-io mi ni minimad) #:use-module (crates-io))

(define-public crate-minimad-0.2.0 (c (n "minimad") (v "0.2.0") (h "1bb7dgbrwn5ydn7kqzcklhzx0d273d5ryz6zv5lihgsz65jgdv4z")))

(define-public crate-minimad-0.2.1 (c (n "minimad") (v "0.2.1") (h "1ddvxc33byk4d78xfl6gjswnii49a0bj28spqrn9li5sc74yf3q9")))

(define-public crate-minimad-0.2.2 (c (n "minimad") (v "0.2.2") (h "099hxad9fhw11zx41byq1a8zb4cbh6ldmjq3ngr2j07r7aa6ap90")))

(define-public crate-minimad-0.2.3 (c (n "minimad") (v "0.2.3") (h "1mam500kni9c143w4bb5yfp9g39y0siw8hi8jvj901afx15pwd16")))

(define-public crate-minimad-0.3.0 (c (n "minimad") (v "0.3.0") (h "1mgkhsbl4xbdlyxjc2x0f3jzv6yc5h0pxly25qzfb7xcacwl0f9p")))

(define-public crate-minimad-0.3.1 (c (n "minimad") (v "0.3.1") (h "0gnbkdlv0hmiayj29b2a56v0yfzrlshk2a30i072daf0m7h0sdjp")))

(define-public crate-minimad-0.3.2 (c (n "minimad") (v "0.3.2") (h "1x12d9wk9irrdrx4cr60fb9dwsg0rcv6mmblkd82371rwkb51bi1")))

(define-public crate-minimad-0.3.3 (c (n "minimad") (v "0.3.3") (h "0as82hdibgwiydpdd7b5r71r4yfm5jgmcjmj7wvjbvsiq32w0ksv")))

(define-public crate-minimad-0.3.4 (c (n "minimad") (v "0.3.4") (h "1wn340gxfxyrc46w6z5d74pd0116z15b3qfg4mqhyad2ax7s85zx")))

(define-public crate-minimad-0.4.0 (c (n "minimad") (v "0.4.0") (h "1np262z2migadj4idx6a567af70gngriiha4y8kmf875l2njhc01")))

(define-public crate-minimad-0.4.1 (c (n "minimad") (v "0.4.1") (h "0z3g4czr6kc536pqyrfr0hni1r235zwflv8p5ps11bbrk05gz9ad")))

(define-public crate-minimad-0.4.2 (c (n "minimad") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "09akd1z6139i8j27pvrdbz7122s5qqwmi0c5svxk22mfp1ibhyrp")))

(define-public crate-minimad-0.4.3 (c (n "minimad") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0mcd27s6pg5qzybxdq7ihlbv8bsbsmrfavlywrf3j1xpx4h85dhv")))

(define-public crate-minimad-0.5.0 (c (n "minimad") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1s09g2f5y4nss5mvpppjfr624zrbh4x5477ky9h4y9qcb7f3vfyg")))

(define-public crate-minimad-0.5.1 (c (n "minimad") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1lv5ix73q5xr6rib8fzz8vprh68090lc96as5nnf2lai2ycri6i3")))

(define-public crate-minimad-0.5.2 (c (n "minimad") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1bm47f47icm08dwxfgn2i86i0cp40f9jz4kxk4p8c1sn6iagb7br")))

(define-public crate-minimad-0.6.0 (c (n "minimad") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1i4hyi9g6l218qgdxb6251d47da5x57qvmipv44a5rm4f8mmf0v6")))

(define-public crate-minimad-0.6.1 (c (n "minimad") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0qss7jxhg5l1mwsbgz9g07j76mslazc7vc07dq0m0gz6f6szslqh")))

(define-public crate-minimad-0.6.2 (c (n "minimad") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1dig36vnfircz3vxzzslwdvnlxv5y2pdrn7gcrwx45rpaidadylf")))

(define-public crate-minimad-0.6.3 (c (n "minimad") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1dwzichvg4ny0hba6v1if9bgxah8na7k7p8h13hczr0rzrx8gmb4")))

(define-public crate-minimad-0.6.4 (c (n "minimad") (v "0.6.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1r9wfqvj4kmlpymnzkpi3szh5cgwiwhn7zfkim9j26rix3s8q84a")))

(define-public crate-minimad-0.6.5 (c (n "minimad") (v "0.6.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "170lrc53vkpkzgcilhl6djanhvsj8gcjazhz7pk2nm27wdpwja2s")))

(define-public crate-minimad-0.6.6 (c (n "minimad") (v "0.6.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1234qcgv2z1qgplma0xgqlh0m106w885vbx2fl4gxjwph1mjp6cb")))

(define-public crate-minimad-0.6.7 (c (n "minimad") (v "0.6.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "11z809brjmwidjbv8jhnc4q2xnqdvxkkkzs4csz61l106d9nzzi4")))

(define-public crate-minimad-0.6.8 (c (n "minimad") (v "0.6.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1pprcnsnqswpjs26zhwxks4lmjd8fknqr1qxganqjxyd1h8ccpqi")))

(define-public crate-minimad-0.6.9 (c (n "minimad") (v "0.6.9") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1m4scp8xp6gn2ivqdlalrgwzsa5ln83gzikymhc674aligmdfpnb")))

(define-public crate-minimad-0.7.0 (c (n "minimad") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1pbil0ziyvxmllvx02pximxfda6fyvhyxddkcd1a9gqyx4h8yq0z") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.7.1 (c (n "minimad") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "04xyphcr9iv4ilrscqwai8rwr5ipm4nzwbijjpp580dbav8ns5y6") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.8.0 (c (n "minimad") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "1v9h8vpajqlwhazmlvcyifk5lzwazs4v2kfgkdllwamqxi0g4mw9") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.9.0 (c (n "minimad") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "02rb2yfw0x47f0n0cnikhhb06zh2hknm53sd3529aidxbzkb4dyd") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.9.1 (c (n "minimad") (v "0.9.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)))) (h "0d2qnjwdrb3ngb0sayzj2b56pjb75ajqiaz4iw7zfs4537q3jxi7") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.10.0 (c (n "minimad") (v "0.10.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "189gp9lbvlcyl2maa2lbl24ly8ihzkk7vacpiliqxg8040zb3lgy") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.11.0 (c (n "minimad") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "0vcldqv72rvbmg5f218rhxzajz0bmcvb153fmnb6fw2fjr86033z") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.12.0 (c (n "minimad") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "1vzglb05pqghc8l6d10vqkal0nqhgkh94jawwlhd1r14952kdc9q") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.13.0 (c (n "minimad") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "0s72q9h0807pc450n23nzis4s38m6ihiil5azj1bhj8f8c7n3i7n") (f (quote (("escaping") ("default" "escaping"))))))

(define-public crate-minimad-0.13.1 (c (n "minimad") (v "0.13.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "1lj5szpri8hjf38qrmg0i7vabp9b1rwakm5nly86a63d484dgid9") (f (quote (("escaping") ("default" "escaping"))))))

