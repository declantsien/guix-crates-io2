(define-module (crates-io mi ni miniqueue) #:use-module (crates-io))

(define-public crate-miniqueue-0.1.0 (c (n "miniqueue") (v "0.1.0") (h "0kb9n5jq3n9gal5vd77dgl29xj0n4ldix6lsvi374n2vkfp5va4b")))

(define-public crate-miniqueue-0.1.1 (c (n "miniqueue") (v "0.1.1") (h "1vz7pv4nlxdk0ps45fmd7yhjc1kxdfc4h6ly1dibgr4klxw5wrmn")))

(define-public crate-miniqueue-0.1.2 (c (n "miniqueue") (v "0.1.2") (h "0023rjap3sk5cs3j1br8icssv4zjmfxdg5fy3fbrd1qivdwwl3gl")))

