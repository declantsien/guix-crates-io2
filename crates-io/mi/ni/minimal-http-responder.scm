(define-module (crates-io mi ni minimal-http-responder) #:use-module (crates-io))

(define-public crate-minimal-http-responder-0.1.0 (c (n "minimal-http-responder") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0294vyr1caa55b39spprbi67xxxxwslpf2wbl4d8h8g0ccgcghcj")))

(define-public crate-minimal-http-responder-0.1.1 (c (n "minimal-http-responder") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0l5ygdib4j2w8p1sav8v3hmrqigmjn59f49vhkm7dq8iw9xcnv0w")))

(define-public crate-minimal-http-responder-0.1.2 (c (n "minimal-http-responder") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1bj6n45dh4vcmcja901cqjqd2vjd6yz1xb9gbrlbv12c9l6cp7kl")))

