(define-module (crates-io mi ni minihttp) #:use-module (crates-io))

(define-public crate-minihttp-0.1.0 (c (n "minihttp") (v "0.1.0") (d (list (d (n "minihttpse") (r "^0.1.2") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "041vj77al78rrpj1rzs3z72a14svqv0nr53dgl888qlrwwrwgi1r")))

(define-public crate-minihttp-0.1.1 (c (n "minihttp") (v "0.1.1") (d (list (d (n "minihttpse") (r "^0.1.2") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "161kj5zj915jb2yj219l4030h2p9i3wxjarhdq3ggk6x2dnhl506")))

(define-public crate-minihttp-0.1.2 (c (n "minihttp") (v "0.1.2") (d (list (d (n "minihttpse") (r "^0.1.2") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "1sn6i5ww6i1a6z7y97pfdjlxnrka5iwvw7yw3wxm7f04n6rzq3x5")))

(define-public crate-minihttp-0.1.3 (c (n "minihttp") (v "0.1.3") (d (list (d (n "minihttpse") (r "^0.1.3") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "02bmnq5nwrxmdknq06yz4inxn61ad058szy5m792726h8ci9aw41")))

(define-public crate-minihttp-0.1.4 (c (n "minihttp") (v "0.1.4") (d (list (d (n "minihttpse") (r "^0.1.4") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "0zcph1czq5sl7jn2r25xvg3qx7fd78iwiw9xn5schnsynsk96vc0")))

(define-public crate-minihttp-0.1.5 (c (n "minihttp") (v "0.1.5") (d (list (d (n "minihttpse") (r "^0.1.5") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "0y5mkjh7gy65bg9hnggp32avxbf463b3v9lskb4r2yl1d20iqjjg")))

(define-public crate-minihttp-0.1.6 (c (n "minihttp") (v "0.1.6") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "1iwra6lx61lj82agsq9xlq136mm2s89ziirwy6r5wljq8jxyib78")))

(define-public crate-minihttp-0.1.7 (c (n "minihttp") (v "0.1.7") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "1s8jg34rsfkf3zal248j2274y36644wii45hj3ycnpziq1zpxpcz")))

(define-public crate-minihttp-0.1.8 (c (n "minihttp") (v "0.1.8") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "0zc2myl7fzqd2ifyzxr4g8mhb6l7mmznv31fq7ma43bprn3mgacz")))

(define-public crate-minihttp-0.1.9 (c (n "minihttp") (v "0.1.9") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)))) (h "0dwd5hsv0azsqzf7m80j8i00vzpkghczxr3vdihgnvmbd2pwbmzj")))

