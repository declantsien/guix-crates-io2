(define-module (crates-io mi ni miniz-sys) #:use-module (crates-io))

(define-public crate-miniz-sys-0.0.1 (c (n "miniz-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 0)))) (h "0pmgf863bidzb5s1fzs8nji5ggxfrnrmgpzl4gswpyildd912h7h")))

(define-public crate-miniz-sys-0.0.2 (c (n "miniz-sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 0)))) (h "0mxdkh5gwlfj03nggqs8xmsxlkh2pxmis65sb6gablwnylyqv092")))

(define-public crate-miniz-sys-0.1.0 (c (n "miniz-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.1.0") (d #t) (k 1)))) (h "08rg90jqb7yvs9y4vi69y361ixsggq1r63vpm4qszw3lgzgk7qjs")))

(define-public crate-miniz-sys-0.1.1 (c (n "miniz-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.1.0") (d #t) (k 1)))) (h "1dkvf1dcrqm4lx2hq0i7xny5y2pcrc2b3d9bbay4hmsxngrqc0rg")))

(define-public crate-miniz-sys-0.1.2 (c (n "miniz-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0nx9vjm006647paksiqqz5mj2mx19i0xs7jvpv3msl0s1miv6gdr")))

(define-public crate-miniz-sys-0.1.3 (c (n "miniz-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1303i1sqv88sy1sd38yivnp6i13bydy8krqanyi1dnlh9vf43af3")))

(define-public crate-miniz-sys-0.1.4 (c (n "miniz-sys") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "15v3rx2bdfqp8wbcpk5n1vllpvgqrq7ldrh452ziyppvkcyf0jl2")))

(define-public crate-miniz-sys-0.1.5 (c (n "miniz-sys") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0611zhxzlidfilbxw091yyjwwbvx6qa42g37r1rpgjwaxnss9z2y")))

(define-public crate-miniz-sys-0.1.6 (c (n "miniz-sys") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "00iwq32a0vn3idqzabyhj47a7v2qhfxsmpsyi8b7b3i2j8xv029n")))

(define-public crate-miniz-sys-0.1.7 (c (n "miniz-sys") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m7dlggsxash0k5jkx576p556g9r8vnhyl9244gjxhq1g8rls7wx")))

(define-public crate-miniz-sys-0.1.8 (c (n "miniz-sys") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "131d4ar425d037alvhjppz5frp602fqacjxkiixp961wsdl3ryld")))

(define-public crate-miniz-sys-0.1.9 (c (n "miniz-sys") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09n7wbdrg0z2l3vcs22qps7310wf8bl4fxan5s3zlwb6cqbyxsi8")))

(define-public crate-miniz-sys-0.1.10 (c (n "miniz-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d72sxlnihgwyxnk6bmp5yrld9x5m94d6rvmxyh1kssahljf1730")))

(define-public crate-miniz-sys-0.1.11 (c (n "miniz-sys") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jg681nb6i6l47113x481aqz8d23md1q5dlr2sam569n43xyl003") (l "miniz")))

(define-public crate-miniz-sys-0.1.12 (c (n "miniz-sys") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00l2r4anm8g35x0js2zfdnwfbrih9m43vphdpb77c5ga3kjkm7hy") (l "miniz")))

