(define-module (crates-io mi ni minigrep_jimloop) #:use-module (crates-io))

(define-public crate-minigrep_jimloop-0.1.0 (c (n "minigrep_jimloop") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive" "color" "suggestions" "unicode" "wrap_help" "unstable-v5"))) (d #t) (k 0)))) (h "1wx5jhjqy9wzc4qbk2ah6jmafdlygl9sqa9hxi1rg08gqklhfwcw")))

