(define-module (crates-io mi ni minigrepd1234) #:use-module (crates-io))

(define-public crate-minigrepd1234-0.1.0 (c (n "minigrepd1234") (v "0.1.0") (h "1dxl7al2vi8f2fjshcwxs77nqgxbfj96r3xxfmlfzzkzqdv96x91")))

(define-public crate-minigrepd1234-0.1.1 (c (n "minigrepd1234") (v "0.1.1") (h "0wxlwj8r2bjpbg411f9bkn1maxiqcry33ni3gzb86v98a436gbcc")))

