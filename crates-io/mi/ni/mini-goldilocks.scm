(define-module (crates-io mi ni mini-goldilocks) #:use-module (crates-io))

(define-public crate-mini-goldilocks-0.1.0 (c (n "mini-goldilocks") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1rdb9b9vmziqs6600gp4p6lhfsw7wnw8ybwr05n8dhiil0r1as6x")))

(define-public crate-mini-goldilocks-0.1.1 (c (n "mini-goldilocks") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "198gg1gf9d238859lcw0q0q5i13mzlyan4wdlrvjahlqw6mpc3qi")))

