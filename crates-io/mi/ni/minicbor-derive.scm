(define-module (crates-io mi ni minicbor-derive) #:use-module (crates-io))

(define-public crate-minicbor-derive-0.1.0 (c (n "minicbor-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1ck5ghkm923l82rvv3pisjsca49khivrwrjlqnvy6qbzhxlypxsl")))

(define-public crate-minicbor-derive-0.1.1 (c (n "minicbor-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0br37yslljql6svzddbp08pqjf6c5hic8nx51gbc85bifhg0mk3i")))

(define-public crate-minicbor-derive-0.2.0 (c (n "minicbor-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0viqa297n4iaiaf18yxw57lmqygb8dp1dwghij2wnn0v4x4gdpvc")))

(define-public crate-minicbor-derive-0.2.1 (c (n "minicbor-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0z6n745v9d03iws1l8iwv4ykmd4z17syw7hv23x03qkwqbb12n9w")))

(define-public crate-minicbor-derive-0.3.0 (c (n "minicbor-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1vh3yvlds9b2p4i185n1vab18dzhvarl2wx34p6bbx7v8rxscdil")))

(define-public crate-minicbor-derive-0.4.0 (c (n "minicbor-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1p8fbfd727dqh6a2282c8mywsg8w6z1074cz14cilpihi4d2qk7k")))

(define-public crate-minicbor-derive-0.4.1 (c (n "minicbor-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0j6z0s8n5d6f1zhx2fk8mc7zsds8wc7swa5kwkrm56q9j0yvy562")))

(define-public crate-minicbor-derive-0.5.0 (c (n "minicbor-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0z60lgdlkdc0y7wgb8slyg083gzmcwmn8k23y7aviwgzvg06jd9b")))

(define-public crate-minicbor-derive-0.6.0 (c (n "minicbor-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1ffg3fvadkaazwxg8f68fzmbfrn3gvfvzg9dyqfyjpl3b4qin1rf")))

(define-public crate-minicbor-derive-0.6.1 (c (n "minicbor-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "07j8k7vbwib3i0pxqmdz1hp1wqpjlqb1hrvrcn7k0gwaw9ar82qk")))

(define-public crate-minicbor-derive-0.6.2 (c (n "minicbor-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0s1h17jz9f5xhr4azhsxfbppjqxgl1p09cqcx09klmrw8asiikhr")))

(define-public crate-minicbor-derive-0.6.3 (c (n "minicbor-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1j5c6w748lv7xv8d72h2rq7sazm7ci2nrldciyqk93nmhf49wavz")))

(define-public crate-minicbor-derive-0.6.4 (c (n "minicbor-derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "12670s0cs7a6md5qxdjxca6vicm239ijczkk0hwv34nhgj8rz6al")))

(define-public crate-minicbor-derive-0.7.0 (c (n "minicbor-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0xjv66c49x4cpg03xkvrsclbqz1gabmi9g0l8shnvv3msgwafcz2")))

(define-public crate-minicbor-derive-0.7.1 (c (n "minicbor-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0gn7166ddmnb79bvkzmqh90i713ypmn3b6a5ilfswfn24vn68lim")))

(define-public crate-minicbor-derive-0.7.2 (c (n "minicbor-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1rnfl9hwa6s5dwf47xk4dk6p9cpc7rzf861smdlgjrs6f6cgx062")))

(define-public crate-minicbor-derive-0.8.0 (c (n "minicbor-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "142j0090x75yqn5sv587cyz6mzjynrghd5vvxxvkrydl7xfrvxsq")))

(define-public crate-minicbor-derive-0.9.0 (c (n "minicbor-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0izxlyrxrbwa4d9jjw0iajkggr1l6xh9sm8p73fwcbc5agzjmhq3")))

(define-public crate-minicbor-derive-0.10.0-rc.1 (c (n "minicbor-derive") (v "0.10.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1i24hpixb5sz8hk4l1m2ba6d680vindk519q8fl1bx3g951mki18")))

(define-public crate-minicbor-derive-0.10.0 (c (n "minicbor-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "08sqirmak6bwm5xrv9rs51lgqa0wv06nsb5pz0w0k8g1vgvnzpb6")))

(define-public crate-minicbor-derive-0.10.1 (c (n "minicbor-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0vffyzmyz24dh0kvbi0q2pmg6i7v88xapg4kixng2l8846lyfsb4")))

(define-public crate-minicbor-derive-0.11.0 (c (n "minicbor-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0mpln9lbci4m4kh64k3gaa2zk0mgi6sqp4df6mvzpy6y0ignra6h")))

(define-public crate-minicbor-derive-0.12.0 (c (n "minicbor-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1vjhs3pf3sm547bb4xsdnqigcv9r1k29bgdpb8ynnnsvh0fgn246") (f (quote (("std") ("alloc"))))))

(define-public crate-minicbor-derive-0.13.0 (c (n "minicbor-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0rz235mzw8k1fd3dcb9lg1pv95fhnc8n7nqshj17knzg0sa80m0i") (f (quote (("std") ("alloc"))))))

(define-public crate-minicbor-derive-0.14.0 (c (n "minicbor-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "13j6pr2i23l6y99c3c6a9szrkqk7m9zgjrp8h9fcndpchsbaqacg") (f (quote (("std") ("alloc"))))))

(define-public crate-minicbor-derive-0.15.0 (c (n "minicbor-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("derive" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1hk50c4gp2ds4ry8f65y1syrs5qla4lp7rndm23dy1d4n4cw3gd6") (f (quote (("std" "alloc") ("alloc"))))))

