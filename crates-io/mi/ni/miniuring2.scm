(define-module (crates-io mi ni miniuring2) #:use-module (crates-io))

(define-public crate-miniuring2-0.1.0 (c (n "miniuring2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1z51jrandvjqk16v612pn3c5xgjzx5p2sz258d6arf5k49lchfa1") (y #t)))

(define-public crate-miniuring2-0.2.0 (c (n "miniuring2") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1zz2iqrl6rm7n0xc6ci2pkna0kz40jn4fqnni82xw7j0547a1dwd") (y #t)))

(define-public crate-miniuring2-0.3.0 (c (n "miniuring2") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0zidid9lkb6zmy6570f79zc0ip3ialm63i28igs2l3bj3zici0ds") (y #t)))

(define-public crate-miniuring2-1.0.0 (c (n "miniuring2") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0mcv7clsbyf8vacripd21d5rc408634qqpdw44qrf6z9kcch71i1") (y #t)))

(define-public crate-miniuring2-2.0.0 (c (n "miniuring2") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1kbv1vn4azy6cps0335v5g60c7aiiqbbgmlaky19w23q16a24pdk") (y #t)))

(define-public crate-miniuring2-2.1.0 (c (n "miniuring2") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0dr3pnnbb29vq7bnbzmpmy7gvnrkbanv47fygadk0r9y0pbf2qj2") (y #t)))

(define-public crate-miniuring2-2.2.0 (c (n "miniuring2") (v "2.2.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1dvhzkhkycvj28qcd9i5h6i519b0qfr8csy5a1c206annv23x240") (y #t)))

(define-public crate-miniuring2-3.0.0 (c (n "miniuring2") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "02j1h7l4cz16376xc3k3nk8x2scqsri1106a18a0mw9f8i5bh9hp") (y #t)))

