(define-module (crates-io mi ni minigrep_comum_by_rust) #:use-module (crates-io))

(define-public crate-minigrep_comum_by_rust-0.1.0 (c (n "minigrep_comum_by_rust") (v "0.1.0") (h "0qhkrij9p1v6kl5yh8jq9k7vn8dqmix5sh9p3k5vxwh89375nnx4")))

(define-public crate-minigrep_comum_by_rust-0.1.1 (c (n "minigrep_comum_by_rust") (v "0.1.1") (h "12m62hp2sxyc5qzs4pbiy6s931rdxhns7skbkm2h386c6gmqnff5")))

