(define-module (crates-io mi ni minijinja-autoreload) #:use-module (crates-io))

(define-public crate-minijinja-autoreload-0.22.0 (c (n "minijinja-autoreload") (v "0.22.0") (d (list (d (n "minijinja") (r "^0.22.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "16r7fzy0b5jv5ig5lma7lw3bh59xazpg4d88rna09f4bnd1jg2yd") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.22.1 (c (n "minijinja-autoreload") (v "0.22.1") (d (list (d (n "minijinja") (r "^0.22.1") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0wad3qaz2wihhi5zky99i90sy294zx48pq3y70fh94idd8ns5mhd") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.23.0 (c (n "minijinja-autoreload") (v "0.23.0") (d (list (d (n "minijinja") (r "^0.23.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "02p6d9v9cczklw0myl65mhh2jw71ly5g1mrqvbs2a8kg43zaxjn4") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.24.0 (c (n "minijinja-autoreload") (v "0.24.0") (d (list (d (n "minijinja") (r "^0.24.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "05kd14pvwfhcl176hagfl83847pbpvmn3wcl8xv298hgf8d3zh8k") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.25.0 (c (n "minijinja-autoreload") (v "0.25.0") (d (list (d (n "minijinja") (r "^0.25.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1szyhrs82srk5hzv9y27413m46pryznrw7f4gv9dlih5nf0kpk38") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.26.0 (c (n "minijinja-autoreload") (v "0.26.0") (d (list (d (n "minijinja") (r "^0.26.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0154qlnfg0xz6n3yk8dhdw7qfymnva3qqmxx3zssrzc4977pzm2c") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.27.0 (c (n "minijinja-autoreload") (v "0.27.0") (d (list (d (n "minijinja") (r "^0.27.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1hl3zgab3x8mal7q2929b355cwyad24nsq0yva5mj5kxld8n03ab") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.28.0 (c (n "minijinja-autoreload") (v "0.28.0") (d (list (d (n "minijinja") (r "^0.28.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0gh9l3wvabhazrpj9x924avvdrinrdb9qv5rl532srr2l707c82j") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.29.0 (c (n "minijinja-autoreload") (v "0.29.0") (d (list (d (n "minijinja") (r "^0.29.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "01kn869z1l6axqykqmsm2if9m2f273wcx2syg9cw2h2qsq4zyxav") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.0 (c (n "minijinja-autoreload") (v "0.30.0") (d (list (d (n "minijinja") (r "^0.30.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1j8a94slixpy6261qzg8ld3j5ga0q2jpsdldrn2bwf3mcc6bdrqk") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.1 (c (n "minijinja-autoreload") (v "0.30.1") (d (list (d (n "minijinja") (r "^0.30.1") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "08mgk5jzdmjyxwxi1vpyvwxwlhib43wkfm8zsqx17jpxx0bl1gyd") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.2 (c (n "minijinja-autoreload") (v "0.30.2") (d (list (d (n "minijinja") (r "^0.30.2") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0d5dvfz6s5fkambz1l5v2rmq0j851v4q8h80v37hq2mjgvkpqpnl") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.3 (c (n "minijinja-autoreload") (v "0.30.3") (d (list (d (n "minijinja") (r "^0.30.3") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1fiz5ardb3r52r5drzqyq1ym356ki3mail3s5gwhljfcanfiy6k0") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.4 (c (n "minijinja-autoreload") (v "0.30.4") (d (list (d (n "minijinja") (r "^0.30.4") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1smi4y5bvvkrva2xi8pj77mp6rp4gydb8rjpqc1cc9zrbkwxml1j") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.5 (c (n "minijinja-autoreload") (v "0.30.5") (d (list (d (n "minijinja") (r "^0.30.5") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0p2ylcln60id1n0jga4ir02m41zsvmz96bllar0d0w423bd0rg8j") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.6 (c (n "minijinja-autoreload") (v "0.30.6") (d (list (d (n "minijinja") (r "^0.30.6") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "116dx286zm3d1mwn81j3xpzg4mjjl9a6dvak3p2rblm5mqhz6lww") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.30.7 (c (n "minijinja-autoreload") (v "0.30.7") (d (list (d (n "minijinja") (r "^0.30.7") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0idfn1rnv03mix5dcwpwsk5a0xmhmwd95hjdx7jy1nxj622x8hdy") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.31.0 (c (n "minijinja-autoreload") (v "0.31.0") (d (list (d (n "minijinja") (r "^0.31.0") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "15n1mqgby3gqrrdgzrdkz4n8dhbi7a98kqqh4dl1ysh40vkzx5xn") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.31.1 (c (n "minijinja-autoreload") (v "0.31.1") (d (list (d (n "minijinja") (r "^0.31.1") (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0vbshnagmm3hz7iscdasi14al1c93apqabw4jz055v9cazgbxk0g") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.32.0 (c (n "minijinja-autoreload") (v "0.32.0") (d (list (d (n "minijinja") (r "^0.32.0") (k 0)) (d (n "minijinja") (r "^0.32.0") (f (quote ("source"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "18yl5g645d6shhym2bhgv1k0503f7fms6hwg9w39jxjp760nhdvb") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.32.1 (c (n "minijinja-autoreload") (v "0.32.1") (d (list (d (n "minijinja") (r "^0.32.1") (k 0)) (d (n "minijinja") (r "^0.32.1") (f (quote ("source"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "10sk3qm11yms7ih6hdz7vs17p9fg9jzvvqqxvhhfw6239aywjdml") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.33.0 (c (n "minijinja-autoreload") (v "0.33.0") (d (list (d (n "minijinja") (r "^0.33.0") (k 0)) (d (n "minijinja") (r "^0.33.0") (f (quote ("source"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1vzp6lxb74c1wav3vicm91w3kawwmagz0kn683swxlngpr4ljpz8") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-0.34.0 (c (n "minijinja-autoreload") (v "0.34.0") (d (list (d (n "minijinja") (r "^0.34.0") (k 0)) (d (n "minijinja") (r "^0.34.0") (f (quote ("source"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "14fgc4a0w7k6ppmil6jz6rlwa42aj2zjb2imac8nmwxq7psadjks") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.0-alpha.1 (c (n "minijinja-autoreload") (v "1.0.0-alpha.1") (d (list (d (n "minijinja") (r "^1.0.0-alpha.1") (k 0)) (d (n "minijinja") (r "^1.0.0-alpha.1") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1214c2fv67701jb0nw5k31kbwrgrxj8rv6y64az0c49zaqvmy2f9") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.0-alpha.2 (c (n "minijinja-autoreload") (v "1.0.0-alpha.2") (d (list (d (n "minijinja") (r "^1.0.0-alpha.2") (k 0)) (d (n "minijinja") (r "^1.0.0-alpha.2") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1afibw7gi19mgpp6c8a93f744i8n9zbhhsxjm7cfd6gjwzlgaszi") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.0-alpha.3 (c (n "minijinja-autoreload") (v "1.0.0-alpha.3") (d (list (d (n "minijinja") (r "^1.0.0-alpha.3") (k 0)) (d (n "minijinja") (r "^1.0.0-alpha.3") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0rcq0gn33f14j452i0v02v3mj6w6s9knnh06qgvkyhph79vbra4h") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.0-alpha.4 (c (n "minijinja-autoreload") (v "1.0.0-alpha.4") (d (list (d (n "minijinja") (r "^1.0.0-alpha.4") (k 0)) (d (n "minijinja") (r "^1.0.0-alpha.4") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1jwnrpk4ss6d0y210rwfh63cgw1yd042rk6xz124m9h0dlmq0yn0") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.0 (c (n "minijinja-autoreload") (v "1.0.0") (d (list (d (n "minijinja") (r "^1.0.0") (k 0)) (d (n "minijinja") (r "^1.0.0") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1bz7h3pyg6q575f4gr5ginykc3dqfk71v3bj4rxldhvlfj3lcxcp") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.1 (c (n "minijinja-autoreload") (v "1.0.1") (d (list (d (n "minijinja") (r "^1.0.1") (k 0)) (d (n "minijinja") (r "^1.0.1") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1dlgc25aq56fri9fp4cgp46dx1yh0gzacjw96kbn5zw02h9g5nfq") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.2 (c (n "minijinja-autoreload") (v "1.0.2") (d (list (d (n "minijinja") (r "^1.0.2") (k 0)) (d (n "minijinja") (r "^1.0.2") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1s7d8spppx35y8189839cfjq92g1qrxp426mszvcym8v10k5l7h3") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.3 (c (n "minijinja-autoreload") (v "1.0.3") (d (list (d (n "minijinja") (r "^1.0.3") (k 0)) (d (n "minijinja") (r "^1.0.3") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "14hyx94kn4064jhng9vmcg5bzbi49h66j7rrkzyih5xzvch8jiys") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.4 (c (n "minijinja-autoreload") (v "1.0.4") (d (list (d (n "minijinja") (r "^1.0.4") (k 0)) (d (n "minijinja") (r "^1.0.4") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1687zc9zs21bhc6pilkk7f0jjmf32i2n5nglg23sia53w47iwg6c") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.5 (c (n "minijinja-autoreload") (v "1.0.5") (d (list (d (n "minijinja") (r "^1.0.5") (k 0)) (d (n "minijinja") (r "^1.0.5") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0w1cs806c53zi1076p7lh6cld3r4bbq7c61zbb5qy0al67yfa6ys") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.6 (c (n "minijinja-autoreload") (v "1.0.6") (d (list (d (n "minijinja") (r "^1.0.6") (k 0)) (d (n "minijinja") (r "^1.0.6") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "11ibh5nxkwbdyhjjgbxb33k2xsvncbfycsy5vfz9kbbwipf3l9z1") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.7 (c (n "minijinja-autoreload") (v "1.0.7") (d (list (d (n "minijinja") (r "^1.0.7") (k 0)) (d (n "minijinja") (r "^1.0.7") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "18727xvpvn1i44fcjfpclm9rycw2rwj8xpk9g4cvxzga15b020vf") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.8 (c (n "minijinja-autoreload") (v "1.0.8") (d (list (d (n "minijinja") (r "^1.0.8") (k 0)) (d (n "minijinja") (r "^1.0.8") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "069dc730rq7n3gz907sg4v577snpq5v5zydsy0jhwndhwaw4irfv") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.9 (c (n "minijinja-autoreload") (v "1.0.9") (d (list (d (n "minijinja") (r "^1.0.9") (k 0)) (d (n "minijinja") (r "^1.0.9") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0shmlsv5693nmw8sslsh61f7lsflwzcl94fv4wsp18j6g1vnc61m") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.10 (c (n "minijinja-autoreload") (v "1.0.10") (d (list (d (n "minijinja") (r "^1.0.10") (k 0)) (d (n "minijinja") (r "^1.0.10") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1wzlbzx58nk6jfgfgr0di8si2q5g3wz5zscj67jilzi1b5j8bpna") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.11 (c (n "minijinja-autoreload") (v "1.0.11") (d (list (d (n "minijinja") (r "^1.0.11") (k 0)) (d (n "minijinja") (r "^1.0.11") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "15ym5b459sdiq1yjwa35b8h59snj949rps3j2p8vx68kczi3y1qd") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.12 (c (n "minijinja-autoreload") (v "1.0.12") (d (list (d (n "minijinja") (r "^1.0.12") (k 0)) (d (n "minijinja") (r "^1.0.12") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "16wc4xzya0jxhc2hsa8y1lxipdwg9qx2akxwsnylk2wbg5ibbkgv") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.13 (c (n "minijinja-autoreload") (v "1.0.13") (d (list (d (n "minijinja") (r "^1.0.13") (k 0)) (d (n "minijinja") (r "^1.0.13") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "123vplzpdxqzg0jqcw7m4d216cxjdw862gsjr7wy0lnbym3ss047") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.14 (c (n "minijinja-autoreload") (v "1.0.14") (d (list (d (n "minijinja") (r "^1.0.14") (k 0)) (d (n "minijinja") (r "^1.0.14") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "18m6yx59ra64ma1ifh0jvyg7qmxckr6j70nbq0q1sh9pk3j03s4l") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.15 (c (n "minijinja-autoreload") (v "1.0.15") (d (list (d (n "minijinja") (r "^1.0.15") (k 0)) (d (n "minijinja") (r "^1.0.15") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0zrhv7srv8ahjbc1zwd5w2g4mjl0fihras3gf07k6a8mz87bmisa") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.16 (c (n "minijinja-autoreload") (v "1.0.16") (d (list (d (n "minijinja") (r "^1.0.16") (k 0)) (d (n "minijinja") (r "^1.0.16") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0kppdq28j05gyf0blcksjkkrs03m4ig1b7mb257mdhza185iyic7") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.17 (c (n "minijinja-autoreload") (v "1.0.17") (d (list (d (n "minijinja") (r "^1.0.17") (k 0)) (d (n "minijinja") (r "^1.0.17") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1mpjqah9wr4c2i3bbw1s8n9nz86qv0k7623dkmrrqlspsdi6907w") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.18 (c (n "minijinja-autoreload") (v "1.0.18") (d (list (d (n "minijinja") (r "^1.0.18") (k 0)) (d (n "minijinja") (r "^1.0.18") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1qgxskvhy6f9nh07zd5nhw68r00j961nxk45zp4709y9cn6f2zh7") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.20 (c (n "minijinja-autoreload") (v "1.0.20") (d (list (d (n "minijinja") (r "^1.0.20") (k 0)) (d (n "minijinja") (r "^1.0.20") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1kfqnkjj5p2mjan0riil2n0xh8khhlqzg5ax6rwj2afni05rxr82") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-2.0.0-alpha.0 (c (n "minijinja-autoreload") (v "2.0.0-alpha.0") (d (list (d (n "minijinja") (r "^2.0.0-alpha.0") (k 0)) (d (n "minijinja") (r "^2.0.0-alpha.0") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "0wlkyj7byll5bixhrmaclaxxl54f0m0klxkwk0s5rxj5r010rfvk") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-1.0.21 (c (n "minijinja-autoreload") (v "1.0.21") (d (list (d (n "minijinja") (r "^1.0.21") (k 0)) (d (n "minijinja") (r "^1.0.21") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1g0757vmdi6di6jfyca86xn1v824y3gm5dakgbg3i7607bj4swsx") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-2.0.0 (c (n "minijinja-autoreload") (v "2.0.0") (d (list (d (n "minijinja") (r "^2.0.0") (k 0)) (d (n "minijinja") (r "^2.0.0") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1m036z5skq5q4km5mja96az3fr7cn7za7a9dwfq83jy0824hl5x1") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

(define-public crate-minijinja-autoreload-2.0.1 (c (n "minijinja-autoreload") (v "2.0.1") (d (list (d (n "minijinja") (r "^2.0.1") (k 0)) (d (n "minijinja") (r "^2.0.1") (f (quote ("loader"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0") (f (quote ("macos_fsevent"))) (o #t) (k 0)))) (h "1bnqfd4ghjy4abr35k58h8lljvn5aqhrwdlf0lk046ws4pghqyhq") (f (quote (("watch-fs" "notify") ("default" "watch-fs")))) (r "1.61")))

