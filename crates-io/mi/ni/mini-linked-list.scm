(define-module (crates-io mi ni mini-linked-list) #:use-module (crates-io))

(define-public crate-mini-linked-list-0.1.0 (c (n "mini-linked-list") (v "0.1.0") (h "0mjhlf6kqxzdy7a8vy3ijkssad4gprg2g6zx1nyhnci2zp8klaxd") (y #t)))

(define-public crate-mini-linked-list-0.1.1 (c (n "mini-linked-list") (v "0.1.1") (h "1chx3rrw341w8lnv1i7grs8sj655h9lrs0jgiwnw63cvbl1qnc40")))

(define-public crate-mini-linked-list-0.1.2 (c (n "mini-linked-list") (v "0.1.2") (h "10a4z5yh3gqgkqm9h7fj4zj2zc78jmylmal1l1igr1xdbvrwqzzs")))

(define-public crate-mini-linked-list-0.1.3 (c (n "mini-linked-list") (v "0.1.3") (h "0x7qgv0i4ivhn32p0vk2bvpymi2xjk51sfrxa30s7ish8c26kifn")))

