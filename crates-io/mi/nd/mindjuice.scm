(define-module (crates-io mi nd mindjuice) #:use-module (crates-io))

(define-public crate-mindjuice-0.1.0 (c (n "mindjuice") (v "0.1.0") (h "031ggy5jl9l84zfdylymyvlizbgmxap6rrjf6hglx941gqbcvqma")))

(define-public crate-mindjuice-0.1.1 (c (n "mindjuice") (v "0.1.1") (h "0wwbbmwm2g029xghb307ja7akjl85c9zvj8crr7a8wdjnncs6zwl")))

