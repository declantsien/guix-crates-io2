(define-module (crates-io mi nd mindfuck) #:use-module (crates-io))

(define-public crate-mindfuck-1.0.0 (c (n "mindfuck") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "190987n6gqn7pywgbscygng1fk8jyxn6j0cyzph94qamwi2r5jcp")))

(define-public crate-mindfuck-1.0.1 (c (n "mindfuck") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sk0n8qqc6ja47dsajgi5gjcvbs9x5a642a9vfajdjziycfm2bkd")))

(define-public crate-mindfuck-1.0.2 (c (n "mindfuck") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lbp4mh4dhhsvvsnanmrcv6n1n7ys9dv0137fh8jiqxfd8z47kfa")))

