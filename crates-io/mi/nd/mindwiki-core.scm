(define-module (crates-io mi nd mindwiki-core) #:use-module (crates-io))

(define-public crate-mindwiki-core-0.1.0 (c (n "mindwiki-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yew") (r "^0.17.2") (d #t) (k 0)))) (h "1sgx4738i926dw9wfarsm72bskr75dkhj3hajfjdiwyd8q22lcdv")))

