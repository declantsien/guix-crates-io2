(define-module (crates-io mi nd mindwiki) #:use-module (crates-io))

(define-public crate-mindwiki-0.1.0 (c (n "mindwiki") (v "0.1.0") (d (list (d (n "mindwiki-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "10r79y9rgzn6xcn9hvwqrgczr9lrzi6gzs3m2xl8y6ypq2r3by8x")))

