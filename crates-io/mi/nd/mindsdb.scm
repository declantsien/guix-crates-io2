(define-module (crates-io mi nd mindsdb) #:use-module (crates-io))

(define-public crate-mindsdb-0.1.0 (c (n "mindsdb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1bbmaj3k5ap396vyvq222nmbpscwclk1fzzp6yxp3r2nazwv9f4j")))

