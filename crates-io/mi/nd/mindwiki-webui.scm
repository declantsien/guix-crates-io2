(define-module (crates-io mi nd mindwiki-webui) #:use-module (crates-io))

(define-public crate-mindwiki-webui-0.1.0 (c (n "mindwiki-webui") (v "0.1.0") (d (list (d (n "mindwiki-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yew") (r "^0.17.2") (d #t) (k 0)))) (h "0j1g5phslvb33hah10936yrnfnn3jzbbysqcz5l87dlhv9lagnmk")))

