(define-module (crates-io mi nd mindblown) #:use-module (crates-io))

(define-public crate-mindblown-0.1.0 (c (n "mindblown") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)))) (h "0ic8q4drfwyv0nra2jcbx7qsgam20g7qw90a16nypshxsvl1zmq9")))

(define-public crate-mindblown-0.2.0 (c (n "mindblown") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)))) (h "0lk94fqwp9c415qa17lk9hyw79ny8nnkw2yvpzmc9f4kzwmc68kh")))

(define-public crate-mindblown-0.2.1 (c (n "mindblown") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)))) (h "1wld4qw55jjvppkf1c3v29xpi809c1rfhm5736bhmg3hm6cndmcb")))

(define-public crate-mindblown-0.3.0 (c (n "mindblown") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)))) (h "029m2r4y7m1spfkizxsg183ngg69d2lalsawgxdia4vldqf73xlb")))

(define-public crate-mindblown-0.3.1 (c (n "mindblown") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)))) (h "10an5dlfa35mkdbgaiygkjaq4a78b2bj8312zs9vpqiv7lg1g3zj")))

(define-public crate-mindblown-0.3.2 (c (n "mindblown") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0jbbs3ysqw5jr67mifcp8s04k7dg1rziw416k29a6v9y8v9z7s1l")))

(define-public crate-mindblown-0.4.0 (c (n "mindblown") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "051f2b0gvbk4vq9z6l6q7lj19dfx388yipzjlbs2v5g669ra66b3")))

(define-public crate-mindblown-0.4.1 (c (n "mindblown") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1rbpdgma16mr9w5cvwy9v1lzz7pk8qrrw2ghpbqbnffsdxic9nw0")))

