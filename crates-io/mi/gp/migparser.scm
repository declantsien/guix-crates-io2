(define-module (crates-io mi gp migparser) #:use-module (crates-io))

(define-public crate-migparser-0.1.0 (c (n "migparser") (v "0.1.0") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0f55l68n3zd4jpc9pxmizd72jpv8869w0hdw71rj4224l4lcmgq3") (y #t)))

(define-public crate-migparser-0.1.1 (c (n "migparser") (v "0.1.1") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0gyk0f39pv8lyh5k6lvsh6iwdiwz5w3hhp0pbdbff4a4njk7nh2c") (y #t)))

(define-public crate-migparser-0.1.2 (c (n "migparser") (v "0.1.2") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "136asxqiw7hf0c19ahfkzy5zib2dxdh1lp6fhwcf6p1029rn7g2b") (y #t)))

(define-public crate-migparser-0.1.3 (c (n "migparser") (v "0.1.3") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "050sxm9qj1mrxnpg7q0q55wr3mv443dsvi540j1mnnnp78ymp48x") (y #t)))

(define-public crate-migparser-0.1.4 (c (n "migparser") (v "0.1.4") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "18ss326izmfj1w4llbq8gfnacm3xrdngamq8rc1ihmp7qxv5j34d") (y #t)))

(define-public crate-migparser-0.1.5 (c (n "migparser") (v "0.1.5") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0x2sbgy8jlgc3jfxw78qfrmi4lpysj9rspzf3a8a1c8fp6rnh0f3") (y #t)))

(define-public crate-migparser-0.1.6 (c (n "migparser") (v "0.1.6") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1zw32g7dnk2myayl9k5mcfn7ll44pp8gsl4mjglv2gi5yinmxl33")))

(define-public crate-migparser-0.2.0 (c (n "migparser") (v "0.2.0") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "150v4k1ckinzw0l3iw4h8w2f4ggaxrjclm66h7zbzijzdrv95am6")))

(define-public crate-migparser-0.2.1 (c (n "migparser") (v "0.2.1") (d (list (d (n "migformatting") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "19n482v0216y5laxwwv7sbxzs2jr9fhxhzngb6qfqbjbr94mf92c")))

