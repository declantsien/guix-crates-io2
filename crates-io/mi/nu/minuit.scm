(define-module (crates-io mi nu minuit) #:use-module (crates-io))

(define-public crate-minuit-0.1.0 (c (n "minuit") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.35.1") (d #t) (k 0)))) (h "1lpkmlrgwlp3njhh8vay3gy4vnm95dg92fhlw7b1qrw3b8p9k0mw") (y #t)))

(define-public crate-minuit-0.2.0 (c (n "minuit") (v "0.2.0") (d (list (d (n "sdl2") (r "^0.35.1") (f (quote ("ttf"))) (k 0)))) (h "16i7qvgijqr33j6kwibqh64kz5dkic5j1vmgvbbscgz3qlj0kf2z") (y #t)))

