(define-module (crates-io mi nu minuteurs) #:use-module (crates-io))

(define-public crate-minuteurs-0.1.0 (c (n "minuteurs") (v "0.1.0") (h "1vbkl5izxilckr5g0iffb07zl62vic07ccp18racc7pjdzcpahi0")))

(define-public crate-minuteurs-1.0.0 (c (n "minuteurs") (v "1.0.0") (h "1hapkw969cazmbag96qx9iyiigzf0h72payrlqngc9j6y55r0l0l")))

(define-public crate-minuteurs-1.0.1 (c (n "minuteurs") (v "1.0.1") (h "1yj83wzkwnrk6zbsvzwa6ljjz138nampy4cm9gcy0j5y8x7gx4fj")))

