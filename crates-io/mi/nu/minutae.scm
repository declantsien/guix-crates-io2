(define-module (crates-io mi nu minutae) #:use-module (crates-io))

(define-public crate-minutae-0.1.0 (c (n "minutae") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jfvzbi4mm1bdlgwqls544x9cwa5w19k928qpgpa2n21gjavzqlf")))

(define-public crate-minutae-0.2.0 (c (n "minutae") (v "0.2.0") (d (list (d (n "rayon") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yb92q914iaifzpsqav2ks6milbwhksk4sv88a03i81wlxyd04id")))

(define-public crate-minutae-0.2.1 (c (n "minutae") (v "0.2.1") (d (list (d (n "rayon") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0msi93j4g17w0d428wadkrfdl82wqrz602r3cdh5xy36xf011020")))

(define-public crate-minutae-0.2.2 (c (n "minutae") (v "0.2.2") (d (list (d (n "num_cpus") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0slr1v5050shvas99qszxw1xr8jnn065zmyq2jmcw6wxifzg0c8c")))

