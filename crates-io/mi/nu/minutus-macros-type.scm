(define-module (crates-io mi nu minutus-macros-type) #:use-module (crates-io))

(define-public crate-minutus-macros-type-0.1.1-alpha.2 (c (n "minutus-macros-type") (v "0.1.1-alpha.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q2b9w1lna6385ylpzrgwgcbma2h0390gvqhhi2hhnkb1brqchq0") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.3 (c (n "minutus-macros-type") (v "0.1.1-alpha.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01w85sqgwrl71ammacs4cqdawacyscw711plxzygrfjgxr9xiczf") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.4 (c (n "minutus-macros-type") (v "0.1.1-alpha.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08rc0cqkg6lvn77l6hri5ihn8h6l8z9hf3h6dzmd1rz2y4032929") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.5 (c (n "minutus-macros-type") (v "0.1.1-alpha.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05zmfx2x09ip1n7zfknw5f38y046gk90k2mvi2x5mqy8fnwscw2f") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.6 (c (n "minutus-macros-type") (v "0.1.1-alpha.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "014sis821b7bad1fzrvhyanax5nsippif0gbbwgxmdb5jwll5hn5") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.7 (c (n "minutus-macros-type") (v "0.1.1-alpha.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04x4xrpd8a491s3r8s1wv4bv7axsd3v0kc5d10hl7vrvxdki2n3w") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.8 (c (n "minutus-macros-type") (v "0.1.1-alpha.8") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "072q9jh0x6n26wdw12mxk50d63v3f1qis2hyh2i5qvyhd0rii9s2") (y #t)))

(define-public crate-minutus-macros-type-0.1.1-alpha.9 (c (n "minutus-macros-type") (v "0.1.1-alpha.9") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pj6l2d1knpbag2b30ipcssw32kr77h27r0r89lln4xgwzflhqxr") (y #t)))

