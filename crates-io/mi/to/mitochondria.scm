(define-module (crates-io mi to mitochondria) #:use-module (crates-io))

(define-public crate-mitochondria-0.1.0 (c (n "mitochondria") (v "0.1.0") (h "0myyfw90jzlrdp059g8riwwq874xxy415d546b3di0xj1cwaqaai")))

(define-public crate-mitochondria-0.1.1 (c (n "mitochondria") (v "0.1.1") (h "1i62vymw1bnm9admnpdqjjyrdsxaa22i4vp7pbw6b5lmxw5h36va")))

(define-public crate-mitochondria-0.2.0 (c (n "mitochondria") (v "0.2.0") (h "1djxs2i5ids5q8y4bmq6b866r4r4yzv6p1b0kbxjkfdh65bv1fg8")))

(define-public crate-mitochondria-0.2.1 (c (n "mitochondria") (v "0.2.1") (h "1ligk0agw65nz8vdd4ri6b8ldyna8wx9nr2acximvkf21yg1psix")))

(define-public crate-mitochondria-0.3.0 (c (n "mitochondria") (v "0.3.0") (h "17r9h166halg7s2iy5i4vv4h220g747yvc4mdzxnd13m5r1bbjmi")))

(define-public crate-mitochondria-0.3.1 (c (n "mitochondria") (v "0.3.1") (h "1hlzyvd2nni4jik9b634b3gvirxryl9fj4k0f52b30rpn18s1li9")))

(define-public crate-mitochondria-0.3.2 (c (n "mitochondria") (v "0.3.2") (h "0asl5cd82b9l34l8rj2cgi0jkynxyfj49gwva933yp011miivx3r")))

(define-public crate-mitochondria-1.0.0 (c (n "mitochondria") (v "1.0.0") (h "1z60da61149xkb29phpxh266md4j7rfc59l4y98qjp3azrmhs6ds")))

(define-public crate-mitochondria-1.0.1 (c (n "mitochondria") (v "1.0.1") (h "0f01m1d951ixp4i54n82h42ilx53231dfhs3d6v43g7lxqsvswr2")))

(define-public crate-mitochondria-1.1.0 (c (n "mitochondria") (v "1.1.0") (h "1rrl5nsrf9dnqz5x0npixs529cvzx15x7ix241iwkmr2fqyw5i2j") (f (quote (("no_std"))))))

(define-public crate-mitochondria-1.1.1 (c (n "mitochondria") (v "1.1.1") (h "1rz71mdymhxyzigkpwmvr07m6hngvvna6bx26kr9y27ixy6fgmm3") (f (quote (("no_std"))))))

(define-public crate-mitochondria-1.1.2 (c (n "mitochondria") (v "1.1.2") (h "0vwgmq55z77db9fa4ypmaq4h0zggji5q6zw07g1k3pvig2ifrqwx") (f (quote (("no_std"))))))

