(define-module (crates-io mi to mitosis) #:use-module (crates-io))

(define-public crate-mitosis-0.1.0 (c (n "mitosis") (v "0.1.0") (d (list (d (n "ipc-channel") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m44fpckw9lfbql0x7c9fa6546fpm5x5112crs3fd8ap4sgp4zgp")))

(define-public crate-mitosis-0.1.1 (c (n "mitosis") (v "0.1.1") (d (list (d (n "ipc-channel") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gnm5741j5rcf3a82m672170awhszdaxih4fbkr7vgi0vzhxy4kh")))

