(define-module (crates-io mi st mistletoe-macros) #:use-module (crates-io))

(define-public crate-mistletoe-macros-0.1.1 (c (n "mistletoe-macros") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 0)) (d (n "mistletoe-api") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 0)))) (h "148hyqhyckngblzyx1n386g3k2r9sgpfvrhmhx8vimxw4z4drwvv")))

(define-public crate-mistletoe-macros-0.1.2 (c (n "mistletoe-macros") (v "0.1.2") (d (list (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 0)) (d (n "mistletoe-api") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "unindent") (r "^0.2") (d #t) (k 0)))) (h "0q9kbls5s8s5ab0prmrrydfvaf40jqlmfklbhs56jhbzxmq2dma1")))

