(define-module (crates-io mi st mistralai) #:use-module (crates-io))

(define-public crate-mistralai-0.1.0 (c (n "mistralai") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)))) (h "1ykamnz37fa2dxf2jq2158vb4cd2ycxdz15fsm80jpq5qsjvvbsd")))

