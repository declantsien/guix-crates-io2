(define-module (crates-io mi st mist-run-utils) #:use-module (crates-io))

(define-public crate-mist-run-utils-0.1.0 (c (n "mist-run-utils") (v "0.1.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)))) (h "1a9ms1fz2kndcjlx0lv555iaipkdfzcxhlq4m9zalgg6vzls5vn3")))

(define-public crate-mist-run-utils-1.0.0 (c (n "mist-run-utils") (v "1.0.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)))) (h "1q2sd16mfz3yrcgbz3bcqf88n9k1mjnx87d59wsvmnh3f6j6nykq") (y #t)))

(define-public crate-mist-run-utils-1.0.1 (c (n "mist-run-utils") (v "1.0.1") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)))) (h "1llc96v609xq0xx52kqfd4w0icpz92c2biq8ih7ij7r9br6la867")))

(define-public crate-mist-run-utils-1.1.0 (c (n "mist-run-utils") (v "1.1.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)))) (h "0b18lyw23wn33k6hhmjsys1lhvlwvdv6lhhzvgrrwnzsxdqmhj2w")))

(define-public crate-mist-run-utils-1.1.1 (c (n "mist-run-utils") (v "1.1.1") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)))) (h "1sgghxjbb2lc1wdgrycpsgf6hkd4vplk37lcr3k0p4wp5qa4b3hm")))

(define-public crate-mist-run-utils-2.0.0 (c (n "mist-run-utils") (v "2.0.0") (d (list (d (n "quick-xml") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "06fckaws07a33nry1kas3akv3n07vhibk1sxgwcg9d7rknawvxbx") (f (quote (("msf" "ron" "serde") ("lss" "quick-xml") ("default"))))))

(define-public crate-mist-run-utils-2.1.2 (c (n "mist-run-utils") (v "2.1.2") (d (list (d (n "quick-xml") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "1aliwmx8v9ks908xj3ii5jf4xmrg7vpbc91yrp81vinzsgzs4sxq") (f (quote (("msf" "ron" "serde") ("lss" "quick-xml") ("default"))))))

