(define-module (crates-io mi st mistletoe-bind) #:use-module (crates-io))

(define-public crate-mistletoe-bind-0.1.1 (c (n "mistletoe-bind") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "mistletoe-api") (r "^0.1.1") (d #t) (k 0)) (d (n "mistletoe-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0hnawiflm3dlld3pix9ly98yqdps74bn7a8d34jb2s1l9g83ya5l")))

(define-public crate-mistletoe-bind-0.1.2 (c (n "mistletoe-bind") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "mistletoe-api") (r "^0.1.2") (d #t) (k 0)) (d (n "mistletoe-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0dgwkm7q78j7l46nrj7fwwfw965hhczprxk7vmakq4c99q6p35fg")))

