(define-module (crates-io mi st mist-pdk) #:use-module (crates-io))

(define-public crate-mist-pdk-1.0.0 (c (n "mist-pdk") (v "1.0.0") (d (list (d (n "built") (r "^0.7") (d #t) (k 1)) (d (n "mist-core") (r "^2.0") (f (quote ("state"))) (k 0)))) (h "0y5mp8xv583iwg2h9j0zxb6ns8fvjf3649i0kdpa66pllx7l45fp") (f (quote (("guest") ("default" "guest"))))))

(define-public crate-mist-pdk-1.1.0 (c (n "mist-pdk") (v "1.1.0") (d (list (d (n "built") (r "^0.7") (d #t) (k 1)) (d (n "mist-core") (r "^2.0") (f (quote ("state"))) (k 0)))) (h "0klpq0szqcn47nd0afjbgrf8x4bqzr6i9ip2yy3cibn8lisfs8y7") (f (quote (("guest") ("default" "guest"))))))

