(define-module (crates-io mi st misty-vm-macro) #:use-module (crates-io))

(define-public crate-misty-vm-macro-0.1.0 (c (n "misty-vm-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0016pvmgg247acqxqmn8wcvi0h224ppi39z21lqnbwd3sfxgynlb")))

(define-public crate-misty-vm-macro-0.1.1 (c (n "misty-vm-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ysqfax085flk3csxs3mv0nmyb7lmbasvklz36wwddfw7ipvvhla")))

(define-public crate-misty-vm-macro-0.1.2 (c (n "misty-vm-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00lc77ljsm39dq4ypmpa35zcdkjnqmgd870yi71k822i19j79b1j")))

(define-public crate-misty-vm-macro-0.1.3 (c (n "misty-vm-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sxnqv51cq46ym8mxk7v21hnpf569i78y5j6cbdv1d7ljpnn8nxb")))

