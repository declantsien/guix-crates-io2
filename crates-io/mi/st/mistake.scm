(define-module (crates-io mi st mistake) #:use-module (crates-io))

(define-public crate-mistake-0.1.0 (c (n "mistake") (v "0.1.0") (h "0zxjg3xqb7y6jcpyb4ljhaxag05khpizifv9hwj3ygq8f4d3v8qg")))

(define-public crate-mistake-0.1.1 (c (n "mistake") (v "0.1.1") (h "1xglnxra4c5978wyn3m07w9s1c3z2mprwm14gqnxh3s43xncrcca")))

