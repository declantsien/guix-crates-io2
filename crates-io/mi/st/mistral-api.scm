(define-module (crates-io mi st mistral-api) #:use-module (crates-io))

(define-public crate-mistral-api-0.0.1 (c (n "mistral-api") (v "0.0.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "~0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "136fg5if2xcja019b37axjbq0mai1msyf8i2vwv44b3mi3717jab")))

