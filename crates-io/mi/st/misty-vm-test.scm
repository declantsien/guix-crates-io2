(define-module (crates-io mi st misty-vm-test) #:use-module (crates-io))

(define-public crate-misty-vm-test-0.1.0 (c (n "misty-vm-test") (v "0.1.0") (d (list (d (n "misty-vm") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 2)))) (h "1qgmjga9pwc5jfyjnvyyz3rspzaxjdn0c93z427kdkvv0sy381y5")))

(define-public crate-misty-vm-test-0.1.1 (c (n "misty-vm-test") (v "0.1.1") (d (list (d (n "misty-vm") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 2)))) (h "0qqv9bqi5d7d5w5naxgdx0s6rv24rl9k6y9ks2f94k665ah199n4")))

(define-public crate-misty-vm-test-0.1.2 (c (n "misty-vm-test") (v "0.1.2") (d (list (d (n "misty-vm") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 2)))) (h "1zvida8wprnijr3lmcppgxxph23jrv54zq7srq7hzv6k55j2h5q0")))

(define-public crate-misty-vm-test-0.1.3 (c (n "misty-vm-test") (v "0.1.3") (d (list (d (n "misty-vm") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 2)))) (h "1w4h2p136hb7sbh4l6as3f86lrva1y62w0qn927lzj2bvbih995q")))

(define-public crate-misty-vm-test-0.1.4 (c (n "misty-vm-test") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "misty-vm") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.0") (d #t) (k 2)))) (h "105w5axyjzinshgm2sakv3pzv3wlr7flj794416bbg8b9gdhrmqd")))

