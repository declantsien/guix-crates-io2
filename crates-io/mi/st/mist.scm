(define-module (crates-io mi st mist) #:use-module (crates-io))

(define-public crate-mist-1.2.7 (c (n "mist") (v "1.2.7") (d (list (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("ttf" "image"))) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (k 0)) (d (n "tinyfiledialogs") (r "^3.3") (d #t) (k 0)))) (h "019g45mk0f9yc1528cjm2y53cxz7ybaw4y495w7r9s7nib5inmiy") (y #t)))

(define-public crate-mist-1.2.8 (c (n "mist") (v "1.2.8") (d (list (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("ttf" "image"))) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (k 0)) (d (n "tinyfiledialogs") (r "^3.3") (d #t) (k 0)))) (h "11igbfg852clkckjf3z5jx4a9x74kwqs1mgasgva4qwfg7cjy64l")))

(define-public crate-mist-1.3.2 (c (n "mist") (v "1.3.2") (d (list (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("ttf" "image"))) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (k 0)) (d (n "tinyfiledialogs") (r "^3.3") (d #t) (k 0)))) (h "040d7cv7kalyzwi8rjz0adkspm0f07jrdvpl3184jmjwh1hvfa8d")))

