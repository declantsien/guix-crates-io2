(define-module (crates-io mi st mistletoe-api) #:use-module (crates-io))

(define-public crate-mistletoe-api-0.1.0 (c (n "mistletoe-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "14rbj8423bbcp32vc2kz41dab6dc8iqbm9p0qrssjrifsav48fg3") (y #t)))

(define-public crate-mistletoe-api-0.1.1 (c (n "mistletoe-api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "07db9rkk8472niq5wmkca1jglhw6x3ksbk0cj1rv74pfr18j68l7")))

(define-public crate-mistletoe-api-0.1.2 (c (n "mistletoe-api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "191ns2733wndqpd6izw06w0aq4cw0alhw2j5nsrr9c4ii7gwc69s")))

