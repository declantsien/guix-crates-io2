(define-module (crates-io mi xe mixed_ref) #:use-module (crates-io))

(define-public crate-mixed_ref-0.1.0 (c (n "mixed_ref") (v "0.1.0") (h "0bjsx8sg5ss1q696qnrvzaxk2dy2d93nmzgvprf3m1ksyr1wfg0m") (f (quote (("std") ("default" "std"))))))

(define-public crate-mixed_ref-0.1.1 (c (n "mixed_ref") (v "0.1.1") (h "0ml05azfprmkx3gby50zx6pniv7jd4gyfjzqb0m1bqnyl1b2qh9f") (f (quote (("std") ("default" "std"))))))

