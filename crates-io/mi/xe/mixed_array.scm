(define-module (crates-io mi xe mixed_array) #:use-module (crates-io))

(define-public crate-mixed_array-0.1.0 (c (n "mixed_array") (v "0.1.0") (d (list (d (n "mixed_type_proc") (r "^0.1.0") (d #t) (k 0)))) (h "1dmnj4fr3c602rzxr24f6bfvz0y3vnm5c7gpxkfy0hsj26cbr0fm")))

(define-public crate-mixed_array-0.1.1 (c (n "mixed_array") (v "0.1.1") (d (list (d (n "mixed_type_proc") (r "^0.1.0") (d #t) (k 0)))) (h "1ips5kfn643rci721r95s95yxas3s0ssjnghzy4ymj7hphn18xj0")))

