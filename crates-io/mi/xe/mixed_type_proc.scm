(define-module (crates-io mi xe mixed_type_proc) #:use-module (crates-io))

(define-public crate-mixed_type_proc-0.1.0 (c (n "mixed_type_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0a310mi06dj1mlz9sx3as3wb7kyd38jfx7smwvi2b58zmk97ggkf")))

