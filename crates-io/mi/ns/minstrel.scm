(define-module (crates-io mi ns minstrel) #:use-module (crates-io))

(define-public crate-minstrel-0.1.0 (c (n "minstrel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1rlbkvqzb5as0xmr695m4zy33a96hwaicrbv583c9hpdyim9kq31")))

(define-public crate-minstrel-0.1.1 (c (n "minstrel") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "01jhkwgpfhgy49azz3xw12ba5qsf1k5sj4j2ivdsds0f345qgxbc")))

(define-public crate-minstrel-0.2.0 (c (n "minstrel") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1aqyi3m5xxi21if40wmiaass77ngd0rj3pddi3422bjdzjyfxvf7")))

(define-public crate-minstrel-0.2.1 (c (n "minstrel") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "027j7y0jr3pdrcbwjwjchl0f847z3wlzibfm5hqrs5iq2530qpdg")))

(define-public crate-minstrel-0.3.0 (c (n "minstrel") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1mnill6mdalp3bg7z9sk4h0x5vwfrrpkb0x75v2pwk54rfyxbk8v")))

(define-public crate-minstrel-0.3.1 (c (n "minstrel") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "13fs655gs70n0zk1wryf96j7cs55kfhdpl5q4q78ndwk216nmq6f")))

(define-public crate-minstrel-0.3.2 (c (n "minstrel") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "12v28465yqbfwm9y1cya9h8b4gyxy5ys1i3hh2fmsi2sa5picxck")))

