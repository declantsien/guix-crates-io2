(define-module (crates-io mi ns minsize) #:use-module (crates-io))

(define-public crate-minsize-0.1.0 (c (n "minsize") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0y7bnxg85awmvf97vn4b3dh9sy2ppmvb5b3mfniw6kak4cfxs336")))

(define-public crate-minsize-0.1.1 (c (n "minsize") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1rqc37lwfl4l1kplgjxagggjwsr4rnmhcyn9wffzwy7aavga97jw")))

(define-public crate-minsize-0.1.2 (c (n "minsize") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0gn7imjzn5458fkz6py2wj6kvlxlfb841nja7mg7xr10nb2gh0py")))

