(define-module (crates-io mi ns minspan) #:use-module (crates-io))

(define-public crate-minspan-0.1.0 (c (n "minspan") (v "0.1.0") (h "0i2fhrx660gc9nnvaxaiy2j0jss1d352ms964qdywvxaqc0ymdrs")))

(define-public crate-minspan-0.1.1 (c (n "minspan") (v "0.1.1") (h "1s7lh0ryq0kk6sm6z5f2ikqq437xca0gzc61ds80pbh8qdxa2s8j")))

