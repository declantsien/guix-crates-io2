(define-module (crates-io mi nl minlambda) #:use-module (crates-io))

(define-public crate-minlambda-0.1.0 (c (n "minlambda") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("rustls-tls"))) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "1ryrgfymjgcjcjmnkvnqmahrxs2k2f5wqf7pvbrc3n0c13vxxj1r")))

(define-public crate-minlambda-0.2.0 (c (n "minlambda") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("rustls-tls"))) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "0qfn2jgpwgzc0fp5fcgc2v18h3jm1xkdpghhh63wcm147zix8508")))

