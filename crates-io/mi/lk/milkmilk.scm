(define-module (crates-io mi lk milkmilk) #:use-module (crates-io))

(define-public crate-milkmilk-0.1.0 (c (n "milkmilk") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0qj8n0djqxrmkxqb3s214y3s1ickv4xis1vf3a66w0pfr7qadnfk")))

(define-public crate-milkmilk-0.1.1 (c (n "milkmilk") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1y9h9zzy3k5wf90wc9xazcqmm861xhrav4c6wyq4izk2906apisq") (y #t)))

