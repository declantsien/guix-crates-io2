(define-module (crates-io mi lk milkpq) #:use-module (crates-io))

(define-public crate-milkpq-0.1.0 (c (n "milkpq") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "ref_thread_local") (r "^0.0.0") (d #t) (k 0)))) (h "141zh8b5g7q7vn969vxcarlrrfm45rs46npdvv6w9c9kfcqb38v4")))

