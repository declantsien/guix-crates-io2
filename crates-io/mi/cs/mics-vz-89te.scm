(define-module (crates-io mi cs mics-vz-89te) #:use-module (crates-io))

(define-public crate-mics-vz-89te-0.1.0 (c (n "mics-vz-89te") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0j6wr76cgb10xcngbcn14b66gvaj4pkg3xlg6dbw77g1mzmnhfry")))

(define-public crate-mics-vz-89te-0.1.1 (c (n "mics-vz-89te") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "047k138ipwnydsdpr6hwj63h5i4iqp9z63g8nl9xi2xnyjmmwina")))

(define-public crate-mics-vz-89te-0.2.0 (c (n "mics-vz-89te") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "time") (r "^0.3.9") (o #t) (d #t) (k 0)))) (h "17yxw4axgjbx7pv4jmh87q3ppl4q7ydn6s92j1sbn2nnyd39srl9") (f (quote (("unproven") ("std")))) (s 2) (e (quote (("time" "dep:time"))))))

(define-public crate-mics-vz-89te-0.2.1 (c (n "mics-vz-89te") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "time") (r "^0.3.9") (o #t) (d #t) (k 0)))) (h "12fmfnilc6bb54mjdgiiq2j6kcckci6bmpznriv48y0hvgkf30w6") (f (quote (("unproven") ("std")))) (s 2) (e (quote (("time" "dep:time"))))))

