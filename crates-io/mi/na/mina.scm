(define-module (crates-io mi na mina) #:use-module (crates-io))

(define-public crate-mina-0.1.0 (c (n "mina") (v "0.1.0") (d (list (d (n "enum-map") (r "^2.5.0") (d #t) (k 2)) (d (n "mina_core") (r "^0.1.0") (d #t) (k 0)) (d (n "mina_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)))) (h "1w4s78n1xbapzsixhwf8yh5x8r61f9a1lxdq9mbfkcf9flj75cy8")))

(define-public crate-mina-0.1.1 (c (n "mina") (v "0.1.1") (d (list (d (n "enum-map") (r "^2.5.0") (d #t) (k 2)) (d (n "mina_core") (r "^0.1.0") (d #t) (k 0)) (d (n "mina_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)))) (h "0ga5fc5y0ngm4zg1jsiqalxqxyf4fvb4h9nry9m909zqa1a1w7iz")))

