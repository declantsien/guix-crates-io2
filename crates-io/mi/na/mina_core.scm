(define-module (crates-io mi na mina_core) #:use-module (crates-io))

(define-public crate-mina_core-0.1.0 (c (n "mina_core") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 0)) (d (n "enum-map") (r "^2.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lyon_geom") (r "^1.0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 2)))) (h "0n6bgnlr9daq5038mr4z96k2l4n2shpx300hwc6kgkrvw6a993a5")))

