(define-module (crates-io mi na mina_macros) #:use-module (crates-io))

(define-public crate-mina_macros-0.1.0 (c (n "mina_macros") (v "0.1.0") (d (list (d (n "mina_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1irngabgk12xnr7cz6rrdv1ssck1c8zkn41q2afj7d925wws9w6i") (f (quote (("parse-debug" "syn/extra-traits"))))))

