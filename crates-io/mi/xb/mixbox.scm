(define-module (crates-io mi xb mixbox) #:use-module (crates-io))

(define-public crate-mixbox-0.0.1 (c (n "mixbox") (v "0.0.1") (h "1x0y9bv4na31icxnrcid1ws8apwf80qk0namzs6va15a1kinncgc")))

(define-public crate-mixbox-0.0.2 (c (n "mixbox") (v "0.0.2") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "0lxjj7n9v9hmnycwdz0cgrqlmra7hkzvhgdn7w2cfc0kdv5fyraj")))

(define-public crate-mixbox-0.0.3 (c (n "mixbox") (v "0.0.3") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "19g6yazha2dqssws2r5gpg7vb5dkadjn322vb26zq6nlpgwihs03")))

(define-public crate-mixbox-0.0.4 (c (n "mixbox") (v "0.0.4") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "1r5v030w17ri8cyxqnpq8c19abjn9byhihxbsrakc1na4z62d9ic")))

(define-public crate-mixbox-0.0.5 (c (n "mixbox") (v "0.0.5") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "09i5l9i495548q0jps030fm7sszrmb6zck1vnbrmshb6fr45zvql")))

(define-public crate-mixbox-0.0.6 (c (n "mixbox") (v "0.0.6") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "05w1paii9xc4idxbfjsd42ivrqz6cn47gkd3ki096s4qizrraddf")))

(define-public crate-mixbox-0.0.7 (c (n "mixbox") (v "0.0.7") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "1vja5sf0c3xrcih6gj59j6pi5fq3c21adbhhbrapgq5jmrk8ywck")))

(define-public crate-mixbox-0.0.8 (c (n "mixbox") (v "0.0.8") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "1rb1wvnja39xlcs4qwzjwlbjixlnq6pyzaqkl9918zazffxj3jm4")))

(define-public crate-mixbox-2.0.0 (c (n "mixbox") (v "2.0.0") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "1ik0clm8kq2xj0gmmifc2s8wldg3vqf0apf5czim7y3ld4n0givx")))

