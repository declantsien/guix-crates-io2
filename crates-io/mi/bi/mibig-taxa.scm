(define-module (crates-io mi bi mibig-taxa) #:use-module (crates-io))

(define-public crate-mibig-taxa-0.1.0 (c (n "mibig-taxa") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07gp7y14xyd54vqfwj3qy7ksr0ip6csgjkp6n8jxl064cbhi3b2x")))

