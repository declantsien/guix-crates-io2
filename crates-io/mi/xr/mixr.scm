(define-module (crates-io mi xr mixr) #:use-module (crates-io))

(define-public crate-mixr-0.3.2 (c (n "mixr") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 2)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "1p4ajqqrh9rph0w2ii57jk9fm5x41kbivppdz385vipcix730llk")))

(define-public crate-mixr-0.3.3 (c (n "mixr") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 2)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "054cb14f5l4kiyfxcgb5h4l8ypkv7m7lflxkgirih3p51f7h0k3x")))

(define-public crate-mixr-0.3.4 (c (n "mixr") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 2)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "1c9wvyys8sprj72y46plnhx7ix0mqnlx2gg7kq3ni9i0sfrwv0n2")))

(define-public crate-mixr-0.3.5 (c (n "mixr") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 2)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "07nkr2qb6ng20npi4yikglq1d5l62vvavlddd8z24wncyy33xa7y")))

