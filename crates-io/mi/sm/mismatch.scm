(define-module (crates-io mi sm mismatch) #:use-module (crates-io))

(define-public crate-mismatch-1.0.0 (c (n "mismatch") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gb5sicaja1bqr8c1gj2nzzkas0dvcwyiykffycalzcya12aq0vp")))

