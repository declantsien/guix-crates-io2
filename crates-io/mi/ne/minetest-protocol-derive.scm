(define-module (crates-io mi ne minetest-protocol-derive) #:use-module (crates-io))

(define-public crate-minetest-protocol-derive-0.1.0 (c (n "minetest-protocol-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "16ncy2g8xc3ai5wyjzi9bkvcdcxfi33kb07xpvp46pwynhhv9y45")))

(define-public crate-minetest-protocol-derive-0.1.1 (c (n "minetest-protocol-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18h77arscmbrcaj2143pf68dvksy88276jc7b57rh3nxbdw03v1p")))

(define-public crate-minetest-protocol-derive-0.1.3 (c (n "minetest-protocol-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1pz8y8m1jm5vmmvqa3qs4sg02g7ybrw40mx1w1k8s6wbr9ha532j")))

(define-public crate-minetest-protocol-derive-0.1.4 (c (n "minetest-protocol-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1394j2ni1m1q5qj55dzyqahxychg8s7gqcr1ajpq62a5rwawvgg7")))

