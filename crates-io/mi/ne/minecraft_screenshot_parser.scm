(define-module (crates-io mi ne minecraft_screenshot_parser) #:use-module (crates-io))

(define-public crate-minecraft_screenshot_parser-0.1.0 (c (n "minecraft_screenshot_parser") (v "0.1.0") (d (list (d (n "image") (r "^0.23.6") (k 0)) (d (n "image") (r "^0.23.6") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.21") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1qjdqqwi480vk9fqkd1y3kmdfs8jl7nn2nlymw9sl6f97cna16wv")))

(define-public crate-minecraft_screenshot_parser-0.2.0 (c (n "minecraft_screenshot_parser") (v "0.2.0") (d (list (d (n "image") (r "^0.23.6") (k 0)) (d (n "image") (r "^0.23.6") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.21") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "06sxwd782dsw1a6djpaxslk7a1ij82jgacx87zrzap7kdaa77psc")))

(define-public crate-minecraft_screenshot_parser-0.2.1 (c (n "minecraft_screenshot_parser") (v "0.2.1") (d (list (d (n "image") (r "^0.23.6") (f (quote ("png"))) (k 0)) (d (n "image") (r "^0.23.6") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.21") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1kypwav5wp7qlg5cwqwxkhy5naxi1gwmcf7zprxlg3s88dwgi0s6")))

(define-public crate-minecraft_screenshot_parser-0.3.0 (c (n "minecraft_screenshot_parser") (v "0.3.0") (d (list (d (n "image") (r "^0.23.6") (f (quote ("png"))) (k 0)) (d (n "image") (r "^0.23.6") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.21") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "03ampb9m4kz0l50qgkmnp7xwxb0v2qd27sz5sabc064k1jvkvb7k")))

(define-public crate-minecraft_screenshot_parser-0.4.0 (c (n "minecraft_screenshot_parser") (v "0.4.0") (d (list (d (n "image") (r "^0.24.1") (f (quote ("png"))) (k 0)) (d (n "image") (r "^0.24.1") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.23") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "05ym4rcz655qwrvx4p4y9mrsb91gk41wpm23jdkqs4yh3f5lka9z")))

(define-public crate-minecraft_screenshot_parser-0.5.0 (c (n "minecraft_screenshot_parser") (v "0.5.0") (d (list (d (n "image") (r "^0.25.1") (f (quote ("png"))) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("png" "jpeg"))) (k 2)) (d (n "imageproc") (r "^0.24.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1c4s5drnczmj6v1awg4slgm3fc5ld4fa7py85ly21s1ph5jblpa6")))

