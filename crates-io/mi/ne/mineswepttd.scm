(define-module (crates-io mi ne mineswepttd) #:use-module (crates-io))

(define-public crate-mineswepttd-0.1.0 (c (n "mineswepttd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "petname") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "057l2myqcyw0mn5z0izn1cm5n8m9191njxh585zj5c4sh50x08hy")))

(define-public crate-mineswepttd-0.1.1 (c (n "mineswepttd") (v "0.1.1") (d (list (d (n "petname") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0n4hckyx20cccqk3rimixx57hj5588mh4vzj6q0pbbzqjacbn35s")))

(define-public crate-mineswepttd-0.2.0 (c (n "mineswepttd") (v "0.2.0") (d (list (d (n "petname") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1l56xynb2xc8l2m4a4cjlzqfimc2jz0k9a4nzdll1dnxli7xznzf")))

