(define-module (crates-io mi ne minesweeper) #:use-module (crates-io))

(define-public crate-minesweeper-0.1.0 (c (n "minesweeper") (v "0.1.0") (d (list (d (n "clap") (r "^1.2") (d #t) (k 0)) (d (n "find_folder") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "~0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0f78g3mkvq0chffy3q2kfji7dbayp853f7i1mw4kap7gpyr8xdvf")))

(define-public crate-minesweeper-1.0.0 (c (n "minesweeper") (v "1.0.0") (d (list (d (n "clap") (r "^1.2") (d #t) (k 0)) (d (n "find_folder") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "~0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kmdvc7g9f4zddhpsj4jv71zcjv435k1n35hl1jcim92v09kzaq1")))

(define-public crate-minesweeper-1.1.0 (c (n "minesweeper") (v "1.1.0") (d (list (d (n "clap") (r "^1.2") (d #t) (k 0)) (d (n "find_folder") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "~0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19x9xmqc4z6h91ax9ylfqgw9c3rlyg9294sr8iw8g59jjmafq4r6")))

(define-public crate-minesweeper-1.2.0 (c (n "minesweeper") (v "1.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^1.4") (d #t) (k 0)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "piston_window") (r "~0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1d7msj7s8w7l3kiig7fi8676m2rlb0nc24b6wg931wg4w7jzjpf8")))

(define-public crate-minesweeper-1.2.1 (c (n "minesweeper") (v "1.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "piston_window") (r "~0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "182b0rjrf4bbcgxv5ww0li77c7h6a62inca8icf215cqnb2wc171")))

(define-public crate-minesweeper-1.3.0 (c (n "minesweeper") (v "1.3.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "piston_window") (r "~0.66.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "195jxvmvnl55q0l6d9p8gh9id5ndkglgyra8gdrp5h9f97aa1cj5")))

