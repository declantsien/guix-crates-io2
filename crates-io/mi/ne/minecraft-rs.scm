(define-module (crates-io mi ne minecraft-rs) #:use-module (crates-io))

(define-public crate-minecraft-rs-0.1.0 (c (n "minecraft-rs") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros" "fs" "sync"))) (d #t) (k 0)) (d (n "tokio-retry") (r "^0.3") (d #t) (k 0)))) (h "0ym9cabm1cv06h5mg4c3yyr40cpy6296gifl7gjq6f3d2z54hmwy") (y #t)))

