(define-module (crates-io mi ne minecraft-client-rs) #:use-module (crates-io))

(define-public crate-minecraft-client-rs-0.1.0 (c (n "minecraft-client-rs") (v "0.1.0") (h "1dgj791zbp10l69kj5p95q8cyvhjfmp8c4g97qzz1d99f83xjbwz")))

(define-public crate-minecraft-client-rs-0.1.3 (c (n "minecraft-client-rs") (v "0.1.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1c4zvbx12mybj0py9z41iazb3rsman5q5fr4068kc1sznwg13my9") (s 2) (e (quote (("cli" "dep:clap" "dep:regex"))))))

