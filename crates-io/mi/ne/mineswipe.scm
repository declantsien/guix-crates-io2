(define-module (crates-io mi ne mineswipe) #:use-module (crates-io))

(define-public crate-mineswipe-0.1.0 (c (n "mineswipe") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1i2wihqdpv116rfp6vdmqs4r4c9p8j6s028c9qswsczwqlr4zyq9")))

