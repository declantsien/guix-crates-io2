(define-module (crates-io mi ne minecraft-version) #:use-module (crates-io))

(define-public crate-minecraft-version-0.1.0 (c (n "minecraft-version") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 1)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 1)))) (h "1m7gmd2wixspfr3mwaaban2ys5qn1483l9gzf4lxnksa4b5f0bq8")))

