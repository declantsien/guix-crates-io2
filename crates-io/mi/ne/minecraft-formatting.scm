(define-module (crates-io mi ne minecraft-formatting) #:use-module (crates-io))

(define-public crate-minecraft-formatting-0.1.0 (c (n "minecraft-formatting") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)))) (h "0bzkajx9j77haqzynyp8d9dl8nvsp4ynx90rdyz1wfvvhi8svbyh")))

