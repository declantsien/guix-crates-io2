(define-module (crates-io mi ne minecrevy_io) #:use-module (crates-io))

(define-public crate-minecrevy_io-0.1.0 (c (n "minecrevy_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (d #t) (k 0)))) (h "1ss8sadfh40jggwzjlvx3mxj48s8rcf9pl42xk69cbb2mb04arja") (f (quote (("default" "codec") ("codec" "bytes" "tokio-util"))))))

