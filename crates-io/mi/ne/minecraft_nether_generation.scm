(define-module (crates-io mi ne minecraft_nether_generation) #:use-module (crates-io))

(define-public crate-minecraft_nether_generation-0.2.2 (c (n "minecraft_nether_generation") (v "0.2.2") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "04bcls6l63mk6318yq938fkrpszpd97k34cwngc69kgbqksyhnm6")))

(define-public crate-minecraft_nether_generation-0.2.3 (c (n "minecraft_nether_generation") (v "0.2.3") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "1rahbbj9sd0v8ck8crcswapfgx67jzkwcj5mvs0n61pcgbhp8vb5")))

(define-public crate-minecraft_nether_generation-0.3.0 (c (n "minecraft_nether_generation") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "0cmnmz45sknfiwrnlwab2rkh9jx7mf78x14asz4pjxn1ljj813xr")))

(define-public crate-minecraft_nether_generation-0.3.1 (c (n "minecraft_nether_generation") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "0pp4m32xf4raly8pyxlkp5vfpqnr7z702vk1mj18zzkdfv403jkn")))

(define-public crate-minecraft_nether_generation-0.3.2 (c (n "minecraft_nether_generation") (v "0.3.2") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "0zshsbz06g2cfy5lkycg5gqnnmnwlg79yh0g42qkzjd5l6352k6x")))

(define-public crate-minecraft_nether_generation-0.3.3 (c (n "minecraft_nether_generation") (v "0.3.3") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "0j4rjgj1fjkdpnqasrrn513njiybkivarl9b7y91id8bxaz52vm7")))

(define-public crate-minecraft_nether_generation-0.3.4 (c (n "minecraft_nether_generation") (v "0.3.4") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "noise_rs") (r "^0.1.15") (k 0)))) (h "1s32bj6xsqpzzyj3p4f59kwvrw3w4dvd9nrmhmqb8hhv4wg03g95")))

(define-public crate-minecraft_nether_generation-0.3.5 (c (n "minecraft_nether_generation") (v "0.3.5") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "noise_rs") (r "^0.1.16") (k 0)))) (h "0pl68k7ghvhcvx9d3dm12lkww5rdhfvz46vn18hc32awng9lfwra")))

