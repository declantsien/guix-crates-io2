(define-module (crates-io mi ne minecraft_folder_path) #:use-module (crates-io))

(define-public crate-minecraft_folder_path-0.1.0 (c (n "minecraft_folder_path") (v "0.1.0") (h "12x57yjwmvvmgara2qd74cb87bqx1yc6mqylz9sjfiigyr7mf5wn")))

(define-public crate-minecraft_folder_path-0.1.1 (c (n "minecraft_folder_path") (v "0.1.1") (h "0xc3niww1anc09fy00fnwlfzwrnf2nwl274cbz7h24r6kl9qdf33")))

(define-public crate-minecraft_folder_path-0.1.2 (c (n "minecraft_folder_path") (v "0.1.2") (h "13a5zxpqrsv9rii226188k4p8vx66viryjk411hgiw85w19662nn")))

