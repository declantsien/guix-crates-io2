(define-module (crates-io mi ne minesweeper_core) #:use-module (crates-io))

(define-public crate-minesweeper_core-0.1.0 (c (n "minesweeper_core") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.127.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17h185csqy6k5wjjjkdq8fx5bniws2mlyv3q24015cpl4mkdc9jj")))

