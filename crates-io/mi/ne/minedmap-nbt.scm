(define-module (crates-io mi ne minedmap-nbt) #:use-module (crates-io))

(define-public crate-minedmap-nbt-0.1.0 (c (n "minedmap-nbt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fastnbt") (r "^2.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "minedmap-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)))) (h "0fl86l2fz28hihbk63bnkpn5f378ijng232sp290aj5dhaabfj7a") (f (quote (("zlib-ng" "flate2/zlib-ng"))))))

(define-public crate-minedmap-nbt-0.1.1 (c (n "minedmap-nbt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fastnbt") (r "^2.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "minedmap-types") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)))) (h "1shcj2iaa2mp2i4yp323hfwwv4mifcb8i9bs5lc0wgczq7bggc30") (f (quote (("zlib-ng" "flate2/zlib-ng"))))))

