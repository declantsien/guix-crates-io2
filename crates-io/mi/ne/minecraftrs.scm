(define-module (crates-io mi ne minecraftrs) #:use-module (crates-io))

(define-public crate-minecraftrs-0.1.0 (c (n "minecraftrs") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.11") (f (quote ("deref" "deref_mut"))) (d #t) (k 0)))) (h "1444dj6gc74vyg7bf2aa4cx53b9rcszf0vbigyvc4skflz4cs07i") (f (quote (("release-1-2") ("default" "release-1-2")))) (y #t)))

(define-public crate-minecraftrs-0.1.1 (c (n "minecraftrs") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.11") (f (quote ("deref" "deref_mut"))) (d #t) (k 0)))) (h "0v6qa6ysb8mgjpz0ybzsfr6v11m3i4i8b3a1qwxy75dxz7c4c16g") (f (quote (("release-1-2") ("default" "release-1-2")))) (y #t)))

