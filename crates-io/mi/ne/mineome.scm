(define-module (crates-io mi ne mineome) #:use-module (crates-io))

(define-public crate-mineome-0.1.0 (c (n "mineome") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nw5i2rywm3xw76ww9jaawcb7q6p868zhbmcisvdski6lfy5hlh5")))

(define-public crate-mineome-0.2.0 (c (n "mineome") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1857w9vsrcj68ypn519pbvlvyfdhqngma9grh8hh8h7m7wryjc8s")))

