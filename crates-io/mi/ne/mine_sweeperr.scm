(define-module (crates-io mi ne mine_sweeperr) #:use-module (crates-io))

(define-public crate-mine_sweeperr-0.1.0 (c (n "mine_sweeperr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dkgwsazpp3vdzxb5f4myy411w0f9i4c1xz87k1qlpb023nw8d30")))

(define-public crate-mine_sweeperr-0.2.0 (c (n "mine_sweeperr") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wvm88x0y34pvgwzpi2xkk5idsqgy7ia6q47y830hn3wnjmbw7xp")))

(define-public crate-mine_sweeperr-0.3.0 (c (n "mine_sweeperr") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "0pfpblijlcbgabz4fwm97zzfjcl114s0hm47ari7xkxd768d1ajd")))

