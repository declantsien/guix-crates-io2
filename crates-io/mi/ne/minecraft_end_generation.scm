(define-module (crates-io mi ne minecraft_end_generation) #:use-module (crates-io))

(define-public crate-minecraft_end_generation-0.1.0 (c (n "minecraft_end_generation") (v "0.1.0") (d (list (d (n "java_random") (r "^0.1.1") (d #t) (k 0)))) (h "0a3mhsslxi1j22ffg8vqf6ba9mfx15rk3arwvs4236rc4x59w6g8")))

(define-public crate-minecraft_end_generation-0.1.1 (c (n "minecraft_end_generation") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0vzhsgxbjr75ymyg70djh1rbfpvw10k0vpjffwgvwgfl5kvksg5x")))

(define-public crate-minecraft_end_generation-0.1.2 (c (n "minecraft_end_generation") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.3") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0rrkmkc5q8f5czplifzyrsl20zmn3nvfy0naz3vydzcapqrs9hly")))

(define-public crate-minecraft_end_generation-0.1.3 (c (n "minecraft_end_generation") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "14nyfcp3sasg02ib53gmbr4w157san89bvsxskf0430yw3jfb6sa")))

(define-public crate-minecraft_end_generation-0.1.4 (c (n "minecraft_end_generation") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.4") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.7") (d #t) (k 0)))) (h "12dqy8l4myl5j9sdaqgahf0kyvsglqgv5qqdnaam1sgnjqzharfq")))

(define-public crate-minecraft_end_generation-0.1.5 (c (n "minecraft_end_generation") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.7") (d #t) (k 0)))) (h "19vfd0vr1i5pppljc0v57cfmjrfmim87dchiis9wcaq8b33555hy")))

(define-public crate-minecraft_end_generation-0.1.6 (c (n "minecraft_end_generation") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.8") (d #t) (k 0)))) (h "1g3vygngh2c84hyr4afli4r608wp6wj4sm3cc7vzr0kli0ffl3p9")))

(define-public crate-minecraft_end_generation-0.1.7 (c (n "minecraft_end_generation") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.11") (d #t) (k 0)))) (h "0mrq15zli2midrz2rr4v2vv8sa37967gdf3b5zzb40q8ikn1sprd")))

(define-public crate-minecraft_end_generation-0.2.0 (c (n "minecraft_end_generation") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.11") (d #t) (k 0)))) (h "0g753scxn5ilspb6q2giggflnj7l9ny8xy9byd7xik39q8a4smp4")))

(define-public crate-minecraft_end_generation-0.2.1 (c (n "minecraft_end_generation") (v "0.2.1") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "07asr42fy22y1bz74y70w9ym77gjqpvnn7ij7qyl10wg65wqxxl0")))

(define-public crate-minecraft_end_generation-0.3.0 (c (n "minecraft_end_generation") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.6") (d #t) (k 0)) (d (n "noise_rs") (r "^0.1.13") (d #t) (k 0)))) (h "0mirkdj4hfkas6qhrr16yjn4b50vcrcd4ylv1m346hkjswxz8rb2")))

(define-public crate-minecraft_end_generation-0.3.1 (c (n "minecraft_end_generation") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "noise_rs") (r "^0.1.15") (k 0)))) (h "1bl3zqknmj89is7vipfgj94wl7dxmmb8qnlshnjzgxzjfbvms8b3") (f (quote (("default" "const_fn") ("const_fn" "java_random/const_fn" "noise_rs/const_fn"))))))

(define-public crate-minecraft_end_generation-0.3.2 (c (n "minecraft_end_generation") (v "0.3.2") (d (list (d (n "cbindgen") (r "^0.17.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "intmap") (r "^0.7.0") (d #t) (k 0)) (d (n "java_random") (r "^0.1.7") (k 0)) (d (n "noise_rs") (r "^0.1.16") (k 0)))) (h "0gz6w3qy6sg171j6prkisslmicbh475mxngzgp927wnh4cbq2zn0") (f (quote (("default" "const_fn") ("const_fn" "java_random/const_fn" "noise_rs/const_fn"))))))

