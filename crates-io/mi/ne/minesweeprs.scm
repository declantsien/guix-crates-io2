(define-module (crates-io mi ne minesweeprs) #:use-module (crates-io))

(define-public crate-minesweeprs-0.1.0 (c (n "minesweeprs") (v "0.1.0") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0annwy36gvqwq34f9zcynxk2qf1jpc0pczcq0r392nlnnpzn87l0")))

(define-public crate-minesweeprs-0.1.1 (c (n "minesweeprs") (v "0.1.1") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0jw0piyrmgfa0m0jd0ca5savqsivvhjwa3kqd22yrc7p9kixrkr1")))

(define-public crate-minesweeprs-0.1.2 (c (n "minesweeprs") (v "0.1.2") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0mwigfvihl237dlgwq19hr2k63fzkd3lzglc90w3msp10q66l4fp")))

(define-public crate-minesweeprs-0.1.3 (c (n "minesweeprs") (v "0.1.3") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1i2wbg2k6d2kplnpshi217j64idahracphnnba9m6gf3akjdccph")))

(define-public crate-minesweeprs-0.2.0 (c (n "minesweeprs") (v "0.2.0") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1znaql7sq94l6lksmdsdr4xwdj4qfi4b7zi1jcd2fciwsa9acaqk")))

(define-public crate-minesweeprs-0.3.0 (c (n "minesweeprs") (v "0.3.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frozenset") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "0y90gcy8d7y1lx0gy3fadi7c055gwbmnkps2djnk5rl36dh297xv")))

(define-public crate-minesweeprs-0.3.1 (c (n "minesweeprs") (v "0.3.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frozenset") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1hvzljhlafkz954acpcl4l574f970k3pz7fk9rfd9s7662y0agli")))

(define-public crate-minesweeprs-0.3.2 (c (n "minesweeprs") (v "0.3.2") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frozenset") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1dxm1sqwzwbhyp1vxb66nmmw8val5x7yph5p7gxj9rgm2njp0k2y")))

(define-public crate-minesweeprs-0.3.3 (c (n "minesweeprs") (v "0.3.3") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frozenset") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "0bfzmpgayxnksrl42dyy630wv7vk52lgvdn3x5919iy9kz11w3y9") (s 2) (e (quote (("serde" "dep:serde" "frozenset/serde"))))))

(define-public crate-minesweeprs-0.3.4 (c (n "minesweeprs") (v "0.3.4") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "frozenset") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "061ppnxs3jg67afbwrpxaz5z2rlzip4zw9yhn9wp4c8w9qjbfy07") (s 2) (e (quote (("serde" "dep:serde" "frozenset/serde" "either/serde"))))))

