(define-module (crates-io mi ne mines) #:use-module (crates-io))

(define-public crate-mines-0.1.0 (c (n "mines") (v "0.1.0") (h "1zw2ci85zlhwfgadhg3xfwip7dsaz0fm2x5y3ifjihkphkqb9fhg")))

(define-public crate-mines-0.1.1 (c (n "mines") (v "0.1.1") (h "00mwpn5aqmq6854znkz82s16ljq7w05qagw9qym28j8dij6z07q6")))

(define-public crate-mines-0.2.0 (c (n "mines") (v "0.2.0") (h "11hv5b3bp5az6p0sbvdqm70fpvdca2fyzaj2harv48pw0dwp48bm")))

(define-public crate-mines-0.2.1 (c (n "mines") (v "0.2.1") (h "0dnga7xngl81hwixq8s6lq9jkw0cy3cqbygaparvmjsyn59qrf4a")))

