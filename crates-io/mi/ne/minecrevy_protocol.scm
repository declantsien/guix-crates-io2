(define-module (crates-io mi ne minecrevy_protocol) #:use-module (crates-io))

(define-public crate-minecrevy_protocol-0.1.0 (c (n "minecrevy_protocol") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "minecrevy_io") (r "^0.1.0") (d #t) (k 0)) (d (n "minecrevy_net") (r "^0.1.0") (d #t) (k 0)) (d (n "minecrevy_text") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6") (d #t) (k 0)))) (h "1qwnnsd6vyfd5lsypfjqyv3s5h6199w4mnzfbia8plbqr54w60s3")))

