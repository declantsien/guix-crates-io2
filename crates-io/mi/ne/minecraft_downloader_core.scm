(define-module (crates-io mi ne minecraft_downloader_core) #:use-module (crates-io))

(define-public crate-minecraft_downloader_core-0.1.0 (c (n "minecraft_downloader_core") (v "0.1.0") (d (list (d (n "requests_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l0rwlj1a31cgynmvvird9b45hpmmk3aai97c5hl5i3c19k3qx5x") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.1 (c (n "minecraft_downloader_core") (v "0.1.1") (d (list (d (n "requests_rs") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s1if4bpc589k5cn5j5h2336kpj85n3n5058adb2c2rpp92l1naq") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.2 (c (n "minecraft_downloader_core") (v "0.1.2") (d (list (d (n "requests_rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0an922v1xf6jrc9hrf3hck4qykhqp8nhvy2jyrmn09x2nl74zb44") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.3 (c (n "minecraft_downloader_core") (v "0.1.3") (d (list (d (n "requests_rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0396mwxgzhlrd11nv4zk9w00k5l5bp4j8i470j3ddd48m4v0zbj2") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.4 (c (n "minecraft_downloader_core") (v "0.1.4") (d (list (d (n "requests_rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1wsimcsms2gvbld371c3kn5wh2zb6l7dzdmdncf2d2bvkp2zf8i8") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.5 (c (n "minecraft_downloader_core") (v "0.1.5") (d (list (d (n "requests_rs") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0sqhyrd35l7xxwbqik8lldv3p5fn8i5scq49hkspjsqvvacg8j84") (r "1.65")))

(define-public crate-minecraft_downloader_core-0.1.6 (c (n "minecraft_downloader_core") (v "0.1.6") (d (list (d (n "requests_rs") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0sv7n6xzs0mlnsklvjva662sk0i138d6zxmnyskk5kg4lm2qf0r4") (r "1.65")))

