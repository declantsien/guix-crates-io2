(define-module (crates-io mi ne minecraft-protocol) #:use-module (crates-io))

(define-public crate-minecraft-protocol-0.1.0 (c (n "minecraft-protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "minecraft-protocol-derive") (r "^0.0.0") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1c62q0dwbc7fm3dl63f2ax5b2r775y5n5wpppnrlajv7gl1kg66a")))

