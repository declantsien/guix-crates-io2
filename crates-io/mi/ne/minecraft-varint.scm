(define-module (crates-io mi ne minecraft-varint) #:use-module (crates-io))

(define-public crate-minecraft-varint-0.1.0 (c (n "minecraft-varint") (v "0.1.0") (h "014k2kg9xcsmr7nwzy7wd8a37z8lpq2f3g7prm943f3qhflyrn7r") (y #t)))

(define-public crate-minecraft-varint-0.1.1 (c (n "minecraft-varint") (v "0.1.1") (h "149jd1icdg0j0s417dgirk9zclwgrjb1c8dry2d3nqa0mz0iklsj") (y #t)))

(define-public crate-minecraft-varint-0.2.0 (c (n "minecraft-varint") (v "0.2.0") (h "0z6583j6v44bjj2w6dbmvmv3606f17a2nayi7xjcm0kfbkbh4vp5")))

