(define-module (crates-io mi ne minecraft_client) #:use-module (crates-io))

(define-public crate-minecraft_client-0.1.0 (c (n "minecraft_client") (v "0.1.0") (h "1jrwlrxxzbdd79dc28pjvmnlwa90ff9kvc8dxbq85f1vxslxjai1") (y #t)))

(define-public crate-minecraft_client-0.0.1 (c (n "minecraft_client") (v "0.0.1") (h "1nln5l7r9wq6cr1d3i89sc1f88zv5cckcqi4k5l19kbb88rspwm5")))

(define-public crate-minecraft_client-0.0.2 (c (n "minecraft_client") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.8") (d #t) (k 0)))) (h "0an8ihhikcv06qq9341waxrqdb80jd2wpgcl0gd5z5m3ag1irkms")))

(define-public crate-minecraft_client-0.1.1 (c (n "minecraft_client") (v "0.1.1") (h "1y5m2l0hvdvwip0pff99c5py3riwbxyb4brks5z2i1r5576k85b4")))

