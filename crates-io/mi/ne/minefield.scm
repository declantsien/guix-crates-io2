(define-module (crates-io mi ne minefield) #:use-module (crates-io))

(define-public crate-minefield-0.1.0 (c (n "minefield") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pljf5wjfmvxnr8s0vgbfafrm053h8mk9gi7lk95kkb9qwlxsx95") (y #t)))

(define-public crate-minefield-0.1.1 (c (n "minefield") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rij4lqwsnp83abx5i0bx06y9i4g1rclz4x558rlm0vybdx5ddba") (y #t)))

(define-public crate-minefield-3.1.0 (c (n "minefield") (v "3.1.0") (d (list (d (n "mvc-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1779fm949s8hgnjh9z844ya2990r3zvh04g7g4yyjzq8wz5gb5ax") (y #t)))

(define-public crate-minefield-3.2.0 (c (n "minefield") (v "3.2.0") (d (list (d (n "mvc-rs") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jvrwfnrwmqyii3xjszdqsllh7xj55ac825wpi9zh7cv4gi65yfc") (y #t)))

(define-public crate-minefield-3.2.1 (c (n "minefield") (v "3.2.1") (d (list (d (n "mvc-rs") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qxml30wbjjqnv3rp3l0kmgw7m69zyrw8639z986ghy4zlxdglcj") (y #t)))

(define-public crate-minefield-3.3.0 (c (n "minefield") (v "3.3.0") (d (list (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11csxprpwcn5560n7c8fafhi49giyr6ixpk42j0vpyz8s9a7i1x4")))

