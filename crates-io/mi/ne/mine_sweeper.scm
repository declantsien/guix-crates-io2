(define-module (crates-io mi ne mine_sweeper) #:use-module (crates-io))

(define-public crate-mine_sweeper-0.1.0 (c (n "mine_sweeper") (v "0.1.0") (d (list (d (n "cursive") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1ji2ly3gj2vcjacd0kbj5s4gy86r0dpy8mphikfdkfws9cdjm64r")))

(define-public crate-mine_sweeper-0.2.0 (c (n "mine_sweeper") (v "0.2.0") (d (list (d (n "cursive") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1r2414j0l0iqmn3vbkl9k2npjmi6llfbn61zwwcvnkl5k5p3v4h1")))

(define-public crate-mine_sweeper-0.2.1 (c (n "mine_sweeper") (v "0.2.1") (d (list (d (n "cursive") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1vijv1n2bmmi2b0an15v6h50c62g7fggl57zmcjcb4ywkxgdvhvx")))

(define-public crate-mine_sweeper-0.2.2 (c (n "mine_sweeper") (v "0.2.2") (d (list (d (n "cursive") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "03jckkxlc2kv3zck7kz1yjpchgf8z4wbsabk0scnryzxia4gr2g4")))

