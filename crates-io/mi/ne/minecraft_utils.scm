(define-module (crates-io mi ne minecraft_utils) #:use-module (crates-io))

(define-public crate-minecraft_utils-0.1.0 (c (n "minecraft_utils") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "base64-serde") (r "^0.6.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "minreq") (r "^2.4.2") (f (quote ("https" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dxz0hnw1rwgxcnkzqcmp3blynvbfasd359ll8kmv19jqc4952jq")))

