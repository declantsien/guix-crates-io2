(define-module (crates-io mi ne minehut) #:use-module (crates-io))

(define-public crate-minehut-1.0.0 (c (n "minehut") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pj93fydizky4afz1s803rc0krmj2ail3axdccif6aic8ycnk7id") (y #t)))

(define-public crate-minehut-1.0.1 (c (n "minehut") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v8vlzh9vr9hg8i311nry3vaskak7m31rbf186pac7bihax81hl0") (y #t)))

(define-public crate-minehut-2.0.0 (c (n "minehut") (v "2.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bvn9gmb0hlsaaq8hjmxfax8icsragxkps5xscbbkzfvf3iq3v0b")))

