(define-module (crates-io mi ne minesweeper_multiplayer) #:use-module (crates-io))

(define-public crate-minesweeper_multiplayer-0.1.0 (c (n "minesweeper_multiplayer") (v "0.1.0") (d (list (d (n "minesweeper_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0ms6ap50fpp90lcfh9nas65rfjmx7q5lbk282y9flyn2njb5a2sq")))

