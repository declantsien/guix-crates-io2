(define-module (crates-io mi ne minecraft-uuid) #:use-module (crates-io))

(define-public crate-minecraft-uuid-1.0.0 (c (n "minecraft-uuid") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0b3n1k5wh7sivz7507iqk848vfc21b2mprmvsk460nvprycbi6jb")))

