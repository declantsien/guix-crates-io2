(define-module (crates-io mi ne mine) #:use-module (crates-io))

(define-public crate-mine-0.0.1 (c (n "mine") (v "0.0.1") (h "0x7pm80ffh1imcw1z1wabgv4fcka120w3agzg91720flrpjmcw72") (y #t)))

(define-public crate-mine-0.0.5 (c (n "mine") (v "0.0.5") (h "15pmych0saj5baw42sar6p4hrgcqndnxphx9s47qglz8lawpnj4f") (y #t)))

(define-public crate-mine-0.0.7 (c (n "mine") (v "0.0.7") (h "095q9h6rkg9d1cmvwsbzr81dml7flaawr2zcrkj4i7cnl8p55vwn") (y #t)))

(define-public crate-mine-0.1.1 (c (n "mine") (v "0.1.1") (h "0rjk47y8frsbq87j3s8rcs8j0ac4jgi7k1ffwvf4m9pcaqb76ldq") (y #t)))

(define-public crate-mine-0.1.3 (c (n "mine") (v "0.1.3") (h "116p5xppc9ngpbbri53s7lkn9g3nkxwfnj0ycn1bkfqp0xz5irrl") (y #t)))

(define-public crate-mine-0.1.4 (c (n "mine") (v "0.1.4") (h "094zgrvz8ny42k9mp2w275qs51cc1alxji8cmnk737lx816sdzx3") (y #t)))

(define-public crate-mine-0.1.77 (c (n "mine") (v "0.1.77") (h "0dnnyda59pbf1y34l798g3q1faljdacprcgyc5x3qhagml0amfk1") (y #t)))

(define-public crate-mine-0.77.0 (c (n "mine") (v "0.77.0") (h "1d9g14lmz5jsgyw1zgb973s0sbqj5fm4817334hxyf8d9fv89lpr") (y #t)))

(define-public crate-mine-7.7.18 (c (n "mine") (v "7.7.18") (h "0pwfbxbg6rl4hync23w7l8vgk8l7izz8v9slps3m25pgs42d5q5g") (y #t)))

(define-public crate-mine-2018.7.7 (c (n "mine") (v "2018.7.7") (h "1ps2xv2xw697hhfpvddg99xg2rhpl2bwpslcdbfwzcafm9bzrpp6") (y #t)))

(define-public crate-mine-2019.12.13 (c (n "mine") (v "2019.12.13") (h "1pyv97f81ff17rqsfci7n0y7fbhy93fc1rsmh8sbx6259sckram6") (y #t)))

(define-public crate-mine-9999.999.99 (c (n "mine") (v "9999.999.99") (h "0368myi7pv8dy3g8a6ff4xzmws1fgn968vqpibwbzkp4n2ajyzqd") (y #t)))

(define-public crate-mine-9.9.9 (c (n "mine") (v "9.9.9") (h "00i4132w8bfb885kfb1g0zw5ypb39j04vf388546l2i5xc3ikq2l") (y #t)))

(define-public crate-mine-99999.99999.99999 (c (n "mine") (v "99999.99999.99999") (h "0kwbp68f07d3c5vzy267l939azh52mliqcaml7kv523wg2pzvpcr") (y #t)))

(define-public crate-mine-9999999.9999999.9999999 (c (n "mine") (v "9999999.9999999.9999999") (h "1gb391j3wdx6i30m9h88y2qq662rbpx2vkf28xwz6f2ppv50p22b") (y #t)))

(define-public crate-mine-999999999.999999999.999999999 (c (n "mine") (v "999999999.999999999.999999999") (h "0nij7yd6h63x5d3l39wy58dbski9klqww9wp3kr34a4gdlqfyimq")))

