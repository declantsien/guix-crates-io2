(define-module (crates-io mi ne minecraft_decorated_strings) #:use-module (crates-io))

(define-public crate-minecraft_decorated_strings-0.2.0 (c (n "minecraft_decorated_strings") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "hashlink") (r "^0.5.1") (d #t) (k 0)))) (h "03cwlpp5sdis1ndya3wfdz77i8cj2qajfgnk453qayin7jnl026b")))

