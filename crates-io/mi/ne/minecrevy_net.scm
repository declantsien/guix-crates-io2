(define-module (crates-io mi ne minecrevy_net) #:use-module (crates-io))

(define-public crate-minecrevy_net-0.1.0 (c (n "minecrevy_net") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "flume") (r "^0.11") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)) (d (n "minecrevy_io") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0ixb2b2mrcqyxfspjgsnlwpc2w0i4wz017ddgkwxgv1xchr5vv4l")))

