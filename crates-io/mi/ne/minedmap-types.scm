(define-module (crates-io mi ne minedmap-types) #:use-module (crates-io))

(define-public crate-minedmap-types-0.1.0 (c (n "minedmap-types") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "051ml3my1h9spqzcn4dmc4k1m0ng23kmrw35q36h7b52q15qx1f2")))

(define-public crate-minedmap-types-0.1.1 (c (n "minedmap-types") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "02lxzjj9jwclaq45zqsg2s2ly6i4nv7rylcnjc0rsp6jkbq3zf79")))

(define-public crate-minedmap-types-0.1.2 (c (n "minedmap-types") (v "0.1.2") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ldysd635565wndr2ifn9lnmv30v0a6f5xyzh0ds86p67jllgfv7")))

