(define-module (crates-io mi ne minetest-shark) #:use-module (crates-io))

(define-public crate-minetest-shark-0.1.0 (c (n "minetest-shark") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minetest-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vy26w5v9w4n1gc91pmh1m306q0z3l5lvxzwyn0ks6r2b00af2cr")))

(define-public crate-minetest-shark-0.1.1 (c (n "minetest-shark") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minetest-protocol") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18g2x2d9m9rkisf1nhhqqq7a5mbp7sklrcn65ajsrh3pkn8xjx8d")))

(define-public crate-minetest-shark-0.1.2 (c (n "minetest-shark") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minetest-protocol") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "02x964d4qsqpnz53w6dhdy8mrqhfa8hjglqq2i7r2dnggcampxdn")))

(define-public crate-minetest-shark-0.1.3 (c (n "minetest-shark") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minetest-protocol") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ph0bpwwbw8h7fbk3004sqcv1r4g5j31nf0h8ggq25g16g0xv31h")))

(define-public crate-minetest-shark-0.1.4 (c (n "minetest-shark") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minetest-protocol") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "097xizvz8izg6qd75gcmdc7cpgn1wi7cx425rsh6vxqv3hllnjqp")))

