(define-module (crates-io mi ne minecraft-assets) #:use-module (crates-io))

(define-public crate-minecraft-assets-0.0.1 (c (n "minecraft-assets") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "024xlg3124a5pa9sw7h8k61ha7844cqdfracxwpj274lhjv00r7j") (f (quote (("tests") ("default"))))))

(define-public crate-minecraft-assets-0.0.2 (c (n "minecraft-assets") (v "0.0.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00vphh25minjnx76sgx5jgs023jpk61yb3pr5ig143llq1cmnr8h") (f (quote (("tests") ("default")))) (y #t)))

(define-public crate-minecraft-assets-0.0.3 (c (n "minecraft-assets") (v "0.0.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04f6lvccmg647k6w1rd55rgaf6axj9rgxqq5ygczsj8fpcl9gjfn") (f (quote (("tests") ("default"))))))

(define-public crate-minecraft-assets-0.0.4 (c (n "minecraft-assets") (v "0.0.4") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19l8laqa5p25ajb1xgs7y386fsrg1knxwr9bcv5m80160s1c4p0d") (f (quote (("tests") ("default"))))))

(define-public crate-minecraft-assets-0.0.5 (c (n "minecraft-assets") (v "0.0.5") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14hl8dz7npc4q4m2lsh96a3c4kij7cy8768490bc0clh4fs4aca8") (f (quote (("tests") ("default"))))))

(define-public crate-minecraft-assets-0.0.6 (c (n "minecraft-assets") (v "0.0.6") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hx9cj8vany5ax358v0q6rsdg4vmdcxypq4zsz0f9xawksdwg37p") (f (quote (("tests") ("default"))))))

