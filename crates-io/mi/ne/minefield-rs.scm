(define-module (crates-io mi ne minefield-rs) #:use-module (crates-io))

(define-public crate-minefield-rs-0.1.4 (c (n "minefield-rs") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0h10avam27s7685r856q7xrvpqqvz0p1hzdzq8lln21i68y8zcij") (y #t)))

(define-public crate-minefield-rs-0.1.5 (c (n "minefield-rs") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0h40nv95yn8rygdzrjy3ykis69zgv6nq28mgjdiyphgqpj6v30cl")))

