(define-module (crates-io mi ne minecraft) #:use-module (crates-io))

(define-public crate-minecraft-0.0.0 (c (n "minecraft") (v "0.0.0") (h "026hc9cv1vjc3y7fjph31as452wl7rjsy3x40mxbw12bh911cvzf")))

(define-public crate-minecraft-0.1.0 (c (n "minecraft") (v "0.1.0") (h "0wyrp738cyyi0iy4w2q0vz0svw0zal2zsgy3rsh2cz09xm91yk99")))

(define-public crate-minecraft-0.2.0 (c (n "minecraft") (v "0.2.0") (h "00km3bch2bxpccwd177dr27ccv52v35mz9k95mc0qxap6ffdkg46")))

