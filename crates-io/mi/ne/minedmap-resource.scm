(define-module (crates-io mi ne minedmap-resource) #:use-module (crates-io))

(define-public crate-minedmap-resource-0.1.0 (c (n "minedmap-resource") (v "0.1.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bzpmly6ll5cz3byw3zhf8rx2rxbny0zrvrwmd1d2i1cvacbaabn")))

(define-public crate-minedmap-resource-0.2.0 (c (n "minedmap-resource") (v "0.2.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1689pgpfnqx14hh9crvah145g81pa6cxk2yfhwkl69f1mb5pj8i8")))

(define-public crate-minedmap-resource-0.3.0 (c (n "minedmap-resource") (v "0.3.0") (d (list (d (n "enumflags2") (r "^0.7.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lkd2fwmbg4hnpvqxzqrfch7yzinwjqy0amzmf7a4ngw8sn27xpc")))

