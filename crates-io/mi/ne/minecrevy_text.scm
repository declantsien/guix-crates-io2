(define-module (crates-io mi ne minecrevy_text) #:use-module (crates-io))

(define-public crate-minecrevy_text-0.1.0 (c (n "minecrevy_text") (v "0.1.0") (d (list (d (n "minecrevy_io") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bfzac64qbcmsfxqqca6004sgna93rbnpizln38fqylnqbsqw3p6")))

