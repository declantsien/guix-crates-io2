(define-module (crates-io mi ne mineer) #:use-module (crates-io))

(define-public crate-mineer-0.1.0 (c (n "mineer") (v "0.1.0") (h "0kj5lzdks3bg9m82214687wxdrbvml8qda3wvhcs9ka3bc6ksy2k")))

(define-public crate-mineer-0.1.1 (c (n "mineer") (v "0.1.1") (h "0lc48j9j1g3k0ifvrxi407h8gwzjv3jxg90i5ai4dai9b63b9yll")))

(define-public crate-mineer-0.1.2 (c (n "mineer") (v "0.1.2") (h "0ia3cpnrczpblpbcx2v02c0czqsc7k7zy104vx6y0b34ipa9d9kk")))

(define-public crate-mineer-0.1.3 (c (n "mineer") (v "0.1.3") (h "1d0hsyz244zgh500pkww1j4gm406jx873fdg9kvlj6pbsiy9kd6m")))

(define-public crate-mineer-0.1.4 (c (n "mineer") (v "0.1.4") (h "0yjgdkkzh7qjv122h7rn9b3li9kld811d7rc9fl582ja193pxg5c")))

