(define-module (crates-io mi ne minecraft_query) #:use-module (crates-io))

(define-public crate-minecraft_query-0.1.0 (c (n "minecraft_query") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "18da84xc1z0932z9j1r289c5n1hy8zhggq6776jl3kzdijs5kbdb")))

(define-public crate-minecraft_query-0.1.1 (c (n "minecraft_query") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "15jpi26v1l5q617vzr62vcpq42jrpdc0dayvg05l55y091jf44y0")))

(define-public crate-minecraft_query-0.1.2 (c (n "minecraft_query") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0y90isldw3am5azdcpn2p30li8y98m8n70ahn1jfbyqsq5818a56")))

