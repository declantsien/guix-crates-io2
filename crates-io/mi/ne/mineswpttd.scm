(define-module (crates-io mi ne mineswpttd) #:use-module (crates-io))

(define-public crate-mineswpttd-0.1.0 (c (n "mineswpttd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "petname") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pkasbm9ch6bbfj209d41h3yarg8aim6ns75wp75xy5x9lm3byyi") (y #t)))

