(define-module (crates-io mi ps mips-mcu) #:use-module (crates-io))

(define-public crate-mips-mcu-0.1.0 (c (n "mips-mcu") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)))) (h "0r6dl23pqas44lb3721g9cgpvdspkyns1905sn0ahzrgadcq591q")))

(define-public crate-mips-mcu-0.2.0 (c (n "mips-mcu") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)))) (h "0ig9h7fbhj0jnhns90r971306j51jsv4g3vyqvg30idb375xzf8c")))

(define-public crate-mips-mcu-0.3.0 (c (n "mips-mcu") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1pgm9yifajyrzfpf918szdil5n7picy64vc105k2m57ygp79ffph") (f (quote (("critical-section-single-core" "critical-section/restore-state-u32"))))))

(define-public crate-mips-mcu-0.3.1 (c (n "mips-mcu") (v "0.3.1") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1d3v0q2xd0nk5al8diyf0jygqa7i74hgwbw9vd69gyv3lfh17q0j") (f (quote (("critical-section-single-core" "critical-section/restore-state-u32"))))))

