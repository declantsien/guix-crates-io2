(define-module (crates-io mi ps mipsasm) #:use-module (crates-io))

(define-public crate-mipsasm-1.0.0 (c (n "mipsasm") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1zrajwj215l3yjqcjnsi17b0b6d627xgfi5rbmp9nin2nfwf6sci")))

(define-public crate-mipsasm-2.0.0 (c (n "mipsasm") (v "2.0.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "02zpv541qvxmja93gxp32iqqnjn6hh2sfv59agrd5g4acmw3gi1n")))

(define-public crate-mipsasm-2.0.1 (c (n "mipsasm") (v "2.0.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "06dp2lj4hrfqqw0q5x34aaqj9fqn6wlwmzzdqz1zkvy01vdaq1wf")))

