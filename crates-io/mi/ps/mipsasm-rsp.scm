(define-module (crates-io mi ps mipsasm-rsp) #:use-module (crates-io))

(define-public crate-mipsasm-rsp-1.0.0 (c (n "mipsasm-rsp") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "1hw50gjdl3xlb7vqkbqli5nwg4b6a2cfm1wqbdpq71abkp63cfvb")))

(define-public crate-mipsasm-rsp-1.0.1 (c (n "mipsasm-rsp") (v "1.0.1") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "14a4lp4slvmn4migx5wdy3dkq27j086k9krr6sacfmyxr0s167r9")))

(define-public crate-mipsasm-rsp-1.1.1 (c (n "mipsasm-rsp") (v "1.1.1") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "1x6z4khz96ja324pp749kszs2pkv743q97hvr7qczc8ymy1c8z28")))

(define-public crate-mipsasm-rsp-1.1.2 (c (n "mipsasm-rsp") (v "1.1.2") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "1rxk2qhvgymw695ggppa6kd01z940ikqi6cix0pafyra71wzsplj")))

(define-public crate-mipsasm-rsp-1.1.3 (c (n "mipsasm-rsp") (v "1.1.3") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "1qs22404c999lbi18ka08k0hrbglsmhhbfa5xybw0pdny3qrjq4w")))

(define-public crate-mipsasm-rsp-1.2.0 (c (n "mipsasm-rsp") (v "1.2.0") (d (list (d (n "num") (r "^0.4.0") (k 0)) (d (n "strum") (r "^0.24.1") (k 0)) (d (n "strum_macros") (r "^0.24.3") (k 0)))) (h "1mrdh5pilmjc5yah23fj266cn7nzwjz4hvivm12m8429gj2j4k0c")))

