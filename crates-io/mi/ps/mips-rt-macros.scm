(define-module (crates-io mi ps mips-rt-macros) #:use-module (crates-io))

(define-public crate-mips-rt-macros-0.2.1 (c (n "mips-rt-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1l999qrawnsg3ssss5g45pnqb0xmlwwfgijjfmsm2322qvxj7hp2")))

(define-public crate-mips-rt-macros-0.3.0 (c (n "mips-rt-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10z0nviz2bavqy3mmzcygcva7hr6vrnrlbzhxv0bn2jbnc5npzs2")))

(define-public crate-mips-rt-macros-0.3.4 (c (n "mips-rt-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vlrbchrlzlfv8gcqzxiv1fmdv903km6bcpg6yhi2v08wzd7yby9")))

