(define-module (crates-io mi ps mips-mcu-alloc) #:use-module (crates-io))

(define-public crate-mips-mcu-alloc-0.5.0 (c (n "mips-mcu-alloc") (v "0.5.0") (d (list (d (n "linked_list_allocator") (r "^0.10.3") (f (quote ("const_mut_refs"))) (k 0)) (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (d #t) (k 0)))) (h "1fc1qnmrzl8jzasapnnwqmp0d6xyl32cid67rw5djz6mhpcd7vmi")))

(define-public crate-mips-mcu-alloc-0.6.0 (c (n "mips-mcu-alloc") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.3") (f (quote ("const_mut_refs"))) (k 0)) (d (n "mips-mcu") (r "^0.3.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (d #t) (k 0)))) (h "1v88xs0rc89qafhij3lyiwyfz9mx25sgfbhk5a3f9slcn9vlzdkh")))

(define-public crate-mips-mcu-alloc-0.6.1 (c (n "mips-mcu-alloc") (v "0.6.1") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.3") (f (quote ("const_mut_refs"))) (k 0)) (d (n "log") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "mips-mcu") (r "^0.3.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (d #t) (k 0)))) (h "1gqbzxa2m3v3q5w4r3wb4w0fgkljcjn2j4yp1sidxpldwazyfzwd")))

(define-public crate-mips-mcu-alloc-0.6.2 (c (n "mips-mcu-alloc") (v "0.6.2") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.3") (f (quote ("const_mut_refs"))) (k 0)) (d (n "log") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "mips-mcu") (r "^0.3.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (d #t) (k 0)))) (h "0fyz9xpbws9gjxlqsb6bniplim1rvxw2xwnwkrilcnlsj2hi6m9r")))

