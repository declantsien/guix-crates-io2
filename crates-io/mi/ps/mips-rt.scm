(define-module (crates-io mi ps mips-rt) #:use-module (crates-io))

(define-public crate-mips-rt-0.2.1 (c (n "mips-rt") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "mips-rt-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "19xb9xxw1jwxwdgly5ppx95bd442yay26044ak660ry4m9prpvq4")))

(define-public crate-mips-rt-0.3.0 (c (n "mips-rt") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "mips-rt-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0n0qgm0ppvvmb4ypk3pf5g72a2ad4q1d89prln7vms5hxzspjyg3")))

(define-public crate-mips-rt-0.3.1 (c (n "mips-rt") (v "0.3.1") (d (list (d (n "mips-rt-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0j9swdccqwmym3hdn2id9xz49c6c90ncjb8131ypw3j4hfn28m9x")))

(define-public crate-mips-rt-0.3.2 (c (n "mips-rt") (v "0.3.2") (d (list (d (n "mips-rt-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0cpkn00npfw5ikgy2mj03vcrghmgvy9ddgi9q3k4r2b620n45m82")))

(define-public crate-mips-rt-0.3.4 (c (n "mips-rt") (v "0.3.4") (d (list (d (n "mips-rt-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0q87p0v719xw3llihgiyx3h9bg64qa86lnwbl0q9rny3z9aph3nm")))

(define-public crate-mips-rt-0.3.5 (c (n "mips-rt") (v "0.3.5") (d (list (d (n "mips-rt-macros") (r "^0.3.4") (d #t) (k 0)))) (h "01ss2px3v1v330cs090cnz8dbxdfhphlah8116v9j3pj4km736b7")))

(define-public crate-mips-rt-0.3.6 (c (n "mips-rt") (v "0.3.6") (d (list (d (n "mips-rt-macros") (r "^0.3.4") (d #t) (k 0)))) (h "1355hs94l088avz03r3msz3q7hlzbd52xsz0kyyc71c3v27cq78s")))

