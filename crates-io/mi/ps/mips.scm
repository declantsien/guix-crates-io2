(define-module (crates-io mi ps mips) #:use-module (crates-io))

(define-public crate-mips-0.1.0 (c (n "mips") (v "0.1.0") (h "152k8c38jbbdadk8xjdh6k1m9dipa30c7irybgz3z4vyzz05977p")))

(define-public crate-mips-0.2.0 (c (n "mips") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "00n8wvqiq69gs05cg9x3g7kja2cc0r16nbb6kyppgnz4cfd49gsc")))

(define-public crate-mips-0.2.1 (c (n "mips") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1yrjdj60v5qxkfallwglchr3i6ax05bvb4mzzvz8x0kvknsi8fh5")))

