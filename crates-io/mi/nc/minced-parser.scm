(define-module (crates-io mi nc minced-parser) #:use-module (crates-io))

(define-public crate-minced-parser-1.0.0 (c (n "minced-parser") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0g6c64d62y2igxf4s7vn035k1djmr3lj5bg826fycq2va1izyaky")))

(define-public crate-minced-parser-2.0.0 (c (n "minced-parser") (v "2.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1s9y1rcyd0gxks8gf81d14nd3in233wsn7q68h1x678knsa0lpg2")))

