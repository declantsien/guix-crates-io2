(define-module (crates-io mi nc mincore) #:use-module (crates-io))

(define-public crate-mincore-0.1.0 (c (n "mincore") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("fs" "mm" "param"))) (d #t) (k 0)))) (h "0l8845jrz3v21amkf3fqb9gf8jckjwm510fbr834966nzhpl74p8") (y #t)))

(define-public crate-mincore-0.1.1 (c (n "mincore") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("fs" "mm" "param"))) (d #t) (k 0)))) (h "0kxng46v2i0jq386kqps2mqbi01xwj55i1wqsc1s1p81phhvcq7z")))

