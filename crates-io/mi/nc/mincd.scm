(define-module (crates-io mi nc mincd) #:use-module (crates-io))

(define-public crate-mincd-0.1.0 (c (n "mincd") (v "0.1.0") (h "0g8jvn1y877ykcn9w4sc8z5lnd5c632r0lqsqz9s81hwvs8f8ysb")))

(define-public crate-mincd-0.1.1 (c (n "mincd") (v "0.1.1") (h "17v3gq7fb1lj068237j0x222f08jnslh33150wxx6g2q8nzqrmp9")))

(define-public crate-mincd-0.1.2 (c (n "mincd") (v "0.1.2") (h "1vqwc44jvkqqw84sy6akgisb4zw1r4gmgs7m4kvnmm35nblxssq7")))

(define-public crate-mincd-0.1.3 (c (n "mincd") (v "0.1.3") (h "04911wzkfj6vng7m06knwsx5c67rvvn1qw2jp0qiaxdsbc8s23b1")))

(define-public crate-mincd-0.1.4 (c (n "mincd") (v "0.1.4") (h "1wpcvchmj7j4wi4cfbvsxfn6n8lkcjr68dqxx7wx0xjcz21x4mzh")))

(define-public crate-mincd-0.1.5 (c (n "mincd") (v "0.1.5") (h "15vsq1fpiiicdh6j9mw7gfak28rirl5n6s5gr0wd3305kdwnrdsk")))

(define-public crate-mincd-0.1.6 (c (n "mincd") (v "0.1.6") (h "0hhcvhckdj35y829wghz3br9ah835vj13ga8ci5akmajv66d3npd")))

