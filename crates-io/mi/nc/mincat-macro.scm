(define-module (crates-io mi nc mincat-macro) #:use-module (crates-io))

(define-public crate-mincat-macro-0.1.0 (c (n "mincat-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0fdq3wg66wlbbx6am2bsh16nfzdvrf82y3v2qhs7q9c3wp8yl3pz")))

