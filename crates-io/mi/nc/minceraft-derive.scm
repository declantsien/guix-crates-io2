(define-module (crates-io mi nc minceraft-derive) #:use-module (crates-io))

(define-public crate-minceraft-derive-0.1.0 (c (n "minceraft-derive") (v "0.1.0") (h "14h8i52jllh03wqpy5k09imw62asynqzfgc59m7zmdkmw9ywanv3")))

(define-public crate-minceraft-derive-0.2.0 (c (n "minceraft-derive") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "minceraft") (r "^0.2.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y0xcvkhj2349nd9ma1gr6ldqiwvvidi9z4ndniy1492dh9n1mav")))

