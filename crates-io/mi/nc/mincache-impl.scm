(define-module (crates-io mi nc mincache-impl) #:use-module (crates-io))

(define-public crate-mincache-impl-0.1.0 (c (n "mincache-impl") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14ncrsvmc4fvlklggkl4w5achiq9mwlzayj5zi5z8ddbh3s2lvl9")))

