(define-module (crates-io mi nc minc) #:use-module (crates-io))

(define-public crate-minc-0.0.0 (c (n "minc") (v "0.0.0") (d (list (d (n "miscreant") (r "^0.2") (d #t) (k 0)))) (h "1n2fpzv4zq7wnxil2a9alk6p6g40ck889f5yh42kzs04c3j7i7sh") (y #t)))

(define-public crate-minc-0.0.1 (c (n "minc") (v "0.0.1") (h "0m891i10p88jgr6ic21mvx61q7h0zacga45kwas2n32214s599wc") (y #t)))

