(define-module (crates-io mi nc mincolor) #:use-module (crates-io))

(define-public crate-mincolor-2.0.0 (c (n "mincolor") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)))) (h "09m67xvf6fjm23qxp3k7i3dm4j0pgrvfr25p3dq8iss360waazm3") (f (quote (("no-color"))))))

(define-public crate-mincolor-2.0.1 (c (n "mincolor") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)))) (h "1yhc11f496c8m430fpf3jgadgghbhygvwixydykh57v34q0708v0") (f (quote (("no-color"))))))

