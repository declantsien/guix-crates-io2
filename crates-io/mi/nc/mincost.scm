(define-module (crates-io mi nc mincost) #:use-module (crates-io))

(define-public crate-mincost-0.1.0 (c (n "mincost") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "1xkpb3f2hiwzyn4g38m9rx2dlhqwasbajvkj1fqhhnr2vvkwfxws") (f (quote (("sa") ("pso") ("ga"))))))

(define-public crate-mincost-0.1.1 (c (n "mincost") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "08y3j0aamxc4hlwzkmqfbpnhvxqqh9sd795w38w1gbd94c54abfa") (f (quote (("sa") ("pso") ("ga") ("default" "ga" "sa" "pso"))))))

(define-public crate-mincost-0.1.2 (c (n "mincost") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "0h15ja3zbxim3hjmk2m8rvrih74qkrdyn75m43j8v7rg1a12q12b") (f (quote (("shuffle") ("sa") ("pso") ("normal") ("ga") ("default" "ga" "sa" "pso" "normal"))))))

(define-public crate-mincost-0.1.3 (c (n "mincost") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "0jlslfr8kda99w9glqyz4a7ar9gpag69bfs160pc3l6db72279q3") (f (quote (("shuffle") ("sa") ("pso") ("normal") ("ga") ("default" "ga" "sa" "pso" "normal"))))))

