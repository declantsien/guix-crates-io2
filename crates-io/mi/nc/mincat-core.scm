(define-module (crates-io mi nc mincat-core) #:use-module (crates-io))

(define-public crate-mincat-core-0.1.0 (c (n "mincat-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "http-body") (r "^1.0.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.3") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "mincat-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "sync_wrapper") (r "^0.1.2") (d #t) (k 0)))) (h "1cb4vpc3c0b658y823y796pim0ljya4rhmx8s7rln4hccdn0dhxi")))

