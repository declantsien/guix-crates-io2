(define-module (crates-io mi gf migformatting) #:use-module (crates-io))

(define-public crate-migformatting-0.1.0 (c (n "migformatting") (v "0.1.0") (h "18hsg1wlh4zdy05qb8mcf6lbd0m4cbcgh1wjv1i3r1sgar33v4aq") (y #t)))

(define-public crate-migformatting-0.1.1 (c (n "migformatting") (v "0.1.1") (h "0sij192z0rln10ighi61140za5bz8rq3dhm08mzph5y7yrd4qmma")))

