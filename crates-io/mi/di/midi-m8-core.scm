(define-module (crates-io mi di midi-m8-core) #:use-module (crates-io))

(define-public crate-midi-m8-core-1.2.0 (c (n "midi-m8-core") (v "1.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "m8-files") (r "^0.2") (d #t) (k 0)) (d (n "midi-msg") (r "^0.4") (d #t) (k 0)))) (h "008m0c2y1rv7pakvbqc7vk3jaxzhi63xvlblv61fwzmxz2nahvvp")))

(define-public crate-midi-m8-core-1.3.0 (c (n "midi-m8-core") (v "1.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "m8-files") (r "^0.2") (d #t) (k 0)) (d (n "midi-msg") (r "^0.4") (d #t) (k 0)))) (h "1hxhfjwxgv78hghw7s1xz8fn08768cjg9zbfyq51jlypxh9ajbqy")))

