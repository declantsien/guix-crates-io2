(define-module (crates-io mi di midi-beeper) #:use-module (crates-io))

(define-public crate-midi-beeper-0.1.1 (c (n "midi-beeper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom-midi") (r "^0.5.1") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0pjysskqqy5zwbw55ffxy9524zxqp3xcxlh3d4bys5qrps9axmjp")))

(define-public crate-midi-beeper-0.1.2 (c (n "midi-beeper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom-midi") (r "^0.5.1") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07xfq75qz6yrzxmhjhfzr0bbfvayzs26f2jgaxhx95xjka0sdv34")))

