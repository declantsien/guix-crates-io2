(define-module (crates-io mi di midi-convert) #:use-module (crates-io))

(define-public crate-midi-convert-0.1.0 (c (n "midi-convert") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.2") (d #t) (k 0)))) (h "0wsz6r1xcsclc1xbbz6c6s88wyv65xxw2a5c2ih7359pfg4md1wg")))

(define-public crate-midi-convert-0.1.1 (c (n "midi-convert") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.3") (d #t) (k 0)))) (h "1pp8ln9bdykx9ww0as9fi3bn2ixqas8fxpzpy3rfmyhhyx7aark4") (s 2) (e (quote (("defmt" "dep:defmt" "midi-types/defmt"))))))

(define-public crate-midi-convert-0.1.2 (c (n "midi-convert") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.6") (d #t) (k 0)))) (h "1lay7nzrlddwlpdywl6h8xj0phswxw5g7hq7ha3pxrhxj8564z3i") (s 2) (e (quote (("defmt" "dep:defmt" "midi-types/defmt")))) (r "1.60.0")))

(define-public crate-midi-convert-0.1.3 (c (n "midi-convert") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.7") (d #t) (k 0)))) (h "1bccla5d2zb0q7mk1wnxa9j6wm843dffhfzf8jqy4zsjy7mxn0s2") (s 2) (e (quote (("defmt" "dep:defmt" "midi-types/defmt")))) (r "1.62.1")))

(define-public crate-midi-convert-0.2.0 (c (n "midi-convert") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.7") (d #t) (k 0)))) (h "1xzjfyagx52y449prjjsjp2rw6mi22fcwg8yqhflh3a5kmppgx0q") (s 2) (e (quote (("defmt" "dep:defmt" "midi-types/defmt")))) (r "1.62.1")))

