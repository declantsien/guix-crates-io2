(define-module (crates-io mi di midi-event) #:use-module (crates-io))

(define-public crate-midi-event-0.1.0 (c (n "midi-event") (v "0.1.0") (d (list (d (n "arbitrary") (r "^0.4.2") (f (quote ("derive"))) (d #t) (t "cfg(fuzzing)") (k 0)))) (h "0dkv29d09j5rmnbnfw6q1803v56ghr9shqsaz4ggsxplzhvsh9hi")))

(define-public crate-midi-event-0.2.0 (c (n "midi-event") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4.2") (f (quote ("derive"))) (d #t) (t "cfg(fuzzing)") (k 0)))) (h "1mxr4bphnc9lbj9wm1ijjmlzr9bkkva89a3lhnhbxr26x8648syz")))

(define-public crate-midi-event-0.2.1 (c (n "midi-event") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.4.2") (f (quote ("derive"))) (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n1dr144vsqka9mjx7f3cawzlqmwl48zkyj916kq8h3i2k0rphjb")))

