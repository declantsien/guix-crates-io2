(define-module (crates-io mi di midi-types) #:use-module (crates-io))

(define-public crate-midi-types-0.1.0 (c (n "midi-types") (v "0.1.0") (h "011fq4z65z14l51cccqsa0yydi7wr38wjn1gk44xscdlsa5cd0nj")))

(define-public crate-midi-types-0.1.1 (c (n "midi-types") (v "0.1.1") (h "1vdr2llv1n2i0msgxb9dzx0g2px300h5k49s4wim77pydankk5z2")))

(define-public crate-midi-types-0.1.2 (c (n "midi-types") (v "0.1.2") (h "0zxx1i04m94jvdd1hjlimymscahgy3xhxic68pgzyigbdh1iln3i") (r "1.51")))

(define-public crate-midi-types-0.1.3 (c (n "midi-types") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1x9awk3hia8v3zn6a53vk78byj6d25bmhr68ndq0962pmc9k70h4") (r "1.51")))

(define-public crate-midi-types-0.1.4 (c (n "midi-types") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0wpazffjjv84nbpkfhxyh0vn09lisk88d97072kck4ygf3f8qj7n") (r "1.56.0")))

(define-public crate-midi-types-0.1.5 (c (n "midi-types") (v "0.1.5") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "018qci3dnj4c68cgg16883dllbx9n8lrbmwwjiam6pars6dqv0db") (r "1.56.0")))

(define-public crate-midi-types-0.1.6 (c (n "midi-types") (v "0.1.6") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0lwz7d1bh4hw47ram6fxkzczfdvrha4q3r48p1wpn2vfsy5csksg") (r "1.56.0")))

(define-public crate-midi-types-0.1.7 (c (n "midi-types") (v "0.1.7") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1dcf9pxygxq86jrdgbl44x876mxndd18h1vrgna39i75ar9bw2zg") (r "1.62.1")))

