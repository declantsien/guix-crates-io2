(define-module (crates-io mi di midi_file) #:use-module (crates-io))

(define-public crate-midi_file-0.0.0 (c (n "midi_file") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "148kgz8b9zahp14g5xa5zna87syly402zilbl74bc90ya8izhxjz")))

(define-public crate-midi_file-0.0.1 (c (n "midi_file") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0azbkwijbj77fws5v8fn4isl24bwqnnm63r3nknpq6mwbaqfl287")))

(define-public crate-midi_file-0.0.2 (c (n "midi_file") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04fpsvkjx5mnp4cz4y2p23j1ki87lshs9if0rxrq9y84qswr66sd")))

(define-public crate-midi_file-0.0.3 (c (n "midi_file") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bamikrmnk2nadn3kcd5ickl94d3kd6jpdjnza4rp83623wdf3bc")))

(define-public crate-midi_file-0.0.4 (c (n "midi_file") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hjfm5jn96w85cxh8cvnhb6h6s4cp4yn8yl7j9a2arm7qz66dcxg")))

(define-public crate-midi_file-0.0.5 (c (n "midi_file") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02c008lllfw30zfcqfg1mvmfj92xq5vbvg23fwxv2xy0p5x24k1s")))

