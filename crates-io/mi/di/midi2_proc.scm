(define-module (crates-io mi di midi2_proc) #:use-module (crates-io))

(define-public crate-midi2_proc-0.2.1 (c (n "midi2_proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "11wlsq5vjxzl4qfzd65jik9y1i36aalydiar44vwh45s6m8ywixi")))

(define-public crate-midi2_proc-0.2.2 (c (n "midi2_proc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1sb0ncm53lv1fjcjcz8q58xpxq34xdd4rz4p6p1hp8dsr1fr3qwl")))

(define-public crate-midi2_proc-0.2.3 (c (n "midi2_proc") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "12dmzn03r7qis48x5w3xhsnzgfyxv8w2ivyicm0dgz9hz9q8qxdg")))

(define-public crate-midi2_proc-0.2.4 (c (n "midi2_proc") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "16m96aqly7v7xb0hf5cy8vh2vg7vs73ckr4f6ww2r22kb71s11aa")))

(define-public crate-midi2_proc-0.3.0 (c (n "midi2_proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1ql11m8q6avwp3fv3vncc97zmpkx0wvx5c022wr46bfcksc0yzkl")))

(define-public crate-midi2_proc-0.3.1 (c (n "midi2_proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0jap3mfhrk5220k7i4j9v8j183axv126mzpwa41y55qz4rpk4qqr")))

(define-public crate-midi2_proc-0.4.0 (c (n "midi2_proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1shy39wmlnb260l1zah1gmfjxs7ljpvb4q87rzpfi5zf08aqmxzs")))

(define-public crate-midi2_proc-0.5.0 (c (n "midi2_proc") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0d4c5vvzj9dl58b1m6brzyapvcylbqjnm2ah1p8x99lhqsn8c22v")))

(define-public crate-midi2_proc-0.5.1 (c (n "midi2_proc") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8bc3sq0382jx50wwjnrwbhm02ls3fapy1hvgaarnxj96i012xc")))

(define-public crate-midi2_proc-0.5.3 (c (n "midi2_proc") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1ss8qbvs2di3yp66yb2d4w92m95b6xd41kjqraipnnavwrw8b5ix")))

