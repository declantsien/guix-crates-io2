(define-module (crates-io mi di midi-reader-writer) #:use-module (crates-io))

(define-public crate-midi-reader-writer-0.1.0 (c (n "midi-reader-writer") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "midly-0-5") (r "^0.5.2") (f (quote ("std"))) (o #t) (k 0) (p "midly")) (d (n "timestamp-stretcher") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "19bzdvfcr53xp8liz4ij1s6scg78w576pgdyqzrhi48slwyaygw9") (f (quote (("read" "itertools") ("engine-midly-0-5" "midly-0-5") ("default" "convert-time" "read") ("convert-time" "timestamp-stretcher"))))))

(define-public crate-midi-reader-writer-0.1.1 (c (n "midi-reader-writer") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "midly-0-5") (r "^0.5.2") (f (quote ("std"))) (o #t) (k 0) (p "midly")) (d (n "timestamp-stretcher") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11wb6nakrprdpjq8x2pd9mb9ah6vzkxb54lblc3wyf54i8wv473k") (f (quote (("read" "itertools") ("engine-midly-0-5" "midly-0-5") ("default" "convert-time" "read") ("convert-time" "timestamp-stretcher"))))))

(define-public crate-midi-reader-writer-0.1.2 (c (n "midi-reader-writer") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "midly-0-5") (r "^0.5.2") (f (quote ("std"))) (o #t) (k 0) (p "midly")) (d (n "timestamp-stretcher") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "06dp133zs71xzvlpjiqm9lp6qxnzl2njfsyv7yxdi1ky46y2x381") (f (quote (("read" "itertools") ("engine-midly-0-5" "midly-0-5") ("default" "convert-time" "read") ("convert-time" "timestamp-stretcher"))))))

