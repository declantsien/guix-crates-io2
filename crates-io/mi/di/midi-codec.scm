(define-module (crates-io mi di midi-codec) #:use-module (crates-io))

(define-public crate-midi-codec-0.0.0 (c (n "midi-codec") (v "0.0.0") (h "1v2qrp1qvddl1a24yi5d0dlkxpd6xs3c14j82fj5x2ahp0ls7r18")))

(define-public crate-midi-codec-0.1.0 (c (n "midi-codec") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "1cnp1y4valmp8jq18cnif6rdm62cnpcr8hv2hr6mfy1pc9frzgws") (f (quote (("std" "alloc" "thiserror") ("default" "std") ("alloc"))))))

(define-public crate-midi-codec-0.2.0 (c (n "midi-codec") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "0wl84llvmgxd2013xx6j0q3bcxfrpqyppmwgh8xl2l1kngddjzs5") (f (quote (("std" "alloc" "thiserror") ("default" "std") ("alloc"))))))

(define-public crate-midi-codec-0.3.0 (c (n "midi-codec") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "1bla0qnn2h3160zgqqg2v1j6xf89c6pvh1cpjm5009b438j3y7z4") (f (quote (("std" "alloc" "thiserror") ("default" "std") ("alloc"))))))

(define-public crate-midi-codec-0.3.1 (c (n "midi-codec") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "083v3wl8mp3vzf2gz1xrhg7xzhfqgdy4vasj29zggl55rj4aig5v") (f (quote (("std" "alloc" "thiserror") ("default" "std") ("alloc"))))))

(define-public crate-midi-codec-0.3.2 (c (n "midi-codec") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "1mr11359jg2q783mqvq1x9dmmi371i5ac5r9lkgfym6qpwnd1623") (f (quote (("std" "alloc" "thiserror") ("default" "std") ("alloc"))))))

(define-public crate-midi-codec-0.4.0 (c (n "midi-codec") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (k 0)))) (h "0gjfyx0p33nr2jpyf1vn2nnl5bwl6hlqki52cmvdilrgd6avm5sx") (f (quote (("std" "alloc" "thiserror" "tracing/std") ("default" "std") ("alloc"))))))

