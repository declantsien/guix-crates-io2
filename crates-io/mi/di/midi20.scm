(define-module (crates-io mi di midi20) #:use-module (crates-io))

(define-public crate-midi20-0.1.0 (c (n "midi20") (v "0.1.0") (h "0506gz2qlm2cg4g23ypin1kl0j6ksx8kk9ps0v2p71gc21bhc1g3") (f (quote (("no-std") ("default"))))))

(define-public crate-midi20-0.1.1 (c (n "midi20") (v "0.1.1") (h "03ypvzya0qn62zn5yd2jvjxxhf79insik6mlv747hkq5c4xp4nsd") (f (quote (("no-std") ("default"))))))

(define-public crate-midi20-0.2.0 (c (n "midi20") (v "0.2.0") (h "1h7gdsxb4zb638j0fd6wi6iyiqg4ishmdgl33qx7pm1bnk9bdik9") (f (quote (("no-std") ("default"))))))

