(define-module (crates-io mi di midi-msg) #:use-module (crates-io))

(define-public crate-midi-msg-0.1.0 (c (n "midi-msg") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q5nr4n4hp5r17j3r6cwxlax8cp2727zzp9znc5d9ka86x2prix6")))

(define-public crate-midi-msg-0.2.0 (c (n "midi-msg") (v "0.2.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0fiy1yfh0mgximr1yyd02g6q2km404bbfwl2vxg9sjxix2wqwh5y")))

(define-public crate-midi-msg-0.2.1 (c (n "midi-msg") (v "0.2.1") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wg9jlyixjkg6b6syxvc7fk2rhyaf3jlvhzqajvl1fkwxf1il54j")))

(define-public crate-midi-msg-0.3.0 (c (n "midi-msg") (v "0.3.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)))) (h "1d30lj1p3hykvmvndfzm1by484mm9wi8sai90y1ii30g451vkdkk")))

(define-public crate-midi-msg-0.4.0 (c (n "midi-msg") (v "0.4.0") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)))) (h "1iks0cv3jf951kwrmwjfdrppans23ylck1h987pxlxckyap9s3zw") (f (quote (("sysex" "bstr") ("std") ("default" "std" "sysex"))))))

(define-public crate-midi-msg-0.5.0 (c (n "midi-msg") (v "0.5.0") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v53kbvgpv6krf2qpnlk8dyc0v6ir0vwpgl8lnxr41cxph9swjms") (f (quote (("sysex" "bstr") ("std" "strum") ("default" "std" "sysex"))))))

(define-public crate-midi-msg-0.6.0 (c (n "midi-msg") (v "0.6.0") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jfnnyh04qs1hw08q1ky813v3r05gj0w936c012m6ljxp48sf15p") (f (quote (("sysex" "bstr") ("std" "strum") ("file" "sysex") ("default" "std" "sysex" "file"))))))

(define-public crate-midi-msg-0.6.1 (c (n "midi-msg") (v "0.6.1") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10x15na383dvga56gp7rqbxpa93xz7c3djk56g97daqrla75p652") (f (quote (("sysex" "bstr") ("std" "strum") ("file" "sysex") ("default" "std" "sysex" "file"))))))

(define-public crate-midi-msg-0.7.0 (c (n "midi-msg") (v "0.7.0") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fz78zxhvdmn70mbyhzs7zwdq2ignvdshl5z43gsm7shsrrrprdn") (f (quote (("sysex" "bstr") ("std" "strum") ("file" "sysex") ("default" "std" "sysex" "file"))))))

(define-public crate-midi-msg-0.7.1 (c (n "midi-msg") (v "0.7.1") (d (list (d (n "bstr") (r "^1.0.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "micromath") (r "^1.1.1") (d #t) (k 0)) (d (n "midir") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19pgidwpmyxnynx7lm5qmi713q1ggwiqn74hvfxafywvxbb1qncz") (f (quote (("sysex" "bstr") ("std" "strum") ("file" "sysex") ("default" "std" "sysex" "file"))))))

