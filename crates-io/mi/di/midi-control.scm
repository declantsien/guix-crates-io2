(define-module (crates-io mi di midi-control) #:use-module (crates-io))

(define-public crate-midi-control-0.1.0 (c (n "midi-control") (v "0.1.0") (d (list (d (n "midir") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0l1pzhghdlgfhdjijjwnb6c8nyyskp9kjsjldv8jn3mz94bni3xf") (f (quote (("transport" "midir") ("default" "transport"))))))

(define-public crate-midi-control-0.2.0 (c (n "midi-control") (v "0.2.0") (d (list (d (n "midir") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "03fmm5awd14gkvqz5ly0h11khl3vih7bld8brpyi65ayhdf6vr41") (f (quote (("transport" "midir") ("default" "transport"))))))

(define-public crate-midi-control-0.2.1 (c (n "midi-control") (v "0.2.1") (d (list (d (n "midir") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1pqgj9c9f040igcqammlqb1c3lzz5kd2563kvra24d5z1kfdq50j") (f (quote (("transport" "midir") ("default" "transport"))))))

(define-public crate-midi-control-0.2.2 (c (n "midi-control") (v "0.2.2") (d (list (d (n "midir") (r "^0.9.1") (o #t) (d #t) (k 0)))) (h "1awg5anvdk0savvl33cyy8xvh3fygghvs8y3yfr65kqhvklynv7x") (f (quote (("transport" "midir") ("default" "transport"))))))

