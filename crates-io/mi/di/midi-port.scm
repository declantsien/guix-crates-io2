(define-module (crates-io mi di midi-port) #:use-module (crates-io))

(define-public crate-midi-port-0.1.0 (c (n "midi-port") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32f4") (r "^0.8.0") (f (quote ("stm32f407" "rt"))) (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.5.0") (f (quote ("rt" "stm32f407"))) (k 2)))) (h "0naps26d9g1w3gxw44wf6y49wjf0naymkvk63z3j72h290vbas6l")))

