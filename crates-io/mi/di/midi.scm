(define-module (crates-io mi di midi) #:use-module (crates-io))

(define-public crate-midi-0.0.1 (c (n "midi") (v "0.0.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "1qi9b0lsk7hmm7yx39h0a5yf7jc1zbff1v5lc8smic2p7c2qd3gr")))

(define-public crate-midi-0.0.2 (c (n "midi") (v "0.0.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "1168442zwzj2d43qay1q0vy8fzf0swqi9fadmgjwim04lbhmc4hh")))

(define-public crate-midi-0.0.3 (c (n "midi") (v "0.0.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "0jnbpdgc608djjz2f867xls4rvh8nq7k36d18rfc5s1lxxcwr3pb")))

(define-public crate-midi-0.0.4 (c (n "midi") (v "0.0.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)))) (h "01pd4kqrawpka491437h94i10nmppixxvh21k0lx0lmbm7chs1cq")))

(define-public crate-midi-0.0.5 (c (n "midi") (v "0.0.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)))) (h "0xhh4xvs896f9ch6wnzk4sj1x0na3mzmb0njcmr29dkm3qp80wp2")))

(define-public crate-midi-0.0.6 (c (n "midi") (v "0.0.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)))) (h "1dcqiid2qw5hccikbk7qf0djm1ri2w4w271sjqsvrmqk5pb5jjgx")))

(define-public crate-midi-0.0.7 (c (n "midi") (v "0.0.7") (h "0pqpf11igk2j9wk8nspv2dradmwkm2w5yllib9gb1kcl1mggxqa4")))

(define-public crate-midi-0.1.0 (c (n "midi") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0x6ayy1jc3gkw5ab8x2n7h7d55qz1sp5hr3a3y8i7yy3fl74j7k9")))

