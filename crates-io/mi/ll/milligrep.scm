(define-module (crates-io mi ll milligrep) #:use-module (crates-io))

(define-public crate-milligrep-1.0.0 (c (n "milligrep") (v "1.0.0") (h "18arcpfb5b3025p0v5vhf0lyk5w9sf8qbcqbk80h1ilc1qjhd5nl")))

(define-public crate-milligrep-1.0.1 (c (n "milligrep") (v "1.0.1") (h "1vy9h8fwcww320xsgcmipb5xm45zdvbcii6himkajmq26xzh07h4")))

(define-public crate-milligrep-1.0.2 (c (n "milligrep") (v "1.0.2") (h "199qg9mcw1d4w0r6mgiw253zxaq5v6kjklik1gcv1hddnxmnd6rh")))

(define-public crate-milligrep-1.0.3 (c (n "milligrep") (v "1.0.3") (h "07jlxgw60z6f8bsxdysmjc82ixl7z15vrrh0s5cg85khf08ja0ij")))

