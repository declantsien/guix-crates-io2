(define-module (crates-io mi ll miller_rabin) #:use-module (crates-io))

(define-public crate-miller_rabin-1.0.0 (c (n "miller_rabin") (v "1.0.0") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "0n1lihcgsl4d0qipn45hmhm827scx6z2w8r9ay3p1rzpacj7asv8")))

(define-public crate-miller_rabin-1.0.1 (c (n "miller_rabin") (v "1.0.1") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "0f4jvc39fqpg3b87c1zrnxm6l5dg9fw11v12phjd68kil9l5f3hf")))

(define-public crate-miller_rabin-1.0.2 (c (n "miller_rabin") (v "1.0.2") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "022r0jkrs6bg2jha8z1my40ajbwb3a6v4hl8wj2lciplak8xggah")))

(define-public crate-miller_rabin-1.0.3 (c (n "miller_rabin") (v "1.0.3") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "0nnb019p4s4dkanxldbi78hflga7hbzr3vrjjm6irshs9kszsnq1")))

(define-public crate-miller_rabin-1.0.4 (c (n "miller_rabin") (v "1.0.4") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "02knim5ch531pscfdkcb1k5l5g1h3n67frc5bwnnrk523s685nfz")))

(define-public crate-miller_rabin-1.0.5 (c (n "miller_rabin") (v "1.0.5") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "1mi64xzzmvbxllkpqv1p5yz4n38zrfaaw76230vikgsyf8f0b1xp")))

(define-public crate-miller_rabin-1.0.6 (c (n "miller_rabin") (v "1.0.6") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "0x3wrcjxc0y3m5idzkyigw1jnkvd5i4xya7g4pw98w33iam2fd5m")))

(define-public crate-miller_rabin-1.1.0 (c (n "miller_rabin") (v "1.1.0") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)))) (h "1mxl577xc8lx3b1dms2c0zr8spn6z74n894mvfvi0nsyg1qf0lhl")))

(define-public crate-miller_rabin-1.1.1 (c (n "miller_rabin") (v "1.1.1") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0d876c1msdr4ka76xy38yi35fq4pliwg82az67g8jxwijwp6k7a4") (f (quote (("default" "rayon"))))))

