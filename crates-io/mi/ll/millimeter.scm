(define-module (crates-io mi ll millimeter) #:use-module (crates-io))

(define-public crate-millimeter-0.1.0 (c (n "millimeter") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rustc") (r "^0.9.4") (d #t) (k 1) (p "version_check")) (d (n "serde") (r "^1.0.132") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qfk0f52bks20nqbcjx13crk41x1jj56vgp222znwwk1ajsspbk9") (f (quote (("std" "serde/std" "thiserror") ("default" "std"))))))

