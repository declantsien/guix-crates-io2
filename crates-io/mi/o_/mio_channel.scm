(define-module (crates-io mi o_ mio_channel) #:use-module (crates-io))

(define-public crate-mio_channel-0.1.0 (c (n "mio_channel") (v "0.1.0") (d (list (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "001kkd8yzyyx13frc2rz3653q765hclmx9855kzhb0vhmknpgnbs")))

(define-public crate-mio_channel-0.1.1 (c (n "mio_channel") (v "0.1.1") (d (list (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0ww337fs1lc93lnrj28r6l3pmq82grkspn6yh558zp9z7m3f9w0d")))

(define-public crate-mio_channel-0.1.2 (c (n "mio_channel") (v "0.1.2") (d (list (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1vpirbhr24zpajb07wpjzw8w49hzi4gpcckd2jx996k1hchhcjzp")))

(define-public crate-mio_channel-0.1.3 (c (n "mio_channel") (v "0.1.3") (d (list (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "15v6cyicqcyjiip9307wnfp10jsm2bsn11xqwmn3hl5iy9nmc2qp")))

