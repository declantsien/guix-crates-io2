(define-module (crates-io mi o_ mio_tcp_loop) #:use-module (crates-io))

(define-public crate-mio_tcp_loop-0.1.0 (c (n "mio_tcp_loop") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "02q1my030nkhf5y1sav45qnqz4lxnvv94rkw9rwrgirb3pwa065v")))

(define-public crate-mio_tcp_loop-0.1.1 (c (n "mio_tcp_loop") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "16by521zrdb8mg7ixh925y7rkyw3aynxmi8pxafi1pm4dl5524hf")))

(define-public crate-mio_tcp_loop-0.1.2 (c (n "mio_tcp_loop") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "02s5ij3ibv8k4mwk5d2s82qzyjaby3bx01i073jlkw032xdx0brj")))

(define-public crate-mio_tcp_loop-0.1.4 (c (n "mio_tcp_loop") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "04258vhaacmv4jg4mpp6xhd4lliaj8qsn5vgpzh24hvpzdnxbrby")))

(define-public crate-mio_tcp_loop-0.1.5 (c (n "mio_tcp_loop") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "0lbz6ya6bp10l9w5w1w8q4sa8m3j5qvri6fmq8zwxini4x59wh3a")))

(define-public crate-mio_tcp_loop-0.1.6 (c (n "mio_tcp_loop") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "05qry9n550dvlj22p11bjdma74pb2hamnmib9aj9i329ky1g6q79")))

(define-public crate-mio_tcp_loop-0.1.7 (c (n "mio_tcp_loop") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "0177kssb8c81d3cn3mfqjp3i2al1sks6x3jrrny7qf76y7m6n6vj")))

(define-public crate-mio_tcp_loop-0.1.8 (c (n "mio_tcp_loop") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "1k3si9dddnimwhhmhbb57yy9p41nla9yfw13hdzdvvbirhmfgz2z")))

(define-public crate-mio_tcp_loop-0.1.9 (c (n "mio_tcp_loop") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "1n98n6c1d809pg1q2wgf05kj069jxlb2p7lp71zcf93kjbvf2m9x")))

(define-public crate-mio_tcp_loop-0.1.10 (c (n "mio_tcp_loop") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "07xcnwzcrnz4dvilp8xwh0bzglcln7xmis92larh474h15yblyqc")))

(define-public crate-mio_tcp_loop-0.1.11 (c (n "mio_tcp_loop") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "1k8j68kdj14mwkivfdfrx43ix66v6wwrb3g24qykjakpw8fzpl0d")))

(define-public crate-mio_tcp_loop-0.1.12 (c (n "mio_tcp_loop") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "1hdkhxfwdhh1q0fv9vk5vn8d0rpk05q7wm0xm59ff3qyj6abzwjx")))

(define-public crate-mio_tcp_loop-0.1.13 (c (n "mio_tcp_loop") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "07w1322sk4d3kpxhfm04mx0k25jig8iqzv4zz4jdjzpc8rj9mls6")))

(define-public crate-mio_tcp_loop-0.1.14 (c (n "mio_tcp_loop") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "179q7hfgzwkbff3rr5k7sgvfcmi5pighn33fyk1x5pc9wjbziysp")))

(define-public crate-mio_tcp_loop-0.1.15 (c (n "mio_tcp_loop") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "11rpdlx4pawpxl88w0lmsripamvn8czjy6gkgjxicz7kla54yg78")))

(define-public crate-mio_tcp_loop-0.1.16 (c (n "mio_tcp_loop") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "17caqqyphp3xd5k1yfilh3jq02xb4appfrc84ll8azk60p0bk150")))

(define-public crate-mio_tcp_loop-0.1.17 (c (n "mio_tcp_loop") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "kaniexpect") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.6") (d #t) (k 0)))) (h "0imqavbqysp22m172v6vk5blqrr67aw2i3yq5liq3v0vf9p7s1jk")))

