(define-module (crates-io mi nh minhtml) #:use-module (crates-io))

(define-public crate-minhtml-0.13.1 (c (n "minhtml") (v "0.13.1") (d (list (d (n "minify-html") (r "^0.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1f7p1qm0ajbafl66r7g8rp8rj31j1sa152d3gg3xvikv588yzk4f")))

(define-public crate-minhtml-0.13.2 (c (n "minhtml") (v "0.13.2") (d (list (d (n "minify-html") (r "^0.13.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dwik6p7pwv819x1am39n2yh8bsgyhl50b3gsr54jc9iv3fyybq5")))

(define-public crate-minhtml-0.13.3 (c (n "minhtml") (v "0.13.3") (d (list (d (n "minify-html") (r "^0.13.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ppz13qwlllfdfsf84c80fib4slq7190m7hxf2sa9lq3813basjz")))

(define-public crate-minhtml-0.14.0 (c (n "minhtml") (v "0.14.0") (d (list (d (n "minify-html") (r "^0.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02yv0l7jj9w7l6fsjn0s0ma5f9mabklaysnj1j7n0md82j6w53m7")))

(define-public crate-minhtml-0.15.0 (c (n "minhtml") (v "0.15.0") (d (list (d (n "minify-html") (r "^0.15.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17iisyd4gggfqav21x7zalhyjc0dnwgiikaycm4ywzd6yy6imkh6")))

