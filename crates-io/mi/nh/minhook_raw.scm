(define-module (crates-io mi nh minhook_raw) #:use-module (crates-io))

(define-public crate-minhook_raw-0.1.0 (c (n "minhook_raw") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0jifp37xd56n57s5fzj9hiw90wr1rcpkb6i0wyz9vrchl31knhwx") (y #t)))

(define-public crate-minhook_raw-0.1.1 (c (n "minhook_raw") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0w3d81xahnlfnyv12zzn894swa4n6gisy1wn534wvdih0qbrbfs6") (y #t)))

(define-public crate-minhook_raw-0.1.2 (c (n "minhook_raw") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "007s63cqyp7z611csq2mvjpc3wxnf4yyf89qgfss8zs9ln3bj5f1") (y #t)))

(define-public crate-minhook_raw-0.1.3 (c (n "minhook_raw") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1s17cz8qsr4fbqplc4hh3mvji1fnjvdwp6b94vg11ymsn26spjg0") (y #t)))

(define-public crate-minhook_raw-0.1.5 (c (n "minhook_raw") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0bmxsh1gz2h3mp79vfkl8bcdx61z87aindxgzfgm6hld1y3ajg47")))

(define-public crate-minhook_raw-0.2.0 (c (n "minhook_raw") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0smsip27i0b3b0y9ma0yc0rvfln4fziq2ijijxqa4h5vcw6rm587") (y #t)))

(define-public crate-minhook_raw-0.2.1 (c (n "minhook_raw") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1ximniw7skx7mvv632zcy7qfv8ni6bnsm6yccli1mzpjrp3d14wb")))

(define-public crate-minhook_raw-0.3.0 (c (n "minhook_raw") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.88") (d #t) (k 1)))) (h "1xyvlihjzajyclcbyiaf74pz60a70sbxjxhrg7pmwssx430x8p03")))

