(define-module (crates-io mi nh minhook) #:use-module (crates-io))

(define-public crate-minhook-0.0.0 (c (n "minhook") (v "0.0.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0akvridxysr2zx83jv0zpjqchy5clnsfzwls6fx1vpfq9w0yk3w4")))

(define-public crate-minhook-0.1.0 (c (n "minhook") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1mnd2wnic5xhh6jq8ws90afzwyz3bm6rm7d7xlvjb0bddwy7n5rv")))

(define-public crate-minhook-0.2.0 (c (n "minhook") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "011ca2kj35nb7r7rbq73qw10i429hfk2g3x2kcywjq8kamvblmcs")))

(define-public crate-minhook-0.3.0 (c (n "minhook") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1il7rk0hb19zxs5fd6k7rihzb9182ng4l00gkayy9a15z6mj4im3")))

(define-public crate-minhook-0.4.0 (c (n "minhook") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "1j8ywfc9zdgiwf9n9rxszsx9dmxw8nlclkcz98ra4v6n3xgi1qwz")))

(define-public crate-minhook-0.4.1 (c (n "minhook") (v "0.4.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0gdrhfb6fjpbrjnd8km0h47nmmjqr1l3rwln1ag75pdl1nyfha77")))

(define-public crate-minhook-0.4.2 (c (n "minhook") (v "0.4.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "1cs93ivf4z3dm6s2cpjc6c4lrn36km0d0miw20rgvxngv5yqdnmq")))

(define-public crate-minhook-0.4.3 (c (n "minhook") (v "0.4.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "128whqb5a0q35bh9xis0rbhjzmf0bzz5chw5hf5hlidaigrzm1jv")))

(define-public crate-minhook-0.5.0 (c (n "minhook") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0pkb508g280z453m5r4l6f5anq8rrijnabsnmlmn595afrwfi2m4")))

