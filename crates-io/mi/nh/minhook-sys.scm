(define-module (crates-io mi nh minhook-sys) #:use-module (crates-io))

(define-public crate-minhook-sys-0.1.0 (c (n "minhook-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "1fd0b1vsrqxv77dmrf8an4jybpnfxqnpian0gw18yd62x17sj5w9")))

(define-public crate-minhook-sys-0.1.1 (c (n "minhook-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "067s024nmx41wxy2ih3l1849l8dav50w4vnqrg02xb2hkxv3spnx")))

