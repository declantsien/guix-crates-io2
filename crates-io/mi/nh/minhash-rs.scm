(define-module (crates-io mi nh minhash-rs) #:use-module (crates-io))

(define-public crate-minhash-rs-0.1.0 (c (n "minhash-rs") (v "0.1.0") (d (list (d (n "hyperloglog-rs") (r "^0.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "08y64crp3xylxklgq0pr0idjnyx8z4dyp8iy6982scyfc108i8zn")))

(define-public crate-minhash-rs-0.1.1 (c (n "minhash-rs") (v "0.1.1") (d (list (d (n "hyperloglog-rs") (r "^0.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0yj2m22awnncqhfh45g6g9paf6zi11w3l71j92zy4qjx0k57drrn")))

(define-public crate-minhash-rs-0.2.0 (c (n "minhash-rs") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "hyperloglog-rs") (r "^0.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "01bzrcxsvp8h2brjzpqbwchccfzdsz8inz06hsfgqx88in0z7538")))

