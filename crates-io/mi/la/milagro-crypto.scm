(define-module (crates-io mi la milagro-crypto) #:use-module (crates-io))

(define-public crate-milagro-crypto-0.1.0 (c (n "milagro-crypto") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "13262cm9jybb4b50licpbb8g1xhb5jbaar67p7iz3p89sga5pqg6")))

(define-public crate-milagro-crypto-0.1.1 (c (n "milagro-crypto") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0v1anl262691jvjzg0ivb49yzb19nrjz2y9dy1p7n099z288ayj5")))

(define-public crate-milagro-crypto-0.1.2 (c (n "milagro-crypto") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1g0x374hnf4wv4vjv42dxpwizw9zcrmsq66xav5m1349vb4rpx9x")))

(define-public crate-milagro-crypto-0.1.3 (c (n "milagro-crypto") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "097ydbi9k7nfh443agh1qrwgj6bzar5wqnlvbg0xxy3lzsi1wpxb")))

(define-public crate-milagro-crypto-0.1.4 (c (n "milagro-crypto") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "01zki8rs81x1y27xcq1k04k14gvxish0dmf5wsq7k2vbclavvz8y")))

(define-public crate-milagro-crypto-0.1.5 (c (n "milagro-crypto") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "09dab0chmid88vk5jvii831l8mx78m55fzc4hx7rawgbjy4m7s1j")))

(define-public crate-milagro-crypto-0.1.6 (c (n "milagro-crypto") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0v58yhnaqcv6apk3wxnhdwjczdqir0i2zp0jaylrq34b967ppvz3")))

(define-public crate-milagro-crypto-0.1.7 (c (n "milagro-crypto") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1s6znn413alz44s1lfd1d1s54b69mlxiiyi83hsl02s3ih4h2j7j")))

(define-public crate-milagro-crypto-0.1.8 (c (n "milagro-crypto") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "151n6avmjj4mqxcvzkhsbjpfw0pkwqgskw93mxgikfihzkjyw9wa")))

(define-public crate-milagro-crypto-0.1.9 (c (n "milagro-crypto") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1cqa3742dyrbazjydmqn34pskddfda4wljg310wr332hgxfqpnyk")))

(define-public crate-milagro-crypto-0.1.10 (c (n "milagro-crypto") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1sk1a5sq1j58qfrw9kday4w8ssfqirfkfa7h0amkm9zd0lwcw136")))

(define-public crate-milagro-crypto-0.1.11 (c (n "milagro-crypto") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "11xjiqny3gv835bf12r7j2a83lfclmyvikggqm1l3hm8ra6ira6d")))

(define-public crate-milagro-crypto-0.1.12 (c (n "milagro-crypto") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1px7drpnqwpdv3d2n3jyhjqx7gwf233l6gq2768fspklisia9y76")))

(define-public crate-milagro-crypto-0.1.13 (c (n "milagro-crypto") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0m9mfmr4hyss46cc1csz29fb4gbcidwnkqsxn47v6h9bkgdqazw1")))

(define-public crate-milagro-crypto-0.1.14 (c (n "milagro-crypto") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0phxannfmcj69fdfaiams9m7qdywavxy5fmcn24djwfz4yyifzam")))

