(define-module (crates-io mi io miio-proto) #:use-module (crates-io))

(define-public crate-miio-proto-0.1.0 (c (n "miio-proto") (v "0.1.0") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c3aac6jxh2qa350cqg2rn294i7prnj340akx252ya6aswwbp435") (y #t)))

(define-public crate-miio-proto-0.1.1 (c (n "miio-proto") (v "0.1.1") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17nxpprahrapdqkm8yxz7mdgigf5fyh84knxsm0rnh9yqcdvzcfy") (y #t)))

(define-public crate-miio-proto-0.1.2 (c (n "miio-proto") (v "0.1.2") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03nymbvfdl6xmhywrvcb8rpjrkzajc64lk2fqp3ipa9a3jqpxmcf") (y #t)))

(define-public crate-miio-proto-0.1.3 (c (n "miio-proto") (v "0.1.3") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10y843wmv82mw1kgs2xd2wr6acqyck0dsk2zg5d8lcqfm5jyz0m5")))

(define-public crate-miio-proto-0.1.4 (c (n "miio-proto") (v "0.1.4") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f3bymbg2vbdl6lly79hiz0jyxgh6bs3gk86zz1wpnq5nznnxlmr")))

(define-public crate-miio-proto-0.1.5 (c (n "miio-proto") (v "0.1.5") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "block-modes") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "md5") (r "^0") (d #t) (k 0)) (d (n "packed_struct") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1si09b2dq56f29507d3vkrba87chqq6rwqvrf6si382qyx5yrw8i")))

