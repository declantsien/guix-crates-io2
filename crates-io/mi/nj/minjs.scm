(define-module (crates-io mi nj minjs) #:use-module (crates-io))

(define-public crate-minjs-0.1.1 (c (n "minjs") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "swc") (r "^0.275.1") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.26") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.113.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "0pgx2hz13l861agnrrg82085myblrlphbkh1jm5riv143md5klxd")))

