(define-module (crates-io mi nm minmaxn) #:use-module (crates-io))

(define-public crate-minmaxn-0.1.0 (c (n "minmaxn") (v "0.1.0") (h "1kwswvx91ckdl6kxahhd64r0na43aa4f9mxhjh43c6bq02y99xzm")))

(define-public crate-minmaxn-0.1.1 (c (n "minmaxn") (v "0.1.1") (h "0cpxv7mcjby3wii5hpyvi9wfmknhxxdw2jyyfxlmmws8rxkq77lh")))

(define-public crate-minmaxn-0.1.2 (c (n "minmaxn") (v "0.1.2") (h "1j0sqdnia55bbhqfm9qkkwm591nqhdx2y9s9n5hqcbjcxc8x33xj")))

(define-public crate-minmaxn-0.2.0 (c (n "minmaxn") (v "0.2.0") (h "00c21vnh0l14k207sk7gw7bs8cph7w8zz692x2zd9al9hr0d434f")))

