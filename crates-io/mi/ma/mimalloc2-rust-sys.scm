(define-module (crates-io mi ma mimalloc2-rust-sys) #:use-module (crates-io))

(define-public crate-mimalloc2-rust-sys-2.1.2-source (c (n "mimalloc2-rust-sys") (v "2.1.2-source") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1k5dxc0pranfy83dhz2zf72dmh5fh3ykmp3mjpa90xcv2hdi6wg8") (f (quote (("skip-collect-on-exit") ("secure") ("local-dynamic-tls") ("asm"))))))

(define-public crate-mimalloc2-rust-sys-2.1.4-source (c (n "mimalloc2-rust-sys") (v "2.1.4-source") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "045f7vry0sj8b6dpxxhhlxahacynm10fc8csw531bk593h69slp8") (f (quote (("skip-collect-on-exit") ("secure") ("local-dynamic-tls") ("asm"))))))

