(define-module (crates-io mi ma mimalloc2-rust) #:use-module (crates-io))

(define-public crate-mimalloc2-rust-0.3.0 (c (n "mimalloc2-rust") (v "0.3.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc2-rust-sys") (r "^2.1.2-source") (d #t) (k 0)))) (h "1qzvl3cip79s8xhbc9ad7ljassh9lhdyy9x6q7j8cccqyi6cd0wc") (f (quote (("unstable") ("skip-collect-on-exit" "mimalloc2-rust-sys/skip-collect-on-exit") ("secure" "mimalloc2-rust-sys/secure") ("local-dynamic-tls" "mimalloc2-rust-sys/local-dynamic-tls") ("asm" "mimalloc2-rust-sys/asm"))))))

(define-public crate-mimalloc2-rust-0.3.1 (c (n "mimalloc2-rust") (v "0.3.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc2-rust-sys") (r "^2.1.4-source") (d #t) (k 0)))) (h "0x4f0d6gw1gl8ml7vzvxp54i1r991cn4z3jwih5zis8ixjnicixf") (f (quote (("unstable") ("skip-collect-on-exit" "mimalloc2-rust-sys/skip-collect-on-exit") ("secure" "mimalloc2-rust-sys/secure") ("local-dynamic-tls" "mimalloc2-rust-sys/local-dynamic-tls") ("asm" "mimalloc2-rust-sys/asm"))))))

