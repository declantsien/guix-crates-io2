(define-module (crates-io mi ma mimallocator) #:use-module (crates-io))

(define-public crate-mimallocator-0.1.0 (c (n "mimallocator") (v "0.1.0") (d (list (d (n "mimalloc-sys") (r "^0.1") (d #t) (k 0)))) (h "02p0fakpkq8v0av6p564z89imbblv0kphnj13xcv7i0rnhw3ikq4")))

(define-public crate-mimallocator-0.1.2 (c (n "mimallocator") (v "0.1.2") (d (list (d (n "mimalloc-sys") (r "^0.1") (d #t) (k 0)))) (h "1w6na92m52kk06ggzlv520gprbmcw2vph4wdz83msip743qvi3xv") (f (quote (("stats" "mimalloc-sys/stats") ("secure" "mimalloc-sys/secure") ("default") ("check_full" "mimalloc-sys/check_full"))))))

(define-public crate-mimallocator-0.1.3 (c (n "mimallocator") (v "0.1.3") (d (list (d (n "mimalloc-sys") (r "^0.1") (d #t) (k 0)))) (h "0yjp1jr2fkq0rpmgz2wfpb8k4cmvj0mmr5yr777qylvbpx7gwi1d") (f (quote (("stats" "mimalloc-sys/stats") ("secure" "mimalloc-sys/secure") ("default") ("check_full" "mimalloc-sys/check_full"))))))

