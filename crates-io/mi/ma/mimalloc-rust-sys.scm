(define-module (crates-io mi ma mimalloc-rust-sys) #:use-module (crates-io))

(define-public crate-mimalloc-rust-sys-1.7.2 (c (n "mimalloc-rust-sys") (v "1.7.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "144m1va3dmbr6c5nj1aqnnfrp0fc4yyqn10n25w7ksvsrhnz7xwn")))

(define-public crate-mimalloc-rust-sys-1.7.3 (c (n "mimalloc-rust-sys") (v "1.7.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0nwapamivwzbxj2iyn8019sjvq6v49q8bbwbnjq3kjrxb7h3kbx8") (y #t)))

(define-public crate-mimalloc-rust-sys-1.7.3-source (c (n "mimalloc-rust-sys") (v "1.7.3-source") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0rh6lx32yb4vnaavwzif3gzcl1c326x0sxv01172z61b4qqqgp1s")))

(define-public crate-mimalloc-rust-sys-1.7.6-source (c (n "mimalloc-rust-sys") (v "1.7.6-source") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0r7sbnx5cv2cvgdsi4pka2yw9wl2nfs577x10ai7kf9nagsdll3s") (f (quote (("skip-collect-on-exit") ("secure") ("local-dynamic-tls") ("asm"))))))

(define-public crate-mimalloc-rust-sys-1.7.9-source (c (n "mimalloc-rust-sys") (v "1.7.9-source") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1nymbp7av46h09aig9n153sjqmwlcv53w4v82llrz05984rf24v4") (f (quote (("skip-collect-on-exit") ("secure") ("local-dynamic-tls") ("asm"))))))

