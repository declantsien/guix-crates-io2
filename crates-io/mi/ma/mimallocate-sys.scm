(define-module (crates-io mi ma mimallocate-sys) #:use-module (crates-io))

(define-public crate-mimallocate-sys-0.1.0 (c (n "mimallocate-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10zblqdxw4r03mz7n9m56vhgxl9mmfpmj7y13mx7hnmqsbhzrxs0")))

(define-public crate-mimallocate-sys-0.1.1 (c (n "mimallocate-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "144xx4aqkmlh27jyl7cbxflbjhb2ihccsvhq9f3kpmdjcfkhm9p3")))

