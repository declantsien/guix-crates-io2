(define-module (crates-io mi ma mimalloc-rust) #:use-module (crates-io))

(define-public crate-mimalloc-rust-0.1.1 (c (n "mimalloc-rust") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc-rust-sys") (r "^1.7.2") (d #t) (k 0)))) (h "06l2jya6va9j146lyd7rqsz3s0pasvx010dmi6dz8nav7k4xbz1g")))

(define-public crate-mimalloc-rust-0.1.4 (c (n "mimalloc-rust") (v "0.1.4") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc-rust-sys") (r "^1.7.3") (d #t) (k 0)))) (h "0dm05wqvw2hsy30mqa7a0q605lwv0pax5xhg8ak37lywjzkwlllw") (f (quote (("unstable"))))))

(define-public crate-mimalloc-rust-0.1.5 (c (n "mimalloc-rust") (v "0.1.5") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc-rust-sys") (r "^1.7.3-source") (d #t) (k 0)))) (h "0wj4hkspsmlgm6lh5bf2blzalgjcc356bpkh4k5vcnyvvzwhvhxw") (f (quote (("unstable"))))))

(define-public crate-mimalloc-rust-0.2.0 (c (n "mimalloc-rust") (v "0.2.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc-rust-sys") (r "^1.7.6-source") (d #t) (k 0)))) (h "12wdhqs80pf602mnspdx3n5cqf88wyvifs3bl41lql661dp8cwv9") (f (quote (("unstable") ("skip-collect-on-exit" "mimalloc-rust-sys/skip-collect-on-exit") ("secure" "mimalloc-rust-sys/secure") ("local-dynamic-tls" "mimalloc-rust-sys/local-dynamic-tls") ("asm" "mimalloc-rust-sys/asm"))))))

(define-public crate-mimalloc-rust-0.2.1 (c (n "mimalloc-rust") (v "0.2.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mimalloc-rust-sys") (r "^1.7.9-source") (d #t) (k 0)))) (h "1glvzha0k0rg275ri7lxgf0cydmywi880vf4n8841ywf5742ddsy") (f (quote (("unstable") ("skip-collect-on-exit" "mimalloc-rust-sys/skip-collect-on-exit") ("secure" "mimalloc-rust-sys/secure") ("local-dynamic-tls" "mimalloc-rust-sys/local-dynamic-tls") ("asm" "mimalloc-rust-sys/asm"))))))

