(define-module (crates-io mi ma mimalloc2) #:use-module (crates-io))

(define-public crate-mimalloc2-0.1.0 (c (n "mimalloc2") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1") (k 0)))) (h "1hsxjrqpljh02nhk2mkkjpnmj43d9kmbznxyh59k2ybwb2v9srks") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("extended" "libmimalloc-sys/extended") ("default" "extended"))))))

