(define-module (crates-io mi ma mimalloc-sys) #:use-module (crates-io))

(define-public crate-mimalloc-sys-0.1.0 (c (n "mimalloc-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rza9h3clikgdz59yfpn9mdlcg2b0zwnjccxdz1g64ylpjrnxnn5") (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.1 (c (n "mimalloc-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pd59755rbp1njq52cp5fyczhfanbc5vrv32m2vpzgd0czgbk75f") (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.2 (c (n "mimalloc-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jmh4g5alsjwp3fi3j51mdk3nw07xy99h2a2jc6px0l9b5b55g86") (f (quote (("stats") ("secure") ("default") ("check_full")))) (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.4 (c (n "mimalloc-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mhmxy50g5zlhkyvbh1719ybpj6086ajpmincqfryms6caxfyjjg") (f (quote (("verbose") ("stats") ("secure") ("override") ("default") ("check_full")))) (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.5 (c (n "mimalloc-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0laki2dw8vpknmyqpgdi8a6ps955kjdvbq7m6wsxnqyy0dnqwpxr") (f (quote (("verbose") ("stats") ("secure") ("override") ("default") ("check_full")))) (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.6 (c (n "mimalloc-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14fjm96imrkykb5b0in2gd6isawxr1w5677pnb8f6skgcbxwx8sa") (f (quote (("verbose") ("stats") ("secure") ("override") ("default") ("check_full")))) (l "mimalloc")))

(define-public crate-mimalloc-sys-0.1.6-dev (c (n "mimalloc-sys") (v "0.1.6-dev") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h2ykf6dlg0f4c8wdvw1nnqidv6bzcx2g7j9h1fligzaay1b6hw9") (f (quote (("verbose") ("stats") ("secure") ("override") ("default") ("check_full")))) (l "mimalloc")))

