(define-module (crates-io mi ma mimalloc) #:use-module (crates-io))

(define-public crate-mimalloc-0.1.0 (c (n "mimalloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0p4lapa8jma39da9j4kgng6n7802d26rrjdb1yf4sfaz74wy57nk") (f (quote (("no_secure" "libmimalloc-sys/no_secure")))) (y #t)))

(define-public crate-mimalloc-0.1.1 (c (n "mimalloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ansp649869cssslryym91hrdahjfdfyw4768xk99hzlykq0685g") (f (quote (("no_secure" "libmimalloc-sys/no_secure")))) (y #t)))

(define-public crate-mimalloc-0.1.2 (c (n "mimalloc") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1x9zrafklpnazrhhd8b1d9a7aphyh96x839qjha70v9dmd6425g6") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.3 (c (n "mimalloc") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1431rklf38s3ghg64n13f4x7fr5lam0ma65cd7w029718iglv4a2") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.4 (c (n "mimalloc") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0vlq7mkcl9ach1ja49y2zdc86s85j54zs6j53gzjv1hrbfxaqkjn") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.5 (c (n "mimalloc") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.4") (d #t) (k 0)))) (h "11xnw0spj1wznkfrny7r6zsq7fy76dq0blhcmdpplpmjxv0a5lq8") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.6 (c (n "mimalloc") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.4") (d #t) (k 0)))) (h "08d0q340x4xysghkp72jai77q6b6n8havplz0v12l78nzd1q23xx") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.7 (c (n "mimalloc") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1dx2abx6k8j1xzs26hnrlzsyidvacb13agrlj7js8jccbw4z8kaw") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.8 (c (n "mimalloc") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0wsk6yncdyc85dcxmkybcwnv2l6kr5wvz0alyyfk1fn9v1gm3zw0") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure")))) (y #t)))

(define-public crate-mimalloc-0.1.9 (c (n "mimalloc") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.6") (d #t) (k 0)))) (h "1l0fmn7gal2106054s9682ffyfgksd59c9xnflvrjyqmp8g0ajq0") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.10 (c (n "mimalloc") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.7") (d #t) (k 0)))) (h "0ayjh26v6pgvr4nmcbp0kqqnnkq7h2hnrp4nl6js589lgijiq5hp") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.11 (c (n "mimalloc") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.8") (d #t) (k 0)))) (h "1jw98wk97py9afmgarwpz6rda98djygvxh1h5kcvqn8p4faynmw0") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.12 (c (n "mimalloc") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.9") (d #t) (k 0)))) (h "1c9r9b0j14pyax7xm8a0bgspj9ypldw9a5bjx11prx3qjxpxxifi") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.13 (c (n "mimalloc") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0n19y2gp87vcy2s7m4zfwy1ffhin2xd923g0d0dpcqj63ssgramf") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.14 (c (n "mimalloc") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmimalloc-sys") (r "^0.1.11") (d #t) (k 0)))) (h "1cjcgn9yxpwa2gaf5gkhxb9wl54kf8yd6grjfzm9a61lm86qsm7b") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.15 (c (n "mimalloc") (v "0.1.15") (d (list (d (n "libmimalloc-sys") (r "^0.1.11") (d #t) (k 0)))) (h "1h7vk0zbjvxrkarz2fdiz2brjxpg4jw41kbv8zmjga39qwlnj0av") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.16 (c (n "mimalloc") (v "0.1.16") (d (list (d (n "libmimalloc-sys") (r "^0.1.12") (d #t) (k 0)))) (h "0w8jbngp7xzbmb1s1wcfzvlhgd54zclx9pavxazij59nakxn6zgr") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.17 (c (n "mimalloc") (v "0.1.17") (d (list (d (n "libmimalloc-sys") (r "^0.1.13") (d #t) (k 0)))) (h "1mvz3yvrs9y3n38m6liy49hnq7s6iy0wfxn0w44ihg2j6jkaagrn") (f (quote (("secure-full" "libmimalloc-sys/secure" "libmimalloc-sys/secure-full") ("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.18 (c (n "mimalloc") (v "0.1.18") (d (list (d (n "libmimalloc-sys") (r "^0.1.14") (k 0)))) (h "1ppjff8i3wnylsa73mym8jirnm0r6byi0sddz2mr4vmq9vp6xccl") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.19 (c (n "mimalloc") (v "0.1.19") (d (list (d (n "libmimalloc-sys") (r "^0.1.15") (k 0)))) (h "183wpc2hmclyzz8a1dzfiy4qffnywzdn8ra34syh37wrd4hdwlkc") (f (quote (("secure" "libmimalloc-sys/secure") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.20 (c (n "mimalloc") (v "0.1.20") (d (list (d (n "libmimalloc-sys") (r "^0.1.16") (k 0)))) (h "0x74b6jv6pxfl6bh44bnch6ajm3l5z3zq8w8mqlscbq8d77rnx80") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.21 (c (n "mimalloc") (v "0.1.21") (d (list (d (n "libmimalloc-sys") (r "^0.1.17") (k 0)))) (h "00jv4q5y2pifmy77nb0nm9hi8nlx9fdgyv5mq2zagi46278b661f") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.22 (c (n "mimalloc") (v "0.1.22") (d (list (d (n "libmimalloc-sys") (r "^0.1.18") (k 0)))) (h "1yl0gglxxvsngvyaqvrd434c92gav39sdb7li1nxr74gn6f2qpaa") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.23 (c (n "mimalloc") (v "0.1.23") (d (list (d (n "libmimalloc-sys") (r "^0.1.19") (k 0)))) (h "0n5ik9gyrf73j410lj3fdfc1h5amhfrkj3za1b9f2z4mf1rchmvq") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.24 (c (n "mimalloc") (v "0.1.24") (d (list (d (n "libmimalloc-sys") (r "^0.1.20") (k 0)))) (h "0gd9d0bcxwylgmfnqsrpsr71d1zyl8gf04nr964hilmki30zwzkm") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.25 (c (n "mimalloc") (v "0.1.25") (d (list (d (n "libmimalloc-sys") (r "^0.1.21") (k 0)))) (h "1yrx0g16nhkkhygdkvnpwf7kjqy73y5x3dlnmj4ydrfimw8nnz0y") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.26 (c (n "mimalloc") (v "0.1.26") (d (list (d (n "libmimalloc-sys") (r "^0.1.22") (k 0)))) (h "0c2i5q90jnknkhwmxil3a7kwdjr2b7y7c57xas8w9rh8wmy8jx7v") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure"))))))

(define-public crate-mimalloc-0.1.27 (c (n "mimalloc") (v "0.1.27") (d (list (d (n "libmimalloc-sys") (r "^0.1.23") (k 0)))) (h "0fi8fzw78ns3c7cyrzki1yn93f3sjqzm8bwbg9kvabw9v70phpyg") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.28 (c (n "mimalloc") (v "0.1.28") (d (list (d (n "libmimalloc-sys") (r "^0.1.24") (k 0)))) (h "09ir1qlg5rwb74l8zh5nyxgwmnpwf7r5zxj26bdnnbqc74qs3pxh") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.29 (c (n "mimalloc") (v "0.1.29") (d (list (d (n "libmimalloc-sys") (r "^0.1.25") (k 0)))) (h "163n88d7mmvxyxv0gy2axhy3kvfhn3g68xch5rrjxbv9r61ssr1g") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.30 (c (n "mimalloc") (v "0.1.30") (d (list (d (n "libmimalloc-sys") (r "^0.1.26") (k 0)))) (h "0dfvbjkldka04q48cn4pkwphg1bsffh1r2797kmzkgyk815nmkkn") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.31 (c (n "mimalloc") (v "0.1.31") (d (list (d (n "libmimalloc-sys") (r "^0.1.27") (k 0)))) (h "0w0ng00c36ipj7pbq49w58kwfjk38zxkf4d3pzbkj0idr6d6lbdk") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.32 (c (n "mimalloc") (v "0.1.32") (d (list (d (n "libmimalloc-sys") (r "^0.1.28") (k 0)))) (h "1jhyhyfsb4hidxsbkkkf96ws9lzivcfim0g1hfssfncrk7i788wv") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.33 (c (n "mimalloc") (v "0.1.33") (d (list (d (n "libmimalloc-sys") (r "^0.1.29") (k 0)))) (h "009xbn4vcrd2izfpgs1fgh483kw128l1vzsa84p1i9nanmz79z0w") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug")))) (y #t)))

(define-public crate-mimalloc-0.1.34 (c (n "mimalloc") (v "0.1.34") (d (list (d (n "libmimalloc-sys") (r "^0.1.30") (k 0)))) (h "189viympjb8fz05cawng4dssq63jwlngrjf6l1hpapv3315igjwx") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.35 (c (n "mimalloc") (v "0.1.35") (d (list (d (n "libmimalloc-sys") (r "^0.1.31") (k 0)))) (h "1amps5pj3hzkny5ja5fcf7cnvnnwkm346x6hav3zbxqjqx1n0rlj") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.36 (c (n "mimalloc") (v "0.1.36") (d (list (d (n "libmimalloc-sys") (r "^0.1.32") (k 0)))) (h "1ni3vw8755jgm1q6wr4bmn67w6nbylk1fynb7xx2dv45z79xm21x") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.37 (c (n "mimalloc") (v "0.1.37") (d (list (d (n "libmimalloc-sys") (r "^0.1.33") (k 0)))) (h "164xcf6hd752vqg7y1xfs00fvy4847c8nq2mnzzz6n9lgac98a2f") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default" "secure") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.38 (c (n "mimalloc") (v "0.1.38") (d (list (d (n "libmimalloc-sys") (r "^0.1.34") (k 0)))) (h "09s1i5cfbh5gxadvlydqqy06lmwjyp5z9c30axk64vviyqimyblp") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.39 (c (n "mimalloc") (v "0.1.39") (d (list (d (n "libmimalloc-sys") (r "^0.1.35") (k 0)))) (h "176w9gf5qxs07kd2q39f0k25rzmp4kyx5r13wc8sk052bqmr40gs") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.40 (c (n "mimalloc") (v "0.1.40") (d (list (d (n "libmimalloc-sys") (r "^0.1.36") (k 0)))) (h "091sa3444d4xdjbc35yxhzk76mgllngjhwad1x8f0fc7xjrg39m4") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug")))) (y #t)))

(define-public crate-mimalloc-0.1.41 (c (n "mimalloc") (v "0.1.41") (d (list (d (n "libmimalloc-sys") (r "^0.1.37") (k 0)))) (h "13chqrci36mfj3qzzp2ald8hdwg8hsxvm67qiinaa3gd1lla4hcz") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("default") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

(define-public crate-mimalloc-0.1.42 (c (n "mimalloc") (v "0.1.42") (d (list (d (n "libmimalloc-sys") (r "^0.1.38") (k 0)))) (h "0xm1f6f84h0yxhdg9qz7dqfxps152ash8mpngakz8llvny36s679") (f (quote (("secure" "libmimalloc-sys/secure") ("override" "libmimalloc-sys/override") ("no_thp" "libmimalloc-sys/no_thp") ("local_dynamic_tls" "libmimalloc-sys/local_dynamic_tls") ("extended" "libmimalloc-sys/extended") ("default") ("debug_in_debug" "libmimalloc-sys/debug_in_debug") ("debug" "libmimalloc-sys/debug"))))))

