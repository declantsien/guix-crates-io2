(define-module (crates-io mi ng ming_test_crate) #:use-module (crates-io))

(define-public crate-ming_test_crate-0.1.0 (c (n "ming_test_crate") (v "0.1.0") (h "1g49v1gxp7xz1fs0pbkg2166m1981nkni8s2rdhnq1fb07n9fzmq")))

(define-public crate-ming_test_crate-0.1.1 (c (n "ming_test_crate") (v "0.1.1") (h "0j91hyi0qvxp8wl5f39crwwwfgmwv4shpxpk6wzsyb19mry0i2xv")))

(define-public crate-ming_test_crate-0.0.1 (c (n "ming_test_crate") (v "0.0.1") (h "1a4jmg8224dyvrjp8v2lpszqgjnaqsxqychhlg4p8b6b8i4ri4qz")))

(define-public crate-ming_test_crate-0.1.2 (c (n "ming_test_crate") (v "0.1.2") (h "093m4qs2yfiqvxl9nb4rnb8s008r6jq62wgnsxbpwhrdndavlaln")))

