(define-module (crates-io mi ng ming-tools) #:use-module (crates-io))

(define-public crate-ming-tools-0.1.0 (c (n "ming-tools") (v "0.1.0") (h "1m5f3xw3fxwhyzqqv5s3rhmpfzhz025bkl9gmxbssqwcad8lc374") (y #t)))

(define-public crate-ming-tools-0.1.1 (c (n "ming-tools") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yv8wb5ma6235bj5sqp3wph1ivnjfn0ai4gdr76xzdy0n016qhcm")))

