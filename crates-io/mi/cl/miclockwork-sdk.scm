(define-module (crates-io mi cl miclockwork-sdk) #:use-module (crates-io))

(define-public crate-miclockwork-sdk-0.1.0 (c (n "miclockwork-sdk") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "miclockwork-thread-program") (r "=0.1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1jfj0nvbkhx7jvw9hr655rw7njidmiv7s5m20rpv2crnbig4z5vf") (f (quote (("default"))))))

