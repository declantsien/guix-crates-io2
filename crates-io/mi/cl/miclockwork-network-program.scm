(define-module (crates-io mi cl miclockwork-network-program) #:use-module (crates-io))

(define-public crate-miclockwork-network-program-0.1.0 (c (n "miclockwork-network-program") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (f (quote ("mint" "token"))) (d #t) (k 0)) (d (n "miclockwork-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "toml_datetime") (r "=0.6.1") (d #t) (k 0)) (d (n "winnow") (r "=0.4.1") (d #t) (k 0)))) (h "1mcynw6dv96bpp802jb7rjcpqnv247g4gp8884s9w9ivwfd34l3m") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

