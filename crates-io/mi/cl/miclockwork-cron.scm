(define-module (crates-io mi cl miclockwork-cron) #:use-module (crates-io))

(define-public crate-miclockwork-cron-0.1.0 (c (n "miclockwork-cron") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0jgz5zpjfvrz3smr3zd359q2s1dq7rvycwv2qb7md6wdxk61fiis")))

