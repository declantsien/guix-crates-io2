(define-module (crates-io mi cl miclockwork-thread-program-v1) #:use-module (crates-io))

(define-public crate-miclockwork-thread-program-v1-0.1.0 (c (n "miclockwork-thread-program-v1") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "clockwork-anchor-gen") (r "^0.3.2") (f (quote ("compat-program-result"))) (d #t) (k 0)))) (h "06jdp3nscmbncg0l6idpx0ak4gx4p5d7b9b2y8q07ycmma1lhhmr") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

