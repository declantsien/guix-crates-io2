(define-module (crates-io mi cl miclockwork-utils) #:use-module (crates-io))

(define-public crate-miclockwork-utils-0.1.0 (c (n "miclockwork-utils") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1vsxb6lrzd1dq670wzhgii0xffn2n94gy7pbrzy1krl342hbw6xc")))

