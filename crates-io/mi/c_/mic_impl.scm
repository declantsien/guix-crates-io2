(define-module (crates-io mi c_ mic_impl) #:use-module (crates-io))

(define-public crate-mic_impl-0.1.0 (c (n "mic_impl") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0s6imv7qwcbz32xar65rfg7ahl1gd9sisf6sn5ajfmymzywnqa3z")))

