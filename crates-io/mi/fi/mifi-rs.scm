(define-module (crates-io mi fi mifi-rs) #:use-module (crates-io))

(define-public crate-mifi-rs-0.3.0 (c (n "mifi-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0503hqg46ha5w67bd75inzsigq28iqhn58r4bm3zq31rhq8l282i")))

