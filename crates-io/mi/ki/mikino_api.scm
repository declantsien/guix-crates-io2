(define-module (crates-io mi ki mikino_api) #:use-module (crates-io))

(define-public crate-mikino_api-0.1.0 (c (n "mikino_api") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rsmt2") (r "^0.12") (d #t) (k 0)))) (h "1br5fbj47f7wqrfqfwpbbrgg6m0nhw94l00sn0b6ihj38aw70skx") (f (quote (("force-color"))))))

(define-public crate-mikino_api-0.2.0 (c (n "mikino_api") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rsmt2") (r "^0.12") (d #t) (k 0)))) (h "0whjg78s7w585v83h43qwfq5y6wb8gm63v2yddjkd87a09yf55r9") (f (quote (("force-color"))))))

(define-public crate-mikino_api-0.3.0 (c (n "mikino_api") (v "0.3.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rsmt2") (r "^0.12") (d #t) (k 0)))) (h "08w5arjyxvcddbvmlx9yw9gm2n88779g5ndbq5mlz8w6ml92mfzm") (f (quote (("force-color"))))))

(define-public crate-mikino_api-0.4.0 (c (n "mikino_api") (v "0.4.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rsmt2") (r "^0.14") (d #t) (k 0)))) (h "0qnyj4fp4i7pdnmz71gm0k8x2akkdfz0d2vhlmcgw7qgasfzfc07") (f (quote (("parser") ("demo"))))))

(define-public crate-mikino_api-0.5.0 (c (n "mikino_api") (v "0.5.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rsmt2") (r "^0.14") (d #t) (k 0)))) (h "0si4dw5cahw1f8252900cx15l5zqms2mrgyq49yhz467iy98zjmw") (f (quote (("parser"))))))

(define-public crate-mikino_api-0.5.1 (c (n "mikino_api") (v "0.5.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rsmt2") (r "^0.14") (d #t) (k 0)))) (h "1hp9p34kyzzsb3dgfp7vr264ldwk5ly7v9p7mg5jr1y0j8bhifsb") (f (quote (("parser"))))))

(define-public crate-mikino_api-0.9.0 (c (n "mikino_api") (v "0.9.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rsmt2") (r "^0.16") (d #t) (k 0)))) (h "1qr4gj25gh2bq99vb018rbm2383skg4zxc2ycpy3mvlxyk0jjcif")))

(define-public crate-mikino_api-0.9.1 (c (n "mikino_api") (v "0.9.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rsmt2") (r "^0.16") (d #t) (k 0)))) (h "1h3d744r3pdqw668fg5xai5v8484ypbnrsyicz2as6anfxrss1qk")))

