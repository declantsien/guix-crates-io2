(define-module (crates-io mi ki mikino) #:use-module (crates-io))

(define-public crate-mikino-0.1.0 (c (n "mikino") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mikino_api") (r "^0.1") (d #t) (k 0)))) (h "0w2y8q4plfkvz75b5c2kyjrny4sz3w73056v5mbkaxvnf3yjblad") (f (quote (("force-color"))))))

(define-public crate-mikino-0.2.0 (c (n "mikino") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mikino_api") (r "^0.2") (d #t) (k 0)))) (h "0dr0yngzccyzqbghv3r3wcbpqxbikhnlngwi0kvcm4sqsvprr7ca") (f (quote (("force-color"))))))

(define-public crate-mikino-0.3.0 (c (n "mikino") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "mikino_api") (r "^0.3") (d #t) (k 0)))) (h "085lq910xbz97kkfsgx3b800jbl1g29h4hf554hfa7y4qdd660hi") (f (quote (("force-color"))))))

(define-public crate-mikino-0.4.0 (c (n "mikino") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "mikino_api") (r "^0.4") (f (quote ("parser" "demo"))) (d #t) (k 0)))) (h "18vwv4mgph45fzjpfb7x7fxihrnw7npmaw355xksp2lhg9jxsagk") (f (quote (("force-color"))))))

(define-public crate-mikino-0.5.0 (c (n "mikino") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "mikino_api") (r "^0.5") (f (quote ("parser"))) (d #t) (k 0)))) (h "07aw3h22y1wx5ybik6l6qkdwgr3dgdy16gyif2s9pdlrm8h4p9q5") (f (quote (("force-color"))))))

(define-public crate-mikino-0.5.1 (c (n "mikino") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "mikino_api") (r "^0.5.1") (f (quote ("parser"))) (d #t) (k 0)))) (h "1x4dsn5vfxpy9ci8zcgffg7qmwrn9ncmn1fbky1biads9imgqmaf") (f (quote (("force-color"))))))

(define-public crate-mikino-0.5.2 (c (n "mikino") (v "0.5.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "mikino_api") (r "^0.5.1") (f (quote ("parser"))) (d #t) (k 0)))) (h "0bm76yf6mmcjdp6gp6gwwsf59y039g8a4d80rmw1wpyhf013xd0y") (f (quote (("force-color"))))))

(define-public crate-mikino-0.9.0 (c (n "mikino") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mikino_api") (r "^0.9") (d #t) (k 0)))) (h "1zkk34v8rzypgklzn4pvw35cskxy34jqbnn0mz2kql96lcm0v0kx") (f (quote (("force-color"))))))

(define-public crate-mikino-0.9.1 (c (n "mikino") (v "0.9.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mikino_api") (r "^0.9.1") (d #t) (k 0)))) (h "1g1jq7cgw9pgrkhwmrqah5b1srg1i8v7cpcl2zad112cj66cgcyr") (f (quote (("force-color"))))))

