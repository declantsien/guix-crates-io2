(define-module (crates-io mi ra miraland-loader-v4-program) #:use-module (crates-io))

(define-public crate-miraland-loader-v4-program-1.18.0 (c (n "miraland-loader-v4-program") (v "1.18.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-measure") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-program-runtime") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "solana_rbpf") (r "=0.8.0") (d #t) (k 0)))) (h "1fkvd1b7zvc0qcip44jv7vn7skk7gwl7xp3hd3am457vhhn8297b")))

(define-public crate-miraland-loader-v4-program-1.18.1 (c (n "miraland-loader-v4-program") (v "1.18.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-measure") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-program-runtime") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "solana_rbpf") (r "=0.8.0") (d #t) (k 0)))) (h "1sijn5j7k48zq94vdngmc3j8wx13sghkyw5xw31v4cf2hh6jy52w")))

(define-public crate-miraland-loader-v4-program-1.18.2 (c (n "miraland-loader-v4-program") (v "1.18.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-measure") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-program-runtime") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "solana_rbpf") (r "=0.8.0") (d #t) (k 0)))) (h "1mrkyw139mpyvgns42k5kpkcp84ws0blbhzmgbfig450h63zvbms")))

(define-public crate-miraland-loader-v4-program-1.18.3 (c (n "miraland-loader-v4-program") (v "1.18.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-measure") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-program-runtime") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "solana_rbpf") (r "=0.8.0") (d #t) (k 0)))) (h "1kvl7z05r4x63p66ar1km4hpgpig0g54djydgr4521p234gnvg44")))

