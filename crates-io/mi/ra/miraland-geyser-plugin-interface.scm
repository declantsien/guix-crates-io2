(define-module (crates-io mi ra miraland-geyser-plugin-interface) #:use-module (crates-io))

(define-public crate-miraland-geyser-plugin-interface-1.14.17-rc4 (c (n "miraland-geyser-plugin-interface") (v "1.14.17-rc4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09kn8l2hy5lbig6pk0c93vzszmaq9fbc1d9jh8xcm099pwi0xvgj")))

(define-public crate-miraland-geyser-plugin-interface-1.14.17-rc5 (c (n "miraland-geyser-plugin-interface") (v "1.14.17-rc5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1lmbg0ilgac8ljmgkm4lvhg622ircxvwnxlmccy6v88wl4c25gzp")))

(define-public crate-miraland-geyser-plugin-interface-1.14.17-rc6 (c (n "miraland-geyser-plugin-interface") (v "1.14.17-rc6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lzaqxgq16jh73w56ac8c3s8nj0ndq80xq6qjgla2wk05fqqxa20")))

(define-public crate-miraland-geyser-plugin-interface-1.14.17 (c (n "miraland-geyser-plugin-interface") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "^1.14.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1bhvjbqfp30wvkiisc11nhr5y524brci4b8f98z3rc9hryqms73n")))

(define-public crate-miraland-geyser-plugin-interface-1.14.18 (c (n "miraland-geyser-plugin-interface") (v "1.14.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "^1.14.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00q4j209pxryccar8yr3q36bhjxl1wh2f6b47p9h82c4l7lv4fql")))

(define-public crate-miraland-geyser-plugin-interface-1.14.19 (c (n "miraland-geyser-plugin-interface") (v "1.14.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.19") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "^1.14.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d81ap3y0mrcrywg3w3db4zhf3n17glpfc0cs2nbqr0kql80xqdj")))

(define-public crate-miraland-geyser-plugin-interface-1.18.0 (c (n "miraland-geyser-plugin-interface") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "01360vmm8y2ivh327cadnnqrs12csikhxwb6yp6q2d8xp1znazw6")))

(define-public crate-miraland-geyser-plugin-interface-1.18.1 (c (n "miraland-geyser-plugin-interface") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.18.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ssx1yklrldc9d9a84l784ac401kyic8mr827nkdlfjrsrdic482")))

(define-public crate-miraland-geyser-plugin-interface-1.18.2 (c (n "miraland-geyser-plugin-interface") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.18.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1c2ry5wlmm1kym8qmvqmr6lgp97wlvgbafwpmfg7nnk42njg96yd")))

(define-public crate-miraland-geyser-plugin-interface-1.18.3 (c (n "miraland-geyser-plugin-interface") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-transaction-status") (r "=1.18.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "165fs8iygm4abjbn4b1svxk7b2nkb3qggzbngffk3r0sxvrkhkxh")))

