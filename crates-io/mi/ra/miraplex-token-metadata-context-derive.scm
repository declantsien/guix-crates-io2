(define-module (crates-io mi ra miraplex-token-metadata-context-derive) #:use-module (crates-io))

(define-public crate-miraplex-token-metadata-context-derive-0.2.1 (c (n "miraplex-token-metadata-context-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "112crj5m8z31m1fr14lxm2mn6ks2g1nw60c9zzbs4myzdj6yarpc")))

