(define-module (crates-io mi ra miractl) #:use-module (crates-io))

(define-public crate-miractl-0.1.0 (c (n "miractl") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f5j80gbghxw8k4ik4i74vsm4dq3rza5bxp83dfz1396a8w9f6c3")))

