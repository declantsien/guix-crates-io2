(define-module (crates-io mi ra miraland-rayon-threadlimit) #:use-module (crates-io))

(define-public crate-miraland-rayon-threadlimit-1.14.17-rc1 (c (n "miraland-rayon-threadlimit") (v "1.14.17-rc1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "093y1srimg4l95mw3w7fr1x4y4i1phhnbbc7jg7nzrgrs5aqsd4w") (y #t)))

(define-public crate-miraland-rayon-threadlimit-1.14.17-rc3 (c (n "miraland-rayon-threadlimit") (v "1.14.17-rc3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0fp6y6g4l8flrc2f98gp7f0cav23skr1p8srx2j1jcyg07z1n09i")))

(define-public crate-miraland-rayon-threadlimit-1.14.17-rc4 (c (n "miraland-rayon-threadlimit") (v "1.14.17-rc4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1wy44zhdkxp74mas2lv841almiy550rjxx05ihfw7ikkf158s359")))

(define-public crate-miraland-rayon-threadlimit-1.14.17-rc5 (c (n "miraland-rayon-threadlimit") (v "1.14.17-rc5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1rplw07140m3s4c07xqrlh13pzxx7yfd4naz7p62kz6mf7am4ciw")))

(define-public crate-miraland-rayon-threadlimit-1.14.17-rc6 (c (n "miraland-rayon-threadlimit") (v "1.14.17-rc6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0gfsd1ljdr6sglpks7smc89asygk5gvzmd5x50226q7ai02qxiy6")))

(define-public crate-miraland-rayon-threadlimit-1.14.17 (c (n "miraland-rayon-threadlimit") (v "1.14.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "00mb8f9i56hwjx9pbrnw86kakxbapzxy2ynf7ygd0x32lvs1x7af")))

(define-public crate-miraland-rayon-threadlimit-1.14.18 (c (n "miraland-rayon-threadlimit") (v "1.14.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "042vrwx5vfg60kbjwmi6rky3yl6igbr6iskx07amnwvcp90fll74")))

(define-public crate-miraland-rayon-threadlimit-1.14.19 (c (n "miraland-rayon-threadlimit") (v "1.14.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "106aa74q3n42rjmrwwhhmy38v5gfjp7mxmp02i9z4p0bmizwq2ly")))

(define-public crate-miraland-rayon-threadlimit-1.18.0 (c (n "miraland-rayon-threadlimit") (v "1.18.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1zrv6y0q29aaig4s4qkcrdg7y0x8wsz1zj088mh9jdd5qvn3z6hr")))

(define-public crate-miraland-rayon-threadlimit-1.18.1 (c (n "miraland-rayon-threadlimit") (v "1.18.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0a2v9k2g2d3k7sai9yld0h5l2kdgi56234fi61wzavg2qy47vzmw")))

(define-public crate-miraland-rayon-threadlimit-1.18.2 (c (n "miraland-rayon-threadlimit") (v "1.18.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1lxka4snrd7227jd8qdbw6lh430fr87mpzmjsv6zbd75xyjdlhh8")))

(define-public crate-miraland-rayon-threadlimit-1.18.3 (c (n "miraland-rayon-threadlimit") (v "1.18.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1ai4c715p0bknl4jshwkvl9ql0msq17dg0sd68zqj5gcfqjb4pdb")))

