(define-module (crates-io mi ra miraland-query-security-txt) #:use-module (crates-io))

(define-public crate-miraland-query-security-txt-1.1.1 (c (n "miraland-query-security-txt") (v "1.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "miraland-client") (r "^1.10.0") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.10.0") (d #t) (k 0)) (d (n "miraland-security-txt") (r "^1.1.1") (f (quote ("parser"))) (d #t) (k 0)))) (h "13friamj8zzppfs8a3ndffzz2vq4x6bd2ff7zsjf9jf9dwygpk0c")))

