(define-module (crates-io mi ra miraland-merkle-tree) #:use-module (crates-io))

(define-public crate-miraland-merkle-tree-1.14.17-rc1 (c (n "miraland-merkle-tree") (v "1.14.17-rc1") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "=1.14.17-rc1") (d #t) (k 0)))) (h "0pbnx5ljxd7dj2i5qjga78cy19cgs20pl1z9h89dh6qqgn88lnip") (y #t)))

(define-public crate-miraland-merkle-tree-1.14.17-rc3 (c (n "miraland-merkle-tree") (v "1.14.17-rc3") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "=1.14.17-rc3") (d #t) (k 0)))) (h "0f0cy50akrk31lf8vzn8g3acnqi7ghv2ymrrydnr499scd6fbdb9")))

(define-public crate-miraland-merkle-tree-1.14.17-rc4 (c (n "miraland-merkle-tree") (v "1.14.17-rc4") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "=1.14.17-rc4") (d #t) (k 0)))) (h "037y7zada23ngia6w58pdiz42lfgz3kirbzla4a0s9kh1yb25fzv")))

(define-public crate-miraland-merkle-tree-1.14.17-rc5 (c (n "miraland-merkle-tree") (v "1.14.17-rc5") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "=1.14.17-rc5") (d #t) (k 0)))) (h "1d0k60hz5xb05rqn7bnv67r688qcgh1j1xn5pvw85dy7gx7im40i")))

(define-public crate-miraland-merkle-tree-1.14.17-rc6 (c (n "miraland-merkle-tree") (v "1.14.17-rc6") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "=1.14.17-rc6") (d #t) (k 0)))) (h "0b8zyp0v9r064hmd46bqyawq43ix45g9kac905l1d0az4wnjv703")))

(define-public crate-miraland-merkle-tree-1.14.17 (c (n "miraland-merkle-tree") (v "1.14.17") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "^1.14.17") (d #t) (k 0)))) (h "1drm33shr2yvcmw65z8axr493fzs0iliqw4drpggkb1k3jl9763m")))

(define-public crate-miraland-merkle-tree-1.14.18 (c (n "miraland-merkle-tree") (v "1.14.18") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "^1.14.18") (d #t) (k 0)))) (h "10z0za5lbakkr5gylcq604y95lvms818x1jw2kwbi7hxs1lh5gnx")))

(define-public crate-miraland-merkle-tree-1.14.19 (c (n "miraland-merkle-tree") (v "1.14.19") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)) (d (n "miraland-program") (r "^1.14.19") (d #t) (k 0)))) (h "0y29gq1acxizcayw1ja6yzk468b0ivdcssnbw8kb171v49sns3lc")))

(define-public crate-miraland-merkle-tree-1.18.0 (c (n "miraland-merkle-tree") (v "1.18.0") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "miraland-program") (r "=1.18.0") (d #t) (k 0)))) (h "0xm5j5p8n11d80bmx2ijy7yngaf4sz3briahwyb2akfpv5pr2lfq")))

(define-public crate-miraland-merkle-tree-1.18.1 (c (n "miraland-merkle-tree") (v "1.18.1") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "miraland-program") (r "=1.18.1") (d #t) (k 0)))) (h "0zj95lr234gcvg1fz96v5zbmamfhj1h3z18c4x93rcvn2da0akz3")))

(define-public crate-miraland-merkle-tree-1.18.2 (c (n "miraland-merkle-tree") (v "1.18.2") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "miraland-program") (r "=1.18.2") (d #t) (k 0)))) (h "0gfjlr6m14gcnm2bjp6pnfi460nn7addbxwi6prmqpzdg45mgmlg")))

(define-public crate-miraland-merkle-tree-1.18.3 (c (n "miraland-merkle-tree") (v "1.18.3") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "miraland-program") (r "=1.18.3") (d #t) (k 0)))) (h "1prmhqcfgchcgr87w383p34sjpxy9bdzkivys14nwfwaz3cagzqf")))

