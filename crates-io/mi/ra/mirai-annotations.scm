(define-module (crates-io mi ra mirai-annotations) #:use-module (crates-io))

(define-public crate-mirai-annotations-0.0.1 (c (n "mirai-annotations") (v "0.0.1") (h "1fl3cmrjxc2c0g83596xxb19bvssvikjza51c7cd2z23qc1v0jgd")))

(define-public crate-mirai-annotations-0.1.0 (c (n "mirai-annotations") (v "0.1.0") (h "03fq4cz2samv6zsdra95s4q4h3i840fwny46w44x2w6zyc9cyih6")))

(define-public crate-mirai-annotations-0.2.0 (c (n "mirai-annotations") (v "0.2.0") (h "0qd4gki6ibh48xjqjdzh76x4xja9cm6a21nqgwqifvcmk14bd5mw")))

(define-public crate-mirai-annotations-0.2.1 (c (n "mirai-annotations") (v "0.2.1") (h "194541jn0xrnxh1mqxb19c40m1lnd9bb22kk6kj6awi0q74379vn")))

(define-public crate-mirai-annotations-1.2.2 (c (n "mirai-annotations") (v "1.2.2") (h "0vvsvagy8m456dyj329l6d0pn3j5n51qhpl093d2gqx07zikii9v")))

(define-public crate-mirai-annotations-1.3.1 (c (n "mirai-annotations") (v "1.3.1") (h "1abfkfxdfza1p3lp75krjrnzsjz99cfrxx43cspsq4dy9zy48m6i")))

(define-public crate-mirai-annotations-1.4.0 (c (n "mirai-annotations") (v "1.4.0") (h "0ag5afgq49mm7kny0dk0i6rf5w8r9jjaagc4d46b2zww3v1wklh4")))

(define-public crate-mirai-annotations-1.5.0 (c (n "mirai-annotations") (v "1.5.0") (h "0i96m4pshwc80nxpm3p8n2xh97g8zl3zsf6p8lp67af7qg6xcs3r")))

(define-public crate-mirai-annotations-1.6.0 (c (n "mirai-annotations") (v "1.6.0") (h "19gy0w2nmymlsknbzdij0rm2sfvnfdix4cqvrxcg1yy8hdymnapi")))

(define-public crate-mirai-annotations-1.6.1 (c (n "mirai-annotations") (v "1.6.1") (h "08h4g6cz6xfx3qxmfwkzy1f8dzcjy8avdxm88igy8w5fr0kpv5pv")))

(define-public crate-mirai-annotations-1.7.0 (c (n "mirai-annotations") (v "1.7.0") (h "1g9yjj38pwh6ccscafil5482470c45id9krkzniz1ycsggklav57")))

(define-public crate-mirai-annotations-1.8.0 (c (n "mirai-annotations") (v "1.8.0") (h "05kvs0yn4gzk6qrgqgnb0xfj1lyk185nvxil936bw1aivcyi9315")))

(define-public crate-mirai-annotations-1.9.0 (c (n "mirai-annotations") (v "1.9.0") (h "0hs2xq9h64gyn66svjdylalkxpbb0alr4zkzalgcnb092nm1glmm")))

(define-public crate-mirai-annotations-1.9.1 (c (n "mirai-annotations") (v "1.9.1") (h "0mgfabdcpy6i5m0xiixgx46pl6mxznw1lzpr5r8dqmnyk2l3khjl")))

(define-public crate-mirai-annotations-1.10.1 (c (n "mirai-annotations") (v "1.10.1") (h "0a0vv83rx9f366gbrmrlfrfkych6f0adhf7z1ncbs9v9caw7i3ml")))

(define-public crate-mirai-annotations-1.12.0 (c (n "mirai-annotations") (v "1.12.0") (h "1lfkgarmzd7mdr739s9yidh0xhc9d3g4jjiwh246mwmkq5i0ign9")))

