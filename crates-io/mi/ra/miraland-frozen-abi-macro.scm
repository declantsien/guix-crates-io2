(define-module (crates-io mi ra miraland-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc.1 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0i9yls9fx1wrnzjz1345bbdhji5j13lfj566q5mlbv29ngy18q3a") (y #t)))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc1 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "12mfjfk1wa24jmm4md8gqskm43mspg0vf9nvmz6ixn2qy74xd4p0") (y #t)))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc2 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1i5n9bn14h2vd7n3qkr66914m5yhxzf7f7nacsyfa75cyqr9sa1i")))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc3 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1ds5arc4x5svxl0q8qmdwbrwskm5iw7a969hhpr7gnsh20khhdal")))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc4 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0pz3dqw9a5zpd6i1qnn23dzcpi2if3xdcpkzngbdwal2y66yym4q")))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc5 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1csp9jp1d32yjhph2li4ri6pip8f8pk3r1ahf9qcmsmlivw0jrrk")))

(define-public crate-miraland-frozen-abi-macro-1.14.17-rc6 (c (n "miraland-frozen-abi-macro") (v "1.14.17-rc6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "15cbdkv08fpv2d4bkqhx8akx453wdf1pdmn5rfn686ckbfw2668y")))

(define-public crate-miraland-frozen-abi-macro-1.14.17 (c (n "miraland-frozen-abi-macro") (v "1.14.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "094xbf3y0f86alqxg7f4n98d3gynmd35zsqgb1wf9y49sr891q6p")))

(define-public crate-miraland-frozen-abi-macro-1.14.18 (c (n "miraland-frozen-abi-macro") (v "1.14.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1vccs1lbz5ggzwyiybsdq6xp334ji2y6nvsb34ky3j1ff5mygsz5")))

(define-public crate-miraland-frozen-abi-macro-1.14.19 (c (n "miraland-frozen-abi-macro") (v "1.14.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "04w42y32wqnqk1kwllbf811jfy7nls2i78x2dvcjp1mri7kmq0x9")))

(define-public crate-miraland-frozen-abi-macro-1.18.0 (c (n "miraland-frozen-abi-macro") (v "1.18.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1k6yz4ksqzn665v0m35mx3wqrypf5gmra5b32m4g8iln2l30inka")))

(define-public crate-miraland-frozen-abi-macro-1.18.1 (c (n "miraland-frozen-abi-macro") (v "1.18.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0gbl765al8l5lvmgzjjjm7vffr27dir7j5wzadwz2862lb7mr0ac")))

(define-public crate-miraland-frozen-abi-macro-1.18.2 (c (n "miraland-frozen-abi-macro") (v "1.18.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bg3g7j2cp8abz48iwlig8hr9cj9a28dv7swpvlwz2ps63jiaagh")))

(define-public crate-miraland-frozen-abi-macro-1.18.3 (c (n "miraland-frozen-abi-macro") (v "1.18.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "00qz7wxm8fy8fjf1s09lz2xjrmqxh812m6grxljc0m52d8awbv2q")))

