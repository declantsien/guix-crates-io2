(define-module (crates-io mi ra miraculous_ui) #:use-module (crates-io))

(define-public crate-miraculous_ui-0.1.0 (c (n "miraculous_ui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0s16fmz9k8m65na6kp64vp8kg4xz58fcfwfnglh365sqqh08drid") (y #t)))

(define-public crate-miraculous_ui-0.1.1 (c (n "miraculous_ui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0fp0hnvzbgpyq10nk4qdy4a9jgyn5x79vv1sp3llg4xgkqkh8ppy") (y #t)))

(define-public crate-miraculous_ui-0.1.2 (c (n "miraculous_ui") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1hq9vqh1cgiak34047v18c0m1snjm5mi29z4kv8sw5mzfnh6ayds") (y #t)))

