(define-module (crates-io mi ra miraland-banks-interface) #:use-module (crates-io))

(define-public crate-miraland-banks-interface-1.14.17-rc1 (c (n "miraland-banks-interface") (v "1.14.17-rc1") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc1") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "186mrkjswggwsw1hl8sc53cf82r9waivgawc844pap5d8bbm5w9v") (y #t)))

(define-public crate-miraland-banks-interface-1.14.17-rc2 (c (n "miraland-banks-interface") (v "1.14.17-rc2") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g5sbclkgwfrh79n3d4a6banh2kdj52c1y957bk3hdyrnxcdssbh")))

(define-public crate-miraland-banks-interface-1.14.17-rc3 (c (n "miraland-banks-interface") (v "1.14.17-rc3") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksskgicnvn2pa7295qzj51x53clrv2zpc3gz222l4rbpwpbai2i")))

(define-public crate-miraland-banks-interface-1.14.17-rc4 (c (n "miraland-banks-interface") (v "1.14.17-rc4") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nyan395mdcqsr6myavywmr3yhvk3m7k9r0lm4fqdvwzvwa291ci")))

(define-public crate-miraland-banks-interface-1.14.17-rc5 (c (n "miraland-banks-interface") (v "1.14.17-rc5") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsq4jdi65kr9lsc80n1aqnsxqfkmr7f8axw2583bi06g35rarjx")))

(define-public crate-miraland-banks-interface-1.14.17-rc6 (c (n "miraland-banks-interface") (v "1.14.17-rc6") (d (list (d (n "miraland-sdk") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q25dmhcvs9s8nsd7rn0lggbw0jjayi4bfgml15bwl9zk1lm5ca9")))

(define-public crate-miraland-banks-interface-1.14.17 (c (n "miraland-banks-interface") (v "1.14.17") (d (list (d (n "miraland-sdk") (r "^1.14.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c544fsxniy0cfpidlsqw6ijabvmb655m0yy11ihw3sk0a9py4q4")))

(define-public crate-miraland-banks-interface-1.14.18 (c (n "miraland-banks-interface") (v "1.14.18") (d (list (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mf2ivj9wda3i2zdf9qmxr920dd80gx2vgwrm6j695inxxp5v15k")))

(define-public crate-miraland-banks-interface-1.14.19 (c (n "miraland-banks-interface") (v "1.14.19") (d (list (d (n "miraland-sdk") (r "^1.14.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0qpld3qzri0rxsj9g69kv1mlnwd4zh7lbfj8cmwkj8m81i8vbc")))

(define-public crate-miraland-banks-interface-1.18.0 (c (n "miraland-banks-interface") (v "1.18.0") (d (list (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iw6i8510h7yrcrnxhpd8i74p47bw75nr44fapwf6ymb00fx2nps")))

(define-public crate-miraland-banks-interface-1.18.1 (c (n "miraland-banks-interface") (v "1.18.1") (d (list (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rs9ddjw61w8wwj7accwakpd9wqrkm11y6fhbivcj1pbyclm92i7")))

(define-public crate-miraland-banks-interface-1.18.2 (c (n "miraland-banks-interface") (v "1.18.2") (d (list (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13gmf36ji80ag36fvljkh7149h9fs59jh18p3c90s7xv60hzs3a2")))

(define-public crate-miraland-banks-interface-1.18.3 (c (n "miraland-banks-interface") (v "1.18.3") (d (list (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "175wbfpq0jrwcc13ys4z0myrx1y93irv4588b6is4vsjgz7mrq78")))

