(define-module (crates-io mi ra miraland-security-txt) #:use-module (crates-io))

(define-public crate-miraland-security-txt-1.1.1 (c (n "miraland-security-txt") (v "1.1.1") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)) (d (n "twoway") (r "=0.2.2") (o #t) (d #t) (k 0)))) (h "1vqzqmfq5y8v6c2a302wby2p6bdrizsrdfxf65h9psc32ysy5484") (f (quote (("parser" "thiserror" "twoway"))))))

