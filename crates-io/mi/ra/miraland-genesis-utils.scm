(define-module (crates-io mi ra miraland-genesis-utils) #:use-module (crates-io))

(define-public crate-miraland-genesis-utils-1.14.17-rc4 (c (n "miraland-genesis-utils") (v "1.14.17-rc4") (d (list (d (n "miraland-download-utils") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-runtime") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 0)))) (h "0zk5d3f4fwkhwqiv6g59bfda3qwfcgbvxf0bijlh6syijphm9zpd")))

(define-public crate-miraland-genesis-utils-1.14.17-rc5 (c (n "miraland-genesis-utils") (v "1.14.17-rc5") (d (list (d (n "miraland-download-utils") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "miraland-runtime") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 0)))) (h "11yfyraiz4fhvx7wzxr5cbf3ms32hbmi0ribbkaqiy73hhiyv1jq")))

(define-public crate-miraland-genesis-utils-1.14.17-rc6 (c (n "miraland-genesis-utils") (v "1.14.17-rc6") (d (list (d (n "miraland-download-utils") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "miraland-runtime") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc6") (d #t) (k 0)))) (h "0rjpamsanvs1zcmrhjbdx97a80p5ssxd7hj5l2kbyw8srj74jmi9")))

(define-public crate-miraland-genesis-utils-1.14.17 (c (n "miraland-genesis-utils") (v "1.14.17") (d (list (d (n "miraland-download-utils") (r "^1.14.17") (d #t) (k 0)) (d (n "miraland-runtime") (r "^1.14.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17") (d #t) (k 0)))) (h "0fwsmhzfvvw5s7sj9jc7ffik44ss9lh4f6xc3vr47a74vcqmxy16")))

(define-public crate-miraland-genesis-utils-1.14.18 (c (n "miraland-genesis-utils") (v "1.14.18") (d (list (d (n "miraland-download-utils") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-runtime") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 0)))) (h "1fwwa9i7jjrbq9k4q09j77caqsrjjzw910sb0hg682dp9qpfszf9")))

(define-public crate-miraland-genesis-utils-1.14.19 (c (n "miraland-genesis-utils") (v "1.14.19") (d (list (d (n "miraland-download-utils") (r "^1.14.19") (d #t) (k 0)) (d (n "miraland-runtime") (r "^1.14.19") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.19") (d #t) (k 0)))) (h "0rws205p1k23p4cia2cw3qym5hgv2djrz3bgga1s5j86v3a84d1n")))

(define-public crate-miraland-genesis-utils-1.18.0 (c (n "miraland-genesis-utils") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-accounts-db") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-download-utils") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-rpc-client") (r "=1.18.0") (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "1jha1082gl2pd8z2ybid22ffjifj8pvyvx18qbl0qfhy04fj2ax5")))

(define-public crate-miraland-genesis-utils-1.18.1 (c (n "miraland-genesis-utils") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-accounts-db") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-download-utils") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-rpc-client") (r "=1.18.1") (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "0rrhl4rdjfp1g3dbpkkjazfhkfazh430364ziwxzqp772lcv0ihh")))

(define-public crate-miraland-genesis-utils-1.18.2 (c (n "miraland-genesis-utils") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-accounts-db") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-download-utils") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-rpc-client") (r "=1.18.2") (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "0cm68w17ivdgk0kk4vxk438a7yiy7q1hig32pwn164vlkl2d4pyc")))

(define-public crate-miraland-genesis-utils-1.18.3 (c (n "miraland-genesis-utils") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-accounts-db") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-download-utils") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-rpc-client") (r "=1.18.3") (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "0avsi3hjf96sqz3p8f5ixl3ihar1cx11riyj2k092a1pla624k22")))

