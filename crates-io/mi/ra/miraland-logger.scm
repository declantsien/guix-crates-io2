(define-module (crates-io mi ra miraland-logger) #:use-module (crates-io))

(define-public crate-miraland-logger-1.14.17-rc.1 (c (n "miraland-logger") (v "1.14.17-rc.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1gfafc9hj1sa56rxgbhvjpzm1j9falmdiqs6hz5pzbn6p9cnxw97") (y #t)))

(define-public crate-miraland-logger-1.14.17-rc1 (c (n "miraland-logger") (v "1.14.17-rc1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01z4pqn6ij8v8ss5p1gazlzvxb9dyzipn5hgfrr7ppd338wjnmg1") (y #t)))

(define-public crate-miraland-logger-1.14.17-rc2 (c (n "miraland-logger") (v "1.14.17-rc2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "03f7d9nhpbfs493bm1xwvd483bqx9fcqnkvi68gqhlf4fja2iys6")))

(define-public crate-miraland-logger-1.14.17-rc3 (c (n "miraland-logger") (v "1.14.17-rc3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ivf2xzl854d8i5h1d15785kckqcplwy511n8gg3acnhl5dvsr99")))

(define-public crate-miraland-logger-1.14.17-rc4 (c (n "miraland-logger") (v "1.14.17-rc4") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1m2376b47aa51pla7qlszr8yh2198xn96gqcig33ds1gi3n6lckf")))

(define-public crate-miraland-logger-1.14.17-rc5 (c (n "miraland-logger") (v "1.14.17-rc5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1xz12fnivqgdd75m5lw07cml94r6xdqjlgvyppwbfd0l6822wm3f")))

(define-public crate-miraland-logger-1.14.17-rc6 (c (n "miraland-logger") (v "1.14.17-rc6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1phmf0kw45p1z6iw9i7irrlmrl6p29ylxrkhpz6xz7bfac5vscfc")))

(define-public crate-miraland-logger-1.14.17 (c (n "miraland-logger") (v "1.14.17") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "151zddhblydxj8ab47hn3lnpy3w79msvgrqjf7jb697pkrbk0byz")))

(define-public crate-miraland-logger-1.14.18 (c (n "miraland-logger") (v "1.14.18") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0way211nhr4nhm0biv1b65zrc2ylcsf26zb20gr3amgy2b1p78w3")))

(define-public crate-miraland-logger-1.14.19 (c (n "miraland-logger") (v "1.14.19") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "049bs8dv1cm3fprd7ahmil01ph49x2ihrpwz34pv0ncfsn4ws3xf")))

(define-public crate-miraland-logger-1.18.0 (c (n "miraland-logger") (v "1.18.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "00x8l7r13xwrgv55aik7zg0n2rhgy57hm30v22rq2r2gk15lv3p9")))

(define-public crate-miraland-logger-1.18.1 (c (n "miraland-logger") (v "1.18.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1p5iz9gs5fbkipbk7r2xpqq51f2awry1dpis2j4ipws1m32mcrpn")))

(define-public crate-miraland-logger-1.18.2 (c (n "miraland-logger") (v "1.18.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1lxdq8288xjnqq9gnhxm6z22aadan1fk7bg7h3vqds8jdziy1nxd")))

(define-public crate-miraland-logger-1.18.3 (c (n "miraland-logger") (v "1.18.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0135r2a7g97jrlsdsvmrqw7vvl4r4n3380z2adrycxnvpnshf95c")))

