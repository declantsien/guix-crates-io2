(define-module (crates-io mi ra miracl_core_ed25519) #:use-module (crates-io))

(define-public crate-miracl_core_ed25519-0.1.0 (c (n "miracl_core_ed25519") (v "0.1.0") (h "08viidxxwnk2b3aa8n4537valf85jlp5gi5qjk97lri619gn3cky")))

(define-public crate-miracl_core_ed25519-0.2.0 (c (n "miracl_core_ed25519") (v "0.2.0") (d (list (d (n "miracl_core_bls12381") (r "^4.1.2") (d #t) (k 0)))) (h "09bnq7q5h35b7klrafjpjii8c19rmdwp4qbmsr585bbfg37233fi")))

