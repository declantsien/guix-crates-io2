(define-module (crates-io mi ra miraland-memory-management) #:use-module (crates-io))

(define-public crate-miraland-memory-management-1.18.0 (c (n "miraland-memory-management") (v "1.18.0") (h "0q36jgqz9z4s3jpskhln9xfsr7pzla9lz27f68lxcpmxbg7lrjcw")))

(define-public crate-miraland-memory-management-1.18.1 (c (n "miraland-memory-management") (v "1.18.1") (h "04lcvy35cs8g2pckn97fsmz1sq5ssqnlp0j3r8m49s9h1qvlqz2n")))

(define-public crate-miraland-memory-management-1.18.2 (c (n "miraland-memory-management") (v "1.18.2") (h "107wcirmd56xpc722r8szy1dlw1m86nfv8bk8g60pligy45bfwc8")))

(define-public crate-miraland-memory-management-1.18.3 (c (n "miraland-memory-management") (v "1.18.3") (h "1bmhyvvjwsgkj1lw9y1ijdhqkk3fqqps30ypsrb10i0128f39gc7")))

