(define-module (crates-io mi ra mira) #:use-module (crates-io))

(define-public crate-mira-0.1.0 (c (n "mira") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "01zvdx4ip2dplm6gk1lz7wzn2wz1wwyr41f4lijgziya6srmqb7r")))

(define-public crate-mira-0.1.1 (c (n "mira") (v "0.1.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1lppy1iawc31mz72mifx350iyp8yi131m7aa6y5sh178yp2aszkm")))

(define-public crate-mira-0.1.2 (c (n "mira") (v "0.1.2") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1wawlwlnqa7w803faddlx28vb0liwdamqzilysvzlgjgqics13p0")))

(define-public crate-mira-0.1.3 (c (n "mira") (v "0.1.3") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1m2a7wrij7vrxzw709fkwdnih0s5ghy2gm15ghs148q519bnmbky")))

(define-public crate-mira-0.1.4 (c (n "mira") (v "0.1.4") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0z47zwdhx6pfs20l0lfqcsxa10qshwhng1824vwqcjgiag7hvd3k")))

(define-public crate-mira-0.1.5 (c (n "mira") (v "0.1.5") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0slk22b1gzk5vdkkjkvbf7qr6pbb9q5ywvs49vciz4z1fzcmhz7z")))

(define-public crate-mira-0.1.6 (c (n "mira") (v "0.1.6") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0byad8yl7ypzzdjwn5rwh7r9jfsmfjdsm7ym4n0121n6h7qvld70")))

(define-public crate-mira-0.1.7 (c (n "mira") (v "0.1.7") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1173klcqc6fdgzbw5i8wls1nws39a9s45n36fkkxgnv1hfxjzxi6")))

(define-public crate-mira-0.1.8 (c (n "mira") (v "0.1.8") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05i0d1hfm1g0v9rjh3ndfcj5j5vl9kraqjq0ry5rn08250mxmiia")))

(define-public crate-mira-0.1.9 (c (n "mira") (v "0.1.9") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05kckr215ykh0nhcql9jf7crby06fa27g7j0n5xnyxvxg7aj7q9q")))

(define-public crate-mira-0.1.10 (c (n "mira") (v "0.1.10") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fpgpkfr1d65ra3daqqrm34m4hk9mj7xxwz3kryzx8zbc55svznr")))

(define-public crate-mira-0.1.11 (c (n "mira") (v "0.1.11") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nnsj6h7gyn02ha5vyw0afs9bcb7xr89kqdmivzhyc63vphmpdli")))

(define-public crate-mira-0.1.12 (c (n "mira") (v "0.1.12") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bhnqkxc8dwbm396nf6bqr5v4fbfi17y4bv04mx4simdhwdyl9cf")))

(define-public crate-mira-0.1.13 (c (n "mira") (v "0.1.13") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03msv1558iib6ipfx6ifbprmkd12sf8c8gkq7a2anzbdhp0l6ygx")))

(define-public crate-mira-0.1.14 (c (n "mira") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09nm7rzm136rwl4lh4cdmx2vz6jna3501ncr4sbbhjkr6pr2ya56")))

(define-public crate-mira-0.1.15 (c (n "mira") (v "0.1.15") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1l3rydyfhsvx9kkp5s8jmmgha4l7gzikqx8l0037axdyvqzymh69")))

(define-public crate-mira-0.1.16 (c (n "mira") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v0ydx1yhjl3sf56mfwilbgy6rw0pj5hwr2qnk4bpk0yf2vps8h8")))

(define-public crate-mira-0.1.17 (c (n "mira") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "138r1afvzxa27ibpj5f2fkiwh8vqhgcmry2xmnyyp16v0lg3z19x")))

(define-public crate-mira-0.1.18 (c (n "mira") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xmzp7mhbh6d9dxqfy3rapq8ws8c0xdswka743jmc40agss0n2gp")))

(define-public crate-mira-0.1.19 (c (n "mira") (v "0.1.19") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pk8r7cj1ciqqfcvn25bbmsszmy52bsvidn9ypa73hzh7w64kxmv")))

(define-public crate-mira-0.1.20 (c (n "mira") (v "0.1.20") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00b58285xhy7dk3d34srx543ndpp5xbmlkjbyvs2683h8pqwqzyh")))

(define-public crate-mira-0.1.21 (c (n "mira") (v "0.1.21") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1dmyww8jygxi7bsxm9qslwpfzyhcs8lmflb3x9yklqk61wybzqlw")))

(define-public crate-mira-0.1.22 (c (n "mira") (v "0.1.22") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13qqxvlhr9r38ld50gc5gcbq442g9lq5hxp7iy640vylm7i5dmk8")))

(define-public crate-mira-0.1.23 (c (n "mira") (v "0.1.23") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0pw481r852fc9ip5cmd2x59nhaagngbrv5sxhyryii2pv81qyaj8")))

(define-public crate-mira-0.1.24 (c (n "mira") (v "0.1.24") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "098b9c1jfsqhavprdf8h2nzbf6jjp05yf5pvfd3bnn6gxf9zfz3r")))

(define-public crate-mira-0.1.25 (c (n "mira") (v "0.1.25") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.2.0") (d #t) (k 2)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13i3vninn5arsjanzi9y0brsx1l09j3y9z24skp6n8iw4r6rwhyr")))

