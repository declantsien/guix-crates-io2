(define-module (crates-io mi ra miraland-notifier) #:use-module (crates-io))

(define-public crate-miraland-notifier-1.14.17-rc1 (c (n "miraland-notifier") (v "1.14.17-rc1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f5g36rfg3r40cqn6ymr62acsyks5xypcyv4qrx1mh4amjpzkbpd") (y #t)))

(define-public crate-miraland-notifier-1.14.17-rc3 (c (n "miraland-notifier") (v "1.14.17-rc3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fv0hg1sawrw4nj0w4282s8f83f2ijpswikr79wbprpafpfwm96s")))

(define-public crate-miraland-notifier-1.14.17-rc4 (c (n "miraland-notifier") (v "1.14.17-rc4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p0vygsi8xvpy0s74y18fpx28kw8y2s24yzsyckd5c0f1l9bmd2c")))

(define-public crate-miraland-notifier-1.14.17-rc5 (c (n "miraland-notifier") (v "1.14.17-rc5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05w5zqqyvkjv4cnkjmzqlkvqdgpc08qls354wbjw3hw51zakw955")))

(define-public crate-miraland-notifier-1.14.17-rc6 (c (n "miraland-notifier") (v "1.14.17-rc6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qap2wd2p8n4bqlb51cswc0hqfgl9k70xngbj2ri5kdzm5jhxvny")))

(define-public crate-miraland-notifier-1.14.17 (c (n "miraland-notifier") (v "1.14.17") (d (list (d (n "h2") (r "=0.3.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d428d8a1l7xp7gwindnkhic1smr8dz9r46mk9mgxv24z3v9f49k")))

(define-public crate-miraland-notifier-1.14.18 (c (n "miraland-notifier") (v "1.14.18") (d (list (d (n "h2") (r "=0.3.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wss5z0bcl7c4fc5ifjmx6z9f758wys0vkn95snf8i7jlxs8xljk")))

(define-public crate-miraland-notifier-1.14.19 (c (n "miraland-notifier") (v "1.14.19") (d (list (d (n "h2") (r "^0.3.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yjrmp68d765f8ylkxcc69id3hwmn5rhns88bs0dzlf1phqyqjl7")))

(define-public crate-miraland-notifier-1.18.0 (c (n "miraland-notifier") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0n0ddk8qg8p048ywzmq71nyw59r68xk0mrj3zkc8hcqrna74lc73")))

(define-public crate-miraland-notifier-1.18.1 (c (n "miraland-notifier") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0vnq3fv8skqxs6y8p6bdyd0j596j5rgw6lc5dmx94izqqsqi5c0n")))

(define-public crate-miraland-notifier-1.18.2 (c (n "miraland-notifier") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1flhp57m5ip3rlv209194jxrw1389h1n1z05cm3i16138379h63c")))

(define-public crate-miraland-notifier-1.18.3 (c (n "miraland-notifier") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0m71i466nfvy9c118vw1gh1qfbn202znyw3yas6yzcb1nj2yb1ck")))

