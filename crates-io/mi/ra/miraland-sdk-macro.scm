(define-module (crates-io mi ra miraland-sdk-macro) #:use-module (crates-io))

(define-public crate-miraland-sdk-macro-1.14.17-rc.1 (c (n "miraland-sdk-macro") (v "1.14.17-rc.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13k7s4kgxhggbiq2x8akcixl1dhpqy2wkqn2sqadyjj9gk0wq5di") (y #t)))

(define-public crate-miraland-sdk-macro-1.14.17-rc1 (c (n "miraland-sdk-macro") (v "1.14.17-rc1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l7wqv8vr9sydp64j8n9lvs1r05cbg58qg1giimqff40kk1xxb0g") (y #t)))

(define-public crate-miraland-sdk-macro-1.14.17-rc2 (c (n "miraland-sdk-macro") (v "1.14.17-rc2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z0rs7crvsc9a89lhpfz7m36y94hljh9fmhv7g9jkgqpgyvjcs8z")))

(define-public crate-miraland-sdk-macro-1.14.17-rc3 (c (n "miraland-sdk-macro") (v "1.14.17-rc3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14kf3li1vzmnah4mirhb0cwx4q4sljkz5fn6cm2njm6q6bc3pjgl")))

(define-public crate-miraland-sdk-macro-1.14.17-rc4 (c (n "miraland-sdk-macro") (v "1.14.17-rc4") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kp5xwbl79kgzh0bhk5s0vj553aqasxak39psh4nh35hh50y2bvd")))

(define-public crate-miraland-sdk-macro-1.14.17-rc5 (c (n "miraland-sdk-macro") (v "1.14.17-rc5") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17mdl36f33drpc6813pniqi9xxa0px4bxn5qjw70cvv3622q7hgk")))

(define-public crate-miraland-sdk-macro-1.14.17-rc6 (c (n "miraland-sdk-macro") (v "1.14.17-rc6") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ijw5wmbanmgyaqy7yqzxa4gblh1h6dq1mlgx3gwb5nv4r01csa6")))

(define-public crate-miraland-sdk-macro-1.14.17 (c (n "miraland-sdk-macro") (v "1.14.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "082xqzayc1dsnmmbb8kh1f66yp6v531ygw2l5nr6l8n92hs16mbi")))

(define-public crate-miraland-sdk-macro-1.14.18 (c (n "miraland-sdk-macro") (v "1.14.18") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13wxy2h01pq24k4bq2i2psrbi8xgxypbbjgl12cqrrb1rrbmzafm")))

(define-public crate-miraland-sdk-macro-1.14.19 (c (n "miraland-sdk-macro") (v "1.14.19") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17zzcriby1g5cam27k53vk00xi0551hyjihyr9l191rdvlqajzaf")))

(define-public crate-miraland-sdk-macro-1.18.0 (c (n "miraland-sdk-macro") (v "1.18.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mxksmgy2gxr10jq8vvzjn0d257qij0rb6xx03n040c14rj5l0ny")))

(define-public crate-miraland-sdk-macro-1.18.1 (c (n "miraland-sdk-macro") (v "1.18.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07klfvd6qp56spsm6p4ak8rnjqakllf82fvcvsxz2zgf1pf1zyw2")))

(define-public crate-miraland-sdk-macro-1.18.2 (c (n "miraland-sdk-macro") (v "1.18.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r43ivv6lq2q31g6aj6ar6y7b1xkr1q3396c3kpqc6mqspl68dzf")))

(define-public crate-miraland-sdk-macro-1.18.3 (c (n "miraland-sdk-macro") (v "1.18.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11lr8im47jzq7spzbl4spb3j4cyypabcqlqfz67zg8ikjfxr17xq")))

