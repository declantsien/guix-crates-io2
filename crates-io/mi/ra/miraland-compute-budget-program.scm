(define-module (crates-io mi ra miraland-compute-budget-program) #:use-module (crates-io))

(define-public crate-miraland-compute-budget-program-1.14.17-rc3 (c (n "miraland-compute-budget-program") (v "1.14.17-rc3") (d (list (d (n "miraland-program-runtime") (r "=1.14.17-rc3") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc3") (d #t) (k 0)))) (h "0vl9bwj4xcmy5hjhc49pmg0b8320cacxi6sg4f396y6y6mz8jy6s")))

(define-public crate-miraland-compute-budget-program-1.14.17-rc4 (c (n "miraland-compute-budget-program") (v "1.14.17-rc4") (d (list (d (n "miraland-program-runtime") (r "=1.14.17-rc4") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 0)))) (h "0vy6cjp4ks5n4lgn9p8njwdn9dmra3agy4r51mfdmwdzbhiaplfw")))

(define-public crate-miraland-compute-budget-program-1.14.17-rc5 (c (n "miraland-compute-budget-program") (v "1.14.17-rc5") (d (list (d (n "miraland-program-runtime") (r "=1.14.17-rc5") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 0)))) (h "1mczfhhnihcwqyp8pfygh8p07xirrphmlw71bsz3aj56ady7ai7r")))

(define-public crate-miraland-compute-budget-program-1.14.17-rc6 (c (n "miraland-compute-budget-program") (v "1.14.17-rc6") (d (list (d (n "miraland-program-runtime") (r "=1.14.17-rc6") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc6") (d #t) (k 0)))) (h "1w1bn2fgals7w2806kwnxvfzfkckw8slfph8swylx4r87cq2pd10")))

(define-public crate-miraland-compute-budget-program-1.14.17 (c (n "miraland-compute-budget-program") (v "1.14.17") (d (list (d (n "miraland-program-runtime") (r "^1.14.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17") (d #t) (k 0)))) (h "07vrbcdm08m3h5ajbh8iqsy4kpsx946na9s8vy73dd9bdgb1bygn")))

(define-public crate-miraland-compute-budget-program-1.14.18 (c (n "miraland-compute-budget-program") (v "1.14.18") (d (list (d (n "miraland-program-runtime") (r "^1.14.18") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 0)))) (h "1s1kswg9hv6crfl1awv6z3jyz5fv9mgbvzjhl9fy51nz57pjpch0")))

(define-public crate-miraland-compute-budget-program-1.14.19 (c (n "miraland-compute-budget-program") (v "1.14.19") (d (list (d (n "miraland-program-runtime") (r "^1.14.19") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.19") (d #t) (k 0)))) (h "1szayfawiv95qnfz5h36q3czdj6xckps9cbwh22l6ybmm3jfd0z8")))

(define-public crate-miraland-compute-budget-program-1.18.0 (c (n "miraland-compute-budget-program") (v "1.18.0") (d (list (d (n "miraland-program-runtime") (r "=1.18.0") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "0485sj72whghx3zb5srf76iyjmnch8sq0zjz80xk95jxp2mzigqd")))

(define-public crate-miraland-compute-budget-program-1.18.1 (c (n "miraland-compute-budget-program") (v "1.18.1") (d (list (d (n "miraland-program-runtime") (r "=1.18.1") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "0jdb27lsxn6ij51jn69srz8fb8am4i4zhz9ind332dj4gbhj57q0")))

(define-public crate-miraland-compute-budget-program-1.18.2 (c (n "miraland-compute-budget-program") (v "1.18.2") (d (list (d (n "miraland-program-runtime") (r "=1.18.2") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "0b3as2l9899ygr1wzgn69s5jbjnv5rq0zd9arfx5603ys8xwagkx")))

(define-public crate-miraland-compute-budget-program-1.18.3 (c (n "miraland-compute-budget-program") (v "1.18.3") (d (list (d (n "miraland-program-runtime") (r "=1.18.3") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "1b9hj2yw8zd6jqaac2x0lwgqrj5f5ksm0mxysch9jcds9fqzqxj6")))

