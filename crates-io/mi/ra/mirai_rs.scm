(define-module (crates-io mi ra mirai_rs) #:use-module (crates-io))

(define-public crate-mirai_rs-0.1.0 (c (n "mirai_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.19") (f (quote ("full"))) (d #t) (k 0)))) (h "07qjwa3vnl6g17plj07wssciv3ps181ccpffbrkd4myjmmfm886n")))

(define-public crate-mirai_rs-0.1.1 (c (n "mirai_rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1kb1g659mgbxybir0ljxpsi3c2yrkf9n33fkmvbcq0kck3iip7hi")))

