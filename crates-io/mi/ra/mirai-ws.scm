(define-module (crates-io mi ra mirai-ws) #:use-module (crates-io))

(define-public crate-mirai-ws-0.0.1 (c (n "mirai-ws") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.16") (f (quote ("async-await" "sink" "std"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread" "macros" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (d #t) (k 0)))) (h "1bxc9gmh711w42n08ipb6i4bvxx0zmyhvqzk730lk8jpf6a5766d")))

(define-public crate-mirai-ws-0.0.2 (c (n "mirai-ws") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.16") (f (quote ("async-await" "sink" "std"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread" "macros" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (d #t) (k 0)))) (h "100d1rkvri4j3qiq4p04fl3wqdzx7r2rdj7ygna5gs3f7vcccl51")))

