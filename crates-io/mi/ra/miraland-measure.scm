(define-module (crates-io mi ra miraland-measure) #:use-module (crates-io))

(define-public crate-miraland-measure-1.14.17-rc1 (c (n "miraland-measure") (v "1.14.17-rc1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc1") (d #t) (k 0)))) (h "0lj07ihh4pwgqdvh8nzb500q6f9jspf8xg9igi9k7yyd4kn362y1") (y #t)))

(define-public crate-miraland-measure-1.14.17-rc2 (c (n "miraland-measure") (v "1.14.17-rc2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc2") (d #t) (k 0)))) (h "0924y8nnayrnkmzlz9yjl48fp4lxas3cy8lrj817yqf67zvcsfrd")))

(define-public crate-miraland-measure-1.14.17-rc3 (c (n "miraland-measure") (v "1.14.17-rc3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc3") (d #t) (k 0)))) (h "11a3jm1b4896ig96yk2irn6xbpblqc4jrw6ab8s55n0h83p2n8p7")))

(define-public crate-miraland-measure-1.14.17-rc4 (c (n "miraland-measure") (v "1.14.17-rc4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc4") (d #t) (k 0)))) (h "1mr3zhyl34cf33c006685fazfi0166qsrr87gbcd0vnqrlgci8y9")))

(define-public crate-miraland-measure-1.14.17-rc5 (c (n "miraland-measure") (v "1.14.17-rc5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc5") (d #t) (k 0)))) (h "0ps1y3zzm2sspg28ivsdm0lddga0g8rb0db016dday19cv2n1sgg")))

(define-public crate-miraland-measure-1.14.17-rc6 (c (n "miraland-measure") (v "1.14.17-rc6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.14.17-rc6") (d #t) (k 0)))) (h "0fh03jlq5581pi115mkkbc2lcr5wlf7810lfdv7hk2bdxkhb7mh3")))

(define-public crate-miraland-measure-1.14.17 (c (n "miraland-measure") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.17") (d #t) (k 0)))) (h "079fmxwa6w8dvhx62vzj75j4xw44anc7s3zv3xn0v5n2zhjx97yl")))

(define-public crate-miraland-measure-1.14.18 (c (n "miraland-measure") (v "1.14.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.18") (d #t) (k 0)))) (h "0nny19nnbwsh67xngzn4qydrgmns2vhi7q7x2c7lx8lh88l0lw63")))

(define-public crate-miraland-measure-1.14.19 (c (n "miraland-measure") (v "1.14.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miraland-sdk") (r "^1.14.19") (d #t) (k 0)))) (h "0wmskmnnm5fbi6l6p7h921qw2c4b53wfza0wychzb83hfals292y")))

(define-public crate-miraland-measure-1.18.0 (c (n "miraland-measure") (v "1.18.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.0") (d #t) (k 0)))) (h "1b6cpk68cw52nwd7ck6gkgkhkwzgdpis7ymdplbpll7vckc63im4")))

(define-public crate-miraland-measure-1.18.1 (c (n "miraland-measure") (v "1.18.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.1") (d #t) (k 0)))) (h "1f8gghy64549mxzpv1gbz8ka1779qw55z9llgg500sj94xzl6nr6")))

(define-public crate-miraland-measure-1.18.2 (c (n "miraland-measure") (v "1.18.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.2") (d #t) (k 0)))) (h "10095bd46v6lxg1482qsvwch1smgyc8fz50d75nr44bcch9mj8wy")))

(define-public crate-miraland-measure-1.18.3 (c (n "miraland-measure") (v "1.18.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miraland-sdk") (r "=1.18.3") (d #t) (k 0)))) (h "0b1gcz27pbsiys2sj75q00qzaxag0yjrngi7xg6anmgmlfb2sfx7")))

