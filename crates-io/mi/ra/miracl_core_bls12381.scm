(define-module (crates-io mi ra miracl_core_bls12381) #:use-module (crates-io))

(define-public crate-miracl_core_bls12381-0.4.1 (c (n "miracl_core_bls12381") (v "0.4.1") (h "1bhbjil5nx066bzw5wyql86s8f0s94gy1sfjnrr4xihczky0kkf9") (y #t)))

(define-public crate-miracl_core_bls12381-4.1.0 (c (n "miracl_core_bls12381") (v "4.1.0") (h "05gisnq8gbzjizjb3ysz4wxz35f7jp5br851g562p10sky4ggxq9")))

(define-public crate-miracl_core_bls12381-4.1.1 (c (n "miracl_core_bls12381") (v "4.1.1") (h "09n1vjc2xf1x0yj5j2j9q2fcikl51f005py3gqsz062kwwb8wyd3")))

(define-public crate-miracl_core_bls12381-4.1.2 (c (n "miracl_core_bls12381") (v "4.1.2") (h "071cg81z7ciqnaalqqk53jdspzb5sc3r0b10prshgq4r2i3i6p7b") (f (quote (("std") ("default" "std"))))))

(define-public crate-miracl_core_bls12381-4.2.2 (c (n "miracl_core_bls12381") (v "4.2.2") (d (list (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.67") (o #t) (d #t) (k 0)))) (h "03sh9d1y1qhldp5iyc6cc5ahp4jdq87y1f1gb3gl3pd8w91bwz6h") (f (quote (("std") ("fallback_separator") ("default" "std" "fallback_separator") ("allow_alt_compress"))))))

