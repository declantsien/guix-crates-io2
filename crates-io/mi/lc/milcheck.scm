(define-module (crates-io mi lc milcheck) #:use-module (crates-io))

(define-public crate-milcheck-0.1.0 (c (n "milcheck") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0150xzg2c876i05d2gk67bmqq255vwswhb3zpinjbx1ilhq3p6zm")))

(define-public crate-milcheck-0.1.1 (c (n "milcheck") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0vny041kr6mwqr28vinfps14l7vnqhn8144vh37ayln4blcb1x19")))

(define-public crate-milcheck-0.1.2 (c (n "milcheck") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0ly563gxgpqyj89gaabg5hi2v6j7fzxawjmi34i1kifl0g9kwbpw")))

(define-public crate-milcheck-0.1.3 (c (n "milcheck") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1ifliymsmn6s19xg3g11dcd9i7028nppsvdxlq440arzfkxamc9a")))

(define-public crate-milcheck-0.1.4 (c (n "milcheck") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "19pggij06piflhiq1smd147kr5sqcpdml2s53f2w56h0ji09mbdx")))

(define-public crate-milcheck-0.1.5 (c (n "milcheck") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1nxvnhblh1011r9nhpgb7xffc32gsa2ichzabxssj5c1mzyzq5zd")))

(define-public crate-milcheck-0.1.6 (c (n "milcheck") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "01sk6m32bgx4d5swv4g6yrv5d2vb4da69w1h4jvc243zrvgi9vx2")))

(define-public crate-milcheck-0.1.7 (c (n "milcheck") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1fdn4j26x1mdzxs2bm1fpggi2s72hmiak96w1lpv2y7w3kqw6s72")))

(define-public crate-milcheck-0.1.8 (c (n "milcheck") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0jb33lbj5plyqp6yz5g4gsnb572vnffbziklxdnxqkgcfiqzywii")))

(define-public crate-milcheck-0.1.9 (c (n "milcheck") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "14hwad7a1ac0j8l8lijx7x55an9imj4kcdxj4i4i8q80isf2bhwn")))

(define-public crate-milcheck-0.2.0 (c (n "milcheck") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1ha95d1g5n4wis0kwyi5wj8b010gf2l0l8rpimfnw7algj3lf1qc")))

(define-public crate-milcheck-0.2.1 (c (n "milcheck") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1mbgilh5v06wvs6517qxlxq46aq40k3ilag5mw6f1nz4pqxgg788")))

(define-public crate-milcheck-0.2.2 (c (n "milcheck") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1h77cxm054bdxvi8nliyqll7y2y8ynbyaj837ph4854253frmwd0")))

(define-public crate-milcheck-0.2.5 (c (n "milcheck") (v "0.2.5") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "07n06zngd7qdfb7gxp22rq1jfxappan5xihz87x3x3361sin89zv")))

(define-public crate-milcheck-0.2.6 (c (n "milcheck") (v "0.2.6") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1bi1g8sz5isbznrpbfdhjjaaixjpi3rph7g6gay522vk2i5gwd6y")))

