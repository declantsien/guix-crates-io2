(define-module (crates-io mi et miette-derive) #:use-module (crates-io))

(define-public crate-miette-derive-0.1.0 (c (n "miette-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1wxh3ibz7cpy6d65gxgmfpj6if945amqa0054sip5h99gcbny6a5")))

(define-public crate-miette-derive-0.7.0 (c (n "miette-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0y1jr027vcdbk4p6khvm88d9davbph5a5r81kqhx4ylwyprd8cvv")))

(define-public crate-miette-derive-0.8.0 (c (n "miette-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "15lm0r97sqjsp1rppkcddjnhczg4v6gglykpa9gs3ia2q7fla0jc")))

(define-public crate-miette-derive-0.9.0 (c (n "miette-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "046cd239d0ly85pgm0b403a0pp0w5v37jja556m82q3kl1wyrwwh")))

(define-public crate-miette-derive-0.10.0 (c (n "miette-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0p8alizqhgi1g8gxgi6xnhxjkfa8vb4kg6riaizpwkzwrg2yag42")))

(define-public crate-miette-derive-0.11.0 (c (n "miette-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0vl9jcjnlgf52j8w0sjw267gn1gbvwfg0dadr5pg1d1xj250shwq")))

(define-public crate-miette-derive-0.12.0 (c (n "miette-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1jnhcziy9d5d8r1g8dsvbdhlw8yynrn9h2fqk6h8v0g3k8kc491r")))

(define-public crate-miette-derive-0.13.0 (c (n "miette-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0qq3j28810r825n47rzl88p9pdnl80p7k6axc87pxc2xw89xp127")))

(define-public crate-miette-derive-1.0.0-beta.1 (c (n "miette-derive") (v "1.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1r2x90zra5qwn05c2b4x6slqnapyzr2k93f0by121pnc9v8wsrvb")))

(define-public crate-miette-derive-1.0.0 (c (n "miette-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "16kfk6hyhkqxk1ddh4grvaxk0rqmnn4avlzl0662pqgzn36r1rch")))

(define-public crate-miette-derive-1.0.1 (c (n "miette-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1s6y9bsx6795834nw16mdp62pwyaryycw6c8rnvj9fss33bmcic0")))

(define-public crate-miette-derive-1.1.0 (c (n "miette-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0r0hm3y545xy123zgznknxv6l4kb8fm49m5rxx20dn7bv7zn8bbg")))

(define-public crate-miette-derive-2.0.0 (c (n "miette-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0m6jzhranq91q9mwix7vmd7cx2cajx38ny5f2hckinlarxpvyxx4")))

(define-public crate-miette-derive-2.1.0 (c (n "miette-derive") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0fkipihyw43irvq2kvjdl24da5yzq7k7snls5bsjr0a3j73ndxq9")))

(define-public crate-miette-derive-2.1.1 (c (n "miette-derive") (v "2.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1zlh1p1fmy813lkqyx1xq6mqd7sm31yakd1i63889zd9mldh3l9y")))

(define-public crate-miette-derive-2.1.2 (c (n "miette-derive") (v "2.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0z518i4hraz808c6hgszmiyj9xmzyxil0h8rfcqi1c4l3m1cij93")))

(define-public crate-miette-derive-2.2.0 (c (n "miette-derive") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "19w948z1yj5naxi3wn3n4bfffxd3ja7zcm18aplhnbl2z8mxns6s")))

(define-public crate-miette-derive-3.0.0-alpha.0 (c (n "miette-derive") (v "3.0.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "009awakrvys87b65zpkw7l0x0hwgmfr43547v9i81qhv17xjfc6v")))

(define-public crate-miette-derive-3.0.0-beta.0 (c (n "miette-derive") (v "3.0.0-beta.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "07n6z1g2najxqpx3zb2dnq2pfgnbfnm0yf0ljd7zw9m58r274cv0")))

(define-public crate-miette-derive-3.0.0 (c (n "miette-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "18w75rn7dwrggs60c5sf0clb3f285g7737d59c6lhlpb8wrwv9x9")))

(define-public crate-miette-derive-3.0.1 (c (n "miette-derive") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "17rl06b6hxcav36nzbnwm75zw95p1zcvi0jxxxnm2ahpi6fcsjh7")))

(define-public crate-miette-derive-3.1.0 (c (n "miette-derive") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1mk6aimqzvsj3vshsvlmnpq3p5i4nrbdgzv5cvrfbkf93fc3mrhf")))

(define-public crate-miette-derive-3.2.0 (c (n "miette-derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0zjv009zzw6z4scslyjb0laz0z2379c2a89jhrz9z6lvk5phn3rc")))

(define-public crate-miette-derive-3.3.0 (c (n "miette-derive") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0zznn57w5qq6iw75bg2xvp3f91qzff3bcswmhz76glqj2fvah0aw")))

(define-public crate-miette-derive-4.0.0 (c (n "miette-derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0cppniq7wdxzqlnqd1snx5xgyi0xvx85kxprjrlgbip4yb4k8ymh")))

(define-public crate-miette-derive-4.0.1 (c (n "miette-derive") (v "4.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1c65lji41nlcnww4r9rkf2hy01k5i38mbpsava4da48ng2gkzsrk")))

(define-public crate-miette-derive-4.1.0 (c (n "miette-derive") (v "4.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1hpy3sf23wvl88dn2yq3d839axxh1cialwsvchvhiypv29l3q3m9")))

(define-public crate-miette-derive-4.2.0 (c (n "miette-derive") (v "4.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "10bipvxqpi36s50x72flhpgpjw1cfx2qpl3pji9ybx6c6rd65rzx")))

(define-public crate-miette-derive-4.2.1 (c (n "miette-derive") (v "4.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "18rs61i4a2pcv7gxc2qmmhdmvz6pgq4clc2szdry9jjj9y6v4iy5")))

(define-public crate-miette-derive-4.3.0 (c (n "miette-derive") (v "4.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1x0zz5kqvzikx95h80hg4w9jrxjq9lkbnmi5k4bc7670kcl2h3al")))

(define-public crate-miette-derive-4.4.0 (c (n "miette-derive") (v "4.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0zajmla9inx4lpls4i3pm4s6zy89vajfi1l2capzja5ws145maa5")))

(define-public crate-miette-derive-4.5.0 (c (n "miette-derive") (v "4.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "087g2xzqd6hbcar8zfci3jxh97hgc1xvnjbqaffik5717s5606bi")))

(define-public crate-miette-derive-4.6.0 (c (n "miette-derive") (v "4.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0ixj6zd7vwz286ljg1il7i7746j5l9r0z5h2zp3lzvqr0zi5ckg0")))

(define-public crate-miette-derive-4.7.0 (c (n "miette-derive") (v "4.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1az8izz111ivkn28jjzda4i8i6ir3wi22303jicrxy0vny2csh1j")))

(define-public crate-miette-derive-4.7.1 (c (n "miette-derive") (v "4.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0k0ph38lxzqamaabind8463j2k5qjg6jhhbcdrg1pkqvfrdw8nvb")))

(define-public crate-miette-derive-5.0.0 (c (n "miette-derive") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "18bf0ccll8f74ird1ksi9b4bbwwp0kyhdw6zf4gc97yib9z17dys")))

(define-public crate-miette-derive-5.1.0 (c (n "miette-derive") (v "5.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0r6alffw1x4vzb6l6wgzmwa1cc20vncr4c8xz406ci652pm37z7x")))

(define-public crate-miette-derive-5.1.1 (c (n "miette-derive") (v "5.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "19lhb41sv51nr0ryjk3ycqmcax0w2ayaxkc7svjdxpk6fay98ra2")))

(define-public crate-miette-derive-5.2.0 (c (n "miette-derive") (v "5.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "155xh1r4jv8h6zmjicz6aqm0cy6i4fmislhqr6shm7yc7g3i139w")))

(define-public crate-miette-derive-5.3.0 (c (n "miette-derive") (v "5.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "028yzm63dald42w6vfbb1jyhk5j3hxivhglfj820gs0zgpnqa92g")))

(define-public crate-miette-derive-5.3.1 (c (n "miette-derive") (v "5.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1jbn8qrrzwgk8d83lvbak190ixh2wckpp2fcdmjdnamr5ks2wyny") (y #t)))

(define-public crate-miette-derive-5.4.0 (c (n "miette-derive") (v "5.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0sxc4ikd3f6wzp8sxd11si2sgm8q9idj1qwc4wxk2c0bpq8nr3g4")))

(define-public crate-miette-derive-5.4.1 (c (n "miette-derive") (v "5.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0d9qybkzd814x4lxkcl6p64xqbwf5dgmdb5hkqqdqhsxxvnihzc2")))

(define-public crate-miette-derive-5.5.0 (c (n "miette-derive") (v "5.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0adsrkp8i62h1jcvk40b5dv966jvcf3qllcbbk584lmcnwd41hlp")))

(define-public crate-miette-derive-5.6.0 (c (n "miette-derive") (v "5.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0q7lbd65xsyc96nyzvwbfyqsmja9kg2dfhnb8jxr46qdm29ss1ra")))

(define-public crate-miette-derive-5.7.0 (c (n "miette-derive") (v "5.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "076yhw7hkyxnshhd308sdzl47dvjczs20dzx7l0l754k4cprfhl8")))

(define-public crate-miette-derive-5.8.0 (c (n "miette-derive") (v "5.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "0ls1hdsnx6afpzhknzmai73axcg127jvx54kd7kcr6va30jwcrac")))

(define-public crate-miette-derive-5.9.0 (c (n "miette-derive") (v "5.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "1yjw5f8n8i6cf6j8sayhjjjl3fi36956ap2nciwv7pa43lg7f0a9")))

(define-public crate-miette-derive-5.10.0 (c (n "miette-derive") (v "5.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "0p33msrngkxlp5ajm8nijamii9vcwwpy8gfh4m53qnmrc0avrrs9")))

(define-public crate-miette-derive-6.0.0 (c (n "miette-derive") (v "6.0.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "1ml8xvirc5i2fnagzb2cba2gj7jin23d76bsxxv32lx5y047341m")))

(define-public crate-miette-derive-6.0.1 (c (n "miette-derive") (v "6.0.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "001820vd3hdcd98ic8lrmygwczgxyhrkqv5wg75cp16xl3r25rki")))

(define-public crate-miette-derive-7.0.0 (c (n "miette-derive") (v "7.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0xyfbk75gyd6wiarkkmkk9qczpyl82s9wj47z0y4p8qly5myz797")))

(define-public crate-miette-derive-7.1.0 (c (n "miette-derive") (v "7.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1v2i022h8vrmkrk40n216jip5r32big2ywpf2b1cdawq9zsw60gk")))

(define-public crate-miette-derive-7.2.0 (c (n "miette-derive") (v "7.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0v4sczg1n88arqwilhir1g97y5vsq9zjmpxn8v1ni05czaprrw6w")))

