(define-module (crates-io mi ss miss-demeanor-pluginutils) #:use-module (crates-io))

(define-public crate-miss-demeanor-pluginutils-0.1.0 (c (n "miss-demeanor-pluginutils") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0pqvlx6cmh8j9czmmfc3sjmcj18fh8cxzv89yk5jv61pyi8vr6z8")))

(define-public crate-miss-demeanor-pluginutils-0.2.0 (c (n "miss-demeanor-pluginutils") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "12gn3rm9h4d0ymgyjqfdc2vikys56265g93m1mgz33a0l04akd6s")))

(define-public crate-miss-demeanor-pluginutils-0.3.0 (c (n "miss-demeanor-pluginutils") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "05k64ki4dsk9l3793zxzrb1fgmfnhawm3yihymxvn1fddqpc6lys")))

