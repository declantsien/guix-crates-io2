(define-module (crates-io mi ss misspeller) #:use-module (crates-io))

(define-public crate-misspeller-0.0.1 (c (n "misspeller") (v "0.0.1") (h "17npi87lggj95gg3y8vp83ddy45h1dzwp8sy56w80i88687g9v7q") (y #t)))

(define-public crate-misspeller-0.0.2 (c (n "misspeller") (v "0.0.2") (h "1zjzw7wlngc2g3gddwbwsqhv6warb815gm6inx1xx17v27p581sc") (y #t)))

(define-public crate-misspeller-0.0.3 (c (n "misspeller") (v "0.0.3") (h "1cmjq994wyh7vwpy6cii8yx1b8j580x0m9fs6x1hrg8fi5c5adrn") (y #t)))

(define-public crate-misspeller-0.0.4 (c (n "misspeller") (v "0.0.4") (h "1ir6p621d1306w36cjiqqix2pfazhj420cx421rj1lrmf8mc7rq4") (y #t)))

(define-public crate-misspeller-0.0.5 (c (n "misspeller") (v "0.0.5") (h "1ymmigjgw76cxzsykmk779dbbc4iik4cnswbaj5l9n3nbbhclwhz") (y #t)))

(define-public crate-misspeller-0.0.6 (c (n "misspeller") (v "0.0.6") (h "0a9pypdqmh4dab5flsi2pch6jbdd9h6kzwssrvjyzzyi3lif4i4b") (y #t)))

(define-public crate-misspeller-0.0.7 (c (n "misspeller") (v "0.0.7") (h "0hz4wfv6xnbkp6v7yg8f2mz1r6z8bcan1h77azj32993ycm5xcvn") (y #t)))

(define-public crate-misspeller-0.0.8 (c (n "misspeller") (v "0.0.8") (h "1lzyarv6j5w7wsd9hrm2w4sg92y1dpkvhjrqdqyr63mgql98vqln") (y #t)))

(define-public crate-misspeller-0.0.9 (c (n "misspeller") (v "0.0.9") (h "109xjqkg82bf8b2dk021hd37wia5xnfi5s8i5gplfy064lwbapnw") (y #t)))

(define-public crate-misspeller-0.0.10 (c (n "misspeller") (v "0.0.10") (h "1mlmvnwgq9czyrwn8dxgf84wagah3n1dawi9dvxqn61xrswvphwg") (y #t)))

(define-public crate-misspeller-0.0.11 (c (n "misspeller") (v "0.0.11") (d (list (d (n "cjk") (r "^0.0.7") (d #t) (k 0)))) (h "05pa4gz5c2lrxqfk3li5xz9z5zbybf0gr3m1zxfhwlmn4mqq3ssx") (y #t)))

(define-public crate-misspeller-0.0.12 (c (n "misspeller") (v "0.0.12") (d (list (d (n "cjk") (r "^0.0.7") (d #t) (k 0)))) (h "10byqbyxpj07al781l33199xr2jbnsjpsai1qdgi57gdymbmsm7y") (y #t)))

(define-public crate-misspeller-0.0.13 (c (n "misspeller") (v "0.0.13") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "0ic26fkn4g2q3whngv0rapw9qlw3h5703nrn2gsjailhc7p5v3q0") (y #t)))

(define-public crate-misspeller-0.0.14 (c (n "misspeller") (v "0.0.14") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "0dv4bhl1p81drgag0labs6dm6x3wsdxj2ny2bx0jdydsdzqknynb") (y #t)))

(define-public crate-misspeller-0.1.0 (c (n "misspeller") (v "0.1.0") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "08c6cih2g0sczzd8h2hxcqxm12hmkkh0dar7w18mjhp1mdmlmiyg") (y #t)))

(define-public crate-misspeller-0.1.1 (c (n "misspeller") (v "0.1.1") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "0ls6gflaynipvm73ny6lpsmhnmbq6a7pww8v0svhd747lsx5nsqm") (y #t)))

(define-public crate-misspeller-0.1.2 (c (n "misspeller") (v "0.1.2") (d (list (d (n "cjk") (r "^0.1") (d #t) (k 0)))) (h "16ldbcqf2rykis8dwqb5407wc5v9n45zi3lr58icsawqdj3bbpxs") (y #t)))

