(define-module (crates-io mi ss misspell) #:use-module (crates-io))

(define-public crate-misspell-0.1.0 (c (n "misspell") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0sqdmzwha32wxw213y62b9w69ly73p1h8a2kih8r7clq1lfmnlcq")))

(define-public crate-misspell-0.1.1 (c (n "misspell") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "1hbjhhqx805jizjm1mp1c15fpn40z6f8dkpvcfzz2lp1f3qlid01")))

(define-public crate-misspell-0.1.2 (c (n "misspell") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0yxaingdy1c1ny2pn7zcqcbdz1p0k919vq1wawhq0irx26n69qql")))

(define-public crate-misspell-0.1.3 (c (n "misspell") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "119h5829by6liz67myqzbsprx5mqwc78xcsxswypla3kqqbb5gnh")))

(define-public crate-misspell-0.2.0 (c (n "misspell") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0aabck8q88rlx4dfk5l2nsaf034xbaryzf0lsif7va2f99wlz88m")))

