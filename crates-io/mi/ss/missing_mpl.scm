(define-module (crates-io mi ss missing_mpl) #:use-module (crates-io))

(define-public crate-missing_mpl-0.1.0 (c (n "missing_mpl") (v "0.1.0") (d (list (d (n "strsim") (r "^0.7.0") (d #t) (k 0)))) (h "1x0mw3a9bghykr4z7zc441aqa6wxzq02xpfrr37pij7nlp0qzhsr")))

(define-public crate-missing_mpl-0.1.1 (c (n "missing_mpl") (v "0.1.1") (d (list (d (n "strsim") (r "^0.7.0") (d #t) (k 0)))) (h "0ncvvbm0cpxzv992f9qlphsvn4sjclqcc9p77yhjxknlrd45zxak")))

(define-public crate-missing_mpl-0.1.2 (c (n "missing_mpl") (v "0.1.2") (d (list (d (n "strsim") (r "~0.8") (d #t) (k 0)))) (h "0prxmn46ldvj12ilmf62khbcs9nxdqv5i1smapl56n6yh4jh3504")))

(define-public crate-missing_mpl-0.2.0 (c (n "missing_mpl") (v "0.2.0") (d (list (d (n "strsim") (r "~0.10") (d #t) (k 0)))) (h "1jdxf4ldfp3a497shavzaxvzhygsq5pxpr5qqy4gb67z45scv01h")))

