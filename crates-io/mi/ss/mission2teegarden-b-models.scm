(define-module (crates-io mi ss mission2teegarden-b-models) #:use-module (crates-io))

(define-public crate-mission2teegarden-b-models-0.1.0 (c (n "mission2teegarden-b-models") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (k 0)) (d (n "strum") (r "^0.25.0") (k 0)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 0)))) (h "1sdf58g584950fqjsl30lkla1zy8jf704h5qgq6nsqa3agxsqbqw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70")))

(define-public crate-mission2teegarden-b-models-0.2.0 (c (n "mission2teegarden-b-models") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (k 0)) (d (n "strum") (r "^0.25.0") (k 0)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 0)))) (h "0chpqgiz236v1ys0m19yz5rma10s35wa9zvrg1f2sw0v6v3l9yva") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70")))

