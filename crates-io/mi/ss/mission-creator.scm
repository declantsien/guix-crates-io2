(define-module (crates-io mi ss mission-creator) #:use-module (crates-io))

(define-public crate-mission-creator-0.1.0 (c (n "mission-creator") (v "0.1.0") (d (list (d (n "gtk") (r "^0.7.3") (f (quote ("v4_12"))) (d #t) (k 0) (p "gtk4")))) (h "1zah91gzyssimigi8xw8cabm0rx66yx2a1zfxwcm91dch4bg1nkx")))

(define-public crate-mission-creator-0.2.0 (c (n "mission-creator") (v "0.2.0") (d (list (d (n "gtk") (r "^0.7.3") (f (quote ("v4_12"))) (d #t) (k 0) (p "gtk4")))) (h "16jdgsly1wcxall0g1h5p1ci7zfm3plfzgpn4f7qsdgsdgpfbl67")))

