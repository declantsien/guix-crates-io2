(define-module (crates-io mi ss misskey-core) #:use-module (crates-io))

(define-public crate-misskey-core-0.1.0 (c (n "misskey-core") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07ga56sh2gg590cdfnfykafi2p4r7wsk82s5mnw8amgrvs6v80hy")))

(define-public crate-misskey-core-0.2.0 (c (n "misskey-core") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bs1rdw951s5grka9ci40mwa76n814kfcxcdsd6ddm9wkpk05iy4")))

(define-public crate-misskey-core-0.3.0-rc.1 (c (n "misskey-core") (v "0.3.0-rc.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w10ca4z7xlqmhlk9qd0ignqqhi01cc5rmyy6yph6lmbw82knb5q")))

