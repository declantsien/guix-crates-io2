(define-module (crates-io mi ku miku-macros) #:use-module (crates-io))

(define-public crate-miku-macros-0.1.0 (c (n "miku-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0jy1ivwq1bxh3g0lv6kp0r1symsjz8aybjzh7d3q9j6cz81v2nkg")))

(define-public crate-miku-macros-0.1.1 (c (n "miku-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "11k392k3dnf46qiva98cv47phpsdd6nd58rj54936ml31nlkc223")))

(define-public crate-miku-macros-0.1.2 (c (n "miku-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0qpkpxs43rl45zwx668gpgg3bclc56asgx4l78jkj848hwh36yzv")))

(define-public crate-miku-macros-0.1.3 (c (n "miku-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0wzzycbpm40hm2yyilbavx6pf7wlivkidq3d7b0y6lryrl652h3p")))

