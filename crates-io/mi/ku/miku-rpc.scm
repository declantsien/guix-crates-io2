(define-module (crates-io mi ku miku-rpc) #:use-module (crates-io))

(define-public crate-miku-rpc-0.1.0 (c (n "miku-rpc") (v "0.1.0") (d (list (d (n "epoll-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "miku-macros") (r "^0.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "17lvgm47lr37ajzbykq611b6cv9132i037mvmxsgi65jnpa2grcs")))

(define-public crate-miku-rpc-0.1.1 (c (n "miku-rpc") (v "0.1.1") (d (list (d (n "epoll-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "miku-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1l1afy032znd8y3sn6gkbgz5b5qvv15nkm3nybcvi5qzk431jwdj")))

(define-public crate-miku-rpc-0.1.2 (c (n "miku-rpc") (v "0.1.2") (d (list (d (n "epoll-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "miku-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0zfv149qfhsy7wgirni8pma17zdk7jyqficm8dlv5zzsg3hrw2kv")))

(define-public crate-miku-rpc-0.1.3 (c (n "miku-rpc") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "epoll-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "miku-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "miniserde-miku") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "08zcacyfcvzy14mkkf0wvqyxlzn02vjnvbkr9gh5yxwz5yigbk1v") (f (quote (("wrappers") ("default" "wrappers"))))))

(define-public crate-miku-rpc-0.1.4 (c (n "miku-rpc") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "epoll-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "miku-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "miniserde-miku") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "19vq22wn2lk3bn5382hy3z2dq02jjayw977q8x895mqvpa5k3z3y") (f (quote (("wrappers") ("default" "wrappers"))))))

