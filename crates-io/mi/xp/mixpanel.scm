(define-module (crates-io mi xp mixpanel) #:use-module (crates-io))

(define-public crate-mixpanel-0.1.0 (c (n "mixpanel") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.15") (d #t) (k 0)) (d (n "hyper") (r "^0.6.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^0.4.2") (d #t) (k 0)))) (h "1bfp54y9d0mpi24qxx2i0jws4nlxb1qgybqyv6wjgcx6xg08psa0")))

(define-public crate-mixpanel-0.1.1 (c (n "mixpanel") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.15") (d #t) (k 0)) (d (n "hyper") (r "^0.6.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^0.4.2") (d #t) (k 0)))) (h "0l52m4lpp3s7sb0db3l3dnzv14jgrp9p2cwzf9hal06rb9m8zyqz")))

