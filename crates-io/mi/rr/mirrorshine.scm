(define-module (crates-io mi rr mirrorshine) #:use-module (crates-io))

(define-public crate-mirrorshine-0.1.0 (c (n "mirrorshine") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "14dihmslfi7npbg73xpqp85jwbi3iwv20h5jr61fjyrapyfbrjf0")))

