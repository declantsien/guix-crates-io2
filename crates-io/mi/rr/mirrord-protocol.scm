(define-module (crates-io mi rr mirrord-protocol) #:use-module (crates-io))

(define-public crate-mirrord-protocol-0.0.1 (c (n "mirrord-protocol") (v "0.0.1") (d (list (d (n "actix-codec") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0sn9ajrnrqz4y9z6sq7ky9p9imvcps09c03qnazzly4fvw2qypaj")))

(define-public crate-mirrord-protocol-0.1.0 (c (n "mirrord-protocol") (v "0.1.0") (d (list (d (n "actix-codec") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0xzrgld97sivci240fq2gmynwak62yn9hm8gp2d368yd1wj51w4h")))

(define-public crate-mirrord-protocol-0.2.0 (c (n "mirrord-protocol") (v "0.2.0") (d (list (d (n "actix-codec") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "08f8glavyyd5mcxrp5q6qjnh5qlv56g3ilhn31n0cpw6x6x7pzpv")))

