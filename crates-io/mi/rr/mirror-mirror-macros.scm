(define-module (crates-io mi rr mirror-mirror-macros) #:use-module (crates-io))

(define-public crate-mirror-mirror-macros-0.1.0 (c (n "mirror-mirror-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "07dl53kxdhx1y7kg8xs43vwmyscz2blka7gd9xp3g1acn6yalp60") (r "1.65")))

(define-public crate-mirror-mirror-macros-0.1.1 (c (n "mirror-mirror-macros") (v "0.1.1") (d (list (d (n "mirror-mirror") (r "^0.1") (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1bh8c1vmarn3ppr9lwcwx7782lw4b20lqccsa77sjp9yhflkfd6r") (r "1.65")))

(define-public crate-mirror-mirror-macros-0.1.2 (c (n "mirror-mirror-macros") (v "0.1.2") (d (list (d (n "mirror-mirror") (r "^0.1") (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ijq3q1q5r66337jny9cw76vnrpqp4s9h4vz28y1lla07i289pjp") (r "1.65")))

(define-public crate-mirror-mirror-macros-0.1.3 (c (n "mirror-mirror-macros") (v "0.1.3") (d (list (d (n "mirror-mirror") (r "^0.1") (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "parsing" "visit"))) (d #t) (k 0)))) (h "1xjix0dgajcbqg2ayfcq4cxgfwkbhkl508b42pjkhci0cvg75480") (r "1.65")))

(define-public crate-mirror-mirror-macros-0.1.4 (c (n "mirror-mirror-macros") (v "0.1.4") (d (list (d (n "mirror-mirror") (r "^0.1") (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "parsing" "visit"))) (d #t) (k 0)))) (h "0ccinwgkc1p9himgh8p6d625ai7ww2ib8q5w0z8dwj9jswv82fl7") (r "1.65")))

