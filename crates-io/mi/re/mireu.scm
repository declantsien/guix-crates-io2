(define-module (crates-io mi re mireu) #:use-module (crates-io))

(define-public crate-mireu-0.0.1 (c (n "mireu") (v "0.0.1") (d (list (d (n "gpio-cdev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0lcg3ras2z7gl04z9h5fh1rvpkvnw3zidwxayyqdz6mxj44772i3") (f (quote (("rpi_os" "gpio-cdev")))) (y #t)))

(define-public crate-mireu-0.0.2 (c (n "mireu") (v "0.0.2") (d (list (d (n "gpio-cdev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1ccdnixh57r7ljrkvcg11qs8nd6c22p4m020ylxl48mv9vsv9cnb") (f (quote (("rpi_os" "gpio-cdev")))) (y #t)))

(define-public crate-mireu-0.0.3 (c (n "mireu") (v "0.0.3") (d (list (d (n "gpio-cdev") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0885ysqd2sswnxpmfm908jr6xy30k1pawm4nxwlw1q9sk24ifqb4") (f (quote (("rpi_linux" "gpio-cdev")))) (y #t)))

