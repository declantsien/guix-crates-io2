(define-module (crates-io mi ls milstian-feedback) #:use-module (crates-io))

(define-public crate-milstian-feedback-0.1.0 (c (n "milstian-feedback") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qh52mxfwisn4s9fv5czfgngn6sdqm8qghwfcskd91y5mdvf0bh0")))

(define-public crate-milstian-feedback-0.1.1 (c (n "milstian-feedback") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0dqw4drdmg14qphml0jr4af9137l12d6kak7j79v34h2jvbqr0c4")))

