(define-module (crates-io mi ls milstian-http) #:use-module (crates-io))

(define-public crate-milstian-http-0.1.0 (c (n "milstian-http") (v "0.1.0") (h "1niphs2nd2fi7gbr92lx39hql78nf27r7k5i88vhbi5kxiwzrnim")))

(define-public crate-milstian-http-0.1.1 (c (n "milstian-http") (v "0.1.1") (h "0izxbjmgnkc09mjc4q9il5wasxd5d8n5idajqhgd43r5c7fdc6w3")))

(define-public crate-milstian-http-0.1.2 (c (n "milstian-http") (v "0.1.2") (h "1k9f88mk9680llk2m7sbnm3rq01sgc65hy7hc0lfp42846aqsfzk")))

(define-public crate-milstian-http-0.1.3 (c (n "milstian-http") (v "0.1.3") (h "1c9rskf9iy86sff4vkyjyxdwgbkfh1qcva2i1rxv81n90pfpncq7")))

(define-public crate-milstian-http-0.1.5 (c (n "milstian-http") (v "0.1.5") (h "1i4m8z265lmygh0dmrj3dwrr3zsvh8w4cszppsfss32rid20iqvj")))

(define-public crate-milstian-http-0.1.6 (c (n "milstian-http") (v "0.1.6") (h "16qxbpgc8m9861wrknvvqinyjnrkyni0kj4mhpcbh09xa5yijgd1")))

(define-public crate-milstian-http-0.1.7 (c (n "milstian-http") (v "0.1.7") (h "1sv1nmv3xqcj9fbrcp6nsb6bqh90j111df8mpry1xywyv89mlbdm")))

(define-public crate-milstian-http-0.1.8 (c (n "milstian-http") (v "0.1.8") (h "0v8zkp3wk1wapmj9y2y6vv40hca0sxg9cfa0an7g2c4fkx9l5gim")))

(define-public crate-milstian-http-0.1.9 (c (n "milstian-http") (v "0.1.9") (h "1jn9jfj66dah4dmwmrh7pnsdqri20w4v6cfgn7xlns801y84mb95")))

