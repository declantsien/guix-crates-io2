(define-module (crates-io mi ck mickey) #:use-module (crates-io))

(define-public crate-mickey-0.1.0 (c (n "mickey") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "06gwqjwf75v5yy9kx7k6ji11026isa0lmx80kl644v51drz8jjvf")))

(define-public crate-mickey-0.1.1 (c (n "mickey") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mbas4bzr224pnybps69r6ap15avcwr620yhnnihkd8z3hz30rz")))

