(define-module (crates-io mi ck mick-jaeger) #:use-module (crates-io))

(define-public crate-mick-jaeger-0.1.0 (c (n "mick-jaeger") (v "0.1.0") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "0jzdyqs489p40adfahg54zqyls24lld96zy1gmi8a52xszycmm29")))

(define-public crate-mick-jaeger-0.1.1 (c (n "mick-jaeger") (v "0.1.1") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "1p8wjw3j051kj0hggl6hsrkdh4awfmsmnwlaly55c29kckk53iv8")))

(define-public crate-mick-jaeger-0.1.2 (c (n "mick-jaeger") (v "0.1.2") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "0msran11ivg4mb70yk23n1f992x774d2yyhybb5ylqkfp60v2is2")))

(define-public crate-mick-jaeger-0.1.3 (c (n "mick-jaeger") (v "0.1.3") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "1m47ygx3r7iv12y4d20yn2kax0qbi84y3sv48spclg6ba4z2p60i") (y #t)))

(define-public crate-mick-jaeger-0.1.4 (c (n "mick-jaeger") (v "0.1.4") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "1pypdlllj4558kivdl3p95djc3h7c7yp7xsilhxg7rq9c7qw68y0")))

(define-public crate-mick-jaeger-0.1.5 (c (n "mick-jaeger") (v "0.1.5") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "07v82x8ligal552n5agixpqpk0qqjx9lbr6qkjiknrh533g4q77q")))

(define-public crate-mick-jaeger-0.1.6 (c (n "mick-jaeger") (v "0.1.6") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)))) (h "1bl7qifid7y2zqml9v8mfw049rf64jg2j4dy046ixfv1hjnpz9za")))

(define-public crate-mick-jaeger-0.1.7 (c (n "mick-jaeger") (v "0.1.7") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1fbgyfjq3klhlfd5ncx4ygiva6bq18gr43lbi7q62x756k0jqb7x")))

(define-public crate-mick-jaeger-0.1.8 (c (n "mick-jaeger") (v "0.1.8") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0ch5y8nl9hh6x6j6ahk0x9hgyiqp38zvy3s0s6hyr2hfadhj2rv9")))

