(define-module (crates-io mi xi mixin) #:use-module (crates-io))

(define-public crate-mixin-0.0.0 (c (n "mixin") (v "0.0.0") (h "15piiz86ylnsny95n0sd11qgxk6vnh733jipi2xby5sa6x79m2p7")))

(define-public crate-mixin-0.1.0 (c (n "mixin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "10drwqpf1rw0dp1v8f6s7lv964kv17dfw24rvbyfsxx4pnazyhjv")))

(define-public crate-mixin-0.2.0 (c (n "mixin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0va1nmkqlis6zziqz0d2xq61fj07kylh343gmxrlz5zrvachxm73")))

