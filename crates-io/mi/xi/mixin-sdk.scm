(define-module (crates-io mi xi mixin-sdk) #:use-module (crates-io))

(define-public crate-mixin-sdk-0.0.1 (c (n "mixin-sdk") (v "0.0.1") (h "1h8wng8bd8ac6bm6hnf8m3kzrdsc4rd4b1pymf699hi4q4cc30z1")))

(define-public crate-mixin-sdk-0.0.2 (c (n "mixin-sdk") (v "0.0.2") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "frank_jwt") (r "^3.1.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "12s6lba6xvcmfxbb7a83qp7dn6qggxhyd8sxgaqqlvc88inkzmmw")))

(define-public crate-mixin-sdk-0.0.3 (c (n "mixin-sdk") (v "0.0.3") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "frank_jwt") (r "^3.1.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1vsapihmis26470i0zi4srr6zkivphcdzy5mmq9sin8zdxwgd5m3")))

