(define-module (crates-io mi ch michie-macro) #:use-module (crates-io))

(define-public crate-michie-macro-0.2.1 (c (n "michie-macro") (v "0.2.1") (d (list (d (n "attribute-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1k04xb32s70iyypi3kwyd50qk9yny4hwk6zx48s9ybl88lgwl4rs")))

(define-public crate-michie-macro-0.2.2 (c (n "michie-macro") (v "0.2.2") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0xaixa4ra9zm3m0hh45523n1mklinpq4y3wfi0ds50qjkdlq3qsw")))

(define-public crate-michie-macro-0.2.3 (c (n "michie-macro") (v "0.2.3") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhy4y9lgck0hdq5cykc5c3v70yapwvrpr2qfgnc461qkimn8phq")))

(define-public crate-michie-macro-0.2.5 (c (n "michie-macro") (v "0.2.5") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0i84bmbkac5n2bsyr455s7anallmz74pvvymj3k2gi5fcakwicrg")))

(define-public crate-michie-macro-0.2.6 (c (n "michie-macro") (v "0.2.6") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "07zymdbfnhqkg0dm5qasarzjqaavgcxx8irn0sprdaamvcws6sb2")))

(define-public crate-michie-macro-0.2.7 (c (n "michie-macro") (v "0.2.7") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1baxn5y38zy2wnygv2wm696bcm6s962cjm05w0p9p00c4ba36k1r")))

(define-public crate-michie-macro-0.2.8 (c (n "michie-macro") (v "0.2.8") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1f49irz10kbydbwp29p5amqms45yzg6x8j9kci2p4cpxz1jyx0jn")))

(define-public crate-michie-macro-0.2.9 (c (n "michie-macro") (v "0.2.9") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1ryxlld7bf40pwicydac0diiq3jsqz8ixjxm718vzjl3hr02721z")))

(define-public crate-michie-macro-0.2.10 (c (n "michie-macro") (v "0.2.10") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "07ysci3imf3a96lv0m9kjl6cy7ycjfk3dry957dmddlkqv6pd0iw")))

(define-public crate-michie-macro-0.2.11 (c (n "michie-macro") (v "0.2.11") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0iwzs2xyyjg3f2adg3lndrvijmjzp6h5zpn920hy2hi6i5ql4anz")))

(define-public crate-michie-macro-0.2.12 (c (n "michie-macro") (v "0.2.12") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1h2vxz9kjym46swcjwfww5wlr32hm2p507krj2zpf32jynz7f901")))

(define-public crate-michie-macro-0.2.13 (c (n "michie-macro") (v "0.2.13") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1ix206bb81siwk71bkih9yhsb1198mmca2zz33sg3wwg4d2p1cg5")))

(define-public crate-michie-macro-0.3.0 (c (n "michie-macro") (v "0.3.0") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1zgr23qsp0pfcwbfspmlniqihdr3yfxdk1px8b2bcihy7015g3i5")))

(define-public crate-michie-macro-1.0.0 (c (n "michie-macro") (v "1.0.0") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0kfp0g4py8r7l19dgi8q5q479kmihwmfdszimq7vnrqhgrnrnic5")))

(define-public crate-michie-macro-1.1.0 (c (n "michie-macro") (v "1.1.0") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0n33avip61l7hw6p7shaabx4nrbgw7b0nchz4ms9s8jc3fichk1l")))

(define-public crate-michie-macro-2.0.0 (c (n "michie-macro") (v "2.0.0") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "06rpd8xy4iazcni6f0bv4ly7czn394fqznxc4pqpwx9265nbb018")))

(define-public crate-michie-macro-3.0.0 (c (n "michie-macro") (v "3.0.0") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1yh74agfimyc90lnl90vs958c8g521qxpldd1qw95pgpbfcpjmfr")))

(define-public crate-michie-macro-3.0.1 (c (n "michie-macro") (v "3.0.1") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "13ddgfmc9xv4q4m0qflzhqy7zw47b1kb66xsgj86fkmhc062y6p3")))

(define-public crate-michie-macro-3.0.2 (c (n "michie-macro") (v "3.0.2") (d (list (d (n "attribute-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0x6wl22qc66kr2pbxhhdhad820walwbdb2na5vc5341rya3f307q")))

