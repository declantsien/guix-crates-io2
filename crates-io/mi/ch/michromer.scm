(define-module (crates-io mi ch michromer) #:use-module (crates-io))

(define-public crate-michromer-0.1.0 (c (n "michromer") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0zk81xcpcgagp91zvwd9xiiiaii2a0pbdlir7d3i3fyvr8wrq9qb")))

(define-public crate-michromer-0.1.1 (c (n "michromer") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0zms6jvp5j5567afancfpdc4wy3x4a1c2in4yzb0hxvqfkal09d9")))

(define-public crate-michromer-0.2.0 (c (n "michromer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.7") (d #t) (k 0)))) (h "1vz5ky8hs84j5smmkapqsgznfc705a7vc54z7s9lwwc9ic4jhdva")))

(define-public crate-michromer-0.3.0 (c (n "michromer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.7") (d #t) (k 0)))) (h "1nyb6v0hdxfhk63dpf91ina8nk79cjpcnaqsp4z9v2g714xdldqz")))

(define-public crate-michromer-0.3.1 (c (n "michromer") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.10") (d #t) (k 0)))) (h "1bavmvm8sdz0cvx5kdbng5s4s54g6hdkkikpbydra89q5cxq3q3s")))

(define-public crate-michromer-0.3.2 (c (n "michromer") (v "0.3.2") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "0w33wq7v9h5qw86j9hwpfq9w6sfia5k7r692fix8f1hma5q2f21s")))

(define-public crate-michromer-0.3.3 (c (n "michromer") (v "0.3.3") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "clippy") (r "^0.0.85") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "16hp7qnyyq8g8pn68i2snc9nv7k8xgj309wbpga0352wwn47z6an") (f (quote (("default"))))))

(define-public crate-michromer-0.3.4 (c (n "michromer") (v "0.3.4") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "clippy") (r "^0.0.85") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "1ww056qsfjj7b9jk9x0zfi0hnvmbhqdr0iq6850yld4v0ghk0n0a") (f (quote (("default"))))))

(define-public crate-michromer-0.4.0 (c (n "michromer") (v "0.4.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "14pf79s9k30ajj0sdkpajz0d25wlkk4sqrakk15nzmcwl4jjfg0d") (f (quote (("default"))))))

