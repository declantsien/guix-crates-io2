(define-module (crates-io mi ch michelson-ast) #:use-module (crates-io))

(define-public crate-michelson-ast-0.1.0 (c (n "michelson-ast") (v "0.1.0") (h "1gsnya002gcs5vn4bc4cjzd8y6s50cf3css75djbr3zvx7nvqh50")))

(define-public crate-michelson-ast-0.1.1 (c (n "michelson-ast") (v "0.1.1") (h "13sz9jdcp1xyfpv3n08f7a2zsnwd8vw8kfm5cmbh0r5x39vbq9k6")))

(define-public crate-michelson-ast-0.1.2 (c (n "michelson-ast") (v "0.1.2") (h "1sxbgirf8kq1nlr3bvganphmb0hhshzj6sbalcjw5lbbz0wnr9w5")))

