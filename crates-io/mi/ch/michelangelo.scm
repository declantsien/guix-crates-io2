(define-module (crates-io mi ch michelangelo) #:use-module (crates-io))

(define-public crate-michelangelo-0.1.0 (c (n "michelangelo") (v "0.1.0") (d (list (d (n "compact") (r "^0.2.4") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)))) (h "1ykc8qb4hly9qwdlwrq6lg51c508qkyd8x7fhd99pxwpaqs7zps6")))

(define-public crate-michelangelo-0.1.1 (c (n "michelangelo") (v "0.1.1") (d (list (d (n "compact") (r "^0.2.4") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)))) (h "1q0dda00rx462in4l8jacm2d7a9ak3s69cs00cd9pmijrvf5cnb4")))

(define-public crate-michelangelo-0.1.2 (c (n "michelangelo") (v "0.1.2") (d (list (d (n "compact") (r "^0.2.8") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.11") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)))) (h "0pg5zb83d44zwy7y1xa077zngfqv8kykqw3gfqjcvgy91rzyv0q3")))

(define-public crate-michelangelo-0.1.3 (c (n "michelangelo") (v "0.1.3") (d (list (d (n "compact") (r "^0.2.9") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.12") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)))) (h "002rasrxvb8nlqrcj01f7g199f63pw48nwcifpap8q8p5kilpjbx")))

(define-public crate-michelangelo-0.1.4 (c (n "michelangelo") (v "0.1.4") (d (list (d (n "compact") (r "^0.2.9") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.13") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.9.1") (d #t) (k 0)))) (h "190p55428an9hcsc95xndysbn6zqrhd4m422vgv66avs2fsqy0sx")))

(define-public crate-michelangelo-0.2.0 (c (n "michelangelo") (v "0.2.0") (d (list (d (n "compact") (r "^0.2.13") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.17") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)))) (h "1ji4g8lidj61l577cf2b47yck9d34h7rp05q7lf1wn1il502afs1")))

(define-public crate-michelangelo-0.2.1 (c (n "michelangelo") (v "0.2.1") (d (list (d (n "compact") (r "^0.2.13") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)))) (h "16gd7plwgiq6ps9v7dry1k7s470jg3iw9bsim5j7m91v4g4qbqm1")))

(define-public crate-michelangelo-0.2.2 (c (n "michelangelo") (v "0.2.2") (d (list (d (n "compact") (r "^0.2.13") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)))) (h "0cqlsvg81xjdjssi6nm260xxa1m58g4q20wpqbr1scww629w976x")))

(define-public crate-michelangelo-0.2.3 (c (n "michelangelo") (v "0.2.3") (d (list (d (n "compact") (r "^0.2.13") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)))) (h "0p3zkc5whypckngykgsp7hya1qzm6a810vpnmjglai4sv97gvg59")))

(define-public crate-michelangelo-0.2.5 (c (n "michelangelo") (v "0.2.5") (d (list (d (n "compact") (r "^0.2.13") (d #t) (k 0)) (d (n "compact_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "descartes") (r "^0.1.19") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.11.0") (d #t) (k 0)))) (h "0gfj34w9awmjcvwkyd00qwzpcxdiwrazx3g2k3lzkwgiz81iksf7")))

