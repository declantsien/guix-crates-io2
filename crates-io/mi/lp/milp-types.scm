(define-module (crates-io mi lp milp-types) #:use-module (crates-io))

(define-public crate-milp-types-0.0.0 (c (n "milp-types") (v "0.0.0") (h "0smxba1cv8rxilvaaalffpsyman42g07ix1aw77a0az32rvj7knj") (f (quote (("default"))))))

(define-public crate-milp-types-0.0.1 (c (n "milp-types") (v "0.0.1") (d (list (d (n "lp-types") (r "^0.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1si2in5sn9b1zdqsq4ihyd71m6w0vl67idp1f7wf5mqn8bibws38") (f (quote (("default"))))))

