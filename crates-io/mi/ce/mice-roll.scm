(define-module (crates-io mi ce mice-roll) #:use-module (crates-io))

(define-public crate-mice-roll-0.1.0 (c (n "mice-roll") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04kf1lj4yfj346g152plc6dnl00nqsszcap5mwq928h8cqzw6gvh")))

(define-public crate-mice-roll-0.1.1 (c (n "mice-roll") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "073s0dvck2dzsv1kdxi7qxy9awqc1cmd7wn612cw75hzpgazgq99")))

