(define-module (crates-io mi as miasht) #:use-module (crates-io))

(define-public crate-miasht-0.0.1 (c (n "miasht") (v "0.0.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "handy_async") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "sha1") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1bf0rmn5mhk5c6bcnzc9hmd9nwfqqrnwbzh2kxghpnl7whhq4zdr")))

(define-public crate-miasht-0.0.2 (c (n "miasht") (v "0.0.2") (d (list (d (n "base64") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "handy_async") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "sha1") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1r4axv9r5ybwg3qy0h7j9lff19p1773cfa6xwjd928l5gaffvpgr")))

(define-public crate-miasht-0.0.3 (c (n "miasht") (v "0.0.3") (d (list (d (n "base64") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "handy_async") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "sha1") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "0v48v4hixjsz1giqlgb266ph39j1vmjjpf9da6yzd8i00nlpl0k0")))

(define-public crate-miasht-0.0.4 (c (n "miasht") (v "0.0.4") (d (list (d (n "base64") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "handy_async") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "sha1") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1vzh008prlbs17qky0yc07bwp0ww9y20bbx7m1wl6ppbrk3hmyn2")))

(define-public crate-miasht-0.0.5 (c (n "miasht") (v "0.0.5") (d (list (d (n "base64") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "handy_async") (r "^0.2") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "sha1") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0g36ls9s5xbh0crc8ggy5ipxjm8yifl1visf88df1r5r40jf2476")))

