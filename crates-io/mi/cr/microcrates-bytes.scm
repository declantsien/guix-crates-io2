(define-module (crates-io mi cr microcrates-bytes) #:use-module (crates-io))

(define-public crate-microcrates-bytes-0.4.6 (c (n "microcrates-bytes") (v "0.4.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1idz4pccs61nb211p39by0aq528pr7ghhvzbnfm10lz7qdav23zz") (f (quote (("std" "alloc" "iovec") ("nightly" "alloc") ("alloc"))))))

(define-public crate-microcrates-bytes-0.4.7 (c (n "microcrates-bytes") (v "0.4.7") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05snrjznwvhaxqfwivjrlg1j8gm1b7dcjri9ryk2zj3kz9jkwib9") (f (quote (("std" "alloc" "iovec") ("nightly" "alloc") ("default" "nightly") ("alloc"))))))

(define-public crate-microcrates-bytes-0.4.8 (c (n "microcrates-bytes") (v "0.4.8") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "17qvnh107awksxkrbbcmxyjn0c1clgkg2hsa80m5ah22gr6im430") (f (quote (("std" "alloc" "iovec") ("nightly" "alloc") ("default" "nightly") ("alloc"))))))

