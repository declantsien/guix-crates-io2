(define-module (crates-io mi cr micro_autotile) #:use-module (crates-io))

(define-public crate-micro_autotile-0.1.0 (c (n "micro_autotile") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "15jr2ma6gd97946y3lc3vzx6xllzbrzjw90piw15rpwnn5ys1vbq") (f (quote (("default" "impl_fastrand")))) (s 2) (e (quote (("impl_fastrand" "dep:fastrand"))))))

