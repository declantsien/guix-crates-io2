(define-module (crates-io mi cr microunit) #:use-module (crates-io))

(define-public crate-microunit-0.1.0 (c (n "microunit") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (d #t) (k 0)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0c2y4sbavbp28p826rxl9fshwjxh6ad527ddqhz9xdzyqbg4bh5v")))

(define-public crate-microunit-0.1.1 (c (n "microunit") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (d #t) (k 0)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "14swpbgv416c3ghrnlxlxrdy5z0aiq92snybc9iy9x329dglzl5s")))

