(define-module (crates-io mi cr micro_types) #:use-module (crates-io))

(define-public crate-micro_types-0.0.0 (c (n "micro_types") (v "0.0.0") (d (list (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vwq7kyv3nrsbm10r0gwh04s41cfmkwbqy6qincf1183bcw6mc6d")))

(define-public crate-micro_types-0.1.0 (c (n "micro_types") (v "0.1.0") (d (list (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1yqjzx7xkh15rdzwim5wfi6z50z2b8qyf6jp3f5jwcyrfif5jb5q")))

(define-public crate-micro_types-0.2.0 (c (n "micro_types") (v "0.2.0") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("script"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0snm5vc8jijcqs6wx2s2yz73xxj3vlc7idrqxvy3nw6bm3q45lr5") (f (quote (("default" "redis")))) (s 2) (e (quote (("redis" "dep:redis" "dep:serde_json" "dep:serde"))))))

(define-public crate-micro_types-0.2.1 (c (n "micro_types") (v "0.2.1") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("script"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "11hvfp1pr5k0gfsas09c7yppn59znn5q87r6ll25vf0b527i6chn") (f (quote (("default" "redis")))) (s 2) (e (quote (("redis" "dep:redis" "dep:serde_json" "dep:serde"))))))

(define-public crate-micro_types-0.2.2 (c (n "micro_types") (v "0.2.2") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("script"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "19pzv131cys0lj57x3h6p6gnap8wzhkagb3s4ybr06gyhk33kqpf") (f (quote (("default" "redis")))) (s 2) (e (quote (("redis" "dep:redis" "dep:serde_json" "dep:serde"))))))

(define-public crate-micro_types-0.2.3 (c (n "micro_types") (v "0.2.3") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("script"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "02zc7fakx12r5v4hljhqz2zyfjagajxbgzir8vmxpqk8m8wzyi8z") (f (quote (("default" "redis")))) (s 2) (e (quote (("redis" "dep:redis" "dep:serde_json" "dep:serde"))))))

(define-public crate-micro_types-0.3.0 (c (n "micro_types") (v "0.3.0") (d (list (d (n "redis") (r "^0.23.3") (f (quote ("script"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "15kgrzljk8iwq0ws6j86736bn7isylqgc6s9vnvidfb7bk7c3z7j") (f (quote (("default" "redis")))) (s 2) (e (quote (("redis" "dep:redis" "dep:serde_json" "dep:serde"))))))

