(define-module (crates-io mi cr microxdg) #:use-module (crates-io))

(define-public crate-microxdg-0.1.0 (c (n "microxdg") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "1hds6gg0njc9hc26bij1hs32780n4prwfzdpy6jwwfgdiqj3ymhc") (r "1.70.0")))

(define-public crate-microxdg-0.1.1 (c (n "microxdg") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "076v2lzynqfgc5m1q4ij0s1fbdvyq838smlyxny9ik91sz3y6gcl") (r "1.70.0")))

(define-public crate-microxdg-0.1.2 (c (n "microxdg") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "0pnymllp80ikpbkbc6m34pdy38qi77lkjrjph0drn6hdrb23vkgx") (r "1.70.0")))

