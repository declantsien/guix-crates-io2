(define-module (crates-io mi cr microfacet) #:use-module (crates-io))

(define-public crate-microfacet-0.1.0 (c (n "microfacet") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0ajf8gxhs8qv09v15fmbhr5wqdsl40dkbsgpa0li0m1xsl5f5qvh")))

