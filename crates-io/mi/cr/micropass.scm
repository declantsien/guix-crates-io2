(define-module (crates-io mi cr micropass) #:use-module (crates-io))

(define-public crate-micropass-0.1.0 (c (n "micropass") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)))) (h "1wnq4i5cp3x2g86fjsgfs1zlxdygchyz6jpa3waa74kj0a6q8kir")))

(define-public crate-micropass-0.1.1 (c (n "micropass") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)))) (h "0hmqm2yn2gzvplkm8xipy2q35i3gmrs922drxbcbrr2cqxkknc4r")))

