(define-module (crates-io mi cr microamp-macros) #:use-module (crates-io))

(define-public crate-microamp-macros-0.1.0-alpha.1 (c (n "microamp-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8nbgr81fj2lhav17vam300zh698l9vwls7az27m923aa49fwym") (y #t)))

(define-public crate-microamp-macros-0.1.0-alpha.2 (c (n "microamp-macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0x5zvjqg49y21am42x70g5aibz346nl3hl2p9hsgk3hly9qcqd81") (y #t)))

(define-public crate-microamp-macros-0.1.0-alpha.3 (c (n "microamp-macros") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sixsrvj5b644mmpsi0m0yjinfcah4kkgqq1cp4j13s1yfm2c76f") (y #t)))

(define-public crate-microamp-macros-0.1.0-alpha.4 (c (n "microamp-macros") (v "0.1.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l6mdqavm3rb1qqyba180fm6lp1ql2mfr9gnp799bqqqknic19h1")))

(define-public crate-microamp-macros-0.1.0-alpha.5 (c (n "microamp-macros") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xqmy0ml88677swgn6iidhrr4b7f21qv5xxm2vg03vjp6cfzgm8h")))

