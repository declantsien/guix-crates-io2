(define-module (crates-io mi cr micromegas-tracing-proc-macros) #:use-module (crates-io))

(define-public crate-micromegas-tracing-proc-macros-0.1.0 (c (n "micromegas-tracing-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v1chf81chlb04y7a8ykd05hn1m9zljg86q4aypkaq66j03mv79g")))

(define-public crate-micromegas-tracing-proc-macros-0.1.1 (c (n "micromegas-tracing-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1a327plrppnm0w0sn5cck0byry8db7lmi96yngns5gkzmr9a25ks")))

(define-public crate-micromegas-tracing-proc-macros-0.1.2 (c (n "micromegas-tracing-proc-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1a0ykdni600nihdjd8p1d8agp9rvd9f040n2g5mv16bjvn1qjqg5")))

(define-public crate-micromegas-tracing-proc-macros-0.1.3 (c (n "micromegas-tracing-proc-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1mc59g6cb27vv1pi4z040x2fy5j0sypclljy1dka86dk88ln94yw")))

