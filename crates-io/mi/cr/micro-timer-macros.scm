(define-module (crates-io mi cr micro-timer-macros) #:use-module (crates-io))

(define-public crate-micro-timer-macros-0.2.0 (c (n "micro-timer-macros") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12iq57k6j6d3iwiz5x8i7rq8b85pa2rg9k3vxwryp0w7ng0cbkj3")))

(define-public crate-micro-timer-macros-0.3.0 (c (n "micro-timer-macros") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01458j5rb1h6mp1jhppmagv9v3941h2cryh78a19xfw4sdfhi52n")))

(define-public crate-micro-timer-macros-0.3.1 (c (n "micro-timer-macros") (v "0.3.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p8wxi9b3hll6mj7dwvr9v9hpbi2mlrfzbkanfmfkmmbwrrk92p2")))

(define-public crate-micro-timer-macros-0.4.0 (c (n "micro-timer-macros") (v "0.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "189vr5ip91gf0lngjmyzq6b6ccpn35yd33dnvx95n4h08ywlisff")))

