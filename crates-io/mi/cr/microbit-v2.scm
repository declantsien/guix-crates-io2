(define-module (crates-io mi cr microbit-v2) #:use-module (crates-io))

(define-public crate-microbit-v2-0.10.0 (c (n "microbit-v2") (v "0.10.0") (d (list (d (n "microbit-common") (r "=0.10.0") (f (quote ("v2"))) (d #t) (k 0)))) (h "1cakj1r7x3vh1x2gl7l9s31xbac2hrpmvz4mfpp7pwxwnpnb8fn9")))

(define-public crate-microbit-v2-0.10.1 (c (n "microbit-v2") (v "0.10.1") (d (list (d (n "microbit-common") (r "=0.10.1") (f (quote ("v2"))) (d #t) (k 0)))) (h "12yi4rmlkkh629vq1g4640hc96y60r79jphv7g3zrl0ffjkd79fq")))

(define-public crate-microbit-v2-0.11.0 (c (n "microbit-v2") (v "0.11.0") (d (list (d (n "microbit-common") (r "=0.11.0") (f (quote ("v2"))) (d #t) (k 0)))) (h "0ajhidn6s0pcrb6pkf5fa6h5adk4q5kvvw2h6mdby4jzi6cc19xm")))

(define-public crate-microbit-v2-0.12.0 (c (n "microbit-v2") (v "0.12.0") (d (list (d (n "microbit-common") (r "=0.12.0") (f (quote ("v2"))) (d #t) (k 0)))) (h "0njcz2axhaj7my9c8xgqk8mjksk7lsrcpvkp9qvapcd8w29grp9c")))

(define-public crate-microbit-v2-0.13.0 (c (n "microbit-v2") (v "0.13.0") (d (list (d (n "microbit-common") (r "=0.13.0") (f (quote ("v2"))) (d #t) (k 0)))) (h "1rw41lk8j10iij3whpyv3p2msd1p5r53wqhjq45p6l4r4m1552d9")))

(define-public crate-microbit-v2-0.14.0 (c (n "microbit-v2") (v "0.14.0") (d (list (d (n "microbit-common") (r "=0.14.0") (f (quote ("v2"))) (d #t) (k 0)))) (h "1ac8f32fg1nas82zjjq0r5vqsxnkng1s2ig36i3kgs46nb2y2wms") (f (quote (("embedded-hal-02" "microbit-common/embedded-hal-02"))))))

