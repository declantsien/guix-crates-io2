(define-module (crates-io mi cr microrm-macros) #:use-module (crates-io))

(define-public crate-microrm-macros-0.1.2 (c (n "microrm-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0ccgini6yn49n28si4ymkiww195jl275irifs119hr2vgc919yda")))

(define-public crate-microrm-macros-0.2.0 (c (n "microrm-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "12x3bl7xih0226mvr2rai8m7wd21j3s0h87fn1a0lzxiglffabri")))

(define-public crate-microrm-macros-0.2.1 (c (n "microrm-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0d16vw4vgc5nji1mmjv7w7ld1jkyhag81v45cidzrcng51a1ng4a")))

(define-public crate-microrm-macros-0.2.2 (c (n "microrm-macros") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1p8ybbn218mjbk4vyyjls423z71j4bp6wb61cazfriza38a8xqm8")))

(define-public crate-microrm-macros-0.2.4 (c (n "microrm-macros") (v "0.2.4") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "18gl65klask69bdi5zpqips0vc6m4lrvx726knnwqshimnrfiwyz")))

(define-public crate-microrm-macros-0.2.5 (c (n "microrm-macros") (v "0.2.5") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1z2p6f991n31928nyff05xbgidz1n9k5605y07iwfvih47blv86v")))

(define-public crate-microrm-macros-0.2.6 (c (n "microrm-macros") (v "0.2.6") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "05w9hd89pfnp1zi3vpmfmsgbd5mk4rdf4pgw5ll0fyglp92sq853")))

(define-public crate-microrm-macros-0.4.0-rc.1 (c (n "microrm-macros") (v "0.4.0-rc.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1dpjrmii9q1w0s2kn209c5ww0dfg01iwsq0airi3sqc5nmnrwl62")))

(define-public crate-microrm-macros-0.4.0-rc.2 (c (n "microrm-macros") (v "0.4.0-rc.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1b2iqld14mh5qa0xqnajqbvi2xx30hq0x4r1pdc6m5j1z4ki6n3q")))

