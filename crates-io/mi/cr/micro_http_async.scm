(define-module (crates-io mi cr micro_http_async) #:use-module (crates-io))

(define-public crate-micro_http_async-0.0.1 (c (n "micro_http_async") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "042gfp62n1wfxrz8zr79z93npfy46q3r34i2263d923hmzkfs81a")))

(define-public crate-micro_http_async-0.0.2 (c (n "micro_http_async") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpcz2fz6dc5904nswdwqc9pdrdy3hcc6988b384n2mykp6d098i")))

(define-public crate-micro_http_async-0.0.3 (c (n "micro_http_async") (v "0.0.3") (d (list (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ffh1s4cfwp6kfmrc2s27ph8f5kcy37fkpkvzljgpmnxsikwrjdf")))

(define-public crate-micro_http_async-0.0.4 (c (n "micro_http_async") (v "0.0.4") (d (list (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0031cwhh2dj10gdlfw1mnk0hxydjngmyyg4nqzc1rbjgz7nshn3k") (y #t)))

(define-public crate-micro_http_async-0.0.5 (c (n "micro_http_async") (v "0.0.5") (d (list (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10y3nlalj07ryvqa2bd37aln4pp50ihgrmf94r2ap55y7nqa4f0p")))

(define-public crate-micro_http_async-0.0.6 (c (n "micro_http_async") (v "0.0.6") (d (list (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "01yxivjfzcfjp6lk933sgf0jmpwk29wr4vb0wnhbb5mgcwmjcqmv")))

(define-public crate-micro_http_async-0.0.7 (c (n "micro_http_async") (v "0.0.7") (d (list (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fgb55l23xni8md80lk47bl3nhrawr3r7acb019afdkkv4f8wpyd")))

(define-public crate-micro_http_async-0.0.8 (c (n "micro_http_async") (v "0.0.8") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m00k2frm4ih8d38wwkgjk4cxkdakqgkphj8q6bqi0lwrnbiv8yv")))

(define-public crate-micro_http_async-0.0.9 (c (n "micro_http_async") (v "0.0.9") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4a36clx2bg2s2b0r8cbfnv8fj8n7dc8fsmb6pjp7l0ykaxyb3g")))

(define-public crate-micro_http_async-0.1.0 (c (n "micro_http_async") (v "0.1.0") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "fs" "macros" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "11rvb06xp5l6099xbv93rr8zxns75hifdzzync2syhpyz0w7pnzc")))

(define-public crate-micro_http_async-0.1.1 (c (n "micro_http_async") (v "0.1.1") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "fs" "macros" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "1xhabc5gj74ljz99s7a44pqnif4cz1kl31daq36r2k8yhwwkb2rx")))

(define-public crate-micro_http_async-0.1.2 (c (n "micro_http_async") (v "0.1.2") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "fs" "macros" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "1fj7ljpql0zcz2jff767d256ngczb0qsx95vr6acyy8jsff94amg")))

(define-public crate-micro_http_async-0.1.3 (c (n "micro_http_async") (v "0.1.3") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "fs" "macros" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "1lvzclhvlhqpa12d7jkvdmzr70j0f4593vlypgxgfjmn77axbf3v")))

(define-public crate-micro_http_async-0.1.4 (c (n "micro_http_async") (v "0.1.4") (d (list (d (n "chunked_transfer") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("net" "fs" "macros" "io-util" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.2") (d #t) (k 0)))) (h "1wnq0ib2q66s7wcv1nz2bynplgi4y3kls428f86vmi3psvmkrg28")))

