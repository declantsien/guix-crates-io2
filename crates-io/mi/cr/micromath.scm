(define-module (crates-io mi cr micromath) #:use-module (crates-io))

(define-public crate-micromath-0.0.1 (c (n "micromath") (v "0.0.1") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "0s66vlnxdaqvvhw2fp73w6h6pgm4jy5a0j4fpjp2n7arja6169kr") (f (quote (("vector") ("statistics") ("default" "statistics" "vector"))))))

(define-public crate-micromath-0.1.0 (c (n "micromath") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "1wikddqd9ckw807bwfmjznri3hlgxqdfmrhxm830p9r4w33n5crx") (f (quote (("vector") ("statistics") ("default" "statistics" "vector"))))))

(define-public crate-micromath-0.2.0 (c (n "micromath") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "1fqp3spg0jaqmhnirsdyya7jfq218jcyzqz115dys2xfwrkjfp9q") (f (quote (("vector") ("statistics") ("default" "statistics" "vector"))))))

(define-public crate-micromath-0.2.1 (c (n "micromath") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "0fh1ivb99pn5g5ps0gbxfg2hwg6583j50q678k8cy0ksvdq6wbjh") (f (quote (("vector") ("statistics") ("default" "statistics" "vector"))))))

(define-public crate-micromath-0.2.2 (c (n "micromath") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "0jfll5bk0bwh45ycarpbmva9j8m76zmyddpzcmq2hinchkhvinmx") (f (quote (("vector") ("statistics") ("default" "statistics" "vector"))))))

(define-public crate-micromath-0.3.0 (c (n "micromath") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "1vqf9jr70gfwixj2yyc85rh41yrna82hmy3b6i7vcli57my4i6lw") (f (quote (("vector") ("statistics") ("quaternion") ("default" "quaternion" "statistics" "vector"))))))

(define-public crate-micromath-0.3.1 (c (n "micromath") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "0j05af5gpx5alqvckpdadki96f0nzq5vnznx87qvvrmshw00i72y") (f (quote (("vector") ("statistics") ("quaternion") ("default" "quaternion" "statistics" "vector"))))))

(define-public crate-micromath-0.4.0 (c (n "micromath") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "0yba9i4vr52d0nflpzl79v2nzssyljg4jls7glqf88djsi7vvm6v") (f (quote (("vector") ("statistics") ("quaternion") ("default" "quaternion" "statistics" "vector"))))))

(define-public crate-micromath-0.4.1 (c (n "micromath") (v "0.4.1") (d (list (d (n "generic-array") (r "^0.13") (k 0)))) (h "17f9bkamk1cs4m8s0nkdg7z8idmcqkrlbm18mk451cfqzzfkawjb") (f (quote (("vector") ("statistics") ("quaternion") ("default" "quaternion" "statistics" "vector"))))))

(define-public crate-micromath-0.5.0 (c (n "micromath") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.13") (o #t) (k 0)))) (h "1gs96ygjsabpz0v8l3pv7gri54mlg613si59ai23lmcpc8hkijb8") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-0.5.1 (c (n "micromath") (v "0.5.1") (d (list (d (n "generic-array") (r "^0.13") (o #t) (k 0)))) (h "0dngnsn5n5nnvk9ra51pkw9dq2qpxk2bgicdb2070zirb5451c8x") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-1.0.0 (c (n "micromath") (v "1.0.0") (d (list (d (n "generic-array") (r "^0.13") (o #t) (k 0)))) (h "04xs5vwnbl4b684mh311gv2n51h2qb7bdqca9wb1avigs8njn7f6") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-1.0.1 (c (n "micromath") (v "1.0.1") (d (list (d (n "generic-array") (r "^0.13") (o #t) (k 0)))) (h "1wfwhmyjqjcycvqmhja15r8bbk774zfi2dajxplrd5jgc09qbpyf") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-1.1.0 (c (n "micromath") (v "1.1.0") (d (list (d (n "generic-array") (r "^0.14") (o #t) (k 0)))) (h "0hxqn323dhvv0gcig213mfapkjsdh6rfhxx4dnc28h9rsi2a405y") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-1.1.1 (c (n "micromath") (v "1.1.1") (d (list (d (n "generic-array") (r "^0.14") (o #t) (k 0)))) (h "157ah5s6zmdz03ynzd9axhli6vqssl2ff7prq9b6cfga7a1i0h5w") (f (quote (("vector" "generic-array") ("statistics") ("quaternion"))))))

(define-public crate-micromath-2.0.0-pre (c (n "micromath") (v "2.0.0-pre") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0xnjigwwl6is7w35zsvfn4q4pxcpjm11gvmqjb8gwbp7bnd25w4b") (f (quote (("vector") ("statistics") ("quaternion"))))))

(define-public crate-micromath-2.0.0 (c (n "micromath") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0m9b5g66h0k9abfgm0qcdmbvb5cicwqkw3pxry6hcjyn174pnq9r") (f (quote (("vector") ("statistics") ("quaternion"))))))

(define-public crate-micromath-2.1.0 (c (n "micromath") (v "2.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "05g8zavgsks2f1rkl8fd8lxsbmb51yjls88phwijyfph9yjdvj63") (f (quote (("vector") ("statistics") ("quaternion"))))))

