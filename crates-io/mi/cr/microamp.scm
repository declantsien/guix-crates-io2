(define-module (crates-io mi cr microamp) #:use-module (crates-io))

(define-public crate-microamp-0.1.0-alpha.1 (c (n "microamp") (v "0.1.0-alpha.1") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1lq4p80kfpgxyhvf3752f6b7v5diz8v0w5ywp4zwjpdfj4fyq4rj") (y #t)))

(define-public crate-microamp-0.1.0-alpha.2 (c (n "microamp") (v "0.1.0-alpha.2") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1raydzqp4gj51xgim3hfxkymcw13wmw5vrmk0rrxnva5rc4scc5a") (y #t)))

(define-public crate-microamp-0.1.0-alpha.3 (c (n "microamp") (v "0.1.0-alpha.3") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "17has8fx8104hplsb5gfn1zi65yj13j0w72bpbqwiprzb4fdkwj1") (y #t)))

(define-public crate-microamp-0.1.0-alpha.4 (c (n "microamp") (v "0.1.0-alpha.4") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0559vvzy0h0qgqsfy1rwxys9al35zds34lf8nrnmpw1mp1vcw8im")))

(define-public crate-microamp-0.1.0-alpha.5 (c (n "microamp") (v "0.1.0-alpha.5") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1wvlc3516d0jz1vlgzs7szz0l4ksmji6nzfsryilb4m41ggq2srq")))

(define-public crate-microamp-0.1.0-alpha.6 (c (n "microamp") (v "0.1.0-alpha.6") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1a734x3y8qyisnmahk53qjqaqcny888frgb9x8zgpys0ibdvrml6")))

(define-public crate-microamp-0.1.0-alpha.7 (c (n "microamp") (v "0.1.0-alpha.7") (d (list (d (n "microamp-macros") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0dphr4dxvs9qnmmw8cpf1r1h59wfm82z7bcjl9jismwhmgv31mdm")))

