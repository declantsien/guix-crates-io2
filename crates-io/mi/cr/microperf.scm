(define-module (crates-io mi cr microperf) #:use-module (crates-io))

(define-public crate-microperf-0.1.1 (c (n "microperf") (v "0.1.1") (h "07a2b8jifmddkx5r28sdjyj639l5znh0j5xw82c5sf9kbbv45zpw")))

(define-public crate-microperf-0.1.3 (c (n "microperf") (v "0.1.3") (h "0x34hf7d8lpq3gv7vngpfb72sjr5dc9cb9vx2qi8vdd68sfb5yk6")))

(define-public crate-microperf-0.1.6 (c (n "microperf") (v "0.1.6") (h "1kcpbazd4cn6sz54g4qsasj3fjfpjbpx9zl6w64liamygzhy7d4j")))

