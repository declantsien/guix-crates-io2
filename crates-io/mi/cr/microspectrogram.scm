(define-module (crates-io mi cr microspectrogram) #:use-module (crates-io))

(define-public crate-microspectrogram-0.1.0 (c (n "microspectrogram") (v "0.1.0") (d (list (d (n "microfft") (r "^0.5") (f (quote ("size-128"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1kqy5hsp2jhvg4zp5d3n0yizz65jcjjfpbmiy9cm02wsj56l0md9")))

