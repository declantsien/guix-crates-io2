(define-module (crates-io mi cr microserde-derive) #:use-module (crates-io))

(define-public crate-microserde-derive-0.1.13 (c (n "microserde-derive") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ckjhddm4shap3wlz1knpq80zkjfkrm4fi5m6pbrxfv2wj7glfkf")))

