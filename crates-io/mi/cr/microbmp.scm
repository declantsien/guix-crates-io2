(define-module (crates-io mi cr microbmp) #:use-module (crates-io))

(define-public crate-microbmp-0.1.0 (c (n "microbmp") (v "0.1.0") (h "07484i12bbsppw8743zi6dvx1m939a2m04k94kzwad1dvn2sc11b")))

(define-public crate-microbmp-0.1.1 (c (n "microbmp") (v "0.1.1") (h "1c0xlsmvj8xcq4q4hvdar38ihk4qbj7gfgzx8dw26k6arxq56qk4")))

(define-public crate-microbmp-0.2.0 (c (n "microbmp") (v "0.2.0") (h "05xf81p7k04v5djpj9ds9qq455sq2pp8z214mbx4mphl0i1d76ad")))

(define-public crate-microbmp-0.2.1 (c (n "microbmp") (v "0.2.1") (h "0zxlsnm4h1p22vkrg225ianp5jcz2rby3l8zl675108xiw5iraam")))

