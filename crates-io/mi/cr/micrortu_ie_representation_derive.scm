(define-module (crates-io mi cr micrortu_ie_representation_derive) #:use-module (crates-io))

(define-public crate-micrortu_ie_representation_derive-0.1.0 (c (n "micrortu_ie_representation_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17412zkwapnawc5y6dpf8naqhsb14jpgs7mi8gkp236qg31va6pq")))

(define-public crate-micrortu_ie_representation_derive-0.1.1 (c (n "micrortu_ie_representation_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kb4b97c2w4adla0kpgm4w5xah1qaj3170ad8m8vab9dq28vvh9f")))

(define-public crate-micrortu_ie_representation_derive-0.1.3 (c (n "micrortu_ie_representation_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0iijzpyypk8vini7s8wg6xl0662l5313rwnz149vmy1miva6pg4w")))

(define-public crate-micrortu_ie_representation_derive-0.1.4 (c (n "micrortu_ie_representation_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1nnanr1789ccsm6bimbyyr0b18d93askm94r5jpqicildgwfiwm8")))

(define-public crate-micrortu_ie_representation_derive-0.1.6 (c (n "micrortu_ie_representation_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "181h5kcab3a9i42y1ym30pmxhax9aiajj5bz5hkb3md0fck48w9k")))

(define-public crate-micrortu_ie_representation_derive-0.1.7 (c (n "micrortu_ie_representation_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1j41vr0jq9zagmb2ip1f527010jkwzzfjbnam5rwclad2frxq4gp")))

(define-public crate-micrortu_ie_representation_derive-0.2.1 (c (n "micrortu_ie_representation_derive") (v "0.2.1") (d (list (d (n "micrortu_build_utils") (r "^0.2.1") (d #t) (k 0) (p "micrortu_build_utils")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0g0ham8f7x3r9w1wdmxhq926ix0b0331issh4qkrgvm9z5jg4kgn")))

(define-public crate-micrortu_ie_representation_derive-0.2.2 (c (n "micrortu_ie_representation_derive") (v "0.2.2") (d (list (d (n "micrortu_build_utils") (r "^0.2.2") (d #t) (k 0) (p "micrortu_build_utils")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0k19mv6p7lfjgy9vji4p95r78llbi863y5rxsgwwq1993c16ajyx")))

(define-public crate-micrortu_ie_representation_derive-0.2.3 (c (n "micrortu_ie_representation_derive") (v "0.2.3") (d (list (d (n "micrortu_build_utils") (r "^0.2.3") (d #t) (k 0) (p "micrortu_build_utils")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wpwmxxnir6ag4zw50br7sz05z4nlh1s123zakdm9d9w764m5sxj")))

(define-public crate-micrortu_ie_representation_derive-0.4.0 (c (n "micrortu_ie_representation_derive") (v "0.4.0") (d (list (d (n "micrortu_build_utils") (r "^0.4.0") (d #t) (k 0) (p "micrortu_build_utils")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "116nw1mp90j7626dxcswg8mpra895y27lwwhwz207kd8cv4wqlf1")))

(define-public crate-micrortu_ie_representation_derive-0.4.1 (c (n "micrortu_ie_representation_derive") (v "0.4.1") (d (list (d (n "micrortu_build_utils") (r "^0.4.1") (d #t) (k 0) (p "micrortu_build_utils")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "093pp0fb18wf8s56nxwy6g9fj4mqq9zvzgmkjvxx3hpqj6szyapl")))

