(define-module (crates-io mi cr micro_quest) #:use-module (crates-io))

(define-public crate-micro_quest-0.1.0 (c (n "micro_quest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (o #t) (d #t) (k 0)) (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_asset" "bevy_sprite" "bevy_winit" "png" "hdr" "x11" "wayland" "serialize" "filesystem_watcher" "bevy_core_pipeline"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "0sr3ssmr46wxplyh3i556abjdb221cni0mcyqh1g5sykds53ijjv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("bevy" "serde" "dep:bevy" "dep:anyhow" "dep:toml"))))))

