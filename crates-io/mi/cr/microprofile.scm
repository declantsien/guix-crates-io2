(define-module (crates-io mi cr microprofile) #:use-module (crates-io))

(define-public crate-microprofile-0.0.1 (c (n "microprofile") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ri15hvv4860p8gqchcrszp5g6s71fwrj50c89bmnyrfj5k4701y")))

(define-public crate-microprofile-0.0.2 (c (n "microprofile") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09ccaqnpaqj08j9y6a1fm2vxxjlypjm94v1qwcdwnfd420zsazsa") (f (quote (("disabled") ("default"))))))

(define-public crate-microprofile-0.1.0 (c (n "microprofile") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17zwflnyrkn1r22snb4cng7x5hz1cpar4sgsvvyycqdihz0lhk8h") (f (quote (("disabled") ("default"))))))

(define-public crate-microprofile-0.2.0 (c (n "microprofile") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17jcv5k6qgjz77k98pnvz77y8c08sllnpp7clg7qdgkgh70m0q8q") (f (quote (("disabled") ("default"))))))

(define-public crate-microprofile-0.2.1 (c (n "microprofile") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ms2fmnnqd2rddly8c0zqlac986ka58g4a25syf00snd1zh8i2cr") (f (quote (("disabled") ("default"))))))

