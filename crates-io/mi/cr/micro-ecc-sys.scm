(define-module (crates-io mi cr micro-ecc-sys) #:use-module (crates-io))

(define-public crate-micro-ecc-sys-0.1.0 (c (n "micro-ecc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0pzfqxkbb14sazc2nwqbd64fkjk0rvz79bi85iikndydzwh2rpan")))

(define-public crate-micro-ecc-sys-0.1.1 (c (n "micro-ecc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "197qa0vn9257df24f86108rrj7pxxf7a7qiwl480qw68kpq5b6rd")))

(define-public crate-micro-ecc-sys-0.1.2 (c (n "micro-ecc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1r4y2nb2izgfi0yv9af9jvl6y0a8223fkm7q3lhqgz6jr4xvgcyb")))

(define-public crate-micro-ecc-sys-0.1.3 (c (n "micro-ecc-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1jbs5ksjma18920ml7lcn5ags89hgb6y3s5wzd7q08njzrryjd31") (f (quote (("square") ("run-bindgen" "bindgen") ("no-umaal") ("default" "run-bindgen"))))))

(define-public crate-micro-ecc-sys-0.2.0 (c (n "micro-ecc-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "03rdjkj8m14vlnqycvpg43h1xnsmhq6z9piya9bzwdkh0cr7plk7") (f (quote (("square") ("run-bindgen" "bindgen") ("no-umaal"))))))

(define-public crate-micro-ecc-sys-0.3.0 (c (n "micro-ecc-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0qpg4kzzm00nm14lykcvg30gvy3v4r822bqw59rwgpgc05y7jn7q") (f (quote (("square") ("no-umaal"))))))

(define-public crate-micro-ecc-sys-0.3.1 (c (n "micro-ecc-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.53.1") (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0ix4g8cp99sdh0qa809g3zld6h7fjqipk0jx43snbsd4g7zpcpvc") (f (quote (("square") ("no-umaal"))))))

(define-public crate-micro-ecc-sys-0.3.2 (c (n "micro-ecc-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.56.0") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1yh09avrjp74yyv08m0a42nx32zkdfj23qpxw0zyq9dsgrz1r1hv") (f (quote (("square") ("no-umaal"))))))

