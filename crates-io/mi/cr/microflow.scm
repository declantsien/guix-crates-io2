(define-module (crates-io mi cr microflow) #:use-module (crates-io))

(define-public crate-microflow-0.1.1 (c (n "microflow") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "microflow-macros") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("macros"))) (k 0)) (d (n "simba") (r "^0.8") (k 0)))) (h "1d2fd6ga69qgma6z6xiqc827b26mfz1yyxcyafgmbrfxs96j8gfy") (y #t)))

(define-public crate-microflow-0.1.2 (c (n "microflow") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "microflow-macros") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("macros"))) (k 0)) (d (n "simba") (r "^0.8") (k 0)))) (h "04aw32yn2d19zdaaqdfygd26k4291mchlqix56rnvv80k10lnm80")))

