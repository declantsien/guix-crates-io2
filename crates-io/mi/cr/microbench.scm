(define-module (crates-io mi cr microbench) #:use-module (crates-io))

(define-public crate-microbench-0.1.0 (c (n "microbench") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1g6sz0khswxnzf3fwx2s1raj0xlf7zlcsq8yrhad0iwqy36h9924")))

(define-public crate-microbench-0.2.0 (c (n "microbench") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "12r92pzfma891fszdfpgi9n0xicclbaick5xnf9w89g9s4g4k59p")))

(define-public crate-microbench-0.2.1 (c (n "microbench") (v "0.2.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0n56ialwsl5acni6y6wkrd79a94lj4f465d8w4ja7jpm2dxscvc1")))

(define-public crate-microbench-0.3.0 (c (n "microbench") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "01m7ahd1rrbjli8nq8g9la0yp91gbcpzh3sckqf314a7q80dvmph")))

(define-public crate-microbench-0.3.1 (c (n "microbench") (v "0.3.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0hngm5nn0pby7l7qlxyhq5g4lkbyy03d57pkn6p3w2fycxxwl1kh") (f (quote (("nightly"))))))

(define-public crate-microbench-0.3.2 (c (n "microbench") (v "0.3.2") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0pwr9vvvn1gkaqm3c8lwxk2mhzyjm054abfvmr2sxqxjwcdjcqa1") (f (quote (("nightly"))))))

(define-public crate-microbench-0.3.3 (c (n "microbench") (v "0.3.3") (h "168h8llb19b8a0z5bp4cz4cm6s91k0f60q2crdh359j4xkly4dm5") (f (quote (("nightly"))))))

(define-public crate-microbench-0.4.0 (c (n "microbench") (v "0.4.0") (h "0bpl7q71g9qlr4v7fjhr6d2amdwpxpjml201d87hfwi69ij40r16") (f (quote (("nightly"))))))

(define-public crate-microbench-0.5.0 (c (n "microbench") (v "0.5.0") (h "02ysyvxz574892a14jdrms2r5rwcjkjr3fsp88pzvrp4mr04xi64") (f (quote (("nightly"))))))

