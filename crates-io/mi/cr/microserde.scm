(define-module (crates-io mi cr microserde) #:use-module (crates-io))

(define-public crate-microserde-0.1.13 (c (n "microserde") (v "0.1.13") (d (list (d (n "automod") (r "^1.0") (d #t) (k 2)) (d (n "microserde-derive") (r "=0.1.13") (d #t) (k 0)))) (h "0imc3yaiq32iiplgi0lpwai2j0y6w2j8qy54lh9yjz65092dcwxw")))

