(define-module (crates-io mi cr microsoft-cognitive-computer-vision) #:use-module (crates-io))

(define-public crate-microsoft-cognitive-computer-vision-0.1.0 (c (n "microsoft-cognitive-computer-vision") (v "0.1.0") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "16iah6isix2c4mh027nvdx8lssqw58qs38npsw4vfvbwcgj5qlss")))

