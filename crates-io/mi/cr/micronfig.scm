(define-module (crates-io mi cr micronfig) #:use-module (crates-io))

(define-public crate-micronfig-0.1.0 (c (n "micronfig") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1pi9qw9hhhwf7y0b5vnspax9a10kzgbdidy3rmrv31fg0231v4mh") (y #t)))

(define-public crate-micronfig-0.1.1 (c (n "micronfig") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "055r1s130yvl4z8ykhhj3q4pw26g3fi3hi0rs6g3aqjq7ff44xa9") (y #t)))

(define-public crate-micronfig-0.1.2 (c (n "micronfig") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1kc3aclqycbnc6pqjgrzyf88plc0xy9hfmgwsvs9zvgy6mbzmm89")))

(define-public crate-micronfig-0.2.0 (c (n "micronfig") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (o #t) (d #t) (k 0)))) (h "0mh184dqjizfxbrs43y8lgj413zycswihim9ijnwbgfyg8cc3wya") (f (quote (("testing" "tempfile") ("single_envvars") ("single_envfiles") ("multi" "single_envvars" "single_envfiles") ("macros" "lazy_static" "handle") ("handle" "multi") ("default" "single_envvars" "single_envfiles" "multi" "handle" "macros"))))))

(define-public crate-micronfig-0.3.0 (c (n "micronfig") (v "0.3.0") (d (list (d (n "micronfig_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1an0i55sf9a5fxgyipbac0zw3khpkzmwlg3h02l2aj46na8xnmpb") (f (quote (("envvars") ("envfiles") ("envdot" "regex") ("default" "envvars" "envfiles" "envdot"))))))

