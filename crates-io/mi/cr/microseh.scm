(define-module (crates-io mi cr microseh) #:use-module (crates-io))

(define-public crate-microseh-0.1.0 (c (n "microseh") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "1cg916s956y3aykhmajhxzznxf9ia53fqnf5ixgv0s50xz3z0gnb") (y #t)))

(define-public crate-microseh-0.1.1 (c (n "microseh") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0f0an9iqqpi1lqibq43y2bp1c5am78zl5nkzspnp1cygbiy2lszj")))

(define-public crate-microseh-0.2.0 (c (n "microseh") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1045z0xhghaxfr8snnl1ckcpm79fd2jbzmi1624lbi5l241dw5fw")))

(define-public crate-microseh-1.0.0 (c (n "microseh") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lx8iy0bizm3lxjcx5jkc2gmdjcs799gnkqlc97s2fm7jn39yrdm") (f (quote (("std") ("default" "std"))))))

(define-public crate-microseh-1.0.1 (c (n "microseh") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1508phi8pjzihhlf5s4xv681i0l3kds0aqdcl7d44kpvijjhgn4i") (f (quote (("std") ("default" "std"))))))

(define-public crate-microseh-1.0.2 (c (n "microseh") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "070m8nk5wraasjjc6abqas16yr3aw2iq4h49mry3l8ynlgjvw5nv") (f (quote (("std") ("default" "std"))))))

(define-public crate-microseh-1.0.3 (c (n "microseh") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j8s5pfi5iqrqiqfm10zsxnpc7c1v9r63kvb94vmsdzalvdw2hwy") (f (quote (("std") ("default" "std"))))))

