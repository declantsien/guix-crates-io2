(define-module (crates-io mi cr micromegas-derive-transit) #:use-module (crates-io))

(define-public crate-micromegas-derive-transit-0.1.0 (c (n "micromegas-derive-transit") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10z2laj53yniq72qm5cbg908mk0ska5h71rfxs5h6ki8xqi0glzx")))

(define-public crate-micromegas-derive-transit-0.1.1 (c (n "micromegas-derive-transit") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0j391fc6db1sfifvdwy78k8mmqmpa0p856r0hzv4lxjsfjy1jfrn")))

(define-public crate-micromegas-derive-transit-0.1.2 (c (n "micromegas-derive-transit") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1r4zlm44abc8wydgs12ksqn2zyymvf7a4klvgpg2p13jphzgmvin")))

(define-public crate-micromegas-derive-transit-0.1.3 (c (n "micromegas-derive-transit") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "01pf4sqww5w1183hdhkq5r63lyvqig31kyrrvmkd68gb4gja5zi7")))

