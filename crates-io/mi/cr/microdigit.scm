(define-module (crates-io mi cr microdigit) #:use-module (crates-io))

(define-public crate-microdigit-0.1.0 (c (n "microdigit") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0fzackpzpj2drrxl45jg05w9hqaqaqyc5bdbxsm3vni51n17qydf")))

