(define-module (crates-io mi cr micro-dns) #:use-module (crates-io))

(define-public crate-micro-dns-0.1.0 (c (n "micro-dns") (v "0.1.0") (h "18nwx35jl1yqb7bcphyb1vb1bwc1jdriwcwpvjqabb73fvzrzrjm")))

(define-public crate-micro-dns-0.1.5 (c (n "micro-dns") (v "0.1.5") (h "0c9sfni0gyp8zj9qzzvrsv37pkpbwmcb6zxmp7n7p0njbh9a7qkb")))

(define-public crate-micro-dns-0.1.1 (c (n "micro-dns") (v "0.1.1") (h "1g83mmcn0p15bpydr2js4arbxgm6224g0ncd5p3206dil6vvqw8y")))

(define-public crate-micro-dns-0.1.11 (c (n "micro-dns") (v "0.1.11") (h "0zvkm3xjddlh6g8m98j0xklr2525kk0rxlnz26rfanc9qdx9g9i5")))

