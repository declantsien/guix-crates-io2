(define-module (crates-io mi cr microtemplate) #:use-module (crates-io))

(define-public crate-microtemplate-1.0.0 (c (n "microtemplate") (v "1.0.0") (d (list (d (n "microtemplate_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0gpgc02055sbj5rkglx4q5y96218y0wr1xwpqadd0faa3ib5r7xd")))

(define-public crate-microtemplate-1.0.1 (c (n "microtemplate") (v "1.0.1") (d (list (d (n "microtemplate_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1psyhm5jfwvhzilawxz0rbgm43wd5270zxvs7w16frfm8qvlvj4p")))

(define-public crate-microtemplate-1.0.2 (c (n "microtemplate") (v "1.0.2") (d (list (d (n "microtemplate_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0h5ng0i1k75jgf972nddzx5jszps2a5slg25c6anhdr1nfbrxm08")))

(define-public crate-microtemplate-1.0.3 (c (n "microtemplate") (v "1.0.3") (d (list (d (n "microtemplate_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0val9p3as451xx50nfbp3bsyxg8bjyvfj34iymrzxw80f9ybs4yw")))

