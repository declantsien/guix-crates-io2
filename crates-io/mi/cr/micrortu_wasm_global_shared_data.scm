(define-module (crates-io mi cr micrortu_wasm_global_shared_data) #:use-module (crates-io))

(define-public crate-micrortu_wasm_global_shared_data-0.1.0 (c (n "micrortu_wasm_global_shared_data") (v "0.1.0") (d (list (d (n "ie_base") (r "^0.1.0") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "0s3shwliyy06v8mm22p60dcijbmwpc3v8k4crqqxb4y0lrq6dxqa")))

(define-public crate-micrortu_wasm_global_shared_data-0.1.1 (c (n "micrortu_wasm_global_shared_data") (v "0.1.1") (d (list (d (n "ie_base") (r "^0.1.1") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "0wnhcyalpy4rg90pnbph9r322b5bwih994ai0dlk4vg380razjps")))

(define-public crate-micrortu_wasm_global_shared_data-0.1.3 (c (n "micrortu_wasm_global_shared_data") (v "0.1.3") (d (list (d (n "ie_base") (r "^0.1.3") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "0l5nvnym8ng21gvz1bdfayyaja589j73ck5pf81733asyiwg0246")))

(define-public crate-micrortu_wasm_global_shared_data-0.1.4 (c (n "micrortu_wasm_global_shared_data") (v "0.1.4") (d (list (d (n "ie_base") (r "^0.1.4") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "079aszngpxiaay78rmnkzsjm6cw2xbpq3japmc5bmbra5036lr3h")))

(define-public crate-micrortu_wasm_global_shared_data-0.1.6 (c (n "micrortu_wasm_global_shared_data") (v "0.1.6") (d (list (d (n "ie_base") (r "^0.1.6") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "0w4pdk75idfpkdp9ggjrfr1qgp8i9c2i9qzy8ymf0cg1szx39chh")))

(define-public crate-micrortu_wasm_global_shared_data-0.1.7 (c (n "micrortu_wasm_global_shared_data") (v "0.1.7") (d (list (d (n "ie_base") (r "^0.1.7") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "1ffi8wbbw1z66w4qfq6nmiaa1d87kwsbwb19h9p42fp9cha0n96d")))

(define-public crate-micrortu_wasm_global_shared_data-0.3.0 (c (n "micrortu_wasm_global_shared_data") (v "0.3.0") (d (list (d (n "ie_base") (r "^0.3.0") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "08m9ij3dn4y0fpx0c31c92d1wdyp5mbw79k8mzwlrnggmvir7c5i") (y #t)))

(define-public crate-micrortu_wasm_global_shared_data-0.2.0 (c (n "micrortu_wasm_global_shared_data") (v "0.2.0") (d (list (d (n "ie_base") (r "^0.2.0") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "1h9ycjpd98h1krqky7nd8ccqajhddd0495ili2lbqsc2ylp307kz") (y #t)))

(define-public crate-micrortu_wasm_global_shared_data-0.2.1 (c (n "micrortu_wasm_global_shared_data") (v "0.2.1") (d (list (d (n "ie_base") (r "^0.2.1") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "1vcsih42p9rki8rw8mprnaqqg3nalydnd6vr887ppwzda057zhp1")))

(define-public crate-micrortu_wasm_global_shared_data-0.2.2 (c (n "micrortu_wasm_global_shared_data") (v "0.2.2") (d (list (d (n "ie_base") (r "^0.2.2") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "1p4kp5wiahvgqj62xcpjc9dshfqlyjlfiz16d4f0ac5hfkz03bys")))

(define-public crate-micrortu_wasm_global_shared_data-0.2.3 (c (n "micrortu_wasm_global_shared_data") (v "0.2.3") (d (list (d (n "ie_base") (r "^0.2.3") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "0fb79rig3mhm5igc1c808ixin9b92yiprm9kpkx8pk3wl1gdzzsj")))

(define-public crate-micrortu_wasm_global_shared_data-0.4.0 (c (n "micrortu_wasm_global_shared_data") (v "0.4.0") (d (list (d (n "ie_base") (r "^0.4.0") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "15yv00yd5kyl03wvf7kibdsyqnr4d4cqf59b2ljyl68mzi97570h")))

(define-public crate-micrortu_wasm_global_shared_data-0.4.1 (c (n "micrortu_wasm_global_shared_data") (v "0.4.1") (d (list (d (n "ie_base") (r "^0.4.1") (d #t) (k 0) (p "micrortu_ie_base")) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (k 0)))) (h "1ry524jcs8npbzcivmaixhmwmkhpnx676icyyg9wln7wwpsqnbk9")))

