(define-module (crates-io mi cr microlog) #:use-module (crates-io))

(define-public crate-microlog-1.0.0 (c (n "microlog") (v "1.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "07b8ic28wwcghk7nd2f6xjdxggf62vz9inw2xyp0z68nnxmgj49x") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-microlog-1.0.1 (c (n "microlog") (v "1.0.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1n24pv6fhyv0b2r5bkylnrmjh627551jk4ymx4gj4fczmv2bxzf6") (f (quote (("no-color" "colored/no-color"))))))

