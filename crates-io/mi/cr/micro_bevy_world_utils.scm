(define-module (crates-io mi cr micro_bevy_world_utils) #:use-module (crates-io))

(define-public crate-micro_bevy_world_utils-0.1.0 (c (n "micro_bevy_world_utils") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.9.0") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.9.0") (d #t) (k 0)))) (h "18flc47n774a6v2118ybvrqkh9am81la0r90ply2gr0gnwzdk9jp")))

(define-public crate-micro_bevy_world_utils-0.2.0 (c (n "micro_bevy_world_utils") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.10.1") (d #t) (k 0)))) (h "0h0mzmggl56h85l8wb42pf0lvnj0fp0awcgxlvm984y0xr9fjdfi")))

(define-public crate-micro_bevy_world_utils-0.2.1 (c (n "micro_bevy_world_utils") (v "0.2.1") (d (list (d (n "bevy_ecs") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.10.1") (d #t) (k 0)))) (h "0gj2kgq7wabqh8xvs6497jlhsak58nvvx2wrby9ggwmvgyx1vp8x")))

(define-public crate-micro_bevy_world_utils-0.3.0 (c (n "micro_bevy_world_utils") (v "0.3.0") (d (list (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.11.0") (d #t) (k 0)))) (h "1ycdy8xsai0inlig6ddnzdkfwz9iz901lyi9k871qxkkm6bv4aav")))

(define-public crate-micro_bevy_world_utils-0.4.0 (c (n "micro_bevy_world_utils") (v "0.4.0") (d (list (d (n "bevy_ecs") (r "^0.13") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "^0.13") (d #t) (k 0)))) (h "01wcxcv4bnakb6myanl82zaawd4ff7n53ai1cscbbmfmfgq8mg70")))

