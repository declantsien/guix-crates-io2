(define-module (crates-io mi cr microchip-tc72r-rs) #:use-module (crates-io))

(define-public crate-microchip-tc72r-rs-0.1.0 (c (n "microchip-tc72r-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0b5lvrsfmzz4ixv3rr9lcwrfrqag591pwq3hkhvv7h7wndyw2c2l")))

(define-public crate-microchip-tc72r-rs-0.1.1 (c (n "microchip-tc72r-rs") (v "0.1.1") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "00r9rsf01rvx5mx3y3bhrbywf9ma1vg1xxa3npijf1nqffjch1zp")))

(define-public crate-microchip-tc72r-rs-0.1.2 (c (n "microchip-tc72r-rs") (v "0.1.2") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1zga0vagdia53w5ngigjavp5vm8hs8a3ipivn3a53w6j9c083a77")))

(define-public crate-microchip-tc72r-rs-0.1.3 (c (n "microchip-tc72r-rs") (v "0.1.3") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1kvrai0vqlwyb2z8dnpmn7ymmhqcp9q2dl02rjlfyf84r4pqnwr7")))

