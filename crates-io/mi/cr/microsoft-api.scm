(define-module (crates-io mi cr microsoft-api) #:use-module (crates-io))

(define-public crate-microsoft-api-0.21.1 (c (n "microsoft-api") (v "0.21.1") (d (list (d (n "windows") (r "^0.21.1") (k 0)))) (h "1cn23my27a89fdvk0y6fa34rh1h3x6vwan1c4xk6k6wfcjpgc0hp") (y #t)))

(define-public crate-microsoft-api-0.1.0 (c (n "microsoft-api") (v "0.1.0") (h "0inn6cmz6hfxhx42jyc54bn35jqr7vx78x1mz5b7cff04hli0z5g")))

