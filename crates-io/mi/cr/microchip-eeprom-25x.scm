(define-module (crates-io mi cr microchip-eeprom-25x) #:use-module (crates-io))

(define-public crate-microchip-eeprom-25x-0.1.0 (c (n "microchip-eeprom-25x") (v "0.1.0") (d (list (d (n "bit_field") (r "~0.10") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1cz8sklar552rhflnvi6gbggy8k6g75ysa3yln0j0gqihmmzg04n")))

