(define-module (crates-io mi cr microformats-cli) #:use-module (crates-io))

(define-public crate-microformats-cli-0.8.2 (c (n "microformats-cli") (v "0.8.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "microformats") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "18hi32rj7hgxvcyylhalz2j7c573lvb9awj025arr1j6xlbpnpr7") (r "1.76")))

