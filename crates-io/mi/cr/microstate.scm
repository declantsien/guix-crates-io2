(define-module (crates-io mi cr microstate) #:use-module (crates-io))

(define-public crate-microstate-0.1.0 (c (n "microstate") (v "0.1.0") (h "1hqbsyy8zhbm84ar28gnnlmf2bgfpr3qxgxl7kpcki7xfaal70wk")))

(define-public crate-microstate-1.0.0 (c (n "microstate") (v "1.0.0") (h "1r9p4h53a84np0w7v3fwhancaxr2nwk9kymr05k117rgx7nlhxir")))

