(define-module (crates-io mi cr microledger) #:use-module (crates-io))

(define-public crate-microledger-0.1.0 (c (n "microledger") (v "0.1.0") (h "18m7znwvpvjwwgg44xs9cmaynjp6p4ilchi9i2jzxsxy7kmkgw0h")))

(define-public crate-microledger-0.1.1 (c (n "microledger") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 2)) (d (n "keri") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("std" "getrandom"))) (d #t) (k 2)) (d (n "said") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "10f79rxfkhlib15sdx7007akw87219abxbh0if25m9qa87nhqsqp")))

