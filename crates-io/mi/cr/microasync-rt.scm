(define-module (crates-io mi cr microasync-rt) #:use-module (crates-io))

(define-public crate-microasync-rt-0.2.0 (c (n "microasync-rt") (v "0.2.0") (d (list (d (n "async-core") (r "^0.3") (d #t) (k 0)) (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "0bfwhzscwsz769z6cfl6j5n4inrffjk9zvp4g2m4cqpfhdmfylw1") (f (quote (("no_std" "microasync/no_std" "async-core/no_std"))))))

(define-public crate-microasync-rt-0.2.1 (c (n "microasync-rt") (v "0.2.1") (d (list (d (n "async-core") (r "^0.3") (d #t) (k 0)) (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "00qb0x9cpw0ba90h3pg2ldpv4d9k2ll338v6l9v5df01p29cqm22") (f (quote (("no_std" "microasync/no_std" "async-core/no_std"))))))

