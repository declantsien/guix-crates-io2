(define-module (crates-io mi cr microsalt) #:use-module (crates-io))

(define-public crate-microsalt-0.1.0 (c (n "microsalt") (v "0.1.0") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "1hnm7a2ygwn1dzdasqd47fcxq74yb0mm1rzn1xr1b9yya6mg4mby")))

(define-public crate-microsalt-0.0.1 (c (n "microsalt") (v "0.0.1") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0fnljqyg6m3qg6c5apfnwdryqydymz7yh7a0mikg9sbzg52an9fj")))

(define-public crate-microsalt-0.1.1 (c (n "microsalt") (v "0.1.1") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "1znmikrg3n9dhasq9xjhfg4i6kq26sslws5wqqkvlziv219vl3js")))

(define-public crate-microsalt-0.1.2 (c (n "microsalt") (v "0.1.2") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)) (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "1v2z5b3wrv0mmgaxmk1qcb345snar7y9zwzyfxcqq1r3nfc6jpy7")))

(define-public crate-microsalt-0.1.3 (c (n "microsalt") (v "0.1.3") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vqq0kjq1znjsi3qkizix31sgwvxpjn316pqa9cjnwgz9nh1aifk")))

(define-public crate-microsalt-0.1.4 (c (n "microsalt") (v "0.1.4") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "18z9bz27d5y0fb9vrljwiz319bfmc5jhcyvws2k4vyswf03hnc9b")))

(define-public crate-microsalt-0.1.5 (c (n "microsalt") (v "0.1.5") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1xgdfss4gz3jn21d5hwm47b7fm8sfblqid5wbgpfw09qi16gi93l")))

(define-public crate-microsalt-0.1.6 (c (n "microsalt") (v "0.1.6") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0ykq358rskmmrpfjij1glcz7zi2vg9nr86f1f726gnxj3l7kn0h9")))

(define-public crate-microsalt-0.1.7 (c (n "microsalt") (v "0.1.7") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1hqi5jk12nqqrfjclifq1jh8knkwwllvrc42anczw995n2fvaj3h")))

(define-public crate-microsalt-0.1.8 (c (n "microsalt") (v "0.1.8") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0qz2n7nn3r7k68gydrdck041vppfnja5rwjmphnilhrihipb6j32")))

(define-public crate-microsalt-0.1.9 (c (n "microsalt") (v "0.1.9") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1r5bin1dcybil899lsa8ndh2yg9yi7abxygflfslr1q85q0ld716")))

(define-public crate-microsalt-0.2.0 (c (n "microsalt") (v "0.2.0") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "140cw65ab5r3nqlkpa14011k966wga124n85j3s7iaay4wv84wmw")))

(define-public crate-microsalt-0.2.1 (c (n "microsalt") (v "0.2.1") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "memsec") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1i9dbzf1x7gv73x9931ck37hfhsfd9lqzyw4zz4iwj12i9rkhk25")))

(define-public crate-microsalt-0.2.2 (c (n "microsalt") (v "0.2.2") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "memsec") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "141ncabz3gql3mv4209lndbz5yiv4bpns7hmgsbsr3n1si0g97j6")))

(define-public crate-microsalt-0.2.21 (c (n "microsalt") (v "0.2.21") (d (list (d (n "index-fixed") (r "0.0.*") (d #t) (k 0)) (d (n "memsec") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "05mjv140vl1y40261f1z8q8p78yy1l6z6p15mdy4mr0a6nhqh5ar")))

