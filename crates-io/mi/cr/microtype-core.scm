(define-module (crates-io mi cr microtype-core) #:use-module (crates-io))

(define-public crate-microtype-core-0.1.0 (c (n "microtype-core") (v "0.1.0") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "069c5qxk0n0r0pr56s1435i4i2ff6fzy07h86czfjy74spl3k4yx")))

(define-public crate-microtype-core-0.1.1 (c (n "microtype-core") (v "0.1.1") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "18lvgjqlcldgmg42ksq2638r019mcrdjw10pf3y76z63672150lc") (f (quote (("serde" "secrecy/serde"))))))

(define-public crate-microtype-core-0.1.2 (c (n "microtype-core") (v "0.1.2") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sp46rbkmyyv26rj6dpf8f8a2l0mip3svdisxbxbz7c6xw1q7571") (f (quote (("serde_support" "secrecy/serde" "serde"))))))

(define-public crate-microtype-core-0.1.3 (c (n "microtype-core") (v "0.1.3") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "13ikn23xxvmwz2j676hz25q2fd3if72b9pnbryj5braqisyakyyw") (f (quote (("serde" "secrecy/serde"))))))

(define-public crate-microtype-core-0.1.4 (c (n "microtype-core") (v "0.1.4") (d (list (d (n "secrecy") (r "^0.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "0qq5i261zqindxms8iz8194h452qpgz182j7fylqi0c7zx3ydxyh")))

(define-public crate-microtype-core-0.1.5 (c (n "microtype-core") (v "0.1.5") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "18razh1b9sgaksp913ac103snhc83m1vrp5ffanxzwsizpbz8vjs") (f (quote (("serde_support" "secrecy/serde"))))))

(define-public crate-microtype-core-0.1.6 (c (n "microtype-core") (v "0.1.6") (d (list (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "04qfl1cjncm3i8vc8v4z5wzp1q5rag0bbk5slggs1ik88m8yy0is") (f (quote (("serde_support" "secrecy/serde"))))))

