(define-module (crates-io mi cr microsoft-directx) #:use-module (crates-io))

(define-public crate-microsoft-directx-0.0.0-alpha.1 (c (n "microsoft-directx") (v "0.0.0-alpha.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11on12" "Win32_Graphics_Dxgi_Common" "Win32_Security"))) (d #t) (k 0)))) (h "1891k6gi8gzz5ajbf6s1varn12qlcr66z6sbkg794z61migncxzf") (f (quote (("implement"))))))

(define-public crate-microsoft-directx-0.0.0-alpha.2 (c (n "microsoft-directx") (v "0.0.0-alpha.2") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11on12" "Win32_Graphics_Dxgi_Common" "Win32_Security"))) (d #t) (k 0)))) (h "0fpa5b7ay6f7kln8m68a3dls15b8gxfv6s5anmah5r9sl9n7q351") (f (quote (("implement"))))))

(define-public crate-microsoft-directx-0.0.0-alpha.3 (c (n "microsoft-directx") (v "0.0.0-alpha.3") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11on12" "Win32_Graphics_Dxgi_Common" "Win32_Security"))) (d #t) (k 0)))) (h "0cvvpkfysbbzkvxvnsa6k8cprblw3jzjfac2sc953yyg7mxwgbcx") (f (quote (("implement"))))))

