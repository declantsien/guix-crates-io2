(define-module (crates-io mi cr microbit-text) #:use-module (crates-io))

(define-public crate-microbit-text-0.1.0 (c (n "microbit-text") (v "0.1.0") (d (list (d (n "tiny-led-matrix") (r "^1.0.0") (d #t) (k 0)))) (h "1n99vsxd62n8ld7r57w2xkjnig6k77hl2sjdmqrqbja9lhbh1vbf")))

(define-public crate-microbit-text-1.0.0 (c (n "microbit-text") (v "1.0.0") (d (list (d (n "tiny-led-matrix") (r "^1.0.0") (d #t) (k 0)))) (h "0h2cq0paj747w6lqrj4zfvgzfcnm1rc1z2il5czjbw5myxlv3782") (r "1.51")))

