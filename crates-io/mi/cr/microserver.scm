(define-module (crates-io mi cr microserver) #:use-module (crates-io))

(define-public crate-microserver-0.1.0 (c (n "microserver") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "warp") (r "^0.1.7") (d #t) (k 0)))) (h "0vbwaqlzf9ngv5g8ivpa872mbkp3i8l6fwklcbr29sw3awcw4zwd")))

(define-public crate-microserver-0.1.1 (c (n "microserver") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "warp") (r "^0.1.7") (d #t) (k 0)))) (h "0h8wqxscjy8wh0q900p3f2rn4valas8ybhzyjbc5chym7a3vcjic")))

(define-public crate-microserver-0.1.2 (c (n "microserver") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "warp") (r "^0.1.7") (d #t) (k 0)))) (h "14qprrwmd7dda29nv9s3myprcdyivmzbxfbp6bdqakbjkqv1hsc3")))

(define-public crate-microserver-0.1.3 (c (n "microserver") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "warp") (r "^0.1.9") (d #t) (k 0)))) (h "048h1w3ay9dcja485yfrby8gmgfjj9s5y40h03pnslb7zvg774j5")))

(define-public crate-microserver-0.1.4 (c (n "microserver") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "warp") (r "^0.1.9") (d #t) (k 0)))) (h "020j618rqa42xlz43cvnxhfsz0di1rzv6km1wv6xr4qyab6mbjrp")))

(define-public crate-microserver-0.1.5 (c (n "microserver") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.7.1") (d #t) (k 0)) (d (n "warp") (r "^0.1.9") (d #t) (k 0)))) (h "19ika2qj9bm5k34752gfl7gmvs1i5ljc7zb7cvnndasv1d9l4xrp")))

(define-public crate-microserver-0.1.6 (c (n "microserver") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "warp") (r "^0.1") (d #t) (k 0)))) (h "0y2n31n4zab52py61rbjicxz5anh1crl85przpjl6jkip4qzr342")))

(define-public crate-microserver-0.1.7 (c (n "microserver") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "05k95l8816gh7v93x23qn57qs0cg13k3ly64fvaaib6psqv6z2h4")))

(define-public crate-microserver-0.1.8 (c (n "microserver") (v "0.1.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "console") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "09aa50wghvrq239q0fhk70w4lr5h0k638ihnz8nx25bd2g80rbrh")))

(define-public crate-microserver-0.2.0 (c (n "microserver") (v "0.2.0") (d (list (d (n "clap") (r ">=2.33.0, <3.0.0") (d #t) (k 0)) (d (n "console") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0rll64d2qchsssdaqb8kbgv784jpr8zb375i6ii9vblpjpznjirb")))

(define-public crate-microserver-0.2.1 (c (n "microserver") (v "0.2.1") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1zxzy9lqc7zirvk0kqikc39a79ba00rcnzdwvv1l07gd9b1lcnbw") (r "1.59.0")))

