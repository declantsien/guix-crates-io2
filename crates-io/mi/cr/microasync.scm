(define-module (crates-io mi cr microasync) #:use-module (crates-io))

(define-public crate-microasync-0.1.0 (c (n "microasync") (v "0.1.0") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "1iy7806wnc2nl23arzv5jms4rim3sxl219hjlm3vbv15am7vps65")))

(define-public crate-microasync-0.2.0 (c (n "microasync") (v "0.2.0") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "1vjw8slgcfqm2l6ps1q5jqqh9r9xs0kr3mcqlx9h0pf87d8i610y")))

(define-public crate-microasync-0.2.1 (c (n "microasync") (v "0.2.1") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "1zjwbqljmm0panzzsazq5zwn9bbgd1lckrjgsw2ygsfl46gmwpsp")))

(define-public crate-microasync-0.2.2 (c (n "microasync") (v "0.2.2") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "0bga8lf8agdb726m6bmmxd7shnhdkfrixbq9r3ylys1bls1mb1v3")))

(define-public crate-microasync-0.2.3 (c (n "microasync") (v "0.2.3") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "1jymhxrzq47c78vn47431igqv04rr47bc1shwyw3lrlygmb37k76")))

(define-public crate-microasync-0.2.4 (c (n "microasync") (v "0.2.4") (h "1y455wpg5p7i67s7b9jg0j8dmws6kz68nfwnm1qvjx36xb7q59cx")))

(define-public crate-microasync-0.2.5 (c (n "microasync") (v "0.2.5") (h "03f3k64139ff9r8c5vgn71wqq28fgi5p7hvygvprqz246cwcgkvv")))

(define-public crate-microasync-0.2.6 (c (n "microasync") (v "0.2.6") (h "1iaq7arrycv31wyaw1brk77q7aq6vml95dm86n57zm0d1lv6351x")))

(define-public crate-microasync-0.2.7 (c (n "microasync") (v "0.2.7") (h "0vbvsa22wfhjvasdvbilcgs3ng36400xq0bcp42h32wzy3d9yvs2") (f (quote (("no_std"))))))

(define-public crate-microasync-0.3.0 (c (n "microasync") (v "0.3.0") (h "133xb2y015chmdx1g88bi526pmaaq0ibbn1cksx7qfcwl61q86vc") (f (quote (("no_std") ("no_joiner"))))))

(define-public crate-microasync-0.3.1 (c (n "microasync") (v "0.3.1") (h "18wshyw0vi5xcvr09cliwgwzx02cyq7r4mn95sk8fdja6va9cn8z") (f (quote (("no_std") ("no_joiner"))))))

(define-public crate-microasync-0.4.0 (c (n "microasync") (v "0.4.0") (h "0vzykqdk22y3vlz3dvc3ywsad61yzvbnlylzcr72jljmc75jm29d") (f (quote (("no_std") ("no_joiner"))))))

(define-public crate-microasync-0.4.1 (c (n "microasync") (v "0.4.1") (h "1zqazvgrf7rd98724k85ka61w86h6vz81iissj5diwkl30bm13ss") (f (quote (("no_std") ("no_joiner"))))))

(define-public crate-microasync-0.4.2 (c (n "microasync") (v "0.4.2") (h "0kzs73jfadw47qy1i87gdx3czml5w2n5q3iqh93wfy83qn3115ij") (f (quote (("no_std") ("no_joiner"))))))

