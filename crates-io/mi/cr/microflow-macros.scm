(define-module (crates-io mi cr microflow-macros) #:use-module (crates-io))

(define-public crate-microflow-macros-0.1.1 (c (n "microflow-macros") (v "0.1.1") (d (list (d (n "byterepr") (r "^0.1") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "simba") (r "^0.8") (d #t) (k 0)) (d (n "structmeta") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08yw57xiic3m9xls6samb65adzvn3lh7cqb7zkx8qydzx7pdq6wd")))

