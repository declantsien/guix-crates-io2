(define-module (crates-io mi cr microwfc) #:use-module (crates-io))

(define-public crate-microwfc-0.1.0 (c (n "microwfc") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bi8cicrij5myi3f7mglrb4nnjhs9ycbg5km0zl42ch3aixh5djy") (y #t)))

(define-public crate-microwfc-0.1.1 (c (n "microwfc") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0q3zdkbq7v0yrvcwp5jzw2yxri823866zfzqjfv6skz74hi7j1ar") (y #t)))

(define-public crate-microwfc-0.1.2 (c (n "microwfc") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "05p2d0rw9vr75fwj6cifn48gdaq5f27q5ddd7v01w16va8cgsr28") (y #t)))

(define-public crate-microwfc-0.2.0 (c (n "microwfc") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0crqcq8kqpj09zxdyr62ip4n5nkpzvfi1sxxpz954a74fs6z24z8") (y #t)))

(define-public crate-microwfc-0.2.1 (c (n "microwfc") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jbkpagc594qb6s2iwd6db2432fc4wlx1hlabr1h7x5ybnsffp9v")))

(define-public crate-microwfc-0.3.0 (c (n "microwfc") (v "0.3.0") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1idhqzslhfhbw6ji4v8fpiwn2wmhqs0n3al44k5c85ckfdqqh9zq")))

(define-public crate-microwfc-0.3.1 (c (n "microwfc") (v "0.3.1") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06z6vypgd2x4564r7cyary7xy7nc3ixm55rgkgqwx1cs2545dmmv")))

(define-public crate-microwfc-0.4.0 (c (n "microwfc") (v "0.4.0") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1s6faiqpgirs4xvb1al1bvnfwl0dh0f1478ljdfwlv5ni3bfi6ph")))

(define-public crate-microwfc-0.5.0 (c (n "microwfc") (v "0.5.0") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17crqas0mn7p775n125znsa87p3mcn6jbyblr2yhy0ky8cbpbjrc")))

(define-public crate-microwfc-0.6.0 (c (n "microwfc") (v "0.6.0") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0cy4ymaksfgyhfnv7jcwyjvisma7xr681064qan72y5z8cgvv219")))

(define-public crate-microwfc-0.6.1 (c (n "microwfc") (v "0.6.1") (d (list (d (n "micro_ndarray") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1rvm38br0myijlcz0306c98wqdprxx4208gvd4vgkkc6gwma6cks")))

