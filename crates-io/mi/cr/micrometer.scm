(define-module (crates-io mi cr micrometer) #:use-module (crates-io))

(define-public crate-micrometer-0.1.0 (c (n "micrometer") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1pi2g0qs6yxc60n6za2lvafgmcsb3p9abnkpqx4zy0zxmnd1lngj") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.0 (c (n "micrometer") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "08z8q6gyhp4wy67xwd4ylc1ssqd0x1hwi6233ikd69f5081q9srj") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.1 (c (n "micrometer") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "0fklf6c7h39qizbs0jyqxd794kcssa1kn1slzw59g8r62x2jb7fk") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.2 (c (n "micrometer") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "0wvqqyrinbm9zlps41dpb15kr8bznrpj22v4vwyv8979fhjcav1j") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.3 (c (n "micrometer") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "152a2azb59dx0cfc7h6f95xqscbc2lisyv0ckirn0ijylp9n1lp6") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.4 (c (n "micrometer") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1hqwpaqawsqjijhcs63gzx97jnw1j42m83xnj3mdhg77bkc3f499") (f (quote (("perforation") ("instant") ("enable") ("default" "perforation" "instant"))))))

(define-public crate-micrometer-0.2.5 (c (n "micrometer") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1r3igliklwklhq886dn3b7jji6mdbvfry8wplckrzfhazfsbay7f") (f (quote (("perforation-1k" "perforation") ("perforation-16k" "perforation") ("perforation-128" "perforation") ("perforation") ("instant") ("enable") ("default" "perforation-1k" "instant"))))))

(define-public crate-micrometer-0.2.6 (c (n "micrometer") (v "0.2.6") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1ih9sl6rr4d71cn4fdsc3qg207p6048901v2l1s8fd6kf3qz404l") (f (quote (("perforation-1k" "perforation") ("perforation-16k" "perforation") ("perforation-128" "perforation") ("perforation") ("instant") ("enable") ("default" "instant"))))))

(define-public crate-micrometer-0.2.7 (c (n "micrometer") (v "0.2.7") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "0xcn9cbdlss7r700nmq7cb0nskfj5r91809g1nv0b2jj2qzjkcc6") (f (quote (("perforation-1k" "perforation") ("perforation-16k" "perforation") ("perforation-128" "perforation") ("perforation") ("instant") ("enable") ("default" "instant"))))))

