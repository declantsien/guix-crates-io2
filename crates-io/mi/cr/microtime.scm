(define-module (crates-io mi cr microtime) #:use-module (crates-io))

(define-public crate-microtime-0.1.0 (c (n "microtime") (v "0.1.0") (h "0vf87j3vqkx0jqwhp7n18rqhfraak0bp6prkh9a5wpn6s3712630")))

(define-public crate-microtime-0.2.0 (c (n "microtime") (v "0.2.0") (h "0rr783f7rdjddnq83g42w2b52hr5mvaax2wcff410q8jqq9nymjl")))

(define-public crate-microtime-0.3.0 (c (n "microtime") (v "0.3.0") (h "09l10j1yinz5lc7x2kd8zhf02f2l3g4dwq0mh1nq5pmmfygm0n2x") (f (quote (("std"))))))

