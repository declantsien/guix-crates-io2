(define-module (crates-io mi cr micro_ndarray) #:use-module (crates-io))

(define-public crate-micro_ndarray-0.1.0 (c (n "micro_ndarray") (v "0.1.0") (h "117s3lxbd7bmsdlqx7c3n2dnf4x68hql21vrvm2xa1843bv55fq0")))

(define-public crate-micro_ndarray-0.2.0 (c (n "micro_ndarray") (v "0.2.0") (h "1sx6myknlndh54rbr9v5hfdyhaf3r4mbyi0w91ddhgf8jsxadick")))

(define-public crate-micro_ndarray-0.2.1 (c (n "micro_ndarray") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "1xlwn2c3rlqk5mb2hab3aii2pflb4wmdm6f3plvjrdwzfc2c5qpk")))

(define-public crate-micro_ndarray-0.2.2 (c (n "micro_ndarray") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "0n2y167v1j6s52bmda9qlynxl3bd3kaa26vkxc89d0d7w3rcjvqm")))

(define-public crate-micro_ndarray-0.2.3 (c (n "micro_ndarray") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "174xl3d1c52z3zjd04v1xkcfd2rbwzpv4qrzwqgqqpzrvs6f15pr")))

(define-public crate-micro_ndarray-0.2.4 (c (n "micro_ndarray") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "1hiqkl4cgfzdd9w6i787qxfwr4x09x79v9w9gkg0w2l3kqm7cy52")))

(define-public crate-micro_ndarray-0.2.6 (c (n "micro_ndarray") (v "0.2.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "1g4nbpp2x36vf78i517hq7idzvhdkkac7qbgsw5ygbv15cf9fan2")))

(define-public crate-micro_ndarray-0.2.7 (c (n "micro_ndarray") (v "0.2.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "0sq2f6zs0rk26zp3dl5q90xh624jr4kbsis3dx2syljjpc43kf9r")))

(define-public crate-micro_ndarray-0.2.8 (c (n "micro_ndarray") (v "0.2.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)))) (h "1q3n26zdxmwn91rcarzyj7m31q5nm6vfpkh7xjcp86dfql37sj14")))

(define-public crate-micro_ndarray-0.3.0 (c (n "micro_ndarray") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "vec_split") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1m6rym8qr439y2jck6cc3iyjb7c12044ypig0vkx7g6clvbpfay8") (f (quote (("default" "vec_split")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.3.1 (c (n "micro_ndarray") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fmjzbqfd02zmyigwfm02i0swvjvkv514kvc7n498w0fagf4l4dh") (f (quote (("default" "vec_split")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.3.2 (c (n "micro_ndarray") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0s9vvvc5k1fbfx9xp087gh3xd10yhklhga6akn2k6w63kv91c4f9") (f (quote (("default" "vec_split")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.3.3 (c (n "micro_ndarray") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "05vc7dyy28n27nz24lf5s3b4b2qqsyi7cms77ikw6jc8gw3pl6vj") (f (quote (("default" "vec_split")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.4.0 (c (n "micro_ndarray") (v "0.4.0") (d (list (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wr6dk56qi0x378wa6811rwqmy53bldb7fqnn4cg9p9panf7ignk") (f (quote (("default" "vec_split")))) (y #t) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.4.1 (c (n "micro_ndarray") (v "0.4.1") (d (list (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0187gdgcal42z1mkg95zawy1rnn72kia43s51k55dii05wzmpp9x") (f (quote (("default" "vec_split")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.5.0 (c (n "micro_ndarray") (v "0.5.0") (d (list (d (n "ident_concat") (r "^0.2") (d #t) (k 0)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1m60q7v3yl94y35fn6whvnsl2pgqhr4m1ggxbm9laynaizyb0ifb") (f (quote (("default" "vec_split") ("allocator")))) (y #t) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.5.1 (c (n "micro_ndarray") (v "0.5.1") (d (list (d (n "ident_concat") (r "^0.2") (d #t) (k 0)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jy77xy3wpaa9ial7ca9g279k3ri0qrh82bjvgj42z4gagc3hjgb") (f (quote (("default" "vec_split") ("allocator")))) (y #t) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.6.0 (c (n "micro_ndarray") (v "0.6.0") (d (list (d (n "ident_concat") (r "^0.2") (d #t) (k 0)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1b5ps0bw4ldvaq0m9awclx8kzc5zsmzpkh4kmnvbsq1v2zlp8mca") (f (quote (("default" "vec_split") ("allocator")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

(define-public crate-micro_ndarray-0.6.1 (c (n "micro_ndarray") (v "0.6.1") (d (list (d (n "ident_concat") (r "^0.2") (d #t) (k 0)) (d (n "vec_split") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nfa279xcnr7nfxh97702pnpcaa4jgaha9d6xvfw1mv4nmpvp4wz") (f (quote (("default" "vec_split") ("allocator")))) (s 2) (e (quote (("vec_split" "dep:vec_split"))))))

