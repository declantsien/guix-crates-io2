(define-module (crates-io mi cr microsoft-direct3d) #:use-module (crates-io))

(define-public crate-microsoft-direct3d-0.0.0-alpha.2 (c (n "microsoft-direct3d") (v "0.0.0-alpha.2") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11on12" "Win32_Graphics_Dxgi_Common" "Win32_Security"))) (d #t) (k 0)))) (h "0ap0gwfifgwkvl0f42ic3rpcm0f0703wpw7qchbrjl5kcpg5g2cy") (f (quote (("implement"))))))

