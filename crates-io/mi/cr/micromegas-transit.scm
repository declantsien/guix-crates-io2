(define-module (crates-io mi cr micromegas-transit) #:use-module (crates-io))

(define-public crate-micromegas-transit-0.1.0 (c (n "micromegas-transit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "micromegas-derive-transit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fgms2sf678cfnfffkhwdhry46f5bqypp2lqdaid533vqq1zk90b")))

(define-public crate-micromegas-transit-0.1.1 (c (n "micromegas-transit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "micromegas-derive-transit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fqprjw5qkq5cdq71p4i2014ra14xi6ivgjczjvvypd099b8kvhf")))

(define-public crate-micromegas-transit-0.1.2 (c (n "micromegas-transit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "micromegas-derive-transit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0029r02pcg91zqqm7h0hk3pwjq0z5yxsa8mzgzgrcgl46gvwlbbm")))

(define-public crate-micromegas-transit-0.1.3 (c (n "micromegas-transit") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "micromegas-derive-transit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1k128k81mavl5p7fg37pfnjviidxa39pli0la6vxbkash31p7zpg")))

