(define-module (crates-io mi cr micro-tower-codegen) #:use-module (crates-io))

(define-public crate-micro-tower-codegen-0.1.0 (c (n "micro-tower-codegen") (v "0.1.0") (d (list (d (n "darling") (r "~0.14.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.21") (d #t) (k 0)) (d (n "syn") (r "~1.0.99") (d #t) (k 0)))) (h "0mqnn6a472jzj068v6nw2aiw8bwy171gr395wvv8n22cv06705jr")))

