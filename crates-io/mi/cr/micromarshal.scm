(define-module (crates-io mi cr micromarshal) #:use-module (crates-io))

(define-public crate-micromarshal-0.1.0 (c (n "micromarshal") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qmk40p2bhkkdk3h33hdlvqcjr42bz3bqrvcgs4xj8d9jb2hnhkp")))

(define-public crate-micromarshal-0.2.0 (c (n "micromarshal") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1asglgcjgz5jx938l3bp3pdjf4rkylz7qmnavp6hv3kliznbklrv")))

(define-public crate-micromarshal-0.2.1 (c (n "micromarshal") (v "0.2.1") (d (list (d (n "ordered-float") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nfjqbj0zdilmc1yrz8cpz9rmg1j65ng34iy8sdbg51sjrv08pl5")))

(define-public crate-micromarshal-0.3.0 (c (n "micromarshal") (v "0.3.0") (d (list (d (n "ordered-float") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ksy285da2gbyp8jq1wwv9plpb5rhj8rf56r5yg0p3z3mjmdax7z")))

(define-public crate-micromarshal-0.4.0 (c (n "micromarshal") (v "0.4.0") (d (list (d (n "ethnum") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cax7bfs11im4mhhla4yiiq52m464fg2nzkrsassz04vdq0ffxr7")))

(define-public crate-micromarshal-0.4.1 (c (n "micromarshal") (v "0.4.1") (d (list (d (n "ethnum") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1sb570zzkaq4fgam40z83flgk7d641bm956m7zsari9gnh8k0yd6")))

(define-public crate-micromarshal-0.5.0 (c (n "micromarshal") (v "0.5.0") (d (list (d (n "ethnum") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0i6cljqn4p9plwyp7q679xjd9z0zr2cag59h0z73ci2vlr1k8wq8")))

(define-public crate-micromarshal-0.6.0 (c (n "micromarshal") (v "0.6.0") (d (list (d (n "ethnum") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hgm0cky113chh0hlc2nrh70qp2d4r5s0v379gy4wby9pgrb9rqv")))

(define-public crate-micromarshal-0.7.0 (c (n "micromarshal") (v "0.7.0") (d (list (d (n "ethnum") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v4rlfs60lxa0gri988gycjfkal3yl4hiwxda964d0yv6g4r6dl6")))

