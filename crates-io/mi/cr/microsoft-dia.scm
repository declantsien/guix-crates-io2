(define-module (crates-io mi cr microsoft-dia) #:use-module (crates-io))

(define-public crate-microsoft-dia-0.0.0 (c (n "microsoft-dia") (v "0.0.0") (h "1lhc2ihkzfkqh313rhmjia3f5qf08x7lwjbn6fykvq7bsyf8qymy")))

(define-public crate-microsoft-dia-0.1.0 (c (n "microsoft-dia") (v "0.1.0") (d (list (d (n "windows") (r "^0.41.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1dq6fql6h6qivz0d3mm2i1xyimww5cbrjaz4pz44awkdnbv7w2nf") (f (quote (("implement"))))))

(define-public crate-microsoft-dia-0.2.0 (c (n "microsoft-dia") (v "0.2.0") (d (list (d (n "windows") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1khpwpvnswjw7h7ig60lfr8bbzd68qn9p43pc7ipjc6f7cj0iq0d") (f (quote (("implement"))))))

(define-public crate-microsoft-dia-0.3.0 (c (n "microsoft-dia") (v "0.3.0") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "03d2af7p4f0gr217q2y77hmk1fmswfcns0f1w225pm0s0r25na53") (f (quote (("implement"))))))

(define-public crate-microsoft-dia-0.4.0 (c (n "microsoft-dia") (v "0.4.0") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("implement" "Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1s2i2qcbbd7r01m63gccl0la0fvcyjd4mv1n93ybw7b070wniqrm")))

(define-public crate-microsoft-dia-0.5.0 (c (n "microsoft-dia") (v "0.5.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("implement" "Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1bswvxk17rj0a07skk373k7jbqf9yzsrm7szspkc3q3nszr0j5ll")))

(define-public crate-microsoft-dia-0.6.0 (c (n "microsoft-dia") (v "0.6.0") (d (list (d (n "windows") (r "^0.47.0") (f (quote ("implement" "Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0dkfdd567gm4zhrnhh3rry6y48q8s0764xz51xwqqmxghaff7rr5")))

(define-public crate-microsoft-dia-0.8.0 (c (n "microsoft-dia") (v "0.8.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("implement" "Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0a434c0yjdfhp9r2w5lmzyv72zrgwsa0z034psi8rw68ik4z66vs")))

(define-public crate-microsoft-dia-0.10.0 (c (n "microsoft-dia") (v "0.10.0") (d (list (d (n "windows") (r "^0.53") (f (quote ("implement" "Win32_Foundation" "Win32_System_Com" "Win32_System_Variant" "Win32_System_LibraryLoader" "Win32_System_Com_StructuredStorage" "Win32_System_Ole"))) (d #t) (k 0)) (d (n "windows-bindgen") (r "^0.53") (d #t) (k 1)) (d (n "windows-core") (r "^0.53") (f (quote ("implement"))) (d #t) (k 0)))) (h "1wms5z6dsn1bk22f01a3iq1dr5qjz8ljh7kqkj1a0zj78nanfskb")))

