(define-module (crates-io mi cr micrograd-rs) #:use-module (crates-io))

(define-public crate-micrograd-rs-0.0.2 (c (n "micrograd-rs") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w6g8sp5vw851j5kmw2mg6vzyq87bl9fkmcjw5r6qswv9l0hd31p")))

(define-public crate-micrograd-rs-0.0.3 (c (n "micrograd-rs") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0d3q56p4x41ig9aqs3sks8mcblnxiywjm70jdrcfcrx3zcza6v2g")))

