(define-module (crates-io mi cr microdsp) #:use-module (crates-io))

(define-public crate-microdsp-0.1.0 (c (n "microdsp") (v "0.1.0") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0hn4a5sdb8rf4f92d9ap5mq2f67appkkc5grdpyam400c2w297lw")))

(define-public crate-microdsp-0.1.1 (c (n "microdsp") (v "0.1.1") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1m03mks74xdl49dviljiql8yqxr57sn17pz59qmhmwzbsliqflsh")))

(define-public crate-microdsp-0.1.2 (c (n "microdsp") (v "0.1.2") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ikxlk9v6grhm6fsix2k4d1ivmd19pjhyj4srsi7np9k0y0rn9y7")))

