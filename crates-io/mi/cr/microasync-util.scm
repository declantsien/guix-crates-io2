(define-module (crates-io mi cr microasync-util) #:use-module (crates-io))

(define-public crate-microasync-util-0.1.0 (c (n "microasync-util") (v "0.1.0") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "0iz014nwwr4zs38wj2gw6krgqc7s96m1brfvpwqpb5dqzlngzm8q") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.1 (c (n "microasync-util") (v "0.1.1") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "1nzwiiw3l8l47wab1javcslrdiqlbpjamb4m9xiy56aspjhh8kh6") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.2 (c (n "microasync-util") (v "0.1.2") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "11mga84rlcnvgy915vfsd49p7l71vfg0ai2jbx7vwan4165wvhvj") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.3 (c (n "microasync-util") (v "0.1.3") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "0dhih5y151vhkh5m09p62zmvvjzj1sgyg3jw1gw9vr6h74fa6wlp") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.4 (c (n "microasync-util") (v "0.1.4") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "1z3r56g7d8y3qjs3nga9yni9zkzf2n8ql2g6lnjpzskgy8jx68kd") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.5 (c (n "microasync-util") (v "0.1.5") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "1gcriqd6piynkid00z95k8r2g6h26ilij6l1kacby042wd7h063s") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.6 (c (n "microasync-util") (v "0.1.6") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "0vh7wwk1m8hgmmy6jnaih2ka9a3051gq825g0fx2zgbp99fx0yy3") (f (quote (("no_std" "microasync/no_std"))))))

(define-public crate-microasync-util-0.1.7 (c (n "microasync-util") (v "0.1.7") (d (list (d (n "microasync") (r "^0.4") (d #t) (k 0)))) (h "016aj1x0kyvdhrf3b7vsgrwwhv8xyvmh9awk94593rnfqylrwx1g") (f (quote (("no_std" "microasync/no_std"))))))

