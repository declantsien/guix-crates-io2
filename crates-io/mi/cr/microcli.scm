(define-module (crates-io mi cr microcli) #:use-module (crates-io))

(define-public crate-microcli-0.1.0 (c (n "microcli") (v "0.1.0") (h "1aazqfkdnh4a4r2ccr7mxshclqw5i62s48gs5idmyypb6v2v4ada") (y #t)))

(define-public crate-microcli-0.1.1 (c (n "microcli") (v "0.1.1") (d (list (d (n "brightcli") (r "^0.1") (d #t) (k 0)) (d (n "gitbranch") (r "^0.1") (d #t) (k 0)) (d (n "i3quitdialog") (r "^0.1") (d #t) (k 0)) (d (n "lidwatch") (r "^0.1") (d #t) (k 0)))) (h "1smw7rzb8lm2xjvqc4pw5y37ig93p91r0bkljihasbgwa1qjlqn0") (y #t)))

(define-public crate-microcli-0.1.2 (c (n "microcli") (v "0.1.2") (h "1zb8d7q2i3himbbivfjpmdmrzxg0j3npk659vnsx1m2xn2j1gzir") (y #t)))

(define-public crate-microcli-0.1.3 (c (n "microcli") (v "0.1.3") (h "0yv44bys213xib3a4xcdhd08npchz1bg3bpm59brwnkj7lqpkg8w") (y #t)))

(define-public crate-microcli-0.1.4 (c (n "microcli") (v "0.1.4") (h "0zd8s83sb8m65a06fkqyhxpm2n58rgbh072pf63py5gck1pyvx9b") (y #t)))

(define-public crate-microcli-0.1.5 (c (n "microcli") (v "0.1.5") (h "1h39kv4g8j57ak22i326pjgxsp2kizlgwfvyysbl952k1iciydrw") (y #t)))

(define-public crate-microcli-0.1.6 (c (n "microcli") (v "0.1.6") (h "1a478q8srqmgmzg4gagh0dq396z697y2g0hp4slq8035ybmslkp6") (y #t)))

(define-public crate-microcli-0.1.7 (c (n "microcli") (v "0.1.7") (h "0hnx39mlpmpiz6qf7gmz8n3csj5mxqwn3mbdxp3ds659wpq6va5a") (y #t)))

(define-public crate-microcli-0.1.8 (c (n "microcli") (v "0.1.8") (h "1242yk70h6qwmk06pdkmmjix6y5p0v6g49npyk5sddslak1gpdmb")))

(define-public crate-microcli-0.1.9 (c (n "microcli") (v "0.1.9") (h "1yi7xd51xp1rdla62kr4pkww1b2q4jipiizac3w6nrjrn4hkcsqd")))

(define-public crate-microcli-0.1.10 (c (n "microcli") (v "0.1.10") (h "1m2z475l4pwx0h8v8xczinn9jdlv8fgggyq489qhhw561ynvp59v")))

(define-public crate-microcli-1.0.1 (c (n "microcli") (v "1.0.1") (h "0b8c8wad9x5drdy4i8p6cr7ixza33klswsd4m6x6mg75mryb3b81")))

