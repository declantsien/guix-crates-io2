(define-module (crates-io mi cr microjson) #:use-module (crates-io))

(define-public crate-microjson-0.1.0 (c (n "microjson") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jpmm6qw1f75dxpvrclffghfcvzzd1dg5la6panbc98jmn1c20fz")))

(define-public crate-microjson-0.1.1 (c (n "microjson") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1rsarw2rcd0g3hr4rk9sa79x8y3gmghivj2fsnpymz81292a4pbq")))

(define-public crate-microjson-0.1.2 (c (n "microjson") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0b3b8gy6y48zkc5mi6wcni9s0vwd9barmw95gywkkq6f4x9v3c7a")))

(define-public crate-microjson-0.1.6 (c (n "microjson") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0b2hw67zci5ila6zjzkaiz8f2y8hfaawxdw173h9y8vk49khgxk6") (f (quote (("std") ("small_number_parsing"))))))

