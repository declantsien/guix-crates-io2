(define-module (crates-io mi cr micro_http_server) #:use-module (crates-io))

(define-public crate-micro_http_server-0.0.1 (c (n "micro_http_server") (v "0.0.1") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "0zfzxrsssibsrfaw2f2czh6a9q9132p7lwpva5wxa7bifn392c3f")))

(define-public crate-micro_http_server-0.0.2 (c (n "micro_http_server") (v "0.0.2") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "06jv7givbvdiw4mnl2rbhd30idqhvv6z7biam2pj23ygr8yk3f72")))

(define-public crate-micro_http_server-0.0.3 (c (n "micro_http_server") (v "0.0.3") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "0a1f8cvhhysbqs46qsa3nxvxrgvfa5qyc09f9pqq8ybpipw3kggr")))

(define-public crate-micro_http_server-0.0.4 (c (n "micro_http_server") (v "0.0.4") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "0ni44337cr7jx4g6sbm28l5rmfc8b694srjl3vrral1516lkjqwm")))

(define-public crate-micro_http_server-0.0.5 (c (n "micro_http_server") (v "0.0.5") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "0qpxzcj3490pkxqfz3lrvqc540xyl4kw83rzgfa0dcawaml0lrb1")))

