(define-module (crates-io mi cr microchip-eeprom-25lcxx) #:use-module (crates-io))

(define-public crate-microchip-eeprom-25lcxx-0.1.0 (c (n "microchip-eeprom-25lcxx") (v "0.1.0") (d (list (d (n "bit_field") (r "~0.10") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1y7vls6cs4z6vd6dmfbcjj769say4fyqb8p9qabbc752kysmmd2q")))

