(define-module (crates-io mi cr microstack) #:use-module (crates-io))

(define-public crate-microstack-0.0.1 (c (n "microstack") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "0h5bh1xxkpp62glcjr3i9z8xsdx0cl2ksgyi4jll5shcpqsa047s")))

(define-public crate-microstack-0.0.2 (c (n "microstack") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "1cbww5nqf51gk30wh2735cxg3dp88vjrsvhjk8199brh6y0sn60g")))

(define-public crate-microstack-0.0.3 (c (n "microstack") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "0ih5clgvnm4rkrbrqzvh4db2c9vdy9skb30p7fm3w3mgs9b05ly5")))

(define-public crate-microstack-0.0.4 (c (n "microstack") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "0z0x723c0qw66vf8bk0mkqy12p63qgh6xcwpjmh2c3s3r33zknwl")))

(define-public crate-microstack-0.0.5 (c (n "microstack") (v "0.0.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "075v9aczrc58705023m7dm3p32brjk52cj3g4m0qm24drp9rcga6")))

(define-public crate-microstack-0.0.6 (c (n "microstack") (v "0.0.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "1zhsswrqw0dnixcbrlzngv80rc8bl8gh7af4wqcmxw3z8529qkxm")))

(define-public crate-microstack-0.0.7 (c (n "microstack") (v "0.0.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (o #t) (k 0)))) (h "17nmnn3gds8qqx3sc27b0qbknp2chyld446mg0dl1hscf4c0wk5q")))

