(define-module (crates-io mi cr micrrou) #:use-module (crates-io))

(define-public crate-micrrou-0.1.0 (c (n "micrrou") (v "0.1.0") (d (list (d (n "ang") (r "^0.5") (d #t) (k 0)) (d (n "macrrou") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nannou") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "test-case") (r "^1.1") (d #t) (k 2)))) (h "1p32m39yd4pcv3bzcbdkzl29l384vxn9jvdyxs23fkwzk2fmp3q0") (f (quote (("macro" "macrrou") ("default")))) (r "1.60")))

