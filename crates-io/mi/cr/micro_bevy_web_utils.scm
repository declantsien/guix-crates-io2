(define-module (crates-io mi cr micro_bevy_web_utils) #:use-module (crates-io))

(define-public crate-micro_bevy_web_utils-0.1.0 (c (n "micro_bevy_web_utils") (v "0.1.0") (d (list (d (n "bevy_ecs") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_input") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_math") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "1kiq0szx6zcgl1rzz0px1fawzi2zljii8h1kc7ii2vzhnk7m4gns")))

(define-public crate-micro_bevy_web_utils-0.2.0 (c (n "micro_bevy_web_utils") (v "0.2.0") (d (list (d (n "bevy_ecs") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.10.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0y34r65x93kyhwscvdia9qmfqq5ickivyxr2ihlh4yr553wbvw8c")))

(define-public crate-micro_bevy_web_utils-0.3.0 (c (n "micro_bevy_web_utils") (v "0.3.0") (d (list (d (n "bevy_ecs") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.11.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0lyf26p5w9c7fk6w85g68mxi9yc6kh5i6c7h99yl1j8y34zrlhxn")))

(define-public crate-micro_bevy_web_utils-0.4.0 (c (n "micro_bevy_web_utils") (v "0.4.0") (d (list (d (n "bevy_ecs") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.13.0") (d #t) (k 0)) (d (n "bevy_window") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "1hpijll1dqn2yryqz2ka723pcqpcpi59ypy63zmljcybmblp83ix")))

