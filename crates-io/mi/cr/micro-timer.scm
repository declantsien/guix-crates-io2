(define-module (crates-io mi cr micro-timer) #:use-module (crates-io))

(define-public crate-micro-timer-0.1.0 (c (n "micro-timer") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kkpvaqsxhw1api7290214kz22k0n63yb0sfmsc05bzq2v8qzbb9")))

(define-public crate-micro-timer-0.1.1 (c (n "micro-timer") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gsqlxyax11z07zygfw6rpx3wdgrnp4kh6byskcam78h1vz6agvv")))

(define-public crate-micro-timer-0.1.2 (c (n "micro-timer") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iblm6x8pgz1y7x9cisbc927hh2dg27ynz3kywfjqwcg73m9yhqn")))

(define-public crate-micro-timer-0.2.0 (c (n "micro-timer") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "micro-timer-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1wfaxm5ri3d9n4dz135zqbz9dh1cfhyn997s8x3nf5hx0w7lqsrk")))

(define-public crate-micro-timer-0.2.1 (c (n "micro-timer") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "micro-timer-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "16yp049djplnfbia4wxc2zbcdccha07pkz24zzahxa32c76jjx4q")))

(define-public crate-micro-timer-0.3.0 (c (n "micro-timer") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "micro-timer-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0iy9p0hh3h15kfmzjx3h61x5vbkjy99s7mq57lr88a8ip5n1vcr5")))

(define-public crate-micro-timer-0.3.1 (c (n "micro-timer") (v "0.3.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "micro-timer-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "13191jc39hnx3m38bha1m75mciwcip2fkw495fvjcgch3lz1a816")))

(define-public crate-micro-timer-0.4.0 (c (n "micro-timer") (v "0.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "micro-timer-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "01d4fshrbnc7i6pjvzmraxs7453pmb24423g1mb749h6kasjrqsx")))

