(define-module (crates-io fb ws fbws) #:use-module (crates-io))

(define-public crate-fbws-0.0.1 (c (n "fbws") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "0m3v5n51j6mmixv564g9a91s78y1qiw2pdhladzv582hwq73crg9")))

(define-public crate-fbws-0.0.2 (c (n "fbws") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "0azlpvndf9wp6vrdqiz3ynpncvclprkqc8z093i1kr7659x3a0i6")))

(define-public crate-fbws-0.0.3 (c (n "fbws") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "1xphsz8vapcqhxy54wwn6cs42b4knz6b8zqirzglrbcgnnywlcd8")))

(define-public crate-fbws-0.0.4 (c (n "fbws") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "08jvf2ci54shfkpw8wgvi5czwshgg7dpx4755vjj0m02rl84cr5w")))

