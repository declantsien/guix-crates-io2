(define-module (crates-io fb _s fb_stats_traits) #:use-module (crates-io))

(define-public crate-fb_stats_traits-0.1.0 (c (n "fb_stats_traits") (v "0.1.0") (d (list (d (n "auto_impl") (r "^0.4") (d #t) (k 0)) (d (n "fbinit") (r "^0.1") (d #t) (k 0)))) (h "0mz4zvi9y76vcr3rakbks9gqliqpkb6kbw4fh14zm92rny86n4ng")))

(define-public crate-fb_stats_traits-0.2.0 (c (n "fb_stats_traits") (v "0.2.0") (d (list (d (n "auto_impl") (r "^1.1.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "fbinit") (r "^0.2") (d #t) (k 0)))) (h "1cxiid571iwr3z78lk0yiqar1dkzawx7gnwnqjk62fcmfg1456n1")))

