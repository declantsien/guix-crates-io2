(define-module (crates-io fb _c fb_cloned) #:use-module (crates-io))

(define-public crate-fb_cloned-0.1.0 (c (n "fb_cloned") (v "0.1.0") (h "0lqmxjdn21acnmcjzqvb0qwfqs0by8pcnb4mq0xj631p3z37bcgn")))

(define-public crate-fb_cloned-0.1.1 (c (n "fb_cloned") (v "0.1.1") (h "03p35ry40ma72liilam83x7vw2f7p7ahnk22lxwywa5lzrd4g9mg")))

(define-public crate-fb_cloned-0.1.2 (c (n "fb_cloned") (v "0.1.2") (h "1v5dj7wwm5sjyi73b0xqich2a7f9f3kak03xy3bivjbfsvyyixh5")))

(define-public crate-fb_cloned-0.1.3 (c (n "fb_cloned") (v "0.1.3") (h "1m16cb2g9b7n6qmxril4s49h20ll8bry9l9lczg13rnz0f2f7v39")))

