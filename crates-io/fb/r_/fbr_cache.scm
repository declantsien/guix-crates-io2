(define-module (crates-io fb r_ fbr_cache) #:use-module (crates-io))

(define-public crate-fbr_cache-0.1.0 (c (n "fbr_cache") (v "0.1.0") (d (list (d (n "intrusive-collections") (r "^0.9.4") (d #t) (k 0)))) (h "1a36g62pizb1cc1pwgp1frgj4dl6s2ns1saq0mqy8hjpklv38zg2")))

(define-public crate-fbr_cache-0.1.1 (c (n "fbr_cache") (v "0.1.1") (d (list (d (n "intrusive-collections") (r "^0.9.4") (d #t) (k 0)))) (h "1gb42zmmfb2mlngdjnrxsqnr6k85mflmsrrjvlwvs8b7jp25v6wm")))

