(define-module (crates-io fb sd fbsd-geom-sys) #:use-module (crates-io))

(define-public crate-fbsd-geom-sys-0.1.0 (c (n "fbsd-geom-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "02iw756shvfkzi50w3bnir9jzgql0xy47nzlz81jk57snswgrvcd")))

(define-public crate-fbsd-geom-sys-0.1.1 (c (n "fbsd-geom-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0xlsl661mij0qnzmdjp7z2cknwg897g2jn58jlpz89f1capf7yr3")))

