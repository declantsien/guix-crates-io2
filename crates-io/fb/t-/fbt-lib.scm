(define-module (crates-io fb t- fbt-lib) #:use-module (crates-io))

(define-public crate-fbt-lib-0.1.0 (c (n "fbt-lib") (v "0.1.0") (d (list (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1yzz9bvny705ys9b4bxgfaj0sxhl53606zahqq38w6yxaasfgp3r")))

(define-public crate-fbt-lib-0.1.1 (c (n "fbt-lib") (v "0.1.1") (d (list (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ainfijaszilgz562zs30q3sjmryw6sgp5zr568zmambnxd8y89h")))

(define-public crate-fbt-lib-0.1.2 (c (n "fbt-lib") (v "0.1.2") (d (list (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14j55gy7dysh3hbkcm4yad4nv1zmwgcdmx1fmdh5dj76nnq07qis")))

(define-public crate-fbt-lib-0.1.3 (c (n "fbt-lib") (v "0.1.3") (d (list (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17nf80k0fhh4i60rlwj1gx4zby4rk63bzz6zlm6j0lg4llw80dxp")))

(define-public crate-fbt-lib-0.1.4 (c (n "fbt-lib") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1j9c4xgy3rg4rg18vrizpiwgym3gpx2ydfjl5z13jrc2d43ik512")))

(define-public crate-fbt-lib-0.1.5 (c (n "fbt-lib") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09z1c6zyfps22chq95l59sh55lflb98i2n6klmsbci0g4cd0a208")))

(define-public crate-fbt-lib-0.1.6 (c (n "fbt-lib") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0lzhg0hldr029hg1gzi4qyg8wfap3l2pav550635xwsccyafnrw7")))

(define-public crate-fbt-lib-0.1.7 (c (n "fbt-lib") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1cvpjcbz1n7zr049qf2ikpkr20la7lqmxybx9wsfg3yvr3b1m2bk")))

(define-public crate-fbt-lib-0.1.8 (c (n "fbt-lib") (v "0.1.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bskwydsl2khwzknwf295bmjkqgks8lihlprz6xy3ndsd0w39xg3")))

(define-public crate-fbt-lib-0.1.9 (c (n "fbt-lib") (v "0.1.9") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1g50dpciqwf4hrxnsqvzq4z6980b6zcd959vl0d9js2xyca10syl")))

(define-public crate-fbt-lib-0.1.10 (c (n "fbt-lib") (v "0.1.10") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wjk1ky1l8syxnbd2h4rpf10r4b2vhiwcydmh2n6083m48w82f5l")))

(define-public crate-fbt-lib-0.1.11 (c (n "fbt-lib") (v "0.1.11") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.11") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "07fhya0z0c6gw34q1nzg2j0qghwnlqbjnhk8sx7gq0g8abhbz6lq")))

(define-public crate-fbt-lib-0.1.12 (c (n "fbt-lib") (v "0.1.12") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.11") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gmzkhyhlsi7lx3lp7q7nvkc7y7v9qfn4nllam1xjb74az9klm03")))

(define-public crate-fbt-lib-0.1.13 (c (n "fbt-lib") (v "0.1.13") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.14") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17mahp6fqm93cayj21zsrv70g1p4r7jg80qrwa8ihkanyvaxrd4w")))

(define-public crate-fbt-lib-0.1.14 (c (n "fbt-lib") (v "0.1.14") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0n4vfq60fl2af7gxnp0cmq8qyw4z94fning9l05pqaba82kdifgj")))

(define-public crate-fbt-lib-0.1.15 (c (n "fbt-lib") (v "0.1.15") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.16") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "06ii3rx41b9yp27yzkapsljznlwm38wg1r6fj3kbsf4an8m99m8j")))

(define-public crate-fbt-lib-0.1.16 (c (n "fbt-lib") (v "0.1.16") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ya5zv59wvsf3lnkb3g5z8312c0vsnlw27bh42f4h9jw21m8vjvd")))

(define-public crate-fbt-lib-0.1.17 (c (n "fbt-lib") (v "0.1.17") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0lg4zx7rzy7pjdpdsb733k88275ljgipqfjrahp6314vir5s0i0i")))

(define-public crate-fbt-lib-0.1.18 (c (n "fbt-lib") (v "0.1.18") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "diffy") (r "^0.2.1") (d #t) (k 0)) (d (n "ftd") (r "^0.1.17") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1l9kqp3jchsks8s2pyp5cxamwniggvd7k02x5lkx247x2wb7kjz2")))

