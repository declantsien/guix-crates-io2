(define-module (crates-io fb _p fb_poster) #:use-module (crates-io))

(define-public crate-fb_poster-0.1.0 (c (n "fb_poster") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qgh39dbxzzcifmil8cry06dwr78c6j90gwfkprvdkskx7m8mmg2") (y #t)))

(define-public crate-fb_poster-0.1.1 (c (n "fb_poster") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10f7975i17rdq0p0gc0vqyrqf6jm6qyx808w74lrj552dyl9ppk8")))

(define-public crate-fb_poster-0.1.2 (c (n "fb_poster") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0csqxra01554mr61fyfpqfzf6w8chk0d3allqq953j59759w6v8g")))

(define-public crate-fb_poster-0.1.3 (c (n "fb_poster") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vj2lkh3acygfprmx98hkr0gzkk3qkygk3hnz0wqhnaj9d93wl1j")))

(define-public crate-fb_poster-0.1.4 (c (n "fb_poster") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlzdp1a5mxl71835a30wz4vcr41j1wnpy2zq0x74vfaryfzrgbm")))

(define-public crate-fb_poster-0.1.5 (c (n "fb_poster") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ii4j3r5c82y3625i3hy3gjxy5dl8k6xshqdkfv2f81lk0hwxm3n") (y #t)))

(define-public crate-fb_poster-0.1.6 (c (n "fb_poster") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m54q1qj23ybcic90nllf1g38sjcnfrjznfbb18snz7r2nfkn6ry")))

(define-public crate-fb_poster-0.1.7 (c (n "fb_poster") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xnjkj9gbpq6z3ka3mryfph6bvdi04bvwzkk0xp28a68k3s2vsdf")))

(define-public crate-fb_poster-0.1.8 (c (n "fb_poster") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hbh09003r9zx0zzhgw8mcixb19kcrjfkpjjs3m5pvxif4s1sqvn")))

