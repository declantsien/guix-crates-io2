(define-module (crates-io fb en fbenv) #:use-module (crates-io))

(define-public crate-fbenv-0.1.0 (c (n "fbenv") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "12s34pcj61x172x7sbgmp1f262damlrlw0l9sqixn2zysbkb53fv")))

(define-public crate-fbenv-0.1.1 (c (n "fbenv") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "02fn76pbhppkbr3wnwcr848nrrc8vwkwk4xzjihhnp29vkxny6jb")))

(define-public crate-fbenv-0.1.2 (c (n "fbenv") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1jzc0kqwzxp1fjkrfqznrb6pimfxcizma7nbwvp48ar6p5qqx75g")))

(define-public crate-fbenv-0.1.3 (c (n "fbenv") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "05mx4daqxgiyr5qqh1dma5fr48n4f8pi39s0zdr5inyyln8h13ag")))

(define-public crate-fbenv-0.1.4 (c (n "fbenv") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "05sxkwb8h5vagzhpjivrbz3wnrj75x6yl3w0n8fcinymiz1fqsx2")))

(define-public crate-fbenv-0.1.5 (c (n "fbenv") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0bxyaa438jzhqyy1aqxflancd2pd9vvy4r4b478pj7ks5851jidg")))

