(define-module (crates-io fb x3 fbx3d) #:use-module (crates-io))

(define-public crate-fbx3d-0.1.0 (c (n "fbx3d") (v "0.1.0") (d (list (d (n "bytepack") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0rjgcb5jw7r4i6z165p9cd7r69kbis0m139iyzln7xz4z1mnxhph")))

