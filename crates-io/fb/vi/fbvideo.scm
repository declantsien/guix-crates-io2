(define-module (crates-io fb vi fbvideo) #:use-module (crates-io))

(define-public crate-fbvideo-0.1.0 (c (n "fbvideo") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0k0qib31a3w64jgcrrhrb1kkk4jjmqgr97iq24yn0s46w83ch62w")))

(define-public crate-fbvideo-0.1.1 (c (n "fbvideo") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1bxbq5dzhh5jqyy330k38qhhmd7rpyyvbzh1ikmps6aj453qmd68")))

(define-public crate-fbvideo-0.2.0 (c (n "fbvideo") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1b7jb72d0s82wlb1qn73hggxbywlkkka99jwpp1r40dnr7dvhfgg")))

(define-public crate-fbvideo-0.3.0 (c (n "fbvideo") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1a9wrngkfdnaj0pis4qg5cfhhxas7xdzfk7ymqrrms96wg5drs49")))

(define-public crate-fbvideo-0.3.1 (c (n "fbvideo") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1h1cyz7i4ahqdqsnwpmqkfqkhz4x3d3mfkqyyaqa1xv2y27gly0s")))

(define-public crate-fbvideo-0.3.2 (c (n "fbvideo") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "09l9f36h9zlkvq7zxf32bp47ic9vc1qcggsjc1vvpbfqwylcf8m3")))

(define-public crate-fbvideo-0.4.0 (c (n "fbvideo") (v "0.4.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("default-tls" "gzip"))) (k 0)))) (h "1q0msgkiqwhqkxl0d53fa01vww3s5jwjjc1a183lpyk25ya4f79h")))

(define-public crate-fbvideo-0.4.1 (c (n "fbvideo") (v "0.4.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("default-tls" "gzip"))) (k 0)))) (h "1v69capcpqby7vpylp4kbzns3f9ymjn11p1076r5dnj29z73hpha")))

