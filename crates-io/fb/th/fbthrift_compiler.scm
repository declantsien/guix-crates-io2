(define-module (crates-io fb th fbthrift_compiler) #:use-module (crates-io))

(define-public crate-fbthrift_compiler-0.1.0 (c (n "fbthrift_compiler") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1b24dl111m9s1r5jbqgfjw7yzyf5rjzyc319fbhj4s3wapi67fd9")))

(define-public crate-fbthrift_compiler-0.1.1 (c (n "fbthrift_compiler") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1idiqimjvdb9m7mkrrj0nyf9pwk4ywmgbi1b64qqkw2b7mhfvbpp")))

(define-public crate-fbthrift_compiler-0.1.2 (c (n "fbthrift_compiler") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xdwh63kwddvbw1sxamxndgx58xzycqadal1jhrc7xn59dpx650n")))

(define-public crate-fbthrift_compiler-0.1.3 (c (n "fbthrift_compiler") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "env" "string" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "15daljw3yrqvc176x80j83ra8flz7zpxy8xri0vy1vg4a1ps1v69")))

