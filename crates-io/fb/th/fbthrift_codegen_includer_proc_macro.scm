(define-module (crates-io fb th fbthrift_codegen_includer_proc_macro) #:use-module (crates-io))

(define-public crate-fbthrift_codegen_includer_proc_macro-0.1.0 (c (n "fbthrift_codegen_includer_proc_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1q441i4qvz6kpv85kdh7pdzlj2ryf4cjp5igbpxpf0ri23q6acms")))

(define-public crate-fbthrift_codegen_includer_proc_macro-0.1.1 (c (n "fbthrift_codegen_includer_proc_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "07x8zwc8364g0ibib439x4k3hfrf3hwfmcx16a0nc19kna7m8fgf")))

(define-public crate-fbthrift_codegen_includer_proc_macro-0.1.2 (c (n "fbthrift_codegen_includer_proc_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ah4fc7g70xih8kadcyacrnqhnnp7vq6kkglp8lmcndg1qsl6whn")))

(define-public crate-fbthrift_codegen_includer_proc_macro-0.1.3 (c (n "fbthrift_codegen_includer_proc_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)))) (h "081r9ymgghl7gm3q3glzjw2f0b62nzxa5pld4wr9m6kdnpdw53af")))

