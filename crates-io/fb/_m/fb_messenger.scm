(define-module (crates-io fb _m fb_messenger) #:use-module (crates-io))

(define-public crate-fb_messenger-0.1.0 (c (n "fb_messenger") (v "0.1.0") (h "0p9iryx933r7jix8hpxjy4y1hllr9bvakib9bb5vp8hnsscxfkr2")))

(define-public crate-fb_messenger-0.1.1 (c (n "fb_messenger") (v "0.1.1") (h "1lgh6rpy3ms7jfpm6klqcdql4shx6afwyh30pabcvpcagqb129wv")))

(define-public crate-fb_messenger-0.1.2 (c (n "fb_messenger") (v "0.1.2") (h "00ph9xwipk9s0n8r81qwrhkznicskvf3gdqw4mag9za524fc3z1i")))

(define-public crate-fb_messenger-0.1.3 (c (n "fb_messenger") (v "0.1.3") (h "185cvcx4npa9l7zd0cds70qp946gkkhpvpi8bsygkhr6n11r47pf")))

(define-public crate-fb_messenger-0.1.4 (c (n "fb_messenger") (v "0.1.4") (h "1ayz57pz4y7lxkscnhn7ws1y7b7mcvh9c1zand0gwv21qqh0kf6n")))

(define-public crate-fb_messenger-0.1.5 (c (n "fb_messenger") (v "0.1.5") (h "0cj9v15dbphahf7gl7rs687vfx6pysjgfw10hhn0wvxd5kgw9ypv")))

(define-public crate-fb_messenger-0.1.6 (c (n "fb_messenger") (v "0.1.6") (h "13vkikvhmmlnni5bgx1kc10274gfs995j530k3myirv1mnv0c1mk")))

(define-public crate-fb_messenger-0.1.7 (c (n "fb_messenger") (v "0.1.7") (h "0l62s934kq4j8g7fny82df7c73pq5njy73wp4zh0j5pm0d7789a1")))

(define-public crate-fb_messenger-0.1.8 (c (n "fb_messenger") (v "0.1.8") (h "09kbsvydkmz5yzi4a9vp9ygs6488zd1ksdg5790ddrc0iq1ys5vg")))

(define-public crate-fb_messenger-0.1.9 (c (n "fb_messenger") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 0)))) (h "18mg8iljwcl9c7icqdm5p8q7c393khm9q2arwgwhnp3fqhpdinb0")))

(define-public crate-fb_messenger-0.1.10 (c (n "fb_messenger") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)))) (h "15dmhg2ihn54pn41ppivqm5bh8knspygz5adr64zmg3s89s54xm1")))

(define-public crate-fb_messenger-0.1.11 (c (n "fb_messenger") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)))) (h "1hdildzf95i7q8nnmv6krlf3m8nw98y9gnl18y6h333fkp8pwiwj")))

(define-public crate-fb_messenger-0.1.12 (c (n "fb_messenger") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)))) (h "1v6jqb756rd454cxn66c8gh2ppv9xy9n80nxqh791fyr7wzq51cd")))

(define-public crate-fb_messenger-0.1.13 (c (n "fb_messenger") (v "0.1.13") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)))) (h "1rjvykh8kb3hw473jslp267ykp84b6k2k225dvkb2ngjpbl5rjjq")))

(define-public crate-fb_messenger-0.1.14 (c (n "fb_messenger") (v "0.1.14") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)))) (h "0h1w1727ww66qkcgw1ahs38lmlpf6wf9a9zy3vcqgq2nz6xdpgfp")))

