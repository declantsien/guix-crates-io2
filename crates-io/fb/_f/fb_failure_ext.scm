(define-module (crates-io fb _f fb_failure_ext) #:use-module (crates-io))

(define-public crate-fb_failure_ext-0.1.0 (c (n "fb_failure_ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2.5") (f (quote ("max_level_debug"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ghydcga73d1d16jawgwihmn5dav889pc9j6fbkxxdrs5x29xvla")))

(define-public crate-fb_failure_ext-0.1.1 (c (n "fb_failure_ext") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2.5") (f (quote ("max_level_debug"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0g32j06mh50z1p008xjkymxn365cg1scpg4ffmkdyvwawcrkkn2a")))

(define-public crate-fb_failure_ext-0.1.2 (c (n "fb_failure_ext") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2.5") (f (quote ("max_level_debug"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1nx25j0im80v94fwgd02bvppwidrakp8b5anjpqdm1xb1d3c4xvl")))

(define-public crate-fb_failure_ext-0.1.3 (c (n "fb_failure_ext") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2.5") (f (quote ("max_level_debug"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1iizg3bqdivwywzzv0b1491nrlml90rqa6v84in6mlxna5r4ldi8")))

(define-public crate-fb_failure_ext-0.2.0 (c (n "fb_failure_ext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.1.31") (d #t) (k 0)) (d (n "slog") (r "^2.7") (f (quote ("max_level_trace" "nested-values"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 2)))) (h "1rqv5nrgrpiapp8hhx6mq0ribg74m2ka144i1a8h0awgpfmdyq51")))

