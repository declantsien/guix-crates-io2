(define-module (crates-io fb _f fb_futures_01_ext) #:use-module (crates-io))

(define-public crate-fb_futures_01_ext-0.1.0 (c (n "fb_futures_01_ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "bytes-old") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0) (p "bytes")) (d (n "fb_cloned") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.1.31") (d #t) (k 0)) (d (n "futures03") (r "^0.3.28") (f (quote ("async-await" "compat"))) (d #t) (k 2) (p "futures")) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "test-util" "tracing"))) (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0h0pwwq82nqi6c0489c0k8h2ddxvk3wdpgrv5ml5xy6kv9bdsinr")))

