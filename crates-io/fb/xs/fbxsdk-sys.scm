(define-module (crates-io fb xs fbxsdk-sys) #:use-module (crates-io))

(define-public crate-fbxsdk-sys-0.0.0 (c (n "fbxsdk-sys") (v "0.0.0") (h "19x2pmfirf9kl367nlafplmjgh7cavk3mf093597p06lqqa11828")))

(define-public crate-fbxsdk-sys-0.1.0 (c (n "fbxsdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 1)))) (h "1yf4xl86jfjlp5mk16y49cqrds2gd3ckdpy01ahspld5yral05y6")))

