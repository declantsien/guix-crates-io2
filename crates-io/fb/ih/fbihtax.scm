(define-module (crates-io fb ih fbihtax) #:use-module (crates-io))

(define-public crate-fbihtax-0.3.1 (c (n "fbihtax") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.18.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "17d8ky3lw71yc14nybpincm8lsasv599g3610vfb4bj9xb5rqa61")))

(define-public crate-fbihtax-0.3.2 (c (n "fbihtax") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.18.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "10pawxk2fcd4r67ica0b9w5476y9cm1kxvkl4nxv0bv6kfqg92vh")))

