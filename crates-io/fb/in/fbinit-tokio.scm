(define-module (crates-io fb in fbinit-tokio) #:use-module (crates-io))

(define-public crate-fbinit-tokio-0.1.0 (c (n "fbinit-tokio") (v "0.1.0") (d (list (d (n "tokio") (r "^1.29.1") (f (quote ("full" "test-util" "tracing"))) (d #t) (k 0)))) (h "14jwk634746mlfdsbfalbmjqld4i3liz7ip4yyjzhidsyakjyz9i")))

