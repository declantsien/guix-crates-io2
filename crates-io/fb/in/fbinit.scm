(define-module (crates-io fb in fbinit) #:use-module (crates-io))

(define-public crate-fbinit-0.1.0 (c (n "fbinit") (v "0.1.0") (d (list (d (n "fbinit_macros") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "=0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1p3amqnp3f5yxhwpvhqwdc7xw9s9nrqqq818k76kfghy1w9s37bf")))

(define-public crate-fbinit-0.1.1 (c (n "fbinit") (v "0.1.1") (d (list (d (n "fbinit_macros") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "=0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1ypvn8k0ry7530xykk8khj3595bf3m2k7l9dlvgsxpd6ah1cpndw")))

(define-public crate-fbinit-0.1.2 (c (n "fbinit") (v "0.1.2") (d (list (d (n "fbinit_macros") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "=0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1m1h9gwbc4vxc1jllmamq14v7lr9g04912f8fdch5zr6dl6cc6v6")))

(define-public crate-fbinit-0.2.0 (c (n "fbinit") (v "0.2.0") (d (list (d (n "fbinit-tokio") (r "^0.1") (d #t) (k 2)) (d (n "fbinit_macros") (r "=0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)))) (h "1652411qfjrjr017rnlixp44qay502ypb3rypb54ld933k7b5ypr")))

