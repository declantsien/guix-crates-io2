(define-module (crates-io fb x_ fbx_direct) #:use-module (crates-io))

(define-public crate-fbx_direct-0.1.1 (c (n "fbx_direct") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)))) (h "0a7k3r9v9hnw7h35mw2al62h2c71kc6lzhw75sn8kljxxhy460d9")))

(define-public crate-fbx_direct-0.2.0 (c (n "fbx_direct") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)))) (h "1q81cnq6yxqgi0n0n49hkl2f05c7s42ysl6nqjr5j02crpq2ghlr")))

(define-public crate-fbx_direct-0.2.1 (c (n "fbx_direct") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)))) (h "1rcc50b373xp2dl4zk82hr0x7m7834a896g35s9mav9hnm8dkcli")))

(define-public crate-fbx_direct-0.2.2 (c (n "fbx_direct") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)))) (h "1wgxqajzbmmvki6p0y39y01wb9adq8j84xj2jv4ycy5zz6sw6rxa")))

(define-public crate-fbx_direct-0.3.0 (c (n "fbx_direct") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "15d5hx0n8fyykvp9hmmw86368v60phv5hg9ayi0z4hvvfwjnl34n")))

(define-public crate-fbx_direct-0.4.0 (c (n "fbx_direct") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0c6gkv0xq416dbghy3ns47ga3330nbbb17kia4wvnxkm3z5k38cg")))

(define-public crate-fbx_direct-0.5.0 (c (n "fbx_direct") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.12") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1byz0x3s2fwfqf36zwwf9gxv8jr5glmx6yzk8424n6alcrwkz1jd")))

(define-public crate-fbx_direct-0.5.1 (c (n "fbx_direct") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "060pxmbi0jxhwnjbn3a805pn4rlrhq8i24z77j6b2pk1hv4w8l8a")))

(define-public crate-fbx_direct-0.6.0 (c (n "fbx_direct") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1jsna3z7xdafcrd5hvaj4y1al9wnv03ns0nyn5b53xnhrp1kwx7f")))

(define-public crate-fbx_direct-0.6.1 (c (n "fbx_direct") (v "0.6.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1b2p69a9v7hxnpwadcm5jw4q5awmpf5y12ijmacd95yrlz04fk3p")))

(define-public crate-fbx_direct-0.6.2 (c (n "fbx_direct") (v "0.6.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "06aq7g3maj5ac4ik5rzndjh0l1fzwahmbh4gwc7dk2p1fjxrvqbb")))

(define-public crate-fbx_direct-0.6.3 (c (n "fbx_direct") (v "0.6.3") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)))) (h "0in8ndbws5hnafzklnph7y7sx0dh39mnjfnx7iwfqh93r2ay9nhj")))

(define-public crate-fbx_direct-0.6.4 (c (n "fbx_direct") (v "0.6.4") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)))) (h "18wz51mrhmc88yf0yll9knzr88p815n6g0ba6qjnw7qilr6krvm1")))

