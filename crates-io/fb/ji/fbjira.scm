(define-module (crates-io fb ji fbjira) #:use-module (crates-io))

(define-public crate-fbjira-0.1.0 (c (n "fbjira") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1j4h1j32qab8wh06n7vq6yksa447al4fqd26csxz5w9h6r5n6lyy")))

(define-public crate-fbjira-0.1.1 (c (n "fbjira") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1zk9sqwb2bhniy0hlrp2v4sp6vczvj41wglqgb9qxi2dc52c8mjh")))

(define-public crate-fbjira-0.1.2 (c (n "fbjira") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0lry33cin4i0dzjywpxfdmf4z44b61fxy79jylh65brcf49am48q")))

(define-public crate-fbjira-0.1.3 (c (n "fbjira") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0f68vhb9rg5slyzi1jsngbx362llxn2nzanzlf46cll1n8srvl8p")))

(define-public crate-fbjira-0.1.4 (c (n "fbjira") (v "0.1.4") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1gkjx5bh9vlzahj7lzh7pd4xj024j35dranm0b5qid2l48lmsawn")))

(define-public crate-fbjira-0.1.5 (c (n "fbjira") (v "0.1.5") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "goji") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "065wnk1h0lczhdw3fc6mrayihf5mhz4p3hgj78qckgjlv2f4hb8p")))

(define-public crate-fbjira-0.1.6 (c (n "fbjira") (v "0.1.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "goji") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0c8aiv1n3s0ljcr4pv60d9xxnkqdn72pl7f0waxrpvz39s91ljy9")))

