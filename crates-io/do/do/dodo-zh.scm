(define-module (crates-io do do dodo-zh) #:use-module (crates-io))

(define-public crate-dodo-zh-0.1.0 (c (n "dodo-zh") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "0qwpjq22jl9d7c6rda8wan7kpvanlwvyff8gx2vivpgra84y7za2")))

(define-public crate-dodo-zh-0.1.1 (c (n "dodo-zh") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "0083ygq6k0lx1abah1xw7w6zp3cr2qvdq65fpjav18hgjcvnbx81")))

