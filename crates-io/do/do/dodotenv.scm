(define-module (crates-io do do dodotenv) #:use-module (crates-io))

(define-public crate-dodotenv-0.1.0 (c (n "dodotenv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0i5zr4kzrdiwdw94al0bghnvw5d048aalsdz2ph54knkrnnx766s")))

