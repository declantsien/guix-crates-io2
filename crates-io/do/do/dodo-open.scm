(define-module (crates-io do do dodo-open) #:use-module (crates-io))

(define-public crate-dodo-open-0.0.1 (c (n "dodo-open") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0aikqx9v8bl8q5q121kg4c4901bsqkkp2a3r4b3wwjh7s9z4bpdc") (f (quote (("default" "console_error_panic_hook"))))))

