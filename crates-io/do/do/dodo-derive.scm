(define-module (crates-io do do dodo-derive) #:use-module (crates-io))

(define-public crate-dodo-derive-0.1.0 (c (n "dodo-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1rbgfsy2sc5zqijan2yxrdn6clkwgb95msf0zsh6l0j8fpv7j0f7")))

(define-public crate-dodo-derive-0.1.1 (c (n "dodo-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1nn7kcbsl3zrcj4x4j881plnsxa1mnd353hxq2ylxv3mx4zv3irv")))

(define-public crate-dodo-derive-0.2.0 (c (n "dodo-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0mgfnm0vryngs3abz06pb0ib4irvsmmb122sc58s4q89n7hpc312")))

(define-public crate-dodo-derive-0.3.0 (c (n "dodo-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0jd22rxgxyha5pi5ic7ki48waynm1x47y3gdd3a73k5in5sihnv0")))

(define-public crate-dodo-derive-0.3.1 (c (n "dodo-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0732wq76lkk3hw546aw6qyz99yd1p14dydsygll1qn2wpxi5cliq")))

