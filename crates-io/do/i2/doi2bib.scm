(define-module (crates-io do i2 doi2bib) #:use-module (crates-io))

(define-public crate-doi2bib-0.1.0 (c (n "doi2bib") (v "0.1.0") (d (list (d (n "biblatex") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0l3sjijngb95n2pz2hsagl9ksm72cix2r8fq2949cva643ygch06")))

(define-public crate-doi2bib-0.1.1 (c (n "doi2bib") (v "0.1.1") (d (list (d (n "biblatex") (r "^0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0v6k4fh6h3d78b9y6ax77c31idja82azb6h850ss9zmb5ix7zk9s")))

(define-public crate-doi2bib-0.1.2 (c (n "doi2bib") (v "0.1.2") (d (list (d (n "biblatex") (r "^0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1qci1ryly1hj986g7cy3ikb0m231jadh63126lyc3lsiqb7kfhqa")))

(define-public crate-doi2bib-0.1.3 (c (n "doi2bib") (v "0.1.3") (d (list (d (n "biblatex") (r "^0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0hcl0g6m7jvcx33bc9fa5nd9jbyg7vd5z6d70fxbr5fsrqn9hqcc")))

(define-public crate-doi2bib-0.1.4 (c (n "doi2bib") (v "0.1.4") (d (list (d (n "biblatex") (r "^0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.14") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0hbgfccaq8717ah2k2hky4jqw14p2fwg9x2iz41rxwdapwghbjnq")))

(define-public crate-doi2bib-0.1.5 (c (n "doi2bib") (v "0.1.5") (d (list (d (n "biblatex") (r "^0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1imw6k6j52az0p266n02v2rd2yx07l5h9yjzm8aq66p76y4rcrp1")))

(define-public crate-doi2bib-0.1.6 (c (n "doi2bib") (v "0.1.6") (d (list (d (n "biblatex") (r "^0.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0k5ssfq6n9l4pmnjk0fr0r6nzy9ybzmmrls087nm9r8g5iavz8ga")))

