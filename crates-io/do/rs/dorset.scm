(define-module (crates-io do rs dorset) #:use-module (crates-io))

(define-public crate-dorset-0.1.0 (c (n "dorset") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "0wxdgd9iazv8wwa4g5bxrqbpfrgbp7f72w485jczyraiby8jbkj3")))

