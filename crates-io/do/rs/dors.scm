(define-module (crates-io do rs dors) #:use-module (crates-io))

(define-public crate-dors-0.0.1 (c (n "dors") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p000ckp9vyf86b8ql21vp596fjnxljz2l5s91kq17af86fmn49j")))

(define-public crate-dors-0.0.2 (c (n "dors") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "12b363l869msaiilqbswskvz3hnlp1pyv8hm33kpig5gnavvyk5v")))

(define-public crate-dors-0.0.3 (c (n "dors") (v "0.0.3") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zsds6fakimnnx6fy6qb6bhsi4rikykha5vyqhns05iiqavnmy3v")))

(define-public crate-dors-0.0.4 (c (n "dors") (v "0.0.4") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ksd27w7hdarwqi652ci9r63aqrka2nk9wpdr7wfqy75206zvfjb")))

(define-public crate-dors-0.0.5 (c (n "dors") (v "0.0.5") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "118ynrifgcf638kv3wc14f7xf818a3050xczm4h2z9fllfs15x51")))

(define-public crate-dors-0.0.6 (c (n "dors") (v "0.0.6") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "09irvcvccb8ljvvyqnf6znfaa22nirrlah2f1vwhcbzdwdlm53qp")))

(define-public crate-dors-0.0.7 (c (n "dors") (v "0.0.7") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1w38gir65w77qaqp7pkmx2kvxhscl5x9qpjq0arvlkby8ayrci9v")))

(define-public crate-dors-0.0.8 (c (n "dors") (v "0.0.8") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dms33phwsb2clqs3lmm2av15dwbh9zsl2bqlgvngqjap4vhxn97")))

