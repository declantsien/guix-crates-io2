(define-module (crates-io do ub double_decker) #:use-module (crates-io))

(define-public crate-double_decker-0.0.1 (c (n "double_decker") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "04csm1vp8hwpjzaalcjanx2a7glzjmnx152ih8gb8f88qna51r5l")))

(define-public crate-double_decker-0.0.2 (c (n "double_decker") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "045rfh422lg9s2sjyb8d6zl1ba9pysd9fzhxg1c99ya87aa8m3rg")))

(define-public crate-double_decker-0.0.3 (c (n "double_decker") (v "0.0.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)))) (h "0nznzq3yyhkha6dybmzdgmq31mdyixlsqndsicwswk9g9mir2byr")))

