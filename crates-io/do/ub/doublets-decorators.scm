(define-module (crates-io do ub doublets-decorators) #:use-module (crates-io))

(define-public crate-doublets-decorators-0.1.0-alpha.0 (c (n "doublets-decorators") (v "0.1.0-alpha.0") (d (list (d (n "data") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-data")) (d (n "doublets") (r "^0.1.0-alpha.7") (d #t) (k 0) (p "doublets")) (d (n "num") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-num")) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19fck5idhlndv56zwji76h659nx9b0d3q90gbnyzvwighj8ycbh3")))

(define-public crate-doublets-decorators-0.1.0-alpha.1 (c (n "doublets-decorators") (v "0.1.0-alpha.1") (d (list (d (n "data") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-data")) (d (n "doublets") (r "^0.1.0-alpha.14") (d #t) (k 0) (p "doublets")) (d (n "num") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-num")) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fl5k2h193hpqv2aap7nfkqrrbn7gdjmlr0646i34s70gs4xpvhj")))

(define-public crate-doublets-decorators-0.1.0-alpha.2 (c (n "doublets-decorators") (v "0.1.0-alpha.2") (d (list (d (n "data") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-data")) (d (n "doublets") (r "^0.1.0-alpha.15") (d #t) (k 0) (p "doublets")) (d (n "num") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-num")) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1vbfzq060l11xfa3ji2rr8j58xaw92xfl140knaz861rms9v6yg6")))

(define-public crate-doublets-decorators-0.1.0-alpha.3 (c (n "doublets-decorators") (v "0.1.0-alpha.3") (d (list (d (n "data") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-data")) (d (n "doublets") (r "^0.1.0-alpha.17") (f (quote ("full"))) (d #t) (k 0) (p "doublets")) (d (n "num") (r "^0.1.0-alpha.0") (d #t) (k 0) (p "platform-num")) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1cixgnvpyd0rg5q1agwn50y18smigjdyp0c14sz5wva4vicqyf6q")))

