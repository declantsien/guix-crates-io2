(define-module (crates-io do ub doubled) #:use-module (crates-io))

(define-public crate-doubled-0.1.0 (c (n "doubled") (v "0.1.0") (d (list (d (n "packed_simd") (r "^0.3.2") (f (quote ("into_bits"))) (o #t) (d #t) (k 0)))) (h "1byciv10awbc0gbk9kxkpnlh8nadlnhvk5qq0rv71s812nwvy7a8") (f (quote (("default"))))))

(define-public crate-doubled-0.2.0 (c (n "doubled") (v "0.2.0") (d (list (d (n "packed_simd") (r "^0.3.8") (f (quote ("into_bits"))) (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "10y60v6qy8ar058hznm4sjjk9nvmqnqk9b7mq8w6ljn40l07ax12") (f (quote (("default"))))))

(define-public crate-doubled-0.2.1 (c (n "doubled") (v "0.2.1") (d (list (d (n "packed_simd") (r "^0.3.8") (f (quote ("into_bits"))) (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "112zi1cngyl7d2sk33fi6dndrlzcplmz9ghh2bkqjmgr2mrjyzcw") (f (quote (("default"))))))

(define-public crate-doubled-0.2.2 (c (n "doubled") (v "0.2.2") (d (list (d (n "packed_simd") (r "^0.3.8") (f (quote ("into_bits"))) (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "1zyf5pc19i9i45hkhswbkdsxjwpg0ik1p6hzx6kxac33s6fg6wyf") (f (quote (("default"))))))

(define-public crate-doubled-0.2.3 (c (n "doubled") (v "0.2.3") (d (list (d (n "packed_simd") (r "^0.3.8") (f (quote ("into_bits"))) (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "0mbdr73p65x98x99w7xhqvc7ki88lam4ddm5aygp7s00ha9flr9s") (f (quote (("default"))))))

(define-public crate-doubled-0.3.0 (c (n "doubled") (v "0.3.0") (h "1z45zaxc0kvb35k6h12xjvl43j60bm5g4v1p8khlz4lm6jy3ncvq") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-doubled-0.3.1 (c (n "doubled") (v "0.3.1") (h "12y9qch7w6ppxnq9vxxglfqcyflk0whw2d0q06y7gf7g2sdgapwi") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-doubled-0.3.2 (c (n "doubled") (v "0.3.2") (h "18z3vy54dda80sanvlsjm2j42m7s8vpkmrm7kfi0lsqfv5rg8q4g") (f (quote (("simd") ("default" "simd"))))))

