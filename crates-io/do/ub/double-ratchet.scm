(define-module (crates-io do ub double-ratchet) #:use-module (crates-io))

(define-public crate-double-ratchet-0.1.0 (c (n "double-ratchet") (v "0.1.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-modes") (r "^0.3") (d #t) (k 2)) (d (n "clear_on_drop") (r "^0.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.12") (d #t) (k 2)) (d (n "hkdf") (r "^0.7") (d #t) (k 2)) (d (n "hmac") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.4") (d #t) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2") (d #t) (k 2)) (d (n "x25519-dalek") (r "^0.5") (d #t) (k 2)))) (h "0x4y8d6kg0zy6hsghsp6nm1j83apl24dsav00yd67zpxgyh19vs8") (f (quote (("test"))))))

