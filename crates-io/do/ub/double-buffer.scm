(define-module (crates-io do ub double-buffer) #:use-module (crates-io))

(define-public crate-double-buffer-0.1.0 (c (n "double-buffer") (v "0.1.0") (h "1rbj9s4nz82jx2y7nnllqnk882zkn5jj5s47w1cbcs5vzkx4pa8h")))

(define-public crate-double-buffer-0.2.0 (c (n "double-buffer") (v "0.2.0") (h "1yjgcpby0q559hdqyygzhz4lx6n00pdn2kx1k3nghd06y9nvcx2l")))

(define-public crate-double-buffer-0.3.0 (c (n "double-buffer") (v "0.3.0") (h "1dbqjfzfs766v5ijz7yrxg4yv5k7rvqvya4s5bk5hry4gb66kzp7")))

(define-public crate-double-buffer-0.3.1 (c (n "double-buffer") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "00imzb02n9wm76cpdy147l70z2n9blp3wl3dy20slawcdzaws0bz")))

(define-public crate-double-buffer-1.0.0 (c (n "double-buffer") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "02wxaifr9vgk6vp9z238kjcx8f3ij3813kqx8w19i1vqy348dznd")))

