(define-module (crates-io do ub double_dot_macro) #:use-module (crates-io))

(define-public crate-double_dot_macro-0.1.0 (c (n "double_dot_macro") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "169mczmlpinh7sksgs14y0i6k4c5yyvzc4ib6g1r7wnpsp9vqnmm")))

(define-public crate-double_dot_macro-0.1.1 (c (n "double_dot_macro") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1j9l64y0dy4xkjqrv2l02hd9sj3lmb3izddinhixfhv4y74qavmk")))

(define-public crate-double_dot_macro-0.1.2 (c (n "double_dot_macro") (v "0.1.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1kxk5z5y74vraqi2fl8baf32nwx84852xxvdki6mc0g7y7aqf1w0")))

(define-public crate-double_dot_macro-0.1.21 (c (n "double_dot_macro") (v "0.1.21") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.1.21") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1m0bxa7jr93wh5xfq45plmmmn5apa5kpg50rv5pa2y5z9dgb0lw6")))

(define-public crate-double_dot_macro-0.2.0 (c (n "double_dot_macro") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.1.21") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1kxqnhzah57hqf3xiw900sxsjkblcvfqsnslljwwr996scglzrjk")))

(define-public crate-double_dot_macro-0.2.1 (c (n "double_dot_macro") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "double_dot_macro_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0gpvbrk6yv3qcmwa68fk1c3p6x7kf3lym9pxis0g9xhvwjd62gvw")))

