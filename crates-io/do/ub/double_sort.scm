(define-module (crates-io do ub double_sort) #:use-module (crates-io))

(define-public crate-double_sort-1.0.0 (c (n "double_sort") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0jabq7klmxsnf1hwq51qlp1q2bm43xnl4rv2j1d6138p3zrgayhn")))

(define-public crate-double_sort-1.1.0 (c (n "double_sort") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "097ryyrqhfda9q5mj5l55d3kpga0kb95af43r06m76ihlyw9jkdc")))

(define-public crate-double_sort-1.2.0 (c (n "double_sort") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "04wdj5ldip8iwdb2qhzqwvr0qzrbgfkzapv9vmca69mh961dpxny")))

