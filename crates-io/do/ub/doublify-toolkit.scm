(define-module (crates-io do ub doublify-toolkit) #:use-module (crates-io))

(define-public crate-doublify-toolkit-0.0.1 (c (n "doublify-toolkit") (v "0.0.1") (h "04wwc6s7izgpbw66176w5qcmvc8svfyi5sdj103wsks1llfbb677")))

(define-public crate-doublify-toolkit-0.1.0 (c (n "doublify-toolkit") (v "0.1.0") (h "0zhwk41rg2f2gxwx9q4kf8fw5ikbz2ayravzh5d7ivigkw4wj7bw")))

(define-public crate-doublify-toolkit-0.1.1 (c (n "doublify-toolkit") (v "0.1.1") (h "0l2qsqcqpdkylq0ncapsxv65k0pbxwmi2gbd4xpb4ins0djxsrkv")))

