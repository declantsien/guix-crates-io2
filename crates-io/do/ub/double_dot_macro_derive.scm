(define-module (crates-io do ub double_dot_macro_derive) #:use-module (crates-io))

(define-public crate-double_dot_macro_derive-0.1.0 (c (n "double_dot_macro_derive") (v "0.1.0") (d (list (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "12ghn92dxcql15vixyzq7g5rz9cla5ygw413maw1a0baiq16rapq")))

(define-public crate-double_dot_macro_derive-0.1.1 (c (n "double_dot_macro_derive") (v "0.1.1") (d (list (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0dhlpbaj042y3pgd1v8qfssh2xaq1rbvxmclzsbabacp10p1d7rh")))

(define-public crate-double_dot_macro_derive-0.1.2 (c (n "double_dot_macro_derive") (v "0.1.2") (d (list (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0jpxbfgwgmxiv3fk3il0sa6rfr96lc4a3fgh2gybwal407qiqq4b")))

(define-public crate-double_dot_macro_derive-0.1.21 (c (n "double_dot_macro_derive") (v "0.1.21") (d (list (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1k4sp1lm1krhqlll9a2k9bl65z4nsrhci9dl8bwya0fh7r99jhci")))

(define-public crate-double_dot_macro_derive-0.2.0 (c (n "double_dot_macro_derive") (v "0.2.0") (d (list (d (n "double_dot_macro_types") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1hb2gx7b9s4c0x41shy032jdgzv6cnxr2y26pvsyb4qjvzsa0yzq")))

