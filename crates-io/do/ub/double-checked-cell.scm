(define-module (crates-io do ub double-checked-cell) #:use-module (crates-io))

(define-public crate-double-checked-cell-1.0.0 (c (n "double-checked-cell") (v "1.0.0") (d (list (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "12ddayyvjrxqv45qf29fvgc18f0yb0q5s6p1v7c5w28jnn2235xa")))

(define-public crate-double-checked-cell-1.0.1 (c (n "double-checked-cell") (v "1.0.1") (d (list (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1wkfdij5zki1rql7mlr5v3rw5b6j5d2kwd69557hhmy2kjq20izq")))

(define-public crate-double-checked-cell-1.1.0 (c (n "double-checked-cell") (v "1.1.0") (d (list (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1ghmpdx6kpipd5vk9yrif5smgzws12811wvzc14bsg6632jgxpc7")))

(define-public crate-double-checked-cell-2.0.0 (c (n "double-checked-cell") (v "2.0.0") (d (list (d (n "parking_lot") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "00sw63zgjp9fgajs73f69yr89sa5dqifynd85kplj6004y0dxagr") (f (quote (("parking_lot_mutex" "parking_lot") ("default") ("const_fn" "parking_lot_mutex" "parking_lot/nightly"))))))

(define-public crate-double-checked-cell-2.0.1 (c (n "double-checked-cell") (v "2.0.1") (d (list (d (n "parking_lot") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "19z2i0pq0xdibzvfzi406vxp7kli4gw4aw393bzyrsifi0m5h7br") (f (quote (("parking_lot_mutex" "parking_lot") ("default") ("const_fn" "parking_lot_mutex" "parking_lot/nightly"))))))

(define-public crate-double-checked-cell-2.0.2 (c (n "double-checked-cell") (v "2.0.2") (d (list (d (n "parking_lot") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1j70dyhsy0f0hksllab702lfaxn4wr38j28fyj863gg9fgmibdfy") (f (quote (("parking_lot_mutex" "parking_lot") ("default") ("const_fn" "parking_lot_mutex" "parking_lot/nightly"))))))

(define-public crate-double-checked-cell-2.0.3 (c (n "double-checked-cell") (v "2.0.3") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "03q7ragnpagb36rx26c2szla0g4n0j5q308kpwjbi82qb1s72k3l") (f (quote (("parking_lot_mutex" "parking_lot") ("default") ("const_fn" "parking_lot_mutex" "parking_lot/nightly"))))))

(define-public crate-double-checked-cell-2.1.0 (c (n "double-checked-cell") (v "2.1.0") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (k 0)) (d (n "scoped-pool") (r "^1.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0smhs2pgx1jpqdhb6z1rpnzr0wlknzm670rjqqn6rag0132xmv4i") (f (quote (("parking_lot_mutex" "parking_lot") ("default") ("const_fn" "parking_lot_mutex" "parking_lot/nightly"))))))

