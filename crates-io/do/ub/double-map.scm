(define-module (crates-io do ub double-map) #:use-module (crates-io))

(define-public crate-double-map-0.1.0 (c (n "double-map") (v "0.1.0") (h "01q5dflj92asnks7nhvdadgr9xd29pgvy8zm7nh3lv89mw0rzkp2")))

(define-public crate-double-map-0.1.1 (c (n "double-map") (v "0.1.1") (h "09yvhs8agd77z6h81ijbaxb3l9vxdpvnryc2a8gv92dmcl28igsm")))

(define-public crate-double-map-0.2.0 (c (n "double-map") (v "0.2.0") (h "003n7pkszpxalrmnbdvr8v8pmxf20z8wf0dj06kjw5manrbcznhi")))

(define-public crate-double-map-0.3.0 (c (n "double-map") (v "0.3.0") (h "1qg5ka26grwkpmhmq9x5c912kygv4kqi88w7g1hbsr14na7hrjg8")))

(define-public crate-double-map-0.4.0 (c (n "double-map") (v "0.4.0") (h "0a4ag2k884xlyjx3assq8q9pph4w0g4jrxian926gxza7m01a8qa")))

(define-public crate-double-map-0.4.1 (c (n "double-map") (v "0.4.1") (h "09qmxgvs2k4wax2z1pkg6hr60a9q95hxcm65nhadq1ml8jkclgqp")))

(define-public crate-double-map-0.5.0 (c (n "double-map") (v "0.5.0") (h "0ygh18rszqy8cbldwn1maj3022blmryazs4ys5f8k1kmm01id598")))

(define-public crate-double-map-0.6.0 (c (n "double-map") (v "0.6.0") (h "1jy9vpmfl9i8fir38d253fj4jwkh0mpbja67smsj980cdladqjzv")))

(define-public crate-double-map-0.7.0 (c (n "double-map") (v "0.7.0") (h "0k9sn4d8rdzyhh81dl5f2vvb6q7bfa27z38qisff3pi9ncg7dryl")))

(define-public crate-double-map-0.8.0 (c (n "double-map") (v "0.8.0") (h "14cga7b9cxb08cydzwgvj71hgd6kihkpygr9ynpcfivah95ll5bd")))

(define-public crate-double-map-0.9.0 (c (n "double-map") (v "0.9.0") (h "128j5bpdiigrxfid86ar65c2aqgan1hds6aa4gabd5pkqzzmasjw")))

(define-public crate-double-map-0.10.0 (c (n "double-map") (v "0.10.0") (h "1d1dzf8wypr3g3yq6msrk4k9j248gzv6rv06mlmliqa7zd61q15p")))

(define-public crate-double-map-0.11.0 (c (n "double-map") (v "0.11.0") (h "0136mb7jnv6v7h2s1zpfskzqn6nzjagrv7bgyv3czl3izvdrsmrz")))

(define-public crate-double-map-0.12.0 (c (n "double-map") (v "0.12.0") (h "0lm7n1lihjk4xv2ydprjg2dn53ycfbsp2bk6mqaczy3jmzcy37fr")))

(define-public crate-double-map-0.14.0 (c (n "double-map") (v "0.14.0") (h "0w0wcd4v92wp8ia66cyxvxbmg2hb4g0xcjqbh71g68asgyfrm257")))

