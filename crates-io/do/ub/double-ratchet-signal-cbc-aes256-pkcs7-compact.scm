(define-module (crates-io do ub double-ratchet-signal-cbc-aes256-pkcs7-compact) #:use-module (crates-io))

(define-public crate-double-ratchet-signal-cbc-aes256-pkcs7-compact-0.1.1 (c (n "double-ratchet-signal-cbc-aes256-pkcs7-compact") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1p52pkp61kkgddndx4qnrb857pkp9r97j5nz359b8lqjzgmb7m7c") (f (quote (("std") ("default" "std"))))))

