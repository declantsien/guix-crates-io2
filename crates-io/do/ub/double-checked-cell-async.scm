(define-module (crates-io do ub double-checked-cell-async) #:use-module (crates-io))

(define-public crate-double-checked-cell-async-2.0.2 (c (n "double-checked-cell-async") (v "2.0.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.2") (f (quote ("macros" "rt-core" "fs" "io-util"))) (d #t) (k 2)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0mx17wn67fskhpkfj0fzs6973l6j206sfxj93hrm8m872m1xn8kg")))

