(define-module (crates-io do ub doublepivot-quicksort) #:use-module (crates-io))

(define-public crate-doublepivot-quicksort-0.1.0 (c (n "doublepivot-quicksort") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0wjwk13y8rizkzar5mnkdfs3gpr5rgz9pg57rwcc4jjjd1pyxyq6") (y #t)))

(define-public crate-doublepivot-quicksort-0.2.0 (c (n "doublepivot-quicksort") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "036009sfy3h8v8y9l4pym8bfrkw5l91nxgmcjblkw7hy33bg9gxh")))

