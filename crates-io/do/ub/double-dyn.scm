(define-module (crates-io do ub double-dyn) #:use-module (crates-io))

(define-public crate-double-dyn-0.1.0 (c (n "double-dyn") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1iyhbz6lag90laavhc7m2l2j0ls5wackfvmswwb8scyl2fv2gr6v")))

(define-public crate-double-dyn-0.1.1 (c (n "double-dyn") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1dmcmpjqv8fb5mpw98j2j07x0pypfjcdkqxj9hjb4zqn1cbm4syy")))

