(define-module (crates-io do ub double-ended-peekable) #:use-module (crates-io))

(define-public crate-double-ended-peekable-0.0.1 (c (n "double-ended-peekable") (v "0.0.1") (h "1lizqrx5hnnqzhydb2i9s8bn58laikqwg6sabw657c8f3zkyx6fn")))

(define-public crate-double-ended-peekable-0.0.2 (c (n "double-ended-peekable") (v "0.0.2") (h "1j4ah052mmfswsjq2xdrbrnk9nw8lqqfmsqvji913dr09k151sg1")))

(define-public crate-double-ended-peekable-0.1.0-alpha.1 (c (n "double-ended-peekable") (v "0.1.0-alpha.1") (h "0774z03ki424ipv391a6n3jslnf5nl5jy634bl4hgg7hpzd8pa77")))

(define-public crate-double-ended-peekable-0.1.0 (c (n "double-ended-peekable") (v "0.1.0") (h "0mvg19iw9a16hzzaq15g5gy9w6znxvf7mnlbqd91pmds1lf5xl60")))

