(define-module (crates-io do ub doublesite) #:use-module (crates-io))

(define-public crate-doublesite-0.1.0 (c (n "doublesite") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "har-v0-8-1") (r "^0.8.1") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0wzy4bs8hisixgmpsgdz90h1x6559l02xdj5jrc46vcjlkx7qvqf")))

