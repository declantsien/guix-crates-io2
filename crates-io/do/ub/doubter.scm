(define-module (crates-io do ub doubter) #:use-module (crates-io))

(define-public crate-doubter-0.0.1 (c (n "doubter") (v "0.0.1") (d (list (d (n "doubter-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1yx6zwffp23a4q1pyf2cpxp510lp6r9r34r68scr70s6kpp0dxqb")))

(define-public crate-doubter-0.0.2 (c (n "doubter") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "doubter-impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "04psfcsgvd5px5flwi5addkz6n99w9822fa7xxsgjlw5lp3wx7xy")))

(define-public crate-doubter-0.0.3 (c (n "doubter") (v "0.0.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "doubter-impl") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0b5lrd50spnzia0vb8dfhvxxf0k6llbddfjggwy62jqzhm367lzd") (f (quote (("external-doc" "doubter-impl/external-doc"))))))

(define-public crate-doubter-0.0.4 (c (n "doubter") (v "0.0.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "doubter-impl") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1c7hs2k1h4p79fyqd4n0dcinag1skn9h4s05yfypdkxwvxzyrd3p") (f (quote (("external-doc" "doubter-impl/external-doc"))))))

(define-public crate-doubter-0.0.5 (c (n "doubter") (v "0.0.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "doubter-impl") (r "^0.0.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1ga1w1w5mgw1sih4kqir4v1gwvgh9pywmar795h3dvlm5hqlvkdl") (f (quote (("external-doc" "doubter-impl/external-doc"))))))

(define-public crate-doubter-0.0.6 (c (n "doubter") (v "0.0.6") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "doubter-impl") (r "^0.0.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1navybbw7jcsapsarwqqa1v6gfggrmn768r4kpdsamm63d617789")))

(define-public crate-doubter-0.1.0 (c (n "doubter") (v "0.1.0") (d (list (d (n "doubter-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "doubter-macros") (r "^0.1.0") (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0y3g8nxpaiz74rlwy16p7lhd6fzgna3lr13a60bil16lg7z3ddsd") (f (quote (("hack" "proc-macro-hack" "doubter-macros/proc-macro-hack") ("default" "hack"))))))

