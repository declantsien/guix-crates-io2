(define-module (crates-io do ub doubter-impl) #:use-module (crates-io))

(define-public crate-doubter-impl-0.0.1 (c (n "doubter-impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1agpbjb1zm4vhfbspfkjinb1m2micqjagg1ga4ybc0gg6i0d015z")))

(define-public crate-doubter-impl-0.0.2 (c (n "doubter-impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f88ciw6ia1nwf5d2z106yy5a5mrbip47s0a3h54jn8bk0s03mjv")))

(define-public crate-doubter-impl-0.0.3 (c (n "doubter-impl") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q8szlc5g2x8vn9fnc3yz6xkb0llydggwx82vxc1wazxm47vg6c1") (f (quote (("external-doc"))))))

(define-public crate-doubter-impl-0.0.4 (c (n "doubter-impl") (v "0.0.4") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03qykzb11s1j3sx78vhk82b1kaj8xrvw38fp6awr06hmpzgsdsa5") (f (quote (("external-doc"))))))

(define-public crate-doubter-impl-0.0.5 (c (n "doubter-impl") (v "0.0.5") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cb208hwrxf6bcpn6mxjqdmdz1ca9gymbvdxbf5rkgh3wxic1n89") (f (quote (("external-doc"))))))

(define-public crate-doubter-impl-0.0.6 (c (n "doubter-impl") (v "0.0.6") (d (list (d (n "bytecount") (r "^0.3.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h90g8hq97sbm0bppvl837j88a2mw2nfi6f555czpndvyd60q00q")))

(define-public crate-doubter-impl-0.1.0 (c (n "doubter-impl") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.3.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00wvah43yz0zrj0g47hzqwvq0iv7c77a95gsw4zb6rqm6w2ibxq8")))

