(define-module (crates-io do ub doubter-macros) #:use-module (crates-io))

(define-public crate-doubter-macros-0.1.0 (c (n "doubter-macros") (v "0.1.0") (d (list (d (n "doubter-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)))) (h "006mbpbi2p40lsqwysi3yd4k1rpx2ypkrd845zqh3cgr6kixbacy") (f (quote (("default" "proc-macro-hack"))))))

