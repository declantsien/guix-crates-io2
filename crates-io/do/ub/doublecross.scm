(define-module (crates-io do ub doublecross) #:use-module (crates-io))

(define-public crate-doublecross-0.1.0 (c (n "doublecross") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)))) (h "15w2f5ywxixh0khpm1khwnargdn3q1iplbk78xv44a5adkfxif8v")))

(define-public crate-doublecross-0.2.0 (c (n "doublecross") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)))) (h "10q7iyv1ds4yvj6skbfnqj8arzmdbgh9b4sddh6halsjai65pxxc")))

(define-public crate-doublecross-0.2.1 (c (n "doublecross") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)))) (h "0j0ylw0595ps3sbhicfdnvqyjlaw883cjvch41jvdh5lbc985dz8")))

