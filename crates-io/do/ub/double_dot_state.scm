(define-module (crates-io do ub double_dot_state) #:use-module (crates-io))

(define-public crate-double_dot_state-0.1.0 (c (n "double_dot_state") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0m1pj72vvm13vj6wvp1ka6827k83b5njggv40ms1dskc687fir6w")))

(define-public crate-double_dot_state-0.1.2 (c (n "double_dot_state") (v "0.1.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro") (r "^0.1.2") (d #t) (k 0)))) (h "063xk9sj9rgnqj1msh3gjdvkgald2bq06bjxp17k2fjwzr7l8mjf")))

(define-public crate-double_dot_state-0.1.21 (c (n "double_dot_state") (v "0.1.21") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "double_dot_macro") (r "^0.1.21") (d #t) (k 0)))) (h "00qcxig1lg6rb2lbghkil21ddx738ryc83g77fvmxkwp9lcq3y8v")))

(define-public crate-double_dot_state-0.2.0 (c (n "double_dot_state") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "double_dot_macro") (r "^0.2.0") (d #t) (k 0)))) (h "174w1cwrb51bhqp65hj6c4wgswx0lz7vkg29avgqz44b9j55pdxd")))

(define-public crate-double_dot_state-0.2.1 (c (n "double_dot_state") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "double_dot_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0m1fld2ib6hkbip051420mb1ip1psq1liyy9rd0lbjz0v3klyx3n")))

