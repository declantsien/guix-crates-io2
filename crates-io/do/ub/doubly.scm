(define-module (crates-io do ub doubly) #:use-module (crates-io))

(define-public crate-doubly-0.0.1 (c (n "doubly") (v "0.0.1") (h "0w4jvmh78hmjgh2yb1p4wikv5hf4knaypxjk8vpma0xmmi62326d")))

(define-public crate-doubly-1.0.0 (c (n "doubly") (v "1.0.0") (h "1mbdh7jkvh5slgidl86a7p9yqhipys920pb4jw8w3wxkf513fcqp")))

(define-public crate-doubly-1.0.1 (c (n "doubly") (v "1.0.1") (h "1h3zibspndswlb1wn97cr59qxpgajx9qmh03wfm2x080n01sa9iz")))

(define-public crate-doubly-1.0.2 (c (n "doubly") (v "1.0.2") (h "0bqwn6qsj4r1pyx032xq4xh5d96hcyj30an2hziz65am1rpg1zds")))

(define-public crate-doubly-1.1.0 (c (n "doubly") (v "1.1.0") (h "0x5mdwa7n62pwb5ai5dpjd8w7rxbqfrdvd0ck1dbbnq8a0pvqzgj")))

(define-public crate-doubly-1.1.1 (c (n "doubly") (v "1.1.1") (h "1gggcg7jafnbhz5k6sy58s7g29pgfnbchpaqy3qqr4vpdqnadx4q")))

(define-public crate-doubly-1.1.2 (c (n "doubly") (v "1.1.2") (d (list (d (n "clippy") (r "^0.0.112") (d #t) (k 0)))) (h "046d1mlwhscnjl02cd20zb2zhs8wgr2hsrdz30p5klahplxqblak")))

(define-public crate-doubly-1.1.3 (c (n "doubly") (v "1.1.3") (h "1y710i58p89qhkzrid5xrqs4xrpjxjii4gwnpapc0wy3fk8q5i14")))

