(define-module (crates-io do ub double_linkedlist) #:use-module (crates-io))

(define-public crate-double_linkedlist-0.1.0 (c (n "double_linkedlist") (v "0.1.0") (h "0yz0y4bxcjj09lq53vkhk16bkaczhr1hffhdib2g79ggp9z2fjvp")))

(define-public crate-double_linkedlist-0.1.1 (c (n "double_linkedlist") (v "0.1.1") (h "0s0vksm5dra7q3fn7r1gd3lc2v2y42jqjrkypl823yzabm3cd22b")))

(define-public crate-double_linkedlist-0.1.2 (c (n "double_linkedlist") (v "0.1.2") (h "1fk0r52phzx5s37xsw91slw850qd3wnz612zb3knr98mpfjdk3xp")))

