(define-module (crates-io do ub double) #:use-module (crates-io))

(define-public crate-double-0.1.0 (c (n "double") (v "0.1.0") (h "00sc1q43kqxc20p2a65z5fydh78z8js3qww9zf3k8lpzc4a6s4f5")))

(define-public crate-double-0.1.1 (c (n "double") (v "0.1.1") (h "1fwjv27zc91mbd9wcf70r59srs0mwcbxydf19vd9ikcd43d2dnd5")))

(define-public crate-double-0.1.2 (c (n "double") (v "0.1.2") (h "1z6ss6rh4lsnvr93v4vvj4brpzg0llnfy5hrxix0ir1y3nswa1sb")))

(define-public crate-double-0.1.3 (c (n "double") (v "0.1.3") (h "19ylal6bbix6f9qvhsj05l53s2j8pg7v1calm16ygadl0x7bbjvq")))

(define-public crate-double-0.1.4 (c (n "double") (v "0.1.4") (h "01yhr6mrh5y4nw0srkvnpljycblyrfrsgxdvr5fzbma9jmjjz34v")))

(define-public crate-double-0.1.5 (c (n "double") (v "0.1.5") (h "19s19mmi7mpl4ihfks3zasj2wp1q72bja4fh7xkwsjrzjcm10xn9")))

(define-public crate-double-0.1.6 (c (n "double") (v "0.1.6") (h "0c0jagn9rpgl0srsw08bsz8qflzf810j1wzgbfz4p4xzv3jmrn0g")))

(define-public crate-double-0.2.0 (c (n "double") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.2.5") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 1)))) (h "1lihbnz7bdy8p41ykdm9if2ml47pf9nnjh15q7447skrjk2sf6xg")))

(define-public crate-double-0.2.1 (c (n "double") (v "0.2.1") (d (list (d (n "float-cmp") (r "^0.2.5") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 1)))) (h "1fayh40x2cnq9ya7vscsaagwymfn5g75bzbs4g9gwglgpczydbgd")))

(define-public crate-double-0.2.2 (c (n "double") (v "0.2.2") (d (list (d (n "float-cmp") (r "^0.2.5") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 1)))) (h "1ygvxjfkvydpj1nrsw9fnz0kz8si9d11j75vwgmr52k8x93vmn91")))

(define-public crate-double-0.2.3 (c (n "double") (v "0.2.3") (d (list (d (n "float-cmp") (r "^0.2.5") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 1)))) (h "0zhsjcff222w2hlssqlm67qp6ldi1a3f0q8919mclkwxxvklahaf")))

(define-public crate-double-0.2.4 (c (n "double") (v "0.2.4") (d (list (d (n "float-cmp") (r "^0.2.5") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 1)))) (h "1i2xddi17nd0cxryln0rbnlqf5gjydim6grjm43vm1gf2b12hfyc")))

