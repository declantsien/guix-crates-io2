(define-module (crates-io do tc dotctl) #:use-module (crates-io))

(define-public crate-dotctl-0.1.0 (c (n "dotctl") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "resolve-path") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "09rqb4fv1ggd0mz1acqc88v1j171rx7j5smapxqp3v3dqvmypmyx")))

