(define-module (crates-io do lo dolores) #:use-module (crates-io))

(define-public crate-dolores-0.1.0 (c (n "dolores") (v "0.1.0") (d (list (d (n "cattlerustler") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0z0caa91l3zzg8r2kncvzsv0x5br0ggpb97b99cvca5kv59gbvq3")))

(define-public crate-dolores-0.1.1 (c (n "dolores") (v "0.1.1") (d (list (d (n "cattlerustler") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1zl7m8f145jicrclyjg9bvah023vzqfpzq19i3w00nbpifzpf9cz")))

(define-public crate-dolores-0.1.2 (c (n "dolores") (v "0.1.2") (d (list (d (n "cattlerustler") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0giy0dbkyfvrj57kgnfjflggjrakzrq1lsfvfgfblyhch3k7085p")))

(define-public crate-dolores-0.1.3 (c (n "dolores") (v "0.1.3") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0rvxbih0wr6g1p90ynlpxrzcjp0ay7whaiqj57nyn6wlx9wjibsm")))

(define-public crate-dolores-0.1.4 (c (n "dolores") (v "0.1.4") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1375jzksk7ngdiwbh9i5lzwz4ayijgk3wsmrmcnkkklfbncdflzl")))

(define-public crate-dolores-0.1.5 (c (n "dolores") (v "0.1.5") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1nsf5k2mgni76yjh6zv403mkdsabqybd3w3qcq0q19xcbgw56x5n")))

(define-public crate-dolores-0.1.7 (c (n "dolores") (v "0.1.7") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1chlgh4ii8dblqxb7dslimxngdxxhh0mmmw8g0787vqdwzhm4x26")))

(define-public crate-dolores-0.1.8 (c (n "dolores") (v "0.1.8") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1h14b1n2dss1hh3hbhibdrzx9hz26pgp06ryby7qfg8x6ax48jk1")))

(define-public crate-dolores-0.1.9 (c (n "dolores") (v "0.1.9") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0gmg9l5rkfi019k5acliqryb4a63waznz45i8sw4b0891yrdx26n")))

(define-public crate-dolores-0.1.10 (c (n "dolores") (v "0.1.10") (d (list (d (n "cattlerustler") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2.0") (d #t) (k 0)) (d (n "recurdates") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0g1r8fh8fkbmckhnp3my679d8rdyc52sd68ymgn2n71ccxvvdj62")))

