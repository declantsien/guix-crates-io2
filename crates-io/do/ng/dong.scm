(define-module (crates-io do ng dong) #:use-module (crates-io))

(define-public crate-dong-1.0.0 (c (n "dong") (v "1.0.0") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "13xla353lrvdgrpnwkmk7qyrc07v1zdkwmv5qwnfzs8fwhbd6f0b")))

(define-public crate-dong-1.0.1 (c (n "dong") (v "1.0.1") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "06m86iz9ysvkw2x9l6ldxgfwxfz4wvqwds8yqy07d9v3qslbsfk4")))

(define-public crate-dong-1.0.2 (c (n "dong") (v "1.0.2") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1myhdlq1kn406g0k99jz749hcrrnwcjxbqzks18pmbhziy0q15ba")))

(define-public crate-dong-1.0.4 (c (n "dong") (v "1.0.4") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0d6iwplrr1bdklgck9bn7j9xbyf97r3fwcp0xgcnfqida8kr51jc")))

(define-public crate-dong-1.0.5 (c (n "dong") (v "1.0.5") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1kww76rzv23xd69798578mr94zyryrhi7wgs3fz35b50k61795d7")))

