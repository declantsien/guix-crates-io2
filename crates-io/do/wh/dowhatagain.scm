(define-module (crates-io do wh dowhatagain) #:use-module (crates-io))

(define-public crate-dowhatagain-0.1.0 (c (n "dowhatagain") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1442cl7xnill1rhsqq38n14izrdgqh6jrsgpfkgjwping2kfziix")))

(define-public crate-dowhatagain-0.1.1 (c (n "dowhatagain") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1m0dwffbgvrrhsjxacx6q62fsbx4z5phfjbg477aiz72m8xfpx6r")))

(define-public crate-dowhatagain-0.1.2 (c (n "dowhatagain") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "105cllvmx69pfnnb1r78ckwdwqqfs7j2mpxgd1iq306ff4426y4h")))

