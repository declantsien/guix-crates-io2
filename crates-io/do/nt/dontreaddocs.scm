(define-module (crates-io do nt dontreaddocs) #:use-module (crates-io))

(define-public crate-dontreaddocs-0.1.0 (c (n "dontreaddocs") (v "0.1.0") (h "15fs0g618vswwwpka7gpa769ig5w0diwckqlrwzs1s6cw4a1hrdd")))

(define-public crate-dontreaddocs-0.1.1 (c (n "dontreaddocs") (v "0.1.1") (h "15kyxpmwcxzvwxha97zgn59l38yqji8y8hgg81y2y351kx8a26z1")))

