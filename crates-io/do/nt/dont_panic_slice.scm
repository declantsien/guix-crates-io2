(define-module (crates-io do nt dont_panic_slice) #:use-module (crates-io))

(define-public crate-dont_panic_slice-0.1.0 (c (n "dont_panic_slice") (v "0.1.0") (d (list (d (n "dont_panic") (r "^0.1") (d #t) (k 0)))) (h "1w4wc7s2jq9ixs0vs8gnd5sy49wcfql3gn9nfbnf75hi5zbmpvj0") (f (quote (("panic" "dont_panic/panic") ("default"))))))

