(define-module (crates-io do nt dontshare) #:use-module (crates-io))

(define-public crate-dontshare-1.0.0 (c (n "dontshare") (v "1.0.0") (h "097hk7kc0mq143zph99nq90kkbz0g608z18s8aqxvlcafqb6sgca") (f (quote (("nightly"))))))

(define-public crate-dontshare-1.1.0 (c (n "dontshare") (v "1.1.0") (h "0bkx6bp4rfsngsjn1w8g8w3hm3f8w1fxdzkpv6vqk06yz9nd02cq") (f (quote (("nightly"))))))

(define-public crate-dontshare-1.2.0 (c (n "dontshare") (v "1.2.0") (h "1p7acrs26mr95y6l3km5a4qszxy01108djxg97klc4m9s1jyy7zl") (f (quote (("nightly"))))))

