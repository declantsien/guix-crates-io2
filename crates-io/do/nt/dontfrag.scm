(define-module (crates-io do nt dontfrag) #:use-module (crates-io))

(define-public crate-dontfrag-1.0.0 (c (n "dontfrag") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bg4vc9f0nyr63a0vam38i5s5z9l7afs3rhclbrdadlwr0bfqhrl")))

(define-public crate-dontfrag-1.0.1 (c (n "dontfrag") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15sk2psihki9nhp2llw5qlq8fbvsl0kskzjd3d3vl9cbbagr85y1")))

