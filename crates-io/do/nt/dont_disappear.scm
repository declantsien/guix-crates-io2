(define-module (crates-io do nt dont_disappear) #:use-module (crates-io))

(define-public crate-dont_disappear-0.0.1 (c (n "dont_disappear") (v "0.0.1") (h "1ynrk61pf1z9cp7jzcwy1cmzmn84ljcy20msnxks0xp2hac5i99r")))

(define-public crate-dont_disappear-0.0.2 (c (n "dont_disappear") (v "0.0.2") (h "0pszvxmfghs27znq484hr0hlmf7q8m1fqc2imzbr4pm7qnjqy8q1")))

(define-public crate-dont_disappear-0.0.3 (c (n "dont_disappear") (v "0.0.3") (h "0xddvxm17cp5bqvrdw317zlyqg0la8p34vhyid4mmrxg7wf9475n")))

(define-public crate-dont_disappear-0.0.4 (c (n "dont_disappear") (v "0.0.4") (h "1bdjvi4fvl6caglwlgqj1iz9f27xw3sirjx99imzf9x2zfmc9a44")))

(define-public crate-dont_disappear-0.0.5 (c (n "dont_disappear") (v "0.0.5") (h "19dpbvwlw0gz9jwnj50d08gbqzwz85kqa032wm3ly4xjb64cgrdw")))

(define-public crate-dont_disappear-0.0.6 (c (n "dont_disappear") (v "0.0.6") (h "1n2nxl68hg77kl2xx2ganxz3inxichnkfkbqrlyhkimmnm8sghc2")))

(define-public crate-dont_disappear-0.0.8 (c (n "dont_disappear") (v "0.0.8") (h "11zx24yg02m050bz74g44g2swmgaqq49zm3a9ia4lb3krzmy4vag")))

(define-public crate-dont_disappear-0.0.9 (c (n "dont_disappear") (v "0.0.9") (h "1qxfn8dklw18vn6r9s5335hrl9d2mz6svzs36dz2xpd2mw30vpvi")))

(define-public crate-dont_disappear-0.0.10 (c (n "dont_disappear") (v "0.0.10") (h "0zx7dxsa18ha7n4br8k4yshvbyx41r16a0nqyah9vp8hx3lzzird")))

(define-public crate-dont_disappear-0.0.7 (c (n "dont_disappear") (v "0.0.7") (h "1dvajv7s2gf61bwfq375r1k3g2z33hfz1nwia6k05m48kphr1hr7")))

(define-public crate-dont_disappear-1.0.0 (c (n "dont_disappear") (v "1.0.0") (h "03bnsxdxn95az566qw346g3nh9p4bzpiv9kj1skdsd6z2qwqr5mi")))

(define-public crate-dont_disappear-1.0.1 (c (n "dont_disappear") (v "1.0.1") (h "1mih6s93qs1bh0c9sb6hfdqfcs4mnf3jsrv8b6gdfc6z5gxrgp8x")))

(define-public crate-dont_disappear-2.0.0 (c (n "dont_disappear") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1wq1f15qs5ns1xbm8qfvc7j2d47ix62vxs28hjjdppp22kwmkv97")))

(define-public crate-dont_disappear-2.0.1 (c (n "dont_disappear") (v "2.0.1") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1swyqz8qbdk2js80jl0ih44y3xmqb5byrwi3v851yhkzr33k2nif")))

(define-public crate-dont_disappear-2.1.0 (c (n "dont_disappear") (v "2.1.0") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "0vaknal83kjjm0jmnlnjg4pai98s5nzsx7pmwq0ii0pk6pzpcj7l")))

(define-public crate-dont_disappear-2.1.3 (c (n "dont_disappear") (v "2.1.3") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1k6s0njldqwm73g6bk95j0vma3fbx7g87pqpagcpv6vkysij5gxf")))

(define-public crate-dont_disappear-2.1.4 (c (n "dont_disappear") (v "2.1.4") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "0868j83m1ni1bwym5kc6cva2d4xd6ss74zkicz4hf1g1ckd26s4g")))

(define-public crate-dont_disappear-2.1.5 (c (n "dont_disappear") (v "2.1.5") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1dmf97yb68r8idm65205xklxjki46rgfi400y56wbzaw82k41ncy")))

(define-public crate-dont_disappear-2.1.6 (c (n "dont_disappear") (v "2.1.6") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "0abn52rxf70jizmg86cgswwzxdpc18mbw6xrlrir4n65wkx74z3b")))

(define-public crate-dont_disappear-2.1.8 (c (n "dont_disappear") (v "2.1.8") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1x6crpjqyiardfw3m42b7whb6ivf96kbsxmm648g227l651ar37r")))

(define-public crate-dont_disappear-2.1.7 (c (n "dont_disappear") (v "2.1.7") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "15bsyg7lrpbqj9yhq1wql8xd8w380xq5ccxvhdq5rz2dhhmsn2lx")))

(define-public crate-dont_disappear-2.1.10 (c (n "dont_disappear") (v "2.1.10") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "15nl9j7c0liypibfw06jw9d29986pc8al9qh43238x1syr5b22ip")))

(define-public crate-dont_disappear-2.1.11 (c (n "dont_disappear") (v "2.1.11") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "02xskamlal6al017v3wnkicl27jdpyc6nxsi2r0zg153cfzbrpxx")))

(define-public crate-dont_disappear-2.1.12 (c (n "dont_disappear") (v "2.1.12") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "15s1lj609kg1bbhv7s4pa7j4w5vi246rb27n8i8a2bn3qdprr9lh")))

(define-public crate-dont_disappear-2.2.0 (c (n "dont_disappear") (v "2.2.0") (d (list (d (n "crossterm") (r "^0.4") (d #t) (k 0)))) (h "1g3z1jzmyfcgxdvxkrv642473km21ykygvdrij3xd4vkc2nh39hf")))

(define-public crate-dont_disappear-2.2.1 (c (n "dont_disappear") (v "2.2.1") (d (list (d (n "crossterm") (r "^0.5") (d #t) (k 0)))) (h "19hgbk6wbn6bn2dzk4psvlaz4svss10rp4kp48yn4n68j5cjxny9")))

(define-public crate-dont_disappear-3.0.0 (c (n "dont_disappear") (v "3.0.0") (d (list (d (n "crossterm") (r "^0.5") (d #t) (k 0)))) (h "11mrxj1w96rf9wqjbwwkgprafr7ja3zbxwcq6c5di48dyrafbcya")))

(define-public crate-dont_disappear-3.0.1 (c (n "dont_disappear") (v "3.0.1") (d (list (d (n "crossterm") (r "^0.8") (d #t) (k 0)))) (h "1dn1zzlhi3bsh78siflr86a78cbjfpjp9bhy0miidwrkb42vpll0")))

