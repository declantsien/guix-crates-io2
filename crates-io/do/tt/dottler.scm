(define-module (crates-io do tt dottler) #:use-module (crates-io))

(define-public crate-dottler-0.1.0 (c (n "dottler") (v "0.1.0") (h "02qvqnm9k32l4w4z38jpa3mis6il7baljshgm3fx8amrgdsvfnwz") (r "1.74")))

(define-public crate-dottler-0.1.1 (c (n "dottler") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "help"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1pa5sbdc7m47jzwwfxwjmyx2yk1p3px7ahs70czqjpx0azs9qwg6") (r "1.74")))

