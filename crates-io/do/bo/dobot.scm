(define-module (crates-io do bo dobot) #:use-module (crates-io))

(define-public crate-dobot-0.1.0 (c (n "dobot") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2.10") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^4.3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 2)))) (h "0yha5q82dd8dhg6g4cz2rl5asr335bffvqhb9dqh2wpzb21h4vkf") (f (quote (("dobot-test"))))))

(define-public crate-dobot-0.1.1 (c (n "dobot") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "getset") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2.10") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^4.3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 2)))) (h "1y1i04bkcjj313bffpqx7a8gp6f91vcp900idg2gbc7dj0s4y5ma") (f (quote (("dobot-test"))))))

