(define-module (crates-io do -p do-proxy) #:use-module (crates-io))

(define-public crate-do-proxy-0.1.0 (c (n "do-proxy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "worker") (r "^0.0.12") (d #t) (k 0)))) (h "03pngpk2v7jm3llp3shv0az2bb8n3dnjdfs8b0nhk3bq78iskkra")))

(define-public crate-do-proxy-0.1.1 (c (n "do-proxy") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "worker") (r "^0.0.12") (d #t) (k 0)))) (h "1kznw9q0w3436ipfd6ax7rk5zwk3cp2kd1gkqhg79m9fbkn8rjzl")))

