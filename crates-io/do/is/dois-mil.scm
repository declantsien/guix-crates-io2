(define-module (crates-io do is dois-mil) #:use-module (crates-io))

(define-public crate-dois-mil-1.0.0 (c (n "dois-mil") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "07illdmvknv3p0ksgm4knpcpfj7x67hfcqxsyn3ymv7hng4y1c5z")))

(define-public crate-dois-mil-1.0.1 (c (n "dois-mil") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "11jvc4klkd6xsrar9fmwssr0vparhfzh0rjhmq5fwshc8nl8g3is")))

(define-public crate-dois-mil-1.0.2 (c (n "dois-mil") (v "1.0.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1g4napwjwdvpp97pw9n1qca8hg992c3m0cgnv5vf50y8h646ml1i")))

