(define-module (crates-io do si dosio-proc) #:use-module (crates-io))

(define-public crate-dosio-proc-0.1.0 (c (n "dosio-proc") (v "0.1.0") (d (list (d (n "hdf5") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1rl05sz0w1qwy1kyjzjjvz5zf1n9i1y6gwyhzj4kr4aypacnpis0") (y #t)))

