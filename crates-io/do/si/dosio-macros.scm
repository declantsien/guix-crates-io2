(define-module (crates-io do si dosio-macros) #:use-module (crates-io))

(define-public crate-dosio-macros-0.1.0 (c (n "dosio-macros") (v "0.1.0") (d (list (d (n "hdf5") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0ky0ds7a8vmxj658r1j3kqqyrvcjv48xk8gdfjj8n4zb46al09v4")))

(define-public crate-dosio-macros-0.1.1 (c (n "dosio-macros") (v "0.1.1") (d (list (d (n "hdf5") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1rm6ymffnpa9hynfvv9vqqz4v4i0rg9i40acp8zdkldzcj8y05vk")))

(define-public crate-dosio-macros-0.1.2 (c (n "dosio-macros") (v "0.1.2") (d (list (d (n "hdf5") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0sa4kdahamarb5rxanhfglxqii752k88szqbchw0dz0aad0x83jf")))

(define-public crate-dosio-macros-0.1.3 (c (n "dosio-macros") (v "0.1.3") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ih3gql5z0fd70xyf67f37k7cb8xqzv6pzcgx96q7yllcjdbyxvi")))

(define-public crate-dosio-macros-0.1.4 (c (n "dosio-macros") (v "0.1.4") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0rhjc07idqmavn6i8ljrskr0baffi95l471c9llskiwa6gmwi7y6")))

(define-public crate-dosio-macros-0.1.5 (c (n "dosio-macros") (v "0.1.5") (d (list (d (n "arrow") (r "^6.5.0") (o #t) (d #t) (k 0)) (d (n "hdf5") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "parquet") (r "^6.5.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (o #t) (d #t) (k 0)))) (h "05n0whqnm6bvc0yhd0lpfq0bv6kjzzkhsg5adxvb38g3ifx87x3z") (f (quote (("prqt" "arrow" "parquet" "thiserror" "zip"))))))

