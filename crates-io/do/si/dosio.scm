(define-module (crates-io do si dosio) #:use-module (crates-io))

(define-public crate-dosio-0.1.0 (c (n "dosio") (v "0.1.0") (d (list (d (n "dosio-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 2)))) (h "1m193n8rirkp4b2jgzpln04jj9nls9fqr5brpkc2xzbvyqv18zrc")))

(define-public crate-dosio-0.1.1 (c (n "dosio") (v "0.1.1") (d (list (d (n "dosio-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 2)))) (h "0dhgnbj0rlins6wbbxafwm8zs468a98ym2wzzkxkd9gc2xk3lgmr")))

(define-public crate-dosio-0.1.2 (c (n "dosio") (v "0.1.2") (d (list (d (n "dosio-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 2)))) (h "0wz696xf3igrq18n5wh73ym4d3xa2lh6df9fjdk30yv45f5qvdli")))

(define-public crate-dosio-0.1.3 (c (n "dosio") (v "0.1.3") (d (list (d (n "dosio-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.2") (d #t) (k 2)))) (h "01jkdv4gjs99l7h9v386smncnj3hrnwzfny4mc26s98pas2d4b01") (f (quote (("prqt" "dosio-macros/prqt") ("default" "dosio-macros/hdf5"))))))

