(define-module (crates-io do cb docblock) #:use-module (crates-io))

(define-public crate-docblock-0.0.0 (c (n "docblock") (v "0.0.0") (d (list (d (n "k9") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0rjdnngj32gja4n7qs5zgqprxb2lp56a9icq5x9hibyms2yw2dv6")))

(define-public crate-docblock-0.0.1 (c (n "docblock") (v "0.0.1") (d (list (d (n "k9") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "07gmx3qz50ngfjqqn68il71v7s00432dpm05fmimisccy33nxrpb")))

(define-public crate-docblock-0.0.2 (c (n "docblock") (v "0.0.2") (d (list (d (n "k9") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "15y33h2pgir7zn6s4kd6kpqcws592pxy5skkc1aad4nlyz6lmiqr")))

