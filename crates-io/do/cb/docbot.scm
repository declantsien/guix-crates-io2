(define-module (crates-io do cb docbot) #:use-module (crates-io))

(define-public crate-docbot-0.2.0-alpha.1 (c (n "docbot") (v "0.2.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "docbot-derive") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mxsf24q7b1vspgksv2knm27nbr7914g9d429bmnpbabpcki6wrv")))

(define-public crate-docbot-0.3.0-alpha.1 (c (n "docbot") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "docbot-derive") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "155h6mfvr4vv5c60svbrfb76g1k5r9mkyx56l9qg36q4hgv5k0ih")))

(define-public crate-docbot-0.3.0-alpha.2 (c (n "docbot") (v "0.3.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "docbot-derive") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wnflh45rd7s5sgw2xm0v3iq825qjb108mng6dg7cp7dl34vlwx8") (f (quote (("did-you-mean" "strsim") ("default" "did-you-mean"))))))

