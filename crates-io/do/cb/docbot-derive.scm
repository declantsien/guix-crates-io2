(define-module (crates-io do cb docbot-derive) #:use-module (crates-io))

(define-public crate-docbot-derive-0.2.0-alpha.1 (c (n "docbot-derive") (v "0.2.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (f (quote ("derive" "extra-traits" "full"))) (d #t) (k 0)))) (h "0jvsrjqr9k0sbk7231bp2l2wv9kaxmfkli6s009y2i95ic6v0n7m")))

(define-public crate-docbot-derive-0.3.0-alpha.1 (c (n "docbot-derive") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (f (quote ("derive" "extra-traits" "full"))) (d #t) (k 0)))) (h "0da708zcnp8j7rvggf3axrd422zpd408kwmhc84q4agd565vbj6n")))

(define-public crate-docbot-derive-0.3.0-alpha.2 (c (n "docbot-derive") (v "0.3.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (f (quote ("derive" "extra-traits" "full"))) (d #t) (k 0)))) (h "04vs50sp7a4bjdi7h9i767lfywxhd695rshmifazjjp43h4k7rih")))

