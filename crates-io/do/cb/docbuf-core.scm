(define-module (crates-io do cb docbuf-core) #:use-module (crates-io))

(define-public crate-docbuf-core-0.1.0 (c (n "docbuf-core") (v "0.1.0") (d (list (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1shqiy34whv4rgnlrlk0f9bb9r14xcnfb54sl3dn70jyxhhkw7zh")))

