(define-module (crates-io do dr dodrio-js-api) #:use-module (crates-io))

(define-public crate-dodrio-js-api-0.1.0 (c (n "dodrio-js-api") (v "0.1.0") (d (list (d (n "dodrio") (r "= 0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.38") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.3.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.38") (d #t) (k 2)))) (h "1xrapzz1liv6vaxpb7mdbqxl30apjx408c6gjvmif2fsyw7afzq7")))

(define-public crate-dodrio-js-api-0.2.0 (c (n "dodrio-js-api") (v "0.2.0") (d (list (d (n "dodrio") (r "= 0.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)))) (h "03zgqwps5ixpxds9nsbwmgfaxqx0006c5lxj5sfmy7zjag27y5pg")))

