(define-module (crates-io do c- doc-sql) #:use-module (crates-io))

(define-public crate-doc-sql-0.0.0 (c (n "doc-sql") (v "0.0.0") (h "0lgnr0xv2jsxbvdc885zbjgfwriwd6zkxc9gws5wb9ljjk0x2bvw")))

(define-public crate-doc-sql-0.0.1 (c (n "doc-sql") (v "0.0.1") (h "14fky4jx33g7855mi99wpcnc72yfl6ryn4rz0r177qr6fjqpxi4k")))

(define-public crate-doc-sql-0.0.2 (c (n "doc-sql") (v "0.0.2") (h "063nb9liwa91p9x6abphkq395gcz8i760swfs5cmkh550wygsysv")))

(define-public crate-doc-sql-0.0.3 (c (n "doc-sql") (v "0.0.3") (h "1amm3xrfl86b46qm0nw017vnwj9lh6ialdk6ddcz7b9fsgfj3wg6")))

