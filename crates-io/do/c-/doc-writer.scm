(define-module (crates-io do c- doc-writer) #:use-module (crates-io))

(define-public crate-doc-writer-0.1.0 (c (n "doc-writer") (v "0.1.0") (h "0wfg55d0k4n4s8qpzdqipahifk5lw4v0b1iwgicagw9ngi01q0vv")))

(define-public crate-doc-writer-0.2.0 (c (n "doc-writer") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1gyk4kkdp259hgp4xamb56igy0k087pprjhh4v9wfm0rl02zbna4") (f (quote (("parse-markdown" "pulldown-cmark"))))))

