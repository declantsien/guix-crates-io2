(define-module (crates-io do c- doc-type) #:use-module (crates-io))

(define-public crate-doc-type-0.1.0 (c (n "doc-type") (v "0.1.0") (h "1yfqv59si7m7wrdnip8f7927jmp5dwfhmg77cvgwvnylyhv56lgp")))

(define-public crate-doc-type-0.1.1 (c (n "doc-type") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "specta") (r "^1.0.2") (d #t) (k 0)))) (h "0j1yzb5vnpi73dxl1l78gaynjrbibg2l34l8g34dgw6prx49psrl")))

(define-public crate-doc-type-0.1.2 (c (n "doc-type") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "specta") (r "^1.0.2") (d #t) (k 0)))) (h "1fnj84hsvp7ikx4mc08sw74zn6lxkhnxfz83zf56ssc0v8c27a1c") (f (quote (("rust") ("default" "rust"))))))

