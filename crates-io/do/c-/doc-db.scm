(define-module (crates-io do c- doc-db) #:use-module (crates-io))

(define-public crate-doc-db-0.0.0 (c (n "doc-db") (v "0.0.0") (h "1h9m0bay1z0b546mdk8q4rz8ih1chgybch4hr3kaa48dzs94wmki")))

(define-public crate-doc-db-0.0.1 (c (n "doc-db") (v "0.0.1") (h "1w99ryjkg151bxhqqcf0n1rasmmx67z0qh8m5ydw68ry4rccf6h3")))

(define-public crate-doc-db-0.0.2 (c (n "doc-db") (v "0.0.2") (h "0v6qm7wnynzh7n52bs77y9wwmw9fsqi8jlhg5hs981jmb622qa63")))

(define-public crate-doc-db-0.0.3 (c (n "doc-db") (v "0.0.3") (h "1acaazj1762l0rs7r9q383x4ffdnrwacfs4qlr6brxmqxp7xbp22")))

