(define-module (crates-io do c- doc-merge) #:use-module (crates-io))

(define-public crate-doc-merge-0.1.0 (c (n "doc-merge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01zj6pkpwn766cnpg01jsinzwcn5kh4d889jfm21jv3hhibrn368")))

(define-public crate-doc-merge-0.2.0 (c (n "doc-merge") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m873l3pbkj2dqs871lndxjdd6c051vzpinyamwpxiw8x80bylr3")))

(define-public crate-doc-merge-0.3.0 (c (n "doc-merge") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "043vgsnrhql3yjkrzw92da66jhbwxj47d94j32dyx27cd78rix62")))

