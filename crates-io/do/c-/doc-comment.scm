(define-module (crates-io do c- doc-comment) #:use-module (crates-io))

(define-public crate-doc-comment-0.1.0 (c (n "doc-comment") (v "0.1.0") (h "0b2098ks0hmf69a1cqaijwwfwgpa2d49bjmc9vfnci1hmz6iv4y5")))

(define-public crate-doc-comment-0.1.1 (c (n "doc-comment") (v "0.1.1") (h "0mdaiq7carsp658s9pvkiq0n185pzairdzqd1jvn67bp88hd7nzs")))

(define-public crate-doc-comment-0.1.2 (c (n "doc-comment") (v "0.1.2") (h "14ilz03nhc7r0zldx8y7sfiqvlki0qj906qk1m9kj881s9jk96d1") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.1.3 (c (n "doc-comment") (v "0.1.3") (h "169q1lv5yihfbmibp6y6nvkly3w0sw2fll69dkyik11hbhz6hq2y") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.1.4 (c (n "doc-comment") (v "0.1.4") (h "16mjgl1ldahgdqi4k8i8zr285s466smfzmq8yxwh1dmmskfl8q6d") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.2.0 (c (n "doc-comment") (v "0.2.0") (h "1slxzxfldawz4kwfrk8b331gdb1xacd59bp4x48x662lkyy36fvi") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.2.1 (c (n "doc-comment") (v "0.2.1") (h "0q5di5wdkw252x9571ixqy0s20f87n33xvxq7x2bh3v3c6hkyjci") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.2.2 (c (n "doc-comment") (v "0.2.2") (h "1l9k7bhmvx3l84qalsyqmw8q0adzzqcypw9l36gf11n4x9dgcs3c") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.2.3 (c (n "doc-comment") (v "0.2.3") (h "1q479gwcwh63756qaqqwyhywvxbn688shf6g19ljn76y94wpbb5d") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.2.4 (c (n "doc-comment") (v "0.2.4") (h "0d3lbcxi4xkiw8xzc77iw8008i9rbfkcb1ax7x08fc4j47l38l6j") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.3.0 (c (n "doc-comment") (v "0.3.0") (h "1fmlj5aw2sfp0v3f0i7i5h6q1rirs4cldn25cia6payqnrss26p0") (f (quote (("no_core"))))))

(define-public crate-doc-comment-0.3.1 (c (n "doc-comment") (v "0.3.1") (h "15rsqxgarfpb1yim9sbp9yfgj7p2dq6v51c6bq1a62paii9ylgcj") (f (quote (("old_macros") ("no_core"))))))

(define-public crate-doc-comment-0.3.2 (c (n "doc-comment") (v "0.3.2") (h "09wwqsmrxb8cfkl5d1pbc2i7cvs0s4p4kpk6mhga3mlsqd3mhzl0") (f (quote (("old_macros") ("no_core"))))))

(define-public crate-doc-comment-0.3.3 (c (n "doc-comment") (v "0.3.3") (h "043sprsf3wl926zmck1bm7gw0jq50mb76lkpk49vasfr6ax1p97y") (f (quote (("old_macros") ("no_core"))))))

(define-public crate-doc-comment-0.4.0 (c (n "doc-comment") (v "0.4.0") (h "024311lw4bh8z2n6g66z5hpk84xi9fnria6xr7440k7l3nl0khh6") (f (quote (("debug")))) (y #t)))

(define-public crate-doc-comment-0.4.1 (c (n "doc-comment") (v "0.4.1") (h "180i7xsn8vl1vgl0dpp7pk7ganbqb9nlb3s389ra3jphjsn2d8x4") (f (quote (("debug")))) (y #t)))

