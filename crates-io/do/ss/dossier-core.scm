(define-module (crates-io do ss dossier-core) #:use-module (crates-io))

(define-public crate-dossier-core-0.0.2 (c (n "dossier-core") (v "0.0.2") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "0bci9ic42pikswh478c7i69r8pwdg50zz3irigkmfahadjcsjmya") (y #t)))

(define-public crate-dossier-core-0.0.4 (c (n "dossier-core") (v "0.0.4") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1ky5dagz1gmx2p6drxp4mbbvvramv6pfnj5924kbsq22xjvbww8d")))

