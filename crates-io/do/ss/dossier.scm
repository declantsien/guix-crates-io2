(define-module (crates-io do ss dossier) #:use-module (crates-io))

(define-public crate-dossier-0.0.4 (c (n "dossier") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dossier-core") (r "^0.0.4") (d #t) (k 0)) (d (n "dossier-py") (r "^0.0.4") (d #t) (k 0)) (d (n "dossier-ts") (r "^0.0.4") (d #t) (k 0)) (d (n "pretty-duration") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wild") (r "^2.2.0") (d #t) (k 0)))) (h "09rb9blf9dn5izq082a6h98p99d4f0lhx7kwl7ii5vnhan2nw1ma")))

