(define-module (crates-io do ss dossier-py) #:use-module (crates-io))

(define-public crate-dossier-py-0.0.4 (c (n "dossier-py") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "dossier-core") (r "^0.0.4") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20.4") (d #t) (k 0)))) (h "0fi6pj20iih6ysgw78mr4q5xz6az8kwwq5gbyzp0kxlxhim11gis")))

