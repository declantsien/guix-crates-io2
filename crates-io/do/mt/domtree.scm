(define-module (crates-io do mt domtree) #:use-module (crates-io))

(define-public crate-domtree-0.1.0 (c (n "domtree") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1sm9bhcz5hgw1nr7m2saaj3ksd1lwa17cjlzp5i4jplmv3r6vrp5")))

(define-public crate-domtree-0.2.0 (c (n "domtree") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p81rdqvj1i0mqbxdk1rwidcx3al23hvlzzj2rkmh5xl58wwskr2") (f (quote (("materialized_idf"))))))

