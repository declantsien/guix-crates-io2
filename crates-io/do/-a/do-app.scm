(define-module (crates-io do -a do-app) #:use-module (crates-io))

(define-public crate-do-app-0.1.0 (c (n "do-app") (v "0.1.0") (d (list (d (n "console") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04hqqsx0dk1vqca18gmicn8ya6nkykpgfc1h63x2gd56zidz6hfb")))

