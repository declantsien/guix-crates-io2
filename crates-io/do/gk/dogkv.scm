(define-module (crates-io do gk dogkv) #:use-module (crates-io))

(define-public crate-dogkv-0.1.0 (c (n "dogkv") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11.0") (d #t) (k 2)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 2)))) (h "1xdk42zxpgri9z1imz7bb3ixq32fsiqkb7vvygbv9jzw7lny0wcn")))

