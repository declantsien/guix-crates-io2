(define-module (crates-io do om doomsayer) #:use-module (crates-io))

(define-public crate-doomsayer-0.1.0 (c (n "doomsayer") (v "0.1.0") (h "1g96cafs1fkj2xkq33cqr72gy4j9yqx85d0qzcwzb5jlmql4alv3")))

(define-public crate-doomsayer-0.1.1 (c (n "doomsayer") (v "0.1.1") (h "1hjsia7xvnzxsick1h3xhz4hyqgbbz7zxgd6zjdq0r4lqcqy5zn6")))

