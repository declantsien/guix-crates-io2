(define-module (crates-io do om doomdooz-lib) #:use-module (crates-io))

(define-public crate-doomdooz-lib-0.1.0 (c (n "doomdooz-lib") (v "0.1.0") (d (list (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lib-ruby-parser") (r "^3.0.0") (d #t) (k 0)) (d (n "merge-yaml-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "11b9rqgy978hmnpdzihwawx9r50776paljywbjyry465k1pivm7n")))

