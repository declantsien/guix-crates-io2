(define-module (crates-io do om doomba) #:use-module (crates-io))

(define-public crate-doomba-0.1.0 (c (n "doomba") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0") (d #t) (k 2)) (d (n "paho-mqtt") (r "^0.11.1") (f (quote ("vendored-ssl"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hmr19n4lqr80s24x94a3yyyzqdx39fqkbwqsnf9wwgab5px5zhn")))

