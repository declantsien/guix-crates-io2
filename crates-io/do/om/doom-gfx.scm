(define-module (crates-io do om doom-gfx) #:use-module (crates-io))

(define-public crate-doom-gfx-0.1.0 (c (n "doom-gfx") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "wad") (r "^0.2.0") (d #t) (k 0)))) (h "0a02fmbrvsdh08v2d9gzz1fz0sh8vmbmab15wk55sibh866r3zlx")))

(define-public crate-doom-gfx-0.1.1 (c (n "doom-gfx") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "wad") (r "^0.2.0") (d #t) (k 0)))) (h "1dhyzc87n687pczx7329l0w5lx50b5ns9v1z4qm1a6r04mpk10cp")))

(define-public crate-doom-gfx-0.1.2 (c (n "doom-gfx") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "wad") (r "^0.2.0") (d #t) (k 0)))) (h "0frd8w9sxgs0dflg3jbdq6mggbpxw34gzxqwg0yk1m1mcvb1qcwf")))

(define-public crate-doom-gfx-0.1.3 (c (n "doom-gfx") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "wad") (r "^0.2.0") (d #t) (k 0)))) (h "0nc6dmd48njarjcfjlr7wbc1r8rdd0bc4znmh4pvmjd4a2nffwy2")))

