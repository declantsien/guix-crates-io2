(define-module (crates-io do om doom) #:use-module (crates-io))

(define-public crate-doom-0.1.0 (c (n "doom") (v "0.1.0") (h "1cv54clq2as09rvg5zklh1j02y2rrq29xahml14jn4jgkh06fvnk")))

(define-public crate-doom-0.1.1 (c (n "doom") (v "0.1.1") (h "1l255kblrvi9ba4fpwvhs7z6zmgz69j7jcpzbkzsx36a7rcs4va5")))

