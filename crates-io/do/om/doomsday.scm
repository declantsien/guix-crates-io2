(define-module (crates-io do om doomsday) #:use-module (crates-io))

(define-public crate-doomsday-0.1.0 (c (n "doomsday") (v "0.1.0") (h "0yd2zcascyapna00rwp0vbfbsa0xg0a2nwadhb92rg7qkkn3497w")))

(define-public crate-doomsday-0.2.0 (c (n "doomsday") (v "0.2.0") (h "0860ffzc8lbxwhxy7jz10ikhk3j74p08iwq1hagj3gyfgs0pw2ij")))

(define-public crate-doomsday-0.3.0 (c (n "doomsday") (v "0.3.0") (h "0jiaazaphparkzq4bkv6l3d0x4icjbsw6065iks0w5rr49sm2yn2")))

