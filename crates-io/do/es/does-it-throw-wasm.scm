(define-module (crates-io do es does-it-throw-wasm) #:use-module (crates-io))

(define-public crate-does-it-throw-wasm-0.1.0 (c (n "does-it-throw-wasm") (v "0.1.0") (d (list (d (n "does-it-throw") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.0") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.3") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.96") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "16zcw0vi38f43ryfxikdgvwqd0gxssapyfpadqkr6zl0l3cl1ykw")))

