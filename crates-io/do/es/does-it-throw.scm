(define-module (crates-io do es does-it-throw) #:use-module (crates-io))

(define-public crate-does-it-throw-0.1.0 (c (n "does-it-throw") (v "0.1.0") (d (list (d (n "swc_common") (r "^0.33") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.96") (d #t) (k 0)))) (h "17kw0ymk5nyj3567jixqrxz3br19hp9m881gxp78pjzp588mmyrk")))

