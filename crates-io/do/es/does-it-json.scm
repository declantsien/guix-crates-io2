(define-module (crates-io do es does-it-json) #:use-module (crates-io))

(define-public crate-does-it-json-0.0.1 (c (n "does-it-json") (v "0.0.1") (d (list (d (n "expectorate") (r "^1.0.5") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1gdf3v1ya0in1cf1kxag6rs941sfd59liirvm3nqvjbqb5mcz507")))

(define-public crate-does-it-json-0.0.2 (c (n "does-it-json") (v "0.0.2") (d (list (d (n "expectorate") (r "^1.0.5") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "072cn7k208sd96c8zrkszndqrmhg4rpskp3dcdmn39l0xkb15z5c")))

(define-public crate-does-it-json-0.0.3 (c (n "does-it-json") (v "0.0.3") (d (list (d (n "expectorate") (r "^1.0.5") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1wxcx5dhcscwxr4k3wg04y5hxd9lp9jhr6hj6zfbx1k9bjc8a4mb")))

(define-public crate-does-it-json-0.0.4 (c (n "does-it-json") (v "0.0.4") (d (list (d (n "expectorate") (r "^1.0.5") (d #t) (k 2)) (d (n "regress") (r "^0.4.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nbm6jiv73sk9q8yf8hagbmb671qcyj1jp9g4wivn5ji2q7nlwpp")))

