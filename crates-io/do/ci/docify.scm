(define-module (crates-io do ci docify) #:use-module (crates-io))

(define-public crate-docify-0.1.0 (c (n "docify") (v "0.1.0") (d (list (d (n "docify_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1yh2x4b6n9ijk4py30n2pqypz11kwp49vfh2f3w3f90v52n0vp4s")))

(define-public crate-docify-0.1.1 (c (n "docify") (v "0.1.1") (d (list (d (n "docify_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0mwffm0iz51bbcbasrdvwc2sx1gdqalzkirqy3bldk2qdv7nkd00")))

(define-public crate-docify-0.1.2 (c (n "docify") (v "0.1.2") (d (list (d (n "docify_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0f623irqch2yb2rh36x3zxa6kv30rcns2jf9d155q9x0r3nhc4js")))

(define-public crate-docify-0.1.3 (c (n "docify") (v "0.1.3") (d (list (d (n "docify_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0mljg15fm9yn7fa3z5x4iiidcwhhscnqdh0jgxym0hyq1mj23z2b")))

(define-public crate-docify-0.1.4 (c (n "docify") (v "0.1.4") (d (list (d (n "docify_macros") (r "^0.1.4") (d #t) (k 0)))) (h "1s6z4d3fv2wz19kcdam04xw4rml99lhdbyrgskidizcp66g1435y")))

(define-public crate-docify-0.1.5 (c (n "docify") (v "0.1.5") (d (list (d (n "docify_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0zhxl4ip1gfhj5dxrrkrvqvdn8lfhfdm73dh1n88rjbsy79mm5d1")))

(define-public crate-docify-0.1.6 (c (n "docify") (v "0.1.6") (d (list (d (n "docify_macros") (r "^0.1.6") (d #t) (k 0)))) (h "0iky33ypf7km2v3qyniq6im2q2nav54v2wb8apmls8baz3fhndqp")))

(define-public crate-docify-0.1.7 (c (n "docify") (v "0.1.7") (d (list (d (n "docify_macros") (r "^0.1.7") (d #t) (k 0)))) (h "03vgj1xdir8bdzb5kpmgbgr204x9v011wdk27pzh1k4xz2fvjhpx")))

(define-public crate-docify-0.1.8 (c (n "docify") (v "0.1.8") (d (list (d (n "docify_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0i3gg5hd7jb0ybgz5fzrn7jwv25r7xi5pbpl8jixfdgpzfpp9qq0")))

(define-public crate-docify-0.1.9 (c (n "docify") (v "0.1.9") (d (list (d (n "docify_macros") (r "^0.1.9") (d #t) (k 0)))) (h "1mwqm1sjpak59wbv2h5lfzazjc65qnivhycd15c15qgl6mh83yf1")))

(define-public crate-docify-0.1.10 (c (n "docify") (v "0.1.10") (d (list (d (n "docify_macros") (r "^0.1.10") (d #t) (k 0)))) (h "00bg27i5kym328670p8dn13p9czkadi57nsi8ac55nacxg1wqrvp")))

(define-public crate-docify-0.1.11 (c (n "docify") (v "0.1.11") (d (list (d (n "docify_macros") (r "^0.1.11") (d #t) (k 0)))) (h "1akklh1kah5lb48i38f0maczx04wy3cx9bxgxsf3m6vb4yy0kraw")))

(define-public crate-docify-0.1.12 (c (n "docify") (v "0.1.12") (d (list (d (n "docify_macros") (r "^0.1.12") (d #t) (k 0)))) (h "0sj92ha6m2flfx3a5s2kzady9lb1h1sw955qzq5xybxrg71qhigf")))

(define-public crate-docify-0.1.13 (c (n "docify") (v "0.1.13") (d (list (d (n "docify_macros") (r "^0.1.13") (d #t) (k 0)))) (h "0agizkkrxi8bv00zs9hf6mbz557z698nc1vazhwfijrh9jvp5f8q")))

(define-public crate-docify-0.1.14 (c (n "docify") (v "0.1.14") (d (list (d (n "docify_macros") (r "^0.1.14") (d #t) (k 0)))) (h "1xiz5cmhhi731llickimj7p02z18l2rd1ics2jrjvxhg6l5j3ahm")))

(define-public crate-docify-0.1.15 (c (n "docify") (v "0.1.15") (d (list (d (n "docify_macros") (r "^0.1.15") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1vhsvk80d012rk1v4cb4fp3pcmbypam74b3ggm0bh59jdpv50nrc") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.1.16 (c (n "docify") (v "0.1.16") (d (list (d (n "docify_macros") (r "^0.1.16") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1i7kn6if3h1cycq2y1ww3q2gx6bzl6y35c5p7sfi289xxzk086xg") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.0 (c (n "docify") (v "0.2.0") (d (list (d (n "docify_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1fblmv9k6g3f3lwq8q2m4ys9h0a89miayk94a6wwxdvgyw4ifjgn") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.1 (c (n "docify") (v "0.2.1") (d (list (d (n "docify_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1lnrqwx56ppn3ka9kzrvhn5lhvdz5kxs74ada9liklbms5qfi782") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.2 (c (n "docify") (v "0.2.2") (d (list (d (n "docify_macros") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "0ml7i21xbvnv3yw92sj8zs5p10969fymmjhgs1m3j99izm8wcscc") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.3 (c (n "docify") (v "0.2.3") (d (list (d (n "docify_macros") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1wscsmj25zcbzlzpdjrwvsbla8345q9r7mdkdsrqdjp7m1m9sl7z") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.4 (c (n "docify") (v "0.2.4") (d (list (d (n "docify_macros") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1yk2hlmdn3dj1kjd2ky4x924h0jrir8yk5qr33aibp8xa2655vkn") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.5 (c (n "docify") (v "0.2.5") (d (list (d (n "docify_macros") (r "^0.2.5") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "0r1g0mpz7gsbsivd58f86a3ap4xpd7gc9xcz1vbx57hmhv18dgw0") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.6 (c (n "docify") (v "0.2.6") (d (list (d (n "docify_macros") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "0bwdip2qnay9mhz72x6w2vggzw5dyd3cdsbz0294pfp292rfjda2") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.7 (c (n "docify") (v "0.2.7") (d (list (d (n "docify_macros") (r "^0.2.7") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1qmf59hgr78zm6l9dgvzm1jy3lb00fh84k3rf2n9iyx9m8wgvi3w") (f (quote (("generate-readme") ("default"))))))

(define-public crate-docify-0.2.8 (c (n "docify") (v "0.2.8") (d (list (d (n "docify_macros") (r "^0.2.8") (d #t) (k 0)) (d (n "proc-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1kh2d0516qx2caq36fcjqns119m691hmg96irsic87ajmlwg38j3") (f (quote (("generate-readme") ("default"))))))

