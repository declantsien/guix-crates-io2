(define-module (crates-io do it doit) #:use-module (crates-io))

(define-public crate-doit-0.2.0 (c (n "doit") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1gah7xjscf1n1wl3za6ay04892lkq424d0dpqr39fip6fbs5a60q")))

