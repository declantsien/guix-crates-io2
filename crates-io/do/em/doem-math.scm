(define-module (crates-io do em doem-math) #:use-module (crates-io))

(define-public crate-doem-math-1.0.0 (c (n "doem-math") (v "1.0.0") (d (list (d (n "staticvec") (r "^0.8.0") (d #t) (k 0)))) (h "0z0xcf94k81qng7wxbf5k9mdfmdb70f5y6caibgimxn4kyk8v4k5")))

(define-public crate-doem-math-1.0.1 (c (n "doem-math") (v "1.0.1") (d (list (d (n "staticvec") (r "^0.8.0") (d #t) (k 0)))) (h "05vbz6s7d2irrn2sdgnw4ncsc2pbscv2b4czl6ac7s8v9v5vs8fr")))

