(define-module (crates-io do cl doclog) #:use-module (crates-io))

(define-public crate-doclog-0.1.0 (c (n "doclog") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0ardc36xjlc00kd37vnxpmmmw7dsnc3j09vd1a10fkdccwv16rf5")))

(define-public crate-doclog-0.1.1 (c (n "doclog") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1a6ppcx6b08zjsrs0vdpx89b1bf5q59vm0abhqzqkqix8cf9v62m")))

(define-public crate-doclog-0.1.2 (c (n "doclog") (v "0.1.2") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "12942wcnya1g9yf00l5rj6jd2xh2l4x4j4b7nbfrc0gb152m4k5d")))

(define-public crate-doclog-0.1.3 (c (n "doclog") (v "0.1.3") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1053qby1qc8bn7v88n3m1yamzwqbw6fdanf5pj1nss86l09qy940")))

