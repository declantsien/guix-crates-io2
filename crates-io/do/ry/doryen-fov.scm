(define-module (crates-io do ry doryen-fov) #:use-module (crates-io))

(define-public crate-doryen-fov-0.1.0 (c (n "doryen-fov") (v "0.1.0") (d (list (d (n "doryen-rs") (r "1.*") (d #t) (k 2)) (d (n "rand") (r "0.6.*") (d #t) (k 2)))) (h "0vmf64z7qnhzycpqrq9yf4zj4k1x672cxf63fd6xpaqm3fpfbl4b") (y #t)))

(define-public crate-doryen-fov-0.1.1 (c (n "doryen-fov") (v "0.1.1") (d (list (d (n "doryen-rs") (r "1.*") (d #t) (k 2)) (d (n "rand") (r "0.6.*") (d #t) (k 2)))) (h "1abssa8ppc9bck3c4vs9xls6cfp0bzq5yr98qy0xalkv2zw6x3ia")))

