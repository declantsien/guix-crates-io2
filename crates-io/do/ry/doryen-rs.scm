(define-module (crates-io do ry doryen-rs) #:use-module (crates-io))

(define-public crate-doryen-rs-0.1.0 (c (n "doryen-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)))) (h "0zakrxahd68cyjq5n18pl4r5g39n5mnx329pl81rshsjaq8s8gcl")))

(define-public crate-doryen-rs-1.0.0 (c (n "doryen-rs") (v "1.0.0") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)))) (h "0x2nhlay8264ghidr04gxxz3w7jamz7sd4zkb666avg8fqr3sd1l")))

(define-public crate-doryen-rs-1.0.1 (c (n "doryen-rs") (v "1.0.1") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)))) (h "1gzx0p6669ajqh225kgplixc6c4xl0wgb69s4ppfdwgskq9gh61r")))

(define-public crate-doryen-rs-1.1.0 (c (n "doryen-rs") (v "1.1.0") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)))) (h "09m3nwwwvchr211i155p8p9kmy9pfcicjfmms4j9yljd0rsqsbl7")))

(define-public crate-doryen-rs-1.2.0 (c (n "doryen-rs") (v "1.2.0") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)))) (h "1dkq6jkhq6xk3zwws7jsnjrp8m4j85jfmrmz6ls0byyi81j5mcfq")))

(define-public crate-doryen-rs-1.2.1 (c (n "doryen-rs") (v "1.2.1") (d (list (d (n "doryen-fov") (r "0.1.*") (d #t) (k 2)) (d (n "image") (r "0.22.*") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 2)))) (h "0klgjw4dbn20r63acvcck2wdn20024x3l786q2jmbsydpdjm8i9l")))

(define-public crate-doryen-rs-1.2.2 (c (n "doryen-rs") (v "1.2.2") (d (list (d (n "doryen-fov") (r "0.1.*") (d #t) (k 2)) (d (n "image") (r "0.22.*") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 2)))) (h "0ssavclfr1h7km3rjglhpr0v84ghv8060lavwwk2sm5pl80ji67z")))

(define-public crate-doryen-rs-1.2.3 (c (n "doryen-rs") (v "1.2.3") (d (list (d (n "doryen-fov") (r "0.1.*") (d #t) (k 2)) (d (n "image") (r "0.22.*") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "0.1.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.1.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 2)))) (h "0inz4injbx1kx3w0a69fx8f9cbid3wd3l1fcvp8b7j3yhgzrkqwx")))

(define-public crate-doryen-rs-1.3.0 (c (n "doryen-rs") (v "1.3.0") (d (list (d (n "doryen-fov") (r "0.1.*") (d #t) (k 2)) (d (n "image") (r "0.23.*") (f (quote ("png"))) (k 0)) (d (n "uni-app") (r "0.2.*") (d #t) (k 0)) (d (n "uni-gl") (r "0.2.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 2)))) (h "0wwrl1ffj5xkwdf78c6hdyw34f8vn88rpj4ml5n9ligz6ccd8jyg")))

