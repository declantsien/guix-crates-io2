(define-module (crates-io do _s do_syscall) #:use-module (crates-io))

(define-public crate-do_syscall-1.0.0 (c (n "do_syscall") (v "1.0.0") (h "0ghd6bc2h3d0avknzwdpyx4l1rym5qlgfd3cq6q91zcp5z8igmnv") (y #t)))

(define-public crate-do_syscall-1.0.1 (c (n "do_syscall") (v "1.0.1") (h "18ya1g2zhj9al1fmkm0df9zn99mygf8z8fnv7jxm4228f2zi837f") (y #t)))

(define-public crate-do_syscall-1.0.2 (c (n "do_syscall") (v "1.0.2") (h "1dxjlkdn3640vbjf7a5br42mb0b7r4lji5ldr4snjy2dn6k0bil7")))

