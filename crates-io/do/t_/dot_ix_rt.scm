(define-module (crates-io do t_ dot_ix_rt) #:use-module (crates-io))

(define-public crate-dot_ix_rt-0.1.0 (c (n "dot_ix_rt") (v "0.1.0") (d (list (d (n "dot_ix_model") (r "^0.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "0ah3qxww4gmb2svx1v1i77hdlh4frzih7z0bxw4fwv6g5nyrx29d")))

(define-public crate-dot_ix_rt-0.2.0 (c (n "dot_ix_rt") (v "0.2.0") (d (list (d (n "dot_ix_model") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "1zqq4vfrrivhvx8wpqm2q8lf3zpmw6a1mp66csphdk9x26w1pyap")))

(define-public crate-dot_ix_rt-0.3.0 (c (n "dot_ix_rt") (v "0.3.0") (d (list (d (n "dot_ix_model") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "1wb0jzysf6d56ajc43njw752r03vr3r16x0lpl1q9kqlsy6b7vjf")))

(define-public crate-dot_ix_rt-0.4.0 (c (n "dot_ix_rt") (v "0.4.0") (d (list (d (n "dot_ix_model") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "1155bd6sib4syf3km6c1f3rpcmlvpsy4x0gqzl2b9jgdx8bj7bvc")))

(define-public crate-dot_ix_rt-0.4.1 (c (n "dot_ix_rt") (v "0.4.1") (d (list (d (n "dot_ix_model") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "18fcylppkdlf8fn7j9414ng2rnng8ah3kpq730hl98d92yc0ynvh")))

(define-public crate-dot_ix_rt-0.5.0 (c (n "dot_ix_rt") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "dot_ix_model") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)))) (h "0nxgg6kh13xivr6phm8pm43pba14s44vpbri0s6pg1qfl1b86bm7") (f (quote (("info_graph_html") ("default"))))))

