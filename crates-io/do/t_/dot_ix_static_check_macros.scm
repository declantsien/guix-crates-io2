(define-module (crates-io do t_ dot_ix_static_check_macros) #:use-module (crates-io))

(define-public crate-dot_ix_static_check_macros-0.1.0 (c (n "dot_ix_static_check_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1qmq5sxxpm3kq7mkndmvaiiykds5b01ifvmxk8vqiv505m6h8827")))

(define-public crate-dot_ix_static_check_macros-0.2.0 (c (n "dot_ix_static_check_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "1lzf3x394qmgap4mxrrjh62jj1aqbcxw5hc5cgm3armrq1s76vpa")))

(define-public crate-dot_ix_static_check_macros-0.3.0 (c (n "dot_ix_static_check_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0ci2qyigx1lfanhhq2ib4zpdlcarzdkwbax8d347ld6h5a6hkcag")))

(define-public crate-dot_ix_static_check_macros-0.4.0 (c (n "dot_ix_static_check_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "14l567nj9a9gzhvnh2yki5js3cj47sl3lx45jm8klyrj12f02vqn")))

(define-public crate-dot_ix_static_check_macros-0.4.1 (c (n "dot_ix_static_check_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1qjddp7an8bdwz2xfqqjnv22awqm4891dcj0q3jcvkgf49mgfadm")))

(define-public crate-dot_ix_static_check_macros-0.5.0 (c (n "dot_ix_static_check_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1a3df2bc3nxa4zhd4ispz126xx52lg287q9f67y911iskk3lj6ma")))

