(define-module (crates-io do t_ dot_graph) #:use-module (crates-io))

(define-public crate-dot_graph-0.1.0 (c (n "dot_graph") (v "0.1.0") (h "1jj3wl9g7ipf223psmqq6c3aw05c02mmi63wxhva3gb2kicpyz6y")))

(define-public crate-dot_graph-0.2.0 (c (n "dot_graph") (v "0.2.0") (h "02r96mv72bfqq66my36jlhmkkwr79ya080h7vg69qffq956d00ik")))

(define-public crate-dot_graph-0.2.1 (c (n "dot_graph") (v "0.2.1") (h "0n18k5wpdqkp799g08qg8s5bsj32axgri9gba5jb09zpk0bfa915")))

(define-public crate-dot_graph-0.2.2 (c (n "dot_graph") (v "0.2.2") (h "0dzbyj6dbvmj3ipk6j2gvgy110hmsk5f9dxk1n6m5n099wdj3zw8")))

(define-public crate-dot_graph-0.2.3 (c (n "dot_graph") (v "0.2.3") (h "0bjyk507v1s4igllwby2f4sxswp85sl6r4bv7zlxv99vcgpys45k")))

