(define-module (crates-io do t_ dot_hide) #:use-module (crates-io))

(define-public crate-dot_hide-0.1.1 (c (n "dot_hide") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "101z0scmnxpgdzs96b6r7rsxa9ccs8s1svisr4wf136cqlm85y1c") (f (quote (("default")))) (s 2) (e (quote (("walkdir" "dep:walkdir"))))))

(define-public crate-dot_hide-0.1.2 (c (n "dot_hide") (v "0.1.2") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "0y861kd7zx5w0nv1ch57nnni6p8rr635h7y20rvi91r7rm038a1z") (f (quote (("default")))) (s 2) (e (quote (("walkdir" "dep:walkdir"))))))

