(define-module (crates-io do t_ dot_ix_model) #:use-module (crates-io))

(define-public crate-dot_ix_model-0.1.0 (c (n "dot_ix_model") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "12fvp6ww13g6lq1mbpl3gjpgpw1krg3aq494cabgqvy3mvmw9z0r")))

(define-public crate-dot_ix_model-0.2.0 (c (n "dot_ix_model") (v "0.2.0") (d (list (d (n "dot_ix_static_check_macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "15gncxr363m4afkl5j5ans9g6ciqjqlmcw91p4wixi7d8x6cb4fn") (s 2) (e (quote (("ssr" "dep:dot_ix_static_check_macros"))))))

(define-public crate-dot_ix_model-0.3.0 (c (n "dot_ix_model") (v "0.3.0") (d (list (d (n "dot_ix_static_check_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "196f2036h7w5qnyycn3cs9yy11m09q11pqc2giq39jllizmb5axs")))

(define-public crate-dot_ix_model-0.4.0 (c (n "dot_ix_model") (v "0.4.0") (d (list (d (n "dot_ix_static_check_macros") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2969fbywdrgqhyl0d7hhdc8s7rhm2nindr1s0q0fcqv0ck77fd")))

(define-public crate-dot_ix_model-0.4.1 (c (n "dot_ix_model") (v "0.4.1") (d (list (d (n "dot_ix_static_check_macros") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z8y9jw41pir489vmxsp11w62zfxsggsby7fzfg97z3vrfm0fg97")))

(define-public crate-dot_ix_model-0.5.0 (c (n "dot_ix_model") (v "0.5.0") (d (list (d (n "dot_ix_static_check_macros") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1digb5cv0cqq4j7wvdrgfy2a25n07ar30kfspqd8qnaiwimxnbms")))

