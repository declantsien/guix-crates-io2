(define-module (crates-io do t_ dot_vox_amethyst) #:use-module (crates-io))

(define-public crate-dot_vox_amethyst-0.1.0 (c (n "dot_vox_amethyst") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.8") (d #t) (k 2)) (d (n "amethyst_assets") (r "^0.4") (d #t) (k 0)) (d (n "amethyst_core") (r "^0.3.0") (d #t) (k 0)) (d (n "amethyst_renderer") (r "^0.8.0") (d #t) (k 0)) (d (n "avow") (r "^0.2") (d #t) (k 2)) (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "dot_vox") (r "^3.0.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.2") (d #t) (k 0)) (d (n "glsl-layout") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)))) (h "1b63nna9rzjx29r8hrxjd7xzq36mq4adgx6zc15fzz8q3b5v4jax")))

