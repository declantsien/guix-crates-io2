(define-module (crates-io do t_ dot_tree) #:use-module (crates-io))

(define-public crate-dot_tree-1.0.0 (c (n "dot_tree") (v "1.0.0") (d (list (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "108wfz6bf2vqlqjwwvr43lp9s0ky7nfi4vjdv28hk56287jzyb3l")))

(define-public crate-dot_tree-1.0.1 (c (n "dot_tree") (v "1.0.1") (d (list (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "0y1dahhdc6rrk7zwmgi15vwsvpslcmwmbx1rhb4x5ga3bg10i8mb")))

