(define-module (crates-io do t_ dot_vox) #:use-module (crates-io))

(define-public crate-dot_vox-0.1.0 (c (n "dot_vox") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0n3q3j7rpx58b3w7gsk6qnvnblggkw022drzrv4qykmm2khzb267")))

(define-public crate-dot_vox-0.2.0 (c (n "dot_vox") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "03knp6zws74w8yihyb2116vnvvinap5iswy7dyi5d8az9l8nvqyx")))

(define-public crate-dot_vox-0.3.0 (c (n "dot_vox") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0c1gng3mcswvdxvl5jdg3ph151639jh5b8rxi6i396q1cb1iva02")))

(define-public crate-dot_vox-0.4.0 (c (n "dot_vox") (v "0.4.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "01r781agqx18xrdkm954fs8753n34maa3w1ia4l52pa7ch6vx6h1")))

(define-public crate-dot_vox-0.5.0 (c (n "dot_vox") (v "0.5.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0am3r6r7cv42pjzjiaj2qg4p02d6hy6lw96w0b45xyr9ksl13j3b")))

(define-public crate-dot_vox-1.0.0 (c (n "dot_vox") (v "1.0.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "1qqkcyskyvvkx0lsbsllznj4mj0kjdljj7s3m8cf7ps5sm1fm1qb")))

(define-public crate-dot_vox-1.0.1 (c (n "dot_vox") (v "1.0.1") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0h8402bldxwha23ws7r8yjzavwba41yl5s14y3zm3w8y77yd395z")))

(define-public crate-dot_vox-2.0.0 (c (n "dot_vox") (v "2.0.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0jwxl6s110jyq3kcl2fm4f59rpm9rbs1637mpj0asv1vbwflyfgb")))

(define-public crate-dot_vox-3.0.0 (c (n "dot_vox") (v "3.0.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "176c0mmdhpzs096cbny6zj18y6ksvrkpzrnn5vh8wpkj7ckmzffd")))

(define-public crate-dot_vox-3.1.0 (c (n "dot_vox") (v "3.1.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0vwp3n9bclq7wlkiv90y024fhwxadyyqrpv30sfi4jh3ka3v4zlq")))

(define-public crate-dot_vox-4.0.0 (c (n "dot_vox") (v "4.0.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0w0ym2s57zq1pp6vshnayamgihjysnhv4nb649q2g3sq3qjx7bqi")))

(define-public crate-dot_vox-4.1.0 (c (n "dot_vox") (v "4.1.0") (d (list (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "0hdb8h6b938y422i63h6pzkkj5is9lwwhgksnyc07pjlxw2q9hc3")))

(define-public crate-dot_vox-5.0.0 (c (n "dot_vox") (v "5.0.0") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "0ismq9yqyb14sbpikzp5bhrm4xnjh77sbl3n0iwsbwfsbynz5whx") (f (quote (("default" "ahash"))))))

(define-public crate-dot_vox-5.1.0 (c (n "dot_vox") (v "5.1.0") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "glam") (r "^0.21") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "0v0h1z1wlirsndfviqjc46dd2f02d41nm9whc4rhswffqxb8nbw2") (f (quote (("default" "ahash"))))))

(define-public crate-dot_vox-5.1.1 (c (n "dot_vox") (v "5.1.1") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)) (d (n "avow") (r "^0.2.0") (d #t) (k 2)) (d (n "glam") (r "^0.21") (d #t) (k 2)))) (h "0fn1jkf2prjd0h67jz7q4habrr2pffsz3pcjdxf18x305nqcl1fx") (f (quote (("default" "ahash"))))))

