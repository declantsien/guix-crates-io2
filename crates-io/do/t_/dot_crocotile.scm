(define-module (crates-io do t_ dot_crocotile) #:use-module (crates-io))

(define-public crate-dot_crocotile-0.1.0 (c (n "dot_crocotile") (v "0.1.0") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0znvwsr1k0az5l233ah3lzkkvhs33xsjj5ix9qmw1c2x7hgzfh3b")))

(define-public crate-dot_crocotile-0.2.0 (c (n "dot_crocotile") (v "0.2.0") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cwpdr5w1cdc89scn9amjkcq0hhnzapm5s0fpwwbdhyiqmc62rz5")))

