(define-module (crates-io do cg docgen) #:use-module (crates-io))

(define-public crate-docgen-0.1.0 (c (n "docgen") (v "0.1.0") (h "1vcr8201s5nh72wp0yz0j0bjhfbfhc469kzvrhjs0ps0dc1s8zx4")))

(define-public crate-docgen-0.1.1 (c (n "docgen") (v "0.1.1") (h "1q8wpl461qd4jmc0bm78x8gpg2zapnc5zzw8bydh5nvbz4zpgzcc")))

(define-public crate-docgen-0.1.2 (c (n "docgen") (v "0.1.2") (h "0cghcg9jliskhvqqx63y4fm5648wigrds3a10br6g898cfgg7f16")))

