(define-module (crates-io do ma domafic) #:use-module (crates-io))

(define-public crate-domafic-0.1.0 (c (n "domafic") (v "0.1.0") (d (list (d (n "either_n") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (o #t) (d #t) (k 0)))) (h "1kdhv51jb3pviifh22ysk35scnslfdl5cngz3nw9ck7pafq5wik7") (f (quote (("web_render" "libc" "use_std") ("use_std") ("use_either_n" "either_n") ("default" "use_either_n" "use_std" "web_render"))))))

(define-public crate-domafic-0.1.1 (c (n "domafic") (v "0.1.1") (d (list (d (n "either_n") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (o #t) (d #t) (k 0)))) (h "1rpds5hxaas6yssnx12ggmsalh8gszhh5pkbi1ymljg07q4zxw5q") (f (quote (("web_render" "libc" "use_std") ("use_std") ("use_either_n" "either_n") ("default" "use_either_n" "use_std" "web_render"))))))

(define-public crate-domafic-0.1.2 (c (n "domafic") (v "0.1.2") (d (list (d (n "either_n") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (o #t) (d #t) (k 0)))) (h "0v0cgzgfpchlk1c952587ix9nz5gwbc1lckvsa10l1v1x7p6qfjl") (f (quote (("web_render" "libc" "use_std") ("use_std") ("use_either_n" "either_n") ("default" "use_either_n" "use_std" "web_render"))))))

(define-public crate-domafic-0.2.0 (c (n "domafic") (v "0.2.0") (d (list (d (n "either_n") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "02qgj7mvlw2r81g8966lssgfds7jy8yn21zqp2i5mch7nmysm5xy") (f (quote (("web_render" "libc" "use_std") ("use_std" "marksman_escape") ("use_either_n" "either_n") ("default" "use_either_n" "use_std" "web_render"))))))

