(define-module (crates-io do ma domainfo) #:use-module (crates-io))

(define-public crate-domainfo-0.1.0 (c (n "domainfo") (v "0.1.0") (d (list (d (n "trust-dns-client") (r "^0.20.3") (d #t) (k 0)))) (h "0q5al2xmjbvyklngj33l0w9qr9n5zalfwz754wxj1dn6zbz34dan")))

(define-public crate-domainfo-0.1.1 (c (n "domainfo") (v "0.1.1") (d (list (d (n "trust-dns-client") (r "^0.20.3") (d #t) (k 0)))) (h "0wxb9fs8x56qf3cv5z6gvprjz95pxi2kl7ir08zqvzxs33mn4aaa")))

(define-public crate-domainfo-0.1.2 (c (n "domainfo") (v "0.1.2") (d (list (d (n "trust-dns-client") (r "^0.20.3") (d #t) (k 0)))) (h "1daj9q8qy44i4289x5a4a7jy4lr03zvqba858nqb1pzx3k60y558")))

