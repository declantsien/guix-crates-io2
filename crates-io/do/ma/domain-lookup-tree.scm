(define-module (crates-io do ma domain-lookup-tree) #:use-module (crates-io))

(define-public crate-domain-lookup-tree-0.1.0 (c (n "domain-lookup-tree") (v "0.1.0") (h "1la7fx557ilx350bl4xs0yx13qbhazzvknn9lvn1y1fqssisfpib")))

(define-public crate-domain-lookup-tree-0.1.1 (c (n "domain-lookup-tree") (v "0.1.1") (h "02yd95mqffsr7rdr35sml40ph9xpi36f8zz810s7qx6vfshjpj4p")))

