(define-module (crates-io do ma domain-core) #:use-module (crates-io))

(define-public crate-domain-core-0.3.0 (c (n "domain-core") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0xv9arh7xj9jfji0bxc3bpimbr5d04vm29cv4fhrmnya0kpyiwi8")))

(define-public crate-domain-core-0.4.0 (c (n "domain-core") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0fq4hhhxmjhpy3c0y56i2sc6gziyc1mq2q4zr1ppfz3x8a816ivz")))

