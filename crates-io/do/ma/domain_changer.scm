(define-module (crates-io do ma domain_changer) #:use-module (crates-io))

(define-public crate-domain_changer-0.1.0 (c (n "domain_changer") (v "0.1.0") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0fkqrfi3kwl7jpqqrz42jpkvj3n7bv7rmly7l9ijxqr0adir0jpv")))

(define-public crate-domain_changer-0.1.1 (c (n "domain_changer") (v "0.1.1") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0dl65yg6azj2z1c5rmj334qqds4v17qyqf64vnx3ifkj9nqhl4fx")))

(define-public crate-domain_changer-0.1.2 (c (n "domain_changer") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1g8b6gz9vqwf9wvl4kdha0rwa0nx56jfg1bli7nc64xpgz39zjiw") (f (quote (("json" "serde" "serde_json" "url/serde"))))))

(define-public crate-domain_changer-0.1.3 (c (n "domain_changer") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1mdk5pbjhlb07j3y1zgksfvf1digqjb3qwg7xhmy10grdp61fxlc") (f (quote (("json" "serde" "serde_json" "url/serde"))))))

(define-public crate-domain_changer-0.1.4 (c (n "domain_changer") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1k84ddjxz0h5rdygazgf25xd6irg8vfd1k8dbk2xa1qx9w0jxgpq") (f (quote (("json" "serde" "serde_json" "url/serde"))))))

