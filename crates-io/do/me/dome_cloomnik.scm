(define-module (crates-io do me dome_cloomnik) #:use-module (crates-io))

(define-public crate-dome_cloomnik-0.1.0 (c (n "dome_cloomnik") (v "0.1.0") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "025nqp7a16jvx4qzbg42amya6jfqflza0vnkpxcf8g0grdqlyspy")))

(define-public crate-dome_cloomnik-0.1.1 (c (n "dome_cloomnik") (v "0.1.1") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0psm2n0q9848rd1gjng72209jr9lpbvwyk086way9li5dcmmkvfi")))

(define-public crate-dome_cloomnik-0.1.2 (c (n "dome_cloomnik") (v "0.1.2") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g6d6ffg1m4fs1i2w0f2348a5srv7qayhr3m5ci8i76sn3s6shym")))

(define-public crate-dome_cloomnik-0.1.3 (c (n "dome_cloomnik") (v "0.1.3") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h6j6wvfm6fxw359kzwphink926iqn5r6hnxfrqli4fbrk070byc")))

(define-public crate-dome_cloomnik-0.1.4 (c (n "dome_cloomnik") (v "0.1.4") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g46pfiz6qa92flplpnl18v1jl329v6fi8bhidwx59giz91xcqjx")))

(define-public crate-dome_cloomnik-0.1.5 (c (n "dome_cloomnik") (v "0.1.5") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11l011nj54rg2h1zsgb76ghvdqhpzi2944lyhw965f3i577p8r4q")))

(define-public crate-dome_cloomnik-0.1.6 (c (n "dome_cloomnik") (v "0.1.6") (d (list (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m528nsmrkarb16mlilhylb2z2y39d6nl9xxx9nyw1cwhbgkvq4g")))

(define-public crate-dome_cloomnik-0.1.7 (c (n "dome_cloomnik") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bcqncgbghmq7amalipyfgsipbhk3dmwx5wk5x0kn9m9dzny3fdw")))

(define-public crate-dome_cloomnik-0.1.8 (c (n "dome_cloomnik") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "137f6rvfi5ywkb9fy8xq8qva5agc4pfp0w2zzy5c27dfd65n3rni")))

(define-public crate-dome_cloomnik-0.1.9 (c (n "dome_cloomnik") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yk08cq764hbpbqvd8vlhxkf6xakp911y5nqw1i31fx4aspbrb26")))

(define-public crate-dome_cloomnik-0.1.10 (c (n "dome_cloomnik") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16hnpssd61k5iikl72j3i3barm8m0praywjn093b1kl8jimwiba5")))

(define-public crate-dome_cloomnik-0.1.11 (c (n "dome_cloomnik") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14vbq016v44yxxxnpra5wrgiqj6if3p0prjx515cjyw3l15263wc")))

(define-public crate-dome_cloomnik-0.1.12 (c (n "dome_cloomnik") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atoi") (r "^0.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07wcd67y03gvjs3nljg3gqadxmci78y3lj5mkc7w8ma3nfay1sm4")))

