(define-module (crates-io do cu document-ready) #:use-module (crates-io))

(define-public crate-document-ready-1.0.0 (c (n "document-ready") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.33") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.10") (f (quote ("Document" "EventTarget" "Window"))) (d #t) (k 0)))) (h "05v49bidr2y7vg8fc68mdgpq8wd6k3f7ayz7ycv17akxjzhjagdd")))

(define-public crate-document-ready-1.0.1 (c (n "document-ready") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.33") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.10") (f (quote ("Document" "EventTarget" "Window"))) (d #t) (k 0)))) (h "1faim256mk25zd7bycs38alrss2mzw9kz15h21rk7irykkpbswaj")))

(define-public crate-document-ready-2.0.0 (c (n "document-ready") (v "2.0.0") (d (list (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.1") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.8") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.35") (f (quote ("Document" "EventTarget" "Window"))) (d #t) (k 0)))) (h "0i7nzzccvvshgw903vi7wrc70wkjal2dr0qzpvwlw8g8nbk184hq")))

