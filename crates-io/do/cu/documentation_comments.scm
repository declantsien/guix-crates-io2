(define-module (crates-io do cu documentation_comments) #:use-module (crates-io))

(define-public crate-documentation_comments-0.1.0 (c (n "documentation_comments") (v "0.1.0") (h "0b6s9rcim7hzbahsxpzgqnf7rfh2j7vhr42p8zrih2ygipra4j5p")))

(define-public crate-documentation_comments-0.1.9 (c (n "documentation_comments") (v "0.1.9") (h "1q4b8k6jvk87shkv04ch91xv62h54dnwhb6wr4rm6jr9k326hg2c")))

