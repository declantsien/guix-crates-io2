(define-module (crates-io do cu document_validator_core) #:use-module (crates-io))

(define-public crate-document_validator_core-0.1.0 (c (n "document_validator_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i6zn6gph5jd72gm2vm6rkhgc3dipp05bjpgjq57wm7dy3m3lb5j")))

(define-public crate-document_validator_core-0.1.1 (c (n "document_validator_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13g588w62qlia4dk1b4mzpd5s6fxkzxm9yqm5xn0i0sxn290ljs2")))

(define-public crate-document_validator_core-0.1.2 (c (n "document_validator_core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ix829rhcwymll6ibxjriy2kqh20wai51szs35bvkj6jmxvqa89m")))

