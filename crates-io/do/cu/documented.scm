(define-module (crates-io do cu documented) #:use-module (crates-io))

(define-public crate-documented-0.1.0 (c (n "documented") (v "0.1.0") (d (list (d (n "documented-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1fx8dyl0whgacbgpi5q0dblj7prim9n0ciysy78026qn5brpaqwf")))

(define-public crate-documented-0.1.1 (c (n "documented") (v "0.1.1") (d (list (d (n "documented-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0z92k0sl4a6s0jml54a8v7fnb8nlgz5bw902iyjbzd9y1qjg468w")))

(define-public crate-documented-0.1.2 (c (n "documented") (v "0.1.2") (d (list (d (n "documented-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0jna9c9iw46l12rdkmycvici64wpa3rmx3c0azg37ksxmw6k8qwb")))

(define-public crate-documented-0.2.0 (c (n "documented") (v "0.2.0") (d (list (d (n "documented-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0cdm21y3k1nwq3y7v9gn8hv5h8mna80sxq65k8ly5ng9hhgaz39k")))

(define-public crate-documented-0.3.0 (c (n "documented") (v "0.3.0") (d (list (d (n "documented-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0jb0qs7nc3zprm0lwqs6wsxh4a1xwn2vkl2hb75fhbhvwdmh3wp2")))

(define-public crate-documented-0.4.0 (c (n "documented") (v "0.4.0") (d (list (d (n "documented-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1w5va2j0p7r9dgwl0ss9bzxjqr16vwzd5a74b9llrxdxh6g8m2hn")))

(define-public crate-documented-0.4.1 (c (n "documented") (v "0.4.1") (d (list (d (n "documented-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1q4rjpia3ss706430ryqpc9r7g7fm76qz8qi8cfvg8mr00l1vk91")))

(define-public crate-documented-0.4.2 (c (n "documented") (v "0.4.2") (d (list (d (n "documented-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1n0gr87z3mqx0vvl62a46s4qai0brhykm3izcsnbda5i757v9lbv") (y #t) (r "1.65.0")))

(define-public crate-documented-0.4.3 (c (n "documented") (v "0.4.3") (d (list (d (n "documented-derive") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0anhc0q0ambz36ksfam5rngi3y0n0s5rvk1szv56wbw47d12svqg") (r "1.65.0")))

