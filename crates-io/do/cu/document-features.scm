(define-module (crates-io do cu document-features) #:use-module (crates-io))

(define-public crate-document-features-0.1.0 (c (n "document-features") (v "0.1.0") (h "02wjaz8zfw2vvy87fqp3z68nlr1wvrj036rq33r76a77ddw65cm7") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.0 (c (n "document-features") (v "0.2.0") (h "0875z5xqjqla6d22zhxaincgvqhw0dscblnrjrh712x7xsc5a55h") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.1 (c (n "document-features") (v "0.2.1") (h "121wr2bd8a4s5i5yrxjz8c5amw2l69xmqqma86x6y4xmcgyhj75h") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.2 (c (n "document-features") (v "0.2.2") (h "0jvlwlwig8nmr2xczf3jn06ryn3qvid8slbhws3kp63s5hyb5fyy") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.3 (c (n "document-features") (v "0.2.3") (h "0ybdc14hlp9550mn8746fr4mf8klrwh71z6dhn5j5sq2ajabx6yr") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.4 (c (n "document-features") (v "0.2.4") (h "0q2cxmwwin7fvwk9qazwcj57a1h6cclbg9pcisy3r5yz6az77z16") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.5 (c (n "document-features") (v "0.2.5") (h "05b47v5j18cvabmsnjyczfgkzigj2zn708vzsibhclbwk7knadg4") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.6 (c (n "document-features") (v "0.2.6") (d (list (d (n "litrs") (r "^0.2.3") (k 0)))) (h "18fpkyf0zqwy9ahwd46szr7j8kjinq24mm7y6pfns7sgvqd7w9n3") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.7 (c (n "document-features") (v "0.2.7") (d (list (d (n "litrs") (r "^0.2.3") (k 0)))) (h "0mv1xg386as8zndw6kdgs4bwxwwlg42srdhkmgf00zz1zirwb4z4") (f (quote (("self-test") ("default"))))))

(define-public crate-document-features-0.2.8 (c (n "document-features") (v "0.2.8") (d (list (d (n "litrs") (r "^0.4.1") (k 0)))) (h "15cvgxqngxslgllz15m8aban6wqfgsi6nlhr0g25yfsnd6nq4lpg") (f (quote (("self-test") ("default"))))))

