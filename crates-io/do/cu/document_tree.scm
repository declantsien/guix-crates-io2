(define-module (crates-io do cu document_tree) #:use-module (crates-io))

(define-public crate-document_tree-0.3.0 (c (n "document_tree") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "08hcqhv54qq3ibdcbh4s60g3chqzq0hcfb9aq9sh4cmkqyz9p7n3")))

(define-public crate-document_tree-0.4.0 (c (n "document_tree") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1q45gmj028mbmbji4arrbnr87jr20914sknwdcclz3lzmgyk1181")))

