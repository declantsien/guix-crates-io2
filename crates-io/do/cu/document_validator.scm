(define-module (crates-io do cu document_validator) #:use-module (crates-io))

(define-public crate-document_validator-0.1.0 (c (n "document_validator") (v "0.1.0") (d (list (d (n "document_validator_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ivkvnfg9m01mgvg9mxzmcpx335v6qq8hn33j2cjljf3p89sbhh4") (f (quote (("derive" "document_validator_macros") ("default" "derive"))))))

(define-public crate-document_validator-0.1.1 (c (n "document_validator") (v "0.1.1") (d (list (d (n "document_validator_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0qs57l6cnxqsyl506vafrx0zymfs8k5rpdv3syhs8lnbf1bi0p35") (f (quote (("derive" "document_validator_macros") ("default" "derive"))))))

(define-public crate-document_validator-0.1.2 (c (n "document_validator") (v "0.1.2") (d (list (d (n "document_validator_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "13i4myi3czriksx5dwvfqkwldb1092dx22fq05yfndqslcm7vcy6") (f (quote (("derive" "document_validator_macros") ("default" "derive"))))))

(define-public crate-document_validator-0.1.3 (c (n "document_validator") (v "0.1.3") (d (list (d (n "document_validator_macros") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0qxifwxqfj8n9bsgf1840p77x8mf6w8vnh42cpfhylzd6hvnkkxs") (f (quote (("derive" "document_validator_macros") ("default" "derive"))))))

(define-public crate-document_validator-0.2.0 (c (n "document_validator") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1y52k29asnbhddclig7pwar36ccv1npmrsjgginxfihz5wwkw3x6")))

