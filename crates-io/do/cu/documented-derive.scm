(define-module (crates-io do cu documented-derive) #:use-module (crates-io))

(define-public crate-documented-derive-0.1.0 (c (n "documented-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0nqm5s0jq70nyszr2kli9xib5adk8k9d0clbykf0qpap60bljnwp")))

(define-public crate-documented-derive-0.1.1 (c (n "documented-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1ffyhqk9jl0xnxx2sbwym6ydm1ymwpi2yqrf143y1qyrw32kw4n7")))

(define-public crate-documented-derive-0.1.2 (c (n "documented-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1yk524kyc1s2iyzz9fhyaz4rnskqwg7d35lfjb3jy7awqg2baypq")))

(define-public crate-documented-derive-0.2.0 (c (n "documented-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1qclsy9i64i3pzqqmlf7xmldn4glfac456sq80krdnwifykmsr51")))

(define-public crate-documented-derive-0.3.0 (c (n "documented-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0w0qz6xf1mkip3daxwz4jz66796gl780imj6pr81vwpkfzy5l2c3")))

(define-public crate-documented-derive-0.4.0 (c (n "documented-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1grz6m1zg2yz7pnaa3wkfba999rnilh2zm9hbscfxcby6n3mm81j")))

(define-public crate-documented-derive-0.4.1 (c (n "documented-derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1nvw1jb2nl2vnvaij4nbzfa6b2l7xibapn2xrfzmybd7y0334n28")))

(define-public crate-documented-derive-0.4.2 (c (n "documented-derive") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "01fp8kf9l0vyjwbhjxqq6dbn9zlcyarhnaq7pybw2ih0d7rcsrac") (r "1.65.0")))

(define-public crate-documented-derive-0.4.3 (c (n "documented-derive") (v "0.4.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0c0zbvwhim5fj0jq6lps14lnddvq5ppbcdi46drn4dsc58n08fp0") (r "1.65.0")))

