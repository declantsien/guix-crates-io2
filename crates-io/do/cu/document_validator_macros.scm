(define-module (crates-io do cu document_validator_macros) #:use-module (crates-io))

(define-public crate-document_validator_macros-0.1.0 (c (n "document_validator_macros") (v "0.1.0") (d (list (d (n "document_validator_core") (r "^0.1.0") (d #t) (k 0)))) (h "0cwc9smh6l3z7r2cy5dspfgh991fcak7xx46jimspan2a0ij5y3k")))

(define-public crate-document_validator_macros-0.1.1 (c (n "document_validator_macros") (v "0.1.1") (d (list (d (n "document_validator_core") (r "^0.1.1") (d #t) (k 0)))) (h "0f1asvkn8lxd56js0bv8m64wj1nbwqsgil8z9a4s3x90ldr5n6ac")))

(define-public crate-document_validator_macros-0.1.2 (c (n "document_validator_macros") (v "0.1.2") (d (list (d (n "document_validator_core") (r "^0.1.2") (d #t) (k 0)))) (h "0s3w2l1vdjgshfxvzy8ly3956ggzn25rc99xkz4gc1fp55s6pbkr")))

