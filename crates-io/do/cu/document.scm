(define-module (crates-io do cu document) #:use-module (crates-io))

(define-public crate-document-0.1.0 (c (n "document") (v "0.1.0") (h "007l5sj0ms1vazkmkg0zkc5glrm1gbfql2j2s4yvz27sh4dyzx0c")))

(define-public crate-document-0.2.0 (c (n "document") (v "0.2.0") (d (list (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "0bw3n2mahrjd0dhk12m9mnimsjq2waiamw7hxnvflxp4dyrvbwnz")))

(define-public crate-document-0.3.0 (c (n "document") (v "0.3.0") (d (list (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1q2a9gwsn2qb38nq937kznb2pnpmhi9520mbz2s7rlx4f2c322y9")))

(define-public crate-document-0.4.0 (c (n "document") (v "0.4.0") (d (list (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1jv8n548vlrb91krq2ab0wy9p8fcs630694rzahyzh3y4r1rrn7q")))

(define-public crate-document-0.5.0 (c (n "document") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "0lbdkrs5sk28v6i9chcxxzryf9pwi6dxbryqi13050pcviqjyrxi")))

(define-public crate-document-0.5.1 (c (n "document") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1jl3h2cskxk5bf9c2g8r3cxsnn8q2vhzv5knqw75bxn9hvzkvczy")))

