(define-module (crates-io do wn download_git) #:use-module (crates-io))

(define-public crate-download_git-0.0.1 (c (n "download_git") (v "0.0.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0firvcxl4njpwcqjv6p8g719sr9zkffsnq4mrhnsf6szk8ypssz9") (r "1.60.0")))

(define-public crate-download_git-0.0.2 (c (n "download_git") (v "0.0.2") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1pzsc618qicrsvg805w6v5n2fycajxs93vsv6jfr9kr0xngn1h1g") (r "1.60.0")))

