(define-module (crates-io do wn downwards) #:use-module (crates-io))

(define-public crate-downwards-0.0.1 (c (n "downwards") (v "0.0.1") (h "0b317d0j4qi3h7r7vxsz1i2fcicx7l750qbd57khzp2ayb9mljdp") (y #t)))

(define-public crate-downwards-0.0.2 (c (n "downwards") (v "0.0.2") (d (list (d (n "downwardsdep") (r "^0.0.1") (d #t) (k 0) (p "downwards")))) (h "08n9x5yc0629gv9n0p4mzhdk64n6kk4ksjw028lxgv4yz7q4pb58") (y #t)))

(define-public crate-downwards-0.0.3 (c (n "downwards") (v "0.0.3") (d (list (d (n "downwardsdep") (r "^0.0.2") (d #t) (k 0) (p "downwards")))) (h "010w5mkkk3qc67z4cnykc85npsr02899yn7qbrl2b01az3fj905d") (y #t)))

