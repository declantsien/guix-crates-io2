(define-module (crates-io do wn downloads_sorter) #:use-module (crates-io))

(define-public crate-downloads_sorter-0.1.0 (c (n "downloads_sorter") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)))) (h "12kkabma3jy0zq7kj81k04fdq63sp7514i52ajlvxq2c8zg2d025")))

(define-public crate-downloads_sorter-0.1.1 (c (n "downloads_sorter") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)))) (h "1zp5m5n2fw5ll8dq2vw3r20gazadnwyk6nly7qikjxb8mdn5bka7")))

(define-public crate-downloads_sorter-0.1.2 (c (n "downloads_sorter") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)))) (h "1cz382jh15g98p5mhg53z3cij3zz0mif9mihgrw2kna3f1n6aw0a")))

(define-public crate-downloads_sorter-0.1.3 (c (n "downloads_sorter") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)))) (h "077cjfpsim4jynxjdh6gfmzph4b5pdh5lj93w4qll971441fisvl")))

(define-public crate-downloads_sorter-0.1.5 (c (n "downloads_sorter") (v "0.1.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "infer") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "094rh4haxyy0kzwiwwbri5bcsc35hfk890njwcdbdfj3jy2l9zs4")))

