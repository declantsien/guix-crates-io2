(define-module (crates-io do wn download_npm) #:use-module (crates-io))

(define-public crate-download_npm-0.0.1 (c (n "download_npm") (v "0.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "mf-utils") (r "^0.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z9dwj91xg2p5fy3xc3crhg6r0prxzhl3f9r48418n3gc0hb2rvs")))

