(define-module (crates-io do wn downsample) #:use-module (crates-io))

(define-public crate-downsample-0.0.1 (c (n "downsample") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1q7bmg6hr7zb66vi9kwy76amyc5x8869cfrl9wfag4w4cn5jxw7c")))

(define-public crate-downsample-0.0.2 (c (n "downsample") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17k97jaawfkfqdgaficamd2ly5sg6av9s3wa4gjhv7dvqbycnbd2") (f (quote (("derive_aggregatable") ("default" "derive_aggregatable"))))))

(define-public crate-downsample-0.0.3 (c (n "downsample") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k9f9i9b1l5jwmgb75b1v46zb0xagrm3517anngb5c2jkmk2dqnf") (f (quote (("derive_aggregatable") ("default" "derive_aggregatable"))))))

