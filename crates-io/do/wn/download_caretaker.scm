(define-module (crates-io do wn download_caretaker) #:use-module (crates-io))

(define-public crate-download_caretaker-0.1.0 (c (n "download_caretaker") (v "0.1.0") (d (list (d (n "bat") (r "^0.15.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "15smg6kigbzv8cpgj9p39pb1q2il6c1rfmlmy36fh4csvvxlig8s")))

(define-public crate-download_caretaker-0.1.1 (c (n "download_caretaker") (v "0.1.1") (d (list (d (n "bat") (r "^0.15.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "1yybvhp5cc93cfn92z63hsvhsirf5hrfj9ywilyn05jr0xwjp389")))

(define-public crate-download_caretaker-0.1.2 (c (n "download_caretaker") (v "0.1.2") (d (list (d (n "bat") (r "^0.15.1") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "1jvkjr9nz8sq18q2l62nfklssr4bl73vfgsca596ayv011qr3yj0")))

(define-public crate-download_caretaker-0.1.3 (c (n "download_caretaker") (v "0.1.3") (d (list (d (n "bat") (r "^0.15.1") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "0b9syxd05l5g1l75j06xdzcyzh31piif968jjnbd8737y33zi041")))

