(define-module (crates-io do wn download_rs_reborned) #:use-module (crates-io))

(define-public crate-download_rs_reborned-0.2.1 (c (n "download_rs_reborned") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0gyhb9zil1b7g54z32053sqf1lz52hk8wsmhs09v0vypkh2fj8zs") (f (quote (("sync_download") ("full" "sync_download" "async_download") ("default" "async_download") ("async_download" "tokio" "indicatif"))))))

(define-public crate-download_rs_reborned-0.2.2 (c (n "download_rs_reborned") (v "0.2.2") (d (list (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "116824x1r5s64svfvkz55fisslg8dm6xmbr702wvqmhv4jpqrjg6") (f (quote (("sync_download") ("full" "sync_download" "async_download") ("default" "async_download") ("async_download" "tokio" "indicatif"))))))

