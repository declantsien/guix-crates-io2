(define-module (crates-io do wn downify) #:use-module (crates-io))

(define-public crate-downify-0.1.0 (c (n "downify") (v "0.1.0") (h "1x091hs3f5bd28y1xw146xni42vrxr0vx9rzfgm8s28w9853ns7l")))

(define-public crate-downify-0.2.0 (c (n "downify") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "07yazifkhnqwcc2r70ja91b8andvjjbpxr13ln9f7wksdgdfwixy")))

(define-public crate-downify-0.2.1 (c (n "downify") (v "0.2.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0zc6z1csp84cs22dq36qvf4aqq390fb7z8a96x8kbs3jhz1rhqa6")))

