(define-module (crates-io do wn download_rs) #:use-module (crates-io))

(define-public crate-download_rs-0.1.0 (c (n "download_rs") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0d3vlfka4v4khdmi36xzbhbp9riq8pq0vgqacbp7cdfpw4sp3r55") (f (quote (("sync_download") ("full" "sync_download" "async_download") ("default" "async_download") ("async_download" "tokio" "indicatif"))))))

(define-public crate-download_rs-0.1.2 (c (n "download_rs") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "19z8yj7s278938917gaq4w1pkcpiji13nnpy50gxzq0k64k2lfm8") (f (quote (("sync_download") ("full" "sync_download" "async_download") ("default" "async_download") ("async_download" "tokio" "indicatif"))))))

(define-public crate-download_rs-0.2.0 (c (n "download_rs") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1b9bdd1qyyg1z1mylqi29fshdgjzfhgrk91fp8571kj9ry09czf4") (f (quote (("sync_download") ("full" "sync_download" "async_download") ("default" "async_download") ("async_download" "tokio" "indicatif"))))))

