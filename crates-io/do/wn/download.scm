(define-module (crates-io do wn download) #:use-module (crates-io))

(define-public crate-download-0.1.0 (c (n "download") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1i2y51rg9sz9ica03225jmh12n2j4y1pimjighbx0yasx13xrwla") (y #t)))

