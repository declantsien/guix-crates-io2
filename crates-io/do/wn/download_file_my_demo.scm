(define-module (crates-io do wn download_file_my_demo) #:use-module (crates-io))

(define-public crate-download_file_my_demo-0.1.0 (c (n "download_file_my_demo") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xya3firjd0sx2aj64plp1qn3nbkbqq9rh3w2q3im2bg5vkk1zfl")))

