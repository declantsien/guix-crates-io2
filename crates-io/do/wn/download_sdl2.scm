(define-module (crates-io do wn download_sdl2) #:use-module (crates-io))

(define-public crate-download_sdl2-0.0.1 (c (n "download_sdl2") (v "0.0.1") (d (list (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "tar") (r "^0.4.13") (d #t) (k 0)) (d (n "zip") (r "^0.2.3") (d #t) (k 0)))) (h "0jqknvsskjdy3yxfa8fwlnm48c2s688hc75nanmib0kssq4da2n6")))

(define-public crate-download_sdl2-0.0.2 (c (n "download_sdl2") (v "0.0.2") (d (list (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "tar") (r "^0.4.13") (d #t) (k 0)) (d (n "zip") (r "^0.2.3") (d #t) (k 0)))) (h "0dgqjq1h9br0la3py96dngxhxhi616aafp3la7i2gr7kjjdrqzbd")))

(define-public crate-download_sdl2-0.0.3 (c (n "download_sdl2") (v "0.0.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.3") (d #t) (k 0)))) (h "1csk5z946d85q1hg65wx9qnd0lpx7lhvw5asb23zzb7ijc2zfl5f")))

