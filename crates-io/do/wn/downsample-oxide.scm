(define-module (crates-io do wn downsample-oxide) #:use-module (crates-io))

(define-public crate-downsample-oxide-0.1.0 (c (n "downsample-oxide") (v "0.1.0") (d (list (d (n "chrono_crate") (r "^0.4.24") (o #t) (d #t) (k 0) (p "chrono")) (d (n "rust_decimal") (r "^1.29.0") (d #t) (k 0)) (d (n "time_crate") (r "^0.3.20") (o #t) (d #t) (k 0) (p "time")) (d (n "chrono_crate") (r "^0.4.24") (d #t) (k 2) (p "chrono")))) (h "195q6ma93809yqsn4xc8rfgb62bv7i9v08b34znjci2k5kmsg8ms") (f (quote (("time" "time_crate") ("chrono" "chrono_crate"))))))

(define-public crate-downsample-oxide-0.1.1 (c (n "downsample-oxide") (v "0.1.1") (d (list (d (n "chrono_crate") (r "^0.4.24") (o #t) (d #t) (k 0) (p "chrono")) (d (n "rust_decimal") (r "^1.29.0") (d #t) (k 0)) (d (n "time_crate") (r "^0.3.20") (o #t) (d #t) (k 0) (p "time")) (d (n "chrono_crate") (r "^0.4.24") (d #t) (k 2) (p "chrono")))) (h "0cxw7yp572vb4rm6arkjgqkd5l5yp1ak5cpvrizl24rcbp26kwpj") (f (quote (("time" "time_crate") ("chrono" "chrono_crate"))))))

(define-public crate-downsample-oxide-0.1.2 (c (n "downsample-oxide") (v "0.1.2") (d (list (d (n "chrono_crate") (r "^0.4.24") (o #t) (d #t) (k 0) (p "chrono")) (d (n "rust_decimal") (r "^1.29.0") (d #t) (k 0)) (d (n "time_crate") (r "^0.3.20") (o #t) (d #t) (k 0) (p "time")) (d (n "chrono_crate") (r "^0.4.24") (d #t) (k 2) (p "chrono")))) (h "12qzbhjf7m82wnmmm6050fcg9fhhwn7wlxi6p59zbfmvah2l7z29") (f (quote (("time" "time_crate") ("chrono" "chrono_crate"))))))

