(define-module (crates-io do wn downup) #:use-module (crates-io))

(define-public crate-downup-0.1.0 (c (n "downup") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "elapsed") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9w9k3352wnr2r1phdmz13s831i58187pgbvaxx0a1gqk2dn1q9") (y #t)))

