(define-module (crates-io do wn downcast-rs) #:use-module (crates-io))

(define-public crate-downcast-rs-0.1.0 (c (n "downcast-rs") (v "0.1.0") (h "1xpkincrhfdrs4cs4h8jxwvrmy8p7wsfjm8vnpl7zi2y69cxdjjp")))

(define-public crate-downcast-rs-0.1.1 (c (n "downcast-rs") (v "0.1.1") (h "1dm88jfyy907c18giqyzky84zhhhz8262v7dl414rsqnpffpzb15")))

(define-public crate-downcast-rs-0.1.2 (c (n "downcast-rs") (v "0.1.2") (h "18j6x9z6ypfhzvdi61rxykkc6a7w4rxiy5djd1jaffingiyfjmgw")))

(define-public crate-downcast-rs-1.0.0 (c (n "downcast-rs") (v "1.0.0") (h "11ldwqjdyy1zbm3cavs5xf6ylr1wqkadbm1wlqsyg7vcdv544w4i")))

(define-public crate-downcast-rs-1.0.1 (c (n "downcast-rs") (v "1.0.1") (h "07z2n7gsnxlslp3n2n1zbqvp5yrmfk2glb99alkjybw9c9v4ha2v")))

(define-public crate-downcast-rs-1.0.2 (c (n "downcast-rs") (v "1.0.2") (h "09z81kbqdblfwq8f77cdsxww913zmpbbhkbgav4x7828s2p397pv")))

(define-public crate-downcast-rs-1.0.3 (c (n "downcast-rs") (v "1.0.3") (h "0v7ya5qakblnwz7v87k3akhk2kslazd24q4jm8c9s60c8zj8rpqq")))

(define-public crate-downcast-rs-1.0.4 (c (n "downcast-rs") (v "1.0.4") (h "03q2pqjk1wik3agbwgsypah7qziqbpwp41bmpw62cx9gbkyjvfgj")))

(define-public crate-downcast-rs-1.1.0 (c (n "downcast-rs") (v "1.1.0") (h "16n4cqw71v0kasmpgl7n6mps1a1id3xxs9sbm5ypni6l5z619r2z")))

(define-public crate-downcast-rs-1.1.1 (c (n "downcast-rs") (v "1.1.1") (h "1xhs2qj02k9m4mm5fgh19y88850y9jsnwwlblf2ffc91gjs6xfjj")))

(define-public crate-downcast-rs-1.2.0 (c (n "downcast-rs") (v "1.2.0") (h "0l36kgxqd5djhqwf5abxjmgasdw8n0qsjvw3jdvhi91nj393ba4y") (f (quote (("std") ("default" "std"))))))

(define-public crate-downcast-rs-1.2.1 (c (n "downcast-rs") (v "1.2.1") (h "1lmrq383d1yszp7mg5i7i56b17x2lnn3kb91jwsq0zykvg2jbcvm") (f (quote (("std") ("default" "std"))))))

