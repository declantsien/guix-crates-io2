(define-module (crates-io do wn downcast) #:use-module (crates-io))

(define-public crate-downcast-0.0.1 (c (n "downcast") (v "0.0.1") (h "0rg56d09ldbzam5ayqb32p8hf5y264ik4s2cbwjimwnzv3zyf11x")))

(define-public crate-downcast-0.1.0 (c (n "downcast") (v "0.1.0") (h "0q4pzv9ljgq62gmr0nljvsnvbrkialvkln7gxxj6xfmmw6s3z21c")))

(define-public crate-downcast-0.2.0 (c (n "downcast") (v "0.2.0") (h "0jrcnfx7c4g2hzpfjnqczypykca2zmhx5piyw524mmn5b1hm3qkx")))

(define-public crate-downcast-0.2.1 (c (n "downcast") (v "0.2.1") (h "0v5c48d5ynwrc0jgypdvmhn0znnvriiqix6j289s7ws1h8jq6wi9")))

(define-public crate-downcast-0.2.2 (c (n "downcast") (v "0.2.2") (h "1kcmlbw2hliwwmwxry3f0gvi9lq1c3nq4qmr3i5y1whvx377g7d7")))

(define-public crate-downcast-0.2.3 (c (n "downcast") (v "0.2.3") (h "1m47yv1xhh6dq6lrkn5i4xdwscbs9h2v0xihmpi4p52fj7m2xpdl")))

(define-public crate-downcast-0.3.0 (c (n "downcast") (v "0.3.0") (h "16ybbfibggc6hbql0izsksw5j5q4mbf01mcfq0xpgsjf1dhmha4c")))

(define-public crate-downcast-0.4.0 (c (n "downcast") (v "0.4.0") (h "1gbi07zb17qx3r17pl9pms7y05q60wyav3z2nivhjsiyizkb1wfm")))

(define-public crate-downcast-0.4.1 (c (n "downcast") (v "0.4.1") (h "0yknmv5f55g6d62lxkdxfnh39sh7la4blyk52n55f1yhpw2l0nc5")))

(define-public crate-downcast-0.4.2 (c (n "downcast") (v "0.4.2") (h "0lnvmy8qnq581gr6qm1l31r8avr5yzxxb2fvlcl2ywmiy6ybn7hb")))

(define-public crate-downcast-0.5.0 (c (n "downcast") (v "0.5.0") (h "0r133xy5i3jg9jla6jmdsbjsd1jj5nribigkd4khj1cgpbi0gzvz")))

(define-public crate-downcast-0.6.0 (c (n "downcast") (v "0.6.0") (h "0ijfidg3xzxhxxkgqgcf1h2kvnvzxnj6i8dgnw1q6r2095q7aahl")))

(define-public crate-downcast-0.7.0 (c (n "downcast") (v "0.7.0") (h "061wv9lqgb9kf66q0slv0y69kmxpliis2bh4xh8vxm56p445iacl") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.7.1 (c (n "downcast") (v "0.7.1") (h "0sd7858f58yn919vgws78zfc58sg855hyn3ngdnsc9hi83261fkd") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.8.0 (c (n "downcast") (v "0.8.0") (h "1fsr1v4ygpw15pp6air1pmva505i3hdvg03pld3i9msqhgw5c9r4") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.9.0 (c (n "downcast") (v "0.9.0") (h "1qklch38vqjyfyv8hsc8rkk9g55vhhkq5nz04i0lpmsxaidd3zrz") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.9.1 (c (n "downcast") (v "0.9.1") (h "1s06wwdqip4lxwqqqzqbrjlvpshkgk8fpsli56jh51v8a718fvfg") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.9.2 (c (n "downcast") (v "0.9.2") (h "1lzfhb47xbgf8ismk7c4fpw1dsrq09l9x0wfdhb23vxn309y6vvc") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.10.0 (c (n "downcast") (v "0.10.0") (h "07bh0l95gwrzak6rj29v8kkm577d8vivxsxhqgscf64b4bq59d2b") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-downcast-0.11.0 (c (n "downcast") (v "0.11.0") (h "1wa78ahlc57wmqyq2ncr80l7plrkgz57xsg7kfzgpcnqac8gld8l") (f (quote (("std") ("nightly") ("default" "std"))))))

