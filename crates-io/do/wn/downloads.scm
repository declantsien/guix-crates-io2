(define-module (crates-io do wn downloads) #:use-module (crates-io))

(define-public crate-downloads-0.0.1 (c (n "downloads") (v "0.0.1") (d (list (d (n "curl") (r "^0.4.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1s1jq7bh230hmcaf7ijlr8ipxg6agv1ywdksfk4zbslfllpkpc7d")))

(define-public crate-downloads-0.0.2 (c (n "downloads") (v "0.0.2") (d (list (d (n "curl") (r "^0.4.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1a9pp2l7bj6pd5zs3vcg7hnbdragil3a0081mfnq93sbz84k6r35")))

