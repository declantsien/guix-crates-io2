(define-module (crates-io do c_ doc_item) #:use-module (crates-io))

(define-public crate-doc_item-0.1.0 (c (n "doc_item") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "027yx0kvq237bh8mkan7rqakbjnisfq0kmd3g2xv7ayj1kl7araw")))

(define-public crate-doc_item-0.1.1 (c (n "doc_item") (v "0.1.1") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "063bmc5lp7m4faf21nj3hzv05zqk4pmzq7jk2s7w68b00g5kp1zi")))

(define-public crate-doc_item-0.2.0 (c (n "doc_item") (v "0.2.0") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "05p5v64km1x90xg4323znm82pigh8f9mp5h68gam3y4inv3w14ar") (y #t)))

(define-public crate-doc_item-0.2.1 (c (n "doc_item") (v "0.2.1") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "1223v144gg7gzpyvj9m0b1xgm02plq2q8fg2pfzd0hgn7ba8bvg9") (y #t)))

(define-public crate-doc_item-0.2.2 (c (n "doc_item") (v "0.2.2") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "0n011rh0w42x9znsphvxq2qvq6ncjgi5yf28m93iyaqycf1j85np")))

(define-public crate-doc_item-0.2.3 (c (n "doc_item") (v "0.2.3") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "1x4647yfy624wi1ran5j39rfbc4909vv86p3s2dwb059jhls5j34")))

(define-public crate-doc_item-0.2.4 (c (n "doc_item") (v "0.2.4") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 2)) (d (n "syn") (r "^1.0.68") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "thirtyfour_sync") (r "^0.22.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "17d44c28lc90sw3arb0g9ivn46d1330kldg93ydlcljr8c5h1dfp") (f (quote (("frontend_test"))))))

(define-public crate-doc_item-0.2.5 (c (n "doc_item") (v "0.2.5") (d (list (d (n "darling") (r "^0.12.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "substring") (r "^1.4.5") (d #t) (k 2)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "thirtyfour_sync") (r "^0.23.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "0783h7dzd1hxwhqd48zfx0qjdm2kjcqzn1c57g766v2xxawps0x7") (f (quote (("frontend_test"))))))

(define-public crate-doc_item-0.3.0 (c (n "doc_item") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.89") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "thirtyfour_sync") (r "^0.27.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.56") (d #t) (k 2)))) (h "0xslrqb9mwmzprk96ngnyjiv3qf7zpy65v543vx03jydj1j2yfsq")))

