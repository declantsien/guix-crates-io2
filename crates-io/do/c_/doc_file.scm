(define-module (crates-io do c_ doc_file) #:use-module (crates-io))

(define-public crate-doc_file-0.0.1 (c (n "doc_file") (v "0.0.1") (h "1418dc01mp3p6xn50ljssizm3x0nk5zbskn6hxl2an1sd90yg3fk")))

(define-public crate-doc_file-0.0.2 (c (n "doc_file") (v "0.0.2") (h "10l73s4if1n729rmm7ydz7hxdg1ac8d02fpbqahyr6zm0gpb40im")))

(define-public crate-doc_file-0.1.0 (c (n "doc_file") (v "0.1.0") (h "11y7wc89d49nflz05m8wijb1786026pwb2505ihnmzrl5icsiwcb")))

(define-public crate-doc_file-0.2.0 (c (n "doc_file") (v "0.2.0") (h "01nycqs3wivgpc2z421ff344n1rpnnczb4jcnmpxk0cbg0prhd5y")))

