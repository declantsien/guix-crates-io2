(define-module (crates-io do c_ doc_link) #:use-module (crates-io))

(define-public crate-doc_link-0.1.0 (c (n "doc_link") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3bsirhlhjlhshhn3718in3v1fsvqys0s7b6qr5ibq7m025rx81")))

