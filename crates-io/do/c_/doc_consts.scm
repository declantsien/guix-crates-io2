(define-module (crates-io do c_ doc_consts) #:use-module (crates-io))

(define-public crate-doc_consts-0.1.0 (c (n "doc_consts") (v "0.1.0") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mvwbrq9bqj3xi72z2q0qdxrj8bcz4j5wk6gff13apaqiq1v9l1x")))

(define-public crate-doc_consts-0.2.0 (c (n "doc_consts") (v "0.2.0") (d (list (d (n "doc_consts_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0ik4gm9pw0jns2rx91pz2378znyw3528xwqrmlz528x464x9spyc")))

