(define-module (crates-io do gg doggo) #:use-module (crates-io))

(define-public crate-doggo-0.1.0 (c (n "doggo") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0n9hfpasxrvilb1mpm2qjkllwrv1642wjnrsw9h5fgsk1xlsaqcs")))

(define-public crate-doggo-0.1.1 (c (n "doggo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0ivq5yrz4mbncayb6iqqnyhns8i1qqykc65cyzin5zr49f99y5v3")))

(define-public crate-doggo-0.1.2 (c (n "doggo") (v "0.1.2") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0d1q6bmqnwj3lzrch3w4i4ih7i713rf2wyz7z3r5lkrnvhs5rwfy")))

(define-public crate-doggo-0.1.3 (c (n "doggo") (v "0.1.3") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "15rgskh6i0rakff0w4qyl2n0fp7mcgyfc8m7b0w6ahzp2zvjaq5z")))

(define-public crate-doggo-0.1.4 (c (n "doggo") (v "0.1.4") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1qgq1mbcnkxa35l3h8m09qhxcpc0689dcndxdbg82hymbsj1n5y1")))

(define-public crate-doggo-0.1.5 (c (n "doggo") (v "0.1.5") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0i62vs8gzb9zl8xcz0c90bxsm4vhqi0yg8z1qfarp386isgndcli")))

(define-public crate-doggo-0.1.6 (c (n "doggo") (v "0.1.6") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1f89450rwwpsjcja7vzlx5r5ak5yhf6kaa1my8naizznpdbrlqyl")))

(define-public crate-doggo-0.1.7 (c (n "doggo") (v "0.1.7") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "07lrpv7vg89jn4dzvy0shccsk93riyj1mapfh6svhxzw9v95ra49")))

(define-public crate-doggo-0.1.8 (c (n "doggo") (v "0.1.8") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1iap5b9i2cvnv64gb4a3v62qxjzpm6fakh3aa0ic7dy5aqzjmkj6")))

(define-public crate-doggo-0.1.10 (c (n "doggo") (v "0.1.10") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "05vys4h9n2avmfzxz4sjb0mcd16scsz1wp32wr0cqrp3lh1ms4zl")))

(define-public crate-doggo-0.1.11 (c (n "doggo") (v "0.1.11") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.20") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13r7dsywwr1wd7p6bvg4i1b9bxcmz4yjs6hcb4y2y2yawy1fxrri")))

(define-public crate-doggo-0.1.13 (c (n "doggo") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.20") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1rj74n2h6b637viqrp83jvpw8jxhlylx2zgm6m01f41fm1rn0rl0")))

(define-public crate-doggo-0.1.12-pre (c (n "doggo") (v "0.1.12-pre") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0jg3rrx4628chkfghi4sy2hw3w398bjm0bw3cavx9r2cwrnxa5hr")))

(define-public crate-doggo-0.1.14 (c (n "doggo") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "13xpbas7gv4fbn6j97wsvgswhrv0lw5z77dzvdw5g5icm9b09cq5")))

(define-public crate-doggo-0.1.16 (c (n "doggo") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0970h6kakh750dg9r66fb3lmkcchki95xjrn2zy7nlw86npk7d0q")))

