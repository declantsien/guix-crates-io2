(define-module (crates-io do gg dogg) #:use-module (crates-io))

(define-public crate-dogg-0.1.0 (c (n "dogg") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "0cgd95i9hgv4bj7217mp2w6qhlvzfhndqxn7jjaigpzfj444009a")))

(define-public crate-dogg-0.1.1 (c (n "dogg") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "0v30zzcwsxpihxyh4rkly8r6fnpgy7b3glf6spda3l9s1yc2l80s")))

(define-public crate-dogg-0.1.2 (c (n "dogg") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "1ifa5ymr5lw5vf0yz80vxkxvb6jdrqkdinlvkzpnsk2fxdyp57zh")))

(define-public crate-dogg-0.1.3 (c (n "dogg") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "13dw5qs7rd62945jigvsz8yif9z5y0g04q88aww7q44ybqz1d5gb")))

