(define-module (crates-io do gg dogged) #:use-module (crates-io))

(define-public crate-dogged-0.1.0 (c (n "dogged") (v "0.1.0") (h "1zx6nc1jwd9fsa9ld41cicr7j8r9c7f8l7aiw1w18z76xkby8l26")))

(define-public crate-dogged-0.2.0 (c (n "dogged") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0yk5l6qqidl5y935x15gi9kkd6niim1wb64r1l7kdzl9jw8dyf16")))

