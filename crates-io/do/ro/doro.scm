(define-module (crates-io do ro doro) #:use-module (crates-io))

(define-public crate-doro-0.1.0 (c (n "doro") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "16kkvw08h3wg2ppmdzdzhn7h7yra3a15li2373065jpzglriwl5x")))

(define-public crate-doro-0.2.0 (c (n "doro") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "12k2h4v7ljigclv44xwd9hbz7my5jlrgwlxl9srgmxz8n4r9f9iv")))

(define-public crate-doro-0.2.1 (c (n "doro") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1mgg0pcxbk7rfflp3jhzh4ls0496hpn9lhqvzihv02j7y37qj46d")))

