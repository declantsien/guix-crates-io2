(define-module (crates-io do ro dorothy-ssr) #:use-module (crates-io))

(define-public crate-dorothy-ssr-0.1.0 (c (n "dorothy-ssr") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "00wmwk2il2h2y3igzlh6ilj5d6hfs1mqfw68jbh29w45q4fnwwqp")))

(define-public crate-dorothy-ssr-0.1.2 (c (n "dorothy-ssr") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1if5f3xynwc1sjvq68nimi7vik3axk4iabcqhss0j204ispdzpn6")))

(define-public crate-dorothy-ssr-0.1.3 (c (n "dorothy-ssr") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0n5wxd65xgjzz9sfbx5rm4r65lksa4mqny17g54jdp9lzk0zp1vg")))

(define-public crate-dorothy-ssr-0.1.4 (c (n "dorothy-ssr") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0nwfzdgw9nqls8k4hqnj3g0fl1y5kx3zk4ksqj1f6fkb5p3wwr00")))

