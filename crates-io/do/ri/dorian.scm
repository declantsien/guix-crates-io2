(define-module (crates-io do ri dorian) #:use-module (crates-io))

(define-public crate-dorian-0.1.0 (c (n "dorian") (v "0.1.0") (d (list (d (n "llvm-sys") (r "^120.1.0") (d #t) (k 0)))) (h "0ryp4694jpd34syg6pp3ww1ka8664nqa8ifi9brd8lc9hxi48kv0")))

(define-public crate-dorian-0.1.1 (c (n "dorian") (v "0.1.1") (d (list (d (n "llvm-sys") (r "^120.1.0") (d #t) (k 0)))) (h "1fkdmwpl2i50jgyk8x4y4css9aaxgrzfmkk1acmc5cys078cnchp")))

(define-public crate-dorian-0.2.0 (c (n "dorian") (v "0.2.0") (d (list (d (n "llvm-sys") (r "^140.0.2") (d #t) (k 0)))) (h "1xsh2yy0dxmfi7a22dkqbm58mlwwky0x5qw1rc5dghq1ih5n9ggg")))

(define-public crate-dorian-0.2.1 (c (n "dorian") (v "0.2.1") (d (list (d (n "llvm-sys") (r "^140.0.2") (d #t) (k 0)))) (h "04m9lam6ckifq8ikj2p3a7vjl1gm5ysy9kx88lnyj0xnp92chg93")))

