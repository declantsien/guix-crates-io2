(define-module (crates-io do ri doric) #:use-module (crates-io))

(define-public crate-doric-0.1.0 (c (n "doric") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0fqzrn1y5zvvi18xibgfb5lqk2rffp3bsbbqy64kksw3937xbdri")))

