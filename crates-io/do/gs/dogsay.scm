(define-module (crates-io do gs dogsay) #:use-module (crates-io))

(define-public crate-dogsay-0.1.0 (c (n "dogsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "04yss7zi6cl2sdjbalg86nrvh2s5gc8armdvsl4xgjs9a9pag83f")))

