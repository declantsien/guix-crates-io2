(define-module (crates-io do hy dohy) #:use-module (crates-io))

(define-public crate-dohy-0.1.0 (c (n "dohy") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dothyphen") (r "^0.1.0") (d #t) (k 0)))) (h "1wgjh00sshsk1zbqjx0l36hlv9v0pk8ggrv74j5nn91hgfzgl6h1")))

(define-public crate-dohy-0.2.0 (c (n "dohy") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dothyphen") (r "^0.2.0") (d #t) (k 0)))) (h "0ir1f0c5cadczqara5c0k2vcyynhncs75lnq7pfcmzmffq25hqvb")))

(define-public crate-dohy-0.2.1 (c (n "dohy") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dothyphen") (r "^0.2.1") (d #t) (k 0)))) (h "1vfkw0357bvmajqsy8pl7dr1wl2r3z4vg3hgibs8c57b4hjgyx88")))

(define-public crate-dohy-0.2.2 (c (n "dohy") (v "0.2.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dothyphen") (r "^0.2") (d #t) (k 0)))) (h "03b6ln08kirakfsb2alm76pdd99ciycfcyaz5dl2pm4bya1qxkfh")))

(define-public crate-dohy-1.0.0 (c (n "dohy") (v "1.0.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dothyphen") (r "^1.0.0") (d #t) (k 0)))) (h "1wbvi3fgl064xgg7p0h1kna78a7kh7vx40javim6xr98fi7svilj")))

