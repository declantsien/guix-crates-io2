(define-module (crates-io do nn donnager) #:use-module (crates-io))

(define-public crate-donnager-0.1.0 (c (n "donnager") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "04lgqq3npwmkzxv2j2yzwfamwb2b8jkgqgzv92jvk1pa36d8zhli")))

(define-public crate-donnager-0.1.1 (c (n "donnager") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1km501nzsd1fac20pab4g7yzh9qc33pvy0i1pmsad1lcg8gg4lv2")))

(define-public crate-donnager-0.1.2 (c (n "donnager") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "0hb15j0yxia2wvm800w9ngclbjk8v2l24b71frvfhnvy118qvfm6")))

