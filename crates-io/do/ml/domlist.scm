(define-module (crates-io do ml domlist) #:use-module (crates-io))

(define-public crate-domlist-1.6.0 (c (n "domlist") (v "1.6.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "09hrxyivm8mmrjd3ywhr8a5fsq42cfsa4dbsnrr61g3r7z7kl8zb")))

(define-public crate-domlist-1.6.1 (c (n "domlist") (v "1.6.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "02y0bas7p63h94sx0j4gargqin42ccz9w0ff07mv8h5v4bnbxczs")))

(define-public crate-domlist-1.6.2 (c (n "domlist") (v "1.6.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "1mcgi2gy8wygjcfx89552qkc2svgg0r8lhzr7da6s1pz88p38qsg")))

(define-public crate-domlist-1.6.3 (c (n "domlist") (v "1.6.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "1xa7niyrkwxy4qfzsz6m57plb19r64kcx3dc1avqfr1srgi44v1k")))

(define-public crate-domlist-1.7.0 (c (n "domlist") (v "1.7.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "1k5jgg82g4nlhi8zw1kl1cz32p212a02bsbfdi4wjblcp5c0sgrh")))

(define-public crate-domlist-1.7.1 (c (n "domlist") (v "1.7.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "03qblbyll45310bjgkrcqi68qwgx5mg2hmb118gvfrkfgnxwzkzd")))

(define-public crate-domlist-1.7.2 (c (n "domlist") (v "1.7.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "1wckkh0jpyzfmax4lb8qpsyi730g4rkgilql75admjbvxzlslkhq")))

(define-public crate-domlist-1.7.3 (c (n "domlist") (v "1.7.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "0z4ywszhj3j82gzslfsbxnm24lfg9vkagzmymn4c89mw5pvhw46h")))

(define-public crate-domlist-1.7.4 (c (n "domlist") (v "1.7.4") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "0lmlsjpvns39qhn5kk1f01p6wi75k7zvawd7ynxzcqnhb3pcy0qs")))

(define-public crate-domlist-1.7.5 (c (n "domlist") (v "1.7.5") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "0m49rkw8zwxb06ljkhfip67j6p48d7rrj5kwbwpzxk3g1gfxvj28")))

