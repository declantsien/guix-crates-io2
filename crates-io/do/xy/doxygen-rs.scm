(define-module (crates-io do xy doxygen-rs) #:use-module (crates-io))

(define-public crate-doxygen-rs-0.2.2 (c (n "doxygen-rs") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "01bm7s8g334ildrq5xzflwhgbf72plzf59pgwwa89fwv1km71xmz")))

(define-public crate-doxygen-rs-0.3.0 (c (n "doxygen-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1fvf9wx8prrcwl82y126zn7f3ipf9dldkn1isw6q6xr6iq8ny7d4")))

(define-public crate-doxygen-rs-0.3.1 (c (n "doxygen-rs") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "0d0din2qf8rsz6sj7ppavdv62smb63cc4h8hmgh625bc4p76qy82")))

(define-public crate-doxygen-rs-0.4.0 (c (n "doxygen-rs") (v "0.4.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "0mrqlf99gkcmsqcr4sfxx6qxci8i42jk5c0823kld9c4xpkv407i")))

(define-public crate-doxygen-rs-0.4.1 (c (n "doxygen-rs") (v "0.4.1") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "146xg2nx6pikfnywdi7jgvq68jzmqw8akmq7hlaakiwcpgh4lp7f")))

(define-public crate-doxygen-rs-0.4.2 (c (n "doxygen-rs") (v "0.4.2") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1n872n45ivhj3pqfwhbi7cvx00rn76a72x368ricykfkh33nwns1")))

