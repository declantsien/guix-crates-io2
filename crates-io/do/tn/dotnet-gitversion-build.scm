(define-module (crates-io do tn dotnet-gitversion-build) #:use-module (crates-io))

(define-public crate-dotnet-gitversion-build-0.3.0 (c (n "dotnet-gitversion-build") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1wpwxw7s0vbr83fb09j9wmj45rv548b74qqhfa60iqkpv74qm996")))

