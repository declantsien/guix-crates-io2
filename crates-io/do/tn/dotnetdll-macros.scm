(define-module (crates-io do tn dotnetdll-macros) #:use-module (crates-io))

(define-public crate-dotnetdll-macros-0.0.1 (c (n "dotnetdll-macros") (v "0.0.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yx9cf6v6m9xpyk1kadyq13nb176p28cbp9xdm3qhmd661v7y6ih")))

