(define-module (crates-io do tn dotnet35_rand_rs) #:use-module (crates-io))

(define-public crate-dotnet35_rand_rs-0.1.0 (c (n "dotnet35_rand_rs") (v "0.1.0") (h "1zb89vnprpl4mfd7yk3pmfkmkr4i1p87ah0d9gds1kfkawzg8vxk")))

(define-public crate-dotnet35_rand_rs-0.1.1 (c (n "dotnet35_rand_rs") (v "0.1.1") (h "1shmn8rlam89fh1846y62b1ry28844zdx4mvqq2f60j34b76qpja")))

(define-public crate-dotnet35_rand_rs-0.1.2 (c (n "dotnet35_rand_rs") (v "0.1.2") (h "16b0ih1pn823m6842kvsprcxvzcyb3cgn8ni23x2g7nsw1kzc3cb")))

(define-public crate-dotnet35_rand_rs-0.1.4 (c (n "dotnet35_rand_rs") (v "0.1.4") (h "0g7xq95gkcjs6ivsbac3c4b5y8r24bgarpwgfpppi2j1afss49y4") (y #t)))

(define-public crate-dotnet35_rand_rs-0.1.5 (c (n "dotnet35_rand_rs") (v "0.1.5") (h "084zqic04px8iidcj1kxvk2yxaa0wnbyxswfd8gz1hny38is03bj")))

(define-public crate-dotnet35_rand_rs-0.2.0 (c (n "dotnet35_rand_rs") (v "0.2.0") (h "0zcfmazrsngsz9i7aj7553l0gzjrlvxldzrfr39nxyaj5qaa9j5p") (y #t)))

(define-public crate-dotnet35_rand_rs-0.2.1 (c (n "dotnet35_rand_rs") (v "0.2.1") (h "1ylh6hnp8b98nw1gmhkxhqpmqjaak860v5zfl2g27w1wkk829kjc")))

