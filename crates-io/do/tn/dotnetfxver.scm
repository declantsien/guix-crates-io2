(define-module (crates-io do tn dotnetfxver) #:use-module (crates-io))

(define-public crate-dotnetfxver-0.1.0 (c (n "dotnetfxver") (v "0.1.0") (d (list (d (n "winreg") (r "^0.8.0") (f (quote ("transactions"))) (d #t) (k 0)))) (h "07wkzdj2w0gn6v68rg5mgv2p03yf4f0qwccvrzgzx6h3zj23w346") (y #t)))

(define-public crate-dotnetfxver-0.1.1 (c (n "dotnetfxver") (v "0.1.1") (d (list (d (n "winreg") (r "^0.8.0") (f (quote ("transactions"))) (d #t) (k 0)))) (h "14mn8gy3nvlgr2w4f6l8qrann8q0755dg9fvb68riq4rsp9kibix")))

(define-public crate-dotnetfxver-0.1.2 (c (n "dotnetfxver") (v "0.1.2") (d (list (d (n "winreg") (r "^0.8.0") (f (quote ("transactions"))) (d #t) (k 0)))) (h "0c41rbb2mxkcqakwfrrbmwpaipyd767c4khnxm9pd0a0aba2x54b")))

