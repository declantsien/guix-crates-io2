(define-module (crates-io do yo doyoumarkdown) #:use-module (crates-io))

(define-public crate-doyoumarkdown-0.0.1 (c (n "doyoumarkdown") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)))) (h "0q7d7p6hg2w2lj9ag7ljqnc6gfh1f0xi315avay140pjzpmbcr99")))

(define-public crate-doyoumarkdown-0.0.2 (c (n "doyoumarkdown") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)))) (h "1nq0y00asanwcwnlpwgqyvccqdsk5zvpsydxnx4xvrdcicd1ykg8")))

