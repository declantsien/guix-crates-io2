(define-module (crates-io do nh donhcd-sentry) #:use-module (crates-io))

(define-public crate-donhcd-sentry-0.1.5 (c (n "donhcd-sentry") (v "0.1.5") (d (list (d (n "chrono") (r "^0.2.22") (d #t) (k 0)) (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "145m09fcbk74izp2jp9v968m35xanhsa0hklpwpwgc93253gcgya")))

