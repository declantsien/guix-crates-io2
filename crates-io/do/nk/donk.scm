(define-module (crates-io do nk donk) #:use-module (crates-io))

(define-public crate-donk-0.1.0 (c (n "donk") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "156mj84f39978059jnhsga5s2yfg5zpxmlrj7icwb1cjfkpas96b") (r "1.65")))

