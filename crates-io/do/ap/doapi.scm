(define-module (crates-io do ap doapi) #:use-module (crates-io))

(define-public crate-doapi-0.1.0-alpha3 (c (n "doapi") (v "0.1.0-alpha3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)))) (h "05ak7zi43mbw2g8xax7i6xzc157bgn69gyrx4hxdshc9hr5gqj5s") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-doapi-0.1.0 (c (n "doapi") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)))) (h "0bb4yfpjmh2d61d6i43v9584s7xgvwls896hx2fw91bda07z52zx") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-doapi-0.1.1 (c (n "doapi") (v "0.1.1") (d (list (d (n "hyper") (r "~0.6.11") (d #t) (k 0)) (d (n "regex") (r "~0.1.41") (d #t) (k 0)) (d (n "serde") (r "~0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.5.3") (d #t) (k 0)))) (h "16fv158c6vi5dsyf9jrxidnd69c45ykl7dpk1jbcyr1cf4cf7h9r") (f (quote (("unstable") ("default") ("debug"))))))

(define-public crate-doapi-0.1.2 (c (n "doapi") (v "0.1.2") (d (list (d (n "clippy") (r "~0.0.22") (o #t) (d #t) (k 0)) (d (n "hyper") (r "~0.7") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 0)))) (h "1hwdjzbw983jcsqzmy6rdk1wjwbhmwccymhg44j860d8gn3pxvmh") (f (quote (("unstable" "nightly" "lints") ("nightly") ("lints" "nightly" "clippy") ("default" "nightly") ("debug"))))))

(define-public crate-doapi-0.1.3 (c (n "doapi") (v "0.1.3") (d (list (d (n "clippy") (r "~0.0.68") (o #t) (d #t) (k 0)) (d (n "hyper") (r "~0.7") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "133c042qkb2qdmxhay3xdslhw8vygx6kxw363srzmssixm6ll8z6") (f (quote (("unstable" "nightly" "lints") ("nightly") ("lints" "nightly" "clippy") ("default" "nightly") ("debug"))))))

