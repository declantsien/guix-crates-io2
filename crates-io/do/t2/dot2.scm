(define-module (crates-io do t2 dot2) #:use-module (crates-io))

(define-public crate-dot2-0.1.0 (c (n "dot2") (v "0.1.0") (h "1rl93dgxvq2ava45l43gp7qzj7mn109g26b2mr16md6x6vc6l12g")))

(define-public crate-dot2-1.0.0-beta.1 (c (n "dot2") (v "1.0.0-beta.1") (h "0xy488scizpych4im44d09sbawsms80559g1lnqlzhffvzxf68cq")))

(define-public crate-dot2-1.0.0 (c (n "dot2") (v "1.0.0") (h "1hwcvqcrazwfvgdr3p3h4fkrfi90qip6d6ividwp7k4b2pr26m45")))

