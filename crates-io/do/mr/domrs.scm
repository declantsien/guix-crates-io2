(define-module (crates-io do mr domrs) #:use-module (crates-io))

(define-public crate-domrs-0.0.1 (c (n "domrs") (v "0.0.1") (h "1xwfi28qa60xp9f8yjk4hkx33mgmfwghh441zlk72ihvzfc5a4mf")))

(define-public crate-domrs-0.0.2 (c (n "domrs") (v "0.0.2") (h "1pchj1nn3fh79qjqarvcd52a036vs9jh3069a2dz5jb5kg0v4sqd")))

(define-public crate-domrs-0.0.3 (c (n "domrs") (v "0.0.3") (h "1a62hjrqcrgq9l2hsjgxhzcs01pz637a5dhdp6w8wp001zqxn2jl")))

(define-public crate-domrs-0.0.4 (c (n "domrs") (v "0.0.4") (h "0c1w5arg8bw6fc906hakzjqb8axywl9y3jvzbbppcqqfj5ljzypq")))

(define-public crate-domrs-0.0.5 (c (n "domrs") (v "0.0.5") (h "0s29g4611n86lpqqnf74f05knpq2xbcqfmczbrvsnzx33rrgsb4y")))

(define-public crate-domrs-0.0.6 (c (n "domrs") (v "0.0.6") (h "0bm3qhmw174q7rklj1dfa698b6zkqy0jy28bd91zsfw4gxz7ia4s")))

(define-public crate-domrs-0.0.7 (c (n "domrs") (v "0.0.7") (h "12p7blrvdjssmy4g3hkvh5g8ah4zrc9qaja6l0h1fdlacccbsa5y")))

(define-public crate-domrs-0.0.8 (c (n "domrs") (v "0.0.8") (h "01blydx930chk3rzym97gsqy3vmkaja2ajz0vqxcmzzjw854iljk")))

(define-public crate-domrs-0.0.9 (c (n "domrs") (v "0.0.9") (h "1fvwn4nlp8xq6nfdq51xwqr8xpv7pgj9nxha9r2bzl2xnavlp0hh")))

(define-public crate-domrs-0.0.10 (c (n "domrs") (v "0.0.10") (h "0ipyhvrhpzflsyhxm339j28m863ihlkqy7wkz8jfq2jaw1y3x4h8")))

(define-public crate-domrs-0.0.11 (c (n "domrs") (v "0.0.11") (h "0bh3jslrjng6wfq8kgg6wn4r0hx7pmdmhgi98zas4zr7l700zgnz")))

(define-public crate-domrs-0.0.12 (c (n "domrs") (v "0.0.12") (h "0cz3bamgq07q6zbb9jlkxjynhjr1dycja98j3wxn24m2k54zj3wc")))

(define-public crate-domrs-0.0.13 (c (n "domrs") (v "0.0.13") (h "1ijzi70y7h1wbphdg0j8szdg52ri5x9kr5yfh4krnd6yqs08zfk3")))

(define-public crate-domrs-0.0.14 (c (n "domrs") (v "0.0.14") (h "0qfc4vq3vzrd8fznjd5w2mw4yf0vpmk2w6ch3ab2r8lf4k60zn6d")))

(define-public crate-domrs-0.0.15 (c (n "domrs") (v "0.0.15") (h "1zqgs3x7pyywhchfkdywrcri00r079rar42kwyg7nmpfq6p8fsxg")))

(define-public crate-domrs-0.0.16 (c (n "domrs") (v "0.0.16") (h "1d4imbqmnfgsj76ybm7rg89lfhcyv9fjpxzifwmx3gzhfk66mnpx")))

