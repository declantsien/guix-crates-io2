(define-module (crates-io do _u do_username) #:use-module (crates-io))

(define-public crate-do_username-0.1.0 (c (n "do_username") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14barcsv0lg5lf4mpysqpc6prcz8b08gwmjfwsncsxzmp6m2dfm9")))

(define-public crate-do_username-1.0.0 (c (n "do_username") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "08vncmls8sghnrkazwbspc9cqjdvxwwlb5npsar1w8wa38z9a2hp")))

