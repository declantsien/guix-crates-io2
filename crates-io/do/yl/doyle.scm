(define-module (crates-io do yl doyle) #:use-module (crates-io))

(define-public crate-doyle-0.0.1 (c (n "doyle") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ri5k2ip44jnf0322jp5ifzg1522xvpjxykjn0212vb4q14gb7py")))

