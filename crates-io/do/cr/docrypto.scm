(define-module (crates-io do cr docrypto) #:use-module (crates-io))

(define-public crate-docrypto-0.1.0 (c (n "docrypto") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0vdw4aabmw7arrd3dkfsx0d2gldccv5y0xzivdlm3ng6jcz6wfh8")))

(define-public crate-docrypto-0.1.1 (c (n "docrypto") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06i2v0d2w4hivmwqh7yx3jsa041g4vibvsz4p59pf7viz0kbn0rg")))

