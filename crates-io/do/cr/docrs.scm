(define-module (crates-io do cr docrs) #:use-module (crates-io))

(define-public crate-docrs-0.1.0 (c (n "docrs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.14") (d #t) (k 0)))) (h "0sp6556y12ka2dr2ly9fcqlxfdj81prb5n9iwq7h6vh8zvc0cbmr") (r "1.74.0")))

