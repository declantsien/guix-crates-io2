(define-module (crates-io do cx docx) #:use-module (crates-io))

(define-public crate-docx-0.1.0 (c (n "docx") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1767bd90cq2hfbj84bc8ccbjc5qrg2jhkad4brrc007qmp7pv1fb")))

(define-public crate-docx-0.1.1 (c (n "docx") (v "0.1.1") (d (list (d (n "docx-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "0c7xbcql4ccv3466ysv5zm5vjar5hx0h6l8ay8478s0id3db802d")))

(define-public crate-docx-0.1.2 (c (n "docx") (v "0.1.2") (d (list (d (n "docx-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "17cj4yxvj4dvlljncxnrfsc510yhz3jjb2y385zldz14saa2xcks")))

(define-public crate-docx-0.1.3 (c (n "docx") (v "0.1.3") (d (list (d (n "docx-codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (d #t) (k 0)))) (h "0xf7lw7kjmp50cllkqad0znfbywhdhxrdqqzgn662flh8m9v5ymj")))

(define-public crate-docx-0.1.4 (c (n "docx") (v "0.1.4") (d (list (d (n "docx-codegen") (r "^0.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (d #t) (k 0)))) (h "1xnlg5hwr61iijhamiyc3c1jqq01ppl2svckgz30iwvk2y8n706v")))

(define-public crate-docx-0.2.0 (c (n "docx") (v "0.2.0") (d (list (d (n "docx-codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (d #t) (k 0)) (d (n "xmlparser") (r "^0.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (d #t) (k 0)))) (h "1iq8l4af0ybldsq5kxbjyaaj37vhkz6nw6sa0d69mbs7qyj8h3qf")))

(define-public crate-docx-0.2.1 (c (n "docx") (v "0.2.1") (d (list (d (n "docx-codegen") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (d #t) (k 0)) (d (n "xmlparser") (r "^0.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (d #t) (k 0)))) (h "01i3irp83n31999dibj09ng9drhymmqn4n3zq55knwkpik74qlla")))

(define-public crate-docx-1.0.0 (c (n "docx") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "strong-xml") (r "^0.2.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "17r8iy6qjmj2b27rdxnxdzisyg6sikvdyncammpdmcws7lwigj2k")))

(define-public crate-docx-1.1.0 (c (n "docx") (v "1.1.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "strong-xml") (r "^0.2.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "0cjr0pnzam2hb2cr8xlywc65mz51qalpi7q7g2m8wivdqhjnlkb5")))

(define-public crate-docx-1.1.1 (c (n "docx") (v "1.1.1") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "strong-xml") (r "^0.4.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "14fw65z1blrbhyp8d1ms3hsdh66l9clrknna760is4mh4852ppva")))

(define-public crate-docx-1.1.2 (c (n "docx") (v "1.1.2") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "strong-xml") (r "^0.5.0") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "19y79nii6m16jx1qlfy2ad4zmg9iy3r87w0s99kq2lhq1rwxs9p6")))

