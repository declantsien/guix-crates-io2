(define-module (crates-io do cx docx-rust) #:use-module (crates-io))

(define-public crate-docx-rust-0.1.0 (c (n "docx-rust") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1bypk0jvs3igyb0wyqr73p93m7x892z8adcall0zrfq9bl778y1x") (y #t)))

(define-public crate-docx-rust-0.1.1 (c (n "docx-rust") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "0x81n27pywi39i3hszl4hdf87bggjxwf5a4fmb3mylr2rha8xvk4") (y #t)))

(define-public crate-docx-rust-0.1.2 (c (n "docx-rust") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1mnj0nmm38s88r5g2xpzs1b1hv9dc19j3k6s95k47swf364500v8")))

(define-public crate-docx-rust-0.1.3 (c (n "docx-rust") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "16p9nx6im1d5xjc757g6cms4rgm3nz4gilqcnnb041ab2din9gd2")))

(define-public crate-docx-rust-0.1.4 (c (n "docx-rust") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1lmfz778fh01sy2r0bbsn9aah6dis4ijmji5dzl3sgp55227rxm3") (y #t)))

(define-public crate-docx-rust-0.1.5 (c (n "docx-rust") (v "0.1.5") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strong-xml") (r "^0.6.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "1asblnjpnf63qmrc6lynzxj2j5sxfg1fv6jmymvmaxflciwx9b50")))

(define-public crate-docx-rust-0.1.6 (c (n "docx-rust") (v "0.1.6") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "hard-xml") (r "^1.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)))) (h "13xia11v080x7663qp0m8vdpqphkpcvjpw31m6jw1xnppj4aqklc")))

(define-public crate-docx-rust-0.1.7 (c (n "docx-rust") (v "0.1.7") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "hard-xml") (r "^1.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zip") (r "^1.1.2") (f (quote ("deflate"))) (k 0)))) (h "1i37jd278g6r0vq5y77rzg6irhwsr9grmckhwy54p7snhqw3sp49")))

(define-public crate-docx-rust-0.1.8 (c (n "docx-rust") (v "0.1.8") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "hard-xml") (r "^1.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zip") (r "^1.1.2") (f (quote ("deflate"))) (k 0)))) (h "08p5ycba3glii70skzmzjkdx65ni6jjyvgpjq57aa2rz844p6l11")))

