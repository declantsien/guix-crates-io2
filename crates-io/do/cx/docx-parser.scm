(define-module (crates-io do cx docx-parser) #:use-module (crates-io))

(define-public crate-docx-parser-0.1.1 (c (n "docx-parser") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "docx-rust") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1r1cyhgqffqq4z913avpzarzfclkd9ad8z53ml8q62kdrhqb49ii")))

