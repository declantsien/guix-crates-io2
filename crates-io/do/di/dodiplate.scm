(define-module (crates-io do di dodiplate) #:use-module (crates-io))

(define-public crate-dodiplate-1.0.0 (c (n "dodiplate") (v "1.0.0") (d (list (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18mfwnwvn8yrkdgbpz2qf54bmk14d5pw7a9vqs1pvd2l6wrhh79s")))

(define-public crate-dodiplate-1.1.0 (c (n "dodiplate") (v "1.1.0") (d (list (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hcbr07c2d0f6jxlawxmw54k0bkq7qbzww06c4q6j3y7p69whrg6")))

(define-public crate-dodiplate-1.1.1 (c (n "dodiplate") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sbav7hzk5iqaxd5w86z2hplk6m2zpnyyy8pr2a8mwyfsqflmb80")))

