(define-module (crates-io do ts dots_internal_utils) #:use-module (crates-io))

(define-public crate-dots_internal_utils-0.3.0 (c (n "dots_internal_utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kbz3pwmcz7a9gkhf8ad62a0m0rzz1cyyhqq4hh9xn4p535v44vc")))

(define-public crate-dots_internal_utils-0.4.0 (c (n "dots_internal_utils") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1cz7ckh16zx3qrbv2ffjbhms1hs208ia65r9qwccxnm1rap4zgib")))

(define-public crate-dots_internal_utils-0.5.0 (c (n "dots_internal_utils") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "supports-color") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0a1r4fc3v916faigh0yba5kzrhw4dikr5skgph8jlc41yxl1xfhr")))

