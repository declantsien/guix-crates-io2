(define-module (crates-io do od doodle) #:use-module (crates-io))

(define-public crate-doodle-0.1.0 (c (n "doodle") (v "0.1.0") (d (list (d (n "doodle_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1swpifzl8f0ggsnag1h7qrc7ihcaq4jkj3ipk5vvvvzif7dmfg9w")))

(define-public crate-doodle-0.1.1 (c (n "doodle") (v "0.1.1") (d (list (d (n "doodle_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kv2m20g8abqlw1sbcqqy111p29ipq315w8j7dln2mzzd0phhavy") (f (quote (("derive" "doodle_derive"))))))

(define-public crate-doodle-0.1.2 (c (n "doodle") (v "0.1.2") (d (list (d (n "doodle_derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01nhf5cskbnqvvc18d7mfic39rj36r845zc9544a2zfwr9cq02md") (f (quote (("derive" "doodle_derive"))))))

(define-public crate-doodle-0.2.0 (c (n "doodle") (v "0.2.0") (d (list (d (n "doodle_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nf1xddv80d9af4arkh0dawirahxg22kzbvpl6zkvpg4kg4kax53") (f (quote (("derive" "doodle_derive"))))))

