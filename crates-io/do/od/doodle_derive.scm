(define-module (crates-io do od doodle_derive) #:use-module (crates-io))

(define-public crate-doodle_derive-0.1.0 (c (n "doodle_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "05x54an9g9r9vxcsyr6ahn9wqqymyva361h4b7jifrjgvkq85n41")))

(define-public crate-doodle_derive-0.1.1 (c (n "doodle_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1kxwh3swc8fm1qqzjxlbgaa313l3f9dsipr3kh587wj81ppssj6m")))

(define-public crate-doodle_derive-0.1.2 (c (n "doodle_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "05qn616gqhxg3d9ald82npcxdxcfx6f88mizlmp18i7lg9sfp039")))

(define-public crate-doodle_derive-0.2.0 (c (n "doodle_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1zapgd3c2j3f8qjbzp0dw2r4aasrjadk67b65g03jj8h59z9cg60")))

