(define-module (crates-io do in doin) #:use-module (crates-io))

(define-public crate-doin-1.0.0 (c (n "doin") (v "1.0.0") (h "0b3k3m9xvd1q60m93q5vlhdgqyrrp21sh0ymjmyl4qp77xv9wfh5")))

(define-public crate-doin-1.0.1 (c (n "doin") (v "1.0.1") (h "0va63xi4725z183g7rypri9ljq2pyqjfj6lbfpz06lv83cs9kjij")))

(define-public crate-doin-1.0.2 (c (n "doin") (v "1.0.2") (h "191b67hmlfl27wy65xmi5cc817w04kvlpdn5ir66gqqfkvpx1qdc")))

(define-public crate-doin-1.0.3 (c (n "doin") (v "1.0.3") (h "160yyrw8q02r8n8xg9gi157ziy2vaja983zpsy6yc9r6yn5zkik4")))

