(define-module (crates-io do ce docext) #:use-module (crates-io))

(define-public crate-docext-0.1.0 (c (n "docext") (v "0.1.0") (h "0zi0593mskblwbd77ip8lcakx3vbcbiqbdxh709ynfkcpijginxj") (y #t)))

(define-public crate-docext-0.0.1 (c (n "docext") (v "0.0.1") (h "17mxxs1jmz565bylf6w2sf3yccazbyfxqd48pwb94p67m15mnhc1")))

(define-public crate-docext-0.0.2 (c (n "docext") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wgspymylgpm9jilwjf7hr2pvigy7sz38ncflqx3lhbk8y6d3j1z")))

(define-public crate-docext-0.0.3 (c (n "docext") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ik95kfbhx72nay1ay8lhla90mggynrncfambsi5fmx3khs8n1an")))

(define-public crate-docext-0.0.4 (c (n "docext") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c4mbraswnai10k7q8mb9gb4pkxf63jsyyvqcwbs72jn5m2vlmlg")))

(define-public crate-docext-0.0.5 (c (n "docext") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q0fqzjsmhk8fk2m8qnvkslc6v7rgx0kvrzm4bg8jlms7h7hvpsb")))

(define-public crate-docext-0.0.6 (c (n "docext") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wazvwx7hrn140pvb3ba3s4wiafcnggg92r71q92bl5vn3i7xbz0")))

(define-public crate-docext-0.0.7 (c (n "docext") (v "0.0.7") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1dlc4rnb0g2fads0cjp0xlvs73baqvmcj0j5bm79abmazpnb1vwl")))

(define-public crate-docext-0.0.8 (c (n "docext") (v "0.0.8") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0hrv4cbvyag80x2ajnfa6di721pbnbqd4qw3n5ph2j2pkc6pnpfp")))

(define-public crate-docext-0.0.9 (c (n "docext") (v "0.0.9") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0gb7lnp64315rs2si31bzfd8d9hv16bgq28in94p5111icrlm2sh")))

(define-public crate-docext-0.0.10 (c (n "docext") (v "0.0.10") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "06lfwdiyv1qk9g5rxjk8a8c1qqpn46fhnkkn3z537sixrpbqwba1")))

(define-public crate-docext-0.0.11 (c (n "docext") (v "0.0.11") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "11fg5rh4k3w1299cqgkbwd7j06hwbqqcwy7pv3yz4x1mf2ia2pk6")))

