(define-module (crates-io do ra dora-record) #:use-module (crates-io))

(define-public crate-dora-record-0.3.0-rc2 (c (n "dora-record") (v "0.3.0-rc2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.0-rc2") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.0-rc2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0v3qml6yvrv41nn7vyr5hfjvkpvwhdr8rwn880zjl805psgi46ki")))

(define-public crate-dora-record-0.3.0-rc3 (c (n "dora-record") (v "0.3.0-rc3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.0-rc3") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.0-rc3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "08cgcga6a973m88362rlasxdl36k4biwz6p4fz3rizf3lzygjxvw")))

(define-public crate-dora-record-0.3.0-rc4 (c (n "dora-record") (v "0.3.0-rc4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.0-rc4") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.0-rc4") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "17rmqhxc8kjmwqgk8a3i4hc3k0fds6dyikg7m9yvrnq6s70plms1")))

(define-public crate-dora-record-0.3.0 (c (n "dora-record") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.0") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0qj8j41jjyafhsjic7df8xh88mnqigr86v6r126ic0l3yg39hdcj")))

(define-public crate-dora-record-0.3.1-rc5 (c (n "dora-record") (v "0.3.1-rc5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.1-rc5") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.1-rc5") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0s5q6ybbqhbsyw0njzh7720rha93n6864frmagyz5cr2jnzh2p6d")))

(define-public crate-dora-record-0.3.1-rc6 (c (n "dora-record") (v "0.3.1-rc6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.1-rc6") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.1-rc6") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0a95fs8zwc1s8m5681j10dwg4j527qjg4ifrcgk5d7ba9wwfz3rn")))

(define-public crate-dora-record-0.3.1 (c (n "dora-record") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.1") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0mi7p6gr898602m8rjjx8r72ncd287h3gd9yagj40vg7bzdna5br")))

(define-public crate-dora-record-0.3.2-rc2 (c (n "dora-record") (v "0.3.2-rc2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.2-rc2") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.2-rc2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1228cz0h55632vwgyhxgvxyxjdr4vgljghvlvc0d3spxsc2gws22")))

(define-public crate-dora-record-0.3.2 (c (n "dora-record") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.2") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1wqx2s4xndxa6r0h4ic77pmvwl44mjkghfqh1c90mcqd6v6dgw4b")))

(define-public crate-dora-record-0.3.3-rc1 (c (n "dora-record") (v "0.3.3-rc1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.3-rc1") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.3-rc1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "parquet") (r "^48.0.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1app6kjc0sg0ba1jpgwdvc2iw72ibrf02qfg74cvaa385qafax2j")))

(define-public crate-dora-record-0.3.3 (c (n "dora-record") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.3") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "parquet") (r "^48.0.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "00yzmqwwabyp7q2ga62ziqcbcl4828dmjz3swzc21dlkgfs73wpa")))

(define-public crate-dora-record-0.3.4-rc1 (c (n "dora-record") (v "0.3.4-rc1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.4-rc1") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.4-rc1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "parquet") (r "^48.0.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1xl557w9s4kax3fr1ax7k5fx09q7yh9d0kj475pg10sdmcja7l34")))

(define-public crate-dora-record-0.3.4-rc2 (c (n "dora-record") (v "0.3.4-rc2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.4-rc2") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.4-rc2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "parquet") (r "^48.0.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0rw5c5d9grprfkc91j3y2p1wmwm140358nv15rg9riyvs5kv6fjk")))

(define-public crate-dora-record-0.3.4 (c (n "dora-record") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dora-node-api") (r "^0.3.4") (f (quote ("tracing"))) (k 0)) (d (n "dora-tracing") (r "^0.3.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "parquet") (r "^48.0.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1j2b6qfp82vv2bpg4z1p4zam1i12fcwgxp9k830dymplkyyf2khc")))

