(define-module (crates-io do ra dora-ssr) #:use-module (crates-io))

(define-public crate-dora-ssr-0.1.4 (c (n "dora-ssr") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1gpinrnw1k8y5b2v2l6964d9g4f1vagmkv3w4c14hglznmwg9xsp")))

(define-public crate-dora-ssr-0.1.5 (c (n "dora-ssr") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1025rsfyp903vw6l6z7sc0wlc77lgif2in3x78fjhprxqg87ivx1")))

(define-public crate-dora-ssr-0.1.6 (c (n "dora-ssr") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1ihc1gjgsvpzzwclbyn1rzalbr75qf8h8lkr10rg5hg3n4c3sg1z")))

(define-public crate-dora-ssr-0.1.7 (c (n "dora-ssr") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1saja7ylf6nw73s044mmzcsv5y5sd1r2dr2wbx3gn58899wav499")))

(define-public crate-dora-ssr-0.1.8 (c (n "dora-ssr") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1ajmxgzfsmd9al805p19l76a62m1cn6vbmlm3yx2n9f0ap7pwwdn")))

(define-public crate-dora-ssr-0.1.9 (c (n "dora-ssr") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0l9z8r6x5f4rdw1z2f0280mzgcq9snwrqlq0y5sf3hz0k73b0m4i")))

(define-public crate-dora-ssr-0.2.0 (c (n "dora-ssr") (v "0.2.0") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1n77wbc4h3fknc2wiva4k3s540p91cjh8x798l266f7vnysiqf1s")))

(define-public crate-dora-ssr-0.2.1 (c (n "dora-ssr") (v "0.2.1") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "00rnzmqnni94995c96v62hc4v4blb7s7646ybznc85l89xsxmz69")))

(define-public crate-dora-ssr-0.2.2 (c (n "dora-ssr") (v "0.2.2") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0kbs2d7yzgygzkf22m7dzjf7rzay75ndhd42kfq635yapmgsxjxi")))

(define-public crate-dora-ssr-0.2.3 (c (n "dora-ssr") (v "0.2.3") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0d1awb6dkmyvr5njg5iqcdhcmfpiywqbpqaid7rwznsdbj9g03g1")))

(define-public crate-dora-ssr-0.2.4 (c (n "dora-ssr") (v "0.2.4") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1n75ljph6744pcg876sjnni71q0amm7j278wv7pjilvkk5gi7yqh")))

(define-public crate-dora-ssr-0.2.5 (c (n "dora-ssr") (v "0.2.5") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "04rr1rjz8jbsl6drpssb4hicksi6h5dlwnn3k1g14jiblmh2mbw6")))

(define-public crate-dora-ssr-0.2.6 (c (n "dora-ssr") (v "0.2.6") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0ww2m7bvr4i6pi4w9yyyrrrzhdwswyhhmv3fxrrhqahzx13p07c6")))

(define-public crate-dora-ssr-0.2.7 (c (n "dora-ssr") (v "0.2.7") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "10sxh7mgskzmankwv27cg34hamly8k2wggghhlqg19y28pxr7hr2")))

(define-public crate-dora-ssr-0.2.8 (c (n "dora-ssr") (v "0.2.8") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "097fg33s5fkmjy8zhn91wh8plal89wmsqrjmgm0b20mrrz3y3y3f")))

