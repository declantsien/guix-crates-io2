(define-module (crates-io do ra dora-operator-api-c) #:use-module (crates-io))

(define-public crate-dora-operator-api-c-0.2.1-rc (c (n "dora-operator-api-c") (v "0.2.1-rc") (d (list (d (n "dora-operator-api-types") (r "^0.2.1-rc") (d #t) (k 1)))) (h "1gh7l3qd27j3ff1790m40p2qmavjc3r0w6lix58y39wxqqwpp7mi")))

(define-public crate-dora-operator-api-c-0.2.1 (c (n "dora-operator-api-c") (v "0.2.1") (d (list (d (n "dora-operator-api-types") (r "^0.2.1") (d #t) (k 1)))) (h "0b87g3fqaw2hq5hlbayx2rx5qjiwb6dbc5qbnin10hd06rggv5vm")))

(define-public crate-dora-operator-api-c-0.2.2-rc (c (n "dora-operator-api-c") (v "0.2.2-rc") (d (list (d (n "dora-operator-api-types") (r "^0.2.2-rc") (d #t) (k 1)))) (h "01hb3ff6ijjmkcn3r1sgp33xybi3gy3dvf0c15shxw8bn8a7bgr9")))

(define-public crate-dora-operator-api-c-0.2.2-rc-2 (c (n "dora-operator-api-c") (v "0.2.2-rc-2") (d (list (d (n "dora-operator-api-types") (r "^0.2.2-rc-2") (d #t) (k 1)))) (h "07z6a533ffwyyi0x8pvs2q9kalcrw2wv90fyhcq6p8m7kkcxvvwh")))

(define-public crate-dora-operator-api-c-0.2.2 (c (n "dora-operator-api-c") (v "0.2.2") (d (list (d (n "dora-operator-api-types") (r "^0.2.2") (d #t) (k 1)))) (h "1r0y9kqdgi775fcrchk7bv2bsj8wagajv14bm3xml173mryqwr8c")))

(define-public crate-dora-operator-api-c-0.2.3-rc (c (n "dora-operator-api-c") (v "0.2.3-rc") (d (list (d (n "dora-operator-api-types") (r "^0.2.3-rc") (d #t) (k 1)))) (h "0245lhw6828cn9c3wfzxnxwp47g8l21y7y4xqpxggcrsxl0w75zn")))

(define-public crate-dora-operator-api-c-0.2.3-rc2 (c (n "dora-operator-api-c") (v "0.2.3-rc2") (d (list (d (n "dora-operator-api-types") (r "^0.2.3-rc2") (d #t) (k 1)))) (h "04wz5cggdpwwy69sj6v0iilmii7pc8s82z39c7qama88j74x0axz")))

(define-public crate-dora-operator-api-c-0.2.3-rc4 (c (n "dora-operator-api-c") (v "0.2.3-rc4") (d (list (d (n "dora-operator-api-types") (r "^0.2.3-rc4") (d #t) (k 1)))) (h "180s24fymz6wm32xp8gkzxwb70ln4yncnrhkr16i4a5m6npdcvyh")))

(define-public crate-dora-operator-api-c-0.2.3-rc5 (c (n "dora-operator-api-c") (v "0.2.3-rc5") (d (list (d (n "dora-operator-api-types") (r "^0.2.3-rc5") (d #t) (k 1)))) (h "0pqbmxhq5zdal78ji80cp4q27j3n7xv66djb0vbygc6nnmhnbxnk")))

(define-public crate-dora-operator-api-c-0.2.3-rc6 (c (n "dora-operator-api-c") (v "0.2.3-rc6") (d (list (d (n "dora-operator-api-types") (r "^0.2.3-rc6") (d #t) (k 1)))) (h "0wrg1y19kbybj916y6zw7kl0x39zpmxz9c12niy039x7lj43kppy")))

(define-public crate-dora-operator-api-c-0.2.3 (c (n "dora-operator-api-c") (v "0.2.3") (d (list (d (n "dora-operator-api-types") (r "^0.2.3") (d #t) (k 1)))) (h "1vqjw5mmb8bsfh28k9nlrsmb61z3ddydqrgm1m0yahc6b8zmhzld")))

(define-public crate-dora-operator-api-c-0.2.4-rc3 (c (n "dora-operator-api-c") (v "0.2.4-rc3") (d (list (d (n "dora-operator-api-types") (r "^0.2.4-rc3") (d #t) (k 1)))) (h "1rv693mz8n85mbxc8bramn5j4cryrlj9rxn43z2rf3b8v6rj24gz")))

(define-public crate-dora-operator-api-c-0.2.4 (c (n "dora-operator-api-c") (v "0.2.4") (d (list (d (n "dora-operator-api-types") (r "^0.2.4") (d #t) (k 1)))) (h "0fr3wlj3q225cpiwxh0i02cw8i47jnw2l444i7q3riqpcn707dci")))

(define-public crate-dora-operator-api-c-0.2.5-alpha.1 (c (n "dora-operator-api-c") (v "0.2.5-alpha.1") (d (list (d (n "dora-operator-api-types") (r "^0.2.5-alpha.1") (d #t) (k 1)))) (h "0faxag8awy0wf8nvrwcyfs9759k5dd5nbfswf5gryh9wm47wk69i")))

(define-public crate-dora-operator-api-c-0.2.5 (c (n "dora-operator-api-c") (v "0.2.5") (d (list (d (n "dora-operator-api-types") (r "^0.2.5") (d #t) (k 1)))) (h "0lc6h2qvrvzh1nfb8wibycg54w75rq6jrajrq6wmhka7cy8xsb19")))

(define-public crate-dora-operator-api-c-0.2.6 (c (n "dora-operator-api-c") (v "0.2.6") (d (list (d (n "dora-operator-api-types") (r "^0.2.6") (d #t) (k 1)))) (h "18nhwfwimi5i9yhcyx69mv31sbj3rf6gwn9f089khjhwfc5i08a3")))

(define-public crate-dora-operator-api-c-0.3.0-rc2 (c (n "dora-operator-api-c") (v "0.3.0-rc2") (d (list (d (n "dora-operator-api-types") (r "^0.3.0-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc2") (d #t) (k 1)))) (h "1bddv9f1dnybhx4shjfkndhxhpzcpz26qkkj6w3gcs68135sxrjk")))

(define-public crate-dora-operator-api-c-0.3.0-rc3 (c (n "dora-operator-api-c") (v "0.3.0-rc3") (d (list (d (n "dora-operator-api-types") (r "^0.3.0-rc3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc3") (d #t) (k 1)))) (h "02cwy24k1ivhw5xgbqmxy7l8bk2qgl76pxxa9ph4i55s6mapm51r")))

(define-public crate-dora-operator-api-c-0.3.0-rc4 (c (n "dora-operator-api-c") (v "0.3.0-rc4") (d (list (d (n "dora-operator-api-types") (r "^0.3.0-rc4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc4") (d #t) (k 1)))) (h "0331v6ag8x76nd9cchhnakz5lmy1qggiah1gb0lpwkrqjvcdzs0c")))

(define-public crate-dora-operator-api-c-0.3.0 (c (n "dora-operator-api-c") (v "0.3.0") (d (list (d (n "dora-operator-api-types") (r "^0.3.0") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0") (d #t) (k 1)))) (h "0j2vz1scq4p05j388nzcv54hbhij80ykhncxfl5b2qy1zn2b6a0z")))

(define-public crate-dora-operator-api-c-0.3.1-rc2 (c (n "dora-operator-api-c") (v "0.3.1-rc2") (d (list (d (n "dora-operator-api-types") (r "^0.3.1-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc2") (d #t) (k 1)))) (h "10n745rbqs6igb6dpm755wlx2wsm5yb0g1lsz8k706gi9s7jgizd")))

(define-public crate-dora-operator-api-c-0.3.1-rc3 (c (n "dora-operator-api-c") (v "0.3.1-rc3") (d (list (d (n "dora-operator-api-types") (r "^0.3.1-rc3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc3") (d #t) (k 1)))) (h "056rz6ja2ynmp08imxxz3c73s80fcxfj8a2hyv7b624lw7k9lfav")))

(define-public crate-dora-operator-api-c-0.3.1-rc5 (c (n "dora-operator-api-c") (v "0.3.1-rc5") (d (list (d (n "dora-operator-api-types") (r "^0.3.1-rc5") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc5") (d #t) (k 1)))) (h "1vlxkgy1dhajihjlnr3rwcckjjgamrczs68dz8zggx1ykkh03p5r")))

(define-public crate-dora-operator-api-c-0.3.1-rc6 (c (n "dora-operator-api-c") (v "0.3.1-rc6") (d (list (d (n "dora-operator-api-types") (r "^0.3.1-rc6") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc6") (d #t) (k 1)))) (h "1j53qqnkx10pfg99ir1jwiiy0mccl99zgmx1akzdxjbs3kvq4y6m")))

(define-public crate-dora-operator-api-c-0.3.1 (c (n "dora-operator-api-c") (v "0.3.1") (d (list (d (n "dora-operator-api-types") (r "^0.3.1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1") (d #t) (k 1)))) (h "0ni3jk4dad6awy44iqyr5abbixr4f5ljrgxfb79vgl7zglshaq1r")))

(define-public crate-dora-operator-api-c-0.3.2-rc1 (c (n "dora-operator-api-c") (v "0.3.2-rc1") (d (list (d (n "dora-operator-api-types") (r "^0.3.2-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2-rc1") (d #t) (k 1)))) (h "1nyv7yjjj3avdg131i681zlrcpfkc61mqm2qlvkzq39ljnxb1gv9")))

(define-public crate-dora-operator-api-c-0.3.2-rc2 (c (n "dora-operator-api-c") (v "0.3.2-rc2") (d (list (d (n "dora-operator-api-types") (r "^0.3.2-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2-rc2") (d #t) (k 1)))) (h "18bs6g7ghmsfwlw370kaf86jznw22w3yjszg9i74np8db9jp86jd")))

(define-public crate-dora-operator-api-c-0.3.2 (c (n "dora-operator-api-c") (v "0.3.2") (d (list (d (n "dora-operator-api-types") (r "^0.3.2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2") (d #t) (k 1)))) (h "0zcp5czpjq6kgp795h91fxfsqjy6vbwjgajnvn70f3rbp2h5d83j")))

(define-public crate-dora-operator-api-c-0.3.3-rc1 (c (n "dora-operator-api-c") (v "0.3.3-rc1") (d (list (d (n "dora-operator-api-types") (r "^0.3.3-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.3-rc1") (d #t) (k 1)))) (h "090dzy3i5yv99zr1b8k97s71i9lrrmq6lsw9gj5ilyrli76szhd2")))

(define-public crate-dora-operator-api-c-0.3.3 (c (n "dora-operator-api-c") (v "0.3.3") (d (list (d (n "dora-operator-api-types") (r "^0.3.3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.3") (d #t) (k 1)))) (h "0l8f3bzmvc0z8g4svnlwx1rn2hk86dkwjc1f7nw6k2ar5hk7skfy")))

(define-public crate-dora-operator-api-c-0.3.4-rc1 (c (n "dora-operator-api-c") (v "0.3.4-rc1") (d (list (d (n "dora-operator-api-types") (r "^0.3.4-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4-rc1") (d #t) (k 1)))) (h "0zjy4px95wr57g5q5w81adldr4m2k9dzldx4dz86i8dnykh6b2jr")))

(define-public crate-dora-operator-api-c-0.3.4-rc2 (c (n "dora-operator-api-c") (v "0.3.4-rc2") (d (list (d (n "dora-operator-api-types") (r "^0.3.4-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4-rc2") (d #t) (k 1)))) (h "0h8rl626rrkap3y1sm0853m0dbkawdirs2lf8043xp9qjgvfjy21")))

(define-public crate-dora-operator-api-c-0.3.4 (c (n "dora-operator-api-c") (v "0.3.4") (d (list (d (n "dora-operator-api-types") (r "^0.3.4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4") (d #t) (k 1)))) (h "1rd7aail7ia7synbkzvjnkkpq1ncvpfccc584azp23pwbpk3yqs0")))

