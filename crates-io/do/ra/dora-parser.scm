(define-module (crates-io do ra dora-parser) #:use-module (crates-io))

(define-public crate-dora-parser-0.0.1 (c (n "dora-parser") (v "0.0.1") (d (list (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "1fgxg6ipsw8cb9cfdbpg73984vgs2fcr29wqycmbfnh6i33nfn98")))

(define-public crate-dora-parser-0.0.2 (c (n "dora-parser") (v "0.0.2") (d (list (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "0aj7xrbd8gaw5w6nga7nbf852sz9b8dn8dawbk69nyw08k7q1x3c")))

