(define-module (crates-io do ra dora-operator-api) #:use-module (crates-io))

(define-public crate-dora-operator-api-0.1.0 (c (n "dora-operator-api") (v "0.1.0") (h "11kngy4216n8fvn3l5dgnx15zyjgglxmpja1kgbs7bmsllj0ram6")))

(define-public crate-dora-operator-api-0.2.0-rc-7 (c (n "dora-operator-api") (v "0.2.0-rc-7") (d (list (d (n "dora-operator-api-macros") (r "^0.2.0-rc-7") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.0-rc-7") (d #t) (k 0)))) (h "0ndjmlr0zd8nkbq797br32zmyyk4lnqgq92qfw0ylxvdscbbmsl9")))

(define-public crate-dora-operator-api-0.2.0-rc-9 (c (n "dora-operator-api") (v "0.2.0-rc-9") (d (list (d (n "dora-operator-api-macros") (r "^0.2.0-rc-9") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.0-rc-9") (d #t) (k 0)))) (h "0m48l9s70ldbyig81n2znnbykwwpz0n8s5dwzb590alaxb6lyfgj")))

(define-public crate-dora-operator-api-0.2.0-rc-10 (c (n "dora-operator-api") (v "0.2.0-rc-10") (d (list (d (n "dora-operator-api-macros") (r "^0.2.0-rc-10") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.0-rc-10") (d #t) (k 0)))) (h "0car5z6m4a41haxbwndwhjb6zr6zszvp6x0n9lknhs3dmrzdg1if")))

(define-public crate-dora-operator-api-0.2.0-rc-11 (c (n "dora-operator-api") (v "0.2.0-rc-11") (d (list (d (n "dora-operator-api-macros") (r "^0.2.0-rc-11") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.0-rc-11") (d #t) (k 0)))) (h "12vvklkznq9wymxidn74a4ksn87ha33l6xvacabnb7c87rxs04zr")))

(define-public crate-dora-operator-api-0.2.1-rc (c (n "dora-operator-api") (v "0.2.1-rc") (d (list (d (n "dora-operator-api-macros") (r "^0.2.1-rc") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.1-rc") (d #t) (k 0)))) (h "0s0z62xbdpshh425jaz3kwrl8jgp1r9789sh67zyispr6praf6jp")))

(define-public crate-dora-operator-api-0.2.1 (c (n "dora-operator-api") (v "0.2.1") (d (list (d (n "dora-operator-api-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.1") (d #t) (k 0)))) (h "0lj11r9ynzp09zms183v0yv9dlxm4nvss443sjj340jg2a2vy5js")))

(define-public crate-dora-operator-api-0.2.2-rc (c (n "dora-operator-api") (v "0.2.2-rc") (d (list (d (n "dora-operator-api-macros") (r "^0.2.2-rc") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.2-rc") (d #t) (k 0)))) (h "0118wil67r7h66gwiw3v9h28sdqp4np5p2rcc54cpj8wyzi0vinq")))

(define-public crate-dora-operator-api-0.2.2-rc-2 (c (n "dora-operator-api") (v "0.2.2-rc-2") (d (list (d (n "dora-operator-api-macros") (r "^0.2.2-rc-2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.2-rc-2") (d #t) (k 0)))) (h "10ij25cgk0pv8jwxf7ijf5vk69z56dgpqnmbc295b0h2y4wnwfnd")))

(define-public crate-dora-operator-api-0.2.2 (c (n "dora-operator-api") (v "0.2.2") (d (list (d (n "dora-operator-api-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.2") (d #t) (k 0)))) (h "1jvyjmbzqrg3n8cfnxxhkpdg481zf2kb4pidamaryzmg7520d10k")))

(define-public crate-dora-operator-api-0.2.3-rc (c (n "dora-operator-api") (v "0.2.3-rc") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3-rc") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3-rc") (d #t) (k 0)))) (h "1pbgmry8q1kgr5cs8p2705p85ixqr47hvp5jhk6fmx4yd5xyj3bc")))

(define-public crate-dora-operator-api-0.2.3-rc2 (c (n "dora-operator-api") (v "0.2.3-rc2") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3-rc2") (d #t) (k 0)))) (h "0vwpwjk644n7valip85gggzd4g2zrp3a17gxxwrhgf4kifjf74gc")))

(define-public crate-dora-operator-api-0.2.3-rc4 (c (n "dora-operator-api") (v "0.2.3-rc4") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3-rc4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3-rc4") (d #t) (k 0)))) (h "0mj88q70h294fnjsmyj31cq7b7ms5955ad8c6s3f3xlkm41vaj4l")))

(define-public crate-dora-operator-api-0.2.3-rc5 (c (n "dora-operator-api") (v "0.2.3-rc5") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3-rc5") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3-rc5") (d #t) (k 0)))) (h "0qfrlxgkv2pf8588fjgk6wlhqcrkqxhlkyyhahycprzdwnjsk9yq")))

(define-public crate-dora-operator-api-0.2.3-rc6 (c (n "dora-operator-api") (v "0.2.3-rc6") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3-rc6") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3-rc6") (d #t) (k 0)))) (h "03013xq6n2rzawjr2bzh5gzzzy38vi388dzrp4bbf4l346dkwz8k")))

(define-public crate-dora-operator-api-0.2.3 (c (n "dora-operator-api") (v "0.2.3") (d (list (d (n "dora-operator-api-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.3") (d #t) (k 0)))) (h "1ijksw8mhjvm43xwsyja2vqf9hdhpsrbcrnmyrirdhjw2xdhp825")))

(define-public crate-dora-operator-api-0.2.4-rc3 (c (n "dora-operator-api") (v "0.2.4-rc3") (d (list (d (n "dora-operator-api-macros") (r "^0.2.4-rc3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.4-rc3") (d #t) (k 0)))) (h "1ibl1jbz2y9rpyqax69lszv4avyh3ba3f5rjxzs1ksa189g3nqm4")))

(define-public crate-dora-operator-api-0.2.4 (c (n "dora-operator-api") (v "0.2.4") (d (list (d (n "dora-operator-api-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.4") (d #t) (k 0)))) (h "0p25i5y61qyz9j1mwivn176hqil9ry7ki6fpxb1h0b0hzqh1jhjf")))

(define-public crate-dora-operator-api-0.2.5-alpha.1 (c (n "dora-operator-api") (v "0.2.5-alpha.1") (d (list (d (n "dora-operator-api-macros") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "13ac1ah4xjmvsyn4lski7xcwblmhrbv0angqwdq2d50rzq659prm")))

(define-public crate-dora-operator-api-0.2.5 (c (n "dora-operator-api") (v "0.2.5") (d (list (d (n "dora-operator-api-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.5") (d #t) (k 0)))) (h "16zvvqnkmsk6qasgqax0fxzb2l8njda5fk13lnnpf021hvzxrr9m")))

(define-public crate-dora-operator-api-0.2.6 (c (n "dora-operator-api") (v "0.2.6") (d (list (d (n "dora-operator-api-macros") (r "^0.2.6") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.2.6") (d #t) (k 0)))) (h "0gaijbl73kpkymxypifkw3la3kzqzwjcxi56lb8sn27199fkvi1f")))

(define-public crate-dora-operator-api-0.3.0-rc2 (c (n "dora-operator-api") (v "0.3.0-rc2") (d (list (d (n "dora-arrow-convert") (r "^0.3.0-rc2") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.0-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc2") (d #t) (k 0)))) (h "0hcb384dsyb47msh9pz1ag0907l0x1x7g4ik66vx3asv532aa49p")))

(define-public crate-dora-operator-api-0.3.0-rc3 (c (n "dora-operator-api") (v "0.3.0-rc3") (d (list (d (n "dora-arrow-convert") (r "^0.3.0-rc3") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.0-rc3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc3") (d #t) (k 0)))) (h "1gjdpmv0z5qi91mp5hinkg7712zl1bi8v6arcxwklin5j1gi6yw0")))

(define-public crate-dora-operator-api-0.3.0-rc4 (c (n "dora-operator-api") (v "0.3.0-rc4") (d (list (d (n "dora-arrow-convert") (r "^0.3.0-rc4") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.0-rc4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0-rc4") (d #t) (k 0)))) (h "1ci1w5rnc5n7v5b5kixb5fi742np6vrg6mlqp3xjx379jzr77q7s")))

(define-public crate-dora-operator-api-0.3.0 (c (n "dora-operator-api") (v "0.3.0") (d (list (d (n "dora-arrow-convert") (r "^0.3.0") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.0") (d #t) (k 0)))) (h "0cfvza1var12ki9rn21x6zphxpgvjscvi24ym9ggv1v4vcsi6fhz")))

(define-public crate-dora-operator-api-0.3.1-rc2 (c (n "dora-operator-api") (v "0.3.1-rc2") (d (list (d (n "dora-arrow-convert") (r "^0.3.1-rc2") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.1-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc2") (d #t) (k 0)))) (h "1r726azw5axljl3g0wsb6mwdlnrv5djncvcbvlnpwj8b43v8gqjc")))

(define-public crate-dora-operator-api-0.3.1-rc3 (c (n "dora-operator-api") (v "0.3.1-rc3") (d (list (d (n "dora-arrow-convert") (r "^0.3.1-rc3") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.1-rc3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc3") (d #t) (k 0)))) (h "01n720sa01rawv8i8qw4i53szfz90yviwvkw9kif2kac8ssyy7dm")))

(define-public crate-dora-operator-api-0.3.1-rc5 (c (n "dora-operator-api") (v "0.3.1-rc5") (d (list (d (n "dora-arrow-convert") (r "^0.3.1-rc5") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.1-rc5") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc5") (d #t) (k 0)))) (h "03r2b7b5nq334jyag95d5f5dmjk2ac0mjpfxml0i6d4an4fys5vb")))

(define-public crate-dora-operator-api-0.3.1-rc6 (c (n "dora-operator-api") (v "0.3.1-rc6") (d (list (d (n "dora-arrow-convert") (r "^0.3.1-rc6") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.1-rc6") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1-rc6") (d #t) (k 0)))) (h "0095ql9fmf733wvwdcx9zdpmk81lddy8yyhikaxkdhyzjw3r7d0h")))

(define-public crate-dora-operator-api-0.3.1 (c (n "dora-operator-api") (v "0.3.1") (d (list (d (n "dora-arrow-convert") (r "^0.3.1") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.1") (d #t) (k 0)))) (h "1kvzdc68qnrv8477jnf6an8a8xgadfdralsy11l1i1igv47a341m")))

(define-public crate-dora-operator-api-0.3.2-rc1 (c (n "dora-operator-api") (v "0.3.2-rc1") (d (list (d (n "dora-arrow-convert") (r "^0.3.2-rc1") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.2-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2-rc1") (d #t) (k 0)))) (h "0lcmws7nywwzzrbcn7np4lhfdgs3ljk1ycx6ynjm7b2x7hzj7jh6")))

(define-public crate-dora-operator-api-0.3.2-rc2 (c (n "dora-operator-api") (v "0.3.2-rc2") (d (list (d (n "dora-arrow-convert") (r "^0.3.2-rc2") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.2-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2-rc2") (d #t) (k 0)))) (h "0gjyhcarqs5lhd4qmay1scq5v072gzrapdssjvvykvaqldbk59n7")))

(define-public crate-dora-operator-api-0.3.2 (c (n "dora-operator-api") (v "0.3.2") (d (list (d (n "dora-arrow-convert") (r "^0.3.2") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.2") (d #t) (k 0)))) (h "08p1dq5ln3s9ri0x21zjiqq02mjbw2rahxr17zj4xm5n5mvlw5jx")))

(define-public crate-dora-operator-api-0.3.3-rc1 (c (n "dora-operator-api") (v "0.3.3-rc1") (d (list (d (n "dora-arrow-convert") (r "^0.3.3-rc1") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.3-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.3-rc1") (d #t) (k 0)))) (h "154r9swpzx12i7y6wjzkqyb35can3ma3225pcljp9qnaidbvza79")))

(define-public crate-dora-operator-api-0.3.3 (c (n "dora-operator-api") (v "0.3.3") (d (list (d (n "dora-arrow-convert") (r "^0.3.3") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.3") (d #t) (k 0)))) (h "1vj4x6d5lzymj3nnh25kvk6gfr9x35vrsfzhpd4n3w5y35r7f8hy")))

(define-public crate-dora-operator-api-0.3.4-rc1 (c (n "dora-operator-api") (v "0.3.4-rc1") (d (list (d (n "dora-arrow-convert") (r "^0.3.4-rc1") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.4-rc1") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4-rc1") (d #t) (k 0)))) (h "03r18ljnfshgs66p0kll23r16wxba098s7vm66k5sjxw42fzr7z4")))

(define-public crate-dora-operator-api-0.3.4-rc2 (c (n "dora-operator-api") (v "0.3.4-rc2") (d (list (d (n "dora-arrow-convert") (r "^0.3.4-rc2") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.4-rc2") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4-rc2") (d #t) (k 0)))) (h "0clmmp7kcc2i2fhlzf7lzhyamn4f4xma2n729495z9av8qnhrr5l")))

(define-public crate-dora-operator-api-0.3.4 (c (n "dora-operator-api") (v "0.3.4") (d (list (d (n "dora-arrow-convert") (r "^0.3.4") (d #t) (k 0)) (d (n "dora-operator-api-macros") (r "^0.3.4") (d #t) (k 0)) (d (n "dora-operator-api-types") (r "^0.3.4") (d #t) (k 0)))) (h "07fi3vv303cwm64gdzgccr848bdlbx974s62x8nbg6p0npbpapgj")))

