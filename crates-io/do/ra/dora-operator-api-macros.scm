(define-module (crates-io do ra dora-operator-api-macros) #:use-module (crates-io))

(define-public crate-dora-operator-api-macros-0.1.0 (c (n "dora-operator-api-macros") (v "0.1.0") (h "00jb4m8n8xm3z61v3j542jyvs21z1raz6l5nlvcf7kf54h3lcgzj")))

(define-public crate-dora-operator-api-macros-0.2.0-rc (c (n "dora-operator-api-macros") (v "0.2.0-rc") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "11hyfc9w1rj1ikl122fc8alnxj50px5fb8hmxg4scncg9rvazw2k")))

(define-public crate-dora-operator-api-macros-0.2.0-rc-7 (c (n "dora-operator-api-macros") (v "0.2.0-rc-7") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1hadk43092bn9z2bb0xi4z2ni2iazq7zf27mwl1d2plh4nij17s1")))

(define-public crate-dora-operator-api-macros-0.2.0-rc-9 (c (n "dora-operator-api-macros") (v "0.2.0-rc-9") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0y0qv147x1r88n3hwxmknzdg329qw2s8ns7d47vc00a5q3y3x3")))

(define-public crate-dora-operator-api-macros-0.2.0-rc-10 (c (n "dora-operator-api-macros") (v "0.2.0-rc-10") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "17mgn5n9dw2c9pz0nwciqjhvjh39404fza59jpvwkbvz7y638rxd")))

(define-public crate-dora-operator-api-macros-0.2.0-rc-11 (c (n "dora-operator-api-macros") (v "0.2.0-rc-11") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1zvjcn1nrh8d5wrdx88y3dh4fjxi691g4qba7ybkxc56fp620c6k")))

(define-public crate-dora-operator-api-macros-0.2.1-rc (c (n "dora-operator-api-macros") (v "0.2.1-rc") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1523996yahmpmr9b0cwjskwglbwcxh7600d9s0r9cc5dn4r77hgk")))

(define-public crate-dora-operator-api-macros-0.2.1 (c (n "dora-operator-api-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "16glychv6jrqcgs000gs450jac8izdw2f55imm1fjqwb7fljivc8")))

(define-public crate-dora-operator-api-macros-0.2.2-rc (c (n "dora-operator-api-macros") (v "0.2.2-rc") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjkyb3msfi69lzvqvdn8i7qzcbf1c6bbp2bzck99cxlayzz5598")))

(define-public crate-dora-operator-api-macros-0.2.2-rc-2 (c (n "dora-operator-api-macros") (v "0.2.2-rc-2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbr9c8gjc3k8rm76gqlj74cfll49agyvzr4g9jpx15q29ypzkz6")))

(define-public crate-dora-operator-api-macros-0.2.2 (c (n "dora-operator-api-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "01xxc6pzi9p9vhdlkhid4dpbm3l1l4azyav9ikghq3khj19ga1sw")))

(define-public crate-dora-operator-api-macros-0.2.3-rc (c (n "dora-operator-api-macros") (v "0.2.3-rc") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1hq2y4n1m4ym4ymf8airsbnxp79c80341fsqfkdywmiif5g3jfj9")))

(define-public crate-dora-operator-api-macros-0.2.3-rc2 (c (n "dora-operator-api-macros") (v "0.2.3-rc2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1r8y1nl1cxm9k1dg5ziiarx1rm0j5y5p6428f8dswfa4dr46biqr")))

(define-public crate-dora-operator-api-macros-0.2.3-rc4 (c (n "dora-operator-api-macros") (v "0.2.3-rc4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "07mbdyiy0vlpmbzhb3gb2gpm34p01msmwnlnmhm9wb8p7dsqp42s")))

(define-public crate-dora-operator-api-macros-0.2.3-rc5 (c (n "dora-operator-api-macros") (v "0.2.3-rc5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1wy14vqzvz6rldhwyx9xfpzw3h5gvkgxkaxvappkb568y4vwb6rx")))

(define-public crate-dora-operator-api-macros-0.2.3-rc6 (c (n "dora-operator-api-macros") (v "0.2.3-rc6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "16snc5pc0qivhigqjxyp66jr8a0gcrqi5fwdarkgvckxxqhnqj8x")))

(define-public crate-dora-operator-api-macros-0.2.3 (c (n "dora-operator-api-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhia4kqc29a4764q3zsgkx2lw4mmbajwszbf3qvcjpir6iixirh")))

(define-public crate-dora-operator-api-macros-0.2.4-rc3 (c (n "dora-operator-api-macros") (v "0.2.4-rc3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1zfwkl8avy3vdbjprw24mriy701frjg1hrw1d2a71631r3b3vjrs")))

(define-public crate-dora-operator-api-macros-0.2.4 (c (n "dora-operator-api-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0dc4mmm09da0lxfygmyrgkbiqkfvnn5n8a7dcxfgaxdsvxhnkd78")))

(define-public crate-dora-operator-api-macros-0.2.5-alpha.1 (c (n "dora-operator-api-macros") (v "0.2.5-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8b26jmlgwr00a7xyhz6zaiwwp727khgrv5z7wnqmz2f0v471dv")))

(define-public crate-dora-operator-api-macros-0.2.5 (c (n "dora-operator-api-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0ig8i4fx3xzzj3di9a5djg0q1vzfiqw8smsayrp6d69dkdiadb8n")))

(define-public crate-dora-operator-api-macros-0.2.6 (c (n "dora-operator-api-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "07yw0p9kyd4ag5cih078qh8bssgg17l5ij6wnmkp2293h3nd8jhk")))

(define-public crate-dora-operator-api-macros-0.3.0-rc (c (n "dora-operator-api-macros") (v "0.3.0-rc") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1ifgz46hdfci5vh1wrn89xi1hclmrap24i3xf6gxalmwal6q882v")))

(define-public crate-dora-operator-api-macros-0.3.0-rc2 (c (n "dora-operator-api-macros") (v "0.3.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0j9p4w2575skhhcjpvir80ly6nq7fqpaaa4jybb9xq5fwrkzfw0r")))

(define-public crate-dora-operator-api-macros-0.3.0-rc3 (c (n "dora-operator-api-macros") (v "0.3.0-rc3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1rib38s97clf80r7nasr1bw2fssxm0n2msbkkjglpdfv1gnqdlnc")))

(define-public crate-dora-operator-api-macros-0.3.0-rc4 (c (n "dora-operator-api-macros") (v "0.3.0-rc4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrgspgkm85j0zc896k9pawqz0mn8rpfrzi2flwwajxhkaqkg49y")))

(define-public crate-dora-operator-api-macros-0.3.0 (c (n "dora-operator-api-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0qhmdfgbd65g64rv79c5mfqdzq0zwl55lp5p02sl48l035mmyb0f")))

(define-public crate-dora-operator-api-macros-0.3.1-rc2 (c (n "dora-operator-api-macros") (v "0.3.1-rc2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1llms82n5g7dxfxm9hvy889br6adk5ysr113m7lbcvd01qb46644")))

(define-public crate-dora-operator-api-macros-0.3.1-rc3 (c (n "dora-operator-api-macros") (v "0.3.1-rc3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbnp1i2d99gbn88jlblxfxwxdggwhh8b0cmxwpbxwwx10qgsg1f")))

(define-public crate-dora-operator-api-macros-0.3.1-rc4 (c (n "dora-operator-api-macros") (v "0.3.1-rc4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1rw4x3imcgbbfph051j4ksjyk5p9lzasfx67nj3w6r1wbf5vdv4k")))

(define-public crate-dora-operator-api-macros-0.3.1-rc5 (c (n "dora-operator-api-macros") (v "0.3.1-rc5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "094xhmfjci2d48i7w3qg7dg4i0x8567bk3dw2w3m7ka0nr513ibx")))

(define-public crate-dora-operator-api-macros-0.3.1-rc6 (c (n "dora-operator-api-macros") (v "0.3.1-rc6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "17kyg7j9kljrziqr9rf7i62i3a7270s0znn67nb4adn11mxffj34")))

(define-public crate-dora-operator-api-macros-0.3.1 (c (n "dora-operator-api-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0y8zypy3ihdw654gyc9l5qyqyskd2h73v67laz65mjjnh0m61j97")))

(define-public crate-dora-operator-api-macros-0.3.2-rc1 (c (n "dora-operator-api-macros") (v "0.3.2-rc1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0xp4y60v7hhi0srf25drc61qqw7mz7kvxq5yyxh8p6gb3ixs9ssx")))

(define-public crate-dora-operator-api-macros-0.3.2-rc2 (c (n "dora-operator-api-macros") (v "0.3.2-rc2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8bps5x2pnd63xspzkblh14rjvxlj2gi0d7rgcphvfaiifyif60")))

(define-public crate-dora-operator-api-macros-0.3.2 (c (n "dora-operator-api-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "00yfw4slyb0dii00nvjvyzfwip1h2ghxpd4igmwv1bfmy3a55ssf")))

(define-public crate-dora-operator-api-macros-0.3.3-rc1 (c (n "dora-operator-api-macros") (v "0.3.3-rc1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0299ysiash2jisrc7xig40nl07bnhjqw0si2fsdfz6mmnpxwrn9c")))

(define-public crate-dora-operator-api-macros-0.3.3 (c (n "dora-operator-api-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1dbhsxbv52l6w07j1ry49wdbjjm7q50n2pm278rwqb7fpglrs91g")))

(define-public crate-dora-operator-api-macros-0.3.4-rc1 (c (n "dora-operator-api-macros") (v "0.3.4-rc1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "13m8zzdz2ci12886k0fbg3i9arl4rzpxq3glpbjyr41rmq17qg6y")))

(define-public crate-dora-operator-api-macros-0.3.4-rc2 (c (n "dora-operator-api-macros") (v "0.3.4-rc2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0jsk30mrxhm7zw4zx90fyrlkhdbdql5p6728nmr5pk1vc2m2bx1a")))

(define-public crate-dora-operator-api-macros-0.3.4 (c (n "dora-operator-api-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1p1sa7q9r0jq1ripcchy0ykw1zq98ya88flqx5mh8yzk1i8wp260")))

