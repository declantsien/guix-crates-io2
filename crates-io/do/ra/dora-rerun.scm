(define-module (crates-io do ra dora-rerun) #:use-module (crates-io))

(define-public crate-dora-rerun-0.3.4-rc1 (c (n "dora-rerun") (v "0.3.4-rc1") (d (list (d (n "dora-node-api") (r "^0.3.4-rc1") (f (quote ("tracing"))) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rerun") (r "^0.15.1") (f (quote ("web_viewer" "image"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "188c86rdchxli67sjn10s90lg3df5wrlk9cw8p4j4r6prmm5r6rn")))

(define-public crate-dora-rerun-0.3.4-rc2 (c (n "dora-rerun") (v "0.3.4-rc2") (d (list (d (n "dora-node-api") (r "^0.3.4-rc2") (f (quote ("tracing"))) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rerun") (r "^0.15.1") (f (quote ("web_viewer" "image"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0ckrn5h348d01md3l533fa1da7lsapbrq3dqbhk8s132q7336a92")))

(define-public crate-dora-rerun-0.3.4 (c (n "dora-rerun") (v "0.3.4") (d (list (d (n "dora-node-api") (r "^0.3.4") (f (quote ("tracing"))) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rerun") (r "^0.15.1") (f (quote ("web_viewer" "image"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1wpxryqnbzw61mnkqzn8fs15rzc2lk8f0kmhzhyxin49w16n5aqq")))

