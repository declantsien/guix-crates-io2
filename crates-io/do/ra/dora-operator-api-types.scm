(define-module (crates-io do ra dora-operator-api-types) #:use-module (crates-io))

(define-public crate-dora-operator-api-types-0.2.0-rc-7 (c (n "dora-operator-api-types") (v "0.2.0-rc-7") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "1lpqj8jwmbfaj8ijjsj5dky5dss537wfpsix4jh7z5vsipably4v")))

(define-public crate-dora-operator-api-types-0.2.0-rc-9 (c (n "dora-operator-api-types") (v "0.2.0-rc-9") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "1nmincwnvj0z59f7wjb99aqqdc3mbr8396hqpl31j3wbq8ags8h5")))

(define-public crate-dora-operator-api-types-0.2.0-rc-10 (c (n "dora-operator-api-types") (v "0.2.0-rc-10") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0pw003h1fiqm8cqh8kihg683pfzl5as3rl9jdng9q1baivmijwdc")))

(define-public crate-dora-operator-api-types-0.2.0-rc-11 (c (n "dora-operator-api-types") (v "0.2.0-rc-11") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0mh1vf4ay9q1ksh8hib0hjm16gg35fpdqkq1dykggwrvjws0afs4")))

(define-public crate-dora-operator-api-types-0.2.1-rc (c (n "dora-operator-api-types") (v "0.2.1-rc") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0w3vrvn6gpqpp8x35xi4y3j6la6xhcingjzfjpb6nxi5n8ykw2ky")))

(define-public crate-dora-operator-api-types-0.2.1 (c (n "dora-operator-api-types") (v "0.2.1") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0js5ni0y870mar3ik6wivrdd5q30y96z4fwfwk31w0mhva4wq09l")))

(define-public crate-dora-operator-api-types-0.2.2-rc (c (n "dora-operator-api-types") (v "0.2.2-rc") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "144s0h73cyhvc9xgf34a9mb5cjf7g9mnyfqmagyigslds1bmgjbv")))

(define-public crate-dora-operator-api-types-0.2.2-rc-2 (c (n "dora-operator-api-types") (v "0.2.2-rc-2") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "06cnp80kngfpvgxr4mcb20gzncafn8hdnr196as6mr548xgfmivv")))

(define-public crate-dora-operator-api-types-0.2.2 (c (n "dora-operator-api-types") (v "0.2.2") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "1vcg8snvl9h5fndzjx84yi8796cq276rzcxhsgcxlh4v808ljj0y")))

(define-public crate-dora-operator-api-types-0.2.3-rc (c (n "dora-operator-api-types") (v "0.2.3-rc") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0wwgxv0a775kvca44477q4v68c4p0wnh5f5x83qvlwklvykf4qsj")))

(define-public crate-dora-operator-api-types-0.2.3-rc2 (c (n "dora-operator-api-types") (v "0.2.3-rc2") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0djh8hbh1fmm9imdjqvgcaal39pfakzb5ij4mn1pg4f0xs0969l5")))

(define-public crate-dora-operator-api-types-0.2.3-rc4 (c (n "dora-operator-api-types") (v "0.2.3-rc4") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0iszfd0srw5ldchvicr79bnk77584bwihgfd4rillgvsx8nbcq53")))

(define-public crate-dora-operator-api-types-0.2.3-rc5 (c (n "dora-operator-api-types") (v "0.2.3-rc5") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "0gsd201dhy7jcgr6g1h5rmjrra71jssksmqzz4ym63z52agy6sbf")))

(define-public crate-dora-operator-api-types-0.2.3-rc6 (c (n "dora-operator-api-types") (v "0.2.3-rc6") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "1yb05gc07mbjrbl85y4ksrhdhkimb3ms08fp8k6n6ipv7khlwv2i")))

(define-public crate-dora-operator-api-types-0.2.3 (c (n "dora-operator-api-types") (v "0.2.3") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("proc_macros" "headers"))) (d #t) (k 0)))) (h "13v9224lxlfvra8lq3s3km6pbc1a3567naxiha96aql0aaqd99q3")))

(define-public crate-dora-operator-api-types-0.2.4-rc3 (c (n "dora-operator-api-types") (v "0.2.4-rc3") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("headers"))) (d #t) (k 0)))) (h "0w14nk4pmn5f2v3ykmfba6i5bnql00qjpvnycmccv54cavdwsga1")))

(define-public crate-dora-operator-api-types-0.2.4 (c (n "dora-operator-api-types") (v "0.2.4") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("headers"))) (d #t) (k 0)))) (h "1r5jzb1bs403w8hlk6ap36g0w648ld41mznvssxhf35r5ksjywzl")))

(define-public crate-dora-operator-api-types-0.2.5-alpha.1 (c (n "dora-operator-api-types") (v "0.2.5-alpha.1") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("headers"))) (d #t) (k 0)))) (h "1cg3g10ihh6k3cc32klvkbzslfvvxm2xfq5d13rw8a6hbifycrn7")))

(define-public crate-dora-operator-api-types-0.2.5 (c (n "dora-operator-api-types") (v "0.2.5") (d (list (d (n "safer-ffi") (r "^0.1.0-rc1") (f (quote ("headers"))) (d #t) (k 0)))) (h "03fgnzqhhzs3cgsi4nvpwl6nzb9xr3z7kdvr6g9v87di82q58c2g")))

(define-public crate-dora-operator-api-types-0.2.6 (c (n "dora-operator-api-types") (v "0.2.6") (d (list (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0q892mwzb7g4kdvdihqjfbxpxlmkv21fql68rn9fbh5gy0gxkwrn")))

(define-public crate-dora-operator-api-types-0.3.0-rc2 (c (n "dora-operator-api-types") (v "0.3.0-rc2") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.0-rc2") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0jix10vdg6r63k2dkczmkxdyd39zx17v4xl4wjdy4rr0x3izwy4a")))

(define-public crate-dora-operator-api-types-0.3.0-rc3 (c (n "dora-operator-api-types") (v "0.3.0-rc3") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.0-rc3") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "153nb0yhxdci160lnmplqmj0scp7v4506487lj5cvs3zc7f7almz")))

(define-public crate-dora-operator-api-types-0.3.0-rc4 (c (n "dora-operator-api-types") (v "0.3.0-rc4") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.0-rc4") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0c00nwgyy4sgx6fas691gr19dc4r55z0hs9fx59ss72s2f1ak9av")))

(define-public crate-dora-operator-api-types-0.3.0 (c (n "dora-operator-api-types") (v "0.3.0") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.0") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1qmg3wf5cmmqgkmmrkdjwjgdr55rsqg3wbdnb68vq4d7rgyn72qm")))

(define-public crate-dora-operator-api-types-0.3.1-rc2 (c (n "dora-operator-api-types") (v "0.3.1-rc2") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.1-rc2") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1j4gfs7qgb1xafzxaxdyxgjy2ac5rb73c74kcwd47lxnpk36ssgn")))

(define-public crate-dora-operator-api-types-0.3.1-rc3 (c (n "dora-operator-api-types") (v "0.3.1-rc3") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.1-rc3") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1a3kzshf5s6nf61jk0x9pwfkxid394bxmy7jgw4wfbx1pfgga2b5")))

(define-public crate-dora-operator-api-types-0.3.1-rc5 (c (n "dora-operator-api-types") (v "0.3.1-rc5") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.1-rc5") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.3") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1yb5y9mwykgpqymbafbifrjpw9q2pyv9kpcvs4pn4p8i0kdrly6a")))

(define-public crate-dora-operator-api-types-0.3.1-rc6 (c (n "dora-operator-api-types") (v "0.3.1-rc6") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.1-rc6") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0k9daq2wja9a5nfny0bp6aqdh91dr0bcssgn5ddjrfg902izjjgi")))

(define-public crate-dora-operator-api-types-0.3.1 (c (n "dora-operator-api-types") (v "0.3.1") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.1") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1ncjj3x3vyvmzdycwwyzbx6vhjnzj8zd62s6vs7qk643sph7f8w4")))

(define-public crate-dora-operator-api-types-0.3.2-rc1 (c (n "dora-operator-api-types") (v "0.3.2-rc1") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.2-rc1") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "13yxi47lvkaq9sdrrq3fz7xmdk2jw0cb349ykm3frfd50drwqhll")))

(define-public crate-dora-operator-api-types-0.3.2-rc2 (c (n "dora-operator-api-types") (v "0.3.2-rc2") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.2-rc2") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "09x6i6r3z59fj1z9r2qhrxx7vlr36hrhbnvyg4lc301mjf4w0pxr")))

(define-public crate-dora-operator-api-types-0.3.2 (c (n "dora-operator-api-types") (v "0.3.2") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.2") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "07h2chp26wjf9svghmby2dgqzdp7yxg7wrckb7f0n5mvq02hx742")))

(define-public crate-dora-operator-api-types-0.3.3-rc1 (c (n "dora-operator-api-types") (v "0.3.3-rc1") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.3-rc1") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "19g3sih87lhsy28nalhamsc85qp4djvpkzbdl9adqq80lpm46y6i")))

(define-public crate-dora-operator-api-types-0.3.3 (c (n "dora-operator-api-types") (v "0.3.3") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.3") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1zj4ywyczxz2cadsr3wyxs7knsvy4agm4aiay1l8c8np38dbfbf9")))

(define-public crate-dora-operator-api-types-0.3.4-rc1 (c (n "dora-operator-api-types") (v "0.3.4-rc1") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.4-rc1") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0z7lbyzdc8kpzjva9scmmg9fqq2d8xxlpzjdfa0jwvp1cgxqq85i")))

(define-public crate-dora-operator-api-types-0.3.4-rc2 (c (n "dora-operator-api-types") (v "0.3.4-rc2") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.4-rc2") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "0q0kqqkwwjsa7kpq45z2khk9mdp73vh5wlh3izgav49d3aprlahs")))

(define-public crate-dora-operator-api-types-0.3.4 (c (n "dora-operator-api-types") (v "0.3.4") (d (list (d (n "arrow") (r "^48.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "dora-arrow-convert") (r "^0.3.4") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.4") (f (quote ("headers" "inventory-0-3-1"))) (d #t) (k 0)))) (h "1k5qbqy42dbz1qipafqzr1qd9npqzq10kxa0xkfl4c1n44dd71yv")))

