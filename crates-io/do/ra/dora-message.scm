(define-module (crates-io do ra dora-message) #:use-module (crates-io))

(define-public crate-dora-message-0.2.0-rc (c (n "dora-message") (v "0.2.0-rc") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "14mbww50f0v06jdf84drqrcaw6jfzda5y7i4jsf4lqzg551gggaz")))

(define-public crate-dora-message-0.2.0-rc-2 (c (n "dora-message") (v "0.2.0-rc-2") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0mf2s8gsk2q0ld49spl3q4d1r4yn5knnq5km42r87f8dyk7pzwsg")))

(define-public crate-dora-message-0.2.0-rc-3 (c (n "dora-message") (v "0.2.0-rc-3") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0pbsa507w59ff478axgmcj1z1plnfyhv1q3fmgv7v5adknidwdd6")))

(define-public crate-dora-message-0.2.0-rc-4 (c (n "dora-message") (v "0.2.0-rc-4") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "052h5vfzv3gn7ps8wa3r5cpycfx2hsfv6ghxklmrb7ivpvww75fx")))

(define-public crate-dora-message-0.2.0-rc-5 (c (n "dora-message") (v "0.2.0-rc-5") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1m2j7v1jwcy33hy3b8rpdbxx167y802q08njbg435niqrkwmkl8w")))

(define-public crate-dora-message-0.2.0-rc-6 (c (n "dora-message") (v "0.2.0-rc-6") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1gsbmwpmb14hv5jnzibscijwhs6nxcqscp4gxrm1cnplk2ifzf7l")))

(define-public crate-dora-message-0.2.0-rc-7 (c (n "dora-message") (v "0.2.0-rc-7") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "09y3snbxrb78mr3cjb7hsscdqw18wfz6np3a180n6rc8m38k6qj2")))

(define-public crate-dora-message-0.2.0-rc-8 (c (n "dora-message") (v "0.2.0-rc-8") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0ny3hkgzk9lrvnqq4jvpnbr9xrj2lsrb08xxaf42kzzr7lqx1fsi")))

(define-public crate-dora-message-0.2.0-rc-9 (c (n "dora-message") (v "0.2.0-rc-9") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1hnnk7rfj0yzc6svvav26636df9jkxc7imskxlmwhr0s9m7l5cxz")))

(define-public crate-dora-message-0.2.0-rc-10 (c (n "dora-message") (v "0.2.0-rc-10") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1h430v85612qql6x1pnxzlgp02g10viay4mpyp2bzb8xz9286c1z")))

(define-public crate-dora-message-0.2.0-rc-11 (c (n "dora-message") (v "0.2.0-rc-11") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0rdyrd9wsicjmhd0m16sl3mj4gw4ipy2j0szvf2k5q8v7al3lphm")))

(define-public crate-dora-message-0.2.1-rc (c (n "dora-message") (v "0.2.1-rc") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "04x5iw1205xrnwmyrv2vhlzqv4s399ij1fmy2phpwnfb25rh5ns3")))

(define-public crate-dora-message-0.2.1 (c (n "dora-message") (v "0.2.1") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0ab77fdi1v19isqvr2h0913chr0djvwxpkc46fw2030l1s1qqbsr")))

(define-public crate-dora-message-0.2.2-rc (c (n "dora-message") (v "0.2.2-rc") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0mcvria8f9dmwbwcyb3m4a69gjkhsyrqv0fxqbn6fj0bphrzyjhk")))

(define-public crate-dora-message-0.2.2-rc-2 (c (n "dora-message") (v "0.2.2-rc-2") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0nczzbip9d7l4n6ic9mch9vgx5wdmp6lsy90n2w6bj9jfiqnsn3r")))

(define-public crate-dora-message-0.2.2 (c (n "dora-message") (v "0.2.2") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1vwyh7prcrmlj6qc2qh7363521pblp1la0jmfv3zm8hhlly3jzkf")))

(define-public crate-dora-message-0.2.3-rc (c (n "dora-message") (v "0.2.3-rc") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0sca4mqvqn0sgl1r0hfcgz8azfqc8yzvlfqqzkfslfiyn5ws9kxm")))

(define-public crate-dora-message-0.2.3-rc2 (c (n "dora-message") (v "0.2.3-rc2") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "11bpxfj5ss1ml6311pnpx5618rln460bg1nyhxhxl668fvvvi7d5")))

(define-public crate-dora-message-0.2.3-rc4 (c (n "dora-message") (v "0.2.3-rc4") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0hh4g7gjxpg7bha4s5y6rlkv91yz8mfg0gy3a4j5546gp98inmd0")))

(define-public crate-dora-message-0.2.3-rc5 (c (n "dora-message") (v "0.2.3-rc5") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1kbq5rc2prjb0r14h51kjifjvsifmx8kbf8mig382f5mpshkm42l")))

(define-public crate-dora-message-0.2.3-rc6 (c (n "dora-message") (v "0.2.3-rc6") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1h90081fii8nvnxwzypyzsbdg4xj5fzqk3ry9mj5lcpcy5gcl6cc")))

(define-public crate-dora-message-0.2.3 (c (n "dora-message") (v "0.2.3") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "035w8a6vxzif1hgg7hgyb8lgmaxx4mkr9p605ayyqzy05sj07p4c")))

(define-public crate-dora-message-0.2.4-rc3 (c (n "dora-message") (v "0.2.4-rc3") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0m7hzgfc8fb1zqsqr0rj85zzxavi49vmaafn5zv6kk6rhhvs396r")))

(define-public crate-dora-message-0.2.4 (c (n "dora-message") (v "0.2.4") (d (list (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "03il0cwwb7i19m7y8vibfhngh1d214bnzwgb5cqakn75pcywkvby")))

(define-public crate-dora-message-0.2.5-alpha.1 (c (n "dora-message") (v "0.2.5-alpha.1") (d (list (d (n "arrow-schema") (r "^45.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.14.11") (f (quote ("unaligned"))) (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1l5a2ylnkc1h8lx43k076sfy9zlgagd8mn2d1w7zbp25sb1pk01a")))

(define-public crate-dora-message-0.2.5 (c (n "dora-message") (v "0.2.5") (d (list (d (n "arrow-data") (r "^45.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^45.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "04v3li42fn6shmfx4swi20w8dw1pj0y7xr3vzwv1fdmw5ibc4fmc")))

(define-public crate-dora-message-0.2.6 (c (n "dora-message") (v "0.2.6") (d (list (d (n "arrow-data") (r "^45.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^45.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0z5a5j6dskfxzpcj9v6vdb7ka88p4khraf99lgkpxjg6lr803ji0")))

(define-public crate-dora-message-0.3.0-rc (c (n "dora-message") (v "0.3.0-rc") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "06kb9fm67hqk3bwhdg2svg89kqazdmpijz6rkzzsvln3njljpr02")))

(define-public crate-dora-message-0.3.0-rc2 (c (n "dora-message") (v "0.3.0-rc2") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "074m395b3hsrb7hr8ipqcfjx81z9mhl5hkyfffghmdlp1800h3k3")))

(define-public crate-dora-message-0.3.0-rc3 (c (n "dora-message") (v "0.3.0-rc3") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0imvqj1fqjchflz9kic7dkpf14hk5x8bpffg8rgqacpvicfa78k2")))

(define-public crate-dora-message-0.3.0-rc4 (c (n "dora-message") (v "0.3.0-rc4") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "13jnvz5695hdwdw6529g7nhxkz6ng5w48lcszry7jdy19l2bnihv")))

(define-public crate-dora-message-0.3.0 (c (n "dora-message") (v "0.3.0") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "12vm1bqvgjfcg1h6lxm4d7fxk4qsbi2acgprr239dllbpbh48s9s")))

(define-public crate-dora-message-0.3.1-rc2 (c (n "dora-message") (v "0.3.1-rc2") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0bdlm5bicmfgzjgy52msj6vjqn95h0w3cw1al7k5d19zrlbxnw0i")))

(define-public crate-dora-message-0.3.1-rc3 (c (n "dora-message") (v "0.3.1-rc3") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1xwccxms7fgbfgqhc0p8if2davawhzsgbyawp9lwbv8yzii8p8px")))

(define-public crate-dora-message-0.3.1-rc4 (c (n "dora-message") (v "0.3.1-rc4") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "13fkn634sh59rk4vk3lq9zg7ivs91r0hmz1xn0aj9z1yccy1wy2h")))

(define-public crate-dora-message-0.3.1-rc5 (c (n "dora-message") (v "0.3.1-rc5") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1n752vkx0pdwq8iwhir7n5mdj4s8pvnkypx1aqfphik4npjny5wz")))

(define-public crate-dora-message-0.3.1-rc6 (c (n "dora-message") (v "0.3.1-rc6") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1zb8npq2amasg9lbsfbifrrjqgs3dghlq4y3i4mvpnn6lrdr345d")))

(define-public crate-dora-message-0.3.1 (c (n "dora-message") (v "0.3.1") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1zajb1kvf2pikm21jgpqyp0yimsq2nyfwq62nybsii5jjw8x89g9")))

(define-public crate-dora-message-0.3.2-rc1 (c (n "dora-message") (v "0.3.2-rc1") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0f4833h2zay1372gy0lary9ns1vl537ckbl97sqgwiqbs0maks6h")))

(define-public crate-dora-message-0.3.2-rc2 (c (n "dora-message") (v "0.3.2-rc2") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "02ibh626c3qw0m26sziwd98q5bbsa9hv0cc8w83qgmw3h5mwvzg0")))

(define-public crate-dora-message-0.3.2 (c (n "dora-message") (v "0.3.2") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0c8g2wb413y9gwa84qwnnsj0hi84ifk2y48vm47hzrmg35r0f82b")))

(define-public crate-dora-message-0.3.3-rc1 (c (n "dora-message") (v "0.3.3-rc1") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0wmhna486yq9cnr1byp5wr4b64sq20jbzpis66z43j2hbb5f5b5c")))

(define-public crate-dora-message-0.3.3 (c (n "dora-message") (v "0.3.3") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0jr0p6bmymprgz4335n45jnswm45ng22man03dylriyffj79z5p6")))

(define-public crate-dora-message-0.3.4-rc1 (c (n "dora-message") (v "0.3.4-rc1") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "141ynhg7d4m7jnrzx57v8bkh1zbhlfrh3nflszx6f8d7y2r7ggba")))

(define-public crate-dora-message-0.3.4-rc2 (c (n "dora-message") (v "0.3.4-rc2") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "1r7z2k9aqjc1rn90vsna6fiv84zqd32j60y8znfcid56llqdly5j")))

(define-public crate-dora-message-0.3.4 (c (n "dora-message") (v "0.3.4") (d (list (d (n "arrow-data") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uhlc") (r "^0.5.1") (d #t) (k 0)))) (h "0v8hb9sb7b76qk0q15nc4vr40bdq27rj7d0kjkm0qir0rninjkya")))

