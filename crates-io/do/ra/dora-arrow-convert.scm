(define-module (crates-io do ra dora-arrow-convert) #:use-module (crates-io))

(define-public crate-dora-arrow-convert-0.3.0-rc2 (c (n "dora-arrow-convert") (v "0.3.0-rc2") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "11nrx06mz1pjskvba6sny452g9ykawmznir4frdkabvl8bsq3ka8")))

(define-public crate-dora-arrow-convert-0.3.0-rc3 (c (n "dora-arrow-convert") (v "0.3.0-rc3") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "05p6j0spsjj4gxv43jfxi2yyhvfkrnq0yqmr7rcg85hjaala158s")))

(define-public crate-dora-arrow-convert-0.3.0-rc4 (c (n "dora-arrow-convert") (v "0.3.0-rc4") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1a37xlzj27xi6yxl090fr8875lgi2033dgicbyd1jf8hkr5gaspl")))

(define-public crate-dora-arrow-convert-0.3.0 (c (n "dora-arrow-convert") (v "0.3.0") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1y0vamv2c8s4xz59p7wag3yd2nz56rhjl5wyfhj917dgysj597ja")))

(define-public crate-dora-arrow-convert-0.3.1-rc2 (c (n "dora-arrow-convert") (v "0.3.1-rc2") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1ncnaw3f8yrrcybyl2alqdv065cx4k9531gdfln07bci2b5l7di7")))

(define-public crate-dora-arrow-convert-0.3.1-rc3 (c (n "dora-arrow-convert") (v "0.3.1-rc3") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1d6sb70paahay68626iy2aaspnarkv9xvqrbbppwm9pp1lyy28ap")))

(define-public crate-dora-arrow-convert-0.3.1-rc4 (c (n "dora-arrow-convert") (v "0.3.1-rc4") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0qdvfl0362az72kc6jvsdz3i7ansk1wnbbk6qbhx8xy5a3i54jha")))

(define-public crate-dora-arrow-convert-0.3.1-rc5 (c (n "dora-arrow-convert") (v "0.3.1-rc5") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0r8zmzcwqcizh2dx5m68msssp6kd44si0c26wrj5vgm0i3l4jxlm")))

(define-public crate-dora-arrow-convert-0.3.1-rc6 (c (n "dora-arrow-convert") (v "0.3.1-rc6") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1xa6nwp387g01wdjx7hscah4qrvrr87c9c2mj5iqzpw2as4cbiy2")))

(define-public crate-dora-arrow-convert-0.3.1 (c (n "dora-arrow-convert") (v "0.3.1") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1h08rgbfb865917qggssgvsisas57gpa51bmr7432y1h2isyh8s7")))

(define-public crate-dora-arrow-convert-0.3.2-rc1 (c (n "dora-arrow-convert") (v "0.3.2-rc1") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0lys4x3pyr35j4kymbk3cdlxp7s329gbs9crq7bk4whvkws516gi")))

(define-public crate-dora-arrow-convert-0.3.2-rc2 (c (n "dora-arrow-convert") (v "0.3.2-rc2") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0csx4hh1p15k9qak6ff99fp2c164js49wm34b6kl59a70px38zsp")))

(define-public crate-dora-arrow-convert-0.3.2 (c (n "dora-arrow-convert") (v "0.3.2") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0289cv8z5n6f5jf22qnbksxhdzqhhr7pjz0hf63sfc1pib43nmbd")))

(define-public crate-dora-arrow-convert-0.3.3-rc1 (c (n "dora-arrow-convert") (v "0.3.3-rc1") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0ycncfkws9smnywnv1r68qa0kiv2gg35py2m2qvn47wp8glki1rv")))

(define-public crate-dora-arrow-convert-0.3.3 (c (n "dora-arrow-convert") (v "0.3.3") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0xa1c0vspmlg9k6di0m5x9adsfmnfjzqfnmrr7qiv7rcjp5asnpn")))

(define-public crate-dora-arrow-convert-0.3.4-rc1 (c (n "dora-arrow-convert") (v "0.3.4-rc1") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1hlgq3k5ikhjlwnsr9ii2cr75p0r6wya5s5qv6bj1i8v992vzh5p")))

(define-public crate-dora-arrow-convert-0.3.4-rc2 (c (n "dora-arrow-convert") (v "0.3.4-rc2") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "12cqx4ysj3ic3xfc82a31i5y0019ici4z7m8yssmpg2l2627mpvv")))

(define-public crate-dora-arrow-convert-0.3.4 (c (n "dora-arrow-convert") (v "0.3.4") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1y9k5x249c1aw3dl8vvv64hwzg2cy1lndj9xdbfijaaa0kq781r4")))

