(define-module (crates-io do ra dora-download) #:use-module (crates-io))

(define-public crate-dora-download-0.2.0-rc-5 (c (n "dora-download") (v "0.2.0-rc-5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1p9l1rfmn0j87xp0xlpb13jgvp4jlsz3n5zvppn5k7wava6zh2sk")))

(define-public crate-dora-download-0.2.0-rc-6 (c (n "dora-download") (v "0.2.0-rc-6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1f0mvghk49n6fh1xbjs67ac6dnq3pmrs443fdwsw3ckqdhbrpfds")))

(define-public crate-dora-download-0.2.0-rc-7 (c (n "dora-download") (v "0.2.0-rc-7") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "068slbxy7naxqk9wwpbislhqa8s87d8cppw1w1hl8y1rivw67xm2")))

(define-public crate-dora-download-0.2.0-rc-8 (c (n "dora-download") (v "0.2.0-rc-8") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "17ijbpgii1swm3s1wwhykmcdshk4ggviqrjyp38nvdl562zjhzfv")))

(define-public crate-dora-download-0.2.0-rc-9 (c (n "dora-download") (v "0.2.0-rc-9") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1d3n3v0f4zn713nphqp7qz80hj3g3bba0x3kmyw6lanvqhr4ll4a")))

(define-public crate-dora-download-0.2.0-rc-10 (c (n "dora-download") (v "0.2.0-rc-10") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1mbwkq8i9jm41avxrphpzrja88qpd3q01cxbijxaam90zwn09qy3")))

(define-public crate-dora-download-0.2.0-rc-11 (c (n "dora-download") (v "0.2.0-rc-11") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0lxgdgqi3h4v9hd8cdjfyn5mc3g2dq22ac22914syx7d7499f6pc")))

(define-public crate-dora-download-0.2.1-rc (c (n "dora-download") (v "0.2.1-rc") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0ac6pa1p8h5hdlnyn4s1h49458gam44njnw2n13nq7b3n08sgmbv")))

(define-public crate-dora-download-0.2.1 (c (n "dora-download") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1sfz98wjl09vqc7ii0k0h4x1ni7my5q2mq6zzm9lrslav9agmqlp")))

(define-public crate-dora-download-0.2.2-rc (c (n "dora-download") (v "0.2.2-rc") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1fhqkvs3anpns8yqiylcpzckv5y3vg6ij8v2ykpzhfw6khkqqf61")))

(define-public crate-dora-download-0.2.2-rc-2 (c (n "dora-download") (v "0.2.2-rc-2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0ka9llpajspz06zavbbzfwrhl7vadfjrngkrnnv19xyiy5xsq4a1")))

(define-public crate-dora-download-0.2.2 (c (n "dora-download") (v "0.2.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "087y4w95mp3ayqhg1b71bydnq64vw9d38lc4cala2fzqw3g45q63")))

(define-public crate-dora-download-0.2.3-rc (c (n "dora-download") (v "0.2.3-rc") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "09w1d8nwqpm7vc46ik129kr8fz4f5940xq5vli17d8zv4z78w2gr")))

(define-public crate-dora-download-0.2.3-rc2 (c (n "dora-download") (v "0.2.3-rc2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0gfypi6c1v27x7v4qj9prxn0b4dw1png81a8gqm8wk08ccgar6z6")))

(define-public crate-dora-download-0.2.3-rc4 (c (n "dora-download") (v "0.2.3-rc4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "01prmgj3nyvavm73j7x36gg0d6hyfa48nv2y5fv6lm1mljv172c6")))

(define-public crate-dora-download-0.2.3-rc5 (c (n "dora-download") (v "0.2.3-rc5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0ndblj3sz7srbpml1sh3zaikg7a3s8aqc7gl3jmw9x9gjv8qf92x")))

(define-public crate-dora-download-0.2.3-rc6 (c (n "dora-download") (v "0.2.3-rc6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "08l01snk9jbdb379h2hg2j89d3gq905rfc7sm9zfzgb4p0m9bshg")))

(define-public crate-dora-download-0.2.3 (c (n "dora-download") (v "0.2.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0mz7sgn069w4mhnjfdljvv42bdriwlg5d03n3dz6rp2pyz7z13k2")))

(define-public crate-dora-download-0.2.4-rc3 (c (n "dora-download") (v "0.2.4-rc3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1y008alhdk7x31q4wsxx1qrydffnjzhwz1snihk777sd3f3a83wi")))

(define-public crate-dora-download-0.2.4 (c (n "dora-download") (v "0.2.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1z4zhc0q6ab4wprbccar127bklcm10c6ws74gzskdghd254r2qdi")))

(define-public crate-dora-download-0.2.5-alpha.1 (c (n "dora-download") (v "0.2.5-alpha.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1mbha7mdm13qjgbpg113fc8p4mn5a1h793l44v4j0lahz71ay9g3")))

(define-public crate-dora-download-0.2.5 (c (n "dora-download") (v "0.2.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0dlry2kdiv9kzkkvkfg9p5xdzwh5aayc045n9dl7xcf700kgrc7m")))

(define-public crate-dora-download-0.2.6 (c (n "dora-download") (v "0.2.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "07p2y0mfpaiwbw600s552zl4qa6hd7rnwd62mckc1my9spldz8qy")))

(define-public crate-dora-download-0.3.0-rc (c (n "dora-download") (v "0.3.0-rc") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1ipyszyhzf3r46n8jakps146lcvw0v8p92anf57q4i3c8c9vvwg0")))

(define-public crate-dora-download-0.3.0-rc2 (c (n "dora-download") (v "0.3.0-rc2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0nad8flda4nfaggpkshc2qqp39wd7ryn63pzs520k0v3v9zdjqh2")))

(define-public crate-dora-download-0.3.0-rc3 (c (n "dora-download") (v "0.3.0-rc3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0z7vbdvffmkn8lqwvg2q0q3srwjvcbc9vhb8la7cwd89gni4962i")))

(define-public crate-dora-download-0.3.0-rc4 (c (n "dora-download") (v "0.3.0-rc4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0fqb227a2n5lnnm58jjpgb3424cv63g2b4sr7mdwpimzcqccjp7n")))

(define-public crate-dora-download-0.3.0 (c (n "dora-download") (v "0.3.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0fj2xw701m43vqrhg0lqmv1rnlhmi4y16957a65xcvdszc2hh2fk")))

(define-public crate-dora-download-0.3.1-rc2 (c (n "dora-download") (v "0.3.1-rc2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "08pji6qhg3d78dwl0pci9x6r1g9dzda3icyw21sq5nv985kjjk4c")))

(define-public crate-dora-download-0.3.1-rc3 (c (n "dora-download") (v "0.3.1-rc3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0rbmjp57afk5j1j29243mxq5ibxy8qz59p60ww5x7rkhn2kkg3ky")))

(define-public crate-dora-download-0.3.1-rc4 (c (n "dora-download") (v "0.3.1-rc4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "16bs9vi44xhj9gr4d4yjfyfbs9vj6mhydj6f3swfk09ldx9p958a")))

(define-public crate-dora-download-0.3.1-rc5 (c (n "dora-download") (v "0.3.1-rc5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "01il1qsfgmpigl937mwz8q4ijjm6n4mahn55d351n4kms78a35q6")))

(define-public crate-dora-download-0.3.1-rc6 (c (n "dora-download") (v "0.3.1-rc6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1ndliij46lr193wvwiyqd1y5lf3yjdwv93my33hrb2620m1h527r")))

(define-public crate-dora-download-0.3.1 (c (n "dora-download") (v "0.3.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0vkkskchakzp95ajhnk25sh1pj8zhmd3b4vp8v5l477lxllkqsi4")))

(define-public crate-dora-download-0.3.2-rc1 (c (n "dora-download") (v "0.3.2-rc1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "085y1yfmw4nn5kcz3qldrrkbsgjx8d0pd398gdji3qxp1hddb8v5")))

(define-public crate-dora-download-0.3.2-rc2 (c (n "dora-download") (v "0.3.2-rc2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1pnscypyblnvfi5s1gfzyv89b2hkzkxbyp166di6d1c07x1bajqc")))

(define-public crate-dora-download-0.3.2 (c (n "dora-download") (v "0.3.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0cq3jaqi3s0sv717qgk2nh60i6l3lk8z7csykln3rncx3x1lwgfs")))

(define-public crate-dora-download-0.3.3-rc1 (c (n "dora-download") (v "0.3.3-rc1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0sf9lqjixvd7j554ppjy4dr83rcsxwd3bb1mmsc9y6lxlihc7lbg")))

(define-public crate-dora-download-0.3.3 (c (n "dora-download") (v "0.3.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1by007gz619q1kmyadnhxq1ih5ka60g0c82k22c8n60bckb241m6")))

(define-public crate-dora-download-0.3.4-rc1 (c (n "dora-download") (v "0.3.4-rc1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1d71h64ppyssh8r502pf5ib175wqvkph65dhmv4brj8crirn6wap")))

(define-public crate-dora-download-0.3.4-rc2 (c (n "dora-download") (v "0.3.4-rc2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0r0bf2dnmgkzkcpv9l10qrma6pcq9ir3b3lm1r880qiml3b4bhj5")))

(define-public crate-dora-download-0.3.4 (c (n "dora-download") (v "0.3.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "080csyrh24ipfap1ymrd3gh8dhf3i168672i6ivdiyym16mg7hkg")))

