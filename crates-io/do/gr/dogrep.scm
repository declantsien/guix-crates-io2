(define-module (crates-io do gr dogrep) #:use-module (crates-io))

(define-public crate-dogrep-0.1.0 (c (n "dogrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "08dv52gbkblc1gpy1a6kqdqg11d6bwqn2l6nv3465ravq9649cda")))

