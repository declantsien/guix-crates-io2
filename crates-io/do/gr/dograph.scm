(define-module (crates-io do gr dograph) #:use-module (crates-io))

(define-public crate-dograph-0.8.3 (c (n "dograph") (v "0.8.3") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1niimnjsy5fkih5cmkrflp6wvapk6p55pnl1m8ck5m8gqnsliwr9")))

(define-public crate-dograph-0.8.4 (c (n "dograph") (v "0.8.4") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1rxl6vb2v5l4mykgvwrhh79xxxz3ra86ha86h9619dyc6bfyv2wi")))

