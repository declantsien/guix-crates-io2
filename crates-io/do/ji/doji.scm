(define-module (crates-io do ji doji) #:use-module (crates-io))

(define-public crate-doji-0.0.1 (c (n "doji") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "deno_ast") (r "^0.23.2") (f (quote ("transpiling" "typescript" "cjs" "swc_bundler" "scopes" "bundler" "compat" "dep_graph"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.167.0") (d #t) (k 0)) (d (n "deno_ops") (r "^0.45.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (f (quote ("serde" "rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j5iy08avh6hnh4bdw3azqvc5mdagpn42p0wl2kz8j2ybrcs5y5r")))

