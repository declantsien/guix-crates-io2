(define-module (crates-io do lp dolphin-memory) #:use-module (crates-io))

(define-public crate-dolphin-memory-0.2.1 (c (n "dolphin-memory") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "process-memory") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "winnt" "memoryapi" "psapi" "tlhelp32" "impl-default"))) (d #t) (k 0)))) (h "1qqj17jyw9y4z3dayhypn5ig11mdpxvylzkjhv6mz039asy0rj17")))

(define-public crate-dolphin-memory-0.2.2 (c (n "dolphin-memory") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "process-memory") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "winnt" "memoryapi" "psapi" "tlhelp32" "impl-default"))) (d #t) (k 0)))) (h "13q984x85isn0d07ydm7wdvyybifpvni21xdwhxdpsqmx9m46i7a")))

(define-public crate-dolphin-memory-0.2.3 (c (n "dolphin-memory") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "process-memory") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "winnt" "memoryapi" "psapi" "tlhelp32" "impl-default"))) (d #t) (k 0)))) (h "157l9v1hpdv5b00ham2wrxlsvsspjz93gz97fbh1d5wjsr9969vs")))

