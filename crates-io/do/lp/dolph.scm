(define-module (crates-io do lp dolph) #:use-module (crates-io))

(define-public crate-dolph-0.0.1 (c (n "dolph") (v "0.0.1") (h "18176fw5gl27i8wkhmpjsym78irq68dznywfdxkiv3c3d59r6g8s")))

(define-public crate-dolph-0.0.2 (c (n "dolph") (v "0.0.2") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bghlv852v46c7yrxbhgmd0iszsj9gxl4q2c79lvl713kqfwq9r9")))

