(define-module (crates-io do ku doku-derive) #:use-module (crates-io))

(define-public crate-doku-derive-0.9.0 (c (n "doku-derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.13") (f (quote ("diagnostics"))) (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0223irrbk3g47kf4lrn90n7vm46w4cv7xc6ppnd11vgjip14lfhp")))

(define-public crate-doku-derive-0.10.0 (c (n "doku-derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.13") (f (quote ("diagnostics"))) (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yr7zpiyi89a8w7limp3qgrqlzv21gz9r83a5k75ipzc97d9pp5q")))

(define-public crate-doku-derive-0.10.1 (c (n "doku-derive") (v "0.10.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09jnfysmgs3mxknw14dhk1dw89j3ry0zyy9csivf4byy6szw17cl")))

(define-public crate-doku-derive-0.10.2 (c (n "doku-derive") (v "0.10.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fylak3a8imqf86ab6bnng1adp75wiy07jbwz448wqvjhvl1lsrn")))

(define-public crate-doku-derive-0.11.0 (c (n "doku-derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n58kfzm7c5b4ifls400wybqy2x9zypawaghdl1i23jd3gwyjgv0")))

(define-public crate-doku-derive-0.12.0 (c (n "doku-derive") (v "0.12.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12ywkx7qvsc2mngy1asccfml2pylai1a218z8cy2z8mzzksvwc34")))

(define-public crate-doku-derive-0.2.0-alpha.1 (c (n "doku-derive") (v "0.2.0-alpha.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hm96mj303b3gq309wcm9zxspx5w1dbi8zd6i865vxwrq2vgbjnl")))

(define-public crate-doku-derive-0.2.0-alpha.2 (c (n "doku-derive") (v "0.2.0-alpha.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "094nqjrxhclls2hj3x989q3a1147znxkjyx2rxx4fymvm0nxpbkn")))

(define-public crate-doku-derive-0.20.0 (c (n "doku-derive") (v "0.20.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19b811jxfgar6wzsk5fxjyi0fj3gqz1141fqnm8b0cgr2rhwabi5")))

(define-public crate-doku-derive-0.21.0 (c (n "doku-derive") (v "0.21.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fxs4mic1jfkggbk0wr8wcxj9swh6ilrk863105as81svpwwrnad")))

(define-public crate-doku-derive-0.21.1 (c (n "doku-derive") (v "0.21.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09g8gw4l7n28c5kr66vmcf9x8n84bf932lb514lr3klm0k8ks1vl")))

