(define-module (crates-io do mc domcom-form-manager) #:use-module (crates-io))

(define-public crate-domcom-form-manager-0.1.0 (c (n "domcom-form-manager") (v "0.1.0") (d (list (d (n "futures-signals") (r "^0.3.33") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "1pj6b31167yxz18s0sm6v9l6mgy68j9a2wbm9yn59ijk5199lgrm")))

(define-public crate-domcom-form-manager-0.2.0 (c (n "domcom-form-manager") (v "0.2.0") (d (list (d (n "futures-signals") (r "^0.3.33") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "1bxvp8qar1580mvxns4vn2rfhm1091m2pa5nn4mzan6ask83l6cy")))

