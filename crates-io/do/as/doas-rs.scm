(define-module (crates-io do as doas-rs) #:use-module (crates-io))

(define-public crate-doas-rs-0.1.0 (c (n "doas-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "pam") (r "^0.8.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1iic30kw9xwm7543yps1ds6h90y8gb8zlqnf0nbpqbdzdc3m2v7v")))

