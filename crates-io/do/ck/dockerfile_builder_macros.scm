(define-module (crates-io do ck dockerfile_builder_macros) #:use-module (crates-io))

(define-public crate-dockerfile_builder_macros-0.1.0 (c (n "dockerfile_builder_macros") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "printing"))) (d #t) (k 0)))) (h "19rwy3nnvvamizfsk3bgmqivlafra41lzajcfvkil4j7qb1f6gwy") (y #t)))

(define-public crate-dockerfile_builder_macros-0.1.3 (c (n "dockerfile_builder_macros") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "printing"))) (d #t) (k 0)))) (h "02d6s93rgi8cwfslqpiinr15w4yl0q8vdshwgq5cja6a9wja4s6q")))

