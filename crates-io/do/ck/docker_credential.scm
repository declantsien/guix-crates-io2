(define-module (crates-io do ck docker_credential) #:use-module (crates-io))

(define-public crate-docker_credential-1.0.0 (c (n "docker_credential") (v "1.0.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pqwayfg319236algdvim626bwh5fngxf5sjmdibppz97v9p50pz")))

(define-public crate-docker_credential-1.0.1 (c (n "docker_credential") (v "1.0.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c61vymcryh1igysrjavx22halvswcygx7d22krp4kjaiyrzh2m4")))

(define-public crate-docker_credential-1.1.0 (c (n "docker_credential") (v "1.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rc4clqgdcnbg9hf0wqspznhqh1pq7zlly7spjw1xwrnj99plaxz")))

(define-public crate-docker_credential-1.2.0 (c (n "docker_credential") (v "1.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z67syx0a23m21n5alwsg7814196lmxk9bzlf0729pl9gyx22a4z")))

(define-public crate-docker_credential-1.2.1 (c (n "docker_credential") (v "1.2.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11snj9vcidvxhsv6gz2rgixgxa2az6yqjh36p74k6bns4kfl7khb")))

(define-public crate-docker_credential-1.2.2 (c (n "docker_credential") (v "1.2.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zmbqk2srdhz4g1dj42yr237ja00ak97klgc14cdbl5il7aqp7y0")))

(define-public crate-docker_credential-1.2.3 (c (n "docker_credential") (v "1.2.3") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17pzan5b5nd0zm76d11r81nkc5k566dj0l24p6snxipiwjaxarc7")))

(define-public crate-docker_credential-1.2.4 (c (n "docker_credential") (v "1.2.4") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19ifwig3lqshifhhsnqcs84sasxn7bhlpy880kqi641600afwvv1")))

(define-public crate-docker_credential-1.3.0 (c (n "docker_credential") (v "1.3.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z4azrqn5m5dipbp6mlmdm9xi82ys5n6qvddy71aa7wvq7c3jf0y")))

(define-public crate-docker_credential-1.3.1 (c (n "docker_credential") (v "1.3.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01qxsknr2pbvcb6axc6aqhls5dqwgvgy2hl3s87djd3fam4iz59i")))

