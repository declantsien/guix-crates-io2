(define-module (crates-io do ck dockerfile) #:use-module (crates-io))

(define-public crate-dockerfile-0.1.0 (c (n "dockerfile") (v "0.1.0") (h "0sg88wcsgszgcqsj2qnpy8qxan1sj6snxg0bdiwh18z9krb7jpvx")))

(define-public crate-dockerfile-0.1.1 (c (n "dockerfile") (v "0.1.1") (h "0mp7la5miiasvlf6a4vvrg2hqh625af4p9xbyih0b5zqjwbcd28d") (f (quote (("docinclude"))))))

(define-public crate-dockerfile-0.2.0 (c (n "dockerfile") (v "0.2.0") (h "0ssj3vhxsg3av52ssbf8h6zpzqw72i32j7k7658608892xc7z4l8") (f (quote (("docinclude"))))))

(define-public crate-dockerfile-0.2.1 (c (n "dockerfile") (v "0.2.1") (h "10fpwfc9f8affgrn10jls0w5wcxapj0898jfp1hy88881f6rxipk") (f (quote (("docinclude"))))))

