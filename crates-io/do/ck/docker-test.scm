(define-module (crates-io do ck docker-test) #:use-module (crates-io))

(define-public crate-docker-test-0.1.0 (c (n "docker-test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "camino") (r "^1.0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "1w41bmbkksmlr1g24cp2afazn6s8gl4mahx2ak417cydrw1ndxzi")))

(define-public crate-docker-test-0.1.1 (c (n "docker-test") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "camino") (r "^1.0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0sk26dv5k6rf4xgvxlrsi3rb7ladqimh37h2jb1isj4d9lk848dy")))

(define-public crate-docker-test-0.1.2 (c (n "docker-test") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "camino") (r "^1.0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "037qy8v1dkz3pidqc2m3clargrhy5d5sxgg6c967mhqsq5p3cjdj")))

(define-public crate-docker-test-0.1.3 (c (n "docker-test") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "camino") (r "^1.0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0zhk8qc2zzig29r050sw2inz5z16hh0a7zrrdw57wwg3c43bslnm")))

(define-public crate-docker-test-0.2.0 (c (n "docker-test") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "camino") (r "^1.0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "14nglfqnvwnz8w53zfybm7fa3wj44kcgkfivdkfw6511c5yn1mby")))

(define-public crate-docker-test-0.4.0 (c (n "docker-test") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "15yip7wzln0psih6v19yb1bxyd419cd86l6jkfrqab6022wj3rik")))

(define-public crate-docker-test-0.5.0 (c (n "docker-test") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "quick_cache") (r "^0.3.0") (d #t) (k 0)))) (h "04qkcdd6m8ksa988li8f8zljm40nxjsm6ylp5czs6cx8j78vh8d4") (f (quote (("docker") ("default"))))))

