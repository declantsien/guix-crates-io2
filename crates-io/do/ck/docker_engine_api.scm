(define-module (crates-io do ck docker_engine_api) #:use-module (crates-io))

(define-public crate-docker_engine_api-0.1.0 (c (n "docker_engine_api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qn37q9fb5mimv8jj8wl648lx62x3q25dajahblr2ipy3nwyiayv") (y #t)))

(define-public crate-docker_engine_api-0.2.0 (c (n "docker_engine_api") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19v6n30m06dw50cxinqnhy08lp666w4q11ybcrwkdhr1p45g46i4") (y #t)))

(define-public crate-docker_engine_api-0.1.1 (c (n "docker_engine_api") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dszb4c4ddhl46ssbr53f7s7q1nni3vinjwmbp7v122pqz7ry2rk") (y #t)))

(define-public crate-docker_engine_api-0.1.2 (c (n "docker_engine_api") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07zmrr59dqppbq96flqvks99gprajk4rilypfylhmh2chxvwhbw1")))

(define-public crate-docker_engine_api-0.1.3 (c (n "docker_engine_api") (v "0.1.3") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00h3fzbmpggp8wn5lqmhgnx4grp26fw8k8hqrkyfqiy49m90hy9p")))

(define-public crate-docker_engine_api-0.1.4 (c (n "docker_engine_api") (v "0.1.4") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ha04dy0ix0fx7cwnjf00jiyzlvn5n05jwb2n3sdmhfz2mcyv8l5")))

(define-public crate-docker_engine_api-0.1.5 (c (n "docker_engine_api") (v "0.1.5") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzkh0xwnciw4vch0s7n9icwacpwwxx5ql84ni83b180y3s9qcm1")))

