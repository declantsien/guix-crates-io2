(define-module (crates-io do ck docker-sync) #:use-module (crates-io))

(define-public crate-docker-sync-0.1.0 (c (n "docker-sync") (v "0.1.0") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "isahc") (r "^1.4.0") (f (quote ("default" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0d92bdm58dpwqkmz6ak9y7czr7fs28dz5j6ga3dz8x1dyc2q6j1q")))

(define-public crate-docker-sync-0.1.1 (c (n "docker-sync") (v "0.1.1") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "isahc") (r "^1.4.0") (f (quote ("default" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "106060vlaqgcvg7czjq4fjhwl2rlbrrz3rfbi24zd83l1s23c91i")))

(define-public crate-docker-sync-0.1.2 (c (n "docker-sync") (v "0.1.2") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "isahc") (r "^1.4.0") (f (quote ("default" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0ckwmg2vv23qxdm2zw938pgk1d9skmlxgrs34k8fsdb5sr59r63c")))

