(define-module (crates-io do ck docker-compose-runner) #:use-module (crates-io))

(define-public crate-docker-compose-runner-0.1.0 (c (n "docker-compose-runner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0725d7vs9ck55jdr5rdq3q8cp6457yr69nd56sinfk2q341j2am0") (r "1.56")))

(define-public crate-docker-compose-runner-0.2.0 (c (n "docker-compose-runner") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1rs1dja5vfr3cmrwygvgd9x75pcgqkwjyvdihkm1765j6v8mvcpr") (r "1.56")))

(define-public crate-docker-compose-runner-0.3.0 (c (n "docker-compose-runner") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0qszinv6j50ifna1ss0380ckmv1imwl4kp0apjn1i33hpxqna0rv") (r "1.56")))

(define-public crate-docker-compose-runner-0.3.1 (c (n "docker-compose-runner") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0zz5zjsbwkrg27vq32wfliw1a4qvm4b4rfw7pj1a9x9ag8x96vqi") (r "1.56")))

