(define-module (crates-io do ck docker-update-hosts) #:use-module (crates-io))

(define-public crate-docker-update-hosts-0.0.1 (c (n "docker-update-hosts") (v "0.0.1") (d (list (d (n "bollard") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros"))) (d #t) (k 0)))) (h "0y3b4ybvnwz7ysyf178ag0hlb6x1pxm1q3qvvmxrs32hsimh4qy4")))

(define-public crate-docker-update-hosts-0.0.2 (c (n "docker-update-hosts") (v "0.0.2") (d (list (d (n "bollard") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1ffr8y75pslb1vpf75chnbdzhwqsmgmhfcsv6pip98bhcq1dpj6w")))

