(define-module (crates-io do ck docker-command) #:use-module (crates-io))

(define-public crate-docker-command-0.5.0 (c (n "docker-command") (v "0.5.0") (d (list (d (n "command-run") (r "^0.12") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0dinmxz8krgrljn88wm31rqdx2x49a0gj0lwspwpvpw3hwkaf1f9")))

(define-public crate-docker-command-0.6.0 (c (n "docker-command") (v "0.6.0") (d (list (d (n "command-run") (r "^0.12") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "15xdkz7ryfwwpd5ii9cqgav8dg02jqar9yx1ml602a6y11sc5w41")))

(define-public crate-docker-command-0.6.1 (c (n "docker-command") (v "0.6.1") (d (list (d (n "command-run") (r "^0.12") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "1bd1n5q05mpilqzxw42mhlxx9kj5pqxylgz988ii58mmppm5y4kz")))

(define-public crate-docker-command-0.6.2 (c (n "docker-command") (v "0.6.2") (d (list (d (n "command-run") (r "^0.12") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "19xwd6b8lpkm7jm8ny0cyczlbc9c7bc5zaqmgnlgzs0h6893fnm6")))

(define-public crate-docker-command-0.7.0 (c (n "docker-command") (v "0.7.0") (d (list (d (n "command-run") (r "^0.13") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "107spsky3myb3ymyhh9csvp5dmkxk25n303pikm89akcvfpg3knc")))

(define-public crate-docker-command-1.0.0 (c (n "docker-command") (v "1.0.0") (d (list (d (n "command-run") (r "^1.0") (k 0)) (d (n "users") (r "^0.11") (k 0)))) (h "03i08kx74nblrwr4dy1lhgb1rx5hjn299qxppqz7kdrmyp8y7bc8") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging")))) (y #t)))

(define-public crate-docker-command-1.0.1 (c (n "docker-command") (v "1.0.1") (d (list (d (n "command-run") (r "^1.0") (k 0)) (d (n "users") (r "^0.11") (k 0)))) (h "1h7081glly7r4378k0drbx3z9ismlfqadxahfjc9yq4z3vhx75ys") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-1.0.2 (c (n "docker-command") (v "1.0.2") (d (list (d (n "command-run") (r "^1.0.3") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "09057qp9lx9b1ywcsywl6cvjv60ymjk019mdf9gvpcf9cpzai729") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-2.0.0 (c (n "docker-command") (v "2.0.0") (d (list (d (n "command-run") (r "^1.1.0") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "1rzm7hhc7dws5nv50kzcsk9w9ac00xwdm38x17kcq0aklc1qmmm2") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-3.0.0 (c (n "docker-command") (v "3.0.0") (d (list (d (n "command-run") (r "^1.1.1") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "0ab62flr546k8nb9cz15i9i0w6i1zpdkjc6c9y55w8pvhad4nznr") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-4.0.0 (c (n "docker-command") (v "4.0.0") (d (list (d (n "command-run") (r "^1.1.1") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "1sn2czbqd6m4zv7p1rz29nmhvw9hpqg2a4zkf9hqr1xdghfkihdl") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-5.0.0 (c (n "docker-command") (v "5.0.0") (d (list (d (n "command-run") (r "^1.1.1") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "0jajnzvy5lb4n153y0jk2i7np45jfd9fm8lv7i2wyrzd9jma0pj4") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

(define-public crate-docker-command-5.0.1 (c (n "docker-command") (v "5.0.1") (d (list (d (n "command-run") (r "^1.1.1") (k 0)) (d (n "users") (r "^0.11.0") (k 0)))) (h "14ma0qkza059b5iyl6y29swr89ijzyjd7swxm1xb8wb9qvazxsq2") (f (quote (("logging" "command-run/logging" "users/logging") ("default" "logging"))))))

