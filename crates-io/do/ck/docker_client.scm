(define-module (crates-io do ck docker_client) #:use-module (crates-io))

(define-public crate-docker_client-0.1.0-alpha1 (c (n "docker_client") (v "0.1.0-alpha1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0mvk6i4nalh8961fzz8h6fxvm79fl3ffy9g37z3flqpkw7l2ixmf") (y #t)))

(define-public crate-docker_client-0.1.0-alpha2 (c (n "docker_client") (v "0.1.0-alpha2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "1brfcp8cap5m79js1b265wifqs39yl59ca0ddik9qrh5h1182j4y")))

(define-public crate-docker_client-0.1.0-alpha3 (c (n "docker_client") (v "0.1.0-alpha3") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0k8x3ndr5r8fyi8c8lbd0n24fhlaf3aypc7bsa0r1bwy8rzhmif0")))

(define-public crate-docker_client-0.1.0-alpha4 (c (n "docker_client") (v "0.1.0-alpha4") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "1qs1qx4jb8prsni6kdxkas0qq7dhhav0n5ykv4sxbzmy5xsacmjg")))

