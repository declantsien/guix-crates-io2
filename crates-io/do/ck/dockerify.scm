(define-module (crates-io do ck dockerify) #:use-module (crates-io))

(define-public crate-dockerify-0.0.1 (c (n "dockerify") (v "0.0.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "1frj0vp17710gk9irmbhk3mcrz28ymn5kh88zg7csv8gy9mfsyf1") (y #t)))

(define-public crate-dockerify-0.0.4 (c (n "dockerify") (v "0.0.4") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "1vq08d4mpizzcfnxgf4d3d4ip8ragms2wjl4i00qvmapj044021z")))

