(define-module (crates-io do ck dockinfo) #:use-module (crates-io))

(define-public crate-dockinfo-1.0.0 (c (n "dockinfo") (v "1.0.0") (h "1skr4gspbg8fai2ia4ac87k19swz3gv4n6c1ll6wmy85vdwnz4jc") (y #t)))

(define-public crate-dockinfo-1.0.1 (c (n "dockinfo") (v "1.0.1") (h "00vdbjy2778sgj0yfiyzvai7205rab463z8r9nczm0jl3jai67sq")))

