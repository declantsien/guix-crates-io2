(define-module (crates-io do ck docker-api-stubs) #:use-module (crates-io))

(define-public crate-docker-api-stubs-0.1.0 (c (n "docker-api-stubs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1jkib8p24k04pvc4fzym24bjjir3s7gd2b11cl5mxalkj37hka41") (y #t)))

(define-public crate-docker-api-stubs-0.2.0 (c (n "docker-api-stubs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1a9ksqzgkb8v5mrg9agcrcklhcyn1jpagn7jgmaq2fnvd4vrx9cm")))

(define-public crate-docker-api-stubs-0.3.0 (c (n "docker-api-stubs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ypl7x0pg0lmk95x6306qzwjglpxas9fxhx17jzi92gcvrvb2g3k")))

(define-public crate-docker-api-stubs-0.4.0 (c (n "docker-api-stubs") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0iysfm074qrgzkw1hkjrgqmv7sl42931p9g1rhm9fnk2azhgawjq")))

(define-public crate-docker-api-stubs-0.5.0 (c (n "docker-api-stubs") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0d0pvz449qa9vbglrs1km2xpr2j26v2sx5rrkd6d11mlgvbyd631")))

