(define-module (crates-io do ck dockerfile-parser) #:use-module (crates-io))

(define-public crate-dockerfile-parser-0.2.0-rc1 (c (n "dockerfile-parser") (v "0.2.0-rc1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0dfmr4dp1y840pp8flz730xiyz0j0n9gr5mgrcigjr0dmljja038")))

(define-public crate-dockerfile-parser-0.2.0-rc2 (c (n "dockerfile-parser") (v "0.2.0-rc2") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1jbilh2ww5xj2qhbycxpxhlxhnmw47x93xh50dlncrr5frb8m1g8")))

(define-public crate-dockerfile-parser-0.2.0 (c (n "dockerfile-parser") (v "0.2.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1ifavwdwd7rrmxd4inlhiqq0zvp6w903ishv5khf0a7w3sy07r6n")))

(define-public crate-dockerfile-parser-0.2.1 (c (n "dockerfile-parser") (v "0.2.1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0ziw1wnzhpdzv35dqibpfksdyq15jggfgpc0f47ym25jkk4a0j3z")))

(define-public crate-dockerfile-parser-0.2.2 (c (n "dockerfile-parser") (v "0.2.2") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1dbi30zaqvbz0ajzxdf74x5yvk2d5drznfwm8qi1l99wqci031y5")))

(define-public crate-dockerfile-parser-0.3.0 (c (n "dockerfile-parser") (v "0.3.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1inwf92qakb2q1franpsfhg7nij69h1a2gyiascwrb66c3q2qw2h")))

(define-public crate-dockerfile-parser-0.3.1 (c (n "dockerfile-parser") (v "0.3.1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1y9b546dda1wgbhvhgab88znpcncmyvpyljkx10gq0jkslgq3zlx")))

(define-public crate-dockerfile-parser-0.4.0 (c (n "dockerfile-parser") (v "0.4.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "12yg7y79niisla1anzgbd6ajbalmcw2qw7mcimasncipgncjx5zb")))

(define-public crate-dockerfile-parser-0.5.0 (c (n "dockerfile-parser") (v "0.5.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1rdkmqfarib3l9drm5mgdni4ccgipbc4h1mgyw7pwiwiva4lbc8z")))

(define-public crate-dockerfile-parser-0.5.1 (c (n "dockerfile-parser") (v "0.5.1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1gf3wkxd48hvr5phbyab75pxvf6m8yv8gbkz33ns5im9asas4ncn")))

(define-public crate-dockerfile-parser-0.6.0 (c (n "dockerfile-parser") (v "0.6.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0fvpmm7h30h1c1p9airwql4bvqmlsjgnlbql2l7pjnvgkykcyjvc")))

(define-public crate-dockerfile-parser-0.6.1 (c (n "dockerfile-parser") (v "0.6.1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "043xxyxzcsq3qjh84zqr9bb7nwryy1pxjzw7nkc67kml7jn1pi6f")))

(define-public crate-dockerfile-parser-0.6.2 (c (n "dockerfile-parser") (v "0.6.2") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "091id4h8nb802j9q8963kaphgnyvifg1mh7a9h22wbq3cwv3cz8s")))

(define-public crate-dockerfile-parser-0.7.0 (c (n "dockerfile-parser") (v "0.7.0") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1rbmira6m9wi9q210g6s0lvrmg23xw1j3l0l5yylmijhph803fpx")))

(define-public crate-dockerfile-parser-0.7.1 (c (n "dockerfile-parser") (v "0.7.1") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1knx278dvqvqnvg39bcrfrp5wcl07rsnxdsjd0a0b484nqdghnh3")))

(define-public crate-dockerfile-parser-0.8.0-alpha.1 (c (n "dockerfile-parser") (v "0.8.0-alpha.1") (d (list (d (n "enquote") (r "^1.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0zaky8daw6lfs6vq6jh2102wy2i7l66ssl49g048l2ajr4i2wzxx")))

(define-public crate-dockerfile-parser-0.8.0 (c (n "dockerfile-parser") (v "0.8.0") (d (list (d (n "enquote") (r "^1.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "026wv0d1kbdkvp4cj3ray8bxs7pqh1yzk8b5lclmch7syla96ybm")))

(define-public crate-dockerfile-parser-0.9.0-alpha.1 (c (n "dockerfile-parser") (v "0.9.0-alpha.1") (d (list (d (n "enquote") (r "^1.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0bzvkb81ii5n9cpviwwgyl7ch7xk7irpnhdbiwf21mcaaaqflw3d")))

