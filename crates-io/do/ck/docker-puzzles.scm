(define-module (crates-io do ck docker-puzzles) #:use-module (crates-io))

(define-public crate-docker-puzzles-0.1.0 (c (n "docker-puzzles") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "1jcdk6mvx1ncn87p4gbmgnrskg2m8jgmw54qv84wpn4cppg92b39")))

(define-public crate-docker-puzzles-0.1.1 (c (n "docker-puzzles") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "04g2hjzp5qd4jr124s3l2rk6q92var6ivak20qa0j3qrmh3kbwly")))

(define-public crate-docker-puzzles-0.1.2 (c (n "docker-puzzles") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "0jczxdd7zzijnj5mg3qj368mph39iydgsi4930mr8yc0p1kaqqri")))

(define-public crate-docker-puzzles-0.1.3 (c (n "docker-puzzles") (v "0.1.3") (d (list (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "03zdc8vnn2sdnpk05h8ywpqfqfbr7bd8lbwfd642k5r690xhvkak")))

