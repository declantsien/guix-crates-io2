(define-module (crates-io do ck docker-stats) #:use-module (crates-io))

(define-public crate-docker-stats-0.1.0 (c (n "docker-stats") (v "0.1.0") (d (list (d (n "byte-unit") (r "^5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "1fzj7c8hzldh65pnwb2sxnnlipm4awfp9s7v2w1amj133nqd74lp")))

(define-public crate-docker-stats-0.1.1 (c (n "docker-stats") (v "0.1.1") (d (list (d (n "byte-unit") (r "^5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "0hjfql6hsc7namkbp1b2m3y3xr5354ny8k23q25qbw2dw37qzz61")))

