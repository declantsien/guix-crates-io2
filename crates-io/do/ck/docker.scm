(define-module (crates-io do ck docker) #:use-module (crates-io))

(define-public crate-docker-0.0.1 (c (n "docker") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3.6") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.1") (d #t) (k 0)))) (h "1fypip282prih1m4k71jfv9mgridvwk23knx108hglydxqxssyr9")))

(define-public crate-docker-0.0.2 (c (n "docker") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "15isgpsvwgdcg4csb0c7z6bqvkszmbg3gggxdd3dz4m56mx2agik")))

(define-public crate-docker-0.0.3 (c (n "docker") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "0ch6q66px4gakqqh9l5v72ci68263sawhr91l4112ccs1n7nxm84")))

(define-public crate-docker-0.0.4 (c (n "docker") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "1jnmdmbv4svjhk2jc012ff44m8gibmhsvhz08hx4s0gpd8b5dzh9")))

(define-public crate-docker-0.0.5 (c (n "docker") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "0q8ja9ivpgjywg18gnnl3jmzmkh5lly6ans62fyqxrlw24bpq3r9")))

(define-public crate-docker-0.0.6 (c (n "docker") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "1l1i1ir9wpp0jh09iwzgy2iy6caqqyg6sg5mjr1r5fyc9crif9ik")))

(define-public crate-docker-0.0.7 (c (n "docker") (v "0.0.7") (d (list (d (n "rustc-serialize") (r "^0.3.10") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.3") (d #t) (k 0)))) (h "1nhnh7naaqcl0b66dzr5j5wgvagncmxwalyind6xsvgmdc2l5w85")))

(define-public crate-docker-0.0.8 (c (n "docker") (v "0.0.8") (d (list (d (n "rustc-serialize") (r "^0.3.11") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "14ikillizjyyx9jxy7fjm6q3gl8c5lgiszy4yb5fkqr8jznqn9bf")))

(define-public crate-docker-0.0.9 (c (n "docker") (v "0.0.9") (d (list (d (n "rustc-serialize") (r "^0.3.11") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "16m5w47y65rfqsj63n7sfqhq46d7fdg3ybx8rxypr3yk64mhx7ys")))

(define-public crate-docker-0.0.10 (c (n "docker") (v "0.0.10") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1xzkafib7q6ivsg2pfv50gqbymkn0r9x6gp9k7j0ls9c2z87ivv8")))

(define-public crate-docker-0.0.11 (c (n "docker") (v "0.0.11") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "0xy0ljkqnkciv2wi4lc8fvgxm65svghg4b7gqpqakixr5zrp60r8")))

(define-public crate-docker-0.0.12 (c (n "docker") (v "0.0.12") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "0b49a1rvp5fnss3fmd1n30s9wy7002m7sdr7brngb572p4nmmjvi")))

(define-public crate-docker-0.0.13 (c (n "docker") (v "0.0.13") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "0ws79fl1024f5dkwsdln9ikzi74icsnvzxnbdwncly92iqwpf67i")))

(define-public crate-docker-0.0.14 (c (n "docker") (v "0.0.14") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1n99qrxzg73f3ywp42xa3gybl9w3yjfcp0pxpm9k72yhr1w8qcx0")))

(define-public crate-docker-0.0.15 (c (n "docker") (v "0.0.15") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "11bwjc0zzbj084vx019a1896sn5fi921kdw7i4xm552ggp0649nj")))

(define-public crate-docker-0.0.16 (c (n "docker") (v "0.0.16") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1ffxhbz8cvn9mfkym88hzjm2nngn9pw3y9qx2jh9d6v4cqyd5him")))

(define-public crate-docker-0.0.17 (c (n "docker") (v "0.0.17") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "0ccmxriqy4zyr3wrgwkk04xdshwk68n43c59r1vr9046cis0y085")))

(define-public crate-docker-0.0.18 (c (n "docker") (v "0.0.18") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1674fjjipmw54sznlawa3y2bid6f6kjsy0y9zmrjq6br4cz9c2zr")))

(define-public crate-docker-0.0.19 (c (n "docker") (v "0.0.19") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "03fc9hsn12z0136x2xj0hcw4g4z6x2xprv7sgxjhb8cdr892y94s")))

(define-public crate-docker-0.0.20 (c (n "docker") (v "0.0.20") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.1") (d #t) (k 0)))) (h "0lx69hr4614w28wbb09mfs0mc27j6qvxp3cm645qb8i1yg15263f")))

(define-public crate-docker-0.0.21 (c (n "docker") (v "0.0.21") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.1") (d #t) (k 0)))) (h "1zqdlfqwybfbra3d1ndjiwhi8rz5yinf70hr2brd0k133w53cwl3")))

(define-public crate-docker-0.0.22 (c (n "docker") (v "0.0.22") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.1") (d #t) (k 0)))) (h "1fzpca6lx0lf5j2x0imvwvbdsd46n4yrrzind710lbadsh542xdy")))

(define-public crate-docker-0.0.23 (c (n "docker") (v "0.0.23") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "11nfkzra8zn9pwvisldf3b66i45c08hsd2v4f9khdzhqky6pc4wb")))

(define-public crate-docker-0.0.24 (c (n "docker") (v "0.0.24") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "08dg8865pwhkil9pjsa59j29g9qndzrg6xpskkbjwm7fi6rpp9v6")))

(define-public crate-docker-0.0.25 (c (n "docker") (v "0.0.25") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "1qbrjzji058vlpyv3hhc5w93xhxxc15mmjflqsw7jcz6plaqacrw")))

(define-public crate-docker-0.0.26 (c (n "docker") (v "0.0.26") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "13crr56pgd0x68nap792xpcrp2s8p9ak4smicdjglcc04rfg1m8w")))

(define-public crate-docker-0.0.28 (c (n "docker") (v "0.0.28") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "1yc4ppna308341p6hb32zzgf7k3ryn9nmdlk8hadw446f1251i67")))

(define-public crate-docker-0.0.29 (c (n "docker") (v "0.0.29") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "0r0mlhhcsiy6m313lw0fjb0yvzb7v8br0q7c36gq9r0b37038bca")))

(define-public crate-docker-0.0.30 (c (n "docker") (v "0.0.30") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "16sljr5a8a6yf4sx7lrzmvyfrhxwp4gwb8238fc90inq78xs3sx8")))

(define-public crate-docker-0.0.31 (c (n "docker") (v "0.0.31") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "0h1bzk1cv683pn4mr3vas7wysh8djh4b68ga8nmhzx9vz2xmbpx0")))

(define-public crate-docker-0.0.32 (c (n "docker") (v "0.0.32") (d (list (d (n "openssl") (r "^0.6.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "0r86swwy32dypcrypqlvb1m739jng48jrcmy5j5smibrn6qafl8c")))

(define-public crate-docker-0.0.33 (c (n "docker") (v "0.0.33") (d (list (d (n "openssl") (r "^0.6.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "0fpkxaps0vqknpys7iq391m048q6f2gnc4qkw5fxyj3kfij3xkp3")))

(define-public crate-docker-0.0.34 (c (n "docker") (v "0.0.34") (d (list (d (n "openssl") (r "^0.6.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.2") (d #t) (k 0)))) (h "062l4khqvkip20qby9pcxq3p5c7h7jhp6xdz8jwcysji735fxsir")))

(define-public crate-docker-0.0.35 (c (n "docker") (v "0.0.35") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "15p5nl521ni6yjskzqirxcgxyvxp4blvvbhnf5a79ckcws5akdnd")))

(define-public crate-docker-0.0.36 (c (n "docker") (v "0.0.36") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "0362fff0iahn996pv8rb7b2na7pj7xn3cm8b3s3cbigbrvhh3g2z")))

(define-public crate-docker-0.0.37 (c (n "docker") (v "0.0.37") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "10gsv9d7q3a0nd8vds64mwfrms7nlxj3bs7wd2jbq9vcvg4mrz5w")))

(define-public crate-docker-0.0.38 (c (n "docker") (v "0.0.38") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1j35jqdrpmf00cq2lg3fapc7v9gyrayqh1bch3j7j9dkcnfjb201")))

(define-public crate-docker-0.0.39 (c (n "docker") (v "0.0.39") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1g5ljyr8adxijbm3wnqp1mrk2pch8qwj3yhr2ai1nwg8b1va26ay")))

(define-public crate-docker-0.0.40 (c (n "docker") (v "0.0.40") (d (list (d (n "openssl") (r "^0.6.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "0s9va8cwbb0rbd69l4y7d51v3bg8vfrr2p0bv5pmp562abs8bn8k")))

(define-public crate-docker-0.0.41 (c (n "docker") (v "0.0.41") (d (list (d (n "openssl") (r "^0.6.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.6") (d #t) (k 0)))) (h "03402cxmfqy5m9ai8kl0f6zbsmgr8hw3abii7fhw4x45njznw1lf")))

