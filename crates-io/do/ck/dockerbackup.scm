(define-module (crates-io do ck dockerbackup) #:use-module (crates-io))

(define-public crate-dockerbackup-0.1.0 (c (n "dockerbackup") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0ygabch7jf1jnax6xpzbkac901aaih059yz1ljb4d6zrc24hvzpw")))

(define-public crate-dockerbackup-0.2.0 (c (n "dockerbackup") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "17kjbgnm4x1cl6v8pn10rfj4jdlbxfdsjvp192z4jfwiqncyx4mp")))

