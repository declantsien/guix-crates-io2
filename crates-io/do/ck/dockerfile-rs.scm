(define-module (crates-io do ck dockerfile-rs) #:use-module (crates-io))

(define-public crate-dockerfile-rs-0.1.0 (c (n "dockerfile-rs") (v "0.1.0") (h "0mdvrppd7lh3js0m1xn81s7jck3cb69qzhhz93spa5gnfdhwp5qb")))

(define-public crate-dockerfile-rs-0.1.1 (c (n "dockerfile-rs") (v "0.1.1") (h "12m2y7cc6rl0ygljs5qadi7fpbbdysr4619c55kmrc104nz7x6zx")))

(define-public crate-dockerfile-rs-0.2.0 (c (n "dockerfile-rs") (v "0.2.0") (h "0lab7szamskl5frip6wc0dphdpwi0315sg4sm00907ijj6scmqpq")))

(define-public crate-dockerfile-rs-0.2.1 (c (n "dockerfile-rs") (v "0.2.1") (h "0ry1yr1h9s82q1rbp5jzajjidjx8px7s3gyf2lj9spkyg5d5bi1h")))

(define-public crate-dockerfile-rs-0.2.2 (c (n "dockerfile-rs") (v "0.2.2") (h "0kd0hbfkka1zdbfidawm55lh32f348zh2j75nf43y77vyyg70bh4") (y #t)))

(define-public crate-dockerfile-rs-0.2.3 (c (n "dockerfile-rs") (v "0.2.3") (h "14f1ig58a46whhs5mww42c89frsqpmbbcqmr3kcgh5hgyxddapmm")))

(define-public crate-dockerfile-rs-0.3.0 (c (n "dockerfile-rs") (v "0.3.0") (h "05psj19nq6x5pl2wbzhd7dvcgrcck1m2fcs1x2pq3ip875k7mrxs")))

