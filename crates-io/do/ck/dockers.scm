(define-module (crates-io do ck dockers) #:use-module (crates-io))

(define-public crate-dockers-0.1.0 (c (n "dockers") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0biv3pvn3sc1s0dghwyvp8v7pilgxrhy3mpz1zkfhd5h28r0zk6i")))

(define-public crate-dockers-0.1.1 (c (n "dockers") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0sqp6g8mq6983rx4621i44xwfdhp0nl1fd8v79xrg880kmyx6gxm")))

(define-public crate-dockers-0.1.2 (c (n "dockers") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0clfb0sicaw1cla8mk05hri1f1zcw5myw434p4bc85xjpp9mmhyf")))

(define-public crate-dockers-0.1.3 (c (n "dockers") (v "0.1.3") (d (list (d (n "curl") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1xlwdr6s7vbnrvip1gfyz487948l99x8ssd9ygnbriayb0a9j51w")))

(define-public crate-dockers-0.1.4 (c (n "dockers") (v "0.1.4") (d (list (d (n "curl") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "00yff30d3zy5jm09dyagigjxgy4iiljzl0v95w89z4n53lhz4ngj")))

(define-public crate-dockers-0.1.5 (c (n "dockers") (v "0.1.5") (d (list (d (n "curl") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0zbxmb933ib8s1i6x0a56fa8kfaff7hy6ijsiyh7dw1zpcj4san5")))

(define-public crate-dockers-0.1.6 (c (n "dockers") (v "0.1.6") (d (list (d (n "curl") (r "^0.4.23") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "16r3wb45c8hi4qrriz0c8b40nggnjsndybzm0m1wv91r3p1lb4s1")))

