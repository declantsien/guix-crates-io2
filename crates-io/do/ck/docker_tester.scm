(define-module (crates-io do ck docker_tester) #:use-module (crates-io))

(define-public crate-docker_tester-0.1.0 (c (n "docker_tester") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jf7rdsfxpmh67rcj1w7kj6kq9g0hllbjjfsvk0lnh83dmmc9caq")))

(define-public crate-docker_tester-0.1.1 (c (n "docker_tester") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ba1y0wpzhcdjas7vhjhl3r9bynrpizpj8v019h6v1964rmm8yfz")))

(define-public crate-docker_tester-0.1.2 (c (n "docker_tester") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ncavv9n1gzgbs6cic0sirfp6rcp0h81n3i1srcfwj1qn8sccq51")))

(define-public crate-docker_tester-0.1.3 (c (n "docker_tester") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02pc6inpfycqgzfwqbdyqimqkc2jmb8xibndpajsmq79xaj00sd0")))

(define-public crate-docker_tester-0.1.4 (c (n "docker_tester") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres" "chrono" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0c3if7xvwcw910br8khj04p355qk2ac8i4kz0gc1ln49iyp9zivw")))

