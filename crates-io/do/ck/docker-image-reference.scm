(define-module (crates-io do ck docker-image-reference) #:use-module (crates-io))

(define-public crate-docker-image-reference-0.1.0 (c (n "docker-image-reference") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1syvdkhzax6kliq39wp0pfxsn3nb4vdryb6pn6pd37anjd0wrilx")))

