(define-module (crates-io do ck dockertags) #:use-module (crates-io))

(define-public crate-dockertags-0.1.0 (c (n "dockertags") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0xbxnjvqnizhnlh09xnbhjwclczhxz76lmvywwjcsqyn0xigdl1b") (r "1.75")))

