(define-module (crates-io do ck docker_plugin) #:use-module (crates-io))

(define-public crate-docker_plugin-0.0.1 (c (n "docker_plugin") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "1yhrc868z00b527ax68mm7y3krgjlsr2drl0k3imdb44qmhvvd2q")))

(define-public crate-docker_plugin-0.0.2 (c (n "docker_plugin") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "1z5kp29ir1k0vj5x5b9rc3ik53niky828hslwmpgzidmicw385n5")))

(define-public crate-docker_plugin-0.0.3 (c (n "docker_plugin") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "01hl5hh0qv72pb7rqrvxwdksdhgnhh0mpz88wcd8z6jcd0c4n6l8")))

