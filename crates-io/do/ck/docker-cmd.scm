(define-module (crates-io do ck docker-cmd) #:use-module (crates-io))

(define-public crate-docker-cmd-0.1.0 (c (n "docker-cmd") (v "0.1.0") (d (list (d (n "console") (r "^0.7.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "shiplift") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "01rza43plad0pamj05g3fwampv4dl7wf0zdc0cf8jdwgwgzrnx6w") (y #t)))

(define-public crate-docker-cmd-0.2.0 (c (n "docker-cmd") (v "0.2.0") (d (list (d (n "console") (r "^0.7.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.1.0") (d #t) (k 0)))) (h "0ihsmgzq1vrz8cv0hz7qk7g64599bgxzc9rjdinn8a1wmxcqmsig") (y #t)))

(define-public crate-docker-cmd-0.3.0 (c (n "docker-cmd") (v "0.3.0") (d (list (d (n "console") (r "^0.7.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^3.1.0") (d #t) (k 0)))) (h "1vh867am5q4h96mkqvdlk4bdxm4yc7lwmprbgmqhl6w0a98cxz8r")))

