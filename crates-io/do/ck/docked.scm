(define-module (crates-io do ck docked) #:use-module (crates-io))

(define-public crate-docked-0.0.22 (c (n "docked") (v "0.0.22") (h "16gblvvvvfj79nlj5jrn9kliv676nb3yrrzd80d6bj9qb14awnz2")))

(define-public crate-docked-0.0.23 (c (n "docked") (v "0.0.23") (h "1z997iwmljwsayg15dwb38dqm1knh4bqz1ljm6wh0vqfg7spd5bf")))

(define-public crate-docked-0.0.24 (c (n "docked") (v "0.0.24") (h "0iw81kgnan70ghmazlrf3m69dyxa8jgnfyghrddbsp4pk1z5kpfp")))

