(define-module (crates-io do ck docker-bisect) #:use-module (crates-io))

(define-public crate-docker-bisect-0.1.0 (c (n "docker-bisect") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "dockworker") (r "^0.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "12fpvq4z3j2jd3vimirq135ibn6x59x6yam7g0vk626kbvwvqqzs")))

