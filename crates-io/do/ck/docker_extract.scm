(define-module (crates-io do ck docker_extract) #:use-module (crates-io))

(define-public crate-docker_extract-0.1.0 (c (n "docker_extract") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1c18n5l41z6s3qrpdm30lghg77cfh4i8kl18i1dxz446fxvwpybi")))

(define-public crate-docker_extract-0.2.0 (c (n "docker_extract") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1mzm2f51c6c6k08v3lgjkgvvr59qxyyy23jqyalf45dbmbjb9v42")))

(define-public crate-docker_extract-0.2.1 (c (n "docker_extract") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "06xac814ckiy2m5c3h047spm24y2zyqfisnr7lrxbssdn4mzgd88")))

