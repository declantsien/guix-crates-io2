(define-module (crates-io do ck docker-generate) #:use-module (crates-io))

(define-public crate-docker-generate-0.1.0 (c (n "docker-generate") (v "0.1.0") (h "1ddj5w2gbc9jcnrkz4r10ddj9qcmcjbz5bxhna5nwzcc9j3y5k02")))

(define-public crate-docker-generate-0.1.1 (c (n "docker-generate") (v "0.1.1") (h "0g8fah4di501p19yib26vsc030gxyhsl0n9wpvjjgkrm94x80idz")))

(define-public crate-docker-generate-0.1.2 (c (n "docker-generate") (v "0.1.2") (h "17c39w1kxa0il9s3sizjaig6p7kd13gdbdih9fr85q08k16j1032")))

(define-public crate-docker-generate-0.1.3 (c (n "docker-generate") (v "0.1.3") (h "04d15z1mprf8rqwfwxjzsg3w0lgn3il8x9zbmsj9zw4fhkh77xnc")))

