(define-module (crates-io do ck docker-container-log-streamer) #:use-module (crates-io))

(define-public crate-docker-container-log-streamer-0.1.0 (c (n "docker-container-log-streamer") (v "0.1.0") (d (list (d (n "bollard") (r "^0.16.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (f (quote ("sink"))) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.7") (d #t) (k 0)))) (h "1jfnwxypm68cpc8ngjlgn7dz0smy42pphyhv4fn42rz908d7nmy5")))

