(define-module (crates-io do ck docker_secrets) #:use-module (crates-io))

(define-public crate-docker_secrets-0.1.0 (c (n "docker_secrets") (v "0.1.0") (h "105png7xq0gi3v4k7dfxfiswgfi5gkydsr8wwwlg6l19z3vffi2d")))

(define-public crate-docker_secrets-0.1.1 (c (n "docker_secrets") (v "0.1.1") (h "0asmav8znsp5i1qjwi6slj98cc6s7pz62rixqnd56il2s3a0qlfz")))

