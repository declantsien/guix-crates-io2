(define-module (crates-io do ck docktor-api-client) #:use-module (crates-io))

(define-public crate-docktor-api-client-0.35.2 (c (n "docktor-api-client") (v "0.35.2") (d (list (d (n "aws-smithy-client") (r "^0.35.2") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.35.2") (d #t) (k 0)) (d (n "aws-smithy-json") (r "^0.35.2") (d #t) (k 0)) (d (n "aws-smithy-types") (r "^0.35.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1sh4m1yh45prjx4415wkhg36qlxlpbyaw49w01rqxbv69icb5k9r") (f (quote (("rustls" "aws-smithy-client/rustls") ("rt-tokio" "aws-smithy-http/rt-tokio") ("native-tls" "aws-smithy-client/native-tls") ("default" "rt-tokio" "rustls"))))))

