(define-module (crates-io do ck dockerfile_builder) #:use-module (crates-io))

(define-public crate-dockerfile_builder-0.1.0 (c (n "dockerfile_builder") (v "0.1.0") (d (list (d (n "dockerfile_builder_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "10plxmdfsipl9379799p5dfw06br2cn54w87givl73hvb5xqsm7l")))

(define-public crate-dockerfile_builder-0.1.1 (c (n "dockerfile_builder") (v "0.1.1") (d (list (d (n "dockerfile_builder_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "040p0qqy74fbg7z2mwwkxc48bjxp29vzyxwghk5hwv8kkrqqx0mh")))

(define-public crate-dockerfile_builder-0.1.2 (c (n "dockerfile_builder") (v "0.1.2") (d (list (d (n "dockerfile_builder_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0shi9bl85di3m14byrr1as1d90caw4kjzjbqgv19s638dxyzl127")))

(define-public crate-dockerfile_builder-0.1.3 (c (n "dockerfile_builder") (v "0.1.3") (d (list (d (n "dockerfile_builder_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1kz08jzsll9j4mzrwq2ac1af628qas7xhhvnsg1vjabqw7yd5qsn")))

(define-public crate-dockerfile_builder-0.1.4 (c (n "dockerfile_builder") (v "0.1.4") (d (list (d (n "dockerfile_builder_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1kma6p0wgmdr5y1r39n65mh6r4vm3rrn0all818k9hvh3ip239a7")))

