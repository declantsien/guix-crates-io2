(define-module (crates-io do ge doge_dns) #:use-module (crates-io))

(define-public crate-doge_dns-0.2.4-beta (c (n "doge_dns") (v "0.2.4-beta") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "unic-idna") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "01clpfc2z1wzcsdiznj73i7v7m3d0fkq5a9j14ygkjj9crp5i661") (f (quote (("with_idna" "unic-idna") ("default"))))))

(define-public crate-doge_dns-1.0.0 (c (n "doge_dns") (v "1.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "unic-idna") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1llbc0ja8z83fgwdzchn3ac24b4xqxwzf3r34dbmhxmn9qlycqw6") (f (quote (("with_idna" "unic-idna") ("default")))) (y #t)))

(define-public crate-doge_dns-1.0.1 (c (n "doge_dns") (v "1.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "unic-idna") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "11fijq5shba5x5v667gnv3vd0gz2bbbvs799g9plihl3dack7cnh") (f (quote (("with_idna" "unic-idna") ("default"))))))

(define-public crate-doge_dns-1.0.2 (c (n "doge_dns") (v "1.0.2") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "unic-idna") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0ah28lk7n067n9f2lwi5m1fa5mhryrbdfz9dywxaxm43cig0dr7n") (f (quote (("with_idna" "unic-idna") ("default"))))))

