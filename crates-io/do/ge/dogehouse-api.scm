(define-module (crates-io do ge dogehouse-api) #:use-module (crates-io))

(define-public crate-dogehouse-api-0.1.0 (c (n "dogehouse-api") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1w1kx027d0cdpj845hik2zbwxg8v8p43i94jn29zii6aqipadzwv")))

(define-public crate-dogehouse-api-0.1.1 (c (n "dogehouse-api") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "http2"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "10nlrmpffi8qkdsi1cyislgqm748klmy7x4cx88hw7601k94dqkn")))

