(define-module (crates-io do ge doge) #:use-module (crates-io))

(define-public crate-doge-0.1.0 (c (n "doge") (v "0.1.0") (d (list (d (n "lexical") (r "^4.0.0") (f (quote ("radix"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.25") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 2)))) (h "02ia8sdkhf8lz8jh30bvr6hv0ivy5dyzbxvqb3v4ywy1s2w8yffp")))

