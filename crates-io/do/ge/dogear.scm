(define-module (crates-io do ge dogear) #:use-module (crates-io))

(define-public crate-dogear-0.1.0 (c (n "dogear") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v3khpzkr7pl0727sj6ybnmy9lhr9gbisn45i8a88hp7cy2ln9qd")))

(define-public crate-dogear-0.1.1 (c (n "dogear") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08dylsy32hglr7ra2wxf3dn5qa8bfmrz13ar9p686dcy8c564fpq")))

(define-public crate-dogear-0.2.0 (c (n "dogear") (v "0.2.0") (d (list (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "0aa8n14glgg87mqq8al8lfdmd04rv6w8bnllqywq8zc9221ylz0i")))

(define-public crate-dogear-0.2.1 (c (n "dogear") (v "0.2.1") (d (list (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1nkcqavqzbdllq7j817nl03zw5x1rnwirc6z8hqqqmc6kb5hbbmg")))

(define-public crate-dogear-0.2.2 (c (n "dogear") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1gzz44afxs3nvmkgq1war413q5ys5fbp7shynsqfy0dr6vbbrv5w")))

(define-public crate-dogear-0.2.3 (c (n "dogear") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1zjsm57siipgc4swr2hnmb8jxbdidnbsap53lz8415r0ddmm0m3d")))

(define-public crate-dogear-0.2.4 (c (n "dogear") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1y51idq7r2lqybl8npb21jclwjd5k8kiysr6nbg04kw3iy74mb1h")))

(define-public crate-dogear-0.2.5 (c (n "dogear") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1jbgji70rx5ipr4p739bfj7nj3l9nfm7w8gkab49dqi72hz5idr6")))

(define-public crate-dogear-0.2.6 (c (n "dogear") (v "0.2.6") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1jsml85xdgivpip8d6y83fs5irb8zd5pfq5y240jd2b6imzla6n0")))

(define-public crate-dogear-0.3.0 (c (n "dogear") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "075hdl424z6ja1yshbf6n3fyaaqfzx8rcxhzjlyvikinh1gyvqlr")))

(define-public crate-dogear-0.3.1 (c (n "dogear") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1vr6bgxjgazb60h0jp31xwd29inqv1rm835v7jshxmwplp4ia6i5")))

(define-public crate-dogear-0.3.2 (c (n "dogear") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1g8a85ywmwr6a5bbkxbgihb8fjidaxpacnah9hmdd7w880jl0zy1")))

(define-public crate-dogear-0.3.3 (c (n "dogear") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1bjaiys63yqngn2mqrk9dp6hyjffn6ib8brydxl9ia6shpknxkap")))

(define-public crate-dogear-0.4.0 (c (n "dogear") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1j8f3jgzwkvfbm1bilg7pmfaizhc16pvdvb1634c5h4nfv7n10r6")))

(define-public crate-dogear-0.5.0 (c (n "dogear") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.5.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.3.0") (d #t) (k 0)))) (h "1kpccind1b61c1v8w5ywx3rn4857y6s3xkiw39l0badn8yi0qhrz")))

