(define-module (crates-io do ge dogehouse-rs) #:use-module (crates-io))

(define-public crate-dogehouse-rs-0.1.0 (c (n "dogehouse-rs") (v "0.1.0") (h "0wv38rf7ingvimyzj76s4r61gnbj14qaackbd405zzlkfzrjjyrz")))

(define-public crate-dogehouse-rs-0.1.1 (c (n "dogehouse-rs") (v "0.1.1") (h "19n3gb8iliz9ya9lf57x0pil6gbmxrws7rxx78n6ghxjmawihwmq")))

(define-public crate-dogehouse-rs-0.1.2 (c (n "dogehouse-rs") (v "0.1.2") (h "0437nx2rypbfisy5vqzf7pi1dhlw7mh49b0030k3l7yd9fjn2ns0")))

(define-public crate-dogehouse-rs-0.1.3 (c (n "dogehouse-rs") (v "0.1.3") (h "16q7gkakabqkr3hv8x8rfkb61x74m7ybccj0zznxahdiy4mvfp7n")))

(define-public crate-dogehouse-rs-0.1.4 (c (n "dogehouse-rs") (v "0.1.4") (d (list (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1rf4sq6cmwfi38g76w1k5ja525jjiqf9ni9yq38dcz6zsd4sjz9z")))

(define-public crate-dogehouse-rs-0.1.5 (c (n "dogehouse-rs") (v "0.1.5") (d (list (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "17w86zcfsb2b6rvixrzs3n1153grnb74rj2j3jwb09j69ykq23pn")))

(define-public crate-dogehouse-rs-0.1.6 (c (n "dogehouse-rs") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "06s78xbxz6byp9n7gjn49qf3902yk7gv3slslmv8dxfpsyvd9pvs")))

(define-public crate-dogehouse-rs-0.1.7 (c (n "dogehouse-rs") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0y5gkrqdpz4zcsg5mzfkk27fp0yad5z3qg3vbcwssbx0fa9zj7j9")))

(define-public crate-dogehouse-rs-0.1.8 (c (n "dogehouse-rs") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0z46qw61xwddqmhyqllzrhbv87lgdgif7sxixl9rsarhwqccggli")))

(define-public crate-dogehouse-rs-0.1.9 (c (n "dogehouse-rs") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0ggymjxhm942vqfbghgmkxp9s8nxyjfm0midibw82r39765ylbxj")))

(define-public crate-dogehouse-rs-0.1.10 (c (n "dogehouse-rs") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1wbjzfdij6372dzckhrjlgplqh6ajl67cqgxw0ab1npbaa4ms8is")))

(define-public crate-dogehouse-rs-0.1.11 (c (n "dogehouse-rs") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0hwcdcyp710jzg02i46ck0ibq1hcaxw7xi1zfgidriw39d15gkwk")))

(define-public crate-dogehouse-rs-0.1.12 (c (n "dogehouse-rs") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0rlbbvx453gwbafpq5z9p743sawa9gyv5kfbd0md09ipcrdqfb26")))

(define-public crate-dogehouse-rs-0.1.13 (c (n "dogehouse-rs") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0qchr2252749qq0gwrqs5872bafgqls6ajxd8g61b67kcqhn9zi8")))

(define-public crate-dogehouse-rs-0.1.14 (c (n "dogehouse-rs") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0c2ldk5ss7paqx6v3s5f8kfm3v8gsqpp2ga0yxxh7c000nxhviy2")))

(define-public crate-dogehouse-rs-0.1.15 (c (n "dogehouse-rs") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1vr8ixmc1wf348vlp1r3bi8g3ygm3kxsz4lc1zp9c55ffws6amxj")))

(define-public crate-dogehouse-rs-0.1.16 (c (n "dogehouse-rs") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1lr27qfxaxyzf1ycwvs781i0lx23axvr5bv5va4m249z7y2daxn5")))

(define-public crate-dogehouse-rs-0.1.17 (c (n "dogehouse-rs") (v "0.1.17") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "153r3fbhighff9xp6h2sbhi2m67mnca52m1sh5iajhl0dlzmcjdk")))

(define-public crate-dogehouse-rs-0.1.18 (c (n "dogehouse-rs") (v "0.1.18") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0abkcwffwx5997dvb5pmsn8h0lm3z3q3glzv9apkggry9qfhpdfa")))

(define-public crate-dogehouse-rs-0.1.19 (c (n "dogehouse-rs") (v "0.1.19") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "12h480zng9pwxsdlzvh3kfdhbzfswx97vmmh9q9i48517l1vnz1g")))

(define-public crate-dogehouse-rs-0.1.20 (c (n "dogehouse-rs") (v "0.1.20") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1k8vn7cdldmlyv1asn5n5akpprkpyjbfj6grklac19d2cf384yxh")))

(define-public crate-dogehouse-rs-0.1.21 (c (n "dogehouse-rs") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "websocket") (r "^0.26.2") (d #t) (k 0)))) (h "032yiwsl6hf66kfg0yzm6gmwh6ydalcpy28jp22603yyzgfk9bxd")))

(define-public crate-dogehouse-rs-0.1.22 (c (n "dogehouse-rs") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.2") (d #t) (k 0)))) (h "1n00jayns3jd6pxhrgl59vmgz7yx9vpxqyhn27igl0jiiwfd53j9")))

(define-public crate-dogehouse-rs-0.1.23 (c (n "dogehouse-rs") (v "0.1.23") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.2") (d #t) (k 0)))) (h "15nd8xk19fpx2nr8r1332r7xr76fcjhw8x4nmgac5pkhb3abr6f9")))

