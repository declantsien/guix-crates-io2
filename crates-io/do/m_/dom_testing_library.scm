(define-module (crates-io do m_ dom_testing_library) #:use-module (crates-io))

(define-public crate-dom_testing_library-0.1.0 (c (n "dom_testing_library") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.9") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("HtmlCollection" "NodeList" "Window" "HtmlElement" "Document" "HtmlLabelElement" "HtmlTextAreaElement" "HtmlInputElement" "HtmlSelectElement"))) (d #t) (k 0)))) (h "0jbwm7yd2lqi700fqa7q4g69lx553sxikdywlz3y59r8i422pb33") (f (quote (("unit_tests") ("leptos"))))))

