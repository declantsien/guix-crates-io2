(define-module (crates-io do m_ dom_renderer) #:use-module (crates-io))

(define-public crate-dom_renderer-0.1.0 (c (n "dom_renderer") (v "0.1.0") (h "0vys1riwhx5d09wcd43s3ysxhsmhv9qmw9qimg25dxv09qbaay1v")))

(define-public crate-dom_renderer-0.1.1 (c (n "dom_renderer") (v "0.1.1") (h "0763g075j6sc4cz8zfhvgxnpchad3gal1d6qvd3nggdhf0y4gdlc")))

(define-public crate-dom_renderer-0.2.1 (c (n "dom_renderer") (v "0.2.1") (h "1mgcjjh51dfbcx746n4p9v5y89fafgkvv0h3n10nvxwbldgcjjfy")))

(define-public crate-dom_renderer-0.2.2 (c (n "dom_renderer") (v "0.2.2") (h "0mkgb9zv0dzpn1w8fsjvxvq66n0ziih4arnrw55221kij8zfd54c")))

