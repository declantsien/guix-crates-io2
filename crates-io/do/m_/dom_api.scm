(define-module (crates-io do m_ dom_api) #:use-module (crates-io))

(define-public crate-dom_api-0.1.0 (c (n "dom_api") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1011g3lrv0l9v6bl80728kwg0vyxqcb71p14xh37pmxflxnazqha")))

