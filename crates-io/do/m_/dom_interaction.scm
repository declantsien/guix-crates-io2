(define-module (crates-io do m_ dom_interaction) #:use-module (crates-io))

(define-public crate-dom_interaction-0.1.0 (c (n "dom_interaction") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "Event"))) (d #t) (k 0)))) (h "1vka2lh0aczk5b20hm425qiyzgc10n8m35m8k6pxzw9ca10cav0l")))

