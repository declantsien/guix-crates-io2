(define-module (crates-io do dg dodgems) #:use-module (crates-io))

(define-public crate-dodgems-0.1.0 (c (n "dodgems") (v "0.1.0") (h "0y6b2amn9q45w8cfzq1vgbjjk6anyvk7bjshjbd68x055v4zmrv2") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-dodgems-0.1.1 (c (n "dodgems") (v "0.1.1") (h "1nwjq9pzhfzl2174m9jr6yx7ad7mvlpsm2pirhpwvppafg11nckz") (f (quote (("default" "alloc") ("alloc"))))))

