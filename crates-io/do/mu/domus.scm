(define-module (crates-io do mu domus) #:use-module (crates-io))

(define-public crate-domus-0.1.0 (c (n "domus") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.59") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.9") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Attr" "console" "Document" "DomTokenList" "Element" "Headers" "HtmlElement" "NamedNodeMap" "Node" "NodeList" "Request" "RequestInit" "RequestMode" "Response" "Window"))) (d #t) (k 0)))) (h "08wym9c699hjahaipjbwhrxl2glxz1zkvm4l3brckdaksgs6x332")))

