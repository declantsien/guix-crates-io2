(define-module (crates-io do nj donjo) #:use-module (crates-io))

(define-public crate-donjo-0.1.0 (c (n "donjo") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0k9p37vv283p26l36n5bsmaz1xx0ndfyy5ls6p1rlhhv8qrn6iq9")))

