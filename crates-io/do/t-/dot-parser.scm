(define-module (crates-io do t- dot-parser) #:use-module (crates-io))

(define-public crate-dot-parser-0.1.0 (c (n "dot-parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0p507ah57sfvmls3da5mnrn53k0daj33s97sy90lbpl3ck99lhb7")))

(define-public crate-dot-parser-0.1.1 (c (n "dot-parser") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0f5kmlpcwpkn261kan1p4aj5qf9gbvp1bsp7nhj4sagf9lnawy49")))

(define-public crate-dot-parser-0.1.2 (c (n "dot-parser") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "16fnq6s23r9b3b2mnzyx0sdaibmhgx2wxq238pzk3vknh5j31iwl")))

(define-public crate-dot-parser-0.1.3 (c (n "dot-parser") (v "0.1.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0mad3lg8wg41wfzrmvs3gddj8n3dgcylyv7ddgi652gw6vkafp7f")))

(define-public crate-dot-parser-0.2.0 (c (n "dot-parser") (v "0.2.0") (d (list (d (n "either") (r "^1.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (o #t) (d #t) (k 0)))) (h "1bznvzgbqrpb3dkv99h4lqfzgfbl5bfkwhpas0mrdm8bkjqj4sgg") (f (quote (("display") ("default")))) (s 2) (e (quote (("to_tokens" "dep:quote" "dep:proc-macro2") ("petgraph" "dep:petgraph"))))))

(define-public crate-dot-parser-0.2.1 (c (n "dot-parser") (v "0.2.1") (d (list (d (n "either") (r "^1.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (o #t) (d #t) (k 0)))) (h "1v2h1brws3dq21cyq9bb620yn1hzmjsv2k41kpcfcr35q5cy7pa1") (f (quote (("display") ("default")))) (s 2) (e (quote (("to_tokens" "dep:quote" "dep:proc-macro2") ("petgraph" "dep:petgraph"))))))

