(define-module (crates-io do t- dot-generator) #:use-module (crates-io))

(define-public crate-dot-generator-0.1.0 (c (n "dot-generator") (v "0.1.0") (d (list (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)))) (h "0rcm6ry4bciycpy85jlczchhaxc3b44aw9cc52k41m9q71w00bij")))

(define-public crate-dot-generator-0.2.0 (c (n "dot-generator") (v "0.2.0") (d (list (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)))) (h "0wdq0pl5kbb8h6bylz1c0k4agi0v3hfljdp3phz8fwazljnwgaha")))

