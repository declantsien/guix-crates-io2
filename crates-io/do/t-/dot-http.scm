(define-module (crates-io do t- dot-http) #:use-module (crates-io))

(define-public crate-dot-http-0.1.0 (c (n "dot-http") (v "0.1.0") (d (list (d (n "Boa") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gc") (r "^0.3.3") (d #t) (k 0)) (d (n "http-test-server") (r "^1.0.0") (d #t) (k 2)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "1fxax55s0mvzjf9axvyda8hp24sjvcr3zf5k5ad87qr5rkwnk2fz")))

