(define-module (crates-io do t- dot-structures) #:use-module (crates-io))

(define-public crate-dot-structures-0.1.0 (c (n "dot-structures") (v "0.1.0") (h "0z7zna482rdd0rls87fq4kxbqggs1dyrlrh6f7gd13vzvzbafpal")))

(define-public crate-dot-structures-0.1.1 (c (n "dot-structures") (v "0.1.1") (h "081ffisd024j9m3n9dlpgiwd3rlwhfrqaj6b3134vfsi5b03apk7")))

