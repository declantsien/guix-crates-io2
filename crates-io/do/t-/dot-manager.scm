(define-module (crates-io do t- dot-manager) #:use-module (crates-io))

(define-public crate-dot-manager-0.1.0 (c (n "dot-manager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0qm73pwij0b0y1lm47ws83ff9qw2hp6a7dyq7c81a4wfwl5wpaqm")))

(define-public crate-dot-manager-0.2.0 (c (n "dot-manager") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1iz8d2csxrlrkzws5x3acak48pyrlq50jgsn1br905pz0c0rw3h8")))

(define-public crate-dot-manager-0.2.1 (c (n "dot-manager") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "15725ks73ry413x21cvcgf31rrmh2x1csf3k5300plr9z7z7hz76")))

(define-public crate-dot-manager-0.2.2 (c (n "dot-manager") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1n7jva36ap8wzk3rq85xck1f88c5j3yxi1zshx1i5jsrp765qf6r")))

