(define-module (crates-io do t- dot-parser-macros) #:use-module (crates-io))

(define-public crate-dot-parser-macros-0.1.0 (c (n "dot-parser-macros") (v "0.1.0") (d (list (d (n "dot-parser") (r "^0.2.0") (f (quote ("to_tokens"))) (d #t) (k 0)) (d (n "dot-parser") (r "^0.2.0") (f (quote ("to_tokens" "petgraph"))) (d #t) (k 2)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "1chpm5b5g9jvvh8vj6ymc721pxms1cdmjkbrz9bqrfj7rw8achds")))

(define-public crate-dot-parser-macros-0.2.0 (c (n "dot-parser-macros") (v "0.2.0") (d (list (d (n "dot-parser") (r "^0.2.0") (f (quote ("to_tokens"))) (d #t) (k 0)) (d (n "dot-parser") (r "^0.2.0") (f (quote ("to_tokens" "petgraph"))) (d #t) (k 2)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "1rphh1n05amlwxlrd4kq0a0ga0zd1w86v43l5vcacn2q6ahwnqzy")))

