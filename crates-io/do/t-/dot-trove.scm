(define-module (crates-io do t- dot-trove) #:use-module (crates-io))

(define-public crate-dot-trove-0.1.0 (c (n "dot-trove") (v "0.1.0") (h "0pdy4k8a6xr9b4xj2f4ysq5z7k2yd69xd0h8n1xvbr74ndfj4441")))

(define-public crate-dot-trove-0.2.0 (c (n "dot-trove") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "098mqqz8jdd92r4kyba7m53ym84kqif5mvgixlsw5i7j1718k4v3")))

(define-public crate-dot-trove-0.2.1 (c (n "dot-trove") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "0jh796hm7fph8zns1p0y1kd2rk57v35v49yzaa9psdb2rkhbr1l0")))

(define-public crate-dot-trove-0.2.2 (c (n "dot-trove") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "17b0x0bilw2g84kkpa6a9p99z8vrwqpp1a26axs9s1j3hwx535fd")))

(define-public crate-dot-trove-0.2.3 (c (n "dot-trove") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "1hsjr78b0fx947nn736d1b1h1l3rqj9k8fc47sjmwwx2r93ci1qy")))

