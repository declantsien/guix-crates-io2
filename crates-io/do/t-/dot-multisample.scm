(define-module (crates-io do t- dot-multisample) #:use-module (crates-io))

(define-public crate-dot-multisample-0.1.0 (c (n "dot-multisample") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "zip") (r "^0.6.6") (k 2)))) (h "0ac9cxwkdm18jk04awgyiqlhzvh2mirdq5ny8rknamkr81wzdsjd") (r "1.71.1")))

