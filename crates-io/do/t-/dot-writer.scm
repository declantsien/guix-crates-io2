(define-module (crates-io do t- dot-writer) #:use-module (crates-io))

(define-public crate-dot-writer-0.1.0 (c (n "dot-writer") (v "0.1.0") (h "0a1d7h6v0aarxb8blvydadn36yx0lq7immp6wlj42mywxvar3qpk")))

(define-public crate-dot-writer-0.1.1 (c (n "dot-writer") (v "0.1.1") (h "1ab4d7h80vg07gdgbg2nwdlf7w3zzmfz0wbhfwj0y5wrw51ahxw9")))

(define-public crate-dot-writer-0.1.2 (c (n "dot-writer") (v "0.1.2") (h "152qvy7ska03pg7f4gfsxngg7sbhr9fsnh5fxhqm2h93n54vkvqx")))

(define-public crate-dot-writer-0.1.3 (c (n "dot-writer") (v "0.1.3") (h "0yjv36hr3klzq4bpzkkqp64s8479ssabr7zkdxn4163ybsyi26rx")))

