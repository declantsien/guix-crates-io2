(define-module (crates-io do t- dot-canvas) #:use-module (crates-io))

(define-public crate-dot-canvas-0.1.0 (c (n "dot-canvas") (v "0.1.0") (h "1kg7nazyf454sz6nag7iipis00m5nfdnry5iky83ic44y0lxi3k7")))

(define-public crate-dot-canvas-0.1.1 (c (n "dot-canvas") (v "0.1.1") (h "09ggmwf4daagwpvza2c8c42rba9k5w97aa076h5kpy6dkb2y3jmk")))

