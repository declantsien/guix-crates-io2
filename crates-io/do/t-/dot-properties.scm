(define-module (crates-io do t- dot-properties) #:use-module (crates-io))

(define-public crate-dot-properties-0.1.0 (c (n "dot-properties") (v "0.1.0") (h "0rh6igqjvrgxv1j12awnga1j0r3ra216zm5jiyfwxx4iq6z478bc")))

(define-public crate-dot-properties-0.2.0 (c (n "dot-properties") (v "0.2.0") (h "0yw9aadz543f7m91k6wgw2lwbmhxmhwmj5qq69s2sfwc2ynsmmag")))

