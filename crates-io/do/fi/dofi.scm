(define-module (crates-io do fi dofi) #:use-module (crates-io))

(define-public crate-dofi-0.1.0 (c (n "dofi") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1l56fxgakx4r6a1rrz7ybz4xwfs8h07hkik76rihycsnliq79379") (y #t)))

(define-public crate-dofi-0.1.1 (c (n "dofi") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0fmiknd85v6ampjawa3c22h142wvi2l7v2lg8dkypjz6wlv7prqc") (y #t)))

(define-public crate-dofi-0.1.2 (c (n "dofi") (v "0.1.2") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zvqc08kk03mrq03affjwa35361b8d5vxzk49bhndm9jzhkx1p47") (y #t)))

(define-public crate-dofi-0.1.3 (c (n "dofi") (v "0.1.3") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gflrxqgkng1nfqnrcwa8dc6ikfcg7g5b9b06m0vig9g6zmm8rla")))

(define-public crate-dofi-0.1.4 (c (n "dofi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "argh") (r "^0.1.6") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0ic5xcz9jwshp6s3j70dcvddkk62b5if25licslgziq5a0p4acyv") (y #t)))

(define-public crate-dofi-0.1.5 (c (n "dofi") (v "0.1.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "03hzm80kmf368ddzqqp1w587lmfp74i110c73d9rly9d2gc5il8m")))

(define-public crate-dofi-0.1.11 (c (n "dofi") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ngxsxl6i1nyj37mq5b1kv8ysrqa8fannrwzihs2qzbgd6j9qqi7")))

