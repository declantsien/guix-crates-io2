(define-module (crates-io do su dosu) #:use-module (crates-io))

(define-public crate-dosu-0.0.1 (c (n "dosu") (v "0.0.1") (d (list (d (n "bsd_auth") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "login_cap") (r "^0.0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "readpassphrase") (r "^0.2.0") (d #t) (k 0)))) (h "1mmcb065xk27h4vahbnsgd2j312x66lh26rsd7ccgy7ypj078s6w")))

(define-public crate-dosu-0.0.2 (c (n "dosu") (v "0.0.2") (d (list (d (n "bsd_auth") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "login_cap") (r "^0.0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "readpassphrase") (r "^0.2.0") (d #t) (k 0)))) (h "0yikbg08qgh9sv3xc98kn19da74rnj4h6s20air8iipmbhn910q6")))

