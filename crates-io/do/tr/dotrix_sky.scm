(define-module (crates-io do tr dotrix_sky) #:use-module (crates-io))

(define-public crate-dotrix_sky-0.1.0 (c (n "dotrix_sky") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotrix_core") (r "^0.5") (d #t) (k 0)) (d (n "dotrix_math") (r "^0.4") (d #t) (k 0)))) (h "0419jlrfdl6iq4q7gq4wfs9skrf4338alhrlxwqh0nzhspivblq3")))

