(define-module (crates-io do tr dotrix_pbr) #:use-module (crates-io))

(define-public crate-dotrix_pbr-0.2.0 (c (n "dotrix_pbr") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotrix_core") (r "^0.5") (d #t) (k 0)) (d (n "dotrix_math") (r "^0.4") (d #t) (k 0)))) (h "0a5xyqcqp5d894kvch30d2amlxzy4vhb6rgz393xa1x5d8xp0632")))

