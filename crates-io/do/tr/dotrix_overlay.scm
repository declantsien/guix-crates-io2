(define-module (crates-io do tr dotrix_overlay) #:use-module (crates-io))

(define-public crate-dotrix_overlay-0.1.0 (c (n "dotrix_overlay") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotrix_core") (r "^0.5") (d #t) (k 0)) (d (n "dotrix_math") (r "^0.4") (d #t) (k 0)))) (h "0gcxpbkk3c7cd7x1d6l6684s1dlv085b9w18k4d0b46388fbzz5r")))

