(define-module (crates-io do tr dotrix_math) #:use-module (crates-io))

(define-public crate-dotrix_math-0.2.0 (c (n "dotrix_math") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)))) (h "19w5nlal7yxr6p647gb6p3ap0x9bflfhahs8zghamcwp0gly3h9m")))

(define-public crate-dotrix_math-0.3.0 (c (n "dotrix_math") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)))) (h "1n12f2sj46ys9npdaj4mi6qr531i90mbnklg0ryc4gwqzxybq9i3")))

(define-public crate-dotrix_math-0.4.0 (c (n "dotrix_math") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)))) (h "17wj4dcri60r6j6phk9pf7d5vh0gjx24ahz2pzcird7jkc129vbx")))

