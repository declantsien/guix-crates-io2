(define-module (crates-io do tr dotrix_terrain) #:use-module (crates-io))

(define-public crate-dotrix_terrain-0.1.0 (c (n "dotrix_terrain") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "dotrix_core") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "17lklbf38r8bd0hygzjw86kkw8jh2wiks7baccwgixhmsgnlzp3m")))

(define-public crate-dotrix_terrain-0.3.0 (c (n "dotrix_terrain") (v "0.3.0") (d (list (d (n "dotrix_core") (r "^0.3") (d #t) (k 0)) (d (n "dotrix_math") (r "^0.3") (d #t) (k 0)) (d (n "noise") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "004y6sgj5026rbq852i5yz1m3864hb21b2z0pzs5a0w82llffk8x")))

(define-public crate-dotrix_terrain-0.5.0 (c (n "dotrix_terrain") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotrix_core") (r "^0.5") (d #t) (k 0)) (d (n "dotrix_math") (r "^0.4") (d #t) (k 0)) (d (n "dotrix_pbr") (r "^0.2") (d #t) (k 0)) (d (n "noise") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.9") (f (quote ("trace"))) (d #t) (k 0)))) (h "1s3zpyjvh5zlpxyxklc5jzjcrqq15qh0rkpbfr35g7wbgwmpqx6l")))

