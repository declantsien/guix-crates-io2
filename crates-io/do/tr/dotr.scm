(define-module (crates-io do tr dotr) #:use-module (crates-io))

(define-public crate-dotr-0.1.0 (c (n "dotr") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0cjglikj3xjaymnrlwdm5smb396i8d7pcrxcwvrbkkxyfivipdhg")))

(define-public crate-dotr-0.2.0 (c (n "dotr") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0xxalq09g7if3l5ivrdvk69jrsxyf1q32nqdkavw1f676ag4cjlj")))

(define-public crate-dotr-0.3.0 (c (n "dotr") (v "0.3.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0kllpbzdbyhy6cy75dbap9vrvn7r42hll6vk0h5gjgsx4w75b5xk")))

(define-public crate-dotr-0.3.1 (c (n "dotr") (v "0.3.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0qys9hj76j8g22lkd0lhp2hqxiapra7l0i19rb4ygg6h8f8846r9")))

(define-public crate-dotr-0.4.0 (c (n "dotr") (v "0.4.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0nqy8j8g4jpa2hylyjjh57niq430d5m0gdrsk45c57bch7hf1q2i")))

