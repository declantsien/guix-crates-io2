(define-module (crates-io do tr dotrust) #:use-module (crates-io))

(define-public crate-dotrust-0.1.0 (c (n "dotrust") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)))) (h "1xk4vizmwbngy40ylmzilbskprcy221df59hrrfk338qlr54ggrs")))

(define-public crate-dotrust-0.1.1 (c (n "dotrust") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)))) (h "03sb2qqbyg0dp7xwmc4kywlz2ajwvbwck8x4wmyc60pvpcjzzp8j")))

(define-public crate-dotrust-0.1.2 (c (n "dotrust") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "libloading") (r "^0.3.2") (d #t) (k 0)))) (h "1cil4lkfphv97rd9hkc665p3jncsxhvq6848vvjs00ab38xj9hma")))

(define-public crate-dotrust-0.2.0 (c (n "dotrust") (v "0.2.0") (d (list (d (n "libloading") (r "^0.4.1") (d #t) (k 0)))) (h "0fqv64x5gqpcyyki112ipdz8bh6h8br7qp8kmfq46kfap9a9ykbs")))

(define-public crate-dotrust-0.2.2 (c (n "dotrust") (v "0.2.2") (d (list (d (n "libloading") (r "^0.4.1") (d #t) (k 0)))) (h "1wd5gzp376pv3hvpp5b4i9f5cswr54gml0drhws40yih8mrhy7m5")))

(define-public crate-dotrust-0.2.1 (c (n "dotrust") (v "0.2.1") (d (list (d (n "libloading") (r "^0.4.1") (d #t) (k 0)))) (h "0mlknahw8phvhkav1fq6zfbkihmyhdkmqylwq9rv3j0nd5cia7x8")))

(define-public crate-dotrust-0.2.3 (c (n "dotrust") (v "0.2.3") (d (list (d (n "libloading") (r "^0.4.1") (d #t) (k 0)))) (h "14mrw9xrrxr7xplh6fi68lg59q9b1nx1688krzd7i3404k2gsj7z")))

(define-public crate-dotrust-0.3.0 (c (n "dotrust") (v "0.3.0") (d (list (d (n "com-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "libloading") (r "^0.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0j5hzdq3ifnzyxc3byczvv063z5v9w19fi1lvl47i14nfxfnwwg6")))

