(define-module (crates-io do tr dotreds-binary-heap-plus) #:use-module (crates-io))

(define-public crate-dotreds-binary-heap-plus-0.4.1 (c (n "dotreds-binary-heap-plus") (v "0.4.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "171j9k69qscs70rhixgisz0sh0dx8l144azi9syigm6xy1bky3q9")))

(define-public crate-dotreds-binary-heap-plus-1.0.0 (c (n "dotreds-binary-heap-plus") (v "1.0.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "144avgww88ik35cvmnx4zf3zbgwb326gdmhmww5dad39db8lqrw9")))

(define-public crate-dotreds-binary-heap-plus-1.0.1 (c (n "dotreds-binary-heap-plus") (v "1.0.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "02f69v2jwksqxvcrpmcjklxz7yiz8ll54bdxhas6bbnf8c6dg00w")))

(define-public crate-dotreds-binary-heap-plus-1.0.2 (c (n "dotreds-binary-heap-plus") (v "1.0.2") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "02xm5cbj5jdy0nm8scj0m3mzkd8qbdfx5y6y708m6s39n4myf89b")))

(define-public crate-dotreds-binary-heap-plus-1.0.3 (c (n "dotreds-binary-heap-plus") (v "1.0.3") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0p5r3yyrgxig84hxi24rsk1nv9lwh9asvqsnkk212f93by75qyb4")))

