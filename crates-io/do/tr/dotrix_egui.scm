(define-module (crates-io do tr dotrix_egui) #:use-module (crates-io))

(define-public crate-dotrix_egui-0.2.0 (c (n "dotrix_egui") (v "0.2.0") (d (list (d (n "dotrix_core") (r "^0.2") (d #t) (k 0)) (d (n "egui") (r "^0.8.0") (d #t) (k 0)))) (h "1drz623a5fdqp5sibvi4lsf7qjrxaxkimvclfx0fby7nqvpvw6mh")))

(define-public crate-dotrix_egui-0.3.0 (c (n "dotrix_egui") (v "0.3.0") (d (list (d (n "dotrix_core") (r "^0.3") (d #t) (k 0)) (d (n "egui") (r "^0.8.0") (d #t) (k 0)))) (h "0b8p7wfwncvhymw61vqb9366w7x4dvhaa1py10g9wg49w0cpxyv5")))

(define-public crate-dotrix_egui-0.5.0 (c (n "dotrix_egui") (v "0.5.0") (d (list (d (n "dotrix_core") (r "^0.5") (d #t) (k 0)) (d (n "dotrix_overlay") (r "^0.1") (d #t) (k 0)) (d (n "egui") (r "^0.14.2") (d #t) (k 0)))) (h "0fmqj27fkdqy78w4jxkg9a4vhsb60hdpi6jnih223r9k3dyhp0hc")))

