(define-module (crates-io do tf dotfive_cargo) #:use-module (crates-io))

(define-public crate-dotfive_cargo-0.1.0 (c (n "dotfive_cargo") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0aa72gxjb7kjplfbbj9wl0nrrlqdapiyamwavjjdixglayb8mzza") (y #t)))

(define-public crate-dotfive_cargo-0.1.1 (c (n "dotfive_cargo") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0k6fys1za8ga65rh1b2iiyg4hyxwh9zc56rgcncmwks014inigff") (y #t)))

