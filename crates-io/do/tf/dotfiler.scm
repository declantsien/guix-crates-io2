(define-module (crates-io do tf dotfiler) #:use-module (crates-io))

(define-public crate-dotfiler-0.1.0 (c (n "dotfiler") (v "0.1.0") (d (list (d (n "tera") (r "^0.1.2") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1511mihpbc33z3m9qb26hsywvzc31wl0y50931n3bf6vx9vc5md3")))

(define-public crate-dotfiler-0.1.1 (c (n "dotfiler") (v "0.1.1") (h "0ls0crg45lkkd9iagzc9dxc71kr6l28cx3zapwk71m2vbwqx0w3g")))

