(define-module (crates-io do or doors) #:use-module (crates-io))

(define-public crate-doors-0.1.0 (c (n "doors") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "0sm5ncmvc89s29gzsw2915dlhi001jn3cm14qphhzm47yldy3w4q")))

(define-public crate-doors-0.1.1 (c (n "doors") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "15ajiphrdwl81gw9028r7bz2wf4wc1xrlbxh2hlb4ydmsfvb7b15")))

(define-public crate-doors-0.2.0 (c (n "doors") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sjzaww6hwfgcf8rjc64zsf3m2xaf30sfsdrbxzwind7dnpnwyk0")))

(define-public crate-doors-0.3.2 (c (n "doors") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s5pl826w95izx5vvd3zj3wxblanh49i4p90hkphddfr96772axs")))

(define-public crate-doors-0.4.0 (c (n "doors") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xbgmkpd9k1mjwkmvymi2hxh77b1qy2la6dq14n7095xrh779zd1")))

(define-public crate-doors-0.4.1 (c (n "doors") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m6k1qjflwnsi46965041c2wi02fkkv9l9dq8s2y8h96sj9a5430")))

(define-public crate-doors-0.4.2 (c (n "doors") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g5v39ja1jf3hj5nivxl8ay1q33qd112ydwaizg7158wpwv170pv")))

(define-public crate-doors-0.5.0 (c (n "doors") (v "0.5.0") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04kwczwns7701mhpky72fvm5zqx677fjw1afg349nqszda7kb6fh")))

(define-public crate-doors-0.5.1 (c (n "doors") (v "0.5.1") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ypdkmhn919zx95iic1pb1j7y94dkvxxk9i3xww8vhwy9p0s8fba")))

(define-public crate-doors-0.5.2 (c (n "doors") (v "0.5.2") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yx4qwp59dm7a0p8scavjk0bi0b05rd9y45r23p9p6bxxkb0bmw1")))

(define-public crate-doors-0.6.0 (c (n "doors") (v "0.6.0") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vrlrwd2yz8888a38yvpv1s333s988hgd25hj11zb3n666q1r6hn")))

(define-public crate-doors-0.7.0 (c (n "doors") (v "0.7.0") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08lqp5jn47gcf84h3d7vjzmaqk385jww1bbjsqg8786djciis3ad")))

(define-public crate-doors-0.7.1 (c (n "doors") (v "0.7.1") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qd9vpzhi9w4pvspvahf7vxvx5xhw6d18797kal2a06ijbld9bws")))

(define-public crate-doors-0.7.2 (c (n "doors") (v "0.7.2") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05cws6qi72q8llnws5hzc9fpnw3r2na60g9kzsjjfchg8g3zjblh")))

(define-public crate-doors-0.8.1 (c (n "doors") (v "0.8.1") (d (list (d (n "door-macros") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yfl9hsdskjpw42ga9yx8irrlxzgk6fnvkn0fjmrd56crsn3242p")))

