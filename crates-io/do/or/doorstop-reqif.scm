(define-module (crates-io do or doorstop-reqif) #:use-module (crates-io))

(define-public crate-doorstop-reqif-0.1.0 (c (n "doorstop-reqif") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doorstop-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "reqif-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0fnnc38qgc6mf2dcrb7kn8qjj1lyi5dmbyv3mr8lylrax2h3k55q")))

(define-public crate-doorstop-reqif-0.1.1 (c (n "doorstop-reqif") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doorstop-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "reqif-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0j20hy974x0a4j19kg253x7h0xn6gx844kz596q2d83xwprz7n2q")))

(define-public crate-doorstop-reqif-0.1.2 (c (n "doorstop-reqif") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doorstop-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "reqif-rs") (r "^0.1.2") (d #t) (k 0)))) (h "1di3srg4frj4hzhfibaaxq01py0l1mvj8axf3i7hgwmivr10fair")))

(define-public crate-doorstop-reqif-1.0.0 (c (n "doorstop-reqif") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doorstop-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "reqif-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0c6kryj2r0dh6z3is31bymwjzkf6b422rhf15gjyj3a3ivva0d0r")))

