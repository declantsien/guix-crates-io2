(define-module (crates-io do or door-macros) #:use-module (crates-io))

(define-public crate-door-macros-0.1.0 (c (n "door-macros") (v "0.1.0") (d (list (d (n "doors") (r "~0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0abihah0lhzbcd1ykwp0nd7la6azpip18h9k5zpqw7hh2izcmhra")))

(define-public crate-door-macros-0.1.1 (c (n "door-macros") (v "0.1.1") (d (list (d (n "doors") (r "~0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "1l13h127azrzcbypww0dpx5i53qla7qabxi5r7bqgmr9fas019gh")))

(define-public crate-door-macros-0.1.2 (c (n "door-macros") (v "0.1.2") (d (list (d (n "doors") (r "~0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "05d7qblbd32fxwfghck46qz5s5mzphfcnjw4g9x7x8f3fhsrz4kq")))

