(define-module (crates-io do ny donyeh) #:use-module (crates-io))

(define-public crate-donyeh-0.1.0 (c (n "donyeh") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hqf2khsn4qk6vfwv7rwssi480max8q8fscsv7i8kpf9wk03jrrk")))

(define-public crate-donyeh-0.1.1 (c (n "donyeh") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19971c88npgacvrlgwss6mharspgwy0qi7zkvjyzx7g2j7b1n63y")))

