(define-module (crates-io do tb dotbits) #:use-module (crates-io))

(define-public crate-dotbits-0.1.0 (c (n "dotbits") (v "0.1.0") (h "1bxd1vfzjk8d4j3zz2n45cqvkl5cl965dvap9nhh3s1rd7cjwhxp")))

(define-public crate-dotbits-0.1.1 (c (n "dotbits") (v "0.1.1") (h "1q1vlwgr75ibsbsjwarfigmk9z3lq00m49yqms1gw953y29afzwm")))

(define-public crate-dotbits-0.1.2 (c (n "dotbits") (v "0.1.2") (h "0lspw9w5s5918la109bh3wnjhwqsf4zd7wwchfpzvqkkg2qc1b8g")))

(define-public crate-dotbits-0.2.0 (c (n "dotbits") (v "0.2.0") (h "0ynacr0p7fd7xy6pf88hhh9cz0jwssk42wdvw9bjc2ik50h5kw5g")))

(define-public crate-dotbits-0.3.0 (c (n "dotbits") (v "0.3.0") (h "1j9vjpcc46madzqv32c4fk4la6q7s7y7s3b606rfr12hzpm95380")))

