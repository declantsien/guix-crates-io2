(define-module (crates-io do ja dojang) #:use-module (crates-io))

(define-public crate-dojang-0.1.0 (c (n "dojang") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0ivihagnwpnjmwmh48kza09b8yav594vp2kpfcn9s81fgl5j73yd")))

(define-public crate-dojang-0.1.1 (c (n "dojang") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0jmwwy9kwb0rv9f298gp87p1hzm3sg4sdp89wz4dqbgxjbsiz5j7")))

(define-public crate-dojang-0.1.2 (c (n "dojang") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "06lsnajbzvlgyl0vjbr6ijp7ggpfj5yyr8p01w7qi5p5fx7h499w")))

(define-public crate-dojang-0.1.3 (c (n "dojang") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0xbsiz1sw16b8ks88rb28f9d2j82668by9qym160fy98vk29vf49")))

(define-public crate-dojang-0.1.4 (c (n "dojang") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1qr94qqqbdm95aj0hrn38q9p73v92bgxyzqr7pyyjp9cxwjjm05c")))

(define-public crate-dojang-0.1.5 (c (n "dojang") (v "0.1.5") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0b9dd632kw9kdx3c3ybljal1phl9xx2bmqrzpaj2wp5xkd4fv9k2")))

(define-public crate-dojang-0.1.6 (c (n "dojang") (v "0.1.6") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0cmwpxf7sv7cirvbnwx618rfs5lw986b8cj4agydbs8hyz598ygh")))

