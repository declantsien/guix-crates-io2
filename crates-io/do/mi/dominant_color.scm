(define-module (crates-io do mi dominant_color) #:use-module (crates-io))

(define-public crate-dominant_color-0.1.0 (c (n "dominant_color") (v "0.1.0") (h "0ak7d360gibm9znkl0xj523zdmf7a9nvigylwcfa7hha3pw0gbz5")))

(define-public crate-dominant_color-0.1.1 (c (n "dominant_color") (v "0.1.1") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 2)))) (h "006jl4icragyszhpqcf9ghnznpk3gi2znkgldfirhqhhwzp4j9k5")))

(define-public crate-dominant_color-0.1.2 (c (n "dominant_color") (v "0.1.2") (d (list (d (n "image") (r "^0.21.2") (f (quote ("jpeg"))) (k 2)))) (h "1rzf9k6q708w8jv7g6zr1026drscz9k8qlcgdwdji2j70ns2hcy7")))

(define-public crate-dominant_color-0.2.0 (c (n "dominant_color") (v "0.2.0") (d (list (d (n "image") (r "^0.21.2") (f (quote ("jpeg"))) (k 2)))) (h "0jzfp5i3nzap559lympbq0zr1ic8c26ayy9n16nk866q4vhgqx4g")))

(define-public crate-dominant_color-0.2.1 (c (n "dominant_color") (v "0.2.1") (d (list (d (n "image") (r "^0.21.2") (f (quote ("jpeg"))) (k 2)))) (h "07x6nnqs10926mg629hb1hg1rgqsasvx1w6llqk29vsfnd1wbgpi")))

(define-public crate-dominant_color-0.2.2 (c (n "dominant_color") (v "0.2.2") (d (list (d (n "image") (r "^0.21.2") (f (quote ("jpeg"))) (k 2)))) (h "1bvjyqvcng0p88rcyjr8na4n48h24b8phcryxnla4j622drfql5j")))

(define-public crate-dominant_color-0.2.3 (c (n "dominant_color") (v "0.2.3") (d (list (d (n "image") (r "^0.23.0") (f (quote ("jpeg"))) (k 2)))) (h "1lmacghwhsjx8w4xcabwbb2jsdzdv2g7jx3nn357g2pwcc7d8hpq")))

(define-public crate-dominant_color-0.2.4 (c (n "dominant_color") (v "0.2.4") (d (list (d (n "image") (r "^0.23.0") (f (quote ("jpeg"))) (k 2)))) (h "03kga3qw5r7m2rxnsr6xgc2w3f0a72gavj45dhhjijsssiz8l1k9")))

(define-public crate-dominant_color-0.3.0 (c (n "dominant_color") (v "0.3.0") (d (list (d (n "image") (r "^0.23.0") (f (quote ("jpeg"))) (k 2)))) (h "15hd62dxxw0ibn6k0b0k2ic0vyviq96ips7mbsvyi1dli7avb514")))

(define-public crate-dominant_color-0.4.0 (c (n "dominant_color") (v "0.4.0") (d (list (d (n "image") (r "^0.24.5") (f (quote ("jpeg"))) (k 2)))) (h "0v85l3if7pwah7d7r1v1pla9x03f0pnn5wcb7blnm245ah04xb54")))

