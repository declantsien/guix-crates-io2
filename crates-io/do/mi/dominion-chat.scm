(define-module (crates-io do mi dominion-chat) #:use-module (crates-io))

(define-public crate-dominion-chat-0.1.0 (c (n "dominion-chat") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dominion") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)))) (h "1lpx7vkg4a9517mraqjnykfjnxmx8kgyj170p35k8x9977hkgkgm")))

(define-public crate-dominion-chat-0.1.1 (c (n "dominion-chat") (v "0.1.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dominion") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)))) (h "0968yx8hfq8bzzpwbgg7mav416bc9d5qk8rvw1fhm0dq1slr6wzv")))

(define-public crate-dominion-chat-0.2.0 (c (n "dominion-chat") (v "0.2.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dominion") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)))) (h "08lv0gcg6ml8lj95hd0kikpmlh2brq55wpfcj5ba5ssp17jgaicb")))

(define-public crate-dominion-chat-0.3.0 (c (n "dominion-chat") (v "0.3.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dominion") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05wcyfyiy7vapxmf5zmv4cz61y01sm6inclhmipfkkdvkk4x3xwh")))

