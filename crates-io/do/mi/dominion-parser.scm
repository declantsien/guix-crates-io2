(define-module (crates-io do mi dominion-parser) #:use-module (crates-io))

(define-public crate-dominion-parser-0.1.0 (c (n "dominion-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gr9kzp1ih85byilfg4yznqw0vl8rw9daldyq0ldapb4728vwwqx")))

(define-public crate-dominion-parser-0.2.0 (c (n "dominion-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03hlj68qnl7zs05iscqy2myd4dmmn95x18gwf8ylk4vzfrs1a6s7")))

(define-public crate-dominion-parser-0.3.0 (c (n "dominion-parser") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h0lzfln392mznxm6dmbmxrndldi53pfwhbybc7cs7a18pqb7xya")))

(define-public crate-dominion-parser-0.4.0 (c (n "dominion-parser") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s88r8q680rg03qpm3ix1s0prcd1xmxxbdcj6s8a9wh3x8v3qamc")))

(define-public crate-dominion-parser-0.4.1 (c (n "dominion-parser") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hxpvvl86mqrn5dgnqy71d7q1rckxmbb8rs6h083m2p3fmfdlb1v")))

(define-public crate-dominion-parser-0.5.0 (c (n "dominion-parser") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.9") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)))) (h "0wxif2nq92dswgx00jfrvdvry2c5mzgrxrbb2qr377g6qf8hwhpa")))

