(define-module (crates-io do mi dominion) #:use-module (crates-io))

(define-public crate-dominion-0.1.0 (c (n "dominion") (v "0.1.0") (d (list (d (n "dominion-parser") (r "^0.2") (d #t) (k 0)))) (h "1h8sfmfkp7rqay7sg1nrkz8114wck8qlmj4w7fxnhsg0lhdfjl84")))

(define-public crate-dominion-0.2.0 (c (n "dominion") (v "0.2.0") (d (list (d (n "dominion-parser") (r "^0.3") (d #t) (k 0)))) (h "11jmka7sq68brrkyiz7v3xx151bmlbj14p8nxaxi1nw47hlpyln4")))

(define-public crate-dominion-0.3.0 (c (n "dominion") (v "0.3.0") (d (list (d (n "dominion-parser") (r "^0.4") (d #t) (k 0)))) (h "1q4myl6jahws6z2kqxawqnd3j1gp18jbhvsgjr55yfxb4dnvhzqd")))

(define-public crate-dominion-0.4.0 (c (n "dominion") (v "0.4.0") (d (list (d (n "dominion-parser") (r "^0.5") (d #t) (k 0)))) (h "0mbmsxds439i3h2wcw9fr5zrybxla9cxw3l5ccdvq951bk4i4aaj")))

