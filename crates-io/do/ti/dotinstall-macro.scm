(define-module (crates-io do ti dotinstall-macro) #:use-module (crates-io))

(define-public crate-dotinstall-macro-0.1.0 (c (n "dotinstall-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "0rrbz8xfxbvpv2810fkiq9v3idwv7j8cvz81j96h6zdylk39i25r")))

(define-public crate-dotinstall-macro-0.1.1 (c (n "dotinstall-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "17sfcmbcc26jk8dzhch84xjznpk5klnz4ipcp1jm7nnwnq7y4cli")))

