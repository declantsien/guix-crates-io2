(define-module (crates-io do ti dotinstall) #:use-module (crates-io))

(define-public crate-dotinstall-0.1.0 (c (n "dotinstall") (v "0.1.0") (d (list (d (n "dotinstall-macro") (r "^0.1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vwb8hiyff3mqjd8kd739dv303zgicz884h3c25ybn2j4fw9yfix")))

(define-public crate-dotinstall-0.1.1 (c (n "dotinstall") (v "0.1.1") (d (list (d (n "dotinstall-macro") (r "^0.1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1wgs5qhbwp1n4wsbfr5m3gf6aw1q5hfarhw72lcp4i4pzb35fbp4")))

(define-public crate-dotinstall-0.1.2 (c (n "dotinstall") (v "0.1.2") (d (list (d (n "dotinstall-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0czynsbbrk97xa4jdlgnbl2pv9px0smybvfsl5rr3jnkgl9wz1w4")))

