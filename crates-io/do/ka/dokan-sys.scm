(define-module (crates-io do ka dokan-sys) #:use-module (crates-io))

(define-public crate-dokan-sys-1.3.1 (c (n "dokan-sys") (v "1.3.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "0z86mnf6w9akxa32rcys4m61mbx7k236lh1bca8zw9cb7qggbnmy") (y #t) (l "dokan")))

(define-public crate-dokan-sys-1.3.1+docfix (c (n "dokan-sys") (v "1.3.1+docfix") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "0wfz7094g8vjzlkcpisg45d1mszd45i2l5hi6y7pg6cp77dzq2na") (y #t) (l "dokan")))

(define-public crate-dokan-sys-0.1.0-alpha.1+dokan131 (c (n "dokan-sys") (v "0.1.0-alpha.1+dokan131") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "0jk3clx968pl5l46fy30g0b0kqnxxmvvs469l6mg76bz21vpsap8") (y #t) (l "dokan")))

(define-public crate-dokan-sys-0.1.0-alpha.2+dokan131 (c (n "dokan-sys") (v "0.1.0-alpha.2+dokan131") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "03ss70zcwih1cs76xsiqby2gr64ngcab6ql83zpy7gmkp9yblg8q") (y #t) (l "dokan")))

(define-public crate-dokan-sys-0.1.0-alpha.3+dokan131 (c (n "dokan-sys") (v "0.1.0-alpha.3+dokan131") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "1n4bx5axc5i3lq4wjw76ilwcima0ff2wkbpm2y8cs625l9jjnlzn") (y #t) (l "dokan")))

(define-public crate-dokan-sys-0.1.0+dokan131 (c (n "dokan-sys") (v "0.1.0+dokan131") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "11xszv1jsyqn27h7x5a469zrk4whm8av3xzsli1hsf0pf22liriz") (l "dokan")))

(define-public crate-dokan-sys-0.1.2+dokan141 (c (n "dokan-sys") (v "0.1.2+dokan141") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "0yyjs2d2k99mixaf5rqr5i6f97hjz4lg2lq04pqn61cs5dw147i5") (l "dokan")))

(define-public crate-dokan-sys-0.1.1+dokan140 (c (n "dokan-sys") (v "0.1.1+dokan140") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "023dvgisqvpssq9i40yzv609iii3a26jylh6r01idjfv6gz51db6") (l "dokan")))

(define-public crate-dokan-sys-0.2.0+dokan150 (c (n "dokan-sys") (v "0.2.0+dokan150") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "1li76ad5l3pfmvb8skwrb91lh35is6a6jsnn36hf8b58q2ikvlii") (l "dokan")))

(define-public crate-dokan-sys-0.3.1+dokan206 (c (n "dokan-sys") (v "0.3.1+dokan206") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "basetsd" "fileapi" "minwinbase" "minwindef" "ntdef" "winnt"))) (d #t) (k 0)))) (h "0s26adaikvwmss32zzw005l2pcsb9njnj67w0k6nfs4bmkl4jzpb") (l "dokan")))

