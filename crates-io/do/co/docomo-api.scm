(define-module (crates-io do co docomo-api) #:use-module (crates-io))

(define-public crate-docomo-api-0.1.0 (c (n "docomo-api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.2") (d #t) (k 0)))) (h "0vnrbwzwjqhcx1kkgas1gqzsgfjjlx0mabh1dwpsy653ixhqbd37")))

