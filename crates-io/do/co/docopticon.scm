(define-module (crates-io do co docopticon) #:use-module (crates-io))

(define-public crate-docopticon-0.1.0 (c (n "docopticon") (v "0.1.0") (d (list (d (n "assert_fs") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cbdld6qxipr2sya2266nckzjvaa95svc9pbqh7b07rmsrjc0ad6") (f (quote (("std" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-docopticon-0.1.1 (c (n "docopticon") (v "0.1.1") (d (list (d (n "assert_fs") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x1b445xvv99fxrvlsw85w744hhpxmzq6wb824mpmx6nfds8vy05") (f (quote (("std" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-docopticon-0.1.2 (c (n "docopticon") (v "0.1.2") (d (list (d (n "assert_fs") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1443n2q2k2nm5hw9lz272rvnd2rajk9axh7knh3in8m2hvnpdrwl") (f (quote (("std" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

