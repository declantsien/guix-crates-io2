(define-module (crates-io do co docopt_macros) #:use-module (crates-io))

(define-public crate-docopt_macros-0.6.8 (c (n "docopt_macros") (v "0.6.8") (d (list (d (n "docopt") (r "~0.6.8") (d #t) (k 0)))) (h "02v68hdi05cshlg58spjr7b19kvw026dryjjzy1jk0facmh8bijx")))

(define-public crate-docopt_macros-0.6.10 (c (n "docopt_macros") (v "0.6.10") (d (list (d (n "docopt") (r "^0.6.10") (d #t) (k 0)))) (h "0d3cr1w94b467c1gdgasrw6iriqmpcq18972wlmdr61dsx4p7nbf")))

(define-public crate-docopt_macros-0.6.11 (c (n "docopt_macros") (v "0.6.11") (d (list (d (n "docopt") (r "^0.6.11") (d #t) (k 0)))) (h "1s58yzfh83irfrjl4ynddiw54c648yqgsvvwsjwbl23a644c242m")))

(define-public crate-docopt_macros-0.6.12 (c (n "docopt_macros") (v "0.6.12") (d (list (d (n "docopt") (r "^0.6.12") (d #t) (k 0)))) (h "00j8c59j92n6wmh0k6v9f7yiqnvj629n8k2rjkv3rjggknn3rnxd")))

(define-public crate-docopt_macros-0.6.13 (c (n "docopt_macros") (v "0.6.13") (d (list (d (n "docopt") (r "^0.6.13") (d #t) (k 0)))) (h "02xj7mfsj04y5nr31zp5aqhvs53f8p01d02cr02f4kig3ic9s1n8")))

(define-public crate-docopt_macros-0.6.14 (c (n "docopt_macros") (v "0.6.14") (d (list (d (n "docopt") (r "^0.6.14") (d #t) (k 0)))) (h "1xygvs4lwm1j20nrnyqw53f0c75amvi26zfw4yxnpj09kasdcbvn")))

(define-public crate-docopt_macros-0.6.15 (c (n "docopt_macros") (v "0.6.15") (d (list (d (n "docopt") (r "^0.6.15") (d #t) (k 0)))) (h "00sg289lqdsbqrh8lbdjxpji61y8v09p4n97nz5xl3wll7qnbzas")))

(define-public crate-docopt_macros-0.6.16 (c (n "docopt_macros") (v "0.6.16") (d (list (d (n "docopt") (r "^0.6.16") (d #t) (k 0)))) (h "05ycnw5c5rnil0nwd9l1hq39b0xckvkjdkwkq0s1iaqw13ah76x1")))

(define-public crate-docopt_macros-0.6.17 (c (n "docopt_macros") (v "0.6.17") (d (list (d (n "docopt") (r "^0.6.17") (d #t) (k 0)))) (h "1il9rd0avijlcwgszl18s7abdhf0c9rnzj7l8n154yqzc4f9zars")))

(define-public crate-docopt_macros-0.6.18 (c (n "docopt_macros") (v "0.6.18") (d (list (d (n "docopt") (r "^0.6.18") (d #t) (k 0)))) (h "0s6rmkhr9aaihiyy8fql9yg83x2dv29kqi21b4336mpw5gsvi1dv")))

(define-public crate-docopt_macros-0.6.19 (c (n "docopt_macros") (v "0.6.19") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "10h1bqvdv5147pw60q87gah8ny7q3vrxnglljy4h8whgc7gn51cb")))

(define-public crate-docopt_macros-0.6.20 (c (n "docopt_macros") (v "0.6.20") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "14v7pr83qf8wg0ymy7indba0pk8sglq2ikfvbzb45xpsikjn9kj9")))

(define-public crate-docopt_macros-0.6.21 (c (n "docopt_macros") (v "0.6.21") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "08l8hjyidbyrq9wdi0qjpbcaldsc4jhfkj40dqlr3ad1dlzsa8gd")))

(define-public crate-docopt_macros-0.6.22 (c (n "docopt_macros") (v "0.6.22") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "1pc2gma5mj2akp0jlrika611rmnzsclwb9plxp9m1k048mf13vpy")))

(define-public crate-docopt_macros-0.6.23 (c (n "docopt_macros") (v "0.6.23") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "1rg4fs561n1p4xszarci0cmn57nqlqc9f07y3db267b9nsqk0ywg")))

(define-public crate-docopt_macros-0.6.24 (c (n "docopt_macros") (v "0.6.24") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "1mamlb39c4857zd6y2lnf53gz2hhlhl6rdn2av7y6yq6nds668g7")))

(define-public crate-docopt_macros-0.6.25 (c (n "docopt_macros") (v "0.6.25") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "1br0lj42rzp88jfn4xn3vzhh9y3q87y3z9ix6indbr3l4sp6vbfb")))

(define-public crate-docopt_macros-0.6.26 (c (n "docopt_macros") (v "0.6.26") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "1ixg6wpd5d6hrh9vyhlp0d1gnbxpybx1nk198dhwrxm4mjbw6bls")))

(define-public crate-docopt_macros-0.6.27 (c (n "docopt_macros") (v "0.6.27") (d (list (d (n "docopt") (r "^0.6.19") (d #t) (k 0)))) (h "01xgdzwsaibfgvl1311hz8fiwsr3f8g3hhz1ix9zzjlfm6frsdsl")))

(define-public crate-docopt_macros-0.6.28 (c (n "docopt_macros") (v "0.6.28") (d (list (d (n "docopt") (r "*") (d #t) (k 0)))) (h "0gz7ps19x5my0vqxhd4qpiybl8ppaj8vasmpk65wdnf996m909xf")))

(define-public crate-docopt_macros-0.6.29 (c (n "docopt_macros") (v "0.6.29") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1rrlgzgxsn1b6c4fqlzqzynkyswrpz900r1f67ph66ksy5w17nf2")))

(define-public crate-docopt_macros-0.6.30 (c (n "docopt_macros") (v "0.6.30") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0f8ajxghmdrgll8nq30q4akbn8hj5fzhlpa89g66jkg5za9c2aqa")))

(define-public crate-docopt_macros-0.6.31 (c (n "docopt_macros") (v "0.6.31") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0rpj2bjhmgfjl0ysk5wr6hdf1ldrzh1pknsadzsywa0z347z825g")))

(define-public crate-docopt_macros-0.6.32 (c (n "docopt_macros") (v "0.6.32") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1vz7y00d24km05qr6p0pa9w2hjh6nka91z90aclclzj2v9910wpw")))

(define-public crate-docopt_macros-0.6.33 (c (n "docopt_macros") (v "0.6.33") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0571bfdys9527xxm8j6dw2f72sihbpnizz2zsi3rn96fkpjsi4z7")))

(define-public crate-docopt_macros-0.6.34 (c (n "docopt_macros") (v "0.6.34") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1fahn8yzh430zy2aw0d4hd8jf3672lh919xhqxnwali3vgbai9gh")))

(define-public crate-docopt_macros-0.6.35 (c (n "docopt_macros") (v "0.6.35") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "030c52psz69hq3r4s7hcrpwvnrax8f423h2s8alvdd1z9hxgqdqy")))

(define-public crate-docopt_macros-0.6.36 (c (n "docopt_macros") (v "0.6.36") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0x8wp3wvp360hfg9grjpmn3gjb1b708w03y6aqk1l66x5pwy7jn0")))

(define-public crate-docopt_macros-0.6.37 (c (n "docopt_macros") (v "0.6.37") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1iacmp88j60355piafxzi5xrsq2jk0qz77jv9dwhnwi8v10xdk6c")))

(define-public crate-docopt_macros-0.6.38 (c (n "docopt_macros") (v "0.6.38") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "04pb51awf0qfdikbhp5fqajln4nsg211w4lckl9l7pidxrqks6r5")))

(define-public crate-docopt_macros-0.6.39 (c (n "docopt_macros") (v "0.6.39") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1g8m2alxwq9b3l5qsr204xsifr1244ppmi0q996z3knyapchx04k")))

(define-public crate-docopt_macros-0.6.40 (c (n "docopt_macros") (v "0.6.40") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0mmdbbk6rymlafnxvjmm13v63gjpjkhnp0z8cwbvfw760qz22h5i")))

(define-public crate-docopt_macros-0.6.41 (c (n "docopt_macros") (v "0.6.41") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "01xldgdh38gk7ckmxfkklzsniv8nr419zi7h62fmzpyl1gi2mhqm")))

(define-public crate-docopt_macros-0.6.42 (c (n "docopt_macros") (v "0.6.42") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.0") (d #t) (k 2)))) (h "07a7miff8m8b27adwmg2lpl9lx0dr6qs6p6x5c0jvp25k1q00wfq")))

(define-public crate-docopt_macros-0.6.43 (c (n "docopt_macros") (v "0.6.43") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1qq1a18phws9vsvh54nzjh0kk532c4w959qxf3a4cxvqp9s4s894")))

(define-public crate-docopt_macros-0.6.44 (c (n "docopt_macros") (v "0.6.44") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0shzggchlnrl8i559xamyjb6gxb7sbq7lvdjgqnb24qh4hq0grj2")))

(define-public crate-docopt_macros-0.6.45 (c (n "docopt_macros") (v "0.6.45") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0lc23s8w5p6r5g734r9059v43x8zrdkgwcnsl03zxpwnn49zhpb9")))

(define-public crate-docopt_macros-0.6.47 (c (n "docopt_macros") (v "0.6.47") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0hd2amz55hjrh0i2vy97ck3sfc26769slv5cx3r8mfxp4dxff4cg")))

(define-public crate-docopt_macros-0.6.49 (c (n "docopt_macros") (v "0.6.49") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "099286vabhy24piqv0za1fjxmrqrmj076756pszlvkimxcjlsbjv")))

(define-public crate-docopt_macros-0.6.50 (c (n "docopt_macros") (v "0.6.50") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0c0vd7b53i80jxx37xypirrzwnd0zck41wpb2ly80kqw92h5x9nl")))

(define-public crate-docopt_macros-0.6.51 (c (n "docopt_macros") (v "0.6.51") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1izr86qqzwnphhi8zsicz4jzrcvdg7s7c1wy1qn7yvisy82jgsba")))

(define-public crate-docopt_macros-0.6.52 (c (n "docopt_macros") (v "0.6.52") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1ln43b62rls4rhpqcnac28kcq7ravx5nxd0qimliy81l69kzg4sn")))

(define-public crate-docopt_macros-0.6.53 (c (n "docopt_macros") (v "0.6.53") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1k6cjma5iqiv4627n9l0wj0jm146cxzvhr5bcigh52nap4dnilcv")))

(define-public crate-docopt_macros-0.6.54 (c (n "docopt_macros") (v "0.6.54") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1axqhh1zlqi9ymp39lc9gsngikfc1y5k8qd11zlg04047immpl5h")))

(define-public crate-docopt_macros-0.6.55 (c (n "docopt_macros") (v "0.6.55") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "04n3mylvcf2a69idpnzvic4d34mr49i6f1ayw1w0mh6wq6rj2bxc")))

(define-public crate-docopt_macros-0.6.56 (c (n "docopt_macros") (v "0.6.56") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "12w2cbgj85xb511xfy235wjkvpp3lyh0y6cp1sqjpyhm7bljvhcn")))

(define-public crate-docopt_macros-0.6.57 (c (n "docopt_macros") (v "0.6.57") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0a9i1vh58wgj8s1zyir8kkpmn8vfzdp92bqbla1vdvhi5njlbpvw")))

(define-public crate-docopt_macros-0.6.58 (c (n "docopt_macros") (v "0.6.58") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "00cqp4f4z4c54cfsb1vgb7h3hvkmx9jqj9795f9zjmmxl2ddjydb")))

(define-public crate-docopt_macros-0.6.59 (c (n "docopt_macros") (v "0.6.59") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0k04399gv87l6bplbm3dwiksxalcgd02jxvgj12lq4s0czz1fq6d")))

(define-public crate-docopt_macros-0.6.60 (c (n "docopt_macros") (v "0.6.60") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1lligzwgjsx3whqzampik4qqags7nqi4678j8jg2bh9s52n3gkiv")))

(define-public crate-docopt_macros-0.6.62 (c (n "docopt_macros") (v "0.6.62") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1gj3d4j96zrckvmslm8j64mamgpnay97kx0igjpmw7nadisz72n9")))

(define-public crate-docopt_macros-0.6.63 (c (n "docopt_macros") (v "0.6.63") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0x9a9qs6zmqhal6d79hrx6hf0x6bf4q662x9rcm25d75q1xrvvv7")))

(define-public crate-docopt_macros-0.6.64 (c (n "docopt_macros") (v "0.6.64") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1lwj76gxkpa7ymzvcrzssfp2mdrcqv89c7d3fiab0g6vzhwsiq6f")))

(define-public crate-docopt_macros-0.6.65 (c (n "docopt_macros") (v "0.6.65") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1zcbdzq0lgz5962hywrqr95z79gfq6j776a97gdz2g7cbfpbgydf")))

(define-public crate-docopt_macros-0.6.66 (c (n "docopt_macros") (v "0.6.66") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1w8mlzag1blnpx16h3cfriqm7d3r71h2cyzj4b2gkqqrpv7gczc1")))

(define-public crate-docopt_macros-0.6.67 (c (n "docopt_macros") (v "0.6.67") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "06nbxnrfmzsma89sr0km3m2k3yzfhahxrgs100ya1cka6p4sb9q9")))

(define-public crate-docopt_macros-0.6.68 (c (n "docopt_macros") (v "0.6.68") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0qcvwsknnr2h6nx7m33a0xhprga2ygbcx7058zw3shkynq4y2bxf")))

(define-public crate-docopt_macros-0.6.69 (c (n "docopt_macros") (v "0.6.69") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0n6nbl49vyjv1apy5bzs6d4afipzz4wq6hksg4813pvx65m93hms")))

(define-public crate-docopt_macros-0.6.70 (c (n "docopt_macros") (v "0.6.70") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1z7xdbvbnxl29bqna09pck3bh9pl36sgz6s0lrgj42fa5vzwdr0y")))

(define-public crate-docopt_macros-0.6.71 (c (n "docopt_macros") (v "0.6.71") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0lzxvzbmm0nyq0qkilgjzm6zdl452mwnrm5634jc9prlrjdf5k5s")))

(define-public crate-docopt_macros-0.6.72 (c (n "docopt_macros") (v "0.6.72") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1646sh6mkz9dafn7pzw3hzml5mpjbwj3qas3v2cwz050h4l889p8")))

(define-public crate-docopt_macros-0.6.73 (c (n "docopt_macros") (v "0.6.73") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0ck88rvimqyml1kpzk340za7485m0viks4waff9p2ac8fkhb0asd")))

(define-public crate-docopt_macros-0.6.74 (c (n "docopt_macros") (v "0.6.74") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "18pdk4b5c7ywqil9r37njh17hbp4jv6fkm6xhclglsl5v819sf1n")))

(define-public crate-docopt_macros-0.6.75 (c (n "docopt_macros") (v "0.6.75") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1hl3b2rivynk8m90a6aj51gzy5643k54jbbbp2rn5xl77gf3jlz1")))

(define-public crate-docopt_macros-0.6.76 (c (n "docopt_macros") (v "0.6.76") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0hcyqf983pl5cwn9g7zr7qz4rflhw1aqlc8hmh8mybxd9kaz0zp2")))

(define-public crate-docopt_macros-0.6.78 (c (n "docopt_macros") (v "0.6.78") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0ly7zdjcqh298pkr192pn3zmlqfhjzqz2v0qy5i88h0jvfq0bcrh")))

(define-public crate-docopt_macros-0.6.79 (c (n "docopt_macros") (v "0.6.79") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0vl6640wxjjyfffg1qlk7fg4b2q9r12ybja6ribgxlyl418b64hj")))

(define-public crate-docopt_macros-0.6.80 (c (n "docopt_macros") (v "0.6.80") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "003xm7simifmj8c3n3xbi9sii3ng6vv6zkjqww1maasmvjsr2qbc")))

(define-public crate-docopt_macros-0.6.81 (c (n "docopt_macros") (v "0.6.81") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1iak7dkjrrg0vjylapxk4ri778kp074g8a794j0nyk6qkfndjsy6")))

(define-public crate-docopt_macros-0.6.82 (c (n "docopt_macros") (v "0.6.82") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "150kkxx4hca6g448qqc0jkqhhbwgm1maanik8qx5p3bap6y1bf4c")))

(define-public crate-docopt_macros-0.6.83 (c (n "docopt_macros") (v "0.6.83") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "15cw7j564g2cmwdiay5bcn7n9gdhvbdi2qrm7zpw4dhqrfmf50xx")))

(define-public crate-docopt_macros-0.6.84 (c (n "docopt_macros") (v "0.6.84") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "06hb8l7f6layvpwpizlhy96rnai0bjrij1vlr5rpiyfvb0vrc1qg")))

(define-public crate-docopt_macros-0.6.85 (c (n "docopt_macros") (v "0.6.85") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0h6q8qmdz30ynd4g8602z9lws0j5nqc6rk2j0cdc9ghb48qamk18")))

(define-public crate-docopt_macros-0.6.86 (c (n "docopt_macros") (v "0.6.86") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1n9a524n3mdxab9dfq7ycmqxf93qgkl5acxp1ihclj05wm1x31n4")))

(define-public crate-docopt_macros-0.6.87 (c (n "docopt_macros") (v "0.6.87") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "136kmwpf562s006vr9prmw0y1kl7wgf0v05rrvi7cjz8viinz5c2")))

(define-public crate-docopt_macros-0.6.88 (c (n "docopt_macros") (v "0.6.88") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0z9j13312ic8pssmi42xy93zv9ns2ihzll91x6vv5rn9cxgsrg5p")))

(define-public crate-docopt_macros-0.7.0 (c (n "docopt_macros") (v "0.7.0") (d (list (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0bq1rnjr8qxa5gqrwn19y86cqh5clkd4kgrdhp4jcf8dmf49nhxc")))

(define-public crate-docopt_macros-0.8.0 (c (n "docopt_macros") (v "0.8.0") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ad9nfh5knllhnzs99y4gzgx2ww0c16dpvnqbg2ichg2vkpc33k7")))

(define-public crate-docopt_macros-0.8.1 (c (n "docopt_macros") (v "0.8.1") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "055nwh0b8c2biml9pckl5dgl6fsff9wk83adwjj8kblr8as0145v")))

