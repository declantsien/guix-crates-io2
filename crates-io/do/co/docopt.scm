(define-module (crates-io do co docopt) #:use-module (crates-io))

(define-public crate-docopt-0.6.8 (c (n "docopt") (v "0.6.8") (h "11zjyn53qh5qwpznsmpr2a0j64plya6fnki7w5is4gpzhlsfaajq")))

(define-public crate-docopt-0.6.9 (c (n "docopt") (v "0.6.9") (h "0kyba7r6zhp8bspbgzsv63xc3y0kk9jjsl9gdizg8v1fhzmc4s0v")))

(define-public crate-docopt-0.6.10 (c (n "docopt") (v "0.6.10") (h "1lnzlijggk02xinqkvgn0mwa681i42ifjishrbmn366v392hf5ql")))

(define-public crate-docopt-0.6.11 (c (n "docopt") (v "0.6.11") (h "0qx1p9qyhfqrv48h0z5lcnmcqwkjxp7x9x38sd3m08sfjayk1q3z")))

(define-public crate-docopt-0.6.12 (c (n "docopt") (v "0.6.12") (h "02m5q4g031rya7hm84wvnri7p6721h1cqacg5sy2ksk4jxkmxj4p")))

(define-public crate-docopt-0.6.13 (c (n "docopt") (v "0.6.13") (h "1hms7kc35x0980g6xx5pkdxf600n3h02qx4cvddlpmhynlgcib1r")))

(define-public crate-docopt-0.6.14 (c (n "docopt") (v "0.6.14") (h "13l041yassyswr8lghg5sk6q8bzr52wimk7j1qn04635vh390akd")))

(define-public crate-docopt-0.6.15 (c (n "docopt") (v "0.6.15") (h "0dsapxzvby7k7ypp3a3hjbnbis8bkz2597my1hl96srkmwymiz9k")))

(define-public crate-docopt-0.6.16 (c (n "docopt") (v "0.6.16") (h "0ajsa7daqnxd39g8pf5ygdnv5ympwwi5y52kzzfskajv61rcsy3h")))

(define-public crate-docopt-0.6.17 (c (n "docopt") (v "0.6.17") (h "0lsw2xa3qxm8qsg474k1vxwr08x8gjja3gds9ad03kqy2ld90g06")))

(define-public crate-docopt-0.6.18 (c (n "docopt") (v "0.6.18") (d (list (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "1n6ry43lprc42v81w4ibsj9b12rqm0l5l3a0fprk9bihzbr185qs")))

(define-public crate-docopt-0.6.19 (c (n "docopt") (v "0.6.19") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0jr10vw4qh6cm85b2xbk58q6macy33cypiq3ln6bdpxmqqbhbm71")))

(define-public crate-docopt-0.6.20 (c (n "docopt") (v "0.6.20") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1ch715s9h38n7x9x3lsc54824h668fxbcfngrsbgraaghqq3wk9k")))

(define-public crate-docopt-0.6.21 (c (n "docopt") (v "0.6.21") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1837gvnxp4rl07wbj6h3mxhwyrf5hyp9ij3344ir7knyhvh6v661")))

(define-public crate-docopt-0.6.22 (c (n "docopt") (v "0.6.22") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1a16pd13kxfq8mlxz7jxi9j539s3sb8kirnqgd7kx5njpzy3q03x")))

(define-public crate-docopt-0.6.23 (c (n "docopt") (v "0.6.23") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1llkqj09qcf3hgf5j539z4i28vibr5qgg8yfmc2mpnas3xaz3hwp")))

(define-public crate-docopt-0.6.24 (c (n "docopt") (v "0.6.24") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0640wry3sk74v5zvl511jhmysjdm49dw68g7n5dg6lkvlwvnqaqf")))

(define-public crate-docopt-0.6.25 (c (n "docopt") (v "0.6.25") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "16gwbrmvxdg733mr9gjg5v3a211v7ayp26mpynjp55saskxx34jj")))

(define-public crate-docopt-0.6.26 (c (n "docopt") (v "0.6.26") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1nibfvshj1fg88l9qshh39fngq1arv8k73vfm7v33p75mhl9rqnh")))

(define-public crate-docopt-0.6.27 (c (n "docopt") (v "0.6.27") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lx3gh1lf43cwy37lkc3ncmshk5v1nf97f9lpyx8w6hlrx20x9r1")))

(define-public crate-docopt-0.6.28 (c (n "docopt") (v "0.6.28") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0cx50c4fq6hryq9ky2d7ccwklis3n04y4knj4r7mdchfm5ld4qqy")))

(define-public crate-docopt-0.6.29 (c (n "docopt") (v "0.6.29") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1qkzdbg56bxpwsrdqswkmhjw7svvbcqp9skscpchvj1g16sx7wjb")))

(define-public crate-docopt-0.6.30 (c (n "docopt") (v "0.6.30") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1c9lx617d641spb6n6crfwwhpvnwybp1zgqsjw5kx4n5p29ybkhy")))

(define-public crate-docopt-0.6.31 (c (n "docopt") (v "0.6.31") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1s9fmwvrpgxsci4mjv02l55p4xyjw19yqkrfz968ma2b1dabh7qg")))

(define-public crate-docopt-0.6.32 (c (n "docopt") (v "0.6.32") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "05scy7i1ca068ab3kraz6488kci4ggzq7j4b192inpj4hywyr97b")))

(define-public crate-docopt-0.6.33 (c (n "docopt") (v "0.6.33") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1mc26vzvrx6x0jn7jq5940zplj44b86smyh6y2n4864rg30r3b2z")))

(define-public crate-docopt-0.6.34 (c (n "docopt") (v "0.6.34") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0230qpfg515da26nn512mdc3mk8sdfwr5i6cy00c4ld63a03r6xn")))

(define-public crate-docopt-0.6.35 (c (n "docopt") (v "0.6.35") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0a09ggp52lbz4gwfw97z7bljbaq5x2wy4rzd746zgh2c7ad33l3i")))

(define-public crate-docopt-0.6.36 (c (n "docopt") (v "0.6.36") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lw88w1281ig8r86g9pa4aprcafgl0qmvgnq2j41vg2r7i8h3vsr")))

(define-public crate-docopt-0.6.37 (c (n "docopt") (v "0.6.37") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0xbxja7mbczx8lsqa1fl6j03nwymq82p4r3hp5ylzhxf7nhqmic1")))

(define-public crate-docopt-0.6.38 (c (n "docopt") (v "0.6.38") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ybqg2qdhaxh2wsq102p8c6dzb9n9dv4x5vawikmh2j6ym61jjrv")))

(define-public crate-docopt-0.6.39 (c (n "docopt") (v "0.6.39") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ymplrwnz2rvf6yxg21i9xq8jgnnh2pdsl9znxmn38m329qzhmh0")))

(define-public crate-docopt-0.6.40 (c (n "docopt") (v "0.6.40") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lg3gji7d6vi1cyn7aprq4vk3nxqsy7pnghhkirhwamasw8yxi1q")))

(define-public crate-docopt-0.6.41 (c (n "docopt") (v "0.6.41") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.0") (d #t) (k 0)))) (h "1paskvdx4hg5wz3sxgnzzf4h4vr2895p2j09q8rx4i60idslzvp2")))

(define-public crate-docopt-0.6.42 (c (n "docopt") (v "0.6.42") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.0") (d #t) (k 0)))) (h "1irg693wjzyip436p24z4v43vp24d5k8xhnhkqd651j8r7rjcl3f")))

(define-public crate-docopt-0.6.43 (c (n "docopt") (v "0.6.43") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0pkbhh9d0sw3pp1c1pjhj4f3mkpf4vrii5d6kjy7lvcrw5n29bmb")))

(define-public crate-docopt-0.6.44 (c (n "docopt") (v "0.6.44") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1i9wi7ig8a7qb1k01zk6g2plp20gzsi454jjg9x5ddnrmxaq7r0l")))

(define-public crate-docopt-0.6.45 (c (n "docopt") (v "0.6.45") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ckiyvf0nxags9kdkk5cz5r3vs177qnz1w0v47z126j3c7vqv7hs")))

(define-public crate-docopt-0.6.47 (c (n "docopt") (v "0.6.47") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1c0m6iq00i6drh2fxsr5fgg9z1yy1igvvpv83j8q1rz40irk1rx4")))

(define-public crate-docopt-0.6.49 (c (n "docopt") (v "0.6.49") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1n62xwn6g1zd3gg7lbb1k18gw1r9klbn07cpzn9dk8y5fm9fyn13")))

(define-public crate-docopt-0.6.50 (c (n "docopt") (v "0.6.50") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "16cjr71fddix0rrmw0j0i406i6bpi06y414mjvdr1ggsrh49h4bz")))

(define-public crate-docopt-0.6.51 (c (n "docopt") (v "0.6.51") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qza44b8kbl4ki1zyqm66gly0slsags444af2y8qdxn9pjq1mbzw")))

(define-public crate-docopt-0.6.52 (c (n "docopt") (v "0.6.52") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14w57mw8bjpgn8ngpnayd548pnzmp39w67mbr20cgk2ryhddf9a3")))

(define-public crate-docopt-0.6.53 (c (n "docopt") (v "0.6.53") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vczvj4kd5bibpxwknihs25chp0kc2g7q1m6402yrbxxawgayzkr")))

(define-public crate-docopt-0.6.54 (c (n "docopt") (v "0.6.54") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1xmy71sh181mqphbdhl99s0mjbzz132xz90rs9asml794scsx975")))

(define-public crate-docopt-0.6.55 (c (n "docopt") (v "0.6.55") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0infpz3v7il312g1m4va1lxbpih8qypslllsra3q6mrkdndvj9vp")))

(define-public crate-docopt-0.6.56 (c (n "docopt") (v "0.6.56") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0nl7bwpdzbncnda5yhqzmjawwv9hng808vnjq44rv20mrjgvr4vw")))

(define-public crate-docopt-0.6.57 (c (n "docopt") (v "0.6.57") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "04gigb422gm1vjhhxx7rh5bg0amldminscmypqiz0m1f6pk33ygg")))

(define-public crate-docopt-0.6.58 (c (n "docopt") (v "0.6.58") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dly2wj6572jr86y2m8wkswzddsffiqsw987vsraikrrbk9vi8wa")))

(define-public crate-docopt-0.6.59 (c (n "docopt") (v "0.6.59") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ixyf8vqb6nfw4gwyrqym1143rigp2d4y0yjs4lrw1q3wnclwxd4")))

(define-public crate-docopt-0.6.60 (c (n "docopt") (v "0.6.60") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0r6xav7f4c62vfflwcbnfnwswvbdh0spc2mkpim75ndp1jb72nqq")))

(define-public crate-docopt-0.6.61 (c (n "docopt") (v "0.6.61") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03gjqc5477rw6aj0zi23zpfh3870jb6afrg8dmwq4s6a5krsq699")))

(define-public crate-docopt-0.6.62 (c (n "docopt") (v "0.6.62") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0p20p7zxcwl5j8fa3mlrljfl4pr7ngvc6c2qa1290qks49qrx26g")))

(define-public crate-docopt-0.6.63 (c (n "docopt") (v "0.6.63") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ydmlyj9av3xkkabkvnrm80jciyjdljm0dzz4fs2fmsk5zgq5d6h")))

(define-public crate-docopt-0.6.64 (c (n "docopt") (v "0.6.64") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "1mfi6pmg7fq7iv3l3jq3daybsgyv71m0szgsybg68ig9z4kjlg42")))

(define-public crate-docopt-0.6.65 (c (n "docopt") (v "0.6.65") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "1392f9znf2nmp3cqw8wxhdql6kjcy6ik2c4y9hg00bnszkj6cx87")))

(define-public crate-docopt-0.6.66 (c (n "docopt") (v "0.6.66") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0rbsxz0qx2k4b2sj5knn1dqj3bmdv3bwl6w6ys5h77sw0rpa654l")))

(define-public crate-docopt-0.6.67 (c (n "docopt") (v "0.6.67") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0lxj9l91ir9xy4b1gvy2c01pia4zida0wjskpl6iw4a5pa6gh0as")))

(define-public crate-docopt-0.6.68 (c (n "docopt") (v "0.6.68") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0abwndljid4dlm4nmiyvhcn810pmzcyqdihlvc9nrc9a2w6y30qp")))

(define-public crate-docopt-0.6.69 (c (n "docopt") (v "0.6.69") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0agpxw12il3py3llmbhrzb40391pq4xavml5lnqn5aj3s4wmmwh2")))

(define-public crate-docopt-0.6.70 (c (n "docopt") (v "0.6.70") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "1kyawdlypz89pv7gs19lsyabd5a7300sp51r570x2ffc2ib51l2b")))

(define-public crate-docopt-0.6.71 (c (n "docopt") (v "0.6.71") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "12m8vh58ca1sna3w1ssqq1xvzhj3lc6jizwn64hvldc39nm3592n")))

(define-public crate-docopt-0.6.72 (c (n "docopt") (v "0.6.72") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0ncd7bx0d6wmsdvmjbyzk0g16agyppl4rg7m3cwdjhg4m9gqnja9")))

(define-public crate-docopt-0.6.73 (c (n "docopt") (v "0.6.73") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "1hmldn0h80z6j23rp1qkc6gzbky7r3l89ddr55xbz98igyzvv318")))

(define-public crate-docopt-0.6.74 (c (n "docopt") (v "0.6.74") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "02x8i7g3kzzwgfa3xixd2cf4v2rnb318d237sgwaxii1gb4d29ag")))

(define-public crate-docopt-0.6.75 (c (n "docopt") (v "0.6.75") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0z5cjr7n1bhmb8lh0h57ac8lchzgcrvid1cmnp0md4p4cn7d3mv2")))

(define-public crate-docopt-0.6.76 (c (n "docopt") (v "0.6.76") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "11c5qi9qz8l8fdhlkglb2bh9y3j1p1dbhy1cdr16bbgrgl53cigr")))

(define-public crate-docopt-0.6.77 (c (n "docopt") (v "0.6.77") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "01d72pbk8as6xdvc7ia6d6sl14zh94if1jbv0lvrnhh2ml3rdmg4")))

(define-public crate-docopt-0.6.78 (c (n "docopt") (v "0.6.78") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0mb16vdfnragxahcj42kjhlv0m7qhf48723qw6imaglkpfjjqyam")))

(define-public crate-docopt-0.6.80 (c (n "docopt") (v "0.6.80") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0xkswpzm9r3kwjjc6a3lqa0mi27y6ajbliqxl6jwca08rssarh2c")))

(define-public crate-docopt-0.6.81 (c (n "docopt") (v "0.6.81") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0a9qnz2lj3ifn36npm9i301vrcjrav0n5sr6r0k3z3bdci85qfrj")))

(define-public crate-docopt-0.6.82 (c (n "docopt") (v "0.6.82") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.3") (d #t) (k 0)))) (h "0zfkn9fl5xk81qhhi6y9kdgnj2yyk48a1m3arzf4brdljdh0284g")))

(define-public crate-docopt-0.6.83 (c (n "docopt") (v "0.6.83") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "0fwc1k0qgzh0yk28v0qhpq61jfmp6ljprm1p1i0n38r3g03wchpw")))

(define-public crate-docopt-0.6.84 (c (n "docopt") (v "0.6.84") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "0p0yygfgz66w7nynggpm4cmd38xlkvyplmi2ii6z86cpin2lxgaw")))

(define-public crate-docopt-0.6.85 (c (n "docopt") (v "0.6.85") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "1y8bgxpdbf1ij3j3vwwh3lacgf5rhmwy2f3j1rbwa8a0cy1xg20v")))

(define-public crate-docopt-0.6.86 (c (n "docopt") (v "0.6.86") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.5") (d #t) (k 0)))) (h "1nf4f4zf5yk0d0l4kl7hkii4na22fhn0l2hgfb46yzv08l2g6zja")))

(define-public crate-docopt-0.7.0 (c (n "docopt") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "1n6gbhsks2w9y0b4bwqyawh4ghbkka09w6pjcrq9i1sd51pflcmb")))

(define-public crate-docopt-0.8.0 (c (n "docopt") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "0h5lqm47vp5khvch9p9jr3sixxgbwf93jdb23z3cawm7x3p0ir33")))

(define-public crate-docopt-0.8.1 (c (n "docopt") (v "0.8.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "0ykq95xrg8l0c0n37q13rrm6rji8x0yw8569zi25aglbixqr6nrv")))

(define-public crate-docopt-0.8.2 (c (n "docopt") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "128qsa5q692y95wixizmplhvh0c201a8nixjp2xq5sbk10lgbxml") (y #t)))

(define-public crate-docopt-0.8.3 (c (n "docopt") (v "0.8.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.6") (d #t) (k 0)))) (h "0jha611mffc2qnxvdl3pmglz07akl99lk1vihhb3nl1cd69x7b6q")))

(define-public crate-docopt-0.9.0 (c (n "docopt") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.7") (d #t) (k 0)))) (h "1zzqvk7wfzgqdgvqng6wnzgv9rrhpprpiwg9khy5qqchlm4fi8m7")))

(define-public crate-docopt-1.0.0 (c (n "docopt") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.7") (d #t) (k 0)))) (h "0aqybssp3krn7b0f6sh90l5zr437nkrghp2psgxzzikgqd8bfzz6")))

(define-public crate-docopt-1.0.1 (c (n "docopt") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.7") (d #t) (k 0)))) (h "1h1s3lg61yivsyqrsll0fwj0zfjmkzbryh2bq7napanzf3gr436n")))

(define-public crate-docopt-1.0.2 (c (n "docopt") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.7") (d #t) (k 0)))) (h "09djlzwkdmmic3jfinr29v25v0x8m6b8fchyzh3p4nwvaz10cafv")))

(define-public crate-docopt-1.1.0 (c (n "docopt") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "0s9rcpmnnivs502q69lc1h1wrwapkq09ikgbfbgqf31idmc5llkz")))

(define-public crate-docopt-1.1.1 (c (n "docopt") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (f (quote ("std" "unicode"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "07x5g52n6fzilxhk5220caznkvdnzzvahlzrzkmgj8y88sc12gvz")))

