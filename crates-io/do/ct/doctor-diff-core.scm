(define-module (crates-io do ct doctor-diff-core) #:use-module (crates-io))

(define-public crate-doctor-diff-core-0.1.0 (c (n "doctor-diff-core") (v "0.1.0") (d (list (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "007cv3fdc8s6jwigfldjwdq205dml5aagr9zhsqb1q81y4yf7jr8")))

