(define-module (crates-io do ct doctor-syn) #:use-module (crates-io))

(define-public crate-doctor-syn-0.1.0 (c (n "doctor-syn") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l4v2z2km4ncajbznq3k3bwz07d2p407igddia7pyxmfhf06l1lh")))

