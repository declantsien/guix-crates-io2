(define-module (crates-io do ct doctor) #:use-module (crates-io))

(define-public crate-doctor-0.1.0 (c (n "doctor") (v "0.1.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "01214lz8swxkhis7kk9nj5kg42l0i24jvzql76y28pscfxan1mpl")))

(define-public crate-doctor-0.2.0 (c (n "doctor") (v "0.2.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16y9526mm8ni5cb39brcgndvs2qxsxjqzvbn0jq2m7s0rf6lh65x")))

(define-public crate-doctor-0.2.1 (c (n "doctor") (v "0.2.1") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s17plirzay6rm9bawjyvg67dy6kghs5smi8bvqp112n1jz4p96k")))

(define-public crate-doctor-0.2.2 (c (n "doctor") (v "0.2.2") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1j8aq3092w3zbxymvac5787gp8rp92n6mgb2g4mkhgx6m9w2j2nw")))

(define-public crate-doctor-0.3.0 (c (n "doctor") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0yxqym40v4grc0zn1nwcz6fgp7vjdjjq84lf3saybv3i2mm05b5q")))

(define-public crate-doctor-0.3.1 (c (n "doctor") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "13g9mni9q242kbajx8k7nsqybwr1nh2ywgb69jj5s64dnbrbccyv")))

(define-public crate-doctor-0.3.2 (c (n "doctor") (v "0.3.2") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qcfzfqa92kax6sna846n8624ds499hj9gkydcha284wv890khp1")))

(define-public crate-doctor-0.3.3 (c (n "doctor") (v "0.3.3") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1hfrbdg0q2kycdbg8vzvnkj2fz3c0fzk07fv1ncvfx6xjl0q7mb6")))

(define-public crate-doctor-0.3.4 (c (n "doctor") (v "0.3.4") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k8x3rsm31ldv6favdqplsrirh9qsz3aqzryd9b4gd5l6ab81z92")))

