(define-module (crates-io do ct doctor-diff-cli) #:use-module (crates-io))

(define-public crate-doctor-diff-cli-0.1.0 (c (n "doctor-diff-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "doctor-diff-core") (r "^0.1") (d #t) (k 0)))) (h "0jvc0v2l4am0b9awx9imwv1y69rkbjb67hwvyn5wzq2csznsir1d")))

