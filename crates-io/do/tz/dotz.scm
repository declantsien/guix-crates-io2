(define-module (crates-io do tz dotz) #:use-module (crates-io))

(define-public crate-dotz-1.0.0 (c (n "dotz") (v "1.0.0") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)))) (h "1hgx60pm0dcia7kcfb2jhw2mzhb8xhhqdnvaxzcrq3kh9pl4ck4a")))

(define-public crate-dotz-1.0.1 (c (n "dotz") (v "1.0.1") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)))) (h "0swq050n34x3x4hd0d54hk0g08rjnhxhc9kcvc28a37knpndxikj")))

(define-public crate-dotz-1.0.2 (c (n "dotz") (v "1.0.2") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)))) (h "1h8y1i1yvjxq6asqf2glnxl1sqhx60c1kc4nmrj4ryy4h077ll39")))

(define-public crate-dotz-1.0.3 (c (n "dotz") (v "1.0.3") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)))) (h "052mmwn2m1zdpjf6nm5ldz7xlyll50af2i6iqf7zc6ybx5qcbnyp")))

