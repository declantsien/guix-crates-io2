(define-module (crates-io do m- dom-manipulator) #:use-module (crates-io))

(define-public crate-dom-manipulator-0.1.0 (c (n "dom-manipulator") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.27") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (o #t) (d #t) (k 0)) (d (n "matches") (r "^0.1.9") (d #t) (k 0)) (d (n "selectors") (r "^0.22.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.3") (d #t) (k 0)))) (h "072dmf2vvqwnwnxawgpi778nfd0iw7pilr9ax53db23n2bdchb92") (f (quote (("main" "getopts") ("deterministic" "indexmap") ("default" "main"))))))

