(define-module (crates-io do m- dom-ex-machina) #:use-module (crates-io))

(define-public crate-dom-ex-machina-0.0.1 (c (n "dom-ex-machina") (v "0.0.1") (d (list (d (n "cairo-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "gio") (r "^0.7.0") (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (d #t) (k 2)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "webkit2gtk") (r "^0.8.0") (d #t) (k 0)))) (h "03n1sg1d0kdpsp60zjgxyrk27p67y4w2qb62kdc92yrxq4i2ws6v")))

