(define-module (crates-io do m- dom-content-extraction) #:use-module (crates-io))

(define-public crate-dom-content-extraction-0.1.0 (c (n "dom-content-extraction") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "0b0x1irrshvyxp4lzs53hpv9n8f5kpppn3wrf25m83bhb078wax7")))

(define-public crate-dom-content-extraction-0.2.3 (c (n "dom-content-extraction") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "01lpbhc8r4z72nrs5pbjs3z5hiy37r2w326lplgm48nqhhzkbi7q")))

(define-public crate-dom-content-extraction-0.2.6 (c (n "dom-content-extraction") (v "0.2.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "130zp5in5m6c4h145w2kimxm56wcmyndkfxg91lpn52zm4yikj6s")))

(define-public crate-dom-content-extraction-0.2.7 (c (n "dom-content-extraction") (v "0.2.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "083n87yijy9pphzykyn88gm8w5s403mlnr0nb7w5ffkqs8sl0zgd")))

(define-public crate-dom-content-extraction-0.2.8 (c (n "dom-content-extraction") (v "0.2.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "078635vwcq8x3zsdbmbwkh2as6q8imvijjv0xmb41m5l5496cyw1")))

(define-public crate-dom-content-extraction-0.2.11 (c (n "dom-content-extraction") (v "0.2.11") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 2)))) (h "1l76xh9pjd70vh2y3mivhklsaymwk38gpszgj8r9ay79crlkcgpa")))

