(define-module (crates-io do cm docmatic) #:use-module (crates-io))

(define-public crate-docmatic-0.1.1 (c (n "docmatic") (v "0.1.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "090hc4rarfb5wyi1yia52s1m8yz24i15zidqb0sps729i4wmqic5")))

(define-public crate-docmatic-0.1.2 (c (n "docmatic") (v "0.1.2") (d (list (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "1hx85n266lxswqxrbbinqlhi1qcnjgd4cc7v42abg72kmz7fnn4d")))

