(define-module (crates-io do th dothyphen) #:use-module (crates-io))

(define-public crate-dothyphen-0.1.0 (c (n "dothyphen") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "05lsbc01b7wb4lb7nj6yfwg33k4ffh9qg5szf7b6f3jr1x0x0vdd")))

(define-public crate-dothyphen-0.2.0 (c (n "dothyphen") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "01w67nb9pfiii2paw9sbib9b906vll7g1wlc50w2djyzdf3pf045")))

(define-public crate-dothyphen-0.2.1 (c (n "dothyphen") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1cfzcyfk3adv59fpk68b2lgrhiahfr5pnkd7mqb728y5dap7si5y")))

(define-public crate-dothyphen-1.0.0 (c (n "dothyphen") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x9xvi721r8qkmnlpzcq3h35rfip4yklmj060i7n4pzf1cifpvs0")))

