(define-module (crates-io do cd docdoc) #:use-module (crates-io))

(define-public crate-docdoc-0.0.1 (c (n "docdoc") (v "0.0.1") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1a16nbnk8i2sm9sz40ygc5rfl8r8db75lz23hmdisyv4rgi87p4m") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-docdoc-0.0.2 (c (n "docdoc") (v "0.0.2") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "notify") (r "^6.0.1") (o #t) (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1qlr5ldx10ycdc5l3kx7k25cy0wifq1bp2pxm649vw9wb1rg4zgb") (f (quote (("default" "clap" "watch")))) (s 2) (e (quote (("watch" "dep:notify" "dep:notify-debouncer-mini") ("clap" "dep:clap"))))))

(define-public crate-docdoc-0.0.3 (c (n "docdoc") (v "0.0.3") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "notify") (r "^6.0.1") (o #t) (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "18fasx0753rwvh8s4wm591b5kixf1h04q36i3xp4kkp5r9ra12x4") (f (quote (("default" "clap" "watch")))) (s 2) (e (quote (("watch" "dep:notify" "dep:notify-debouncer-mini") ("clap" "dep:clap"))))))

