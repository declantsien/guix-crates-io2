(define-module (crates-io do so doson) #:use-module (crates-io))

(define-public crate-doson-0.1.0 (c (n "doson") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qbi3v1insjv4l0w7p07fxa46y08fqsfzcwv0yzh20pz0ky2jv8i")))

(define-public crate-doson-0.1.1 (c (n "doson") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jl0hp9j3mmf3q9x9yniafylrw41dh5psk09h6m42rxcbrp5kmln")))

(define-public crate-doson-0.1.2 (c (n "doson") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0y75ya63gsv814lcm02rmhcnlrjvcq2hmzdyryrm7jjd113qxnzc")))

(define-public crate-doson-0.1.3 (c (n "doson") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1k83nlj2c7y9ynryfjrzpczpvngvy0v946jwyslmip85bqnkjzp6")))

(define-public crate-doson-0.1.4 (c (n "doson") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1f2jsrayk66ndgvz82vw46w6fv73nj4qdrlx3s8zwz7766715vv3")))

(define-public crate-doson-0.1.5 (c (n "doson") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1xjiq77wag83wgp6i2gcpc2s8l6fsmzwlfvcyb8xxjsyvif1whqi")))

