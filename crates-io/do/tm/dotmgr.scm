(define-module (crates-io do tm dotmgr) #:use-module (crates-io))

(define-public crate-dotmgr-0.1.0 (c (n "dotmgr") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1i64khlj9m3jpy7yglg5y0zjdrk3jx4f0sby2xd2gnw5azfrk21q") (y #t)))

