(define-module (crates-io do tm dotmake) #:use-module (crates-io))

(define-public crate-dotmake-0.1.0 (c (n "dotmake") (v "0.1.0") (h "13hzh9b15yaxkgny2fg1pm3qk0af0bjs15019li7i45ky9b9w6ak")))

(define-public crate-dotmake-0.1.1 (c (n "dotmake") (v "0.1.1") (h "0qyk487y6mmbnhq2c3a97hkysjgxrca6795rbn5z0nxi56p5qv5l")))

