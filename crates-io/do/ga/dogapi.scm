(define-module (crates-io do ga dogapi) #:use-module (crates-io))

(define-public crate-dogapi-1.0.0 (c (n "dogapi") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0r5psv0rd3wyk3i9b2jkq3m2gbg05c40rrw4lpdcfvy6j6m8i1pm")))

