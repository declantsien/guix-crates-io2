(define-module (crates-io do ga doga-multilingual-greetings) #:use-module (crates-io))

(define-public crate-doga-multilingual-greetings-0.1.0 (c (n "doga-multilingual-greetings") (v "0.1.0") (d (list (d (n "doga-reverse-string") (r "^0.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "0xrw7z8zj311f5gr56jlia1hq1w6ydxlyg6algn308fclm69kb3j")))

(define-public crate-doga-multilingual-greetings-0.1.1 (c (n "doga-multilingual-greetings") (v "0.1.1") (d (list (d (n "doga-reverse-string") (r "^0.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "0kyv2lfhr5rmnk7cmk312dnbb74maja79cwnf3406y5anz30aqsb")))

(define-public crate-doga-multilingual-greetings-0.1.2 (c (n "doga-multilingual-greetings") (v "0.1.2") (d (list (d (n "doga-reverse-string") (r "^0.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "1jbpjaysj5y19s5qn9vm6gwmj0rkvqzn1385gypspx4d0k173kgi")))

