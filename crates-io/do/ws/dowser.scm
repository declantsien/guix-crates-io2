(define-module (crates-io do ws dowser) #:use-module (crates-io))

(define-public crate-dowser-0.1.0 (c (n "dowser") (v "0.1.0") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (o #t) (d #t) (k 0)))) (h "0dcsfa8aysx3n2mpmax4xjqz6ipfwn44fsnig0g5fnrsrb5qxi7j") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.1.1 (c (n "dowser") (v "0.1.1") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (o #t) (d #t) (k 0)))) (h "1n53ckaq571c6mka6nj8vrjq2fpzymkn4q7r9h6r77xh744fp4yb") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.2.0 (c (n "dowser") (v "0.2.0") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (o #t) (d #t) (k 0)))) (h "1yslvyfb356xfbmhnyj6fmqrg6dcxff23l1lfa5lkbarl7d3319n") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.2.1 (c (n "dowser") (v "0.2.1") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (o #t) (d #t) (k 0)))) (h "0w8r9amgjkcfha6dsi70kvp8y62lq1w23zphph8c40fd9vmra8m4") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.2.2 (c (n "dowser") (v "0.2.2") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (o #t) (d #t) (k 0)))) (h "0a9ys8zq9rbkx1w1lzsh5jvqxjyxhzabgrlmwzs77hal91a8q1rv") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.2.3 (c (n "dowser") (v "0.2.3") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "1jzg3xxwgdf09i1xp5hdd7y46qva8vr48vif8xc2pl74jw1ii95v") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.2.4 (c (n "dowser") (v "0.2.4") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "125l5ss87q1n5al1bprkdclsfs77n6i2948x37ik314rffgp3p9k") (f (quote (("regexp" "regex"))))))

(define-public crate-dowser-0.3.0 (c (n "dowser") (v "0.3.0") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0xazmvsi7vv942c0zr8g4ckpzi5nzyayfzzj883cw0rarw50pfad") (f (quote (("regexp" "regex")))) (r "1.56")))

(define-public crate-dowser-0.3.1 (c (n "dowser") (v "0.3.1") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0fflnva0qcymjhmmnmy1jv5aln8cj850xwyhnssax7ml8i99ddjk") (f (quote (("regexp" "regex")))) (r "1.56")))

(define-public crate-dowser-0.3.2 (c (n "dowser") (v "0.3.2") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "1210flx6z6vm8zc6j02x8xjy942mp9sjqblzsf96bbnyqdlvl3rm") (f (quote (("regexp" "regex")))) (r "1.56")))

(define-public crate-dowser-0.3.3 (c (n "dowser") (v "0.3.3") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0w5c493plhj6x5bq4h38ypp8flkj7ljbf9g8bnf5wka5a0jfszh9") (f (quote (("regexp" "regex")))) (r "1.56")))

(define-public crate-dowser-0.3.4 (c (n "dowser") (v "0.3.4") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0bdg0likmygn0pkgafcxgw3c3dm6yb6ald01r7saj8jw7khhjjm6") (f (quote (("regexp" "regex")))) (r "1.56")))

(define-public crate-dowser-0.3.5 (c (n "dowser") (v "0.3.5") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "flume") (r "^0.10.9") (k 0)) (d (n "parking_lot") (r "0.11.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0y5li4ah9rb4z3ip91ag6lk3jybqmqqygb3ajqmd91fj0jfhpyml") (f (quote (("regexp" "regex") ("default")))) (r "1.56")))

(define-public crate-dowser-0.3.6 (c (n "dowser") (v "0.3.6") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "crossbeam-channel") (r "0.5.*") (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (o #t) (d #t) (k 0)))) (h "0lb72hp9jb3waf2m3rp18jsp3pwmyrr8kj8bm6nzp23hsibpyryj") (f (quote (("regexp" "regex") ("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.0 (c (n "dowser") (v "0.4.0") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "crossbeam-channel") (r "0.5.*") (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1wycbvbi7cbxrgvm9nx0qwqqp7spr2zji4psxyj2r688pbfsm6ay") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.1 (c (n "dowser") (v "0.4.1") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "crossbeam-channel") (r "0.5.*") (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1wg4qjnjxp8jk7ji92n27jxfgxlh5dczl9yz92md5bmd084jawm7") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.2 (c (n "dowser") (v "0.4.2") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "02s4ijs5xfmpnf1m8w1aq4p7cpmambfw72xflpgkvh3v6i79zzgv") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.3 (c (n "dowser") (v "0.4.3") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "137x8iz09yv8qgvqfxiw00z7mlbnrk891rhwwn1ms894sspmalq9") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.4 (c (n "dowser") (v "0.4.4") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1j36ax01svnwh61y0ywrcriq5nxpv111zkc36l3jcay99ciai699") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.5 (c (n "dowser") (v "0.4.5") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.3.*, >=0.3.3") (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "0wk2zwj0rrp3wbqlqrxawkad5xg11zh2r3bjgrlda2j0q6mjg53n") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.6 (c (n "dowser") (v "0.4.6") (d (list (d (n "ahash") (r "0.7.*") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.3.*, >=0.3.4") (d #t) (k 0)) (d (n "parking_lot") (r "0.12.*") (o #t) (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1fald761y09vnx7wxksg08q02kj5r0z269b7c7wqwzzw0dncc3hd") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.4.7 (c (n "dowser") (v "0.4.7") (d (list (d (n "ahash") (r "=0.7.6") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.3.*, >=0.3.4") (d #t) (k 0)) (d (n "parking_lot") (r "=0.12.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (d #t) (k 0)))) (h "1k06djwfxgb9mrfyhlyga886cdcgjbavy7bysl2mc1lbrqx8821b") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (r "1.56")))

(define-public crate-dowser-0.5.0 (c (n "dowser") (v "0.5.0") (d (list (d (n "ahash") (r "=0.7.6") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.3.*, >=0.3.4") (d #t) (k 0)) (d (n "parking_lot") (r "=0.12.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (d #t) (k 0)))) (h "1nqrnr25pkmwhz1ja9f7cxr3312kgavl9j816a0axvm1l0jj2kdm") (f (quote (("parking_lot_mutex" "parking_lot") ("default" "parking_lot_mutex")))) (y #t) (r "1.56")))

(define-public crate-dowser-0.5.1 (c (n "dowser") (v "0.5.1") (d (list (d (n "ahash") (r "=0.7.6") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.3.*, >=0.3.4") (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (d #t) (k 0)))) (h "1lqnxzbngbniqdrqsgj1h0bgn7zkgw1dh7piz06v5r4cc28rh9qn") (r "1.56")))

(define-public crate-dowser-0.5.2 (c (n "dowser") (v "0.5.2") (d (list (d (n "ahash") (r "=0.7.6") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (d #t) (k 0)))) (h "03xd2spxa09wwl3gd5jx2zyi8mnk4bsm8g7rs9gkrfw52kwz72xy") (r "1.61")))

(define-public crate-dowser-0.5.3 (c (n "dowser") (v "0.5.3") (d (list (d (n "ahash") (r "=0.8.0") (d #t) (k 0)) (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (d #t) (k 0)))) (h "0m087fbk834safxjzl74j8fs3scn5spjkkcq4m613wy6116ws92a") (r "1.61")))

(define-public crate-dowser-0.6.0 (c (n "dowser") (v "0.6.0") (d (list (d (n "ahash") (r "=0.8.0") (d #t) (k 0)) (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)) (d (n "rayon") (r "=1.5.3") (o #t) (d #t) (k 0)))) (h "0a0faypmnpbsfz2qwkkkpqlzqz51yq4cc4g85avhd6aqsdmfih8a") (f (quote (("docsrs") ("default")))) (r "1.61")))

(define-public crate-dowser-0.6.1 (c (n "dowser") (v "0.6.1") (d (list (d (n "ahash") (r "=0.8.0") (k 0)) (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)))) (h "1pisddzgbgk91jp961lmgkhm851ak1rhw5q2kkkhiwmjan9knqvz") (f (quote (("docsrs") ("default")))) (r "1.61")))

(define-public crate-dowser-0.6.2 (c (n "dowser") (v "0.6.2") (d (list (d (n "ahash") (r "=0.8.0") (k 0)) (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)))) (h "0ja0mv8napppa8i9a9q2054mi5yg89gxxax2i5v5i3i5hzps99gj") (f (quote (("docsrs") ("default")))) (r "1.63")))

(define-public crate-dowser-0.6.3 (c (n "dowser") (v "0.6.3") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)))) (h "1gfcwicj0i60cl4v7zn4hwgw760qmvc9jqq77jffia7niqk9flz0") (f (quote (("docsrs") ("default")))) (r "1.63")))

(define-public crate-dowser-0.6.4 (c (n "dowser") (v "0.6.4") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.4.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)))) (h "0z8qf0qy96vv1rqpp8lvkg2pgy9y95qna4aizb2s0f2pqd1avsvy") (f (quote (("docsrs") ("default")))) (r "1.63")))

(define-public crate-dowser-0.7.0 (c (n "dowser") (v "0.7.0") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.4.*") (d #t) (k 2)) (d (n "dactyl") (r "0.4.*") (d #t) (k 0)))) (h "0chjzmdxz6ypfc1vaaphaab21jd5vdnmqrwkjm2q0jr5hf92bdj8") (r "1.63")))

(define-public crate-dowser-0.8.0 (c (n "dowser") (v "0.8.0") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "dactyl") (r "0.5.*") (d #t) (k 0)))) (h "062994rn2rgv69by03jmnkpayyr204haimksm5c4mignj0xb9a2f") (r "1.70")))

(define-public crate-dowser-0.8.1 (c (n "dowser") (v "0.8.1") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "dactyl") (r "0.6.*") (d #t) (k 0)))) (h "1q91215vds6hm0nnx355niflxsj8a32p4dd79glnpi7mgp38cvq8") (r "1.70")))

(define-public crate-dowser-0.8.2 (c (n "dowser") (v "0.8.2") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "dactyl") (r "0.7.*") (d #t) (k 0)))) (h "049vr60gvwbbiaiy9g0hbx3a3wrxqqbx5cff08hbr4g5zhf4xday") (r "1.70")))

(define-public crate-dowser-0.9.0 (c (n "dowser") (v "0.9.0") (d (list (d (n "ahash") (r "0.8.*") (k 0)) (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "dactyl") (r "0.7.*") (d #t) (k 0)))) (h "1byah15fnqak7yhc7m05ww2m1inm41h3vcaf5kngjphq8khs9ga7") (r "1.72")))

