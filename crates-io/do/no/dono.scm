(define-module (crates-io do no dono) #:use-module (crates-io))

(define-public crate-dono-1.0.0 (c (n "dono") (v "1.0.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1gvn27ba2fxqx00xhff9xzz4x2xns43j026whzbrgivcqsy3pkqa")))

(define-public crate-dono-1.1.0 (c (n "dono") (v "1.1.0") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "053jmwzwrjb2d7fb9wzml8q7nxdn1qkxh8qmqdzhqp13adjqvv5v")))

(define-public crate-dono-1.1.1 (c (n "dono") (v "1.1.1") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1a6b33mm8n8kh02x32zg0vma7xamkb3qlgqrpxl67a39afcgs5xq")))

(define-public crate-dono-1.1.2 (c (n "dono") (v "1.1.2") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1s63w6x5spzyll0wdm9crmhb32ivn14gldqb6kk3k897c3kdln1x")))

(define-public crate-dono-1.1.3 (c (n "dono") (v "1.1.3") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1wsj21ki0b3xjy4hflc5m874s212zjysgpvvi1k88mas16arky18")))

(define-public crate-dono-1.1.4 (c (n "dono") (v "1.1.4") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0bnaz7saz84ik2g5ndc4b44d8yafp39w1q0z51x5pjjn8v1ahcf6")))

(define-public crate-dono-1.1.5 (c (n "dono") (v "1.1.5") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01dfa14dvwhryhglh8nz0krkz5w7wg8p1h33x0xw2ii2953dfp66")))

(define-public crate-dono-2.0.0 (c (n "dono") (v "2.0.0") (d (list (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "18wigz5a0iw8mnwcdmcvdp5s7z11p3fjszjvcg5b69darpdf345h")))

