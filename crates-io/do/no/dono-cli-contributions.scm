(define-module (crates-io do no dono-cli-contributions) #:use-module (crates-io))

(define-public crate-dono-cli-contributions-0.1.2 (c (n "dono-cli-contributions") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1mkbs46zij5gdxmcpm2i5wpvaphwrr1w6m90gr98a3ziwrjj75mb")))

