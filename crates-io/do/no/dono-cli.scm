(define-module (crates-io do no dono-cli) #:use-module (crates-io))

(define-public crate-dono-cli-0.1.0 (c (n "dono-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "graphql_client") (r "^0.12.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1xmw3ffxzsryzib0ggai1zjwxq72drgh6dysan78aicy3yvgivmz")))

