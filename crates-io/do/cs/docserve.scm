(define-module (crates-io do cs docserve) #:use-module (crates-io))

(define-public crate-docserve-0.1.0 (c (n "docserve") (v "0.1.0") (d (list (d (n "actix-files") (r "^0") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "0qwr3caxmh089zvbbrrqwfwc0frlwiffmzqdp2kavsbbwkp78w61")))

(define-public crate-docserve-0.1.1 (c (n "docserve") (v "0.1.1") (d (list (d (n "actix-files") (r "^0") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "0lprb1yx3gk4mknip5a31nv6h2424yv2nsrwgcsz8kyp3nvn0dx4")))

