(define-module (crates-io do cs docsify-cli) #:use-module (crates-io))

(define-public crate-docsify-cli-0.1.0 (c (n "docsify-cli") (v "0.1.0") (d (list (d (n "notedown_ast") (r "^0.6") (d #t) (k 0)))) (h "1rdxjm87mdsk35k5dgfziy1qic8gbvz8z6s4l6f74imxfnihfan2") (f (quote (("default"))))))

(define-public crate-docsify-cli-0.1.1 (c (n "docsify-cli") (v "0.1.1") (d (list (d (n "notedown_ast") (r "^0.6") (d #t) (k 0)))) (h "0b2d9mh0npblg8gr65b9vfnyfkyjdi1zilan5fy7pb0fw6dhp542") (f (quote (("default"))))))

(define-public crate-docsify-cli-0.2.0 (c (n "docsify-cli") (v "0.2.0") (d (list (d (n "notedown_ast") (r "^0.6") (d #t) (k 0)))) (h "1rsc9rjyrwvkm2g3s1jp50b1qjvfhndanzhi1w0x682blcgk82b1") (f (quote (("default"))))))

