(define-module (crates-io do cs docsplay-macros) #:use-module (crates-io))

(define-public crate-docsplay-macros-0.1.0 (c (n "docsplay-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "04zjlyjpp40026ja60qsvk1h1p9x4zv4alm62padnbsnq19xgsp3") (f (quote (("std"))))))

(define-public crate-docsplay-macros-0.1.1 (c (n "docsplay-macros") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)))) (h "1mx8zjq8z97xiv4fhpyvsm61cjszx6gdhjvh4sybzykdvczghws6") (f (quote (("std"))))))

