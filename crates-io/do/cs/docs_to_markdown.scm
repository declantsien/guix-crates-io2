(define-module (crates-io do cs docs_to_markdown) #:use-module (crates-io))

(define-public crate-docs_to_markdown-0.1.0 (c (n "docs_to_markdown") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)))) (h "097fi4lws51irb78i04h919nqir84znmzcq67jqh8931brsvk0p3") (y #t)))

(define-public crate-docs_to_markdown-0.1.1 (c (n "docs_to_markdown") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)))) (h "0fb8pmk4wmazjsxza4jpcl9h4kach10v3rxqylzq4r5giv4vklhx") (y #t)))

(define-public crate-docs_to_markdown-0.1.2 (c (n "docs_to_markdown") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)))) (h "17pv7yn0niqrvanx37m5gqn45dvwwn7bmwwj7ipw44npw0pzj2v6")))

(define-public crate-docs_to_markdown-0.1.3 (c (n "docs_to_markdown") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)))) (h "1pdnq17a79yrbm8l8kaqhdrqj6zjwbbijk7hnb2sr7089kj90vh2")))

(define-public crate-docs_to_markdown-0.1.4 (c (n "docs_to_markdown") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)))) (h "0xjvqf3idysnwz361xjl2iyd2g23jcv23v8yfzaxfaljdzyyri8v")))

