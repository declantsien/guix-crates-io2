(define-module (crates-io do cs docsplay) #:use-module (crates-io))

(define-public crate-docsplay-0.1.0 (c (n "docsplay") (v "0.1.0") (d (list (d (n "docsplay-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0b8zf4xsnaqjz0xapnqlhivdz49p9f8y64lmkjhc97hygfikiybp") (f (quote (("std" "docsplay-macros/std") ("default" "std"))))))

(define-public crate-docsplay-0.1.1 (c (n "docsplay") (v "0.1.1") (d (list (d (n "docsplay-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d39ps38ii93c3m969f6igsq8fjqa32j8i77rhvqa5v9mdll3ii1") (f (quote (("std" "docsplay-macros/std") ("default" "std"))))))

