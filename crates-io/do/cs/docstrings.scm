(define-module (crates-io do cs docstrings) #:use-module (crates-io))

(define-public crate-docstrings-0.1.0 (c (n "docstrings") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.0.14") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "00f1fncznjbrqr38gs008wlang19zzdg74g304p7f5yv6rm3f2bx")))

(define-public crate-docstrings-0.1.1 (c (n "docstrings") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.0.14") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1402iwv5k638qfd9ffswcswsmalfz76cp9589g8di4nww2bz442d") (f (quote (("default" "pulldown-cmark"))))))

