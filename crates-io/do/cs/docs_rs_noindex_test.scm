(define-module (crates-io do cs docs_rs_noindex_test) #:use-module (crates-io))

(define-public crate-docs_rs_noindex_test-0.1.0 (c (n "docs_rs_noindex_test") (v "0.1.0") (h "0ihlh0i93qlr67waxkjjmgh0lyc5gmwpiz9cdnqlyndaxwafbp4h")))

(define-public crate-docs_rs_noindex_test-0.2.0 (c (n "docs_rs_noindex_test") (v "0.2.0") (h "1j0vih5r53na48gdp3wvwrk0sx7nhy07pzg1nx08008l2hmnrqd2")))

