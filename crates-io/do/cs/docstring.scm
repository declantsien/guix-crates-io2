(define-module (crates-io do cs docstring) #:use-module (crates-io))

(define-public crate-docstring-0.2.3 (c (n "docstring") (v "0.2.3") (h "0391nq9n23avvifrr4nx3ghhx747hcjy5srjjk4dcs14bixxs5a6")))

(define-public crate-docstring-0.2.4 (c (n "docstring") (v "0.2.4") (h "0phq34l58g01l8hvqb44w1l478sh7qyxhdgwz3gk2awm154h4kky")))

