(define-module (crates-io do cs docs_rs_test_incorrect_lockfile) #:use-module (crates-io))

(define-public crate-docs_rs_test_incorrect_lockfile-0.1.0 (c (n "docs_rs_test_incorrect_lockfile") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1mk7m34dd5q22cri9dqqhz501q1nn4bxrjznirjw2jg6blz5h9v0")))

(define-public crate-docs_rs_test_incorrect_lockfile-0.1.1 (c (n "docs_rs_test_incorrect_lockfile") (v "0.1.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1lin0zpfarz3mdjwfjwwdfbbiyp05dq7zzb15p9plv6z0c28sy96")))

(define-public crate-docs_rs_test_incorrect_lockfile-0.1.2 (c (n "docs_rs_test_incorrect_lockfile") (v "0.1.2") (d (list (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0x36zrb408mw6lvbkig7ipqnisdll2x4jc59siyxjzdfgcf7ai94")))

(define-public crate-docs_rs_test_incorrect_lockfile-0.2.0 (c (n "docs_rs_test_incorrect_lockfile") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_sval"))) (d #t) (k 0)))) (h "0nj5w98n6iacs5yl5y2sn5hm7d9qh3zz0hg9sz0g8dqc8sxjyz9l")))

