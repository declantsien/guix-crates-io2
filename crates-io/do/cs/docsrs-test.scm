(define-module (crates-io do cs docsrs-test) #:use-module (crates-io))

(define-public crate-docsrs-test-0.0.0 (c (n "docsrs-test") (v "0.0.0") (h "072dg58dv0g65kdrfvsdb6z3w2jwqdqf6m51ma24j1342cqhbc60") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

(define-public crate-docsrs-test-0.0.1 (c (n "docsrs-test") (v "0.0.1") (h "14ccq0fgplqfqzd0k72hyb5hwhwvbyidd5anl40cjgk2iasiiq15") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

(define-public crate-docsrs-test-0.0.2 (c (n "docsrs-test") (v "0.0.2") (h "0vznm36a0i6ra5jj3mimy6c0ma5p7lhbliqvkhncwncpypgcmbpk") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

(define-public crate-docsrs-test-0.0.3 (c (n "docsrs-test") (v "0.0.3") (h "0dnpk16qn0jhqx8hvng50cvp9241njnhlgkfkfxllgr8r7x09hd1") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

(define-public crate-docsrs-test-0.0.4 (c (n "docsrs-test") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)))) (h "08bmqp9rj38pd746av9ph7kkm74y50sl0d10xkyq5zbd9fra50m7") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

(define-public crate-docsrs-test-0.0.5 (c (n "docsrs-test") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)))) (h "1pqdhfq50cpn6avfmsl54vmqs699pb92qbg7wnr08z8javm0rn4y") (f (quote (("feature3") ("feature2") ("feature1") ("default" "feature1" "feature3"))))))

