(define-module (crates-io do cs docs_js_injection_test) #:use-module (crates-io))

(define-public crate-docs_js_injection_test-0.1.0 (c (n "docs_js_injection_test") (v "0.1.0") (h "1h8nqxgjg5xgljamfvl1j6i31a1rk1p2cizkzg6wl0880a6bdkvk")))

(define-public crate-docs_js_injection_test-0.1.1 (c (n "docs_js_injection_test") (v "0.1.1") (h "0six6pfgh6ivd5zjkk5q1qm3hxvsr3nb6fqmjia1flk48vq6fhmy")))

