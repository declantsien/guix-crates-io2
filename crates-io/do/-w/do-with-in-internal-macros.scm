(define-module (crates-io do -w do-with-in-internal-macros) #:use-module (crates-io))

(define-public crate-do-with-in-internal-macros-0.1.0 (c (n "do-with-in-internal-macros") (v "0.1.0") (d (list (d (n "do-with-in-base") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "103lmb7qbwkkfqqxgy8i0cgmsc01jwmpsq7ks0qgqpwd549hwczc")))

(define-public crate-do-with-in-internal-macros-0.1.1 (c (n "do-with-in-internal-macros") (v "0.1.1") (d (list (d (n "do-with-in-base") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1fpijmq1vywq030nfk0mq7rg9f5hjwqk8nl508kcv53a35801ddf") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-internal-macros-0.1.2 (c (n "do-with-in-internal-macros") (v "0.1.2") (d (list (d (n "do-with-in-base") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1mlxgxw3ixqpb3z277qdi28ys3a7d61nydgm1a90xzpi7iln5fpl") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-internal-macros-0.1.3 (c (n "do-with-in-internal-macros") (v "0.1.3") (d (list (d (n "do-with-in-base") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "052blvlz0v37kr0892qh12lc6issbpqhqwrx6ff6wmx10kyify21") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-internal-macros-0.1.4 (c (n "do-with-in-internal-macros") (v "0.1.4") (d (list (d (n "do-with-in-base") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0c71k04shr8707wfz2ai40fmy4qpzppnygzh150slniislflm8jp") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-internal-macros-0.1.5 (c (n "do-with-in-internal-macros") (v "0.1.5") (d (list (d (n "do-with-in-base") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "05va5la06chxn5jcb10js5cnbxcn67f9q0hfwqzdjmjf7pc0f2sw") (f (quote (("doc-kludge"))))))

