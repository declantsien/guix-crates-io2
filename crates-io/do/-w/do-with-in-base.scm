(define-module (crates-io do -w do-with-in-base) #:use-module (crates-io))

(define-public crate-do-with-in-base-0.1.0 (c (n "do-with-in-base") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1a75gg04b47gm6pfxijhg8lyyad0n2c0pg1jgak1jlkz499aggs2")))

(define-public crate-do-with-in-base-0.1.1 (c (n "do-with-in-base") (v "0.1.1") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1327xmp1pf1k74fsz2kkdyv0dpk08783blkyrqgvz6mdk7dwpz59") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-base-0.1.2 (c (n "do-with-in-base") (v "0.1.2") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "07887zi1xgb6i5978dagqrz34b4clxd5vkl27mrjrfifyp0hdd3j") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-base-0.1.3 (c (n "do-with-in-base") (v "0.1.3") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1plfq75lkqwfw5q6pnh8yfzqnhpgw30s2572idmqwdhg4gvl7lh7") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-base-0.1.4 (c (n "do-with-in-base") (v "0.1.4") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "020x95mq1gsnk8pacd46nxj7cxv1sqsbb91phipmiijn3sgwqvnc") (f (quote (("doc-kludge"))))))

(define-public crate-do-with-in-base-0.1.5 (c (n "do-with-in-base") (v "0.1.5") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1g8mzicq92bpfza49vl2pbv5cb3swj9bi5x851mhbcdyzqxkq050") (f (quote (("doc-kludge"))))))

