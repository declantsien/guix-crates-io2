(define-module (crates-io do pe dope) #:use-module (crates-io))

(define-public crate-dope-0.1.0 (c (n "dope") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "slab") (r "^0") (d #t) (k 0)))) (h "1s94jka8l2r45hib2my0bnvap9szk9spvcv2wcvrg172d77knpkb")))

