(define-module (crates-io do se dose) #:use-module (crates-io))

(define-public crate-dose-0.1.0 (c (n "dose") (v "0.1.0") (d (list (d (n "dose-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09gzywp756scvw3nhpmj47153ff25qpkr597sqwmfq9p97ggxzy2")))

(define-public crate-dose-0.1.1 (c (n "dose") (v "0.1.1") (d (list (d (n "dose-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1n8ihqh0c2fvm3r28fcvrlf1rxq3n8y4jg527ybcn1nfv72scw8v")))

(define-public crate-dose-0.1.2 (c (n "dose") (v "0.1.2") (d (list (d (n "dose-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xbwngpkqch7pmmyvs8yqsg4qbph3andrrni1j9r1l3318lvjpax")))

(define-public crate-dose-0.1.3 (c (n "dose") (v "0.1.3") (d (list (d (n "dose-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ra53jbmg25agxynvyiyw46npcgd3zvbbgwsjrm9apb9p8k8a7j6")))

