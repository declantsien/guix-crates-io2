(define-module (crates-io do se dose-derive) #:use-module (crates-io))

(define-public crate-dose-derive-0.1.0 (c (n "dose-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wla2mx7y9ldfc6726x4kzcfand04zxsx8bwwmqr0m8bgp6z547s")))

(define-public crate-dose-derive-0.1.1 (c (n "dose-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05jwfbsppwcs1ysr5l5m6d6d60m78k037qwi698g8nb0h2m8nfgr")))

(define-public crate-dose-derive-0.1.2 (c (n "dose-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j97387npm4fdw6w8dia6nfp26f1snr11023zqjk4599ki61r9v9")))

(define-public crate-dose-derive-0.1.3 (c (n "dose-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0cc51azmnl3vypax265w9n3vk6p1na1f4w85rr796ppcbrsrdy")))

