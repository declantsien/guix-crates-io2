(define-module (crates-io do se dose2gmsh) #:use-module (crates-io))

(define-public crate-dose2gmsh-0.1.0 (c (n "dose2gmsh") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1rsi5nr5mkw7v6vpvqmp3s5n81vrpr7gcl44pk90raz723126pi8")))

(define-public crate-dose2gmsh-1.0.0 (c (n "dose2gmsh") (v "1.0.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1kca9y7rcafn6mscr9j7hmph4lw9w508rh1djz1q75y747pcxs1c")))

(define-public crate-dose2gmsh-1.0.1 (c (n "dose2gmsh") (v "1.0.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "0f96206c3yscrc1hsn9v1rg5prph3ly2an3rkzkg4i9zdrl99zs3")))

