(define-module (crates-io do ta dotago) #:use-module (crates-io))

(define-public crate-dotago-0.1.0 (c (n "dotago") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "claims") (r "^0.7") (d #t) (k 2)))) (h "1gfr3bay5snd1jsb5an54a5mdgswawjgj2qqj9w7dh400kybaaw0")))

