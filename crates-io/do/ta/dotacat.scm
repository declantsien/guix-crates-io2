(define-module (crates-io do ta dotacat) #:use-module (crates-io))

(define-public crate-dotacat-0.1.0 (c (n "dotacat") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1z1pbgds1yqfrfz7kjf3v6xhp5s5sjphdw8iklfn7l8h1mqs23kh")))

(define-public crate-dotacat-0.1.1 (c (n "dotacat") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "10f6g6xvxhkddbc06jvywllphld481ylcnpzvpsc00ygrfaxj4bs")))

(define-public crate-dotacat-0.1.2 (c (n "dotacat") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1gqvi5sly2vr07kb7sjp71ypx1zjb6vmv7nvg8q08329pdk6sa9m")))

(define-public crate-dotacat-0.1.3 (c (n "dotacat") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nxzc6yg9lvpc3i8yr7bvm2hcjixy9iamrv5vyy517lnm1c1p6il")))

(define-public crate-dotacat-0.2.0 (c (n "dotacat") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "097br9y28g4y8sj49z28pgbyr55wim3i4pc0ds1vp1af8aw6r4m0")))

(define-public crate-dotacat-0.3.0 (c (n "dotacat") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mz67111fk0q2ikc67r5f6bics338clwd895dq00wjmlmvk9p4rb")))

