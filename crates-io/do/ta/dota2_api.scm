(define-module (crates-io do ta dota2_api) #:use-module (crates-io))

(define-public crate-dota2_api-0.1.0 (c (n "dota2_api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "0kb5688lff53xa5fwj0dxsz0vwi1k97xxb5widlldbm872imsp9p")))

(define-public crate-dota2_api-0.1.1 (c (n "dota2_api") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "0cy4j687a6kxzjzfjlpl0yszwq23ix7wpdidzh3vqc29wphh0zv9")))

(define-public crate-dota2_api-0.2.0 (c (n "dota2_api") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "1xacckaq2divkw0ypc661j5yx2xf9v6wifscn1j7mg413jna7m3m")))

(define-public crate-dota2_api-0.2.1 (c (n "dota2_api") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "063hqmg86jcx03b53p9wbs7ck9r0mfdsv09a11982bc39jby8pvg")))

(define-public crate-dota2_api-0.2.2 (c (n "dota2_api") (v "0.2.2") (d (list (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "1rwif1k0wl54jr5im0j84mfk2hrmixfx6lbg0jhz6cg41mvz05q7")))

(define-public crate-dota2_api-0.2.3 (c (n "dota2_api") (v "0.2.3") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "04629qs4rh81wpg36bc4919f411jgyk9war1dv5448knhrivfzq7")))

