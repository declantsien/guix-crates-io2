(define-module (crates-io do ta dota2cat) #:use-module (crates-io))

(define-public crate-dota2cat-0.2.1 (c (n "dota2cat") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "clap_derive") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07jsiqgq2ldfg46vyw8mfcnlqpkfcsb6i7f4cls4nvazgvzqjpl6")))

(define-public crate-dota2cat-0.2.2 (c (n "dota2cat") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10cp1jvcnjiykq9bnxgkhf5gcgx90fs3jzdrsmms8w1gs4skpb9g")))

(define-public crate-dota2cat-0.2.3 (c (n "dota2cat") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1fiahmywx1zhx8vm6mi2c7ddgzi8yzcix41y31vvvvv98g6sc2qz")))

