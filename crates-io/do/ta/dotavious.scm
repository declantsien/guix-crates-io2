(define-module (crates-io do ta dotavious) #:use-module (crates-io))

(define-public crate-dotavious-0.1.0 (c (n "dotavious") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "11ihp9bhcwn0aqz0gdzhg4q3sh9sqdxw3afhncizlif868dy5kcv")))

(define-public crate-dotavious-0.2.0 (c (n "dotavious") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "0kl8vm6rr6d4d8h7bsljs7xqwy7nsm3a30wcrkl8aqlmj1n9zbkr")))

(define-public crate-dotavious-0.2.1 (c (n "dotavious") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)))) (h "1xi8r29n48fd6m860jqyf7lfm3dfjh0vn8bpw82ygmnllzia87wn")))

