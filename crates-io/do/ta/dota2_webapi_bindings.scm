(define-module (crates-io do ta dota2_webapi_bindings) #:use-module (crates-io))

(define-public crate-dota2_webapi_bindings-0.5.0 (c (n "dota2_webapi_bindings") (v "0.5.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0by869ww00f3jx7zhkb58l0i35bs7zmr6533rwbwdqxpclk32dp9")))

(define-public crate-dota2_webapi_bindings-0.5.1 (c (n "dota2_webapi_bindings") (v "0.5.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0f1mrav6gq9ai6dkyh436v4bc5755g3590pj1wnjys2s6xyzq44q")))

(define-public crate-dota2_webapi_bindings-0.6.0 (c (n "dota2_webapi_bindings") (v "0.6.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_with") (r "^1.5.1") (d #t) (k 0)))) (h "1bq7mbk5g46aj49v9bfbps5rbp3qh025ppf41pz12ng7ffv15qbc")))

