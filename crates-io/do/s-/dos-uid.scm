(define-module (crates-io do s- dos-uid) #:use-module (crates-io))

(define-public crate-dos-uid-0.1.0 (c (n "dos-uid") (v "0.1.0") (d (list (d (n "uid-derive") (r "^0.1.0") (d #t) (k 2) (p "dos-uid-derive")))) (h "0d1ymn112abr9m07iyxlrp9ajydy1dwsk2v1r6yyk3gpjfzkd1n3")))

(define-public crate-dos-uid-0.1.1 (c (n "dos-uid") (v "0.1.1") (d (list (d (n "uid-derive") (r "^0.2.0") (d #t) (k 2) (p "dos-uid-derive")))) (h "0cpggh5px5fz7aw765zbzqnlzn4as134nl6jgsa2qnsm7lhn906y")))

(define-public crate-dos-uid-0.1.2 (c (n "dos-uid") (v "0.1.2") (d (list (d (n "uid-derive") (r "^0.3.1") (d #t) (k 0) (p "dos-uid-derive")))) (h "0zj00dmpk48bzw90mvlpm7s5pw2f2kwhiyi1n5dj96ck4pj0aavi")))

