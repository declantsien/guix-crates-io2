(define-module (crates-io do s- dos-cp-generator) #:use-module (crates-io))

(define-public crate-dos-cp-generator-0.0.1 (c (n "dos-cp-generator") (v "0.0.1") (d (list (d (n "dos-cp") (r "^0.0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "18dh9pww3ancs6z2y5gm6cf2x27qi1xv4s7h782vrbbpg6q0vqqy") (r "1.61")))

(define-public crate-dos-cp-generator-0.0.2 (c (n "dos-cp-generator") (v "0.0.2") (d (list (d (n "dos-cp") (r "^0.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0i3xkb7n9crk2nfmjb7m2z5ryywqfpjp1r4jxhghbpk3ir125ym9") (r "1.61")))

(define-public crate-dos-cp-generator-0.0.3 (c (n "dos-cp-generator") (v "0.0.3") (d (list (d (n "dos-cp") (r "^0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1mkwkcydbhbpqgbgxk4cz6hfrrppn5svrpajf70d9x78l3m6q1wg") (r "1.61")))

(define-public crate-dos-cp-generator-0.1.0 (c (n "dos-cp-generator") (v "0.1.0") (d (list (d (n "dos-cp") (r "^0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1mqi6wzyawr3w0hdjpray0s53lydx19wsysiz49qqcs589hba5pf") (r "1.61")))

(define-public crate-dos-cp-generator-0.1.1 (c (n "dos-cp-generator") (v "0.1.1") (d (list (d (n "dos-cp") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1m99778478hmh7ppzs5zi5p19mmg8v6jf77kfgy13gfsf4lzhgxd") (r "1.61")))

(define-public crate-dos-cp-generator-0.1.2 (c (n "dos-cp-generator") (v "0.1.2") (d (list (d (n "dos-cp") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1d1kykib33r5hlmnicfs1gadcb890hf7ifywb558hzzq6501iaw6") (r "1.61")))

(define-public crate-dos-cp-generator-0.1.3 (c (n "dos-cp-generator") (v "0.1.3") (d (list (d (n "dos-cp") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0c8jzlbyannnr5107d41wc62gshwr3938ji1kpd672w3abqk6c0x") (r "1.61")))

(define-public crate-dos-cp-generator-0.2.0 (c (n "dos-cp-generator") (v "0.2.0") (d (list (d (n "dos-cp") (r "^0.2.0") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "13cxfvm9i1rjxfv3bwns2b91bw7m34nhb9d454rdv3qqf6fq3z6x") (r "1.61")))

(define-public crate-dos-cp-generator-0.2.1 (c (n "dos-cp-generator") (v "0.2.1") (d (list (d (n "dos-cp") (r "^0.2.1") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1a1ickzigsf0ywr909rajdzdpfw9flnvn9b3kca2wrzby0mb6hvs") (r "1.61")))

(define-public crate-dos-cp-generator-0.3.0 (c (n "dos-cp-generator") (v "0.3.0") (d (list (d (n "dos-cp") (r "^0.3.0") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1zxkj21bd8v86k95n6i7m7c1723z264ib4qyzjm38avv4kngbrb2") (r "1.61")))

(define-public crate-dos-cp-generator-0.3.1 (c (n "dos-cp-generator") (v "0.3.1") (d (list (d (n "dos-cp") (r "^0.3.1") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1amchf8731dgpah787i68pxqk10n9x2xm4pa5ypfflnbzpyj5fwc") (r "1.61")))

(define-public crate-dos-cp-generator-0.4.0 (c (n "dos-cp-generator") (v "0.4.0") (d (list (d (n "dos-cp") (r "^0.4.0") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "024nnganjpwxyk7bliy2qg4wwvrwdhag89azb2paw2bin8s3rd56") (r "1.61")))

(define-public crate-dos-cp-generator-0.5.0 (c (n "dos-cp-generator") (v "0.5.0") (d (list (d (n "dos-cp") (r "^0.5.0") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "05fxq628rnd8k8k6y1s4b6r1aiz8gr0lr8g7hwqd6wgdipkgjgdx") (r "1.71")))

(define-public crate-dos-cp-generator-0.5.1 (c (n "dos-cp-generator") (v "0.5.1") (d (list (d (n "dos-cp") (r "^0.5.1") (d #t) (k 0)) (d (n "panicking") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1yfqpkr8nmm0ffv6zsj2fcv27scrsqsc5y7kbhgzkxzx9rjj1rfn") (r "1.71")))

