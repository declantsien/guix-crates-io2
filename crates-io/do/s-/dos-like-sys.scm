(define-module (crates-io do s- dos-like-sys) #:use-module (crates-io))

(define-public crate-dos-like-sys-0.1.0 (c (n "dos-like-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "19dr1kkwcj1nh8bvnmm0igc40qfpjpc2dzzadma3h2skb71zfisy") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-screen-frame") ("default"))))))

(define-public crate-dos-like-sys-0.2.0 (c (n "dos-like-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "1bhsidlh3jmzaxfrlcw4aaivfi3j42ddbiqk5sbpdwkrhnf33r5n") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-screen-frame") ("default"))))))

(define-public crate-dos-like-sys-0.2.1 (c (n "dos-like-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "1153wwlxkb5w9aj6g7q3mm0grnqk5w6kypaw7fb7jpcrpvjx2iin") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-screen-frame") ("default"))))))

(define-public crate-dos-like-sys-0.2.2 (c (n "dos-like-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "0vzycbfnf62iczfln6rlcs0ywnraw03h69fc83hqwqx2j1y0fmn0") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-screen-frame") ("default"))))))

(define-public crate-dos-like-sys-0.2.3 (c (n "dos-like-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "0gpaw498cm5wvfwhamha92v6812bl3h7jb4hq00zx42967j0wkzn") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-system-cursor") ("disable-screen-frame") ("default"))))))

(define-public crate-dos-like-sys-0.3.0 (c (n "dos-like-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.12") (o #t) (d #t) (k 1)))) (h "0l43pam88h6yakhk9p74id7z14fwprjqq679lw9qxvnyzbwfggsj") (f (quote (("use-vcpkg" "vcpkg") ("use-pkgconfig" "pkg-config") ("disable-system-cursor") ("disable-screen-frame") ("default"))))))

