(define-module (crates-io do s- dos-errno-and-panic) #:use-module (crates-io))

(define-public crate-dos-errno-and-panic-0.0.1 (c (n "dos-errno-and-panic") (v "0.0.1") (d (list (d (n "exit-no-std") (r "^0.1.4") (d #t) (k 0)) (d (n "pc-ints") (r "^0.2.0") (d #t) (k 0)))) (h "00zcxs738dhvni2gbxf7ldvy2gcrghwzzkaxma0s6p6hj6nflvw5")))

(define-public crate-dos-errno-and-panic-0.0.2 (c (n "dos-errno-and-panic") (v "0.0.2") (d (list (d (n "exit-no-std") (r "^0.1.4") (d #t) (k 0)) (d (n "pc-ints") (r "^0.2.0") (d #t) (k 0)))) (h "18bff3q7yr5g26ishnplq8c9q66s8jmnvcllyv036yzcy306p1f1") (y #t)))

(define-public crate-dos-errno-and-panic-0.0.3 (c (n "dos-errno-and-panic") (v "0.0.3") (d (list (d (n "exit-no-std") (r "^0.2.0") (d #t) (k 0)) (d (n "pc-ints") (r "^0.3.0") (d #t) (k 0)))) (h "0gc0hqq8jgd78x261diz498w8yg5phprhi0zdkv9cv1fn4lggcpw")))

(define-public crate-dos-errno-and-panic-0.0.4 (c (n "dos-errno-and-panic") (v "0.0.4") (d (list (d (n "exit-no-std") (r "^0.2.0") (d #t) (k 0)) (d (n "pc-ints") (r "^0.3.0") (d #t) (k 0)))) (h "0916ypj090pzib15vk5gx2wll9ljnhd1sklzqnjzb111ca88nsc1") (f (quote (("errno") ("default"))))))

