(define-module (crates-io do s- dos-like) #:use-module (crates-io))

(define-public crate-dos-like-0.1.0 (c (n "dos-like") (v "0.1.0") (d (list (d (n "dos-like-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07jnx8qhzk1fps2j9fjqh63waj871v922bgfwia2n5dpf8bls3l6") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default"))))))

(define-public crate-dos-like-0.2.0 (c (n "dos-like") (v "0.2.0") (d (list (d (n "dos-like-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "00r4vvvkvngshj1d3hnm8c17ygghfinhnaac0rcqkyzxr09aw61c") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default")))) (y #t)))

(define-public crate-dos-like-0.2.1 (c (n "dos-like") (v "0.2.1") (d (list (d (n "dos-like-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "19j05dbrrgimsqybwkxdarks2dk5sxgf14qcpn2zbq9ffsqzc0s1") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default")))) (y #t)))

(define-public crate-dos-like-0.2.2 (c (n "dos-like") (v "0.2.2") (d (list (d (n "dos-like-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0fiqxbqq5cps3x62894kc3gfs0rnlw60yg7rjdkm7p7cj0rkrw0a") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default")))) (y #t)))

(define-public crate-dos-like-0.2.3 (c (n "dos-like") (v "0.2.3") (d (list (d (n "dos-like-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1azzf94bh9gqrrajkfs5vizhmzdqgfscl7k0lsl4k84bzb2zrq26") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default"))))))

(define-public crate-dos-like-0.3.0 (c (n "dos-like") (v "0.3.0") (d (list (d (n "dos-like-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0akrsg2gj6kml86s5b4fbya46lf00rm2p55hvrjh6nj72rhwfjml") (f (quote (("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default"))))))

(define-public crate-dos-like-0.3.1 (c (n "dos-like") (v "0.3.1") (d (list (d (n "dos-like-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1c75nng7lsyyc4sy6q0hagvhypy5rgqji2h6mfpnpph6x9j5782a") (f (quote (("disable-system-cursor" "dos-like-sys/disable-system-cursor") ("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default"))))))

(define-public crate-dos-like-0.4.0 (c (n "dos-like") (v "0.4.0") (d (list (d (n "dos-like-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "13iar0ig20p0avg0zsza8383jy53i0a3w3yksd696crrp08livqz") (f (quote (("disable-system-cursor" "dos-like-sys/disable-system-cursor") ("disable-screen-frame" "dos-like-sys/disable-screen-frame") ("default"))))))

