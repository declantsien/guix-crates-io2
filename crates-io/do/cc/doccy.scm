(define-module (crates-io do cc doccy) #:use-module (crates-io))

(define-public crate-doccy-0.1.2 (c (n "doccy") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1l4qldqpcyb7n1d89h3ri3afyr8f3fgmcx2mbr71b6vcfw03pmn3")))

(define-public crate-doccy-0.2.1 (c (n "doccy") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "106vlpigyshd7hq0bvrmi4mgipy9m27bd2ch3dm4g758yqqrhjdn")))

(define-public crate-doccy-0.3.1 (c (n "doccy") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1cbks5cln8rqncvk7hq67dq84ap0jc2z2np0llzlvcza2zms5zxc")))

(define-public crate-doccy-0.1.3 (c (n "doccy") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1nkv7y6mwkvbni538z4jpb1m2n6z70i598s8w9xfsjry4z8d29xj")))

(define-public crate-doccy-0.2.2 (c (n "doccy") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "060wimw0jw4rpdjy5y5j141yvkcgzzajhhrfr2jzr3cky2lx387m")))

(define-public crate-doccy-0.2.3 (c (n "doccy") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0vcwy73786mw9qlgkzfqp3pqrmp900blzk8d9an9kda8vp3vaji3")))

(define-public crate-doccy-0.3.2 (c (n "doccy") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0ac8sfng4yiazprmpic9dr3d3hxwrh2rinlw00biy3i5jxp7y9x8")))

