(define-module (crates-io do cc docchi_intf) #:use-module (crates-io))

(define-public crate-docchi_intf-0.7.0 (c (n "docchi_intf") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7.0") (d #t) (k 0)))) (h "1b0ycikvxlx3vms93sk723cxpgkrzngqcg9z57sdlsqn0aay17iq")))

(define-public crate-docchi_intf-0.7.3 (c (n "docchi_intf") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7") (d #t) (k 0)))) (h "016rlp8109m16qjx457qvywcjz0lqhxm6qivy3791l8dndnwjslk")))

(define-public crate-docchi_intf-0.8.0 (c (n "docchi_intf") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_core") (r "^0.8") (d #t) (k 0)))) (h "0wzk2rraqzs9csjybzf2fpj39vhdjzpnhipi81dp3x68lv5f18l0")))

