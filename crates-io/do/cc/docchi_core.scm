(define-module (crates-io do cc docchi_core) #:use-module (crates-io))

(define-public crate-docchi_core-0.7.0 (c (n "docchi_core") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.7.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1xjwihfmhyn5ibbga6paazczxr88pv3cpndb77ikfcm0a219gm9y")))

(define-public crate-docchi_core-0.7.3 (c (n "docchi_core") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0n715anacnvv49g7vwmjpdv3xg9yyvi22a7xqk96rgw930dxw5rm")))

(define-public crate-docchi_core-0.8.0 (c (n "docchi_core") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.8") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.8") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "14hbcgdqc4ns7l5and8v3iiaml4xjrn5qfv58yzwrrnxxk21vkjv")))

