(define-module (crates-io do cc docchi_compaction) #:use-module (crates-io))

(define-public crate-docchi_compaction-0.7.0 (c (n "docchi_compaction") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "1xychfb4cm3jy473vv2cx5wm6mk1134qph8nmb2268mdg3svsj52")))

(define-public crate-docchi_compaction-0.7.3 (c (n "docchi_compaction") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "1vkjsmqbrcwv2aiq7wmcafspvar86i3k5l1pfzjkxzd0zfswj2ym")))

(define-public crate-docchi_compaction-0.8.0 (c (n "docchi_compaction") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "1rj4gl9ia1rngyfx7p2hzhnv73kibzgfmw74kqm6w1h8hx1bi4bb")))

