(define-module (crates-io do cc docchi_archiver2) #:use-module (crates-io))

(define-public crate-docchi_archiver2-0.7.0 (c (n "docchi_archiver2") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "0gxsv69117gp8n2p9qhxj2bnb05xrj4mw96x36gjyx4g80s5p4g0")))

(define-public crate-docchi_archiver2-0.7.3 (c (n "docchi_archiver2") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "0i0lisfci0gs6is3zy7n0kb0mf6wmvs7allql4xc98vdgnnd1gy9")))

(define-public crate-docchi_archiver2-0.8.0 (c (n "docchi_archiver2") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.8") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "0gzjjl449108v30rzh5ja5sm3k5zp3x3sxrqvkn2r33pkbqbhrwa")))

