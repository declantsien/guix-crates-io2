(define-module (crates-io do cc docchi_diff) #:use-module (crates-io))

(define-public crate-docchi_diff-0.7.0 (c (n "docchi_diff") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "101dmk4gp9hp8wj0aswpxn174qfdww6sv6s6cdxm92llsql2kwx1")))

(define-public crate-docchi_diff-0.7.3 (c (n "docchi_diff") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "1zzpnl670i531y1vvsz39wali9syy59r7y41n9p3k7r199c75dc7")))

(define-public crate-docchi_diff-0.8.0 (c (n "docchi_diff") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.8") (d #t) (k 0)) (d (n "docchi_core") (r "^0.8") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4") (d #t) (k 0)))) (h "0nml7jyvpwa22wrrw7hhm3ci44ymf6r89xffqh3asm8sgd59bf09")))

