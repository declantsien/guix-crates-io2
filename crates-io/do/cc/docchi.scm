(define-module (crates-io do cc docchi) #:use-module (crates-io))

(define-public crate-docchi-0.7.0 (c (n "docchi") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_diff") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_fs") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_intf") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.7.0") (d #t) (k 0)))) (h "1gqrgbdwjmslafbf51nz0aih7hra9pk5y7n6jzpad7krzj6lyk7j")))

(define-public crate-docchi-0.7.1 (c (n "docchi") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_diff") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_fs") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_intf") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.7.0") (d #t) (k 0)))) (h "1i01w5rdbm0nw22zcnfcmf92khqaqwc6gvx0am8lx523p5h4p57a")))

(define-public crate-docchi-0.7.2 (c (n "docchi") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_diff") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_fs") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_intf") (r "^0.7.0") (d #t) (k 0)) (d (n "docchi_json5") (r "^0.7.0") (d #t) (k 0)))) (h "0r16lja5gpf4y5824h5inl8p6j7ycpkybr7nbxr7qnkhfynqcdv5")))

(define-public crate-docchi-0.7.3 (c (n "docchi") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "docchi_archiver2") (r "^0.7") (d #t) (k 0)) (d (n "docchi_core") (r "^0.7") (d #t) (k 0)) (d (n "docchi_diff") (r "^0.7") (d #t) (k 0)) (d (n "docchi_fs") (r "^0.7") (d #t) (k 0)) (d (n "docchi_intf") (r "^0.7") (d #t) (k 0)))) (h "19dcc6ch4q969y9mw4jihqbm5snndd7pf15akzfwydn7v0bsfxva")))

(define-public crate-docchi-0.8.0 (c (n "docchi") (v "0.8.0") (d (list (d (n "docchi_core") (r "^0.8") (d #t) (k 0)) (d (n "docchi_fs") (r "^0.8") (d #t) (k 0)) (d (n "docchi_intf") (r "^0.8") (d #t) (k 0)))) (h "05bd0ai3sn2fvyvgb7bk9iiijvwiywzl193nfdgkvl6n56cg8fj9")))

