(define-module (crates-io do cc docchi_json5) #:use-module (crates-io))

(define-public crate-docchi_json5-0.7.0 (c (n "docchi_json5") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "16vbkk0nkgrni70sy1bnwsrw1k1sz4mlrxh0m3xpwms1kk5ahhrk")))

(define-public crate-docchi_json5-0.7.3 (c (n "docchi_json5") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "10wcji6h6nd0vyikqy7646v9qymiy92lcwmz069ksazl0nkxqgi5")))

(define-public crate-docchi_json5-0.8.0 (c (n "docchi_json5") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "11zqfb6drv3ly15fais4m1pzmvawi8dy94baavaqxfcvynj86r14")))

