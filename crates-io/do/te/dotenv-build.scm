(define-module (crates-io do te dotenv-build) #:use-module (crates-io))

(define-public crate-dotenv-build-0.1.0 (c (n "dotenv-build") (v "0.1.0") (h "004ag3926aliiiz18rb7b7q6x8yi39rk26l40ax1886mm2rdyx1b")))

(define-public crate-dotenv-build-0.1.1 (c (n "dotenv-build") (v "0.1.1") (h "1z41z5yp62xk632sfjn70xlfwjzr0fw6536v5jhm2q3zq4b7ym7l")))

