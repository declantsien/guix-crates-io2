(define-module (crates-io do te dotenv-enum) #:use-module (crates-io))

(define-public crate-dotenv-enum-0.2.1 (c (n "dotenv-enum") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 0)))) (h "1kqwpicxzs4riivnspq16h576xyg8hbscxl379bpgsl16d8rsj1h") (y #t) (r "1.69.0")))

(define-public crate-dotenv-enum-0.2.2 (c (n "dotenv-enum") (v "0.2.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 0)))) (h "14hsbkshgxbswivf9r18wa10av1vncd9vhgwb1z6nkfrvfpgck75") (y #t) (r "1.69.0")))

(define-public crate-dotenv-enum-0.2.3 (c (n "dotenv-enum") (v "0.2.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 0)))) (h "07y2y9x7i4lrc7cdzd6faxvn52644qvhd1z5maw2vxv1j8bjjji4") (y #t) (r "1.67.0")))

(define-public crate-dotenv-enum-0.2.4 (c (n "dotenv-enum") (v "0.2.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 0)))) (h "15x75f5g4i6k2k267lbl992rhak20sn82n8n1f285bsa0iy03pfi") (y #t)))

