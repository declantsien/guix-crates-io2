(define-module (crates-io do te dotenvx_codegen) #:use-module (crates-io))

(define-public crate-dotenvx_codegen-0.0.1 (c (n "dotenvx_codegen") (v "0.0.1") (d (list (d (n "dotenvx_codegen_implementation") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "12cg1f33jaixs95zz3xga7k8sjhkmsp2khw92kd8980c7r31rd5i")))

(define-public crate-dotenvx_codegen-0.0.2 (c (n "dotenvx_codegen") (v "0.0.2") (d (list (d (n "dotenvx_codegen_implementation") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0f025231kiw96010w2fy973yb5x3m81iycdaq6zc0v7qr7q5x7rx")))

