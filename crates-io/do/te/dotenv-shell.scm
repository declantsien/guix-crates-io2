(define-module (crates-io do te dotenv-shell) #:use-module (crates-io))

(define-public crate-dotenv-shell-1.0.0 (c (n "dotenv-shell") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1xscq9zzldacag7n08mlgsxzc1hr7h5bxga9pzsi6zmqayxvrqza")))

(define-public crate-dotenv-shell-1.0.1 (c (n "dotenv-shell") (v "1.0.1") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "040cjilr0dn7lasv53p5qj5aa97d1q4vayn0298d2g11yxwf9jb2")))

