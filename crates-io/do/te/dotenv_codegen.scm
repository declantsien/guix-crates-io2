(define-module (crates-io do te dotenv_codegen) #:use-module (crates-io))

(define-public crate-dotenv_codegen-0.7.0 (c (n "dotenv_codegen") (v "0.7.0") (d (list (d (n "dotenv") (r "^0.7.0") (d #t) (k 0)) (d (n "syntex") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.22.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.22.0") (o #t) (d #t) (k 0)))) (h "1rm9rbsyw4y38vxim76582x6p572gyy24zgzskb64mwsdmcgl4vi") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.7.1 (c (n "dotenv_codegen") (v "0.7.1") (d (list (d (n "dotenv") (r "^0.7.1") (d #t) (k 0)) (d (n "syntex") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.22.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.23.0") (o #t) (d #t) (k 0)))) (h "19xjszwmvkss1qw647q518fggpbcarl7yjifdcaad2p31795kqg0") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.7.2 (c (n "dotenv_codegen") (v "0.7.2") (d (list (d (n "dotenv") (r "^0.7.2") (d #t) (k 0)) (d (n "syntex") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.24.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "0ymq2qrnqz3mqklndx9bcni0r45f2fin1f94706npqqmkzabv4p7") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.8.0 (c (n "dotenv_codegen") (v "0.8.0") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.24.0, < 0.27.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.24.0, < 0.27.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.24.0, < 0.27.0") (o #t) (d #t) (k 0)))) (h "0brn6v114zdv0m9rfs0gf28whf6hmx7ay3bwphpxfgd65n9rzmny") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.8.1 (c (n "dotenv_codegen") (v "0.8.1") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.24.0, < 0.32.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.24.0, < 0.32.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.24.0, < 0.32.0") (o #t) (d #t) (k 0)))) (h "03a79mpzg4nkpssfx3kyrdmhbgdb5nbw1cay39zrnzgj7mxsjhyy") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.9.0 (c (n "dotenv_codegen") (v "0.9.0") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.39.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.37.0, < 0.39.0") (o #t) (d #t) (k 0)))) (h "0l3n2mqw8lcbv343zid9j53rzb027952bw4zwgr7hr1mgzkwmk3p") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.9.1 (c (n "dotenv_codegen") (v "0.9.1") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.42.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.42.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.37.0, < 0.42.0") (o #t) (d #t) (k 0)))) (h "073jmq00ipr6xq913yvr7wf71qrqb78v5shbl8jvfqs1rb8mcq5k") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.9.2 (c (n "dotenv_codegen") (v "0.9.2") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.43.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.43.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.37.0, < 0.43.0") (o #t) (d #t) (k 0)))) (h "0dkvjm1s5k46vr08xi50p9x0q31181ghbm42kja5hly7azj1nswh") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.9.3 (c (n "dotenv_codegen") (v "0.9.3") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.45.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.37.0, < 0.45.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.37.0, < 0.45.0") (o #t) (d #t) (k 0)))) (h "1msg8qlh1liyzn9a6bnydgcjysgz6cxxb667rqkjj85g18hb67sa") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.10.0 (c (n "dotenv_codegen") (v "0.10.0") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "syntex") (r ">= 0.51.0, < 0.54.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r ">= 0.51.0, < 0.54.0") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r ">= 0.51.0, < 0.54.0") (o #t) (d #t) (k 0)))) (h "1ig03xa4axk64yf7x8p1042wzg53adcf2idki1ik9lj3q4wkcf6v") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-dotenv_codegen-0.11.0 (c (n "dotenv_codegen") (v "0.11.0") (d (list (d (n "dotenv") (r "^0.8.0") (d #t) (k 0)) (d (n "dotenv_codegen_impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1yn3a0m0xbp7hzgj93g12mszhkaii06ds730m480qb9wk03v2v8h")))

(define-public crate-dotenv_codegen-0.14.0 (c (n "dotenv_codegen") (v "0.14.0") (d (list (d (n "dotenv_codegen_implementation") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0sa4a0knmf7hqgiig2yh98l5q5w9n13z1w4s2xsnr4wcxada4z00")))

(define-public crate-dotenv_codegen-0.14.1 (c (n "dotenv_codegen") (v "0.14.1") (d (list (d (n "dotenv_codegen_implementation") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0qk5vzqq1r09skwn41z4333z4zx1k2ypnijd21cj89r0y63x4kxb")))

(define-public crate-dotenv_codegen-0.15.0 (c (n "dotenv_codegen") (v "0.15.0") (d (list (d (n "dotenv_codegen_implementation") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1am8nipabx2kkihnrcjqnr882z2fsxga28r1qbl8wkqfq5wn55jn")))

