(define-module (crates-io do te dotenvy_macro) #:use-module (crates-io))

(define-public crate-dotenvy_macro-0.15.1 (c (n "dotenvy_macro") (v "0.15.1") (d (list (d (n "dotenvy_codegen_impl") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "19r28x0zx4i8ynajk5w6yg0yy8jcfg9hvicvmdwh5x688gvgyngm")))

(define-public crate-dotenvy_macro-0.15.7 (c (n "dotenvy_macro") (v "0.15.7") (d (list (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "09341ciy7mhnc04ih98xzkz8rhmlafi8rwf9w3s4kix82bcka0nb") (r "1.56.1")))

