(define-module (crates-io do te dotenvy) #:use-module (crates-io))

(define-public crate-dotenvy-0.15.1 (c (n "dotenvy") (v "0.15.1") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0psjavasdqgqn5c5vk0ybiqa2x2yx643b4avsw0zsrh3qf1im1by") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.2 (c (n "dotenvy") (v "0.15.2") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1lx64751jzqj68vy8mrrn6x6igidvs9wykpz4imx5sw7jgqifhni") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.3 (c (n "dotenvy") (v "0.15.3") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "113f07zyahgd16kh6y8j36ldcc6vliv55gwrvnmw87vwmpybcgfs") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.4 (c (n "dotenvy") (v "0.15.4") (d (list (d (n "clap") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1scm365hm8mq7jq02pqa5c4hrd3vqqlrmw1rj0sgsivk2rj18y4k") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.5 (c (n "dotenvy") (v "0.15.5") (d (list (d (n "clap") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0553pgpnisgjbc7ynsiz7c4lalkqqrikznp9193wfmfwyk45b4gd") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.6 (c (n "dotenvy") (v "0.15.6") (d (list (d (n "clap") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "182cwm14w6asklr0fdzrkzvkfz7bylaxir9p1hp3djx8swbw9n03") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvy-0.15.7 (c (n "dotenvy") (v "0.15.7") (d (list (d (n "clap") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "16s3n973n5aqym02692i1npb079n5mb0fwql42ikmwn8wnrrbbqs") (f (quote (("cli" "clap")))) (r "1.56.1")))

