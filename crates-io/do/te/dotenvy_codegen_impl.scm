(define-module (crates-io do te dotenvy_codegen_impl) #:use-module (crates-io))

(define-public crate-dotenvy_codegen_impl-0.15.1 (c (n "dotenvy_codegen_impl") (v "0.15.1") (d (list (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0l6hc6ccsf3vnqhr6igh0w9nbh2s7bbb29p4i5lhq61kkza12mp0")))

(define-public crate-dotenvy_codegen_impl-0.15.2 (c (n "dotenvy_codegen_impl") (v "0.15.2") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0aj6ilkli81kfwmda2q1fp12qaxw1vqanap1ypx30j7zwmd36d7z") (r "1.56.1")))

