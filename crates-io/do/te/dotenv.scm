(define-module (crates-io do te dotenv) #:use-module (crates-io))

(define-public crate-dotenv-0.1.1 (c (n "dotenv") (v "0.1.1") (h "0cdjfh0qzjblb8jabi3gmzvwaakdbwhzw9nblvqz3n49p0ngi1bw")))

(define-public crate-dotenv-0.2.0 (c (n "dotenv") (v "0.2.0") (h "1bjl0h63wb44q48f4napddfbc1cq41rhmcfnma95pmqn8y3fz7pg")))

(define-public crate-dotenv-0.2.1 (c (n "dotenv") (v "0.2.1") (h "1z57xwhpisy0ck8gjv7j3ydmcx1brkbwll0w6ywl0r0f4nywphda")))

(define-public crate-dotenv-0.2.2 (c (n "dotenv") (v "0.2.2") (h "1gvfr2bh17k41jybvfw1cbclddwjcw6gllbbygz2yk389ry1my9q")))

(define-public crate-dotenv-0.3.0 (c (n "dotenv") (v "0.3.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0nlmqip48w714q12zhr5l2h7brn14ijfi5rbzchvaq0yrxlhxdgz")))

(define-public crate-dotenv-0.4.0 (c (n "dotenv") (v "0.4.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "114i8y5rw9rj5acrwla4mvr9nkvb9kn5gvkpxzpbiqhf2ms11vsy")))

(define-public crate-dotenv-0.5.0 (c (n "dotenv") (v "0.5.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "1qf0lfxabc4c13z79fxwr80s174wid74izvd6dn9hzqxy0wi1afh")))

(define-public crate-dotenv-0.6.0 (c (n "dotenv") (v "0.6.0") (d (list (d (n "regex") (r "< 0.2") (d #t) (k 0)))) (h "1j6imwlqcrmm4cj4amp7cy7n22ipn1cs1wh08bvx0mlzzj8kaq59")))

(define-public crate-dotenv-0.7.0 (c (n "dotenv") (v "0.7.0") (d (list (d (n "regex") (r "< 0.2") (d #t) (k 0)))) (h "0wfnyvxrvrbrjnvgcl91xlkdj34wgdadxfj8s79jjxyiv4gfdwdq")))

(define-public crate-dotenv-0.7.1 (c (n "dotenv") (v "0.7.1") (d (list (d (n "regex") (r "< 0.2") (d #t) (k 0)))) (h "0hw4cadw8cr55n3b6aq4wk8m93wx0x3q4kjmpl6b8j2c9w2lgw3z")))

(define-public crate-dotenv-0.7.2 (c (n "dotenv") (v "0.7.2") (d (list (d (n "regex") (r "< 0.2") (d #t) (k 0)))) (h "1vir6mfb96hdd3nx9pax9a58y3j8lb049dpwlmpdfnp96njapc6v")))

(define-public crate-dotenv-0.8.0 (c (n "dotenv") (v "0.8.0") (d (list (d (n "regex") (r "< 0.2") (d #t) (k 0)))) (h "1wcfb5yxc3m8i905q44ddqlh70spv6b9503pqm6k9dgk5mfkk8gf")))

(define-public crate-dotenv-0.9.0 (c (n "dotenv") (v "0.9.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "06f0jj7kj1csia1sm6n4zqisy32bs1srm7awaj7vvkswwrzk82s0")))

(define-public crate-dotenv-0.10.0 (c (n "dotenv") (v "0.10.0") (d (list (d (n "derive-error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1npbhgb37imlfkxvr0dsh65w4z6zsiqzz12i1zyfzhbd37m6n2sz")))

(define-public crate-dotenv-0.10.1 (c (n "dotenv") (v "0.10.1") (d (list (d (n "derive-error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1ww0wfnilz4cy789fni06gckm45xsb9fplrih26l4qyi4jxy5w6n") (f (quote (("default" "backtrace") ("backtrace" "error-chain/backtrace"))))))

(define-public crate-dotenv-0.11.0 (c (n "dotenv") (v "0.11.0") (d (list (d (n "derive-error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "1p3cqvyk6z8cy57b1n3gnah3fmjn2b7y3b1wfiqdy66fj32y63d7") (f (quote (("default" "backtrace") ("backtrace" "error-chain/backtrace"))))))

(define-public crate-dotenv-0.12.0 (c (n "dotenv") (v "0.12.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "derive-error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "1kwx1xilr50rbjchmw4vllvclljb1bh8309bpn3pp6n848vs7b3w") (f (quote (("default" "backtrace") ("cli" "clap") ("backtrace" "error-chain/backtrace"))))))

(define-public crate-dotenv-0.13.0 (c (n "dotenv") (v "0.13.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "11jdifvvwbn60gf5iq2awyq9fik1d9xk7rhch332nwwnkhks3l60") (f (quote (("default" "backtrace") ("cli" "clap") ("backtrace" "failure/backtrace"))))))

(define-public crate-dotenv-0.14.0 (c (n "dotenv") (v "0.14.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1n8q5q9jflsgk54ppwcr4aimklv42fycdkbrnjv0c4cidaampnvv") (f (quote (("default" "backtrace") ("cli" "clap") ("backtrace" "failure/backtrace"))))))

(define-public crate-dotenv-0.14.1 (c (n "dotenv") (v "0.14.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0xd10hcdrdvbpqxjjy3rh75bn9d6dd967r0y6npfdzxhd3cbl924") (f (quote (("default" "backtrace") ("cli" "clap") ("backtrace" "failure/backtrace"))))))

(define-public crate-dotenv-0.15.0 (c (n "dotenv") (v "0.15.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "13ysjx7n2bqxxqydvnnbdwgik7i8n6h5c1qhr9g11x6cxnnhpjbp") (f (quote (("cli" "clap"))))))

