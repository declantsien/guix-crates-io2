(define-module (crates-io do te dotenv_macros) #:use-module (crates-io))

(define-public crate-dotenv_macros-0.7.0 (c (n "dotenv_macros") (v "0.7.0") (d (list (d (n "dotenv_codegen") (r "^0.7.0") (k 0)))) (h "1sjzn5zrf4p30pcyd1hvghrlmja3f0wf2pbhshfc5bd9p3j8hwa6")))

(define-public crate-dotenv_macros-0.7.1 (c (n "dotenv_macros") (v "0.7.1") (d (list (d (n "dotenv_codegen") (r "^0.7.1") (k 0)))) (h "1w2fy43zc2277lf4ag17wibi3iwzpck3kvc3780z04xymy7w98nk")))

(define-public crate-dotenv_macros-0.7.2 (c (n "dotenv_macros") (v "0.7.2") (d (list (d (n "dotenv_codegen") (r "^0.7.2") (k 0)))) (h "1l3bc457bqm5l92i40f3c2bfq0i7g3k78921i7ml4azxbml9ck8f")))

(define-public crate-dotenv_macros-0.8.0 (c (n "dotenv_macros") (v "0.8.0") (d (list (d (n "dotenv_codegen") (r "^0.8.0") (k 0)))) (h "1jx4p56qs04cmpaghs6gxbfjamg0mh0m358k4w2rxpany7mj7fk8")))

(define-public crate-dotenv_macros-0.9.0 (c (n "dotenv_macros") (v "0.9.0") (d (list (d (n "dotenv_codegen") (r "^0.9.0") (k 0)))) (h "13m2pl4lx3xz619jwgfdqdavsh44xyircm66hmn5cvwyflimx1p7")))

(define-public crate-dotenv_macros-0.10.0 (c (n "dotenv_macros") (v "0.10.0") (d (list (d (n "dotenv_codegen") (r "^0.10.0") (k 0)))) (h "0nry3gdag99qf3k6h738xpy2lf27j88bjyvdjsan1yd2hzxbql2i")))

