(define-module (crates-io do te dotenv-exec) #:use-module (crates-io))

(define-public crate-dotenv-exec-0.1.0 (c (n "dotenv-exec") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "022imx9nd0r6vzdglf25rspzmhkx2nnxbr606mxyl3jf6ydm6zgf")))

(define-public crate-dotenv-exec-0.1.1 (c (n "dotenv-exec") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "00d0435qm2i8l3zqp3sva1j1zd1f3cn0ak44ihvbjznw9wqvr34x")))

