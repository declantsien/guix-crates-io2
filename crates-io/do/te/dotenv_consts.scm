(define-module (crates-io do te dotenv_consts) #:use-module (crates-io))

(define-public crate-dotenv_consts-0.1.0 (c (n "dotenv_consts") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)))) (h "1lvsbvwf5fk7hfkpwavxnpphwvslvv7l6z30xx0isipkjlpwwva5") (y #t)))

(define-public crate-dotenv_consts-0.1.1 (c (n "dotenv_consts") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)))) (h "1gyjpx221m5r1bclairnajr5mfh7c8p43pfvvg0h1vcxwmgllvrq") (y #t)))

