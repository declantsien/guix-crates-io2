(define-module (crates-io do te dotenvx_codegen_implementation) #:use-module (crates-io))

(define-public crate-dotenvx_codegen_implementation-0.0.1 (c (n "dotenvx_codegen_implementation") (v "0.0.1") (d (list (d (n "dotenvx") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "19072szryc33ccw9zjy1iyl15dw97s8xrfc7g2lz2hm8c37bk9xd")))

(define-public crate-dotenvx_codegen_implementation-0.0.2 (c (n "dotenvx_codegen_implementation") (v "0.0.2") (d (list (d (n "dotenvx") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1v0rcldj59pxp7clv7n39y476yjski5bnyd2xix11pmhgvy77aca")))

