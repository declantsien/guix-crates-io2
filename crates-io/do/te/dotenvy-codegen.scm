(define-module (crates-io do te dotenvy-codegen) #:use-module (crates-io))

(define-public crate-dotenvy-codegen-0.15.1 (c (n "dotenvy-codegen") (v "0.15.1") (d (list (d (n "dotenvy_codegen_impl") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0dbd9jpr607qkn5l9n7gsrxjdz66dnh1jmiqbf62y6cr0nfn4ijn") (y #t)))

