(define-module (crates-io do te dotenv-linter) #:use-module (crates-io))

(define-public crate-dotenv-linter-1.1.0 (c (n "dotenv-linter") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1qp2sd57jxkpcx6747q7y92p6szz45hj1wcca7krfr9dcbjk8zy9")))

(define-public crate-dotenv-linter-1.1.1 (c (n "dotenv-linter") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "07xdzl4i08kl6bgx01jb04rpr189znfjssgjk1ch76650hhfapx1")))

(define-public crate-dotenv-linter-1.1.2 (c (n "dotenv-linter") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1bb33ic45mplyyxya2kq27nsscwhm3fxjiyx20jjdv2bj9gp9yrs")))

(define-public crate-dotenv-linter-1.2.0 (c (n "dotenv-linter") (v "1.2.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "03x4d53q6pcp7d86g9fjf095q9dg4k91h6a7jdbij0ffzdp7dg2c")))

(define-public crate-dotenv-linter-2.0.0 (c (n "dotenv-linter") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17fl7n9rivygldkgklw7cydx1410dsyxsr3m9mf88gkb2vicy314")))

(define-public crate-dotenv-linter-2.1.0 (c (n "dotenv-linter") (v "2.1.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "069fl4rm4crrwsczc5y475ajp4ln16j4azpbm0m66cl6hfqd8gg4")))

(define-public crate-dotenv-linter-2.2.0 (c (n "dotenv-linter") (v "2.2.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1li5qhj7p8y8ffqc6mza1vg1skpmwjrg63xpyfwaxswaxz4fana3")))

(define-public crate-dotenv-linter-2.2.1 (c (n "dotenv-linter") (v "2.2.1") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0pmhggg3mjp7nq80632jdp9zn7w2iqln7c4b03dy4s8vv3zizl6c")))

(define-public crate-dotenv-linter-3.0.0 (c (n "dotenv-linter") (v "3.0.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "19l2rck3lblhxdh90isqy6fc4lnn1gmdbciggwk0w0jnyzlhkp6i")))

(define-public crate-dotenv-linter-3.1.0 (c (n "dotenv-linter") (v "3.1.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "gag") (r "^0.1.10") (d #t) (t "cfg(not(windows))") (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "06vi6q31wckydrcyl2lfcq9a6d350spbff8xn8kch17dprava92l")))

(define-public crate-dotenv-linter-3.1.1 (c (n "dotenv-linter") (v "3.1.1") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "dunce") (r "^1.0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "gag") (r "^0.1.10") (d #t) (t "cfg(not(windows))") (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1j7ry9l40xka8aknyza0ab99ww11r3rs3hdf7bvr98zwap4z15by")))

(define-public crate-dotenv-linter-3.2.0 (c (n "dotenv-linter") (v "3.2.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "dunce") (r "^1.0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (t "cfg(not(windows))") (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "update-informer") (r "^0.2.0") (d #t) (k 0)))) (h "1iky4qy20i6f2y96i2nilkpg9niilv1yqn9h5prwfi7nglsvf0qi") (f (quote (("stub_check_version"))))))

(define-public crate-dotenv-linter-3.3.0 (c (n "dotenv-linter") (v "3.3.0") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "clap") (r "^3.1.10") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "dunce") (r "^1.0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (t "cfg(not(windows))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "update-informer") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0s8lcrfpwn9vxqfmv0vvlmblam5k9s0rpc5qnn6s30wk0kvrp3sx") (f (quote (("stub_check_version") ("default" "update-informer"))))))

