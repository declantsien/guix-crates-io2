(define-module (crates-io do te dotenvs) #:use-module (crates-io))

(define-public crate-dotenvs-0.0.1 (c (n "dotenvs") (v "0.0.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05p7h4dkn3q2k9ayjbq9ifnm2pigcs0napdqrr6bqk10dwzka4yn")))

(define-public crate-dotenvs-0.1.0 (c (n "dotenvs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1nwzkrn84jijjwp993r63niqqpxwzi4qrlqkynd1ki4yn4fj1qw8")))

