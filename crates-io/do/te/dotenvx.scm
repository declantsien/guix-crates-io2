(define-module (crates-io do te dotenvx) #:use-module (crates-io))

(define-public crate-dotenvx-0.0.1 (c (n "dotenvx") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1h0smw4hnw886nd2981jbq9nby5rfxz8brfsa21sppmqkgz5lqkn") (f (quote (("cli" "clap"))))))

(define-public crate-dotenvx-0.0.2 (c (n "dotenvx") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0lx412zam4rmizhypgnzd56qj8fiw16kg2kiicvn5bzh1kdj91a4") (f (quote (("cli" "clap"))))))

