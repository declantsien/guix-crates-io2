(define-module (crates-io do te dotenv-parser) #:use-module (crates-io))

(define-public crate-dotenv-parser-0.1.0 (c (n "dotenv-parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1h63yyah30j3x9b3nl1kqf7p3a0kl91li4z0k046h7pi8s2ackap") (y #t)))

(define-public crate-dotenv-parser-0.1.1 (c (n "dotenv-parser") (v "0.1.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1zzwhxq6fddp0dy6v5d1zxv8xwh0a9l1vvnap7cig0dwm9yqfzkq")))

(define-public crate-dotenv-parser-0.1.2 (c (n "dotenv-parser") (v "0.1.2") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "15bn3h7s77pa74nipkq55504lg0qphrq2qlqfmpj3dwlbv6xgydw")))

(define-public crate-dotenv-parser-0.1.3 (c (n "dotenv-parser") (v "0.1.3") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1w78b0rgkdanjxanb72sjlmlq4biwq31kjzix039hscyh8ypwkjd")))

