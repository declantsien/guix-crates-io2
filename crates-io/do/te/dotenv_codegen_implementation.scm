(define-module (crates-io do te dotenv_codegen_implementation) #:use-module (crates-io))

(define-public crate-dotenv_codegen_implementation-0.14.0 (c (n "dotenv_codegen_implementation") (v "0.14.0") (d (list (d (n "dotenv") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1p24n0ws9irya7gpxg8x8fq3ah87fib930cij7d61yh3vd1vhi5j")))

(define-public crate-dotenv_codegen_implementation-0.14.1 (c (n "dotenv_codegen_implementation") (v "0.14.1") (d (list (d (n "dotenv") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00ab1b1idn7wa5zpzv8fn9cmwgb0khapji70hrrjqraly4hmh8wh")))

(define-public crate-dotenv_codegen_implementation-0.15.0 (c (n "dotenv_codegen_implementation") (v "0.15.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0cygsjmnix614pj2sbaz0kly2lzg8g749dhrvim5zm1caaikgrsk")))

