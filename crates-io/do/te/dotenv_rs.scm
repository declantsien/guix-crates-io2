(define-module (crates-io do te dotenv_rs) #:use-module (crates-io))

(define-public crate-dotenv_rs-0.15.0 (c (n "dotenv_rs") (v "0.15.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0diiyxzv3wvfaj375wvzpif3wjchgpiy8260lmxm2n5i345z2k7k") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv_rs-0.15.1 (c (n "dotenv_rs") (v "0.15.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1kd81xn1c4pk9rs0gb7iczqm3n87ahb75b6lwdf9rsfa2m1v49sj") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv_rs-0.15.2 (c (n "dotenv_rs") (v "0.15.2") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "19fcw9j031b4ikv0pwd9bbm6q0wigxz19pxvyzc8fi47xs5kc3i8") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv_rs-0.16.0 (c (n "dotenv_rs") (v "0.16.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1zhk6rf06hi25h9jm9pd17r75qi22ybbyi0xam191pcis23l90sw") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv_rs-0.16.1 (c (n "dotenv_rs") (v "0.16.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "15s33ma7x65r6j07xf3ngzhjkifac9bjmbxm0kxzxhj1jhjh3142") (f (quote (("cli" "clap"))))))

