(define-module (crates-io do te dotenv-flow) #:use-module (crates-io))

(define-public crate-dotenv-flow-0.15.0 (c (n "dotenv-flow") (v "0.15.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1d3yvynbmyl39wnfchh225w1sm21pv68a0pqlv3123nrjqf799d7") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv-flow-0.16.0 (c (n "dotenv-flow") (v "0.16.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "00ynsqmv1841rx3wp253cywwdg7yni09jyjz0i2bm9i8m33yfia8") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv-flow-0.16.1 (c (n "dotenv-flow") (v "0.16.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1yg35mkyyq8xcb1gb0cixjm78px66hqxwayivqskfq7w4kx9a60n") (f (quote (("cli" "clap"))))))

(define-public crate-dotenv-flow-0.16.2 (c (n "dotenv-flow") (v "0.16.2") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "06i7ilg7hcp0cspdzajs4p8d63lkby6azrxi59rl46ifvxh21zyc") (f (quote (("cli" "clap"))))))

