(define-module (crates-io do te dotext) #:use-module (crates-io))

(define-public crate-dotext-0.1.0 (c (n "dotext") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)) (d (n "zip") (r "^0.2") (k 0)))) (h "1knqhs60fk1r3rsjfx8q1g7x1wa4b7m7phxswx3mvj8gbm3nq7q0")))

(define-public crate-dotext-0.1.1 (c (n "dotext") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)) (d (n "zip") (r "^0.2") (k 0)))) (h "1mmfskgigz9qv8jd0k159pfcbl2k48ss5ibx0xbl1wlvwamypfxf")))

