(define-module (crates-io do tv dotvanity) #:use-module (crates-io))

(define-public crate-dotvanity-0.1.0 (c (n "dotvanity") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "schnorrkel") (r "^0.9.1") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "163rmwki0kr4f8qx6kjnwqwbfrxcpknp5kbs2sqizsjyjj7n80xy") (y #t)))

(define-public crate-dotvanity-0.1.2 (c (n "dotvanity") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.0") (d #t) (k 0)))) (h "0zh36nr1j6salasfcljm010pwvg79mbav0nqqdgjd15z074i4wxg")))

(define-public crate-dotvanity-0.2.0 (c (n "dotvanity") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "1xr3xry612m187x1lw8mqakcgghkbw66pbh540hfmr9l3y4lsphh")))

(define-public crate-dotvanity-0.2.1 (c (n "dotvanity") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "0556g59i6lc0zzl6xpdgnvqk5x8cr1wi0ri94k6wzhrnzf1z2ds2")))

(define-public crate-dotvanity-0.2.2 (c (n "dotvanity") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "0ibhcg57dfdcbsdwfcd8sy995h8mmv49jmk4f2yfr0nz8dn21cbd")))

(define-public crate-dotvanity-0.2.6 (c (n "dotvanity") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "1kixmziwd5zz7nb6qaj2h06x09dzllnhl0wljggaxg83d22y5q7a")))

(define-public crate-dotvanity-0.2.7 (c (n "dotvanity") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "15cn9alva1xas1pksvd3f8shd11xlwgls9q2s1368f0aq3f6am41")))

(define-public crate-dotvanity-1.0.0 (c (n "dotvanity") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc3") (d #t) (k 0)))) (h "1dm1amb5pi4lxrc51a1kam4kk54q59r44lqrcb9xh4ibxspj2nc9")))

