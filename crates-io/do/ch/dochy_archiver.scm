(define-module (crates-io do ch dochy_archiver) #:use-module (crates-io))

(define-public crate-dochy_archiver-0.1.0 (c (n "dochy_archiver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "13yh40j2n6yklharz61kkj2ja4d9n913jsrj07z8pdxw2nigix0p")))

(define-public crate-dochy_archiver-0.1.1 (c (n "dochy_archiver") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0yfvgc48k64wl4jcca8nm6jqcms3846qsaax1gdmg9jhc6np9rqy")))

(define-public crate-dochy_archiver-0.1.2 (c (n "dochy_archiver") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0yr340c0h82sg4d9c7ixd2mw13c6g9m1wmjpk5zz6yg2c9pv89dg")))

(define-public crate-dochy_archiver-0.1.3 (c (n "dochy_archiver") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0jyxaj863yl865sidsxvjhn6bkg2sbq805klqqqb09si4nw9cj1c")))

(define-public crate-dochy_archiver-0.2.0 (c (n "dochy_archiver") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0hypm20f4nx5gcx263i14nnhc5gxh7p1k83s28f5y33mbikn4lcc")))

(define-public crate-dochy_archiver-0.3.0 (c (n "dochy_archiver") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "1h4dgc9nndknpvfhwvsyh955ryi9rjfg35fzidbh34b6vszshn6j")))

(define-public crate-dochy_archiver-0.3.1 (c (n "dochy_archiver") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0dmj1s4j40hbvbbxm1cahayrn7v1ssjffjcjiq4zxvhzq043iib6")))

(define-public crate-dochy_archiver-0.3.2 (c (n "dochy_archiver") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "0r1cxn2i0x7m3d01b7l3iiz0v7w9d3av6v0hp4rbf9nbhljs6l0w")))

(define-public crate-dochy_archiver-0.3.3 (c (n "dochy_archiver") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "13wpfd2hqix30wyn7myhlm988wn6h7wbg2gwwv1hywgi4ad30h0c")))

(define-public crate-dochy_archiver-0.4.0 (c (n "dochy_archiver") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.4.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1jf1xwzikc06v0g7is6zw0y7ibgn89c4s6pgs57fm7jhgk6wzikg")))

(define-public crate-dochy_archiver-0.5.0 (c (n "dochy_archiver") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.5.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0d9kxirsqx95wiq4hhm4smnk4x0akh0i1rvk50lddrvdlqg8501z")))

(define-public crate-dochy_archiver-0.6.0 (c (n "dochy_archiver") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.6.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "19rzkp9kg28mi4lsqk5lvfjlq2ndp6pi9mbpbzbhasbvp0zy34pr")))

(define-public crate-dochy_archiver-0.6.1 (c (n "dochy_archiver") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.6.1") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "189s49kq3r966p3hl553795wzmagkbazf616g5yf5cw5ypc7j0dl")))

(define-public crate-dochy_archiver-0.7.0 (c (n "dochy_archiver") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0lbmwhschj333yr7b1hkqw498491ssv7cl8iqcacgnv4pyin1cxl")))

(define-public crate-dochy_archiver-0.7.1 (c (n "dochy_archiver") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.7.1") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "03hnm0rxnhabzk0ifna5i4rns0hyjb6di89vskjlagnhk2xmhfha")))

(define-public crate-dochy_archiver-0.8.0 (c (n "dochy_archiver") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.8.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0wlmzxr7a7pip4qxcc2y7m6zxvb4s121mwywi7clrixy235n8qk4")))

