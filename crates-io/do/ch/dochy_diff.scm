(define-module (crates-io do ch dochy_diff) #:use-module (crates-io))

(define-public crate-dochy_diff-0.1.0 (c (n "dochy_diff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.1.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.1.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "1xc27bkw8cc7mp92xiy6fylcqpycxssns1ap0dm3am8p4yscnmiy")))

(define-public crate-dochy_diff-0.4.0 (c (n "dochy_diff") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.4.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.4.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "195qlvc97mz6b75z2gmf7gf9j6pgsgjkyddfijjz59qvhr8qdzkq")))

(define-public crate-dochy_diff-0.5.0 (c (n "dochy_diff") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.5.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.5.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1kljmc9448d312fc8nc5wyi9v79r0yk4c52280qhi2gwkphkls5q")))

(define-public crate-dochy_diff-0.6.0 (c (n "dochy_diff") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.6.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.6.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1dlkazs1pxv3qr3p1vi31p7206hlxphzy37nqcqvjm3cf3v95igc")))

(define-public crate-dochy_diff-0.6.1 (c (n "dochy_diff") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_core") (r "^0.6.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0gkr6ysv4jiph2m78djcybmn0c5vv622sccx97k7hanamni1l7wj")))

(define-public crate-dochy_diff-0.7.0 (c (n "dochy_diff") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.7.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.7.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0103ykzd0gzaq1xljdqpbjqnwl9jazjaz1a7h5502csnpi3za5wk")))

(define-public crate-dochy_diff-0.7.1 (c (n "dochy_diff") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.7.1") (d #t) (k 0)) (d (n "dochy_core") (r "^0.7.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1q45x6d8pbwzjz8f2bz2b7ird9y61xbac5jq9s1qgkd1lr23vpik")))

(define-public crate-dochy_diff-0.8.0 (c (n "dochy_diff") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.8.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.8.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1v3hmq3v8nc57jnywlq2n7rbaf23yp46d4i7mwq04ma05yychvcl")))

(define-public crate-dochy_diff-0.9.0 (c (n "dochy_diff") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.9.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.9.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1s3k3if78h48s2pk7fifawmwci4mp1id8lvybr24wrgw6zw3z8z7")))

(define-public crate-dochy_diff-0.10.0 (c (n "dochy_diff") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.10.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.10.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "0qrf4q9b9rsnmi43z32hyxwhdjwpn1ymkrkz049lky7jk1jdzp1l")))

(define-public crate-dochy_diff-0.10.1 (c (n "dochy_diff") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.10.0") (d #t) (k 0)) (d (n "dochy_core") (r "^0.10.0") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "0v3z3g8v3hkrzf0jybfwqj5a010x6sjwwk56s9x9vi5kq0bxq1xi")))

