(define-module (crates-io do ch dochy_compaction) #:use-module (crates-io))

(define-public crate-dochy_compaction-0.1.0 (c (n "dochy_compaction") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.2") (d #t) (k 0)))) (h "040plbxlzbf6nxzbigk1f910z2s9gfqrnkzswzda010nyfv95hmh")))

(define-public crate-dochy_compaction-0.4.0 (c (n "dochy_compaction") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "09mi82z8d9shvhxvmyr0gg96swn6qdxh2pggmzyxzdhsg804hhg9")))

(define-public crate-dochy_compaction-0.5.0 (c (n "dochy_compaction") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "078picfkpbq80jjr4gfbk1l289xsmjcigpasfzzxsm8rp9kcr0cp")))

(define-public crate-dochy_compaction-0.6.0 (c (n "dochy_compaction") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0k31gmsxgfv2vj624yyphhbbqrf9hvdfrbdc190ybhwijdqqbljv")))

(define-public crate-dochy_compaction-0.6.1 (c (n "dochy_compaction") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "03qlbaysdpgnd9p0nhzhlsgmqylq6pyqmbvqyw001x6hyzfls78g")))

(define-public crate-dochy_compaction-0.7.0 (c (n "dochy_compaction") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "1jrs1791l0v02y35s22rw7v2jw7dlnh0424n1pg57a5ycz6882aq")))

(define-public crate-dochy_compaction-0.7.1 (c (n "dochy_compaction") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "01g5xm4maxm6zr48fr65qjrkl89q7k51730d0flsy8v3vkz7aknm")))

(define-public crate-dochy_compaction-0.8.0 (c (n "dochy_compaction") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "16c2yrx2dr6k47v77aq7cwm94krmwgi4sf7igcpgid9kmjgnw840")))

(define-public crate-dochy_compaction-0.9.0 (c (n "dochy_compaction") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "116dscg9h51q0aqc4jb6a2fdpg2ms5xbn1f080h2gpxj8clzjibm")))

(define-public crate-dochy_compaction-0.10.0 (c (n "dochy_compaction") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "12q9irz9zslgvnqwk8cflvj4yhqrhi1vg7hkp6kw2z3ra5g5n472")))

(define-public crate-dochy_compaction-0.10.1 (c (n "dochy_compaction") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "0hnp30fsgaa7y0mwiwz4mbl5p0il4n64mc1nf9mbbd3jn004sfhr")))

