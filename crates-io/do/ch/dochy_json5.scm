(define-module (crates-io do ch dochy_json5) #:use-module (crates-io))

(define-public crate-dochy_json5-0.1.0 (c (n "dochy_json5") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0rbfbzi1liidd6py7n95nzd010n43l19fvls5p0mny9imzdnrps1")))

(define-public crate-dochy_json5-0.4.0 (c (n "dochy_json5") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1wypziq2gqacwc73z5hg0ynz083jvlms7g4ymd6gzmxcgndvnkby")))

(define-public crate-dochy_json5-0.5.0 (c (n "dochy_json5") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1270hdbbrdrajzwlsk9c0gkxzfma5dn3y0061abmi0dz10p82939")))

(define-public crate-dochy_json5-0.6.0 (c (n "dochy_json5") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "115hz0hac5c78b0q2mha1yv89yhd8ah1dm04m4nsangb2wrc87vp")))

(define-public crate-dochy_json5-0.6.1 (c (n "dochy_json5") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "19llwagqq79lzcr1r37s1g6v7xapkdjwfi0kxvhblv6xw246r0jf")))

(define-public crate-dochy_json5-0.7.0 (c (n "dochy_json5") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0zczmhgprcn3ibk8a5gh3drx9nrqga6s6gadssqz97ixgp2aysaw")))

(define-public crate-dochy_json5-0.7.1 (c (n "dochy_json5") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "19d8x9hnvdb1v3m7kcac6m3xw5c8hvmi4yjmivmaa68hvr9dk14c")))

(define-public crate-dochy_json5-0.8.0 (c (n "dochy_json5") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0drnjdw621vjfwi6adqz1yinxids8lyl7h7l1s3av6qsx31vwbaj")))

(define-public crate-dochy_json5-0.9.0 (c (n "dochy_json5") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "00pnclgggm0645nsw4hggz9xg4a2i1qvz1zbh38nycrl2f5z1gr8")))

(define-public crate-dochy_json5-0.10.0 (c (n "dochy_json5") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1k1n03hdak212cazf6hh8v91rzyqbjirlp9arb6pyp05n6ckz2b9")))

(define-public crate-dochy_json5-0.10.1 (c (n "dochy_json5") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0lgz2l8p3aivv1p9036xz620f909x5n21fy9kfysdb63pxfvqx31")))

