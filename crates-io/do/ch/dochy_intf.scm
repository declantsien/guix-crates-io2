(define-module (crates-io do ch dochy_intf) #:use-module (crates-io))

(define-public crate-dochy_intf-0.1.0 (c (n "dochy_intf") (v "0.1.0") (d (list (d (n "dochy_core") (r "^0.1.0") (d #t) (k 0)))) (h "1xn444bbx442q4kr1gqb1hnw9aibxix27lfj9wqznd795igafyib")))

(define-public crate-dochy_intf-0.4.0 (c (n "dochy_intf") (v "0.4.0") (d (list (d (n "dochy_core") (r "^0.4.0") (d #t) (k 0)))) (h "1mdbrfcdg9xwxbsg9xkx8fsgbm4mkiijpxbcl5ry2h30y1akd258")))

(define-public crate-dochy_intf-0.5.0 (c (n "dochy_intf") (v "0.5.0") (d (list (d (n "dochy_core") (r "^0.5.0") (d #t) (k 0)))) (h "0mqkip19abk92jx7igqsix9y0lxpf89n4mrixikfz2f99rlr3ccy")))

(define-public crate-dochy_intf-0.6.0 (c (n "dochy_intf") (v "0.6.0") (d (list (d (n "dochy_core") (r "^0.6.0") (d #t) (k 0)))) (h "0651c9r6knw6i9ap58i6z410y8xf9p9xs3xfzcggrxc69hwv2bdf")))

(define-public crate-dochy_intf-0.6.1 (c (n "dochy_intf") (v "0.6.1") (d (list (d (n "dochy_core") (r "^0.6.1") (d #t) (k 0)))) (h "00l5244bf9nb5bdr2c0k73f3i7nzc57zyzrqr16lh3y88fmgdf3g")))

(define-public crate-dochy_intf-0.7.0 (c (n "dochy_intf") (v "0.7.0") (d (list (d (n "dochy_core") (r "^0.7.0") (d #t) (k 0)))) (h "1wpfvf4bv5b341243m651m623nj9m61kq38c3dwp81r0vamsl0r8")))

(define-public crate-dochy_intf-0.7.1 (c (n "dochy_intf") (v "0.7.1") (d (list (d (n "dochy_core") (r "^0.7.1") (d #t) (k 0)))) (h "1mzjn2zw355v7ws10643xrzjgfv260z8zv1q00pk36amfkl3wj8f")))

(define-public crate-dochy_intf-0.8.0 (c (n "dochy_intf") (v "0.8.0") (d (list (d (n "dochy_core") (r "^0.8.0") (d #t) (k 0)))) (h "126zxpzw5dffs3i3bx5pw6mwjhjwvj6q6jmnnvbrl9kldhak7drb")))

(define-public crate-dochy_intf-0.9.0 (c (n "dochy_intf") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_core") (r "^0.9.0") (d #t) (k 0)))) (h "06sw459jim2gk4ijbhcw4mlqm68l1713mwj962z9ad8kmhd50yhc")))

(define-public crate-dochy_intf-0.10.0 (c (n "dochy_intf") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_core") (r "^0.10.0") (d #t) (k 0)))) (h "12d0w3xnhxa0aaq754ym9dy5q5a2ldy94p0i41lg4s5ykgzdyjk4")))

(define-public crate-dochy_intf-0.10.1 (c (n "dochy_intf") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_core") (r "^0.10.0") (d #t) (k 0)))) (h "1dr0w2brjqc8c9iry50b4n04adjijgza87fzg8snhy6bci195kd9")))

