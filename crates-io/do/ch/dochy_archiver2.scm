(define-module (crates-io do ch dochy_archiver2) #:use-module (crates-io))

(define-public crate-dochy_archiver2-0.9.0 (c (n "dochy_archiver2") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.9.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.3.0") (d #t) (k 0)))) (h "0i25kh7dh7h9ikjq29n98n7fpcr5x4fkdsy8iwf1fiq5m66cja96")))

(define-public crate-dochy_archiver2-0.10.0 (c (n "dochy_archiver2") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.10.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "0h4s0glkzbjd6cwz0f3xnfm2r8xmknjdnrnr3xrfccz92y61i55s")))

(define-public crate-dochy_archiver2-0.10.1 (c (n "dochy_archiver2") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "dochy_compaction") (r "^0.10.0") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "snap") (r "^1.0.1") (d #t) (k 0)) (d (n "with_capacity_safe") (r "^0.4.0") (d #t) (k 0)))) (h "00jhzbmbxbpymf7msw1g5l6vmifmim5hv6hz0n4xfzzydi4h4lr7")))

