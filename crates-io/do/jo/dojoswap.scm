(define-module (crates-io do jo dojoswap) #:use-module (crates-io))

(define-public crate-dojoswap-2.9.0 (c (n "dojoswap") (v "2.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.5.0") (d #t) (k 0)) (d (n "cw2") (r "^0.13.2") (d #t) (k 0)) (d (n "cw20") (r "^0.13.2") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0vzngl7pfrvslgwipmx69xzanmkq0jsfjz9qjbjyiwmvz9z7wc3m") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

