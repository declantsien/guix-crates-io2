(define-module (crates-io do ug dough) #:use-module (crates-io))

(define-public crate-dough-0.1.0 (c (n "dough") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0r82gkfpnmfplc94n5njixabnjw34y2dnzn11lhrizx34jli4prc")))

(define-public crate-dough-0.1.1 (c (n "dough") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1s7ynvxkflkrd93nvn9bix1d8rsj63fhcrv0g802rxgx3wdg9da6")))

(define-public crate-dough-0.1.2 (c (n "dough") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0hdx8wiffbarilplvwi85xbmy096g6hn5br51j229qw2yxngjjnh") (y #t)))

(define-public crate-dough-0.1.3 (c (n "dough") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "07yc2jfd3jp2ca9mn4d7qrmkvqkq6yy9qr65rfdmlk50cmp0kqh0")))

