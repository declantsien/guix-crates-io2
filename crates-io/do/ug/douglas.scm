(define-module (crates-io do ug douglas) #:use-module (crates-io))

(define-public crate-douglas-0.1.0 (c (n "douglas") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)))) (h "1wi1pm4v5zvbhrijhpkqic2jkfzdxm4k7a898c4gxpy62rjfdl7i")))

(define-public crate-douglas-0.1.1 (c (n "douglas") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0nsn04xllwb9viz03v9mfdafk03plifwqmijv0i8z88h4cpvl205")))

