(define-module (crates-io do er doer) #:use-module (crates-io))

(define-public crate-doer-0.1.0 (c (n "doer") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "process" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)))) (h "1gdlqc7j704d5shq7m679pv7zhnhq79xqnqi5qqav02rkj24847w")))

(define-public crate-doer-0.1.1 (c (n "doer") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "process" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)))) (h "1y0l04yhnvb34zhjkgpgka26szylfhgrybs4r00wgas7wk1xncbj")))

(define-public crate-doer-0.1.2 (c (n "doer") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "process" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)))) (h "0p66rkd4vgr3xsmgfldahi4y10mi91p1gz9cy5qmbvq3wg25dldd")))

(define-public crate-doer-0.1.3 (c (n "doer") (v "0.1.3") (d (list (d (n "async-recursion") (r "^1.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "process" "rt-multi-thread" "fs" "time" "io-util"))) (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)))) (h "0n84y83ndp4i4xx39snyqbhbhv9k09dk85bd0pr8jahwg97kp98i")))

