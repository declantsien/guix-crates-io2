(define-module (crates-io do ki doki-web) #:use-module (crates-io))

(define-public crate-doki-web-0.1.0 (c (n "doki-web") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.7") (f (quote ("web" "ssr"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 0)))) (h "0wv56wrqgk00pmmv9mrrp67gxakmih69m0l6ppds8bdki2ns6bi0") (f (quote (("default"))))))

