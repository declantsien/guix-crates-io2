(define-module (crates-io do ki doki-core) #:use-module (crates-io))

(define-public crate-doki-core-0.1.0 (c (n "doki-core") (v "0.1.0") (d (list (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)))) (h "19pjnbgpqq320bn8w5wq0alzjfh95szhvs8ncv6pyj30pxmp8blx") (f (quote (("default"))))))

