(define-module (crates-io do ki doki-git) #:use-module (crates-io))

(define-public crate-doki-git-0.1.0 (c (n "doki-git") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1s62g6l0vhbn4559db0pdvapmlns4zmnfvsr9v0xh5s8n3bm5xgl") (f (quote (("default"))))))

(define-public crate-doki-git-0.1.1 (c (n "doki-git") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0jda7hdavfkqn13bn1mfgnakib5nlcfy60qss6dzm5q9fiydccc9") (f (quote (("default"))))))

