(define-module (crates-io do cf docfg) #:use-module (crates-io))

(define-public crate-docfg-0.1.0 (c (n "docfg") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0wbh57b01fsa66rxh3d79mzm278k4mwr7k25fh99b8d86yd82lx2")))

