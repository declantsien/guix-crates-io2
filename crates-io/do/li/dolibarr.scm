(define-module (crates-io do li dolibarr) #:use-module (crates-io))

(define-public crate-dolibarr-0.1.0 (c (n "dolibarr") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q471gs4hsfqrkfiwhxskw1535szq27353yk836cwi98fxnakacs")))

