(define-module (crates-io do ve dovemail) #:use-module (crates-io))

(define-public crate-dovemail-0.0.1 (c (n "dovemail") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "seahash") (r "^4.1.0") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("serde" "serde-well-known"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0c3sxnlqppyjci35cpiq370g6yg488vw6vxf7jx3yxy2gma9s2c7")))

