(define-module (crates-io do ve dovetail) #:use-module (crates-io))

(define-public crate-dovetail-0.1.0 (c (n "dovetail") (v "0.1.0") (d (list (d (n "fluffer") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "14d6p4c6nrwmvsfshki477qm4wxyl0q0m7zqgch9rwp8y80m3rnl")))

(define-public crate-dovetail-1.0.0 (c (n "dovetail") (v "1.0.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluffer") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0n73f1kw6r37jkqs1lsr2pwaih6dgqgmfdfzn2w4hx185qy2l9zl")))

(define-public crate-dovetail-2.0.0 (c (n "dovetail") (v "2.0.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fluffer") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0ik42sskn62vfcgzhnw6h5827cr2arb5yaprh9fqw7bwva1a4pz8")))

