(define-module (crates-io do ve dovepipe) #:use-module (crates-io))

(define-public crate-dovepipe-0.1.0 (c (n "dovepipe") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v9dlsmps89dmg22gbzg9l5snfsk9dd29p2vyk7pm926j74hbygw")))

(define-public crate-dovepipe-0.1.1 (c (n "dovepipe") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i68nnwa9yqidsk7nsdx1wa0vwg1l99k14qq352b5jhn590v81aw") (f (quote (("logging"))))))

(define-public crate-dovepipe-0.1.2 (c (n "dovepipe") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwkgqi7sf9yanr111488iz08zqp9i0zm2lriij1q4h8hr45widk") (f (quote (("logging"))))))

(define-public crate-dovepipe-0.1.3 (c (n "dovepipe") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05094y87z8ni4yp2zhj2q2i3ikv5y6clls2scdlpwlc5b5p29i7y") (f (quote (("logging"))))))

(define-public crate-dovepipe-0.1.4 (c (n "dovepipe") (v "0.1.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfn5m0q6w41fzd66nznvfj5w5h7vbm92i8gbmzbyg8s7nnq7yfc") (f (quote (("logging"))))))

(define-public crate-dovepipe-0.1.5 (c (n "dovepipe") (v "0.1.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10hpprwi8wi3a2y0p63r2ivxmadkr83d7yhcl9fwb5yg7wdaa3ix") (f (quote (("logging"))))))

(define-public crate-dovepipe-0.1.6 (c (n "dovepipe") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06iw5d9m27cnddks5dcnxj5p220mqbw20hxx1arg484nn9qb34x0") (f (quote (("logging"))))))

