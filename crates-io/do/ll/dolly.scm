(define-module (crates-io do ll dolly) #:use-module (crates-io))

(define-public crate-dolly-0.1.0 (c (n "dolly") (v "0.1.0") (d (list (d (n "glam") (r ">=0.15, <=0.17") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0y9kjh24cjkqzs8gr79v5a7r363p3aapldspg44vymdxall67j61")))

(define-public crate-dolly-0.1.1 (c (n "dolly") (v "0.1.1") (d (list (d (n "glam") (r ">=0.15, <=0.17") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "02ysiikbkbggnpjk92mwqqzzi5sv892w4ws7sryhznp043j1v89a")))

(define-public crate-dolly-0.1.2 (c (n "dolly") (v "0.1.2") (d (list (d (n "glam") (r ">=0.15, <=0.17") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0r4lsr4ylw2am5yb5m0rgdycmc5pj7v11v29gzwy3fv7fz7ndsr9")))

(define-public crate-dolly-0.1.3 (c (n "dolly") (v "0.1.3") (d (list (d (n "glam") (r ">=0.15, <=0.18") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "1n1cpbpxiza3v60l6kv79gjkssk97azj48a7jyi4j0hc3p1p2s4j")))

(define-public crate-dolly-0.1.4 (c (n "dolly") (v "0.1.4") (d (list (d (n "glam") (r ">=0.15, <=0.20") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0k79fcw73cpd2f8r8ap658lasrbi32jg6c7fvjp90x0lsanzm46w")))

(define-public crate-dolly-0.2.0 (c (n "dolly") (v "0.2.0") (d (list (d (n "glam") (r ">=0.15, <=0.20") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0yzz1wph39dj7mzgrnk2cav1rmc5lca4616xkkq86v8y43kmda8k")))

(define-public crate-dolly-0.3.0 (c (n "dolly") (v "0.3.0") (d (list (d (n "glam") (r ">=0.15, <=0.20") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "1pl7mphh2ij5bfb03pmi0h0libh6j7w1hjammsg8k3h2h2pn5jkj")))

(define-public crate-dolly-0.3.1 (c (n "dolly") (v "0.3.1") (d (list (d (n "glam") (r ">=0.15, <=0.21") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0cns2d0fw2l01kssyp9icz8n19mbrd8q5pc45vmm119ggkz3bbn2")))

(define-public crate-dolly-0.3.2 (c (n "dolly") (v "0.3.2") (d (list (d (n "glam") (r ">=0.15, <=0.21") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "09vgnfv34qi0v8npaghv2zry66yl9ljkm4yxylqjnjb9sck4d1nq")))

(define-public crate-dolly-0.4.0 (c (n "dolly") (v "0.4.0") (d (list (d (n "glam") (r ">=0.21, <=0.22") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "04bz0xhn7rwpkck3izg3hmdfgafx313jd4z6izm43dv0w6i03gg8") (r "1.58.1")))

(define-public crate-dolly-0.4.1 (c (n "dolly") (v "0.4.1") (d (list (d (n "glam") (r ">=0.21, <=0.23") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0z8856w487xcwglp9fv5yaacc0v8nz0bjqgwamyfkbc2c3d5fyl1") (r "1.60")))

(define-public crate-dolly-0.4.2 (c (n "dolly") (v "0.4.2") (d (list (d (n "glam") (r ">=0.21, <=0.24") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "0pv8hryni8jzn7ww4blw4vzb3p2613q65rgd1414yw0kw36v6rg6") (r "1.60")))

(define-public crate-dolly-0.4.3 (c (n "dolly") (v "0.4.3") (d (list (d (n "glam") (r ">=0.21, <=0.25") (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "1jpl07w7jpg512d1ibiwkfikkq11xd486iv9cw3aw51n7vds3byy") (r "1.60")))

(define-public crate-dolly-0.5.0 (c (n "dolly") (v "0.5.0") (d (list (d (n "glam") (r ">=0.21, <=0.25") (f (quote ("mint"))) (d #t) (k 0)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "mint") (r "^0.5.8") (d #t) (k 0)))) (h "13ahjjy37bkf0z8frhfpcrzlabdwylyyjwv4xv5lbgiqvj0b4byz") (r "1.60")))

