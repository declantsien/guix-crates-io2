(define-module (crates-io do -n do-not-cry) #:use-module (crates-io))

(define-public crate-do-not-cry-1.0.0 (c (n "do-not-cry") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)))) (h "0d4q7g3avymmhba7z7xfx5xp8v26y07aix5mksgj4ry7cx96q4cf")))

(define-public crate-do-not-cry-1.1.0 (c (n "do-not-cry") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)))) (h "1fzslmmvgc223zv75b0dds6qfagk8014bvq9awx53w621zh0zkmv")))

(define-public crate-do-not-cry-1.2.0 (c (n "do-not-cry") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)))) (h "167qbp5q2bd9yvajm0kmnkxxglw3bbby6p8bfmng8piw2ivdirgl")))

(define-public crate-do-not-cry-1.2.1 (c (n "do-not-cry") (v "1.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)))) (h "1zaz73kvi2mbgrx39ggmkj3mn7sjfzk4a82xisdvgriairlq0r67")))

(define-public crate-do-not-cry-1.3.0 (c (n "do-not-cry") (v "1.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "02gnngl04zrk28yj6x9hnrqfasay4953y4cvynrls4wkpn0k69ra")))

(define-public crate-do-not-cry-1.4.0 (c (n "do-not-cry") (v "1.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kmnc9188wpc0gjyddbfk0jrcsplvvbfka9gwl29x8j9rna2x5cy")))

(define-public crate-do-not-cry-1.4.1 (c (n "do-not-cry") (v "1.4.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libaes") (r "^0.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qw7jh0mkx1q06kssz8h03rxy3g35sgkhjg8dh51smjizrzfmzwm")))

