(define-module (crates-io do -n do-not-use-this-hello_cratesio) #:use-module (crates-io))

(define-public crate-do-not-use-this-hello_cratesio-0.1.0 (c (n "do-not-use-this-hello_cratesio") (v "0.1.0") (h "1zh7fx48q45m03ybajp2yywh6yya8saqknx0jdqw16n7bd8a6haf") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.1 (c (n "do-not-use-this-hello_cratesio") (v "0.1.1") (h "14cbil0ysamw0znh3mgm2n532idfq9fy31wsbgz7v7ly46bq8320") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.2 (c (n "do-not-use-this-hello_cratesio") (v "0.1.2") (h "13jls4m7a723d22lb76c2iv9n6mg7hrr80bjmga4rpl17ypj2m1r") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.3 (c (n "do-not-use-this-hello_cratesio") (v "0.1.3") (h "0awv8bc06982abzdnf7nq6glh5jpxb4anpwx2dyx1p98c2idgg03") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.4 (c (n "do-not-use-this-hello_cratesio") (v "0.1.4") (h "1cm7sdmajlwzfqlk3drdbc444kn00hx9y6f7z88lvq4zkwd70hkd") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.5 (c (n "do-not-use-this-hello_cratesio") (v "0.1.5") (h "02xbd1ic3sksaxkm2vasli7xwsgixbikavjmq0nv5pcy1q19drmi") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.6 (c (n "do-not-use-this-hello_cratesio") (v "0.1.6") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1qvbk7vs132l12b6ayqbx4ipzqjkpnccaiifhsl92026qknfcx62") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.7 (c (n "do-not-use-this-hello_cratesio") (v "0.1.7") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "09b765bayhnb9h2784lkmmgjrr2l17h8xlsi1n7r7292jj9vfjgp") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.8 (c (n "do-not-use-this-hello_cratesio") (v "0.1.8") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0j4ai7fncyl541q7hmk1wakh8gfp59bivdz1lxkbdk51s8n28m0y") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.9 (c (n "do-not-use-this-hello_cratesio") (v "0.1.9") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1fkci2wg5y6ykzjhs0rrc44b0s53qf1jx75qrggvwznf16bz2j8q") (y #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1.10 (c (n "do-not-use-this-hello_cratesio") (v "0.1.10") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "10np28n6m6ap59995db8bi8v4anb9zxm1r8fn4fkr2gkb5lyvl18")))

