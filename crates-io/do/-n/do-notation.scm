(define-module (crates-io do -n do-notation) #:use-module (crates-io))

(define-public crate-do-notation-0.1.0 (c (n "do-notation") (v "0.1.0") (h "1md4ngsv3123hjz56nlk5bid24kn5i1737shfk6k56ra9apk7gi1")))

(define-public crate-do-notation-0.1.1 (c (n "do-notation") (v "0.1.1") (h "0aryqir3fckvbc7i9ccs806gh6b0apx0fiqwbcfx0962rrkw06xv")))

(define-public crate-do-notation-0.1.2 (c (n "do-notation") (v "0.1.2") (h "10h8d2yxyypwacjxy746cwm4qlrm0li0hil3sws6xx4k9r1g9svc")))

(define-public crate-do-notation-0.1.3 (c (n "do-notation") (v "0.1.3") (h "1lkdiz3pz7y3p3y5b0fa3lrvd66psckn8407z99cz8nxq606mqd3")))

