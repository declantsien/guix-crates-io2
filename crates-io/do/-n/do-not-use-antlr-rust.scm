(define-module (crates-io do -n do-not-use-antlr-rust) #:use-module (crates-io))

(define-public crate-do-not-use-antlr-rust-0.3.0 (c (n "do-not-use-antlr-rust") (v "0.3.0") (d (list (d (n "better_any") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "bit-set") (r "=0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "murmur3") (r "=0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0") (d #t) (k 0)) (d (n "uuid") (r "=0.8") (d #t) (k 0)))) (h "0sni5f854naky3sm1x7mx8k225w1bqqs9iwf1gzbrn6rdpbzvqwk")))

