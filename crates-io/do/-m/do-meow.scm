(define-module (crates-io do -m do-meow) #:use-module (crates-io))

(define-public crate-do-meow-0.1.0 (c (n "do-meow") (v "0.1.0") (d (list (d (n "inline_colorization") (r "^0.1") (d #t) (k 0)) (d (n "open") (r "^5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "02nja77039y9kn8i43azbskdbl38gqyxnxwsc2xww3s976kxgpwr")))

(define-public crate-do-meow-0.1.1 (c (n "do-meow") (v "0.1.1") (d (list (d (n "inline_colorization") (r "^0.1") (d #t) (k 0)) (d (n "open") (r "^5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "1x7j1gmfvagn5n70ngh349k2y2ki3wdxyn606gf6vllp6jzjdcn0")))

(define-public crate-do-meow-0.2.0 (c (n "do-meow") (v "0.2.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "enable-ansi-support") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "0a44wv3f999sazxzj8gcq4cvn30k528vy1jjg8cd6l25imzdwxjr")))

