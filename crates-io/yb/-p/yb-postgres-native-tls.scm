(define-module (crates-io yb -p yb-postgres-native-tls) #:use-module (crates-io))

(define-public crate-yb-postgres-native-tls-0.5.0-yb-1 (c (n "yb-postgres-native-tls") (v "0.5.0-yb-1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "rt"))) (d #t) (k 2)) (d (n "tokio-native-tls") (r "^0.3") (d #t) (k 0)) (d (n "yb-postgres") (r "^0.19.7-yb-1-beta") (d #t) (k 2)) (d (n "yb-tokio-postgres") (r "^0.7.10-yb-1-beta") (d #t) (k 0)))) (h "1irswxdlc4n0hr2bm20wfl38mwcgwf05xa2afw7rbfjk79wjb36r") (f (quote (("runtime" "yb-tokio-postgres/runtime") ("default" "runtime"))))))

