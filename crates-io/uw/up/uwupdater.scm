(define-module (crates-io uw up uwupdater) #:use-module (crates-io))

(define-public crate-uwupdater-0.1.0 (c (n "uwupdater") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08dycmghncpkir1lvi01yyxrg68zxbmrshp7lsh9g3hsv6b8p3bl")))

(define-public crate-uwupdater-0.1.1 (c (n "uwupdater") (v "0.1.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10xs2yy4k50c3mqlypql8hl4kkkiijm01ix3h1p7lbz89s8z3d7q")))

(define-public crate-uwupdater-0.1.3 (c (n "uwupdater") (v "0.1.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0p2c3q7yhqapfsab53i81zwlbiv7srkxdm064yxgapim95x0vnk2")))

