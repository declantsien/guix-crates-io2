(define-module (crates-io uw uh uwuhi-async) #:use-module (crates-io))

(define-public crate-uwuhi-async-0.3.0 (c (n "uwuhi-async") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "uwuhi") (r "^0.3.0") (d #t) (k 0)))) (h "1hsvcg4i7kby1y024fwf3rr1zfrbrfns0sblywpxc58sgxc2g692")))

(define-public crate-uwuhi-async-0.4.0 (c (n "uwuhi-async") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "uwuhi") (r "^0.4.0") (d #t) (k 0)))) (h "18gxr58hsaicayxrkabg9y7cs4k03c1jyc8iglp52y59mnr28qv8")))

(define-public crate-uwuhi-async-0.4.1 (c (n "uwuhi-async") (v "0.4.1") (d (list (d (n "async-io") (r "^2.3.2") (d #t) (k 0)) (d (n "futures-lite") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "uwuhi") (r "^0.4.1") (d #t) (k 0)))) (h "0nf9p06hznwxhn35nyps7cl9k3sb34pnk8j65s7bf80sv09z517m")))

