(define-module (crates-io uw u_ uwu_cli) #:use-module (crates-io))

(define-public crate-uwu_cli-0.1.0 (c (n "uwu_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "uwu-rs") (r "^0.1.0") (d #t) (k 0)))) (h "0shds98dq60fiwchh63zs51kf6miy3nq3m0pbf1i9vbp0x9vdc47")))

(define-public crate-uwu_cli-1.0.0 (c (n "uwu_cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "uwu-rs") (r "^1.0.0") (d #t) (k 0)))) (h "0gfxx5y6x985kizqdcpdg1y78m3xp7zf94jnvnapdf744klkhnd5")))

