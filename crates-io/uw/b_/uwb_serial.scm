(define-module (crates-io uw b_ uwb_serial) #:use-module (crates-io))

(define-public crate-uwb_serial-1.0.0 (c (n "uwb_serial") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serialport") (r "^4.2") (d #t) (k 0)))) (h "012r2kk1vqg6p0zmvm79bkr663l3kc8x1bllasbdbzr86x6pqgii")))

