(define-module (crates-io uw eb uwebsockets_rs) #:use-module (crates-io))

(define-public crate-uwebsockets_rs-0.0.1 (c (n "uwebsockets_rs") (v "0.0.1") (d (list (d (n "libuwebsockets-sys") (r "^0.0.2") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "04jrrg2ba1c06ii3h97nd5b8p0a4hc2q87i3hxyxw83jxb21s65z") (y #t)))

(define-public crate-uwebsockets_rs-0.0.2 (c (n "uwebsockets_rs") (v "0.0.2") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "1df50mf1g690l4j18rxvfzf7z9wd80zzxi8ynx8nxrw9nd5bj3sk") (y #t)))

(define-public crate-uwebsockets_rs-0.0.3 (c (n "uwebsockets_rs") (v "0.0.3") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "0my1jhqmqhxhs09a943h5mkjdxw53dcwgnz4k4lkvdwy8764i5dd") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.4 (c (n "uwebsockets_rs") (v "0.0.4") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "0gsrfnjwnyfyxkshjrbvzavwjklyv6z391sv4kkgazjp62jr1jnh") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.5 (c (n "uwebsockets_rs") (v "0.0.5") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "1zfiam6jka2gxvnyc5dwa8j6iwy1xd22wj0jn8wpygbq3v9326sd") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.6 (c (n "uwebsockets_rs") (v "0.0.6") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "0bk7brcpysd166lpij6rq4m62p466md1zayr6v2zzm9bdszcxd2j") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.7 (c (n "uwebsockets_rs") (v "0.0.7") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "1r9v0mxfxyyw6y6fc8nmpgdkffg0izm8nqjal1a1as7dgg4jxva9") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.8 (c (n "uwebsockets_rs") (v "0.0.8") (d (list (d (n "libuwebsockets-sys") (r "^0.0.7") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "0fj4b008fg13fwz34h5hcdh9j6hvd25frrx71ilb8kh0fzq838wb") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.9 (c (n "uwebsockets_rs") (v "0.0.9") (d (list (d (n "libuwebsockets-sys") (r "^0.0.9") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "1schi7xpq8nhrvmb8x4hqbkdw7gpxksb6a5yhp0ppd4mqnfdszka") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.10 (c (n "uwebsockets_rs") (v "0.0.10") (d (list (d (n "libuwebsockets-sys") (r "^0.0.9") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "1n07am2vfcg3f2yjg14mqd74j32xiclhjhcympilrl6ld315ric6") (f (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.11 (c (n "uwebsockets_rs") (v "0.0.11") (d (list (d (n "libuwebsockets-sys") (r "^0.0.9") (f (quote ("uws_vendored"))) (d #t) (k 0)))) (h "0rk76vs8z8dx630dj1gccwjsxwdwj1cinaskwma0v07rnva46ixl") (f (quote (("native-access"))))))

