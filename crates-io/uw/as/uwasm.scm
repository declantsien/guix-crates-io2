(define-module (crates-io uw as uwasm) #:use-module (crates-io))

(define-public crate-uwasm-0.1.0 (c (n "uwasm") (v "0.1.0") (h "17z3bvh49qlc88w11w53hxclh4qk55xmd27nphasxf4dq7nnn1wh")))

(define-public crate-uwasm-0.1.1 (c (n "uwasm") (v "0.1.1") (h "17jgcf7i7l32pa5qyi0n2v7bxc1nb9hwa86c6g2lbjkxg2gsb7k7")))

(define-public crate-uwasm-0.2.0 (c (n "uwasm") (v "0.2.0") (h "19vf0nmy1yrp42ghjawzkmishr41d2ipdj78iz0garp4vmf333an")))

