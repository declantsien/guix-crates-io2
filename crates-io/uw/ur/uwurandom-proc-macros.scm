(define-module (crates-io uw ur uwurandom-proc-macros) #:use-module (crates-io))

(define-public crate-uwurandom-proc-macros-1.0.0 (c (n "uwurandom-proc-macros") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0pipnnksqj8jc99aqdk3kx1x76b1v6vzj64vcpw0r8zdrx3sdzq4")))

