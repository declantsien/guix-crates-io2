(define-module (crates-io uw ur uwurandom-rs) #:use-module (crates-io))

(define-public crate-uwurandom-rs-1.0.0 (c (n "uwurandom-rs") (v "1.0.0") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "uwurandom-proc-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "010a30npm117qqapnvbmrwx1yzwmik7xp73la5r99fx8wc6af7qh")))

(define-public crate-uwurandom-rs-1.1.0 (c (n "uwurandom-rs") (v "1.1.0") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "uwurandom-proc-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0jsb210pnc3vckypsbi785r01hkdgqnm45rmx3lh8qjaai6qmn6i")))

