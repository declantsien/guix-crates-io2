(define-module (crates-io uw ur uwurandom) #:use-module (crates-io))

(define-public crate-uwurandom-0.1.0 (c (n "uwurandom") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "uwurandom-rs") (r "^1.0.0") (d #t) (k 0)))) (h "0yr8kqxv7al3qwfg88hz1pz0wwbz21iyy4yx1ihmpwmp75sqw1kn")))

