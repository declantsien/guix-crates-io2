(define-module (crates-io uw ui uwuifyy) #:use-module (crates-io))

(define-public crate-uwuifyy-0.1.0 (c (n "uwuifyy") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "linkify") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)))) (h "07fsaznnkg2mqrlji8l8fbg6j9clil1mqp4s7q0lyfjjhskrczh3")))

(define-public crate-uwuifyy-0.1.1 (c (n "uwuifyy") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "linkify") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)))) (h "0l3i9jqprkh7jv1jlibf6xjrs1s5jcdgf6asdqq4pin0788p2dlr")))

(define-public crate-uwuifyy-0.2.0 (c (n "uwuifyy") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "linkify") (r "^0.8.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "19ad9zys0k42xqwllrz91qida9byf9c6bw1srfs9221xg844jing") (f (quote (("bench"))))))

(define-public crate-uwuifyy-0.3.0 (c (n "uwuifyy") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "linkify") (r "^0.8.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "1ayl1dlpnq66hgnab91xpxzhjw339vf1v33jab8dagfmj6g8nf4y") (f (quote (("bench"))))))

