(define-module (crates-io uw ui uwuify) #:use-module (crates-io))

(define-public crate-uwuify-0.1.0 (c (n "uwuify") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0fc641dhqndzza72hvs57nnv1pbpvflhccjw7rpkn9j2kdgyh38v")))

(define-public crate-uwuify-0.2.0 (c (n "uwuify") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0q9v0srk7i2qk9ywgv4dz38fv86s5rhmn052ja0mwicy7f8rx4c8") (f (quote (("default" "bin") ("bin" "clap" "owo-colors" "thiserror"))))))

(define-public crate-uwuify-0.2.1 (c (n "uwuify") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "05v3izicwbcg1xx2lrb0bp4nhn1pya2lb4qz6zafdgaxzvfn0m92") (f (quote (("default" "bin") ("bin" "clap" "owo-colors" "thiserror"))))))

(define-public crate-uwuify-0.2.2 (c (n "uwuify") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "093yq2kflr5f750161wnfcgvrnzcj347q5brdj32xzfwg85q9dix") (f (quote (("default" "bin") ("bin" "clap" "owo-colors" "thiserror"))))))

