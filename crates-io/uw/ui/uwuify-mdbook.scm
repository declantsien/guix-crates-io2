(define-module (crates-io uw ui uwuify-mdbook) #:use-module (crates-io))

(define-public crate-uwuify-mdbook-0.1.0 (c (n "uwuify-mdbook") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^6.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uwuify") (r "^0.2") (d #t) (k 0)))) (h "02625hnx5gwy5jh1hcbg06ajbyqh5l6czvn7i0ll2qaybfg5mfj9")))

(define-public crate-uwuify-mdbook-0.2.0 (c (n "uwuify-mdbook") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mdbook") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^6.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uwuify") (r "^0.2") (d #t) (k 0)))) (h "07zy28jl5cx13x6wby4fwxygfpsvxg382vy4g5p59p9p2mgw9jpk")))

