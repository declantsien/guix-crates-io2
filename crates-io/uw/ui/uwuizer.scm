(define-module (crates-io uw ui uwuizer) #:use-module (crates-io))

(define-public crate-uwuizer-0.1.0 (c (n "uwuizer") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0wqkczamrfdh6m0lc9jfhpg9klsam2yfkz2sp7s05wfl25zmmm6x")))

(define-public crate-uwuizer-0.1.1 (c (n "uwuizer") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0nbsihybb2z9mnyw8ja7qkjw6c264xff05chrbc2prfs4xvlp7d3")))

(define-public crate-uwuizer-0.1.2 (c (n "uwuizer") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1wp7s1wvwr5kkjddpb3n3wph8clcqw9y98cwlvn6n8zjxckpgp5g")))

(define-public crate-uwuizer-0.2.0 (c (n "uwuizer") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1kf2n7wmn4zshg7n7gqijvibgzjcymbbsqac26ik9jh93vx0m5ls")))

(define-public crate-uwuizer-0.2.1 (c (n "uwuizer") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ya72bi0n2z66lh4wys2gsbx5wyx2ywdn9h5hcmy8l11gwda901h")))

(define-public crate-uwuizer-0.3.0 (c (n "uwuizer") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i8ia7d5n6vrdgba6cc7gsmnsa2z12pm3qj2s7xrlx616qw9kpp7")))

