(define-module (crates-io uw ui uwuid) #:use-module (crates-io))

(define-public crate-uwuid-0.1.0 (c (n "uwuid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1dwh3g571db46lx1win3l0z91dpm6y7vnidx6761a36fha40chgj")))

(define-public crate-uwuid-0.2.0 (c (n "uwuid") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "0jlpizjj48382bcwsyr4d2145rwy9k5q33lqm3318508as56sda3")))

(define-public crate-uwuid-0.3.0 (c (n "uwuid") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "19wi8yz6dbf9dq2q0sb5gnd080xvjj3hngnkcwfsvwbigg9lvgx7")))

(define-public crate-uwuid-0.3.1 (c (n "uwuid") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)))) (h "1mrbs3vkwp87s8xfjkmi4gxq5iivmh0kalpgv1wj1y6lyqa87h9f")))

