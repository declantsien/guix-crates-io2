(define-module (crates-io uw il uwildmat) #:use-module (crates-io))

(define-public crate-uwildmat-0.1.0 (c (n "uwildmat") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "0qbz72x7hgi8wjvqd938jy2j7lbi53hzr2iiiffn5pkk7c3nkj96")))

(define-public crate-uwildmat-0.2.0 (c (n "uwildmat") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "1x9j13zj0040mwjpz6gvfilkdklddyapdj5ajhw3pv5dhi3dj317")))

(define-public crate-uwildmat-0.3.0 (c (n "uwildmat") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "1y8201k339mwgsiwpz6vyahyapqbjv4q8xw5mprxr3jqv6hhaa1z")))

