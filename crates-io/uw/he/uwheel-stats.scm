(define-module (crates-io uw he uwheel-stats) #:use-module (crates-io))

(define-public crate-uwheel-stats-0.1.0 (c (n "uwheel-stats") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "minstant") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "sketches-ddsketch") (r "^0.2") (d #t) (k 0)))) (h "19vdqhklv91qb4mgy9zr7s6wq8za948scxgqqi4cvk89rzvym0w4") (s 2) (e (quote (("serde" "dep:serde" "sketches-ddsketch/use_serde"))))))

(define-public crate-uwheel-stats-0.1.1 (c (n "uwheel-stats") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "minstant") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "sketches-ddsketch") (r "^0.2") (d #t) (k 0)))) (h "0ikm0ngvdjrf27lwbb4bvgymwf0q4ihfvlqgjvvakqhi70phsplg") (s 2) (e (quote (("serde" "dep:serde" "sketches-ddsketch/use_serde"))))))

(define-public crate-uwheel-stats-0.1.2 (c (n "uwheel-stats") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "minstant") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "sketches-ddsketch") (r "^0.2") (d #t) (k 0)))) (h "0s82s2j3i4a89hdsjvj2w9m94z642fh2w95ilh7hvyr7s0pxfa76") (s 2) (e (quote (("serde" "dep:serde" "sketches-ddsketch/use_serde"))))))

