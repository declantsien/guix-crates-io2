(define-module (crates-io uw ik uwiki-types) #:use-module (crates-io))

(define-public crate-uwiki-types-0.1.0 (c (n "uwiki-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0d68mzgd9b8w6qqb4fxgx0avylb29i49lan75x81sq2bpnj2djyq")))

(define-public crate-uwiki-types-0.2.0 (c (n "uwiki-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hv34fq2ynic1073wzxhcfgwhqcg983m853g170crsh1cbd7h8yl")))

(define-public crate-uwiki-types-0.3.0 (c (n "uwiki-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fzil1v34xjxw8pnapxardf1d41v61w8mbkn5rnp6acpcr47mg89")))

