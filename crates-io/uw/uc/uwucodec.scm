(define-module (crates-io uw uc uwucodec) #:use-module (crates-io))

(define-public crate-uwucodec-0.1.0 (c (n "uwucodec") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "174hvig48rx7yfj63xv8kinmsysaidg9dalcx3ybmx0di12kn9q0")))

(define-public crate-uwucodec-0.1.1 (c (n "uwucodec") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1h6l3k3hgm4nj1v3qxdv6vn4pq2n5hfyb3vg89rw8qxslp5q7j27")))

