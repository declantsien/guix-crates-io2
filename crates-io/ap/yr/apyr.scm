(define-module (crates-io ap yr apyr) #:use-module (crates-io))

(define-public crate-apyr-0.0.0 (c (n "apyr") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (f (quote ("crossbeam-channel"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "0fhzgq2a5lxaww09jzqy3rpjpzxral2swqa6418hqrgqrhcc3q36")))

(define-public crate-apyr-0.1.0 (c (n "apyr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (f (quote ("crossbeam-channel"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "1dx2jwf3l3yc8rgb1nbhc8k6q655x3hw94r49fn02cg54862cpx0")))

