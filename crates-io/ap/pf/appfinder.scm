(define-module (crates-io ap pf appfinder) #:use-module (crates-io))

(define-public crate-appfinder-0.1.0 (c (n "appfinder") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "freedesktop-desktop-entry") (r "^0.5.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0ar508yjklnl9mh9yl0az860b3bl77ja9qk7qpgcb2n5wzyrb1zi")))

(define-public crate-appfinder-0.1.1 (c (n "appfinder") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "freedesktop-desktop-entry") (r "^0.5.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "02gq72a06cz0afgp1plknz461xbsh6zqqsc0qgs0650qypxahyqb")))

(define-public crate-appfinder-0.1.2 (c (n "appfinder") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "freedesktop-desktop-entry") (r "^0.5.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1dv03cd0giv7kw3qhmzams5akvhba6kpc070jkvxssmyyrm5v161")))

