(define-module (crates-io ap pl apple-security-framework-sys) #:use-module (crates-io))

(define-public crate-apple-security-framework-sys-2.9.1 (c (n "apple-security-framework-sys") (v "2.9.1") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0hlsjn27xw86d33hmy1aqzqqn58j1yfz37jf8y8gbsc7gq1gmb09") (f (quote (("default" "OSX_10_9") ("OSX_10_9") ("OSX_10_15" "OSX_10_14") ("OSX_10_14" "OSX_10_13") ("OSX_10_13" "OSX_10_12") ("OSX_10_12" "OSX_10_11") ("OSX_10_11" "OSX_10_10") ("OSX_10_10" "OSX_10_9")))) (r "1.60")))

