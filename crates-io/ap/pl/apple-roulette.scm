(define-module (crates-io ap pl apple-roulette) #:use-module (crates-io))

(define-public crate-apple-roulette-0.1.0 (c (n "apple-roulette") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)))) (h "0fnvslx5dy3fwizlqziimwf8cjnb5pdl6b38mjv7jfmajk7vpxsv")))

(define-public crate-apple-roulette-0.1.1 (c (n "apple-roulette") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)))) (h "1apzykslw180fs8ivzd9cs7mnbigc5iglg2yami77v2ibf25yzib")))

