(define-module (crates-io ap pl apple-bundles) #:use-module (crates-io))

(define-public crate-apple-bundles-0.5.0 (c (n "apple-bundles") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0sb0f9y5yxmb1rfm310hmb2a12ggjcccwafnqymgizjc8zaz2rd9")))

(define-public crate-apple-bundles-0.6.0 (c (n "apple-bundles") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1hyy5kbhad9n6nk0xk3wjcgrafbnv7b5h1rw4imn32b7zx2ins28")))

(define-public crate-apple-bundles-0.7.0 (c (n "apple-bundles") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "15942k08axpix55pkf6ydfrdj2p2dyv6rgcsw8zwq6yzb80wsa4h")))

(define-public crate-apple-bundles-0.8.0 (c (n "apple-bundles") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0z6l38wp8if96j90vwqfyamspckk30g81cf2ask1gk5a9ri36by7")))

(define-public crate-apple-bundles-0.9.0 (c (n "apple-bundles") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tugger-file-manifest") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "15hbfnbhapdz4hmnjlpryv09wxcvcpdgg6vx9w5klymxzsdc3saq")))

(define-public crate-apple-bundles-0.10.0 (c (n "apple-bundles") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tugger-file-manifest") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0qhg5qqpxfw3w85inll5d27hs9k89jp1nwdkv8v16a1m5y5zmcxd")))

(define-public crate-apple-bundles-0.11.0 (c (n "apple-bundles") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tugger-file-manifest") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0big2nam9vpgjyb060rx4ajji9sykka1qb1l2xf3z4802lb5mxp0")))

(define-public crate-apple-bundles-0.12.0 (c (n "apple-bundles") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tugger-file-manifest") (r "^0.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1vks2zsjdfpdx5s87c10c6zjjs3r41zx50ngblj0kh5n1s8r1l64")))

(define-public crate-apple-bundles-0.13.0 (c (n "apple-bundles") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "tugger-file-manifest") (r "^0.10.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "10kxwxaadfnzabl0ghm7mg4b924nsz3ljlqd5ccfhgilx1pxw2gf")))

(define-public crate-apple-bundles-0.14.0 (c (n "apple-bundles") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1agqsvw9h0fxbjv7k4xkljvkixh7bcl4257i6ykz2ivrrxyk9h4n")))

(define-public crate-apple-bundles-0.15.0 (c (n "apple-bundles") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.2") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1ivb33pvrd7v9bhvxbc62vhs0qpw1crpcws46yghljk4fg8gjf02")))

(define-public crate-apple-bundles-0.16.0 (c (n "apple-bundles") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plist") (r "^1.3") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1w1vy34a0hmbys4z57y4rzflza6n9wdicw1f35qa2fadqmnc8y05")))

(define-public crate-apple-bundles-0.17.0 (c (n "apple-bundles") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13px46xf9qd0vrga4mlvdphy34fsskmgb9x1wyrmwcppmixqlsvi")))

(define-public crate-apple-bundles-0.18.0 (c (n "apple-bundles") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0jnvp0600hc6qh82b0i34iz2kiw5bkdv5s7zxgl98xd2fyl38c6x") (r "1.70")))

(define-public crate-apple-bundles-0.19.0 (c (n "apple-bundles") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "18p571963l3w3nfjc7d1n1bsp12s9k6jhhn8znnjcy6aw9zc5dxb") (r "1.70")))

