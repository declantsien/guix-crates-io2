(define-module (crates-io ap pl apple-bom) #:use-module (crates-io))

(define-public crate-apple-bom-0.2.0 (c (n "apple-bom") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "07m8lqp37wvz64ghk8zn98gwkl203q0h6p96z7dnhpwz5bh6k7nx") (r "1.70")))

