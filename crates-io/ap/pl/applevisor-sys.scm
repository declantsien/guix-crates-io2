(define-module (crates-io ap pl applevisor-sys) #:use-module (crates-io))

(define-public crate-applevisor-sys-0.1.0 (c (n "applevisor-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17x7nzq0y9wl0bkymw3ynfpqq9padappd8qlb4ias2gwwnp3zzbg")))

(define-public crate-applevisor-sys-0.1.1 (c (n "applevisor-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "121kd6jiff9w2c2hbzcw7vnxf0rsvw8yn6vmq4871lvn07zxj8fv")))

