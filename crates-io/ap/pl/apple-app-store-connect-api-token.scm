(define-module (crates-io ap pl apple-app-store-connect-api-token) #:use-module (crates-io))

(define-public crate-apple-app-store-connect-api-token-0.1.0 (c (n "apple-app-store-connect-api-token") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "jsonwebtoken") (r "^8") (f (quote ("use_pem"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "1x1dazdggkz1qlrqmzn0z5kx2292jagvq5gqx1m7pcnnqjk91m53")))

