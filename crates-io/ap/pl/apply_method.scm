(define-module (crates-io ap pl apply_method) #:use-module (crates-io))

(define-public crate-apply_method-0.1.0 (c (n "apply_method") (v "0.1.0") (h "0x5ql39srm1g4hnqq8jw8vd60apdxgp5j1gdhvj8x66c8rjdjyw1")))

(define-public crate-apply_method-0.1.1 (c (n "apply_method") (v "0.1.1") (h "0005mawlhsbvvxdhapydhfsb0rvv5blfk7niy17jvafnrydvr7i9")))

