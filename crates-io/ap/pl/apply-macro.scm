(define-module (crates-io ap pl apply-macro) #:use-module (crates-io))

(define-public crate-apply-macro-0.1.0 (c (n "apply-macro") (v "0.1.0") (h "0d3xygwv926d5ddc1kgy5rk9w46b8cvdxp7a3mkglrpmnnr915id") (y #t)))

(define-public crate-apply-macro-0.1.1 (c (n "apply-macro") (v "0.1.1") (h "0li23gv17jhbfcl4lxi08p88a60gs4smnr25zvaydbnxwrg5f5nr") (y #t)))

(define-public crate-apply-macro-0.1.2 (c (n "apply-macro") (v "0.1.2") (h "1krq775h5i7kpj7bg6izl4ckxzn5c06a101l1j6s0kasi50924g7") (y #t)))

(define-public crate-apply-macro-1.0.0 (c (n "apply-macro") (v "1.0.0") (h "1v2fv6lplvhbh7324vw24lm6h6kqj0yb4wwp35cdv2qrywyaziv4") (y #t)))

(define-public crate-apply-macro-0.2.0 (c (n "apply-macro") (v "0.2.0") (h "14b38y5j9y4kknpbaijln8r0kpyzv8ayan3l0n5rsg75ypimnpfh") (y #t)))

(define-public crate-apply-macro-1.0.1 (c (n "apply-macro") (v "1.0.1") (h "0cdlx10jmgaaj808vcqah5zv2469lnyb0bznbl1wppakq381av2c") (y #t)))

(define-public crate-apply-macro-1.0.2 (c (n "apply-macro") (v "1.0.2") (h "126b8j3vcb8bgidqc75aqgw5n46jlg9z566fjb4063gxnx9nnza8") (y #t)))

(define-public crate-apply-macro-1.0.3 (c (n "apply-macro") (v "1.0.3") (h "0wk7lp15a95g7092b5z50chbyalcaki5baaazl2cjzk7psajdy0j") (y #t)))

(define-public crate-apply-macro-1.0.4 (c (n "apply-macro") (v "1.0.4") (h "1cb0c10ssbxb4wr05w7xnc72yk9g0j5hkzzx1wd7127apka5r1aw") (y #t)))

(define-public crate-apply-macro-1.0.5 (c (n "apply-macro") (v "1.0.5") (h "09d1ayy91h99nipykan38f6w1jaj7zna9winhxi37w05sydj3yjl") (y #t)))

(define-public crate-apply-macro-1.0.6 (c (n "apply-macro") (v "1.0.6") (h "1l7xr8cqzapmnjissxyvpy121iczqig08zj1z1nyasl51i79ycyq") (y #t)))

(define-public crate-apply-macro-1.0.7 (c (n "apply-macro") (v "1.0.7") (h "1m30f1q1qlz7k8aibdba6ri579k3xw0i27rbvzk7hhldqsibb0id") (y #t)))

(define-public crate-apply-macro-1.0.8 (c (n "apply-macro") (v "1.0.8") (h "12a9x53m2da6zyiqb053wn47l82vawimz188rdkjvm3m074l0xym") (y #t)))

(define-public crate-apply-macro-1.0.9 (c (n "apply-macro") (v "1.0.9") (h "0n6f4rxbdj4jy248lscqfx01kxs6v1swkdjvji4wcb4hg0qxy8v4") (y #t)))

(define-public crate-apply-macro-1.0.10 (c (n "apply-macro") (v "1.0.10") (h "0nsvljl6r9sfrhlp5yjy098kd83gv8qrmpnsd2pz0s6zixkpmybn") (y #t)))

(define-public crate-apply-macro-1.0.11 (c (n "apply-macro") (v "1.0.11") (h "1f6mh76a5h3pxy576k7yxhfnw52gprfvm1syjw28hqi574m0l6cm") (y #t)))

(define-public crate-apply-macro-1.0.12 (c (n "apply-macro") (v "1.0.12") (h "15dj0la3kv9rr5g112g1qp3rkg0dkmba1b0zlhriahqmvhvv0kws") (y #t)))

(define-public crate-apply-macro-1.0.13 (c (n "apply-macro") (v "1.0.13") (h "04x6fchq0i2azba4sfcarf3rx7qdfkv989vy2sz7vwgj3mck62vz") (y #t)))

