(define-module (crates-io ap pl apple-web-service-client) #:use-module (crates-io))

(define-public crate-apple-web-service-client-0.1.0 (c (n "apple-web-service-client") (v "0.1.0") (d (list (d (n "apple-web-service-endpoint") (r "^0.1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)))) (h "0vc7cx8zczj3qpziz1k83pyvvr0m8v353zc6557r46zqjv3ayfjx")))

(define-public crate-apple-web-service-client-0.1.1 (c (n "apple-web-service-client") (v "0.1.1") (d (list (d (n "apple-web-service-endpoint") (r "^0.1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)))) (h "0xr7yivcbaz8i8p6w1p6054c62sm3brq9mbc15ham081h5p28mmx")))

