(define-module (crates-io ap pl applevisor) #:use-module (crates-io))

(define-public crate-applevisor-0.1.0 (c (n "applevisor") (v "0.1.0") (d (list (d (n "applevisor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1aj6mkipdbmj3r7260mzxn91mi4g0kzay1y50lardf4gb18mba9i")))

(define-public crate-applevisor-0.1.1 (c (n "applevisor") (v "0.1.1") (d (list (d (n "applevisor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "03i7hvphqqwjghav8nfvnra9lfhs13sls4s8f13n207gv0jx983p")))

(define-public crate-applevisor-0.1.2 (c (n "applevisor") (v "0.1.2") (d (list (d (n "applevisor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0rdxwgj1j8hfhq4v3rvl6hs6q4p1b6lj8jg4w4zlllwvhj4kq3c0")))

