(define-module (crates-io ap pl apple-sdk) #:use-module (crates-io))

(define-public crate-apple-sdk-0.1.0 (c (n "apple-sdk") (v "0.1.0") (d (list (d (n "plist") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yssrmlp7x8avb0psgwbnn9nlg29z5d07kwkr1vnkagvi6fzhg2k") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.2.0 (c (n "apple-sdk") (v "0.2.0") (d (list (d (n "plist") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w0dj6w2yp8a0r9jbsabihmcmvijmv4ak6q01xg480qvjvv6a6d4") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.3.0 (c (n "apple-sdk") (v "0.3.0") (d (list (d (n "plist") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17nm4lbr72m9zys1wklmjqhkx2mrbc1qjg5d1ny9h89fvpclfada") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.4.0 (c (n "apple-sdk") (v "0.4.0") (d (list (d (n "plist") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (o #t) (d #t) (k 0)))) (h "0amxy2lvjdlcsklpwfxr9a0i2zgyfvmy9x4g01qfx1h6f0m1jkx0") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.5.0 (c (n "apple-sdk") (v "0.5.0") (d (list (d (n "plist") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)))) (h "0v5bqijgb0mz8bs4xr09zwcbmaw420cpgcjzxl5zbqcck2k1v376") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.5.1 (c (n "apple-sdk") (v "0.5.1") (d (list (d (n "plist") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)))) (h "1fa34n64mv4y74v1wfgddkway91h0in9psj1xidgvv0p9xplayaa") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

(define-public crate-apple-sdk-0.5.2 (c (n "apple-sdk") (v "0.5.2") (d (list (d (n "plist") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)))) (h "1dz3hmig72kdqjc8cy84zgr18cbyvji14rdh5kiiz8hd07qnva69") (f (quote (("parse" "plist" "serde_json" "serde") ("default" "parse"))))))

