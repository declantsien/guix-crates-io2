(define-module (crates-io ap pl apple-errnos) #:use-module (crates-io))

(define-public crate-apple-errnos-0.1.0 (c (n "apple-errnos") (v "0.1.0") (h "1wf6w9wa6j21jmn0msw0g31siiszm07jbgrvy2rgv2gk5s01kl51") (f (quote (("std") ("iter") ("default" "std")))) (y #t)))

(define-public crate-apple-errnos-0.1.1 (c (n "apple-errnos") (v "0.1.1") (h "0p2s18cicv6rdg51kk32rgpvsc45gfdxsg5ayl961zgwc6qacswy") (f (quote (("std") ("iter") ("default" "std")))) (y #t)))

(define-public crate-apple-errnos-0.1.2 (c (n "apple-errnos") (v "0.1.2") (h "08pb8pd1sfcap0rcyx9xvclicdw95x555lfnmjmzaxjf0l63a3al") (f (quote (("std") ("iter") ("default" "std")))) (y #t)))

(define-public crate-apple-errnos-0.1.3 (c (n "apple-errnos") (v "0.1.3") (h "16n6nlh80fa1ib4jmvx8afqpyws78y89yz6szczcr8bgwwwqzqkp") (f (quote (("std") ("iter") ("default" "std")))) (y #t)))

