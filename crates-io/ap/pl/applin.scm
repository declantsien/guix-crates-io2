(define-module (crates-io ap pl applin) #:use-module (crates-io))

(define-public crate-applin-0.1.0 (c (n "applin") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0138zyprfs99jqszy40l6yj2x92ih3pm20zll26jlv3y018icwnh")))

(define-public crate-applin-0.1.1 (c (n "applin") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0bk6dypgaw4kxdfy58jfxisll1wnfwwp7jzk15m5ganv9kzv06kv")))

(define-public crate-applin-0.1.2 (c (n "applin") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0187d6l2zz2l44vhdqilya8ld1qzk6v29dnc104ig6apq13iqd3y")))

(define-public crate-applin-0.1.3 (c (n "applin") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "102lnvjnzk8mhmf4vb9026zyx5idclwxqyx66jrzfq9lw0s31ca7")))

(define-public crate-applin-0.1.4 (c (n "applin") (v "0.1.4") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0236xzbrhfq7lhjl28qgi0k219l92qy0jmaka1j93dmv09ablvsf")))

(define-public crate-applin-0.1.5 (c (n "applin") (v "0.1.5") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "09viplhmc99snxvkhh20rdf03pdz6khrcj00cjarxy2l2kc4wkvb")))

(define-public crate-applin-0.1.6 (c (n "applin") (v "0.1.6") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "021ym75ncslwwgb9x3m3w2xjqy94k3qdvpwxhxg96vwbvmdk5915")))

(define-public crate-applin-0.1.7 (c (n "applin") (v "0.1.7") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0s63f78s5jzvxwl39yngczxf8w20mcpk3brwns4w1hkyajyga4gc") (y #t)))

(define-public crate-applin-0.2.0 (c (n "applin") (v "0.2.0") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0h3rzpiiinc1iv93nn59pi6nv4xal1khdj581s4g989c39qb3l4i")))

(define-public crate-applin-0.2.1 (c (n "applin") (v "0.2.1") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0nz9xcccmwcykncidw4wr1c2xzy5qp0mvqzp95rxvbp3g2fr7vfi")))

(define-public crate-applin-0.2.2 (c (n "applin") (v "0.2.2") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0rlrlfqjk6c4wcjz5b47rcxzic0bamxykj0fps9bfv3qlswshwkh")))

(define-public crate-applin-0.2.3 (c (n "applin") (v "0.2.3") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "04v0xmg7kx987apjmvzfgy3b1w1r44fsh01q532k69bnhjqcp2sn")))

(define-public crate-applin-0.2.4 (c (n "applin") (v "0.2.4") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "18kpvn0bqsp6dqxg2sh4hxhkbisklhbk2pk05plbbjlij0pi3mwn")))

(define-public crate-applin-0.2.5 (c (n "applin") (v "0.2.5") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "196p9njv7hm0ailmcyjfsxgn6dn99qfdzxy54nqs33wp657yxjzg")))

(define-public crate-applin-0.2.6 (c (n "applin") (v "0.2.6") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "1gghncvx0sc2mszgzwjbk6zhpwijhpb0nm0jvimmfgri56gpfxiq")))

(define-public crate-applin-0.2.7 (c (n "applin") (v "0.2.7") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0xf2r84cccwkdq3hqz5ca9g4020ssr33ylpzi029hk83pipi4s4p")))

(define-public crate-applin-0.2.8 (c (n "applin") (v "0.2.8") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("alloc" "chacha" "std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "servlin") (r "^0.4.3") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "03iy00wnf0bihlqqsh5fspdazymdn9hqnmj1kr8gqasnd474ygyw")))

