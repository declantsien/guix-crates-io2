(define-module (crates-io ap pl apple-search-ads-client-secret-cli) #:use-module (crates-io))

(define-public crate-apple-search-ads-client-secret-cli-0.1.0 (c (n "apple-search-ads-client-secret-cli") (v "0.1.0") (d (list (d (n "apple-search-ads-client-secret") (r "^0.1") (d #t) (k 0)))) (h "1fym01jn787knqi0b36468x757qyv6sp7xqibw12dnvpfbv0vawm")))

(define-public crate-apple-search-ads-client-secret-cli-0.1.1 (c (n "apple-search-ads-client-secret-cli") (v "0.1.1") (d (list (d (n "apple-search-ads-client-secret") (r "^0.2") (d #t) (k 0)))) (h "1dscs2wmjmxi6nhrb5323m8p3pvw5ja7hf1qx6d6m4ga2a27n2ca")))

