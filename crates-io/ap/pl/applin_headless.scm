(define-module (crates-io ap pl applin_headless) #:use-module (crates-io))

(define-public crate-applin_headless-0.1.0 (c (n "applin_headless") (v "0.1.0") (d (list (d (n "applin") (r "^0.2.7") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "082z7aabq57qrwhh25r48zzprc0sy1b83jsc6vwskwli2rp48bns")))

