(define-module (crates-io ap pl apple-bindgen-helmer-fork) #:use-module (crates-io))

(define-public crate-apple-bindgen-helmer-fork-0.2.0 (c (n "apple-bindgen-helmer-fork") (v "0.2.0") (d (list (d (n "apple-sdk") (r "^0.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.6") (d #t) (k 0)))) (h "1r551jzrrk1qfxdsd6cynrj4crbyqsx768k4vvqkxcazknyx25wp") (f (quote (("default" "bin") ("bin" "clap"))))))

