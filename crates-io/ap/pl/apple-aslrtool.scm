(define-module (crates-io ap pl apple-aslrtool) #:use-module (crates-io))

(define-public crate-apple-aslrtool-1.0.0 (c (n "apple-aslrtool") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "mach2") (r "^0.4.1") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (k 0)))) (h "12kg1amlrz2iw7kbq4q1vph4g0vzz2v8yiix32vygnkdzv950gh0")))

