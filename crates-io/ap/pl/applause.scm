(define-module (crates-io ap pl applause) #:use-module (crates-io))

(define-public crate-applause-0.0.1 (c (n "applause") (v "0.0.1") (h "1cmirvm6n1klm4l2z4l26vp51qby78skpkfb7wvqaqjys6f9yinx")))

(define-public crate-applause-0.1.1 (c (n "applause") (v "0.1.1") (d (list (d (n "applause_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "inventory") (r "^0.3.14") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "1ij9q1l9a1rm13q40dwbxs32yijlrvs8l4a99gwkb1vshbbc3nmw") (f (quote (("default" "derive" "log")))) (s 2) (e (quote (("log" "dep:log") ("derive" "dep:applause_derive"))))))

