(define-module (crates-io ap pl apple-web-service-endpoint) #:use-module (crates-io))

(define-public crate-apple-web-service-endpoint-0.1.0 (c (n "apple-web-service-endpoint") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "05k2kr5gmqhjbczsn8a2pl1gid9kfxhvv0j01f327nxrv9cch19c")))

(define-public crate-apple-web-service-endpoint-0.1.1 (c (n "apple-web-service-endpoint") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (k 0)))) (h "17jfg49idkwgxmvxjj10396b1r3zqb7sw99q625k3vgld1qj12pp")))

