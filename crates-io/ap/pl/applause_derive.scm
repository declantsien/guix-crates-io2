(define-module (crates-io ap pl applause_derive) #:use-module (crates-io))

(define-public crate-applause_derive-0.1.0 (c (n "applause_derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "16vrmvk5bfa442xplrvhayxav694kzwpd6fwpahfi2x2vj39d30g") (r "1.60.0")))

(define-public crate-applause_derive-0.1.1 (c (n "applause_derive") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1379ainpqaqwlyp60r63g0q41pznhgbr4mlc0ald52g870ggsqd6") (r "1.60.0")))

