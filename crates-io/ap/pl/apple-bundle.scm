(define-module (crates-io ap pl apple-bundle) #:use-module (crates-io))

(define-public crate-apple-bundle-0.0.1 (c (n "apple-bundle") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)))) (h "0a9i5xir193ri4j14d25zdi0aj5ksaqpscfr9rqvxfzz7wkyaw6a")))

(define-public crate-apple-bundle-0.0.2 (c (n "apple-bundle") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)))) (h "1l5kkqjz9hgr4sgh2s0260wy8d94vchhfphd1k1b75xc5q6xkn59")))

(define-public crate-apple-bundle-0.0.3 (c (n "apple-bundle") (v "0.0.3") (d (list (d (n "plist") (r "^1.0.1") (d #t) (k 2) (p "creator-plist")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0v6rjd8cnfsshb2bbk56v43g71hv0id4k2dyw66nzr6ivdfc4nbi")))

(define-public crate-apple-bundle-0.0.4 (c (n "apple-bundle") (v "0.0.4") (d (list (d (n "plist") (r "^1.0.1") (d #t) (k 2) (p "creator-plist")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1v9lhnvca7pl3bkmxsiwmws1pvnbibziwsfxpmr7nsnx91ds2nfk")))

(define-public crate-apple-bundle-0.0.5 (c (n "apple-bundle") (v "0.0.5") (d (list (d (n "plist") (r "^1.0.1") (o #t) (d #t) (k 0) (p "creator-plist")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "16yw00fsfh0x4lif4l8lnsp8szqg84303l009jj3r72qhmrd6f1z") (f (quote (("default" "plist"))))))

(define-public crate-apple-bundle-0.1.0 (c (n "apple-bundle") (v "0.1.0") (d (list (d (n "plist") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0ydr0rk8fyw37wdga6wdd2hsv0vn1ri4iw5i63cyhr8mvldxr0lp") (f (quote (("default" "plist"))))))

(define-public crate-apple-bundle-0.1.1 (c (n "apple-bundle") (v "0.1.1") (d (list (d (n "plist") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "07ypfg3klx0i068w4kc976q5d666qaknljfbi7i8nv36n1ggv6ay") (f (quote (("default" "plist"))))))

(define-public crate-apple-bundle-0.1.2 (c (n "apple-bundle") (v "0.1.2") (d (list (d (n "plist") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0zp7bvh8rpqi6xfsk9rfxw9vz5cxahqch52jpgqf3lwyqpi6xa39") (f (quote (("default" "plist"))))))

(define-public crate-apple-bundle-0.1.3 (c (n "apple-bundle") (v "0.1.3") (d (list (d (n "plist") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "010ry45zc3xk0gicf39f3n1mcrbzjgxnzxs0z3dww92rr6d0068g") (f (quote (("default" "plist"))))))

(define-public crate-apple-bundle-0.1.4 (c (n "apple-bundle") (v "0.1.4") (d (list (d (n "plist") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "10kjbqkk4z8sbzxqykg7x7cp7g0dbszbc0jsxv9200mslyfkm6ls") (f (quote (("default" "plist"))))))

