(define-module (crates-io ap pl apple-rs-sys) #:use-module (crates-io))

(define-public crate-apple-rs-sys-0.0.1-pre.1 (c (n "apple-rs-sys") (v "0.0.1-pre.1") (d (list (d (n "objc") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "objc2") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1av5rdgqlhnnk8kaczfpdwh7y7mr3rhfnpqpi58cg6lbjmyarlwn") (f (quote (("foundation") ("default" "objc" "foundation")))) (y #t) (s 2) (e (quote (("objc2" "dep:objc2") ("objc" "dep:objc"))))))

