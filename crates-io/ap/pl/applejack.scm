(define-module (crates-io ap pl applejack) #:use-module (crates-io))

(define-public crate-applejack-0.1.0 (c (n "applejack") (v "0.1.0") (h "0767g0km45vsm2mssii53rb8ihmwfrwjhhf85n6g0npyq9d4p5ma")))

(define-public crate-applejack-0.2.0 (c (n "applejack") (v "0.2.0") (d (list (d (n "minivec") (r "^0.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "14gagamb91ya67y89gicab4izan3jzry01lm9xsvxf41v7y8d203")))

(define-public crate-applejack-0.2.1 (c (n "applejack") (v "0.2.1") (h "1x7a1mb1j6hp9zi5121ln9rawkk0d6riq4a1dvfggihliy0chv7y")))

