(define-module (crates-io ap pl apple-siwa-client-secret) #:use-module (crates-io))

(define-public crate-apple-siwa-client-secret-0.1.0 (c (n "apple-siwa-client-secret") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "jwt") (r "^0.15") (f (quote ("openssl"))) (k 0)) (d (n "openssl") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1np9jbb6d41wycnw1i7132zgk61kifsgjzfr3jq96qsqxq2rz9bg")))

