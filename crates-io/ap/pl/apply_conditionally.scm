(define-module (crates-io ap pl apply_conditionally) #:use-module (crates-io))

(define-public crate-apply_conditionally-1.0.0 (c (n "apply_conditionally") (v "1.0.0") (d (list (d (n "either") (r "^1.10.0") (k 0)))) (h "013kd3ri136k8rb209a880cxwpi1ili8mlcxbh38k1l9ma1dvxa2")))

(define-public crate-apply_conditionally-2.0.0 (c (n "apply_conditionally") (v "2.0.0") (d (list (d (n "either") (r "^1.11.0") (k 0)))) (h "0xwkzz9ank144bar9pbrg4xnf1m9x6drsh1ys9yjl9dgvym1qx0v")))

