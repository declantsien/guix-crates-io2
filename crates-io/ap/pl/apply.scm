(define-module (crates-io ap pl apply) #:use-module (crates-io))

(define-public crate-apply-0.1.0 (c (n "apply") (v "0.1.0") (h "12ryv4d8dh1xs114fc672bgs8abw35id6x28r8ha0j81rb1hpbd0")))

(define-public crate-apply-0.1.1 (c (n "apply") (v "0.1.1") (h "0nyavb9minndm88pi86pnrp88py8dlhilg7rr81bkkcp01p7dw3w")))

(define-public crate-apply-0.2.1 (c (n "apply") (v "0.2.1") (h "1ajxvf76j11g564diqfns25l2kzxncg8j81mjmrxr1k73cm41ywk")))

(define-public crate-apply-0.2.2 (c (n "apply") (v "0.2.2") (h "1x9qs9n1875mqwqzmax14xdc418zbfijdfra7rlkgq5qxi8a2w8x")))

(define-public crate-apply-0.3.0 (c (n "apply") (v "0.3.0") (h "1fdfp2pi25sxipyzxq2v6i6fxbcggwibaiaddbicmqr18py5fyzl")))

