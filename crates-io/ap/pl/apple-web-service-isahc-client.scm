(define-module (crates-io ap pl apple-web-service-isahc-client) #:use-module (crates-io))

(define-public crate-apple-web-service-isahc-client-0.1.0 (c (n "apple-web-service-isahc-client") (v "0.1.0") (d (list (d (n "apple-web-service-client") (r "^0.1") (k 0)) (d (n "futures-lite") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "isahc") (r "^0.9") (k 0)))) (h "0rw5zrxcczjldaqlxsn6raddb48g4xa9iawiqrzp4j2ll71hsl8l")))

(define-public crate-apple-web-service-isahc-client-0.1.1 (c (n "apple-web-service-isahc-client") (v "0.1.1") (d (list (d (n "apple-web-service-client") (r "^0.1") (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "isahc") (r "^1.0") (k 0)))) (h "0rkgfpc4visfykb5xp9cswdf39c4sc0g0ggmya2fhrd9825wdhgk")))

(define-public crate-apple-web-service-isahc-client-0.1.2 (c (n "apple-web-service-isahc-client") (v "0.1.2") (d (list (d (n "apple-web-service-client") (r "^0.1") (k 0)) (d (n "isahc") (r "^1.1") (k 0)))) (h "19n8sy1xiw8s3xa0dzvh66rk9rs9j6ypxs1kdkl3yidfpd12qzkz")))

(define-public crate-apple-web-service-isahc-client-0.1.3 (c (n "apple-web-service-isahc-client") (v "0.1.3") (d (list (d (n "apple-web-service-client") (r "^0.1") (k 0)) (d (n "isahc") (r "^1.4") (k 0)))) (h "17myph6mbsd8060iziap29jc4pyra5a04sabirdxpf8p16gxjhx4")))

(define-public crate-apple-web-service-isahc-client-0.1.4 (c (n "apple-web-service-isahc-client") (v "0.1.4") (d (list (d (n "apple-web-service-client") (r "^0.1") (k 0)) (d (n "isahc") (r "^1.4") (k 0)))) (h "0xila6ivvyyyi10l3rzxrrxy1l0hz0lsknmw8rvjkx3wq58vkqz9")))

