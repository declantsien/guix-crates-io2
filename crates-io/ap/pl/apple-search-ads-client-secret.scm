(define-module (crates-io ap pl apple-search-ads-client-secret) #:use-module (crates-io))

(define-public crate-apple-search-ads-client-secret-0.1.0 (c (n "apple-search-ads-client-secret") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "jwt") (r "^0.16") (f (quote ("openssl"))) (k 0)) (d (n "openssl") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0lka4qbp1bqmbxic6k6gvjz3nkfak6kzjm1lgy8ckzn12mzxv8p4")))

(define-public crate-apple-search-ads-client-secret-0.2.0 (c (n "apple-search-ads-client-secret") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "serde"))) (k 0)) (d (n "jsonwebtoken") (r "^8") (f (quote ("use_pem"))) (k 0)) (d (n "openssl") (r "^0.10") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)))) (h "1dg7i2w19vl473xp4kc5v4payiz4kq52rhpqm55cxxsd3ax498ba")))

