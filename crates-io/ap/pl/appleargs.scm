(define-module (crates-io ap pl appleargs) #:use-module (crates-io))

(define-public crate-appleargs-0.1.0 (c (n "appleargs") (v "0.1.0") (h "168nl4rl91vm195dq88v54mfh7r668xxy9v0qvdjczsqgnslq37a")))

(define-public crate-appleargs-0.1.1 (c (n "appleargs") (v "0.1.1") (h "1c4hks04yf1i43vcf8x5a6l6pf8lmrnikw5a1lz5azdnh43733x5")))

