(define-module (crates-io ap pl apple-security-sys) #:use-module (crates-io))

(define-public crate-apple-security-sys-2.9.2 (c (n "apple-security-sys") (v "2.9.2") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0ncvwmqp0izwx8dfg3hgq77gvkzkqb5cav04i0d5lsn7h1rl0z2k") (f (quote (("default" "OSX_10_9") ("OSX_10_9") ("OSX_10_15" "OSX_10_14") ("OSX_10_14" "OSX_10_13") ("OSX_10_13" "OSX_10_12") ("OSX_10_12" "OSX_10_11") ("OSX_10_11" "OSX_10_10") ("OSX_10_10" "OSX_10_9"))))))

(define-public crate-apple-security-sys-2.9.3 (c (n "apple-security-sys") (v "2.9.3") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "11023cwba2pylk8l2fy42sq8by78v0wxmxkhqyf9xa5zqcm9mnm4") (f (quote (("default" "OSX_10_9") ("OSX_10_9") ("OSX_10_15" "OSX_10_14") ("OSX_10_14" "OSX_10_13") ("OSX_10_13" "OSX_10_12") ("OSX_10_12" "OSX_10_11") ("OSX_10_11" "OSX_10_10") ("OSX_10_10" "OSX_10_9"))))))

(define-public crate-apple-security-sys-2.9.4 (c (n "apple-security-sys") (v "2.9.4") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1aba69dajr9j9f12i4pichp47nvai83rzg5cmpw7lkq7svj0bd2i") (f (quote (("default" "OSX_10_9") ("OSX_10_9") ("OSX_10_15" "OSX_10_14") ("OSX_10_14" "OSX_10_13") ("OSX_10_13" "OSX_10_12") ("OSX_10_12" "OSX_10_11") ("OSX_10_11" "OSX_10_10") ("OSX_10_10" "OSX_10_9"))))))

