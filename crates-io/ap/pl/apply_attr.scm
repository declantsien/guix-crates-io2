(define-module (crates-io ap pl apply_attr) #:use-module (crates-io))

(define-public crate-apply_attr-0.1.0 (c (n "apply_attr") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (d #t) (k 2)))) (h "11zwqmrxzh5pidn369jd7h08riv90i0a7mb9jrsz5w2mfbc3nwhr")))

(define-public crate-apply_attr-0.2.0 (c (n "apply_attr") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (d #t) (k 2)))) (h "10ibxs30qx4isd4yqd9bprm265hs0z9n1ib925v5qcmxarqdswkq")))

(define-public crate-apply_attr-0.2.1 (c (n "apply_attr") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (d #t) (k 2)))) (h "17ccr2pm1vc91d6y9pydm7hf18kbvm3ligc1kfpky8bg0yph91nn")))

(define-public crate-apply_attr-0.2.2 (c (n "apply_attr") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.1") (d #t) (k 2)))) (h "1iancvq511qdjaz42ldfhzw5gi7ac358mngvcp66m8pw0y53f1cw")))

(define-public crate-apply_attr-0.2.3 (c (n "apply_attr") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.1") (d #t) (k 2)))) (h "0x22271cv213mjbwnxsxam72fk5r0pzm3vq78al1k3yjvl91w2l7")))

(define-public crate-apply_attr-0.2.4 (c (n "apply_attr") (v "0.2.4") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.1") (d #t) (k 2)))) (h "1sd0plnmljd4n1jyzylc4xgmdghdsdqj5yn4cvdbmkd89c40lm8p")))

