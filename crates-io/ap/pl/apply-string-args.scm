(define-module (crates-io ap pl apply-string-args) #:use-module (crates-io))

(define-public crate-apply-string-args-1.0.0 (c (n "apply-string-args") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1xnsxd4z8qg5krkrz4v9wjf1pn4hnvnascpzz9dl2djqlffqahan") (y #t)))

