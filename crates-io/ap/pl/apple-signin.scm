(define-module (crates-io ap pl apple-signin) #:use-module (crates-io))

(define-public crate-apple-signin-0.1.0 (c (n "apple-signin") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^9.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0fghhnssk2839m7fc5fbhl7wxmm136p3kqwhwnx2nk72m4v3r5si") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls"))))))

(define-public crate-apple-signin-0.1.1 (c (n "apple-signin") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^9.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0n12miblc0b54qlk978bgqrz2i4x80zzjvyc1mwgh7lq6vynzvdp") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls"))))))

