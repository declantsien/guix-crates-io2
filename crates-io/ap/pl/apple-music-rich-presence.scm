(define-module (crates-io ap pl apple-music-rich-presence) #:use-module (crates-io))

(define-public crate-apple-music-rich-presence-0.1.0 (c (n "apple-music-rich-presence") (v "0.1.0") (d (list (d (n "discord-presence") (r "^0.5.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0h6gah56acaxzp20s4n61szg0dfvnrmwqshx04dz47agi8db21c0")))

