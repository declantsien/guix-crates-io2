(define-module (crates-io ap pl apply-user-defaults) #:use-module (crates-io))

(define-public crate-apply-user-defaults-0.1.0 (c (n "apply-user-defaults") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("color" "suggestions"))) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("color" "suggestions"))) (k 1)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "153q7dy7frz0s26xq1jnh992kz1q715xz9spnk0y17iq8zjyqwad")))

(define-public crate-apply-user-defaults-0.1.1 (c (n "apply-user-defaults") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.33") (f (quote ("color" "suggestions"))) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("color" "suggestions"))) (k 1)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0kzy5adwcw7yb2b285b1jl1xvy7llasywaibhamdjn85wsm53h9q")))

(define-public crate-apply-user-defaults-0.1.2 (c (n "apply-user-defaults") (v "0.1.2") (d (list (d (n "assert_cli") (r "~0.6.0") (d #t) (k 2)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "suggestions"))) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "suggestions"))) (k 1)) (d (n "colored") (r "~1.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "regex") (r "~1.3.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4.0") (d #t) (k 0)))) (h "14736c9c86r4afm2yfp52b1mxqzfqrlldwdd2zq77rmh1f5b18rl")))

