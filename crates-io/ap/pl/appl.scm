(define-module (crates-io ap pl appl) #:use-module (crates-io))

(define-public crate-appl-0.1.0 (c (n "appl") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rapier2d") (r "^0.11") (d #t) (k 0)))) (h "14halkcqcyvnj7x12fzx3sq0yri0128hc1nmk85xai0005qd80nb")))

(define-public crate-appl-0.1.1 (c (n "appl") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rapier2d") (r "^0.11") (d #t) (k 0)))) (h "1mmlgfqdsvgbry7yz7czqjk908h1m04d8lapdkrh20cvw9mk7hay")))

(define-public crate-appl-0.2.0 (c (n "appl") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rapier2d") (r "^0.11") (d #t) (k 0)))) (h "05d6s510pl2djgxhzlwwgsh9ggsz77nbjmywv2sz3h4adgj0piq5")))

(define-public crate-appl-0.2.1 (c (n "appl") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rapier2d") (r "^0.11") (d #t) (k 0)))) (h "1gabclqgnm8ck1nyj7ygzbf5j6ycx2k29s1ad5q3s6pkcpf4p9rg")))

(define-public crate-appl-0.2.2 (c (n "appl") (v "0.2.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "rapier2d") (r "^0.11") (d #t) (k 0)))) (h "0q5ybbc0c89i475nhm7y14rr8g9sq0w0srvkc9a45lngckyx3nq3")))

