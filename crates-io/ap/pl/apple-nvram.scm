(define-module (crates-io ap pl apple-nvram) #:use-module (crates-io))

(define-public crate-apple-nvram-0.1.0 (c (n "apple-nvram") (v "0.1.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)))) (h "1gv5k1x7yl1m4iwjfv4hh25jcvlzw9ws5v437n0821d3nd5p5anv")))

(define-public crate-apple-nvram-0.2.0 (c (n "apple-nvram") (v "0.2.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "0sczsjjg2qmf8kgmz0n2ihc57fhx1mwildma2g9h4wi1sf6xvfz9")))

(define-public crate-apple-nvram-0.2.1 (c (n "apple-nvram") (v "0.2.1") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "0v6cv4v0m1z46vaj53vb3z9mnkcjc364ql09zyiyzlcqblvhyljx")))

