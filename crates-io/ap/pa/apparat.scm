(define-module (crates-io ap pa apparat) #:use-module (crates-io))

(define-public crate-apparat-0.4.0 (c (n "apparat") (v "0.4.0") (h "0yx9yp03j59jsn8q3nlcp41qhpnbdwg76kmxzczzww5jrpq3py27")))

(define-public crate-apparat-0.5.0 (c (n "apparat") (v "0.5.0") (h "1nd0ck3dcwzak2rk52vqrpzhykmr270v52ydnxrfgjpiyqjqcwc5")))

(define-public crate-apparat-0.5.1 (c (n "apparat") (v "0.5.1") (h "1263vix2shjahskcidjfaq2ilxv08wk7jnjz0zydnlpaxadafdh4")))

(define-public crate-apparat-0.5.2 (c (n "apparat") (v "0.5.2") (h "0xzn5dym531khp1bccd2mr79yxj6y07i1aab6c6la12ngzrx1cg5")))

(define-public crate-apparat-0.5.3 (c (n "apparat") (v "0.5.3") (h "1ywn5l8w6pg4k82svpd9f2rw6z5ksvc23v2nnmkbf65zk8b7kqwi") (f (quote (("unsafe"))))))

(define-public crate-apparat-0.5.4 (c (n "apparat") (v "0.5.4") (h "1ywrjc0k9wsf5xbkw4sg7h7wgg1xjx4i3f2763m0z0s9k07my492") (f (quote (("unsafe"))))))

(define-public crate-apparat-0.5.5 (c (n "apparat") (v "0.5.5") (h "1ql4xi24ihpfy5hz4skwf40vafcb1hfhwxi6lcqd4nlk5v36ckvh") (f (quote (("unsafe"))))))

