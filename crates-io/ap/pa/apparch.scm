(define-module (crates-io ap pa apparch) #:use-module (crates-io))

(define-public crate-apparch-0.1.0 (c (n "apparch") (v "0.1.0") (h "1wmij8p6bzwxiqlhmxqkr56y3lr2m23896jinbx5g8lqk9ssa87z") (y #t)))

(define-public crate-apparch-0.1.1 (c (n "apparch") (v "0.1.1") (h "0bxv9spg8kb0y0xf1if1rddm9knk77gyphff6awlzhv4j62g9hga")))

