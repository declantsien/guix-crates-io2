(define-module (crates-io ap pa apparmor_util) #:use-module (crates-io))

(define-public crate-apparmor_util-0.0.1 (c (n "apparmor_util") (v "0.0.1") (h "03s7pvjjls9djc5jw774br58sqlkb5xmqwgkq0w7s1ca3m8rnlwf") (y #t)))

(define-public crate-apparmor_util-0.1.8 (c (n "apparmor_util") (v "0.1.8") (h "16l6sbfnr4nxkm2562r56gq0gcv2v6xljng91i7zn0v6kwayc7dw")))

