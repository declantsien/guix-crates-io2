(define-module (crates-io ap ng apng) #:use-module (crates-io))

(define-public crate-apng-0.1.0 (c (n "apng") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)))) (h "1bw6v22s3kpgwfk6na9v7gvpc3njnnk3yfzcwf8d08irl90m09mx")))

(define-public crate-apng-0.1.1 (c (n "apng") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)))) (h "1vskxm6397hpb81c5dpazi0cdypfdpmxg8g4zq9wcxb8g048vgwg")))

(define-public crate-apng-0.1.2 (c (n "apng") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)))) (h "0pyn6zx7k7p3fj6yingbp62rnmm7h5yhri87mm6m99vd2f7pv6wk")))

(define-public crate-apng-0.1.3 (c (n "apng") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "png") (r "^0.15.0") (d #t) (k 0)))) (h "0hklprxg04xcljcqxl85qk9y8k83bl1vbjzb8idbbpywlc1kag80")))

(define-public crate-apng-0.2.0 (c (n "apng") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "flate2") (r "^1.0.14") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "png") (r "^0.16.0") (d #t) (k 0)))) (h "1fi1gapam28pxghciglp9lmihgn49v0a8fl4nawp1ld02sv0rfx1")))

(define-public crate-apng-0.3.0 (c (n "apng") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "02xdip2xh9dwcpgvn0kkvm83a1qyri42lrkr0d9dqas21xzijjyr")))

(define-public crate-apng-0.3.1 (c (n "apng") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0dasxv0pfq6flv8d6vpfnjaf2k6qi09yzckly7ipln4znf51ac0q")))

(define-public crate-apng-0.3.2 (c (n "apng") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0gi5g0k9g1ly6j4p7rn9xx603k5dnpgl66qfq9sr8xmpp870lpv4") (f (quote (("png"))))))

(define-public crate-apng-0.3.3 (c (n "apng") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r8j8fa5hz76gjqxgkvbzzzkgvb35s0pdq31f35cmylcj6s8x5jx") (f (quote (("png"))))))

(define-public crate-apng-0.3.4 (c (n "apng") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0k0503q8m3sacq7lvfz1s9jqsj6im8qr9w1vf0gb2pg2qhpi8y8a") (f (quote (("png"))))))

