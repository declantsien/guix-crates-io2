(define-module (crates-io ap if apiflash) #:use-module (crates-io))

(define-public crate-apiflash-0.1.0 (c (n "apiflash") (v "0.1.0") (h "154wwrj7jz3ggy9myqgq2vhl7ba1a0kgjmm3m4l46hb8znvck072")))

(define-public crate-apiflash-0.1.1 (c (n "apiflash") (v "0.1.1") (h "1rzr4r2289hwdh0ic6rp29vir5fq3qy70zija2f3nbagvk6xnlrb")))

(define-public crate-apiflash-0.1.4 (c (n "apiflash") (v "0.1.4") (h "0xfl5pq3wq60y15vx853m8v62nra6xnx0bb222vh5c5vx6my0jqc")))

