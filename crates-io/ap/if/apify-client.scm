(define-module (crates-io ap if apify-client) #:use-module (crates-io))

(define-public crate-apify-client-0.1.0 (c (n "apify-client") (v "0.1.0") (d (list (d (n "query_params") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1g8jh5bwyhdafqbcvg65znzy6ss2bairfirnr3ajr3259mdpjyss")))

(define-public crate-apify-client-0.2.0 (c (n "apify-client") (v "0.2.0") (d (list (d (n "query_params") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "06ncarkai6ll8va48xyijpg3i9pb40q7mp9jv6yrqkkjqqsmsjbh")))

