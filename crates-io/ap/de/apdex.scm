(define-module (crates-io ap de apdex) #:use-module (crates-io))

(define-public crate-apdex-0.1.0 (c (n "apdex") (v "0.1.0") (d (list (d (n "yansi") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1h8ipib75knrxwcxxdrpfaxvaz2m4is6b9xjykzigkbs07r9bifn") (f (quote (("default" "yansi"))))))

(define-public crate-apdex-0.1.1 (c (n "apdex") (v "0.1.1") (d (list (d (n "yansi") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "03mp3h30ap5xixcw88xlmpsn2lqfjji1xgbjhyd1c1za0ric2msz") (f (quote (("default" "yansi"))))))

