(define-module (crates-io ap oo apool) #:use-module (crates-io))

(define-public crate-apool-0.1.0 (c (n "apool") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "1wskk5xhyszndfiqyc7x1x2v2r0y8b85pxr943s47s1rmnicbgd2") (f (quote (("default" "tokio"))))))

(define-public crate-apool-0.1.1 (c (n "apool") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "0x1200vn94fwjg8qipny2xnv34p7ha0iymxkvham4vyh8c20my4i") (f (quote (("default" "tokio"))))))

(define-public crate-apool-0.1.2 (c (n "apool") (v "0.1.2") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "1i9fi0f57m2h8ynra12akdmdfpcij66fks8sq7sr9q6410xxl25d") (f (quote (("default" "tokio"))))))

(define-public crate-apool-0.1.3 (c (n "apool") (v "0.1.3") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "18wbkivzg107qhv43z1f8jdqls0l5gh8x2gpvjcmwlc90051xplc") (f (quote (("default" "tokio"))))))

