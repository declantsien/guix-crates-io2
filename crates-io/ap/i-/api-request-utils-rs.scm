(define-module (crates-io ap i- api-request-utils-rs) #:use-module (crates-io))

(define-public crate-api-request-utils-rs-0.1.0 (c (n "api-request-utils-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05r9cciyi2rvq1172mg62jcgn2xxrd2zlkrmwz5v0ymn83915ahp")))

(define-public crate-api-request-utils-rs-0.1.1 (c (n "api-request-utils-rs") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wqlyfls4xrymhfs9lnl1y1igigr2jnc4k27ckwm73xk8caqkzlf")))

(define-public crate-api-request-utils-rs-0.1.2 (c (n "api-request-utils-rs") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cm5p9rjs1l96w0i7nq57sij8y9ixbq1w87s5sp9x9x0nbhcyzr3") (y #t)))

(define-public crate-api-request-utils-rs-0.1.3 (c (n "api-request-utils-rs") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14lgzmsr9l83ls31dlpx1lr8ajdapf4wi4jn8g3nxfd7f25d7p4m")))

(define-public crate-api-request-utils-rs-0.1.5-preview (c (n "api-request-utils-rs") (v "0.1.5-preview") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nar52lzc8bdvzlp8jdjgz4lfixr21l8zrhj4z39lga7yw61q88w") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview2 (c (n "api-request-utils-rs") (v "0.1.5-preview2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mhxwy1gninlsajc8dpa9xb2zz78z7d3d43wph8phamz6677yb10") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview3 (c (n "api-request-utils-rs") (v "0.1.5-preview3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f0q8pd7jk3sxzdmxmk0333zsr4lm41abvcw5zp1q3masf194kgd") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview4 (c (n "api-request-utils-rs") (v "0.1.5-preview4") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bb985gidxklkdnmsin5349dsrgzggigwjszsg1chyalw2kslcn7") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview5 (c (n "api-request-utils-rs") (v "0.1.5-preview5") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v43mj7l4ammbb9hy1iyi1mn68j9m4xd4kavck54cr2x951sz32g") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview6 (c (n "api-request-utils-rs") (v "0.1.5-preview6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10ibw0f8skgs9vlwqrhc53mpsndxp0gcmwpqsisfrbk2ap38nynz") (y #t)))

(define-public crate-api-request-utils-rs-0.1.5-preview7 (c (n "api-request-utils-rs") (v "0.1.5-preview7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qhw0f5mrmba3k6q72kr0issihfldqq6zq4fr2vmv7n5nz6319sn") (y #t)))

(define-public crate-api-request-utils-rs-0.1.6 (c (n "api-request-utils-rs") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06cfzr510bp3y4sgi2kfpjz37kzfn0j9zb97di2rz0llf1wkklv4") (f (quote (("export"))))))

(define-public crate-api-request-utils-rs-0.1.6-fix1 (c (n "api-request-utils-rs") (v "0.1.6-fix1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03y02dv66dgjl52ccbr1k1kngq5cbi34sbilnzl8syq94s6x82la") (f (quote (("export")))) (y #t)))

(define-public crate-api-request-utils-rs-0.1.7 (c (n "api-request-utils-rs") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05r1klq4nx7d6xgi6ppx1bnzj6paxr98g8l2lmk9945rg1nmyikn") (f (quote (("export"))))))

(define-public crate-api-request-utils-rs-0.1.8 (c (n "api-request-utils-rs") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mc31qsg47kf1pswzfrsz6dqki32f35ja0saas37pvdz9b4ni4fl")))

(define-public crate-api-request-utils-rs-0.1.9 (c (n "api-request-utils-rs") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g0rmnmiwmavr2zf0kpw9ps0jz28a7phyz97wh3xvwkq7npj56a5")))

(define-public crate-api-request-utils-rs-0.2.0 (c (n "api-request-utils-rs") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l29wl17dibwcbqkqzj76h3bm9x6ifryxvkjqb6qvag6p5wz3h67")))

(define-public crate-api-request-utils-rs-0.2.1 (c (n "api-request-utils-rs") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14irdh18qyx8i3bwkg7jidql2lwz0x52f308qpm4lm3sphmamfsa")))

(define-public crate-api-request-utils-rs-0.2.2 (c (n "api-request-utils-rs") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a565brvlisqqqw027ax687gl0iyinh7cm99c27ia0mlm8fsdkz2")))

(define-public crate-api-request-utils-rs-0.2.3 (c (n "api-request-utils-rs") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09fr224bl85f5abbzj8ijj3q6arcfrlmsg8hm2ppn0sii00ap1lx")))

(define-public crate-api-request-utils-rs-0.2.4 (c (n "api-request-utils-rs") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1ls44cam2sgmk1qan6w2vw9yh021dm89zcwfvs8pzb2w669lyx0m")))

(define-public crate-api-request-utils-rs-0.2.5 (c (n "api-request-utils-rs") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1f6lx8a7bq5pzslwkddx41lmarq0q9qyk1mn9r6x7pkgd0rr7pzn")))

