(define-module (crates-io ap i- api-resp) #:use-module (crates-io))

(define-public crate-api-resp-0.1.0 (c (n "api-resp") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dcb51x91w2cqm8rk6cfr7qra8x954r24dcg54vxg44jkch6blcb")))

(define-public crate-api-resp-0.1.1 (c (n "api-resp") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i74wr6xvj1kfy6i9igj7cmw2wvgidxzcgfqvnbs94c98qv7qjjv")))

(define-public crate-api-resp-0.1.2 (c (n "api-resp") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pis34npgqxz5y6z5faqdzxpdmc4r80jifnzv9jmdgna0wbfvq2p")))

(define-public crate-api-resp-0.1.3 (c (n "api-resp") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pf1nqn4ijzk0dc24sny62x9r7p2qs6mx16hsxmlzz6h3qn4z39g")))

