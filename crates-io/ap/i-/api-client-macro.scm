(define-module (crates-io ap i- api-client-macro) #:use-module (crates-io))

(define-public crate-api-client-macro-0.1.0 (c (n "api-client-macro") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1my7l7h7vaiy71zn9cpcn5rjn6gp1l1g0fa7i326bff3nwm6q0ji")))

(define-public crate-api-client-macro-0.1.1 (c (n "api-client-macro") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1qbkg8xyg6vpnzcnns3qdinqh1jkq9858nm9bnqqzbgjrnvjzaqi")))

(define-public crate-api-client-macro-0.1.2 (c (n "api-client-macro") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "1k2acp0yg549rpxn2ml552als8ly4lqryw9yvz6hhmirbdg0nwn7")))

