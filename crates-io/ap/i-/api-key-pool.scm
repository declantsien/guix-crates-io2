(define-module (crates-io ap i- api-key-pool) #:use-module (crates-io))

(define-public crate-api-key-pool-0.0.1 (c (n "api-key-pool") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "145gqgrpz1wycbldnfaih9k1h18d12fnsgad59gqcwvc9jk4g1hs")))

(define-public crate-api-key-pool-0.0.2 (c (n "api-key-pool") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12md6nlaiha7239dvmbqg6g5q57q2r7ga5nb4vddvqjkli67jal9")))

