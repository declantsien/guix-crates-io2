(define-module (crates-io ap i- api-swgoh-gg) #:use-module (crates-io))

(define-public crate-api-swgoh-gg-0.1.0 (c (n "api-swgoh-gg") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00vwciz90fskjggpk8si4y0140msgyq84r7apbfxc999hyjlyf52")))

(define-public crate-api-swgoh-gg-0.2.0 (c (n "api-swgoh-gg") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mf8r255lpqanayq19f5xwabr9s6d1mrrlpg7clnj9z5mzg97ry4")))

(define-public crate-api-swgoh-gg-0.3.0 (c (n "api-swgoh-gg") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwd3i7rn7aldfpn4sxshm6gw5xkai2znhr3dfhw3pw0mgbadd95")))

