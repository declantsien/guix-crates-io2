(define-module (crates-io ap ds apds9960) #:use-module (crates-io))

(define-public crate-apds9960-0.1.0 (c (n "apds9960") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "11dz8dsba0wmj3i7d1069yqykfl96lflsiin55dppdrh5bzs7043")))

