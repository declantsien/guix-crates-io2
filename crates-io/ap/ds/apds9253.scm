(define-module (crates-io ap ds apds9253) #:use-module (crates-io))

(define-public crate-apds9253-1.0.0 (c (n "apds9253") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0yiggq7pi78szkmp9g4dv4ywv2nciagw5xc6p5cmh3j9s44gzwd2")))

