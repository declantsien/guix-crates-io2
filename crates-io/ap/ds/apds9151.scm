(define-module (crates-io ap ds apds9151) #:use-module (crates-io))

(define-public crate-apds9151-0.1.0 (c (n "apds9151") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "12qbx0dk196790b2l046skmysj1cccmzpg9kam9adgpc6pxs75a0")))

(define-public crate-apds9151-0.1.1 (c (n "apds9151") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1c4yl75q8pax6h1jws9ki6z7chcfxy3g5fsl41qxq3vbnqrdxad8")))

