(define-module (crates-io ap pr approxeq) #:use-module (crates-io))

(define-public crate-approxeq-0.1.0 (c (n "approxeq") (v "0.1.0") (h "0ydx12k6jc332008k4aqd296r8383fsypjqialr1fv5p7wb56fgg")))

(define-public crate-approxeq-0.1.1 (c (n "approxeq") (v "0.1.1") (h "1g42rk39z57g1j832r22d9kbm499s58hbvncyxszy8pxik5qb2jr")))

