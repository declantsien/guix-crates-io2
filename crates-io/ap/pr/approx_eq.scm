(define-module (crates-io ap pr approx_eq) #:use-module (crates-io))

(define-public crate-approx_eq-0.1.0 (c (n "approx_eq") (v "0.1.0") (h "0kw0fvzlp4p0mp09hyva35ciy61ahlh3wz4p21fa6xkcxwk7g7w7")))

(define-public crate-approx_eq-0.1.2 (c (n "approx_eq") (v "0.1.2") (h "10lkkl5r2sggmd9zz2l684ssdmf8l5hp50s9kwmkma4328knh0n9")))

(define-public crate-approx_eq-0.1.3 (c (n "approx_eq") (v "0.1.3") (h "1myj7y6f0ivl7jibajs747zj685lz6vx68mflyccyaz0i33j9hns")))

(define-public crate-approx_eq-0.1.4 (c (n "approx_eq") (v "0.1.4") (h "0v4x88hw9v3ndnkrr2lba716997d6jf3si5k3v0qm7w8gazvlv3z")))

(define-public crate-approx_eq-0.1.5 (c (n "approx_eq") (v "0.1.5") (h "0hydrnxd4a8wy31iyycc1rc0vd5m5h4rpiy64k0q3c7injz6nq6m")))

(define-public crate-approx_eq-0.1.6 (c (n "approx_eq") (v "0.1.6") (h "1adjijnqwlv325gabfs6jlln5dinj2hk0yxhw787hj5nw2zrqymj")))

(define-public crate-approx_eq-0.1.7 (c (n "approx_eq") (v "0.1.7") (h "1bqiv84hsy4kvaa8rfarcsxdgdsy04l31ywcnpg4ryfx47r66bb7")))

(define-public crate-approx_eq-0.1.8 (c (n "approx_eq") (v "0.1.8") (h "143mfy1qjlhpjlbgjhkd7n4scfjsjb3mqgif02zkyy3agj1ypydk")))

