(define-module (crates-io ap pr approx-derive) #:use-module (crates-io))

(define-public crate-approx-derive-0.1.0 (c (n "approx-derive") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0hkg41wvf91k9sk8kzsvr8xi0d4db17kxc90qfmmm1lmri9x9bpp")))

(define-public crate-approx-derive-0.1.1 (c (n "approx-derive") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "019c508bgqhpnbi4q0s0n8grilqbm3j8c8svifk08xw9c9zylnzg")))

(define-public crate-approx-derive-0.1.2 (c (n "approx-derive") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0a00qzw755cfnkcgq9s5nqds6ix1y150vzpbbn84dph1g9j6q3w1")))

(define-public crate-approx-derive-0.1.3 (c (n "approx-derive") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1ppzai47h8lglbc01291b8r01nymyq9d3g2k1iijaphnlszz9jlf")))

