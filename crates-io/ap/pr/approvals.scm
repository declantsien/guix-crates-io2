(define-module (crates-io ap pr approvals) #:use-module (crates-io))

(define-public crate-approvals-0.0.1 (c (n "approvals") (v "0.0.1") (h "1fis01vvslcx39svdblzsi8s6kfh13b91f7jjwp4k94y0593pqnr")))

(define-public crate-approvals-0.0.2 (c (n "approvals") (v "0.0.2") (h "037z4ag6xk4bjkxl521p7v005yr36nxr035ngp7jzq37hab8pxrv")))

(define-public crate-approvals-0.0.3 (c (n "approvals") (v "0.0.3") (h "1ywp9zcvlf4zgv64bpd0bym8fplrh0sc2f470783ghpazihrl0ac")))

(define-public crate-approvals-0.0.4 (c (n "approvals") (v "0.0.4") (h "08lq8wab3c1c619j37wpgivnpshasmf8xw2cimq3kyrd0x2d5p02")))

(define-public crate-approvals-0.0.5 (c (n "approvals") (v "0.0.5") (h "13bn1d77kq15xy0jy8akhrf8m9f2j24iid728mh8w6gddvs2asz1")))

(define-public crate-approvals-0.0.6 (c (n "approvals") (v "0.0.6") (d (list (d (n "backtrace") (r "^0.1") (d #t) (k 0)))) (h "055qz3s9vlzwfsdwqkmsz9sp0z6yy62ki005k1grk2177c061pvc")))

(define-public crate-approvals-0.0.7 (c (n "approvals") (v "0.0.7") (d (list (d (n "backtrace") (r "^0.1") (d #t) (k 0)))) (h "1i7pzy00k6f0gbdlm2jdlqwn5wsvcish6f9xxgljkxfkvaachdma")))

