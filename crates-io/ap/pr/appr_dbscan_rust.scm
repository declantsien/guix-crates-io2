(define-module (crates-io ap pr appr_dbscan_rust) #:use-module (crates-io))

(define-public crate-appr_dbscan_rust-0.1.0 (c (n "appr_dbscan_rust") (v "0.1.0") (d (list (d (n "partitions") (r "^0.2.4") (d #t) (k 0)) (d (n "rstar") (r "^0.8.2") (d #t) (k 0)))) (h "1zycf68wz7x8rmblxn5ngs7pl1g36k0qj2smvr8df9wp7mrv3ls7")))

(define-public crate-appr_dbscan_rust-0.1.1 (c (n "appr_dbscan_rust") (v "0.1.1") (d (list (d (n "partitions") (r "^0.2.4") (d #t) (k 0)) (d (n "rstar") (r "^0.8.2") (d #t) (k 0)))) (h "12cvczb1vvc3srn7chirawkgz54g1vqw6lan3058sys4qgbajcyh")))

(define-public crate-appr_dbscan_rust-0.1.2 (c (n "appr_dbscan_rust") (v "0.1.2") (d (list (d (n "partitions") (r "^0.2.4") (d #t) (k 0)) (d (n "rstar") (r "^0.8.2") (d #t) (k 0)))) (h "0dcjkih2fg16mydglsn1rcnq0c92zmq90r7y900zkp02pkrhdjqk")))

