(define-module (crates-io ap pr approx_shannon_entropy) #:use-module (crates-io))

(define-public crate-approx_shannon_entropy-0.1.0 (c (n "approx_shannon_entropy") (v "0.1.0") (d (list (d (n "micromath") (r "^1.1.1") (d #t) (k 0)))) (h "128vzqff5rfap1ld72wifz8sv2amh17cvm3phdgwlyaabv759gww")))

(define-public crate-approx_shannon_entropy-0.1.1 (c (n "approx_shannon_entropy") (v "0.1.1") (d (list (d (n "micromath") (r "^1.1.1") (d #t) (k 0)))) (h "0cgidlaa7jwx0ghjd7z3ibq14gily5843qbnrl33lcj87ygc061r")))

