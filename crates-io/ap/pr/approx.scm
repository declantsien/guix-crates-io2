(define-module (crates-io ap pr approx) #:use-module (crates-io))

(define-public crate-approx-0.0.1 (c (n "approx") (v "0.0.1") (h "01k859a8divxzhm2i6mlhixsy2qkhw3snk6gpskzhsp98hhlbf1x")))

(define-public crate-approx-0.1.0 (c (n "approx") (v "0.1.0") (h "1vm2my95bmvcqns9cilm7hqxi1z8qg89f4bgn7bwcdx51mv6sjji")))

(define-public crate-approx-0.1.1 (c (n "approx") (v "0.1.1") (h "153awzwywmb61xg857b80l63b1x6hifx2pha7lxf6fck9qxwraq8") (f (quote (("no_std"))))))

(define-public crate-approx-0.2.0 (c (n "approx") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1539pj6sh4chiicg4slsipwl85ad5727iip8fjvwxwrvzwval5sq") (f (quote (("use_complex" "num-complex") ("std") ("default" "std"))))))

(define-public crate-approx-0.2.1 (c (n "approx") (v "0.2.1") (d (list (d (n "num-complex") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "14rdksmdq5fg4m9c746r54fwgyp1p72m086n6cjac0blphf9cd71") (f (quote (("use_complex" "num-complex") ("std") ("default" "std"))))))

(define-public crate-approx-0.3.0 (c (n "approx") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1d37vgv3qhi23ncrb1a4zhidmlvc83inbkxqsym68sllqjsi07zp") (f (quote (("std") ("default" "std"))))))

(define-public crate-approx-0.3.1 (c (n "approx") (v "0.3.1") (d (list (d (n "num-complex") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1vjs93i7vw6vj3mk0ypacld727psczmg9g5vmr3kcx80bcdgymrw") (f (quote (("std") ("default" "std"))))))

(define-public crate-approx-0.3.2 (c (n "approx") (v "0.3.2") (d (list (d (n "num-complex") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1hx580xjdxl3766js9b49rnbnmr8gw8c060809l43k9f0xshprph") (f (quote (("std") ("default" "std"))))))

(define-public crate-approx-0.4.0 (c (n "approx") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "0y52dg58lapl4pp1kqlznfw1blbki0nx6b0aw8kja2yi3gyhaaiz") (f (quote (("std") ("default" "std"))))))

(define-public crate-approx-0.5.0 (c (n "approx") (v "0.5.0") (d (list (d (n "num-complex") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "17p9g3yl061zdlrqj10npybmng81jz71dzmcanmjgcb35qhgfb87") (f (quote (("std") ("default" "std"))))))

(define-public crate-approx-0.5.1 (c (n "approx") (v "0.5.1") (d (list (d (n "num-complex") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1ilpv3dgd58rasslss0labarq7jawxmivk17wsh8wmkdm3q15cfa") (f (quote (("std") ("default" "std"))))))

