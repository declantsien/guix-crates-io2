(define-module (crates-io ap ic apicall) #:use-module (crates-io))

(define-public crate-apicall-0.1.0 (c (n "apicall") (v "0.1.0") (h "1wqhhs1qbx6w1jacb4mwb6sswr0s91h87plqh5r46rv1ia6ffpqn") (y #t)))

(define-public crate-apicall-0.1.1 (c (n "apicall") (v "0.1.1") (h "04idqyg4k8hvdg9m65c1jdir5g7h8fprsdmq38jjy3vzd6jfm2wp") (y #t)))

(define-public crate-apicall-0.2.0 (c (n "apicall") (v "0.2.0") (h "0yid0fs0agbj29fxd8jjpylcwi4hhqcxgcbg9gvqzxl6zfwsdza4")))

