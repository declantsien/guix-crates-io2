(define-module (crates-io ap pd appdirs) #:use-module (crates-io))

(define-public crate-appdirs-0.1.0 (c (n "appdirs") (v "0.1.0") (h "1nwz6l75gk6mkw0iv7k8hgiqdy0api1mhy0x4l4g4a7zm6v8p2na")))

(define-public crate-appdirs-0.2.0 (c (n "appdirs") (v "0.2.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0y6ky4brl6qhjrzsh0raxlxkvad8ifjlc567vii7blln27kb64nm")))

