(define-module (crates-io ap e- ape-table-trig-macros) #:use-module (crates-io))

(define-public crate-ape-table-trig-macros-0.1.0 (c (n "ape-table-trig-macros") (v "0.1.0") (h "1cfm4zv0kbkkhzcisyg29mmq2f3rj9rfcmfv3w6vxvss4il9lg9k")))

(define-public crate-ape-table-trig-macros-0.2.0 (c (n "ape-table-trig-macros") (v "0.2.0") (h "0ggw8kqrxh40bfs17xiwrwv52z67ax0cpf62lhywb244914qx1q4")))

