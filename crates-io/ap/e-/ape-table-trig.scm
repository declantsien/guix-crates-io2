(define-module (crates-io ap e- ape-table-trig) #:use-module (crates-io))

(define-public crate-ape-table-trig-0.1.0 (c (n "ape-table-trig") (v "0.1.0") (d (list (d (n "ape-table-trig-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "1cb4fhyqvz3n6r26bviin5d6ymqxwamx4sr56ycypm4h16n6hp66")))

(define-public crate-ape-table-trig-0.2.0 (c (n "ape-table-trig") (v "0.2.0") (d (list (d (n "ape-table-trig-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "0hb7b89h54yh1kzknjbqbc5dx5999v6npqscblqazy6xffw5qrzn")))

