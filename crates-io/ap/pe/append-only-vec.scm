(define-module (crates-io ap pe append-only-vec) #:use-module (crates-io))

(define-public crate-append-only-vec-0.1.0 (c (n "append-only-vec") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)))) (h "1r2cn5gil7a4n643b0fja02kyh82n7xkwb4r4alyw7hr25crfp76")))

(define-public crate-append-only-vec-0.1.1 (c (n "append-only-vec") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)))) (h "1h9idnr0jmgq218jw0s3rknhi75gq66mk5mr8lqghrfav83ysa5y")))

(define-public crate-append-only-vec-0.1.2 (c (n "append-only-vec") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)))) (h "1vjzbp1rncny41g9hda740yzwmgb3dmjzf3v9kgr3203jiypc22n")))

(define-public crate-append-only-vec-0.1.3 (c (n "append-only-vec") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)))) (h "0al137anbgg38n404vb3cs4wnrp9dms7j0jx2vc9shfg9s3qzjzk")))

