(define-module (crates-io ap pe append_db_postgres_derive) #:use-module (crates-io))

(define-public crate-append_db_postgres_derive-0.2.0 (c (n "append_db_postgres_derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ah5vfs4345qv6qsf98nhb6svwh4h1hxs48cyyfj28irnz35y6np")))

