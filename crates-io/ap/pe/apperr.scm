(define-module (crates-io ap pe apperr) #:use-module (crates-io))

(define-public crate-apperr-0.1.0 (c (n "apperr") (v "0.1.0") (h "0fkrm6gj11d40a7m1wv4pq2bn7a8bhg34rdksv89pd8mizkm3rc8") (r "1.0")))

(define-public crate-apperr-0.2.0 (c (n "apperr") (v "0.2.0") (h "1hblkz8wc5qfxgsp0y3zk80l62010vcijdc9vla9p0bs1b7r96gx") (r "1.0")))

