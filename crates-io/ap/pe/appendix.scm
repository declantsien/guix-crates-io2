(define-module (crates-io ap pe appendix) #:use-module (crates-io))

(define-public crate-appendix-0.1.0 (c (n "appendix") (v "0.1.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "103lgda0369niq07xmrddvaszi3514kc9qlvbnsnr988lfn0z34n")))

(define-public crate-appendix-2.0.0 (c (n "appendix") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "06k44ng1yzzfwa42a6dy63w8f17i34afpkzaic1yfr8ip5s9vci2") (y #t)))

(define-public crate-appendix-0.1.1 (c (n "appendix") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "17bpnqxci54h2c184n20307y57jvh0gh7kq9hdswmq3xnncj5fms")))

(define-public crate-appendix-0.1.2 (c (n "appendix") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0hb15pf6fv9ag318ala0rffs6kxpp6pxyc2xkm33a9kiggxjag0m")))

(define-public crate-appendix-0.1.3 (c (n "appendix") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1ryzd6v4xpvb1w79rgg77zjk2c33cqazw1gpi0w6igjvrm1b9m36")))

(define-public crate-appendix-0.2.0 (c (n "appendix") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "14dx86cak8a8avgfb5vinyhmnimwl4ziggblbiqx3d8i26c5yiak")))

(define-public crate-appendix-0.2.1 (c (n "appendix") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "11ih94ws4qvjd0i267zr7pdr3mpkc5nl839knn5fcga4d66fbymd")))

(define-public crate-appendix-0.2.2 (c (n "appendix") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1a482yly871b9gpp9xdd7sh6hgkkdda58d2kmci0r2qh1f7mfgbg")))

