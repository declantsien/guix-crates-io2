(define-module (crates-io ap pe append_db) #:use-module (crates-io))

(define-public crate-append_db-0.2.0 (c (n "append_db") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "089r70ds7ny5ahzkl6h88whdwizf40m37bsr69scy42dym3ihksj")))

(define-public crate-append_db-0.3.1 (c (n "append_db") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "stm") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ji0j2l2zq5nzsbl28h6carnyml8rrgi2zhi29n2gyh8az6gc6p6")))

(define-public crate-append_db-0.3.2 (c (n "append_db") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "stm") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dw9jsnjsd9wv055qrq9xcgwm49bhvznwcqillv2rs2grg6kpj69")))

