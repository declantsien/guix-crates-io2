(define-module (crates-io ap pe append-only-bytes) #:use-module (crates-io))

(define-public crate-append-only-bytes-0.1.0 (c (n "append-only-bytes") (v "0.1.0") (h "0j2s630jm5zcfadv55465wjki1i63igdx2l8kbqw3mjcpnkwwd01")))

(define-public crate-append-only-bytes-0.1.1 (c (n "append-only-bytes") (v "0.1.1") (h "0npmsbch9lgfp6hvzv767r2vj9y49y82pcwk5wl649qjhk2b4ahm")))

(define-public crate-append-only-bytes-0.1.2 (c (n "append-only-bytes") (v "0.1.2") (h "100991v0jrp6jrs6343w1p0wdf5413jizfyjq6icn7irbhdnsgf3") (f (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1.3 (c (n "append-only-bytes") (v "0.1.3") (h "1gynh5jqvyq2w7mylyx07lzvjp6fv57gvxmwq4ka3r3l7mh9p1na") (f (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1.4 (c (n "append-only-bytes") (v "0.1.4") (h "0yl3r132wbw6m1vywaym5hjk0c303i8bmj1fdd4qvnkdr1av4nzn") (f (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1.5 (c (n "append-only-bytes") (v "0.1.5") (h "0hhkyq3rp63pd1b5igfvrknspcrhsg39d6w8qdsb576lp91b9n25") (f (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1.6 (c (n "append-only-bytes") (v "0.1.6") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "1h402aw78fjipl8ii9hnbly3ki19zng8w3si8125b0cyacmz724h") (f (quote (("u32_range") ("default" "serde"))))))

(define-public crate-append-only-bytes-0.1.7 (c (n "append-only-bytes") (v "0.1.7") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "0zbs0s15j7m07r268sa1vja1zksvrmqppyyf71823a422ndjbdr5") (f (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1.8 (c (n "append-only-bytes") (v "0.1.8") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "1nc4wlksjk5fkzb0mbj9dzvr6b9q14saq2fk5ssgyli8l5bncwyx") (f (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1.9 (c (n "append-only-bytes") (v "0.1.9") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "14lppd9xnapvgm2pqy3csrls6ild6sxdva40kpvqg4kcf3m79162") (f (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1.10 (c (n "append-only-bytes") (v "0.1.10") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "1bw14iydmj197pvg6xhmyvgn52m7mcxa3cdkqf07yjjk0mj88pky") (f (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1.11 (c (n "append-only-bytes") (v "0.1.11") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "103pc6cmrkxx3dj492i1v7q9vb1r7vdvjs0bf7fj312p2jaqd3rw") (f (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1.12 (c (n "append-only-bytes") (v "0.1.12") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "1r9ypzsl0b8fs9nka6rmghwb7rzz57l97dbz1m56gpmxsq0nchxc") (f (quote (("u32_range") ("default"))))))

