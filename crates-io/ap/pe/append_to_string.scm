(define-module (crates-io ap pe append_to_string) #:use-module (crates-io))

(define-public crate-append_to_string-0.1.0 (c (n "append_to_string") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1rjsivqhsbp0fals0j4n2yr7850xd52nb8fd7hw2xkk0gbs1ik8y")))

