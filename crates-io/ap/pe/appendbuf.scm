(define-module (crates-io ap pe appendbuf) #:use-module (crates-io))

(define-public crate-appendbuf-0.0.1 (c (n "appendbuf") (v "0.0.1") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "1frv9jvbphvk4sc21k5jpapnyki61baca4azh7qhg9hjk4g4vr38")))

(define-public crate-appendbuf-0.0.2 (c (n "appendbuf") (v "0.0.2") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "10ljvd3dxr98nndl5j049hp86b300fm25a1mkx5iq6ii32y2m1va")))

(define-public crate-appendbuf-0.0.3 (c (n "appendbuf") (v "0.0.3") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "1iyhvzc7lyghvm7m8ygb46af34lqm41d1qyadk1q7h0bhlrry3f9")))

(define-public crate-appendbuf-0.1.0 (c (n "appendbuf") (v "0.1.0") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "1yad1h11cbb4cjp0xbdcxpxgnkiyh5za5wlpwm7pvfgn6rfw1k08")))

(define-public crate-appendbuf-0.1.1 (c (n "appendbuf") (v "0.1.1") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "06mxa31v0qkdgbfllm8c83hp4y5j0mca15phvv7v6c5cfc1q5il1")))

(define-public crate-appendbuf-0.1.2 (c (n "appendbuf") (v "0.1.2") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "04dfkx7jnvnazxjxvm9kfcbfp14hqsxnfs0vzzg7y8xcyq2sly3d")))

(define-public crate-appendbuf-0.1.3 (c (n "appendbuf") (v "0.1.3") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "0fb3f81v8vs8r4fd1b48idqm0vb9biam5wd9bd1ckjgl1mzln9c6")))

(define-public crate-appendbuf-0.1.4 (c (n "appendbuf") (v "0.1.4") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "14i0b5janmqqj8inbix9z8qdn564giwmxbh587j630pgg31bq9ws")))

(define-public crate-appendbuf-0.1.5 (c (n "appendbuf") (v "0.1.5") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "061b10zw2dlvaml1b102ci11qb6pdr3dfn1sfyarip4xb3bw2fsx")))

(define-public crate-appendbuf-0.1.6 (c (n "appendbuf") (v "0.1.6") (d (list (d (n "memalloc") (r "^0") (d #t) (k 0)))) (h "1wm5nvzh9s1yq3dql6vqiw5frj462dankx68y15mfk4rp0n54yvq")))

