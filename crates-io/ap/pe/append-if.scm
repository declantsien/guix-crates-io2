(define-module (crates-io ap pe append-if) #:use-module (crates-io))

(define-public crate-append-if-0.1.0 (c (n "append-if") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0i3ls71nsy9r5iv4v2ffq69kdzbx3dfrxjbjbwhbawmyl7slndcl")))

