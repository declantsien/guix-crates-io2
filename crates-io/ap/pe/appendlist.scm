(define-module (crates-io ap pe appendlist) #:use-module (crates-io))

(define-public crate-appendlist-0.1.0 (c (n "appendlist") (v "0.1.0") (h "1iwsjmddmw6bzx10i1ajbvxiynp7i0zag1q3fa92730chna9gcic")))

(define-public crate-appendlist-1.0.0 (c (n "appendlist") (v "1.0.0") (h "0l5wfrmwk390369pcpg01ky2m499x3992c0qis75qapnbq8yvvkn")))

(define-public crate-appendlist-1.1.0 (c (n "appendlist") (v "1.1.0") (h "0ca60jhimvdpmj5mqv2yx46s88vw1niig3h2998sxwykjqh29pck")))

(define-public crate-appendlist-1.2.1 (c (n "appendlist") (v "1.2.1") (h "0dh5qwjddswrl4r1cinfg08yff4v98ph8bsmyb08d49cqsjzz68q")))

(define-public crate-appendlist-1.3.0 (c (n "appendlist") (v "1.3.0") (h "0qd0akaiwkvmpzb4yiafwmsdncqm79mdwl21zgab8mr9wpc094ha")))

(define-public crate-appendlist-1.4.0 (c (n "appendlist") (v "1.4.0") (h "1lnbl7mc7capcqj1z1ylxvm4h492sb9sr8pzww3q6lrhrmrxqjg1")))

