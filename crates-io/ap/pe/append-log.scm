(define-module (crates-io ap pe append-log) #:use-module (crates-io))

(define-public crate-append-log-0.1.0-dev (c (n "append-log") (v "0.1.0-dev") (d (list (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0ysscccssgxlhb15pm8yk1hm68nk1zmd8brznan9q7h93psdl6mi")))

(define-public crate-append-log-0.1.1-dev (c (n "append-log") (v "0.1.1-dev") (d (list (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1wx7n4kcfhf3smcmylsav5l9af2q7lkwcg83bqr7shvf8p2fjhq4")))

(define-public crate-append-log-0.1.2-dev (c (n "append-log") (v "0.1.2-dev") (d (list (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1w44mqb752i81jrr1wcib0h55hwbzvpx2488dpm3g1aia13ghpxj")))

(define-public crate-append-log-0.1.3-dev (c (n "append-log") (v "0.1.3-dev") (d (list (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1ppgkqyyj42sfdhjbcsm62b2qhqzvpqjnf9wj4v4yxm8pl7766cs")))

(define-public crate-append-log-0.1.4-dev (c (n "append-log") (v "0.1.4-dev") (d (list (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1qpg1jw88i97ib3lfpbr5br6ywm11bxz9l20j9wa9c2nxan5lja4")))

