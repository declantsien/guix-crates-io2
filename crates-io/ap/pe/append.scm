(define-module (crates-io ap pe append) #:use-module (crates-io))

(define-public crate-append-0.1.0 (c (n "append") (v "0.1.0") (h "05lngyax8q62r0qnv5080pzvh7dj34jl46v8spgjld1d2prdqrzr")))

(define-public crate-append-0.2.0 (c (n "append") (v "0.2.0") (h "0njqklx2v4xxara1r11bhrn8bfhm8hh68rb2i84mw3k5w7670h9z")))

