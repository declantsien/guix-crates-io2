(define-module (crates-io ap is apisdk-macros) #:use-module (crates-io))

(define-public crate-apisdk-macros-0.0.1 (c (n "apisdk-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "02znaiyfzwvgprs9fdn2j0kizq7pj29ylyfns82qampj5fwr874v")))

(define-public crate-apisdk-macros-0.0.2 (c (n "apisdk-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09w3193a6yqiz04xvvgnlph3n4pnfifsa36dympwjrfnlspz0vbz")))

(define-public crate-apisdk-macros-0.0.3 (c (n "apisdk-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1znn8j5glanx8icrir92hs5j2cf53ykch5sx6bxncq9c4xhacx1z")))

(define-public crate-apisdk-macros-0.0.4-beta.1 (c (n "apisdk-macros") (v "0.0.4-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ylwa6m87ryvh68k13xk05ph1lsp50arpd9nf38rcp0igqn0yiix")))

(define-public crate-apisdk-macros-0.0.4-beta.2 (c (n "apisdk-macros") (v "0.0.4-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "08d91n8w8wzj8y3sncyn78msi2jkq5av4rim3rrmkhjj61y5qpkv")))

(define-public crate-apisdk-macros-0.0.4-beta.3 (c (n "apisdk-macros") (v "0.0.4-beta.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "15fl0f28zm1j9nvi3pzzl8jy2np5am3vy94i2l0ig2b2n1nyglx6")))

(define-public crate-apisdk-macros-0.0.4-beta.4 (c (n "apisdk-macros") (v "0.0.4-beta.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1g527670rpaqrh819b6x9krmh3gllph00fdin4hwkslyn6phiqqp")))

(define-public crate-apisdk-macros-0.0.4-beta.5 (c (n "apisdk-macros") (v "0.0.4-beta.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0s9axspiyakihrsd9rqzq4b24imx00nh9ci9awv9zv0k3ijjcl61")))

(define-public crate-apisdk-macros-0.0.4-beta.6 (c (n "apisdk-macros") (v "0.0.4-beta.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cnr8cdlqzzxy4yazdmvi8jwqvc01cfqawxlmx9n636m07c15lvk")))

(define-public crate-apisdk-macros-0.0.4-beta.7 (c (n "apisdk-macros") (v "0.0.4-beta.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0k2xbr698y4n5sk2ydrkl85wi4cd8hy5fsjhbdann6wyz61jjhiq")))

(define-public crate-apisdk-macros-0.0.4-beta.8 (c (n "apisdk-macros") (v "0.0.4-beta.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0qf9bscp8ja88xcxjadaxq9xn5vvrbmmsypg6z2afrfwgy2rw56c")))

(define-public crate-apisdk-macros-0.0.4-rc.1 (c (n "apisdk-macros") (v "0.0.4-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1fn41wn8h5j6xyzmf82vslanbyqcsvpq9vwsnk8lbnk70fc0zivg")))

(define-public crate-apisdk-macros-0.0.4-rc.2 (c (n "apisdk-macros") (v "0.0.4-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cq5prlv2rbyangl9mq9r6kpr1pa27pr3x0ah52z0h2b0s48lb44")))

(define-public crate-apisdk-macros-0.0.4 (c (n "apisdk-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0d2wgkmmky4lmpkk64aq12lkd7ip07gdrpj2nl797j5nj9ndwmxc")))

(define-public crate-apisdk-macros-0.0.5 (c (n "apisdk-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "130i7pn6370m9wv66r5qcys3zbmjnw5f3famhvqh364gqg8kq652")))

(define-public crate-apisdk-macros-0.0.6-beta.1 (c (n "apisdk-macros") (v "0.0.6-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hbk83irdmgp1ggmr0f7a2k7ggc4qx6k6gd2nh4jd3jfdak855kj")))

(define-public crate-apisdk-macros-0.0.6-beta.2 (c (n "apisdk-macros") (v "0.0.6-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1l0h67iinhg5bcgrchijjai4a61gvkk0qh1zmfkwg397di729jcr")))

(define-public crate-apisdk-macros-0.0.6-beta.3 (c (n "apisdk-macros") (v "0.0.6-beta.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1bn2zhylg7qiad2iiyl9hvv6b7bqsfs73p0rw8fk56q4jkawr88g")))

(define-public crate-apisdk-macros-0.0.6-beta.4 (c (n "apisdk-macros") (v "0.0.6-beta.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cv0lh5lk6ndf7vahmnbad7rm867wqp798f8fk21xnjri6ycw291")))

(define-public crate-apisdk-macros-0.0.6-beta.5 (c (n "apisdk-macros") (v "0.0.6-beta.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "00spz9s3fswri6pgbcib8aibbg86cnhzckq7inlr3lqqq5njqv7p")))

(define-public crate-apisdk-macros-0.0.6-rc.1 (c (n "apisdk-macros") (v "0.0.6-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "03ra64zjbr9v4cgn16f7jfpss1jw0wwsn3l7kqprmqdlmv0yyi4p")))

(define-public crate-apisdk-macros-0.0.6-rc.2 (c (n "apisdk-macros") (v "0.0.6-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1i0728mqcga4zdvdppghm0bj1r5250f1yhgmzvn4i59d0dcaf1y5")))

(define-public crate-apisdk-macros-0.0.6-rc.3 (c (n "apisdk-macros") (v "0.0.6-rc.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1c28nmcylv1syv1sc4lhbn40sdmfg42f9m4la4f6nxvx09j96dv7")))

(define-public crate-apisdk-macros-0.0.6-rc.4 (c (n "apisdk-macros") (v "0.0.6-rc.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "070j9s2v7057jf6lvr3hvlazani3a4j89hjnchyyd8gcd2f1fvm2")))

(define-public crate-apisdk-macros-0.0.6-rc.5 (c (n "apisdk-macros") (v "0.0.6-rc.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "00w8d6bmx4b59vjy998wqkj8v8dxhzhn9n820qkd56vgd95w0wmb")))

(define-public crate-apisdk-macros-0.0.6-rc.6 (c (n "apisdk-macros") (v "0.0.6-rc.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "035wj5305x7kiiw8g9dk9qcbrna1jr0vzz3jk960s405ach8kvk3")))

(define-public crate-apisdk-macros-0.0.6 (c (n "apisdk-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1qp4smkc3ddbjqsqzd4ymddx81gxi09d0j72qa4q0v14fkw47jkw")))

(define-public crate-apisdk-macros-0.0.7 (c (n "apisdk-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jy846hn49iwi51lw617fqzry01wdnwgbcqxml6cw8sncr70rdwc")))

(define-public crate-apisdk-macros-0.0.8-rc.1 (c (n "apisdk-macros") (v "0.0.8-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0zkiwdd4l9h6c3ig78ws6vkiicjrf74g1g7jz4zlf3gv8wlxig65")))

(define-public crate-apisdk-macros-0.0.8 (c (n "apisdk-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1s8l57rqsn846091wqwf20vv9cz06i4mjli5v306zw5lpzwkafdj")))

(define-public crate-apisdk-macros-0.0.9 (c (n "apisdk-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hnm8gkpzwkln0hyq6l7v6ijld59i2p8rsck6zdicsmvnj6sb2yi")))

(define-public crate-apisdk-macros-0.0.10 (c (n "apisdk-macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1jdj5836lf0vs5v9vc28iaydmp7kj6qy9fm03rl3ipaqmwdlxkws")))

(define-public crate-apisdk-macros-0.0.11 (c (n "apisdk-macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cxmddwxcqj8d293xijlgxxxg5fiy0xgfijgp4i844kccr6j5fqv")))

(define-public crate-apisdk-macros-0.0.12 (c (n "apisdk-macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1amal6gmc9xbx2h1m1qx9xx8nacc6zv2pagimnzqm91vqibr0v1d")))

(define-public crate-apisdk-macros-0.0.13 (c (n "apisdk-macros") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cxmyibd7v6cxqhsnqi1hj332ala50hxrg3fvj48980zmx6z6bki")))

(define-public crate-apisdk-macros-0.0.14-beta.1 (c (n "apisdk-macros") (v "0.0.14-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0v08i0wn5lw2m7rdalgj7js5h5s9rxas1mgkb6k7fxj08sa54liz")))

