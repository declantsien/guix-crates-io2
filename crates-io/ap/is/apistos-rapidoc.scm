(define-module (crates-io ap is apistos-rapidoc) #:use-module (crates-io))

(define-public crate-apistos-rapidoc-0.2.0 (c (n "apistos-rapidoc") (v "0.2.0") (d (list (d (n "apistos-plugins") (r "^0.2.0") (d #t) (k 0)))) (h "0am1r8vpvn1br296npw0abdsjpn712pqj2bq4mfbxrpkl53rrv39") (r "1.71")))

(define-public crate-apistos-rapidoc-0.2.1 (c (n "apistos-rapidoc") (v "0.2.1") (d (list (d (n "apistos-plugins") (r "^0.2.1") (d #t) (k 0)))) (h "0m2arhd7ckgbip2by4041xhvh6zd1vj2z6m7j8sl7i357q35jb0v") (r "1.71")))

(define-public crate-apistos-rapidoc-0.2.2 (c (n "apistos-rapidoc") (v "0.2.2") (d (list (d (n "apistos-plugins") (r "^0.2.2") (d #t) (k 0)))) (h "172zlazx5wlzd6qp551hq1z2q6w0l2ppbyprylxc87pyrc0sg31f") (r "1.71")))

(define-public crate-apistos-rapidoc-0.2.3 (c (n "apistos-rapidoc") (v "0.2.3") (d (list (d (n "apistos-plugins") (r "^0.2.3") (d #t) (k 0)))) (h "1hg7yy435p4qwly3vz0vc135yw4s0l9l2q5bpa6kh6ry1ga6h5kz") (r "1.75")))

(define-public crate-apistos-rapidoc-0.2.4 (c (n "apistos-rapidoc") (v "0.2.4") (d (list (d (n "apistos-plugins") (r "^0.2.4") (d #t) (k 0)))) (h "1y3vbzj4vnp1dip8zyq7hs5k4zj1418d2i24zzbcr947q3s1vqwg") (r "1.75")))

