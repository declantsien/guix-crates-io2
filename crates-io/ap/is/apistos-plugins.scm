(define-module (crates-io ap is apistos-plugins) #:use-module (crates-io))

(define-public crate-apistos-plugins-0.2.0 (c (n "apistos-plugins") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "0wm9s84imr50nby17f90vi8b4gjdvabw90lwlrlbx7kgqlppf3gx") (r "1.71")))

(define-public crate-apistos-plugins-0.2.1 (c (n "apistos-plugins") (v "0.2.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "00pjn0vvazfj0m8mr68ywsjmrz0js5zxcldfzkcg5pcwpdpx2h8f") (r "1.71")))

(define-public crate-apistos-plugins-0.2.2 (c (n "apistos-plugins") (v "0.2.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "1jnwndyb3mkjzqhd1l6s15f91jbjc0hc9gc35pnh6003jjj4qw25") (r "1.71")))

(define-public crate-apistos-plugins-0.2.3 (c (n "apistos-plugins") (v "0.2.3") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "1rnj00a5hiwn6aswycirnqgrzag9azkrmhf9cgl30n7gzd1ircs6") (r "1.75")))

(define-public crate-apistos-plugins-0.2.4 (c (n "apistos-plugins") (v "0.2.4") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)))) (h "0hxfy1l4piwwngrhjg4v1ca1syx4j9ky27b34mj87zymmp67g7bh") (r "1.75")))

