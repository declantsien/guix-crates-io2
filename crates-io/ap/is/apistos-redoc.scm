(define-module (crates-io ap is apistos-redoc) #:use-module (crates-io))

(define-public crate-apistos-redoc-0.2.0 (c (n "apistos-redoc") (v "0.2.0") (d (list (d (n "apistos-plugins") (r "^0.2.0") (d #t) (k 0)))) (h "1w7sa673pxwj3mbjijq8s9zkniddy6jmvx9zvkf7qlxabyz0s03r") (r "1.71")))

(define-public crate-apistos-redoc-0.2.1 (c (n "apistos-redoc") (v "0.2.1") (d (list (d (n "apistos-plugins") (r "^0.2.1") (d #t) (k 0)))) (h "0hv3qji4kqanxa093s297z3mivw3q1j9bssyycv2scmwxj82dasp") (r "1.71")))

(define-public crate-apistos-redoc-0.2.2 (c (n "apistos-redoc") (v "0.2.2") (d (list (d (n "apistos-plugins") (r "^0.2.2") (d #t) (k 0)))) (h "0jjvkcjibhjy7ri0zd1iyls5k8mcppw0g39802dlvicqlric7g63") (r "1.71")))

(define-public crate-apistos-redoc-0.2.3 (c (n "apistos-redoc") (v "0.2.3") (d (list (d (n "apistos-plugins") (r "^0.2.3") (d #t) (k 0)))) (h "0h1hzwv2x20mb1xskm2ypbfa5l5pgamfbaag6k1r93aymlkghx3m") (r "1.75")))

(define-public crate-apistos-redoc-0.2.4 (c (n "apistos-redoc") (v "0.2.4") (d (list (d (n "apistos-plugins") (r "^0.2.4") (d #t) (k 0)))) (h "0gny1fyc2p2r7s8c5al23lsvmw0d29g3iyszacyyz4ycqymd7zzc") (r "1.75")))

