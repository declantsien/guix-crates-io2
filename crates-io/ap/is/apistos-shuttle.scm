(define-module (crates-io ap is apistos-shuttle) #:use-module (crates-io))

(define-public crate-apistos-shuttle-0.2.3 (c (n "apistos-shuttle") (v "0.2.3") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "apistos") (r "^0.2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44") (k 0)))) (h "0pi6g6nmfw8jbrvb9gvf2vpwq61wq45cr7j7ca3zsh86n3vzwfqh") (r "1.75")))

(define-public crate-apistos-shuttle-0.2.4 (c (n "apistos-shuttle") (v "0.2.4") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "apistos") (r "^0.2.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44") (k 0)))) (h "0nmw8c36w2gpdzyrn27g74m6dx6hzpgwvk05c6mmx2lrlmbivngk") (r "1.75")))

