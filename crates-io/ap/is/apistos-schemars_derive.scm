(define-module (crates-io ap is apistos-schemars_derive) #:use-module (crates-io))

(define-public crate-apistos-schemars_derive-0.8.16 (c (n "apistos-schemars_derive") (v "0.8.16") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "104z13mlwjcgsgxsj13bqkhyir7lqycpvl90pzcjv6b9z3hby3hd") (r "1.60")))

(define-public crate-apistos-schemars_derive-0.8.17 (c (n "apistos-schemars_derive") (v "0.8.17") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r1zd41p9l2wwbzqmsx1kvx099rwljabjj9fj8fly6dz7frdlz0q") (r "1.60")))

(define-public crate-apistos-schemars_derive-0.8.18 (c (n "apistos-schemars_derive") (v "0.8.18") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hlhs6n4c79lh3aj10783s5f2z7zga16ywwayz8hb6khaz14f66n") (r "1.60")))

(define-public crate-apistos-schemars_derive-0.8.19 (c (n "apistos-schemars_derive") (v "0.8.19") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1irz9gb06adhh2194702m8bhqsg9az9dxdasshh0cvy6ld0sbn3h") (r "1.60")))

(define-public crate-apistos-schemars_derive-0.8.20 (c (n "apistos-schemars_derive") (v "0.8.20") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09d5m58arywqmjpb74q59fqnl883dclcxfbs3xawym0x8ab4ah71") (r "1.60")))

(define-public crate-apistos-schemars_derive-0.8.21 (c (n "apistos-schemars_derive") (v "0.8.21") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pqgrm3ywqphdksmgvf07yh34a5k3k823sjdiyjgjbij64rw0a36") (r "1.60")))

