(define-module (crates-io ap is apistos-swagger-ui) #:use-module (crates-io))

(define-public crate-apistos-swagger-ui-0.2.0 (c (n "apistos-swagger-ui") (v "0.2.0") (d (list (d (n "apistos-plugins") (r "^0.2.0") (d #t) (k 0)))) (h "0hqa7mv0r3ymbyhb5zrp569n9n9g8i2p31pp86wh3nfcs63jdhap") (r "1.71")))

(define-public crate-apistos-swagger-ui-0.2.1 (c (n "apistos-swagger-ui") (v "0.2.1") (d (list (d (n "apistos-plugins") (r "^0.2.1") (d #t) (k 0)))) (h "1ipb3mpjsxhr8j1xp1bbsjajlvbv2spr8g7scnqld8mf17prnp92") (r "1.71")))

(define-public crate-apistos-swagger-ui-0.2.2 (c (n "apistos-swagger-ui") (v "0.2.2") (d (list (d (n "apistos-plugins") (r "^0.2.2") (d #t) (k 0)))) (h "1c163frid5vaa22c8sv685mvaz9qv7mr7fxig6h57zpn6lnldwb2") (r "1.71")))

(define-public crate-apistos-swagger-ui-0.2.3 (c (n "apistos-swagger-ui") (v "0.2.3") (d (list (d (n "apistos-plugins") (r "^0.2.3") (d #t) (k 0)))) (h "0kyb0yfzz1bwgxl84y61cy07w8r0xxs02das1vi8990cyb5wkmsx") (r "1.75")))

(define-public crate-apistos-swagger-ui-0.2.4 (c (n "apistos-swagger-ui") (v "0.2.4") (d (list (d (n "apistos-plugins") (r "^0.2.4") (d #t) (k 0)))) (h "182sfncny4b3gb60axackvpbx1gwndv8xsrj45954cnszhj6n7m1") (r "1.75")))

