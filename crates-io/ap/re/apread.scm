(define-module (crates-io ap re apread) #:use-module (crates-io))

(define-public crate-apread-0.1.0 (c (n "apread") (v "0.1.0") (h "1as42rrj9di16x0d5ymjhzym38lbq2fn9gfmminl79a0i4kq6dw9")))

(define-public crate-apread-0.2.0 (c (n "apread") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("rustls" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt" "rt-multi-thread" "tracing"))) (d #t) (k 0)))) (h "1abz2rhnws59j2j0312lgvcqd8xd5f9ajbfg8g0n2cxx2vz3pbs8")))

(define-public crate-apread-0.2.1 (c (n "apread") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "html2md") (r "^0.2.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("rustls" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt" "rt-multi-thread" "tracing"))) (d #t) (k 0)))) (h "0l3psg29y1drd8rzv3ivd4x92lz4z7r6vn0hyk5ksz8lp9ydjzxn")))

