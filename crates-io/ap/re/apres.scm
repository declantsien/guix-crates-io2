(define-module (crates-io ap re apres) #:use-module (crates-io))

(define-public crate-apres-0.1.0 (c (n "apres") (v "0.1.0") (h "02d41ibc16kykb1r33w5wi1nijl49ak19h94kg3jdby4pbw6901q")))

(define-public crate-apres-0.2.1 (c (n "apres") (v "0.2.1") (h "0s4jdcpalv8p0lwrc9pjv0mqkvvb7nd2dgmh2gmqzwq5lpfgz8sv")))

(define-public crate-apres-0.2.2 (c (n "apres") (v "0.2.2") (h "1fandrlhgq809pnssqp0jpwm0inps26rygmrx1c8x10la8plm69j")))

(define-public crate-apres-0.2.3 (c (n "apres") (v "0.2.3") (h "0a442d7sncswsabv3gmwl5sfb058421fb99hzzjpjzsnai81s16j")))

(define-public crate-apres-0.2.4 (c (n "apres") (v "0.2.4") (h "1mmjsnc84qh3wnyb13j906qy7qcv7mxqy2i5ps84h5c45pcapsay")))

(define-public crate-apres-0.2.5 (c (n "apres") (v "0.2.5") (h "18a5s2xfrk1k4a98di90kb5vz1ksjwsy08rznizxv3amgdxna6jl")))

(define-public crate-apres-0.2.8 (c (n "apres") (v "0.2.8") (h "07z47lrhsnm9cmrs43078sn45nxrd8f88s3nwxn5a7a9ll81l59m")))

(define-public crate-apres-0.2.9 (c (n "apres") (v "0.2.9") (h "0zpd9wwbn8dw46sib75zlinkqwqxqs6c04rm0s9fijmaykdvgpcw")))

(define-public crate-apres-0.2.10 (c (n "apres") (v "0.2.10") (h "139f81an468k3qaxnxga7mfpvbnjw7d9l33c4p6yn5hkjvwhkhkd")))

(define-public crate-apres-0.2.11 (c (n "apres") (v "0.2.11") (h "19g2wa5h9yr63xw7d2h2incg928cqb9xbjlmazf8mkqfkcrvignz")))

(define-public crate-apres-0.3.0 (c (n "apres") (v "0.3.0") (h "1nrhpmipvba9zqwnlj9k6gri41qll03j8n62x7xx90hcn6xczy14")))

(define-public crate-apres-0.3.1 (c (n "apres") (v "0.3.1") (h "1yps1qaisb4sggwiq1ls8jc6cnb6wp00q2y9wyl86qw1wd9r0fgg")))

(define-public crate-apres-0.3.2 (c (n "apres") (v "0.3.2") (h "0hycgzk9ykr1npns9vz55zfx715n77zs00rp4zlmbyrmf94ipss5")))

(define-public crate-apres-0.3.3 (c (n "apres") (v "0.3.3") (h "151z189lwmh42vy8g6ypq7ns8d685v6sjfnj6sja0xnbhpanvshv")))

(define-public crate-apres-0.3.4 (c (n "apres") (v "0.3.4") (h "0x8wa7fd0vjkcyimnk1ff9f03iy2y60gim0jmgwn0f0rdj8hpgjn")))

