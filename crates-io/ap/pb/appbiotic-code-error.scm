(define-module (crates-io ap pb appbiotic-code-error) #:use-module (crates-io))

(define-public crate-appbiotic-code-error-0.1.0 (c (n "appbiotic-code-error") (v "0.1.0") (d (list (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ghh78m84c3szd1bayzvhp46c9vxdn19zsiqxf214qh26cbgmjyk")))

(define-public crate-appbiotic-code-error-0.1.1 (c (n "appbiotic-code-error") (v "0.1.1") (d (list (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ndvcq2dkqxnyd3ay107prgn4snnjbngr477g6scp9f0lhsby752")))

(define-public crate-appbiotic-code-error-0.1.2 (c (n "appbiotic-code-error") (v "0.1.2") (d (list (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0lrdsnm8bzirwv06jpbsnsip9v9jplwlbb3zi3chy3d6imijai3i")))

(define-public crate-appbiotic-code-error-0.1.3 (c (n "appbiotic-code-error") (v "0.1.3") (d (list (d (n "http") (r "^0.2.9") (o #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (o #t) (k 0)))) (h "0435s3iqvd4x0s2xbd4g809vzcfvb8bbl6y4kp75v89qblj157cp") (f (quote (("with-grpc")))) (s 2) (e (quote (("with-tonic" "dep:tonic") ("with-http" "dep:http"))))))

(define-public crate-appbiotic-code-error-0.2.0 (c (n "appbiotic-code-error") (v "0.2.0") (d (list (d (n "http") (r "^0.2.9") (o #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (o #t) (k 0)))) (h "0j012q96714wxv2bbcbs4chy75vmj4pp1vcxm9d653dmnqfdln49") (s 2) (e (quote (("with-tonic" "dep:tonic") ("with-http" "dep:http"))))))

