(define-module (crates-io ap pb appbiotic) #:use-module (crates-io))

(define-public crate-appbiotic-0.1.0 (c (n "appbiotic") (v "0.1.0") (d (list (d (n "appbiotic-code-error") (r "^0.1.0") (k 0)))) (h "18sgcqqcl9radd0d1mk8a467jlcidpnwd6p8sx7d0bc6rxfjw0fi")))

(define-public crate-appbiotic-0.1.1 (c (n "appbiotic") (v "0.1.1") (d (list (d (n "appbiotic-code-error") (r "^0.1.1") (k 0)))) (h "0vi0xilykjaq1fxmjghfnji7g47lcjif5c9gm5xcd0s81kgf4hm2")))

(define-public crate-appbiotic-0.1.2 (c (n "appbiotic") (v "0.1.2") (d (list (d (n "appbiotic-code-error") (r "^0.1.2") (k 0)))) (h "0pzqv66853iis1wb5q7fgxkkm138zxzxqvii20c18dwp50d2sfav")))

(define-public crate-appbiotic-0.1.3 (c (n "appbiotic") (v "0.1.3") (d (list (d (n "appbiotic-code-error") (r "^0.1.3") (k 0)))) (h "1r4pynb5kpar9j9n39cdifkr86v5xjibqdq9xi58z42l2xda8kmq") (f (quote (("with-tonic" "appbiotic-code-error/with-tonic") ("with-http" "appbiotic-code-error/with-http") ("with-grpc" "appbiotic-code-error/with-grpc") ("full" "with-grpc" "with-http" "with-tonic"))))))

