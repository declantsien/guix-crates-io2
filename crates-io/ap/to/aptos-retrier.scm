(define-module (crates-io ap to aptos-retrier) #:use-module (crates-io))

(define-public crate-aptos-retrier-0.1.0 (c (n "aptos-retrier") (v "0.1.0") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "03gm252639yl91rrvq7gjz8inkf7x8zp5kxz89qxnhynpyzkyf9c") (y #t)))

(define-public crate-aptos-retrier-0.1.1 (c (n "aptos-retrier") (v "0.1.1") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "16bkhjfdy12df3mpccdqsslf1rkhhq9f4ddah8s67060dn7bmcw6") (y #t)))

(define-public crate-aptos-retrier-0.1.3 (c (n "aptos-retrier") (v "0.1.3") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "18qp75z6yvwx90q2pmjpzsdl3whvxspzs0k2m2bqrsb02xwaxhzk") (y #t)))

(define-public crate-aptos-retrier-0.1.4 (c (n "aptos-retrier") (v "0.1.4") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0sa29wjrxk8nj5cw8z1mnwqihlsiial7wpgciiknxsfa6vzsbk5y") (y #t)))

(define-public crate-aptos-retrier-0.1.6 (c (n "aptos-retrier") (v "0.1.6") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "13cyr306qgzk8a0sm9xliw7rjqd5w8dmkrvsxz8amqb5i9i19dlp") (y #t)))

(define-public crate-aptos-retrier-0.1.7 (c (n "aptos-retrier") (v "0.1.7") (d (list (d (n "aptos-logger") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0k0s86aqhx9rhq0yn0agshd9cilxisg3njy00nph4g3grd34w5my") (y #t)))

(define-public crate-aptos-retrier-0.2.1 (c (n "aptos-retrier") (v "0.2.1") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0q6sp9f2r6ivj4vglgj1das0z8s0q65mzici8d77ad7480f5n0kd") (y #t)))

(define-public crate-aptos-retrier-0.2.2 (c (n "aptos-retrier") (v "0.2.2") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "10nxc00r7kccg0ajd65y9ypj8n0vncp7n7cfgc80a7blllwjin84") (y #t)))

(define-public crate-aptos-retrier-0.2.6 (c (n "aptos-retrier") (v "0.2.6") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "05wq7abbfz81hqkfd5av6q3pqhmgny6d0fwqs3p4kaq7b3a2p4gp") (y #t)))

(define-public crate-aptos-retrier-0.2.7 (c (n "aptos-retrier") (v "0.2.7") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("time"))) (d #t) (k 0)))) (h "07xjc9yj25wvdhjvgih775pij6mkqd2p8sa7kqibx7i1p4cmgnlb") (y #t)))

