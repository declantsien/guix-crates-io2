(define-module (crates-io ap to aptos-pprint) #:use-module (crates-io))

(define-public crate-aptos-pprint-0.1.0 (c (n "aptos-pprint") (v "0.1.0") (h "0iwgyi51yln5kgwqmyn7x6mhinsg15hzsscl7q7dhdnp2rn32cxa")))

(define-public crate-aptos-pprint-0.1.1 (c (n "aptos-pprint") (v "0.1.1") (h "1ygby2hpshy2qrny3q2qfajibmycapi3ldbijv6p7a6xh18i6lrs")))

(define-public crate-aptos-pprint-0.2.0 (c (n "aptos-pprint") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qlzrglycddlcg2h6lbjfg6if8iax772xnks4mggxbli3ziq4nrf")))

