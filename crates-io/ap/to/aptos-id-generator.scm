(define-module (crates-io ap to aptos-id-generator) #:use-module (crates-io))

(define-public crate-aptos-id-generator-0.1.0 (c (n "aptos-id-generator") (v "0.1.0") (h "1gn34rvsly1i1cgd55vdanw88d6vlnx8h48rj49zv02l9f0kyms3") (y #t)))

(define-public crate-aptos-id-generator-0.1.1 (c (n "aptos-id-generator") (v "0.1.1") (h "1nymm8n46qjpnv9fq566sd3hm3a4imlai2hpjgp2j6i0qz83qnz4") (y #t)))

(define-public crate-aptos-id-generator-0.1.2 (c (n "aptos-id-generator") (v "0.1.2") (h "10595hn2kcbdvspkxcxl7n8p34q5lsk7qr264shavcq9120lbrca") (y #t)))

(define-public crate-aptos-id-generator-0.1.3 (c (n "aptos-id-generator") (v "0.1.3") (h "10h7xyj0zwxa2gp29295dja63l3ac2ci59yaq80f0jjvz81jirv2") (y #t)))

(define-public crate-aptos-id-generator-0.1.4 (c (n "aptos-id-generator") (v "0.1.4") (h "1yd346lbamv5zkyfyn8nsnn3wcr00hvvacqbrp9cha9wy7gm1bhg") (y #t)))

(define-public crate-aptos-id-generator-0.1.5 (c (n "aptos-id-generator") (v "0.1.5") (h "010dmcnv487n2m4qfkrafp5bwv0ffwml0s9hw0sjnrh0wzpg5fyh") (y #t)))

(define-public crate-aptos-id-generator-0.1.6 (c (n "aptos-id-generator") (v "0.1.6") (h "145gir9g0n545phg93fyk7rswxy89vw2sf3fw2mydlgzhj8r3c5b") (y #t)))

(define-public crate-aptos-id-generator-0.1.7 (c (n "aptos-id-generator") (v "0.1.7") (h "0hn6b5s7g9dsg5cpxgrs9r97cpfg1d6z9a71pv6vvfh5jk29z6vd") (y #t)))

(define-public crate-aptos-id-generator-0.2.1 (c (n "aptos-id-generator") (v "0.2.1") (h "1f1k1a651qsdzdcqvhx7hvbr0hv1sbplnkg1gx0ayynrjkmhmsmc") (y #t)))

(define-public crate-aptos-id-generator-0.2.2 (c (n "aptos-id-generator") (v "0.2.2") (h "1sb7n7zwkwaq8cqa02bpnb6nyq0p56m8amvwkjjscjkpgnyxg5s7") (y #t)))

(define-public crate-aptos-id-generator-0.2.6 (c (n "aptos-id-generator") (v "0.2.6") (h "0vll0ka6gfsfhz0aq7a57bhcwh5xvxsp4p9dh98gdszymrx8b4vn") (y #t)))

(define-public crate-aptos-id-generator-0.2.7 (c (n "aptos-id-generator") (v "0.2.7") (h "11wygw9sikbbg13y72zq44pyklnadxh3cg3bfk8mxb74v25h5q32") (y #t)))

