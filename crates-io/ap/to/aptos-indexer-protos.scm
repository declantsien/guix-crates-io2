(define-module (crates-io ap to aptos-indexer-protos) #:use-module (crates-io))

(define-public crate-aptos-indexer-protos-1.0.9 (c (n "aptos-indexer-protos") (v "1.0.9") (d (list (d (n "pbjson") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("tls-roots" "transport" "prost" "gzip" "codegen"))) (d #t) (k 0)))) (h "0cl2zp6xa4bp6ab8vhq1cg419j5l7c80zvhx9j3a06izs84i93cc") (y #t)))

