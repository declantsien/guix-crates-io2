(define-module (crates-io ap to aptos-fallible) #:use-module (crates-io))

(define-public crate-aptos-fallible-0.1.0 (c (n "aptos-fallible") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dmlj19mqswhsl4ljbmqx9fqjk745vsfxnm7irs8sllkivmhlvl3") (y #t)))

(define-public crate-aptos-fallible-0.1.1 (c (n "aptos-fallible") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "166d7qhc5h4a82ngp52hbmc607nr44x2m82gfiz8wp6414xq53qi") (y #t)))

(define-public crate-aptos-fallible-0.1.2 (c (n "aptos-fallible") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0yswj7vc349hc1m0jbw8r7rblqjx2zx02z01r4rifdwl8lv5bs0w") (y #t)))

(define-public crate-aptos-fallible-0.1.3 (c (n "aptos-fallible") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xhr20zy525dxw6zl20n3a53wz06lvip720269ndvdj0xl6c25cw") (y #t)))

(define-public crate-aptos-fallible-0.1.4 (c (n "aptos-fallible") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1apgjcv7grcqsws61mhwanfp0brsj086m54vn5346psbzacsi6yl") (y #t)))

(define-public crate-aptos-fallible-0.1.6 (c (n "aptos-fallible") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nij9dgghfrrpq6lcz4njgbsgx2v86l4jp954s4qh6hkvdchcgzh") (y #t)))

(define-public crate-aptos-fallible-0.1.7 (c (n "aptos-fallible") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10x042sm1h01sg59l87zahvgjl94skwkfipbwfssywfiyz60sjns") (y #t)))

(define-public crate-aptos-fallible-0.2.1 (c (n "aptos-fallible") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mjipsm8v86nlfqids6anqrr0snc1ar225xxv8749hyrrkvfyvvg") (y #t)))

(define-public crate-aptos-fallible-0.2.2 (c (n "aptos-fallible") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rljjqnxr95chish85fwnviir7rff3v64ci7b4a69350xgrqrpv6") (y #t)))

(define-public crate-aptos-fallible-0.2.6 (c (n "aptos-fallible") (v "0.2.6") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1kjxkp8v6ycff2dn8zj406q04znf5j34gk778a55rrg8zhmc4h9g") (y #t)))

(define-public crate-aptos-fallible-0.2.7 (c (n "aptos-fallible") (v "0.2.7") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00rnvhyswqzm9ql2n7j6lj0wlnsiz01xb3n51lmapi0ph63q27hn") (y #t)))

