(define-module (crates-io ap to aptos-crypto-derive-link) #:use-module (crates-io))

(define-public crate-aptos-crypto-derive-link-0.0.3 (c (n "aptos-crypto-derive-link") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wpqm9l0jayjjxbvhykdag5kq9anlck0yjs2swfhsflvk2wnrjsh") (r "1.64")))

