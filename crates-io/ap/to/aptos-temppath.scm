(define-module (crates-io ap to aptos-temppath) #:use-module (crates-io))

(define-public crate-aptos-temppath-0.1.0 (c (n "aptos-temppath") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19g0knq3l0v8pqzqx3ji68bzw8fd04yim7kl23jwkdn3l33i7ivb") (y #t)))

(define-public crate-aptos-temppath-0.1.1 (c (n "aptos-temppath") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fd8ws5liq2pfhcf02pryl5akdaxyixhx7j5k88l7pfilwcad0jl") (y #t)))

(define-public crate-aptos-temppath-0.1.2 (c (n "aptos-temppath") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i5fpgk9qaaj8l8dnxwzmfpgwr18hzwqawqrxmiqkhw5wvqpmyvh") (y #t)))

(define-public crate-aptos-temppath-0.1.3 (c (n "aptos-temppath") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1b8q94sif7b08qsgzymvgdzr2mdgqdmlzswdpsr140gc7ncjnbyn") (y #t)))

(define-public crate-aptos-temppath-0.1.4 (c (n "aptos-temppath") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "12pvdiy26h0n45mvrpl0y7lhddrij4aqdqndisylaa8bvi8gz7x5") (y #t)))

(define-public crate-aptos-temppath-0.1.5 (c (n "aptos-temppath") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1f8js5nw3zv5jgamn3i2jbydrpm01x6sfzcy4rh7f92k6cl2xrw2") (y #t)))

(define-public crate-aptos-temppath-0.1.6 (c (n "aptos-temppath") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1a3wiwdrdzi9n2ql6f3my24lgp629gazgmqy0sz7pd0wd17nr1f6") (y #t)))

(define-public crate-aptos-temppath-0.1.7 (c (n "aptos-temppath") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0w2s0llym2hpfa1amj9809n8mfhg6xpvq7qmfw0dbrw1glsxvgf5") (y #t)))

(define-public crate-aptos-temppath-0.2.1 (c (n "aptos-temppath") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1i1kdhgw0bh6b99lmj3ycryg6wmz59450qb7ddvikb0b7s0ybinf") (y #t)))

(define-public crate-aptos-temppath-0.2.2 (c (n "aptos-temppath") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "18ms7zj0pnp60iiv93l0d9q4l69r51hwz5wk4ck3071lcl2z83qy") (y #t)))

(define-public crate-aptos-temppath-0.2.6 (c (n "aptos-temppath") (v "0.2.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0raxkv4dy87wxn22zhnhwakviwq4i8s5jmwdzksb8k5y86rjk6y9") (y #t)))

(define-public crate-aptos-temppath-0.2.7 (c (n "aptos-temppath") (v "0.2.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1iwpq1925vicci76qkm5crr1rryv0h6saa3dw4hf16qx5nd3pq9a") (y #t)))

