(define-module (crates-io ap to aptos-global-constants) #:use-module (crates-io))

(define-public crate-aptos-global-constants-0.1.0 (c (n "aptos-global-constants") (v "0.1.0") (h "1n2dqdxnk100km2y0i3d9ayg9fdkrdwc42j1hzxahvnra902blsd") (y #t)))

(define-public crate-aptos-global-constants-0.1.1 (c (n "aptos-global-constants") (v "0.1.1") (h "1djx057zm1lsxjx7wj092zhpgch48zqq76i76zvjb31v4df6j0d5") (y #t)))

(define-public crate-aptos-global-constants-0.1.2 (c (n "aptos-global-constants") (v "0.1.2") (h "1aqqp0j1ps9yljbhwaspcv0k1ym5wd63xhi4jkd818jjny1kq4g0") (y #t)))

(define-public crate-aptos-global-constants-0.1.3 (c (n "aptos-global-constants") (v "0.1.3") (h "176596m0qz1rfvm4gz8ilginvw4i0n9l1vlh4fg5rsnaivzkji4f") (y #t)))

(define-public crate-aptos-global-constants-0.1.4 (c (n "aptos-global-constants") (v "0.1.4") (h "0kwfs6kvx6jqgyx7vw5qqyw7zxr32pbgxb0418as88qbx0pi07qd") (y #t)))

(define-public crate-aptos-global-constants-0.1.5 (c (n "aptos-global-constants") (v "0.1.5") (h "04igl0wb8nsqxnlp8nav8336zfs3rjf73f7p0gma45gdzai4gbla") (y #t)))

(define-public crate-aptos-global-constants-0.1.6 (c (n "aptos-global-constants") (v "0.1.6") (h "1nhqqcsly4slavb8cmf89djvgjgpvzj0y9zn07pgdv9j5bah9h0k") (y #t)))

(define-public crate-aptos-global-constants-0.1.7 (c (n "aptos-global-constants") (v "0.1.7") (h "06yw4yhwcx39iys6plx023j8cfz1bli32sn31nza9ww4vd13xv1g") (y #t)))

(define-public crate-aptos-global-constants-0.2.1 (c (n "aptos-global-constants") (v "0.2.1") (h "0ml41jfxc5zx7x8vl02y5fva7r20zzmrsi6fnz2c9afh8iw7im4b") (y #t)))

(define-public crate-aptos-global-constants-0.2.2 (c (n "aptos-global-constants") (v "0.2.2") (h "0ir2bqai86rnhssmilr7ngavw028ap2ppcq61x62wp2mkb0hmcvb") (y #t)))

(define-public crate-aptos-global-constants-0.2.6 (c (n "aptos-global-constants") (v "0.2.6") (h "19ip6x38fwhdbnv6ak286i8g00xd22gi11fz313kxqpkgmrli39l") (y #t)))

(define-public crate-aptos-global-constants-0.2.7 (c (n "aptos-global-constants") (v "0.2.7") (h "0fjsr7hbig5ks71dlqp0xwyrvn0ps63cghpzwlfkcgc2b2hs0am9") (y #t)))

