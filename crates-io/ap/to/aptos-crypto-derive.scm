(define-module (crates-io ap to aptos-crypto-derive) #:use-module (crates-io))

(define-public crate-aptos-crypto-derive-0.1.0 (c (n "aptos-crypto-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hgw74ch1m4plg0ca90j7knf7zmy3xpn169fb95y3x8g0bhh3mcn") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.1 (c (n "aptos-crypto-derive") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xzich37m88qlnvavcgfnh8rqj6gy83bxljkyr2vqsb11zpyqb0c") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.2 (c (n "aptos-crypto-derive") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "00z896fayfi9fs5sxz6yhzix3q3s03zzwqwawprx219i2w1w25br") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.3 (c (n "aptos-crypto-derive") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h7crw5darrjabld894ahk983jsa8jw3h7m4dcc5h23b4ibp7zn") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.4 (c (n "aptos-crypto-derive") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "08dilas70162zmdiskcj0i4scn7skw8gr0zjyw5d2bafhhkp1mxk") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.5 (c (n "aptos-crypto-derive") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s89y3wfw65cnwbpv4byc6fj783y11yk8p79c8ifcjnlaf68q134") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.6 (c (n "aptos-crypto-derive") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "10x6lf8i6h72m0vija95hanppq72is2g36hyz3v4vzp1a981akkc") (y #t)))

(define-public crate-aptos-crypto-derive-0.1.7 (c (n "aptos-crypto-derive") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "19wlzqgf0f1nzcq2720zjk2nf5qbdj4zawjh15rf11wgbr656xhv") (y #t)))

(define-public crate-aptos-crypto-derive-0.2.1 (c (n "aptos-crypto-derive") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "03qm643z002b58468124sq5qiwn4cnddvsjl5wfkzah1cai98nsl") (y #t)))

(define-public crate-aptos-crypto-derive-0.2.2 (c (n "aptos-crypto-derive") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "15s5l6dgsb0qav8fdpwry7pjgqpnk1li6ic9y29b7imn523ad4ja") (y #t)))

(define-public crate-aptos-crypto-derive-0.2.6 (c (n "aptos-crypto-derive") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bbyf58wz9nsvanb9a02cwxx8b056khbmx83zvgv8q0mv0g5l7k8") (y #t)))

(define-public crate-aptos-crypto-derive-0.2.7 (c (n "aptos-crypto-derive") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ay030jlylj20wl9vdwah26f0y1nipgqcyfqnqn6fwkyi45xli2y") (y #t)))

