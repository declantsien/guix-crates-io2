(define-module (crates-io ap to aptos-metrics-core) #:use-module (crates-io))

(define-public crate-aptos-metrics-core-0.1.0 (c (n "aptos-metrics-core") (v "0.1.0") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "10767k58s2yzxhh1i22jarysr6jcp32grc6v4hxwkpy6iyz2i4vw") (y #t)))

(define-public crate-aptos-metrics-core-0.1.1 (c (n "aptos-metrics-core") (v "0.1.1") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "148cn7lv4rj2j8lswkx8kx8aqbpi3ch4g01k4xjnkpn6dhnz5921") (y #t)))

(define-public crate-aptos-metrics-core-0.1.2 (c (n "aptos-metrics-core") (v "0.1.2") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "1bv7pwm6ljhlb7plkjpyzr23xgz6ais1qm4028aj782b9zc9lzn5") (y #t)))

(define-public crate-aptos-metrics-core-0.1.3 (c (n "aptos-metrics-core") (v "0.1.3") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "061rfn1gk8dsjhkksylh9x32aplk93zw707z7nv5kis1yvyxhyad") (y #t)))

(define-public crate-aptos-metrics-core-0.1.4 (c (n "aptos-metrics-core") (v "0.1.4") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "0dbdl9ybnybnrm32744fklqhp2j0i3gv05rfh0iivzmpm9dygiiv") (y #t)))

(define-public crate-aptos-metrics-core-0.1.5 (c (n "aptos-metrics-core") (v "0.1.5") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "0r4nv8f5xbcl4yqadcla8csp2my8g6qkwhx4virsgwfjkkv5d7c0") (y #t)))

(define-public crate-aptos-metrics-core-0.1.6 (c (n "aptos-metrics-core") (v "0.1.6") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "1aqdai5ia0ks6ihz18v33j4yinpxcgi31d705bd1p2an6lnz7jc5") (y #t)))

(define-public crate-aptos-metrics-core-0.1.7 (c (n "aptos-metrics-core") (v "0.1.7") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "1g66p32kd5dzy92v1afnz7pbq9lzxsr7vc0ipyvfjavs0f3j56zp") (y #t)))

(define-public crate-aptos-metrics-core-0.2.1 (c (n "aptos-metrics-core") (v "0.2.1") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "0shf54c40zkp5cdhq53a7nl7da8lwrk779cppvmfn5sg1h0ffv5g") (y #t)))

(define-public crate-aptos-metrics-core-0.2.2 (c (n "aptos-metrics-core") (v "0.2.2") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "08y8z7lbamm4m9yf3ib8m3a79d6y1qnx3jv5kqzgflym1a3c6aj0") (y #t)))

(define-public crate-aptos-metrics-core-0.2.6 (c (n "aptos-metrics-core") (v "0.2.6") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "1r06slwfa4mwd12c5d8wd197p12kpdycfxgnx19pyzxqy83b4amy") (y #t)))

(define-public crate-aptos-metrics-core-0.2.7 (c (n "aptos-metrics-core") (v "0.2.7") (d (list (d (n "prometheus") (r "^0.13.0") (k 0)))) (h "13x2smjhj1ml6g7fwz74mkpdmf0440b2a66s6kqnk49zd6z7jxzn") (y #t)))

