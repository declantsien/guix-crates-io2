(define-module (crates-io ap to aptos-upload) #:use-module (crates-io))

(define-public crate-aptos-upload-0.1.2 (c (n "aptos-upload") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "aptos-cli-common") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-publish") (r "^0.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clitool") (r "^0.1.0") (d #t) (k 0)) (d (n "move-package") (r "^0.2.1") (d #t) (k 0) (p "mv-package")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0mzyaas89w6a6byq8ab3ah13rr3yawn5yml8s8fsnyfyg5rddw")))

