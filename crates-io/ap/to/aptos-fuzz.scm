(define-module (crates-io ap to aptos-fuzz) #:use-module (crates-io))

(define-public crate-aptos-fuzz-0.1.1 (c (n "aptos-fuzz") (v "0.1.1") (d (list (d (n "aptos-fuzzer") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "08b81vmdp21lr5vkzg85f423vwml4xg195pj4jgj68rxg6p78r65") (y #t)))

(define-public crate-aptos-fuzz-0.1.3 (c (n "aptos-fuzz") (v "0.1.3") (d (list (d (n "aptos-fuzzer") (r "^0.1.0") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "18mjjy3bqj7wygbvvprf1hmw6wcf0sgqw2f0q71dhhwgnxzqll9l") (y #t)))

(define-public crate-aptos-fuzz-0.1.4 (c (n "aptos-fuzz") (v "0.1.4") (d (list (d (n "aptos-fuzzer") (r "^0.1.0") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1ikpilv4rnfhaqxg1kwybads1h99db9nibjwm2rgw49rpkr6kwwq") (y #t)))

(define-public crate-aptos-fuzz-0.1.7 (c (n "aptos-fuzz") (v "0.1.7") (d (list (d (n "aptos-fuzzer") (r "^0.1.7") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0gfq0zaav80xf3qbpglccvnds68vmyxcnwbfd281nyfykwxlk3cv") (y #t)))

(define-public crate-aptos-fuzz-0.2.1 (c (n "aptos-fuzz") (v "0.2.1") (d (list (d (n "aptos-fuzzer") (r "^0.2.1") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1j2l1ackfkvk6zk92r24lh5lq5q9c9j723gss9x339qrcyvs9shz") (y #t)))

(define-public crate-aptos-fuzz-0.2.2 (c (n "aptos-fuzz") (v "0.2.2") (d (list (d (n "aptos-fuzzer") (r "^0.2.1") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1kfmyycwmxqyi15g75prxp763h88ig8s54f65maxd92bjj3i7rlz") (y #t)))

(define-public crate-aptos-fuzz-0.2.6 (c (n "aptos-fuzz") (v "0.2.6") (d (list (d (n "aptos-fuzzer") (r "^0.2.1") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1my2cmxs9bkc64ca51a22cflz1nyljrrd8m0cj1sc4ngx97mayww") (y #t)))

(define-public crate-aptos-fuzz-0.2.7 (c (n "aptos-fuzz") (v "0.2.7") (d (list (d (n "aptos-fuzzer") (r "^0.2.1") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "=0.3.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "09bqnp6q817dax91n700nrah1xpw2c6dpjh2kvqqba2mv7l4nsgr") (y #t)))

