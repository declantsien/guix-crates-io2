(define-module (crates-io ap to aptos-ledger) #:use-module (crates-io))

(define-public crate-aptos-ledger-0.1.0 (c (n "aptos-ledger") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ledger-apdu") (r "^0.10.0") (d #t) (k 0)) (d (n "ledger-transport-hid") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1aakcfz9kvv8rg9xd2zyzm68qaqx8yiz0ykixd38b0ml835h3vdh") (y #t) (r "1.64")))

