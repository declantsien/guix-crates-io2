(define-module (crates-io ap to aptos-memsocket) #:use-module (crates-io))

(define-public crate-aptos-memsocket-0.1.0 (c (n "aptos-memsocket") (v "0.1.0") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "07h4n2zgfci4103jd63m46y1vxpwxl80d4pq50agwj7rzkd8gqcm") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.1 (c (n "aptos-memsocket") (v "0.1.1") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "01ig6khn7l0rm7dy21bif5igr9dyjx841vzvk3f3kd8zc690vc8d") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.2 (c (n "aptos-memsocket") (v "0.1.2") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0mz2zwf6h4yqiv3242vin1vi458h59hb8nihs393cng8iz3q89hk") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.3 (c (n "aptos-memsocket") (v "0.1.3") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "02csjhzl0i8mkc1lfcl511iflazl5vlxqawsyfg22dcg1c9kj0jq") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.4 (c (n "aptos-memsocket") (v "0.1.4") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1rc3fc70zb73ivr36gm2nfn41zqjr94zcdc82nbnbpks80wyccgz") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.5 (c (n "aptos-memsocket") (v "0.1.5") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "06kw0yy149kdbw3181kcr5kbw82h0pr0v8yx7rs7pxbckw4jymd7") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.6 (c (n "aptos-memsocket") (v "0.1.6") (d (list (d (n "aptos-infallible") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "09wivf9397yqg501dqpzlqvf5g59w2pcb6l79pcczbmsw2z5icqi") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.1.7 (c (n "aptos-memsocket") (v "0.1.7") (d (list (d (n "aptos-infallible") (r "^0.1.7") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0cjk1z1ffq4dqw6wi0c0w199sswnj81bdr5bfzxwvm5agdiczjh0") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.2.1 (c (n "aptos-memsocket") (v "0.2.1") (d (list (d (n "aptos-infallible") (r "^0.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "165i500rwbfljvb3i08ida73i5c52aar67g2lb9nhy4qbf8xark3") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.2.2 (c (n "aptos-memsocket") (v "0.2.2") (d (list (d (n "aptos-infallible") (r "^0.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "1v29jxzkwkn8wvci6idzj92blk634gc6jhmc8shllpg7k90kl5g9") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.2.6 (c (n "aptos-memsocket") (v "0.2.6") (d (list (d (n "aptos-infallible") (r "^0.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0bdncmz4cvhvyf1hnc5rrx419m9rnlswjzhm3vb8f26635n4ywjv") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

(define-public crate-aptos-memsocket-0.2.7 (c (n "aptos-memsocket") (v "0.2.7") (d (list (d (n "aptos-infallible") (r "^0.2.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "04x9wv6ngprddzmkp5vhhb89y0w7j3cwkm5612xc1xv1vxvi3j5k") (f (quote (("testing") ("fuzzing") ("default")))) (y #t)))

