(define-module (crates-io ap to aptos-infallible) #:use-module (crates-io))

(define-public crate-aptos-infallible-0.1.0 (c (n "aptos-infallible") (v "0.1.0") (h "1lxl7yc80idqrww11k3dnyrw7qs4xic5dn88bkg8gvyxi1y6jv8m") (y #t)))

(define-public crate-aptos-infallible-0.1.1 (c (n "aptos-infallible") (v "0.1.1") (h "1db47z672j1m8319a8m0rra957qxp6816x3mz23qna9n6gylhhh1") (y #t)))

(define-public crate-aptos-infallible-0.1.2 (c (n "aptos-infallible") (v "0.1.2") (h "1jzbblnxjz3s2hidv91wdd3pqvs10jqba3gxpv7bk5qvc6cnildr") (y #t)))

(define-public crate-aptos-infallible-0.1.3 (c (n "aptos-infallible") (v "0.1.3") (h "1fnkd06512l66whpbpj0mp19gnzaq5fi1nij2fbzxfxpp9rvb1qx") (y #t)))

(define-public crate-aptos-infallible-0.1.4 (c (n "aptos-infallible") (v "0.1.4") (h "1c3hwxynb2mvhdrbwansynhd4v954zljzj38yn33ar9s0zmjyrc9") (y #t)))

(define-public crate-aptos-infallible-0.1.5 (c (n "aptos-infallible") (v "0.1.5") (h "03vk6gyj2v53swz9m5df3w1l4wima9s0vyhyiylfnrw7n0kwxdzv") (y #t)))

(define-public crate-aptos-infallible-0.1.6 (c (n "aptos-infallible") (v "0.1.6") (h "0n8b0krs2wzrkyjzwllzdybn2yaingp7zjbjj4a1z2hqadayp4nv") (y #t)))

(define-public crate-aptos-infallible-0.1.7 (c (n "aptos-infallible") (v "0.1.7") (h "1sl5zmw369703b1zsa6r2k16qpfch3zpd66kli30qb2vnpgcbjj3") (y #t)))

(define-public crate-aptos-infallible-0.2.1 (c (n "aptos-infallible") (v "0.2.1") (h "1176r2lsbv38ra8jgrmz6md021bmk55g96ww5a879pjznx2a41pz") (y #t)))

(define-public crate-aptos-infallible-0.2.2 (c (n "aptos-infallible") (v "0.2.2") (h "1lrx52kfkn7n6ghvck8rsjjwi0yxxvfbiq5xnkc95nvj5y2rayhw") (y #t)))

(define-public crate-aptos-infallible-0.2.6 (c (n "aptos-infallible") (v "0.2.6") (h "0i3vfma8fp2i2fpfz7c0s72z5g0h3237hgxj6x2lxnhxmcg9rli1") (y #t)))

(define-public crate-aptos-infallible-0.2.7 (c (n "aptos-infallible") (v "0.2.7") (h "156vca4cq8r2bpcwn7pcalb15ygcszymna3m45pqmykwzls4rvx9") (y #t)))

