(define-module (crates-io ap to aptos-keygen) #:use-module (crates-io))

(define-public crate-aptos-keygen-0.1.0 (c (n "aptos-keygen") (v "0.1.0") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15jyy7dxnzfqf2nha0hwkrcbz6lidr77zbayi3fg38af9lk778j1") (y #t)))

(define-public crate-aptos-keygen-0.1.1 (c (n "aptos-keygen") (v "0.1.1") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04fxnrfk1pggj7pchiv0nkmkqkikryxcn3cax4nkq9fgpyn28082") (y #t)))

(define-public crate-aptos-keygen-0.1.2 (c (n "aptos-keygen") (v "0.1.2") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14q9hzf0rpibix5pdgzdn51zfag507jd7cwnj5iagqlqqnxrv3n9") (y #t)))

(define-public crate-aptos-keygen-0.1.3 (c (n "aptos-keygen") (v "0.1.3") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ax5wvr9gsqlxapy71y83kf0186hbni09n72mp664hf5wmadxn83") (y #t)))

(define-public crate-aptos-keygen-0.1.4 (c (n "aptos-keygen") (v "0.1.4") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1wb0xrgfnxw1n4smdsk7r4x7ncqf8q4vbx2pmwlbl09ms4567vhl") (y #t)))

(define-public crate-aptos-keygen-0.1.5 (c (n "aptos-keygen") (v "0.1.5") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02l0gplc2vfzsm6vp70wg8d9qxrviqwbgjzi6rr6ryicxskmsnb8") (y #t)))

(define-public crate-aptos-keygen-0.1.6 (c (n "aptos-keygen") (v "0.1.6") (d (list (d (n "aptos-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "091rz4f4v6yaaxj2pz5mrmjzmvcsiigav5kmzmsg5f59fyal7skf") (y #t)))

(define-public crate-aptos-keygen-0.1.7 (c (n "aptos-keygen") (v "0.1.7") (d (list (d (n "aptos-crypto") (r "^0.1.7") (d #t) (k 0)) (d (n "aptos-types") (r "^0.1.7") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0q1inpr5w9k2b55nwwyd85r20qyp91va9griibvaaamv37j6xix7") (y #t)))

(define-public crate-aptos-keygen-0.2.1 (c (n "aptos-keygen") (v "0.2.1") (d (list (d (n "aptos-crypto") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1qggxry9lmvfqbgm4lc03zw9v9jifszlmyza7js0hygl9ig5xswm") (y #t)))

(define-public crate-aptos-keygen-0.2.2 (c (n "aptos-keygen") (v "0.2.2") (d (list (d (n "aptos-crypto") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0pki9g5wy9rydnwa7jr0bydfpvrlm0a60yly9l2cmrnhal41wh3i") (y #t)))

(define-public crate-aptos-keygen-0.2.6 (c (n "aptos-keygen") (v "0.2.6") (d (list (d (n "aptos-crypto") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0353hrgflf9bh2rlgbyih000b05pq0q6bjkayass8a7nkwr5kjw7") (y #t)))

(define-public crate-aptos-keygen-0.2.7 (c (n "aptos-keygen") (v "0.2.7") (d (list (d (n "aptos-crypto") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1fj7jr3di6wgiahvmc1ak337rzkc9rx60p1n43lyhy9jbjiwx6g6") (y #t)))

