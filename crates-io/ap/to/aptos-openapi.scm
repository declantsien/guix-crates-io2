(define-module (crates-io ap to aptos-openapi) #:use-module (crates-io))

(define-public crate-aptos-openapi-0.2.2 (c (n "aptos-openapi") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.38") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1s38chh0w1h00lb2f1c07dscx2bb60gl0d2245flpqs09ganhhpk") (y #t)))

(define-public crate-aptos-openapi-0.2.7 (c (n "aptos-openapi") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.38") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0krx81x5frfjbw860yv4zsmnic9l82bdnfa7zh2zrahfyyapiqrb") (y #t)))

