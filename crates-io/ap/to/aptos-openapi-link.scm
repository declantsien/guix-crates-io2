(define-module (crates-io ap to aptos-openapi-link) #:use-module (crates-io))

(define-public crate-aptos-openapi-link-0.1.0 (c (n "aptos-openapi-link") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.40") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0xk5r2yjc7dm6w2jk4yl9d0njin2z8npypxdqnsyrr2gfqzi3ap1") (r "1.64")))

