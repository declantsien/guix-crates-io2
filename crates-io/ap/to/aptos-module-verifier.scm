(define-module (crates-io ap to aptos-module-verifier) #:use-module (crates-io))

(define-public crate-aptos-module-verifier-0.1.5 (c (n "aptos-module-verifier") (v "0.1.5") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "0jrsr5g7zvxhy0n7pnrx53rxxqs908dq71xavhj0cy5m20vnhxqf") (y #t)))

(define-public crate-aptos-module-verifier-0.1.6 (c (n "aptos-module-verifier") (v "0.1.6") (d (list (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "1b08zyqs434hhqsvj1204q4h7qaj4l7wx3k1b9rry5xca5nkmxlj") (y #t)))

(define-public crate-aptos-module-verifier-0.1.7 (c (n "aptos-module-verifier") (v "0.1.7") (d (list (d (n "aptos-workspace-hack") (r "^0.1.7") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.7") (f (quote ("address32"))) (d #t) (k 0)))) (h "1w5kl4z3q0ihpyv2lpq5nh3yiak9yl7y3bjbpkrsb6b3lhm7brk2") (y #t)))

(define-public crate-aptos-module-verifier-0.2.1 (c (n "aptos-module-verifier") (v "0.2.1") (d (list (d (n "move-deps") (r "^0.2.1") (f (quote ("address32"))) (d #t) (k 0)))) (h "16iimgkxj6hj5i0y2mjmbrjn4ii0g6nf5h8a45bvcwizsh55bag7") (y #t)))

(define-public crate-aptos-module-verifier-0.2.2 (c (n "aptos-module-verifier") (v "0.2.2") (d (list (d (n "move-deps") (r "^0.2.1") (f (quote ("address32"))) (d #t) (k 0)))) (h "0m4hnpvyqczssc8lrnq60q0hkxm2p64dnmx03gl4h4mbg4wvd4f5") (y #t)))

(define-public crate-aptos-module-verifier-0.2.6 (c (n "aptos-module-verifier") (v "0.2.6") (d (list (d (n "move-deps") (r "^0.2.1") (f (quote ("address32"))) (d #t) (k 0)))) (h "08rr161ys3p5r7105agndwwhigx3m65vpxkgc79ld7smajc9xhlj") (y #t)))

(define-public crate-aptos-module-verifier-0.2.7 (c (n "aptos-module-verifier") (v "0.2.7") (d (list (d (n "move-deps") (r "^0.2.1") (f (quote ("address32"))) (d #t) (k 0)))) (h "00i18f59i72597bq0fbqkxwqamqxlj50lf0ba7k9ysrinhn3vvj9") (y #t)))

