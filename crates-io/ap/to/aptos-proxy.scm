(define-module (crates-io ap to aptos-proxy) #:use-module (crates-io))

(define-public crate-aptos-proxy-0.1.0 (c (n "aptos-proxy") (v "0.1.0") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "0r9vh9ncsp18iyjm1w881yjhw48sv5pxp5sisf4jjiqx3z4n0aqg") (y #t)))

(define-public crate-aptos-proxy-0.1.1 (c (n "aptos-proxy") (v "0.1.1") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "0f2zd0d8ylc7i7x1x9p2p3q1jlgp40jypc9nd738j3zrw629alni") (y #t)))

(define-public crate-aptos-proxy-0.1.2 (c (n "aptos-proxy") (v "0.1.2") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1m75q4ydc9vivk112cycg0ldqsgvmndm97w8f0gap6ci69kd06lw") (y #t)))

(define-public crate-aptos-proxy-0.1.3 (c (n "aptos-proxy") (v "0.1.3") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "0kfqfyf2h688vg2f3mv492w3pv7c9jy1j5y7g3alhb44anm483c8") (y #t)))

(define-public crate-aptos-proxy-0.1.4 (c (n "aptos-proxy") (v "0.1.4") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "08a0ihq3b8b6gl95rh7mr1rpb1k6xym8q7n8mcsz6hslqikzml9a") (y #t)))

(define-public crate-aptos-proxy-0.1.5 (c (n "aptos-proxy") (v "0.1.5") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "16rma08mc21ka71ppjd03nzmh1cyz5vsz91zdd7sjhds0rdkxmq7") (y #t)))

(define-public crate-aptos-proxy-0.1.6 (c (n "aptos-proxy") (v "0.1.6") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1kha3bdj6wdzm7sqxp9zk0ckgnbr7j5hm0bz0wg7varzplbz3qh4") (y #t)))

(define-public crate-aptos-proxy-0.1.7 (c (n "aptos-proxy") (v "0.1.7") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1acf6lwgw5l2fa6bd274d17j38vmf9ligm4q6idr23xdhj1hcpvs") (y #t)))

(define-public crate-aptos-proxy-0.2.1 (c (n "aptos-proxy") (v "0.2.1") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "19ac49fczslz54z1h8is0sgf73cahysf8s4kb261yydla9vf4308") (y #t)))

(define-public crate-aptos-proxy-0.2.2 (c (n "aptos-proxy") (v "0.2.2") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "0pbrssahig66n04blnkzb7mvsr6acmgh8szrphdngjq9bc7a53mv") (y #t)))

(define-public crate-aptos-proxy-0.2.6 (c (n "aptos-proxy") (v "0.2.6") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1vnmdxhhc0fdvvfp78xy9ws9va2hpyxdg4y22i3945rkd6689zvn") (y #t)))

(define-public crate-aptos-proxy-0.2.7 (c (n "aptos-proxy") (v "0.2.7") (d (list (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1zmbp45d1h4n05f2y9iajf99ay6dd5jq2r0fwc549n8jpj3wrgw7") (y #t)))

