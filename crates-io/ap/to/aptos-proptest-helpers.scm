(define-module (crates-io ap to aptos-proptest-helpers) #:use-module (crates-io))

(define-public crate-aptos-proptest-helpers-0.1.0 (c (n "aptos-proptest-helpers") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "08y4vmzf23dqp68g59hb3vs6iyihav6w3vcf9m98q23snr3d1d0q") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.1 (c (n "aptos-proptest-helpers") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1g1hmi1m9a8kfnddcwjnvxqg4hczsz750a0zkk5i8x1l2wdvl14q") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.2 (c (n "aptos-proptest-helpers") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "186n76n2dna74k0a9jd2xfdim505hhlbi52kccl0m0cy29wvcnnl") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.3 (c (n "aptos-proptest-helpers") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0vlvj2b68xkqf4km2q6c276xyv5x2izmz9iviqb3lfi671k3kzr8") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.4 (c (n "aptos-proptest-helpers") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0zkd3fxfkk4rdqj9jsiv3igi9jdihdd56wdcayz8yhcrz0rp4fa9") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.5 (c (n "aptos-proptest-helpers") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "07ckz9gykqh4p47xzjxrgb1q81pjr6vhxqw6krq0rzijcdp5yd9d") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.6 (c (n "aptos-proptest-helpers") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1dxn3kxrdkgwi82xg9f0gjbphz5j4s44692j37537ajvh64pzvx8") (y #t)))

(define-public crate-aptos-proptest-helpers-0.1.7 (c (n "aptos-proptest-helpers") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0h8angbr4xh2n6d1h6idp0i3k5xin3xc27p6k826jjfjasv5z7n2") (y #t)))

(define-public crate-aptos-proptest-helpers-0.2.1 (c (n "aptos-proptest-helpers") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1wz20nzzmwpyx5svcim39qqp2x51p47dl4c8kag7r6kqd5ri269p") (y #t)))

(define-public crate-aptos-proptest-helpers-0.2.2 (c (n "aptos-proptest-helpers") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1z12ifv1ms7ddmm7wafy4qri31ixqlkhjgfw15b9l9rhnxjml8s1") (y #t)))

(define-public crate-aptos-proptest-helpers-0.2.6 (c (n "aptos-proptest-helpers") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1pj357xvzav8cv9bfjggiagqbj9x90l4s8250fxsimyl91f6q7j8") (y #t)))

(define-public crate-aptos-proptest-helpers-0.2.7 (c (n "aptos-proptest-helpers") (v "0.2.7") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0v68hz07k912fp9xnjx7s0g1chdjmwgmp3vsiqr57i7dl142v2pd") (y #t)))

