(define-module (crates-io ap to aptos-secure-push-metrics) #:use-module (crates-io))

(define-public crate-aptos-secure-push-metrics-0.1.0 (c (n "aptos-secure-push-metrics") (v "0.1.0") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0jdjmpljgq0v6gg0niis17xxgrx5ndq1vq7lca2lgljsqrk6fgc4") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.1 (c (n "aptos-secure-push-metrics") (v "0.1.1") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0zffqr7f59sks8gkyrbms8120fdpj3i9p7jprv7vl4n834g9w40w") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.2 (c (n "aptos-secure-push-metrics") (v "0.1.2") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "1fllpnglvw03glz2azz037zqwn4w78w1ljzigz8iqbdykxmayigw") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.3 (c (n "aptos-secure-push-metrics") (v "0.1.3") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0lnmdmfwb8q7fy7cdscm57i00csbxmp54v107pfij43bzqfvy2yd") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.4 (c (n "aptos-secure-push-metrics") (v "0.1.4") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0j61x5694xq23nrs2yqfzhl9f97iwp5z3fxvkqkasrjm4945lsgl") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.5 (c (n "aptos-secure-push-metrics") (v "0.1.5") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "1gzm1ijqai7xlky4sgnfy5z6inbkbimc11czgsj2ws39nq2jabbi") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.6 (c (n "aptos-secure-push-metrics") (v "0.1.6") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "1f4xm9w2r41c8z33v536ylv1zd8smcydbdvdwm3pmsm3wnag2310") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.1.7 (c (n "aptos-secure-push-metrics") (v "0.1.7") (d (list (d (n "aptos-logger") (r "^0.1.7") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.1.7") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "1yz84s6h878gx8y1lls39vy0115vnww9g6waxzlpi4m2x7zpwybw") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.2.1 (c (n "aptos-secure-push-metrics") (v "0.2.1") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0517pfzc2cyq66wb28s8qg1i8dbysz5j9ayl16a2jqjl2lwkcg4w") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.2.2 (c (n "aptos-secure-push-metrics") (v "0.2.2") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "184r0z65ffbhfl3g8vhvxlpiqmlxrb6vw2shl4jlhbkx3xi0zhb9") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.2.6 (c (n "aptos-secure-push-metrics") (v "0.2.6") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "0vsmv3kbp5vk1nnj0nvpjfyqgs6r56bw2c1j9k2wydiv64w55pvp") (y #t)))

(define-public crate-aptos-secure-push-metrics-0.2.7 (c (n "aptos-secure-push-metrics") (v "0.2.7") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-metrics-core") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^1.5.4") (f (quote ("json" "native-tls"))) (k 0)))) (h "1g3wz3p2hj12yqwxmbijai6n3dkqrsk3m7wjkr7x91d1r7zk457d") (y #t)))

