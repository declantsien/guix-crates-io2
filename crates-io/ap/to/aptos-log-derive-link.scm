(define-module (crates-io ap to aptos-log-derive-link) #:use-module (crates-io))

(define-public crate-aptos-log-derive-link-0.1.0 (c (n "aptos-log-derive-link") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vxbnyczvs63aq2l90dm354wqmiinmb6d5lnjynaacjn5qyn6sk0") (r "1.64")))

