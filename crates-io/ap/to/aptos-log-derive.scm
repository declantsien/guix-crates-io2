(define-module (crates-io ap to aptos-log-derive) #:use-module (crates-io))

(define-public crate-aptos-log-derive-0.1.0 (c (n "aptos-log-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bc0v4gyvcsbnmaaf038a4yy4gc0dk195j3gm24a7sc6w5gnlxw2") (y #t)))

(define-public crate-aptos-log-derive-0.1.1 (c (n "aptos-log-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05pdlynzwfv83440g8idn0injqi9ihdjjkx14sirgzhkxjzsli8a") (y #t)))

(define-public crate-aptos-log-derive-0.1.2 (c (n "aptos-log-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hmink3sbhpg8xw1p0jy28vgiw6hl9zm0n2i61y64w6y1gl1h7xy") (y #t)))

(define-public crate-aptos-log-derive-0.1.3 (c (n "aptos-log-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0szdbnh5zb9vyp0wsg0445lqgr5axqvamqibsr3y1487qnkkaidi") (y #t)))

(define-public crate-aptos-log-derive-0.1.4 (c (n "aptos-log-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j86sqyqqkx9ig3ir8rqqcjbbhyq7idj1pxflbr5xk444syi72hw") (y #t)))

(define-public crate-aptos-log-derive-0.1.5 (c (n "aptos-log-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yr5l25hajm75svrmb6ga0zli3blrv56awknf232p4pigkkblnhh") (y #t)))

(define-public crate-aptos-log-derive-0.1.6 (c (n "aptos-log-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x2x4g1d5kfv3kxzndrvhi6xkwayf32gnrrrh0qqncnqz8icpin8") (y #t)))

(define-public crate-aptos-log-derive-0.1.7 (c (n "aptos-log-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j0442zk3bbwq1pxk1jyxbzwcfy6yyjccpqpa3if61kyjdarvqgk") (y #t)))

(define-public crate-aptos-log-derive-0.2.1 (c (n "aptos-log-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ac1rbj0nssm1yg1mqz4i3yjn5hd2ab07k9s0rp6m14sjy637wiw") (y #t)))

(define-public crate-aptos-log-derive-0.2.2 (c (n "aptos-log-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08c12a03v64xadslb8ll76fvx2lv25a4xi4x68xpc4ks7lk02xmz") (y #t)))

(define-public crate-aptos-log-derive-0.2.6 (c (n "aptos-log-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17ahbj326ji78pnk78h7zhl48fqgpm7wa408srzp2n09srbzhg9f") (y #t)))

(define-public crate-aptos-log-derive-0.2.7 (c (n "aptos-log-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yxqx20l8j3ha85xgzz5v61db258jk3ppqfm6ajl6w0rd6mkyvdh") (y #t)))

