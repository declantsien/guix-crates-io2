(define-module (crates-io ap ip apipe) #:use-module (crates-io))

(define-public crate-apipe-0.1.0 (c (n "apipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0ijdza3k3gd2z8wav27d9bcbkj0374lnp677mrm4hv9nzyd7jvm6")))

(define-public crate-apipe-0.1.1 (c (n "apipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "04pn084p99s08ljq823zn8f9r0da0b0mijrkcn8a63lp2yj9b6kg")))

(define-public crate-apipe-0.2.0 (c (n "apipe") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1bba9fsrwc72rdah6zkcyf247ywshmn7vkar0zkkz83bsnik0025") (f (quote (("nodeps") ("default" "parser")))) (s 2) (e (quote (("parser" "dep:lazy_static" "dep:regex"))))))

