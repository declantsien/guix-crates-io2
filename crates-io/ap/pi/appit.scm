(define-module (crates-io ap pi appit) #:use-module (crates-io))

(define-public crate-appit-0.1.0 (c (n "appit") (v "0.1.0") (d (list (d (n "winit") (r "^0.29.3") (f (quote ("rwh_05"))) (k 0)))) (h "1xzfkqasfpzs6hd6bwavjr03yn0p62kjz6rl2vkr0pn0h66h0hy8") (f (quote (("x11" "winit/x11") ("wayland-dlopen" "winit/wayland-dlopen") ("wayland-csd-adwaita" "winit/wayland-csd-adwaita") ("wayland" "winit/wayland") ("default" "x11" "wayland" "wayland-dlopen"))))))

(define-public crate-appit-0.1.1 (c (n "appit") (v "0.1.1") (d (list (d (n "winit") (r "^0.29.3") (f (quote ("rwh_05"))) (k 0)))) (h "1q1ap44ils3cqlm7306z7sg6xfxn0al1dk2qizjcrmv9a4niyi73") (f (quote (("x11" "winit/x11") ("wayland-dlopen" "winit/wayland-dlopen") ("wayland-csd-adwaita" "winit/wayland-csd-adwaita") ("wayland" "winit/wayland") ("default" "x11" "wayland" "wayland-dlopen"))))))

(define-public crate-appit-0.2.0 (c (n "appit") (v "0.2.0") (d (list (d (n "winit") (r "^0.29.3") (f (quote ("rwh_05"))) (k 0)))) (h "16rcngxbxbwbd700iwdmlh01mnr693ralghzrq9glmgvfmv15x0r") (f (quote (("x11" "winit/x11") ("wayland-dlopen" "winit/wayland-dlopen") ("wayland-csd-adwaita" "winit/wayland-csd-adwaita") ("wayland" "winit/wayland") ("default" "x11" "wayland" "wayland-dlopen" "wayland-csd-adwaita"))))))

(define-public crate-appit-0.3.0 (c (n "appit") (v "0.3.0") (d (list (d (n "winit") (r "^0.30.0") (k 0)))) (h "0526vmk43iy0mik6c32f3cf62jlwjvvv9lnc8q3zi918lcxq8bfs") (f (quote (("x11" "winit/x11") ("wayland-dlopen" "winit/wayland-dlopen") ("wayland-csd-adwaita" "winit/wayland-csd-adwaita") ("wayland" "winit/wayland") ("rwh_06" "winit/rwh_06") ("rwh_05" "winit/rwh_05") ("default" "x11" "wayland" "wayland-dlopen" "wayland-csd-adwaita")))) (r "1.70.0")))

