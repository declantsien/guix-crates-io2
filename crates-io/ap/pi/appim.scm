(define-module (crates-io ap pi appim) #:use-module (crates-io))

(define-public crate-appim-1.0.0 (c (n "appim") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x8ylap93m46xif1izf8pm6gkr827ilk0i8bpsbkb5zwxbj1k8bs") (y #t)))

(define-public crate-appim-1.0.1 (c (n "appim") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01sq3qjrgnd8f4h13xhg6wadlv5db0kzwb0hxv3vwd7ckln4z717")))

(define-public crate-appim-1.0.2 (c (n "appim") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01sw23sy1d375ahzw4bv0j40zfpqapbri2nv2ck2bkswhf2ax6cy")))

(define-public crate-appim-1.0.3 (c (n "appim") (v "1.0.3") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02skz502vi1qxyi86dansfm9rgrkvvg5vivpr31p7nyninq9w38y")))

