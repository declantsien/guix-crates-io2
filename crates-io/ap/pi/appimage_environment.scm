(define-module (crates-io ap pi appimage_environment) #:use-module (crates-io))

(define-public crate-appimage_environment-0.1.2 (c (n "appimage_environment") (v "0.1.2") (d (list (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "phollaits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "158s4ri9j01brkqy2l9nqlpbpfh13fygcf3b5cpdkncnn1pd3x7g")))

(define-public crate-appimage_environment-0.2.0 (c (n "appimage_environment") (v "0.2.0") (d (list (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "phollaits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0klkxq09g04gvcqashp1llvxqz8ziyypbnhzg4ypib36iwfqx8m3")))

(define-public crate-appimage_environment-0.2.1 (c (n "appimage_environment") (v "0.2.1") (d (list (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "phollaits") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0lm2n5zlhd63p7dc1nac5xki25cdi5z9k96yrxdp9vvfa6byzz4y")))

