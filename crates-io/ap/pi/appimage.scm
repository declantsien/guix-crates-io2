(define-module (crates-io ap pi appimage) #:use-module (crates-io))

(define-public crate-appimage-0.1.0 (c (n "appimage") (v "0.1.0") (h "0c76rw2sis4irnns69brw1dlzb725ck7469795b637jig3bdxlwx")))

(define-public crate-appimage-0.2.0 (c (n "appimage") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "xcommon") (r "^0.1.0") (d #t) (k 0)))) (h "0a9pi076pgnhbhm3sdk4w0xckbbamy6wbgsvhfmlq613dr0419k7")))

(define-public crate-appimage-0.3.0 (c (n "appimage") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "xcommon") (r "^0.2.0") (d #t) (k 0)))) (h "1havxh824zl807l779kgpanq9rwvn4fz01gbhxizkz83va7zkc2k")))

(define-public crate-appimage-0.4.0 (c (n "appimage") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "xcommon") (r "^0.3.0") (d #t) (k 0)))) (h "080rddyj7sr0gl9pnzgynmsz8k611z4362b24w5c4ajap6k4syjn")))

