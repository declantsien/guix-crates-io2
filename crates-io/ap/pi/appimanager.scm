(define-module (crates-io ap pi appimanager) #:use-module (crates-io))

(define-public crate-appimanager-1.0.0 (c (n "appimanager") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ac2md16b3dldm79ia1srj0x2ag2jh1pk048lgi5cw8f1hb1c51s")))

(define-public crate-appimanager-1.0.1 (c (n "appimanager") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1289xyli1xggyxfzp846bw5iwnbwv603x3xgm7hg4mwrfzhqgc01")))

