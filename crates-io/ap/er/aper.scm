(define-module (crates-io ap er aper) #:use-module (crates-io))

(define-public crate-aper-0.0.1 (c (n "aper") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "04nbjs2mcfiv1siip5qwja5154ybcv7rn0l75iv7m60q3janb85h")))

(define-public crate-aper-0.0.2 (c (n "aper") (v "0.0.2") (d (list (d (n "aper_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fractional_index") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0wa46bkcwqxc0zzr13mgvf4jia15xrnbw80flwj8yb63aasa5ay9")))

(define-public crate-aper-0.1.3 (c (n "aper") (v "0.1.3") (d (list (d (n "aper_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "fractional_index") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "17xg4c3p42f6zsp30yjgcz7ykfxvgng86vc53fic8jq08534z26g")))

(define-public crate-aper-0.1.4 (c (n "aper") (v "0.1.4") (d (list (d (n "aper_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "fractional_index") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde" "v4" "js"))) (d #t) (k 0)))) (h "1d8vx19xnzryj65f55p1nk4mwb4mcx5di7kiz81bpky5fab2wfpg")))

(define-public crate-aper-0.2.0 (c (n "aper") (v "0.2.0") (d (list (d (n "aper_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "fractional_index") (r "^1.0.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4" "js"))) (d #t) (k 0)))) (h "16xvi4sii32yaxi70yarrqjgi21yav02qv2qicwz8q618vl2shdy")))

(define-public crate-aper-0.2.2 (c (n "aper") (v "0.2.2") (d (list (d (n "aper_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "fractional_index") (r "^1.0.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4" "js"))) (d #t) (k 0)))) (h "19w49j41fskbly4aszm352ykm55v5wm6p924k974hs16az99zg9d")))

(define-public crate-aper-0.3.0 (c (n "aper") (v "0.3.0") (d (list (d (n "aper_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "fractional_index") (r "^2.0.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4" "js"))) (d #t) (k 0)))) (h "0hjf71fbfdbh0w950mscrr72gycd267wfjbihg17wxh8nk7i7ana")))

