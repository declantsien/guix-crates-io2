(define-module (crates-io ap er aper_derive) #:use-module (crates-io))

(define-public crate-aper_derive-0.0.2 (c (n "aper_derive") (v "0.0.2") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1fj4vgw8i80ig1fb72pw0cgh6sa4kawdbn3isam8ips2vm2ayxbx")))

(define-public crate-aper_derive-0.1.3 (c (n "aper_derive") (v "0.1.3") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "06ln3xswynnsykxn0s6z8als76sx61n8js82jqs4mr3mqknziaxf")))

(define-public crate-aper_derive-0.1.4 (c (n "aper_derive") (v "0.1.4") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "020dkia8rwf513302kw2z7mwxzdyrffpn36lcj8bpasri81891p2")))

(define-public crate-aper_derive-0.2.0 (c (n "aper_derive") (v "0.2.0") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0wcdp1k77hmcgs0gamk73zryxwkhnw5rf7vdzxp5hwjhr4yb9h3r")))

(define-public crate-aper_derive-0.2.2 (c (n "aper_derive") (v "0.2.2") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0zjabzpksz32l9xzqg9132ys74ifkdccbcr6mbij12xifqjp7c3x")))

(define-public crate-aper_derive-0.3.0 (c (n "aper_derive") (v "0.3.0") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "146j3r1rs4iqnvwk1h3sxbdksr4233yikza2hzqv6zmgrcmw0cbg")))

