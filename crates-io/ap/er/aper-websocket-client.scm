(define-module (crates-io ap er aper-websocket-client) #:use-module (crates-io))

(define-public crate-aper-websocket-client-0.3.0 (c (n "aper-websocket-client") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "aper") (r "^0.3.0") (d #t) (k 0)) (d (n "aper-stateroom") (r "^0.3.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde" "wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("BinaryType" "WebSocket" "MessageEvent"))) (d #t) (k 0)))) (h "1j6mv596i3abn1xj73p9nswinw4pj63nyn8nq6mdv64slx4nb2as")))

