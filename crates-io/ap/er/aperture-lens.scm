(define-module (crates-io ap er aperture-lens) #:use-module (crates-io))

(define-public crate-aperture-lens-0.4.0 (c (n "aperture-lens") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethers") (r "^2") (f (quote ("default"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "1mmpaafjpiqq0jgfv5x9ml57ag9zna9mqcf18jvijbvzaapvawa4")))

(define-public crate-aperture-lens-0.4.1 (c (n "aperture-lens") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ethers") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1hkmfxhdn2wbsjsz3w1i12rrh98bp2866kys5vr7yhglk2cr1b6y")))

