(define-module (crates-io ap er aperature) #:use-module (crates-io))

(define-public crate-aperature-0.1.0 (c (n "aperature") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.6") (d #t) (k 1)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.17.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.6") (d #t) (k 1)) (d (n "tar") (r "^0.4.20") (d #t) (k 1)) (d (n "tensorflow") (r "^0.11.0") (d #t) (k 0)))) (h "064gx6giw27y6nswmn3n350s80v7garrps47fn8ygjycvlvwayim")))

(define-public crate-aperature-0.1.1 (c (n "aperature") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.6") (d #t) (k 1)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.17.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.6") (d #t) (k 1)) (d (n "tar") (r "^0.4.20") (d #t) (k 1)) (d (n "tensorflow") (r "^0.11.0") (d #t) (k 0)))) (h "1lvib58849dad5gc9cniy1cpz0ll5x8jbr0v6s4pmld9n4h5j0si")))

