(define-module (crates-io ap er aper-stateroom) #:use-module (crates-io))

(define-public crate-aper-stateroom-0.3.0 (c (n "aper-stateroom") (v "0.3.0") (d (list (d (n "aper") (r "^0.3.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "stateroom") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "02nk8hjcmdyyn5dwhs1scf1l0asyyjj75z4lgnz85sh9n7mlpcn3")))

