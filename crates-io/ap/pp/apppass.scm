(define-module (crates-io ap pp apppass) #:use-module (crates-io))

(define-public crate-apppass-0.1.0 (c (n "apppass") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hnbzrcq6h5224gg2cvjizf2761825h5pg6s8c9v6qdx8jg2jx3f") (y #t)))

(define-public crate-apppass-0.1.1 (c (n "apppass") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ycb6aw61wi9p3vyzzgac0nf6cl3vz9h1cl8ssj8dgigpw3q27wy") (y #t)))

(define-public crate-apppass-0.1.2 (c (n "apppass") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1br9zh8hgxiw9l8dgn6qcs545r2lhrrywj4zdavykz6n189x558p")))

