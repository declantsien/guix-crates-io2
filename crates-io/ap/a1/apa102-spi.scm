(define-module (crates-io ap a1 apa102-spi) #:use-module (crates-io))

(define-public crate-apa102-spi-0.1.0 (c (n "apa102-spi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.1") (d #t) (k 0)))) (h "1fxilyk1mgdv25y11g8apazjddc2446bpcczhciwnn2sqc2kl05g")))

(define-public crate-apa102-spi-0.2.0 (c (n "apa102-spi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "1mcg2l1669dbqkv1cgxacq0s5nnfpwjgw29lwbyavgnaadg30k14")))

(define-public crate-apa102-spi-0.2.1 (c (n "apa102-spi") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "12viz5hp6yp75qlwdmdy8xrfvb355fqksic6cwkf9cci3z84mdkx")))

(define-public crate-apa102-spi-0.3.0 (c (n "apa102-spi") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "04h1i11aypl2xrcv4y13cr4qrj92175p6kf8h7va4s0rdg7zyz5j")))

(define-public crate-apa102-spi-0.3.1 (c (n "apa102-spi") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "17dznj4plj66wmf9klfwn17jwd62wzk8bnq0a6k7jffkbzql7la9")))

(define-public crate-apa102-spi-0.3.2 (c (n "apa102-spi") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "18sp9i7r7cl4mfq3kajiz36b7al2j0nkkp72972mpbiazn8dcb6r")))

(define-public crate-apa102-spi-0.4.0 (c (n "apa102-spi") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.3") (d #t) (k 0)))) (h "04lbcd1pcqalib9vj8gaap7zln9v7p2dp27qyzmkyh9v5sqvsyvl")))

