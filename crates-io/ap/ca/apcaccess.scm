(define-module (crates-io ap ca apcaccess) #:use-module (crates-io))

(define-public crate-apcaccess-0.1.0 (c (n "apcaccess") (v "0.1.0") (h "0y9hyqgbqg25llykp9w09pmjylzhkk6cgn2wwzn4dgw2iy24pchv") (r "1.72")))

(define-public crate-apcaccess-0.1.1 (c (n "apcaccess") (v "0.1.1") (h "1vg564cxsmsgn1phrsym3zqi0p600dcsxq1qwm68995nx0w1yn3a") (r "1.72")))

(define-public crate-apcaccess-0.1.2 (c (n "apcaccess") (v "0.1.2") (h "053hxb594g83fwnjl8ggzr30pm0n7xsm5f8l9gfrgzszwhnqb6jp") (r "1.72")))

(define-public crate-apcaccess-0.1.3 (c (n "apcaccess") (v "0.1.3") (h "0rxfg08fldx423hfyxb24nq6z005z57lvq5zaw9gk1l9nz27ixar") (r "1.72")))

