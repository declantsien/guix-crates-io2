(define-module (crates-io ap ps appscraps_dynamic_event) #:use-module (crates-io))

(define-public crate-appscraps_dynamic_event-0.1.0 (c (n "appscraps_dynamic_event") (v "0.1.0") (d (list (d (n "appscraps_module") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1x730zaw077q6g70dc54y1mws75hqr01h90jss1zip1s5hn0kz8n") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

