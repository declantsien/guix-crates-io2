(define-module (crates-io ap ps appscraps_dll) #:use-module (crates-io))

(define-public crate-appscraps_dll-0.1.0 (c (n "appscraps_dll") (v "0.1.0") (d (list (d (n "appscraps_dll_error") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_module") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sharedlib") (r "^6") (d #t) (k 0)))) (h "0g56zwv83bnfp9dmbvlgzr0cbcm9lyl8r15v3ki3yngvhpjaycha") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

