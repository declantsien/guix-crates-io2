(define-module (crates-io ap ps appscraps_module) #:use-module (crates-io))

(define-public crate-appscraps_module-0.1.0 (c (n "appscraps_module") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0xsi5rkkcwa69qfaqfhbq78hid1pj86snl7h7gk9vb3qr4assc7c") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

