(define-module (crates-io ap ps appscraps_static) #:use-module (crates-io))

(define-public crate-appscraps_static-0.1.0 (c (n "appscraps_static") (v "0.1.0") (d (list (d (n "appscraps_environment") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_static_util") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0hj18lw198m53nyakq9jxy4icgf2sgs4gg35c60jq77pyzw2s2sk")))

