(define-module (crates-io ap ps appscraps_environment) #:use-module (crates-io))

(define-public crate-appscraps_environment-0.1.0 (c (n "appscraps_environment") (v "0.1.0") (d (list (d (n "appscraps_dll") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_event") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_module") (r "^0.1") (d #t) (k 0)) (d (n "simplisp") (r "^0.4") (d #t) (k 0)))) (h "05svgb53v596fnzsq6n3xv4aisp1qnbcqh84fdp2ys9dx9x7zgz3")))

