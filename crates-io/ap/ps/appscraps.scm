(define-module (crates-io ap ps appscraps) #:use-module (crates-io))

(define-public crate-appscraps-0.1.0 (c (n "appscraps") (v "0.1.0") (d (list (d (n "appscraps_dll") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_environment") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_event") (r "^0.1") (d #t) (k 0)) (d (n "appscraps_module") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "simplisp") (r "^0.4") (d #t) (k 0)) (d (n "simplisp_extensions") (r "^0.4") (d #t) (k 0)))) (h "1azfbsipnksmwkz4za2668ilwpglq66yk8my9ryz9qcwbkpl7d9a")))

