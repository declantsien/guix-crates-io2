(define-module (crates-io ap ye apyee-macros) #:use-module (crates-io))

(define-public crate-apyee-macros-0.6.0 (c (n "apyee-macros") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "16jxn5h2v3a94zx1j5c45bw6r0wi7wr60ix8pgca8ccffbhghss8")))

