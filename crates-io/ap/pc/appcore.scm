(define-module (crates-io ap pc appcore) #:use-module (crates-io))

(define-public crate-appcore-0.0.1 (c (n "appcore") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1865c533l56j8msrqi6g5kmd69jd6m7r5zdwzknzr679kahdqhcz") (f (quote (("orm") ("defaults" "orm")))) (y #t)))

(define-public crate-appcore-0.0.2 (c (n "appcore") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "0lj86fahzlgqna11lzg572g3zhyznray5hsxzny3xwjgcxlh86vj") (f (quote (("orm" "time") ("defaults" "orm"))))))

