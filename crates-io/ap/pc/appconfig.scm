(define-module (crates-io ap pc appconfig) #:use-module (crates-io))

(define-public crate-appconfig-0.1.0 (c (n "appconfig") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "18kyfrdv6qax8r4ki5mlfxp9z5hp6gwf90z94y0f1wcv6mz0iq19")))

(define-public crate-appconfig-0.1.1 (c (n "appconfig") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xb3yd6hxm4zpl650sfgy0yxdidfa8qq7qwn3cpc4mv6gsn18bbb")))

(define-public crate-appconfig-0.1.2 (c (n "appconfig") (v "0.1.2") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xvhv9drij0c3d1caf9cpp8xy9gn1fb6ff9nx9gdr9f5cgn10qqi")))

(define-public crate-appconfig-0.1.3 (c (n "appconfig") (v "0.1.3") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qam7jbw2i2zbgjkivffq9h377r22wy0rjl2nv1snvzmfh9b896g")))

(define-public crate-appconfig-0.1.4 (c (n "appconfig") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0innzb7rq31w7n35iqldyq2521632f2kfckh8dnfvzbdrn8s38kv")))

(define-public crate-appconfig-0.2.0 (c (n "appconfig") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1zm3480jg7ssmvxn0dn1j8mxgshzzbznx4mngvj1nchlbzh5xrfd")))

(define-public crate-appconfig-0.2.1 (c (n "appconfig") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "13gxacacxkzd7r6hamlxw9yfy81iwysyn7nwmh3c21l39315055p")))

