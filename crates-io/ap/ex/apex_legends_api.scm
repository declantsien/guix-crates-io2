(define-module (crates-io ap ex apex_legends_api) #:use-module (crates-io))

(define-public crate-apex_legends_api-0.1.4 (c (n "apex_legends_api") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f0kfvb1i60kjv2gf1i33b2zmfkgz4z2qcw37yd9h2bskpqb0djp")))

(define-public crate-apex_legends_api-0.1.5 (c (n "apex_legends_api") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0drqhfqw0vcy0vg47s16scr0a397lky0ij6sw9w8jviccjdds4a0")))

