(define-module (crates-io ap ex apex) #:use-module (crates-io))

(define-public crate-apex-0.1.0 (c (n "apex") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "1hippxlh9h5slg67vzkf2fvk6gmk178r9r66lyx6qgj8mzzvasq0")))

(define-public crate-apex-0.1.1 (c (n "apex") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "0x6s5gxaczkljvkyxgkgzrmx0ih2hljdc2pk32xg0h6w2vk1g4xp")))

(define-public crate-apex-0.1.2 (c (n "apex") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)))) (h "175zzadd00cp48blk8y8a2jnj78alzrk6zizll4fhh2mapskwqhr")))

