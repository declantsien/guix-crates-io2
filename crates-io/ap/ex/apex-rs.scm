(define-module (crates-io ap ex apex-rs) #:use-module (crates-io))

(define-public crate-apex-rs-0.1.0 (c (n "apex-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1yn56vys9y045dgyakkpkjg0cnc30ywxqbwinx2zp4kjgxikasic")))

