(define-module (crates-io ap ex apex_legends) #:use-module (crates-io))

(define-public crate-apex_legends-0.1.0 (c (n "apex_legends") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0224lf9dsz3yqm5a0v63ijqrh02ghdxn4q65p9620nqb9nsk9cbh")))

(define-public crate-apex_legends-0.1.1 (c (n "apex_legends") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07qvkscighji7wd6jvak56c6dp2qq6v0k7xa2zapy0xxhxxdg1mv")))

(define-public crate-apex_legends-0.1.2 (c (n "apex_legends") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0akyiwmwy0m5i4dbmk5932pp0f4j2mh4zicmyamhx2p59lff5jzp")))

