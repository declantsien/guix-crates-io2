(define-module (crates-io ap ub apub-ingest) #:use-module (crates-io))

(define-public crate-apub-ingest-0.1.0 (c (n "apub-ingest") (v "0.1.0") (d (list (d (n "apub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "01bjqd50vwmc9wp7q7qxi5aqyhvl63jv3k1qlvfy64fy5w5z7fah")))

(define-public crate-apub-ingest-0.2.0 (c (n "apub-ingest") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0bsdarvvxpygv6f1kiik3jsihk109p6nbc0f7m6n56wzajcv41kr")))

