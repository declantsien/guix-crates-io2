(define-module (crates-io ap ub apub-simple-activitypub) #:use-module (crates-io))

(define-public crate-apub-simple-activitypub-0.1.0 (c (n "apub-simple-activitypub") (v "0.1.0") (d (list (d (n "apub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "apub-publickey") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0x6jc7pzdp40vjwac15ci2101h47x1amx7w83iyz8r7v9iz1qkzr")))

(define-public crate-apub-simple-activitypub-0.2.0 (c (n "apub-simple-activitypub") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "apub-publickey") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "15005qdyp09zmgd1mp9vk4y77899vqw65fs90iqbd9qvzfz6r9yr")))

