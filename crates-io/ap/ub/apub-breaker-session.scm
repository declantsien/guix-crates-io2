(define-module (crates-io ap ub apub-breaker-session) #:use-module (crates-io))

(define-public crate-apub-breaker-session-0.1.0 (c (n "apub-breaker-session") (v "0.1.0") (d (list (d (n "apub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0mwhvjh5zpnhspc5ybwb1nrm4kpvyyqbgzd72whlr0rsq47vmf9f")))

(define-public crate-apub-breaker-session-0.2.0 (c (n "apub-breaker-session") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gngbk40vidz6x813rxbg3i9jsjf60qbifxvc0miy2jipvawj5ca")))

