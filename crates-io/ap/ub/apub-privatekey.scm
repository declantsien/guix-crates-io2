(define-module (crates-io ap ub apub-privatekey) #:use-module (crates-io))

(define-public crate-apub-privatekey-0.1.0 (c (n "apub-privatekey") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ks5yqfhjkwsgwq8x1sxbp4grmhd37p10y906925j7mzf3jslcf9")))

(define-public crate-apub-privatekey-0.2.0 (c (n "apub-privatekey") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "176h72pdmjvk9qzq8gxvq0fllv95m5gs8j6fpwbfcd6q44rj5nhj")))

