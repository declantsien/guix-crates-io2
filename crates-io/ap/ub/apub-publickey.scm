(define-module (crates-io ap ub apub-publickey) #:use-module (crates-io))

(define-public crate-apub-publickey-0.1.0 (c (n "apub-publickey") (v "0.1.0") (d (list (d (n "apub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "apub-privatekey") (r "^0.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0wza62vh57zb3vc8cdy0mzg8lg1vc4ay1lr7870k19p8fqlmcwaw")))

(define-public crate-apub-publickey-0.2.0 (c (n "apub-publickey") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "apub-privatekey") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "16r662lklha9kva8bj7z0mprhcv3ibn0a6lpv1wdpfm7pyyman0y")))

