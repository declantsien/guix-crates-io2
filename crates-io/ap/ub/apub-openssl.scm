(define-module (crates-io ap ub apub-openssl) #:use-module (crates-io))

(define-public crate-apub-openssl-0.1.0 (c (n "apub-openssl") (v "0.1.0") (d (list (d (n "apub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09scwk58rshwsbp3m5zlmyyg2p9la2i3iyxgb99d1gx5mci124ms")))

(define-public crate-apub-openssl-0.2.0 (c (n "apub-openssl") (v "0.2.0") (d (list (d (n "apub-core") (r "^0.2.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rrvlxxfykpwpxwjaq3jf1xnwv7g6zx1xzvp5ip1a05hl7116ikf")))

