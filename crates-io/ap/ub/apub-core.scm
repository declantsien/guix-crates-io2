(define-module (crates-io ap ub apub-core) #:use-module (crates-io))

(define-public crate-apub-core-0.1.0 (c (n "apub-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "12fr9wcb3q5fhcjks9vk9amk2y230pjbryzh0j10xfz4vlym8vs4")))

(define-public crate-apub-core-0.2.0 (c (n "apub-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1ryhi5a0i84211x1g6gl557xhc2x6sh3z9iv52pvnp4nw1paj83g")))

