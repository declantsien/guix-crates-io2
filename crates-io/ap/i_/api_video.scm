(define-module (crates-io ap i_ api_video) #:use-module (crates-io))

(define-public crate-api_video-0.1.0 (c (n "api_video") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("stream" "multipart" "json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("codec"))) (d #t) (k 0)))) (h "1xlxrki8pj14ssp4niwdhd7cxnp5yhiaqzljig7blqzzrq4qz2c4")))

