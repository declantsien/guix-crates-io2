(define-module (crates-io ap i_ api_kit) #:use-module (crates-io))

(define-public crate-api_kit-0.1.0 (c (n "api_kit") (v "0.1.0") (d (list (d (n "error") (r "^0.1.9") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1ixa5x7l1qvxn2x7cyxsylv0gqa9bryl3s4s2qy3vrr64az0y1sz")))

(define-public crate-api_kit-0.2.0 (c (n "api_kit") (v "0.2.0") (d (list (d (n "error") (r "^0.1.9") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0904a43y5w8bcdx1j6x36zi4h3kzn69hxbkpl2kj66lyssh4h261")))

