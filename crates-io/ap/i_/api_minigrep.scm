(define-module (crates-io ap i_ api_minigrep) #:use-module (crates-io))

(define-public crate-api_minigrep-0.1.0 (c (n "api_minigrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1v15a7217p54ysp2nbriclajc4r68rwphnd2zxyw5z7fc86frglx")))

(define-public crate-api_minigrep-0.1.1 (c (n "api_minigrep") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1x5wjp1x85nl777nvancjrb74hcv2xn0qfg7q6pxk2q9420jcpx3")))

