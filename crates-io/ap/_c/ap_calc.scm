(define-module (crates-io ap _c ap_calc) #:use-module (crates-io))

(define-public crate-ap_calc-0.1.0 (c (n "ap_calc") (v "0.1.0") (d (list (d (n "fraction") (r "^0.12.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "09cla8hafj1xak9w4wxxfj2d1i3alzjvhw0h79l6zl3nm5sv4929")))

(define-public crate-ap_calc-0.1.1 (c (n "ap_calc") (v "0.1.1") (d (list (d (n "fraction") (r "^0.12.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "12rpa6dlxr31w70z48lbbya0kappkvg3nw42vh795wlwhlsh1w7d")))

