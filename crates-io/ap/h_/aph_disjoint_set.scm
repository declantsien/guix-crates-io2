(define-module (crates-io ap h_ aph_disjoint_set) #:use-module (crates-io))

(define-public crate-aph_disjoint_set-0.1.0 (c (n "aph_disjoint_set") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (k 2)))) (h "198ff9ghdryv53hjhrw19bizsbi1c2899qyrml5x0h4wk6gy2fx4") (r "1.70")))

(define-public crate-aph_disjoint_set-0.1.1 (c (n "aph_disjoint_set") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (k 2)))) (h "023wjdk07c4chsb4hlkjcil1ira4q5vkn6xdmr25prl3b1y6x4gc") (r "1.70")))

