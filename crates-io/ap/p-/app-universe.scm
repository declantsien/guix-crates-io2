(define-module (crates-io ap p- app-universe) #:use-module (crates-io))

(define-public crate-app-universe-0.1.0 (c (n "app-universe") (v "0.1.0") (h "07w6fcxgz48ysggxarh15sx1wx5yrpdkh9d70d8k9m8j130694wp") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-0.1.1 (c (n "app-universe") (v "0.1.1") (h "1aypwm2dgirxbzc0ks91sa5wcnbyk5kslr1khw1lv03kn0piq1zk") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-0.1.2 (c (n "app-universe") (v "0.1.2") (h "1dsc0d8mp91xxzhzyzbb2y4rfbwipjgw4r1vnfr2661817y79zz3") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-0.1.3 (c (n "app-universe") (v "0.1.3") (h "0ir227ipdkcjhnnsfkqig9gd7lnsx53jzv7lz7kg0kmcgyp7kj6n") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-0.2.0 (c (n "app-universe") (v "0.2.0") (h "124l2z4n4f066h60vg7fgdzb43smxbal8gzb9wdbbj85m3lakxvm") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-0.2.1 (c (n "app-universe") (v "0.2.1") (h "1a0gg4pxpmk3dr53amzan0w1bb7d6y8ji6rwcqw1im8iq6b19xmi") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-universe-1.0.0 (c (n "app-universe") (v "1.0.0") (h "1jr1wy8wmxxd6c1kbik8q9r9kidhb9zv6kr25bfw812r3b8fxg7v") (f (quote (("test-utils"))))))

