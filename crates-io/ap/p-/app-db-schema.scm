(define-module (crates-io ap p- app-db-schema) #:use-module (crates-io))

(define-public crate-app-db-schema-0.1.0 (c (n "app-db-schema") (v "0.1.0") (d (list (d (n "db_dep") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "surrealdb-obj-derive") (r "^0.1.0") (d #t) (k 0)))) (h "114vsy175qnhc8kw8yhkgqj5nmdi1vbhzi9r54vzapmpsg85vaih")))

(define-public crate-app-db-schema-0.1.1 (c (n "app-db-schema") (v "0.1.1") (d (list (d (n "db_dep") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "surrealdb-obj-derive") (r "^0.1.0") (d #t) (k 0)))) (h "16bp8c1xdv6ssyzm9bs5kj0qnkabw8cqlv0iz6d7v3j26sybxqql") (f (quote (("test" "db_dep/test"))))))

(define-public crate-app-db-schema-0.1.2 (c (n "app-db-schema") (v "0.1.2") (d (list (d (n "db_dep") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("filters"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "surrealdb-obj-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0qlz0mkyln9iq283s49f77ibqq1vnqvn9bsmaby346sz3anv9bs6") (f (quote (("test" "db_dep/test"))))))

