(define-module (crates-io ap p- app-world) #:use-module (crates-io))

(define-public crate-app-world-0.1.0 (c (n "app-world") (v "0.1.0") (h "1pnllmnasxrkd99q1vm37mn0k4c97h2lxw5hndrkd65g2y26c73i")))

(define-public crate-app-world-0.1.1 (c (n "app-world") (v "0.1.1") (h "1zmcwys1b9awjhl9s60zp05mp1s5jripbv8g8ywh1m8dn4y9m98m") (f (quote (("send"))))))

(define-public crate-app-world-0.1.3 (c (n "app-world") (v "0.1.3") (h "0mb74swhh62a6bfj2jmm00rykyk3plisy54mc816xpyx9yav4mbb") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.1.4 (c (n "app-world") (v "0.1.4") (h "0m5hz93rly6bhn7vw1d2shb7iall7gp0mnybq25hswwgcsqcc16x") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.1.5 (c (n "app-world") (v "0.1.5") (h "0kjl87vhhv1f1fipf3yslszgnxaz519hfz4g6xlrfmqifvv24kwb") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.1.6 (c (n "app-world") (v "0.1.6") (h "00crl5jkkiqa1sl8yfcqa4wivb1y3nxg3lp9kkzwqh95f9pl3v2x") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.1.7 (c (n "app-world") (v "0.1.7") (h "1l9ifslylngk3dgjr8qkw83zr50snm6fp663kr4nwz1nknnqxwxr") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.1.8 (c (n "app-world") (v "0.1.8") (h "1s8v7pklpzwb22638abdqpz13r8kw409vl2p9cqx15kbmh88p4jd") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.2.0 (c (n "app-world") (v "0.2.0") (h "03pqpwzpdbk87wcdk1blzlzgd8icyq3129iaq5pypiafhd43kw68") (f (quote (("test-utils") ("send"))))))

(define-public crate-app-world-0.3.0 (c (n "app-world") (v "0.3.0") (h "03wky45kmpgcd1fgllwp9k7rl16mwrscy47ximb9s87dhkhxm8fg") (f (quote (("test-utils"))))))

