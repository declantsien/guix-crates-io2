(define-module (crates-io ap p- app-state) #:use-module (crates-io))

(define-public crate-app-state-0.1.0 (c (n "app-state") (v "0.1.0") (d (list (d (n "app-state-macros") (r "^0") (d #t) (k 0)) (d (n "ctor") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dwridv6bfyk5vb9sqpp2dqv130s2fbfqfc70224996g3cfbkqlw") (s 2) (e (quote (("log" "app-state-macros/log" "dep:log"))))))

