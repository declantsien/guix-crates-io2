(define-module (crates-io ap p- app-machine-id) #:use-module (crates-io))

(define-public crate-app-machine-id-0.1.0 (c (n "app-machine-id") (v "0.1.0") (d (list (d (n "hmac-sha256") (r "^1.1.6") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "0w0amcfnfzx3j1af59sa4mwwrwd6p7fzay3pfh1n7zg5jpvrgg47")))

