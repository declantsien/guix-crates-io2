(define-module (crates-io ap p- app-state-macros) #:use-module (crates-io))

(define-public crate-app-state-macros-0.1.0 (c (n "app-state-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("fold" "full" "extra-traits"))) (d #t) (k 0)))) (h "02c5jmw42j0k93kfzvls5c2vj1d63a10w84lahcwrc5gpjx47j0k") (f (quote (("log"))))))

