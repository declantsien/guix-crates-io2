(define-module (crates-io ap p- app-fun) #:use-module (crates-io))

(define-public crate-app-fun-0.3.1 (c (n "app-fun") (v "0.3.1") (h "0g5ywcm0ci5piy060w7vg8jf13c5fjbc15n1v4wwg0ad3xji9fx2")))

(define-public crate-app-fun-0.3.2 (c (n "app-fun") (v "0.3.2") (h "0fvv23qf356bbad17plb1si007jrzlgy2siwyr0sr5iyq11wvf1w")))

