(define-module (crates-io ap es apes) #:use-module (crates-io))

(define-public crate-apes-0.1.0 (c (n "apes") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.1.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "0ppmnr7lmgsqx3mngrb01smcqd9jbjk00b5nr5ax2vpswfbjncb9")))

(define-public crate-apes-0.1.1 (c (n "apes") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.1.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "124pwvqg3y76z6p8cbmiaq1jywiaa0msmcvwp7gjp83a228iz8bj")))

(define-public crate-apes-0.1.2 (c (n "apes") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.1.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "1h2q8w0l1d8h8fp1lvydlxixgpbwb3hw1vgssm3pbbvwh0ga4kva")))

(define-public crate-apes-0.1.3 (c (n "apes") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.1.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "06six3yphlmk9mwxnhjlhn38zjg4lv1lwnnwf43r0wh8i40qqym5")))

(define-public crate-apes-0.1.4 (c (n "apes") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "1a7m0zxx9gkhl5sc0w6y4skagqbmmijx61sz36a2ygfb9rqbi0zi")))

(define-public crate-apes-0.1.5 (c (n "apes") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "1r8v18ina7yzpc80raxx4nl4gapkqis3vq3grzdwzh01rjrlimsw")))

(define-public crate-apes-0.1.6 (c (n "apes") (v "0.1.6") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "0a33gvhwssx0wpzl7rvsaxmzj6x0a622jaxc6j3k3vax07lx2bnx")))

(define-public crate-apes-0.1.7 (c (n "apes") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "0lyzj8s9ipr0rrk55brirm8kzb9y4p3jq9sczix6m4kc4kab6q8q")))

