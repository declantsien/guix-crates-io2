(define-module (crates-io ap ol apollon) #:use-module (crates-io))

(define-public crate-apollon-0.0.2 (c (n "apollon") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0761jz7psnvsr86ii078zxcdr9s0lr915yd0nb0vckznm3zp14dq")))

(define-public crate-apollon-0.0.21 (c (n "apollon") (v "0.0.21") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "13rcg2nda4s2nsskx0m1p1nlwaspv3mj96b9fqvx7svx9zpyqr3a")))

(define-public crate-apollon-0.0.3 (c (n "apollon") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0phw8infh0zml15avpgr1bypb36vkjbqh7jy7g9q13sh6mh976fx")))

(define-public crate-apollon-0.1.0 (c (n "apollon") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wvlfzm2i6kb41h068kw1xdl8hiaw22fh278nsk4rc32vn2zkmvm")))

