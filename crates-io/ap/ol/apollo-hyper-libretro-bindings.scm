(define-module (crates-io ap ol apollo-hyper-libretro-bindings) #:use-module (crates-io))

(define-public crate-apollo-hyper-libretro-bindings-0.1.0 (c (n "apollo-hyper-libretro-bindings") (v "0.1.0") (d (list (d (n "apollo-hyper-api-standard") (r "^0.1.0") (d #t) (k 0)) (d (n "apollo-hyper-libretro-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "printf-compat") (r "^0.1.1") (d #t) (k 0)))) (h "03xjkc1isdsb7fz99hcnynbvlc2x2xz279yx26jcy9n3h296v5ql")))

(define-public crate-apollo-hyper-libretro-bindings-0.1.1 (c (n "apollo-hyper-libretro-bindings") (v "0.1.1") (d (list (d (n "apollo-hyper-api-standard") (r "^0.1.1") (d #t) (k 0)) (d (n "apollo-hyper-libretro-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "printf-compat") (r "^0.1.1") (d #t) (k 0)))) (h "02hdmhi7ijy0vxzfs6rbqvrfrk1q6h8286xz6ffx4amycpdfppns")))

