(define-module (crates-io ap ol apollo-supergraph-config) #:use-module (crates-io))

(define-public crate-apollo-supergraph-config-0.1.0 (c (n "apollo-supergraph-config") (v "0.1.0") (d (list (d (n "apollo-federation-types") (r "^0.1") (d #t) (k 0)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "11q2frxaizrkhcj3dlka0zlspbd4haflyyd3al8crc4vrvf9laka")))

