(define-module (crates-io ap ol apollo-ariadne) #:use-module (crates-io))

(define-public crate-apollo-ariadne-0.2.0-alpha.0 (c (n "apollo-ariadne") (v "0.2.0-alpha.0") (d (list (d (n "concolor") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "02jgd7kfjdqzh9ycnqd4hxbvmff9n4ngrbndfl668fvmj6a5kl18") (f (quote (("auto-color" "concolor" "concolor/auto"))))))

