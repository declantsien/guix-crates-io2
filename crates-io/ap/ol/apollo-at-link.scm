(define-module (crates-io ap ol apollo-at-link) #:use-module (crates-io))

(define-public crate-apollo-at-link-0.0.1 (c (n "apollo-at-link") (v "0.0.1") (d (list (d (n "apollo-compiler") (r "=1.0.0-beta.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "salsa") (r "^0.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "00ninh61i3sklgk49nkx57shacy1kw3ly2130xfw2fq5xkrd6kv6")))

