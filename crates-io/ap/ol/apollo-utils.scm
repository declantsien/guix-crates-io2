(define-module (crates-io ap ol apollo-utils) #:use-module (crates-io))

(define-public crate-apollo-utils-0.1.0 (c (n "apollo-utils") (v "0.1.0") (d (list (d (n "apollo-cw-asset") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1vz5jgz24bqz00nlmpfw1kfqqbhc970kc700dr8naz106gkksd5m")))

(define-public crate-apollo-utils-0.1.1 (c (n "apollo-utils") (v "0.1.1") (d (list (d (n "apollo-cw-asset") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1hn245lwzh19018smpqvmjkhl7wz3jx0k3q0b4j8synz556yy1ab")))

(define-public crate-apollo-utils-0.1.2-rc.1 (c (n "apollo-utils") (v "0.1.2-rc.1") (d (list (d (n "apollo-cw-asset") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1pvgsa536ca18ljqaf6jgl139gmzx7ds83i5xbs3y4l72s13sgnf")))

(define-public crate-apollo-utils-0.1.2 (c (n "apollo-utils") (v "0.1.2") (d (list (d (n "apollo-cw-asset") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1v02597wa8mjlvjx6pdgc4d8ix66mzps4g3vm1ijv0vqqfjb2sg6")))

