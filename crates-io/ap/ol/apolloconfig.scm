(define-module (crates-io ap ol apolloconfig) #:use-module (crates-io))

(define-public crate-apolloconfig-0.0.0 (c (n "apolloconfig") (v "0.0.0") (d (list (d (n "apolloconfig-sig") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (k 0)))) (h "16wv1zjdd7i3ijiq7s3r917i8ydl8hndhp97yl8y3sgyn63sxz1s")))

(define-public crate-apolloconfig-0.1.0 (c (n "apolloconfig") (v "0.1.0") (d (list (d (n "apolloconfig-sig") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (k 0)))) (h "03i60cvpxwbmywmx8zbrkrglxn92pymv3329s3zqs2g5kbf39h4y")))

