(define-module (crates-io ap ol apollo-hyper-api-interface) #:use-module (crates-io))

(define-public crate-apollo-hyper-api-interface-0.1.0 (c (n "apollo-hyper-api-interface") (v "0.1.0") (d (list (d (n "apollo-hyper-libretro-bindings") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1ypzbdz9d2l0a6pb9fairdsv93id9pi6pnbiidfz9jz2q22xbwwm") (f (quote (("default" "libretro")))) (s 2) (e (quote (("libretro" "dep:apollo-hyper-libretro-bindings"))))))

(define-public crate-apollo-hyper-api-interface-0.1.1 (c (n "apollo-hyper-api-interface") (v "0.1.1") (d (list (d (n "apollo-hyper-libretro-bindings") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "193rq0sip268cmda37n2430adcgcaxwgrg0cyj4655pb1c6rcfwv") (f (quote (("default" "libretro")))) (s 2) (e (quote (("libretro" "dep:apollo-hyper-libretro-bindings"))))))

