(define-module (crates-io ap ol apollo-subgraph) #:use-module (crates-io))

(define-public crate-apollo-subgraph-0.0.1 (c (n "apollo-subgraph") (v "0.0.1") (d (list (d (n "apollo-at-link") (r "^0.0.1") (d #t) (k 0)) (d (n "apollo-compiler") (r "=1.0.0-beta.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "salsa") (r "^0.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w8y4rq1vbgw0hr0cwrr4rm2pbvi3inic6kb67sfbw5zr4h8m25i")))

