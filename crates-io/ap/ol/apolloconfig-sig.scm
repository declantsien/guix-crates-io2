(define-module (crates-io ap ol apolloconfig-sig) #:use-module (crates-io))

(define-public crate-apolloconfig-sig-0.0.0 (c (n "apolloconfig-sig") (v "0.0.0") (h "19mm79255y9yswk3xy6j8jszym345fgc0ihclnh3lracd5wlxawj")))

(define-public crate-apolloconfig-sig-0.1.0 (c (n "apolloconfig-sig") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (f (quote ("std"))) (k 0)) (d (n "error-macro") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "hmac") (r "^0.12") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "sha1") (r "^0.10") (k 0)))) (h "1mj9bq600l4q9k1p8f0qbvw7s7l290b7bi8ghbxiqdfdpfpzsx5d")))

