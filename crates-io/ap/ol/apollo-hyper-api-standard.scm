(define-module (crates-io ap ol apollo-hyper-api-standard) #:use-module (crates-io))

(define-public crate-apollo-hyper-api-standard-0.1.0 (c (n "apollo-hyper-api-standard") (v "0.1.0") (h "1qndhx3ndn5wv6y65i0q0xxg2laa3vdh0f2vzxj0wi6v7vdh3jax")))

(define-public crate-apollo-hyper-api-standard-0.1.1 (c (n "apollo-hyper-api-standard") (v "0.1.1") (h "0yj2nrfk73lj920pdgy4vmsizl3bl5iza7jdzsash5561h1y4ang")))

