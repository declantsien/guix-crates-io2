(define-module (crates-io ap ol apollo) #:use-module (crates-io))

(define-public crate-apollo-0.0.1 (c (n "apollo") (v "0.0.1") (h "16iihd3sg64zsccvk2xsidh7wb3hd17rzh4fb1hf17gc3j37x2vw")))

(define-public crate-apollo-0.0.2 (c (n "apollo") (v "0.0.2") (h "0jg8f4bj4kp23j6jcs0pmcq031svp4qyjyq0p2xz47i4yl1zapc5")))

