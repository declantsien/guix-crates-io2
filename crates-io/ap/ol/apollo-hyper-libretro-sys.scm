(define-module (crates-io ap ol apollo-hyper-libretro-sys) #:use-module (crates-io))

(define-public crate-apollo-hyper-libretro-sys-0.1.1 (c (n "apollo-hyper-libretro-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1scbcirdk0rs5xivy81gcbh1g0r8iipgn68qaqrx4d783l71qs02")))

(define-public crate-apollo-hyper-libretro-sys-0.1.2 (c (n "apollo-hyper-libretro-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "164dqq8404h73xwn4xp8cwf3sqmfqsk5b60nr0jrg5h246ylfyg3")))

