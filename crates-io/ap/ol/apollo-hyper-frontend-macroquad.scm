(define-module (crates-io ap ol apollo-hyper-frontend-macroquad) #:use-module (crates-io))

(define-public crate-apollo-hyper-frontend-macroquad-0.1.0 (c (n "apollo-hyper-frontend-macroquad") (v "0.1.0") (d (list (d (n "apollo-hyper-api-interface") (r "^0.1.0") (d #t) (k 0)))) (h "1mf2p40ccs73c2xb0qn1b08ds6m2d71mzyj00ngfvbcp3qzk1gzl")))

(define-public crate-apollo-hyper-frontend-macroquad-0.1.1 (c (n "apollo-hyper-frontend-macroquad") (v "0.1.1") (d (list (d (n "apollo-hyper-api-interface") (r "^0.1.1") (d #t) (k 0)))) (h "0hv5ckp138wx3fjdlvc2khvff37lmph3rwqyp2yg8dv52jswp3rx")))

