(define-module (crates-io ap ol apollo-hyper-libretro-build) #:use-module (crates-io))

(define-public crate-apollo-hyper-libretro-build-0.1.0 (c (n "apollo-hyper-libretro-build") (v "0.1.0") (d (list (d (n "ci_info") (r "^0.14.9") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "13s7kcn0700zwxjpyfcc68ar3rlyppchdrnhzdg226ps7hna8dg4")))

