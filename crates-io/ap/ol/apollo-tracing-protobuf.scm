(define-module (crates-io ap ol apollo-tracing-protobuf) #:use-module (crates-io))

(define-public crate-apollo-tracing-protobuf-0.1.0 (c (n "apollo-tracing-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.18.0") (d #t) (k 0)))) (h "1ipagf64nwn1r4aadd8j43pgqxgiignmaw41pxrwkkk54db7b0nc")))

