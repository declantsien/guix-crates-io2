(define-module (crates-io ap ol apollo-rs) #:use-module (crates-io))

(define-public crate-apollo-rs-0.1.0 (c (n "apollo-rs") (v "0.1.0") (h "1ljzag2zr6zwqzgfz37nv88zixb4a4hkz8mgkgazl797lkgkf9bw") (y #t)))

(define-public crate-apollo-rs-0.1.1 (c (n "apollo-rs") (v "0.1.1") (h "0sfy6jmd72miaqbnbw9jg9ab4zk803vkjr2r7wh9k64j03mj7c6f")))

(define-public crate-apollo-rs-0.1.2 (c (n "apollo-rs") (v "0.1.2") (h "07kdwja10jva5nbq60rzdhgv0qbplaid59x9w74s3fcrp8k446lx")))

