(define-module (crates-io ap ol apollo-encoder) #:use-module (crates-io))

(define-public crate-apollo-encoder-0.1.0 (c (n "apollo-encoder") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "0cd6vn2ja93mblinx4x0aaw8mrmlvikvijc4799xvhb2490rrnn1")))

(define-public crate-apollo-encoder-0.2.0 (c (n "apollo-encoder") (v "0.2.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "10y7iizwgmh90d7qwfy0n86icxzi5dkypsrlxyna9blza1sl7l2k")))

(define-public crate-apollo-encoder-0.2.1 (c (n "apollo-encoder") (v "0.2.1") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "172qzjlkpa0d3vph4752mrbwqdhay2jh8vysxs4hb2k79ch5fc9h")))

(define-public crate-apollo-encoder-0.2.2 (c (n "apollo-encoder") (v "0.2.2") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1pm3bhwc35xaymnc1c33q3w8k5jdn14r2vs1366vn4mrzaix9wzh")))

(define-public crate-apollo-encoder-0.2.3 (c (n "apollo-encoder") (v "0.2.3") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "04rfmjaab0hl9wd4r6v16r3rwvv29l6iz6zc6d71mk7ml7c27bn2")))

(define-public crate-apollo-encoder-0.3.0 (c (n "apollo-encoder") (v "0.3.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "02w2g51y9gjpbcaiqc0w3w3rh2xlab58h9frkiipca0rab0hwf67")))

(define-public crate-apollo-encoder-0.3.1 (c (n "apollo-encoder") (v "0.3.1") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "0wccxcl0i849pawfbqq8d5927agxb3kz861awb38afk60n79bphw")))

(define-public crate-apollo-encoder-0.3.2 (c (n "apollo-encoder") (v "0.3.2") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1ws2vf9si2ik8iiafj07zqhbcjjw9sq0a486q8yjgq0wqgz1ljil")))

(define-public crate-apollo-encoder-0.3.3 (c (n "apollo-encoder") (v "0.3.3") (d (list (d (n "apollo-parser") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1bjsc4yfrq5s09zqdrcw0jfbdddy8gk9b5lfd7vhqxs6mr2xqmr5") (f (quote (("default" "apollo-parser"))))))

(define-public crate-apollo-encoder-0.3.4 (c (n "apollo-encoder") (v "0.3.4") (d (list (d (n "apollo-parser") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0hvb5gckc3d66jicyakvb0m84283w9wbhwdjn3lmc8p90s7x65wb") (f (quote (("default" "apollo-parser"))))))

(define-public crate-apollo-encoder-0.4.0 (c (n "apollo-encoder") (v "0.4.0") (d (list (d (n "apollo-parser") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1w683vjrzvjlbnkv0zn6ymxyjlxwp0alrcsxjbayabk7np7qap2m") (f (quote (("default" "apollo-parser"))))))

(define-public crate-apollo-encoder-0.5.0 (c (n "apollo-encoder") (v "0.5.0") (d (list (d (n "apollo-parser") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1bd2dx4087hd615690ysh0vmgr6hg291mksiag7nfimjhd3s885h") (f (quote (("default" "apollo-parser"))))))

(define-public crate-apollo-encoder-0.5.1 (c (n "apollo-encoder") (v "0.5.1") (d (list (d (n "apollo-parser") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1s211wvgi02vs21rj0i5f0qgwg4w2gpll7wfhinada7sh99cyrqa") (f (quote (("default" "apollo-parser"))))))

(define-public crate-apollo-encoder-0.6.0 (c (n "apollo-encoder") (v "0.6.0") (d (list (d (n "apollo-compiler") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "apollo-parser") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "04f0rgnsxbxp06rrn4rscv0nh54s8wmny1xg4qf0jc2sc9yln5yr") (f (quote (("default" "apollo-parser" "apollo-compiler"))))))

(define-public crate-apollo-encoder-0.7.0 (c (n "apollo-encoder") (v "0.7.0") (d (list (d (n "apollo-compiler") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "apollo-parser") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ksjvdn9amqg1iiclnr9qri72afvhhz93v8s81m81a4m7g2dy8xk") (f (quote (("default" "apollo-parser" "apollo-compiler"))))))

(define-public crate-apollo-encoder-0.8.0 (c (n "apollo-encoder") (v "0.8.0") (d (list (d (n "apollo-parser") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "195paqd4fl8nbnmrhbrd94ihndl6kyki81szvlilkla112r2g7zf") (f (quote (("default" "apollo-parser"))))))

