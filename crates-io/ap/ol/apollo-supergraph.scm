(define-module (crates-io ap ol apollo-supergraph) #:use-module (crates-io))

(define-public crate-apollo-supergraph-0.0.1 (c (n "apollo-supergraph") (v "0.0.1") (d (list (d (n "apollo-at-link") (r "^0.0.1") (d #t) (k 0)) (d (n "apollo-compiler") (r "=1.0.0-beta.3") (d #t) (k 0)) (d (n "apollo-subgraph") (r "^0.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "salsa") (r "^0.16.1") (d #t) (k 0)))) (h "10wv2lmsa54c2b7996hvp380xng0211ccv51drzdx6iqv1asir42")))

