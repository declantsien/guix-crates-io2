(define-module (crates-io ap ac apache-nimble-sys) #:use-module (crates-io))

(define-public crate-apache-nimble-sys-0.0.1 (c (n "apache-nimble-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cstr_core") (r "^0.2.2") (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1vxalh0f3vjzn8x7jfybi5xjmzc92wi4n1r6xb356hlxcb9y3lvs")))

