(define-module (crates-io ap ac apacheloganonymizer) #:use-module (crates-io))

(define-public crate-ApacheLogAnonymizer-0.1.0 (c (n "ApacheLogAnonymizer") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "0nifipk370aamjlzv1qajp85p83ibxc1prbb4g4hlirdkmm2symw")))

(define-public crate-ApacheLogAnonymizer-0.1.1 (c (n "ApacheLogAnonymizer") (v "0.1.1") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "regex") (r "~1.3.9") (d #t) (k 0)))) (h "1aqgd6l4f1hlfh2s9kmka7wi5qhsyjf5j25wcr336zzjlxb92sad")))

