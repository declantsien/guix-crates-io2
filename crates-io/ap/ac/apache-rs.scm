(define-module (crates-io ap ac apache-rs) #:use-module (crates-io))

(define-public crate-apache-rs-0.0.0 (c (n "apache-rs") (v "0.0.0") (h "1mywpf8y7c80vcrm3h82lw8whq244zgpm6sicb74nmj45sxknhia")))

(define-public crate-apache-rs-0.0.1 (c (n "apache-rs") (v "0.0.1") (h "1bm66425hcwr27vjgmc48asdijm2fzhrfnb51s86is15s4nwqglj")))

(define-public crate-apache-rs-0.0.2 (c (n "apache-rs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0f4fw3gqssbxyqmypssl6c75h4g0vbfw8ds65y14afwj3g9qafnf")))

(define-public crate-apache-rs-0.0.3 (c (n "apache-rs") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0a8a0zhf0znljvn9vhz90ab9w0mhgjbminba09zjj9232cmw11q5")))

(define-public crate-apache-rs-0.0.4 (c (n "apache-rs") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1zy3yqfzqx4zjanrvznx5pjqa8sgrrigdv3zzlshzgkkyc8x4wmz")))

