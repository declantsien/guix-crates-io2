(define-module (crates-io ap ac apache-avro-test-helper) #:use-module (crates-io))

(define-public crate-apache-avro-test-helper-0.14.0 (c (n "apache-avro-test-helper") (v "0.14.0") (d (list (d (n "color-backtrace") (r "^0.5.1") (k 0)) (d (n "ctor") (r "^0.1.22") (k 0)) (d (n "env_logger") (r "^0.9.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "ref_thread_local") (r "^0.1.1") (k 0)))) (h "1mkabrm790nvgxq4bp9rxr7rn8w96s47dqkypp75vap04vvcp4v5")))

(define-public crate-apache-avro-test-helper-0.15.0 (c (n "apache-avro-test-helper") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "color-backtrace") (r "^0.5.1") (k 0)) (d (n "ctor") (r "^0.2.2") (k 0)) (d (n "env_logger") (r "^0.10.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "log") (r "^0.4.19") (k 0)) (d (n "ref_thread_local") (r "^0.1.1") (k 0)))) (h "1b8x681h2wimsww21mw1i4bxx5gqspzci9484mh0sgn8la0b7lwj") (r "1.60.0")))

(define-public crate-apache-avro-test-helper-0.16.0 (c (n "apache-avro-test-helper") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "color-backtrace") (r "^0.5.1") (k 0)) (d (n "ctor") (r "^0.2.4") (k 0)) (d (n "env_logger") (r "^0.10.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "ref_thread_local") (r "^0.1.1") (k 0)))) (h "1mgj73sli4m49frp4y460p7sxpkc73f00n5rqcbby4sh5fdqh9hf") (r "1.65.0")))

