(define-module (crates-io ap hr aphreco) #:use-module (crates-io))

(define-public crate-aphreco-0.1.0 (c (n "aphreco") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "1vzfcn89qwwgw2jcyxk5wl9p0pkbb8ql8wchxrx8y5mybyqn5mr2")))

(define-public crate-aphreco-0.1.1 (c (n "aphreco") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "1ns9jxhnzxdlkj0qh2d2sljz30bn4zsh41plcg6snf50yhg6bwqs")))

(define-public crate-aphreco-0.1.2 (c (n "aphreco") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "0n34w5v3ymqd06rxv7jy1bm9lnmz06x44g9caxd3f3gfavnwrj10")))

(define-public crate-aphreco-0.1.3 (c (n "aphreco") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "084223dd2giv2dby9fd4gx7qqijrb8w34rz5qd73gln2bnq2qfjy")))

(define-public crate-aphreco-0.1.4 (c (n "aphreco") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "05x887abm1fjz2sccxlxdswgfl2j0yy5f6k6p6c8pwgr4pksbp2i")))

(define-public crate-aphreco-0.1.5 (c (n "aphreco") (v "0.1.5") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "1w6nh7fwh9dd3dvr5d5h4p11y6219w3rqfql3xvsiwifqm2wjhzh")))

(define-public crate-aphreco-0.1.6 (c (n "aphreco") (v "0.1.6") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "0naylngp53lp076pykyvll2ccd5bf4z2lfn1zp8snwcx09jm32h6")))

(define-public crate-aphreco-0.1.7 (c (n "aphreco") (v "0.1.7") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "186kakv5dinl8i8kyxxlgiwz9axkhfdiqfqywsq7hdwhwvmiw8m9")))

