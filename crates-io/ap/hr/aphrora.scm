(define-module (crates-io ap hr aphrora) #:use-module (crates-io))

(define-public crate-aphrora-0.1.0 (c (n "aphrora") (v "0.1.0") (h "181p4nchwwq6k1c1qzrkg56kz4hxn20lwbxwp59rldjq6yxw7fhk")))

(define-public crate-aphrora-0.1.1 (c (n "aphrora") (v "0.1.1") (h "1zldwhrw01ky4glj1mqshdvs79slfc6sdyhh4x9aqiz85sl6pr8c")))

(define-public crate-aphrora-0.2.0 (c (n "aphrora") (v "0.2.0") (h "1bj6x55cq5zmv38warq689j73spclp040ic95d4wbxghzfpdw472")))

