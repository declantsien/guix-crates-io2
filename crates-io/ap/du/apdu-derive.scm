(define-module (crates-io ap du apdu-derive) #:use-module (crates-io))

(define-public crate-apdu-derive-0.1.0 (c (n "apdu-derive") (v "0.1.0") (d (list (d (n "apdu-core") (r "=0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0sik0mgmbchlfbsdv9mlgn12xxa3s30l7pq63smniqx5cxmf1giv")))

(define-public crate-apdu-derive-0.1.1 (c (n "apdu-derive") (v "0.1.1") (d (list (d (n "apdu-core") (r "=0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "08c1sjb32fkiw2lyqfiv6pvyaj9qqir09zhwp0gnj8np6bn6xcac")))

(define-public crate-apdu-derive-0.2.0 (c (n "apdu-derive") (v "0.2.0") (d (list (d (n "apdu-core") (r "=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1c968v4h3rrkqqxl2jm089r9g01hn3kgyfcy70a8f2d7bzslwgv1")))

(define-public crate-apdu-derive-0.2.1 (c (n "apdu-derive") (v "0.2.1") (d (list (d (n "apdu-core") (r "=0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0cl88i58g0fkws5x5dcgc6zkf48zr5m24kz32xfd362r2ilbzw0f")))

(define-public crate-apdu-derive-0.3.0 (c (n "apdu-derive") (v "0.3.0") (d (list (d (n "apdu-core") (r "=0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1fcndn5h26f23j2qbzky363fxagpip2pc9alb0yscy38sfg5dam2")))

(define-public crate-apdu-derive-0.4.0 (c (n "apdu-derive") (v "0.4.0") (d (list (d (n "apdu-core") (r "=0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0z7yyw7xsv3jb42iffbgrrq17rcgc67fxf9rmh2h098hrvvpbmig")))

