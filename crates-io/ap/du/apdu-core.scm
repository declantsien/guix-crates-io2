(define-module (crates-io ap du apdu-core) #:use-module (crates-io))

(define-public crate-apdu-core-0.1.0 (c (n "apdu-core") (v "0.1.0") (h "1z5z8ifais40065mf1n36d1d8cvpfci9yvybmn64qbwpzw9lhmax")))

(define-public crate-apdu-core-0.1.1 (c (n "apdu-core") (v "0.1.1") (h "1wnrfz2m0k5mkvz6h5pzi6lndm7n782hb61c4zhhxy8hm4s2mzsm")))

(define-public crate-apdu-core-0.2.0 (c (n "apdu-core") (v "0.2.0") (h "0bk41a2yj17a70ii7ac9ym6wy46hjch6ydyyz9z6mzr26jz7xygw")))

(define-public crate-apdu-core-0.2.1 (c (n "apdu-core") (v "0.2.1") (h "0z0f81a4jpv3ycl5zhr4zf1g0kns350cfx8j1c15sz102nglm8qv")))

(define-public crate-apdu-core-0.3.0 (c (n "apdu-core") (v "0.3.0") (h "0mrm4wrkhrrwhxk0kmhi5kwgbi1qxw5xjxr9p756rfzy94fss8r8") (f (quote (("std") ("longer_payloads") ("default" "std"))))))

(define-public crate-apdu-core-0.4.0 (c (n "apdu-core") (v "0.4.0") (h "11kzwa96kzn2l739bhn9wjgj70f6w8g73lx6bcr6igkbllhvjniw") (f (quote (("std") ("longer_payloads") ("default" "std"))))))

