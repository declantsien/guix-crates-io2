(define-module (crates-io ap du apdu) #:use-module (crates-io))

(define-public crate-apdu-0.1.0 (c (n "apdu") (v "0.1.0") (d (list (d (n "apdu-core") (r "=0.1.0") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.1.0") (d #t) (k 0)))) (h "124lilxl514qd59g4cmkk5mpv3cy33hn82r7dy1vilkpcz4k1zmn")))

(define-public crate-apdu-0.1.1 (c (n "apdu") (v "0.1.1") (d (list (d (n "apdu-core") (r "=0.1.1") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.1.1") (d #t) (k 0)))) (h "0qmyfbz8drdkxsn6b8sm23fi1jbnypam3mln5bxmv413pajh2z0w")))

(define-public crate-apdu-0.2.0 (c (n "apdu") (v "0.2.0") (d (list (d (n "apdu-core") (r "=0.2.0") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rnxh6qgf8v33fcndkdr2qh1nn66zdcqqfr5v2j231c62cnqix0l")))

(define-public crate-apdu-0.2.1 (c (n "apdu") (v "0.2.1") (d (list (d (n "apdu-core") (r "=0.2.1") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z2jj6lz6p7aj3vqdrf74054h7zy41gx890111007blr8hivhlhn")))

(define-public crate-apdu-0.3.0 (c (n "apdu") (v "0.3.0") (d (list (d (n "apdu-core") (r "=0.3.0") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kfi1nixhavp18f8ic4jh94mkqgxsj7w9zs8r7zbbfqgmcfldc4n")))

(define-public crate-apdu-0.4.0 (c (n "apdu") (v "0.4.0") (d (list (d (n "apdu-core") (r "=0.4.0") (d #t) (k 0)) (d (n "apdu-derive") (r "=0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zcgn3zywvbf044hwi22x6rh6lj3xd5p3hnnkd0m9cz9lq6a58bs")))

