(define-module (crates-io ap id apidoc-core) #:use-module (crates-io))

(define-public crate-apidoc-core-0.1.1 (c (n "apidoc-core") (v "0.1.1") (d (list (d (n "apidoc-attr") (r "^0.1.1") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.1") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pzgqa3skckihxws8r09vwz2dylwv3npq504pc6jzccwpb8854b1")))

(define-public crate-apidoc-core-0.1.2 (c (n "apidoc-core") (v "0.1.2") (d (list (d (n "apidoc-attr") (r "^0.1.2") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.1") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05zfd3mjvhl6zadpf2gr2mhjr4nap9qlq7qc8dl59dwya41amy0i")))

(define-public crate-apidoc-core-0.1.3 (c (n "apidoc-core") (v "0.1.3") (d (list (d (n "apidoc-attr") (r "^0.1.3") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.1") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a9v2m0lrc5igdwj2fliig8k5vg1fqpabm51vwgm777g86j227z0")))

(define-public crate-apidoc-core-0.1.4 (c (n "apidoc-core") (v "0.1.4") (d (list (d (n "apidoc-attr") (r "^0.1.4") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.3") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mw2y1rjvxcz031wnjgdkv9zlbfwpwi3zbpnp064gzmz3mbh5q04")))

(define-public crate-apidoc-core-0.1.5 (c (n "apidoc-core") (v "0.1.5") (d (list (d (n "apidoc-attr") (r "^0.1.5") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.5") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08f3nhkvdslkfw6gjwxlyr5vkmklkyxi6sllzmdqjm18smjncxfn")))

(define-public crate-apidoc-core-0.1.6 (c (n "apidoc-core") (v "0.1.6") (d (list (d (n "apidoc-attr") (r "^0.1.6") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.6") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hbabbs546f0qv81201sjf3y49ja07fsmkkgbvgmikd92vny0vwb")))

(define-public crate-apidoc-core-0.1.7 (c (n "apidoc-core") (v "0.1.7") (d (list (d (n "apidoc-attr") (r "^0.1.7") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.6") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zf1zky84jhz09ckxw1r4z5x5b0kk38v5swz6bgidgkny9a3wfdm")))

(define-public crate-apidoc-core-0.1.8 (c (n "apidoc-core") (v "0.1.8") (d (list (d (n "apidoc-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.7") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nq92fw5rw011ad7pjailyb3cx1rx3k2qj7qrzss9yxsw0h699r6")))

(define-public crate-apidoc-core-0.1.9 (c (n "apidoc-core") (v "0.1.9") (d (list (d (n "apidoc-attr") (r "^0.1.9") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.7") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m8jpm8m0vzlww2aahn9h3z23h0j5ynbhhvr9c1x31g1l1n08p7q")))

(define-public crate-apidoc-core-0.2.0 (c (n "apidoc-core") (v "0.2.0") (d (list (d (n "apidoc-attr") (r "^0.2.0") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dbmhfy5dfpqjhv82fmp8xm7m4dwadipvzb3h83vdg7zvb451z6x")))

(define-public crate-apidoc-core-0.2.1 (c (n "apidoc-core") (v "0.2.1") (d (list (d (n "apidoc-attr") (r "^0.2.1") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gdgxayh9qk70zwmcjf6iamaashhxaqygbnjrbl8wypk1vk4143k")))

(define-public crate-apidoc-core-0.2.2 (c (n "apidoc-core") (v "0.2.2") (d (list (d (n "apidoc-attr") (r "^0.2.2") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gkrj08h031f8nbpyf0gw9blb9aqz2im3jil9i29sq8fjn5m96rd")))

(define-public crate-apidoc-core-0.2.3 (c (n "apidoc-core") (v "0.2.3") (d (list (d (n "apidoc-attr") (r "^0.2.3") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jjxhl15f15wdccv0y61x2m8r5cvjxh1yc9hxymgdc2dpckl3hz6")))

(define-public crate-apidoc-core-0.2.4 (c (n "apidoc-core") (v "0.2.4") (d (list (d (n "apidoc-attr") (r "^0.2.4") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wfpr5f4qpi4mi7mpzp11d3mb0kr6dq3hmmld91799bzm2nlm69x")))

(define-public crate-apidoc-core-0.2.5 (c (n "apidoc-core") (v "0.2.5") (d (list (d (n "apidoc-attr") (r "^0.2.5") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ilg03909aaww901ic4sbc8rcsv8mxkfqpcclq0qqmc42rf6cgc8")))

(define-public crate-apidoc-core-0.2.6 (c (n "apidoc-core") (v "0.2.6") (d (list (d (n "apidoc-attr") (r "^0.2.6") (f (quote ("core"))) (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n4z12yxbgbw6piwdkgyw57dvs4by25frgdlrr665jhbaqmn3wvz")))

