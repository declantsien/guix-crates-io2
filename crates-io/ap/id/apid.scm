(define-module (crates-io ap id apid) #:use-module (crates-io))

(define-public crate-apid-0.1.0 (c (n "apid") (v "0.1.0") (h "00v6lrsqd74f7ksry9pp48b411ai5gnj7126bxcn36mn76qcy7r9") (y #t)))

(define-public crate-apid-0.1.1 (c (n "apid") (v "0.1.1") (h "14958rfy9v2mnvp3bhg8pq8wns3yp732gkdgn3ackpzlaf2kwim5")))

(define-public crate-apid-0.2.0 (c (n "apid") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0a3cr6x92lnwr10f9wvsmljp1mcml8hwscrfdzzwiiy12mb4997n")))

