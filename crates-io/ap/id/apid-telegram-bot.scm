(define-module (crates-io ap id apid-telegram-bot) #:use-module (crates-io))

(define-public crate-apid-telegram-bot-0.1.0 (c (n "apid-telegram-bot") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gcpzgyq1v5kqgj23n7y8f0sjbj55iz980dkh08q01gmc2kh03yq")))

(define-public crate-apid-telegram-bot-0.1.1 (c (n "apid-telegram-bot") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rkryy1g1b7jak30y87543xpdmh8mcb2zsasx9sw0kix1w6lb1xm")))

(define-public crate-apid-telegram-bot-0.1.2 (c (n "apid-telegram-bot") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ap42cbp2h91wq365znb9x3kk95vn4gcd56513xn8lj4qzgzk9k5")))

(define-public crate-apid-telegram-bot-0.1.3 (c (n "apid-telegram-bot") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0n9xji7x0xx5cnjkl5jrnjyaxcf6d0p102v8p1gjnnin7ywz5ghf")))

(define-public crate-apid-telegram-bot-0.1.4 (c (n "apid-telegram-bot") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "148kz122439l5bp3gssbp00mshczg7p910ika0qgc79x9nlimk0n")))

(define-public crate-apid-telegram-bot-0.2.0 (c (n "apid-telegram-bot") (v "0.2.0") (d (list (d (n "apid") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-enum-str") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1w2k5jr72d0533dn4xm20a1d3gvl3d2sybj2kalfxsr11shmi81c")))

(define-public crate-apid-telegram-bot-0.2.1 (c (n "apid-telegram-bot") (v "0.2.1") (d (list (d (n "apid") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-enum-str") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1arfg5yc6k1csvxfl41dx9h2441vxpslhlm0bgrvjcv076mwxayn")))

(define-public crate-apid-telegram-bot-0.2.2 (c (n "apid-telegram-bot") (v "0.2.2") (d (list (d (n "apid") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-enum-str") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0sd343qnqpxiq2di7pl0lk717y5blmqzjv0l0dm1pm2nvywiy5ac")))

(define-public crate-apid-telegram-bot-0.2.3 (c (n "apid-telegram-bot") (v "0.2.3") (d (list (d (n "apid") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-enum-str") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ipv1y3p9l6maz2m1zxvn3da9rhbmvcvqmwlybbq481cbjf3awdj")))

