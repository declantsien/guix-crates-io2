(define-module (crates-io ap id apidoc-proc) #:use-module (crates-io))

(define-public crate-apidoc-proc-0.1.1 (c (n "apidoc-proc") (v "0.1.1") (d (list (d (n "apidoc-expand") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1c2hkz7zrk194zlsl2322ivxhphzv0pmjha7ar25pmghg94fk1gm") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.2 (c (n "apidoc-proc") (v "0.1.2") (d (list (d (n "apidoc-expand") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0z35wnzwx76phw33srjhmhcvy3w84nrswvh0z63nl8bvqcaimnv9") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.3 (c (n "apidoc-proc") (v "0.1.3") (d (list (d (n "apidoc-expand") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0zwmwz0624f84lyns5lxbx5i9mqlgrvp8hv5qqql4h6bb6m4a6pz") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.4 (c (n "apidoc-proc") (v "0.1.4") (d (list (d (n "apidoc-expand") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1aryvsrckk8z2l0vmkvhadvx1mqw25wd801ji834n6ig41wgqxwb") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.5 (c (n "apidoc-proc") (v "0.1.5") (d (list (d (n "apidoc-expand") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0np6aycg2w9k8lh5dwvq6s1vfqbxk74np3cz9jifr5jk1l53lvfh") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.6 (c (n "apidoc-proc") (v "0.1.6") (d (list (d (n "apidoc-expand") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "11p59w7jci6dbfyvk9izawwylgzl3rk5i1lglnfwv91lzipcdjd2") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.7 (c (n "apidoc-proc") (v "0.1.7") (d (list (d (n "apidoc-expand") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0i6p8mya6l3lq728diyq91wz135c647shad8447dly0ms6v2rfrv") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.8 (c (n "apidoc-proc") (v "0.1.8") (d (list (d (n "apidoc-expand") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0zqp48hf9hiva1pfkm9wmhz9lcdh353y2x0zvhl11bv1kfkry8j0") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.1.9 (c (n "apidoc-proc") (v "0.1.9") (d (list (d (n "apidoc-expand") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0liymjs974h9rjgk96lq313hm8b2ivjlsrnmd6wnqylswvhykfx2") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.0 (c (n "apidoc-proc") (v "0.2.0") (d (list (d (n "apidoc-expand") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0sczipx7a2p2c328mxh1zbv73rhl5bhwwi2hz8dnm1qq8a0s0k22") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.1 (c (n "apidoc-proc") (v "0.2.1") (d (list (d (n "apidoc-expand") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "03s90sz9vvvbfskf0qcprbq4b0rqw2wrp622b1rv18vl6l2yygr8") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.2 (c (n "apidoc-proc") (v "0.2.2") (d (list (d (n "apidoc-expand") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0pmdyp9b1rrzmkr4agsgfbiqb5n33hszz5112n3da5x70bgxsj4b") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.3 (c (n "apidoc-proc") (v "0.2.3") (d (list (d (n "apidoc-expand") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "10ln15y579wrhd5p3n229630bakip93gn1rj5b0ncy1i7slllnpi") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.4 (c (n "apidoc-proc") (v "0.2.4") (d (list (d (n "apidoc-expand") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0rkcl553wayh8ix2qqbsw3c8cv8kwgvrgqmmqckaglxcm3488s5y") (f (quote (("proc" "apidoc-expand" "syn") ("cod"))))))

(define-public crate-apidoc-proc-0.2.5 (c (n "apidoc-proc") (v "0.2.5") (d (list (d (n "apidoc-expand") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "14bn4cid3gf80hvcpy344qrslmn8l6wsc770ks38j4mrq2flfjxn") (f (quote (("generate" "apidoc-expand" "syn"))))))

(define-public crate-apidoc-proc-0.2.6 (c (n "apidoc-proc") (v "0.2.6") (d (list (d (n "apidoc-expand") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0ai3fmwr25rjq3q1zhlfm1p9l5ylvzjzfrfjq0jkap9b91wvh1fv") (f (quote (("generate" "apidoc-expand" "syn"))))))

