(define-module (crates-io ap id apidoc-attr) #:use-module (crates-io))

(define-public crate-apidoc-attr-0.1.1 (c (n "apidoc-attr") (v "0.1.1") (d (list (d (n "procmeta") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1i1q16x6y5nggx98khla3hsayxdnnzr6srd4v3iqzv4i3c3599hp") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.2 (c (n "apidoc-attr") (v "0.1.2") (d (list (d (n "procmeta") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "093aai2m61njlp4g5x5lwmjp6q2i8z5bnaj1mls5c77gvrdal3fr") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.3 (c (n "apidoc-attr") (v "0.1.3") (d (list (d (n "procmeta") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0cchiihy7pr5fxsh756cpgiypbf2bdabpw0b5xyw38dgapm10ikc") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.4 (c (n "apidoc-attr") (v "0.1.4") (d (list (d (n "procmeta") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1dlxbqsn506f13lram84xhvnwm9fbx1f3axijgf8y5cc1zrp26ij") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.5 (c (n "apidoc-attr") (v "0.1.5") (d (list (d (n "procmeta") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0g98709nyzs2n1kd7zfvsgs4r2xa42gbi83s7hgsfrhlsz73dwdm") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.6 (c (n "apidoc-attr") (v "0.1.6") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "11dqjg227q0wnxi26lkz58cv79l70ag6yj7qrjm1fnmb7ncy3lnw") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.7 (c (n "apidoc-attr") (v "0.1.7") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1jnxk7ijhyqwh0511ck7syr9wgn4s0dfj6ffihv4847f421b85by") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.8 (c (n "apidoc-attr") (v "0.1.8") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0ia75g5m77xg3m8j3rw4g6493wpsxlv3dlcp5zprn48apfqjiv5a") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.1.9 (c (n "apidoc-attr") (v "0.1.9") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1b0l8agjr400ydlnhrw4jb5crps1zyzcdszynf4i2glshamjcyb1") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.0 (c (n "apidoc-attr") (v "0.2.0") (d (list (d (n "procmeta") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0kjychcj835hld7hl5j63gcb2vmwvv0l2yd4d4my9l551ms646q0") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.1 (c (n "apidoc-attr") (v "0.2.1") (d (list (d (n "procmeta") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "01vxy8qq7z4iah0prabwbgnb506zdk04q9k3npmbfhp7zsg8raf8") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.2 (c (n "apidoc-attr") (v "0.2.2") (d (list (d (n "procmeta") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "07maw85q17w2b62scqspk7g41l3nhxpkq22rcjb3hn4gf557919c") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.3 (c (n "apidoc-attr") (v "0.2.3") (d (list (d (n "procmeta") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "11k03w0nd1kd2pyfc0ix3lwkkc2valfqdaw6lp66yncmj0pgkmq5") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.4 (c (n "apidoc-attr") (v "0.2.4") (d (list (d (n "procmeta") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0b1k63pzwc6j5m28q0l20aq3i2ja8gcc48n4bhigcz2vbrb6jrk4") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.5 (c (n "apidoc-attr") (v "0.2.5") (d (list (d (n "procmeta") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "197sj4f2mlrnh99d4gws6azdirfjba2y2y75g3ppbghhf293hwap") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

(define-public crate-apidoc-attr-0.2.6 (c (n "apidoc-attr") (v "0.2.6") (d (list (d (n "procmeta") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (o #t) (d #t) (k 0)))) (h "13ypva7nkaj47iay977mim3pkn214kqqz2qc92i3hzslraivxnc2") (f (quote (("proc" "procmeta" "syn") ("core" "serde"))))))

