(define-module (crates-io ap ns apns) #:use-module (crates-io))

(define-public crate-apns-0.0.1 (c (n "apns") (v "0.0.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "153p808k24yab0j88yl4hj4n01yjdjy60a9v8fsxpb7zl6s4pvgg")))

(define-public crate-apns-0.0.2 (c (n "apns") (v "0.0.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0af9ams30hjb3ivl9qkmpf8v3kiizdngxnzdbb0af5wfblqcxyx4")))

(define-public crate-apns-0.0.3 (c (n "apns") (v "0.0.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0js0ic5flybpvb79h3b9h3rlk9n34gyx9lmyx9fmlf85avjwrxav")))

(define-public crate-apns-0.0.4 (c (n "apns") (v "0.0.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "09y2n1nqm423j7c703cvi9j9vs5kgpdwxwz65s1jqmvas3kra4sb")))

(define-public crate-apns-0.0.5 (c (n "apns") (v "0.0.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1lvwci3as1mldayfzky4m3sqw9wpai5a7rijj9har4biz6vbq5q9")))

(define-public crate-apns-0.0.6 (c (n "apns") (v "0.0.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0lyq6xr06pfcaw4l08xnia4f6pj2xyblfdy4cb7wcqssjxsgy6yr")))

(define-public crate-apns-0.0.7 (c (n "apns") (v "0.0.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0krcpmw9iw5j9wwz9jj8k6y9xy90km7g1zcyi4x170sh3aa8sli7")))

(define-public crate-apns-0.0.8 (c (n "apns") (v "0.0.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0p5m93gnp7jsvx2xxrmynfmvfyh57cz41b4wf6ba28fcxg4c04pk")))

(define-public crate-apns-0.0.9 (c (n "apns") (v "0.0.9") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "05kfc65c9x98n5555jrl8f0yy5qwppmnad3vnh7qvyx2jxk9vp7r")))

(define-public crate-apns-0.0.10 (c (n "apns") (v "0.0.10") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0hfmclgyqv9cjf4s71sqq7fpzxc39fj1jmvnlvgvawa01wzfjk3j")))

(define-public crate-apns-0.1.0 (c (n "apns") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "159gl7v1v1bg30fi49lambfjrpjlgcnppld0i7zh5nqnq9qgn89a")))

(define-public crate-apns-0.1.1 (c (n "apns") (v "0.1.1") (d (list (d (n "byteorder") (r "~0.4") (d #t) (k 0)) (d (n "num") (r "~0.1") (d #t) (k 0)) (d (n "openssl") (r "~0.7") (d #t) (k 0)) (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "13yf39c542zsvvq5hd0k502a7pmd0a3qjz4zj0fh8hjkd1n97ads")))

