(define-module (crates-io ap ns apns2) #:use-module (crates-io))

(define-public crate-apns2-0.1.0 (c (n "apns2") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.6.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0j89hlwr7klik75cvpf6l5h0k8kzmh4g85g08660dr1yxa2d03yb")))

