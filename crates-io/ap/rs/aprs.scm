(define-module (crates-io ap rs aprs) #:use-module (crates-io))

(define-public crate-aprs-0.1.0 (c (n "aprs") (v "0.1.0") (h "0l01ffad7jagxjxw92glq9pn1bz2srykjiniyj66r8hyqn9nvws1")))

(define-public crate-aprs-0.2.0 (c (n "aprs") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)))) (h "13gc4jpa2jpfz7wi49s578yl6wchqnl05i0p38xc6jlqlvq0v6hv") (y #t)))

(define-public crate-aprs-0.2.1 (c (n "aprs") (v "0.2.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)))) (h "06z0yhmdc61g1qxjnrvbhmy6zyaazm5l90lf9z1x1r8d5220cls6")))

(define-public crate-aprs-0.3.1 (c (n "aprs") (v "0.3.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)))) (h "1165cdnrljsxf4vl6h1sy7318rld9yzah2qyxdcp7cxpx2vflj8h")))

