(define-module (crates-io ap rs aprshttp) #:use-module (crates-io))

(define-public crate-aprshttp-0.1.0 (c (n "aprshttp") (v "0.1.0") (d (list (d (n "aprs-encode") (r "^0.1.2") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "callpass") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "03jcwhvkvzrns39lhqd9xrg3p8cmzvhpa09gi8zspwsvx91s9jr7")))

