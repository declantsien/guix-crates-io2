(define-module (crates-io ap rs aprsproxy) #:use-module (crates-io))

(define-public crate-aprsproxy-0.3.5 (c (n "aprsproxy") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.21") (d #t) (k 0)))) (h "1hw6cwhs8cdmgrzj7g83wdczddgcqz912idqy8hzxzcww5rgpdqa")))

