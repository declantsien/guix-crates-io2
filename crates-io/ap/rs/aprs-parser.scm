(define-module (crates-io ap rs aprs-parser) #:use-module (crates-io))

(define-public crate-aprs-parser-0.1.0 (c (n "aprs-parser") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1njjlsyg2myk42ww37pacd8r4mppdj84nmma7fwfa5af8r5brbqa")))

(define-public crate-aprs-parser-0.1.1 (c (n "aprs-parser") (v "0.1.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "028aqx4gda86rcvxa9dplqn6idnhvqggb7m55p2qckrkpvq6gyvn")))

(define-public crate-aprs-parser-0.2.0 (c (n "aprs-parser") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "04x8bab2309arl2r5cavz65fp2y8nbmcqbvyp2mdv4h45nxzzqr7") (r "1.52.0")))

(define-public crate-aprs-parser-0.3.0 (c (n "aprs-parser") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1kq8g6dr831zp0bxadzqzmaq8ilyvc5p3qq619gbdhh7anni59yk") (r "1.52.0")))

(define-public crate-aprs-parser-0.4.0 (c (n "aprs-parser") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "12rm0qisgj3ha53zfz8l70zlf3cdkhvbww144s2qqgxp4y0lav3p") (r "1.60.0")))

(define-public crate-aprs-parser-0.4.1 (c (n "aprs-parser") (v "0.4.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0187bjfpysngc2g5bawn1740gxhlq3ppdln0bcc056zsabpv3xm5") (r "1.60.0")))

(define-public crate-aprs-parser-0.4.2 (c (n "aprs-parser") (v "0.4.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "01v27qj6ai8l7bwl58rh1h3zfyfq04x3p2ybhi81nv8if8mq2jyk") (r "1.60.0")))

