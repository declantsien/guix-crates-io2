(define-module (crates-io ap rs aprs-encode) #:use-module (crates-io))

(define-public crate-aprs-encode-0.1.0 (c (n "aprs-encode") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)))) (h "0ni5rmzca1d57g48bz034jkhs72a4dp0pxqlswyfc00h1s4wxsk5")))

(define-public crate-aprs-encode-0.1.1 (c (n "aprs-encode") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)))) (h "1qd609ar7vnwsbqlwxjx7rrp275zp7hsb0h5pm479h8x0ldpc80m")))

(define-public crate-aprs-encode-0.1.2 (c (n "aprs-encode") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)))) (h "0672119ys5jnass3k4z0fs42v1mbmvcqxg7kc6hjgnsv3nav5nmj")))

