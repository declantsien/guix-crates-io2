(define-module (crates-io ap ia apiarchivesouvertesrust) #:use-module (crates-io))

(define-public crate-apiarchivesouvertesrust-0.1.0 (c (n "apiarchivesouvertesrust") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16nki400z3f87j7xsr9pnnblzza2j3bs7slbkzan3m8fyhwgxgld")))

