(define-module (crates-io ap ia apiary) #:use-module (crates-io))

(define-public crate-apiary-0.1.0 (c (n "apiary") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros"))) (k 2)))) (h "0bwssxxj6nph9fl7cjb2yk754xkb7wmz30c22rss4q64m37javrj")))

