(define-module (crates-io ap ia apiai) #:use-module (crates-io))

(define-public crate-apiai-0.1.0 (c (n "apiai") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1qdf71g2v28dk3hprr910llphmqdviy5k8qb6np3zlyiwjlmpc7r")))

(define-public crate-apiai-0.1.1 (c (n "apiai") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1fibm10s1syq9cyvfglnkffksbd9jhfzmv86614wbiwsigigfwi6")))

