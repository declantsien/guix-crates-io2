(define-module (crates-io ap h- aph-cli) #:use-module (crates-io))

(define-public crate-aph-cli-0.1.0 (c (n "aph-cli") (v "0.1.0") (d (list (d (n "aph") (r "^0.1.0") (d #t) (k 0)))) (h "15jm3vmwd7117w7qgp4c22sskkjsdbari1a9p9adi33z55mlbycy")))

(define-public crate-aph-cli-0.2.0 (c (n "aph-cli") (v "0.2.0") (d (list (d (n "aph") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "11v25bdvh4qyj79xcxgvwjzrjzkv6k28ww7w6d1p9f1d9jlja8qd")))

