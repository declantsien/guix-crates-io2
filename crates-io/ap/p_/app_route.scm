(define-module (crates-io ap p_ app_route) #:use-module (crates-io))

(define-public crate-app_route-0.1.0 (c (n "app_route") (v "0.1.0") (d (list (d (n "app_route_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_qs") (r "^0.4.5") (d #t) (k 0)))) (h "0mlgdgbrb1wd6flbn7p3i7a97spwmq27hjqlpxx2ph9nhahl73w6")))

(define-public crate-app_route-0.1.1 (c (n "app_route") (v "0.1.1") (d (list (d (n "app_route_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_qs") (r "^0.4.5") (d #t) (k 0)))) (h "1d13cvf4b8bdm6b3141axrk825j4247zmzxgwsahc9y0l6jspzb8")))

(define-public crate-app_route-0.2.1 (c (n "app_route") (v "0.2.1") (d (list (d (n "app_route_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_qs") (r "^0.4.5") (d #t) (k 0)))) (h "14i8iykv13a4hr434649k6brxg0g5rj4q7q5nvznvrk4fzh6n9fh")))

(define-public crate-app_route-0.3.0 (c (n "app_route") (v "0.3.0") (d (list (d (n "app_route_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_qs") (r "^0.4.5") (d #t) (k 0)))) (h "09kz85kqba0mg6j3bak14d39dg1k2gyfnichxaazr5b74hg2b68y")))

