(define-module (crates-io ap p_ app_route_derive) #:use-module (crates-io))

(define-public crate-app_route_derive-0.1.0 (c (n "app_route_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q0mhvlhz9jcf9kjjv438ij18s4cyixg1f9k6j5ax3pfwx1px0b7")))

(define-public crate-app_route_derive-0.1.1 (c (n "app_route_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03jc5k6mh2qzi0s4yhlipbl58am4gdmj5fl90c4jg4720ic26b4j")))

(define-public crate-app_route_derive-0.2.0 (c (n "app_route_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "098sjp1a3bd7a6giyqfdb3y97kk8700q0yrxs9l670h0bz9cscrc")))

(define-public crate-app_route_derive-0.2.1 (c (n "app_route_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13wxxxgpliyqy6kw99awy2jk6jdh1randb25bh9qhm18vmkiapvf")))

(define-public crate-app_route_derive-0.3.0 (c (n "app_route_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qbin17vqi4ld4c80q0brd0zpnds31mvclzmbwi4h0vdjdy76pk5")))

