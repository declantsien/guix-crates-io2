(define-module (crates-io ap p_ app_dirs) #:use-module (crates-io))

(define-public crate-app_dirs-0.0.0 (c (n "app_dirs") (v "0.0.0") (h "1jdvdc8958wrad60pjjv1nw2wvc7zxknmh3jas7rvi50pxqlb0qp")))

(define-public crate-app_dirs-0.0.1 (c (n "app_dirs") (v "0.0.1") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "13j5lcybl67rk3x8yy4g0fm83rivfcsf6szmpji5jf7dkvzlbxxn")))

(define-public crate-app_dirs-0.0.2 (c (n "app_dirs") (v "0.0.2") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "0h4kfiiqad7zj8ws57slzxsazj2h50ay2nc0galn2c4jf4p12zh3")))

(define-public crate-app_dirs-0.1.0 (c (n "app_dirs") (v "0.1.0") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1b4hhqicd0npsd1j1gwjiwkqbbavsllcfxr7qa7711zaxfkddpfk")))

(define-public crate-app_dirs-1.0.0 (c (n "app_dirs") (v "1.0.0") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0dss68rfv63afxa3klspgccqxda0l06z350xhjnijnckkdnlrip5")))

(define-public crate-app_dirs-1.0.1 (c (n "app_dirs") (v "1.0.1") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1qg48sgcsyx77i9xl140wv6120q2n80xy0x3dmxz9354an3n2lvn")))

(define-public crate-app_dirs-1.1.0 (c (n "app_dirs") (v "1.1.0") (d (list (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1bxg3p5dhfvxdh9rr49mbxynjnk1ai1md4z2mn8kwf9pm4bzlb7k")))

(define-public crate-app_dirs-1.1.1 (c (n "app_dirs") (v "1.1.1") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1m4g0njcp8sbx3jyr01daj0prn7lf5wrazw48cqb3fw1ibac1ldp")))

(define-public crate-app_dirs-1.2.0 (c (n "app_dirs") (v "1.2.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "1wyr56amqqngc6pfr159cixxh9apylby6qvbhnxn80vvdyqxqf4x")))

(define-public crate-app_dirs-1.2.1 (c (n "app_dirs") (v "1.2.1") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)))) (h "0g9ijvl4xqbmahy7d7s0wj570wg0kz3ad0jk77b98smxv6x28fp7")))

(define-public crate-app_dirs-2.0.0 (c (n "app_dirs") (v "2.0.0") (h "05p0m5a1jknbbydanpyldgl38j1xm0v86007q5n06ill68lgi20n")))

(define-public crate-app_dirs-2.0.1 (c (n "app_dirs") (v "2.0.1") (h "0yziwhs1hv4zgzdzpbwlyfhhdmyp19gggsbd5nnm40cpzwmaq3by")))

