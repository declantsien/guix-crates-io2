(define-module (crates-io ap p_ app_properties) #:use-module (crates-io))

(define-public crate-app_properties-0.1.0 (c (n "app_properties") (v "0.1.0") (d (list (d (n "serde_yaml") (r "^0.9.18") (d #t) (k 0)))) (h "1j8qx9n9w28nq43jy5vlmj3fldnxa1i720k7xmkfndlfldpkdqc2")))

(define-public crate-app_properties-0.1.1 (c (n "app_properties") (v "0.1.1") (d (list (d (n "serde_yaml") (r "^0.9.18") (d #t) (k 0)))) (h "1gkwmliv7k00gfy7l3304cpw1w1x8i2psgb64gxjac5mg7c8c4gh")))

(define-public crate-app_properties-0.1.2 (c (n "app_properties") (v "0.1.2") (d (list (d (n "serde_yaml") (r "^0.9.18") (d #t) (k 0)))) (h "11dgwkk7rcjmfcd60qkklr9yf03v53amndp1r3gz0a8377z45wmb")))

