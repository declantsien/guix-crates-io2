(define-module (crates-io ap p_ app_units) #:use-module (crates-io))

(define-public crate-app_units-0.1.0 (c (n "app_units") (v "0.1.0") (d (list (d (n "euclid") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.5") (d #t) (k 0)))) (h "1mb6cmbgbrckazapq3xpdmhndadwfygh2mh3ibi5xzwm5rfmm1wq")))

(define-public crate-app_units-0.1.1 (c (n "app_units") (v "0.1.1") (d (list (d (n "euclid") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0699hapwhbyqrl03137733wq21kf4y2rsz9hgv0v4g4n1hsyqmzn") (f (quote (("plugins" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.1.2 (c (n "app_units") (v "0.1.2") (d (list (d (n "euclid") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0dl4vm2b14i8pmmkql636mqcvmkmfq10lby4ws5in01rdznal4cy") (f (quote (("plugins" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.1.3 (c (n "app_units") (v "0.1.3") (d (list (d (n "euclid") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1d63bjzjrxzz02d4xpzvzbihbg6dwcb9pvjp2kg7vnw4js0ddp3i") (f (quote (("plugins" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.1.4 (c (n "app_units") (v "0.1.4") (d (list (d (n "euclid") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0c83a75aq5x46nxl0a9mppz7xfqhq7qf7ayci93mbjcj3cx2d7yq") (f (quote (("plugins" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.2.0 (c (n "app_units") (v "0.2.0") (d (list (d (n "euclid") (r "^0.6.1") (d #t) (k 0)) (d (n "heapsize") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "13f68ddhnsr3gfmqjsg4baina5l2b716cb0h7v9lc8mqzbim4jg9") (f (quote (("plugins" "heapsize" "heapsize_plugin" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.2.1 (c (n "app_units") (v "0.2.1") (d (list (d (n "euclid") (r "^0.6.1") (d #t) (k 0)) (d (n "heapsize") (r ">= 0.2.2, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0j1wxidpb5jz2a78lizv27jl6vb65835qby9ssaddmm286z84mgr") (f (quote (("plugins" "heapsize" "heapsize_plugin" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.2.2 (c (n "app_units") (v "0.2.2") (d (list (d (n "euclid") (r "^0.6.2") (d #t) (k 0)) (d (n "heapsize") (r ">= 0.2.2, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1r6g50bg85bzaf135pdv42dg69f9r67c7vhq47dqsryvydnlwijy") (f (quote (("plugins" "heapsize" "heapsize_plugin" "serde" "serde_macros" "euclid/plugins") ("default")))) (y #t)))

(define-public crate-app_units-0.2.3 (c (n "app_units") (v "0.2.3") (d (list (d (n "euclid") (r "^0.6.2") (d #t) (k 0)) (d (n "heapsize") (r ">= 0.2.2, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.6, < 0.8") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r ">= 0.6, < 0.8") (o #t) (d #t) (k 0)))) (h "0f9l8n0glnb227k0g5pibgrnf6ak88si5mf9lnpb1yliw2vjm6r6") (f (quote (("plugins" "heapsize" "heapsize_plugin" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.2.4 (c (n "app_units") (v "0.2.4") (d (list (d (n "euclid") (r "^0.6.2") (d #t) (k 0)) (d (n "heapsize") (r ">= 0.2.2, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.6, < 0.8") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r ">= 0.6, < 0.8") (o #t) (d #t) (k 0)))) (h "14b2f310qvmhsi5h5kz5aavrprl2g5qc1p5d6kx0md2wyp67c2ac") (f (quote (("plugins" "heapsize" "heapsize_plugin" "serde" "serde_macros" "euclid/plugins") ("default"))))))

(define-public crate-app_units-0.2.5 (c (n "app_units") (v "0.2.5") (d (list (d (n "heapsize") (r ">= 0.2.2, < 0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.6, < 0.8") (d #t) (k 0)))) (h "0vnj9k0b9zbk2721mnn9si0jy8b4akk4n7gb7dv8xayp0jxzjd0n") (f (quote (("plugins") ("default"))))))

(define-public crate-app_units-0.3.0 (c (n "app_units") (v "0.3.0") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "1kq49q6yjc41zhb3g5lmcmp4xc089w0cc6havh8vq7g329pyavk3") (f (quote (("plugins") ("default"))))))

(define-public crate-app_units-0.3.1 (c (n "app_units") (v "0.3.1") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "00i65i8c9jlmnfw40bsb5ypavabb8m9xxmq9m5sn44kx3q9cbqgf") (f (quote (("plugins") ("default"))))))

(define-public crate-app_units-0.4.0 (c (n "app_units") (v "0.4.0") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "10qf3v28hycxdq1aknl0mk59p5zrgqp2n5hs7vzaylzdwidkn32s")))

(define-public crate-app_units-0.4.1 (c (n "app_units") (v "0.4.1") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "19bdrv8w7cy2y2x8vk66rvpb6icgfhfh5r1d665zi19990lfp6y8")))

(define-public crate-app_units-0.4.2 (c (n "app_units") (v "0.4.2") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0xrw0vyp62vxibjkcg6bx0sjrcspxyz5wprl3123fzjhpc298gxh")))

(define-public crate-app_units-0.5.0 (c (n "app_units") (v "0.5.0") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xmbgrhvjn7cdm5b952492blla13nlbm2h56bpcm9h67s22szwwr")))

(define-public crate-app_units-0.5.1 (c (n "app_units") (v "0.5.1") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0xghs9nkqs0z6r9sdg62bkgm8bb54pxwiyvfmj4d6jfz642z60mm")))

(define-public crate-app_units-0.5.2 (c (n "app_units") (v "0.5.2") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0cg6m12020haylkfx720wlm2qyyd8wxhix6477l5gpsjrb6w4gl1")))

(define-public crate-app_units-0.5.3 (c (n "app_units") (v "0.5.3") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0f446lkdj20xsvia9l2jkgsc99mrykvbc9mh745iafirazzg7x3z")))

(define-public crate-app_units-0.5.4 (c (n "app_units") (v "0.5.4") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zgiwszcslmzhpb2brz2x8ji2pvk7gpnahf51gsl9jbmxfk8jwz2")))

(define-public crate-app_units-0.5.5 (c (n "app_units") (v "0.5.5") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0k59gjaxghil91vfdpwj6sfh8xg818710m3707k66ci154zmgcby")))

(define-public crate-app_units-0.5.6 (c (n "app_units") (v "0.5.6") (d (list (d (n "heapsize") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0x4061yrp83n0zmrnqqh5py1bskj9yw3p7v4br8lk11vkbh4s2pd")))

(define-public crate-app_units-0.6.0 (c (n "app_units") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1c9p1k8qy535d8rqv37rq7yyw9b21hvgpnjmpfp80xrz92drl1i9")))

(define-public crate-app_units-0.6.1 (c (n "app_units") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "029w95di4vn1hh4641l5gd4l6583x2wgvlmdyvcq9q9xaj1hqwn4")))

(define-public crate-app_units-0.7.0 (c (n "app_units") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0x6p87hxiipbdk03jbwghljrmc1qfbqc3gxbwirkwdqb75lcdbcx")))

(define-public crate-app_units-0.7.1 (c (n "app_units") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zwaxsgvfs6vfm6l6dqnvg7cfc83ci9y0j68wnlsa9bvqkacjgpw") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

(define-public crate-app_units-0.7.2 (c (n "app_units") (v "0.7.2") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0alqridiyn15ykvdxddzszd1crbvjlqvzhcbvax36x04jg2yn1dd") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

(define-public crate-app_units-0.7.3 (c (n "app_units") (v "0.7.3") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j9cgg6sppfqi3a1017vny82vsv6fn8blf048gfrzdgvayb5yr9l") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

(define-public crate-app_units-0.7.4 (c (n "app_units") (v "0.7.4") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fbgdckm5514kj0kjg0v2yz6hh1yzmrjw4h8fza6nhdbkd5qrgkw") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

(define-public crate-app_units-0.7.5 (c (n "app_units") (v "0.7.5") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11yi3rdk1czi3xv4a65r2n207ldsm71i4rvqj53n0046zbwqw108") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

(define-public crate-app_units-0.7.6 (c (n "app_units") (v "0.7.6") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "072j3r2g3b0xl87jmkygf3m09abz00saxr19nxawyj2vwic6196k") (f (quote (("serde_serialization" "serde") ("num_traits" "num-traits") ("default" "num_traits" "serde_serialization"))))))

