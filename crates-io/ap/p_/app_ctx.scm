(define-module (crates-io ap p_ app_ctx) #:use-module (crates-io))

(define-public crate-app_ctx-0.1.0 (c (n "app_ctx") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0iqh0rs34njrgc135mfmg42k2v0274jnk97av8pfi4pmx8w1kmfm")))

(define-public crate-app_ctx-0.1.1 (c (n "app_ctx") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "09936h5h3npzada3ww9bv54gz1h1p5nggw37b7pkgv79xxvbncf7")))

(define-public crate-app_ctx-0.1.2 (c (n "app_ctx") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "07x6qz9n4ydxlpg45plwxp0cxp043rivchc9psr3hv9mcy09l9qr")))

(define-public crate-app_ctx-0.1.3 (c (n "app_ctx") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "1y8ymhiqky52yi5b4xlas55pnxb7md370y5szjr60kv0220ky8la")))

(define-public crate-app_ctx-0.1.3-Fixed (c (n "app_ctx") (v "0.1.3-Fixed") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "094qjfiw47pf6qmy7xpbf541ir3p07vpbqkdil3wjn32dd816fqk")))

(define-public crate-app_ctx-0.1.4 (c (n "app_ctx") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "063mhbn7nj9jx8acm46d2inz62bs8hgn6bi3mlxr2w7866mg28r3") (y #t)))

(define-public crate-app_ctx-0.1.4-FIX (c (n "app_ctx") (v "0.1.4-FIX") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "173ih43h9aw4w0d477lzjgc63x5nyrsb3jkcs9n72bz8jixgkk1q") (y #t)))

(define-public crate-app_ctx-0.1.5 (c (n "app_ctx") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0a0c3j3aw8npw671whybdpw0frsz364asxvcnm6ijyk6h83bmw46")))

(define-public crate-app_ctx-0.2.0-SNAPSHOT (c (n "app_ctx") (v "0.2.0-SNAPSHOT") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "1g93kxp7kfa075af0nbc29blsbqwhjryrjh9gbhj9z43ggy55ywq")))

