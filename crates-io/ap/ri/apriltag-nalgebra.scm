(define-module (crates-io ap ri apriltag-nalgebra) #:use-module (crates-io))

(define-public crate-apriltag-nalgebra-0.1.0 (c (n "apriltag-nalgebra") (v "0.1.0") (d (list (d (n "apriltag") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("rand"))) (d #t) (k 2)))) (h "08p0y1xdx68c2vkhywf7c8ny9aszp28gnl035zn56gi5acbmb1ly")))

