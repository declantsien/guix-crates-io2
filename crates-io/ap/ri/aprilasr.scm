(define-module (crates-io ap ri aprilasr) #:use-module (crates-io))

(define-public crate-aprilasr-0.1.0 (c (n "aprilasr") (v "0.1.0") (d (list (d (n "aprilasr-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "0w5zp7dqmzb5qmpiflj2ymgmphv43i44zvsvlsiv4g4m355qv1c6")))

(define-public crate-aprilasr-0.1.1 (c (n "aprilasr") (v "0.1.1") (d (list (d (n "aprilasr-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "0i6k6ikvmb10g4kwqqdgi7j1i6ilkhc1x8pwbdllpzq17n8z0qca")))

(define-public crate-aprilasr-0.1.2 (c (n "aprilasr") (v "0.1.2") (d (list (d (n "aprilasr-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "1pq11hbkk5vc1h798s5dh2qwdgc8s3hx9sl9k005q4q50d5y9c2p")))

(define-public crate-aprilasr-0.2.0 (c (n "aprilasr") (v "0.2.0") (d (list (d (n "aprilasr-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)))) (h "0xswfgg5my7xf5pc3vhpf6vbaqiby20cqkigpl060nx23y6ad58b")))

