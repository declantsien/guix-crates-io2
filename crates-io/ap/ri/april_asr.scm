(define-module (crates-io ap ri april_asr) #:use-module (crates-io))

(define-public crate-april_asr-0.1.0 (c (n "april_asr") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "1wxfip8q5axs467f6cqapdh4gxap4s6bkmq7bp0h474zaq6a5qn1") (y #t)))

(define-public crate-april_asr-0.1.1 (c (n "april_asr") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "1xvq1ak1zvk41hr2an5pqark6k47bbphbf34hkadk7b1qjj9lgis") (y #t)))

(define-public crate-april_asr-0.1.2-dev1 (c (n "april_asr") (v "0.1.2-dev1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "1sycsgrgdl4n39biximjv50b0nn0666w56384gdf6scx738g8kg3") (y #t)))

(define-public crate-april_asr-0.1.2 (c (n "april_asr") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "0xadpxfkvhl0j2n9zrvrqv18vcknih4xag1lw05y6v4jd8v83szd")))

(define-public crate-april_asr-0.1.21-dev1 (c (n "april_asr") (v "0.1.21-dev1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "0z9yzl9bgy2xairqsahjzvnxd60xq5kkjd6ww1zzz96pnajgysdk") (y #t)))

(define-public crate-april_asr-0.1.21-dev2 (c (n "april_asr") (v "0.1.21-dev2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "10x49x3bnb93kd592bs2aqbf1j80rc37qirfd438as35jhj1v9qn") (f (quote (("disable-sys-build-script") ("default"))))))

(define-public crate-april_asr-0.1.3 (c (n "april_asr") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "12mxhs92qajda1imc1g5r8fsa146zzmbagcvx0ma5640f9pi174l") (f (quote (("disable-sys-build-script") ("default"))))))

(define-public crate-april_asr-0.1.31 (c (n "april_asr") (v "0.1.31") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (d #t) (k 1)))) (h "1jdwz9yw04cf3sh2y7m7ycbcn8blsp5wx4cf8zw1c7jyvr00y0ws") (f (quote (("disable-sys-build-script") ("default"))))))

