(define-module (crates-io ap ri apricot-client) #:use-module (crates-io))

(define-public crate-apricot-client-0.9.0 (c (n "apricot-client") (v "0.9.0") (d (list (d (n "solana-program") (r "=1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0skyam5l09ncpigiyq82agiw891l6szpckgxz731yaa57cqdmsb3")))

(define-public crate-apricot-client-0.9.1 (c (n "apricot-client") (v "0.9.1") (d (list (d (n "solana-program") (r "=1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1gz3ikl8p75waqkr4ahi0drxbm65ddbqq5bngznhpnqwhbad7giz")))

(define-public crate-apricot-client-0.10.0 (c (n "apricot-client") (v "0.10.0") (d (list (d (n "solana-program") (r "=1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1dj0n4cblds5vy4bywlmrbdzayfnb4sb215lcjw0kbxmpq04ryj5")))

(define-public crate-apricot-client-0.10.1 (c (n "apricot-client") (v "0.10.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "14zb5avlwhmn1bxw5082kvk1a5pngn0l3h5sq1852rrm6baq2k42")))

(define-public crate-apricot-client-0.10.2 (c (n "apricot-client") (v "0.10.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0qrsfxdbmwzvxdddk63nrjjb5xvdw2ndjsqpgl4hixglh39pdi81")))

(define-public crate-apricot-client-0.10.3 (c (n "apricot-client") (v "0.10.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "06h9cryryw4rn1q56gwm72vbc73bwb0ck8zivr12swdhrs5y9lkv")))

(define-public crate-apricot-client-0.10.4 (c (n "apricot-client") (v "0.10.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1rigfficib1905p6n1bqch8p3kqq05y8wa09420kib0af9nh3j0j")))

(define-public crate-apricot-client-0.14.0 (c (n "apricot-client") (v "0.14.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "02qwk77m7cbz5gkyxfch1rmqgh05lhpi69rfwiqgj0dn52ynhmmi")))

(define-public crate-apricot-client-0.15.0 (c (n "apricot-client") (v "0.15.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0l6h13x182m19iqwnjcjrlcj6haf43899m0aa8vf5lqprrrb03cb")))

(define-public crate-apricot-client-0.15.1 (c (n "apricot-client") (v "0.15.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.1") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0ilsfxzadbkp9djk14hpmf9wva3mw244la87fw2qw4bijisw9j98")))

