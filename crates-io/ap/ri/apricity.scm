(define-module (crates-io ap ri apricity) #:use-module (crates-io))

(define-public crate-apricity-0.1.0 (c (n "apricity") (v "0.1.0") (d (list (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i67mkfcsxnqinwjjvr7qg5rs0qyq1pfnhrkp4964wg6iv3bjvwg")))

