(define-module (crates-io ap ri apriori_pattern_miner) #:use-module (crates-io))

(define-public crate-apriori_pattern_miner-0.1.1 (c (n "apriori_pattern_miner") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "0a40rbkagzkzm20pffd86hk6rid346ry11iq5nwfs3hj55jqqfj5")))

