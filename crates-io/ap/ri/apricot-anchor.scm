(define-module (crates-io ap ri apricot-anchor) #:use-module (crates-io))

(define-public crate-apricot-anchor-0.1.0 (c (n "apricot-anchor") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "apricot-client") (r "^0.15.1") (d #t) (k 0)))) (h "19kqimxrqgsnj0scnqh7l98bw9zma5aa8wg19vajg1kf8glmzsql")))

