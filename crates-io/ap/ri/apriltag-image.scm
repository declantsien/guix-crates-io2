(define-module (crates-io ap ri apriltag-image) #:use-module (crates-io))

(define-public crate-apriltag-image-0.1.0 (c (n "apriltag-image") (v "0.1.0") (d (list (d (n "apriltag") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)))) (h "1cwwhn31q9xif4hv3d846vp1z4aplf8sny6l2fpj574d18pna68j")))

