(define-module (crates-io ap g_ apg_proc) #:use-module (crates-io))

(define-public crate-apg_proc-0.0.1 (c (n "apg_proc") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0j2i7bd9xql1ly21zxs1imi6b1vr0j1cwphj1hmvvr9b59yf5flz")))

(define-public crate-apg_proc-0.0.2 (c (n "apg_proc") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0rbs5n4n9dn7h904shlb8gwdciikplasj67hg4vpscj6pprnjcnv")))

(define-public crate-apg_proc-0.0.8 (c (n "apg_proc") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0vazm7qafwd9xc3cyw8mpjra8y9iansf3p56yhr8mvw8y700gz7g")))

(define-public crate-apg_proc-0.0.9 (c (n "apg_proc") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0ah36n0b9p7wbn4hhvi9m3rlhs95p30a28pb1x7zkbxjl6zxibrz")))

