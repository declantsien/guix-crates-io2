(define-module (crates-io ap pv appvkek) #:use-module (crates-io))

(define-public crate-appvkek-0.1.0 (c (n "appvkek") (v "0.1.0") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0q7hnbi2zr3p3p7zb5pk771h74z3whqm9xkrky5qi02mysg49mr6") (y #t)))

(define-public crate-appvkek-0.1.1 (c (n "appvkek") (v "0.1.1") (d (list (d (n "bscscan") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0wy606v053j8yy8m9mgz65jc54nbfg2amxzp7hjlpwaglm21bind")))

(define-public crate-appvkek-0.2.0 (c (n "appvkek") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "evmscan") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0sn3l22w1j6s3pm81qwrng4w6kjb043128lba707ysw649qz0x63")))

