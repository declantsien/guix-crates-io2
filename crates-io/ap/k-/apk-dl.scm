(define-module (crates-io ap k- apk-dl) #:use-module (crates-io))

(define-public crate-apk-dl-0.4.3 (c (n "apk-dl") (v "0.4.3") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "clap") (r "^2.3") (d #t) (k 1)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (d #t) (k 0)) (d (n "gpapi") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-dl-stream-to-disk") (r "^0.2") (d #t) (k 0)))) (h "1ihasrkarx54yvvp197hcggxfn6xi08f96agiv9266r5zv5if6mb")))

