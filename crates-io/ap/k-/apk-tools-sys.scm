(define-module (crates-io ap k- apk-tools-sys) #:use-module (crates-io))

(define-public crate-apk-tools-sys-0.1.0 (c (n "apk-tools-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1rlcjfvvl2f3fzcqzfw8v71imi46whf3ihxlb0r4y58bb4ildvwn") (l "apk") (r "1.56")))

