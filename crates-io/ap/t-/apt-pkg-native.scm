(define-module (crates-io ap t- apt-pkg-native) #:use-module (crates-io))

(define-public crate-apt-pkg-native-0.1.0 (c (n "apt-pkg-native") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "05wvywgbvrj6w3yp90yzhhg009hj0q59w8si3k6jj7zg5fsg1bsj")))

(define-public crate-apt-pkg-native-0.2.0 (c (n "apt-pkg-native") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "itertools") (r "^0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "10vc99a54yyvhq4av0qczskk6pq0sr7ri7a49yszw2h19k47z3b3")))

(define-public crate-apt-pkg-native-0.2.1 (c (n "apt-pkg-native") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "itertools") (r "^0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "0jn65z4iw2dngbvwlshxm9q8b1k5dq00bcg716mg4dbc62ig34yh")))

(define-public crate-apt-pkg-native-0.3.0 (c (n "apt-pkg-native") (v "0.3.0") (d (list (d (n "boolinator") (r "^2") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jqacq48vpk9fb1ggk155sgm63h6z9447hqv6r4gpn6r5iz1w91y")))

(define-public crate-apt-pkg-native-0.3.1 (c (n "apt-pkg-native") (v "0.3.1") (d (list (d (n "boolinator") (r "^2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wmayfmirn0dwxds1jpblflgjnnshg3pr34vmda6jbravk727jmq") (f (quote (("ye-olde-apt") ("default"))))))

(define-public crate-apt-pkg-native-0.3.2 (c (n "apt-pkg-native") (v "0.3.2") (d (list (d (n "boolinator") (r "^2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "itertools") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zv2r2j5r141x5g3p2bz8slvwhrsv0vqs6mzv03gzgnx2vy62k16") (f (quote (("ye-olde-apt") ("default"))))))

