(define-module (crates-io ap t- apt-parser) #:use-module (crates-io))

(define-public crate-apt-parser-1.0.0 (c (n "apt-parser") (v "1.0.0") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1z01ilq0m5cnsj70bbqwjfby3janblwl7rqa4javg52k62pww2sw") (r "1.56")))

(define-public crate-apt-parser-1.0.1 (c (n "apt-parser") (v "1.0.1") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1f09h5f42s5jjc38i5i1gg5hxysfp5hdaqcqxwy5rq4kgnb0ifs5") (r "1.56")))

(define-public crate-apt-parser-1.0.2 (c (n "apt-parser") (v "1.0.2") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1i5vr7x1jxsh86caf4kyvsgk5x64xnadwadwdw6mkp8jign2743i") (r "1.56")))

(define-public crate-apt-parser-1.0.3 (c (n "apt-parser") (v "1.0.3") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1cxi6xrnqpqkh7ml2l5v5jvxzlz5p2haa1cd1nzqkhwn34sshjya") (r "1.56")))

(define-public crate-apt-parser-1.0.4 (c (n "apt-parser") (v "1.0.4") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1cpimw23a1vsryic4smba83g86fv6b64grrnd8ny998kv5a43cvs") (r "1.56")))

(define-public crate-apt-parser-1.0.5 (c (n "apt-parser") (v "1.0.5") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1fsi1p6sx1gmy1qlb9aqal8gdlz1jr19adhjn2w2z9jza8nymq75") (r "1.56")))

(define-public crate-apt-parser-1.0.6 (c (n "apt-parser") (v "1.0.6") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0wbsyh6hzrdk5704l0xwyd3ash6xhcm3hxnp1hfwd6ny30b7y7fk") (r "1.56")))

