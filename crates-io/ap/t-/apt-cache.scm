(define-module (crates-io ap t- apt-cache) #:use-module (crates-io))

(define-public crate-apt-cache-0.1.0 (c (n "apt-cache") (v "0.1.0") (h "0m3df1s8074j2qdgx42p3ap56z1k684m24c0zbfpp9lj2wkym62d")))

(define-public crate-apt-cache-0.1.1 (c (n "apt-cache") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19p7hdqs9zrb998zpcz3v66m0m5x9h11d9k1lgbjsn2p518znc3k")))

(define-public crate-apt-cache-0.1.2 (c (n "apt-cache") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "176cgzlxgybma2ns8d81a10ynsn03vw7j3njm65xkislmfmwa3pk")))

(define-public crate-apt-cache-0.1.3 (c (n "apt-cache") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f4mw4d6q5z5jsqxa5l4aa3xk0vvbfqpq1cddhspsbzl1q11bs1m")))

