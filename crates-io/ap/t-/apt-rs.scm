(define-module (crates-io ap t- apt-rs) #:use-module (crates-io))

(define-public crate-apt-rs-0.1.0 (c (n "apt-rs") (v "0.1.0") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0m68g3g3jvnfplq7kafpglvmm7ywkcla12hajsiipphhynkpkplz")))

(define-public crate-apt-rs-0.1.1 (c (n "apt-rs") (v "0.1.1") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0drzs6an5sb6bs99pl1dhlxgagzkzbl9mi3880wywmdm6ih08bxh")))

(define-public crate-apt-rs-0.2.1 (c (n "apt-rs") (v "0.2.1") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "05klg48sfymnh0zc2xnzm165mjr8n7hsmlpkkiw5283ifj9sbj1n")))

