(define-module (crates-io ap t- apt-release-file) #:use-module (crates-io))

(define-public crate-apt-release-file-0.1.0 (c (n "apt-release-file") (v "0.1.0") (d (list (d (n "cascade") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "deb-architectures") (r "^0.1.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.2.0") (d #t) (k 0)))) (h "025bn822bg0xikk113iwx1k0slmp0kr8v9cjlsnw0n91qxz6y6hn") (y #t)))

(define-public crate-apt-release-file-0.1.1 (c (n "apt-release-file") (v "0.1.1") (d (list (d (n "cascade") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "deb-architectures") (r "^0.1.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.2.0") (d #t) (k 0)))) (h "0lraak16cian49w49z68vi7p70dqa0gvjvw9di6a9qccfz1lcn5r")))

