(define-module (crates-io ap he aphelion-util) #:use-module (crates-io))

(define-public crate-aphelion-util-0.1.0 (c (n "aphelion-util") (v "0.1.0") (d (list (d (n "half") (r "^2.4.0") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "01d1b15j8pmzi0vfccgg0fnqc95x2lsv893v8a89nhj9pz58crw0")))

(define-public crate-aphelion-util-0.1.1 (c (n "aphelion-util") (v "0.1.1") (d (list (d (n "half") (r "^2.4.0") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1p7qw7y1p1i690mqwzcra1i7kfdwn2l5rfaydwf2v7j4n3pzy0ya")))

(define-public crate-aphelion-util-0.1.2 (c (n "aphelion-util") (v "0.1.2") (d (list (d (n "half") (r "^2.4.0") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0si2idgzbpkrf3cd0xncqfghggixngx5gv81sz9553ciip7kjq3v")))

