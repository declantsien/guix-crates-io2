(define-module (crates-io ap ar apart) #:use-module (crates-io))

(define-public crate-apart-0.1.1 (c (n "apart") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "12200qc4i8hr0hdz4kyynhn87s5nbh1hbfamz2dx5yyryzj89096")))

(define-public crate-apart-0.1.2 (c (n "apart") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1z13h3d52niyjsqwmjv1k68jgkk0ji6xyw1n93pn9spj86jksxdf")))

(define-public crate-apart-0.1.3 (c (n "apart") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1q7mphnxliw2g5pqjkp3979dk3n83xagq83fj4bixlfpxaj23qc9")))

(define-public crate-apart-0.1.4 (c (n "apart") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "18jvvffgywsb1xc75q0wva3ll0x4nb57s6dv1z699sqwij83c0pb")))

(define-public crate-apart-0.1.5 (c (n "apart") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04vvgqdynwanp51jnkviirwvsjdablmwcci5pvrvpnn66izr3x6i")))

(define-public crate-apart-0.1.7 (c (n "apart") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "18955s8r6n0k6yk2vc1z1kv2fb7vaad4255xy0p2zpfzv8cxww2s")))

(define-public crate-apart-0.1.8 (c (n "apart") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0wjvzlx0c3hgfqscpb6f75rvdl4xaa0ar6drgl480p1f9n51v9ra")))

