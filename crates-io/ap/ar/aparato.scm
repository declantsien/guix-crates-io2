(define-module (crates-io ap ar aparato) #:use-module (crates-io))

(define-public crate-aparato-3.0.0 (c (n "aparato") (v "3.0.0") (h "0z2ak91d21hpxx5if8flzhh8rpn55c0bys8daw6kf4da77kyjx1f") (y #t)))

(define-public crate-aparato-3.1.0 (c (n "aparato") (v "3.1.0") (h "1bqqgvz75qsq1fzpbd9s0nfnph4fz3srj5g08knp8vjvkh8si3lz") (y #t)))

(define-public crate-aparato-3.2.0 (c (n "aparato") (v "3.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0pmbxynpdjpjwxh0ajccbwrakib1hhg80pzq79niq2aa8shx2d50") (y #t)))

(define-public crate-aparato-3.2.1 (c (n "aparato") (v "3.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "14kfxzzapqakmx9xny1q3pn07pg9xzz5ddm9g9valwxfix46v0n2") (y #t)))

(define-public crate-aparato-3.3.1 (c (n "aparato") (v "3.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "19wcwi6gx1ngf8806zkyliwgyp0ab72gmbx56l5m6vxf5sqk49hs") (y #t)))

(define-public crate-aparato-3.4.1 (c (n "aparato") (v "3.4.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "07q0q76nrixvkvb7ha1yv4dph9gb83nqzb1dsyvmjgi08i4wqb2m") (y #t)))

(define-public crate-aparato-3.4.2 (c (n "aparato") (v "3.4.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1jl4qnjymvgj4i47f1n3bfhp5a9a23ikvqc7baz6l7251g0ga32i") (y #t)))

(define-public crate-aparato-4.0.0 (c (n "aparato") (v "4.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1z4hpaa1r39yfyy464jpbx3ac00znvdflqa3v3bnbzmwdhaaz24r") (y #t)))

(define-public crate-aparato-5.0.0 (c (n "aparato") (v "5.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1j9c2w716sy4y2j8a2hqanarxx07135iyzl2n5d8iw816ks6ssc9") (y #t)))

(define-public crate-aparato-5.0.1 (c (n "aparato") (v "5.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0raykhiivgwvjijnwzrm5rycag3gyblm0qnnhbkkvynn4gxrppaq") (y #t)))

(define-public crate-aparato-5.1.0 (c (n "aparato") (v "5.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0cgsn407i5pylqwnfr614c8558mci7bmp8yk3b976hjalw6f8976") (y #t)))

(define-public crate-aparato-6.0.0 (c (n "aparato") (v "6.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "05ybsrcg0950r6m3v4k38qmrbzzf0z7pwl11i88hbrdd86dwb9w0") (y #t)))

(define-public crate-aparato-6.0.1 (c (n "aparato") (v "6.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1n2cd50c69nnc9pdvskxf5ix7rxv4craqwbsmzq17mvqwmx2m2a8") (y #t)))

(define-public crate-aparato-6.0.2 (c (n "aparato") (v "6.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0j76hpnrznapv0zmmvwhpl8kn0kkyls1qpqqkw8r7vzvdshlsjvj") (y #t)))

