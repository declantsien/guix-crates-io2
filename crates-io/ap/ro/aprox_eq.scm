(define-module (crates-io ap ro aprox_eq) #:use-module (crates-io))

(define-public crate-aprox_eq-0.1.0 (c (n "aprox_eq") (v "0.1.0") (d (list (d (n "aprox_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1n46pixyai78dg2r1b2ir88l349c833pacqivzpqb93xihdaizh7")))

(define-public crate-aprox_eq-1.1.0 (c (n "aprox_eq") (v "1.1.0") (d (list (d (n "aprox_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0b77221qpxx8wqv2xvdv375kna0i1k78pmr7ml0zvb670wp4s4q7")))

(define-public crate-aprox_eq-1.2.0 (c (n "aprox_eq") (v "1.2.0") (d (list (d (n "aprox_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0bg8c7sqdvs51wwv6d9d4yhqr2pcgnlx0xa9x2pg30rbrc9jh86i") (y #t)))

(define-public crate-aprox_eq-1.2.1 (c (n "aprox_eq") (v "1.2.1") (d (list (d (n "aprox_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0j872laqbvr17cnxapvkn4r0hnlfizw2bmw481pd4kf4m1w0ingh")))

(define-public crate-aprox_eq-1.3.0 (c (n "aprox_eq") (v "1.3.0") (d (list (d (n "aprox_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0wh4py9a5i1icl6qzvaxzlsp7xpvvid9n9wzhp98sv9lpiv09dik")))

(define-public crate-aprox_eq-1.4.0 (c (n "aprox_eq") (v "1.4.0") (d (list (d (n "aprox_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1n32lcbnxqdk6c0w6byldhwislldqd6j55pkvhd4597y8kx0wfck")))

(define-public crate-aprox_eq-1.5.0 (c (n "aprox_eq") (v "1.5.0") (d (list (d (n "aprox_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1ixl7vz3qr994x4r2wi02wifd1pmmv9qs2vqvqx7xc7ca1mgk1lc")))

(define-public crate-aprox_eq-1.6.0 (c (n "aprox_eq") (v "1.6.0") (d (list (d (n "aprox_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1ygd9b7lzm793dg1k7jby9xhfh7622b66xzrwsnvjzq8di8mnw9d")))

(define-public crate-aprox_eq-1.6.1 (c (n "aprox_eq") (v "1.6.1") (d (list (d (n "aprox_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0ph8ylmlac4h5lpm8f0zcxwhs9nl61c01fk1wgj93j043qypz4c7")))

(define-public crate-aprox_eq-1.6.2 (c (n "aprox_eq") (v "1.6.2") (d (list (d (n "aprox_derive") (r "^0.3.2") (d #t) (k 0)))) (h "1y2f66nx9mv9x5g1k3p4058lww029brk0553yan0ylnmqzi6w0fp")))

(define-public crate-aprox_eq-2.0.0 (c (n "aprox_eq") (v "2.0.0") (d (list (d (n "aprox_derive") (r "^0.3.2") (d #t) (k 0)))) (h "0nsid6n2bd310mjnljnmxpm8bnrarw62wjfkc00jdyxzk4jrlxmk")))

(define-public crate-aprox_eq-2.0.1 (c (n "aprox_eq") (v "2.0.1") (d (list (d (n "aprox_derive") (r "^0.3.2") (d #t) (k 0)))) (h "0vr0q70m63ali0808nwc27f3rcav4h5m5xsyzbfavygdjrh9jy4i")))

