(define-module (crates-io ap ro aprox_derive) #:use-module (crates-io))

(define-public crate-aprox_derive-0.1.0 (c (n "aprox_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0i1hh9k4dcdv2r8jyfrcbkpf7j06rifja2jn3amvi2f38l12560n")))

(define-public crate-aprox_derive-0.2.0 (c (n "aprox_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08f5qlxwqihx6nvq6rf946pgx55jqv8j6b3nnqw5p2idp19wmgpy") (y #t)))

(define-public crate-aprox_derive-0.2.1 (c (n "aprox_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14b0iz5yxgn0ys4fvz26pzqwxgh7m9km4qm7hmwm4i3n179waafy")))

(define-public crate-aprox_derive-0.3.0 (c (n "aprox_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sp2s1p4vs1j423mkq4nx07wdx65fxazzka76wqya3xjjdff953q")))

(define-public crate-aprox_derive-0.3.1 (c (n "aprox_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ygd42n17sdbff3bh0c9nlimq7rdmqf157vbrlh9vvnxivszikx2")))

(define-public crate-aprox_derive-0.3.2 (c (n "aprox_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xip05jkq2sx6p2iijl811mgz7jw64rhfvab08px0d7bmnj013c4")))

