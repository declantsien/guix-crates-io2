(define-module (crates-io ap pw appwrite_openapi) #:use-module (crates-io))

(define-public crate-appwrite_openapi-1.4.9 (c (n "appwrite_openapi") (v "1.4.9") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "183g7vdl6f7x85hcnyidxflrhnqz3hmjk6cx4b06mzw8nmw4flhv") (y #t)))

(define-public crate-appwrite_openapi-1.0.0 (c (n "appwrite_openapi") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "043rb77y3d8xlyri7p6kdrlaii7ljyfxk94m47zbw0y8gsfxzirv")))

