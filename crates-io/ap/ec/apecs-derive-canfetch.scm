(define-module (crates-io ap ec apecs-derive-canfetch) #:use-module (crates-io))

(define-public crate-apecs-derive-canfetch-0.1.0 (c (n "apecs-derive-canfetch") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "088cahilndsc227ihbwhsp0g32m1bdgz57yy2jjric2c2h81h3vn")))

(define-public crate-apecs-derive-canfetch-0.2.0 (c (n "apecs-derive-canfetch") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m7qc5g86g1inck6gacwzydn0zjwkfjm5nj1zif2kj4dgaf9bhnp")))

(define-public crate-apecs-derive-canfetch-0.2.1 (c (n "apecs-derive-canfetch") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d4h28d2sbq40vc7c5rw07rh8a4h6wy2ljs7ph5kdlqdjck33q1i")))

(define-public crate-apecs-derive-canfetch-0.2.2 (c (n "apecs-derive-canfetch") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ni7g2i23fp5k5c34jr53kif1rliaf095lvxn75zkz8mazi64xn")))

(define-public crate-apecs-derive-canfetch-0.2.3 (c (n "apecs-derive-canfetch") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1q0agsph1lqlmw78fpmyq1wc36lq1v7slgnr2xxhnascdw7nqax1")))

