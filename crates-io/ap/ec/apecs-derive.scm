(define-module (crates-io ap ec apecs-derive) #:use-module (crates-io))

(define-public crate-apecs-derive-0.1.0 (c (n "apecs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "061wvrln2djw2ypzb4j3170lrxha0v2x174r2dnlzh9km7whqjar")))

(define-public crate-apecs-derive-0.1.1 (c (n "apecs-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qn4h8j30hh9afa41g9axn2dvsrdwribxrzfcv0z9dbqf6hdmhm3")))

(define-public crate-apecs-derive-0.1.2 (c (n "apecs-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rpa5blc5wq1q50x1n4slg9cly5xmcb92nfwibws9xcdamm8zlwy")))

(define-public crate-apecs-derive-0.1.3 (c (n "apecs-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pi1j9i0nd28l40kgaa9rwrxbixvy5c7j8ni7z0h4i85xvqlg1zc")))

(define-public crate-apecs-derive-0.1.4 (c (n "apecs-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06a3wj8wv1ylljn74shb7hn1yv1gm4bx2lhsxh1lmr92n5pi1xgj")))

(define-public crate-apecs-derive-0.2.0 (c (n "apecs-derive") (v "0.2.0") (d (list (d (n "apecs-derive-canfetch") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "006nmy8s1pxd9g9aznd3qa3lxmwv6i97bwn7d6bmvli3ckv7bsxi")))

(define-public crate-apecs-derive-0.2.1 (c (n "apecs-derive") (v "0.2.1") (d (list (d (n "apecs-derive-canfetch") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03pwqalmkr0z50ml1b34fx0bfnyssrl8v3mjcwld9w8pz5i50b3r")))

(define-public crate-apecs-derive-0.2.2 (c (n "apecs-derive") (v "0.2.2") (d (list (d (n "apecs-derive-canfetch") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s0lcpz7jkkw53hmxd7hn361jpd0ajiak3ky7dwfhlkn7xbzvzs9")))

(define-public crate-apecs-derive-0.2.3 (c (n "apecs-derive") (v "0.2.3") (d (list (d (n "apecs-derive-canfetch") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "071kcr9paya161in559s08zdc9q572s105zdw5fp0s9b3w0wy93n")))

(define-public crate-apecs-derive-0.3.0 (c (n "apecs-derive") (v "0.3.0") (d (list (d (n "moongraph-macros-syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fsg97g8zvy6h3j6kaap6whj2k189pf7b6q3p1pjgm8zsggks3xw")))

