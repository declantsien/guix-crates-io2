(define-module (crates-io ap pk appkit-nsworkspace-bindings) #:use-module (crates-io))

(define-public crate-appkit-nsworkspace-bindings-0.1.0 (c (n "appkit-nsworkspace-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0jcqdx0hbhplichmbmzxm7whxa5d3wdfh3n7jqqry0y5jqv4czsf")))

(define-public crate-appkit-nsworkspace-bindings-0.1.2 (c (n "appkit-nsworkspace-bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1mrdf28533yizayb5l50bcbpksz7y1dbg6k80cna1kq4hs9q48q6")))

