(define-module (crates-io ap od apod_wallpaper) #:use-module (crates-io))

(define-public crate-apod_wallpaper-1.0.0 (c (n "apod_wallpaper") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "ureq") (r "^1.0.0") (d #t) (k 0)) (d (n "wallpaper") (r "^2.0.1") (d #t) (k 0)))) (h "0n9jvzh98nk8yr26vcqv57xpv231crky39c3napyjha1ra9nv024")))

