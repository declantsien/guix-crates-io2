(define-module (crates-io ap od apod_async_client) #:use-module (crates-io))

(define-public crate-apod_async_client-0.1.0 (c (n "apod_async_client") (v "0.1.0") (d (list (d (n "mockito") (r "^0.22.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.0-alpha.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0g8hv5nklwzbd9ngpzwkw6bc78ah6f30gj0m6sc04r836r8kfi56")))

(define-public crate-apod_async_client-0.1.1 (c (n "apod_async_client") (v "0.1.1") (d (list (d (n "mockito") (r "^0.25") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1kvqkm11w228yq87gvl10x1pm2gk2fwacdznmab4yv8lk6jhz0ma")))

(define-public crate-apod_async_client-0.2.0 (c (n "apod_async_client") (v "0.2.0") (d (list (d (n "mockito") (r "^0.29") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "04141bx114vhj71znqld3z67adknzfpsb9vj8wljxp9idqk42zz8")))

