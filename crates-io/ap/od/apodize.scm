(define-module (crates-io ap od apodize) #:use-module (crates-io))

(define-public crate-apodize-0.1.0 (c (n "apodize") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0jf62rkx8jwarrhifaxifk8w3v2dwhgd36n60x75jaavl2yk37rk")))

(define-public crate-apodize-0.1.1 (c (n "apodize") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0kdbq3vn96wcimgbdq7isq19ah00wqdiccdpa81q0hn679lw5wdl")))

(define-public crate-apodize-0.1.2 (c (n "apodize") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "1qwxz4n0zlxkqk690ncvqymmry3pgad58hi0bnwq3s22finkhh0l")))

(define-public crate-apodize-0.2.0 (c (n "apodize") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "1j3cspnnqxfknpynl7b5nj1db77db7ba52wnxnykvhahqz3j96rd")))

(define-public crate-apodize-0.3.0 (c (n "apodize") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0w6r7316k9xzcymk838xnay5wcmyh0zy3zvkhg6fikr3wy99kqz7")))

(define-public crate-apodize-0.3.1 (c (n "apodize") (v "0.3.1") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)))) (h "05wkxvl2rxxi7d9q6hrbjzbdyx4gg2ssblpq584wj0nhwg2acxyb")))

(define-public crate-apodize-1.0.0 (c (n "apodize") (v "1.0.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)))) (h "1v8lp55dwg00c542w223k19gq1rdma2damk5qakwgyd1q36qg8zw")))

