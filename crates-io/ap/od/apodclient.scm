(define-module (crates-io ap od apodclient) #:use-module (crates-io))

(define-public crate-apodclient-0.1.0 (c (n "apodclient") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.32") (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_proxy") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0xi9cksxym4fpabrv8saq3ci1pvlqr66pjv6ig3a5na50scwsn78") (y #t)))

