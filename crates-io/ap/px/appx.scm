(define-module (crates-io ap px appx) #:use-module (crates-io))

(define-public crate-appx-0.1.0 (c (n "appx") (v "0.1.0") (d (list (d (n "wchar") (r "^0.6.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "wchar") (r "^0.6.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt" "winreg"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rk86xg0w0m5yl635787lpvh836pzb8xw1rrajkll4la3l7czhaz")))

