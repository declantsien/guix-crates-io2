(define-module (crates-io ap la aplay) #:use-module (crates-io))

(define-public crate-aplay-0.1.0 (c (n "aplay") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)))) (h "118qj7m8nnh4cmy3vwwbsbxj3lx355gxgp9xnbgmsh141b28i8zm") (y #t)))

