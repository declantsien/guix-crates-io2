(define-module (crates-io ap la aplayer-wasmbind) #:use-module (crates-io))

(define-public crate-aplayer-wasmbind-0.1.0 (c (n "aplayer-wasmbind") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Element"))) (d #t) (k 0)))) (h "10hclh2ycnbaxndbg5bqdy803mrwm609y9x2ik6yqiv63mhxhzs1")))

(define-public crate-aplayer-wasmbind-0.2.0 (c (n "aplayer-wasmbind") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Element"))) (d #t) (k 0)))) (h "19a42j5vsbvs9cijgcvqd3g3iq8p6f0323v2inafjp7yf514wida")))

