(define-module (crates-io ap la aplang) #:use-module (crates-io))

(define-public crate-aplang-0.0.1 (c (n "aplang") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "16s9jnwjnzlg4d2fyns7nspplyzmsvdg6cc42y1ijqlgbrpx4jaw")))

