(define-module (crates-io ap ie apiety) #:use-module (crates-io))

(define-public crate-apiety-0.0.1 (c (n "apiety") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09yamwr77810ikl289iscc8rgbjkyf7gi7f1s9kikxyn148qfajy") (y #t)))

(define-public crate-apiety-0.0.2 (c (n "apiety") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ghah78kjxsq3zwmh0bi80rh1qsng4jz1ya1qqaw0x2zh6ad6x9s") (y #t)))

