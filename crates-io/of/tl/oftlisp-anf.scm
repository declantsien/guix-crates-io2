(define-module (crates-io of tl oftlisp-anf) #:use-module (crates-io))

(define-public crate-oftlisp-anf-0.1.1 (c (n "oftlisp-anf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.1.0") (d #t) (k 0)) (d (n "gc") (r "^0.3.1") (d #t) (k 0)) (d (n "gc_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "oftlisp") (r "^0.1.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.4") (d #t) (k 0)))) (h "1k8qc4vwmxcnv14y0d3fdrr367h4bablq1c09as9bv74l2q4g55l")))

(define-public crate-oftlisp-anf-0.1.2 (c (n "oftlisp-anf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.1.0") (d #t) (k 0)) (d (n "gc") (r "^0.3.2") (d #t) (k 0)) (d (n "gc_derive") (r "= 0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "oftlisp") (r "^0.1.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.4") (d #t) (k 0)))) (h "1qnx1g8nbjlvflic5ww129c461anc8lgl0f7jvn1488jr47sm677")))

(define-public crate-oftlisp-anf-0.1.3 (c (n "oftlisp-anf") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.1.0") (d #t) (k 0)) (d (n "gc") (r "^0.3.2") (d #t) (k 0)) (d (n "gc_derive") (r "= 0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "oftlisp") (r "^0.1.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.4.4") (d #t) (k 0)))) (h "1y56sdpw21g1wx2kqpfc8zyn8lknfb0mkbrnsnvmzk0ydczs29pm")))

