(define-module (crates-io of fi office-hours) #:use-module (crates-io))

(define-public crate-office-hours-1.0.0 (c (n "office-hours") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0gmh5a4g3h5z8r5b476ljpxyam6b9dkcba7ih2j3r6vbns57ds3p")))

(define-public crate-office-hours-1.1.0 (c (n "office-hours") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0ir793975fhmdxzxpgl5g8hj98fxwdnpv1r7y7lsgg38y801vikf")))

