(define-module (crates-io of fi office) #:use-module (crates-io))

(define-public crate-office-0.8.0 (c (n "office") (v "0.8.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.4.1") (d #t) (k 0)) (d (n "zip") (r "^0.1.19") (k 0)))) (h "1fdms3cnax21lpg5670pnq0f1byj54nn4gdjssc85ckk9va1dq97")))

(define-public crate-office-0.8.1 (c (n "office") (v "0.8.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.4.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (k 0)))) (h "0yfjwk5zi8apa81q3mhfwk6n6ijgbqzfw6mxn81mlqprkb5yim34")))

(define-public crate-office-0.8.2 (c (n "office") (v "0.8.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.4.1") (d #t) (k 0)) (d (n "zip") (r "^0.2.0") (k 0)))) (h "1zxf38xpklqs02if5bc8rr42c6hdaic9vqdw6z7vnv042ifzk9ns") (y #t)))

