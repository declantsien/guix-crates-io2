(define-module (crates-io of fi office-jobs) #:use-module (crates-io))

(define-public crate-office-jobs-0.0.1 (c (n "office-jobs") (v "0.0.1") (h "11kq8kyh9s250ywrn7yhzps827rmvnf3wid0lcnbnyzlc3nx5144")))

(define-public crate-office-jobs-0.0.2 (c (n "office-jobs") (v "0.0.2") (h "1clq500bxljb2rc610dbw70frnqksp9m0q1fzqnknrciym2mr8l3")))

(define-public crate-office-jobs-0.0.3 (c (n "office-jobs") (v "0.0.3") (h "14b4fb85ppxq6pay2ip539hr443mhd2wmppk28msmym5r1sj9z5m")))

(define-public crate-office-jobs-0.0.4 (c (n "office-jobs") (v "0.0.4") (h "0af1ylxf0wx6bzic1nivkyfiyig26n773544w0g0x0vxi45lv4ra")))

(define-public crate-office-jobs-0.0.5 (c (n "office-jobs") (v "0.0.5") (h "02k9cgn5mn81xzy4w82z5c195d5d2pzra39ka3kk3qkydkl20p5f")))

(define-public crate-office-jobs-0.0.6 (c (n "office-jobs") (v "0.0.6") (h "118b2ykn15hik7a117jaq8x9vjwg2lj4g24gsam6viacydw38l30")))

(define-public crate-office-jobs-0.0.7 (c (n "office-jobs") (v "0.0.7") (h "0va9ngd8kf3177gya8lzja0kac0pzkbbknxabbp1hi78xaxvfnyl")))

(define-public crate-office-jobs-0.0.8 (c (n "office-jobs") (v "0.0.8") (h "1z16yhgxmpja61djhknca9zc499yzrsil4m6rhil6rbwykwfrl00")))

