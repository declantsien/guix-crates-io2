(define-module (crates-io of x_ ofx_sys) #:use-module (crates-io))

(define-public crate-ofx_sys-0.1.0 (c (n "ofx_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19ljqrk108xdcb54plc1dxlxis68l71c2pprpf64w5h17xply8n4") (y #t)))

(define-public crate-ofx_sys-0.1.1 (c (n "ofx_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gywbajxnagkc8l26g6bqjqzrljdv9306lkf93lg4pwgv1c2klih")))

(define-public crate-ofx_sys-0.2.0 (c (n "ofx_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cgmath") (r "^0.16") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xf5naigq556vbflv3ndxh2a5i6dk4mrvs0clr144y08wb589vqc")))

