(define-module (crates-io of db ofdb-seed) #:use-module (crates-io))

(define-public crate-ofdb-seed-0.1.0 (c (n "ofdb-seed") (v "0.1.0") (d (list (d (n "ofdb-boundary") (r "^0.9") (d #t) (k 0)) (d (n "seed") (r "^0.7") (d #t) (k 0)))) (h "17a9wygl2m33j0bv80a9r4w0f16653akp1bmlzvq8k7b9flpyj81")))

(define-public crate-ofdb-seed-0.2.0 (c (n "ofdb-seed") (v "0.2.0") (d (list (d (n "ofdb-boundary") (r "^0.9.3") (d #t) (k 0)) (d (n "ofdb-entities") (r "^0.9") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "seed") (r "^0.8") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "0lk39szhv7kxqm93gbvx0y1jd0vmlpmwbvd9d4q4jfvd561pqpd7")))

(define-public crate-ofdb-seed-0.3.0 (c (n "ofdb-seed") (v "0.3.0") (d (list (d (n "ofdb-boundary") (r "^0.10") (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "seed") (r "^0.8") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "1ch8mbdfx317xaa55w7w6bkg4pbs1bwv6ypikwxwx7aql9xq35qc")))

(define-public crate-ofdb-seed-0.11.0 (c (n "ofdb-seed") (v "0.11.0") (d (list (d (n "ofdb-boundary") (r "^0.11.0") (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "seed") (r "^0.9.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (d #t) (k 0)))) (h "1q854pwpmwdkzigni6n3k80fgc5j5jg6yzcrp7ljrid1cnd1iqb4")))

(define-public crate-ofdb-seed-0.12.2 (c (n "ofdb-seed") (v "0.12.2") (d (list (d (n "ofdb-boundary") (r "^0.12.2") (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "seed") (r "^0.9.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (d #t) (k 0)))) (h "1wgdf72w9hflv3hsxjax1hxpc0dy15mzyksr6nfy9c1b4qhxna3f")))

