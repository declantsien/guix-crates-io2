(define-module (crates-io of cl ofcli) #:use-module (crates-io))

(define-public crate-ofcli-0.1.0 (c (n "ofcli") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.4") (d #t) (k 0)))) (h "1g4pixd8anlpjyzhw0ahdcyfc7v56rqa6296ixznwqvdh06ydsf2")))

