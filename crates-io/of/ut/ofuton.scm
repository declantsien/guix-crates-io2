(define-module (crates-io of ut ofuton) #:use-module (crates-io))

(define-public crate-ofuton-0.1.0 (c (n "ofuton") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.10.13") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "0bf2jasvbqz79vy77rkwss00qslsijmqxbipjwdk2p40q9b2y4yd")))

