(define-module (crates-io of _d of_dn_parser) #:use-module (crates-io))

(define-public crate-of_dn_parser-0.1.0 (c (n "of_dn_parser") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "error" "from"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)))) (h "1nkdlgsq797yn43i2bxp6cgdvvwcqsm6k9ilw3b7ad5djrldb496")))

(define-public crate-of_dn_parser-0.2.0 (c (n "of_dn_parser") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (k 2)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "error" "from"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)))) (h "0mxgirs7gbf0svq08hi0whav2s2f8ygymg8xswyz2lwa9dczzhj2")))

(define-public crate-of_dn_parser-0.2.1 (c (n "of_dn_parser") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5.0") (k 2)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "error" "from"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)))) (h "1mvpr17dlg7q00kj7z27s68j33ppl20ncc7wnz09mzqd7qgjg342")))

(define-public crate-of_dn_parser-0.3.0 (c (n "of_dn_parser") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (k 2)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "error" "from"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)))) (h "1vrzhbk4njrjvwz0nhm2hq4xqyghr8zp6k6jy9hhq4g9r47zhgp6")))

(define-public crate-of_dn_parser-0.3.1 (c (n "of_dn_parser") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.5.0") (k 2)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "error" "from"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("std"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)))) (h "0cdx1m8v063wac0yy83n2z9fg92pahjpj8r7waxvmj9fawk5qm8c")))

