(define-module (crates-io of fl offline_license_rs) #:use-module (crates-io))

(define-public crate-offline_license_rs-0.1.0 (c (n "offline_license_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1wxjiq2imkvcp3nb50zvxc0lzwxkg2pdwn5k4vlrxxk1ld8nc7y7")))

