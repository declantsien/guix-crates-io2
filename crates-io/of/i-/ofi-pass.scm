(define-module (crates-io of i- ofi-pass) #:use-module (crates-io))

(define-public crate-ofi-pass-0.2.1 (c (n "ofi-pass") (v "0.2.1") (d (list (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mocktopus") (r "^0.7.0") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gl2j0an9iav72i7hjzhzwcdgy5k9m8llcg4jq2hjfp3k68sc13d")))

(define-public crate-ofi-pass-0.3.0 (c (n "ofi-pass") (v "0.3.0") (d (list (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mocktopus") (r "^0.7.0") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "058da29lm9np031ykj1xpvgv9dmz4rfigxxawhvsvrsc3crv5lix")))

(define-public crate-ofi-pass-0.3.1 (c (n "ofi-pass") (v "0.3.1") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mocktopus") (r "^0.8.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0wmkb7wvdhmfp04j8jsg30bhzvcnr4kkr10x8vygji5hjnnrxkdp")))

(define-public crate-ofi-pass-0.4.0 (c (n "ofi-pass") (v "0.4.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mocktopus") (r "^0.8.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "15bbrb3yzyjnf36y6yfaslcv1fycqycc5hqmrbw65ihyw08qb406")))

