(define-module (crates-io of f- off-side) #:use-module (crates-io))

(define-public crate-off-side-0.1.0 (c (n "off-side") (v "0.1.0") (d (list (d (n "indent-stack") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("proc-macro" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0wf445hclqdj0rkm12jpa9hg1yml2g8sm3972rv470qdz2zw14ky")))

(define-public crate-off-side-0.1.1 (c (n "off-side") (v "0.1.1") (d (list (d (n "indent-stack") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("proc-macro" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1dnqg4jpqhb2rs80hmzv8alq0kqi4cdmlq82yycp2clrkvcnf8mz")))

