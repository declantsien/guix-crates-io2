(define-module (crates-io of f- off-rs) #:use-module (crates-io))

(define-public crate-off-rs-0.1.0 (c (n "off-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "030yawv4ii1j509c9hd24ag43dnz9vyyviyh2xmqa1wzkpn8walh")))

(define-public crate-off-rs-0.1.1 (c (n "off-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1c4zjn5d5ymj08w3hk6wwp6d69h5w6mjjr0vfw59hzr0khygckz1")))

(define-public crate-off-rs-0.1.2 (c (n "off-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1za1rsl4mc8kl7sl0ghgcv82bi61v62wb9s14rlrinkp6kncp8v0")))

(define-public crate-off-rs-0.1.3 (c (n "off-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "01gcqzkhw7vqcncrqscicrwwd205cz907n9dd09qw4m3raw2n8pk")))

(define-public crate-off-rs-0.1.4 (c (n "off-rs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1ff9y9wzsmh4w2091kfmzf4abw0ii1iy5phjysbrrd47r6pbcl1x")))

(define-public crate-off-rs-1.0.0 (c (n "off-rs") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0y5gdv3ssbwl2nvvm6sxl5f4px79rr917ivxzc3gy32kig32kvj9")))

