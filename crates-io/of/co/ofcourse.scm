(define-module (crates-io of co ofcourse) #:use-module (crates-io))

(define-public crate-ofCourse-0.1.0 (c (n "ofCourse") (v "0.1.0") (h "10zvcg853p39qaxvfmivy4fjcc2cyv8az5cx8fcw28hfsc98av3f")))

(define-public crate-ofCourse-1.0.0 (c (n "ofCourse") (v "1.0.0") (h "0cr9bypqis0jy4f48v7pazgxz5qnq21h1nh5inckpa4jbvbca5fq")))

(define-public crate-ofCourse-1.1.0 (c (n "ofCourse") (v "1.1.0") (h "1gmby4gpn7agssqk76hz3yjgl7r0bzkv8wjg6pnjfgha2kpj0n22")))

(define-public crate-ofCourse-1.1.1 (c (n "ofCourse") (v "1.1.1") (h "0qzj0q96j319pnyzmdbyff927aqfxc210gbazijmimr9gx9ifiwf")))

