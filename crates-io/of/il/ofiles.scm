(define-module (crates-io of il ofiles) #:use-module (crates-io))

(define-public crate-ofiles-0.1.0 (c (n "ofiles") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0873abamkd9svq5ac7zbxd2arlhmx5da03c2b8cj1n8f13ly7w9l")))

(define-public crate-ofiles-0.1.1 (c (n "ofiles") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1n6nwm9bxn8dmp83v7j603v9lzl21jibvxk9d550fds9ivqcc2h1")))

(define-public crate-ofiles-0.2.0 (c (n "ofiles") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0zdxq6ja31x0virsnjnyr516liijhvhz06cx6bgr9mhjjyi9dnf6")))

