(define-module (crates-io of il ofilter) #:use-module (crates-io))

(define-public crate-ofilter-0.1.0 (c (n "ofilter") (v "0.1.0") (d (list (d (n "bloomfilter") (r "^1.0") (d #t) (k 0)))) (h "1pdkhikqk1bxki5646fpa67njkns1dn3il9g2m4wyni1sk9r8ypq")))

(define-public crate-ofilter-0.1.1 (c (n "ofilter") (v "0.1.1") (d (list (d (n "bloomfilter") (r "^1.0") (d #t) (k 0)))) (h "0ssaymq8x5qgppiaiicqnlb65pczcfvpfgp3b1din2xm7zlhhznd")))

(define-public crate-ofilter-0.2.0 (c (n "ofilter") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (f (quote ("serde_std"))) (d #t) (k 0)))) (h "0vb2slfnh3yqiy6qrrgav1iiliwrxj8i2wj6hh8wr9f3118r5vaq")))

(define-public crate-ofilter-0.3.0 (c (n "ofilter") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (f (quote ("serde_std"))) (d #t) (k 0)))) (h "1h09h45r3kxs3z2gzvx09s32dcwf4c8javzhn61h60f3vnxkcw94")))

(define-public crate-ofilter-0.3.1 (c (n "ofilter") (v "0.3.1") (d (list (d (n "bitvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (f (quote ("serde_std"))) (d #t) (k 0)))) (h "053vgykc18pbfxfi7ks2qnpsc4g5zfcijh2a30nrf89zlflvm16g")))

(define-public crate-ofilter-0.4.0 (c (n "ofilter") (v "0.4.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "1ym2lqrasiaw3rbygrg10csirfp6rsrf4cgz5hahxwwl42snr35p") (f (quote (("default" "rand")))) (s 2) (e (quote (("serde" "dep:serde" "bitvec/serde" "siphasher/serde_std") ("rand" "dep:getrandom"))))))

(define-public crate-ofilter-0.4.1 (c (n "ofilter") (v "0.4.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "1fq80am7bi0kc5vmy6wp47iw1hq5ygx79lclc3f5ad0949rqzfk9") (f (quote (("default" "rand")))) (s 2) (e (quote (("serde" "dep:serde" "bitvec/serde" "siphasher/serde_std") ("rand" "dep:getrandom")))) (r "1.60")))

(define-public crate-ofilter-0.4.2 (c (n "ofilter") (v "0.4.2") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "1kssz1kdd1jq86y7g5y4frm3djgpr87ymlpn7lnqkhcs3lyyi7b9") (f (quote (("default" "rand")))) (s 2) (e (quote (("serde" "dep:serde" "bitvec/serde" "siphasher/serde_std") ("rand" "dep:getrandom")))) (r "1.60")))

(define-public crate-ofilter-0.4.3 (c (n "ofilter") (v "0.4.3") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "187r4afv8dwsn137h7rxrlv6sg9wri8w3jiwzfkpayfscjrrc066") (f (quote (("default" "rand")))) (s 2) (e (quote (("serde" "dep:serde" "bitvec/serde" "siphasher/serde_std") ("rand" "dep:getrandom")))) (r "1.60")))

