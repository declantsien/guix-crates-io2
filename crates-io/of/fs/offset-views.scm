(define-module (crates-io of fs offset-views) #:use-module (crates-io))

(define-public crate-offset-views-0.1.0 (c (n "offset-views") (v "0.1.0") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)))) (h "1yxzd59pixi6mp5ma4xv4gpdc75fcxywd8wr541j4hcpva5mkmpy")))

(define-public crate-offset-views-0.1.1 (c (n "offset-views") (v "0.1.1") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)))) (h "06nz82bnx8mny0laf08g3g9m8am5vd1007xba60742n2y218xap6")))

(define-public crate-offset-views-0.1.2 (c (n "offset-views") (v "0.1.2") (d (list (d (n "cool_asserts") (r "^2.0.3") (d #t) (k 2)))) (h "0br68y3wmh8wmr4qzx5vc8n9217h015ilhdyjhbvmvrngx17ci8l")))

