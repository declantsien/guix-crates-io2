(define-module (crates-io of fs offset-allocator) #:use-module (crates-io))

(define-public crate-offset-allocator-0.1.0 (c (n "offset-allocator") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)))) (h "0xq50x1sd9a65n6cyn4ni5l2bd99k6lp2a4nls73hlc398xm5sza")))

