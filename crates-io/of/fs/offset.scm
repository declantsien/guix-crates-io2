(define-module (crates-io of fs offset) #:use-module (crates-io))

(define-public crate-offset-0.1.0 (c (n "offset") (v "0.1.0") (h "0bcl33xfi7nq4r4930414si4sz6rw52sy8miy6ig8li9m601h70y") (y #t)))

(define-public crate-offset-0.1.1 (c (n "offset") (v "0.1.1") (h "1hj20ddn6wq4d7r5spmm7cyhn8pbkiaf84ggcq9n6wv960wwf0gc")))

(define-public crate-offset-0.1.2 (c (n "offset") (v "0.1.2") (h "1nfs6cra318bb2j77hdv9b8zj1n4rhlkvfcrdj33r2zq980sb8d6")))

(define-public crate-offset-0.1.3 (c (n "offset") (v "0.1.3") (h "1171nfz49nrlfrgpwyfqf50lv5afak1bkxip7q1qxph502imlz5i")))

(define-public crate-offset-0.1.4 (c (n "offset") (v "0.1.4") (h "0dizk08naldvsv7bh5rffhcim4fys08c2gipqxa8zj6xgqw0n30k")))

