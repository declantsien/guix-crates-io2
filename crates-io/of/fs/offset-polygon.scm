(define-module (crates-io of fs offset-polygon) #:use-module (crates-io))

(define-public crate-offset-polygon-0.1.0 (c (n "offset-polygon") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q1pdz0spr6nan5w4y7zn7y40wwzgw8bl3448bikbg59kg4pv408")))

