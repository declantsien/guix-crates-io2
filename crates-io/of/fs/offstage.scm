(define-module (crates-io of fs offstage) #:use-module (crates-io))

(define-public crate-offstage-0.1.0 (c (n "offstage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (f (quote ("zlib-ng-compat"))) (d #t) (k 0)) (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "02simdrdsjvf9likwnhcpavzq2d2b3rqpp4wq9g8rl55wgfbxhn5")))

(define-public crate-offstage-0.1.1 (c (n "offstage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (f (quote ("zlib-ng-compat"))) (d #t) (k 0)) (d (n "globset") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0dik31xbxz2zp3hsz7mp50c24zswah864hgzs0j7a71qzyyymzxj")))

