(define-module (crates-io of f6 off64) #:use-module (crates-io))

(define-public crate-off64-0.0.1 (c (n "off64") (v "0.0.1") (h "0fsxz2kb7kdz2s2h02dp1id22d7115qcqzwgfiz3hx991x8hwkvq")))

(define-public crate-off64-0.0.2 (c (n "off64") (v "0.0.2") (h "14rfgi649c87mq03m4p95gz9c10a04scn6mn2s2zpkgl0sra05c6")))

(define-public crate-off64-0.1.0 (c (n "off64") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1zlkxxq4l15z1z4w39xc69z0gdmw4cxi8c5ig1c0dim69r69h42r") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.2.0 (c (n "off64") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "12rh53l6w4p8h1v8724fil9j0gpfpr7z7sl9bjhyq0b274j25b6v") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.3.0 (c (n "off64") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "07a06rqnqx5grqc56mm1jdah5n45zd6rq3y824bd04rkakg0dw9j") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.3.1 (c (n "off64") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0zpc3kcm7q2mnk9pqwpzgp9k5vrppbbcmyc7b5xjdj9lvfppk3kz") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.4.0 (c (n "off64") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1jivibjgr02qnfdvpdqk4awz84cvl7hrsy8wdbr27nlvydia4pvm") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.5.0 (c (n "off64") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0vpqx2gdcv9p84y63sgdvx438gx6pqncmc73vj3h2gyll74bswqp") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.5.1 (c (n "off64") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1bfx93iy4i0vaxl7z2bjvwn6nyvz9by3j04w0qffj8kym5y2ajff") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.6.0 (c (n "off64") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "11xv7qb58c9a7i5pr3x9w1lgxgr89snwcswdawyy5ln86h8b0d23") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

