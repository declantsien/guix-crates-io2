(define-module (crates-io of nn ofnn) #:use-module (crates-io))

(define-public crate-ofnn-0.1.3 (c (n "ofnn") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xns2ixxdjf77z0lgs3fqr97n7ljh4sa58h0cx8abmzfy8is4phw") (f (quote (("floats-f64"))))))

