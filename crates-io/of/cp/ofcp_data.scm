(define-module (crates-io of cp ofcp_data) #:use-module (crates-io))

(define-public crate-ofcp_data-0.1.0 (c (n "ofcp_data") (v "0.1.0") (h "1myln3xsj1spisn1gv0zayjkbsy01vbrks1xlvp6lrh0cs70zwjk")))

(define-public crate-ofcp_data-0.2.0 (c (n "ofcp_data") (v "0.2.0") (h "07aks8dkjjj3ni92l8pr0m6rb0jfziglfp0hdqz0msvl7hhymky8")))

(define-public crate-ofcp_data-0.3.0 (c (n "ofcp_data") (v "0.3.0") (h "0v1jmd4hf7iniy29wa477g98s04rqxk92sg4zi17k7kckxhqd510")))

(define-public crate-ofcp_data-0.4.0 (c (n "ofcp_data") (v "0.4.0") (h "0rcjzwx16452iygy2hv20awv5qa85rzf552lhlsjzclfc068ffn1")))

(define-public crate-ofcp_data-0.5.0 (c (n "ofcp_data") (v "0.5.0") (h "0lcsn0a5jplb8lmidqa3d44wjyjvvz00y31lqrqlxp7hm3z8rnhl")))

(define-public crate-ofcp_data-0.6.0 (c (n "ofcp_data") (v "0.6.0") (h "178fydvp5csn0xda4h7nila4zspyafsn25dx0ar35fd9511v20kk")))

(define-public crate-ofcp_data-0.7.0 (c (n "ofcp_data") (v "0.7.0") (h "1wvgr5dqd2adsy40ihx0sv1dfn6hxlqvrbd3hkfpk4nw8nsxg264")))

(define-public crate-ofcp_data-0.8.0 (c (n "ofcp_data") (v "0.8.0") (h "0218vg69j02gwdapvs22mf6fdgix35n64121468qnv8ibsy2k3ay")))

(define-public crate-ofcp_data-0.9.0 (c (n "ofcp_data") (v "0.9.0") (h "1xnv7ncgahhmgn59bwdyx3jif4rdnln1vgv1g9lcjki5n2mfv7d8")))

(define-public crate-ofcp_data-0.11.0 (c (n "ofcp_data") (v "0.11.0") (h "0zya9nczn2q643h0bb7w805zway2ibikjdg32ag2h50xypz0ip3k")))

(define-public crate-ofcp_data-0.12.0 (c (n "ofcp_data") (v "0.12.0") (h "0ap8qqpj6f2c6vvsqiqlqp2i18nm95yjqcff31q24vy4m2vlh8vi")))

(define-public crate-ofcp_data-0.12.1 (c (n "ofcp_data") (v "0.12.1") (h "0xpzmh9ldvlz7hjfgmcvx8l73dzgirw0718ng9prc44yhrfgpril")))

(define-public crate-ofcp_data-0.13.0 (c (n "ofcp_data") (v "0.13.0") (h "1walrsgyiajrwqsbgs5ymckmlvva1vrpi7r9ijv6f1b6l3ksld4r") (y #t)))

(define-public crate-ofcp_data-0.14.0 (c (n "ofcp_data") (v "0.14.0") (h "0g48iya6qagcnp3fg16y8r35k6bm8v77zill347bp9nlscgbj5fn") (y #t)))

(define-public crate-ofcp_data-0.13.1 (c (n "ofcp_data") (v "0.13.1") (h "0rk781d9y5ng1gi2p2jqx27ppcs04l7xhinq32g9k6k78545j8i6") (y #t)))

(define-public crate-ofcp_data-0.13.2 (c (n "ofcp_data") (v "0.13.2") (h "1vc028kw9x63jhiwyvdly4l7ldjph07h4wchwqf2m74zplcfb4sn") (y #t)))

(define-public crate-ofcp_data-0.15.0 (c (n "ofcp_data") (v "0.15.0") (h "0fz3rzypqi78s9xb10jqbvnpazs0v6h1nx72161z0g11q5sfw2d4") (y #t)))

(define-public crate-ofcp_data-0.15.1 (c (n "ofcp_data") (v "0.15.1") (h "19vg8b27gbzvvzg4ami7icbiihwpwhn3gpdv5dn1si2klccv9jlv") (y #t)))

(define-public crate-ofcp_data-0.15.2 (c (n "ofcp_data") (v "0.15.2") (h "19ny9fx2chc6xxl2h7wnam78w2awjjchnix5r0ihcgdqp9lx4h1x") (y #t)))

(define-public crate-ofcp_data-0.16.0 (c (n "ofcp_data") (v "0.16.0") (h "054hyqqdy273jq2bbxm5dbs7mic26k0i01v4pxcxjns042m8w8qs") (y #t)))

(define-public crate-ofcp_data-0.17.0 (c (n "ofcp_data") (v "0.17.0") (h "1rbd2hii4fsrfkav2kdgwkvsd94ppmmrnrawfdg646xfdcjsnyn9") (y #t)))

(define-public crate-ofcp_data-0.17.1 (c (n "ofcp_data") (v "0.17.1") (h "1bwf974d8hx9ps5vyd3rn5fcmb5a0fcbr4qxw308mcqpdk6iz1k9") (y #t)))

(define-public crate-ofcp_data-0.17.2 (c (n "ofcp_data") (v "0.17.2") (h "0pxxxi0lc6zilqpps47ym10242al6j3jdixafhl7m4vfm8sgvvxg") (y #t)))

(define-public crate-ofcp_data-0.17.3 (c (n "ofcp_data") (v "0.17.3") (h "1xwz10zw30zw1lwrcvm05q17y9m990mpfd0xn9widjirn73gc020") (y #t)))

(define-public crate-ofcp_data-0.18.0 (c (n "ofcp_data") (v "0.18.0") (h "0qv9s38ybsfawm6cvfvlrnqicfldwqllhfn02amhl1yim4pq8n0d") (y #t)))

(define-public crate-ofcp_data-0.17.4 (c (n "ofcp_data") (v "0.17.4") (h "1cc3683w4f7qbmhjvysprrrgr20f77hkk6l19ml8jrccc2zvxpws")))

(define-public crate-ofcp_data-0.19.0 (c (n "ofcp_data") (v "0.19.0") (h "1lb55iq7m7z6c5457sg7vda8np2zdgdzar4kpjxvx4v6b06s1gn5") (y #t)))

(define-public crate-ofcp_data-0.20.0 (c (n "ofcp_data") (v "0.20.0") (h "0aav68aciwhfxp0nh3hp24wpfajxgwf5n2b2n9l48asn92dg6h67") (y #t)))

(define-public crate-ofcp_data-0.20.1 (c (n "ofcp_data") (v "0.20.1") (h "0hp2bpqx6mds4xjna7s0mdnjqdnxjqbc26hzf71xmf6s86581p9c") (y #t)))

(define-public crate-ofcp_data-0.20.2 (c (n "ofcp_data") (v "0.20.2") (h "0yv5vc6lff7zy7fhr9cmqy5z1f7r80b794rvdqjgl913z17sia6b") (y #t)))

(define-public crate-ofcp_data-0.20.3 (c (n "ofcp_data") (v "0.20.3") (h "00rmkgpw089iydi9hx81wqzpd3n7ax4xgpby0wg63xglllipx3jl")))

(define-public crate-ofcp_data-0.21.0 (c (n "ofcp_data") (v "0.21.0") (h "0hn1nz381i8ijjvlk58397sbpdlhinaq3j90acvkjc3j8xy2ibm2")))

(define-public crate-ofcp_data-0.21.1 (c (n "ofcp_data") (v "0.21.1") (h "13xfbgp8jha4nn7h8dzz12psfhgicg214jvj2flik6hijwd9bhm3")))

(define-public crate-ofcp_data-0.21.2 (c (n "ofcp_data") (v "0.21.2") (h "00ndzl7vpa5037x1vdcj0kikcqd4j0mg26i334474dsf4lp346z9")))

