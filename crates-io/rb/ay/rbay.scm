(define-module (crates-io rb ay rbay) #:use-module (crates-io))

(define-public crate-rbay-0.1.0 (c (n "rbay") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ihdydv7cdvzhl5jq9q1h4fbpfi4mh53b4wchqql9nk947bpl2hx")))

