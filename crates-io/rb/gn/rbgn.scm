(define-module (crates-io rb gn rbgn) #:use-module (crates-io))

(define-public crate-rbgn-0.1.0 (c (n "rbgn") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "0jkrfldqx2kz444843b2x6hzvvyw9ydj7rjwbaqphi9ylj0h88bl") (r "1.75")))

(define-public crate-rbgn-0.1.1 (c (n "rbgn") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "0mbn5048031b8gcdzfmhqc58gvpg4x4h5r8kiq4i0gd8hqqlzih5") (r "1.75")))

(define-public crate-rbgn-0.2.0 (c (n "rbgn") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "1fnkfcdlbrfjchw0iy1gb0i3agckch0zksijpblyhjrgy9k1nnpd") (r "1.75")))

