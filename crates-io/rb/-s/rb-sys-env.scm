(define-module (crates-io rb -s rb-sys-env) #:use-module (crates-io))

(define-public crate-rb-sys-env-0.1.0 (c (n "rb-sys-env") (v "0.1.0") (h "08m0zikac2d5ivqy4kg9r4iwap3d802nsvcs8sx74bw7shxn5cfd")))

(define-public crate-rb-sys-env-0.1.1 (c (n "rb-sys-env") (v "0.1.1") (h "0pyj8993zrf9jhwx77biiymfd7pxl9mw0064hbmzl9898598ghvl")))

(define-public crate-rb-sys-env-0.1.2 (c (n "rb-sys-env") (v "0.1.2") (h "1fxkl0ai8g4mzih1anyk07g6nwc9birx398qnia08dh7kxkh4n53") (r "1.51")))

