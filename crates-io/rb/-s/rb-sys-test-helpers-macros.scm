(define-module (crates-io rb -s rb-sys-test-helpers-macros) #:use-module (crates-io))

(define-public crate-rb-sys-test-helpers-macros-0.1.0 (c (n "rb-sys-test-helpers-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1jil8mgxys2ky6kqh5ac7rijqv1kqyjdz2l59dvcss5lldyhzd8k") (r "1.54")))

(define-public crate-rb-sys-test-helpers-macros-0.2.0 (c (n "rb-sys-test-helpers-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1xf142lwghdf3mypyqsycc1v9s0xvj1xgnvkizambdxdb5j1a7y2") (r "1.60")))

