(define-module (crates-io rb -s rb-sys-test-helpers) #:use-module (crates-io))

(define-public crate-rb-sys-test-helpers-0.1.0 (c (n "rb-sys-test-helpers") (v "0.1.0") (d (list (d (n "rb-sys") (r "^0.9.71") (f (quote ("link-ruby"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "05cxwwa6l5q3w5ffqzca5gqw4cv9h5xq7dzyyd0jc23a9y6gs21x") (r "1.54")))

(define-public crate-rb-sys-test-helpers-0.1.1 (c (n "rb-sys-test-helpers") (v "0.1.1") (d (list (d (n "rb-sys") (r "^0.9.74") (f (quote ("link-ruby"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "19djmpwv6328aiya5a7vi48k8j6ij602s5dga2pw3hbkfa4d9ijx") (r "1.54")))

(define-public crate-rb-sys-test-helpers-0.1.2 (c (n "rb-sys-test-helpers") (v "0.1.2") (d (list (d (n "rb-sys") (r "^0.9") (f (quote ("link-ruby"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "08rv49njnz0xq2db4d9ag8zk2m2fkm3rmhz30navkjkvj4mv7xvm") (r "1.54")))

(define-public crate-rb-sys-test-helpers-0.1.3 (c (n "rb-sys-test-helpers") (v "0.1.3") (d (list (d (n "rb-sys") (r "^0.9") (f (quote ("link-ruby"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "0xs084hqmzqkzygv6769xvma3v2315dc2lihrkj5hffb3kbh1vgn") (r "1.57")))

(define-public crate-rb-sys-test-helpers-0.1.4 (c (n "rb-sys-test-helpers") (v "0.1.4") (d (list (d (n "rb-sys") (r "^0.9") (f (quote ("link-ruby"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "1p4pj0j9gk7m6fkmsqky0w1sp9z4h20ffsfq8a2pnq62qx7zsg96") (r "1.57")))

(define-public crate-rb-sys-test-helpers-0.2.0 (c (n "rb-sys-test-helpers") (v "0.2.0") (d (list (d (n "rb-sys") (r "~0.9.80") (f (quote ("link-ruby" "stable-api-compiled-testing"))) (d #t) (k 0)) (d (n "rb-sys-env") (r "^0.1.2") (d #t) (k 1)) (d (n "rb-sys-test-helpers-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "0ja9hri68xgys605yqcnmgz5gk4w1nzv383clxapkpskjc195mdv") (r "1.60")))

