(define-module (crates-io rb pf rbpf) #:use-module (crates-io))

(define-public crate-rbpf-0.0.1 (c (n "rbpf") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1gbl08vc1gdm8dgsl17cprp5d7f6zr50h1lpn7w93z94ng6jilql")))

(define-public crate-rbpf-0.0.2 (c (n "rbpf") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "19p4xm5mv00cbrjf9imkwsf00sff3k44nhw3f9laikwchwjdg1ja")))

(define-public crate-rbpf-0.1.0 (c (n "rbpf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1i6ij4bscki9jazl73k3abid6pq2hgpyb8kyw7nx54ks5qb1374w")))

(define-public crate-rbpf-0.2.0 (c (n "rbpf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0rhwy28n50m0a3qxb6vs18j6dkfczgqiv93qql30swrsgrfdqdmm")))

