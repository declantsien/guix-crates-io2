(define-module (crates-io rb me rbmenu-tui) #:use-module (crates-io))

(define-public crate-rbmenu-tui-0.1.0 (c (n "rbmenu-tui") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cursive-aligned-view") (r "^0.5.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rbmenu") (r "^0.6.1") (d #t) (k 0)))) (h "0kmrvfa518dxkil033jfii49plccyixq5kfdmps2abr6mig137dv")))

(define-public crate-rbmenu-tui-0.1.1 (c (n "rbmenu-tui") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cursive-aligned-view") (r "^0.5.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rbmenu") (r "^0.6.1") (d #t) (k 0)))) (h "11dfjgbgggcxvq1v1hskdaf1a6z1cr7570z35j00drcyzj1ib1sa")))

(define-public crate-rbmenu-tui-0.2.0 (c (n "rbmenu-tui") (v "0.2.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("toml"))) (d #t) (k 0)) (d (n "cursive-aligned-view") (r "^0.5.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rbmenu") (r "^0.6.3") (d #t) (k 0)))) (h "0bf1qgz7ag7rgh5fyrppci0w131w0vpfs8c7ja5dznkjqgj5zwk1")))

