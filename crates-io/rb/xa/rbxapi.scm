(define-module (crates-io rb xa rbxapi) #:use-module (crates-io))

(define-public crate-rbxapi-0.1.0 (c (n "rbxapi") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1crc0mbkbmlzafvaab9yp4hhg27xz9qz8n7fz926ch0fiah81q7c")))

(define-public crate-rbxapi-0.1.1 (c (n "rbxapi") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1kyhjp8iazc6b3w21jqhg3bxjbg27wjxdzr8rxg7jhr5zwcf4yh3")))

(define-public crate-rbxapi-0.1.2 (c (n "rbxapi") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1yyw3q22ghmlxxv0zzh0xwjb1h05a924sjllzgd9mcpkcycj2g9k")))

