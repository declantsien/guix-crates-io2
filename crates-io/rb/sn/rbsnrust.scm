(define-module (crates-io rb sn rbsnrust) #:use-module (crates-io))

(define-public crate-rbsnrust-0.1.0 (c (n "rbsnrust") (v "0.1.0") (d (list (d (n "robson_compiler") (r "^0.1.4") (d #t) (k 0)) (d (n "robson_compiler") (r "^0.1.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.4.2") (d #t) (k 1)))) (h "1cslsm269253zdidpj2k7hgs4nlw9z7imbjgarw5fbf1ikv2dy13")))

