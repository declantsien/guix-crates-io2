(define-module (crates-io rb xp rbxpacker) #:use-module (crates-io))

(define-public crate-rbxpacker-1.0.0 (c (n "rbxpacker") (v "1.0.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "globset") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ynsvnhvwx6hwrypm4j3hk0fylvilrly4zkcj4ylp69mwbjyf89f")))

(define-public crate-rbxpacker-1.1.0 (c (n "rbxpacker") (v "1.1.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "globset") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "166z3z92ln434qw06pc7zxgv287dqzaam0rfwcjagpr2s3n0gbw3")))

(define-public crate-rbxpacker-1.2.0 (c (n "rbxpacker") (v "1.2.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "globset") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v3gxly87cijvwgdpsvw8k5yaml1ysw3r2njk738r6qjbx358jkg")))

(define-public crate-rbxpacker-1.2.1 (c (n "rbxpacker") (v "1.2.1") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "globset") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sjv4g9wwdz52vmvqkj4i40ddhy9gix5vl66fy6z4wymxw0nwb4m")))

(define-public crate-rbxpacker-1.2.2 (c (n "rbxpacker") (v "1.2.2") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "globset") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04jcn3cqsmsynj5nipmyx7igc08iad46xpll3y5880hh3pczi40r")))

