(define-module (crates-io rb lc rblcheck) #:use-module (crates-io))

(define-public crate-rblcheck-0.1.0 (c (n "rblcheck") (v "0.1.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dbus") (r "^0.9.1") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9.1") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "1yr5f890ipc39nbmdy6fr4g96zm652m19i3vsf7rjv7z2wikwjbk")))

(define-public crate-rblcheck-0.2.0 (c (n "rblcheck") (v "0.2.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.2.0") (d #t) (k 0)))) (h "15mq9cmkyn25sscw24s5iipbzlf7pjjw2va13gwq5hw87daf3s8p")))

(define-public crate-rblcheck-0.3.0 (c (n "rblcheck") (v "0.3.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.3.0") (d #t) (k 0)))) (h "124hq40lnx06dnh2xmw0vbx8am1fiy4hcqbnp0ihidl78sg5f31j")))

(define-public crate-rblcheck-0.4.0 (c (n "rblcheck") (v "0.4.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "0n4zr5w1s1ib7mj7wg51w67208f75bdp8ajikp12ap27ilpxm5yi")))

(define-public crate-rblcheck-0.5.0 (c (n "rblcheck") (v "0.5.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "146q1bhb1zmwvq9wfdbi97aiqbjvfm4146vxv2khivli8ikaj34h")))

(define-public crate-rblcheck-0.5.1 (c (n "rblcheck") (v "0.5.1") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "00a0d39vcrr17q0g9694l92lrrpg4p69a9zw19ll8a6apxd6lfkr")))

