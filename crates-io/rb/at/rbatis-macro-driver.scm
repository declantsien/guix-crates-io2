(define-module (crates-io rb at rbatis-macro-driver) #:use-module (crates-io))

(define-public crate-rbatis-macro-driver-1.5.0 (c (n "rbatis-macro-driver") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1mlyfqb2yg7cbvkmf23ncb9d1qpm91q9vi773cn24zpf6ff3avw5")))

(define-public crate-rbatis-macro-driver-1.5.1 (c (n "rbatis-macro-driver") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1y1v25lmp66p707k9f2djjxr4wwsqh0fj63nfgihbmb5xab3ap9w")))

(define-public crate-rbatis-macro-driver-1.5.2 (c (n "rbatis-macro-driver") (v "1.5.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0v06csrhv30gzwji645s6xmlzrbr7x2z26ysk5kkmfh0g7mhs97d")))

(define-public crate-rbatis-macro-driver-1.5.3 (c (n "rbatis-macro-driver") (v "1.5.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0bpzz04liwhxvsrrh88n9skqbblkxw9pf8x09shv271mvsxqfrjs")))

(define-public crate-rbatis-macro-driver-1.5.4 (c (n "rbatis-macro-driver") (v "1.5.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0yfah1rwsp07b92pg0z56rx56ywnfni5yncpd7w590j159k7sf9y")))

(define-public crate-rbatis-macro-driver-1.5.5 (c (n "rbatis-macro-driver") (v "1.5.5") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1haby8j292i84y7iip36dpp0hh4k7i504wzdfn7ylmk0x0dz268c")))

(define-public crate-rbatis-macro-driver-1.5.6 (c (n "rbatis-macro-driver") (v "1.5.6") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0qsg1qwajzcnrd9zx3nk0kj6a0k00sd1xiwqzia3mzzfix1zyl5j")))

(define-public crate-rbatis-macro-driver-1.5.7 (c (n "rbatis-macro-driver") (v "1.5.7") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "13srlknx1hqf4i16rl7bvid6yhagwa61jsfcikaqydpfxc2nzw81")))

(define-public crate-rbatis-macro-driver-1.5.8 (c (n "rbatis-macro-driver") (v "1.5.8") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1p01jw0z2v4rpghjdq9cwvvdb93n75shf4dl0v85rhlnla4ivw6s")))

(define-public crate-rbatis-macro-driver-1.5.9 (c (n "rbatis-macro-driver") (v "1.5.9") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "127lh11ay6m297z1xcdiclia5bhdphi15zs8mjw91j43c9zll6hh")))

(define-public crate-rbatis-macro-driver-1.6.0 (c (n "rbatis-macro-driver") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0yia0a9snf2w6rfkb4cxj7k8cknhspkq6cfsmw3bjpjw688qmjvm")))

(define-public crate-rbatis-macro-driver-1.6.1 (c (n "rbatis-macro-driver") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0xi25s3s3c4s4qg2ihd7sh1pmjhaphdcy43iwxnicj8rps4rhbp7")))

(define-public crate-rbatis-macro-driver-1.6.2 (c (n "rbatis-macro-driver") (v "1.6.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "10fz9mwxslppnmblkg90p4rnswc2ymxspvg1i68dqyf2b08wkxpz")))

(define-public crate-rbatis-macro-driver-1.6.3 (c (n "rbatis-macro-driver") (v "1.6.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "138fnh3178aqd436z6fzm4faf74nrdx1jfxhbcwfwrgnxvhdplwh")))

(define-public crate-rbatis-macro-driver-1.6.31 (c (n "rbatis-macro-driver") (v "1.6.31") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "107641fcpnyhqnvqv0n5yjdf1f6a0fraxb9417gagmjrsl08n8xl")))

(define-public crate-rbatis-macro-driver-1.6.32 (c (n "rbatis-macro-driver") (v "1.6.32") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1nzdqrj8zyqyxs9hk9y5m21jz3axf55yzl4bmshkqymiwk36kshc")))

(define-public crate-rbatis-macro-driver-1.6.34 (c (n "rbatis-macro-driver") (v "1.6.34") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1vjdpsn03kb86n7kdcn35y40rx6b6zq4kkh5ck8pag8np0k7wj9y")))

(define-public crate-rbatis-macro-driver-1.6.35 (c (n "rbatis-macro-driver") (v "1.6.35") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1d5rx9q69wga0a8rbi9yzrdn5rf4rz1w53bsdps8imfid7b5pvij")))

(define-public crate-rbatis-macro-driver-1.6.36 (c (n "rbatis-macro-driver") (v "1.6.36") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1lvas2cxfzbxd0ydi3s9hs0g66q67vn0ldg4p33c9mgys5ad6k55")))

(define-public crate-rbatis-macro-driver-1.6.37 (c (n "rbatis-macro-driver") (v "1.6.37") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0222pz0fnf4jqv5v3ccnnp6f830421s4r81i0zxdcqqzjgdfjjfz")))

(define-public crate-rbatis-macro-driver-1.6.38 (c (n "rbatis-macro-driver") (v "1.6.38") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0r7qm3ya3jvizkhpgpdjf7wg9s6kxcv7zycgasrw6vv13kcqjb27")))

(define-public crate-rbatis-macro-driver-1.6.39 (c (n "rbatis-macro-driver") (v "1.6.39") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0wll5cssnmg15g8cbqchbjskq7jgy4v2bb25dizs05fl4sdrzv2p")))

(define-public crate-rbatis-macro-driver-1.7.0 (c (n "rbatis-macro-driver") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1jna9wzdh9zdjkw8i0wy5ln569l64qcsbi4j94f5cxgc67y0h29i")))

(define-public crate-rbatis-macro-driver-1.7.1 (c (n "rbatis-macro-driver") (v "1.7.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1a2v8z0jk5i4jb6f00g08dmidya55i8h1zxyrr88cr4mcd7h56rm")))

(define-public crate-rbatis-macro-driver-1.7.2 (c (n "rbatis-macro-driver") (v "1.7.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1ixl7klvwd1zg7bc6cicsmk0c2zrc6w23ql04bbm40fcbn37qzmz") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.21 (c (n "rbatis-macro-driver") (v "1.7.21") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1h80f5vg8b6qppqhs6az5b4j6883v1srcsarwdfhqw1jws1dgkwm") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.22 (c (n "rbatis-macro-driver") (v "1.7.22") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1bbvmzrznrjq0kzn4l7nsi17md9jz2qx47agkrwfbcz7r6ws2y5r") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.23 (c (n "rbatis-macro-driver") (v "1.7.23") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "01660zlga2s1hj0k57l6w98wk03yc2n3r9a447lk1k65v3c3lv2k") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.24 (c (n "rbatis-macro-driver") (v "1.7.24") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "17gayxpqrjdicn5an1j6jyb4i295rwk1mc5d9j6cc3dkhsq1jgkc") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.25 (c (n "rbatis-macro-driver") (v "1.7.25") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "126m8v6if6fsg8qm89wvdy3snwp2cmkjgplpz8c9kfjzv9m2na6z") (f (quote (("print") ("no_print") ("default" "print"))))))

(define-public crate-rbatis-macro-driver-1.7.26 (c (n "rbatis-macro-driver") (v "1.7.26") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0h599v03l49ahc1hxl4w256nbds1z44fhvydxmfmjclb9ckxg30z") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.2 (c (n "rbatis-macro-driver") (v "1.8.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1cznvv9vh507mvs875q7hayw7w8dc6yy376x6jl72h9hbwfqaj92") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.3 (c (n "rbatis-macro-driver") (v "1.8.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1pdq1py3klsbjx4hv4yl3cvrr4d4y3jqy2lni5g1lrr7pqglzbh2") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.4 (c (n "rbatis-macro-driver") (v "1.8.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1ap3ym82aisjv849a9810ig1sf4yf27nmvj33vn4yxkdxhskmd4y") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.5 (c (n "rbatis-macro-driver") (v "1.8.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "15k8mq1pgwzqganli925gpvh9c111laxkdpspd27kivmx3m6g1h7") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.6 (c (n "rbatis-macro-driver") (v "1.8.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1faq98mj255688lip61r8667ranxsfb3nfm6lmaxl9jxfzj6ylin") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.7 (c (n "rbatis-macro-driver") (v "1.8.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1g5v2ks5x051f83kd2l2c1aprgy5z592dr8sfqygnm6qaqnglxn3") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.8 (c (n "rbatis-macro-driver") (v "1.8.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "17bsivhykfm19kb4y1l918jh9np373vczpma98v80kkzv56ky5li") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.9 (c (n "rbatis-macro-driver") (v "1.8.9") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0v6j38vga6qi36n2hf70ipb4457ywzr4ncj7794f5ycpsx6r3zsc") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.10 (c (n "rbatis-macro-driver") (v "1.8.10") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0v3wgsk209yvjl197bawk9bzsfggb54fibdnvj6jcfvyv5s5qqzx") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.11 (c (n "rbatis-macro-driver") (v "1.8.11") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0qlh4fjj8v2iw0h132ccpw09clkk1gzwf7hnn8wq8qgg2sbsb46m") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.12 (c (n "rbatis-macro-driver") (v "1.8.12") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0inh36drda8nizq6rdrzk9ypj7jg47w8y7hgx79fggv82c2kbvwx") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.13 (c (n "rbatis-macro-driver") (v "1.8.13") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1qwf7xkf073yc45w7qacf877abyqmsyfdglff107q4mrv5j8dvbr") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.14 (c (n "rbatis-macro-driver") (v "1.8.14") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0i5f3yqmamiv75km4s9ldav95n2w6bp0qhi4vggq7xq6cjqqsp68") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.15 (c (n "rbatis-macro-driver") (v "1.8.15") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1ywib4k9il64w965cq58810wk8gm8ryz09s60a027fml3qkn05ma") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.16 (c (n "rbatis-macro-driver") (v "1.8.16") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "03656n84p991x2y0jyr6gn0l16j9z6qs8af84hcn1x69ck8z50z7") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.17 (c (n "rbatis-macro-driver") (v "1.8.17") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "05f987scqg35xq4janf8jmzzl8hv0rr3vx71nfvbrllb8s0p4ilx") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.18 (c (n "rbatis-macro-driver") (v "1.8.18") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "13wxz87f5i0h509scdv06zkbmcd5giyw6qpg9hi1z6rvrjp3yhf5") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.19 (c (n "rbatis-macro-driver") (v "1.8.19") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1y93masdwv1mrmvrgy49pp9qfihs9pjh4gwskjkfv67jp53cqm5d") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.20 (c (n "rbatis-macro-driver") (v "1.8.20") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "11dgi8b9mf310n36nxrmmis0dzi06bymhraxgg6fb9yznjjj96n6") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.21 (c (n "rbatis-macro-driver") (v "1.8.21") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "01njrajnvq703czhad7y5bfrfmz2vm9v51q6rw9p8wrhfgdwpww4") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.22 (c (n "rbatis-macro-driver") (v "1.8.22") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1r6i6lc20vi0k257ra6rrqp7wgfzl5n40hdnc2g58nijqn8dz88d") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.23 (c (n "rbatis-macro-driver") (v "1.8.23") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0bpb0qz81iv8770mlx57w9m6d6r2wmc4w5zmmb84c1ik52gkdqz9") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.24 (c (n "rbatis-macro-driver") (v "1.8.24") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0y23253wfs8ab3za334x25s21fhamb75dgzk6xvr4w8rc8wcj9v9") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.25 (c (n "rbatis-macro-driver") (v "1.8.25") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "05lfs7rdm4d2hfgfvyn16541hb9sgah9c7ak9gnz2ahmy85y7jkq") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.26 (c (n "rbatis-macro-driver") (v "1.8.26") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0v0r0vyjzhg7kjqxnpachw942kfvxq10pyjn3rb7r6z5q45k8kzs") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.27 (c (n "rbatis-macro-driver") (v "1.8.27") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1dsl8cp5zqky3b6hf0cdfinspkknb00qnj3s74pc8qb0n7k45435") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.28 (c (n "rbatis-macro-driver") (v "1.8.28") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0cpay024nxbbgmia0myn91g7wcvgmsgkxpvckr0zxph67s7r1y4r") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.29 (c (n "rbatis-macro-driver") (v "1.8.29") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "170qq3bf5qwpw7v7rpxp702wwq50bp0sqrj2jkakq74xlsmx0jla") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.30 (c (n "rbatis-macro-driver") (v "1.8.30") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1fxpaqgpj917lb3gqkwiip58gmm50271kb4xwgil8vkx6f3l46lb") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.31 (c (n "rbatis-macro-driver") (v "1.8.31") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "19681x3darnz31gpbf71wdbvar2j0kzxqpq5cmw8zxb15rhd21qp") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.32 (c (n "rbatis-macro-driver") (v "1.8.32") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "167bgzhkgx7xaf04xxpdpf242w9xlipkhcc7yfz0zd1ax2s9b2q7") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.33 (c (n "rbatis-macro-driver") (v "1.8.33") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "0z7mig38czdk00ah4y7icwrijhvhayvrxvinnks6rpg4j7fcycv5") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.34 (c (n "rbatis-macro-driver") (v "1.8.34") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "1by3bg1b93m7xls7rld3a84ymb17lqcj6pb19za07qzn9g2mbf83") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.35 (c (n "rbatis-macro-driver") (v "1.8.35") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "0hs2jb6xvznx3iw10i3329wf98hdflgc658d0zdb5jj6ys6fljni") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.36 (c (n "rbatis-macro-driver") (v "1.8.36") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.44, <2.0.0") (d #t) (k 0)))) (h "0cfnhqrd3411by0h0bw80znji644zvcssg2yv7b97qbgks4v0qd0") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.37 (c (n "rbatis-macro-driver") (v "1.8.37") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "031dpihp0xm8axh3cd9a76fmlywfjij9s22hjb079na5vf28q2pf") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.38 (c (n "rbatis-macro-driver") (v "1.8.38") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "126jyhyf6bdyrnl356k4i0v04ki5nbh779msq7qv5jpfd5prb32k") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.39 (c (n "rbatis-macro-driver") (v "1.8.39") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "00vk12hpws27204x2pvbwa8dwz7izizim9ji8xz2d0rcrbdvsmcg") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.40 (c (n "rbatis-macro-driver") (v "1.8.40") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1p9swg2kzzp5riczz78a3c0lfzbwc6nbf3xyn1ri7sk41vi3dbdp") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.41 (c (n "rbatis-macro-driver") (v "1.8.41") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "03arzz694gfna3rm5gbsv1k11k0k6jjasybd5c816h9fyf43iqmc") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.42 (c (n "rbatis-macro-driver") (v "1.8.42") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "04d6msv2d5vzvfiqb152vnvqdcpcssp3k2f5ypnlqb570mn0mh1y") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.43 (c (n "rbatis-macro-driver") (v "1.8.43") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "12zc1m2294nds97h16n6dgy0pi0ypb3492xnqvvla5bajnv1h40c") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.44 (c (n "rbatis-macro-driver") (v "1.8.44") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "00nqh75aa3g4jggygjzsr51fv52x1naz6c8nh56wdrkr4xlrdz24") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.45 (c (n "rbatis-macro-driver") (v "1.8.45") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1jl1r703gmzdh2n2nnb1bj875rs7pkwigibqhc4f3z3hc9d43dhn") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.46 (c (n "rbatis-macro-driver") (v "1.8.46") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0pam1qz1kxxwfhjijaiwbf6a3cvw6h62nscfabkzgbi7l6mjb7ah") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.47 (c (n "rbatis-macro-driver") (v "1.8.47") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1mzg91r5scb6wr7lryjhjq76i64k1vkkl2ki4176x5hqhlc079d8") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.49 (c (n "rbatis-macro-driver") (v "1.8.49") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0f99zwlbi1p2wdav01lln7ybikp3zsznmw7m47nvsh4wy80k05g3") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.50 (c (n "rbatis-macro-driver") (v "1.8.50") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0g4yzsl57b4aqlcr1ykbi83343wzbiqp4xfcjzp5akf2z63kkfig") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.51 (c (n "rbatis-macro-driver") (v "1.8.51") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1yw7qmbjmxkiln0rrc7k4wpksqsa6w9jpmip7s7cm24381vyn3w3") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.52 (c (n "rbatis-macro-driver") (v "1.8.52") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "12hinsy39i0acx82b2955yvvnj5s6zjxglkb738chik4icl3l6q5") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.53 (c (n "rbatis-macro-driver") (v "1.8.53") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0s9q7qjj2zbbpw3x7yjmf7crmf6g68kk0dd1hn2hknl7ld6m9g4p") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.54 (c (n "rbatis-macro-driver") (v "1.8.54") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1clpfr7b6vz5rcfhan094ywnibd8sd7hjd9nyn1hmxfqsy6kjnq1") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.55 (c (n "rbatis-macro-driver") (v "1.8.55") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0w0npw92snsd9w4vlii1i3rk8s7xdz71vbrp2bvrp168d7x7kq7l") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.56 (c (n "rbatis-macro-driver") (v "1.8.56") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "1a87i6zc214hlbp2g6av24q4i9w6w98i894sk6lm13fxdqgl8zj4") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.57 (c (n "rbatis-macro-driver") (v "1.8.57") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "164kq55cdw7ss1ka46ycbycj0fljrdxlsxjfhm2zk4s45ccqakf3") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.58 (c (n "rbatis-macro-driver") (v "1.8.58") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "067km8xzribq9zy8g14ll2b0fsj4cs9x71wbdwihagr8plkqdgdk") (f (quote (("no_print") ("default"))))))

(define-public crate-rbatis-macro-driver-1.8.59 (c (n "rbatis-macro-driver") (v "1.8.59") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "16aq0i39zdhy4035b4420hv04r3qsfksj1i8lj82myp2z624l752") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.60 (c (n "rbatis-macro-driver") (v "1.8.60") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0pkwk4kkp6z9vf5ny2jym6n87dv23f4iwic7yspyq2ii6lc6pk99") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.61 (c (n "rbatis-macro-driver") (v "1.8.61") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0r2gcymh38cc7rvyavz8196wwz0k84zgl9jcwf6k7jd8g3wanl59") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.62 (c (n "rbatis-macro-driver") (v "1.8.62") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "13hqrg52fny84ail01wzgrzg7jvyk31vzv1545m6cgh36nr7b3bn") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.63 (c (n "rbatis-macro-driver") (v "1.8.63") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0qxi4blyvhav6kpi0nz5j27gb3fdszfynw8bn1iyw1489f4cvx0m") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.64 (c (n "rbatis-macro-driver") (v "1.8.64") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0520l3gx1zf0jard0rjnbrkmdys9lzc99q5dbsscjbsizd7rplrb") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.65 (c (n "rbatis-macro-driver") (v "1.8.65") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (d #t) (k 0)))) (h "0y1gh729hxawiw1zbb92gn5yiwpxsqq7bvgf7v6y2arn28r7dg3n") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.66 (c (n "rbatis-macro-driver") (v "1.8.66") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "043ik4qj33vn5p6n86p5kqyy7a84bkija12dw06fzmaw98xwk4ai") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.68 (c (n "rbatis-macro-driver") (v "1.8.68") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1m6zrpww2p9sap1gjjhv1azj3460q3h0v8njsjfdmzbh9ypwvihz") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.70 (c (n "rbatis-macro-driver") (v "1.8.70") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1ka17hq6lkfz2rx91qx92dxykjdbxp0w5ilcwq2n213dpx38myxq") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.74 (c (n "rbatis-macro-driver") (v "1.8.74") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "0hl6zfpzgs673zrzh0cnzxjc5v0qgd1q9k3614n71rnznfylzgj8") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.77 (c (n "rbatis-macro-driver") (v "1.8.77") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "17aindsgsrpmvrk7w0zhvir1c7y7qyf8845i5r617ym4p3pd238c") (f (quote (("upper_case_sql_keyword") ("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.78 (c (n "rbatis-macro-driver") (v "1.8.78") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1yraasm9335m7p3lxkknf91xbljpfsswg6am83c1gyh13n9v5aqy") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.80 (c (n "rbatis-macro-driver") (v "1.8.80") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "0hgfvpa28n83k472cm1mj1npmzsyrspwg09qah0lb692zj3zc86d") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-1.8.87 (c (n "rbatis-macro-driver") (v "1.8.87") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0si282w212s0s7mzp3w5zggz172r13yfqjwil0xy8lyzsc7hbri8") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.0 (c (n "rbatis-macro-driver") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq7ln465ysjvkx4xscw155ib63k9q5iwadn0ml5j0y57z2bkz6y") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-2.0.1 (c (n "rbatis-macro-driver") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "18jy6g9iq3rzhnyl4m0vqqqcpqqfxxrmr1imhwcdmsa9zxqd7gmc") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-2.0.2 (c (n "rbatis-macro-driver") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0845as64a27vzppyd50gy4v8xxp1h0y9gqh5c309w56pjhv1zmpv") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.3 (c (n "rbatis-macro-driver") (v "2.0.3") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhxfajas38k3m9078vrji08rp5dkw5hhxg1xrzxpj84hzy4s1sk") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.4 (c (n "rbatis-macro-driver") (v "2.0.4") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "02vm2lx6kv4q6y8js3hji723slqqc0d193s6zwi1dfyp6v6si3b7") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.5 (c (n "rbatis-macro-driver") (v "2.0.5") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0dgqmkm8lf6cdg5dw1cs4ljm9ax185xgdp4n73zfpl9l5mz2yf") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.8 (c (n "rbatis-macro-driver") (v "2.0.8") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "0jj4rfm3mpyzlx43p3wii1d56b4qyjbwa51lpi2p9scs755av3f3") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.9 (c (n "rbatis-macro-driver") (v "2.0.9") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "05ncn3m839g603idg0aqc408pyll6djfvcl8wq50nx1vzqh1rsni") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.22 (c (n "rbatis-macro-driver") (v "2.0.22") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17kxmaj6rymzsxasxpas2f6jci8ssw5hldsqwidpjk8isq7glalw") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.24 (c (n "rbatis-macro-driver") (v "2.0.24") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01cllvz6rqjb1sgxmd1kmaw5kf5kkr8aadc4wnw5bj02r122s1yg") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.0.25 (c (n "rbatis-macro-driver") (v "2.0.25") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01ip98z08crqyrwxmxrcq30mwjypxf4i7x2aakvgzmm9rxpmcl6x") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-2.1.0 (c (n "rbatis-macro-driver") (v "2.1.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "177xmrm9dy2cxsfq9fwywyzs1a3c5l3sg00gv5dp2ilkfs4a1a88") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.0 (c (n "rbatis-macro-driver") (v "3.0.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "067ml64jmi3ms26fszd49hq7djfixpj15jwhiy9lp11ylnllz0g7") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.1 (c (n "rbatis-macro-driver") (v "3.0.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06g7v9n6k6fvj659cbl932bzlyjbxwkzlbi66rh90cs8l1f6j6m1") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.2 (c (n "rbatis-macro-driver") (v "3.0.2") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vmq4jvrpd6m37016lks90i3g0d49sfhg44yj7mhln482ax1jzrh") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.3 (c (n "rbatis-macro-driver") (v "3.0.3") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hf69cv3a4m3q0gh2j9b3c0zmvhs4kx5zs52r1n1g7vmirnvj0bz") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.4 (c (n "rbatis-macro-driver") (v "3.0.4") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vmfizbici77zpyj294qidqnaba8y0ddncl9d672qlfyn09c5v3n") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.0.5 (c (n "rbatis-macro-driver") (v "3.0.5") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11kzlhf0h3s9r89ygzm75qj2cmsj7ay5sj6hbjag8w6adrqkq9w2") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.1.0 (c (n "rbatis-macro-driver") (v "3.1.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vzc3hzbrrnr63xlm5n1h6kl5jpk8131mm7bbhgl2ak512qsm6bm") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.1.1 (c (n "rbatis-macro-driver") (v "3.1.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3rlf4gzsqsl4wcplvg4v9y36hrc8rn3z98nnkarhp6z2j4i3qx") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.1.2 (c (n "rbatis-macro-driver") (v "3.1.2") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hz95sdp33a1yycjz1sgbbk6jdz55rxs0ik3n0c2dkzn6xib29b2") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-3.1.3 (c (n "rbatis-macro-driver") (v "3.1.3") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6jdvm3q18vvi3spg16a7wvfl4f131qk43f4jxnz03j2av8fv0c") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbatis-macro-driver-4.0.0 (c (n "rbatis-macro-driver") (v "4.0.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c3jlyk9z3wayjbndd9v5a29ivzx8jf3d7977rnhbg65ygpvn5ax") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.1 (c (n "rbatis-macro-driver") (v "4.0.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xri63v4b5yj6q3xcc0jbc9jc4b456q37yl4qxglmajzb992n1bg") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.2 (c (n "rbatis-macro-driver") (v "4.0.2") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2p8cyih998bianwm6d6x9bw7kkv8rgdlpnd7l0qrxy730n9a9d") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.3 (c (n "rbatis-macro-driver") (v "4.0.3") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r9r1kp7z05csm1r9fqfdmhw42vn5g97khi56hgnc0177ymk6pps") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.4 (c (n "rbatis-macro-driver") (v "4.0.4") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ihlsdwqky3a8zc6yhczl3hjbj2cl0p8gwfz22bcqp01nc8ixxx7") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.5 (c (n "rbatis-macro-driver") (v "4.0.5") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xypnmp38x7lkmn6c5r3w7q21v4412zvpgqfd06vyqwb1r6vpw3s") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.6 (c (n "rbatis-macro-driver") (v "4.0.6") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1880cb8ys977hxqrvx5vlpxgkxj9859k3pkhvyicm65hspbqyg7w") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.7 (c (n "rbatis-macro-driver") (v "4.0.7") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0716c7bpf4m6qjfj8lbicl1m7a81j0xzyhfpg3wd8874l0xv40ng") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.8 (c (n "rbatis-macro-driver") (v "4.0.8") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2lgh6dinrlsc0rqjy6k28aw5n8l3jwyav0qv1adxd9dy5gskqn") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.9 (c (n "rbatis-macro-driver") (v "4.0.9") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00pg3cj4gqbk5rsxvza7z3yv1k2h0mv4gi7sxfgccxn8x283p878") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.10 (c (n "rbatis-macro-driver") (v "4.0.10") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16v0y86a1xjpaz0n4zrp581zg8gf59d83d29xg3pphpj0jvf77zs") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.0.11 (c (n "rbatis-macro-driver") (v "4.0.11") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9x9lqn7sqwsnb94q66ndpvjh48vpxikw9v8q2qd95fcpvbj4aj") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.0.12 (c (n "rbatis-macro-driver") (v "4.0.12") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01hhjx46kz8qgccy2b0l260fmf2fycb3fnmd47qgyd9al6gg00fj") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rbatis-codegen/debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.0.13 (c (n "rbatis-macro-driver") (v "4.0.13") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wjm36fh7fd5b4fqplkprvzazs45nxgnyc9n8ssq4qxdpx3f5668") (f (quote (("default" "rbatis-codegen"))))))

(define-public crate-rbatis-macro-driver-4.0.14 (c (n "rbatis-macro-driver") (v "4.0.14") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i56sfw4qby8rlpdsljna03dls4v3cqz27w0yl7il0fdzvwz7rhm") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.0.15 (c (n "rbatis-macro-driver") (v "4.0.15") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fra4mvfwbln833p5rmp1v7ayxqw2v4x8lnid0r223sccx48d16x") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.0.16 (c (n "rbatis-macro-driver") (v "4.0.16") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y5dny3cxz7q1yvrmv8b1xyv4nahl92r719famrw4g2z9b2bd58n") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.0.17 (c (n "rbatis-macro-driver") (v "4.0.17") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d6j0ca8phgkwn67rbvarw9x88fzy7nkxmy0w2lf7ccwxp0m8grs") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.1.0 (c (n "rbatis-macro-driver") (v "4.1.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rkpfq8f2gsm6mzz9b5qmmykv51nwqwr7cgs20hq8ay3z741dncl") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.2.0 (c (n "rbatis-macro-driver") (v "4.2.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.2") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13vshdgrzp6b9jnn8gdn71i61bwfwajs6h4agxw5dbyv49lzbnmy") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.3.0 (c (n "rbatis-macro-driver") (v "4.3.0") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zgdly7hmjm298xhxjv0x6mrv4zrnxxjp7fa7vyq28jd7x5r26a5") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.3.1 (c (n "rbatis-macro-driver") (v "4.3.1") (d (list (d (n "html_parser") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w5x3lym88hmz2sc4kzr35n3w71fb59c5rx1ql2l70i06sbrgwk3") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.3.2 (c (n "rbatis-macro-driver") (v "4.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f3lls6d9km71c62gfnb0pi93ahaaaxkzsz7dqw9p7f0q5aygb2d") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.3.3 (c (n "rbatis-macro-driver") (v "4.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sns6w7sd697akkjjblsn3wlkp3lpwgr23ngx3isdk7c2hw07127") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.0 (c (n "rbatis-macro-driver") (v "4.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ynvs1yadyv39w3s6cmifi9x4vhphrmw3gpcrp7lvfx9p7vbfj0i") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.19 (c (n "rbatis-macro-driver") (v "4.4.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l80wfzvrgx2y5wi6iwcalxn6w8avphx0d8cq4d80py15ap2ffz8") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.20 (c (n "rbatis-macro-driver") (v "4.4.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.4") (d #t) (k 2)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rbs") (r "^4.4") (d #t) (k 2)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n52vgadfn8h17frn0qrvlgf092ih5m4pw68z1l5ln7bmd8jpjyf") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.21 (c (n "rbatis-macro-driver") (v "4.4.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.4") (d #t) (k 2)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rbs") (r "^4.4") (d #t) (k 2)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lj37wr3palgm7nhm82wz9p5c67m49g6g7vpb4s0j3wkkm8ddc7a") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.22 (c (n "rbatis-macro-driver") (v "4.4.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.4") (d #t) (k 2)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rbs") (r "^4.4") (d #t) (k 2)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qxc342ys8qp4q0559jp8ghwhdgiv305jf4s28dn384dlmgwr2qb") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.4.23 (c (n "rbatis-macro-driver") (v "4.4.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.4") (d #t) (k 2)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rbs") (r "^4.4") (d #t) (k 2)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yrgblywyb60i135dlf4jridsxy7d0vyxkhp716impdvw3llyr15") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format")))) (y #t)))

(define-public crate-rbatis-macro-driver-4.4.24 (c (n "rbatis-macro-driver") (v "4.4.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.4") (d #t) (k 2)) (d (n "rbatis-codegen") (r "^4.4") (o #t) (d #t) (k 0)) (d (n "rbs") (r "^4.4") (d #t) (k 2)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jjxsp3968y8bv8jqxj6j2d42yh43n01jnl16zyzf8l9j6wyrkjg") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.5.0 (c (n "rbatis-macro-driver") (v "4.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r24j9k7qv9bbk822gna1ad60y6jlwnlknlzdmllkb56lf73dapf") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format"))))))

(define-public crate-rbatis-macro-driver-4.5.1 (c (n "rbatis-macro-driver") (v "4.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cszm2flcypwzisabhp4l0bpnap727s35mdi42n0anb6z34lhs6m") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format" "rbatis-codegen"))))))

(define-public crate-rbatis-macro-driver-4.5.2 (c (n "rbatis-macro-driver") (v "4.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cy6clgwyfzxwb81hly9i37iq800izqsgqrwajnimf4g51biqb8z") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format" "rbatis-codegen"))))))

(define-public crate-rbatis-macro-driver-4.5.3 (c (n "rbatis-macro-driver") (v "4.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cywnfrqm2613xvzd8mx2rf3gnsvmmk5s3xdpnph5xz25ddd48pd") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format" "rbatis-codegen"))))))

(define-public crate-rbatis-macro-driver-4.5.4 (c (n "rbatis-macro-driver") (v "4.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgy3z9w9gng5sgfql60j8xxghlfd2b52lilhgdwqlr2gwmz5p1g") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format" "rbatis-codegen"))))))

(define-public crate-rbatis-macro-driver-4.5.5 (c (n "rbatis-macro-driver") (v "4.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rbatis-codegen") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vkx4izf3x0vd7zaghvjp7bbjphcvm1k5ikzka0m4vmqjz2yyz6s") (f (quote (("default" "rbatis-codegen") ("debug_mode" "rust-format" "rbatis-codegen"))))))

