(define-module (crates-io rb at rbatis_xml_parser) #:use-module (crates-io))

(define-public crate-rbatis_xml_parser-0.1.0 (c (n "rbatis_xml_parser") (v "0.1.0") (d (list (d (n "html5ever-atoms") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.3") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)) (d (n "tendril") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0hdq0a5iyxkfqz7yscmlg95yl5kvgqan7vg55mxj6mfgwbkgdihl") (f (quote (("unstable" "tendril/unstable"))))))

