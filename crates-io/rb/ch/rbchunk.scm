(define-module (crates-io rb ch rbchunk) #:use-module (crates-io))

(define-public crate-rbchunk-2.0.3 (c (n "rbchunk") (v "2.0.3") (h "0kpy35hbl84qfmbgyk48z9v8vzy25ijr4wmcrq2v1c20crmcpd4g")))

(define-public crate-rbchunk-2.1.0 (c (n "rbchunk") (v "2.1.0") (h "0s9qqga5k0napzvyl9jzwc8mgcsdyclc60vcass00jkqa7s2rjw2")))

