(define-module (crates-io rb n- rbn-lib) #:use-module (crates-io))

(define-public crate-rbn-lib-0.1.3 (c (n "rbn-lib") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "hambands") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ns5qjfd5029chylw4qcwls4lrclvcmhxr0hc7n5gwfc81yz4253")))

(define-public crate-rbn-lib-0.1.4 (c (n "rbn-lib") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "hambands") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q2g68wnvvqc0njiprcz6zn4apyd6sjmwsl5sy4y6qwlmnrl7mqg")))

