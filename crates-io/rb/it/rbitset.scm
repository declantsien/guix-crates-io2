(define-module (crates-io rb it rbitset) #:use-module (crates-io))

(define-public crate-rbitset-0.1.0 (c (n "rbitset") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)))) (h "1a2svnq6x1y3gbjnilfr3q47zpkadflf2fxgw7aifgikv4h75w5y")))

(define-public crate-rbitset-0.2.0 (c (n "rbitset") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)))) (h "1d52szwlnsksc47xa4hxq2k5lz54xkddhkhjgihxxkfcj1kbvlfw")))

(define-public crate-rbitset-0.2.1 (c (n "rbitset") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0khj0c9c5ia0iywrrcpqn8swq6a90w1m1jabamsrlbbq69zibq5n") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rbitset-0.3.0 (c (n "rbitset") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0h7s9hhi9m3f6nkmiw3fi5hf361kgx10h7zy5q84yys7c48nn7ij") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rbitset-0.3.1 (c (n "rbitset") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.8") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0dqnbxdvnfigfbxywd1xzb63g078hrwpjk53zal9win67s93ahhx") (s 2) (e (quote (("serde" "dep:serde"))))))

