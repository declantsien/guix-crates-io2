(define-module (crates-io rb xm rbxm) #:use-module (crates-io))

(define-public crate-rbxm-0.1.0 (c (n "rbxm") (v "0.1.0") (d (list (d (n "lz4_flex") (r "^0.7") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.1.0") (d #t) (k 0)))) (h "02y434i7n27cilrsnkfn4aqfs28rpxas526khrks3fdsb8rdfqr2") (f (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1.1 (c (n "rbxm") (v "0.1.1") (d (list (d (n "lz4_flex") (r "^0.7") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.1.0") (d #t) (k 0)))) (h "1asz1mwig13kc6d28li5nqjr5xv0w1n471vpfkiwir3klsqyh5p5") (f (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1.2 (c (n "rbxm") (v "0.1.2") (d (list (d (n "lz4_flex") (r "^0.7") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.1.0") (d #t) (k 0)))) (h "1y31f9lw8irx4hl38pd25s1svvig20jm387i91kq7a78h6gw6kd1") (f (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1.3 (c (n "rbxm") (v "0.1.3") (d (list (d (n "lz4_flex") (r "^0.7") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.1.0") (d #t) (k 0)))) (h "1fayihl2laqjx74fmv3z1c2gbw0yayhrh3w7sad3mb6fy7lj7sf6") (f (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.2.0 (c (n "rbxm") (v "0.2.0") (d (list (d (n "lz4_flex") (r "^0.9") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.2.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "1nmchhf2whkyjb8af7wpd3k4gnr5n8cp1spiinz9w8xcsddbbkzn") (f (quote (("unstable") ("std" "lz4_flex/std" "uuid/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.2.1 (c (n "rbxm") (v "0.2.1") (d (list (d (n "lz4_flex") (r "^0.9") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "rbxm-proc") (r "^0.2.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "1pq5vf6bs1bvjbvl3w07z3ly1pmlf7kh04mqp6d270n3r0y24y3x") (f (quote (("unstable") ("std" "lz4_flex/std" "uuid/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.3.0 (c (n "rbxm") (v "0.3.0") (d (list (d (n "lz4_flex") (r "^0.9") (f (quote ("safe-decode" "safe-encode"))) (k 0)) (d (n "num") (r "^0.4") (f (quote ("libm"))) (k 0)) (d (n "rbxm-proc") (r "^0.2.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (k 0)) (d (n "uuid") (r "^1.1") (k 0)))) (h "0xih2lipagq0gnzx53610r5n0j3mlsrci3xl9pw2qg1s6hm4r51h") (f (quote (("unstable") ("std" "lz4_flex/std" "uuid/std" "num/std") ("mesh-format") ("default" "std"))))))

