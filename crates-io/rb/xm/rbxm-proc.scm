(define-module (crates-io rb xm rbxm-proc) #:use-module (crates-io))

(define-public crate-rbxm-proc-0.1.0 (c (n "rbxm-proc") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "022rz18yqy8ndh5bg59r0lv6h61x5cgin1plgx66vahr0611na86")))

(define-public crate-rbxm-proc-0.2.0 (c (n "rbxm-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jfyybybkswysa6s8n6r47z9k300svs68nzfbg4indqq93yafp54")))

(define-public crate-rbxm-proc-0.2.1 (c (n "rbxm-proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gfpwbyi79j0k7whh8p8sx0d8d4h7vvcjyai3sr5zlcpavpglmc4")))

