(define-module (crates-io rb ot rbotlib) #:use-module (crates-io))

(define-public crate-rbotlib-0.0.1 (c (n "rbotlib") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "rbothal") (r "^0.0.1") (d #t) (k 0)))) (h "0lnpk6bqrxhwyjamypiwc36s195y2gb9g6kbndvpqwiccy52fhr7")))

(define-public crate-rbotlib-0.0.2 (c (n "rbotlib") (v "0.0.2") (d (list (d (n "rbothal") (r "^0.0.2") (d #t) (k 0)))) (h "100czwwl0nznszfvjlm864x5yi2hn9cr6ssx9q69lrx4n1vkx6yw")))

