(define-module (crates-io rb ot rbothal) #:use-module (crates-io))

(define-public crate-rbothal-0.0.1 (c (n "rbothal") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1q09p69pa1zc7h7gpg6av6il16ifk48yljyd0zz2az12mhqi5yx5")))

(define-public crate-rbothal-0.0.2 (c (n "rbothal") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "0yyphkgd9ncb7jdplv8jb882l7dqmk3jvh0ibavdkag7qsypay19")))

