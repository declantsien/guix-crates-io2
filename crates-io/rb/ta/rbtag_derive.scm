(define-module (crates-io rb ta rbtag_derive) #:use-module (crates-io))

(define-public crate-rbtag_derive-0.1.0 (c (n "rbtag_derive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.24") (d #t) (k 0)))) (h "0zyysry3splsf4s2rq3sypw5gnkdqfb5lnvkpcsi4589yqix37zm")))

(define-public crate-rbtag_derive-0.2.0 (c (n "rbtag_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.24") (d #t) (k 0)))) (h "0i1ycg6ljg2zvh1d0kg4bn5m34qdh621iql1c69lg42f1jad6h12")))

(define-public crate-rbtag_derive-0.3.0 (c (n "rbtag_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.24") (d #t) (k 0)))) (h "1i37apchv6zd4251iv5hhpazwy4cpmilgq0inbdqmjnc22vi2mdp")))

