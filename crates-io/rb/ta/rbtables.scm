(define-module (crates-io rb ta rbtables) #:use-module (crates-io))

(define-public crate-rbtables-0.1.1 (c (n "rbtables") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i10y0csh10zl5i7jhr6k2p9hyf38qk1bb4a2c2hpd3xgx3n4mb9")))

(define-public crate-rbtables-0.1.2 (c (n "rbtables") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ayq1xbi7kp8c9l8q4rgn04z37cn5dzrm2d3sf48mh8dqidx59ww")))

(define-public crate-rbtables-0.1.3 (c (n "rbtables") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0rgi6hbbwhxc6hn6kc63l5gpgrjxwh6z1s6gpz26w4nd1z587yhq")))

(define-public crate-rbtables-0.1.4 (c (n "rbtables") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0gngwgl8z4m1zhabbrb8gw0yvfc1ck4lnckqbjczx4kxjgypnny7")))

