(define-module (crates-io rb ta rbtag) #:use-module (crates-io))

(define-public crate-rbtag-0.1.0 (c (n "rbtag") (v "0.1.0") (d (list (d (n "rbtag_derive") (r "^0.1.0") (d #t) (k 0)))) (h "11pqryma48kmchsj72vhq4myxd519ws34yxjf6650736yvcq6vcb")))

(define-public crate-rbtag-0.2.0 (c (n "rbtag") (v "0.2.0") (d (list (d (n "rbtag_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1jsxbxv36kyd5dikg7c8wcqlfgpz86qq2crl6g86hgyrf0rvqdk8")))

(define-public crate-rbtag-0.3.0 (c (n "rbtag") (v "0.3.0") (d (list (d (n "rbtag_derive") (r "^0.3.0") (d #t) (k 0)))) (h "18rja4v5v8xngnpqippg0k3ckkpm7l7h544x1a4i3f60zhv4kikj")))

