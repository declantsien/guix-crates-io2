(define-module (crates-io rb re rbrew) #:use-module (crates-io))

(define-public crate-rbrew-0.0.1 (c (n "rbrew") (v "0.0.1") (d (list (d (n "argp") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1sl188wx8hwpnfsa9jf0x3gq361p92l7di85z6254js7a8ncj9jm") (y #t)))

(define-public crate-rbrew-0.0.2 (c (n "rbrew") (v "0.0.2") (d (list (d (n "argp") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "05s6dcwsk2lnnd3gg9593zv8r0p2dx6v9m1kvf6v5cs5lwh6pyyy") (y #t)))

