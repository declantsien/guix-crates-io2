(define-module (crates-io rb re rbrep) #:use-module (crates-io))

(define-public crate-rbrep-0.1.0 (c (n "rbrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0wsnlslhr7mqx3cxk6wf171kyn5fb3nzqjma313g73i2yr2y23cc")))

