(define-module (crates-io rb x_ rbx_reflection) #:use-module (crates-io))

(define-public crate-rbx_reflection-0.1.0 (c (n "rbx_reflection") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^0.3.0") (d #t) (k 0)))) (h "0p7qmmnsg57sjxmgsf10khfchzflajsq665g8rwy1hnby36xmapl")))

(define-public crate-rbx_reflection-0.2.373 (c (n "rbx_reflection") (v "0.2.373") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^0.3.0") (d #t) (k 0)))) (h "137jkaaa5j7id05lqzvw5lvlxwg0ga2cb6zhz9v3rxdv2ad5g5ya")))

(define-public crate-rbx_reflection-1.0.373 (c (n "rbx_reflection") (v "1.0.373") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^0.3.0") (d #t) (k 0)))) (h "0as4lad6vsvgnnczdr9jnyzg63h6zda6ywgz1sz66w095vf9jx5k")))

(define-public crate-rbx_reflection-2.0.374 (c (n "rbx_reflection") (v "2.0.374") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)))) (h "1zh9z55k7f2197fnv97mj6bpg2ksvkqpd5vzfbdm8fxkd7w6z0la")))

(define-public crate-rbx_reflection-2.0.377 (c (n "rbx_reflection") (v "2.0.377") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)))) (h "14lyymm46vb2w15wn72qkz3spiaa07k1rpa8xs80alw100k4kfi1")))

(define-public crate-rbx_reflection-3.0.384 (c (n "rbx_reflection") (v "3.0.384") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03q1whzkn4hxmdydkzi68i7i3sai244c34b517wdvghs5xdf9w4b")))

(define-public crate-rbx_reflection-3.1.384 (c (n "rbx_reflection") (v "3.1.384") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k0n86v6pacp5zfzybdznc9yqwaikfmpnhpqy4k848xr0qc3c28j")))

(define-public crate-rbx_reflection-3.1.388 (c (n "rbx_reflection") (v "3.1.388") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fa5gwy07965yb528cxfa597x6p862665zbqaw75f094mb8hfl3i")))

(define-public crate-rbx_reflection-3.2.389 (c (n "rbx_reflection") (v "3.2.389") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bn7ix7zwnnwsi0mbksz0hh35fz7yqi7697sq6f72vrq57p75pwx")))

(define-public crate-rbx_reflection-3.2.390 (c (n "rbx_reflection") (v "3.2.390") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01bkdb6vz1faxrzh4cx7fbzc53vpch163zs53qs2a60gwzcy002g")))

(define-public crate-rbx_reflection-3.2.395 (c (n "rbx_reflection") (v "3.2.395") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15vrr9k4ixawnksh1p0nsjbvihd0drmvk71mfhbrz7n2x763rd73")))

(define-public crate-rbx_reflection-3.2.399 (c (n "rbx_reflection") (v "3.2.399") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04vq6gnbb9gpzq0fk9ps59z1mhvf9m68mvygzq2xjmmdxbd0mpqk")))

(define-public crate-rbx_reflection-3.2.404 (c (n "rbx_reflection") (v "3.2.404") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1368mvmryhz95rzjgipz71ds6nqyy8qw16y2a5gyq935msjw04kp")))

(define-public crate-rbx_reflection-3.3.408 (c (n "rbx_reflection") (v "3.3.408") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xqj7clhz83lyxim9ypsji5qva7wsn8nxlvj30xgs73c03wlghd4")))

(define-public crate-rbx_reflection-3.3.418 (c (n "rbx_reflection") (v "3.3.418") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iywfawj02b8smrvkxx9vb7r1p2f6ndmy1lam5xzysbxdxr7rh08")))

(define-public crate-rbx_reflection-3.3.454 (c (n "rbx_reflection") (v "3.3.454") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i7d7s3ri4lj6gc5j537kagb46pikf3n07zyxjl17svmlvidvk79")))

(define-public crate-rbx_reflection-4.0.0-alpha.1 (c (n "rbx_reflection") (v "4.0.0-alpha.1") (d (list (d (n "rbx_types") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ia6j2m6sk8ws7b7v5c9cjsf6fi3y86nysj0p7asm44i4cnn6hvw")))

(define-public crate-rbx_reflection-3.3.482 (c (n "rbx_reflection") (v "3.3.482") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1npik0b4zykqksw9syhnpaigy5ra65xd7h4qizfsrizp0f1ggfr8")))

(define-public crate-rbx_reflection-4.0.0 (c (n "rbx_reflection") (v "4.0.0") (d (list (d (n "rbx_types") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11yndj9ai3v8xg5g8xaip19yaim331r27qhc32a12whybh9gc9bv") (y #t)))

(define-public crate-rbx_reflection-4.1.0 (c (n "rbx_reflection") (v "4.1.0") (d (list (d (n "rbx_types") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10icn474jvfd0y5jwvn3925i4hmq9i3ac0l0h0adb0fa5c3z0j2k")))

(define-public crate-rbx_reflection-4.2.0 (c (n "rbx_reflection") (v "4.2.0") (d (list (d (n "rbx_types") (r "^1.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m8iwvq8202xmqwcz9mhc8nxyj6mxfcmxqz480jskcm3r145g4d8")))

(define-public crate-rbx_reflection-4.3.0 (c (n "rbx_reflection") (v "4.3.0") (d (list (d (n "rbx_types") (r "^1.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hq3b3zqmgcj3adqbc4hk3mmnz9xkyi3ypw0izn6y14jf544ig88")))

(define-public crate-rbx_reflection-4.4.0 (c (n "rbx_reflection") (v "4.4.0") (d (list (d (n "rbx_types") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hzqp82k6p9x4qrjc2hslvd6wqfil0pxwcd6gp9x45rjrbgn5rs1")))

(define-public crate-rbx_reflection-4.5.0 (c (n "rbx_reflection") (v "4.5.0") (d (list (d (n "rbx_types") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08hpzr3mp6bn37v20j8nk3rh94ibmrg7jskld8ksflqvk6f50h8d")))

