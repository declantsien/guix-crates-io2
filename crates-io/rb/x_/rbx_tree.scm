(define-module (crates-io rb x_ rbx_tree) #:use-module (crates-io))

(define-public crate-rbx_tree-0.1.0 (c (n "rbx_tree") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1ixfr9ml8qi8r8ghhiriahbnshyx4jylvfgxx4dam37lkf2j6mza")))

(define-public crate-rbx_tree-0.2.0 (c (n "rbx_tree") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0qh4fvv2a23p4i3zryyk7kcshagwk5a2lf0ff82lg1pzwbrmwq3v")))

