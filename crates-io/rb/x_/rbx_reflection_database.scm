(define-module (crates-io rb x_ rbx_reflection_database) #:use-module (crates-io))

(define-public crate-rbx_reflection_database-0.1.0+roblox-465 (c (n "rbx_reflection_database") (v "0.1.0+roblox-465") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.0.0-alpha.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0g171xdr89f6sf4f10h7yd2yzzcrhwlhlabw4fvnq0f2r2hyrjdw")))

(define-public crate-rbx_reflection_database-0.1.1+roblox-478 (c (n "rbx_reflection_database") (v "0.1.1+roblox-478") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.0.0-alpha.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1jll72jgig7i5q5hxwl07b4ixdaxvldxc92mp442czp77x1hayq2")))

(define-public crate-rbx_reflection_database-0.2.0+roblox-478 (c (n "rbx_reflection_database") (v "0.2.0+roblox-478") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.0.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0i2gbnbd0prbliv8ir3gy95n6082jyj8j6waf50pfjh7biq2gqlv") (y #t)))

(define-public crate-rbx_reflection_database-0.2.1+roblox-484 (c (n "rbx_reflection_database") (v "0.2.1+roblox-484") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.1.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1mym9455w3gzqcx0y8646wdjbbdy65gvrqc2w7kg31hsl19djbyq")))

(define-public crate-rbx_reflection_database-0.2.2+roblox-498 (c (n "rbx_reflection_database") (v "0.2.2+roblox-498") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0c6g431gm8ga15w6njmv27lfinidaqm75fyk5896915yhzf5kr74")))

(define-public crate-rbx_reflection_database-0.2.3+roblox-503 (c (n "rbx_reflection_database") (v "0.2.3+roblox-503") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1n0qgqf1csn7fzpyhpjxfpqk59bsdss5n59zyws4xhs7lbf21xqk")))

(define-public crate-rbx_reflection_database-0.2.4+roblox-504 (c (n "rbx_reflection_database") (v "0.2.4+roblox-504") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1qg19mgh94ycsghsyarj5rnpi4k9v7bnv3plrq2cv5x6bal8s7ml")))

(define-public crate-rbx_reflection_database-0.2.5+roblox-530 (c (n "rbx_reflection_database") (v "0.2.5+roblox-530") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1cds9wh1s05riscm8p7vwvy5rsvmmx6424kykrk37vidr70hly9y")))

(define-public crate-rbx_reflection_database-0.2.6+roblox-572 (c (n "rbx_reflection_database") (v "0.2.6+roblox-572") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0nzxipcmrl92bj4zix1acb75drk77ag48wifpwiakhphmzi702kq")))

(define-public crate-rbx_reflection_database-0.2.7+roblox-588 (c (n "rbx_reflection_database") (v "0.2.7+roblox-588") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.3.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0xkj917bgm2qbbh5l0xfy68pb5zyx8gxv6a8ml8nz04285kwzrhv")))

(define-public crate-rbx_reflection_database-0.2.8+roblox-296 (c (n "rbx_reflection_database") (v "0.2.8+roblox-296") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "041wyfzij11y9nw04r88hxyg8k1nncrjia5h95lzxiixd10slj7z")))

(define-public crate-rbx_reflection_database-0.2.9+roblox-596 (c (n "rbx_reflection_database") (v "0.2.9+roblox-596") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0rlfnbdx38fnjmylkwqrxy51hlybz9pmnyy99qr6damlla4g063b")))

(define-public crate-rbx_reflection_database-0.2.10+roblox-607 (c (n "rbx_reflection_database") (v "0.2.10+roblox-607") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.5.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "185f63ydl62pxn0107n6s3sc5cljyma86p00g7famxs1z830rqhj")))

