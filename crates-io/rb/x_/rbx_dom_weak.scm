(define-module (crates-io rb x_ rbx_dom_weak) #:use-module (crates-io))

(define-public crate-rbx_dom_weak-0.3.0 (c (n "rbx_dom_weak") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0fp6xcdr6sjnmvp8yjl79wyn2f7yngg4grpjvf92kwgj4g3bwyx6")))

(define-public crate-rbx_dom_weak-0.3.1 (c (n "rbx_dom_weak") (v "0.3.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0f1sylhwqnn540g1yj20rdh1lw946i11qrh78rn2z55s7l2hcdk7")))

(define-public crate-rbx_dom_weak-1.0.0 (c (n "rbx_dom_weak") (v "1.0.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0pvmilxk4mwaq3vpq1w1gvp36w8q21wq0lds28mhmmgy8974k2h8")))

(define-public crate-rbx_dom_weak-1.1.0 (c (n "rbx_dom_weak") (v "1.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1g49w1fxvykpspriilpwyshlg6jzb3l06fisfgma6pz8c0422ky4")))

(define-public crate-rbx_dom_weak-1.2.0 (c (n "rbx_dom_weak") (v "1.2.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "011vhfaxk0sgd69yy0dy2xslabaac4rhp8d8airabcdf7hic67p3")))

(define-public crate-rbx_dom_weak-1.3.0 (c (n "rbx_dom_weak") (v "1.3.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "07y3vicxikkzq07y5bl3jmsjpy6c8radrrixrqzzl25wb8cbm8pj")))

(define-public crate-rbx_dom_weak-1.4.0 (c (n "rbx_dom_weak") (v "1.4.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1rqiaqs08hh8vxyf1jns4pbiah8jhfcf7vz14dcmlfgbwnvdr0fm")))

(define-public crate-rbx_dom_weak-1.5.0 (c (n "rbx_dom_weak") (v "1.5.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "09rc4pp2wn1rafm3k1zsjxxyp5rx7lpwa2f23cccsbi5wdvb9azq")))

(define-public crate-rbx_dom_weak-1.6.0 (c (n "rbx_dom_weak") (v "1.6.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1krwvqqv9krqrkg9zj9yl5jzixz887bj7qhgwpy87hla5rc95byr")))

(define-public crate-rbx_dom_weak-1.7.0 (c (n "rbx_dom_weak") (v "1.7.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0c20fn3pp6gb8v5mqaags3lvcjm30jmhxfc9l6z9k3d2bsljwn79")))

(define-public crate-rbx_dom_weak-1.8.0 (c (n "rbx_dom_weak") (v "1.8.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0fs1d63hjf8yvfway0lds8c21ldznyd11b3ab7ljl9awrm87kbh8")))

(define-public crate-rbx_dom_weak-1.8.1 (c (n "rbx_dom_weak") (v "1.8.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "18h82vm44g28195d701mmyji9qbzq8zdwds9dyv5r9vji0c25yr8")))

(define-public crate-rbx_dom_weak-1.8.2 (c (n "rbx_dom_weak") (v "1.8.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0m355bwnwvhjas23v0wlmzmw4fml9aigs5y1wdpl23ihpmxjjk2y")))

(define-public crate-rbx_dom_weak-1.9.0 (c (n "rbx_dom_weak") (v "1.9.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1i0fmkrm0w185yz92ilipihyxbx6szsn31iab0cbv5h0hpz7k05v")))

(define-public crate-rbx_dom_weak-1.10.0 (c (n "rbx_dom_weak") (v "1.10.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0bgxwlgal5sv6rgcfcxvp8w6yifswackxqv4l48hikxzsy1wkdy6")))

(define-public crate-rbx_dom_weak-1.10.1 (c (n "rbx_dom_weak") (v "1.10.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1i3q7dazx8nbp4jj1l8lkazfd42m39h4ijk41f6b6g1gfk76b2wp")))

(define-public crate-rbx_dom_weak-2.0.0-alpha.1 (c (n "rbx_dom_weak") (v "2.0.0-alpha.1") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "rbx_types") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)))) (h "159swiyzrnw87xcgq159wmy194q1j280fx2x62z7r1k9mva1dyjz")))

(define-public crate-rbx_dom_weak-2.0.0 (c (n "rbx_dom_weak") (v "2.0.0") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "rbx_types") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)))) (h "1xsqpwwpyw50mr8hi4j2im25113sj1cfclxhipx5nl3fy0x9b8h4") (y #t)))

(define-public crate-rbx_dom_weak-2.1.0 (c (n "rbx_dom_weak") (v "2.1.0") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "rbx_types") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)))) (h "0w3ng7bd2yhgvbr318gzk30rmnz93k18yljrqg0njaqg88h3izfr")))

(define-public crate-rbx_dom_weak-2.2.0 (c (n "rbx_dom_weak") (v "2.2.0") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "rbx_types") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)))) (h "1rf173z5lq6a4y0dghclnwi5kzl6j0jxn7kvvl079wb80918qnnn")))

(define-public crate-rbx_dom_weak-2.3.0 (c (n "rbx_dom_weak") (v "2.3.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "rbx_types") (r "^1.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "07rdjbnd9ik9c7lhf0l1569nf1bpfizyii42bh3dfc4gn57m4zvg")))

(define-public crate-rbx_dom_weak-2.4.0 (c (n "rbx_dom_weak") (v "2.4.0") (d (list (d (n "insta") (r "^1.14.1") (d #t) (k 2)) (d (n "rbx_types") (r "^1.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0cvgy8x59y43wqs6b2ywz3ri0jndyfd0ysjw9xpp13c5zlw25k5p")))

(define-public crate-rbx_dom_weak-2.5.0 (c (n "rbx_dom_weak") (v "2.5.0") (d (list (d (n "insta") (r "^1.14.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "rbx_types") (r "^1.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1ffdrbf3aiz1mg4zv7ldidnz02hd2rmcmg77gijzl35vkd22j5qn")))

(define-public crate-rbx_dom_weak-2.6.0 (c (n "rbx_dom_weak") (v "2.6.0") (d (list (d (n "insta") (r "^1.14.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "rbx_types") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0d2bhq32as006rrn8fby061kikxmskcjhwizjhjkcqj62h72wfl4")))

(define-public crate-rbx_dom_weak-2.7.0 (c (n "rbx_dom_weak") (v "2.7.0") (d (list (d (n "insta") (r "^1.14.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "rbx_types") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "05n39rvyxr730h6r0kbzrxbirxr7d41pnm2p7hp9r14rmimvarwv")))

