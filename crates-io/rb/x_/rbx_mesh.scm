(define-module (crates-io rb x_ rbx_mesh) #:use-module (crates-io))

(define-public crate-rbx_mesh-0.1.0 (c (n "rbx_mesh") (v "0.1.0") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)))) (h "02bfp3m7dr19h6nbmhsz2f6j9z1giijsfja0wbdflml54qzxw5w8")))

(define-public crate-rbx_mesh-0.1.1 (c (n "rbx_mesh") (v "0.1.1") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)))) (h "1l7jcxrzj06flmjaxdz6bh19ysqwm8aiyvqp2dfy3d7m6g8cmxdr")))

