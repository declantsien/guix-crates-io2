(define-module (crates-io rb ts rbtset) #:use-module (crates-io))

(define-public crate-rbtset-1.0.0 (c (n "rbtset") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1kzl6wzbhlc3rdvillzbff5krv6ivghvcmmdlp40zndhvfzl31wv")))

(define-public crate-rbtset-1.0.1 (c (n "rbtset") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1maaha2c14b091d4w34zs2r7s4h8ggcf39g3wiiak9w0jnahjh9k")))

(define-public crate-rbtset-1.0.2 (c (n "rbtset") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1nl098yq2rxc0zd2k4klfm0mcll4ki0smh23dl6388hjiipqvjmw")))

