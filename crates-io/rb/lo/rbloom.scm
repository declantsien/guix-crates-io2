(define-module (crates-io rb lo rbloom) #:use-module (crates-io))

(define-public crate-rbloom-0.1.0 (c (n "rbloom") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0qp0q7jrkgyaj5a8hmk96amwsbj29w53yrlbci13445h2f8v6y8w")))

