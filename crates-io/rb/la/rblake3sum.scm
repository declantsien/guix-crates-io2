(define-module (crates-io rb la rblake3sum) #:use-module (crates-io))

(define-public crate-rblake3sum-0.4.0 (c (n "rblake3sum") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (f (quote ("traits-preview"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "16i5jwcryc3bbamsv6y05kq90i10k1zal879lsahpj7x44kzzixx")))

