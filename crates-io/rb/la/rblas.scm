(define-module (crates-io rb la rblas) #:use-module (crates-io))

(define-public crate-rblas-0.0.1 (c (n "rblas") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "12lv3agzjnnfyfn7gnj94x41982h7wqsl9k08rk746hvwaqq0s17")))

(define-public crate-rblas-0.0.2 (c (n "rblas") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0v7y6z6n7iqj2srmhr8zaix2wwk947dml3a5qjbwj1asw2v87qri")))

(define-public crate-rblas-0.0.4 (c (n "rblas") (v "0.0.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "09c7bk02wp8i6s6v2gb9bhb1zd3m2miqvyrn9r54zjzhgly9g5c9")))

(define-public crate-rblas-0.0.5 (c (n "rblas") (v "0.0.5") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "09slmzylwzpavnizsszi6v74l7q0z15mqv7phyk5dvsnyvv40q9a")))

(define-public crate-rblas-0.0.6 (c (n "rblas") (v "0.0.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0a3pmv21xz4pch3vp2xmfcygg2dj9cd5wmynzmn6zblfzmqpy6cd")))

(define-public crate-rblas-0.0.7 (c (n "rblas") (v "0.0.7") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "01kawxv6asf4lbb547armls3h52pwkdyafx02v276kvzcni1rb8d")))

(define-public crate-rblas-0.0.8 (c (n "rblas") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1kbkj62w84hyx36hpbnhwgirxyz1221rlbygzxqbg2s11yxnzhbb")))

(define-public crate-rblas-0.0.9 (c (n "rblas") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1gi2wgwvy7931pxflgn3igpqmigbzyzkzixh4ip8r0yr3psidlpv")))

(define-public crate-rblas-0.0.10 (c (n "rblas") (v "0.0.10") (d (list (d (n "libc") (r "~0.1.10") (d #t) (k 0)) (d (n "num") (r "~0.1.27") (d #t) (k 0)))) (h "1jqsrxm5zcz2qpsdib2yivd71g52ir96b2blq54a7w11wfrk3b8j")))

(define-public crate-rblas-0.0.11 (c (n "rblas") (v "0.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "00ydr9hixslxw9wykzjiqbspa0jz24bwywck14w5sgbbmjx6k0r1")))

(define-public crate-rblas-0.0.12 (c (n "rblas") (v "0.0.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "10hifv4pymij52dhv6z0hf6is87qb9q6pp5cr7lq21x6yrmqiqrr")))

(define-public crate-rblas-0.0.13 (c (n "rblas") (v "0.0.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "07c4wrwi6wp6fg2077kzxx3vh72jjfbdwak7yj9hmd63256kmx52")))

