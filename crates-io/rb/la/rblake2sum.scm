(define-module (crates-io rb la rblake2sum) #:use-module (crates-io))

(define-public crate-rblake2sum-0.1.0 (c (n "rblake2sum") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "blake2") (r "^0.8") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1w6zlwq4ximd20fg1lhy2pl6rrd3m1gmvxdan0r8vfxw91p3ik59")))

(define-public crate-rblake2sum-0.2.0 (c (n "rblake2sum") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "blake2") (r "^0.8") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1w76kjs7yfkafk6wi66pxlikh1f5ayzh7a5jqc3gvan3q8hvnqz4")))

(define-public crate-rblake2sum-0.2.1 (c (n "rblake2sum") (v "0.2.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2") (r "^0.8") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0k0907wjwsc92f27jk3dnb71zswar7vhirf9dyx539asd88738lw")))

(define-public crate-rblake2sum-0.3.0 (c (n "rblake2sum") (v "0.3.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nvzr1l7cdrh9mlajm5crvislxxy3pjsxbr1b58dimn7mvm1m579")))

(define-public crate-rblake2sum-0.3.1 (c (n "rblake2sum") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "08pzmd6c7b6dx9pzv1chivp40vfnl3ykcih0726376fng81z52h2")))

