(define-module (crates-io rb ar rbar) #:use-module (crates-io))

(define-public crate-rbar-1.0.0 (c (n "rbar") (v "1.0.0") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "085k6gfzhrm4nq07bj6y9qfvyks7287ycjmahfi6bcjc597iijr9") (y #t)))

(define-public crate-rbar-1.0.1 (c (n "rbar") (v "1.0.1") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "125z7z5z855f4n5cji0pqdpfjhkiy25ffpwfi73bq28jzy3a9bd1") (y #t)))

(define-public crate-rbar-1.0.2 (c (n "rbar") (v "1.0.2") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "167vq5f47fjfzppq805yskhms84hq3znw9yxgwr2gagj7bsgif9c") (y #t)))

(define-public crate-rbar-1.0.3 (c (n "rbar") (v "1.0.3") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "11sspdyvw4ankwyknj7lfw44xkqbyfb7l17jd7j1x5nw94vc71c8") (y #t)))

(define-public crate-rbar-1.0.4 (c (n "rbar") (v "1.0.4") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1akmbxdid4mb9r9zad76p7psnyv9sp3d32faaakm49zp4g58c9cl") (y #t)))

(define-public crate-rbar-1.1.0 (c (n "rbar") (v "1.1.0") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0bscsqji6llqs7m8vbglvi34176y0z5p7n5qxq1sfv4caag7jg7i") (y #t)))

(define-public crate-rbar-1.1.1 (c (n "rbar") (v "1.1.1") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1lc35lp2y3skd2kk6wizpl105prcx1sl9hh8rnfacj4zz551ip57") (y #t)))

(define-public crate-rbar-0.0.0 (c (n "rbar") (v "0.0.0") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1c4q13x4a2pax98kia4x71ljcgyfnwmm6gqxk346f23lahhj4p11")))

(define-public crate-rbar-0.1.0 (c (n "rbar") (v "0.1.0") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "11c28dlca1wylblcifksw34ffivkhqic0gcj6w1as891ypna411j")))

(define-public crate-rbar-0.1.1 (c (n "rbar") (v "0.1.1") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0v656vih5pb6b5ygbhqh210z9im6s07a64mckhj2fdz633z36227")))

(define-public crate-rbar-0.1.2 (c (n "rbar") (v "0.1.2") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0i6pfhc32yfs6jflq9d2izn7v784cq2d1ns9zdradpgmbn3cvcps")))

(define-public crate-rbar-0.2.0 (c (n "rbar") (v "0.2.0") (d (list (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1s1babdl89j218q0n2cnzjmi3pp2wp80x697yljayapl5s9vlrh8")))

(define-public crate-rbar-0.2.1 (c (n "rbar") (v "0.2.1") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1d4n14r96x4spad8v2hzkfhjklplir3v6v3cjm7bwx4wzskxxn31")))

(define-public crate-rbar-0.2.2 (c (n "rbar") (v "0.2.2") (d (list (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0pgw5wm07h93mfa49pl9srz7hd3nwshgjbnmpprrg8j5djich7iy")))

