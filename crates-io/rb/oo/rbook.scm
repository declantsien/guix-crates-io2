(define-module (crates-io rb oo rbook) #:use-module (crates-io))

(define-public crate-rbook-0.0.0 (c (n "rbook") (v "0.0.0") (h "1h9445ln9x4x5l5xmi65rja1j4sivfkx8hm6jmhbx9ld06s5vh0x") (y #t)))

(define-public crate-rbook-0.1.0 (c (n "rbook") (v "0.1.0") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0vjryvby8gs3ndgf6w0n0mpgm1dwfm51wrz5vdxvdxg119mzs2yz") (f (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (y #t)))

(define-public crate-rbook-0.1.1 (c (n "rbook") (v "0.1.1") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0cj9wsnjpnl2cdgx8avs1873igx0dcvgs2xq7dv3mbd1q2836mbz") (f (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (y #t)))

(define-public crate-rbook-0.1.2 (c (n "rbook") (v "0.1.2") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0wsimxpkfqd198fb5dzaqp7dlv3nvp39w4kcm1l2m8f4rg4alsdh") (f (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (y #t)))

(define-public crate-rbook-0.1.3 (c (n "rbook") (v "0.1.3") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0acg0n4f5505c1147kiyy5s4sq7z2bpxx59ybmb9vnj9c95aagfg") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.1.4 (c (n "rbook") (v "0.1.4") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "13v7mzh07d5p7gk2vhfi2r6jzb20ah903cimsk90dd9rphggwrnx") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.1.5 (c (n "rbook") (v "0.1.5") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0ga6rhb93fgrk0lf8y2nh41cbcvif2j3hhsf963ljrkk0jl5zwj0") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.2.0 (c (n "rbook") (v "0.2.0") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "02dyfaxh48648ghg8zmh40cybjh6gmabggj8rzd4ydnq1scsz9sb") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3.0 (c (n "rbook") (v "0.3.0") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "09l3x4m4y6c4jqn0ml9kn8d7rym0yqjf1czj703gvp7q2sy69qzf") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3.1 (c (n "rbook") (v "0.3.1") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "00xs92pp829gxfas7bikyyrzk28f8yjpsda1xndjmb11vwcbxra2") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3.2 (c (n "rbook") (v "0.3.2") (d (list (d (n "lol_html") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0bcwgkbv5pd420jddxm0q132xxslhj2n1v60sg8v7mvbwq60jzji") (f (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.4.0 (c (n "rbook") (v "0.4.0") (d (list (d (n "lol_html") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "000r9hq4ljk84qsxmgs2qr2km0krpl9lp5a8n5qvmc6ai2wf57fl") (f (quote (("statistics") ("reader") ("multi-thread") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.5.0 (c (n "rbook") (v "0.5.0") (d (list (d (n "lol_html") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0pv0yi7mrrsq8b9fbi5wwddri8xhhv03bhkhih85raraglq3n8gp") (f (quote (("statistics") ("reader") ("multi-thread") ("default" "reader" "statistics"))))))

