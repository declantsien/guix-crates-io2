(define-module (crates-io rb oo rboot) #:use-module (crates-io))

(define-public crate-rboot-0.1.0 (c (n "rboot") (v "0.1.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1ab8abvg75dgh7aq6my249lcwnkyc5bs6jkqqlnwlinpkgbfrdbb") (r "1.56")))

(define-public crate-rboot-0.1.1 (c (n "rboot") (v "0.1.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1925d797l2d1ylw46hx715ansilinvn3ss2ay7k9l1xgzw2qaf6i") (r "1.56")))

(define-public crate-rboot-0.1.2 (c (n "rboot") (v "0.1.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1540l57myp0lh7y6v58g0lcmmli7v0grigcb1iiszv512iy2gkjp") (r "1.56")))

