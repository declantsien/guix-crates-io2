(define-module (crates-io rb dc rbdc-pool-mobc) #:use-module (crates-io))

(define-public crate-rbdc-pool-mobc-4.5.1 (c (n "rbdc-pool-mobc") (v "4.5.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1n8dnmvx1hcwdsxadkf5hdgvn20vasaws6alzyiii9h2ic0wz8fg") (y #t)))

(define-public crate-rbdc-pool-mobc-4.5.2 (c (n "rbdc-pool-mobc") (v "4.5.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0vfk2p2i289v9bzv77dvp4g53f7cnzi88qpx00m1xx1fhpr9ja02") (y #t)))

(define-public crate-rbdc-pool-mobc-4.5.3 (c (n "rbdc-pool-mobc") (v "4.5.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0znwsv4iad038svvx44zqgyn66d8ylhgf3l5zis6jacgis5ir57s") (y #t)))

(define-public crate-rbdc-pool-mobc-4.5.4 (c (n "rbdc-pool-mobc") (v "4.5.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "02iyf8kqz5rpplsjknp01j4zcy905grxkxxbn03za09hv8rpqwil") (y #t)))

(define-public crate-rbdc-pool-mobc-4.5.5 (c (n "rbdc-pool-mobc") (v "4.5.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1k5njw3vm09hvd4j6acflyhqnxw3lxbsylynxbcr2sp9219l2b4b")))

(define-public crate-rbdc-pool-mobc-4.5.6 (c (n "rbdc-pool-mobc") (v "4.5.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0li4w7nqbi99jbl0ir08v5dfpxz5il0mgn6mfl7cxa80l3p683b4")))

(define-public crate-rbdc-pool-mobc-4.5.7 (c (n "rbdc-pool-mobc") (v "4.5.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0kl24pkj0r7kqd13rhxn3gwqxlibhy213dwsv4kj4b331zmps9pp")))

