(define-module (crates-io rb dc rbdc-pool-deadpool) #:use-module (crates-io))

(define-public crate-rbdc-pool-deadpool-4.5.2 (c (n "rbdc-pool-deadpool") (v "4.5.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1vhd6194kmyx65xvzsw1dar9jxc58acpfkj0xkqp748gbvmps6n6")))

(define-public crate-rbdc-pool-deadpool-4.5.3 (c (n "rbdc-pool-deadpool") (v "4.5.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (d #t) (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1n9iklp2am3ca2g7q3kjbdr2as2bklzy48la4dsp4pdr4vmsl5g6")))

(define-public crate-rbdc-pool-deadpool-4.5.6 (c (n "rbdc-pool-deadpool") (v "4.5.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "06a9l07k2s6m5l261mzhrwpn4mf0f17wqxdw1q0dx5kjcrr1xwva")))

(define-public crate-rbdc-pool-deadpool-4.5.7 (c (n "rbdc-pool-deadpool") (v "4.5.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deadpool") (r "^0.10") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0mnb1f7vbhpzwyvxxi433fn6sh9nhlk4nr4m6m37kpr69gca44sh")))

