(define-module (crates-io rb dc rbdc-pool-fast) #:use-module (crates-io))

(define-public crate-rbdc-pool-fast-4.5.0 (c (n "rbdc-pool-fast") (v "4.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1lxxmwqhmmnns5wsd8k3iibn5pafi0mqs2mwm17z0xqinydhhz5b") (y #t)))

(define-public crate-rbdc-pool-fast-4.5.1 (c (n "rbdc-pool-fast") (v "4.5.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0iagnfq3qymjgnki7n79ihjyyxnh0y9rxg4yb06s3g9ib9i8rwzc") (y #t)))

(define-public crate-rbdc-pool-fast-4.5.2 (c (n "rbdc-pool-fast") (v "4.5.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1mnyfdhp8yidm3fkxfd1wa4qvj89kz74kinf0x3cqyklggwf3w1d") (y #t)))

(define-public crate-rbdc-pool-fast-4.5.3 (c (n "rbdc-pool-fast") (v "4.5.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0h2pbw56928jpn4l98j9l0bw5pdbwhn4f6wh3h27l905mgjlahl2")))

(define-public crate-rbdc-pool-fast-4.5.4 (c (n "rbdc-pool-fast") (v "4.5.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "1j36a0gg39nlqkn4nd0mvhxp499r62v5bw2vi73x3fxrgqqb7nb9")))

(define-public crate-rbdc-pool-fast-4.5.5 (c (n "rbdc-pool-fast") (v "4.5.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "05aqs8j6s26p3ljzz7rn06ggyr2f54596mk5iylnkv7dci3lrzia")))

(define-public crate-rbdc-pool-fast-4.5.6 (c (n "rbdc-pool-fast") (v "4.5.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0yby8ncimfjklg8rc7240mhyxx266dk2ah8zl7klrrxy7wqfg6rz")))

(define-public crate-rbdc-pool-fast-4.5.7 (c (n "rbdc-pool-fast") (v "4.5.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dark-std") (r "^0.2") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0zygm1n29w61fxfbj049rcas6d5kj2810pbqmminbbmgkyi73gzr") (y #t)))

(define-public crate-rbdc-pool-fast-4.5.8 (c (n "rbdc-pool-fast") (v "4.5.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dark-std") (r "^0.2") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "0b18cy3ss3g38hygncb6pbgg94m925n524al95g8rnqccbr0i5q6")))

(define-public crate-rbdc-pool-fast-4.5.9 (c (n "rbdc-pool-fast") (v "4.5.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dark-std") (r "^0.2") (d #t) (k 0)) (d (n "fast_pool") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rbdc") (r "^4.5") (k 0)) (d (n "rbs") (r "^4.5") (d #t) (k 0)))) (h "01xs687zpl4nb01d0fh3k5i2p8jdzrahsmci9wkzzpiwwwni05zq")))

