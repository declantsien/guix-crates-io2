(define-module (crates-io rb gg rbgg) #:use-module (crates-io))

(define-public crate-rbgg-0.1.0 (c (n "rbgg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "time" "macros"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)) (d (n "xmltojson") (r "^0.1") (d #t) (k 0)))) (h "0bm9mf0k8m862zcphbadcidfp0yj0k3x6jpgz8bm1aqwbl90pspq")))

(define-public crate-rbgg-0.1.1 (c (n "rbgg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "time" "macros"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)) (d (n "xmltojson") (r "^0.1") (d #t) (k 0)))) (h "1rwc1g6102rsxy5cdryq9hcziav4pd6xrnxka0pr1xmxazhqlsa5")))

