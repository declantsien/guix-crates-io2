(define-module (crates-io rb sg rbsg-backend) #:use-module (crates-io))

(define-public crate-rbsg-backend-0.0.0 (c (n "rbsg-backend") (v "0.0.0") (h "08q3yv79p4k8993dffbkxf6v56czxrg3lzb5p89ay1skzsx0lrzz")))

(define-public crate-rbsg-backend-0.0.1 (c (n "rbsg-backend") (v "0.0.1") (h "16smq887m8x3lc9mk0mhsr4cy63rhdkdaihhiifyp3p3vgmp4bpc")))

