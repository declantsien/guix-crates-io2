(define-module (crates-io rb _t rb_tree) #:use-module (crates-io))

(define-public crate-rb_tree-0.1.0 (c (n "rb_tree") (v "0.1.0") (h "0mbyngmjzmwsmknhxabw5mk6plljfpjqfqk059lljd0pbaz2zf51")))

(define-public crate-rb_tree-0.1.1 (c (n "rb_tree") (v "0.1.1") (h "092yvqapbwy1w91y44zxr12n8mssk1gvrpc2qbf6vk8gaw8lx9fj")))

(define-public crate-rb_tree-0.2.0 (c (n "rb_tree") (v "0.2.0") (h "1a9d747k4qh3zbjg9gz1q61n30wmlq5s1mj775gj8w49yrnrzkwh")))

(define-public crate-rb_tree-0.3.0 (c (n "rb_tree") (v "0.3.0") (h "13w4mgscs7six9j4hkqvrprwpmabivbb9mdfgbm3zl36gfpc05gn")))

(define-public crate-rb_tree-0.3.1 (c (n "rb_tree") (v "0.3.1") (h "0pa9abrx5l2d6w2y7smpnmlcmzyayx211h99padswb67b3zdrxic")))

(define-public crate-rb_tree-0.3.2 (c (n "rb_tree") (v "0.3.2") (h "0rigzc92pv9kqb0bxcvvx99jcpc4ycsh907yhsmid6zxkzfppqcq")))

(define-public crate-rb_tree-0.3.3 (c (n "rb_tree") (v "0.3.3") (h "0r69vfh612a6pbzlk8pvhsc32s7ff8gjgh28dwnqrrz4rk1lwbl9") (y #t)))

(define-public crate-rb_tree-0.3.4 (c (n "rb_tree") (v "0.3.4") (h "0iiyph8rwhwrcmqc41bgr10yb14jb3qh0agkqdxv79zb5y9sxg7l") (y #t)))

(define-public crate-rb_tree-0.3.5 (c (n "rb_tree") (v "0.3.5") (h "0lakgn665m7grg9w4bj318pi1cwbwpyksrz3807x0jgkj0zk1qd7")))

(define-public crate-rb_tree-0.4.0 (c (n "rb_tree") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)))) (h "1wqkqcp1882ay191n8f1h73nrzqqjz6d651395rwpx15qav1qh23")))

(define-public crate-rb_tree-0.4.1 (c (n "rb_tree") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)))) (h "09acvan4c8s04crh3r5iqlnbpgi4pxlwabvkn839ax1j65lp8qmk") (f (quote (("set") ("queue") ("map" "set") ("default" "set" "queue" "map"))))))

(define-public crate-rb_tree-0.5.0 (c (n "rb_tree") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zmfn5ajzgf2vld6r666753kib897mcpcgsdbcaw9q9kw1w4qwp6") (f (quote (("set") ("queue") ("map" "set") ("default" "set" "queue" "map"))))))

