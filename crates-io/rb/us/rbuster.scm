(define-module (crates-io rb us rbuster) #:use-module (crates-io))

(define-public crate-rbuster-0.1.0 (c (n "rbuster") (v "0.1.0") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0n0idylmzqnxqlg8f07h31qiy8ijc609nsadrrnskz5f4fp88v3l")))

(define-public crate-rbuster-0.1.1 (c (n "rbuster") (v "0.1.1") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0jsjpg07vkzz9ildqcdk4mz4idzjqxa7m2yr62acpz42g9yjkwy0")))

(define-public crate-rbuster-0.1.2 (c (n "rbuster") (v "0.1.2") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0jy7nb3h3p98bab1s5llgq5rhjvf56mgawwbzs9vxj66s6vh0w83")))

(define-public crate-rbuster-0.2.0 (c (n "rbuster") (v "0.2.0") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1a0jwn95rsrcyb83fars3bxpm4dlrnama2h2mnfzqm6zmzj0f7rh")))

(define-public crate-rbuster-0.2.1 (c (n "rbuster") (v "0.2.1") (d (list (d (n "nanoid") (r "^0.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0f1fcrj19g8fxwsph9ri3vz125iqqni6safp8abld1k6wqdzldir")))

(define-public crate-rbuster-0.3.0 (c (n "rbuster") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.10") (d #t) (k 0)) (d (n "nanoid") (r "^0.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kvs0da1zdi28wnwg4l2khmpxmgn1hckli8h1fdxm7ncmzdbi5mr")))

