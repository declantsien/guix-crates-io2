(define-module (crates-io rb ui rbuild) #:use-module (crates-io))

(define-public crate-rbuild-0.1.0 (c (n "rbuild") (v "0.1.0") (d (list (d (n "paragraphs") (r ">= 0.3.0") (d #t) (k 0)) (d (n "rgparse") (r ">= 0.1.2") (d #t) (k 0)))) (h "0piknbgcfm819ycddj33q1zmi5h9r2mly1xbxlk4bw1afm4ybh8k")))

(define-public crate-rbuild-0.2.0 (c (n "rbuild") (v "0.2.0") (d (list (d (n "paragraphs") (r ">= 0.3.1") (d #t) (k 0)) (d (n "rgparse") (r ">= 0.1.2") (d #t) (k 0)))) (h "0dsa0h752qkkzzwxkxx1c2qnlbgx41y5376biy2v66kp86z15pfc")))

(define-public crate-rbuild-0.3.0 (c (n "rbuild") (v "0.3.0") (d (list (d (n "paragraphs") (r ">= 0.3.1") (d #t) (k 0)) (d (n "rgparse") (r ">= 0.1.2") (d #t) (k 0)))) (h "0jiqwc384z16cw3vbp6ii2qnfbdd5b5jdwkk4wx0szcb3sl4dwn3")))

(define-public crate-rbuild-0.3.1 (c (n "rbuild") (v "0.3.1") (d (list (d (n "paragraphs") (r ">= 0.3.1") (d #t) (k 0)) (d (n "rgparse") (r ">= 0.1.2") (d #t) (k 0)))) (h "1sws42yzdvyiri66b7y6waxbim4xc6laji8vqdsgz2245yghccnz")))

