(define-module (crates-io rb tr rbtree-defrag-buffer) #:use-module (crates-io))

(define-public crate-rbtree-defrag-buffer-0.1.0 (c (n "rbtree-defrag-buffer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "intrusive-collections") (r "^0.9.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08mpd26z10v5db3wcrx5qb64xwlvbjcra4kzb9y8whk818mh3s61")))

