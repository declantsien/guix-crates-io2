(define-module (crates-io rb tr rbtree) #:use-module (crates-io))

(define-public crate-rbtree-0.1.0 (c (n "rbtree") (v "0.1.0") (h "1f280z7qxd0pfj39pzzw24qvy9ad2qrs8yx09yf748vsr4alx7z5")))

(define-public crate-rbtree-0.1.1 (c (n "rbtree") (v "0.1.1") (h "0pfskwparvjzcw4ss9fyhr0vrl8h9n6zvbswx3lbxxzfwki7yxx1")))

(define-public crate-rbtree-0.1.2 (c (n "rbtree") (v "0.1.2") (h "15jsbzxk30ph9agzwygfy91grzmlp8vaz7nigrqjr8rhdw1g0qgp")))

(define-public crate-rbtree-0.1.3 (c (n "rbtree") (v "0.1.3") (h "06pvqwgyy7m2ckxzn142nrazblh1fafy6cgsn85qizb6p11x9gk6")))

(define-public crate-rbtree-0.1.4 (c (n "rbtree") (v "0.1.4") (h "0aps0bsknds303cd526420a141j20cgnms71jsb7xz8dfi611nf9")))

(define-public crate-rbtree-0.1.5 (c (n "rbtree") (v "0.1.5") (h "0q279mnnlij9gn9pwwc7wg76w93sc1zsnxrmkg71vmwdp2zh40y3")))

(define-public crate-rbtree-0.1.6 (c (n "rbtree") (v "0.1.6") (h "0r75nyfj5p3f5zd7skwvm31ygc9gahg03c4ra54as6x8ki63dcza")))

(define-public crate-rbtree-0.1.7 (c (n "rbtree") (v "0.1.7") (h "0nkcdkzfpic7974k4lsga0gbmh528yqgfw545qp8pfww5hrc87qc")))

(define-public crate-rbtree-0.2.0 (c (n "rbtree") (v "0.2.0") (h "133h0cmx03cyhpx85wb21c95g361vf6hlqbq1lj7icfvykwhjk81")))

