(define-module (crates-io rb uw rbuwu) #:use-module (crates-io))

(define-public crate-rbuwu-0.0.1-ALPHA (c (n "rbuwu") (v "0.0.1-ALPHA") (h "0fx4daay33z6sipzrawcsd0vpl1bhb2fbwkkr5r5j780dyn5p76y")))

(define-public crate-rbuwu-0.0.2-ALPHA (c (n "rbuwu") (v "0.0.2-ALPHA") (h "061l61s7ls3p1amm5a92w5yw9n1nwb92km46312jri6n71h7gag0")))

(define-public crate-rbuwu-0.0.3-ALPHA (c (n "rbuwu") (v "0.0.3-ALPHA") (h "138dyyq3d5i113i9rs6nwkvzyhdkkcjf5fgr1fwgmpyarlali2ha")))

(define-public crate-rbuwu-0.0.4-ALPHA (c (n "rbuwu") (v "0.0.4-ALPHA") (h "17amyxqa5f6m3y5jqlvxg6xhzfr2j5wjajzq2v1aypm8w0klcd5b")))

(define-public crate-rbuwu-0.0.5-ALPHA (c (n "rbuwu") (v "0.0.5-ALPHA") (h "1gnhbr8vy4diwjx38p8v14xy9g4dpv2nm2hpfc15z1zzpm9cr4vf")))

(define-public crate-rbuwu-0.0.6-ALPHA (c (n "rbuwu") (v "0.0.6-ALPHA") (h "1rfrv55sb7i3b20cq4wfpm0xb8p4dmjjw01kvz86d29a0dcnxlfr")))

(define-public crate-rbuwu-0.0.7-ALPHA (c (n "rbuwu") (v "0.0.7-ALPHA") (h "0nslja3vs76ajydvcfb2ppr6ql7f8mw9iqkz4v0vw51apg6j4xb8")))

(define-public crate-rbuwu-0.0.8-ALPHA (c (n "rbuwu") (v "0.0.8-ALPHA") (h "1mm38f6188fbyff3icq65bdrfc4rwp8idshczkwmiv37ablwqnva")))

(define-public crate-rbuwu-0.0.9-ALPHA (c (n "rbuwu") (v "0.0.9-ALPHA") (h "0mbwh7i1nfnv8y67bc722adg53v5wybp9064rgk4z04cr8xzghm2")))

(define-public crate-rbuwu-0.1.0-BETA (c (n "rbuwu") (v "0.1.0-BETA") (h "1mdh66hwx25r90swwk25593iv1q89lsx1lvyi6ypzbi7lswj36b4")))

