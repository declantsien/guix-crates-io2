(define-module (crates-io rb tw rbtw) #:use-module (crates-io))

(define-public crate-rbtw-1.0.0 (c (n "rbtw") (v "1.0.0") (d (list (d (n "efibootnext") (r "^0.3") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)))) (h "1md6ss1y8bs8xwlzq6rfssm9xhgf6wf313fs11bssvhwfy7za03x")))

(define-public crate-rbtw-1.0.1 (c (n "rbtw") (v "1.0.1") (d (list (d (n "efibootnext") (r "^0.3") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0aln43x94i5km2qhjkrb5prrrj4z11q28q17kg1qszhmbm7pxbvr")))

(define-public crate-rbtw-1.0.2 (c (n "rbtw") (v "1.0.2") (d (list (d (n "efibootnext") (r "^0.3") (d #t) (k 0)) (d (n "sudo") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0cc03g67s9gyj7ha0j9dhf6klr1drd63z696026ngxblq7q59dwq")))

