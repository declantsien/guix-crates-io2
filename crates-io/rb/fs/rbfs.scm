(define-module (crates-io rb fs rbfs) #:use-module (crates-io))

(define-public crate-rbfs-0.1.0 (c (n "rbfs") (v "0.1.0") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "032pyhw75zppvhlgmmmydk3ya2hq0hxzbgxwwyb1a82ma056s1l3") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.1 (c (n "rbfs") (v "0.1.1") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1wf1qh3vwkf1ccd3fvrr1g0xzj8v089hjfqx7svjcddikccss28g") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.2 (c (n "rbfs") (v "0.1.2") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "139m93hsi2jahymcm244rxlk2bvbb169kpbsmis7csad5xri3qhw") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.3 (c (n "rbfs") (v "0.1.3") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0pf2jwy4n84mcn5yf16qx1cpkhmsjvf8n6cf6782iv2n7zcfar5w") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.4 (c (n "rbfs") (v "0.1.4") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0y0q4jfhh65hirbjv818bg7831sqjwmhs1rmg986qi6g80ya0v65") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.5 (c (n "rbfs") (v "0.1.5") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "07c220ylsy4hf9sp8bhay0fnli9vlnlr67mkd1xhzc63rz04idrw") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.6 (c (n "rbfs") (v "0.1.6") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "182d0278ibn7p9zqfkx6jgb3x6wx0cyz1s9amjiymxcfs3iy7cx4") (s 2) (e (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1.7 (c (n "rbfs") (v "0.1.7") (d (list (d (n "rust-format") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1qcf8l09bgx37fcznhmmycmjr5755wdld50dvz7xnbvxb1xrr5qm") (s 2) (e (quote (("format" "dep:rust-format"))))))

