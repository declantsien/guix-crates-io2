(define-module (crates-io rb fi rbfi) #:use-module (crates-io))

(define-public crate-rbfi-1.2.0 (c (n "rbfi") (v "1.2.0") (h "13qp7sfhax0h5p328rqa0l0qlqj7n0ci02b1axh4x0kaibfk501y") (y #t)))

(define-public crate-rbfi-1.2.1 (c (n "rbfi") (v "1.2.1") (h "0w2831rqymsx3fpizbbi2yviy7bjjfhcav8pi7xbh99728pvzkb1") (y #t)))

