(define-module (crates-io rb t3 rbt3) #:use-module (crates-io))

(define-public crate-rbt3-0.1.0 (c (n "rbt3") (v "0.1.0") (d (list (d (n "glam") (r "^0") (d #t) (k 0)))) (h "1f1127wbrkpac6468l3p9nrsnqzna8sh7wkgvpcf5k2arxqnc2s5")))

(define-public crate-rbt3-0.1.1 (c (n "rbt3") (v "0.1.1") (d (list (d (n "glam") (r "^0") (d #t) (k 0)))) (h "1rddrxndq7y6gryl5i3ikc7xdlakfmzcbwwgxs9hgmxjdp2rqxp2")))

