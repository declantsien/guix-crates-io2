(define-module (crates-io rb tc rbtc) #:use-module (crates-io))

(define-public crate-rbtc-0.1.0 (c (n "rbtc") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "18m9352qfpgrcib2qs6cydn98mxlrs2r2rhxcd0m7dd066021yl7")))

(define-public crate-rbtc-0.1.1 (c (n "rbtc") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "15c19pwwgi1nl0anfhb32ihcanhspyl63iky3drn194d1rhj7j0n")))

(define-public crate-rbtc-0.1.2 (c (n "rbtc") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0w9q5b1pqx4wpl7dnas0j8li9sv7g3narpdx5iqw3srwafn457zv")))

(define-public crate-rbtc-0.2.0 (c (n "rbtc") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1a11r95lg8lni8l9jmzan47anp99j8jvpbmyqqm27f24hg76p613")))

(define-public crate-rbtc-0.2.1 (c (n "rbtc") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1hmv1j9zqj89layx8wgqzxcc81jk3vg7plbp80mjprimywj50pc2")))

(define-public crate-rbtc-0.2.2 (c (n "rbtc") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "15y6mcya16i1n4878q3ki82a1dvda6xy75nk97x99x3xnn635jz1")))

