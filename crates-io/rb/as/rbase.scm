(define-module (crates-io rb as rbase) #:use-module (crates-io))

(define-public crate-rbase-0.1.0 (c (n "rbase") (v "0.1.0") (h "1vf0bpip8vnpz917v4iyyq3dav1p4crm9jjl0b438fygdzn0gpa0")))

(define-public crate-rbase-0.2.0 (c (n "rbase") (v "0.2.0") (h "014fcrf831m7d7kriykjbwcp6dcbh74jhsk1pgv5sz3px4rxkmny")))

