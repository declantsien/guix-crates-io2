(define-module (crates-io rb en rbench) #:use-module (crates-io))

(define-public crate-rbench-0.1.0 (c (n "rbench") (v "0.1.0") (h "0r72730hzb92xl6yq20xrx4xnzwci4zimapp557jwsw7s93w1bv7")))

(define-public crate-rbench-1.0.0 (c (n "rbench") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0pw7vrwzqj8byq7vs2r5f7fbc06bzvxhns7vbgy0ch75fsm9say3")))

