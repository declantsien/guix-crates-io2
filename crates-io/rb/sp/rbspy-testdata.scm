(define-module (crates-io rb sp rbspy-testdata) #:use-module (crates-io))

(define-public crate-rbspy-testdata-0.1.0 (c (n "rbspy-testdata") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "read-process-memory") (r "^0.1.0") (d #t) (k 0)))) (h "0hij4dqkp8y0w09sryxlmqq32c6ywsql423mc0fxl6nrc3vvdg1b")))

(define-public crate-rbspy-testdata-0.1.1 (c (n "rbspy-testdata") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "read-process-memory") (r "^0.1.0") (d #t) (k 0)))) (h "12s0fnwlm2yg0nblyvp0br81xyziph31qqn6ldsgki3al5shsmny")))

(define-public crate-rbspy-testdata-0.1.2 (c (n "rbspy-testdata") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "read-process-memory") (r "^0.1.0") (d #t) (k 0)))) (h "0335mjrlpz135v280gz2frj0iw15gyy8f11hrp0pbv6wb4062cal")))

(define-public crate-rbspy-testdata-0.1.4 (c (n "rbspy-testdata") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "0wdrxqq48xq8j5q5fddf025m93xll6nssi3ys11s4a03268cn72s")))

(define-public crate-rbspy-testdata-0.1.5 (c (n "rbspy-testdata") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "1zwwrpv0c95sz17mxyp57bk94vypr187vcmcdsx3skqxh9rml6i8")))

(define-public crate-rbspy-testdata-0.1.6 (c (n "rbspy-testdata") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "1bswfcqhbwda73x94yh70zds5xz6b02gcvnc62ybkk85c27w9ajw")))

(define-public crate-rbspy-testdata-0.1.7 (c (n "rbspy-testdata") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "1638w5ai01ypv7ga8lhc7r51d8g85q7y8vah619jpn7x3baaxjxn")))

(define-public crate-rbspy-testdata-0.1.8 (c (n "rbspy-testdata") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "0zbskbhdqxd9ib01j2fpgn9ahch1n5xcq9d1wcykzqs6ly529bi8")))

(define-public crate-rbspy-testdata-0.1.9 (c (n "rbspy-testdata") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "0q5hjfx9z97wq3n3wsndi90nn4312insyfdrs5v21iclvfh4716p")))

(define-public crate-rbspy-testdata-0.1.10 (c (n "rbspy-testdata") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.12") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "12d19zflhhw4v8rnk10ib5db07mb6qjgbcm2y1dsk9d12i9w1p9j")))

(define-public crate-rbspy-testdata-0.2.0 (c (n "rbspy-testdata") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "goblin") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "remoteprocess") (r "^0.4.3") (d #t) (k 0)))) (h "0ghkhahvhy6jy6mzk5apvvcy28vn57khcd1fq0619vvzkrkjbrij")))

