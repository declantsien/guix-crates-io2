(define-module (crates-io rb -a rb-allocator) #:use-module (crates-io))

(define-public crate-rb-allocator-0.9.0 (c (n "rb-allocator") (v "0.9.0") (d (list (d (n "rb-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1zcrigdgycra2yhsl2sr9b8wxkmffm6zllsqdcs8v2rb2sw0pk56")))

(define-public crate-rb-allocator-0.9.1 (c (n "rb-allocator") (v "0.9.1") (d (list (d (n "rb-sys") (r "^0.9.1") (d #t) (k 0)))) (h "0h2wr9718y6an8wkx6j3gpid4xyxbz5gnylhir27lq2srgiqcdjg")))

(define-public crate-rb-allocator-0.9.2 (c (n "rb-allocator") (v "0.9.2") (d (list (d (n "rb-sys") (r "^0.9.2") (d #t) (k 0)))) (h "1srzgwlmnf4r6lf0vvyk2p583cmclns7gbkjq67j88wrbakcg3z1")))

(define-public crate-rb-allocator-0.9.3 (c (n "rb-allocator") (v "0.9.3") (d (list (d (n "rb-sys") (r "^0.9.2") (d #t) (k 0)))) (h "000j3smmanviqjmvxx251by45z3cyi38gfxvj6piwd0plmcy8sgd")))

(define-public crate-rb-allocator-0.9.4 (c (n "rb-allocator") (v "0.9.4") (d (list (d (n "rb-sys") (r "^0.9.2") (d #t) (k 0)))) (h "1vvsin9biry10phs44w9vnj80a5a4c4jxnmgl345v7b6wzhjbvms")))

(define-public crate-rb-allocator-0.9.5 (c (n "rb-allocator") (v "0.9.5") (d (list (d (n "rb-sys") (r "^0.9.2") (d #t) (k 0)))) (h "1v9axyrvlq236jwwzcqcfxr3d2ajl4mjicxcjaaqb2mpvdcya1dr")))

(define-public crate-rb-allocator-0.9.6 (c (n "rb-allocator") (v "0.9.6") (d (list (d (n "rb-sys") (r "^0.9.2") (d #t) (k 0)))) (h "12s22wjgrlzi54bjl1jm9wfhjvs610a0nx0qqfj0y566hn7mplbp")))

