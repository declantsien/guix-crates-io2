(define-module (crates-io rb f- rbf-interp) #:use-module (crates-io))

(define-public crate-rbf-interp-0.1.0 (c (n "rbf-interp") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "01rdkgxzlds5r6bdxx2grwx9pajwwfrq9g36wa9i235w5l4az5im")))

(define-public crate-rbf-interp-0.1.1 (c (n "rbf-interp") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1965r4hnz813igm6b4pry3qsq241dc033a482xfi9cfxffn59kkn")))

(define-public crate-rbf-interp-0.1.2 (c (n "rbf-interp") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0d5dqjvr42sl9w1xlfrkskyyrq23494rbzqbhcrc5nkkfzsrvw5x")))

(define-public crate-rbf-interp-0.1.3 (c (n "rbf-interp") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1zjgxm2vx9p2av8vi24mm4za6plc85pbswiwk3i9i67wsj28bi3r")))

