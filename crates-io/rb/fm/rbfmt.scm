(define-module (crates-io rb fm rbfmt) #:use-module (crates-io))

(define-public crate-rbfmt-0.0.1 (c (n "rbfmt") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "prism") (r "^0.24.0") (d #t) (k 0) (p "ruby-prism")) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 2)))) (h "0zlq4rd062g1d3blm6jxajcs8sfq76wlh5vfsp15imfw37y5wx65")))

(define-public crate-rbfmt-0.0.2 (c (n "rbfmt") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "prism") (r "^0.24.0") (d #t) (k 0) (p "ruby-prism")) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 2)))) (h "0lmh75x8392v0w6s8fhc6czy95mmhhjnl9sz855d94y63xlyghky")))

