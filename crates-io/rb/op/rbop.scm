(define-module (crates-io rb op rbop) #:use-module (crates-io))

(define-public crate-rbop-0.1.0 (c (n "rbop") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1.14") (f (quote ("maths"))) (k 0)) (d (n "rust_decimal_macros") (r "^1.14") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 2)))) (h "149faikx0y9bwkyxy3vc5g85bp338dmf5q9qx1gvn27wqaljfy7s")))

(define-public crate-rbop-0.2.0 (c (n "rbop") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1.44") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rust_decimal") (r "=1.23.1") (f (quote ("maths"))) (k 0)) (d (n "speedy2d") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "04m43zn561s3q7zsgbys415kcs0hiq9anpi0b736331cdc2lcv3m") (f (quote (("examples" "termion" "speedy2d"))))))

