(define-module (crates-io nh de nhdesigngen) #:use-module (crates-io))

(define-public crate-nhdesigngen-0.1.0 (c (n "nhdesigngen") (v "0.1.0") (h "16cvyfgdwjcwmgzr8ggxcnr2zgjpilclbdkrs3gd4lllqprsqvnc")))

(define-public crate-nhdesigngen-0.2.0 (c (n "nhdesigngen") (v "0.2.0") (d (list (d (n "exoquant") (r "^0.2") (d #t) (k 0)) (d (n "gdk") (r "^0") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0") (d #t) (k 0)) (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0.8") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "pango") (r "^0") (d #t) (k 0)))) (h "1kcsg1qgbslj1krbn0mwg13xi9yc9rzc33aa6vmkq0m049dia09f")))

(define-public crate-nhdesigngen-1.0.0 (c (n "nhdesigngen") (v "1.0.0") (d (list (d (n "exoquant") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "1z06am9m67zj74ynz953qa7b26q08nvm808z5wj26x01hgq9bzqm")))

(define-public crate-nhdesigngen-1.1.0 (c (n "nhdesigngen") (v "1.1.0") (d (list (d (n "exoquant") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "0xa316jwpg778q61xq06xd62disn5i841w9ci7kw6wy5wwggx13l")))

(define-public crate-nhdesigngen-1.1.3 (c (n "nhdesigngen") (v "1.1.3") (d (list (d (n "exoquant") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "0xaw5r36gfb5vjs8rsif18x6hzwf7v1qak0fhqf3rjgsfnpvqn8k")))

