(define-module (crates-io nh l- nhl-api-client) #:use-module (crates-io))

(define-public crate-nhl-api-client-0.1.0 (c (n "nhl-api-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "01cg71b5rb7y3d1ww4qbdaax6y28dnv26hqsg0iiff1s258slw65")))

(define-public crate-nhl-api-client-0.1.1 (c (n "nhl-api-client") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0czx3l0ilywxz1rh8w4db8b2rffa7l2x1kmwp3yli6dimc2fll4x")))

(define-public crate-nhl-api-client-0.1.11 (c (n "nhl-api-client") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1an53nvxqh2sm9p04jqf7g0i2s6hy8yw3lf80jfryjn5x05a27f1")))

