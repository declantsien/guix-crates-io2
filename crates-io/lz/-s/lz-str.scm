(define-module (crates-io lz -s lz-str) #:use-module (crates-io))

(define-public crate-lz-str-0.1.0 (c (n "lz-str") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.46") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (o #t) (d #t) (k 0)))) (h "132vdlalivgqj302f748yfhc3jrsganlh30a9bpiy3swwpqiw21s") (f (quote (("wasm-bindgen-support" "wasm-bindgen" "js-sys") ("nightly" "criterion/real_blackbox"))))))

(define-public crate-lz-str-0.2.0 (c (n "lz-str") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0w79casxjk05vji62f0kl4kkmbs83dp2m9563dyrvmp7ciz8l3w0") (f (quote (("nightly" "criterion/real_blackbox"))))))

(define-public crate-lz-str-0.2.1 (c (n "lz-str") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1k3h7k104xvmymzka7sa6xw6wwjkb8l338syszp90w12fwnxgwrr") (f (quote (("nightly" "criterion/real_blackbox"))))))

