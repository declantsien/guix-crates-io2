(define-module (crates-io lz -s lz-string) #:use-module (crates-io))

(define-public crate-lz-string-0.1.0 (c (n "lz-string") (v "0.1.0") (h "0ygyr1l092ijd7mj6r5lvxr9wa1raf0c9qnrw0p2b8nyq4drf2q8")))

(define-public crate-lz-string-0.1.1 (c (n "lz-string") (v "0.1.1") (h "03jwa71gk6s9yl3yvbzr6c833f7an0vb1gnrhl4zsxgv0a35k8gp")))

