(define-module (crates-io lz f- lzf-sys) #:use-module (crates-io))

(define-public crate-lzf-sys-0.1.0 (c (n "lzf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)))) (h "1v1ab0rsy3gfvx7fzjrn43dzk7wjmcmfjvfrgv3y4189rqix1607") (f (quote (("static" "cc") ("paranoid" "bindgen") ("default" "static")))) (l "lzf")))

