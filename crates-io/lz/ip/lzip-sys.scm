(define-module (crates-io lz ip lzip-sys) #:use-module (crates-io))

(define-public crate-lzip-sys-0.1.0+1.13 (c (n "lzip-sys") (v "0.1.0+1.13") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.120") (d #t) (k 0)))) (h "1hggvwgq5b7r3f1jqv4sr3z0qjzj1cbjipmwvknqlc1kh7zcbk70") (f (quote (("static")))) (l "lzip")))

(define-public crate-lzip-sys-0.1.1+1.13 (c (n "lzip-sys") (v "0.1.1+1.13") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.120") (d #t) (k 0)))) (h "1ycwkgzjcfqvw31rb9hbahaim2yzys0m9kpq0jll4q3wj36avahd") (f (quote (("static")))) (l "lzip")))

(define-public crate-lzip-sys-0.1.3+1.13 (c (n "lzip-sys") (v "0.1.3+1.13") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "1jmbbikaa3ka9z764nsqawrird8fyjy3v6zax9slw4l4p6bq2d8n") (f (quote (("static")))) (l "lzip")))

(define-public crate-lzip-sys-0.1.4+1.13 (c (n "lzip-sys") (v "0.1.4+1.13") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)))) (h "007rcn6iap96k9gaamw47fcmlpwv3jpkax6q4wnr9zm6kndc312x") (f (quote (("static")))) (l "lzip")))

