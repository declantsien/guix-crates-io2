(define-module (crates-io lz ip lzip) #:use-module (crates-io))

(define-public crate-lzip-0.1.0 (c (n "lzip") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "lzip-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.13") (o #t) (d #t) (k 0)))) (h "09abhsfkcwjg8y7mhb729dbkjrwxgybvwkyyvhyva5zdnsyw6v93") (f (quote (("tokio" "futures" "tokio-io") ("static" "lzip-sys/static"))))))

(define-public crate-lzip-0.1.1 (c (n "lzip") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "lzip-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.13") (o #t) (d #t) (k 0)))) (h "0ydfg4m3nz91v9smvzrr920pdaqj0g8h94d6wffwacdf31gr8d3v") (f (quote (("tokio" "futures" "tokio-io") ("static" "lzip-sys/static"))))))

