(define-module (crates-io lz _e lz_eytzinger_tree) #:use-module (crates-io))

(define-public crate-lz_eytzinger_tree-0.1.0 (c (n "lz_eytzinger_tree") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)))) (h "0gib9fi78pm8l2jvhb8jmrdphzj12ijjb74wijxgwsnq8j7299l5")))

(define-public crate-lz_eytzinger_tree-0.2.0 (c (n "lz_eytzinger_tree") (v "0.2.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)))) (h "0c3cqa4x4vnffipm1l6s5m7wxnkxw55xw5ciya0ncxkqc7rkcyb1")))

(define-public crate-lz_eytzinger_tree-0.3.0 (c (n "lz_eytzinger_tree") (v "0.3.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)))) (h "1ddww1csk06krcs2xf1kg4sc29wgq13ix2bkhh08lw6ay35b7i5z")))

