(define-module (crates-io lz _e lz_etzynger_tree) #:use-module (crates-io))

(define-public crate-lz_etzynger_tree-0.1.0 (c (n "lz_etzynger_tree") (v "0.1.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "1jxcxgji29ifa738svgibgldggw3xlcza70qcn9lgxbwjfrlxypa") (y #t)))

