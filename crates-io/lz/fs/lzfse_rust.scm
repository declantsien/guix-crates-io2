(define-module (crates-io lz fs lzfse_rust) #:use-module (crates-io))

(define-public crate-lzfse_rust-0.1.0 (c (n "lzfse_rust") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "02mml99gxmb6vbsm9x3358k9088xnbprkacz4iw1hcblpsaj1z2g") (y #t)))

(define-public crate-lzfse_rust-0.2.0 (c (n "lzfse_rust") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1kxz04x6zvlx2y18rlpc2h9am9827fqfnwbb6iaz521fki0k6gzq")))

