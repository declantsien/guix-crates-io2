(define-module (crates-io lz fs lzfse) #:use-module (crates-io))

(define-public crate-lzfse-0.1.0 (c (n "lzfse") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lzfse-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0b5jk6xlpvp1pckssqjayb9b1gbq0p6wq4h3fs583w8ndphg5rz5")))

