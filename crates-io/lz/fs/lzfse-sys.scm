(define-module (crates-io lz fs lzfse-sys) #:use-module (crates-io))

(define-public crate-lzfse-sys-1.0.0 (c (n "lzfse-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ybad5kl6pfw1fai3wg3ffg81xnbwgvkwwrr8jxylhw3y7v76am3") (l "lzfse")))

