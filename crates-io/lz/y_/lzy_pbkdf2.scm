(define-module (crates-io lz y_ lzy_pbkdf2) #:use-module (crates-io))

(define-public crate-lzy_pbkdf2-0.1.0 (c (n "lzy_pbkdf2") (v "0.1.0") (d (list (d (n "hmac-sha512") (r "^0.1.9") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha256") (r "^1.0.2") (d #t) (k 0)))) (h "03mvvj4dv18ghammsrnw04z5f83rsdcdjggxf68f0v94mv0yb0jv")))

