(define-module (crates-io lz -f lz-fear) #:use-module (crates-io))

(define-public crate-lz-fear-0.1.0 (c (n "lz-fear") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "1lyxcjikmlv89fh9b5c0hfncilkg74zpgrdddbb9gcnn6s0rlx6n")))

(define-public crate-lz-fear-0.1.1 (c (n "lz-fear") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (k 0)))) (h "078qqvxn3n9r7jdg59bla4jx5icdscx0qgnlsylggk748p7d3ah6")))

(define-public crate-lz-fear-0.2.0 (c (n "lz-fear") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "culpa") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (k 0)))) (h "0789vffb4hwrh6f0y89rh7vpvppz731n6j7k3jjxvyaz3r3q18i6")))

