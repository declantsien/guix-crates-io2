(define-module (crates-io lz #{4u}# lz4util) #:use-module (crates-io))

(define-public crate-lz4util-0.1.0 (c (n "lz4util") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lz4") (r "^1") (d #t) (k 0)))) (h "15sbsrzpb687bk8fk76sqc94yh4w2zf7901203ggrnh8znqcf8bn")))

(define-public crate-lz4util-0.1.1 (c (n "lz4util") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lz4") (r "^1") (d #t) (k 0)))) (h "0cfa2vfjy33g11nlz6amld1zyqxrsazlbp8slg08qfdjxxnjpvbi")))

