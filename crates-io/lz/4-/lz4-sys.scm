(define-module (crates-io lz #{4-}# lz4-sys) #:use-module (crates-io))

(define-public crate-lz4-sys-1.0.1+1.7.3 (c (n "lz4-sys") (v "1.0.1+1.7.3") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "16imwc0vrph4lkj0ammmyzl8mjm5wvlzr0x7pnxv9ib7wzdn05kz")))

(define-public crate-lz4-sys-1.0.1+1.7.5 (c (n "lz4-sys") (v "1.0.1+1.7.5") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "1vh16cz5ny5vpnqy4qabnrby7apsd0x9ndvlzj8k9macbngjbnq0")))

(define-public crate-lz4-sys-1.7.5 (c (n "lz4-sys") (v "1.7.5") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "16wy4178sgwaa5yx568d6d3x9azcjd7v7jfvvm89n5n4md8s9s1r")))

(define-public crate-lz4-sys-1.8.0 (c (n "lz4-sys") (v "1.8.0") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "1db2sjm3ckb0wpypxrh1jlhj4yclqpbdsw12mb9g751rpb1l9455")))

(define-public crate-lz4-sys-1.8.2 (c (n "lz4-sys") (v "1.8.2") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "0dz97hm35mc9ckz40y4m95wxd9a80g5bchzjdjr0wlv496yff7x4") (l "lz4")))

(define-public crate-lz4-sys-1.8.3 (c (n "lz4-sys") (v "1.8.3") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1znsrhpkl8bg4sw3fs5cb3haqnq8k1mxvb3ksdc1qcz948l05ar0") (l "lz4")))

(define-public crate-lz4-sys-1.9.2 (c (n "lz4-sys") (v "1.9.2") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1bmc82bddx2lm0r9bn422cxbwlwq6qld6m6l78hjcclbbnlrm9yw") (l "lz4")))

(define-public crate-lz4-sys-1.9.3 (c (n "lz4-sys") (v "1.9.3") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "05vybnw0ccxf7qv3gr7xzd9nrqq35ybgmadq5p032vzdw848kgnp") (l "lz4")))

(define-public crate-lz4-sys-1.9.4 (c (n "lz4-sys") (v "1.9.4") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0059ik4xlvnss5qfh6l691psk4g3350ljxaykzv10yr0gqqppljp") (l "lz4")))

