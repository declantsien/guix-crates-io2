(define-module (crates-io lz #{4-}# lz4-compression) #:use-module (crates-io))

(define-public crate-lz4-compression-0.6.0 (c (n "lz4-compression") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "0iba90miy47asy7gxpahvxxf3qrdx7qx8rkj6gl7flabdn2aavxh")))

(define-public crate-lz4-compression-0.6.1 (c (n "lz4-compression") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "13m4whip682h29x89wkhz1jz44jdi78y1ndghj3j8i3ffkgh6j4n")))

(define-public crate-lz4-compression-0.7.0 (c (n "lz4-compression") (v "0.7.0") (h "0w42pg1kzxk94sn8n510s800v8b00y54j8nq8ypkqfpijyzh84bn")))

