(define-module (crates-io lz #{4-}# lz4-java-wrc) #:use-module (crates-io))

(define-public crate-lz4-java-wrc-0.2.0 (c (n "lz4-java-wrc") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lz4-sys") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (f (quote ("std" "safe-encode" "safe-decode"))) (o #t) (k 0)) (d (n "twox-hash") (r "^1.6") (k 0)))) (h "03bdsz1wg60fir1095iw3rvjgfwnfvj5rfwca38aazriplfw3l9y") (f (quote (("use_lz4_flex" "lz4_flex") ("use_lz4-sys" "lz4-sys" "libc") ("default" "use_lz4_flex"))))))

