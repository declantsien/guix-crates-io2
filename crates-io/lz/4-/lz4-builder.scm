(define-module (crates-io lz #{4-}# lz4-builder) #:use-module (crates-io))

(define-public crate-lz4-builder-1.0.0 (c (n "lz4-builder") (v "1.0.0") (h "16r5axbhg1l0axyhnhkg8r1y8afwi2bi3vm378bvg89jqnpls5df") (y #t)))

(define-public crate-lz4-builder-1.0.1 (c (n "lz4-builder") (v "1.0.1") (h "1kkf5c4v7r13wz2w5hvblgn09y2cnccxbfmf0qnq9n5b8cywwwnv")))

