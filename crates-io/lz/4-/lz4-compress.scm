(define-module (crates-io lz #{4-}# lz4-compress) #:use-module (crates-io))

(define-public crate-lz4-compress-0.1.0 (c (n "lz4-compress") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1jfycwj5vi21dhafblfcmzmxanfw5x29r3i2z5zyng2bzjm4r8ln")))

(define-public crate-lz4-compress-0.1.1 (c (n "lz4-compress") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "quick-error") (r "^1") (d #t) (k 0)))) (h "14cb8rpdfk6q3bjkf7mirpyzb6rvvcglqnayx6lvpa92m4rnb5hg")))

