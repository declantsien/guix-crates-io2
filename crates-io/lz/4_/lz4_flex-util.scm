(define-module (crates-io lz #{4_}# lz4_flex-util) #:use-module (crates-io))

(define-public crate-lz4_flex-util-0.1.0 (c (n "lz4_flex-util") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.8") (f (quote ("checked-decode" "frame"))) (k 0)))) (h "0rciy0lr76xnrf92fk3k7p687p8f7ac7a6368w5lbnj4sx5f612b")))

