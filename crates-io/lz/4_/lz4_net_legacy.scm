(define-module (crates-io lz #{4_}# lz4_net_legacy) #:use-module (crates-io))

(define-public crate-lz4_net_legacy-0.1.0 (c (n "lz4_net_legacy") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "bytes-varint") (r "^1.0.3") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.2") (k 0)))) (h "0nkwk2zkww4v2891vpfbdz9xqy80yxqwhkbacz5wg96ldk9sm6rv")))

