(define-module (crates-io lz o- lzo-sys) #:use-module (crates-io))

(define-public crate-lzo-sys-0.1.0 (c (n "lzo-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1kwl6m2nyk8wnjvmacdw5rz8g1l68cpm1zvs2gjpx4i2zpqys67n") (r "1.66.1")))

(define-public crate-lzo-sys-0.2.0 (c (n "lzo-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "11cljanp0s6rqc989972g4qkdp6rvymilil8m7w1cq8axmfmmk1j") (r "1.66.1")))

(define-public crate-lzo-sys-0.3.0 (c (n "lzo-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0gn8nl6psvvyqb26v3pvw2zcp8hswaxzr8w3fkmndhj1mbc8xqxz") (r "1.66.1")))

(define-public crate-lzo-sys-0.3.1 (c (n "lzo-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0iqlp85pl460lc9np7pdc3rhxyn8cky4wj3jn8v469bnwwggqaqh") (r "1.66.1")))

