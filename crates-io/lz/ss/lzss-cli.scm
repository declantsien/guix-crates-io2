(define-module (crates-io lz ss lzss-cli) #:use-module (crates-io))

(define-public crate-lzss-cli-0.8.0 (c (n "lzss-cli") (v "0.8.0") (d (list (d (n "lzss") (r "^0.8") (d #t) (k 0)))) (h "1r5d5a5qb5m7akhiggc0kqnvamdn9fc8w1kgc7lanflq46yri4ql")))

(define-public crate-lzss-cli-0.9.0 (c (n "lzss-cli") (v "0.9.0") (d (list (d (n "lzss") (r "^0.9") (d #t) (k 0)))) (h "0g3gshyvk2lbwvj6hw45afxcm1iwnalgaq8gr2bcln9d857r7z3c")))

