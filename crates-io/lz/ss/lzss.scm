(define-module (crates-io lz ss lzss) #:use-module (crates-io))

(define-public crate-lzss-0.8.0 (c (n "lzss") (v "0.8.0") (d (list (d (n "void") (r "^1.0.2") (k 0)))) (h "1mxsjg8z5bhimc89l5g64fcniw0nbbzlydrhym2jcvrxp64vgb74") (f (quote (("std" "void/std") ("default" "std") ("const_panic"))))))

(define-public crate-lzss-0.8.1 (c (n "lzss") (v "0.8.1") (d (list (d (n "void") (r "^1.0.2") (k 0)))) (h "0d0jhih2kna2y10lcs337lhmf2pn9dcvr5g0wnbvp0h86wpnff57") (f (quote (("std" "void/std" "alloc") ("default" "std") ("const_panic") ("alloc"))))))

(define-public crate-lzss-0.8.2 (c (n "lzss") (v "0.8.2") (d (list (d (n "void") (r "^1.0.2") (k 0)))) (h "1swpr8v9w0qh528m3dk9zwdlafzr9c9d3idm7g0h0ljf2akbkqir") (f (quote (("std" "void/std" "alloc") ("default" "std") ("const_panic") ("alloc"))))))

(define-public crate-lzss-0.9.0 (c (n "lzss") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "14j0lz661syp2510hmj08arjwmii8x7dhm4zx8i39rl5jp4k5dj4") (f (quote (("std" "void/std" "alloc") ("safe") ("default" "std" "safe") ("alloc"))))))

(define-public crate-lzss-0.9.1 (c (n "lzss") (v "0.9.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "00bs4px0in02rvgn4dzah0gkja18606vzm1p9r6mfyy2gzldkjzz") (f (quote (("std" "void/std" "alloc") ("safe") ("default" "std" "safe") ("alloc"))))))

