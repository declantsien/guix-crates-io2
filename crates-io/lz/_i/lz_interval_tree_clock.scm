(define-module (crates-io lz _i lz_interval_tree_clock) #:use-module (crates-io))

(define-public crate-lz_interval_tree_clock-0.1.0 (c (n "lz_interval_tree_clock") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "11mlylr5mnw7njwa1n3kdc3fggdnciq8x3zl63b7flk42mcmj6lb")))

