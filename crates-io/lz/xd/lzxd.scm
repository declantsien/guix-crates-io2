(define-module (crates-io lz xd lzxd) #:use-module (crates-io))

(define-public crate-lzxd-0.1.0 (c (n "lzxd") (v "0.1.0") (h "05hpm2llflwjia5bw25r7wq5ha1bgkc9szrx7vl9jxmvgk395bpm")))

(define-public crate-lzxd-0.1.1 (c (n "lzxd") (v "0.1.1") (h "1a39gws6f2z8f3zmd1kjinqypn14rg4dzy8diyl2r8ksxr8mlnm4")))

(define-public crate-lzxd-0.1.2 (c (n "lzxd") (v "0.1.2") (h "1y8x40fwzj6yc5hcvlixkyynm9sv6187rc9bzqsrh53b40rs331r")))

(define-public crate-lzxd-0.1.3 (c (n "lzxd") (v "0.1.3") (h "0pgkig1rfjgircay9qs25br6gxirdcfwyg562idcybz8j1ywi396")))

(define-public crate-lzxd-0.1.4 (c (n "lzxd") (v "0.1.4") (h "04xjqs0n9yrl5ifsr2bq1aqqqa2amnj3z5ny8pdxznfx1pr64i3q")))

(define-public crate-lzxd-0.2.0 (c (n "lzxd") (v "0.2.0") (h "1x2x11a2y8awlc44bzb07lpm550k602b1kan4jmxclxnyd1aw7ry")))

(define-public crate-lzxd-0.2.1 (c (n "lzxd") (v "0.2.1") (h "0pcwa6h6gxz9mpz3zzxmhsb7svyl2bzk5jh66xgj59gxgl908xb1")))

(define-public crate-lzxd-0.2.2 (c (n "lzxd") (v "0.2.2") (h "0g0a8gl1vrqlw0c2p0x3rx2m9fm66p05v8dn1y0fr2a7rnilgxmk")))

(define-public crate-lzxd-0.2.3 (c (n "lzxd") (v "0.2.3") (h "020nffy6akp2jflcbs6zcx8wn4xs80vc15j6ibfjk2lii42m198w")))

(define-public crate-lzxd-0.2.4 (c (n "lzxd") (v "0.2.4") (h "0v4ril3kqwr9z6dfmf2qlhvpb6apy0lfn8ni4arvbxs9k11n6kyy")))

(define-public crate-lzxd-0.2.5 (c (n "lzxd") (v "0.2.5") (h "0090w6yjgcg5267mmf7n2qllm76widnxa4bdssd440ri31m37rsx")))

