(define-module (crates-io lz f_ lzf_headers) #:use-module (crates-io))

(define-public crate-lzf_headers-0.1.0 (c (n "lzf_headers") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "0q274av3w4ifiwmxi1vf5nf6ljmxi7jvb63498drb6gq062v2als")))

(define-public crate-lzf_headers-0.1.1 (c (n "lzf_headers") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "1n5pb6lk3q2pcw8mkc35b39zj6js7dz5j3qlhx336naljah6gh55")))

