(define-module (crates-io lz ok lzokay) #:use-module (crates-io))

(define-public crate-lzokay-1.0.0 (c (n "lzokay") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "1063i42c2317xk7lbk1nviffnw66ni8riw4zi50krjnav24ii29q") (f (quote (("std" "alloc") ("default" "compress" "decompress" "std") ("decompress") ("compress") ("alloc"))))))

(define-public crate-lzokay-1.0.1 (c (n "lzokay") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0sc0ccfsbi81kkzlybx3nn5va9q6v1j94mgqbcd304rhv8vf526g") (f (quote (("std" "alloc") ("default" "compress" "decompress" "std") ("decompress") ("compress") ("alloc"))))))

