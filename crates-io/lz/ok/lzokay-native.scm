(define-module (crates-io lz ok lzokay-native) #:use-module (crates-io))

(define-public crate-lzokay-native-0.1.0 (c (n "lzokay-native") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (o #t) (d #t) (k 0)) (d (n "minilzo-rs") (r "^0.6.0") (d #t) (k 2)) (d (n "sha1") (r "^0.10.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0c475icg27hinwjlh11mqz5va8dr9q7n73lq7rn8qyfjmmkscavr") (f (quote (("default" "compress" "decompress") ("decompress" "byteorder") ("compress"))))))

