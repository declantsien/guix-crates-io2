(define-module (crates-io lz ma lzma2) #:use-module (crates-io))

(define-public crate-lzma2-0.1.0 (c (n "lzma2") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "lzma") (r "^0.2") (d #t) (k 0)))) (h "17zcy5v3mll3hys9wg1ilrhy7wh0qrp4d2hw0gnm14qalnnb3r8y")))

