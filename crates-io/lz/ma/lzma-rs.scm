(define-module (crates-io lz ma lzma-rs) #:use-module (crates-io))

(define-public crate-lzma-rs-0.1.0 (c (n "lzma-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1dv7cp5wcj3zpdpx8bv6qc7ibb68wgmbal2257xml8z4s8ccqjax")))

(define-public crate-lzma-rs-0.1.1 (c (n "lzma-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1xcsvcdrw557cf3jkmzpgd8iq553ycxfkg3kybknpfynlhsvll4m")))

(define-public crate-lzma-rs-0.1.2 (c (n "lzma-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0n2b3a5d3wz9vnbcpilwhib8cz3v3d1xhljvny70ifaiga2hc1md")))

(define-public crate-lzma-rs-0.1.3 (c (n "lzma-rs") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "07qhqg0mf1scw93i92sqryghxxmn3rsyavs71j3bnr1vgvsq3hdd") (f (quote (("enable_logging" "env_logger" "log"))))))

(define-public crate-lzma-rs-0.2.0 (c (n "lzma-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5") (d #t) (k 2)))) (h "1cbz5pdsjkjn71ba1bgp467zxpvm1bnmw23jmpjbryhd8nqfra5b") (f (quote (("stream") ("enable_logging" "env_logger" "log"))))))

(define-public crate-lzma-rs-0.1.4 (c (n "lzma-rs") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "0h894fg3vhxlf8v3pjm8hz7x7imp2lavsdnbzzhbw7s82v4v6614") (f (quote (("enable_logging" "env_logger" "log"))))))

(define-public crate-lzma-rs-0.3.0 (c (n "lzma-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5") (d #t) (k 2)))) (h "0phif4pnjrn28zcxgz3a7z86hhx5gdajmkrndfw4vrkahd682zi9") (f (quote (("stream") ("raw_decoder") ("enable_logging" "env_logger" "log"))))))

