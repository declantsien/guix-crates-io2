(define-module (crates-io lz ma lzma-sys) #:use-module (crates-io))

(define-public crate-lzma-sys-0.1.0 (c (n "lzma-sys") (v "0.1.0") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18bnyfh6rcfm78v4q5vjah6dfbai12x579npjvn3j32398ix4mpj")))

(define-public crate-lzma-sys-0.1.1 (c (n "lzma-sys") (v "0.1.1") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zc9gvnf2sfy8z3qaimwlp9kfd0mkm19b3s50xsg908q97ijasjh")))

(define-public crate-lzma-sys-0.1.2 (c (n "lzma-sys") (v "0.1.2") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "064r6h2wf2nqdslpdpqr50dgj6bw5gcjjd88kfnww3mg4nr03xv2")))

(define-public crate-lzma-sys-0.1.3 (c (n "lzma-sys") (v "0.1.3") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j326qrfwggf7xk75fpsw1xb89r84h21n01cxs1798aznd9smsn5")))

(define-public crate-lzma-sys-0.1.4 (c (n "lzma-sys") (v "0.1.4") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yis0527by4vsikpfk70xlgid1y1qnd4wy74dvn98i5jrfjzdpzy")))

(define-public crate-lzma-sys-0.1.5 (c (n "lzma-sys") (v "0.1.5") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wpm9yf779pd4nzv80wsknvmyx0ccb3809bnkx9khzfbwhhrhlsb")))

(define-public crate-lzma-sys-0.1.6 (c (n "lzma-sys") (v "0.1.6") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cnvfzmx36vdw8fq8zkhmnlkgasqvh75k1j35wd0jj5mzvzhyvyh")))

(define-public crate-lzma-sys-0.1.7 (c (n "lzma-sys") (v "0.1.7") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y3sysliwl1jb7baj95znnb2hknbgk5rxavdqi061lmpdr8acgq1")))

(define-public crate-lzma-sys-0.1.8 (c (n "lzma-sys") (v "0.1.8") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02yh4v65l5nh7yhrjn1s0mjcj29aqn81yir5azq89awpxccf7ck6")))

(define-public crate-lzma-sys-0.1.9 (c (n "lzma-sys") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "filetime") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nni8r9xajdr7ld2x45sphm1db11an7zqdqqr3d3g1wyz1w3pff1")))

(define-public crate-lzma-sys-0.1.10 (c (n "lzma-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "filetime") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05764byvs7fkjv8f15mg6di2yhdhsrkyq3mv3199fh9580ks1sni") (l "lzma")))

(define-public crate-lzma-sys-0.1.11 (c (n "lzma-sys") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dk8nam5gd873k1c5idnrd3ygixb70s9yxrnnaivjblr3qzx0jql") (l "lzma")))

(define-public crate-lzma-sys-0.1.12 (c (n "lzma-sys") (v "0.1.12") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "16q9jnlzr7nf4wn9ymm479awn6rxccifdc8xsib78q3mk44n5iah") (l "lzma")))

(define-public crate-lzma-sys-0.1.13 (c (n "lzma-sys") (v "0.1.13") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1qsflfxvkqwbs0zbnhbq8z6ad1a6pzfxbk8w66dvlildf445k0sa") (l "lzma")))

(define-public crate-lzma-sys-0.1.14 (c (n "lzma-sys") (v "0.1.14") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0dbz5rkvsrrm1jxkzclkgjppishjlcd46l4zk7kkjkfcayfcbd8n") (l "lzma")))

(define-public crate-lzma-sys-0.1.15 (c (n "lzma-sys") (v "0.1.15") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "14gyj256yh0wm77jbvmlc39v7lfn0navpfrja4alczarzlc8ir2k") (l "lzma")))

(define-public crate-lzma-sys-0.1.16 (c (n "lzma-sys") (v "0.1.16") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "16dc9mqxq9a937zz35ihl26yhz51phkf7mhm36ij7b588kn7ckzj") (f (quote (("static")))) (l "lzma")))

(define-public crate-lzma-sys-0.1.17 (c (n "lzma-sys") (v "0.1.17") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "06fnjsx5cj2w6rsqb12x30nl9lnj0xv4hv78z4x1vlfsxp1vgd5x") (f (quote (("static")))) (l "lzma")))

(define-public crate-lzma-sys-0.1.18 (c (n "lzma-sys") (v "0.1.18") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1ysscazcdvf33f1dslzch6scynww2k0azbvnv4vdz6gx7mwn4g5q") (f (quote (("static")))) (y #t) (l "lzma")))

(define-public crate-lzma-sys-0.1.19 (c (n "lzma-sys") (v "0.1.19") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0wl7ibvqw56x3vakpv8mlg6m3242zflmqrnm4z3ljzglmk258rz0") (f (quote (("static")))) (l "lzma")))

(define-public crate-lzma-sys-0.1.20 (c (n "lzma-sys") (v "0.1.20") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "09sxp20waxyglgn3cjz8qjkspb3ryz2fwx4rigkwvrk46ymh9njz") (f (quote (("static")))) (l "lzma")))

