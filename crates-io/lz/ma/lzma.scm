(define-module (crates-io lz ma lzma) #:use-module (crates-io))

(define-public crate-lzma-0.1.0 (c (n "lzma") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "12giad32h6r4zl4dbx4h4bdsq5jyp4l3hbwj7n74i7scy4wfzsz5")))

(define-public crate-lzma-0.1.1 (c (n "lzma") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1p255qlwdj08qna17f09d63gx3kgwvaviv9qllb6ljagdxl3z12h")))

(define-public crate-lzma-0.2.0 (c (n "lzma") (v "0.2.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "08p55i7p8bc1arbaip8cz6vn8v3v1jir32nn2lc06m3ghmrdp1cg")))

(define-public crate-lzma-0.2.0-1 (c (n "lzma") (v "0.2.0-1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0mxmn4rbyqm4r1998875vi68v9nhw4s9rcildz048l493b5v4kpf")))

(define-public crate-lzma-0.2.1 (c (n "lzma") (v "0.2.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "103czdykrpwh485kkimps9132af6j27hfxjlzgj61bbwrain3bkb")))

(define-public crate-lzma-0.2.2 (c (n "lzma") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1r6lvyxqhyncccw6pybrcjlnvbhr67ffj5bc714i7g788bss6avq")))

