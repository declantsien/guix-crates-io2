(define-module (crates-io lz ma lzma-rs-perf-exp) #:use-module (crates-io))

(define-public crate-lzma-rs-perf-exp-0.2.1 (c (n "lzma-rs-perf-exp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5") (d #t) (k 2)) (d (n "seq-macro") (r "^0.3") (d #t) (k 2)))) (h "1vhqpw2kqzvq6d23wap74dij8qgi7hf88hl2r44b92sl0l9mqhrq") (f (quote (("stream") ("raw_decoder") ("enable_logging" "env_logger" "log"))))))

