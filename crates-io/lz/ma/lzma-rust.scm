(define-module (crates-io lz ma lzma-rust) #:use-module (crates-io))

(define-public crate-lzma-rust-0.1.0 (c (n "lzma-rust") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1n1nbcrqrzb7z5lxfpy2dmya3q73q6jsl30qjw96dyik0v9pnnf5") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.1 (c (n "lzma-rust") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1hi5f4vn5v4hkv5aqygyllibajs4mgdfm028609w4yd9rdyc73c0") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.2 (c (n "lzma-rust") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0z0ma3sics6k0qqsjyns4rzzs3pxxch0xm9wy1f75ssm9bznlkbh") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.3 (c (n "lzma-rust") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1rvcdzhmj1dhirsfwhmpypq6cd2yrf1a52j316zzvxq1w9yf7niv") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.4 (c (n "lzma-rust") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1qfh0ikd4k400sv07j1c7l8z87l58w632h03zxb05krbzpasspcx") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.5 (c (n "lzma-rust") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "19bkhqishxif645kzhsadabachwnp35lrayvf35l1h3c2qr82ybz") (f (quote (("encoder") ("default" "encoder"))))))

(define-public crate-lzma-rust-0.1.6 (c (n "lzma-rust") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)))) (h "12dj3w2pnvx014pzjl8pc115rldgk6cbc7w6lwg24y2d3xfwzvfm") (f (quote (("encoder") ("default" "encoder")))) (r "1.60.0")))

