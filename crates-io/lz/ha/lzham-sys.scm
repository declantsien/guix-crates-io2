(define-module (crates-io lz ha lzham-sys) #:use-module (crates-io))

(define-public crate-lzham-sys-0.1.0 (c (n "lzham-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0g5ycq5lnk1lqx5pa6m0v50wqmab551ya7y2crwv5qj76jq8c3wj") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic")))) (y #t)))

(define-public crate-lzham-sys-0.1.1 (c (n "lzham-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0mnpxsxpbwjbhkkd6mdpsmnjjw4bk8rk4s3lql927gyj0vrcr5sy") (f (quote (("static") ("generate_binding" "bindgen") ("dynamic"))))))

