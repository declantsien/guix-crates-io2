(define-module (crates-io lz #{4j}# lz4jb) #:use-module (crates-io))

(define-public crate-lz4jb-0.1.0 (c (n "lz4jb") (v "0.1.0") (d (list (d (n "lz4_flex") (r "^0.8") (f (quote ("std" "safe-encode" "safe-decode"))) (k 0)) (d (n "twox-hash") (r "^1") (k 0)))) (h "048i50jk783qdrnir9s72xl3kvdjr51ggw2xkg8lmlbjp54rja0m")))

