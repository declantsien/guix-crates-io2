(define-module (crates-io lz fo lzfoo) #:use-module (crates-io))

(define-public crate-lzfoo-0.1.0 (c (n "lzfoo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "lzfse_rust") (r "^0.1.0") (d #t) (k 0)))) (h "0pnw0qqry516wc7y9zqwik5q14hbgrnp6mnbgl6s9455plafhqbz") (y #t)))

(define-public crate-lzfoo-0.2.0 (c (n "lzfoo") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "lzfse_rust") (r "^0.2.0") (d #t) (k 0)))) (h "0zahalii991440w9w4v4a1pigj0zrxh054qi8nsmxiwgjgddby6i")))

