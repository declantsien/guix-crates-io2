(define-module (crates-io lz _f lz_fnv) #:use-module (crates-io))

(define-public crate-lz_fnv-0.1.0 (c (n "lz_fnv") (v "0.1.0") (d (list (d (n "extprim") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "extprim_literals") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "10dbn23lhj6zwmknpqj1lgdn4fy8n55mx3vr0d59hij3yldk5pji") (f (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

(define-public crate-lz_fnv-0.1.1 (c (n "lz_fnv") (v "0.1.1") (d (list (d (n "extprim") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "extprim_literals") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0jk9gg1djn6s13sgvgw3g8xwcnw4q23lw5yq1xayp8gwphkic8ja") (f (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

(define-public crate-lz_fnv-0.1.2 (c (n "lz_fnv") (v "0.1.2") (d (list (d (n "extprim") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "extprim_literals") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0f4fn8axxj6f1krjdvla03l6z18ivh5lwvs6m9p9gw2ipq6ipfwv") (f (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

