(define-module (crates-io lz o1 lzo1x) #:use-module (crates-io))

(define-public crate-lzo1x-0.1.0 (c (n "lzo1x") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lzo-sys") (r "^0.3.1") (d #t) (k 2)) (d (n "zip") (r "^0.6.6") (d #t) (k 2)))) (h "0xs1lykn9ig4dcfjd9b9rkjc7j794pv4g40a4cyxf64v3lphrkb6") (r "1.75.0")))

(define-public crate-lzo1x-0.2.0 (c (n "lzo1x") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lzo-sys") (r "^0.3.1") (d #t) (k 2)) (d (n "zip") (r "^2.1.0") (d #t) (k 2)))) (h "04mvhfsvf4n9d6adsgmjxrg8i7zhb7knvwdsrhhhs13wxckn06yi") (f (quote (("std") ("default" "std")))) (r "1.75.0")))

