(define-module (crates-io lz o1 lzo1x-1) #:use-module (crates-io))

(define-public crate-lzo1x-1-0.1.0 (c (n "lzo1x-1") (v "0.1.0") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)))) (h "1gr8d13zy1mhsyx8vh5zl6z0ysrhcvahy1ia84irzbahaq4hcn7n") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

