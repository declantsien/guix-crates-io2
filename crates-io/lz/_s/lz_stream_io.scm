(define-module (crates-io lz _s lz_stream_io) #:use-module (crates-io))

(define-public crate-lz_stream_io-0.1.0 (c (n "lz_stream_io") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)))) (h "1kcr11phpc6rldn658ph69y65fg0d5wvhv0zm0zdjhqx69rd9ixd")))

