(define-module (crates-io lz _s lz_shared_udp) #:use-module (crates-io))

(define-public crate-lz_shared_udp-0.1.0 (c (n "lz_shared_udp") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)))) (h "1xr6m215pgisvp3gr5z3ijm9f5nh3n54qdap85xkk3yswl44ph78")))

(define-public crate-lz_shared_udp-0.1.1 (c (n "lz_shared_udp") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)))) (h "0s41dmig1nm0ci733wm0vfgvmxbpgg51m89533d0k4afgzfbdhzy")))

(define-public crate-lz_shared_udp-0.1.2 (c (n "lz_shared_udp") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)))) (h "1h4r3698837rl2czdjiw4wynvxzi73zjsc4i7wphrh15m16z7frp")))

