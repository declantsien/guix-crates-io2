(define-module (crates-io yo -h yo-html) #:use-module (crates-io))

(define-public crate-yo-html-0.1.0 (c (n "yo-html") (v "0.1.0") (d (list (d (n "implicit-clone") (r "^0.4.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Element" "Document" "Window" "HtmlElement" "Text" "DocumentFragment" "Event" "HtmlInputElement"))) (d #t) (k 2)) (d (n "xtask-wasm") (r "^0.2") (f (quote ("run-example"))) (d #t) (k 2)))) (h "0qqrzi5ij882af80ya9bz3l1ijl119hrhpjssfkals442f9qxl10") (r "1.76")))

