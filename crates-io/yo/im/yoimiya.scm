(define-module (crates-io yo im yoimiya) #:use-module (crates-io))

(define-public crate-yoimiya-0.1.0 (c (n "yoimiya") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "084sl7blj8x1gyqjy4zc7qq6ba84hd7an2cz09q6y0wny04mi7d2")))

