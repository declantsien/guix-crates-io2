(define-module (crates-io yo us youstat) #:use-module (crates-io))

(define-public crate-youstat-0.1.0 (c (n "youstat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "007c4lh26gpsbzy4mz9a2yxjsj780j0gj9g5xi5lzl2izi4vlpn9")))

