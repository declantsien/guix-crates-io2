(define-module (crates-io yo ke yoke-derive) #:use-module (crates-io))

(define-public crate-yoke-derive-0.0.1 (c (n "yoke-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.2.0") (d #t) (k 2)))) (h "0yri62il1irh9qk56jxvb78mlspkm10zc75fpirv6gry92yz9rnc")))

(define-public crate-yoke-derive-0.1.0 (c (n "yoke-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.2.0") (d #t) (k 2)) (d (n "zerovec") (r "^0.2.3") (f (quote ("yoke"))) (d #t) (k 2)))) (h "09s1wx17zi7j6nxqwx3qsv42w8disq6k8827klvxh4s1z7gmmmvm")))

(define-public crate-yoke-derive-0.1.1 (c (n "yoke-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.2.0") (d #t) (k 2)) (d (n "zerovec") (r "^0.2.3") (f (quote ("yoke"))) (d #t) (k 2)))) (h "14ndzr41glmwkkl205riyl3ipkly2pkzq9jybxpsisl3z38r61js")))

(define-public crate-yoke-derive-0.1.2 (c (n "yoke-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "zerovec") (r "^0.4") (f (quote ("yoke"))) (d #t) (k 2)))) (h "1ih8zlpb2zhh5vzw5al1ijpwjrqjx15v7ggk360x00zd9chgq9ba")))

(define-public crate-yoke-derive-0.4.0 (c (n "yoke-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0x0sn5kw0vb0f9pa5jh97hyf7i2qgl43r3l560nmkhc64g6hws9j")))

(define-public crate-yoke-derive-0.4.1 (c (n "yoke-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.4.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "zerovec") (r "^0.6") (f (quote ("yoke"))) (d #t) (k 2)))) (h "0hary5gyajz0483wd1qq9fv04r0yx58hdzidig4aq9jjg5qh5dvp")))

(define-public crate-yoke-derive-0.4.2 (c (n "yoke-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "zerovec") (r "^0.6") (f (quote ("yoke"))) (d #t) (k 2)))) (h "1j9cs4n5h0xabnjmxzwm8si6d7d2c94dhagfj297isnj9185y1jy") (y #t)))

(define-public crate-yoke-derive-0.5.0 (c (n "yoke-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "zerovec") (r "^0.6") (f (quote ("yoke"))) (d #t) (k 2)))) (h "17rx0xqynay79ich9hna93g19qfxf5sqf74l4pggamzv3kk133vn")))

(define-public crate-yoke-derive-0.6.0 (c (n "yoke-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "zerovec") (r "^0.7") (f (quote ("yoke"))) (d #t) (k 2)))) (h "0x23dkh3cndjfdwvd7pxniw0hpw31ldhjsgcp70qb6wjgjxwbhjq")))

(define-public crate-yoke-derive-0.6.1 (c (n "yoke-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "yoke") (r "^0.6.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "zerovec") (r "^0.9") (f (quote ("yoke"))) (d #t) (k 2)))) (h "0kmiarz2xbw9d345bwsmxaaajcxbcpmwgsk6hnw1is2s0b6y8ihk")))

(define-public crate-yoke-derive-0.7.0 (c (n "yoke-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1jx8nh47gp5bnfy1l32hr5qwm7slig4hb0k0y9afx9xpsrrhv06a")))

(define-public crate-yoke-derive-0.7.1 (c (n "yoke-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "18mpypz6xpcxni3bx3j5aygj46v5s9b2p8qaxxp53mryj9lw2img")))

(define-public crate-yoke-derive-0.7.2 (c (n "yoke-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)) (d (n "yoke") (r "^0.7.1") (k 2)) (d (n "zerovec") (r "^0.9.4") (k 2)))) (h "0pxk3pdihc2kbxvchl249a8nzrg0adza7zq3ajmjn020xnv9zqfm")))

(define-public crate-yoke-derive-0.7.3 (c (n "yoke-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1f52qcg6vmqh9l1wfa8i32hccmpmpq8ml90w4250jn74rkq3cscy")))

(define-public crate-yoke-derive-0.7.4 (c (n "yoke-derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0.61") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.21") (f (quote ("fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "15cvhkci2mchfffx3fmva84fpmp34dsmnbzibwfnzjqq3ds33k18")))

