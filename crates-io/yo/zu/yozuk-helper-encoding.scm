(define-module (crates-io yo zu yozuk-helper-encoding) #:use-module (crates-io))

(define-public crate-yozuk-helper-encoding-0.21.0 (c (n "yozuk-helper-encoding") (v "0.21.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.21.0") (d #t) (k 0)))) (h "0rhm6jpb5gf3s48lpiqkwln9nlpznipmx0fs2a5q7bzksp2aqji9")))

(define-public crate-yozuk-helper-encoding-0.21.1 (c (n "yozuk-helper-encoding") (v "0.21.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.21.0") (d #t) (k 0)))) (h "1m8l73wc2mjxy82g9q717mbxbipj8223bbv8qlwr0r21cvshg5vq")))

(define-public crate-yozuk-helper-encoding-0.22.0 (c (n "yozuk-helper-encoding") (v "0.22.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.22.0") (d #t) (k 0)))) (h "1mdi8k56knrwqail38g9c7x7xgkwiwi069dbf2bf4b025m0sl00m")))

(define-public crate-yozuk-helper-encoding-0.22.6 (c (n "yozuk-helper-encoding") (v "0.22.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.22.0") (d #t) (k 0)))) (h "1zvgkiicaq5pmmasfw4i83wr29gmxxrbiyj35svdn686lwvy602b")))

(define-public crate-yozuk-helper-encoding-0.22.7 (c (n "yozuk-helper-encoding") (v "0.22.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.22.7") (d #t) (k 0)))) (h "1dy4jzlkzr9alixqni52q50absrwizm3fw5jr2z7bgf6wfp3hnpm")))

(define-public crate-yozuk-helper-encoding-0.22.11 (c (n "yozuk-helper-encoding") (v "0.22.11") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "yozuk-sdk") (r "^0.22.11") (d #t) (k 0)))) (h "1aygnr45wvy91j00ncx5ga39z4mmw867pw6i26l763as4ivdkijs")))

