(define-module (crates-io yo zu yozuk-bundle) #:use-module (crates-io))

(define-public crate-yozuk-bundle-0.4.0 (c (n "yozuk-bundle") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.4.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.1.0") (d #t) (k 1)))) (h "169frp04rkzr7ddi0hwm2p8m5cklmk2rbvqp70b9ncm09fsg6za9")))

(define-public crate-yozuk-bundle-0.4.2 (c (n "yozuk-bundle") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.4.2") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.1.0") (d #t) (k 1)))) (h "14p45psw8ysdk5b9xlyqy5y0byr5qrh4dilixamafk2vmrwh6pvx")))

(define-public crate-yozuk-bundle-0.5.0 (c (n "yozuk-bundle") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.5.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.2.0") (d #t) (k 1)))) (h "0wy5wgypfxzacz9w0rr3pa7f9ps6rkk2qkkd2hy9asbz6zhxx7dl")))

(define-public crate-yozuk-bundle-0.6.0 (c (n "yozuk-bundle") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.6.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.3.0") (d #t) (k 1)))) (h "0g3kvrwhj4j3hzkxizsc8nhamdhxzqff0fv8m08qi21lrw3958bw")))

(define-public crate-yozuk-bundle-0.7.0 (c (n "yozuk-bundle") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.7.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.4.0") (d #t) (k 1)))) (h "10cn5v95wnbif4fbi8cdanrz740ac7n00fwypxk41hfhnws8rddv")))

(define-public crate-yozuk-bundle-0.8.0 (c (n "yozuk-bundle") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.8.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.8.0") (d #t) (k 1)))) (h "08mskdwjqnp6bdq065yqy1m8rz22ryasaclyahzl8qj1s4phraba")))

(define-public crate-yozuk-bundle-0.9.0 (c (n "yozuk-bundle") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "yozuk") (r "^0.9.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.9.0") (d #t) (k 1)))) (h "19w5dvxyy9zamd58h5l73plz5hvdqh732ajirrlzpk8c8bfg8ax6")))

(define-public crate-yozuk-bundle-0.10.0 (c (n "yozuk-bundle") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.10.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.10.0") (d #t) (k 1)))) (h "0d1cvbc4n8c9gaixscsh8dgqra3h2gxs1vzmwc9pxzv4xpzpgrc5")))

(define-public crate-yozuk-bundle-0.11.0 (c (n "yozuk-bundle") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.11.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.11.0") (d #t) (k 1)))) (h "17g0b3vb4gi4a85b7syshp7f1k4fmsjwsr3p0v7vqnkwkya5lip1")))

(define-public crate-yozuk-bundle-0.11.1 (c (n "yozuk-bundle") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.11.1") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.11.1") (d #t) (k 1)))) (h "02svkawzyfa5sgb6zv9i433bdjkxjj5z4n8n1v79iw456iakc13f")))

(define-public crate-yozuk-bundle-0.11.2 (c (n "yozuk-bundle") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.11.2") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.11.1") (d #t) (k 1)))) (h "0sjpnpy5fhdgkf4bdpqjawjpki4dm91wi604h3kqw0n6cwrfh93m")))

(define-public crate-yozuk-bundle-0.12.0 (c (n "yozuk-bundle") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.12.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.12.0") (d #t) (k 1)))) (h "0gqp8vhfd6rm2vr2c6lpipd2ljzjhg6l9561k6k0js0fw4577vj9")))

(define-public crate-yozuk-bundle-0.13.0 (c (n "yozuk-bundle") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.13.0") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.13.0") (d #t) (k 1)))) (h "1ivryj373msi5x1mgk43jcw23qb0mgc0ms0bnbij796qn4b6ck9w")))

(define-public crate-yozuk-bundle-0.13.1 (c (n "yozuk-bundle") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 1)) (d (n "yozuk") (r "^0.13.1") (f (quote ("modelgen"))) (d #t) (k 1)) (d (n "yozuk-sdk") (r "^0.13.0") (d #t) (k 1)))) (h "0f980b4zyj1i41vldgk1kfzlwm7mpncs81dxb0jiyddi46bbi2dy")))

