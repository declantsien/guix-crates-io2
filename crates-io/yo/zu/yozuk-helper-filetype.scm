(define-module (crates-io yo zu yozuk-helper-filetype) #:use-module (crates-io))

(define-public crate-yozuk-helper-filetype-0.18.3 (c (n "yozuk-helper-filetype") (v "0.18.3") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.3") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "0q2hrfcjinf0a6xik5px2k8r4w0c2y0bq0hs48g30qz1571qqjjz")))

(define-public crate-yozuk-helper-filetype-0.19.1 (c (n "yozuk-helper-filetype") (v "0.19.1") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.3") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "1c96309r584ysjrvkaljwdykhlfg9pg5f6f9fwgimqd16dsvns6g")))

(define-public crate-yozuk-helper-filetype-0.20.2 (c (n "yozuk-helper-filetype") (v "0.20.2") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.4") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "0d32yxc6nmq6ihcpdqhhjiz0lmp727d70zvaxp9vcyiimadpvjhb")))

(define-public crate-yozuk-helper-filetype-0.20.6 (c (n "yozuk-helper-filetype") (v "0.20.6") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.5") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "0h31zlbwsi4jacx2jqrznz4dyr4gy4j35px8lwm9z6z979jjnh9d")))

(define-public crate-yozuk-helper-filetype-0.22.8 (c (n "yozuk-helper-filetype") (v "0.22.8") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.5") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "1wxwyaa09hpnwmrc31qrydixggzdi6brys1dabrss06gkwp6gj0b")))

(define-public crate-yozuk-helper-filetype-0.22.11 (c (n "yozuk-helper-filetype") (v "0.22.11") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "file-format") (r "^0.6.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.9") (d #t) (k 0)) (d (n "mime2ext") (r "^0.1.52") (d #t) (k 0)))) (h "1j5wbhwm0kvj52b2ccjfw01wwr0gylgqbggs6s6gf7rkabzz9625")))

