(define-module (crates-io yo zu yozuk-helper-preprocessor) #:use-module (crates-io))

(define-public crate-yozuk-helper-preprocessor-0.1.0 (c (n "yozuk-helper-preprocessor") (v "0.1.0") (d (list (d (n "yozuk-sdk") (r "^0.4.0") (d #t) (k 0)))) (h "0d4syl1xc53br04ayblkyhwb5gmyrciqffdd1s6nwhajwbxz73xj")))

(define-public crate-yozuk-helper-preprocessor-0.8.0 (c (n "yozuk-helper-preprocessor") (v "0.8.0") (d (list (d (n "yozuk-sdk") (r "^0.8.0") (d #t) (k 0)))) (h "1sgf3bj1jw2s0996d1kvlkab0xfcdzw71f6xjzx7xpfd7i5f1q53")))

(define-public crate-yozuk-helper-preprocessor-0.9.0 (c (n "yozuk-helper-preprocessor") (v "0.9.0") (d (list (d (n "yozuk-sdk") (r "^0.9.0") (d #t) (k 0)))) (h "0np8mj7f3vapjcl8m21p7l21962kwzzw49m0f0iyyw0jcdkj2053")))

(define-public crate-yozuk-helper-preprocessor-0.10.0 (c (n "yozuk-helper-preprocessor") (v "0.10.0") (d (list (d (n "yozuk-sdk") (r "^0.10.0") (d #t) (k 0)))) (h "0xh75l5cfzy9scyvwyw2ajj3835h1vc70fxcyaabplvdsigqf3m4")))

(define-public crate-yozuk-helper-preprocessor-0.11.0 (c (n "yozuk-helper-preprocessor") (v "0.11.0") (d (list (d (n "yozuk-sdk") (r "^0.11.0") (d #t) (k 0)))) (h "1zavx6kbldgy05sl26q3vl1cf8xrk348lsxsg7k82haq2rl2dmwx")))

(define-public crate-yozuk-helper-preprocessor-0.11.1 (c (n "yozuk-helper-preprocessor") (v "0.11.1") (d (list (d (n "yozuk-sdk") (r "^0.11.1") (d #t) (k 0)))) (h "05cs705y086yvwpswn13zgvg1k3wbf0wc5n42qkwy1llh6i9i7pj")))

(define-public crate-yozuk-helper-preprocessor-0.12.0 (c (n "yozuk-helper-preprocessor") (v "0.12.0") (d (list (d (n "yozuk-sdk") (r "^0.12.0") (d #t) (k 0)))) (h "1ilzqhzpz2jbvkrf46w2gznhkcgs2q7r53cvfffmg041dm7m94h0")))

(define-public crate-yozuk-helper-preprocessor-0.13.0 (c (n "yozuk-helper-preprocessor") (v "0.13.0") (d (list (d (n "yozuk-sdk") (r "^0.13.0") (d #t) (k 0)))) (h "1310fkmw9wzibgcv5i0c0pq3j6s35jlq3ykg0g4ap4df3qw9zgr9")))

(define-public crate-yozuk-helper-preprocessor-0.14.0 (c (n "yozuk-helper-preprocessor") (v "0.14.0") (d (list (d (n "yozuk-sdk") (r "^0.14.0") (d #t) (k 0)))) (h "10mh0r798vpj2ahaxn486k2282alkccqzbwr7mybzy0sc6snnip6")))

(define-public crate-yozuk-helper-preprocessor-0.14.1 (c (n "yozuk-helper-preprocessor") (v "0.14.1") (d (list (d (n "yozuk-sdk") (r "^0.14.1") (d #t) (k 0)))) (h "1ihsq3q15ilp7va22dfcy0vqxs8zpcnyfkanp6vp71xds6i626gb")))

(define-public crate-yozuk-helper-preprocessor-0.14.2 (c (n "yozuk-helper-preprocessor") (v "0.14.2") (d (list (d (n "yozuk-sdk") (r "^0.14.2") (d #t) (k 0)))) (h "07d8aki3q3654vp5hzinm8a0gzmaddyzk66w91jlvzafzma0z32d")))

(define-public crate-yozuk-helper-preprocessor-0.15.0 (c (n "yozuk-helper-preprocessor") (v "0.15.0") (d (list (d (n "yozuk-sdk") (r "^0.15.0") (d #t) (k 0)))) (h "176k98zwsac5gzjz9l2m263nbavh55wrkk81n671rgaqgh2hjn1f")))

(define-public crate-yozuk-helper-preprocessor-0.15.1 (c (n "yozuk-helper-preprocessor") (v "0.15.1") (d (list (d (n "yozuk-sdk") (r "^0.15.1") (d #t) (k 0)))) (h "00hdidwmllxk4snpsddhafhrjad6c6s3apk7bsxmxj8vb15g7s03")))

(define-public crate-yozuk-helper-preprocessor-0.16.0 (c (n "yozuk-helper-preprocessor") (v "0.16.0") (d (list (d (n "yozuk-sdk") (r "^0.16.0") (d #t) (k 0)))) (h "0lhy6gv72hwrnvkg7wyvdkx87ama1wjd60g1srvf0j1h04b3r5np")))

(define-public crate-yozuk-helper-preprocessor-0.16.1 (c (n "yozuk-helper-preprocessor") (v "0.16.1") (d (list (d (n "yozuk-sdk") (r "^0.16.1") (d #t) (k 0)))) (h "1iih865x52qndf881vf6nz8m71g0s0ab5a4b37fcglni3g13a9mm")))

(define-public crate-yozuk-helper-preprocessor-0.16.2 (c (n "yozuk-helper-preprocessor") (v "0.16.2") (d (list (d (n "yozuk-sdk") (r "^0.16.2") (d #t) (k 0)))) (h "0r0va6rmkghc9w4c12lmckm6p3fkisgaggnb8gbk5dpajn5cpm99")))

(define-public crate-yozuk-helper-preprocessor-0.16.3 (c (n "yozuk-helper-preprocessor") (v "0.16.3") (d (list (d (n "yozuk-sdk") (r "^0.16.3") (d #t) (k 0)))) (h "0mvag4njh8zxxrnfyayg296qv16xpcin4ldk95kdgpw78qjkfzag")))

(define-public crate-yozuk-helper-preprocessor-0.17.0 (c (n "yozuk-helper-preprocessor") (v "0.17.0") (d (list (d (n "yozuk-sdk") (r "^0.17.0") (d #t) (k 0)))) (h "19wx9wphwxanzxgc5hb16pl8ahk1b42wsw6gacacjqv93cpfhwgc")))

(define-public crate-yozuk-helper-preprocessor-0.17.1 (c (n "yozuk-helper-preprocessor") (v "0.17.1") (d (list (d (n "yozuk-sdk") (r "^0.17.1") (d #t) (k 0)))) (h "1mgh5nzs930a08pi7c57yky81pbypd588vg3ax05lwc8qn98blvw")))

(define-public crate-yozuk-helper-preprocessor-0.18.0 (c (n "yozuk-helper-preprocessor") (v "0.18.0") (d (list (d (n "yozuk-sdk") (r "^0.18.0") (d #t) (k 0)))) (h "1n1gaap8bkbxi07r6m4jji2ynnskx1jca87s6jgxjn39c66wjmll")))

(define-public crate-yozuk-helper-preprocessor-0.19.1 (c (n "yozuk-helper-preprocessor") (v "0.19.1") (d (list (d (n "yozuk-sdk") (r "^0.19.1") (d #t) (k 0)))) (h "1c4p3j0c62v895dixpfxxi8cr14l85fzzamxnnc6ykmng5m7bg66")))

(define-public crate-yozuk-helper-preprocessor-0.20.0 (c (n "yozuk-helper-preprocessor") (v "0.20.0") (d (list (d (n "yozuk-sdk") (r "^0.20.0") (d #t) (k 0)))) (h "0a5dza06qbkr37a75cgmgfpi3bah94iqc6fwv31af1c9ibbwarqy")))

(define-public crate-yozuk-helper-preprocessor-0.20.2 (c (n "yozuk-helper-preprocessor") (v "0.20.2") (d (list (d (n "yozuk-sdk") (r "^0.20.2") (d #t) (k 0)))) (h "0q61csipbwinjyafrgx4dg9h015cwbigkbf5c35dai1iq9fa88fa")))

(define-public crate-yozuk-helper-preprocessor-0.20.6 (c (n "yozuk-helper-preprocessor") (v "0.20.6") (d (list (d (n "yozuk-sdk") (r "^0.20.6") (d #t) (k 0)))) (h "1i6wsmp327z4qlm4d6y7846kbxsyix6ilpr3j8sx4v7kqn32frd6")))

