(define-module (crates-io yo yo yoyo-physics) #:use-module (crates-io))

(define-public crate-yoyo-physics-0.1.0 (c (n "yoyo-physics") (v "0.1.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "12wsps453d1z3nxww1qm224mj6vzmvayxvb05wb8sljxb8gdppx4")))

(define-public crate-yoyo-physics-0.2.0 (c (n "yoyo-physics") (v "0.2.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "10yiv6w3zi6nr3i3cd0znflq2abv8gkk2a3w33n2k6wqs5qjs5h5")))

(define-public crate-yoyo-physics-0.3.0 (c (n "yoyo-physics") (v "0.3.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "0zcqrmijb47qca8ap6jn33vymi6fk9admhnp2pbd0k9jcvr955nf")))

(define-public crate-yoyo-physics-0.3.1 (c (n "yoyo-physics") (v "0.3.1") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "0h0h9xjl8qns5din8qdrxvvziaj1ws66nmmw9z2bcb5mr1aa2rp8")))

(define-public crate-yoyo-physics-0.3.2 (c (n "yoyo-physics") (v "0.3.2") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "0rsnca6pj1z0gg4kq6bk0vfcwnpx87hkdjw8iq30hgpqkhqgbzfy")))

(define-public crate-yoyo-physics-0.4.0 (c (n "yoyo-physics") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0wrgg5mav7y3ailkssjfq0120g8fbh4h0wa14pxi264d1gl6k33y")))

