(define-module (crates-io yo lk yolk) #:use-module (crates-io))

(define-public crate-yolk-0.1.0 (c (n "yolk") (v "0.1.0") (d (list (d (n "pest") (r "^2.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.0") (d #t) (k 0)))) (h "0idwlvirc0mkvar56c25q4z2n6lagad0h0wcyda2mg6r2jjgvi1b")))

(define-public crate-yolk-0.2.0 (c (n "yolk") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "yolol_number") (r "^0.2.0") (d #t) (k 0)) (d (n "yoloxide") (r "^0.3.2") (d #t) (k 0)))) (h "1gbbi2yll84qbhflzm4nqjcgj9p07jhpdhq0h89wf168v178vxmd")))

(define-public crate-yolk-0.3.0 (c (n "yolk") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "yolol_number") (r "^0.6.1") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4.1") (d #t) (k 0)))) (h "0zz2kacwmb4dfzhmki26i4h4s4innlrhyc7b5z1vw727w2vvhzwb")))

(define-public crate-yolk-0.4.0 (c (n "yolk") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "yolol_number") (r "^0.6.1") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4.1") (d #t) (k 0)))) (h "0b943q0xd0zii5zjansg9nnhifvc8fl17mb4s2bsfn8mac5d590n")))

(define-public crate-yolk-0.4.1 (c (n "yolk") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "yolol_number") (r "= 0.6.1") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4") (d #t) (k 0)))) (h "1rh3fbp8wx3kq9i847nlwnxsiynkgw9pvfdckqj0wv5zj2h2v0ax")))

(define-public crate-yolk-0.4.2 (c (n "yolk") (v "0.4.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "yolol_number") (r "= 0.6.1") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4") (d #t) (k 0)))) (h "0idssadlz1xbnsxksisg8gsjb2np6s99gnz9vgv2bh54x05cbl5h")))

(define-public crate-yolk-0.5.0 (c (n "yolk") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "yolol_number") (r "^0.6") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4") (d #t) (k 0)))) (h "12dzmzq9p2npgf1ylh9k76xjib751w6dpqfjaw4kg5zx3p6rky3m")))

(define-public crate-yolk-0.5.1 (c (n "yolk") (v "0.5.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "yolol_number") (r "^0.6") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4") (d #t) (k 0)))) (h "052hn9wx4yzbig87nz64aq0qcplxcsixl1rr769293g5v2a1lbbs")))

(define-public crate-yolk-0.6.0 (c (n "yolk") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "yolol_number") (r "^0.6") (d #t) (k 0)) (d (n "yoloxide") (r "^0.4") (d #t) (k 0)))) (h "1k1b8gf7jm8xx74mwim7x58x3zcrh437dj9dicnvx0c4jpz4haw0")))

