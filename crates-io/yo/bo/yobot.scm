(define-module (crates-io yo bo yobot) #:use-module (crates-io))

(define-public crate-yobot-0.1.0 (c (n "yobot") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack") (r "^0.18.0") (d #t) (k 0)))) (h "18jzddbby083z745vgs9327c6xcxlfgbicq8vqp250aal1snn3z4")))

(define-public crate-yobot-0.1.1 (c (n "yobot") (v "0.1.1") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack") (r "^0.18.0") (d #t) (k 0)))) (h "08axhhk8syvlzymvb6y4dk3iqplamwa6ikik523l2xw6vi4nddnp")))

