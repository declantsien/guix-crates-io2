(define-module (crates-io yo lo yolol-yaml-deserializer) #:use-module (crates-io))

(define-public crate-yolol-yaml-deserializer-0.1.0 (c (n "yolol-yaml-deserializer") (v "0.1.0") (d (list (d (n "libyaml") (r "^0.1.0") (d #t) (k 0)) (d (n "yolol-devices") (r "^0.3.1") (d #t) (k 0)))) (h "0yfxbvny1c04h9bnz0l2bh9g82c21j7qrp8wk47k6w1k2b5b44gm")))

(define-public crate-yolol-yaml-deserializer-0.1.1 (c (n "yolol-yaml-deserializer") (v "0.1.1") (d (list (d (n "libyaml") (r "^0.1.0") (d #t) (k 0)) (d (n "yolol-devices") (r "^0.3.3") (d #t) (k 0)))) (h "1rjhw19n4naslpdspjihbqn9xbpza560v7jndyk28lj94fpqkwg2")))

