(define-module (crates-io yo lo yolo-labels) #:use-module (crates-io))

(define-public crate-yolo-labels-0.1.0 (c (n "yolo-labels") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1n83695r4wjmsmdgw07sffm717rz6937ak33gciqcpq9vfwf1wb6")))

(define-public crate-yolo-labels-0.2.0 (c (n "yolo-labels") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0ws1gqabvg7b15zjgfdpvjzwnv5zhxlgl4rfglndv48dl555vifh")))

(define-public crate-yolo-labels-0.3.0 (c (n "yolo-labels") (v "0.3.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03kbq9c3k48ja7rc19w68d8k9iwapfxzi281c33hxincb5dk2a47")))

(define-public crate-yolo-labels-0.3.1 (c (n "yolo-labels") (v "0.3.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1yb3gljifsnnfby7i0k8xg47g6d3px2273b32bnxv9pbh7dca85p")))

(define-public crate-yolo-labels-0.4.0 (c (n "yolo-labels") (v "0.4.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1nh46jisib1xgbljsqfzv323aj21p3cvrrrm0vvrlbxi3ddqnxzl")))

(define-public crate-yolo-labels-0.5.0 (c (n "yolo-labels") (v "0.5.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06rgvl1yjgpypjn9h2nbd5cf47dnqca64jwx3wxmg0z79swk5a9h")))

