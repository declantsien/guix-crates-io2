(define-module (crates-io yo lo yolo) #:use-module (crates-io))

(define-public crate-yolo-0.1.0 (c (n "yolo") (v "0.1.0") (h "1k3l063qxfpj8sc4v2hsmb0sk0zlhpnsw5vgr5lgyc000f57z9rp")))

(define-public crate-yolo-0.1.1 (c (n "yolo") (v "0.1.1") (h "1qznvabrwmp51qinb98pycrzh3gk0dj86409v66fqsa9s04vvnyk")))

(define-public crate-yolo-0.1.2 (c (n "yolo") (v "0.1.2") (h "0hxjvz4717np6q92v8lm9wj32qs21l2n3lk86b6f2nsl49nsimlp")))

