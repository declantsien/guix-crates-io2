(define-module (crates-io yo lo yolol-runner) #:use-module (crates-io))

(define-public crate-yolol-runner-0.2.1 (c (n "yolol-runner") (v "0.2.1") (d (list (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "yolol-devices") (r "^0.2.5") (d #t) (k 0)))) (h "0hhkis857nx4zxn9jkdagsx4xw5qi54nk711ys1ywfwxhmq3cbbi")))

(define-public crate-yolol-runner-0.2.2 (c (n "yolol-runner") (v "0.2.2") (d (list (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "yolol-devices") (r "^0.3.1") (d #t) (k 0)))) (h "1bnfhkcy9f1qpdyg0p5nn6gppndvlj016if42zxlnm0rzqrvg01d")))

