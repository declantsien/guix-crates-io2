(define-module (crates-io yo lo yolol_number) #:use-module (crates-io))

(define-public crate-yolol_number-0.1.0 (c (n "yolol_number") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1qihhs8gcf8zhydwi3wpmjq5ypahpyci9fvfqf8c4r4cyddsajfn")))

(define-public crate-yolol_number-0.1.1 (c (n "yolol_number") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0ag994f4v9ml6r26yshbm3qs0ycff1fcr45f805asir7jx3i8rhp")))

(define-public crate-yolol_number-0.1.2 (c (n "yolol_number") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "17vxbdmz6rsrwml8ywbscisjgwlb4641sn6d3qp9mc2py6q7xly4")))

(define-public crate-yolol_number-0.1.3 (c (n "yolol_number") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0hs91rpa9y2v93rj927aalcgrxwk3kd2ynfy086j7m71sym3l7fs")))

(define-public crate-yolol_number-0.2.0 (c (n "yolol_number") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0bji75985qlk4rqbr3yjrdv58bxnqrc59q1fld5vp4f47j3xmhhb")))

(define-public crate-yolol_number-0.2.1 (c (n "yolol_number") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1n5hnk94sjkf1ajwjpx7x3vxdy0fph9mnfmn91zdp417zhf5fyy6")))

(define-public crate-yolol_number-0.2.2 (c (n "yolol_number") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "10qj4rm5zx8mpvm7n44x90pghflxdiky4rk91yhs4g1sjp2vysm2")))

(define-public crate-yolol_number-0.3.0 (c (n "yolol_number") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1qlykblmwh0hdnp2z26qx778ac734q19xyrgslp4vs2b8vh0m3xb")))

(define-public crate-yolol_number-0.4.0 (c (n "yolol_number") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "02h1wak51c7zbcn8k7r2lbkv8wgpsn0hid4cs9577n3r4pvw964i")))

(define-public crate-yolol_number-0.5.0 (c (n "yolol_number") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0fjliicrxcxvw0aqwx3352rymaxblh6g02mjkyvizyv3sdwnbfg0")))

(define-public crate-yolol_number-0.5.1 (c (n "yolol_number") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1jlknxffkkbqndvgsmhdi29hfbd7ppjb3sk9yn7bwm5zwfbg3ijz")))

(define-public crate-yolol_number-0.5.2 (c (n "yolol_number") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "03srq18b3a6qgj71586g4fvvlc9xd8axfc6a0gm8ygvk2ri6x159")))

(define-public crate-yolol_number-0.5.3 (c (n "yolol_number") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1p91ppjcsav47b8j952fdp5ffgd8007pjrp2cwi9i92lqlb6rv6l")))

(define-public crate-yolol_number-0.5.5 (c (n "yolol_number") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0h2w2fhzzy6328ywv4rmyid7ikp4xp4wc9v99mgbs5prvs4x6z5f")))

(define-public crate-yolol_number-0.5.6 (c (n "yolol_number") (v "0.5.6") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "17yjg9gyvqxfgc9y4d416dxwp3wixvzx43ikm1djrlm4jd2il731")))

(define-public crate-yolol_number-0.6.0 (c (n "yolol_number") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1f9ygyp192fgbwv038l8z6xf8d2qvsjf8ngjbir6shhhbjhz1cyb")))

(define-public crate-yolol_number-0.6.1 (c (n "yolol_number") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "118cciwlwkj8g3pbjzxm5h0ci9zs1p9kcdqi9mpmwnd8vdsq640c")))

(define-public crate-yolol_number-0.6.2 (c (n "yolol_number") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0n3xhjsfvcf8smkidxwgr6scxhm3ahlhlz0r8crpkgw73z089mrm")))

(define-public crate-yolol_number-0.7.0 (c (n "yolol_number") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1ig4g7bbdbg4wmcgvjc89mksqqf2b40kp8cn97iqb80j1rzbnzfv")))

(define-public crate-yolol_number-0.7.1 (c (n "yolol_number") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1hb6605kpqaf2hc4xw4vnvngapc5lbimqv33qhcslgpyahffn93w")))

(define-public crate-yolol_number-0.7.2 (c (n "yolol_number") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "19bsvdrcsh0g4rbrdj6ds93r7g2827kacdw2lp98y48rflyb6zqi")))

(define-public crate-yolol_number-0.8.0 (c (n "yolol_number") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0dbm103bsgbzimkximj6pwrvgx1fvzj2cvrdikx9334m3zg23x97")))

(define-public crate-yolol_number-0.9.0 (c (n "yolol_number") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1rggy05zrvq6lad3y9fv4jp33xzzw44mac2icicvzrlk7x2xxpl6")))

