(define-module (crates-io yo lo yolo2dot) #:use-module (crates-io))

(define-public crate-yolo2dot-0.1.0 (c (n "yolo2dot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17vg7klh0l5gmzhazcpib42y5zaaiz8w9radspvdlyfxx1wwincs")))

