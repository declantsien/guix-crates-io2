(define-module (crates-io yo lo yolo-rustc-bootstrap) #:use-module (crates-io))

(define-public crate-yolo-rustc-bootstrap-1.0.0 (c (n "yolo-rustc-bootstrap") (v "1.0.0") (h "0vwzkncx3vzps1lmvqxwv0xqva91kc9ihxwggsfmxxa9fg0m2dll")))

(define-public crate-yolo-rustc-bootstrap-1.0.1 (c (n "yolo-rustc-bootstrap") (v "1.0.1") (h "0c90mckixyqjf65k22y8zn8ig6cz61sksyrd1n5g5mvchs5rdd05")))

(define-public crate-yolo-rustc-bootstrap-1.0.2 (c (n "yolo-rustc-bootstrap") (v "1.0.2") (h "0d54ms5qv9ddrlbp8nq07r4z9fd87kiij8sw16wp3h155xs637bn")))

