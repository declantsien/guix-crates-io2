(define-module (crates-io yo uc youchoose) #:use-module (crates-io))

(define-public crate-youchoose-0.1.0 (c (n "youchoose") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0wzbpm4yrpiw7gvw04cfy34k90rkjyxqndsz3ggaggn5nwrwz1wd")))

(define-public crate-youchoose-0.1.1 (c (n "youchoose") (v "0.1.1") (d (list (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "10srnvk7wv7gklqkcrrfi225m2vkf0cby60ldiz25vrqhzp3lq1q")))

