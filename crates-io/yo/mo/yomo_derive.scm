(define-module (crates-io yo mo yomo_derive) #:use-module (crates-io))

(define-public crate-yomo_derive-0.2.0 (c (n "yomo_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kkskg331m1nxlljraizgbsbr0f2dx21y7pa1xgwn7lnc4x62rjh") (f (quote (("default"))))))

(define-public crate-yomo_derive-0.3.0 (c (n "yomo_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gmkkmiqp05p910v6mmb7zcvild17z594z9fclydncb9ajrl3336") (f (quote (("default"))))))

