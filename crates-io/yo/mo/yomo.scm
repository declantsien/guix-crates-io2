(define-module (crates-io yo mo yomo) #:use-module (crates-io))

(define-public crate-yomo-0.1.0 (c (n "yomo") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bndprsxjl9vj5941n8z45yqdwxzrzbgp8l7i3149wwjq5zqibnc") (y #t)))

(define-public crate-yomo-0.1.1 (c (n "yomo") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02nhvkf6lrfp1arn5v978ndkkfl5qx1bkc5vswhbnfaivvz3ia32") (y #t)))

(define-public crate-yomo-0.1.2 (c (n "yomo") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04dis2ynrig6wwpa8d0f2fbgxn7aa62p54zrgryp6agygj6xscwn") (y #t)))

(define-public crate-yomo-0.1.3 (c (n "yomo") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13xh54w0dqw60vpbv6095rsnb8qxw0619g4iyfvjl9hl9g1pzkib")))

(define-public crate-yomo-0.2.0 (c (n "yomo") (v "0.2.0") (d (list (d (n "yomo_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1ay4lmgj67azb663n9wbrw45jhn5x0frc4pcibyhpimb6k76dmdy")))

(define-public crate-yomo-0.3.0 (c (n "yomo") (v "0.3.0") (d (list (d (n "yomo_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0z737gxyrnbcl651mq9ggz6n95a9yax8ggkpxbxhzv4sy6z2ynhs")))

