(define-module (crates-io yo uh youhtmod) #:use-module (crates-io))

(define-public crate-youhtmod-0.1.0 (c (n "youhtmod") (v "0.1.0") (h "06mvql0zqnld77g1jg35h411kfb28d5b8m5k0ihq3zmhlmifx56d")))

(define-public crate-youhtmod-0.1.1 (c (n "youhtmod") (v "0.1.1") (h "1av2i5xqcc8k5gm634fdl3ha2pxr199cpn6qd64df7fmpqvj9d83")))

(define-public crate-youhtmod-0.1.2 (c (n "youhtmod") (v "0.1.2") (h "040apq6x0p5q8l3hhp0rzxm9dwypcygnkycrxhfmam01xid7sr91")))

(define-public crate-youhtmod-0.1.3 (c (n "youhtmod") (v "0.1.3") (h "0g248qw3ydyya2bsmp07l43rhgbqb8y1c659dn5pg6anb7y8nnj5")))

