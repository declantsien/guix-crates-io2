(define-module (crates-io yo ga yoga-sys) #:use-module (crates-io))

(define-public crate-yoga-sys-0.1.0 (c (n "yoga-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "0w45aslsqkrfy60gyng41kn9ypi43mpcrwiiyfsdqlbzrn4ykkmi")))

(define-public crate-yoga-sys-0.2.0 (c (n "yoga-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "09yzijmbdppjsgsx19gcypm11pk4haivfxqqdj8lc2kbwfhykpyv")))

(define-public crate-yoga-sys-0.2.1 (c (n "yoga-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1y5pip35fbfl9h5z5frir1pzc74wbxabsj6xpklxhih3gdy4vkfv")))

(define-public crate-yoga-sys-0.2.2 (c (n "yoga-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1b77i4d20psckwfjljf910m4fcmx8xnqpy839qil0ry2mvrqgmmh")))

(define-public crate-yoga-sys-0.2.3 (c (n "yoga-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0r4si97h8mzmj853j7yxczi1krlq341w1fxd2rxfa68bjaapksb7")))

