(define-module (crates-io yo ga yoga) #:use-module (crates-io))

(define-public crate-yoga-0.1.0 (c (n "yoga") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)))) (h "0bzmv3l1f1siwzzh1yk0diipiccdpxcphrmlaxs9q6zmcl4kscwq")))

(define-public crate-yoga-0.1.1 (c (n "yoga") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.23.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)))) (h "1l0r8dcp1jjlqw9dwza2g198rkfrkm49vid5jxdsdnzgr65428ql")))

(define-public crate-yoga-0.2.0 (c (n "yoga") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "1lnw3yb91z7gya5hppbk1kv0bk4v3sk6adxh1pzbfnin23065yqg") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

(define-public crate-yoga-0.3.0 (c (n "yoga") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "0ykygrmgx9a0naz047i2jqjfawgvww59bbjwwyrr99p2vwp6046q") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

(define-public crate-yoga-0.3.1 (c (n "yoga") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "1bfsxxq5npipl212nmxkrwrl054ph9r6svh85jq1d1wi92dyirxd") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

(define-public crate-yoga-0.4.0 (c (n "yoga") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (o #t) (d #t) (k 0)))) (h "1cmlznx1qgmlh9gs27h1nyqj3l2nk7rkginba6x08r5h960q2ibj") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

