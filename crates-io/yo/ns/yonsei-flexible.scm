(define-module (crates-io yo ns yonsei-flexible) #:use-module (crates-io))

(define-public crate-yonsei-flexible-0.1.0 (c (n "yonsei-flexible") (v "0.1.0") (h "0d0adq19dfm2agkrqqfdw26s68djwmxbpzn4dx3n2c9nm1hbgypv")))

(define-public crate-yonsei-flexible-0.1.1 (c (n "yonsei-flexible") (v "0.1.1") (h "1mb4z7qb59bvb13w1bbpq9vbd8z473l11czfw3i4a0aycq2yh9sr")))

(define-public crate-yonsei-flexible-0.2.0 (c (n "yonsei-flexible") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)))) (h "1m2043jb7sbpgk1irk9gw1mkgljj5x3s14n7jxg15s13cyqf3ny8")))

(define-public crate-yonsei-flexible-0.2.1 (c (n "yonsei-flexible") (v "0.2.1") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "dont_disappear") (r "^3.0.1") (d #t) (k 0)))) (h "0fmk0h3qblxrpxwyww2x3c3ss9014116sn79yvaflsz8link2l0q")))

