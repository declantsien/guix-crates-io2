(define-module (crates-io yo ux youxi-codegen) #:use-module (crates-io))

(define-public crate-youxi-codegen-0.1.0 (c (n "youxi-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1khrvw0f6ygik6yh3i039f82z1ayryfh1x4vd1cyi7vpi81m07ds")))

