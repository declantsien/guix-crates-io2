(define-module (crates-io yo ux youxi) #:use-module (crates-io))

(define-public crate-youxi-0.1.0 (c (n "youxi") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "mini_gl_fb") (r "^0.6.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.3") (d #t) (k 0)) (d (n "youxi-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "1p6b3h75zgbzdrfnlsjhbp31g9yh61gq130shsika0b4sbjd5px9")))

