(define-module (crates-io yo ct yocto_client) #:use-module (crates-io))

(define-public crate-yocto_client-0.0.1 (c (n "yocto_client") (v "0.0.1") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 2)) (d (n "yocto") (r "^0.1.1") (d #t) (k 2)))) (h "0sph7r8sbadcqlpgs83q68cys77i6nmswxvw8vwbax2bpz8hs5gv")))

(define-public crate-yocto_client-0.0.2 (c (n "yocto_client") (v "0.0.2") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 2)) (d (n "yocto") (r "^0.2.0") (d #t) (k 2)))) (h "0l8r5lwp29friwg5z7wj9j7lsm13bp2lp8af3jv5i4pz4h5y9zm9")))

(define-public crate-yocto_client-0.1.0 (c (n "yocto_client") (v "0.1.0") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 2)) (d (n "yocto") (r "^0.3.0") (d #t) (k 2)))) (h "1107jcbqhalxjz18jh3vig64zc8zy5hnp4252mzbhrzq2fjg2184")))

