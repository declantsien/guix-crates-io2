(define-module (crates-io yo ct yocto) #:use-module (crates-io))

(define-public crate-yocto-0.1.0 (c (n "yocto") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1iclm93ps8nvz9acwipbqydcwkl5xa1gvv0qji6ll4rd0c8pv8ha")))

(define-public crate-yocto-0.1.1 (c (n "yocto") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "148qzd5y21wq3dqnchdr8a9f9yq62d5a2y97w971kyyqxlm6r0jp")))

(define-public crate-yocto-0.1.2 (c (n "yocto") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1bbgbb1fpfs27vkrrkv8f0ynw96bcdggig92nl2jxd8j6c5iqxfx")))

(define-public crate-yocto-0.1.3 (c (n "yocto") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "00d3bzxv3zmnzq5mpcwg7z25d9b82r2a4wf97vh2vlm3ajnc1p6f")))

(define-public crate-yocto-0.1.4 (c (n "yocto") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0mjz62zws3z2g48avj4j0qc1lrxvyjdiyn9115vfcxwa5569fyvy")))

(define-public crate-yocto-0.1.5 (c (n "yocto") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1l2yf8vhbmicnz51s9hvwv82a1rlz0s88bdsi944b39qqc0l9sji")))

(define-public crate-yocto-0.2.0 (c (n "yocto") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0nkm26ppsbi0wh6qnas1x9w13si06d35kw0ybh0mjjzrfqwvb5qk")))

(define-public crate-yocto-0.3.0 (c (n "yocto") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.10.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1jis47vgm52kb5wlvpsn12gqicha1g52jr1hmbp2vpbadbahh6q4")))

