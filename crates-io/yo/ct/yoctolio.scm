(define-module (crates-io yo ct yoctolio) #:use-module (crates-io))

(define-public crate-yoctolio-0.1.0 (c (n "yoctolio") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m5140qybjk1wjasc3k2wc84bzhy7baw07blakh2ahw353npxhmf")))

(define-public crate-yoctolio-0.1.1 (c (n "yoctolio") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15lq7jimr7r9q5lkvvw0b8rwkjrl0h23785686wvljysw909i0jr")))

(define-public crate-yoctolio-0.1.2 (c (n "yoctolio") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0japqk7pdjb7rqijng39zpx9p5ga44arssfqrynhp54c2wxrrp4w")))

(define-public crate-yoctolio-0.1.3 (c (n "yoctolio") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "085yh6p5vjrgdn5pp5lcs8vn39izs0v0l2cwdc2z1xnbbnkz5dj4")))

(define-public crate-yoctolio-0.2.0 (c (n "yoctolio") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hscx3y5yjd03k3i11kvbb1151i7r73b03fg7669yphyljyq0j9y")))

(define-public crate-yoctolio-0.3.0 (c (n "yoctolio") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04rz29d7ijgs6fh6gn0755rnw507pib2vg16ijk4v02z6y18b56d")))

(define-public crate-yoctolio-0.4.0 (c (n "yoctolio") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17dz499yk282ighvwmh5kc3hg3sr7y7ggr53pykmbd4bf8pfdw6l")))

