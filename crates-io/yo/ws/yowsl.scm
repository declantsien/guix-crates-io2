(define-module (crates-io yo ws yowsl) #:use-module (crates-io))

(define-public crate-yowsl-0.1.0 (c (n "yowsl") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.4.2") (d #t) (k 0)))) (h "1sxz8q15c0sk4zz21d7s1qjjzqbz8j29h451lk4mvpyp8r5bcj49")))

(define-public crate-yowsl-0.1.1 (c (n "yowsl") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.4.2") (d #t) (k 0)))) (h "1z13mz4yxdai2l1m9hl8qw8jiswdkwqirldbi9pxrg4japg73gcp")))

(define-public crate-yowsl-0.1.2 (c (n "yowsl") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.4.2") (d #t) (k 0)))) (h "07bv4h27yk6mbpprrr91629fdl1cv58lsgsnl6ihprw6dxhbd74g")))

(define-public crate-yowsl-0.1.3 (c (n "yowsl") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^2.28.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.4.2") (d #t) (k 0)))) (h "0iy6w2g8mxhf7zriilqyinb15cqzk5qz69a8g692vbivfzz84z05")))

