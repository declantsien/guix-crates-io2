(define-module (crates-io yo js yojson-rs) #:use-module (crates-io))

(define-public crate-yojson-rs-0.1.0 (c (n "yojson-rs") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)))) (h "1qd6bjdbrnykn5162wh779hygmfcaj1jk11sqlyvzgpvdvaasw5p") (y #t)))

(define-public crate-yojson-rs-0.1.1 (c (n "yojson-rs") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)))) (h "0x11c01pndi03x3h7ljjp9vxzhknsrqkxlyl00im84w6q9axx41i")))

(define-public crate-yojson-rs-0.1.2 (c (n "yojson-rs") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)))) (h "1xxc4csgw0yz287ah68dxy3hd7a3ybij2al7cszb8pvcmsg0hn43")))

