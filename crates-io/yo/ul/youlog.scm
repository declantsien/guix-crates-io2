(define-module (crates-io yo ul youlog) #:use-module (crates-io))

(define-public crate-youlog-0.1.0 (c (n "youlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)))) (h "1hnqcvci1prc281511sij28kj9w7hdwncfwwqbk61nwxyi2j7249") (f (quote (("default"))))))

(define-public crate-youlog-0.1.1 (c (n "youlog") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)))) (h "1y48dip3l4vl1rxl977q7q4cbib4fz00lf6271l5vc6jrm608vxg") (f (quote (("default"))))))

(define-public crate-youlog-0.1.2 (c (n "youlog") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)))) (h "0mzg4ajlwfs3nka6ixng96h0casmvy6204q5bi0bd2qd49pwya5j") (f (quote (("default"))))))

