(define-module (crates-io yo in yoin-ipadic) #:use-module (crates-io))

(define-public crate-yoin-ipadic-0.0.1 (c (n "yoin-ipadic") (v "0.0.1") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "yoin-core") (r "^0.0.1") (d #t) (k 0)))) (h "0nmkg6ycmpj7b8nz2q1s3fyvqhdv7nl92jfzipgsnw9pzk74g8j1")))

