(define-module (crates-io yo re yorex_data) #:use-module (crates-io))

(define-public crate-yorex_data-0.1.0 (c (n "yorex_data") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1220h9if8nz8qh89my2m97g1a3vj8vsz0wl26dh6mn7asfm8vhpw")))

(define-public crate-yorex_data-0.1.1 (c (n "yorex_data") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "08m7is79wcbfqj52yqw5gchrr11qhq88xghahg2pkya94bbx8j9v")))

