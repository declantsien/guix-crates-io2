(define-module (crates-io yo re yore) #:use-module (crates-io))

(define-public crate-yore-0.1.0 (c (n "yore") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0f0x9k5xc5l34clf2psf0brjsdg8cbzi3vrk7b6kfgv331ja16b9")))

(define-public crate-yore-0.2.0 (c (n "yore") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0620xy00la178ci96xdgnm1h7p482l4q1y0shrbm633vypf54sn2")))

(define-public crate-yore-0.3.0 (c (n "yore") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1ydlzjkgwk6d5pq6dbnmwbqh1lr4hbnkr06anr09b30qg5hxhd34")))

(define-public crate-yore-0.3.1 (c (n "yore") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "05d6mnvf7r129c436y9qji89qycq7l5hcfi2s7di2p2zkg7aqv4i")))

(define-public crate-yore-0.3.2 (c (n "yore") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0vrybz491q4fbn5q5kmmwzx00qjyawg9x7lf321r3q3l2i2xz1db")))

(define-public crate-yore-0.3.3 (c (n "yore") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0qbyvfqqhnimi9dkfl7m607jz4mvbibdhm2x572nki1vflw8rmc1")))

(define-public crate-yore-1.0.0 (c (n "yore") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0fzk6iy34y4v58s5y6pn0v2kfhi4l5f3mnpz3vrni3gx24hz14aq")))

(define-public crate-yore-1.0.1 (c (n "yore") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1dxfs0bndj967b99sd79kzac36pyckwbcwrkpqgk4mfhb3qjk511")))

(define-public crate-yore-1.0.2 (c (n "yore") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "09caq5ldsrfp2dil4h59dvj8y17axmldng3a19c7y0safx9rh5pz")))

(define-public crate-yore-1.1.0 (c (n "yore") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "02x2m28v4dw0csvp5d3v176vvjl9ii15pd58qnjqbakz3g7cfn4g")))

