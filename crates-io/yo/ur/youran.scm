(define-module (crates-io yo ur youran) #:use-module (crates-io))

(define-public crate-youran-0.1.0 (c (n "youran") (v "0.1.0") (h "0h25r8nr4bdd4sfh5kf11nq0xpwj6aalx2xbrlkzvvki82d803hq")))

(define-public crate-youran-0.2.0 (c (n "youran") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "17k0ds6wximr4c4qvskd19iy9v6fq7zci565vz6w901s5hg6gjm3")))

(define-public crate-youran-0.2.1 (c (n "youran") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0h7jjdy54mb46q8dhykg2q072cvpzkhl3cvlsjlhnizs1n2mxz3m")))

(define-public crate-youran-0.2.2 (c (n "youran") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1zsg10vjbfwqkrn9vxhmwxy5gsw3mvmfnb7l0vn6bwxnr1n21vgi")))

(define-public crate-youran-0.2.3 (c (n "youran") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0ld5x0s1068cgmb2rvzqz6fmp6djnf9c8d3bax5cmxq65k4slwwz")))

(define-public crate-youran-0.2.4 (c (n "youran") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "sqlite" "migrate"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0ibvrp8mm3pb6ymrxg5mqfblyjv7fniv52gwg0ph9kvsska4z23p")))

