(define-module (crates-io yo sy yosys-netlist-json) #:use-module (crates-io))

(define-public crate-yosys-netlist-json-0.0.1 (c (n "yosys-netlist-json") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cag7pj35kwixy60wmd2k5bnr2q9jg1b28z5z4nwxcqy7kvmg11k")))

(define-public crate-yosys-netlist-json-0.0.2 (c (n "yosys-netlist-json") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1g2daknl70q8m50phz084cfvc1z8n8kj7iqmqvy42gdyxlidc5f5")))

(define-public crate-yosys-netlist-json-0.0.3 (c (n "yosys-netlist-json") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)))) (h "0qg82bww8cvbwlmrnj0diyvvsf1gz1k9fkg5h0gb8ax80z9i48qh")))

(define-public crate-yosys-netlist-json-0.1.0 (c (n "yosys-netlist-json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (o #t) (d #t) (k 0)))) (h "0gyv79zabdyjlcgx80dac762zylnayspb4ycng9nl9y1zcshwpww") (f (quote (("default" "slog"))))))

