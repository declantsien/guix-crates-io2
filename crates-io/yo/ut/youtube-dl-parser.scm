(define-module (crates-io yo ut youtube-dl-parser) #:use-module (crates-io))

(define-public crate-youtube-dl-parser-0.1.0 (c (n "youtube-dl-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "0dp15sf1v8jlxlv5pq6nznkmbw66434j2h7k5prvk39rxryria8g")))

(define-public crate-youtube-dl-parser-0.1.1 (c (n "youtube-dl-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "1kwryrlcf0izd6z9bg3j5p80y2szi2gd3sifjrl0c5mf0v6dcwy8")))

(define-public crate-youtube-dl-parser-0.2.0 (c (n "youtube-dl-parser") (v "0.2.0") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "1h1dd4bhf27ya71hbvnmdgims4n3ryb9ckdvm6br4wi88xdzz9pj")))

