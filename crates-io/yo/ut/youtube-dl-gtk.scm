(define-module (crates-io yo ut youtube-dl-gtk) #:use-module (crates-io))

(define-public crate-youtube-dl-gtk-0.1.0 (c (n "youtube-dl-gtk") (v "0.1.0") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "040m2jgha4h0v1cq6qbwh9kjv8d44gaz3w79ksxkavn12zlbd91z") (y #t)))

(define-public crate-youtube-dl-gtk-0.1.1 (c (n "youtube-dl-gtk") (v "0.1.1") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "11sfl2dybmrxm7pv9r93m4qlw8yddrh2dbcly9apscgkvmlh811x") (y #t)))

(define-public crate-youtube-dl-gtk-0.1.2 (c (n "youtube-dl-gtk") (v "0.1.2") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "08lliazmsl0sjs8aw1nmqp55z403mmr45bz7zjgd9cdgwnw8bm3z") (y #t)))

(define-public crate-youtube-dl-gtk-0.1.3 (c (n "youtube-dl-gtk") (v "0.1.3") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "0la32ranv9p3xwwyi0sdi4k4xsn9rarxnsclbaq6gh4wyxd6b9w9") (y #t)))

