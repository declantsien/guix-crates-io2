(define-module (crates-io yo ut youtube_downloader_cli) #:use-module (crates-io))

(define-public crate-youtube_downloader_cli-0.1.0 (c (n "youtube_downloader_cli") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "15mm56xb20nd504zpxhmnhyi8vpf6mmwaldx6frnj93s41qys2bm")))

(define-public crate-youtube_downloader_cli-0.1.1 (c (n "youtube_downloader_cli") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "1hm825k0zcla235g54388rzf8892xrmi58zxsm1q59d4j8899qyf")))

(define-public crate-youtube_downloader_cli-0.1.2 (c (n "youtube_downloader_cli") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "04rgavdj7xyljp4rrsfm3z98xv0y9ws8gi7djw51rdj9gakpzgdb")))

(define-public crate-youtube_downloader_cli-0.1.3 (c (n "youtube_downloader_cli") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "1jyjxbxn0vcz5ax589069zc8kqq07wf6s7rl380qzjqmi1j5pqj6")))

(define-public crate-youtube_downloader_cli-0.1.4 (c (n "youtube_downloader_cli") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "0rv8d77l0iv8vm2fsn8cp00s6n04i13h91s0z3s06z3gmp6ci2ma") (y #t)))

(define-public crate-youtube_downloader_cli-1.0.0 (c (n "youtube_downloader_cli") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "1jp83rdga78avvr3hysn1l552ii46m991dqm1cmmygyfm5vm0nzl")))

(define-public crate-youtube_downloader_cli-1.1.0 (c (n "youtube_downloader_cli") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "rustube") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "ytextract") (r "^0.11.0") (d #t) (k 0)))) (h "1j1cmv73g0ai23xr0h8p6p9vdks51c0fn2bcg4lm85ajpaqalar1")))

