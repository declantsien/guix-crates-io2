(define-module (crates-io yo ut youtube-dl-repl) #:use-module (crates-io))

(define-public crate-youtube-dl-repl-0.0.0-alpha.1 (c (n "youtube-dl-repl") (v "0.0.0-alpha.1") (h "1pb5131ra04178l21csyp7jr9v5whrb2v5mvji1znjyci13dwq5b") (y #t)))

(define-public crate-youtube-dl-repl-1.0.0-alpha.1 (c (n "youtube-dl-repl") (v "1.0.0-alpha.1") (h "0v49c6sb23rpvh7h3s0ik9lalq65ham2w850ib07pbd873q4d4yi") (y #t)))

(define-public crate-youtube-dl-repl-1.0.0 (c (n "youtube-dl-repl") (v "1.0.0") (h "1am7cpfzcxbdcvqiaajyzsf7q8kilz5yfanxbbg9vd9drx9mgwin")))

(define-public crate-youtube-dl-repl-1.0.1 (c (n "youtube-dl-repl") (v "1.0.1") (h "09fy6wkvl51a85lpzszhs89iniq3gs83vvl9859g81w27z3v844q")))

