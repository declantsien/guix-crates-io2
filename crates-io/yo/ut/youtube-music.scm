(define-module (crates-io yo ut youtube-music) #:use-module (crates-io))

(define-public crate-youtube-music-1.0.0 (c (n "youtube-music") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "04l9zzxchn2ymc7w5px7b7zxkb4k4riw48flb4ig685pfg73xwnz")))

(define-public crate-youtube-music-1.0.1 (c (n "youtube-music") (v "1.0.1") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqfm6gynwnq003p4m00i5b877c3r3hjprq8r583x2nixc7nyikf")))

(define-public crate-youtube-music-1.0.2 (c (n "youtube-music") (v "1.0.2") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i3gdc435gaa7i0l5md7fi49jmjk7hh9ghwsrzpszhj941nk4ppf")))

(define-public crate-youtube-music-1.1.0 (c (n "youtube-music") (v "1.1.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0l4s40vhgg009qjy2986gqmw03q6ckxj5ybm1p6scll7v6337lfg")))

(define-public crate-youtube-music-1.1.1 (c (n "youtube-music") (v "1.1.1") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmravm7lpgk77l0zj31x0nxbf2jzqb4jgnkm8m7dbq88m9j22dj")))

(define-public crate-youtube-music-1.2.0 (c (n "youtube-music") (v "1.2.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lx1dig55ks4ffx73hsdmb65041gn5rsp2lm8fr937i6c3dvgdxg")))

(define-public crate-youtube-music-1.3.0 (c (n "youtube-music") (v "1.3.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vymmd4lcviinfsrbigwp1l1ahqpmp9n88pwxysn38gn0jcmx5pc")))

(define-public crate-youtube-music-1.4.0 (c (n "youtube-music") (v "1.4.0") (d (list (d (n "lazy-regex") (r "^2.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hgjc4z7dq1q25ivip0pgykhynjqjz7d7zj9a3kj5skdwfdgn9rn")))

(define-public crate-youtube-music-1.4.1 (c (n "youtube-music") (v "1.4.1") (d (list (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgbx1zqrsrvirx3n7wvnmil8zyln3yvkcg86ci2qdawmd0xp3n3")))

(define-public crate-youtube-music-2.0.0-alpha.1 (c (n "youtube-music") (v "2.0.0-alpha.1") (d (list (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yc1ikd449y11igickamqhzxfs00qnx3p0h5im8a730xjjsz4v03")))

(define-public crate-youtube-music-2.0.0-alpha.2 (c (n "youtube-music") (v "2.0.0-alpha.2") (d (list (d (n "lazy-regex") (r "^2.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14zjlnw6a0kzr3bvv2mxjwij41bkx54s2s4di8rvcls4sshki3d8")))

(define-public crate-youtube-music-2.0.0-alpha.3 (c (n "youtube-music") (v "2.0.0-alpha.3") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dx0d4ybysbc7wvjmmjdys6lhmc6ijvx5f6q9ayzwc29hj9ck3js")))

(define-public crate-youtube-music-2.0.0 (c (n "youtube-music") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r18m8l2a4jd7xf2ikvc9acsz0zq4bmnhwh0yjd64lrk2i8arx2j")))

(define-public crate-youtube-music-2.1.0 (c (n "youtube-music") (v "2.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "102d3549rbwzd0b6qcch6l845hrhj6gn2fsgvj550zjq5x6yvlxk")))

(define-public crate-youtube-music-2.1.1 (c (n "youtube-music") (v "2.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dhk8zxil9p1k3sqg40n5aa9f37z707pf13jiwaydhlhd143h22r")))

(define-public crate-youtube-music-2.2.0 (c (n "youtube-music") (v "2.2.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qnzjx0a470ha47f5siqm54xk1iwhcy3304jdrricf1k25srda66")))

