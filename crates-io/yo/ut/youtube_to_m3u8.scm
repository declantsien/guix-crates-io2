(define-module (crates-io yo ut youtube_to_m3u8) #:use-module (crates-io))

(define-public crate-youtube_to_m3u8-0.1.0 (c (n "youtube_to_m3u8") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1308crjczbq5vbjc78jzm1gigayh161zyszdfydaw040r1b0rgd8")))

(define-public crate-youtube_to_m3u8-0.1.1 (c (n "youtube_to_m3u8") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z7rjvd2ja22nixyd45gqf18irbihcxsx97z5fdb10ykx1y429v9") (f (quote (("tls-vendored" "native-tls/vendored"))))))

(define-public crate-youtube_to_m3u8-0.1.3 (c (n "youtube_to_m3u8") (v "0.1.3") (d (list (d (n "native-tls") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xak48hrwgywhs0bzryrw3k5s468aajyvgwaixcvxyvjf520kfsq") (f (quote (("tls-vendored" "native-tls/vendored"))))))

(define-public crate-youtube_to_m3u8-0.1.4 (c (n "youtube_to_m3u8") (v "0.1.4") (d (list (d (n "native-tls") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zb1q1x9snkivvvqx2p8518cbjd3b1l0f5llxx8gmrmjjscc4md6") (f (quote (("tls-vendored" "native-tls/vendored"))))))

