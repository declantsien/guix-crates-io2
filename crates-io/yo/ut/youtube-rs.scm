(define-module (crates-io yo ut youtube-rs) #:use-module (crates-io))

(define-public crate-youtube-rs-0.1.0 (c (n "youtube-rs") (v "0.1.0") (d (list (d (n "cpython") (r "^0.7.1") (d #t) (k 0)))) (h "123qvcy65kwdwn73nzshsx69vhznh8ijrszyavh5xylazkpi00b2")))

(define-public crate-youtube-rs-0.1.1 (c (n "youtube-rs") (v "0.1.1") (d (list (d (n "cpython") (r "^0.7.1") (d #t) (k 0)))) (h "1vnnkmz1h499c996d0dld4v6s8xh2hr73hs5134fhl6yjnbksdax")))

(define-public crate-youtube-rs-0.1.2 (c (n "youtube-rs") (v "0.1.2") (d (list (d (n "cpython") (r "^0.7.1") (d #t) (k 0)))) (h "0gwm3fdw942fvwxjjl344mkbbl86s3n10sxzqz9rf2fi6w0n9k45")))

(define-public crate-youtube-rs-0.1.3 (c (n "youtube-rs") (v "0.1.3") (d (list (d (n "cpython") (r "^0.7.1") (d #t) (k 0)))) (h "1sx8mzg281h4297swck9cwvwhaj246sjfpgxripgb6505kb8c7pg")))

