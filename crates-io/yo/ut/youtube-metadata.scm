(define-module (crates-io yo ut youtube-metadata) #:use-module (crates-io))

(define-public crate-youtube-metadata-0.1.0 (c (n "youtube-metadata") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0qwci1lph5a5m6d0zxpv08kdwys98zybnjj5qx04n7xvrzdxa5hi")))

(define-public crate-youtube-metadata-0.1.1 (c (n "youtube-metadata") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1lgj7g7k04f0b87z801q6bzim15rmhyjfv6p6s5jxayfq1zyhgqc")))

(define-public crate-youtube-metadata-0.2.0 (c (n "youtube-metadata") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1sz7xg1rvpbqilblikd9f4rksfv25w0y614ka1kral680c4240hb")))

