(define-module (crates-io yo ut youtube3-util) #:use-module (crates-io))

(define-public crate-youtube3-util-0.1.0 (c (n "youtube3-util") (v "0.1.0") (d (list (d (n "google-youtube3") (r "^1.0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "yup-oauth2") (r "^1.0.6") (d #t) (k 0)))) (h "15qn22h10w00khvjc1cs2zdsp30klm0iv3m9aw0g6vnmyzsfmn71")))

