(define-module (crates-io yo ut youtube_chat_macro) #:use-module (crates-io))

(define-public crate-youtube_chat_macro-0.1.0 (c (n "youtube_chat_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "0vi25gidhqhc8rf43v9y5nbnqdj1z02h93r9abh3sn8iv4x05s4m")))

