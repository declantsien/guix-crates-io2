(define-module (crates-io yo ts yotsuba) #:use-module (crates-io))

(define-public crate-yotsuba-0.0.1 (c (n "yotsuba") (v "0.0.1") (h "0q6y2dw1z22frjwcx371iarhgk0wj0lsngwnmjbvwpy300wngsi1") (y #t)))

(define-public crate-yotsuba-0.0.2 (c (n "yotsuba") (v "0.0.2") (h "0wfcy71c38pf76qnpsygy4bhqlr15yqdmf5cb08vnydlrnjav4d5") (y #t)))

