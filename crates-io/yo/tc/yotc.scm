(define-module (crates-io yo tc yotc) #:use-module (crates-io))

(define-public crate-yotc-0.1.0 (c (n "yotc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "16xkb94pdll5xax5fj5ph8zp0g4yqv2wy5m96k2wq2v6r0s6ryfm")))

(define-public crate-yotc-0.1.1 (c (n "yotc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0lgvi09wzmvrb3kf350mxw882dcbk9afps3789hih0l8v7z0jzww")))

(define-public crate-yotc-0.2.0 (c (n "yotc") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "13mclbhkcdq1h0vqvw5cx9x1nhkrpn1dpwwibc0py2bs7gmis19a")))

(define-public crate-yotc-0.2.1 (c (n "yotc") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0fxpm8hwlff12b1rlcb3aai4jcq6slfzmqrx1s27wf0mlzzl50g0")))

(define-public crate-yotc-0.2.2 (c (n "yotc") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1q84agvnn8a0db8cf92n3nrbxfmhn4blj84nhjyi8xmprski27ig")))

(define-public crate-yotc-0.2.3 (c (n "yotc") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1mx5disrls54gncvcdxwn8wdq1rrbpj6m58pcy6pdrwk9619vddb")))

(define-public crate-yotc-0.3.0 (c (n "yotc") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "llvm-sys") (r "^90") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "02mdxbl702zpqil7isc8fhxvq7qx9fsqxn295pbqa41k3vfma2za")))

