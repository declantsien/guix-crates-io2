(define-module (crates-io yo gu yogurt-parse) #:use-module (crates-io))

(define-public crate-yogurt-parse-0.0.0 (c (n "yogurt-parse") (v "0.0.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "06ab6mr5xmadvddkijc1s4q9ywnm58rz7617559qxmwaqkl2z1zy")))

