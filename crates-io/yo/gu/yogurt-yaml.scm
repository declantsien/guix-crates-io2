(define-module (crates-io yo gu yogurt-yaml) #:use-module (crates-io))

(define-public crate-yogurt-yaml-0.0.0 (c (n "yogurt-yaml") (v "0.0.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0zy147r2i7v513kb0k7anpvaxcpv8lg12cc21s3rn9d4iwlqn3w1")))

(define-public crate-yogurt-yaml-0.0.1 (c (n "yogurt-yaml") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1gdy1abrq4x3m2rm5jy48p3402dawlx755vw5zmsabgbmpsbxfvc")))

(define-public crate-yogurt-yaml-0.0.2 (c (n "yogurt-yaml") (v "0.0.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "06kraf2v7sr1ax372k42nmqfh2gazihfym6p4gzfr3axcyqqkvyb")))

(define-public crate-yogurt-yaml-0.0.3 (c (n "yogurt-yaml") (v "0.0.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0qd378gdy8bfm1kldh07f0554z2syaxb822754b9awjj5wabbjfp")))

(define-public crate-yogurt-yaml-0.0.4 (c (n "yogurt-yaml") (v "0.0.4") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1h3hkv73pgdb9008d9ln88pp8agrk4ihppyfxa2dn64510bbizw5")))

(define-public crate-yogurt-yaml-0.1.0 (c (n "yogurt-yaml") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1hdywzzyc4jq9pic1nx0jpbf0qmfspfia1v31r4glskzvc01vklp")))

(define-public crate-yogurt-yaml-0.1.1 (c (n "yogurt-yaml") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0j4v7xpsyapdv5nmbij40q614d8hi2bm2gqxdcq095fl6npr4j8h")))

(define-public crate-yogurt-yaml-0.2.0 (c (n "yogurt-yaml") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "04whfi0gqkrivxld6cayads7nfhqviyih6ckyxp9rayzh0b1ci79")))

