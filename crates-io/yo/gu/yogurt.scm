(define-module (crates-io yo gu yogurt) #:use-module (crates-io))

(define-public crate-yogurt-0.1.0 (c (n "yogurt") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0hgra32334zykx5fkjsa6lpwx9xv8qq5vv95px37b82amy34qalh")))

(define-public crate-yogurt-0.1.1 (c (n "yogurt") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1l9if90qwvg9lqbdn64jm4livck04cs4g96akxnslxh7sank8x8s")))

(define-public crate-yogurt-0.2.0 (c (n "yogurt") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "00p2nlg2ipc94wyhj2akcf8xargnwb5hf3fg0d5ky73izc7k8l96")))

(define-public crate-yogurt-0.3.0 (c (n "yogurt") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1prf3fj20qjh9hvm1q71q98w3aqspwzl0fadsgarba34lid9cj7r")))

(define-public crate-yogurt-0.3.1 (c (n "yogurt") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0z21c70wj4nzlpjc8k5wph6j7z6i5di5w2q4jjk78hp6bbshgjhg")))

