(define-module (crates-io yo gc yogcrypt) #:use-module (crates-io))

(define-public crate-yogcrypt-0.0.0 (c (n "yogcrypt") (v "0.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "16c9ipl61ygpbaxdhddcrpwwbm94rwq17wnywfbxxk3yrsz9sw5q")))

