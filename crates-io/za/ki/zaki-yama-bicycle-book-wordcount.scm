(define-module (crates-io za ki zaki-yama-bicycle-book-wordcount) #:use-module (crates-io))

(define-public crate-zaki-yama-bicycle-book-wordcount-0.1.0 (c (n "zaki-yama-bicycle-book-wordcount") (v "0.1.0") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0sdmmhb6c00x8s76gamrq4ric0lyc3rw87k6r6fx1bra7sqrpx0w")))

(define-public crate-zaki-yama-bicycle-book-wordcount-0.1.1 (c (n "zaki-yama-bicycle-book-wordcount") (v "0.1.1") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1rh2z9mlb9xf8gr30aj68c60hps14ajmxbrsnyqrk58fp4nvnp9s")))

