(define-module (crates-io za im zaim) #:use-module (crates-io))

(define-public crate-zaim-0.1.0 (c (n "zaim") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "reqwest-oauth1") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1svq18145y93mhqkxarb0nny6zr2k7cafqpc82iwc8jm19wdq5rz")))

(define-public crate-zaim-0.2.0 (c (n "zaim") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "reqwest-oauth1") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0505g0shp18ixb0vh1181ijabvxnv49f6aavlqccykdh3crmqlfh")))

