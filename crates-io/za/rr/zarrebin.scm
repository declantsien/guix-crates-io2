(define-module (crates-io za rr zarrebin) #:use-module (crates-io))

(define-public crate-zarrebin-0.1.0 (c (n "zarrebin") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "1jkrkrgbifx06pxbqgs5hz1xxrz5nki86zlvvd2v4r8aghwg27rb")))

(define-public crate-zarrebin-0.1.1 (c (n "zarrebin") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "1r0jjh3nhdq52cjxryxasxinnhfkmcjwv82jiafav1w9xpz00g86")))

