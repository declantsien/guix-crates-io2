(define-module (crates-io za ll zalloc) #:use-module (crates-io))

(define-public crate-zalloc-0.1.0 (c (n "zalloc") (v "0.1.0") (d (list (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1r5lvycx17rl0z0yn4wn0f7np4dz55wwmfg9m4j3qm9zi6if2wv5") (f (quote (("allocator"))))))

