(define-module (crates-io za lg zalgo) #:use-module (crates-io))

(define-public crate-zalgo-0.1.0 (c (n "zalgo") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f051fa68mih577kh6cdzzf4pi8i3xs7b2wsscmwbimphqr93xk5")))

(define-public crate-zalgo-0.1.1 (c (n "zalgo") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10rkdwqr86l90lprwpj5pzsdf13v7nnnha4kd1lkxwd0kxjpjn9x")))

(define-public crate-zalgo-0.2.0 (c (n "zalgo") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1cqawkcpf1dsvy0786jwbz6bs23qa5zmk3dw3x3i6217mrb4awx0")))

