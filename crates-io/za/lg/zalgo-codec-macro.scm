(define-module (crates-io za lg zalgo-codec-macro) #:use-module (crates-io))

(define-public crate-zalgo-codec-macro-0.1.0 (c (n "zalgo-codec-macro") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.2.3") (d #t) (k 0)))) (h "1yc5cga2l0jzqsrvbsrjgrgy1360hp2rc4j0hkbhr8glrbp5hx7b")))

(define-public crate-zalgo-codec-macro-0.1.1 (c (n "zalgo-codec-macro") (v "0.1.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.2.4") (d #t) (k 0)))) (h "1w7p1bfwr9q6hrj38s3vwqd9sr7ncd3vkc5075jhdl4fml513pb4")))

(define-public crate-zalgo-codec-macro-0.1.2 (c (n "zalgo-codec-macro") (v "0.1.2") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.2.6") (d #t) (k 0)))) (h "12mzlmqk1ia6bb7123615z7fw92wyf1f9ki23q5hz6jw5dbkzjas")))

(define-public crate-zalgo-codec-macro-0.1.3 (c (n "zalgo-codec-macro") (v "0.1.3") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.4.0") (d #t) (k 0)))) (h "0mgkic42abn2vgly1h9manm14dkhnx1310g3kfvvbqcd2aszdmdp")))

(define-public crate-zalgo-codec-macro-0.1.4 (c (n "zalgo-codec-macro") (v "0.1.4") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.5.0") (d #t) (k 0)))) (h "1pjrhslrbxlq43f12jnz78as7r4ghbicgg123aq76nzd48lffmbg")))

(define-public crate-zalgo-codec-macro-0.1.5 (c (n "zalgo-codec-macro") (v "0.1.5") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.6.0") (d #t) (k 0)))) (h "1v471xb8n23vr4mibv3b0valn8wjrnymxkpfybk7606h7k1ca6z6")))

(define-public crate-zalgo-codec-macro-0.1.6 (c (n "zalgo-codec-macro") (v "0.1.6") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.7.0") (d #t) (k 0)))) (h "1kmngih7l12x1bggz2ssnbfnw69qr04zgjhhq2zr8wbx7lcp5qkd")))

(define-public crate-zalgo-codec-macro-0.1.7 (c (n "zalgo-codec-macro") (v "0.1.7") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.0") (d #t) (k 0)))) (h "166kgxgxqf7yf1nanxhxq7g544m4aby6ns4vdw10hbd13psh7vv3")))

(define-public crate-zalgo-codec-macro-0.1.8 (c (n "zalgo-codec-macro") (v "0.1.8") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.1") (d #t) (k 0)))) (h "05zs39scm6lh43x8scdrmq0032a7yzjd7brqirwzkh11vhlfacmk")))

(define-public crate-zalgo-codec-macro-0.1.9 (c (n "zalgo-codec-macro") (v "0.1.9") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.2") (d #t) (k 0)))) (h "1lmbqpfvhwf5pm1h8wfqm5mh7cyna5klkcmzycci6ig22dm123g3")))

(define-public crate-zalgo-codec-macro-0.1.10 (c (n "zalgo-codec-macro") (v "0.1.10") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.3") (d #t) (k 0)))) (h "1hid7j6my0mxa833g1d8i8k4s4sh8w76mxfza70skcspf6gppzgd")))

(define-public crate-zalgo-codec-macro-0.1.11 (c (n "zalgo-codec-macro") (v "0.1.11") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.4") (d #t) (k 0)))) (h "06jmv1w91hhcin5zdh17j9w3mfmgxm705yi98bjk8bh0d5pf8m7z")))

(define-public crate-zalgo-codec-macro-0.1.12 (c (n "zalgo-codec-macro") (v "0.1.12") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.5") (d #t) (k 0)))) (h "0j15dfcld5j5r6k9kdhdsib730z2m6br6zcwjlvgdxi02kds2zjy")))

(define-public crate-zalgo-codec-macro-0.1.13 (c (n "zalgo-codec-macro") (v "0.1.13") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.6") (d #t) (k 0)))) (h "1sbywzkg2i1mx1dw45bkzi7mdcjp683qqnffcfh1yy5i5dnsbj96")))

(define-public crate-zalgo-codec-macro-0.1.14 (c (n "zalgo-codec-macro") (v "0.1.14") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.8.7") (d #t) (k 0)))) (h "0d09kqxmp9s95mg1blknffzv8jxs0r3likhsjrwqgb0m8mn6a27v")))

(define-public crate-zalgo-codec-macro-0.1.15 (c (n "zalgo-codec-macro") (v "0.1.15") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.0") (d #t) (k 0)))) (h "1gaz1y4y37pkjsmdkmwm8iqnbd160429x07y1swigwr1rc74by5i")))

(define-public crate-zalgo-codec-macro-0.1.16 (c (n "zalgo-codec-macro") (v "0.1.16") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.1") (d #t) (k 0)))) (h "1ccyw40fhfzbyn6318mj0br7g49l0fy1mi7rdj485igxmxi9w9b1")))

(define-public crate-zalgo-codec-macro-0.1.17 (c (n "zalgo-codec-macro") (v "0.1.17") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.2") (d #t) (k 0)))) (h "1wqfll8y60dwf57nnad21idymrkxk3fmsr8grsmnngn7d2aqqxwp")))

(define-public crate-zalgo-codec-macro-0.1.18 (c (n "zalgo-codec-macro") (v "0.1.18") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.3") (d #t) (k 0)))) (h "1mjgn7nkjw05gm755f1lqzm1nhi8k1pdn8kx6pcckrxsqw6l4fx4")))

(define-public crate-zalgo-codec-macro-0.1.19 (c (n "zalgo-codec-macro") (v "0.1.19") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.4") (d #t) (k 0)))) (h "0jlq9r53rmxf7rblrg1b4yj08kxn2c6gn4y5jvabjcz4kpb9c7g7")))

(define-public crate-zalgo-codec-macro-0.1.20 (c (n "zalgo-codec-macro") (v "0.1.20") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.9.5") (d #t) (k 0)))) (h "1mcsagi5wlsbklspw18szvkkabis3aajpkdzbililwmc2bl6mk82")))

(define-public crate-zalgo-codec-macro-0.1.21 (c (n "zalgo-codec-macro") (v "0.1.21") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.10.0") (d #t) (k 0)))) (h "1x6ajzg91lfpj7rgnzmpw203yhwmvj0r58651bpjv33ad0zl57nh")))

(define-public crate-zalgo-codec-macro-0.1.22 (c (n "zalgo-codec-macro") (v "0.1.22") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.10.1") (d #t) (k 0)))) (h "16c8749c81709pch71l0nrqb8ik2bxkkhasian2xyagdrfhxnjmi")))

(define-public crate-zalgo-codec-macro-0.1.23 (c (n "zalgo-codec-macro") (v "0.1.23") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.10.2") (d #t) (k 0)))) (h "1x0kqr3imgxh0xc8cvv7b7yx2k6y2gwmg17da8145dvyx9sy34zp")))

(define-public crate-zalgo-codec-macro-0.1.24 (c (n "zalgo-codec-macro") (v "0.1.24") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.10.3") (d #t) (k 0)))) (h "0wd9qhx94gk66s149x94za37zn96z1xjd2na9ib2cmn1rxpfnvs1")))

(define-public crate-zalgo-codec-macro-0.1.25 (c (n "zalgo-codec-macro") (v "0.1.25") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zalgo-codec-common") (r "^0.10.4") (d #t) (k 0)))) (h "1nznqliywpf8nsi2n11qnwjk7x175yn9d937mcl21x6qj11gzra5")))

