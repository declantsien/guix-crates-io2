(define-module (crates-io za ch zachs18-testing) #:use-module (crates-io))

(define-public crate-zachs18-testing-0.1.0 (c (n "zachs18-testing") (v "0.1.0") (h "1vfw33hnr9fgi3bmj7rjrx0y05jc4kl1dzim4xprxmkr4x4yxy84")))

(define-public crate-zachs18-testing-0.3.0 (c (n "zachs18-testing") (v "0.3.0") (h "07jyj1ck0fllj4n7cx4inxd7i6n16xxs4bng9s26s0k61gf73n05")))

(define-public crate-zachs18-testing-0.2.0 (c (n "zachs18-testing") (v "0.2.0") (h "1pp48vwnbv8kb9hwm56m389s4pddkrgakzn6623sf8b5v7f6r3p8")))

(define-public crate-zachs18-testing-0.4.0 (c (n "zachs18-testing") (v "0.4.0") (h "0b3alv1rgp7gi9rp4cwqcknjsx71lz5zs2y8zfpi5c7997pjcys2")))

