(define-module (crates-io za ch zachs18-stdx) #:use-module (crates-io))

(define-public crate-zachs18-stdx-0.1.0 (c (n "zachs18-stdx") (v "0.1.0") (h "11n09wn5yr4gbflfi2qggrph2kwm0a6mvhgbs1kx8ibfjxlf4crp") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1.1 (c (n "zachs18-stdx") (v "0.1.1") (h "1x00pc01b07bcwxh2bzpgwagqnw9nddakjqdpk5pnrdwyy7fyik4") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1.2 (c (n "zachs18-stdx") (v "0.1.2") (h "114z2z2nnrmg2ryrh815cpaq10brccyk8jr7mj1w5hasqxmxgv98") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1.3 (c (n "zachs18-stdx") (v "0.1.3") (h "0200v1fz70m1i859bzyhjbvrbnnc1222qpjz53q3fz3c3x1nhbw6") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

