(define-module (crates-io za ch zachsbot) #:use-module (crates-io))

(define-public crate-zachsbot-0.0.0 (c (n "zachsbot") (v "0.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0ryzb0rljz1a9ny35cz6y7dc7fcnmb357hjypxl5mfjq99ymzy1y") (y #t)))

(define-public crate-zachsbot-0.0.0-a (c (n "zachsbot") (v "0.0.0-a") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0r15ahyb711lj1sjrwy8l52f4z5mgbk7wn0732bsmsw9m9ppzwa3")))

(define-public crate-zachsbot-0.0.1 (c (n "zachsbot") (v "0.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0yihix5hsvc51sqbb5qb5p1s091pgpgl95n64a4mdc4yhyv1ipbc")))

(define-public crate-zachsbot-0.0.2 (c (n "zachsbot") (v "0.0.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1mccrjkcx20sgzvp3qz7c8mr635fcdh97df70dxs1sa240pgclg2")))

