(define-module (crates-io za sh zash) #:use-module (crates-io))

(define-public crate-zash-0.1.1 (c (n "zash") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "whoami") (r "^1.1.5") (d #t) (k 0)))) (h "1sz10qmagw3jh9agd52160rwh3zqm80ny28xzm073yz3xylvzr48")))

