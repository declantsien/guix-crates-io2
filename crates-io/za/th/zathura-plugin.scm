(define-module (crates-io za th zathura-plugin) #:use-module (crates-io))

(define-public crate-zathura-plugin-0.1.0 (c (n "zathura-plugin") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1yq8gi3b25vwdzdrb6vza5bfbrzjxprw0slw4kcizpx48g2rz1d1")))

(define-public crate-zathura-plugin-0.2.0 (c (n "zathura-plugin") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0hcwfnygmfsrlndjksasc0jhbvkkr4x1apbk0l4r2cg5a6d8bnh2") (f (quote (("testplugin"))))))

(define-public crate-zathura-plugin-0.2.1 (c (n "zathura-plugin") (v "0.2.1") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.2.0") (d #t) (k 0)))) (h "10r50a8rbfwwns0bnliyhy3v3hg6xayk55rcv0fpv3djr75syhfl") (f (quote (("testplugin"))))))

(define-public crate-zathura-plugin-0.3.0 (c (n "zathura-plugin") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.2.0") (d #t) (k 0)))) (h "071ys5byhvh39vxn0aawin5vfszf7xwlc6xzn0n52c4g2bx52mx7") (f (quote (("testplugin"))))))

(define-public crate-zathura-plugin-0.3.1 (c (n "zathura-plugin") (v "0.3.1") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "pkg-version") (r "^0.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0f51plf2hr179askgsipmd97s331dc9x6nqnqxk0v2s8rd6x76w3") (f (quote (("testplugin"))))))

(define-public crate-zathura-plugin-0.4.0 (c (n "zathura-plugin") (v "0.4.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("v1_14"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "pkg-version") (r "^0.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zathura-plugin-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0v5mm9q09ail0c9lq0klknai94dhvwxzazzzxbf7p9vvkv3w94w9") (f (quote (("testplugin"))))))

