(define-module (crates-io za th zathura-plugin-sys) #:use-module (crates-io))

(define-public crate-zathura-plugin-sys-0.1.0 (c (n "zathura-plugin-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "033bf17azv7i29cp3x637bx5kr29fi9sw4q3s46n4q2p1pamvda9") (l "zathura")))

(define-public crate-zathura-plugin-sys-0.2.0 (c (n "zathura-plugin-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0j5ra0k1isp7a05a7abmcpmipnd0aabl6psib2mmxkhkj4g7n8ll") (l "zathura")))

