(define-module (crates-io za ng zang) #:use-module (crates-io))

(define-public crate-zang-0.1.0 (c (n "zang") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "02765gg7z5a30mq3v957j5zm1w8g592xh4wzjfpgcm12qji77mdy") (y #t)))

(define-public crate-zang-0.1.1 (c (n "zang") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1gh5jr3gysa8nrc0a2n1sd5gip633sdm7kb9d5bszv8ghcilb860")))

