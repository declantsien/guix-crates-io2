(define-module (crates-io za pa zapalloc) #:use-module (crates-io))

(define-public crate-zapalloc-0.0.1 (c (n "zapalloc") (v "0.0.1") (h "1lmi0h52b60sr76i3j62wndqbjfk1i4ny02jcqhwqqbv8llzb66h") (y #t)))

(define-public crate-zapalloc-0.0.2 (c (n "zapalloc") (v "0.0.2") (h "0ijfr2049hn3v11h0v767a4rsl5liyy7ck57rxq66zi46x5g1yrh") (y #t)))

(define-public crate-zapalloc-0.0.3 (c (n "zapalloc") (v "0.0.3") (h "0fj1wp1qp53m8xwg12igd549gpw3gj3pj3f8iidgfvrn8d6807i6") (y #t)))

(define-public crate-zapalloc-0.0.4 (c (n "zapalloc") (v "0.0.4") (h "020w0qfs3g3qc2cmgvlqm64pmwcnyz3qw16mrlnp932y3540xzrb") (y #t)))

