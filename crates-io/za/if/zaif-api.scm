(define-module (crates-io za if zaif-api) #:use-module (crates-io))

(define-public crate-zaif-api-0.1.0 (c (n "zaif-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "1g5sh11k7d0b67lpagcwf6hv5w0h6j0yzqh4qb981ygyf9gj8wip")))

(define-public crate-zaif-api-0.1.1 (c (n "zaif-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "0m5s0x7k6scj9r5ys1bhdalh9hqysz8lyvnfhb5xm09jc23y89s9")))

(define-public crate-zaif-api-0.2.0 (c (n "zaif-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "1sngrcdqfw96qajgd9x3srap2m737z831y18r35yncxf5d8lwkmk")))

(define-public crate-zaif-api-0.3.0 (c (n "zaif-api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0qjin308qv4kr1arsy0kl0npg5cll04icc9k72jnrw53yg8d1r21")))

(define-public crate-zaif-api-0.4.0 (c (n "zaif-api") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "001c4fa6sivicl5dynj2vjlwngf4hj1rq1zsc8b2zhw0xqhgmnfk")))

(define-public crate-zaif-api-0.5.0 (c (n "zaif-api") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0bks1l5w5hzjm09gg5a77zyaxdrxyyw8jw6l1w1n4i6hfhmsk0wd")))

(define-public crate-zaif-api-0.6.0 (c (n "zaif-api") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "1dbdw67z8qad5jk1xb7yk38vb0wxjgw0sri0z9hiyyj5lhqkrwlf")))

