(define-module (crates-io za bb zabbix_passive_checks) #:use-module (crates-io))

(define-public crate-zabbix_passive_checks-0.1.0 (c (n "zabbix_passive_checks") (v "0.1.0") (h "1ywfpfw6m17dv0kwnc31y5446k8km1ipyb9d5yg1zb03zxn1wng3")))

(define-public crate-zabbix_passive_checks-0.1.1 (c (n "zabbix_passive_checks") (v "0.1.1") (h "1xpwviwjwfz8gy8p2wlzyr4s21j5a33djvp0np34ircn5kq7sh1i")))

