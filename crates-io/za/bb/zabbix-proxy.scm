(define-module (crates-io za bb zabbix-proxy) #:use-module (crates-io))

(define-public crate-zabbix-proxy-0.1.0 (c (n "zabbix-proxy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)) (d (n "zabbix") (r "^0.1") (d #t) (k 0)) (d (n "zabbix-utils") (r "^0.1") (d #t) (k 0)))) (h "145yd9mw8yizasymhmv7n8ranqqajnd0wsn8kbfdnbq6sh7g0k16") (y #t)))

