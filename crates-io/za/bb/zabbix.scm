(define-module (crates-io za bb zabbix) #:use-module (crates-io))

(define-public crate-zabbix-0.1.0 (c (n "zabbix") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0qg6gsysml7hjvdvnscz2b6p0k7y9lg19z9s7mj58553277n25gj") (y #t)))

(define-public crate-zabbix-0.1.1 (c (n "zabbix") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0dvwppqgs7g403ss4c7nm7psbjv3awcq7rpzvhh2hx21zlmgl2vx") (y #t)))

(define-public crate-zabbix-0.2.0 (c (n "zabbix") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "humantime") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0vjvyyi3r1v934yc6aldnyb655108l0yxcdlrizgsvhw33apzi52")))

(define-public crate-zabbix-0.3.0 (c (n "zabbix") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "humantime") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19aacvbqjjfmsv9djsydrcwyzp6apsj9fngdibbwvj42bga006lz")))

