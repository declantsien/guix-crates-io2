(define-module (crates-io za bb zabbix-api) #:use-module (crates-io))

(define-public crate-zabbix-api-0.1.0 (c (n "zabbix-api") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "fake") (r "^2.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0g9zzx2l8hf6cjsb61vkn7lia0qhjfy5b2n804gahv0v3psawgfq")))

(define-public crate-zabbix-api-0.2.0 (c (n "zabbix-api") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 2)) (d (n "fake") (r "^2.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1xla659vkjk3f46lv3r0xm76lfs0ja6hscmcw6p040m9ar8xz6jh")))

