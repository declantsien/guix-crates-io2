(define-module (crates-io za p_ zap_api) #:use-module (crates-io))

(define-public crate-zap_api-0.0.1 (c (n "zap_api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ll2d2dldp0aijys98c227sk9nhgn87m35jgpiqcxcb3kyja7dsl")))

(define-public crate-zap_api-0.0.2 (c (n "zap_api") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1knvrs5kyx2q7r2w91xi6m9fimis1xbcl6qmh75jgvbdacsnmsw5")))

