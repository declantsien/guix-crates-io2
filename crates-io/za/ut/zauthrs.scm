(define-module (crates-io za ut zauthrs) #:use-module (crates-io))

(define-public crate-zauthrs-0.1.2 (c (n "zauthrs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "yubico_manager") (r "^0.8.1") (d #t) (k 0)))) (h "0z9s5643zald9nqd0p87jkl3xw8777a3bwqzqmwndbgz99ddayx7") (y #t)))

(define-public crate-zauthrs-0.1.21 (c (n "zauthrs") (v "0.1.21") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "yubico_manager") (r "^0.8.1") (d #t) (k 0)))) (h "0bvhrhr9pb5c0fx2438pm3fr5n07r87fzww2cfnm69if5nhzpb93") (y #t)))

(define-public crate-zauthrs-0.1.22 (c (n "zauthrs") (v "0.1.22") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "yubico_manager") (r "^0.8.1") (d #t) (k 0)))) (h "1s3m09g0nfrr948v951kh4m1q8y5p8jl60c7hqzrly0h2225kkcb") (y #t)))

(define-public crate-zauthrs-0.1.23 (c (n "zauthrs") (v "0.1.23") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "yubico_manager") (r "^0.8.1") (d #t) (k 0)))) (h "1whghnzb5jilbps11gmdbds21rm3ys0819nsfsqkaj1dcnnjbb71") (y #t)))

(define-public crate-zauthrs-0.1.24 (c (n "zauthrs") (v "0.1.24") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "yubico_manager") (r "^0.8.1") (d #t) (k 0)))) (h "162qmb9chjmvm6n3l9r3zxmflfys8lw22bhv0lzh25bx762lxfjr") (y #t)))

