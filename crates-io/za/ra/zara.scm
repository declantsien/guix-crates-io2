(define-module (crates-io za ra zara) #:use-module (crates-io))

(define-public crate-zara-1.0.0 (c (n "zara") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "06lxzmxpzhqrhsm8n94b4wjmcdz4dm1g14yl5aa8vhclycxpqdmy")))

(define-public crate-zara-1.0.1 (c (n "zara") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "1kalmjg1wwak3wn9by5a1vf5hxrhsw3qzg72ksr4zd8pxf2smpl8")))

(define-public crate-zara-1.0.2 (c (n "zara") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "08gi51xwbi2w8g6112rmc35n9pvpwpammbszfcmscsc3m6ay00n1")))

(define-public crate-zara-1.0.3 (c (n "zara") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "12i5scps01fmfqq0b5yrpi6fjb5jiv1ykdkali1p31v7r7skk82d")))

(define-public crate-zara-1.0.4 (c (n "zara") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "08px665dbmy3m3c295h1kvi6pibwnyfv8ysskqz81r2429qm0m60")))

(define-public crate-zara-1.0.5 (c (n "zara") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0b6wi7cdpz69ny6l5169kqbh0qv11gkw9720bygyxwqn9vz8b4id")))

(define-public crate-zara-1.0.6 (c (n "zara") (v "1.0.6") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0qmzr2c9v3rfpi5k0kfqvnfn46a9b8px5ckn8aavdwkcvclhqxyp")))

(define-public crate-zara-1.0.7 (c (n "zara") (v "1.0.7") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1anx2pb3b3fykvqwcrlw4r1z0mamrmc4is5l72fkcax1562w3vqx")))

