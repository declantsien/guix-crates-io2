(define-module (crates-io za rg zargs) #:use-module (crates-io))

(define-public crate-zargs-0.1.0 (c (n "zargs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1shsmp6lsaii8w1m7qb3cbccsv1cc2h4yxqh20nnqrj8hh1rjh0f")))

(define-public crate-zargs-0.1.1 (c (n "zargs") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1r14d4af4jbcdrvgrwqygqvshpmj4l0vjrrmzziqdgs92k7lm51l")))

