(define-module (crates-io za rt zarthus_env_logger) #:use-module (crates-io))

(define-public crate-zarthus_env_logger-0.1.0 (c (n "zarthus_env_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "09ligffn0r27di3j5gyr7caba3fknpcjp3azmvdmi4544mip9kky") (f (quote (("default" "chrono"))))))

(define-public crate-zarthus_env_logger-0.2.0 (c (n "zarthus_env_logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0db2b3c6h8yra471cm59w6da90vjq0q46z42c7gc2kivwzffrlwk") (f (quote (("default" "chrono"))))))

(define-public crate-zarthus_env_logger-0.3.0 (c (n "zarthus_env_logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("macros" "formatting" "parsing"))) (o #t) (d #t) (k 0)))) (h "0is0msar2wq719hi2wljdhvcldbk3iccm53wdsbmm7vsm8nk0dw3") (f (quote (("default" "chrono"))))))

