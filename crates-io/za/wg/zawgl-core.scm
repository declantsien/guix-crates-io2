(define-module (crates-io za wg zawgl-core) #:use-module (crates-io))

(define-public crate-zawgl-core-0.1.2 (c (n "zawgl-core") (v "0.1.2") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0si5wm7ssjba810z8ba1qfwwc642jjkhcn0hjzgw9nlwlg5bwnbk") (y #t)))

(define-public crate-zawgl-core-0.1.3 (c (n "zawgl-core") (v "0.1.3") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0mkjaa576l227hkqa4dk5sabrz54lmk3jdy3118pal8a4zynr45l")))

(define-public crate-zawgl-core-0.1.4 (c (n "zawgl-core") (v "0.1.4") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0g15x6ncnlj26wfkbyz0zsyha5pwwn6l0hxd3f45lingsh8n4nfc")))

(define-public crate-zawgl-core-0.1.5 (c (n "zawgl-core") (v "0.1.5") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1phgf16vzzmjfbd4plwh37hx9idfhkhsx90yslpbhqhw0218hq9r")))

(define-public crate-zawgl-core-0.1.6 (c (n "zawgl-core") (v "0.1.6") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1kwhhx841b6341s6dz4vnlchxx15205khkl1vxvrp0if1z0v2vw4") (y #t)))

(define-public crate-zawgl-core-0.1.7 (c (n "zawgl-core") (v "0.1.7") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1a9x6rxm5pjdrd125szmsk78fxflkw8nzn2dmpxspswx441vi76d") (y #t)))

(define-public crate-zawgl-core-0.1.8 (c (n "zawgl-core") (v "0.1.8") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0fc5hanpzbhiz7yj817n8ijbv2hdrsfdd5zx08jirwqjahxbs2jd")))

(define-public crate-zawgl-core-0.1.9 (c (n "zawgl-core") (v "0.1.9") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "049sh20hk3ijhfsfmyvdpfh4c5hcn5ng70g5zj9i929vqd90qnmv")))

(define-public crate-zawgl-core-0.1.10 (c (n "zawgl-core") (v "0.1.10") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1678krd7c90zwxkdjmga3jq51yzyy30af5z2h30x1a6ifr01i5fp")))

