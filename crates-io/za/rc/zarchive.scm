(define-module (crates-io za rc zarchive) #:use-module (crates-io))

(define-public crate-zarchive-0.1.0 (c (n "zarchive") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.1") (d #t) (k 0)))) (h "0qn95a3hg04qaw02czkgapszmyfk2rs9a8avnvvg21914ba2jxkf")))

(define-public crate-zarchive-0.1.1 (c (n "zarchive") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.1") (d #t) (k 0)))) (h "0mcrqgbj478xy94ic24fanzgflv9xn698s5m5jv32wp0dyj1xzqk")))

(define-public crate-zarchive-0.2.0 (c (n "zarchive") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)))) (h "022x4dlf0m5lsdgz8jhilpy0n4cznza81j62vp4zbdwl6b92xwc5")))

