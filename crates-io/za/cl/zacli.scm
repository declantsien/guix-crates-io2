(define-module (crates-io za cl zacli) #:use-module (crates-io))

(define-public crate-zacli-0.1.0 (c (n "zacli") (v "0.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "zaif-api") (r "^0.5.0") (d #t) (k 0)))) (h "0ksh8hl3dsgsibyg9cb7li5g4djpjpqlp70ksal3qhsf7jn0j33g")))

