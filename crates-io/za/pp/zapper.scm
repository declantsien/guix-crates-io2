(define-module (crates-io za pp zapper) #:use-module (crates-io))

(define-public crate-zapper-0.1.0 (c (n "zapper") (v "0.1.0") (d (list (d (n "zapper_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1hk37v4sygpxrmrpl4vyvhs317bw8xqn3c63jyazcl9m41zkc8l6")))

(define-public crate-zapper-0.1.1 (c (n "zapper") (v "0.1.1") (d (list (d (n "zapper_derive") (r "^0.1.0") (d #t) (k 0)))) (h "07kplink26k4cy1335m6srisibgfmn0gi5xrmikawrz6bb9pil00")))

(define-public crate-zapper-0.1.2 (c (n "zapper") (v "0.1.2") (d (list (d (n "zapper_derive") (r "^0.1.0") (d #t) (k 0)))) (h "00midrgwvp4vknqp3mwpp6sw716wmii1q1dbypwv2qykgpm6qvnl")))

(define-public crate-zapper-0.1.3 (c (n "zapper") (v "0.1.3") (d (list (d (n "zapper_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "15lvqkdd8471g1pzmkffdzivh1y3nlg0mvhicmyymxsdgvh4nzvz") (f (quote (("derive" "zapper_derive") ("default" "derive"))))))

(define-public crate-zapper-0.1.4 (c (n "zapper") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "zapper_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1gmdgy1v5lxgfrv1cj1i2pr18vzrjxrbni79fdivyhnmg9nj77g2") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

(define-public crate-zapper-0.1.5 (c (n "zapper") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "zapper_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0vbjq7fdyw1xsmsd3swbd4g7jxag3fvk2g5f8ggz4h9mjrlz6n4j") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

(define-public crate-zapper-0.1.7 (c (n "zapper") (v "0.1.7") (d (list (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "zapper_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0w5xkiwmcmsrcalngsr586m16lknw7mnm9pyr9qkf4hzy01r30k4") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

(define-public crate-zapper-0.1.8 (c (n "zapper") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "zapper_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "09n3ivhq7j60ay5sda8zi91lvp8ljjclnj6kc429ddzb6hsvqc4r") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

(define-public crate-zapper-0.9.0 (c (n "zapper") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "zapper_derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0xjjw2h0jwa8vp91dksv0xw893z8jqvy30jk7yx2zvl7rxl5mz27") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

(define-public crate-zapper-0.9.1 (c (n "zapper") (v "0.9.1") (d (list (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "handlebars") (r "^0.32.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "zapper_derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1mnmbv3ana7000d5gjdg2b4gq5144ns12jj03cbayj31ikar55bb") (f (quote (("derive" "zapper_derive") ("default" "derive" "rayon"))))))

