(define-module (crates-io za pp zapper_derive) #:use-module (crates-io))

(define-public crate-zapper_derive-0.1.0 (c (n "zapper_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kj7f2df9bncg5z2azlsyzfb8nm76y5ynm342sbpwhz1z7dabjk5")))

(define-public crate-zapper_derive-0.9.0 (c (n "zapper_derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02ncvrj3l08vrxjqixxppn1agx5lmsq2id80zx5dn23ki328n3v9")))

