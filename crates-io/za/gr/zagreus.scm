(define-module (crates-io za gr zagreus) #:use-module (crates-io))

(define-public crate-zagreus-0.1.0 (c (n "zagreus") (v "0.1.0") (h "1gvc3x4aqwzp7lr9gjhb474fh09hx51z402g5vdj9c6ic71li4l8") (f (quote (("nightly_can_backoff"))))))

(define-public crate-zagreus-0.1.1 (c (n "zagreus") (v "0.1.1") (h "1hrsp8q5wma6v0j0lzp25z5bllfrd00j34x8dmh3d5ajm96hsagl") (f (quote (("nightly_can_backoff"))))))

