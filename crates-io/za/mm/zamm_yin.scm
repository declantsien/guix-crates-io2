(define-module (crates-io za mm zamm_yin) #:use-module (crates-io))

(define-public crate-zamm_yin-0.0.1 (c (n "zamm_yin") (v "0.0.1") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "18hx9fh0fzvb51py562gw4kmifw6sjwjh4afg8kcvwymhvkxc8gp") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.2 (c (n "zamm_yin") (v "0.0.2") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "05wlhbalymg830fcqsjbdcfxnb0j4q1zqbrz0w64z8mkg4cin0s3") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.3 (c (n "zamm_yin") (v "0.0.3") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0ynqlhizh50bziyzq3j066r1srzjfmf0n0dr29cx7zii0l61v4l7") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.4 (c (n "zamm_yin") (v "0.0.4") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0aygmwf8dr4sjy9k2w3rlj3p5parzdlyrcqap0v52hq5yv9sfpcg") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.5 (c (n "zamm_yin") (v "0.0.5") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1bji3bn02w9ipinbbgm9m7av68272i0z37qa6r8chfw1z6byw0yb") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.6 (c (n "zamm_yin") (v "0.0.6") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1kq9wjawb5z8jcmc3hkz89gr9aa1xrpq5dgkzf43z7d203iqcxf7") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.7 (c (n "zamm_yin") (v "0.0.7") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1dm7q78b9kfb9rm5vf8fd85fvq99218jqcn8izj736dmh1z7bvzs") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.8 (c (n "zamm_yin") (v "0.0.8") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0bkf78101w8ai8fjzfvbs0gxr8ry3s5x273scnrcnnra9828l5i1") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.9 (c (n "zamm_yin") (v "0.0.9") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "156hwc1axld5kpa9nds9cym4lawd8rp9rxnmlxy8dzjkfyf48i30") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.10 (c (n "zamm_yin") (v "0.0.10") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1nxnl66h9xnvwqg6dcvzxayzcdv23cax43p35pnvbp9a9p4ijd3v") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.11 (c (n "zamm_yin") (v "0.0.11") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0i15gd83b27981i9ajhlvz7ylwa5wkhh8pan053bw436qv7rva6y") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.12 (c (n "zamm_yin") (v "0.0.12") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "03f8zir6bmdqzsisn4kmjw9gnsyqypvpi52gd9f905gw5zzqlcgf") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.13 (c (n "zamm_yin") (v "0.0.13") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "13jidqfp8rcybz4g6718vyzsq69r47i861cfz76gsjgdnhc6kkl7") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.0.14 (c (n "zamm_yin") (v "0.0.14") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "17njvp94ds2yqas9dkjvpgan7g1sj1hb3jy5zl3p8j6sqi03kcx6") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.0 (c (n "zamm_yin") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "16n85l2in3bhd3rmkdpq1xp1qgwhlrwmymqvvbhjzyl6r9rgn9km") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.1 (c (n "zamm_yin") (v "0.1.1") (d (list (d (n "petgraph") (r ">=0.5.1, <0.6.0") (d #t) (k 0)) (d (n "rusted_cypher") (r ">=1.1.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r ">=0.1.2, <0.2.0") (d #t) (k 1)))) (h "17drp8sfx274d0032vzrw73fnwxj2yr94zy70506g239f9nvyi8b") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.2 (c (n "zamm_yin") (v "0.1.2") (d (list (d (n "petgraph") (r ">=0.5.1, <0.6.0") (d #t) (k 0)) (d (n "rusted_cypher") (r ">=1.1.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r ">=0.1.2, <0.2.0") (d #t) (k 1)))) (h "1sw69ix03wmbqr9vdqca8pmiivlxy6cjryrqxq8bvf5lhn5gs5f5") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.3 (c (n "zamm_yin") (v "0.1.3") (d (list (d (n "petgraph") (r ">=0.5.1, <0.6.0") (d #t) (k 0)) (d (n "rusted_cypher") (r ">=1.1.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r ">=0.1.2, <0.2.0") (d #t) (k 1)))) (h "1cjx5fx0q0x90z0c83lacqa86slzj0yj3zvq2pg07chpcjd5z2zv") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.4 (c (n "zamm_yin") (v "0.1.4") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r "^0.1.2") (d #t) (k 1)))) (h "1lca92jd7a9y1xqdh3yx71di1bbzfd579rrbajkgh618hdvji6kd") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.5 (c (n "zamm_yin") (v "0.1.5") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r "^0.1.2") (d #t) (k 1)))) (h "1gpwpvnyn429y8gq0g8wp02vvf1cs9yq5118s948bdhl74yh3c9c") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.1.6 (c (n "zamm_yin") (v "0.1.6") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r "^0.1.2") (d #t) (k 1)))) (h "0yd54a5p56zgm13vm9dyfqnj3vbn0jdfjm58c5hh9365xll75a6l") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.2.0 (c (n "zamm_yin") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r "^0.1.6") (d #t) (k 1)))) (h "0g2n0xw0ylyh8vky96rd9h815cx1zy5h13j2adv4hzhpplhvwy6k") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

(define-public crate-zamm_yin-0.2.1 (c (n "zamm_yin") (v "0.2.1") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "zamm") (r "^0.1.6") (d #t) (k 1)))) (h "1vvy5k8k3i545xram6lbaw9q768kxzlgbyp5gk6qyb195nfcp71f") (f (quote (("default" "cypher") ("cypher" "rusted_cypher"))))))

