(define-module (crates-io za pl zaplib_glx_sys) #:use-module (crates-io))

(define-public crate-zaplib_glx_sys-0.0.1 (c (n "zaplib_glx_sys") (v "0.0.1") (h "1hp08xv2glw2xsarzb7psan7sfah9m7sd1dnrzmaxd80w24vrg2c")))

(define-public crate-zaplib_glx_sys-0.0.3 (c (n "zaplib_glx_sys") (v "0.0.3") (h "07w7kmrdd5d67z31hrpj84djhzqni5wpr91916vx0h0p0a2fx7ma")))

