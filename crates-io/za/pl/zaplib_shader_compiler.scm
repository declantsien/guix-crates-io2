(define-module (crates-io za pl zaplib_shader_compiler) #:use-module (crates-io))

(define-public crate-zaplib_shader_compiler-0.0.1 (c (n "zaplib_shader_compiler") (v "0.0.1") (h "1j7m6vz4rrkl08yqldkpjmpwjl2jf32i6b64jki5gvv8y59jkmkg")))

(define-public crate-zaplib_shader_compiler-0.0.3 (c (n "zaplib_shader_compiler") (v "0.0.3") (h "1bcrh20w5ywx0fibcij77l44v3p41lzdsgrx2x0yz4ks8jbab4b6")))

