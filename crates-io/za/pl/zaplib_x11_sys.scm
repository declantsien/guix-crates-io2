(define-module (crates-io za pl zaplib_x11_sys) #:use-module (crates-io))

(define-public crate-zaplib_x11_sys-0.0.1 (c (n "zaplib_x11_sys") (v "0.0.1") (h "1a504795fflh98c3qlh731szqqjmgbigfq7rp7yscvgf3lqj85j1")))

(define-public crate-zaplib_x11_sys-0.0.3 (c (n "zaplib_x11_sys") (v "0.0.3") (h "0ch0phs2xs04r6qkrrabnf54p8f08qdml6v2zk32qwf38kfsp3hr")))

