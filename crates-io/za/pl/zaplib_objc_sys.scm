(define-module (crates-io za pl zaplib_objc_sys) #:use-module (crates-io))

(define-public crate-zaplib_objc_sys-0.0.1 (c (n "zaplib_objc_sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0f472jnh8mc73x1f00slxcv6javilk6piqyhaaz0q84i62yrng72")))

(define-public crate-zaplib_objc_sys-0.0.3 (c (n "zaplib_objc_sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "14xqm9ijsxlj1gmvz20p02qq7lmmc3jj0qhn0l1s63mqrkn10qvq")))

