(define-module (crates-io za pl zaplib_cef_sys) #:use-module (crates-io))

(define-public crate-zaplib_cef_sys-0.0.3 (c (n "zaplib_cef_sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "1rwqnljz4hsfr6yba1ql58xyzbw6lk7i0mirgi7r3nl7wm5db7k1") (f (quote (("debug")))) (l "cef")))

