(define-module (crates-io za pl zaplib_vector) #:use-module (crates-io))

(define-public crate-zaplib_vector-0.0.1 (c (n "zaplib_vector") (v "0.0.1") (h "0lphpc6r3carf8hfgyjh1f9qvwc2ajfy861qqil8d7wh69ycqxlv")))

(define-public crate-zaplib_vector-0.0.3 (c (n "zaplib_vector") (v "0.0.3") (h "0fqmyalw3g31vd63zccg4y1lyx8akly4ir1032f5j9px9dbvdqf5")))

