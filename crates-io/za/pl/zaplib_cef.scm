(define-module (crates-io za pl zaplib_cef) #:use-module (crates-io))

(define-public crate-zaplib_cef-0.0.3 (c (n "zaplib_cef") (v "0.0.3") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "zaplib_cef_sys") (r "^0.0.3") (d #t) (k 0)))) (h "02zmg1ai3znx5rb2mxys3a9qdp3vyw8387q92gz4qa83fpf73d52") (f (quote (("debug" "zaplib_cef_sys/debug"))))))

