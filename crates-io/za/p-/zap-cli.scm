(define-module (crates-io za p- zap-cli) #:use-module (crates-io))

(define-public crate-zap-cli-0.1.0 (c (n "zap-cli") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "gumdrop") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "ssh2") (r "~0.9.0") (d #t) (k 0)) (d (n "zap-model") (r "~0.1") (d #t) (k 0)))) (h "06dcbb67lywi47xgmzl6qby0m11jnzx57z4a9wz5k0vf3irgv7gs")))

