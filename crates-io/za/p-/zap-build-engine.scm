(define-module (crates-io za p- zap-build-engine) #:use-module (crates-io))

(define-public crate-zap-build-engine-0.4.1 (c (n "zap-build-engine") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "zap-buildscript") (r "^0.4.1") (d #t) (k 0)) (d (n "zap-core") (r "^0.4.1") (d #t) (k 0)))) (h "0wl3kjb7d09qg6kbzlpxa40kcwlbc3wcbg9z148xd1zr6gklmz6y")))

(define-public crate-zap-build-engine-0.4.2 (c (n "zap-build-engine") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "zap-buildscript") (r "^0.4.2") (d #t) (k 0)) (d (n "zap-core") (r "^0.4.2") (d #t) (k 0)))) (h "0hxi6hfly6wwb2225mjswsqha5s76hi30qiaz372fx4xa55619zd")))

