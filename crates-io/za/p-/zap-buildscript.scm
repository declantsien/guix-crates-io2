(define-module (crates-io za p- zap-buildscript) #:use-module (crates-io))

(define-public crate-zap-buildscript-0.4.1 (c (n "zap-buildscript") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deno_core") (r "^0.75") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusty_v8") (r "^0.15") (d #t) (k 0)))) (h "04nrl9vdbyss75knmqwk4qg8sp8mjqyhz3lvhnnjpfc03g1896q8")))

(define-public crate-zap-buildscript-0.4.2 (c (n "zap-buildscript") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deno_core") (r "^0.75") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusty_v8") (r "^0.15") (d #t) (k 0)))) (h "1qhn5kk02j5v94mh2vds7vyd4r91l5wgq0nlld42ixapr2cc0hkl")))

