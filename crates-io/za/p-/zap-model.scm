(define-module (crates-io za p- zap-model) #:use-module (crates-io))

(define-public crate-zap-model-0.1.0 (c (n "zap-model") (v "0.1.0") (d (list (d (n "handlebars") (r "~3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "~2.1") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1") (d #t) (k 0)))) (h "1b9gkgw0if8bpnvqzl6vvrjkmkz4bs23w13qlhi92wvk23hh467x")))

