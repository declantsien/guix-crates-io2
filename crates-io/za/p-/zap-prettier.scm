(define-module (crates-io za p- zap-prettier) #:use-module (crates-io))

(define-public crate-zap-prettier-0.1.0 (c (n "zap-prettier") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0db8pgfg2z3409qfwlm1gxbg3fw6cc5w8j52i39xgqqyxq9snwh3")))

(define-public crate-zap-prettier-0.1.1 (c (n "zap-prettier") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1wfp8h5819xqkw6bwsz71spkpc45x7vwpfxw2r9kzxa5m7l5cmc7")))

