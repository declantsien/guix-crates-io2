(define-module (crates-io s_ cu s_curve) #:use-module (crates-io))

(define-public crate-s_curve-0.1.0 (c (n "s_curve") (v "0.1.0") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "1q1ii6ipaid7y528lwplyl6viaqk7lcwg3mixr4fds6d8yvp2bm2")))

(define-public crate-s_curve-0.1.1 (c (n "s_curve") (v "0.1.1") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "01h7gx5rkjfxw9jn49b6b96zhrmh0dvk0a86sjyh4idr3i6ra2mg")))

(define-public crate-s_curve-0.1.2 (c (n "s_curve") (v "0.1.2") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "159123i0zif79m11am8byihqdal4n9pjf59fmklvskvig20j298b")))

(define-public crate-s_curve-0.1.3 (c (n "s_curve") (v "0.1.3") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "1ml3bjsfvw76xdhlm53a4ykyhdjpjgrk6ifpl0277sggxzcw2cn0")))

(define-public crate-s_curve-0.1.4 (c (n "s_curve") (v "0.1.4") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "1w6smadwjiiybfxnrka6f3nbdfspnpff9y6mnsnglfbgzxlmlkld")))

(define-public crate-s_curve-0.1.5 (c (n "s_curve") (v "0.1.5") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "16j25wzzqgm2aqkz1wzydihdlikwp4181hg7khgmm8kwcifahdv1")))

(define-public crate-s_curve-0.1.6 (c (n "s_curve") (v "0.1.6") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "146zd1pvf73klrk7vx1w798d1pw2c61b97mdvywarpnd4c87qaqz")))

(define-public crate-s_curve-0.1.7 (c (n "s_curve") (v "0.1.7") (d (list (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)))) (h "06lx83a89xnpandsm3biq81nzpx9b3c1999vaasa8hrqw3f1wl3d")))

