(define-module (crates-io s_ no s_nor) #:use-module (crates-io))

(define-public crate-s_nor-1.0.0 (c (n "s_nor") (v "1.0.0") (d (list (d (n "clearscreen") (r "^3.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "099c4wwxjassycb58fr8k4r2clmw07jya32gj9gbrf29c9ml1akh")))

(define-public crate-s_nor-1.0.1 (c (n "s_nor") (v "1.0.1") (d (list (d (n "clearscreen") (r "^3.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1n4xbnnnc7v52mlvsp5ga5ssaa21y3jxdrqqljqk5lzmsyl50115")))

