(define-module (crates-io s_ te s_test_fixture) #:use-module (crates-io))

(define-public crate-s_test_fixture-0.1.0 (c (n "s_test_fixture") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1gbhllvbkip4qslkfndia4rsagy1acwyhpckqrabgqld6shn27w9")))

(define-public crate-s_test_fixture-0.1.1 (c (n "s_test_fixture") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qh0a8s2fqsj65xwkkz6y0m608riqcw6imxqk0bdky7jis6pih64")))

(define-public crate-s_test_fixture-0.1.2 (c (n "s_test_fixture") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1590w7kw4rcxghd72ccpp17f9zagw17sz84jywjfqzsap3grg3sx")))

(define-public crate-s_test_fixture-0.1.3 (c (n "s_test_fixture") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1nwywr7jgb7adwrqmzrhvxcg4slsgbj7khxz2ah9f3bd5bl4rzik")))

(define-public crate-s_test_fixture-0.1.4 (c (n "s_test_fixture") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0bsw7aj5fg5y7ldyiay9wfhy4lgnyy0sqjw1jqqwcfxp6ivw90jb")))

(define-public crate-s_test_fixture-0.1.5 (c (n "s_test_fixture") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1lmah2698hj4rr3kja7nvf5jqwa9hh02prh2zam5inkc3vhpq8y4")))

(define-public crate-s_test_fixture-0.1.6 (c (n "s_test_fixture") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "visit" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1xmdd9457rwg0b16hm9h8xirqg6bxycc57wmf390hfwrflm8ifb8")))

(define-public crate-s_test_fixture-0.1.7 (c (n "s_test_fixture") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2l2lg1g2dgrfi28sp9ia6jlz48jp0fm4rb4rgjbirq8kx3c7jr")))

(define-public crate-s_test_fixture-0.1.8 (c (n "s_test_fixture") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1nhwd7nd585mk7j4n9cpqb4911khhzk6fcz6xbgnk1i4xwimvs8g")))

