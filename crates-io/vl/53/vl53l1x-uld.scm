(define-module (crates-io vl #{53}# vl53l1x-uld) #:use-module (crates-io))

(define-public crate-vl53l1x-uld-1.0.0 (c (n "vl53l1x-uld") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "00m8jh6kjldza2lhl34m66p1v84iqhx3gybxm6hs2ddgb6cz81m0")))

(define-public crate-vl53l1x-uld-2.0.0 (c (n "vl53l1x-uld") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.5") (f (quote ("std"))) (d #t) (k 2)) (d (n "vl53l1-reg") (r "^0.1.0") (d #t) (k 0)))) (h "01v7dcqwkcq8f56xq11zzzalhli8vf7whyrbvda4dqmvwjw9hbd9") (f (quote (("i2c-iter") ("default"))))))

(define-public crate-vl53l1x-uld-2.0.1 (c (n "vl53l1x-uld") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.5") (f (quote ("std"))) (d #t) (k 2)) (d (n "vl53l1-reg") (r "^0.1.0") (d #t) (k 0)))) (h "1cf82s1zsc1hhqpabf805r4bf8zanc141xvk6709qzlglgbmsv3a") (f (quote (("i2c-iter") ("default"))))))

