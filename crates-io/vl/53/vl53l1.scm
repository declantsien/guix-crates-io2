(define-module (crates-io vl #{53}# vl53l1) #:use-module (crates-io))

(define-public crate-vl53l1-0.1.0 (c (n "vl53l1") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "vl53l1-reg") (r "^0.1") (d #t) (k 0)))) (h "0s2fi2z33xp3zh5sv5f2swcn2rarf9xhxa84z1a9iqna5zjlsc04")))

(define-public crate-vl53l1-0.1.1 (c (n "vl53l1") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vl53l1-reg") (r "^0.1") (d #t) (k 0)))) (h "0bjfb82gpjr5qdszh7nqjg5sqamjiwzrjdbzvs5d4w1bcyjrdc0l")))

(define-public crate-vl53l1-0.1.2 (c (n "vl53l1") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vl53l1-reg") (r "^0.1") (d #t) (k 0)))) (h "1b7mcfrw087ldi9i3hw8mjggiqk93yc4zjrwkwxvdsg4x5a0pb4w")))

