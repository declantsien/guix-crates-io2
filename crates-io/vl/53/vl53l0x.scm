(define-module (crates-io vl #{53}# vl53l0x) #:use-module (crates-io))

(define-public crate-vl53l0x-0.1.5 (c (n "vl53l0x") (v "0.1.5") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)))) (h "119mjkvv6mqppzg3b7b1avpidb00mgxabsz09yfl15azq0j98zhi")))

(define-public crate-vl53l0x-0.2.0 (c (n "vl53l0x") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)))) (h "04v014x18ypz3jp367lx27ix63n6vhnqivls24vgnd17wj7yh3lk")))

(define-public crate-vl53l0x-0.2.2 (c (n "vl53l0x") (v "0.2.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)))) (h "003d2wv8dqn1ydcdsr7nc7zspdik66v6cy7wzj7y0l66d1i6zads")))

(define-public crate-vl53l0x-0.3.0 (c (n "vl53l0x") (v "0.3.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "hal") (r "^0.2") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1gca7l6vk2wsvh72wds1zjvszb11xzdimbc4a31lsz77r0qx5n39")))

(define-public crate-vl53l0x-0.3.1 (c (n "vl53l0x") (v "0.3.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "hal") (r "^0.2") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "05zyk6gm98wx9yifqr7zbwr73518hivhq61prz0q4jjzrngvl6qw")))

(define-public crate-vl53l0x-0.4.0 (c (n "vl53l0x") (v "0.4.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "hal") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1wn8qxsgz131sw6wpimgrc0xsmfhiqx403q9j3y11rda4q7h03p4") (y #t)))

(define-public crate-vl53l0x-1.0.0 (c (n "vl53l0x") (v "1.0.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "hal") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0lill109103p9s97xnw0pvq4365y1y8wkpkhl9lyz5amcnjdcc2w")))

