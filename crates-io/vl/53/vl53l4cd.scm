(define-module (crates-io vl #{53}# vl53l4cd) #:use-module (crates-io))

(define-public crate-vl53l4cd-0.1.0 (c (n "vl53l4cd") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)))) (h "019i1zni7j60nmr1qhyg8icb5awvmlh5i8jbznqsxkr4rxk2w6ih")))

(define-public crate-vl53l4cd-0.2.0 (c (n "vl53l4cd") (v "0.2.0") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "16vl1znr971nprfqg48qzjvv6skz917ys2avvx5gidyzc5xc84jm")))

(define-public crate-vl53l4cd-0.2.1 (c (n "vl53l4cd") (v "0.2.1") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "02j6xlfn3n1m1mm8v37p3ygfjxhmhx04hr3rg670byqbw530awl7")))

(define-public crate-vl53l4cd-0.2.2 (c (n "vl53l4cd") (v "0.2.2") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "0anqap7xpplckpwxm0an88d73c5yrg04qbp091xa2fmryg6hnd53")))

(define-public crate-vl53l4cd-0.2.3 (c (n "vl53l4cd") (v "0.2.3") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "17ihyyl17js3vd47bmfvsdsbj6z9dy1sdpnavlr7pb5qhm5rif20")))

(define-public crate-vl53l4cd-0.2.4 (c (n "vl53l4cd") (v "0.2.4") (d (list (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "1n0jwhi4y41f8mm2577m2agar9lph8lkrz52bk475ydr7np73rdm")))

(define-public crate-vl53l4cd-0.3.0 (c (n "vl53l4cd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "i2cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1rj3bbz4qbzff7zwaig5w5yds8769jl4mlax2sanlan15sn6aza7") (f (quote (("std" "tracing/std") ("default" "std"))))))

(define-public crate-vl53l4cd-0.4.0 (c (n "vl53l4cd") (v "0.4.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "07v00lsas341n0h5n19bcsi67p1ps7hf19zc86ybwqq9dypclmz4") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("defmt-03" "dep:defmt" "embedded-hal-async/defmt-03"))))))

