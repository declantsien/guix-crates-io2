(define-module (crates-io vl #{53}# vl53l1x) #:use-module (crates-io))

(define-public crate-vl53l1x-1.0.0 (c (n "vl53l1x") (v "1.0.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1xk4fgpm2vfpydc6qrr11r9li7qb4h2mp1njcgjc1zsry5d3z98s")))

(define-public crate-vl53l1x-1.1.0 (c (n "vl53l1x") (v "1.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0wz1j4yph5yyfg4m9ndn8bs1hqpi7vln68z8wyspv83vbjgbdcwy")))

(define-public crate-vl53l1x-1.2.0 (c (n "vl53l1x") (v "1.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1ixxv6vm0dq33gan088d317cg39zrm619500mhbijfxkh89gfrks") (f (quote (("i2c-2v8-mode") ("default"))))))

