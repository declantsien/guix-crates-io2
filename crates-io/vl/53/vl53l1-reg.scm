(define-module (crates-io vl #{53}# vl53l1-reg) #:use-module (crates-io))

(define-public crate-vl53l1-reg-0.1.0 (c (n "vl53l1-reg") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0q5prmnkkql8v693yx0c2i3yr0s0inb8622aabvpifa7h81vnyld")))

(define-public crate-vl53l1-reg-0.1.1 (c (n "vl53l1-reg") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1fd9lhzafsdys4xsn8vppvywni1z8il3aqqwy673sy0whrpcgr15")))

