(define-module (crates-io vl q- vlq-rust) #:use-module (crates-io))

(define-public crate-vlq-rust-0.1.0 (c (n "vlq-rust") (v "0.1.0") (h "02fr6z1fl0l995lyavm0myj87nl5gsnbqd32zzidlkplrqkplnda")))

(define-public crate-vlq-rust-0.1.1 (c (n "vlq-rust") (v "0.1.1") (h "18h46qka7yww7xzzwsq8idm4jfb06h2fpdc3h62d8y4wp5n6ad7l")))

(define-public crate-vlq-rust-0.2.0 (c (n "vlq-rust") (v "0.2.0") (h "10r06dqvn53zmzfk3nj0kmw3d6z2b3jy4g0w6c8r82j0gjbj8bqw")))

(define-public crate-vlq-rust-0.3.0 (c (n "vlq-rust") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1f0pzia8bh9gw1p9ljikwbb2yzavrwda5pdxikwydhxk9lq9zpwj")))

(define-public crate-vlq-rust-0.4.0 (c (n "vlq-rust") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "16vvg08nkh7yil5gpzmc3fc4bh43wdichp2mss5lwslzd3wvk7q8")))

