(define-module (crates-io vl q- vlq-bij) #:use-module (crates-io))

(define-public crate-vlq-bij-0.1.0 (c (n "vlq-bij") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)))) (h "0n56by9270i7szawvl0y3vndj6bzhrnmacp0jhn2xvacvgns2b62")))

(define-public crate-vlq-bij-0.2.0 (c (n "vlq-bij") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1x2rijyj8rswhf0nrxl0q44x08kaxvi75g1zf2kmkd9gvv2r9ffc")))

