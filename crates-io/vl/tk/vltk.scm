(define-module (crates-io vl tk vltk) #:use-module (crates-io))

(define-public crate-vltk-0.1.0 (c (n "vltk") (v "0.1.0") (d (list (d (n "ash") (r "^0.37") (f (quote ("linked"))) (k 0)) (d (n "ash-window") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (o #t) (d #t) (k 0)))) (h "1q4jhl6798qra64ayq3rrd6dnr17a6bybaln50q8p0444a3nxdgy") (f (quote (("winit" "winit/default" "raw-window-handle" "ash-window") ("default" "ash/linked"))))))

(define-public crate-vltk-0.1.1 (c (n "vltk") (v "0.1.1") (d (list (d (n "ash") (r "^0.37.2") (f (quote ("linked"))) (k 0)) (d (n "ash-window") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (o #t) (d #t) (k 0)))) (h "1n2lq340zm5hlg8vksr8i05lvjia42h4v0p6c448adb31ai3jjh3") (f (quote (("winit" "winit/default" "raw-window-handle" "ash-window") ("default" "ash/linked"))))))

(define-public crate-vltk-0.1.2 (c (n "vltk") (v "0.1.2") (d (list (d (n "ash") (r "^0.37.2") (f (quote ("linked"))) (k 0)) (d (n "ash-window") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (o #t) (d #t) (k 0)))) (h "0qrlarx6qp2zpfmsxf0r2kc4hngi50sxnsklgqa23pwlsi7kv9d1") (f (quote (("winit" "winit/default" "raw-window-handle" "ash-window") ("default" "ash/linked"))))))

