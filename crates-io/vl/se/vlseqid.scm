(define-module (crates-io vl se vlseqid) #:use-module (crates-io))

(define-public crate-vlseqid-0.1.0 (c (n "vlseqid") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "1nx2hd7ayjvkxrdlsg93d9kdlwc3biivwv7008fwi5l6wr8v4r36")))

(define-public crate-vlseqid-0.2.0 (c (n "vlseqid") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "0v55jfkw3y78lxi8af79r9vyvj966xn4gzffmhn5fnvmz1jjr11c")))

(define-public crate-vlseqid-0.3.0 (c (n "vlseqid") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "0zhi06lkqh7vs1r34wzds1vsl16bnl14xm3s3gq2h1gszmcqn3c8")))

(define-public crate-vlseqid-0.4.0 (c (n "vlseqid") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "1rxz6klp7f7bqmix7f7wasgckbmk2gvyg8rk6fkf50w59i6fn93c")))

