(define-module (crates-io vl eu vleue_kinetoscope) #:use-module (crates-io))

(define-public crate-vleue_kinetoscope-0.1.0 (c (n "vleue_kinetoscope") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_ui" "bevy_winit" "default_font"))) (k 2)) (d (n "image") (r "^0.24") (f (quote ("gif"))) (k 0)))) (h "1ffficvr56d7q6kay1wh5a96cphpafmsbl6qbvij1cdrm67z8nrl")))

(define-public crate-vleue_kinetoscope-0.1.1 (c (n "vleue_kinetoscope") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite"))) (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_sprite" "bevy_ui" "bevy_winit" "default_font" "x11"))) (k 2)) (d (n "image") (r "^0.24") (f (quote ("gif"))) (k 0)))) (h "1sn1aq6l066v1n5w2bay1lpicc5lynqgr37za0w0kkmvgxddhwyb")))

