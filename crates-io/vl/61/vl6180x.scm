(define-module (crates-io vl #{61}# vl6180x) #:use-module (crates-io))

(define-public crate-vl6180x-0.1.0 (c (n "vl6180x") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.3") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1zvgd56724i09xd35fx4409hkwph2j2rkndlm0yisaybh23csbz4")))

(define-public crate-vl6180x-0.1.3 (c (n "vl6180x") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.3") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0v4mz0fd6cj7z57v050a9mhvl1x3hzb60d3jw01n6yjdsrydvv08")))

