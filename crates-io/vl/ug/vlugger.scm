(define-module (crates-io vl ug vlugger) #:use-module (crates-io))

(define-public crate-vlugger-0.1.0 (c (n "vlugger") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "0xng0bzkxs3dq86lnhazhvp4zy37syf81pbkcx84izc7vnpwlkm1")))

(define-public crate-vlugger-0.2.0 (c (n "vlugger") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "1cdxk20q2y78jvbk12y1l3zjxywbvygqw3xvs7gy87lam1c7ryqv")))

(define-public crate-vlugger-0.3.0 (c (n "vlugger") (v "0.3.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p5252cx6nhh36w7qnh8vlyb5x3dgw4868a4z028xrmljb0bdj10")))

