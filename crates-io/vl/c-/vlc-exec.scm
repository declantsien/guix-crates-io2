(define-module (crates-io vl c- vlc-exec) #:use-module (crates-io))

(define-public crate-vlc-exec-0.1.0 (c (n "vlc-exec") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 1)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0wzwkjpfiqkrcmh3p2cbkfkz6q5n7nhzikm0hfayylgs9q3044bl")))

