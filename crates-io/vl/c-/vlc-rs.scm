(define-module (crates-io vl c- vlc-rs) #:use-module (crates-io))

(define-public crate-vlc-rs-0.1.0 (c (n "vlc-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rfy1b1xa72s8dap6wcchgpyhwbm4ir7df59ajw60ya7jk8vfn82")))

(define-public crate-vlc-rs-0.2.0 (c (n "vlc-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k1zrbsyb37cirykyy6751125nsyd832c76ggcla4x3b1vqds0fi")))

(define-public crate-vlc-rs-0.3.0 (c (n "vlc-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vm9b6n3f9wagyj3sng43pwkcwpk1nq1rzybb03px5xjlzab48v2")))

