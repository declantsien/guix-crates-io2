(define-module (crates-io vl c- vlc-rc) #:use-module (crates-io))

(define-public crate-vlc-rc-0.1.0 (c (n "vlc-rc") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1r5xxxmvkzxi1z64xpkdwjx9s39kk3a3g4dsixfsm5gd0pzbv8i6")))

(define-public crate-vlc-rc-0.1.1 (c (n "vlc-rc") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0rq48z8q471ngysgnbck90fxlri6xs500jbk1s6wfy8w4ir9wrlx")))

