(define-module (crates-io h4 x_ h4x_core) #:use-module (crates-io))

(define-public crate-h4x_core-0.0.1 (c (n "h4x_core") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "minwinbase" "winerror" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gl6iz62gwl76hfmqh3q5jfx9aza32crpn9r32bxkbdjza39hnky")))

(define-public crate-h4x_core-0.0.2 (c (n "h4x_core") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "minwinbase" "winerror" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b581my128p7kjhaqi9way6vk1gffq4vvivpwb31jpih1i9rqvis")))

