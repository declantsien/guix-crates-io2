(define-module (crates-io h4 x_ h4x_re) #:use-module (crates-io))

(define-public crate-h4x_re-0.1.0 (c (n "h4x_re") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0z55kzd50pz55rkg6cxi2d4q505x5hr7s9i7xywihdcbzd2sn699")))

(define-public crate-h4x_re-0.2.0 (c (n "h4x_re") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1xdhm1r7glmwvj3f32jzfrx2afi59syy9z5n5hhbvqjm710hzqcp")))

(define-public crate-h4x_re-0.2.1 (c (n "h4x_re") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1j4q2cllv9kmhnw9mff3n4xmry8rhilv24b281qc23ziqjnh2qsh")))

(define-public crate-h4x_re-0.2.2 (c (n "h4x_re") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1x0zfwdvrm4xd2mmc70f357x4a5bz10mrhqnz202knnisr3jj2sl")))

(define-public crate-h4x_re-0.2.3 (c (n "h4x_re") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0lf654lx6wjg92l5jk04187fz78ljr7jnimg2fwpl94jgdawzpf2")))

(define-public crate-h4x_re-0.2.4 (c (n "h4x_re") (v "0.2.4") (h "1cxlslhnzdinyhp80jkvf3sx554dsz6rg4gg13c226g1gd8kgym9")))

