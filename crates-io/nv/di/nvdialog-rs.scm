(define-module (crates-io nv di nvdialog-rs) #:use-module (crates-io))

(define-public crate-nvdialog-rs-0.1.0-rc0 (c (n "nvdialog-rs") (v "0.1.0-rc0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lj86rg6zk0kibwmhzsivv1qqlvk35vigrslccgrcl50dv52byha")))

(define-public crate-nvdialog-rs-0.1.0 (c (n "nvdialog-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kw217zlbwbhjxw6xzzmrh1sg079a11lxs0sb7pc6r1qwzqidj1k")))

