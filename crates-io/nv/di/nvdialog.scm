(define-module (crates-io nv di nvdialog) #:use-module (crates-io))

(define-public crate-nvdialog-0.1.0 (c (n "nvdialog") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "0fk0nfplffsz9jn9rkmy9q3zbf62fn5py85dk9y56xlh1x4qi76w") (y #t)))

(define-public crate-nvdialog-0.1.3 (c (n "nvdialog") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "1hzwzgijif8sxnqxy4bxyh3bm6935ajgjkzcasvdq2wk9gazlmrk") (y #t)))

(define-public crate-nvdialog-0.1.5 (c (n "nvdialog") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "1snpyz7bflg2m384s7afsf7f7ca1y1i6b31p4sh9rd5r2x9xqgj6") (y #t)))

