(define-module (crates-io nv tt nvtt_sys) #:use-module (crates-io))

(define-public crate-nvtt_sys-0.1.0 (c (n "nvtt_sys") (v "0.1.0") (h "1217aaxryjw8nlfdq0psz48qq9v67sjzf18m1zpnyv9cksr24fpf")))

(define-public crate-nvtt_sys-0.2.0 (c (n "nvtt_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.0") (k 1)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "semver") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "vswhere") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0nchwh9zmn77fq4l1n9gb0llmizlcprlj55syswh4h8c5sjdkx7y")))

(define-public crate-nvtt_sys-0.2.1 (c (n "nvtt_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.51.0") (k 1)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "semver") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "vswhere") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1sddyhgnk153n12xdppqvw6gy3y4nq7cwddd9cslppq8hv5mzbid")))

(define-public crate-nvtt_sys-0.2.2 (c (n "nvtt_sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.52.0") (k 1)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "semver") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "vswhere") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "10x88d41hj8h6zqdm6811c4malnc9d8pb06ad6kll4a5b8kqd5ma")))

(define-public crate-nvtt_sys-0.3.0 (c (n "nvtt_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (k 1)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "semver") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "vswhere") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1csr0qlw59hvspl0p53mcakarxk9rcw37gjil0dsy3q5cvrd6rj9")))

(define-public crate-nvtt_sys-0.4.0 (c (n "nvtt_sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55") (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "semver") (r "^0.10") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "vswhere") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0jbagqf1yw44rxp6d5vg4pzhkjrg0lkcz817gznfm34znm7dyj7a")))

(define-public crate-nvtt_sys-0.5.0 (c (n "nvtt_sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 1)))) (h "186mv25jdjms69gcs6na6pc63g9yfig9cqzlzs40nwachax4i184") (r "1.66")))

(define-public crate-nvtt_sys-0.5.1 (c (n "nvtt_sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 1)))) (h "00iss4m5ilmpl5ch18isj3j8b1x2xxfad224in7f7zxh75q2x3y4") (r "1.66")))

(define-public crate-nvtt_sys-0.5.2 (c (n "nvtt_sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 1)))) (h "11clfk2dg6gcdmg02mrjdd7pd0jl7c4mxil1shpwf9l07w8bfwpw") (r "1.66")))

