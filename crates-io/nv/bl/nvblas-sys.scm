(define-module (crates-io nv bl nvblas-sys) #:use-module (crates-io))

(define-public crate-nvblas-sys-0.1.0 (c (n "nvblas-sys") (v "0.1.0") (h "0ml94ndiphmrd87xalwqkbi72psprdad2y91n3qfs4w6z8cps36m") (l "nvblas")))

(define-public crate-nvblas-sys-0.1.1 (c (n "nvblas-sys") (v "0.1.1") (h "0jaib6al3dhhmhckgcsk2baxxn6ff9xg6bamix8jvz41z34ylqxr") (l "nvblas")))

