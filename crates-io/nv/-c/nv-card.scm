(define-module (crates-io nv -c nv-card) #:use-module (crates-io))

(define-public crate-nv-card-0.1.0 (c (n "nv-card") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qpfvn8cz7xfzn30mgzmm11gps5phi0kcgsmyzs5w7ypyfqq1znj")))

(define-public crate-nv-card-0.1.1 (c (n "nv-card") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0wb1mm4wl335s37azjjz21n31zcm6c2nwd1hp9ygp62d734b6bmc")))

(define-public crate-nv-card-0.1.2 (c (n "nv-card") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jfa7rczn22g9h9262j9qp8p1mq9fl3m9az5p7hzawz5v8mnv97v")))

