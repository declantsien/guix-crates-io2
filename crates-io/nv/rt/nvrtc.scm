(define-module (crates-io nv rt nvrtc) #:use-module (crates-io))

(define-public crate-nvrtc-0.1.0 (c (n "nvrtc") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.43") (d #t) (k 1)))) (h "0s6wjgx7y44zxf87c0ylqlynjb887jywvs6jz9b9hs35gl1v7xh4")))

(define-public crate-nvrtc-0.1.1 (c (n "nvrtc") (v "0.1.1") (d (list (d (n "bindgen") (r "~0.43") (d #t) (k 1)))) (h "031n82n67jrnlgrgppm91dz4v2r9pvi1yafc02gij7rngm6cd0fx")))

(define-public crate-nvrtc-0.1.2 (c (n "nvrtc") (v "0.1.2") (d (list (d (n "bindgen") (r "~0.43") (d #t) (k 1)))) (h "0kwp46c2rh13qd98gw7k2734n5fnllnd66lgk2kkd68y47r747dv")))

(define-public crate-nvrtc-0.1.3 (c (n "nvrtc") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1dj2invl3bn1qh18hxszfs71z5ql863fg0zlvm5rb2f7f1d8mbkr") (f (quote (("static"))))))

