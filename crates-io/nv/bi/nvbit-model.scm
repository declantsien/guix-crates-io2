(define-module (crates-io nv bi nvbit-model) #:use-module (crates-io))

(define-public crate-nvbit-model-0.0.18 (c (n "nvbit-model") (v "0.0.18") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13z45hlknjqj5dn1xfxk4hrn0id9zk0h9l6pf9758pshf5vclhph")))

(define-public crate-nvbit-model-0.0.19 (c (n "nvbit-model") (v "0.0.19") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0snji52ch2bj012i5y9hmsgfk9jris1h7rb4by7xr3l8lsb6hss6")))

(define-public crate-nvbit-model-0.0.20 (c (n "nvbit-model") (v "0.0.20") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05qkncczfy2ii60rrqr2513myk3lfqn9krgpax5k3pxaiswl0zxh")))

(define-public crate-nvbit-model-0.0.21 (c (n "nvbit-model") (v "0.0.21") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19m6bmy142qvvpy28j5slmkqz7j1sw098annl5hwaranm200ls44")))

(define-public crate-nvbit-model-0.0.22 (c (n "nvbit-model") (v "0.0.22") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h2va864wycrmq8jc0cw80j4lzadjyysyx9fgvhiyzkklz5dxlbn")))

(define-public crate-nvbit-model-0.0.23 (c (n "nvbit-model") (v "0.0.23") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "058gywdngfiiv4vrala8942wvpv1gsvil8z0qhjcivlfbgzz9v9r")))

(define-public crate-nvbit-model-0.0.24 (c (n "nvbit-model") (v "0.0.24") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wns9fbmbavm5b9x4364293qdrkrj5svcx04xaidbkvnham4bh5n")))

(define-public crate-nvbit-model-0.0.25 (c (n "nvbit-model") (v "0.0.25") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15765bzrjm1xf077hp38vbz6k4mmihvwz4dhrmfvvycfnqcswilh")))

(define-public crate-nvbit-model-0.0.26 (c (n "nvbit-model") (v "0.0.26") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lwlfyjyk2bwqwfilshnvygmgk1r1h60y34fz65vsnn317z0r07n")))

(define-public crate-nvbit-model-0.0.27 (c (n "nvbit-model") (v "0.0.27") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lwf8s9qwyq1j5mkkwj7l10hywv2znzzv9w9dfad69jf8p3vijq8")))

(define-public crate-nvbit-model-0.0.28 (c (n "nvbit-model") (v "0.0.28") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g15pcgvj5qkg2w40vzv62l4zjsvl38kdymnmzlap2dpp4ydidmp")))

(define-public crate-nvbit-model-0.0.29 (c (n "nvbit-model") (v "0.0.29") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16i2650l2nmawlbpj4hlvlj3hbp6zfyjlv7fxhfq16j51ky230pd")))

(define-public crate-nvbit-model-0.0.30 (c (n "nvbit-model") (v "0.0.30") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hh6ga23812570qaxng5pm5d8d8dhc4552mbcfa8cc8z3za5v8p7")))

(define-public crate-nvbit-model-0.0.31 (c (n "nvbit-model") (v "0.0.31") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k5jdi8cvrk81lac99zqyk18r1jc3wwnrgzj7b4yzr09kixhmkxy")))

(define-public crate-nvbit-model-0.0.32 (c (n "nvbit-model") (v "0.0.32") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xsr3ic1y1y24ik863c7pn9zc2b77dwlqykysdgsjbndmnxq2l1y")))

(define-public crate-nvbit-model-0.0.33 (c (n "nvbit-model") (v "0.0.33") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10bcb3jsbr8z0xdl62af0fggzx6fjdnx6k2zagf9l0zgi7s892q9")))

(define-public crate-nvbit-model-0.0.34 (c (n "nvbit-model") (v "0.0.34") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17qgrgqc3kiw62cbn1sma748dgaag7wrvnjdx40sjvkxv1pfvffc")))

(define-public crate-nvbit-model-0.0.35 (c (n "nvbit-model") (v "0.0.35") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wzsccy446in96h3d552ilrx76xwcz6n96kyn7kjh2xkwb3v8l26")))

(define-public crate-nvbit-model-0.0.36 (c (n "nvbit-model") (v "0.0.36") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02w2fn7zi0r48zsrmymcsv0zhaf2yfk4pzmpr5bzbpqhk7r2si0j")))

(define-public crate-nvbit-model-0.0.37 (c (n "nvbit-model") (v "0.0.37") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "069c82ba5qqsra61ri2pg3pn9mc2ykq129qkyxilzdnqjkqjidsw")))

(define-public crate-nvbit-model-0.0.38 (c (n "nvbit-model") (v "0.0.38") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q1j0ifjrv2zyfrr5dmkl6xlzrn50vcij54q2qpli90zbnp4wpj0")))

