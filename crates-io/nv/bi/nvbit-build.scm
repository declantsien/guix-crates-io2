(define-module (crates-io nv bi nvbit-build) #:use-module (crates-io))

(define-public crate-nvbit-build-0.0.7 (c (n "nvbit-build") (v "0.0.7") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yg8nwh4xjdj3gvvi38w72qwk526bni7a9p24bxilqy7j3bx3v5i")))

(define-public crate-nvbit-build-0.0.8 (c (n "nvbit-build") (v "0.0.8") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sjaqckznicf6gcwrz1bzmrn4clcyhy0nggdl3n6yci70jymkb8q")))

(define-public crate-nvbit-build-0.0.9 (c (n "nvbit-build") (v "0.0.9") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qdldimr8224f5a1i9hsgjvyififdf7mxpw5jpbh6yzpdprq2xr1")))

(define-public crate-nvbit-build-0.0.10 (c (n "nvbit-build") (v "0.0.10") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hqpq49xi5rd39qrns9nn0llivrzlsb04nq8wmsxkq043z1wxasy")))

(define-public crate-nvbit-build-0.0.11 (c (n "nvbit-build") (v "0.0.11") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "175jzgx2h4pl89pxpxkccxgpxy8ydy4fb33dnz6fpndw6dyck8ss")))

(define-public crate-nvbit-build-0.0.12 (c (n "nvbit-build") (v "0.0.12") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wzwiqjv82hkxcdcyrl8kbyzn7ci37vrhag7x4fkgzfn0gvn0dfh")))

(define-public crate-nvbit-build-0.0.13 (c (n "nvbit-build") (v "0.0.13") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bdz0dvqcw56c2wh07n9dvlfvc0bj2ifcp71md9xn2nigyci9bvb")))

(define-public crate-nvbit-build-0.0.14 (c (n "nvbit-build") (v "0.0.14") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ayawi0zi5dncgxkx36cjsdy131gcc35a4kbwk0rih9r6vi8005m")))

(define-public crate-nvbit-build-0.0.15 (c (n "nvbit-build") (v "0.0.15") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x126jycl3dnbgcksy2gpq4icn84xlkzx6qpq4wgjv7bsahpl5yp")))

(define-public crate-nvbit-build-0.0.16 (c (n "nvbit-build") (v "0.0.16") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17vpqz85z7z124jk3jvcmqcg44arlfzg16if8zl1v25wh12yd9hm")))

(define-public crate-nvbit-build-0.0.18 (c (n "nvbit-build") (v "0.0.18") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05ww6lx4j7piw8w86v534phsjq48y20d2nwv5lzi7sr56y3q9w6b")))

(define-public crate-nvbit-build-0.0.19 (c (n "nvbit-build") (v "0.0.19") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g828axyrn0001r4s2pzy7jbgg53kykr2rsrkfkprb4mh56kw5g4")))

(define-public crate-nvbit-build-0.0.20 (c (n "nvbit-build") (v "0.0.20") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qhyb7cm80mmc3pdm7b9xyr6q486ckl3lw8r3pbsc5br3nz0058i")))

(define-public crate-nvbit-build-0.0.21 (c (n "nvbit-build") (v "0.0.21") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mrhg0rmr6gknpxfdvqw26yzwqlxka2ff2v3kddjxg5m6yf7l75f")))

(define-public crate-nvbit-build-0.0.22 (c (n "nvbit-build") (v "0.0.22") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kxh9d6ri758bv8gkrc70hmf8lxpzs55q8c66jzw1q0is6kp2qbr")))

(define-public crate-nvbit-build-0.0.23 (c (n "nvbit-build") (v "0.0.23") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a66yv0sqq7nkinc4iq1fihjrf0cwf8825zyddqw8k4076zkwl9h")))

(define-public crate-nvbit-build-0.0.24 (c (n "nvbit-build") (v "0.0.24") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1218sfnagbn0346jrphfji3mr68ry32zs7r0dg4msqwn88yqrjhs")))

(define-public crate-nvbit-build-0.0.25 (c (n "nvbit-build") (v "0.0.25") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lp467li8ggybjj83rs5pxhrhmw7yz7ll44i4bnc3pdn289b41a2")))

(define-public crate-nvbit-build-0.0.26 (c (n "nvbit-build") (v "0.0.26") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gnfzpdh2kdr98wb9gvzsyhdx4z44lr290f7fjmxak1rp4m23839")))

(define-public crate-nvbit-build-0.0.27 (c (n "nvbit-build") (v "0.0.27") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0inyb2z80s6zravbi5zy8cnal87ala8aclnnm0hba81gi23474ff")))

(define-public crate-nvbit-build-0.0.28 (c (n "nvbit-build") (v "0.0.28") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kvsd0ms5yh5cd0178wvlwfxq9w35x3zaql9hc2p8s6k3wwig8b6")))

(define-public crate-nvbit-build-0.0.29 (c (n "nvbit-build") (v "0.0.29") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wn6616rb75swkiwnbjivm4amvchjm4nizlkfkh1glciq9iszs3k")))

(define-public crate-nvbit-build-0.0.30 (c (n "nvbit-build") (v "0.0.30") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vfzyh1hriyri9lcs2d6rhx124mzjam4fyjfpwrf9hvjybih4025")))

(define-public crate-nvbit-build-0.0.31 (c (n "nvbit-build") (v "0.0.31") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h4n6k4nhvdz20yzkqc146vdf3dag6y600xqjzrp2zrnvg9ssqz9")))

(define-public crate-nvbit-build-0.0.32 (c (n "nvbit-build") (v "0.0.32") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "032340cygpxyw9vkk2dhya9iw0kj8gcklksfni4pi6m3fby4clw6")))

(define-public crate-nvbit-build-0.0.33 (c (n "nvbit-build") (v "0.0.33") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qa4xqgy7273qknyig2726q5f7s52i54qsid50l0f9pgv8kqgi6q")))

(define-public crate-nvbit-build-0.0.34 (c (n "nvbit-build") (v "0.0.34") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02826if1hwkjn2g2x38l7qly75020341vyy0myyhk88xmplwammy")))

(define-public crate-nvbit-build-0.0.35 (c (n "nvbit-build") (v "0.0.35") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d8lxgd06d8ipyc6w4km8jq3bqm89r15x2hp1szlxyvbrdqhf0i7")))

(define-public crate-nvbit-build-0.0.36 (c (n "nvbit-build") (v "0.0.36") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1j4d1d46255862qarwb7crfna5rda7qq2jnymscj4v19vm6dzfri")))

(define-public crate-nvbit-build-0.0.37 (c (n "nvbit-build") (v "0.0.37") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02phmrkhsslrfifznmhgckmzbxa3dp6mpngs63byz2nw4mdqii1c")))

(define-public crate-nvbit-build-0.0.38 (c (n "nvbit-build") (v "0.0.38") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qbpd6y4p74kws0ghqq201jk6ry37b64ca23zk08cdn8s6czk3s2")))

