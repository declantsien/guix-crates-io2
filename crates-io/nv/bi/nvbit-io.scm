(define-module (crates-io nv bi nvbit-io) #:use-module (crates-io))

(define-public crate-nvbit-io-0.0.18 (c (n "nvbit-io") (v "0.0.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01ixinkqibwy6jmz8jjbr980a4ahm8cc62waglb2la0rxlsp5ygy")))

(define-public crate-nvbit-io-0.0.19 (c (n "nvbit-io") (v "0.0.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "06pk1p1qypqh863h1pwxa49hgnp5i8c3jw0w1pd1anxvr3vjhpby")))

(define-public crate-nvbit-io-0.0.20 (c (n "nvbit-io") (v "0.0.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0y0nygfnab147qnimqa03d4jn45q60ghl07cna2ivn723lgkkmi3")))

(define-public crate-nvbit-io-0.0.21 (c (n "nvbit-io") (v "0.0.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nhzp8p2kqn9r5b3w8krr5pkfr0q5y4s1fhpd71i2bp9n70q9hpb")))

(define-public crate-nvbit-io-0.0.22 (c (n "nvbit-io") (v "0.0.22") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0msbgjgzdhh3bbk2cak5jb87z3xvadmwfddmsf0rpnfwra0fywnn")))

(define-public crate-nvbit-io-0.0.23 (c (n "nvbit-io") (v "0.0.23") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1frz03hxfhbhck7k9n42nvj87y8y36qsbcahqbpyw5n34nzqlpml")))

(define-public crate-nvbit-io-0.0.24 (c (n "nvbit-io") (v "0.0.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12vp1chzrzfma8ms47nmnvlxncwx2nhcg3vfaz87wwji8f7wfvgg")))

(define-public crate-nvbit-io-0.0.25 (c (n "nvbit-io") (v "0.0.25") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1g663k6jyk09hxpglrhjhdnspl88ly6s4mlx802ypwvq6gsl36b4")))

(define-public crate-nvbit-io-0.0.26 (c (n "nvbit-io") (v "0.0.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1z8a136byby6c99nyb03bc522z86wamyymkwrdlml5bbmjdvrjpy")))

(define-public crate-nvbit-io-0.0.27 (c (n "nvbit-io") (v "0.0.27") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0dnpdgx4vxdj927k2ac8hm4jg7ki6z73jwdxp5xq83riw3anam1r")))

(define-public crate-nvbit-io-0.0.28 (c (n "nvbit-io") (v "0.0.28") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "10vxbbqdykpbgmm0xz7ahc5d20wd7z03vc846hac7rcq50n5mmgl")))

(define-public crate-nvbit-io-0.0.29 (c (n "nvbit-io") (v "0.0.29") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0541a9sdc8vvlmagl3pzplmnkpqh9rgc4v4vh41ypl78nwaagcba")))

(define-public crate-nvbit-io-0.0.30 (c (n "nvbit-io") (v "0.0.30") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0qq0ppfffbhm25rxkhxhasr7dkgaxyiz3v50jnd57y0388lj4k05")))

(define-public crate-nvbit-io-0.0.31 (c (n "nvbit-io") (v "0.0.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0bg14qr4kdg6xm3z6zpjywr54irdz2fd7408080g5dwhrsrq2x6m")))

(define-public crate-nvbit-io-0.0.32 (c (n "nvbit-io") (v "0.0.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lqri4fxcyi5cdccjxpv8q0ffr3cc2gfwqb6lhg3n2bmjkzschlv")))

(define-public crate-nvbit-io-0.0.33 (c (n "nvbit-io") (v "0.0.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ibvaxkma1jnss72xynzhfpw5xkfjsimm6s1r7hlf7rpamb3pa7z")))

(define-public crate-nvbit-io-0.0.34 (c (n "nvbit-io") (v "0.0.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08psql99j15b2r4p4lv2zq6y573qmvhvh3h597cxcd66iraags5k")))

(define-public crate-nvbit-io-0.0.35 (c (n "nvbit-io") (v "0.0.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00y7xm4jh0y5lhwvkadgzhq9vk0hw33kkfrpf2qbcbhj8wsc33v1")))

(define-public crate-nvbit-io-0.0.36 (c (n "nvbit-io") (v "0.0.36") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12w0z8s6pyh2n9162ji6fh5m15ps7819m627hacy3fbx2x5058c5")))

(define-public crate-nvbit-io-0.0.37 (c (n "nvbit-io") (v "0.0.37") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lhc0qcd7nmbn9vd2qzq804hyzkz23bm46ibmkwa9xfb5a8d7w4w")))

(define-public crate-nvbit-io-0.0.38 (c (n "nvbit-io") (v "0.0.38") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1wrvrjjhlj9hdmim6jn3cg1v12166w7x06pygk37wlj340a7l2kh")))

