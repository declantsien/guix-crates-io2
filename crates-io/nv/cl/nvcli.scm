(define-module (crates-io nv cl nvcli) #:use-module (crates-io))

(define-public crate-nvcli-1.0.0 (c (n "nvcli") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^0.1.4") (d #t) (k 0)))) (h "0dn7rfzw7fgxmrs1kfcv4cxgfm7m19qnq651f8xgj3x4ccqg83xm")))

(define-public crate-nvcli-1.1.0 (c (n "nvcli") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^0.1.4") (d #t) (k 0)))) (h "1x9vy5pklv9rzqmrf5kp4zl93w6hwi883fvrs4s9f844zcnzy9q9")))

(define-public crate-nvcli-1.1.1 (c (n "nvcli") (v "1.1.1") (d (list (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^0.1.4") (d #t) (k 0)))) (h "1pxk0lmbbjp76nbrr6gpi1y24z0rwpw4ba2jp0zsqiqhd4xml84a")))

(define-public crate-nvcli-1.1.2 (c (n "nvcli") (v "1.1.2") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^0.1.4") (d #t) (k 0)))) (h "0pzxk9wm3v09r85ldgcmww5w50w4pfvzx8d143blx0805vdy7nmf")))

(define-public crate-nvcli-1.1.3 (c (n "nvcli") (v "1.1.3") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^1.530.1") (d #t) (k 0)))) (h "1k9jg22m960ly40k31x1jbqx1nw3s9xly577klzwqy4fsdxfa5p8") (y #t)))

(define-public crate-nvcli-1.1.4 (c (n "nvcli") (v "1.1.4") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nvapi_sys_new") (r "^1.530.1") (d #t) (k 0)))) (h "0qs8ikndf6bzpi8mdlp1c2cdbmnybsjv249w69p8b4hzd6xz0yby")))

