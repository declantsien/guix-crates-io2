(define-module (crates-io nv g- nvg-gl) #:use-module (crates-io))

(define-public crate-nvg-gl-0.5.0 (c (n "nvg-gl") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.1") (d #t) (k 2)) (d (n "nvg") (r "^0.5.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0zbylsh5hvbflkfz72kbdc0fgjsz5g6rk272la04h3zj1pfqj6z9")))

(define-public crate-nvg-gl-0.5.4 (c (n "nvg-gl") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 2)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.21.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nvg") (r "^0.5.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "1nyvqx9gm4nxns53sgdzigm5gkv11vjw5ja277pzgb0r620pqj2l")))

