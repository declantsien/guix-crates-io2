(define-module (crates-io nv m- nvm-win) #:use-module (crates-io))

(define-public crate-nvm-win-0.0.0 (c (n "nvm-win") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdkn86ilwaymds2rp55g3pnrb8lfffs0fmwcd8lsixb3sjn1xhw")))

(define-public crate-nvm-win-0.0.1 (c (n "nvm-win") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1afzibrr041yyr925xm8rdbhz9gadbxqifn0f06x7zahjngafk4v")))

(define-public crate-nvm-win-0.0.2 (c (n "nvm-win") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1slacsdymzj408nrs23ab10ccj2k7cg05fxrka10fw9lgns80j4i")))

(define-public crate-nvm-win-0.0.3 (c (n "nvm-win") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "00278x1vh82bz01cd1f54sbgz24hs8m49mv4w438532bf6nk0y0m")))

