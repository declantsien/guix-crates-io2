(define-module (crates-io nv d- nvd-cves) #:use-module (crates-io))

(define-public crate-nvd-cves-0.1.0 (c (n "nvd-cves") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "nvd-cpe") (r "^0.1.0") (d #t) (k 0)) (d (n "nvd-cvss") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03fsi3cxl45f6yhk4khq7fjlkzqr9mpsk6d83803rqnqnbbj5dnl")))

(define-public crate-nvd-cves-0.1.1 (c (n "nvd-cves") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "nvd-cpe") (r "^0.1.0") (d #t) (k 0)) (d (n "nvd-cvss") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sdyywq1xzxsyl3w3h2dn0js2771iz1nqn1vb30vg15a1ksl0m4r")))

