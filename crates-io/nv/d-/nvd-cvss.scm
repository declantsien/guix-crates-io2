(define-module (crates-io nv d- nvd-cvss) #:use-module (crates-io))

(define-public crate-nvd-cvss-0.1.0 (c (n "nvd-cvss") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l98abpdnbhgbzq2lxx54adzm2pl7jkdp9zj2nkqxjvzpl6xf00j")))

