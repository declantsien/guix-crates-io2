(define-module (crates-io nv d- nvd-cpe) #:use-module (crates-io))

(define-public crate-nvd-cpe-0.1.0 (c (n "nvd-cpe") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.1") (d #t) (k 0)))) (h "0yra2cqzcwsvfgy0bza0m3jd6axm192wflmid3sy6zbjlplbqpvr")))

(define-public crate-nvd-cpe-0.1.1 (c (n "nvd-cpe") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.1") (d #t) (k 0)))) (h "1m2xamv7c3fiid5mdllxdx60p7a737wl8b8ydb4iimz8vzqn8idb")))

