(define-module (crates-io nv im nvim) #:use-module (crates-io))

(define-public crate-nvim-0.0.0 (c (n "nvim") (v "0.0.0") (h "0ky8jxb9bw15xlkilbmh4b3abak2kcjw66imwlnz9fp6zbsip9j5")))

(define-public crate-nvim-0.0.1 (c (n "nvim") (v "0.0.1") (h "08c56iwv1ki4gd3yj3k6wc5r93hq348jbhvzaycj6qlsrl02l4ap")))

