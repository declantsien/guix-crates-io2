(define-module (crates-io nv im nvim-oxi-libuv) #:use-module (crates-io))

(define-public crate-nvim-oxi-libuv-0.5.0 (c (n "nvim-oxi-libuv") (v "0.5.0") (d (list (d (n "luajit") (r "^0.5.0") (d #t) (k 0) (p "nvim-oxi-luajit")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1126pl28lpniva2wffw17zvqldvvcw8p5lyv3i5mmb60i76q4m6g")))

