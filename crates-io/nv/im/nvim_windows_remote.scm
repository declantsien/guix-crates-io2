(define-module (crates-io nv im nvim_windows_remote) #:use-module (crates-io))

(define-public crate-nvim_windows_remote-0.1.0 (c (n "nvim_windows_remote") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)))) (h "0fxa3k787a55v5lwb0r6mmcm5vrz1b406vgpqyziawlz2mjsl4dl")))

