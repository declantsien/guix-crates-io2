(define-module (crates-io nv im nvimpam) #:use-module (crates-io))

(define-public crate-nvimpam-0.1.0 (c (n "nvimpam") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.5.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.1") (d #t) (k 0)))) (h "0whsipyc9bj8f7qzc0f32hzhk0hj9aq4h0fspx8nrhlbpmpram1v")))

(define-public crate-nvimpam-0.1.1 (c (n "nvimpam") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.5.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.1") (d #t) (k 0)))) (h "17zb1fh0zcpjlxffg5x4fj8pkxpisddxfy1xxrd14f4iwv09wb62")))

(define-public crate-nvimpam-0.2.0 (c (n "nvimpam") (v "0.2.0") (d (list (d (n "atoi") (r "^0.2.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.5.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.2") (d #t) (k 0)))) (h "0wvcc73ymwwwb6s1pjrf53ppcb0syfs3v8hacpiih974kbyb4vmx") (f (quote (("valgrind") ("no_valgrind") ("default" "no_valgrind"))))))

(define-public crate-nvimpam-0.2.1 (c (n "nvimpam") (v "0.2.1") (d (list (d (n "atoi") (r "^0.2.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.5.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.2") (d #t) (k 0)))) (h "115mg0fcj2b1s9d1rhixyrdfykdpvcrdypyn9mw27afjnvz902bh") (f (quote (("valgrind") ("no_valgrind") ("default" "no_valgrind"))))))

