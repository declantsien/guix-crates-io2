(define-module (crates-io nv im nvim-wiretapper-client) #:use-module (crates-io))

(define-public crate-nvim-wiretapper-client-0.1.0 (c (n "nvim-wiretapper-client") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0y92285qc7y8bpvhfn398li64q91ld9zxk5b3nm9diphibfgidbp")))

