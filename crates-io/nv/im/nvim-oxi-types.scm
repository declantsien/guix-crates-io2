(define-module (crates-io nv im nvim-oxi-types) #:use-module (crates-io))

(define-public crate-nvim-oxi-types-0.5.0 (c (n "nvim-oxi-types") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "luajit") (r "^0.5.0") (d #t) (k 0) (p "nvim-oxi-luajit")) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v0g1yd8wia1fjwrld0irmrpm0lrzqmvws7fmlny37cd1qz5di1f") (s 2) (e (quote (("serde" "dep:serde"))))))

