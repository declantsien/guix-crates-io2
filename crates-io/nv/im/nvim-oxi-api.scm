(define-module (crates-io nv im nvim-oxi-api) #:use-module (crates-io))

(define-public crate-nvim-oxi-api-0.5.0 (c (n "nvim-oxi-api") (v "0.5.0") (d (list (d (n "luajit") (r "^0.5.0") (d #t) (k 0) (p "nvim-oxi-luajit")) (d (n "macros") (r "^0.5.0") (d #t) (k 0) (p "nvim-oxi-macros")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "types") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0) (p "nvim-oxi-types")))) (h "07fmzq23820jjixrpg5gg36qiaca465d9iy53vvfbb4a7pkhyar1") (f (quote (("neovim-nightly" "neovim-0-10") ("neovim-0-9") ("neovim-0-10" "neovim-0-9"))))))

