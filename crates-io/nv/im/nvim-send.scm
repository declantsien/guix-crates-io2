(define-module (crates-io nv im nvim-send) #:use-module (crates-io))

(define-public crate-nvim-send-0.0.0 (c (n "nvim-send") (v "0.0.0") (h "0yx05nsspjhyw92kkj1jw712zpcw5gq8by37pfamfnh8ws0m3fm3") (y #t)))

(define-public crate-nvim-send-0.0.1 (c (n "nvim-send") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "nvim-rs") (r "^0.3") (f (quote ("use_tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "08ichaamqiy8a13b4dd6r83n3qglrch6xrmb244m4sjkfbqf2a6i")))

(define-public crate-nvim-send-0.0.2 (c (n "nvim-send") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "nvim-rs") (r "^0.3") (f (quote ("use_tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "15bhk0rhiy0jzzg0vig32qjn143mqlzpqc3qjaw9kgk9bzg2gj06")))

(define-public crate-nvim-send-0.0.3 (c (n "nvim-send") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "nvim-rs") (r "^0.3") (f (quote ("use_tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1zvahbiy3449i9ikw5pfmm8rc1ys10nzy1xsnsx6gachwqk9fjaq")))

(define-public crate-nvim-send-0.0.4 (c (n "nvim-send") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nvim-rs") (r "^0.3") (f (quote ("use_tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1wf05zvcw2vgamb495a9f4zcp2h2zxwspswvhxf4n1c338nbb6pi")))

