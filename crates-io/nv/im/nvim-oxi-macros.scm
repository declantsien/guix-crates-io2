(define-module (crates-io nv im nvim-oxi-macros) #:use-module (crates-io))

(define-public crate-nvim-oxi-macros-0.5.0 (c (n "nvim-oxi-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ask1iy74m13f6ri07j971gxa9cd44myrg0nzxhnmikjmrw6lhj8") (f (quote (("test-terminator" "test") ("test" "plugin") ("plugin"))))))

