(define-module (crates-io nv im nvim-switcher) #:use-module (crates-io))

(define-public crate-nvim-switcher-0.1.0 (c (n "nvim-switcher") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cliclack") (r "^0.1.13") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fb40xfsqjhswmgf49yyn6va96d1d3wq295vpa28h1ympva7v9lv")))

