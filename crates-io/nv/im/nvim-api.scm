(define-module (crates-io nv im nvim-api) #:use-module (crates-io))

(define-public crate-nvim-api-0.2.0 (c (n "nvim-api") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.11") (d #t) (k 0)) (d (n "luajit-bindings") (r "^0.2.0") (d #t) (k 0)) (d (n "nvim-types") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13ydpa3yjlx5mdfqjvn9c7c2knric4r66qc1bav1px3kv5q5fgww") (f (quote (("neovim-nightly" "nvim-types/neovim-nightly") ("neovim-0-8" "nvim-types/neovim-0-8") ("neovim-0-7" "nvim-types/neovim-0-7"))))))

