(define-module (crates-io nv im nvim-types) #:use-module (crates-io))

(define-public crate-nvim-types-0.1.0 (c (n "nvim-types") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "141qc3bzvgw41mxrcvg8lh2hn1hq4i7pbsrrb8rbls1dn22llqb7")))

(define-public crate-nvim-types-0.1.1 (c (n "nvim-types") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f239pnvq804lr8fvsspxxra701md1fg3ivj8xykj4vfyv0lsksk")))

(define-public crate-nvim-types-0.2.0 (c (n "nvim-types") (v "0.2.0") (d (list (d (n "luajit-bindings") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hhhzkvvla2ihc5k1z7a0n6w5njlvi3f9y291mrpayrhii0pn29j") (f (quote (("neovim-nightly") ("neovim-0-8") ("neovim-0-7"))))))

