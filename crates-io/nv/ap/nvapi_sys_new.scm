(define-module (crates-io nv ap nvapi_sys_new) #:use-module (crates-io))

(define-public crate-nvapi_sys_new-0.1.0 (c (n "nvapi_sys_new") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0ai7hv3c1f8b97cgyr9qh46q2h5i5bilm1awz5ill5wplhh7nqnn")))

(define-public crate-nvapi_sys_new-0.1.1 (c (n "nvapi_sys_new") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "04zh3ln739l9ylc76qr8lml2286f8niczm35308r2isixklvqb88")))

(define-public crate-nvapi_sys_new-0.1.2 (c (n "nvapi_sys_new") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "13lqv8a8ykbjmnyla4116x5rla1dpa7w6wvaajw9g8mfv24pk75f")))

(define-public crate-nvapi_sys_new-0.1.3 (c (n "nvapi_sys_new") (v "0.1.3") (h "0gmpk84r5jxl8ac534acg44n3ylrnb6p832rrps7cqf83jb4yzza")))

(define-public crate-nvapi_sys_new-0.1.4 (c (n "nvapi_sys_new") (v "0.1.4") (h "06jymji738gfkgb2a64z366cf6gaybbdizj10k11blj4pmp4zz4y")))

(define-public crate-nvapi_sys_new-0.1.5 (c (n "nvapi_sys_new") (v "0.1.5") (h "1lmfx059fhri067103wx14kndk4d8wx4bf43gb75vs3kbz2x43d6")))

(define-public crate-nvapi_sys_new-1.530.0 (c (n "nvapi_sys_new") (v "1.530.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "062a8shabf89c13l9kildd0ivsrw20lbh6b8nr508cyx601zdlmf")))

(define-public crate-nvapi_sys_new-1.530.1 (c (n "nvapi_sys_new") (v "1.530.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0dvp6fr63f8hpvbdq9ghxnbl4imk9wnw7rvp4bs9vmi3czsnn78j")))

