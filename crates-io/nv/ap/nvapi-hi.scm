(define-module (crates-io nv ap nvapi-hi) #:use-module (crates-io))

(define-public crate-nvapi-hi-0.0.2 (c (n "nvapi-hi") (v "0.0.2") (d (list (d (n "nvapi") (r "^0.0.2") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0qnrxm9jvljbnk994nd9w15z9kws0f5wj9pcf9l8rg6pkfmg1sy7") (f (quote (("serde_types" "serde" "serde_derive" "nvapi/serde_types") ("default" "serde_types"))))))

(define-public crate-nvapi-hi-0.0.3 (c (n "nvapi-hi") (v "0.0.3") (d (list (d (n "nvapi") (r "^0.0.3") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0jigj7b0v90yc9llz358yks9b8rsx75l2bzjpzs4dsnr2ri28zrj") (f (quote (("serde_types" "serde" "serde_derive" "nvapi/serde_types") ("default" "serde_types"))))))

(define-public crate-nvapi-hi-0.1.0 (c (n "nvapi-hi") (v "0.1.0") (d (list (d (n "nvapi") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0ah21lp60hcxsli4cknh934w56mhlw9k0h9aic2p7w7xin1jbi8x") (f (quote (("serde_types" "serde" "serde_derive" "nvapi/serde_types") ("default" "serde_types"))))))

