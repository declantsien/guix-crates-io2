(define-module (crates-io nv ng nvngx) #:use-module (crates-io))

(define-public crate-nvngx-0.1.0 (c (n "nvngx") (v "0.1.0") (d (list (d (n "ash") (r "^0.37") (d #t) (k 0)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)))) (h "04jq90fd9y8rh6wfwjfqpnmzmx4m38mf18wiil4rkczc3zq22i6z") (r "1.57")))

(define-public crate-nvngx-0.1.1 (c (n "nvngx") (v "0.1.1") (d (list (d (n "ash") (r "^0.37") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)))) (h "1xhg0b8qr37229myf148pfn3zjpzzrw6y25lf2mqw34xvw66nr9h") (r "1.57")))

(define-public crate-nvngx-0.1.2 (c (n "nvngx") (v "0.1.2") (d (list (d (n "ash") (r "^0.37") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)))) (h "1n77c9gdp7068gnaspbspq5vndam4msmnyis9fn25zycxdnxz1rg") (r "1.57")))

