(define-module (crates-io nv pa nvpair-rs) #:use-module (crates-io))

(define-public crate-nvpair-rs-0.2.0 (c (n "nvpair-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "stone-libnvpair") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0384cyz4l3yqxvriz8f139q9v5f8v7rw2bxddrxwph2w3vc3fkrr")))

(define-public crate-nvpair-rs-0.3.0 (c (n "nvpair-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "stone-libnvpair") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0haj4s3pvginynl49pl11zygkii72dcnkhr1nk24a1kdbrazn0a7")))

