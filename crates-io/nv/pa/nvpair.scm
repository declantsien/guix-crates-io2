(define-module (crates-io nv pa nvpair) #:use-module (crates-io))

(define-public crate-nvpair-0.1.0 (c (n "nvpair") (v "0.1.0") (d (list (d (n "cstr-argument") (r "^0.0") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)))) (h "0srv20j0c376xas0sxlzl29cr1j318wwmmrz7qaxpfq8r00kyjmv")))

(define-public crate-nvpair-0.2.0 (c (n "nvpair") (v "0.2.0") (d (list (d (n "cstr-argument") (r "^0.0") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)))) (h "0qmvm3g7bks04nhq3y8jwb5p8pw7cvp5z8h12pcg1mm6ayh4skjs")))

(define-public crate-nvpair-0.3.0 (c (n "nvpair") (v "0.3.0") (d (list (d (n "cstr-argument") (r "^0.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)))) (h "15f1nq4qw4lf260f24093zzl4d8gc1h6gbi8fqcnywc43rhbsmvw")))

(define-public crate-nvpair-0.4.0 (c (n "nvpair") (v "0.4.0") (d (list (d (n "cstr-argument") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1d21faf3zqh3mdbpphpwx4hzb4ymh5v28ianwj716fz1gdp7w94f")))

(define-public crate-nvpair-0.5.0 (c (n "nvpair") (v "0.5.0") (d (list (d (n "cstr-argument") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (d #t) (k 0)))) (h "178c182pmv0q3sgk7a844565qzns1y8fmkgvgqsk8p2xhlnz1vkq")))

