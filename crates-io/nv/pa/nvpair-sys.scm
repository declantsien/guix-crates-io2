(define-module (crates-io nv pa nvpair-sys) #:use-module (crates-io))

(define-public crate-nvpair-sys-0.1.0 (c (n "nvpair-sys") (v "0.1.0") (h "1j5ncby70yr2z8q6rxjb37lylylj0q3xscm7lya11nv035swri2z")))

(define-public crate-nvpair-sys-0.4.0 (c (n "nvpair-sys") (v "0.4.0") (h "0nmpdr7m4ii7bdrrf7ml7h7k5v2w8wwzh4c4knf9582h5a22l8l1") (l "nvpair")))

