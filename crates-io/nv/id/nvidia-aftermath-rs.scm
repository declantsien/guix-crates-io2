(define-module (crates-io nv id nvidia-aftermath-rs) #:use-module (crates-io))

(define-public crate-nvidia-aftermath-rs-0.0.1 (c (n "nvidia-aftermath-rs") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0jdb3ac9x9hy4ggz1v0fbi1qdm5y4w4cgjrzbhxd3ajcddg3fdv2")))

(define-public crate-nvidia-aftermath-rs-0.0.2 (c (n "nvidia-aftermath-rs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1jmq0xdnac9s5cvab6b6ix9wr2l8wjplx24p3k24mpc1702z5xg8")))

