(define-module (crates-io nv id nvidia-gamestream-presets-tool) #:use-module (crates-io))

(define-public crate-nvidia-gamestream-presets-tool-0.0.1 (c (n "nvidia-gamestream-presets-tool") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mslnk") (r "^0.1.8") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "steam_shortcuts_util") (r "^1.1.8") (d #t) (k 0)))) (h "07d434k5cc9ykkv7pjlwvzgn3s52zc9vxf89m1bjk0sbyy0xkp3r")))

(define-public crate-nvidia-gamestream-presets-tool-0.0.2 (c (n "nvidia-gamestream-presets-tool") (v "0.0.2") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mslnk") (r "^0.1.8") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "steam_shortcuts_util") (r "^1.1.8") (d #t) (k 0)))) (h "0cijbgp9r4gia9ss386i5rxsnw7xm6ddvj3p3bby85krj97q3ig6")))

