(define-module (crates-io nv tx nvtx) #:use-module (crates-io))

(define-public crate-nvtx-1.1.0 (c (n "nvtx") (v "1.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1s7cr006n0jxg9k486x8caz3qz1zzz57bmfrjx818d8sw2fh58m7")))

(define-public crate-nvtx-1.1.1 (c (n "nvtx") (v "1.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1iryx0xxr0qnyn2v7yh75s1sf5c27qqywrgh3daffj3kn2cc739m")))

(define-public crate-nvtx-1.2.0 (c (n "nvtx") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q8j8fy26n6m5fm2jcag72gc19lpys30z33sqqdy149mm42s3nfz")))

(define-public crate-nvtx-1.3.0 (c (n "nvtx") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0b7586w86zrc1fd79ws4w2i71xg4nh76fcxcji5rxy8rh1g8abmd")))

