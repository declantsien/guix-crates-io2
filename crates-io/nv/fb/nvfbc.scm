(define-module (crates-io nv fb nvfbc) #:use-module (crates-io))

(define-public crate-nvfbc-0.1.0 (c (n "nvfbc") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "1bgfrj4z4v46hp6k96rmxj092ww7wgii96w2gr9l5kdp1ssjixx0")))

(define-public crate-nvfbc-0.1.1 (c (n "nvfbc") (v "0.1.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "0dw7bj7k5n07yrk6rk99ngnhgmjjaswpj1pgy3s6bkgcs7kv2c6q")))

(define-public crate-nvfbc-0.1.2 (c (n "nvfbc") (v "0.1.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "0z9ynr2m1iw39r860yxq3wmbw5mhnagh02xj0ajd3aw7fd0igqxb")))

(define-public crate-nvfbc-0.1.3 (c (n "nvfbc") (v "0.1.3") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "0gcabajg0pffq6hvmnf3pybw5ac6s8fk30gk15myr00lbpbabb6h")))

(define-public crate-nvfbc-0.1.4 (c (n "nvfbc") (v "0.1.4") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "1qqc749scy2ib9ssm0vlpzk4849snz3ap6zv5gd4dsqda4njqixw")))

(define-public crate-nvfbc-0.1.5 (c (n "nvfbc") (v "0.1.5") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "nvfbc-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 2)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 2)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 2)))) (h "1hpc0dw74r94pjmai29vmnz8y6q7j60mj0zc6ah08nf5ak2vafdk")))

