(define-module (crates-io nv fb nvfbc-sys) #:use-module (crates-io))

(define-public crate-nvfbc-sys-0.1.0 (c (n "nvfbc-sys") (v "0.1.0") (h "1aw9vd6laasyx6fxq4shbv68xiskdjk3vlva8r31485av4qlxgfh")))

(define-public crate-nvfbc-sys-0.1.3 (c (n "nvfbc-sys") (v "0.1.3") (h "1r9dyjgd1i65fj3sb1d4g5d2pa5xcj4lqndpm79hj3hhzl8i4lp3")))

(define-public crate-nvfbc-sys-0.1.4 (c (n "nvfbc-sys") (v "0.1.4") (h "17hlrs807x3l5a6gmhyc9lw2ay6qf3sl36g4i1s23wbbk2hvzyc6")))

(define-public crate-nvfbc-sys-0.1.5 (c (n "nvfbc-sys") (v "0.1.5") (h "0nsdy78xliqyq0b8j09a48naxpgdcyxcknl8l83qzk4ahhlw64hk")))

