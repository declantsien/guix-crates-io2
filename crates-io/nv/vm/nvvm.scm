(define-module (crates-io nv vm nvvm) #:use-module (crates-io))

(define-public crate-nvvm-0.1.0 (c (n "nvvm") (v "0.1.0") (d (list (d (n "find_cuda_helper") (r "^0.1") (d #t) (k 1)))) (h "0x126f9r895hmmvjqbdm02fs49s5kzk85p2zf44ycghdqrz3ip2h")))

(define-public crate-nvvm-0.1.1 (c (n "nvvm") (v "0.1.1") (d (list (d (n "find_cuda_helper") (r "^0.2") (d #t) (k 1)))) (h "120h6w7kf9qwbi4r47acvh1pfiln6x0j537z8s9mg4m0nyjy8qr8")))

