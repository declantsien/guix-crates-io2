(define-module (crates-io nv -x nv-xml) #:use-module (crates-io))

(define-public crate-nv-xml-0.1.0 (c (n "nv-xml") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "0f8cm8qlrid2sngw1clhxrmsz3zx1qiad1i5i693rz731lxkzhgc")))

(define-public crate-nv-xml-0.1.1 (c (n "nv-xml") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "04b9qpj8hxzs3vncdy3j8v2fjm2jv84kdy7j4505743m4wvmivkc")))

(define-public crate-nv-xml-0.1.2 (c (n "nv-xml") (v "0.1.2") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "1rs9x4vd1hb1awhbq77fn06hxiz9afr3pq6cz03c9mys694vpgmq")))

(define-public crate-nv-xml-0.1.3 (c (n "nv-xml") (v "0.1.3") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "0zixvgyk33cn3ikb9k53mhwllaa1601220c06x6kp5dhpak3yp98")))

