(define-module (crates-io nv -f nv-flip) #:use-module (crates-io))

(define-public crate-nv-flip-0.1.0 (c (n "nv-flip") (v "0.1.0") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "nv-flip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0j7gq818rmpxdf13nm7bkx6c2rlf6ncn1v08ymw9izkmp4xb44i8")))

(define-public crate-nv-flip-0.1.1 (c (n "nv-flip") (v "0.1.1") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "nv-flip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1glm1pfhvyfjs6bycgb06pidg7l7rg6qsadg2s045bw7bhnbazgq")))

(define-public crate-nv-flip-0.1.2 (c (n "nv-flip") (v "0.1.2") (d (list (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "nv-flip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x0r3bgmciqs5amrj8dn6hmdhciqhnshx78xvrq04y96qa5cdhsf")))

