(define-module (crates-io nv -f nv-flip-sys) #:use-module (crates-io))

(define-public crate-nv-flip-sys-0.1.0 (c (n "nv-flip-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)))) (h "10z1dfpsz2dx3zhdifac5hhwbj8vjgbmf903pcl4gx813b70rqbp") (l "nv-flip")))

(define-public crate-nv-flip-sys-0.1.1 (c (n "nv-flip-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "float_eq") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)))) (h "14y9q7hx9i49978qps1ygih02cxychxsnvdcrh7sfaq083niwblk") (l "nv-flip")))

