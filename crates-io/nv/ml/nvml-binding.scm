(define-module (crates-io nv ml nvml-binding) #:use-module (crates-io))

(define-public crate-nvml-binding-0.1.0 (c (n "nvml-binding") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14wkaygd992lnlpw3fr1r2k8n04n4jxy0pxvrjc1ical6zdbpxxf")))

