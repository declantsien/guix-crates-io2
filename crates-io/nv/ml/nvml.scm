(define-module (crates-io nv ml nvml) #:use-module (crates-io))

(define-public crate-nvml-0.0.1 (c (n "nvml") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nvml-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.12") (d #t) (k 0)))) (h "0080k1vk1r0j1qjz9m7pricqd020v71r4lry6sra4yq00jald85z")))

(define-public crate-nvml-0.0.2 (c (n "nvml") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nvml-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.12") (d #t) (k 0)))) (h "1732wp8x96sjl82dkl35rr4ldlf259vab1hmcg76mj63bf53111y")))

