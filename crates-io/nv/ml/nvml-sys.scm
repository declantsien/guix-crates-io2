(define-module (crates-io nv ml nvml-sys) #:use-module (crates-io))

(define-public crate-nvml-sys-0.0.1 (c (n "nvml-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_c") (r "^0.1.0") (f (quote ("build"))) (d #t) (k 1)))) (h "19dbm207miz79rx2938vy1mm14hvrc19fmqaqd36yxfswk6i78m7")))

(define-public crate-nvml-sys-0.0.2 (c (n "nvml-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vgfd0gcrkxvcr3kw0mslf79k3i2d6qnscyqqv6rrq4wk50z1rgl")))

(define-public crate-nvml-sys-0.0.3 (c (n "nvml-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ifll6bx2ghbd9ikrmvfflb9950mkwdkrg49dqk6kv3sgx8sff8d")))

(define-public crate-nvml-sys-0.0.4 (c (n "nvml-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05afg3axy0k0rv6vhsv11vwa5rj8lh9pcl3vnpm8ada9krqjrhmv")))

(define-public crate-nvml-sys-0.0.5 (c (n "nvml-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f58gw0n1qdv3idqni07ybzi466ws9cj4im4p0rwzxqi4rmq0r8s")))

(define-public crate-nvml-sys-0.0.6 (c (n "nvml-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yscqwpibixhv2kd1dibkh266niihq6ayh8gr027dp6nj2s98m3g")))

