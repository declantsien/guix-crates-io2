(define-module (crates-io nv is nvis) #:use-module (crates-io))

(define-public crate-nvis-0.1.0 (c (n "nvis") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "0qx321vsm7nczlbhh07q5x1vp6i97j4mb3mzax889a169gzr0c4g")))

(define-public crate-nvis-0.1.1 (c (n "nvis") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "1z3arlwshn9avnr8z93d36iqx09vijci962b0l391p3v3bsafqac")))

(define-public crate-nvis-0.1.2 (c (n "nvis") (v "0.1.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "cursive") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "0644yfdd7cwvblglnfdcjn9ywnl9i2hnpfmjivpbdc9rcxlgh909")))

