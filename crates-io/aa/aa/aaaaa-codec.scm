(define-module (crates-io aa aa aaaaa-codec) #:use-module (crates-io))

(define-public crate-aaaaa-codec-3.2.0 (c (n "aaaaa-codec") (v "3.2.0") (d (list (d (n "aaaaa-codec-derive") (r "^3.1") (o #t) (k 0)) (d (n "aaaaa-codec-derive") (r "^3.1") (k 2)) (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pcx2nfp6gqdw3l2nd13hdmwcs3sgpnds90nq5za0m8j0znda6s4") (f (quote (("std" "serde") ("full") ("derive" "aaaaa-codec-derive") ("default" "std")))) (y #t)))

