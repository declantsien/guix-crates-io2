(define-module (crates-io aa aa aaaaa-codec-derive) #:use-module (crates-io))

(define-public crate-aaaaa-codec-derive-3.1.0 (c (n "aaaaa-codec-derive") (v "3.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0aw876m6y0a20xwjvxa0q435j3dj29rp8xc8w0ffg6jsvsfz467h") (f (quote (("std") ("default" "std")))) (y #t)))

