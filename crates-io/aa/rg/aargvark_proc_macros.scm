(define-module (crates-io aa rg aargvark_proc_macros) #:use-module (crates-io))

(define-public crate-aargvark_proc_macros-0.0.1 (c (n "aargvark_proc_macros") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "06myxi69k2i7crhqfrslpw1g0m5d8hy8aqclg0jgnl42dad6w0br")))

(define-public crate-aargvark_proc_macros-0.0.2 (c (n "aargvark_proc_macros") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1cf09pvr91l1fbvzyp9i0vd231vjg3prnmnr0csk26l4gfv7wfiz")))

(define-public crate-aargvark_proc_macros-0.0.3 (c (n "aargvark_proc_macros") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0ciq18hhfvxnd8ppvrd2jlyagkrmwq694m4cgg8832h1z7wyb3ck")))

(define-public crate-aargvark_proc_macros-0.0.4 (c (n "aargvark_proc_macros") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "genemichaels") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1113xfcfa25g0yk0r50gyi5hlbpipj3kdgbikipqdx45xx7iin06")))

(define-public crate-aargvark_proc_macros-0.0.5 (c (n "aargvark_proc_macros") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "genemichaels") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "17fl22kxac3pm01v3b943348y069vhfd30ql4w7mmf6d5wk7fd3d")))

(define-public crate-aargvark_proc_macros-0.0.6 (c (n "aargvark_proc_macros") (v "0.0.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "genemichaels") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1i1bqza4v74i8dvcryyl1p7vvn46ba1lcgs90h4r56cyhdrsv62s")))

(define-public crate-aargvark_proc_macros-0.0.7 (c (n "aargvark_proc_macros") (v "0.0.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "genemichaels") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1wvcrsq0pns3skrbn706kfcr9lmi5da1wdl0kj0szi5cfx5yll6s")))

