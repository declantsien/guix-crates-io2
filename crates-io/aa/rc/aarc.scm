(define-module (crates-io aa rc aarc) #:use-module (crates-io))

(define-public crate-aarc-0.0.1 (c (n "aarc") (v "0.0.1") (h "1ihxciqs5zg64hc7bl5ylb0zgqyn2i3g5nfl6fipzwy846db8cga") (y #t)))

(define-public crate-aarc-0.1.0 (c (n "aarc") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1inris25fjbpxn0qx0jilj2vfhq5kc76disl454bm3180lm9daqc")))

(define-public crate-aarc-0.2.0 (c (n "aarc") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1yy9pxk2cvjn8zbymrwk4qdgbfcmrrwim2lbgqzbld2v60hp2wld")))

(define-public crate-aarc-0.2.1 (c (n "aarc") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1nq10712w1srs0vx3ga1gkyg6fis3ql459r5pdh9qcnl8v9zxq5g")))

