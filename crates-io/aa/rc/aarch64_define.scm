(define-module (crates-io aa rc aarch64_define) #:use-module (crates-io))

(define-public crate-aarch64_define-0.1.0 (c (n "aarch64_define") (v "0.1.0") (h "0lwimi6dm1414appsnwc2ij6xbzdpwvw8g9jlm6iph171i4z8izg") (r "1.78")))

(define-public crate-aarch64_define-0.1.1 (c (n "aarch64_define") (v "0.1.1") (h "0nc0p85lxrd1xbvbffwrrdsgjy645w5z8vbhrkijx44jra2h2hch") (r "1.78")))

