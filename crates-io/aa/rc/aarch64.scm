(define-module (crates-io aa rc aarch64) #:use-module (crates-io))

(define-public crate-aarch64-0.0.1 (c (n "aarch64") (v "0.0.1") (h "1c9q5xz8sp7dyls6j740mih3hhsknh6s8maaq5ga5zyzlkvzcxcf") (y #t)))

(define-public crate-aarch64-0.0.2 (c (n "aarch64") (v "0.0.2") (h "1hk68i0q7yr71j99snsys24r0v2cij8xnsnzi4xjkqr1kzlfqfbb") (y #t)))

(define-public crate-aarch64-0.0.3 (c (n "aarch64") (v "0.0.3") (h "0wffg5vha9268iznjj9b3d1z7aahk3qwkm6223yp6km6wlh677vl")))

(define-public crate-aarch64-0.0.4 (c (n "aarch64") (v "0.0.4") (d (list (d (n "cortex-a") (r "^3.0.4") (d #t) (k 0)))) (h "01lndkv4cz5c34h4dbfrrgsdd4llm0r7zif5yf9pbnibqr6ahnag")))

(define-public crate-aarch64-0.0.5 (c (n "aarch64") (v "0.0.5") (d (list (d (n "cortex-a") (r "^5.0") (d #t) (k 0)))) (h "0xyyk99jlrq40zkzg17153d7a30r9g1y1z6i1xrmzz0bawm3aw32")))

(define-public crate-aarch64-0.0.6 (c (n "aarch64") (v "0.0.6") (d (list (d (n "cortex-a") (r "^6.1") (d #t) (k 0)))) (h "1jv40z7hwasfxfpwl23jw0v42z2chyh3brm9gf0fap6zb8x9rdag")))

(define-public crate-aarch64-0.0.7 (c (n "aarch64") (v "0.0.7") (d (list (d (n "cortex-a") (r "^7") (d #t) (k 0)))) (h "1cvsazqp27s5hdr2ii1ybr1y84wr9z7rpcw1l1n4zr4wi4qxyj4x")))

(define-public crate-aarch64-0.0.9 (c (n "aarch64") (v "0.0.9") (d (list (d (n "cortex-a") (r "^8.1") (d #t) (k 0)))) (h "0gzr0xyvdk3hi1r67cvrs9svli2307dhflmg0gmbizp35yvhs4mg")))

(define-public crate-aarch64-0.0.10 (c (n "aarch64") (v "0.0.10") (d (list (d (n "aarch64-cpu") (r "^9.3") (d #t) (k 0)) (d (n "tock-registers") (r "0.8.*") (k 0)))) (h "1brv0z478pxm5ymxgfczpq5639zb0xwqv91wr40s25ddkdpkd1dw")))

(define-public crate-aarch64-0.0.11 (c (n "aarch64") (v "0.0.11") (d (list (d (n "aarch64-cpu") (r "^9.3") (d #t) (k 0)) (d (n "tock-registers") (r "0.8.*") (k 0)))) (h "1yhvp5dd618k5pvcqmaz35s9z2pfjd4hknqicl0n2a2fidfk9pqa") (f (quote (("nightly") ("default" "nightly"))))))

