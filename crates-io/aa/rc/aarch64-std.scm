(define-module (crates-io aa rc aarch64-std) #:use-module (crates-io))

(define-public crate-aarch64-std-0.1.0 (c (n "aarch64-std") (v "0.1.0") (d (list (d (n "aarch64-cpu") (r "^9.2.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.0") (d #t) (k 0)))) (h "1y4c2mqj31zincmpz43llfxs8inb2wky0816za6mkpg466g6fill") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-std-0.1.1 (c (n "aarch64-std") (v "0.1.1") (d (list (d (n "aarch64-cpu") (r "^9.2.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.0") (d #t) (k 0)))) (h "04sdvirly7z8jzzhw2p22lfa36f2lm1am5805mhvp766lh7lf8qk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-std-0.1.2 (c (n "aarch64-std") (v "0.1.2") (d (list (d (n "aarch64-cpu") (r "^9.2.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.0") (d #t) (k 0)))) (h "1253xig1fmxwpvjsm74m5hbi1nii3xqir1xikvmbi7c1xf8cs7a6") (f (quote (("default" "alloc") ("alloc"))))))

