(define-module (crates-io aa rc aarch64-esr-decoder) #:use-module (crates-io))

(define-public crate-aarch64-esr-decoder-0.1.0 (c (n "aarch64-esr-decoder") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "050j1cm2zmgh5mknsdcdq6ikmw8ja2avkv9kvvnbja8zgn6fw702")))

(define-public crate-aarch64-esr-decoder-0.2.0 (c (n "aarch64-esr-decoder") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m3lk5cyhg0zcqkpjvy0zsrra6nj67hx2y223i5h59dcxc24rwyh")))

(define-public crate-aarch64-esr-decoder-0.2.1 (c (n "aarch64-esr-decoder") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03b2m41z3gl1lz907x3f3vbxq99j50ygg501488in1if09mcagay")))

(define-public crate-aarch64-esr-decoder-0.2.2 (c (n "aarch64-esr-decoder") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1bv1qkxd3gjsbb3ia7hhncwlv46k6ifn321200br9h1jppr10fwk")))

(define-public crate-aarch64-esr-decoder-0.2.3 (c (n "aarch64-esr-decoder") (v "0.2.3") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "191kzxbwxk81rdbf6c32sll0j7vg51ivfmkb6hz8f1l7bypr0g9r")))

