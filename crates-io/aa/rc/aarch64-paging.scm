(define-module (crates-io aa rc aarch64-paging) #:use-module (crates-io))

(define-public crate-aarch64-paging-0.1.0 (c (n "aarch64-paging") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "13in4kp8a8700b369v70p8b36zp0hh50cbsxm29cib7w8im7jj9v")))

(define-public crate-aarch64-paging-0.2.0 (c (n "aarch64-paging") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "17glgmqwlj3nb991r3z2rn67qwv6qysy7ggbrnjrxxa5hny72wjm")))

(define-public crate-aarch64-paging-0.2.1 (c (n "aarch64-paging") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "073ql5qcp5gyd8saf0k5zhayzxrcrf16cvp43qrxzibqyli02kzh")))

(define-public crate-aarch64-paging-0.3.0 (c (n "aarch64-paging") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "10k2838rfim7082k8bbixrdj4y05px5jdssb4n8871y2zp88jzpb") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.4.0 (c (n "aarch64-paging") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "1ln1ff5y1xllqjkrzr2zkhxx2wiz5892yjx6cm3cxbrc45z7cci8") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.4.1 (c (n "aarch64-paging") (v "0.4.1") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "16fk7pan3az55w0a0pw0jq23rdr0iblvxyfl1a5r4mkzy7wzkl6j") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.5.0 (c (n "aarch64-paging") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "0qdghln29w8n3x5svz1ljbzxjwa55phf4nixy7n68ah5a95gk5b2") (f (quote (("default" "alloc") ("alloc"))))))

