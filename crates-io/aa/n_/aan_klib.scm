(define-module (crates-io aa n_ aan_klib) #:use-module (crates-io))

(define-public crate-aan_klib-0.1.0 (c (n "aan_klib") (v "0.1.0") (h "190zv3zhy2jsig1sm7v0mx7w6lwm040fa22wpan9f6xzwkvjai56")))

(define-public crate-aan_klib-0.1.1 (c (n "aan_klib") (v "0.1.1") (h "1y0mv7d0zawzl7ldqmjwwjxxa8bpw1bxcjhzvs8zzkkf0fa08h20")))

(define-public crate-aan_klib-0.1.2 (c (n "aan_klib") (v "0.1.2") (h "044822p6bw4slpn272bvq61933fa5dvpgvdja6q4ixin45wr1zzv")))

