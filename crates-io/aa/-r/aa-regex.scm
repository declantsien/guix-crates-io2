(define-module (crates-io aa -r aa-regex) #:use-module (crates-io))

(define-public crate-aa-regex-0.2.1 (c (n "aa-regex") (v "0.2.1") (d (list (d (n "displaydoc") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0v4wqqypw9yki55zdv0w17cn7sq8cl19kh15ssb3gz4wry1bjgzg") (y #t)))

(define-public crate-aa-regex-0.2.2 (c (n "aa-regex") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10x6f2lq87jjipi6nj4yaq84anz4iva62jc8ac3nb95h7f3ra4ba") (y #t)))

(define-public crate-aa-regex-0.2.3 (c (n "aa-regex") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05ach4ij0id4y8hf9gbvss8cdgw2ls8z7rhmqvzx1zwmmjsqgky6") (y #t)))

(define-public crate-aa-regex-0.2.4 (c (n "aa-regex") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12rrip9cigic1x2li76k9s0j99zib0yr3fcclqzq2p4953k8hbgh")))

(define-public crate-aa-regex-0.2.5 (c (n "aa-regex") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08lsz1f4r97m1y6iyrgs9sra2dmscdwa47i4nm4y9as91kpjp41w")))

(define-public crate-aa-regex-0.3.0 (c (n "aa-regex") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fz0h07q9bgik9b9q0ask20rpvafpjz6xdimmn0r4cd4r6dv7xfc")))

(define-public crate-aa-regex-0.3.1 (c (n "aa-regex") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nrajrqssn27sjv7l6xg7snv4kx5sx0f82fh43456agyrwm8s22h")))

