(define-module (crates-io aa bb aabb-quadtree) #:use-module (crates-io))

(define-public crate-aabb-quadtree-0.1.0 (c (n "aabb-quadtree") (v "0.1.0") (d (list (d (n "fnv") (r "1.*.*") (d #t) (k 0)))) (h "1jlnrjmfh3ka4rbnwnm940vias56wgzpbg7zpns5v272v9137rq8")))

(define-public crate-aabb-quadtree-0.2.0 (c (n "aabb-quadtree") (v "0.2.0") (d (list (d (n "euclid") (r "0.19.*") (d #t) (k 0)) (d (n "fnv") (r "1.*.*") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)))) (h "08ddm7c3xvzbz57r3dyi0qgbgfzl8bh09l6kik7mn5v5n5l6jlxr")))

