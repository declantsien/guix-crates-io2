(define-module (crates-io aa bb aabb3) #:use-module (crates-io))

(define-public crate-aabb3-0.1.0 (c (n "aabb3") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.1") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "13zmlp2qkbh7ggxnmpn5874w2nhcn0m9fnxrkqxn8b2yydnvfczx")))

(define-public crate-aabb3-0.1.1 (c (n "aabb3") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "1lcrw9arxxz9d633z958k97zqjzr5xjyyrqvznzdcfjn794dvv32")))

(define-public crate-aabb3-0.1.2 (c (n "aabb3") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.1") (d #t) (k 0)))) (h "0aga0zsgnad6hm0vs92i3l2g58jfl49aky8s3j5hapvvlbh49vga")))

(define-public crate-aabb3-0.2.0 (c (n "aabb3") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "1k0jrxj8xdc6yy18s4wdvf08qbnd0cjbppll305d4qr9qkd3krkl")))

(define-public crate-aabb3-0.2.1 (c (n "aabb3") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)))) (h "0s7y85cqk4kv327sdch0ls50zd9562d0938lw832shdhmx95ml77")))

