(define-module (crates-io aa be aabel-multihash-rs) #:use-module (crates-io))

(define-public crate-aabel-multihash-rs-0.1.0 (c (n "aabel-multihash-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "0d6qwlh1lgz1npfjaqwid98q5wxh3nhhil7mqir2n3jl5kh3ip05")))

(define-public crate-aabel-multihash-rs-0.1.1 (c (n "aabel-multihash-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "11wvvgcaac302mfj2zc2and99pc8q3pbdrkh6y82y7vyyxz8q3gg")))

(define-public crate-aabel-multihash-rs-0.1.2 (c (n "aabel-multihash-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "0k0wklv301wi8avck0x1vy3jsv3r4lzjfj02cyrms3slyjvv9bm2")))

