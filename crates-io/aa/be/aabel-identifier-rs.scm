(define-module (crates-io aa be aabel-identifier-rs) #:use-module (crates-io))

(define-public crate-aabel-identifier-rs-0.1.0 (c (n "aabel-identifier-rs") (v "0.1.0") (h "0kqd01iishdx1ffixng5rbxf58nb7czygnk80520ivfx63wz06fb")))

(define-public crate-aabel-identifier-rs-0.1.1 (c (n "aabel-identifier-rs") (v "0.1.1") (h "1b0hapygc3al7dbmhpgbd47zq4cfh7jj02dy5kb2r8w47dcq6v4y")))

(define-public crate-aabel-identifier-rs-0.1.2 (c (n "aabel-identifier-rs") (v "0.1.2") (h "176qdk3pxadc4vs1g3d95303d79vc0xlc5xgx27r3hfdlgn35c98")))

