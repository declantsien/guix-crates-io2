(define-module (crates-io aa be aabel-bloom-rs) #:use-module (crates-io))

(define-public crate-aabel-bloom-rs-0.1.0 (c (n "aabel-bloom-rs") (v "0.1.0") (d (list (d (n "aabel-multihash-rs") (r "^0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)))) (h "0mw1dpfqx51zj7shhcdway19sbvvm379hhjjf93a01za7izgijym")))

(define-public crate-aabel-bloom-rs-0.1.1 (c (n "aabel-bloom-rs") (v "0.1.1") (d (list (d (n "aabel-multihash-rs") (r "^0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)))) (h "0rbh4bhap6l82x2wmkgsi8x6qma5k86p6ww6k0inq7ygzva5hzbr")))

