(define-module (crates-io aa rd aardvark) #:use-module (crates-io))

(define-public crate-aardvark-0.1.0-alpha (c (n "aardvark") (v "0.1.0-alpha") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kzijy1dxc8av94pmajraqy0pphpqac8jwf7k8f6mpdbf82cbxlk") (y #t)))

(define-public crate-aardvark-0.0.0 (c (n "aardvark") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0krfqdd5ycls94cw3dlylagl44s9nbs8h8834b6cyfnqikqmab03")))

