(define-module (crates-io aa a_ aaa_csv_challenge) #:use-module (crates-io))

(define-public crate-aaa_csv_challenge-0.1.0 (c (n "aaa_csv_challenge") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4.16") (d #t) (k 0)))) (h "1bld5k30ms9vch0gc21rckk4ix7r8a5yhb52kxc0f43pai1p0apm")))

