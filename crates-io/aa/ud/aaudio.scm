(define-module (crates-io aa ud aaudio) #:use-module (crates-io))

(define-public crate-aaudio-0.1.0 (c (n "aaudio") (v "0.1.0") (d (list (d (n "aaudio-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kvjgq3b91x3138382jwp4c9w2jqffsdbz7k8ppkqx4x6whlybbk") (y #t)))

(define-public crate-aaudio-0.1.1 (c (n "aaudio") (v "0.1.1") (d (list (d (n "aaudio-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a480scwfvjfm9kk2nyq7hs9z5k67i1m787bxyrf609kask7nvkl")))

