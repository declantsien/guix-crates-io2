(define-module (crates-io aa d- aad-join-info) #:use-module (crates-io))

(define-public crate-aad-join-info-0.1.0 (c (n "aad-join-info") (v "0.1.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Security_Authentication_Identity" "Win32_Security_Authorization" "Win32_System_Memory" "Win32_System_SystemInformation" "Win32_NetworkManagement_NetManagement" "Win32_Security_Cryptography"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "075qv36iswsa5l7dv5rkpnqhmzh79jag2hl93lw2ac94np9z4md0")))

(define-public crate-aad-join-info-0.1.1 (c (n "aad-join-info") (v "0.1.1") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Security_Authentication_Identity" "Win32_Security_Authorization" "Win32_System_Memory" "Win32_System_SystemInformation" "Win32_NetworkManagement_NetManagement" "Win32_Security_Cryptography"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0n3a8mzj2llgn4p2gbw8na7j3j7rx34b0b8nh07wrnnkm2qzrlbs")))

(define-public crate-aad-join-info-0.1.2 (c (n "aad-join-info") (v "0.1.2") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Security_Authentication_Identity" "Win32_Security_Authorization" "Win32_System_Memory" "Win32_System_SystemInformation" "Win32_NetworkManagement_NetManagement" "Win32_Security_Cryptography"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0iln9yz7i5farx5g73bzqg893np7jmjl0b623r2nqxym1yxd3825")))

