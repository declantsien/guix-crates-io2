(define-module (crates-io aa tr aatree) #:use-module (crates-io))

(define-public crate-aatree-0.1.0 (c (n "aatree") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "06pdkkr11qnfssv9i74rp93fi33j1yrfn54knwrfm682ybnpcp90") (f (quote (("default" "log")))) (r "1.50")))

(define-public crate-aatree-0.1.1 (c (n "aatree") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1p3py9ap4dyjhix9cs73d68ny5gnpmbjrx6b23b73248clw4r4zs") (f (quote (("default" "log")))) (r "1.56")))

(define-public crate-aatree-0.1.2 (c (n "aatree") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "0g2xz71qq1xawwcjk1nk7kfmy0liihxalchkfxk275lgicy9gg2q") (f (quote (("default" "log")))) (r "1.56")))

(define-public crate-aatree-0.2.0 (c (n "aatree") (v "0.2.0") (h "00jzk03p1ravx4d3zz8vr29y9nv4wbmdqix3wv9gn9fian1arvvd") (r "1.56")))

(define-public crate-aatree-0.2.1 (c (n "aatree") (v "0.2.1") (h "14p5al5nlivx4vpg377i5acwc3v248vz9568923r1h5h9d0gby3q") (r "1.56")))

(define-public crate-aatree-0.2.2 (c (n "aatree") (v "0.2.2") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "openapi_type") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0xsk13fb0flph9icg0i2pbdwzpd1d4kfsdhpxj3z6if7262cpsax") (s 2) (e (quote (("serde" "dep:serde") ("openapi" "dep:openapi_type")))) (r "1.60")))

