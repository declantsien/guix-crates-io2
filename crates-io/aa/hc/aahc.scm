(define-module (crates-io aa hc aahc) #:use-module (crates-io))

(define-public crate-aahc-0.1.0 (c (n "aahc") (v "0.1.0") (d (list (d (n "async-compat") (r "^0.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "io-util" "rt-core" "tcp"))) (d #t) (k 2)))) (h "095pcsb328d92icjz2a66fiwv48zr9i15ix437mk839gk07ncqfj") (f (quote (("detailed-errors") ("default" "detailed-errors"))))))

