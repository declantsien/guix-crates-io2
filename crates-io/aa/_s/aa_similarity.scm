(define-module (crates-io aa _s aa_similarity) #:use-module (crates-io))

(define-public crate-aa_similarity-0.1.0 (c (n "aa_similarity") (v "0.1.0") (d (list (d (n "aa-name") (r "^0.1.4") (d #t) (k 0)) (d (n "aa-name") (r "^0.1.4") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "nom") (r "^6.0") (d #t) (k 1)))) (h "1j8gkdysf2f5g7b37z76lad4xinr7pnbgnbac4hcahlgvd7ip6nw")))

