(define-module (crates-io aa ro aaronia-rtsa) #:use-module (crates-io))

(define-public crate-aaronia-rtsa-0.0.1 (c (n "aaronia-rtsa") (v "0.0.1") (d (list (d (n "aaronia-rtsa-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0pz4ixjfikpn4vfx2rw2vcfzwdwpg8q7scvlyvb7pqmb48064401")))

(define-public crate-aaronia-rtsa-0.0.3 (c (n "aaronia-rtsa") (v "0.0.3") (d (list (d (n "aaronia-rtsa-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1s90aqamrcy3dnz267z5w2mn62j1gf7jqs4vc33nnwjd8ajs4lvb")))

(define-public crate-aaronia-rtsa-0.0.4 (c (n "aaronia-rtsa") (v "0.0.4") (d (list (d (n "aaronia-rtsa-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0rrdkwadgx648ickk6nnhkxqjvzdpm7gbzrwca5nbqb3wn5rw1nq")))

(define-public crate-aaronia-rtsa-0.0.5 (c (n "aaronia-rtsa") (v "0.0.5") (d (list (d (n "aaronia-rtsa-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0d41izqgwrgy4xbwzzhp0k5hqa8jj1a12fnjvkdwh73kx1w2rdfp")))

(define-public crate-aaronia-rtsa-0.0.6 (c (n "aaronia-rtsa") (v "0.0.6") (d (list (d (n "aaronia-rtsa-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 2)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 2)))) (h "03jfxmzmffwdhw0z7qgdgi5anzfjz0jpwpw3c9dbnkm9zkqp4i6p")))

