(define-module (crates-io aa ro aaron_art) #:use-module (crates-io))

(define-public crate-aaron_art-0.1.0 (c (n "aaron_art") (v "0.1.0") (h "0z54hd0k82yff7pwbyhx6d2a3mj80333vwd5jxxczyzbcfnl4nqg") (y #t)))

(define-public crate-aaron_art-0.1.1 (c (n "aaron_art") (v "0.1.1") (h "0z55jblbg5qvklwp5wchxpi2xpc9jb5xdy7lv7vs704zx24z5z8f")))

