(define-module (crates-io aa ro aaronia-rtsa-sys) #:use-module (crates-io))

(define-public crate-aaronia-rtsa-sys-0.0.1 (c (n "aaronia-rtsa-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q7936dv4q6gaxa1ik4gppapp55wf53z97h2fg370vdapk0agsml") (l "AaroniaRTSAAPI")))

(define-public crate-aaronia-rtsa-sys-0.0.3 (c (n "aaronia-rtsa-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "0b3457qbzjh8k4xvz8zyslqr5q46kpmhmbjyslypx4yfn2vgi9kv") (l "AaroniaRTSAAPI")))

(define-public crate-aaronia-rtsa-sys-0.0.4 (c (n "aaronia-rtsa-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "14d1nw052nbkj2pkqk6szdanpdkxqk0vqdrfyyg2lbx61ax2mc4h") (l "AaroniaRTSAAPI")))

