(define-module (crates-io aa rt aart) #:use-module (crates-io))

(define-public crate-aart-0.1.0 (c (n "aart") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.7") (d #t) (k 0)))) (h "026hlczf57a1lswb3pb041y70khif3qwdgx8j4gaifpxpixpgc2v")))

(define-public crate-aart-0.1.1 (c (n "aart") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.7") (d #t) (k 0)))) (h "1wxv9dwl3yqn8xmiaci6mp12h7mhx5wy053jxrjxf1pg90vv6prv")))

