(define-module (crates-io mh _v mh_very_own_guessing_game) #:use-module (crates-io))

(define-public crate-mh_very_own_guessing_game-0.1.0 (c (n "mh_very_own_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bgg6kyaphzcx3afqg3ldj2sc17qgwhpk3r8jz8cqhknisvx625p") (y #t)))

(define-public crate-mh_very_own_guessing_game-0.1.1 (c (n "mh_very_own_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h0rcqyrls84spdnm2z3l37gd7kxjdyhpvlc954qmkk7hcj7c35d")))

