(define-module (crates-io mh -z mh-z19c) #:use-module (crates-io))

(define-public crate-mh-z19c-0.1.0 (c (n "mh-z19c") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1zy641ia6dmzb19phlhdw0mjlj0qy7phaapw3v8cwnmgq6jqiygb")))

(define-public crate-mh-z19c-0.1.1 (c (n "mh-z19c") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0f7js7n4vgfvgfhgwlsqp6dmyg40flqqpxzjh046848rhha1q7s6")))

(define-public crate-mh-z19c-0.2.0 (c (n "mh-z19c") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "17i5ax8bfl6ncp1szj3lmabdvccsjaqrxr0x0ghb50izpl68dvnc") (f (quote (("std"))))))

(define-public crate-mh-z19c-0.3.0 (c (n "mh-z19c") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "11061lrqf2vw4mc4p9s0328bf466rkm3mhl273fkzl4cjxdlgs91") (f (quote (("std"))))))

