(define-module (crates-io mh -z mh-z19) #:use-module (crates-io))

(define-public crate-mh-z19-0.1.0 (c (n "mh-z19") (v "0.1.0") (h "1njrlh24y9ql7isih7f05gy3k3k32xjbdypi25cs0c88cv3r3by9")))

(define-public crate-mh-z19-0.2.0 (c (n "mh-z19") (v "0.2.0") (h "1lra0bn7s9z038hxpa4104h5ydmwsa5lg05c18mnzpbnc60miyq3")))

(define-public crate-mh-z19-0.2.1 (c (n "mh-z19") (v "0.2.1") (h "04inwgk7npkra0lj5lsj8vdwsmrpjikqgf4lf62lnrffya8f4fvj")))

(define-public crate-mh-z19-0.3.0 (c (n "mh-z19") (v "0.3.0") (h "1324dh9vmmd6rjgrlf1fbh97hlq0ckw7m9mr0smbcikljshxgm6z") (f (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3.1 (c (n "mh-z19") (v "0.3.1") (h "067jpm2xhjb279l42yz4qf67kp9a7kafp4qrakfqyv6drq9nh2sd") (f (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3.2 (c (n "mh-z19") (v "0.3.2") (h "0pargl3n5s12rxd2h7kfmav00xhgbpqcr3r038kjbqyqllr0vn7m") (f (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3.3 (c (n "mh-z19") (v "0.3.3") (h "0a7a5s7aj78wcjyrz6g4f20xij1fpa06fcg747aip9nxlhl05qh8") (f (quote (("std") ("default"))))))

