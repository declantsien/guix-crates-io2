(define-module (crates-io mh te mhteams) #:use-module (crates-io))

(define-public crate-mhteams-0.1.0 (c (n "mhteams") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pbgq8l0x6vpk3s0hzsqnh29a8z1gdyhxcd11nmh0adwlbkbbi74")))

