(define-module (crates-io mh lo mhlog) #:use-module (crates-io))

(define-public crate-mhlog-1.0.0 (c (n "mhlog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0zffcwm9hnjv5rqkmp9zyrrfwrb0jhj0w21cf8g0ypb7fj1j46r6")))

(define-public crate-mhlog-1.0.1 (c (n "mhlog") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yv5rac1z3xpv7ys0jj8grx3g9xmilw5mq5ww9q2gdpjv1qh49a3")))

(define-public crate-mhlog-1.1.0 (c (n "mhlog") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0y7bn8k86dy9ifhjdp95wzlccpfq8mrwgx32lbljvajkjn1a013d")))

(define-public crate-mhlog-2.0.0 (c (n "mhlog") (v "2.0.0") (d (list (d (n "chrono") (r "~0.4.11") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)))) (h "0v1hgl64k5bidi08gbja84wfqg0z1air219ng35162qb5vkram70")))

(define-public crate-mhlog-2.1.0 (c (n "mhlog") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "console") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0b6j0wlk7n7q07ws5p7077vd90645fwp7h28d3hiiwc76wyprxid") (f (quote (("log2stdout") ("colours" "console"))))))

(define-public crate-mhlog-2.1.1 (c (n "mhlog") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "console") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "1b3pgsczpayxd13232y862bx9mvlysfi4c6hkdycyqhkd1ignmh0") (f (quote (("log2stdout") ("colours" "console"))))))

(define-public crate-mhlog-2.1.2 (c (n "mhlog") (v "2.1.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "console") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "00jmnjya3vl39n1vi9vyhjhqmyim8fz10zc29hgngyhzblpgqbvi") (f (quote (("log2stdout") ("colours" "console"))))))

(define-public crate-mhlog-3.0.0 (c (n "mhlog") (v "3.0.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 2)) (d (n "console") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)))) (h "02rsv4ps0pb6rawggvpmi2hiwkmssknbp57s26abx1nxrqsf9qxa") (f (quote (("only_stdout") ("only_stderr") ("colours" "console"))))))

(define-public crate-mhlog-3.0.1 (c (n "mhlog") (v "3.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 2)) (d (n "console") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "~1.4") (d #t) (k 0)))) (h "1ym96qifs521mipvdh3cf5vq9rlr64k8k20h3g4rjvq1y03v2vgi") (f (quote (("only_stdout") ("only_stderr") ("colours" "console"))))))

