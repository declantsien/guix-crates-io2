(define-module (crates-io mh ra mhra_products_search_client_temp) #:use-module (crates-io))

(define-public crate-mhra_products_search_client_temp-0.0.2-rc2 (c (n "mhra_products_search_client_temp") (v "0.0.2-rc2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "14rnrsm4qvgx5a2cyr929gx46whz25asina73250l4vw2rilh6j3") (y #t)))

