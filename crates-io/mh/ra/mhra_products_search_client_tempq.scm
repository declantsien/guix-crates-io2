(define-module (crates-io mh ra mhra_products_search_client_tempq) #:use-module (crates-io))

(define-public crate-mhra_products_search_client_tempq-0.0.2-rc2 (c (n "mhra_products_search_client_tempq") (v "0.0.2-rc2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0y7yhk1q3h0nn95kw8zals9rclv1l58js11dzxm1gdf37jlx7g4d") (y #t)))

