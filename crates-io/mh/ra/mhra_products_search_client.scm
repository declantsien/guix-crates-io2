(define-module (crates-io mh ra mhra_products_search_client) #:use-module (crates-io))

(define-public crate-mhra_products_search_client-0.0.1 (c (n "mhra_products_search_client") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0kmqlkrcnsiv4af158jfgpyf7czysg5sb9abj26krwiap7h774w9")))

(define-public crate-mhra_products_search_client-0.0.2-rc1 (c (n "mhra_products_search_client") (v "0.0.2-rc1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "18c8g969glfl5131wil07rdm7ls7lfy7q8cjy5dql7xrwh6zrxmk")))

