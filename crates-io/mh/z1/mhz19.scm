(define-module (crates-io mh z1 mhz19) #:use-module (crates-io))

(define-public crate-mhz19-0.1.0 (c (n "mhz19") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0cn00zsnx1jvd78m7lb5czrqn05ichfss5rhv2n3w5xxffmabrrs")))

