(define-module (crates-io mh ro mhrono) #:use-module (crates-io))

(define-public crate-mhrono-0.1.0 (c (n "mhrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "eyre") (r "^0.6.7") (d #t) (k 0)) (d (n "moldenfile") (r "^0.1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vpcrd999kvj5kj19d1cyrlfg2xvz3yfhbz8789s3gd2ifk0wiky")))

