(define-module (crates-io mh db mhdb) #:use-module (crates-io))

(define-public crate-mhdb-1.0.0 (c (n "mhdb") (v "1.0.0") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "1xc18ga8pzx4039v36kbz99fbk0sk4421pp1grr11c986gp7phgz") (f (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1.0.1 (c (n "mhdb") (v "1.0.1") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "1ricx58msj88f0n115i69rxq50cfx3n3rri0adlai0bn4jbx0h0c") (f (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1.0.2 (c (n "mhdb") (v "1.0.2") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "1glgl2njr520gmdh9fzmgi2lcjilvwncjpncxpf6irjdarid3m2v") (f (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1.0.3 (c (n "mhdb") (v "1.0.3") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "1ad6maxmbvpk2x7ijk8a027yw4flfghzvrhqh4nnayl3gr721ph4") (f (quote (("std" "serde" "bincode") ("default" "std"))))))

