(define-module (crates-io sy sc syscall-intercept) #:use-module (crates-io))

(define-public crate-syscall-intercept-0.1.0 (c (n "syscall-intercept") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0jmvb5cp16gg4707yqngk68l3rjmq7q0z4h78nmvj3iyazw3xy13")))

