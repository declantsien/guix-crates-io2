(define-module (crates-io sy sc syscall-alt) #:use-module (crates-io))

(define-public crate-syscall-alt-0.0.1 (c (n "syscall-alt") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1ljrp9mr7kkzaigkvcjplkv1fifd8p74lcx3lwhk443ya20r53gw")))

(define-public crate-syscall-alt-0.0.2 (c (n "syscall-alt") (v "0.0.2") (h "1civn7dl2vpjb3lfjizskbg4xs3qycj989p36mf7m5q13k7gqfn2")))

(define-public crate-syscall-alt-0.0.3 (c (n "syscall-alt") (v "0.0.3") (h "19gi2fp99c3dip4im4dpdy4hk39mxv9yjmdxc102jqcb6p2vsm9s")))

(define-public crate-syscall-alt-0.0.4 (c (n "syscall-alt") (v "0.0.4") (h "0421arfkk6z4n0f8f8myfx1rir3jz0qzgv97l2d6qnycy6lgwpw3")))

(define-public crate-syscall-alt-0.0.5 (c (n "syscall-alt") (v "0.0.5") (h "00h1gcfvrnv1z3n1am2dblva06173qn4j76id8293jc3nrcskwqm")))

(define-public crate-syscall-alt-0.0.7 (c (n "syscall-alt") (v "0.0.7") (h "1d7nxssbmvhm0csl8idg85cqjpk347m0ajknf53yza30ng10cjn0")))

(define-public crate-syscall-alt-0.0.8 (c (n "syscall-alt") (v "0.0.8") (h "1ibjgv2n87vj296b8s2ama1fqynxzn7gbskklzdcg5g2iczaa933")))

(define-public crate-syscall-alt-0.0.9 (c (n "syscall-alt") (v "0.0.9") (h "1mzflvfdh38q1m5bss77dfs2q7bf6svbs3c3kccqsg7v47yw78iz")))

(define-public crate-syscall-alt-0.0.10 (c (n "syscall-alt") (v "0.0.10") (h "0r534ksl39qabgil40xzs649qlzsiarnhinr53vizcjml7sjc2y9")))

(define-public crate-syscall-alt-0.0.11 (c (n "syscall-alt") (v "0.0.11") (h "1045cwmgbsr69sm3aj1dmwnp1mhzx12qjmwlpa9rhlq2218afgfx")))

(define-public crate-syscall-alt-0.0.12 (c (n "syscall-alt") (v "0.0.12") (h "01x2b5zaknxxa2fh2ga81dmdlja6s4w7182qp26y2wb5q64lzw3p")))

(define-public crate-syscall-alt-0.0.13 (c (n "syscall-alt") (v "0.0.13") (h "078ip00kdr3v3qg4pw8myj65p8q6fpwjb8m4c805rjfsxds4l655")))

(define-public crate-syscall-alt-0.0.14 (c (n "syscall-alt") (v "0.0.14") (h "1w020b6sj3i6sn0hhd0mc9jfr3r0bpw257bzh2v01gpnjzvmnhq3")))

