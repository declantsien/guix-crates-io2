(define-module (crates-io sy sc syscall) #:use-module (crates-io))

(define-public crate-syscall-0.1.0 (c (n "syscall") (v "0.1.0") (h "0pn4ygkwqfnmd8xc04wkhcmd14pqyrs3zg2ky9gbx3fa5k1yi1rp") (y #t)))

(define-public crate-syscall-0.2.0 (c (n "syscall") (v "0.2.0") (h "0j0zb18fszl88498alis7l8glinmcig5ypy6pjlyhn46k1j0zjdf") (y #t)))

(define-public crate-syscall-0.2.1 (c (n "syscall") (v "0.2.1") (h "0mmmnn660wpl8qxl1ifs8l811i120br22rpljvfkiwwv0ggc9qns") (y #t)))

