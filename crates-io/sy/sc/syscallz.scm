(define-module (crates-io sy sc syscallz) #:use-module (crates-io))

(define-public crate-syscallz-0.1.0 (c (n "syscallz") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "133hy992hx8q8d06w11igf5l1aw0z4dwzm4hgqv0174qrm8m2bs5")))

(define-public crate-syscallz-0.2.0 (c (n "syscallz") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0qi48w27c1s25ryaka695v8a3gzrdrka02s7b8pid018d2apljvc")))

(define-public crate-syscallz-0.3.0 (c (n "syscallz") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1w7pxjp2k7q8rjlc9347zc288v884rilfxcfmyzxkqk3zchqxdg3")))

(define-public crate-syscallz-0.4.0 (c (n "syscallz") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0mnvf577vl9nbfn052v89kjhp2fghngx0s563xybk8wh8da35ia2")))

(define-public crate-syscallz-0.5.0 (c (n "syscallz") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "15js7sbjy9fxz547vj8nwfrkrnj42zaim6y3112x1icrrgnxll2p")))

(define-public crate-syscallz-0.6.0 (c (n "syscallz") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0dg297znmngil0g58yi3mxw7lkgz2j6kc3w20wpmnjfs0ghz9xdk")))

(define-public crate-syscallz-0.7.0 (c (n "syscallz") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0if0psfvd1fzzhyvbmll7527wsqc00vqmj59nhgbbh82m0k6rfp7")))

(define-public crate-syscallz-0.8.0 (c (n "syscallz") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1qdv93xk5yxmlm7661ys5w6m9fkd8xglmlg7vzh01sigpvxwwjc2")))

(define-public crate-syscallz-0.9.0 (c (n "syscallz") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "12aly10398pv9lyy4avqivvzv74gl8m3xazqskkrk10yzn7qg2xq")))

(define-public crate-syscallz-0.10.0 (c (n "syscallz") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1nvd2cv08j23k9mrcjfd0g7dwaa7sbwmj98bh1jjblvlqgi1rfxw")))

(define-public crate-syscallz-0.11.0 (c (n "syscallz") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1cfqyz7vnw1ca2wysa8mwj5ynsgl4wsnp19g4b14fh9hgdlxp2rs")))

(define-public crate-syscallz-0.11.1 (c (n "syscallz") (v "0.11.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1liqm1pf7k8vkbahxbsacl7x09g51q148mzyaaxjxhqwv0ilq9ss")))

(define-public crate-syscallz-0.11.2 (c (n "syscallz") (v "0.11.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "10yl9x3lmrpl0vypw2sn0yl903snxiv6dr6gwpw587q3yhx9xn78")))

(define-public crate-syscallz-0.11.3 (c (n "syscallz") (v "0.11.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "05zjgmpdpna3zjnkx7k9187akhhd25yyh5i11knh424r91z0givb")))

(define-public crate-syscallz-0.11.4 (c (n "syscallz") (v "0.11.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "1xhjp33m58gb03vn54zrwz5qrvicyz1g3izh3w8rz5ph9rvbv5ld")))

(define-public crate-syscallz-0.12.0 (c (n "syscallz") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "0qp4776n0fixsg2rjyzh0qccyi330qq3w6yhyzkl517hw5pl36mq")))

(define-public crate-syscallz-0.13.0 (c (n "syscallz") (v "0.13.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "1x7vs7xdx9ks7h9h1agvlnd96xphxhaxa1rqzl4lgscd01z3ngvl")))

(define-public crate-syscallz-0.13.1 (c (n "syscallz") (v "0.13.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1dzd1i62lxgfbvsrhk0qx518z193s23asxwcyb0qhfhwwfqsh526")))

(define-public crate-syscallz-0.13.2 (c (n "syscallz") (v "0.13.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1g1vy2i9ncgd9cm5qsd9dbgw5fj3rphsjprq8kz5z3wql9z5nw2h")))

(define-public crate-syscallz-0.14.0 (c (n "syscallz") (v "0.14.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "00ziagcw5l3k5pq8c87pwywdc2bvcgvr2r3x320k6vn57ggiinq9")))

(define-public crate-syscallz-0.14.1 (c (n "syscallz") (v "0.14.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 0)))) (h "0981x494pijsnc1cpvc74ssdynp4bfrhzhmyndx6r0la5n0s4419")))

(define-public crate-syscallz-0.15.0 (c (n "syscallz") (v "0.15.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 0)))) (h "0z5yfgb6086gdvbrm8lm0i745j6d8qw653xxy3mlrzdcjk3j1skz")))

(define-public crate-syscallz-0.16.0 (c (n "syscallz") (v "0.16.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0nj6r9d1mjmpkza3fwvva60rkxlay15kh80cagrz6h8lgzn9358g")))

(define-public crate-syscallz-0.16.1 (c (n "syscallz") (v "0.16.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "05ws3w0lfbcazx2ay5nkfhgbs4kb6nvk230kbvql44ykjrjm0jnr")))

(define-public crate-syscallz-0.16.2 (c (n "syscallz") (v "0.16.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1bd6k8v52fwvzspz6g8rrlrc0wffcbx022xvi62bb0z5czrgf3qi")))

(define-public crate-syscallz-0.17.0 (c (n "syscallz") (v "0.17.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "seccomp-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "19zc9rdjxhddbmnkdhk2aixh8csqv4rhbll78smvy471qdvxhpx0")))

