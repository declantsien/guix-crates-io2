(define-module (crates-io sy sc sysconf) #:use-module (crates-io))

(define-public crate-sysconf-0.1.0 (c (n "sysconf") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0jgbjzr1jjhbc35gwqwx18g63lay2gkcxvp3nbfdiyv2h89ffwmy")))

(define-public crate-sysconf-0.1.1 (c (n "sysconf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "1sm336ari6f64aa5hr1lyjzsjd39dbsrrv3pbag2vnz3xz1sdphw")))

(define-public crate-sysconf-0.1.2 (c (n "sysconf") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0anwrdlrzbpbgy8n9hs4aqp8k4f9ybdwr4gmdw4bmm6vn5fbx6wh")))

(define-public crate-sysconf-0.2.0 (c (n "sysconf") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0a3ccylcisnng7f4k5a2k3xcc5nzip7n2alq7vgahsxivh6grkg8")))

(define-public crate-sysconf-0.3.0 (c (n "sysconf") (v "0.3.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "19xc6w9h36kwba9ybzp74dp0ymbiwvs82g11x9nbirbkyy4kn4rd") (f (quote (("nightly" "lazy_static"))))))

(define-public crate-sysconf-0.3.1 (c (n "sysconf") (v "0.3.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "14qgmznmcj3hfj74153w4kmf66yrra2vmjl7azssjsr2wbz9z5zk") (f (quote (("nightly" "lazy_static"))))))

(define-public crate-sysconf-0.3.2 (c (n "sysconf") (v "0.3.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "00j7vy027mc5j9s9x0bhdzlkjjwjx98m32m30lh9gx8hysgn0gm0") (f (quote (("nightly" "lazy_static"))))))

(define-public crate-sysconf-0.3.3 (c (n "sysconf") (v "0.3.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1m96nd7d6y82bfgh6m6hq6n7gc4iwjsr07c3p7g9xkzg15ymqwd0") (f (quote (("nightly" "lazy_static"))))))

(define-public crate-sysconf-0.3.4 (c (n "sysconf") (v "0.3.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "18xy96c2qq2i4x8kmxac97g2hpbr1wpsrxsyl2v4jpsk8mfkzsar") (f (quote (("nightly" "lazy_static"))))))

