(define-module (crates-io sy sc sysctrlcmd) #:use-module (crates-io))

(define-public crate-sysctrlcmd-1.0.0 (c (n "sysctrlcmd") (v "1.0.0") (d (list (d (n "nix") (r "^0.27") (f (quote ("reboot"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "powrprof" "securitybaseapi" "processthreadsapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1rq1zp3binbdfw6yxymz1xxxmfyjc9vbqjbiqzznl4pjiwx8fvxh")))

(define-public crate-sysctrlcmd-1.0.1 (c (n "sysctrlcmd") (v "1.0.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("reboot"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "powrprof" "securitybaseapi" "processthreadsapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1j3270y381i2h73bj7f50sadm1k3f3m925cd1rav71ya16kc38xk")))

(define-public crate-sysctrlcmd-1.0.2 (c (n "sysctrlcmd") (v "1.0.2") (d (list (d (n "nix") (r "^0.27") (f (quote ("reboot"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "powrprof" "securitybaseapi" "processthreadsapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0y3b07lz8iz3lj3gn51337qcs2gx5i0zj9wnchniqy30m76bcr2d")))

