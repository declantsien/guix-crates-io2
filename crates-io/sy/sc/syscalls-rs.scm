(define-module (crates-io sy sc syscalls-rs) #:use-module (crates-io))

(define-public crate-syscalls-rs-0.1.0 (c (n "syscalls-rs") (v "0.1.0") (d (list (d (n "ntapi") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef" "winnt"))) (d #t) (k 0)))) (h "05a087jm6xmr5c1b6ikirrk1gkmrrq32s57av38635rkxhp3mih2") (y #t)))

(define-public crate-syscalls-rs-0.1.1 (c (n "syscalls-rs") (v "0.1.1") (d (list (d (n "ntapi") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef" "winnt"))) (d #t) (k 0)))) (h "0vlsgqnc7f286b8995xlsjcaykbq3dwhd4igd4z3z2bs4j0wrfqv") (y #t)))

