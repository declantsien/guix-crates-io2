(define-module (crates-io sy sc syscall_attr) #:use-module (crates-io))

(define-public crate-syscall_attr-0.1.0 (c (n "syscall_attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "08iw16k0mwxv3x4ranhamcs4br03c22mg9n9kvva33qwhgks0vlv")))

(define-public crate-syscall_attr-0.1.1 (c (n "syscall_attr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0fp8hxf1v73jj8bsa8r6f1n9k80gmmhaa1vs7dyg1xrgk17flias")))

