(define-module (crates-io sy sc syscall-linux-raw) #:use-module (crates-io))

(define-public crate-syscall-linux-raw-0.0.1 (c (n "syscall-linux-raw") (v "0.0.1") (d (list (d (n "direct-asm") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "1w18wnscn902z39fiq1m4gxyppdag016m5qswkv7lnrjq8bm40ld") (y #t)))

(define-public crate-syscall-linux-raw-0.0.2 (c (n "syscall-linux-raw") (v "0.0.2") (d (list (d (n "direct-asm") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "1l2qwqqpiwaxpbm8ismgas3v27dwb5fy4xih8shp4b6da3jg775j")))

