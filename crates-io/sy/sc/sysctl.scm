(define-module (crates-io sy sc sysctl) #:use-module (crates-io))

(define-public crate-sysctl-0.1.0 (c (n "sysctl") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "070lw9sv33450lqwdd0xy4fsrxn4w4vbyci1px72k4dbjmyshz9w")))

(define-public crate-sysctl-0.1.1 (c (n "sysctl") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "1fnpz97nmmcd1difn2wxjgja2jflywr3432c1k3spw0xn54jfp79")))

(define-public crate-sysctl-0.1.2 (c (n "sysctl") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "1548fy2l1j83p9ar27xvyhxaa31b5041njak77vfw70g5kz5ig67")))

(define-public crate-sysctl-0.1.3 (c (n "sysctl") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1npgv4jra21r75mvxxkd7qbd42ffc5hx0rpjcr6jsvh8qh28fr1x")))

(define-public crate-sysctl-0.1.4 (c (n "sysctl") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "10wxlk4nkzlxp4fg435jmdmfwl4hy0y4w2rcsgs634lvbp8pgksb")))

(define-public crate-sysctl-0.2.0 (c (n "sysctl") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "0d4lyw93lin4n30iyc055sdv2ivjv1bgp24bxan22294ncy520r7")))

(define-public crate-sysctl-0.3.0 (c (n "sysctl") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "0s4rb91iw44mzkx71ccr7jx9whvqss54g1py2bfq0m83lf3q97pv")))

(define-public crate-sysctl-0.4.0 (c (n "sysctl") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0p6bfjsw3v12nb2qsgm6r9klwb5qyh4w55zzmccv8r5aqb8g0085")))

(define-public crate-sysctl-0.4.1 (c (n "sysctl") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1rxyr9jscrd01mx3rqvlryfkmbqd0lqsfva68vwn3bwy6khq44jp")))

(define-public crate-sysctl-0.4.2 (c (n "sysctl") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0dm9zfpm8kva8qxmry2riraxilcy47h2nb3l5285l61l7g3qhd4n")))

(define-public crate-sysctl-0.4.3 (c (n "sysctl") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0kfvdig557ji1qjg7w7xjz5pfn281yj5vq6j0lvrwqqp5sizgczy")))

(define-public crate-sysctl-0.4.4 (c (n "sysctl") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "191y6vgiv33mxz50wxraxyw8kisrqfpxv2n8nv3fmdgjz9fn88qi")))

(define-public crate-sysctl-0.4.6 (c (n "sysctl") (v "0.4.6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1a6g8cb8c7wk0rsz6ixydbmaxhs0i8d3hmywd1qi1bfh08zlhpi2")))

(define-public crate-sysctl-0.5.1 (c (n "sysctl") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1z08cvdffdhn5klr4k1d2229p2yaq0528h2jvyg87i8yrpm0zyhi") (y #t)))

(define-public crate-sysctl-0.5.2 (c (n "sysctl") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1q8rxj3v6wlkg3bssrbk5sd1zfzc6g60ljrzjsw7l8pg5dxh77gr")))

(define-public crate-sysctl-0.5.3 (c (n "sysctl") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "11q154h559lm4vj017swibkyah4yb38y4ilg3907qnbh949lrbf8")))

(define-public crate-sysctl-0.5.4 (c (n "sysctl") (v "0.5.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0ghxnlffv8xc65hgr84pr3gbnyjvi5khgjcv51cncrdxrjidcrpd")))

(define-public crate-sysctl-0.5.5 (c (n "sysctl") (v "0.5.5") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1sly34bm4w2vcfqgn7f6255pxwa2wa4vkzdrz2x0drgyy32xszgc")))

