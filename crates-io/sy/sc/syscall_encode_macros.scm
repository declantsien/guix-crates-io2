(define-module (crates-io sy sc syscall_encode_macros) #:use-module (crates-io))

(define-public crate-syscall_encode_macros-0.1.0 (c (n "syscall_encode_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "syscall_encode_traits") (r "^0.1") (d #t) (k 0)))) (h "0dgwsm0gpi8fzp0kdpq2yak7qbnm6qqbm8fwp8rqvi0samf9kwbh")))

(define-public crate-syscall_encode_macros-0.1.1 (c (n "syscall_encode_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "syscall_encode_traits") (r "^0.1") (d #t) (k 0)))) (h "1gvq8acnjizvnvfimqj74ia1lgxn7a9384phnqjmdxfh37vnixhk") (f (quote (("rustc-dep-of-std" "syscall_encode_traits/rustc-dep-of-std"))))))

(define-public crate-syscall_encode_macros-0.1.2 (c (n "syscall_encode_macros") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "syscall_encode_traits") (r "^0.1") (d #t) (k 0)))) (h "1ss5ggsmgh2mqpkzk75wl33a9kkjgk3byd5ipz5g6pabv92gzb1y") (f (quote (("rustc-dep-of-std" "syscall_encode_traits/rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-syscall_encode_macros-0.1.3 (c (n "syscall_encode_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "syscall_encode_traits") (r "^0.1.3") (d #t) (k 0)))) (h "1fg867d2czr1vqkj7nfbin5lj920cmrqhwzvvrmky008zc4hk23x") (f (quote (("rustc-dep-of-std" "syscall_encode_traits/rustc-dep-of-std"))))))

(define-public crate-syscall_encode_macros-0.1.7 (c (n "syscall_encode_macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0fi7rk0cr62l4xy5v5lqx1hb9jqc6ma3lgfyk718617d4f1yq8dw") (f (quote (("rustc-dep-of-std"))))))

(define-public crate-syscall_encode_macros-0.1.8 (c (n "syscall_encode_macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0rbbp819s2c40xhvlrkkyw1l4jp05axfflcrpwwpq6rczb05qkwp") (f (quote (("rustc-dep-of-std"))))))

