(define-module (crates-io sy sc syscall-numbers) #:use-module (crates-io))

(define-public crate-syscall-numbers-1.0.0 (c (n "syscall-numbers") (v "1.0.0") (h "0a793kyyhmwj168rgryzbsip1yq088l0yzbxmjwdj2642v8pspp0")))

(define-public crate-syscall-numbers-2.0.0 (c (n "syscall-numbers") (v "2.0.0") (h "1m480nyabwr3p6a0lpq4l506rkcf4iz5xbml8dqg23179ksv8gl8")))

(define-public crate-syscall-numbers-3.0.0 (c (n "syscall-numbers") (v "3.0.0") (h "061qkabq8ps8dia4c0pmwljin2bzbfcc63vffk71rspb3cqs81si")))

(define-public crate-syscall-numbers-3.1.0 (c (n "syscall-numbers") (v "3.1.0") (h "08hrgmic21v76cm63faybwa9faix6xrsldzjg6gqhdi0czmdi5jv")))

(define-public crate-syscall-numbers-3.1.1 (c (n "syscall-numbers") (v "3.1.1") (h "0a9avjsjvgqa14dln03as7h2wy856n40s27czn0k7kb7yhmkhkh8")))

