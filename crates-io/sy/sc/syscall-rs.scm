(define-module (crates-io sy sc syscall-rs) #:use-module (crates-io))

(define-public crate-syscall-rs-0.0.1 (c (n "syscall-rs") (v "0.0.1") (h "1xs5p9khzinnlzmamw86k6ghx3mdf5ysiqprpkpl768hbpcmfi2s")))

(define-public crate-syscall-rs-0.0.2 (c (n "syscall-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06wjkj840xpw032ndcsd271x49qmprviqslv5769vgnab9xs09wj")))

(define-public crate-syscall-rs-0.0.3 (c (n "syscall-rs") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "os-ext" "net"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sbmkl7hnfirsgada49m2j06kj3hvq76p9icr8hrb92mdns3bbl2")))

