(define-module (crates-io sy sc syscoin-bech32) #:use-module (crates-io))

(define-public crate-syscoin-bech32-0.8.2 (c (n "syscoin-bech32") (v "0.8.2") (d (list (d (n "bech32") (r "^0.6.0") (d #t) (k 0)))) (h "1pkbvi8v2skyfg71y5ncgarcq7jllhmsx446wc2vp5w8vrfrlh8s")))

(define-public crate-syscoin-bech32-0.8.3 (c (n "syscoin-bech32") (v "0.8.3") (d (list (d (n "bech32") (r "^0.5.0") (d #t) (k 0)))) (h "087f9lbwivnxhprhslscsa0i72dzhlgxf8vnsnr1mn6m4dqgg73q")))

(define-public crate-syscoin-bech32-0.8.4 (c (n "syscoin-bech32") (v "0.8.4") (d (list (d (n "bech32") (r "^0.6.0") (d #t) (k 0)))) (h "18b1j838bdx17bq08jv30k5wkb6gci6fyzm5kx52pxvbs8q325kh")))

