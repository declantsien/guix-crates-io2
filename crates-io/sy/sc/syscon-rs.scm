(define-module (crates-io sy sc syscon-rs) #:use-module (crates-io))

(define-public crate-syscon-rs-0.1.0 (c (n "syscon-rs") (v "0.1.0") (d (list (d (n "fdt") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02jv87a5gy57nii9mb4h03klv5cl179256akrvjhqqp0rl7yh2w7")))

(define-public crate-syscon-rs-0.1.1 (c (n "syscon-rs") (v "0.1.1") (d (list (d (n "fdt") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmio") (r "^2.1.0") (d #t) (k 0)))) (h "1v2pi5hwwbcydmc4j0wsb2axfy01bj5744jws8c31mzkzm7bf6r8")))

