(define-module (crates-io sy ev syeve) #:use-module (crates-io))

(define-public crate-syeve-0.1.0 (c (n "syeve") (v "0.1.0") (d (list (d (n "brotli2") (r "^0.3.2") (d #t) (k 0)) (d (n "deflate") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "packed_simd_2") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.17.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1h9278fpd0qfhryk8v4sziyp2pkm9dpa6zxpszb9nk656s79vs43") (f (quote (("simd" "packed_simd_2") ("default" "simd") ("compression_deflate" "deflate" "inflate"))))))

