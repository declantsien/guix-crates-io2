(define-module (crates-io sy mt symtern) #:use-module (crates-io))

(define-public crate-symtern-0.1.0-pre.0 (c (n "symtern") (v "0.1.0-pre.0") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "1mc344di95kw4cbnjyf5za3zxfn87qvrkg5d4q5adkl6qssx6imb") (f (quote (("default" "fnv"))))))

(define-public crate-symtern-0.1.0-pre.1 (c (n "symtern") (v "0.1.0-pre.1") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "03d9vhyrlad92pdq1r56jk1wwsm89f815l000b2ay1i8znm7qhbw") (f (quote (("default" "fnv"))))))

(define-public crate-symtern-0.1.0 (c (n "symtern") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 2)))) (h "0rxk9f88lrscffig50lnjd5ms40zhavr2j7l7i9p2bd1m4a1p8a5") (f (quote (("default" "fnv"))))))

