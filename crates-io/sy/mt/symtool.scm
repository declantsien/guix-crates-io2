(define-module (crates-io sy mt symtool) #:use-module (crates-io))

(define-public crate-symtool-0.1.2 (c (n "symtool") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "goblin") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "symtool-backend") (r "^0.2") (d #t) (k 0)))) (h "1557zm40d1b9b7kkgv9l8a8pj9c25bw35wr8qw6wr9f1v6579v6c")))

(define-public crate-symtool-0.1.3 (c (n "symtool") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "goblin") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "symtool-backend") (r "^0.2") (d #t) (k 0)))) (h "19ymp1nbrfssqqipsdq1n0cwwriwsdzkss5r0pmkdavam0b3g3hv")))

(define-public crate-symtool-0.1.4 (c (n "symtool") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "symtool-backend") (r "^0.3") (d #t) (k 0)))) (h "1vxsx5i4ww6n02jrx405vkq7d413c43gfn90qy15lgvk81ij556a")))

