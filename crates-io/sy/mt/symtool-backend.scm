(define-module (crates-io sy mt symtool-backend) #:use-module (crates-io))

(define-public crate-symtool-backend-0.2.0 (c (n "symtool-backend") (v "0.2.0") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.1") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "15m3gj6v2kbf1dqhiw41fh4ls9dlphlv6q7iy31b4ay5pi4rys3r")))

(define-public crate-symtool-backend-0.3.0 (c (n "symtool-backend") (v "0.3.0") (d (list (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "1jhyq10wj4g60znqw077p01qpbg7kdknwj274yqirarwnzbfhzgv")))

