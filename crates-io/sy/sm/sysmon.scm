(define-module (crates-io sy sm sysmon) #:use-module (crates-io))

(define-public crate-sysmon-0.1.0 (c (n "sysmon") (v "0.1.0") (d (list (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0z8xbrs77w0a323dn5cmihmqsfw9q9wy77q88k39j3gv2iigxbs4")))

(define-public crate-sysmon-0.1.1 (c (n "sysmon") (v "0.1.1") (d (list (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "09x1bhabrilgsg3idrln75aj9m54vnqr1fxwqx4c75yv0v7d24fb")))

(define-public crate-sysmon-0.1.2 (c (n "sysmon") (v "0.1.2") (d (list (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0zfii21z1la12y5b32sp3rzy9j4pvid5ysvl1b03ikyn488ly5c4")))

(define-public crate-sysmon-0.1.3 (c (n "sysmon") (v "0.1.3") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0mc1whmykhd73nv1pin4w2xxv1n4bsc71wvq3dxjq044d4i5k0jh")))

(define-public crate-sysmon-0.1.4 (c (n "sysmon") (v "0.1.4") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1xml8z7smivdgwdqcd201l6rpx5vnyizfjp1gmslv6gksh18l4qv")))

(define-public crate-sysmon-0.1.5 (c (n "sysmon") (v "0.1.5") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0dppz3rxml9r8jwd2azgh60zq16lg9saw086kx2skrq578zj2z0a")))

(define-public crate-sysmon-0.1.6 (c (n "sysmon") (v "0.1.6") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1zfmxk3bprsy8zqiv1g04zc4f9z2sb738cpy6jz8mmlspj4wvxbk")))

(define-public crate-sysmon-0.1.7 (c (n "sysmon") (v "0.1.7") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0jshyvqv3llnr63abxsk70pfx3kzksap9kz1lngsdx273p6w5jai")))

(define-public crate-sysmon-0.1.8 (c (n "sysmon") (v "0.1.8") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "unhtml") (r "^0.1.1") (d #t) (k 0)) (d (n "unhtml_derive") (r "^0.2.1") (d #t) (k 0)))) (h "0m8dz8db4r6848a1y5jjv5zsf13m2n39m1b67l97ir9nibpcmwh3")))

(define-public crate-sysmon-0.2.0 (c (n "sysmon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "01a5ywz4wlzf5qh5s1pfmc3f9m7cpi8nb9in6pgb3zip9gwy53r4")))

(define-public crate-sysmon-0.2.1 (c (n "sysmon") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0l0spp814k638awnwkw1q94k0z46j34w119y7pwzqs4aji8w8jj7")))

(define-public crate-sysmon-0.2.2 (c (n "sysmon") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0lqz1sbxliv1yhnqygxcvfsa97bfkv966hck8n02zlbmvcpg8ca5")))

(define-public crate-sysmon-0.2.3 (c (n "sysmon") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rl012453gvbx7bq9km13gvfw2j1arg6258ygi2wyjdhwvvxy2gj")))

(define-public crate-sysmon-0.2.4 (c (n "sysmon") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "146dwacvahpyp2362jggnrlxs2f7pmszrsn00c1xbir3r2n2vrl5")))

(define-public crate-sysmon-0.2.5 (c (n "sysmon") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1cyjgiqglwcf8fshrbm00vzsqg9qjln2wms5vryl9f9ajxv8hz2q")))

