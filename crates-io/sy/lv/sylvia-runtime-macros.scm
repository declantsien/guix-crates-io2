(define-module (crates-io sy lv sylvia-runtime-macros) #:use-module (crates-io))

(define-public crate-sylvia-runtime-macros-0.6.0 (c (n "sylvia-runtime-macros") (v "0.6.0") (d (list (d (n "cargo-tarpaulin") (r "^0.22.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("derive" "parsing" "full" "visit" "extra-traits" "printing"))) (k 0)))) (h "0rvjrqjyfl6zkvi0jlplm47a2hw9bv5avsrqnkm2cj0kx6v56212")))

