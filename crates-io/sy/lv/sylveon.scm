(define-module (crates-io sy lv sylveon) #:use-module (crates-io))

(define-public crate-sylveon-0.0.1 (c (n "sylveon") (v "0.0.1") (h "053i6nlipqjbzjy2z69lcgp9s4fm2qij4xgbng02ilkfplkcvj7q") (y #t)))

(define-public crate-sylveon-0.1.0 (c (n "sylveon") (v "0.1.0") (h "05jlvk39x086jadciwlbd5mryjw1n7svwx5ha6i8c1fhh8ijk01l")))

(define-public crate-sylveon-0.2.0 (c (n "sylveon") (v "0.2.0") (h "0drrjnglxj5zay95jr4ag4bq5mks3pbh2xqld2gcxjl2mipmmwij")))

(define-public crate-sylveon-0.2.1 (c (n "sylveon") (v "0.2.1") (d (list (d (n "sylveon_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0azd8i5d0348pzzm5vzbyhiknw6z8fvwm04i8vdyk0z59kv6ffbj")))

