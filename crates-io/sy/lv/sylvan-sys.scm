(define-module (crates-io sy lv sylvan-sys) #:use-module (crates-io))

(define-public crate-sylvan-sys-1.0.0 (c (n "sylvan-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q07mimxs45zqvy1iyn4q9kffyzqgwy2cxs62lm14s62i6xa4ydy") (f (quote (("default" "build_sylvan") ("build_sylvan"))))))

(define-public crate-sylvan-sys-1.1.0 (c (n "sylvan-sys") (v "1.1.0") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d30djyh95m99vqjs459br0xrg3h0qwxp3yphh4kikvir4vsk6b1") (f (quote (("default" "build_sylvan") ("build_sylvan"))))))

