(define-module (crates-io sy lv sylv) #:use-module (crates-io))

(define-public crate-sylv-0.1.0 (c (n "sylv") (v "0.1.0") (h "1pbamlz197ajpnk2wxvj0gj0dxxzbdwr7kyqlcyixmfpkm5a0da5") (y #t)))

(define-public crate-sylv-0.0.0 (c (n "sylv") (v "0.0.0") (h "1rqz70fqyhv6dc2w8aanm2wws6jha7l4hcgz385g17sqdcbixjik")))

