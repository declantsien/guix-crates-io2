(define-module (crates-io sy lv sylvans_parser) #:use-module (crates-io))

(define-public crate-sylvans_parser-0.1.0 (c (n "sylvans_parser") (v "0.1.0") (h "1pba6qdcn3bay7a7nm8kbnw3kxvan9ym4wfjmr93jrwl22q5yz8c")))

(define-public crate-sylvans_parser-0.1.1 (c (n "sylvans_parser") (v "0.1.1") (h "1ws6qmqr1dli8059hnplymk91ydyf32q75b2x6b4ga1wayc3kixr")))

