(define-module (crates-io sy ca sycamore-dnd) #:use-module (crates-io))

(define-public crate-sycamore-dnd-0.1.1 (c (n "sycamore-dnd") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sycamore") (r "^0.9.0-beta.1") (f (quote ("serde" "suspense"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("DragEvent" "DataTransfer" "HtmlImageElement"))) (d #t) (k 0)))) (h "0vcniq338w11hjall2lrpvp65nwvza690sfiqvrs2f53acz3bmai")))

