(define-module (crates-io sy ca sycamore-state-macros) #:use-module (crates-io))

(define-public crate-sycamore-state-macros-0.0.1 (c (n "sycamore-state-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "sycamore-state-core") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ml24l1qhcsqp612lpgd1y1qrp4pczm7x1d73p754hzn7gfz473r")))

(define-public crate-sycamore-state-macros-0.0.2 (c (n "sycamore-state-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "sycamore-state-core") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c1piigckj0n1vgzspvr1wdp7yq7yngfnk49q107j63wd5f1vcq4")))

