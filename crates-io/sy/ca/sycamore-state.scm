(define-module (crates-io sy ca sycamore-state) #:use-module (crates-io))

(define-public crate-sycamore-state-0.1.0 (c (n "sycamore-state") (v "0.1.0") (d (list (d (n "sycamore") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)))) (h "0xadpzq9f3kxrg7n77bj8qwk6n0mj2lpyjlyng376w56hfmjw1kx") (f (quote (("async" "sycamore/suspense"))))))

