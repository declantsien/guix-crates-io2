(define-module (crates-io sy ca sycamore-query) #:use-module (crates-io))

(define-public crate-sycamore-query-0.1.0 (c (n "sycamore-query") (v "0.1.0") (d (list (d (n "fluvio-wasm-timer") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sycamore") (r "^0.8") (f (quote ("suspense"))) (d #t) (k 0)) (d (n "weak-table") (r "^0.3") (d #t) (k 0)))) (h "1rffhsrv7j8ivf5lh6mnk00jafgmsyn4jv2r5ns2bqjzxfjjirdb")))

(define-public crate-sycamore-query-0.1.1 (c (n "sycamore-query") (v "0.1.1") (d (list (d (n "fluvio-wasm-timer") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sycamore") (r "^0.8") (f (quote ("suspense"))) (d #t) (k 0)) (d (n "weak-table") (r "^0.3") (d #t) (k 0)))) (h "0c85ladvkscvp5nl6vw7ywrxqglpp5m82a7iy9vpwqg22z3wymq7")))

