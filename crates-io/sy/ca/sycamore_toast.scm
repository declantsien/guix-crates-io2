(define-module (crates-io sy ca sycamore_toast) #:use-module (crates-io))

(define-public crate-sycamore_toast-0.1.15 (c (n "sycamore_toast") (v "0.1.15") (d (list (d (n "gloo-timers") (r "^0.2.1") (f (quote ("futures"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sycamore") (r "^0.8.0") (f (quote ("suspense" "ssr"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "js" "serde"))) (d #t) (k 0)) (d (n "wasm-cookies") (r "^0.1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (d #t) (k 0)))) (h "1ph1jzv5iszyksis0v469cwhhb9rh3kfhkbfhmn8gglyw7lyxkm7")))

