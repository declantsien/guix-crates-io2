(define-module (crates-io sy ca sycamore_helpers) #:use-module (crates-io))

(define-public crate-sycamore_helpers-0.3.0 (c (n "sycamore_helpers") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sycamore") (r "^0.8.0-beta.6") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (d #t) (k 0)))) (h "1rhwd47jhgfmk1vdb9v1p2mgxbcfvl0lvylyh2aqfk0023gnfcwl")))

