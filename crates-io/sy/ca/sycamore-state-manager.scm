(define-module (crates-io sy ca sycamore-state-manager) #:use-module (crates-io))

(define-public crate-sycamore-state-manager-0.0.1 (c (n "sycamore-state-manager") (v "0.0.1") (d (list (d (n "sycamore") (r "^0.9.0-beta.1") (d #t) (k 0)) (d (n "sycamore-state-core") (r "^0.0.1") (d #t) (k 0)) (d (n "sycamore-state-macros") (r "^0.0.1") (d #t) (k 0)))) (h "0wmncz1i625nb5xlz3ycbcddjcykr9p36fbfk77hnr4sdn4kikzg")))

(define-public crate-sycamore-state-manager-0.0.2 (c (n "sycamore-state-manager") (v "0.0.2") (d (list (d (n "sycamore") (r "^0.9.0-beta.1") (d #t) (k 0)) (d (n "sycamore-state-core") (r "^0.0.2") (d #t) (k 0)) (d (n "sycamore-state-macros") (r "^0.0.2") (d #t) (k 0)))) (h "1fg7pkkgjz50dwrcc58cplq53xibc4fghrg6isgrg7k42czygfib")))

