(define-module (crates-io sy ca sycamore-core) #:use-module (crates-io))

(define-public crate-sycamore-core-0.8.0-beta.6 (c (n "sycamore-core") (v "0.8.0-beta.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.8.0-beta.6") (d #t) (k 0)))) (h "13zasdnj852mrli09mb042bc72y5hyxa2d6i78sdn3894j3prqmg") (f (quote (("hydrate") ("default"))))))

(define-public crate-sycamore-core-0.8.0-beta.7 (c (n "sycamore-core") (v "0.8.0-beta.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.8.0-beta.7") (d #t) (k 0)))) (h "13glg22l8zx56gbh4nzxbmkv1psm8m0y6z4ymgjamanqskk2ngsy") (f (quote (("hydrate") ("default"))))))

(define-public crate-sycamore-core-0.8.0 (c (n "sycamore-core") (v "0.8.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.8.0") (d #t) (k 0)))) (h "0pqlawldzg0qn7k07fhcs87xj69zng4aaj4qkczyiry1d8bmzik2") (f (quote (("hydrate") ("default"))))))

(define-public crate-sycamore-core-0.8.2 (c (n "sycamore-core") (v "0.8.2") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.8.0") (d #t) (k 0)))) (h "1659w6sklgrx88akbrinvvx27h086x1bbpj4lms2psn58027zkid") (f (quote (("hydrate") ("default"))))))

(define-public crate-sycamore-core-0.9.0-beta.1 (c (n "sycamore-core") (v "0.9.0-beta.1") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "sycamore-futures") (r "^0.9.0-beta.1") (o #t) (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.9.0-beta.1") (d #t) (k 0)))) (h "1ycass5yix1a2y5bmzc6x3vsdk4c3lwra7z08kxd1mip62a3qrjg") (f (quote (("suspense" "sycamore-futures") ("hydrate") ("default"))))))

(define-public crate-sycamore-core-0.9.0-beta.2 (c (n "sycamore-core") (v "0.9.0-beta.2") (d (list (d (n "hashbrown") (r "^0.14.1") (d #t) (k 0)) (d (n "sycamore-futures") (r "^0.9.0-beta.2") (o #t) (d #t) (k 0)) (d (n "sycamore-reactive") (r "^0.9.0-beta.2") (d #t) (k 0)))) (h "0xgxjmna8z305v0qgkv82ns63vpqr2y9qmwkrxjczf5j34z3vs4m") (f (quote (("suspense" "sycamore-futures") ("hydrate") ("default"))))))

