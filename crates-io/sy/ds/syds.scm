(define-module (crates-io sy ds syds) #:use-module (crates-io))

(define-public crate-syds-0.1.0 (c (n "syds") (v "0.1.0") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1w8pmh2ns2lyh3hqd96fzm99whpvpdgl0alfj3figp0nvvr5ncy4")))

(define-public crate-syds-0.1.1 (c (n "syds") (v "0.1.1") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0ghzxca32k5w1xjcaxyrsjn9sjmw82zzmbnaa5c7cyicclcaksva")))

(define-public crate-syds-0.1.2 (c (n "syds") (v "0.1.2") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0dnr196yn4nnnl9wdcfbyaypnb9jigl6v14v82adgbq6k05wfldy")))

(define-public crate-syds-0.2.0 (c (n "syds") (v "0.2.0") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "08qp5ckscjgf8p1rfwfckhkya9qw113s7nh4hrk4fv9vqknavwzi")))

(define-public crate-syds-0.2.1 (c (n "syds") (v "0.2.1") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0cbv8zv6c7vid6sqwhcqvm367h9vmasifn3p1hzkg0aqmdjrbgx3")))

