(define-module (crates-io sy nf synfx-dsp) #:use-module (crates-io))

(define-public crate-synfx-dsp-0.5.0 (c (n "synfx-dsp") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ysn3y9jl340mwqf12ci1cmy5a59z6h0hd9a4a3ch7i71234finl")))

(define-public crate-synfx-dsp-0.5.1 (c (n "synfx-dsp") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0na79lx0mnx1xwb5fj67x473p99z26rdhfvcxjcqdwy6p3fwjmbq")))

(define-public crate-synfx-dsp-0.5.2 (c (n "synfx-dsp") (v "0.5.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1hb20irnf9kzys2rw0m53c4rz90kg933fdsc07gdq5w1nb28lrvy")))

(define-public crate-synfx-dsp-0.5.3 (c (n "synfx-dsp") (v "0.5.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1i3pb4faak08ab0qgn61chv1flzbqgsd3dcn7fsdf9fyj531hn31")))

(define-public crate-synfx-dsp-0.5.4 (c (n "synfx-dsp") (v "0.5.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1s3fzh3ir6965dmi938ndq3s715nxydlfcs2jnkhlp1hi1rlvjcf")))

(define-public crate-synfx-dsp-0.5.5 (c (n "synfx-dsp") (v "0.5.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0w9y142ysdsvdy2z2ippkqlvg17k5ni72a43ssk7w0cx0c3x27jq")))

(define-public crate-synfx-dsp-0.5.6 (c (n "synfx-dsp") (v "0.5.6") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1pz5b943vrgrr4xff2hjrz29h2650j8i3d8xqvasbjbm0vxpm2hs")))

