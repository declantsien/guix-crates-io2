(define-module (crates-io sy nf synfuzz) #:use-module (crates-io))

(define-public crate-synfuzz-0.0.0 (c (n "synfuzz") (v "0.0.0") (h "0v16al7m4dmkb5sxcmb3dxy1aphfwrdxsf6bryk258aprdpxcgfd")))

(define-public crate-synfuzz-0.1.0 (c (n "synfuzz") (v "0.1.0") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 2)))) (h "1vh11qaizx1mymr559r4f2l8dlllchyb76yd8frj5x6amrpkb0sw")))

