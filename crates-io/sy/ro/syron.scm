(define-module (crates-io sy ro syron) #:use-module (crates-io))

(define-public crate-syron-0.1.0 (c (n "syron") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)))) (h "166c3wvac553wpq00g3kr7zalqy4442njy2zx1lblda2x695dw32")))

(define-public crate-syron-0.1.1 (c (n "syron") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)))) (h "0rja43cf14y29z74h4z0w3gbgi5qa02y3szimkn0gpqxhq84fp71")))

