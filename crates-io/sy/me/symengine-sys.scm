(define-module (crates-io sy me symengine-sys) #:use-module (crates-io))

(define-public crate-symengine-sys-0.1.0 (c (n "symengine-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "18i6ybwdwgwrnqa8q9d7rl55kqd9qfzpfpbra8sai61d558ijrcy")))

(define-public crate-symengine-sys-0.1.0+1 (c (n "symengine-sys") (v "0.1.0+1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0cnykh0k27b4nk0igdci30c56rlwbbnafycz5cbqsh4ax1zx611h")))

