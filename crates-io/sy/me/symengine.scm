(define-module (crates-io sy me symengine) #:use-module (crates-io))

(define-public crate-symengine-0.1.0 (c (n "symengine") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "1rmyvxi2hw9xil6kr4f0gvmfjsfsh4rngnjmgk95rq2x2dclr6il") (f (quote (("default" "serde"))))))

(define-public crate-symengine-0.1.1 (c (n "symengine") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "0wqy036rxn360ii7pl228g0jywjys6xizsyxy6l6w02kxd1x8p6c") (f (quote (("default" "serde"))))))

(define-public crate-symengine-0.1.2 (c (n "symengine") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "1b2f0bm9q8qrs0yikw4p2hxal98hgy2zp8hgwa41hzilmwcb8cb7") (f (quote (("default" "serde"))))))

(define-public crate-symengine-0.2.0 (c (n "symengine") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "0f056yk470y96yh09dmka1v0hg01zf5klb0fmi5nks3r3pdw8045") (f (quote (("default" "serde"))))))

(define-public crate-symengine-0.2.1 (c (n "symengine") (v "0.2.1") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "0mpjfqpdj0sp4352s6y00ql8js1j730ca4920f5q4d8aabsf6hyi") (f (quote (("default" "serde"))))))

(define-public crate-symengine-0.2.2 (c (n "symengine") (v "0.2.2") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "symengine-sys") (r "^0.1") (d #t) (k 0)))) (h "0h615hqll15f31hhsj1jb9gmzd9q61kvfz7bickqjgzkv57f6gac") (f (quote (("default" "serde"))))))

