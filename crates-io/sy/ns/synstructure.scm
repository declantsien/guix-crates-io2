(define-module (crates-io sy ns synstructure) #:use-module (crates-io))

(define-public crate-synstructure-0.1.0 (c (n "synstructure") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "03fh1hp44239s46d5a1dvamkvc5qcmjg3sni92vgx2qaf2z22vk8")))

(define-public crate-synstructure-0.2.0 (c (n "synstructure") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1ba404vn45lh8j9vx6qdb3y44k0b2y1dsqbi1xj7sn3nfrn5dzph")))

(define-public crate-synstructure-0.2.1 (c (n "synstructure") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "0z34k2k8f08fsbim5cjzkzl56pn183gdhg928rvqchs3wjamafy9")))

(define-public crate-synstructure-0.3.0 (c (n "synstructure") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "051ydn3a2zlmm27x329b2viybad6vpk0s8q0xina0mwq1xjcb99s")))

(define-public crate-synstructure-0.4.0 (c (n "synstructure") (v "0.4.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "19s66yjzaf64v30xvir14ba55v7lzld3d8plrzqmz7bkbbf0hf71")))

(define-public crate-synstructure-0.4.1 (c (n "synstructure") (v "0.4.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r ">= 0.10, < 0.12") (d #t) (k 0)))) (h "0v6cbsi9gj5lhpmh4dwqh4wydr66v9gnpgikq0kxmjjk2kjzh4d8") (y #t)))

(define-public crate-synstructure-0.5.0 (c (n "synstructure") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "12zknfh0m3gidam8k5kks4dc04r4n9m8ghl876a039hspy09gk2w")))

(define-public crate-synstructure-0.5.1 (c (n "synstructure") (v "0.5.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1qamnvs0g09p4zp571fddmj2wqb87m81vxx5jl4kc63hyl7vqkl0")))

(define-public crate-synstructure-0.5.2 (c (n "synstructure") (v "0.5.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0340ckhpnr00v77clmmyik6v38mw8ldwkcjd7m7ilf7ql8s8qcfg")))

(define-public crate-synstructure-0.6.0 (c (n "synstructure") (v "0.6.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "08hacim9ws575s160m3fvkkvglmh473s6dd4pb18sb6j3vj8ryji") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.6.1 (c (n "synstructure") (v "0.6.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1g8l55j7f2a73bh0g4x6wilrsfjy8ydsi9sjz7fb9p6qwq91sxis") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.7.0 (c (n "synstructure") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "000hyhrbbqi721dpmkmjykkqjwip0gd5hk4hal2jbn25c04nc0q1") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.8.0 (c (n "synstructure") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1hbiim3npbnxz9s6x56sy650vclxqpbb7xkxdnbcyxagq38wn9zl") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.8.1 (c (n "synstructure") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0jdhls35wyy6dajsnzaz9rp6kai0yv0ggjdfy3hri313rn8xijlq") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.9.0 (c (n "synstructure") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1dx7q5np97gbpi03lsz0scvcsry11b1bij9780cflqyha1srpfw5") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.10.0 (c (n "synstructure") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1ckd9kqgkmzxmf3rx4l4iyd0akaxmbk5878wcaszdymsnkxg8dzc") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.10.1 (c (n "synstructure") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "059078h1zsgn8ka9f7dcpql6axy3hbaavh3ar61m8a4rpwwp2s3k") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.10.2 (c (n "synstructure") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0grirdkgh2wl4hf9a3nbiazpgccxgq54kn52ms0xrr6njvgkwd82") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.11.0 (c (n "synstructure") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1spqy31qcss57mciklc4nky4v778fvqs9qwdjgvnmf0hr5ichcca") (f (quote (("simple-derive"))))))

(define-public crate-synstructure-0.12.0 (c (n "synstructure") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "1xphg4zcbp5xhy25v94bwcb0fyb40qw1xd7ypa3p8kvny7ajgz5g")))

(define-public crate-synstructure-0.12.1 (c (n "synstructure") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "00x28sjln2w7vfmnh5m98d7wzbm49q2cz20jr90h834kamc5l21z")))

(define-public crate-synstructure-0.12.2 (c (n "synstructure") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1fmic6z9zmynz53z73hxnjcy42v65dzai55qxwvxms46rd6fjnsp")))

(define-public crate-synstructure-0.12.3 (c (n "synstructure") (v "0.12.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0igmc5fzpk6fg7kgff914j05lbpc6ai2wmji312v2h8vvjhnwrb7") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

(define-public crate-synstructure-0.12.4 (c (n "synstructure") (v "0.12.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "00c72ij813xsilssgya6m6f73d0s5zzsld1y26bvhk3kdzbg4d5q") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

(define-public crate-synstructure-0.12.5 (c (n "synstructure") (v "0.12.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1yplh90imfrp1rycnl18rg8l9d19xbmak1bq1g2065mady9aljj7") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

(define-public crate-synstructure-0.12.6 (c (n "synstructure") (v "0.12.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "03r1lydbf3japnlpc4wka7y90pmz1i0danaj3f9a7b431akdlszk") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

(define-public crate-synstructure-0.13.0 (c (n "synstructure") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "01jvj55fxgqa69sp1j9mma09p9vj6zwcvyvh8am81b1zfc7ahnr8") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

(define-public crate-synstructure-0.13.1 (c (n "synstructure") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "visit" "extra-traits"))) (k 0)) (d (n "synstructure_test_traits") (r "^0.1") (d #t) (k 2)))) (h "0wc9f002ia2zqcbj0q2id5x6n7g1zjqba7qkg2mr0qvvmdk7dby8") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro" "quote/proc-macro") ("default" "proc-macro"))))))

