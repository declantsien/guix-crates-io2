(define-module (crates-io sy ew syewreal_macros) #:use-module (crates-io))

(define-public crate-syewreal_macros-0.1.0 (c (n "syewreal_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "surrealdb") (r "^1.2.0") (f (quote ("protocol-ws"))) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v36l6y7fspgdmznj54xcfk4m77wd5nfyr2h0x6a6lfcqx9wrhjx")))

