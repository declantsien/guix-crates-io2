(define-module (crates-io sy ng syngestures) #:use-module (crates-io))

(define-public crate-syngestures-0.1.0 (c (n "syngestures") (v "0.1.0") (d (list (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "regex") (r ">=1.3.9, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "toml") (r ">=0.5.7, <0.6.0") (d #t) (k 0)))) (h "1kas78rj5zb6kbvl099di5nby4ds8wzn1bvdfgsbzaafy51i8i0s")))

(define-public crate-syngestures-1.0.0 (c (n "syngestures") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0wivhws2dardf9k8vzqhvnybqbd6dhcdkg7xazi9dlwc447yilaz")))

(define-public crate-syngestures-1.0.1 (c (n "syngestures") (v "1.0.1") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0zn7arya4cxn11fj9pbsknhf604id74vjs8vyshvpqw55lrmsv0y")))

