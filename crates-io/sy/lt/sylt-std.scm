(define-module (crates-io sy lt sylt-std) #:use-module (crates-io))

(define-public crate-sylt-std-0.1.0 (c (n "sylt-std") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lingon") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^2.0.0") (d #t) (k 0)) (d (n "sungod") (r "^0.3") (d #t) (k 0)) (d (n "sylt-common") (r "^0.1.0") (d #t) (k 0)) (d (n "sylt-macro") (r "^0.1.0") (d #t) (k 0)))) (h "01fpn0j474n5nq2fwf5nfrbwbpdapaidlzr11ip4rbzjc3qxchgn") (f (quote (("network" "bincode") ("default" "lingon" "network"))))))

