(define-module (crates-io sy lt sylt-macro) #:use-module (crates-io))

(define-public crate-sylt-macro-0.1.0 (c (n "sylt-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wbiyllgn077sszx94zz4zrss8mwfdjak4vfx4dj3vbcw88f5ppi")))

