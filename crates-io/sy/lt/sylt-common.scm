(define-module (crates-io sy lt sylt-common) #:use-module (crates-io))

(define-public crate-sylt-common-0.1.0 (c (n "sylt-common") (v "0.1.0") (d (list (d (n "owo-colors") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "sungod") (r "^0.3.1") (f (quote ("default_is_random"))) (d #t) (k 2)) (d (n "sylt-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "sylt-tokenizer") (r "^0.1.0") (d #t) (k 0)))) (h "077v1s14bnrssm1rqygkm25hqim71hlgxzwzl9by080jfcfjlkxj")))

