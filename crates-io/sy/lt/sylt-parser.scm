(define-module (crates-io sy lt sylt-parser) #:use-module (crates-io))

(define-public crate-sylt-parser-0.1.0 (c (n "sylt-parser") (v "0.1.0") (d (list (d (n "sylt-common") (r "^0.1.0") (d #t) (k 0)) (d (n "sylt-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "sylt-tokenizer") (r "^0.1.0") (d #t) (k 0)))) (h "0npcmwmb1j763xhm9k8sl12ypwpk6d8zvzyq9ij5qjb93gf7z6n4")))

