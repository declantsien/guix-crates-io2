(define-module (crates-io sy ma symagen) #:use-module (crates-io))

(define-public crate-symagen-0.1.0 (c (n "symagen") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3-py39" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ba6habfic31acy421d4hd60sqpxkr03czmnl64fiymjhi1wccp3")))

(define-public crate-symagen-0.1.1-dev0 (c (n "symagen") (v "0.1.1-dev0") (d (list (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3-py39" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0arpqdc4s3pabz2zjva0dxaj7baci5k5brdyhjy3z9p319a2gjr5")))

(define-public crate-symagen-0.1.1 (c (n "symagen") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3-py39" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0wg9mpd7kizanv20a8rnwf471z0zm77nwl8dmrjpl9aivygi2sfw")))

(define-public crate-symagen-0.1.2 (c (n "symagen") (v "0.1.2") (d (list (d (n "distances") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3-py39" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "166myxfczqbp49jyydf6mpmambf54x0g473v38zz9k6vznys6b79")))

(define-public crate-symagen-0.1.3 (c (n "symagen") (v "0.1.3") (d (list (d (n "distances") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3-py39" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1798pjjws11y5qpcdjnshiknhabibgap48rppyd280ibl377y1wy")))

(define-public crate-symagen-0.2.0 (c (n "symagen") (v "0.2.0") (d (list (d (n "distances") (r "^1.6.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("abi3-py311" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "18n17b76s776crmvq5pbn98d1f1lw9nd735lrji401g2qbm87cj0")))

(define-public crate-symagen-0.2.1 (c (n "symagen") (v "0.2.1") (d (list (d (n "distances") (r "^1.6.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("abi3-py311" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1gwwknmy8fr6g4c00miyn3aj3xz9yvrgwavkbg8vzhw2jvz0d4fd")))

(define-public crate-symagen-0.2.2 (c (n "symagen") (v "0.2.2") (d (list (d (n "distances") (r "^1.6.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("abi3-py311" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1ym5binwnafzv1l895ngn6s44jl97knggr3hi91r25ilhgfiviyq")))

(define-public crate-symagen-0.3.0 (c (n "symagen") (v "0.3.0") (d (list (d (n "distances") (r "^1.6.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("abi3-py311" "extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "18czi6xs8w1ad9cdkwygwjs1npmqms0gh0q1gcyil45x9wbbcahw")))

