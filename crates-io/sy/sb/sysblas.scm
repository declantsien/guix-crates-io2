(define-module (crates-io sy sb sysblas) #:use-module (crates-io))

(define-public crate-sysblas-0.1.0 (c (n "sysblas") (v "0.1.0") (h "1x0rbavh8b0q3nq94bsri378r2961p6jnl4ccd530f12832q3jc0")))

(define-public crate-sysblas-0.1.1 (c (n "sysblas") (v "0.1.1") (h "0939jas7gzv4nkgdp5q9zfp3wq2v438a347n0rqdl4aaaq1vxijm")))

