(define-module (crates-io sy sp sysproxy) #:use-module (crates-io))

(define-public crate-sysproxy-0.1.0 (c (n "sysproxy") (v "0.1.0") (d (list (d (n "interfaces") (r "^0.0.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wm3a29icgz6qx5g89gyanlvq09xxbfr5x9yqh4q0kc85bh9gza6")))

(define-public crate-sysproxy-0.2.0 (c (n "sysproxy") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.10") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "01pha1qzliysd693pckqyjwrxxb9nfaa6367pw5qrzfjd0hf1jaq")))

(define-public crate-sysproxy-0.3.0 (c (n "sysproxy") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.10") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1zzz57aqbyspj6k2az3ji6r8z2w7zn7nj7jjm6jkls4m7ffsf1wp")))

