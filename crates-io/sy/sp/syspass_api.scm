(define-module (crates-io sy sp syspass_api) #:use-module (crates-io))

(define-public crate-syspass_api-0.1.0 (c (n "syspass_api") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11bvayiq27gjizhwlxcrkyr3xch3h3qk725nz08yc9fsrjcvl6zd")))

(define-public crate-syspass_api-0.1.1 (c (n "syspass_api") (v "0.1.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iam53c7vhjdlpinn6z5rririga6bmm0sawbmmiafch5yl7di5wk")))

(define-public crate-syspass_api-0.1.2 (c (n "syspass_api") (v "0.1.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "098xizh1kg4xblklsw337v5hxr80xm41s65zhpmkww300b9kbrdh")))

(define-public crate-syspass_api-0.1.3 (c (n "syspass_api") (v "0.1.3") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cic8azhlbq8lm6hkx73bqys35ssk410i0kc2gqjjj395mgcb2gm")))

(define-public crate-syspass_api-0.1.4 (c (n "syspass_api") (v "0.1.4") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1va780a2mrvvrgm59mw8ga9q8fp787fpv0xhi2jqhzx1wxna5mf6")))

(define-public crate-syspass_api-0.1.5 (c (n "syspass_api") (v "0.1.5") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qv8r0wppys78sbzpzpwb28lqmbdy4sq7q7ljgl90dqr8in01n64")))

(define-public crate-syspass_api-0.1.6 (c (n "syspass_api") (v "0.1.6") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0anc5zs2qfx5c1xnvp0jakrd07k1aa2flb5w58xx9p9glrs2v8bd")))

(define-public crate-syspass_api-0.1.7 (c (n "syspass_api") (v "0.1.7") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k68ncqqdpwszc09ajv0d6qbgwygbshaj7f2bvq7fal05d3ss666")))

