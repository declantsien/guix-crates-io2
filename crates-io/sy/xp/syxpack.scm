(define-module (crates-io sy xp syxpack) #:use-module (crates-io))

(define-public crate-syxpack-0.1.0 (c (n "syxpack") (v "0.1.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0nxlpx3f5z5wx60bxgm5d8fxxzrampprfl9p18lbpmsdvw6qk7y3") (y #t)))

(define-public crate-syxpack-0.2.0 (c (n "syxpack") (v "0.2.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1n06p3igpv2hnk2ymf3n5rpxa15cpl7y1z7564y5g8ab5nfcc59s")))

(define-public crate-syxpack-0.3.0 (c (n "syxpack") (v "0.3.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "16mf9p0h91hkicqrh175r8xmnfd7m40pii1v4f680p6afqf9ap1d")))

(define-public crate-syxpack-0.3.1 (c (n "syxpack") (v "0.3.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "11bfdgp500ya2whz5xhja19asg38jh6ap9vk6kpfmwr0crsl0qhk")))

(define-public crate-syxpack-0.4.0 (c (n "syxpack") (v "0.4.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0xl8zqdx5khafkycxiqq2sspwbpfmwlzqkds5m2mimwb0y8irw7l")))

(define-public crate-syxpack-0.4.1 (c (n "syxpack") (v "0.4.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0dkz2nr5yakrkwd5ndiasjkyqm80bsp2hmykv3dijdzka7dn3k9s")))

(define-public crate-syxpack-0.5.0 (c (n "syxpack") (v "0.5.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0w4vjvsqmby985gahc94x2x9wi5nnhva90agz3pqvrq0kv1487qq")))

(define-public crate-syxpack-0.6.0 (c (n "syxpack") (v "0.6.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0b6s51yhzwqfx3zcjal5827n4s6f4hy3w6xmp1wl1nzl23hvbamq")))

(define-public crate-syxpack-0.7.0 (c (n "syxpack") (v "0.7.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16zn7av3yii7bmbrwyfvf8in7f8mrlwix3h8jk20ag30nig9nzyy")))

(define-public crate-syxpack-0.7.1 (c (n "syxpack") (v "0.7.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1p7z03himbp4dvfbh7bc89p8azg83q9rvhammq3bcyvwgkgv3yhb")))

(define-public crate-syxpack-0.7.2 (c (n "syxpack") (v "0.7.2") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0srg8mkp6flykc8249sfahl0zq4p48d7gajlb93b5di5rrw7gk3w")))

(define-public crate-syxpack-0.8.0 (c (n "syxpack") (v "0.8.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1hagz1g5yczqfljwznm3w608rnkw8lx2q7aikhabby3f2449hkq9")))

(define-public crate-syxpack-0.9.0 (c (n "syxpack") (v "0.9.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1fbrqij0a41c9in63i3izddqvlc3aybw044y3hmi331mz9508nrx")))

(define-public crate-syxpack-0.10.0 (c (n "syxpack") (v "0.10.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1i0ya41m29rnfrpshnnflrh3pqds27m9bb9y2r1ga8mrxi8gf9m9")))

(define-public crate-syxpack-0.11.0 (c (n "syxpack") (v "0.11.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0gs1pw9lzi8q7i52ssms38nvga3brmmag5ywy05wa0l65sg2dixs")))

(define-public crate-syxpack-0.11.1 (c (n "syxpack") (v "0.11.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0zb5fm439n9x0l9bm3jrdh91k1v3hfbdk2a76dcw719kig1xbifg")))

(define-public crate-syxpack-0.12.0 (c (n "syxpack") (v "0.12.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nybble") (r "^0.1.0") (d #t) (k 0)))) (h "0p89zkbv91h2vmy9pnc6vi5czgnndlasbxb56qv56c9rvfn0s6n0")))

(define-public crate-syxpack-0.13.0 (c (n "syxpack") (v "0.13.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nybble") (r "^0.1.0") (d #t) (k 0)))) (h "0j62m6q0fbqdp2cp7dp6ddc7jb64hkk1fl4x6j6q0jjqd2jwlhys")))

(define-public crate-syxpack-0.14.0 (c (n "syxpack") (v "0.14.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nybble") (r "^0.1.0") (d #t) (k 0)))) (h "1s2c9zglh6q1q5f15i9qdrw406ad39wvhmpq6n6fnz06in5rc3py")))

(define-public crate-syxpack-0.15.0 (c (n "syxpack") (v "0.15.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nybble") (r "^0.1.0") (d #t) (k 0)))) (h "0pk3kw87xxxkpvwaqlia1kjd3kdhhdvmar1hkpf9n9is3kj2zsw2")))

(define-public crate-syxpack-0.16.0 (c (n "syxpack") (v "0.16.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nybble") (r "^0.1.0") (d #t) (k 0)))) (h "14wdasa0hm8ymqa35ccbg2pphw5qvkfl8b92g0kvnl3sjzxw72c0")))

