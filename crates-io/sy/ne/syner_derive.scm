(define-module (crates-io sy ne syner_derive) #:use-module (crates-io))

(define-public crate-syner_derive-0.1.0 (c (n "syner_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wks6mjnxvyxay8sprsdi5wpzgrrfa6nzlii1ydz1lcqkmw32sns")))

(define-public crate-syner_derive-0.2.0 (c (n "syner_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0cinlq42096kfl3cmnnsfva462igqk3gbhgzb22nidr520ci2ym4")))

(define-public crate-syner_derive-0.2.1 (c (n "syner_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zzxidazb6y9w1axfdswfiqgzn41rpzs5ps91i8x2j6lhxz73kkg")))

(define-public crate-syner_derive-0.2.2 (c (n "syner_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18k51302rs8aybgm80ib8q0svdpss15i7zaixi76rfyqjbd8imj3")))

(define-public crate-syner_derive-0.3.0 (c (n "syner_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hy697skqrsp77dfqiq0b861n7sx2rf8yigshpgl4qcjma5b2s3z")))

