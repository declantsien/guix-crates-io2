(define-module (crates-io sy ne syner) #:use-module (crates-io))

(define-public crate-syner-0.1.0 (c (n "syner") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syner_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pd7f5gpczin8f4jxh4azr8x1y3qc9hhsyma3wrbzgm9ipv6scmc")))

(define-public crate-syner-0.2.0 (c (n "syner") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syner_derive") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1lxhb8gg45c2z0fqsyhz4nrwzq61z9mldr7pa5riiif0phhb5z12")))

(define-public crate-syner-0.2.1 (c (n "syner") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syner_derive") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vrhyr0nval0mhw8jlaby3apmk645inwk4qlmj77md6sjz4s5c2i")))

(define-public crate-syner-0.2.2 (c (n "syner") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syner_derive") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0jd7rkg8pi025jm59jqyq3l3q51jdnif54bzafxgsz240s0rk210")))

(define-public crate-syner-0.3.0 (c (n "syner") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syner_derive") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "15rsga357q6phs1gnkgaqwcgaicv0wvvxdvc4v23m51pbiab33y7")))

