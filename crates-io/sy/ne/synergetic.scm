(define-module (crates-io sy ne synergetic) #:use-module (crates-io))

(define-public crate-synergetic-0.1.0 (c (n "synergetic") (v "0.1.0") (d (list (d (n "async-task") (r "^4.3.0") (k 0)) (d (n "spin") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "0sjfmzva1zsgixy3cp0qr6lr7hmdmarj341m0hlb9sjd2c7b4lbx") (f (quote (("default" "spin")))) (y #t) (s 2) (e (quote (("spin" "dep:spin"))))))

(define-public crate-synergetic-0.1.1 (c (n "synergetic") (v "0.1.1") (d (list (d (n "async-task") (r "^4.3.0") (k 0)) (d (n "spin") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "0ypr5x6lnflxkywhbkymrm0pzcw6385a5p45jwnpxjmdiaqika1d") (f (quote (("default" "spin")))) (s 2) (e (quote (("spin" "dep:spin"))))))

(define-public crate-synergetic-0.1.2 (c (n "synergetic") (v "0.1.2") (d (list (d (n "async-task") (r "^4.3.0") (k 0)) (d (n "spin") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "0k1haqgipayn5nqaazr9xb0q6920ql5pv62kmc3py6nkz27ryv57") (f (quote (("default" "spin")))) (s 2) (e (quote (("spin" "dep:spin"))))))

