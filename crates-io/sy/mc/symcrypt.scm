(define-module (crates-io sy mc symcrypt) #:use-module (crates-io))

(define-public crate-symcrypt-0.1.0 (c (n "symcrypt") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "symcrypt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "182p536n4q24nmsr3pjgryl0xi4s4jfsf12q58n7f76a035i2ahi")))

(define-public crate-symcrypt-0.1.1 (c (n "symcrypt") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "symcrypt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1688gaj922pc0ljb4yz821lmcr2c9jq2nqidjzcy3b7mdmfizww2")))

(define-public crate-symcrypt-0.1.2 (c (n "symcrypt") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "symcrypt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0ljj78vi92q574b94147a37hbfyckxv7nf9gxr62l7msda9j4bak")))

(define-public crate-symcrypt-0.1.3 (c (n "symcrypt") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "symcrypt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0rs3vfwand5m082kby7sbv9z1y51b9j4dm0yp9hy7fd9wxf98c6v")))

(define-public crate-symcrypt-0.2.0 (c (n "symcrypt") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "symcrypt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1149r42fiycakcsw7b3hn7rw09hqiywka33xs0m2h58l24jv7hlp")))

