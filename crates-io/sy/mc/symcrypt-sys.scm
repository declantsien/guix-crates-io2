(define-module (crates-io sy mc symcrypt-sys) #:use-module (crates-io))

(define-public crate-symcrypt-sys-0.1.0 (c (n "symcrypt-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "121rpddziwkwk67ph6kkrxzm3pj7g94123nipl4cl2z1lh6hckx1")))

(define-public crate-symcrypt-sys-0.1.1 (c (n "symcrypt-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0dwxjzn24b797l0hws1razxyg3a6a6qk4jrr7y5cyinmxbb2zhlw")))

(define-public crate-symcrypt-sys-0.1.2 (c (n "symcrypt-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ik4d2w1aswdcv123lpawal1r51b64dhskaqkfw5rsk03h0ji5dr")))

(define-public crate-symcrypt-sys-0.2.0 (c (n "symcrypt-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0sq75cs1l6f30mhil8ygbkshl3ky7nhz7lk26h2g34hxdxf9xbzr")))

