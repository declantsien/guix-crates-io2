(define-module (crates-io sy mc symcode) #:use-module (crates-io))

(define-public crate-symcode-0.1.0 (c (n "symcode") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "crczoo") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "permutator") (r "^0.4.0") (d #t) (k 0)) (d (n "visioncortex") (r "^0.5") (d #t) (k 0)))) (h "1k7s47q3zy5pg9hnwdlgxxqn2bbwvqr8d18jqvgjfrj020qk0qfa")))

