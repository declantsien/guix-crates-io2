(define-module (crates-io sy kl sykl) #:use-module (crates-io))

(define-public crate-sykl-0.1.0 (c (n "sykl") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.9.0") (f (quote ("json" "charsets"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q0pyic601vxksf48k3xqqzyjssh5mn6rv4516nkx1mawqsql9ym")))

