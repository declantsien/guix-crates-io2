(define-module (crates-io sy sr sysrepo) #:use-module (crates-io))

(define-public crate-sysrepo-0.1.0 (c (n "sysrepo") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "07kvvaqhmg5vh8dfsanh6z6lzx1gqv8awwrbgnxln6q87xspqkvr")))

(define-public crate-sysrepo-0.2.0 (c (n "sysrepo") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.70") (d #t) (k 0)))) (h "05lywyjn4ihsh69ab3p9ss822dsjb14iqc43nmvkbrhiz7d6acfb")))

(define-public crate-sysrepo-0.3.0 (c (n "sysrepo") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1mqzs6rvr047gymrcf4v5v2x0zp7f70s5cm999naz673w3m7ibvf")))

(define-public crate-sysrepo-0.4.0 (c (n "sysrepo") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "libyang2-sys") (r "^0.6") (d #t) (k 0)) (d (n "yang2") (r "^0.7") (d #t) (k 0)))) (h "1jlybl4n026h3gmqq9rnxb92b25x7qjmlb3w387gyyliajmzwq96")))

