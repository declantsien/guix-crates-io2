(define-module (crates-io sy sr sysreq) #:use-module (crates-io))

(define-public crate-sysreq-0.1.0 (c (n "sysreq") (v "0.1.0") (d (list (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gnsj0jxnwcfb5cr44kx8qgkbn0zm5890hzy8cqr3by72sys0flc") (y #t)))

(define-public crate-sysreq-0.1.1 (c (n "sysreq") (v "0.1.1") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0383016108clm1z1nd6gml9m80jkqj66qz3kdicnlkwwyp2vzyz0")))

(define-public crate-sysreq-0.1.2 (c (n "sysreq") (v "0.1.2") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0imijhrgdgf2m155miaqza896c4iyhwzb8km7qgq75z2k0fb2mqf")))

(define-public crate-sysreq-0.1.3 (c (n "sysreq") (v "0.1.3") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "19c1yy5z15msxw81zry3il2r1d2c2nsc4lmnwh45bh1ddgcax0lm")))

(define-public crate-sysreq-0.1.4 (c (n "sysreq") (v "0.1.4") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1hyfc4p2mx74gr047yl5j81m6bf249wsjspdxcpj4x2qn6vzfz0r")))

(define-public crate-sysreq-0.1.5 (c (n "sysreq") (v "0.1.5") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11pkwqglrd63zabl22fxb9x3l4qh6wqf4925z13vdr42bjqn3yir")))

(define-public crate-sysreq-0.1.6 (c (n "sysreq") (v "0.1.6") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "rustls-tls-native-roots"))) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "046kh743v19gv5s5acilc7c3wv5nrhdlzmb9pgdjyzqgx2iah468")))

