(define-module (crates-io sy no synom) #:use-module (crates-io))

(define-public crate-synom-0.11.0 (c (n "synom") (v "0.11.0") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing" "full"))) (k 2)) (d (n "unicode-xid") (r "^0.0.4") (d #t) (k 0)))) (h "1vb2dds1b98cik5x877qbq98w0blqhvqzy1zvjnb0wmq7y2y3v4g")))

(define-public crate-synom-0.11.1 (c (n "synom") (v "0.11.1") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing" "full"))) (k 2)) (d (n "unicode-xid") (r "^0.0.4") (d #t) (k 0)))) (h "0ikilx8xjc6yahmhrfd5dpa4jxvss804v6242c1r9p36f6awnzfs") (y #t)))

(define-public crate-synom-0.11.2 (c (n "synom") (v "0.11.2") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing" "full"))) (k 2)) (d (n "unicode-xid") (r "^0.0.4") (d #t) (k 0)))) (h "1g7540wnxdys696pcga14jkb25rya2x30g7z5nqlr7wvn2j1mqr7")))

(define-public crate-synom-0.11.3 (c (n "synom") (v "0.11.3") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing" "full"))) (k 2)) (d (n "unicode-xid") (r "^0.0.4") (d #t) (k 0)))) (h "1dj536sh5xxhan2h0znxhv0sl6sb7lvzmsmrc3nvl3h1v5p0d4x3")))

