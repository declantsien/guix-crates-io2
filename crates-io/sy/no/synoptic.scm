(define-module (crates-io sy no synoptic) #:use-module (crates-io))

(define-public crate-synoptic-0.1.0 (c (n "synoptic") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1lqc72n2dp9810cv31zm2ibvdcr6w20p4mla8mzmqwdxqsp4g742")))

(define-public crate-synoptic-0.2.0 (c (n "synoptic") (v "0.2.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0lgar044banwnw3lx1rcxpfcksr4lpsgsxzdb43n3ccq3c02pf8k")))

(define-public crate-synoptic-0.3.0 (c (n "synoptic") (v "0.3.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1cdyynmggigddl4fw2hgskx2rwj9rb9k0bv8dmm6mwwv18hq2pdm")))

(define-public crate-synoptic-0.3.1 (c (n "synoptic") (v "0.3.1") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1cc5zx82syjnmbq8pgh8d9f8yzxbll54y91riqsx69yw9a0h5qgh")))

(define-public crate-synoptic-0.3.2 (c (n "synoptic") (v "0.3.2") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1mjddw3l93j1csc167n4fkx50smb6rj54nlvxs8j41rk6yixwc51")))

(define-public crate-synoptic-0.3.3 (c (n "synoptic") (v "0.3.3") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1sa046qm9zrhn8cwdafnsd61svdw3w82120ghp5sxfyfhhx748j8")))

(define-public crate-synoptic-1.0.0 (c (n "synoptic") (v "1.0.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1qcbyn59sr6gkw97c1cjiz40kn1my9ban5rv8j66rg78dvbc33lf")))

(define-public crate-synoptic-1.0.1 (c (n "synoptic") (v "1.0.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1wfcpgfd2hcd8p4dsc8w5x7aycbxax81dikx6pdzvx0qq8kgbzaw")))

(define-public crate-synoptic-1.0.2 (c (n "synoptic") (v "1.0.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1mrszavawn5b7p0vdvkii1w3pv5r7vr7qd71ayhgczc2b65b5in8")))

(define-public crate-synoptic-1.0.3 (c (n "synoptic") (v "1.0.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0vys1djbgbfihvf4cszkwhvxc2l6i7h75rzwvvww2jlmfv0k6qas")))

(define-public crate-synoptic-1.1.0 (c (n "synoptic") (v "1.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1myj51vxlnzv8n069lqx1yqb0cyifyac9g6g56zvyl91h28a47n8")))

(define-public crate-synoptic-1.2.0 (c (n "synoptic") (v "1.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "06lpkg2z8fag4i39nc99jfviknbc3lx7z3ppy4y5j59ma9y0w7p6")))

(define-public crate-synoptic-2.0.0 (c (n "synoptic") (v "2.0.0") (d (list (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "lliw") (r "^0.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "11ijdl4yfvnx46a5isc43gxb2dxrm8grsa0a6ysqkl67f5bq0pg5")))

