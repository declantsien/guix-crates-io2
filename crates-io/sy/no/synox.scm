(define-module (crates-io sy no synox) #:use-module (crates-io))

(define-public crate-synox-0.1.0 (c (n "synox") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0x4pdgb6vzdiy1d4y0fi773lpknmqmdpnflgs5rcdadagqxvhjli")))

