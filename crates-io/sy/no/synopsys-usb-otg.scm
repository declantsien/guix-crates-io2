(define-module (crates-io sy no synopsys-usb-otg) #:use-module (crates-io))

(define-public crate-synopsys-usb-otg-0.1.0 (c (n "synopsys-usb-otg") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "stm32ral") (r "^0.3.1") (f (quote ("stm32f429"))) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1c4dnd2rxmknym29ys9ml0hnfc1wddglic72vwrjzka6x6j2rkbb") (f (quote (("stm32f429xx" "cortex-m") ("stm32f401xx" "cortex-m" "fs") ("hs") ("gd32vf103xx" "riscv" "fs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.2.0 (c (n "synopsys-usb-otg") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "stm32ral") (r "^0.3.1") (f (quote ("stm32f429"))) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1asy13lgcvqyf0vzyr9zrml74aq21ynjdnsa9kz792lmv0r2y76x") (f (quote (("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.2.1 (c (n "synopsys-usb-otg") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1krw1pssl2klijss0ima2mx3gig9g2qp40hr87dsxs78f65sbckk") (f (quote (("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.2.2 (c (n "synopsys-usb-otg") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0mb25jkj71jyw7wqlv2spzgms3pl4plgkv6lpfl49n0g213g3ir5") (f (quote (("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.2.3 (c (n "synopsys-usb-otg") (v "0.2.3") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0lqxavi7jg37yyd46bygma3d2339d873kqh27cymnrr3y7f7c5j6") (f (quote (("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.2.4 (c (n "synopsys-usb-otg") (v "0.2.4") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rc6cw871rnvp8mzdll53llx3vh209j69hq3zvzmpxi9zsq6q8fi") (f (quote (("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.3.0 (c (n "synopsys-usb-otg") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "16vw0fi7x4mkw0p68ij07gfn33a2w7dq34g3dp14yaby9lyvk6ar") (f (quote (("xcvrdly") ("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.3.1 (c (n "synopsys-usb-otg") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "08h9p1v0n6mfwf7c6vsxd8kdxpxjkzjadb0nzk3f6a5kgcpz6gcw") (f (quote (("xcvrdly") ("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.3.2 (c (n "synopsys-usb-otg") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0i8v4bzz06idc6p42vq9n3lv1fn6fcpw94ij09ilizdilw3kg3v7") (f (quote (("xcvrdly") ("hs") ("fs"))))))

(define-public crate-synopsys-usb-otg-0.4.0 (c (n "synopsys-usb-otg") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1zm60sl4g1lpyxgzkf91agi0q5gggb14h6mjv12rb4qnncixaj79") (f (quote (("xcvrdly") ("hs") ("fs"))))))

