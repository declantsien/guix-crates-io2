(define-module (crates-io sy no syno_api) #:use-module (crates-io))

(define-public crate-syno_api-0.1.0 (c (n "syno_api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vxx4v5zjwzx77pn383lgm8s705lgrnm2l03n96szlh087hwzj1r") (f (quote (("error") ("dto") ("default" "dto" "error" "serde")))) (s 2) (e (quote (("serde" "dto" "dep:serde")))) (r "1.60.0")))

(define-public crate-syno_api-0.2.0 (c (n "syno_api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "106gwsprq9yh8kyafzalkq5b28zrflviqnljmsv4vydssdbgvwqz") (f (quote (("error") ("dto") ("default" "dto" "error" "serde")))) (s 2) (e (quote (("serde" "dto" "dep:serde")))) (r "1.60.0")))

(define-public crate-syno_api-0.3.0 (c (n "syno_api") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dbm3hbfps0qxdkl9nzb7c623p64w0hw3c08kdcpv9hav2v0vf13") (f (quote (("error") ("dto") ("default" "dto" "error" "serde")))) (s 2) (e (quote (("serde" "dto" "dep:serde")))) (r "1.60.0")))

