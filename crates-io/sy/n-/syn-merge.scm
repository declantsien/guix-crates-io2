(define-module (crates-io sy n- syn-merge) #:use-module (crates-io))

(define-public crate-syn-merge-0.0.0 (c (n "syn-merge") (v "0.0.0") (d (list (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1") (k 2)) (d (n "similar") (r "^2.3") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (k 2)))) (h "081s29dkmirj240a0d8i8ls3ml3hs6gdis926ia69ic8jhh0kwi6") (f (quote (("std") ("default" "std")))) (r "1.56")))

