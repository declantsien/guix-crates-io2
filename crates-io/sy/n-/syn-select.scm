(define-module (crates-io sy n- syn-select) #:use-module (crates-io))

(define-public crate-syn-select-0.1.0 (c (n "syn-select") (v "0.1.0") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0b5m6hdvfbggyna1b3z1mfm83yqigqlk2l0vl5583wn90dcldb10")))

(define-public crate-syn-select-0.1.1 (c (n "syn-select") (v "0.1.1") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1aaxsyy90swxa1jzsmzk1lb9y58k0ri4528l46jrj721dbp10z5l")))

(define-public crate-syn-select-0.1.2 (c (n "syn-select") (v "0.1.2") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "07ndq5pm8llb86v636zgjla634y9fgp3x3vz8z7nly9vr3qvvcmf")))

(define-public crate-syn-select-0.1.3 (c (n "syn-select") (v "0.1.3") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "01yacdsa2biwpv6qdq889cr2yxn4fvxbbli4yjjv0nvq9r6vyvqv")))

(define-public crate-syn-select-0.1.4 (c (n "syn-select") (v "0.1.4") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1a1k8c939wg7j9hmvxmwacw480xk3p5b5rrsix3mpvm6kbz9grza")))

(define-public crate-syn-select-0.2.0 (c (n "syn-select") (v "0.2.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "03nadmbf66i22jjvq89pv5rx441l31z66r06nw7nyfanzxjqr4dk")))

(define-public crate-syn-select-0.2.1 (c (n "syn-select") (v "0.2.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0j5slk26c5d0q3kh265dlifng33s4sg2nibsi85467c2c1gx0hak")))

(define-public crate-syn-select-0.3.0 (c (n "syn-select") (v "0.3.0") (d (list (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1d4bjfx7h7ch6p7qhrl01xhsfj4sw1320rmgphl5lqp2j4kl097a")))

