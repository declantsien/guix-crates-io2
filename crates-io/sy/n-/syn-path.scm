(define-module (crates-io sy n- syn-path) #:use-module (crates-io))

(define-public crate-syn-path-1.0.0 (c (n "syn-path") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "printing"))) (k 2)))) (h "0bvyka2bikrb3ph6sm83w6mlm7a5vw4fb2ld59gks268zdpv9gpi") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro") ("default" "proc-macro"))))))

(define-public crate-syn-path-1.0.1 (c (n "syn-path") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "printing"))) (k 2)))) (h "0p282nahm5s6h1pq5jnwv3k777d8yln1af3hgk2gfv86x7plz869") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro") ("default" "proc-macro"))))))

(define-public crate-syn-path-2.0.0 (c (n "syn-path") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing" "printing"))) (k 2)))) (h "0c55dilyv4m7y8drn0c4ag1xx3shxzg9424l8nqpn5z97d54w2y4") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro") ("default" "proc-macro"))))))

