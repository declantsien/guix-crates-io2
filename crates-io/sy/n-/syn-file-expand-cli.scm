(define-module (crates-io sy n- syn-file-expand-cli) #:use-module (crates-io))

(define-public crate-syn-file-expand-cli-0.1.1 (c (n "syn-file-expand-cli") (v "0.1.1") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-file-expand") (r "^0.1.0") (d #t) (k 0)))) (h "0pcm9xqi8zq27dq7xqzcakd1vfdrv839v8hf61mxxf8r9bpi1bra")))

(define-public crate-syn-file-expand-cli-0.1.2 (c (n "syn-file-expand-cli") (v "0.1.2") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-file-expand") (r "^0.1.0") (d #t) (k 0)))) (h "04xlrl109v4axp259vy1pr2pz2anzvj7mw490dafjm6ax3ghr6ag")))

(define-public crate-syn-file-expand-cli-0.2.0 (c (n "syn-file-expand-cli") (v "0.2.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.10") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-file-expand") (r "^0.2.0") (d #t) (k 0)))) (h "0krl4l52adbvaydh1w9jgl15yji3nkrk9yi40lr5fclflsfsz82z") (f (quote (("default" "prettyplease"))))))

(define-public crate-syn-file-expand-cli-0.3.0 (c (n "syn-file-expand-cli") (v "0.3.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-file-expand") (r "^0.3.0") (d #t) (k 0)))) (h "18pl7vv7466vdqxfdcgsc34r6r455rc9lfzwgky39lnws5qxqfji") (f (quote (("default" "prettyplease"))))))

