(define-module (crates-io sy n- syn-unnamed-struct) #:use-module (crates-io))

(define-public crate-syn-unnamed-struct-0.1.0 (c (n "syn-unnamed-struct") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0s82vsj703mbdp8190rsafyxkz8q0r3h4lwgiz4m6i9s3ccr81yv")))

