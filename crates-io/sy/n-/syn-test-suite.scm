(define-module (crates-io sy n- syn-test-suite) #:use-module (crates-io))

(define-public crate-syn-test-suite-0.0.0 (c (n "syn-test-suite") (v "0.0.0") (h "15yz9q7rgxaplv0zbnanzyv1la4gmg47yq5wlkcwv3cck4qlncdm")))

(define-public crate-syn-test-suite-0.0.0+test (c (n "syn-test-suite") (v "0.0.0+test") (h "1d9ffrbgci1qjdcpvgrsg3sh24qdsdh66zcsvn5prrk05ycn3mm0") (f (quote (("all-features"))))))

