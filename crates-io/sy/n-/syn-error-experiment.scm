(define-module (crates-io sy n- syn-error-experiment) #:use-module (crates-io))

(define-public crate-syn-error-experiment-0.0.0 (c (n "syn-error-experiment") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "syn") (r "^0.14") (f (quote ("parsing"))) (k 0)))) (h "10jzs5w77ydyv8csijx21n9wcki7r6pzki4pmfkxpfq71cvqnnqz") (f (quote (("proc-macro" "proc-macro2/proc-macro" "syn/proc-macro") ("default" "proc-macro")))) (y #t)))

