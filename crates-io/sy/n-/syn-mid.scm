(define-module (crates-io sy n- syn-mid) #:use-module (crates-io))

(define-public crate-syn-mid-0.1.0 (c (n "syn-mid") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)))) (h "0y8azv2vg2mnplg44yf0gbvf2k3lk6kxwrl2p9wsp6klpjiw7z3b") (f (quote (("full" "syn/full") ("extra-traits" "syn/extra-traits") ("derive" "syn/derive") ("default" "derive" "clone-impls") ("clone-impls" "syn/clone-impls")))) (y #t)))

(define-public crate-syn-mid-0.2.0 (c (n "syn-mid") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("parsing" "printing" "proc-macro" "derive"))) (k 0)))) (h "183vy550rj50gg744xr69k9q55hmw1lm5dyrcnhyis1dz6jijvgk") (f (quote (("extra-traits" "syn/extra-traits") ("default" "clone-impls") ("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.3.0 (c (n "syn-mid") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "11w00xp5bw1ld1sbfngq5bfk4d4z0h0p3zvy3j1l594zmss9l6vv") (f (quote (("default") ("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.4.0 (c (n "syn-mid") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "1l5ignvhrjqfxa992fljb0463gqdlbqhmfd53gvczv5791vr7lwz") (f (quote (("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.5.0 (c (n "syn-mid") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "12ikg5jfklixq0wsgfl7sdzjqlxgq50ygklxy4f972hjdjgm7qvv") (f (quote (("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.5.1 (c (n "syn-mid") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "1yzc4cdmijqqrw0psp6laqagn1qv48jyh44n22w3wslhzzq26a64") (f (quote (("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.5.2 (c (n "syn-mid") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "0kg8l6b43l4ql7gjb76m5qwh8dghf5k3fjmn5gxia95703ijid8b") (f (quote (("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.5.3 (c (n "syn-mid") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "1jgslzpdf78646wafyplc39lkgwsqnh1hpd544bdnkhn19bfga5s") (f (quote (("clone-impls" "syn/clone-impls"))))))

(define-public crate-syn-mid-0.5.4 (c (n "syn-mid") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0.60") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "1vfr75gjdadzsxh2bcgpwd87zwabr0pbc57b9z88rk26fpahb8zy") (f (quote (("clone-impls" "syn/clone-impls")))) (r "1.56")))

(define-public crate-syn-mid-0.6.0 (c (n "syn-mid") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "derive"))) (k 0)))) (h "0jg0phwwy2nnm7crg26825klsa8k5pyr3klxn3gs676x12xkbp5m") (f (quote (("clone-impls" "syn/clone-impls")))) (r "1.56")))

