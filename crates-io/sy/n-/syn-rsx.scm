(define-module (crates-io sy n- syn-rsx) #:use-module (crates-io))

(define-public crate-syn-rsx-0.1.0 (c (n "syn-rsx") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrwnppavz21xgv1liafnkryd0f6j18d380xk0b3k4rva3abk2az") (f (quote (("syn-extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-syn-rsx-0.1.1 (c (n "syn-rsx") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "19yn13annrw9nshhq9cxglhs2ymqv4w2a543n2szxmi9z8pk5d3v") (f (quote (("syn-extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-syn-rsx-0.1.2 (c (n "syn-rsx") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1fspzxvx60np89ak6h2wki2fgh6j4bw73vn1knyr46k62pfxp9fb") (f (quote (("syn-extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-syn-rsx-0.2.0 (c (n "syn-rsx") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "05fsbap6am8g4bk55hjvgn1xw6rgqqmjj8k7n9wff2d297q0danw") (f (quote (("syn-extra-traits" "syn/extra-traits") ("default"))))))

(define-public crate-syn-rsx-0.3.0 (c (n "syn-rsx") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1mjdg2bk14z3ammgz34xp9n7vc5jqxb9pbm6hh31gbgl6pk5h4q2") (y #t)))

(define-public crate-syn-rsx-0.3.1 (c (n "syn-rsx") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1s33h94cyj3i850bkapazm5kl10gyxnjl1gq78v1y0a2p4lqbb1c") (y #t)))

(define-public crate-syn-rsx-0.3.2 (c (n "syn-rsx") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0l3a10imbqaaprc4ra15acs00brwvpr006knrn8jisa2zgxlf2cl")))

(define-public crate-syn-rsx-0.3.3 (c (n "syn-rsx") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0gcf39hf95p28rvmnhfk4gfhm2sdqpkb801bq11fvffi88dldisw")))

(define-public crate-syn-rsx-0.3.4 (c (n "syn-rsx") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1v61b3860m8m5p3d722jy7psz9c996vawd2bwgjfwy0v7j360p01")))

(define-public crate-syn-rsx-0.4.0 (c (n "syn-rsx") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1vr28jn3k7v9yl1ygwkwhh23kcxiz475nszyjhv9v2p7bbmgl65q")))

(define-public crate-syn-rsx-0.4.1 (c (n "syn-rsx") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1371abarqqp8vf2b6bm68sq1qdp4lz7y9cnm4jd9rf3n263wihaw")))

(define-public crate-syn-rsx-0.5.0 (c (n "syn-rsx") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0lyqsssic1il802i3rj2gkylg64rlippygzkz0s8rgsz8bflmiyi")))

(define-public crate-syn-rsx-0.6.0 (c (n "syn-rsx") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "103141n0akdpc0jl8xc1x041za2fygyzhh99hidhqyyxb5np3liw")))

(define-public crate-syn-rsx-0.6.1 (c (n "syn-rsx") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0s9jx92a2yiigdqacl82w6ic27q02pmsqzngd707fg9vb98ifblk")))

(define-public crate-syn-rsx-0.7.0 (c (n "syn-rsx") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0a21017h2qqrlrcrahdq2wfx5myrm8qxc3lr0vp7s4d0xs8r5i0j") (y #t)))

(define-public crate-syn-rsx-0.7.1 (c (n "syn-rsx") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0m7qshwi4d2bk2zracvrkk6b1rq1i8ha24dz0jx8sbfapkkiaki7")))

(define-public crate-syn-rsx-0.7.2 (c (n "syn-rsx") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0r1v8r94r3ya4lsdzh54646xrlcf2lclwjaph0155q52s6jwq335")))

(define-public crate-syn-rsx-0.7.3 (c (n "syn-rsx") (v "0.7.3") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1x1ac4nv3h04fw7lvw86ckydzww778bcy6pr009a7pl2lv5pbzr2")))

(define-public crate-syn-rsx-0.8.0-beta.1 (c (n "syn-rsx") (v "0.8.0-beta.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13663nswpyn096xfhfgr8jnb70m12rv7iy45z05hcp1lbnx22hgz")))

(define-public crate-syn-rsx-0.8.0-beta.2 (c (n "syn-rsx") (v "0.8.0-beta.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0fp526xgpysgz7i4alflqfwdbr1wzjl0xsgzn2nc6dqc4h8dnpia")))

(define-public crate-syn-rsx-0.8.0 (c (n "syn-rsx") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1gyqgqlcp6kjfnaazh7h0xa86fqsjnk9brbxhrxlmwkzcbp9zkq3")))

(define-public crate-syn-rsx-0.8.1 (c (n "syn-rsx") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1k1cjm8hcl2h7f37fy83nwqr5lb9abzm55ld3rb3b2xdkj23wybd")))

(define-public crate-syn-rsx-0.9.0-alpha.1 (c (n "syn-rsx") (v "0.9.0-alpha.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "extrude") (r "^0.1.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mzbh1bkhf5r29ax08jidfqbb1lly9m9qa3963sb8prl30rnzszr")))

(define-public crate-syn-rsx-0.9.0-beta.1 (c (n "syn-rsx") (v "0.9.0-beta.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1cg1z7x26rfvpaiywrxwky95h0mabgrcz8s5mizn4c2zm2mc6n12")))

(define-public crate-syn-rsx-0.9.0-beta.2 (c (n "syn-rsx") (v "0.9.0-beta.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "12xcq7qfb4x5bnzhsanszmdnry5rdndzbsi33cg2gsb91c0i7das")))

(define-public crate-syn-rsx-0.9.0 (c (n "syn-rsx") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0dipiiacqy5lcw2jnksisdxv5fxhw2irjm80ngjbpsfkqy0hyyqb")))

