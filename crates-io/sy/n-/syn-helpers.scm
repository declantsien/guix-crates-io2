(define-module (crates-io sy n- syn-helpers) #:use-module (crates-io))

(define-public crate-syn-helpers-0.1.0 (c (n "syn-helpers") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qvz9igiy4ik90d1r4bk9c71rc9zgpxglfghs9sbz7lp310c3mjm")))

(define-public crate-syn-helpers-0.2.0 (c (n "syn-helpers") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wi98ncw3wi7hnvralyj527q90bz1fnv8pr65f26njdm4i8pf4jp")))

(define-public crate-syn-helpers-0.2.1 (c (n "syn-helpers") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1sxrp3bfaa3ww2hak1ksr23vy232lrbx8aaxawsap9hf4mk7jk3m")))

(define-public crate-syn-helpers-0.3.0 (c (n "syn-helpers") (v "0.3.0") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1x4qjyqrr736zz1nscvc8z4pxy4cqzy2fg5w5ry5asxj7wbfylxa")))

(define-public crate-syn-helpers-0.4.0 (c (n "syn-helpers") (v "0.4.0") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0pscy18830k65fjxnvzx0cfvviyma88zh51slazchc22zra4rm7a")))

(define-public crate-syn-helpers-0.4.1 (c (n "syn-helpers") (v "0.4.1") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1asgzr9k0j320zs8q3mqxmp3npc53m8981kpa0n9wqn3g0dib9h3")))

(define-public crate-syn-helpers-0.4.2 (c (n "syn-helpers") (v "0.4.2") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "10nakvqn9zgw0jw44fsh630shr4hansr7h1mi4hsj05w3mb7vk7z")))

(define-public crate-syn-helpers-0.4.3 (c (n "syn-helpers") (v "0.4.3") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1zb47cddapzvwbfl9f9fvd66mib4fay6h4jzjh7bm7ssf2cmfixa")))

(define-public crate-syn-helpers-0.4.4 (c (n "syn-helpers") (v "0.4.4") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1kng18dpqiwdz9274558hsj9a3pg0kiv4m93jgzrz8wr08gj2bw7") (f (quote (("syn-extra-traits" "syn/extra-traits") ("field_is_type" "syn-extra-traits"))))))

(define-public crate-syn-helpers-0.4.5 (c (n "syn-helpers") (v "0.4.5") (d (list (d (n "either_n") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0f9n2fqacj0cgzrj7j51zs25lhawl6kism5zfnxbvrs41lsija9a") (f (quote (("syn-extra-traits" "syn/extra-traits") ("field_is_type" "syn-extra-traits"))))))

(define-public crate-syn-helpers-0.5.0 (c (n "syn-helpers") (v "0.5.0") (d (list (d (n "either_n") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "179x40is5cjvr6p6nnqrk8959gmljwmr48f9gpyiwykyxvf93hsr") (f (quote (("syn-extra-traits" "syn/extra-traits") ("field_is_type" "syn-extra-traits"))))))

