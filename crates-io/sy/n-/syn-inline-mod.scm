(define-module (crates-io sy n- syn-inline-mod) #:use-module (crates-io))

(define-public crate-syn-inline-mod-0.1.0 (c (n "syn-inline-mod") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0cpl7c7y0s1a9vhzicn84szk74kam13x8b5rkac6p836426m6ppy")))

(define-public crate-syn-inline-mod-0.1.1 (c (n "syn-inline-mod") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1q06sfc1dy1s85ysjaah2dgrcqvqbfwkirryqbbibzdzq9fpal70")))

(define-public crate-syn-inline-mod-0.2.0 (c (n "syn-inline-mod") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01yqinv2w6gjqlf61jm2jmwnnn7p8pysx5gd18aqagsv8ryasl5c")))

(define-public crate-syn-inline-mod-0.3.0 (c (n "syn-inline-mod") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14n80hpg424fxpz5901wmlb0l90r68j6q8jy3lv5vpjymcw9x4jn")))

(define-public crate-syn-inline-mod-0.4.0 (c (n "syn-inline-mod") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0izwl01ln3kmynd59382k02xh6cknw9zl4vs6p0qvcrmna462dfh")))

(define-public crate-syn-inline-mod-0.5.0 (c (n "syn-inline-mod") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.0") (f (quote ("parsing" "printing" "full" "visit-mut"))) (k 0)))) (h "1sx7vbxf0bnj8mnwm30yqz9lb6wzazrbjq7vry73arsc6qszaw5n")))

(define-public crate-syn-inline-mod-0.6.0 (c (n "syn-inline-mod") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "printing" "full" "visit-mut"))) (k 0)))) (h "1yfh4pmyaq18iddcin131yqlz7a14mkk4jjkvm3fvcmpznhxr9ig")))

