(define-module (crates-io sy n- syn-codegen) #:use-module (crates-io))

(define-public crate-syn-codegen-0.1.0 (c (n "syn-codegen") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "semver") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qvi4bi15lbg1yg4ds9xk32wj01sk3hq53p5ypjiw15zkbn7i7cx")))

(define-public crate-syn-codegen-0.2.0 (c (n "syn-codegen") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "semver") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "178sphzi8gvkzpx9saz3gxipqi5cqy7i0afjgm5hng6kmsglh9jw")))

(define-public crate-syn-codegen-0.3.0 (c (n "syn-codegen") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "163d49287filgxcrjki5m4fph17qmb4ldjrwvkwj7x81hg6f1nc7")))

(define-public crate-syn-codegen-0.3.1 (c (n "syn-codegen") (v "0.3.1") (d (list (d (n "indexmap") (r "^1") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l62s7vl2azj0fszdld25yaxcr2iq8d4xgy8lqipzsiy915zb576")))

(define-public crate-syn-codegen-0.4.0 (c (n "syn-codegen") (v "0.4.0") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bc3g8acak1q9rvcbnkinb0b9d41dyq3267yhix4cw4z9y43mzrb")))

(define-public crate-syn-codegen-0.4.1 (c (n "syn-codegen") (v "0.4.1") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "19wqnapg7zf8ja3iw90r7cnk9zq169qi97l0iarvnbpvrgkba6m7")))

(define-public crate-syn-codegen-0.4.2 (c (n "syn-codegen") (v "0.4.2") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1fianjiy801q8dl308gr357r63z2ad8pym76m7gflckj5hxc1s4a") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive" "indexmap/serde" "semver/serde"))))))

