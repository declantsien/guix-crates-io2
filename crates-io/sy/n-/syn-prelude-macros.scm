(define-module (crates-io sy n- syn-prelude-macros) #:use-module (crates-io))

(define-public crate-syn-prelude-macros-0.1.0 (c (n "syn-prelude-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09dy6g6b4ybp6dxybrmhj8q476si30vr2gwxqbzqra950r1bpwx1")))

(define-public crate-syn-prelude-macros-0.1.1 (c (n "syn-prelude-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1az5kj7v649gp29l1qxdlrby0lkz9pww28xkrrcbf62qxar352zf")))

(define-public crate-syn-prelude-macros-0.1.2 (c (n "syn-prelude-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ri897q3ig9f0ivcig7zz4alxs824al3pbzlaayxjsgb5ch3srg")))

(define-public crate-syn-prelude-macros-0.1.3 (c (n "syn-prelude-macros") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10bdw18q1zwhx6wwffbd69h0x0vks2pzyh08rbcnfyd8sxkv6jwh")))

