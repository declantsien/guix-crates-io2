(define-module (crates-io sy s- sys-datetime) #:use-module (crates-io))

(define-public crate-sys-datetime-1.0.0 (c (n "sys-datetime") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0njvsnrgvfsifdy2rdsyijrgrf2a0494ms03a3g62n1f3rc1130f") (f (quote (("postgres" "sqlx/postgres") ("mysql" "sqlx/mysql")))) (y #t)))

(define-public crate-sys-datetime-1.0.1 (c (n "sys-datetime") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1lng651p3xm92hjyihm93m9y3a2nh76wm7f90445lkwys8yzrr6x") (f (quote (("postgres" "sqlx/postgres") ("mysql" "sqlx/mysql"))))))

(define-public crate-sys-datetime-1.0.2 (c (n "sys-datetime") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0") (o #t) (d #t) (k 0)))) (h "1afk2gq7pw7cs3d4pfxmv0rzncay8s6zv6za1l5gwglgdxy9x757") (f (quote (("postgres" "sqlx/postgres") ("mysql" "sqlx/mysql"))))))

