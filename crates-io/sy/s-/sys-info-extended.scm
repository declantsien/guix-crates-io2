(define-module (crates-io sy s- sys-info-extended) #:use-module (crates-io))

(define-public crate-sys-info-extended-0.1.0 (c (n "sys-info-extended") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0yfyvcwsy31xgf9rp3yij5s9rnz1pds0iiyq9zzvij8pvipgczhy") (l "info")))

(define-public crate-sys-info-extended-0.1.1 (c (n "sys-info-extended") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0zmqldr42d7sjkkdv0zfqcsd48jpzqvkjhqcww5zz7q8kl3x47z9") (l "info")))

(define-public crate-sys-info-extended-0.1.2 (c (n "sys-info-extended") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0rm74m12v99pf6023088fpqcj2cx3krqr1n2qiz3njylqkhgr035") (l "info")))

(define-public crate-sys-info-extended-0.2.0 (c (n "sys-info-extended") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "palin") (r "^0.1.0") (d #t) (k 0)))) (h "0zxapn2s23n0889kgvmxq6363ld1dj0xiddmj9i67szmyjwjdh7w") (y #t) (l "info")))

(define-public crate-sys-info-extended-0.2.1 (c (n "sys-info-extended") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "palin") (r "^0.1.0") (d #t) (k 0)))) (h "0cirfsg0qg9v9khlr4l9hswwsa9cg0vq0k58mvagaiz7yznv5drp") (y #t) (l "info")))

(define-public crate-sys-info-extended-0.2.2 (c (n "sys-info-extended") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "palin") (r "^0.2.0") (d #t) (k 0)))) (h "0284lvjmxmxr7xx4x41q2ridg54sqxy3x7k30drnpn8wwacyabbj") (l "info")))

(define-public crate-sys-info-extended-0.3.0 (c (n "sys-info-extended") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "palin") (r "^0.3.0") (d #t) (k 0)))) (h "0w11nmkcgkjy1msgb0mg3xg6kk2jrfjkr8hg2wj1k00xjnx0lrrm") (l "info")))

(define-public crate-sys-info-extended-0.4.0 (c (n "sys-info-extended") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "palin") (r "^0.4.0") (d #t) (k 0)))) (h "17kq1kp5y0n2wpxblflrns7cl2r98hq3wvzgppnhz72ba6z175qr") (l "info")))

