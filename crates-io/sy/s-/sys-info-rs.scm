(define-module (crates-io sy s- sys-info-rs) #:use-module (crates-io))

(define-public crate-sys-info-rs-0.1.1 (c (n "sys-info-rs") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1g0wz84wbzxhxqikgrld0hg7s6bzw0nhwlnz4sv6ml9am1hpk4sc")))

(define-public crate-sys-info-rs-0.1.2 (c (n "sys-info-rs") (v "0.1.2") (h "1cn57mqi1r3qhskcqs9iapvlms1pvq0gif328y2cq7r6hlvql2wa")))

