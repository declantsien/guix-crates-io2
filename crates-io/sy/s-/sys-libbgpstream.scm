(define-module (crates-io sy s- sys-libbgpstream) #:use-module (crates-io))

(define-public crate-sys-libbgpstream-0.0.1 (c (n "sys-libbgpstream") (v "0.0.1") (h "0zyhd3xpm0x6m2bwq89nw1lam7vwp3dys9s342fxfc2p8z7kkhk5") (y #t)))

(define-public crate-sys-libbgpstream-0.0.2 (c (n "sys-libbgpstream") (v "0.0.2") (h "1lrgxmrnzrbc8rw6qgrxghfnpb7gs7b1fbca27q2hsdjma8lwg56")))

