(define-module (crates-io sy s- sys-call) #:use-module (crates-io))

(define-public crate-sys-call-0.1.0 (c (n "sys-call") (v "0.1.0") (h "131q7zj37aidm49shx9psbs88jq40na1c8sn80qfd42clp0nkxs3") (y #t)))

(define-public crate-sys-call-0.1.1 (c (n "sys-call") (v "0.1.1") (h "004vafagcy1vxndbfh2fqy2gv5j2bspa5f4q660758838vrvqi05")))

