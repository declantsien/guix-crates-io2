(define-module (crates-io sy s- sys-time) #:use-module (crates-io))

(define-public crate-sys-time-0.0.1 (c (n "sys-time") (v "0.0.1") (h "0qsn7gy06kbjcd3ac600vyb598galfi12cwsahnga7ffr29anvdx")))

(define-public crate-sys-time-0.0.2 (c (n "sys-time") (v "0.0.2") (h "0b7j3i15r6f18nivgfa9mwvlhl2wjjhaqdx2nnkva8f0wbalxbzc")))

(define-public crate-sys-time-0.0.3 (c (n "sys-time") (v "0.0.3") (h "1yjkgdl6iym9w09v92655f1vxkag77y4r8n77rxm9yrrx67plf6x")))

(define-public crate-sys-time-0.0.4 (c (n "sys-time") (v "0.0.4") (h "1s8ws6vvdbjcb19wwprz9n2a1njnfinrnszm1vlzbf2rr8swynlc")))

(define-public crate-sys-time-0.0.5 (c (n "sys-time") (v "0.0.5") (h "11pyfigk4jrdlpw6mfi3y3rga65p673xzs071c05lcjlb9iasmxk")))

