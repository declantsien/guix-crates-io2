(define-module (crates-io sy s- sys-info) #:use-module (crates-io))

(define-public crate-sys-info-0.1.1 (c (n "sys-info") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "10qg2m73lxnm3qdi7gci8rrymizkxzhdb8qx6d4qf32vr7wybhaz")))

(define-public crate-sys-info-0.1.2 (c (n "sys-info") (v "0.1.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0v0xxspsb0xwzwpfmfgf1wcjp8ggcdfz9i3cyxp8mk73k95qh75k")))

(define-public crate-sys-info-0.1.3 (c (n "sys-info") (v "0.1.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0nh923rkh5jwzdvy1kq6i6mn2hfc675amxs1xlvq961qf4im2fdx")))

(define-public crate-sys-info-0.1.4 (c (n "sys-info") (v "0.1.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "07rlk2qs56bz8vcim657ni6gi0zvfp19y4pmmhyc84y857yva12j")))

(define-public crate-sys-info-0.2.1 (c (n "sys-info") (v "0.2.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1wy8s3prbwf3l5cwvj3c9ib1w0i39dcn4dx2l8nlypf81m8vn6am")))

(define-public crate-sys-info-0.2.2 (c (n "sys-info") (v "0.2.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1k5bny2qmkkjk6db1b9ssncpxcjg7ydqmckvmrz7m362ialqq00a")))

(define-public crate-sys-info-0.2.3 (c (n "sys-info") (v "0.2.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "081736h800gsfqhs8qhi549z6lyfygpxdby23bfs2ib75dii3mq5")))

(define-public crate-sys-info-0.2.4 (c (n "sys-info") (v "0.2.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "161sbbg4yc3kndm9d4jqd0n448lzjns5bf33sk3gzhv5cyah6d0i")))

(define-public crate-sys-info-0.2.5 (c (n "sys-info") (v "0.2.5") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0wxplbknwk8m7kf2sm638p7zgv1i9rrqp7snyx689lavg4qk7697")))

(define-public crate-sys-info-0.3.1 (c (n "sys-info") (v "0.3.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1wsxfqknqvmg6g3cw8sbfii5lijaarbsbn7lp08fjnzkmxizf9ji")))

(define-public crate-sys-info-0.3.2 (c (n "sys-info") (v "0.3.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "16fw7wd6qi28zb9vcplz2h8k3an9pdv6s4hbnr5llg28mcx79l3c")))

(define-public crate-sys-info-0.3.3 (c (n "sys-info") (v "0.3.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1a9kq3daxcy10ndb1sc2lc5a0av85bk49c8jr3vk54h387j6krk6")))

(define-public crate-sys-info-0.3.4 (c (n "sys-info") (v "0.3.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1jmhhzh9im98nhixhfwl4cpsj1zjnva69323v3n4k88lv19h87kk")))

(define-public crate-sys-info-0.4.0 (c (n "sys-info") (v "0.4.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0nwckjh3hyldcif6vyaznzfl28aydz69712lngxv111z7gycl7kg")))

(define-public crate-sys-info-0.4.1 (c (n "sys-info") (v "0.4.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "134781dl9zrvzbwsyfscnk438inhjzcjcv16j9n05q1s8jxqnvr4")))

(define-public crate-sys-info-0.5.0 (c (n "sys-info") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)))) (h "0kisqyr46743d9yvvdi68ayj1wp9z9jdz0nvnhnxm5hakk7l48gj")))

(define-public crate-sys-info-0.5.1 (c (n "sys-info") (v "0.5.1") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "10rnxmxmrp404c8ghbzjzbf588az763b6ql21va6pkjlir8q033f")))

(define-public crate-sys-info-0.5.2 (c (n "sys-info") (v "0.5.2") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "13y84m75sgb8y0jz0n4wiyjv8hkcj8xjgg25ih582sl10jsapsrd")))

(define-public crate-sys-info-0.5.3 (c (n "sys-info") (v "0.5.3") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "03bhv7ijv913r8q4yn18z41vw0hsm98zrmnslcjv2gxwsv4cac08")))

(define-public crate-sys-info-0.5.4 (c (n "sys-info") (v "0.5.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "12m073ysw5zp5s824bs0dsvcqsmcb04fqj3vq7pikb4hdzpi3l79")))

(define-public crate-sys-info-0.5.5 (c (n "sys-info") (v "0.5.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0lpz4ibsm3kssfc6c5ry9nxvl33q56cagd06id18235i75lg9a2l")))

(define-public crate-sys-info-0.5.6 (c (n "sys-info") (v "0.5.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "11i9q740p6gshibsvx6pfs0s3wkr995jb41k8dqii039716mjzv1")))

(define-public crate-sys-info-0.5.7 (c (n "sys-info") (v "0.5.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0hc5jhwv57wvwaksrq4qsf6qq47lw8kp4yd3yym6sslv6ixwzmkn") (l "info")))

(define-public crate-sys-info-0.5.8 (c (n "sys-info") (v "0.5.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "1pzhfxmyj0dn67smi0xqc30h8403r774rg5h45g23j62rqwzwy80") (l "info")))

(define-public crate-sys-info-0.5.9 (c (n "sys-info") (v "0.5.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "1fd709zcklb7hw78srar5na2zqn7g0idq2s7nivkqram9r3zbvyl") (l "info")))

(define-public crate-sys-info-0.5.10 (c (n "sys-info") (v "0.5.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0g22by74q93jpbpjv3c8ykdpnspdgnzqp7q6wanzqai1r543la2q") (l "info")))

(define-public crate-sys-info-0.6.0 (c (n "sys-info") (v "0.6.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "1lfzmlnbvi7iacx6pn1cda3s2vw9m0x0p02sxfawm1v3p7p0gfsb") (l "info")))

(define-public crate-sys-info-0.6.1 (c (n "sys-info") (v "0.6.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "1hsi940jj3p95jw8pv8g6y6zqw37wnqz06avnz9gzxlcrkzb63xv") (l "info")))

(define-public crate-sys-info-0.7.0 (c (n "sys-info") (v "0.7.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0facyh6hswp1i7airri8ly5kl6sv5bvkkd21vs51k2b3z22bvkz5") (l "info")))

(define-public crate-sys-info-0.8.0 (c (n "sys-info") (v "0.8.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "15apgp1gnmkknr31jb8271gq8mdh153awdjf13qdqbd1i2l7ngiz") (l "info")))

(define-public crate-sys-info-0.9.0 (c (n "sys-info") (v "0.9.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0fiqhnj1rk69rahz4077lzs8x72gv4zcyknqdg7k359k97pfrz1k") (l "info")))

(define-public crate-sys-info-0.9.1 (c (n "sys-info") (v "0.9.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)))) (h "0b759814ng0cj5a1iiqqjgrzfg9vqlpkbp6z3l76mycbp850sfhb") (l "info")))

