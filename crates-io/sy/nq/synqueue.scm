(define-module (crates-io sy nq synqueue) #:use-module (crates-io))

(define-public crate-synqueue-0.1.0 (c (n "synqueue") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "0i0m5m6fb00ry06zglr345ijn56v3pq9qwi0qdcl81a01xpqkg4d")))

