(define-module (crates-io sy sn sysnames) #:use-module (crates-io))

(define-public crate-sysnames-0.1.0 (c (n "sysnames") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 1)) (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04cymxpll9q2illdcwiy1fpwa9vg8dwvp13h0l2w5dkcrrhnja04")))

(define-public crate-sysnames-0.1.1 (c (n "sysnames") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 1)) (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1blmag19qr9hahnhkr7fagljzjxibgyn0p5kb1hc8qwk9awz2wh2")))

(define-public crate-sysnames-0.1.2 (c (n "sysnames") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 1)) (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gyfnz9b972snfrmj1h29jj2795v7lcl0q5bh89xhy61aih8pcv3")))

(define-public crate-sysnames-0.1.3 (c (n "sysnames") (v "0.1.3") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0gc1fxiqs5z9r9a7wbfkq0jvzxan93ad7famva4nffvhghyvd9s1")))

