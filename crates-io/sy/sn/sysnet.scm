(define-module (crates-io sy sn sysnet) #:use-module (crates-io))

(define-public crate-sysnet-0.1.0 (c (n "sysnet") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.19.0") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.3") (d #t) (k 0)))) (h "08ip50027pg2iyfq6hybmwxb22xadxv79jk8vgz1snk951my8psv")))

(define-public crate-sysnet-0.1.1 (c (n "sysnet") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.19.0") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.3") (d #t) (k 0)))) (h "1iq68r3sm5cdffzk4a35zl4kihpms522735xdrad26zj8fqv41g5")))

