(define-module (crates-io sy mb symbolic-sets-integers) #:use-module (crates-io))

(define-public crate-symbolic-sets-integers-0.1.0 (c (n "symbolic-sets-integers") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)) (d (n "symbolic-sets") (r "^0.1") (f (quote ("proptest"))) (d #t) (k 0)))) (h "1rdnx4s6iwqvlbgwmigz2h8a1nyl07baxxw6bnvx5sd6fyb0agkf")))

