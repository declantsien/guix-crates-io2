(define-module (crates-io sy mb symbolic_polynomials) #:use-module (crates-io))

(define-public crate-symbolic_polynomials-0.0.1 (c (n "symbolic_polynomials") (v "0.0.1") (h "0w19kd7nhgspj2qpmx4r0lm7v3slkxzfp92yp9fd9pc8y8g8cn7l")))

(define-public crate-symbolic_polynomials-0.0.2 (c (n "symbolic_polynomials") (v "0.0.2") (h "0294rvmpc2z1kdhi65f4cajqliy2p475nr0zsb6gxb0hbm984cmk")))

(define-public crate-symbolic_polynomials-0.1.0 (c (n "symbolic_polynomials") (v "0.1.0") (d (list (d (n "num") (r "^0.1.36") (k 0)))) (h "14773cmli9qp5xby7jnxcmvv8m6rxxx943y29bm03v63d8lwfhc9")))

