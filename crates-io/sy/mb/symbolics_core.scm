(define-module (crates-io sy mb symbolics_core) #:use-module (crates-io))

(define-public crate-symbolics_core-0.1.0 (c (n "symbolics_core") (v "0.1.0") (h "0ik4yjxjvdk0257ihg07qf0ln349hmlrznvgmz92ax8yhff46id6")))

(define-public crate-symbolics_core-0.1.2 (c (n "symbolics_core") (v "0.1.2") (h "0z5045dpcgq1yij7gyx83q9hjyr2wykcjydy94xq9p84bd61m7r9")))

(define-public crate-symbolics_core-0.1.4 (c (n "symbolics_core") (v "0.1.4") (h "1kp8ff357h607824a17f5k321blb5r6casfqlpmjmwb7rwjifz5z")))

(define-public crate-symbolics_core-0.1.5 (c (n "symbolics_core") (v "0.1.5") (h "0792hig2yagqphbm35m3f0krdrl0ffswz1rxfi2pv8g9j8m8v0jb")))

