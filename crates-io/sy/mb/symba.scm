(define-module (crates-io sy mb symba) #:use-module (crates-io))

(define-public crate-symba-0.1.0 (c (n "symba") (v "0.1.0") (h "05jp94j21a2c3gcfhm36avj30h4x2w157jcsqaf3g9d20z1nv36d")))

(define-public crate-symba-0.1.1 (c (n "symba") (v "0.1.1") (h "1942bbymb2zvsa59aljx13a99v38fyaw5sqnpr69qg9a0glcwdj2")))

(define-public crate-symba-0.1.2 (c (n "symba") (v "0.1.2") (h "1gksn24wrw3kn2gxz7n6xzn3xc6rifk7k8gpzffsi7hl1qfvr904")))

