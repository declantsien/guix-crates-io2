(define-module (crates-io sy mb symbol_table) #:use-module (crates-io))

(define-public crate-symbol_table-0.1.0 (c (n "symbol_table") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s3xqsh0m30vj96nw8sw0q3wbd23isppz292x5l0xr2k77nhr1ws") (f (quote (("global") ("default"))))))

(define-public crate-symbol_table-0.2.0 (c (n "symbol_table") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.6") (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "055cvvw6vi8q3qjjfyy5hdrng1mwf4sdl1hvf6vb5wkx3n6higrj") (f (quote (("global") ("default"))))))

(define-public crate-symbol_table-0.3.0 (c (n "symbol_table") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7.6") (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0ni4lvic2jv0v21k6dx8bf79kz6hqy13n7caxbv0n8hwccmng3w2") (f (quote (("global") ("default"))))))

