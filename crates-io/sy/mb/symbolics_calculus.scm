(define-module (crates-io sy mb symbolics_calculus) #:use-module (crates-io))

(define-public crate-symbolics_calculus-0.1.0 (c (n "symbolics_calculus") (v "0.1.0") (d (list (d (n "symbolics_core") (r "^0.1.2") (d #t) (k 0)))) (h "1j81z6706zr23crnjw8ly651l6ylmw2g61gap2hwyn1xj25zvqan")))

(define-public crate-symbolics_calculus-0.1.1 (c (n "symbolics_calculus") (v "0.1.1") (d (list (d (n "symbolics_core") (r "^0.1.4") (d #t) (k 0)))) (h "076g89k5035ra41q0ia7m88qbrhyc8q6lp0flm5f3gc92rdk1f8s")))

(define-public crate-symbolics_calculus-0.1.2 (c (n "symbolics_calculus") (v "0.1.2") (d (list (d (n "symbolics_core") (r "^0.1.4") (d #t) (k 0)))) (h "0ii68hk6334zp26x86cxpxrsd0vxsq7ivrlkmwd2yjvakva6kkji")))

(define-public crate-symbolics_calculus-0.1.3 (c (n "symbolics_calculus") (v "0.1.3") (d (list (d (n "symbolics_core") (r "^0.1.4") (d #t) (k 0)))) (h "1zfzsjrnbjp05saw4sfsgclfa5fhyl8qldjlq28j646z4frlxr43")))

(define-public crate-symbolics_calculus-0.1.4 (c (n "symbolics_calculus") (v "0.1.4") (d (list (d (n "symbolics_core") (r "^0.1.4") (d #t) (k 0)))) (h "15prhqfbi9racdygbcz6bkvfix2kaws41y6a99nwdbjnilbl0kv7")))

(define-public crate-symbolics_calculus-0.1.5 (c (n "symbolics_calculus") (v "0.1.5") (d (list (d (n "symbolics_core") (r "^0.1.5") (d #t) (k 0)))) (h "0yrh1s7ngd8hig2whpjgcf2x17lknsqcfwn96zvy82n129k4zam1")))

