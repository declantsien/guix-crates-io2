(define-module (crates-io sy mb symbolic-proguard) #:use-module (crates-io))

(define-public crate-symbolic-proguard-1.1.5 (c (n "symbolic-proguard") (v "1.1.5") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^1.1.5") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "01cbv811w7xg0ls9wjdb7224xjahhisf9giwk550yxi70m8z70fi")))

(define-public crate-symbolic-proguard-2.0.0 (c (n "symbolic-proguard") (v "2.0.0") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "03gni611a8ggzfm10pwdca8csf754173xzdbbfyd0glav7ngyjyr")))

(define-public crate-symbolic-proguard-2.0.1 (c (n "symbolic-proguard") (v "2.0.1") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "1qspk17sx63cakcpv4rc4dkq9p29qyd386ijx7nxi7vc94rn06iw")))

(define-public crate-symbolic-proguard-2.0.2 (c (n "symbolic-proguard") (v "2.0.2") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "12axij8gwscfljm38skg1b84hp422f5dv46ih9anf83iyxpw3572")))

(define-public crate-symbolic-proguard-2.0.3 (c (n "symbolic-proguard") (v "2.0.3") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.3") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "1fszgj5vh2zwr4nwg2jaqdgj5na0raf1zj3ddlsv41gl3j8m90p3")))

(define-public crate-symbolic-proguard-2.0.4 (c (n "symbolic-proguard") (v "2.0.4") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.4") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "10m5g8hi5jgrpkmd4mxi17r4cij645gnwp1afasiv2lk3y3h5vqj")))

(define-public crate-symbolic-proguard-2.0.5 (c (n "symbolic-proguard") (v "2.0.5") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "0ycnmd9vbv6rvynw43mswkd64xc55mc4n23gvxmr7cw7i16a0q7s")))

(define-public crate-symbolic-proguard-2.0.6 (c (n "symbolic-proguard") (v "2.0.6") (d (list (d (n "proguard") (r "^1.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.6") (d #t) (k 0)) (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "0zh33gpqilfyqlb71n0chz8qs2gz8ddwqsk3dfgny0gdn0b6zr4g")))

(define-public crate-symbolic-proguard-2.0.7 (c (n "symbolic-proguard") (v "2.0.7") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^2.0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.6.0") (d #t) (k 0)))) (h "0q5wx30bv4mp867vd80cbh02jx5jklc5sxn1v2ibl13jbj5gyqm2")))

(define-public crate-symbolic-proguard-3.0.1 (c (n "symbolic-proguard") (v "3.0.1") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^3.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6.1") (d #t) (k 0)))) (h "094k2psp0imgzj19kav6csfajngkp5cvh0g6rvvxdpgpyfn34zm2")))

(define-public crate-symbolic-proguard-4.0.0 (c (n "symbolic-proguard") (v "4.0.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^4.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6.1") (d #t) (k 0)))) (h "1cm5jsgfxfhwhp0vrd3db6n787x18lgikkmpsayajqn3xgaan4an")))

(define-public crate-symbolic-proguard-5.0.0 (c (n "symbolic-proguard") (v "5.0.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "1v86pzhv9nxxw5ni132ilpvgyyzxhr2w38vcz1k7a4ybbv5icjgz")))

(define-public crate-symbolic-proguard-5.0.1 (c (n "symbolic-proguard") (v "5.0.1") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "1dyhinb8ffvvqd64a567zaaxiyk2ay0xxjjzrkacxll9hil277qw")))

(define-public crate-symbolic-proguard-5.0.2 (c (n "symbolic-proguard") (v "5.0.2") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "19s1864b8h21ny5qg05nh9gfn9w6qlgqfqn28i0w8kkl9lvlrhhi")))

(define-public crate-symbolic-proguard-5.1.0 (c (n "symbolic-proguard") (v "5.1.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "0d4v3a3yyh0dmdhdpmxmb54rh47cypi0ri3zhfzwk57c2wsha0wp")))

(define-public crate-symbolic-proguard-5.1.1 (c (n "symbolic-proguard") (v "5.1.1") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "111bgccp309l0n481rmrzbgv1p8ypp9rpwn0i7r59p3frcksvxzb")))

(define-public crate-symbolic-proguard-5.1.2 (c (n "symbolic-proguard") (v "5.1.2") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (k 0)))) (h "1b6vfj4405jifqxhlrdk2wzyylkpb697qjr6i3ahajc8sv9qs6za")))

(define-public crate-symbolic-proguard-5.2.0 (c (n "symbolic-proguard") (v "5.2.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0vd8m18fjd163r8nlxkdi5l2dczixz2hshb8abj7v7c6ihj85czr")))

(define-public crate-symbolic-proguard-5.3.0 (c (n "symbolic-proguard") (v "5.3.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1f42zkhpxs0zabsmrgs4m745kkf44appfy4nf7hmg6lmq4rd6agh")))

(define-public crate-symbolic-proguard-5.4.0 (c (n "symbolic-proguard") (v "5.4.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0kchgprgvianh49piicc472xi2p96y2fwr4y1bv58r3r74pax6x0")))

(define-public crate-symbolic-proguard-5.5.0 (c (n "symbolic-proguard") (v "5.5.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1826h1zxfnmd99p9g62njfnqc5g0jwh2s449pafm7xvvh0idqyj2")))

(define-public crate-symbolic-proguard-5.5.1 (c (n "symbolic-proguard") (v "5.5.1") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1zw8l5xqppjcldviyz0r0xd82ni6dradf5ffwl9rcc58qcn3y060")))

(define-public crate-symbolic-proguard-5.5.2 (c (n "symbolic-proguard") (v "5.5.2") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "16mm7j3xrzi0jhalmzlmk0crpj25lgr0mc52pmm4lc2acwd3j83w")))

(define-public crate-symbolic-proguard-5.5.3 (c (n "symbolic-proguard") (v "5.5.3") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1wc61w9w2cavqrwxfs9pwmgs2rqnxn2q1xxm59nn19569pci0ivq")))

(define-public crate-symbolic-proguard-5.5.4 (c (n "symbolic-proguard") (v "5.5.4") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "198zxwknkikb5rar3c69sg6qijfmcprvkrn8d0fn14jqn4z3rdxi")))

(define-public crate-symbolic-proguard-5.5.5 (c (n "symbolic-proguard") (v "5.5.5") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0l4rmlglplgf9ky20qlwim99yaa9ryyy8prqzj7k6ij7yxps0spz")))

(define-public crate-symbolic-proguard-5.5.6 (c (n "symbolic-proguard") (v "5.5.6") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.5.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0pipxkxp4syqalawkjmblab44zwkiy8549nlf8jmjchf8j715sd3")))

(define-public crate-symbolic-proguard-5.7.0 (c (n "symbolic-proguard") (v "5.7.0") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1y817pl6xmcq6a4fxjzmvavf1kw90y046858kgl5swmabdkc4k9p")))

(define-public crate-symbolic-proguard-5.7.1 (c (n "symbolic-proguard") (v "5.7.1") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0f5v1mgfi25clb3c8fn7j27lcish1khyihi9sxpb9gv36p7alv3f")))

(define-public crate-symbolic-proguard-5.7.2 (c (n "symbolic-proguard") (v "5.7.2") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "002vzai3ql4n5gblzl52xmc6zff2vacks2nfxqpkhh2wlfa936h6")))

(define-public crate-symbolic-proguard-5.7.3 (c (n "symbolic-proguard") (v "5.7.3") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "01b9wal91p4fz2ypihwpkm6bd2xwq6wgm5p4vj1y3by8cklc9rf4")))

(define-public crate-symbolic-proguard-5.7.4 (c (n "symbolic-proguard") (v "5.7.4") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.4") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "19nxvb2k4x69gskphd1hys731ap0rq66qnqgcj2pdylgf7vrhkqh")))

(define-public crate-symbolic-proguard-5.7.5 (c (n "symbolic-proguard") (v "5.7.5") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1cxgqg1n21nl2q9527sci84plmcn89zva0zmx8y1prkz5sh37cic")))

(define-public crate-symbolic-proguard-5.7.6 (c (n "symbolic-proguard") (v "5.7.6") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1dyy7j667m6h0xdsd6k2cr705jbcv8gl8pa407szl69z06am1gzs")))

(define-public crate-symbolic-proguard-5.7.7 (c (n "symbolic-proguard") (v "5.7.7") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "02zbgb8kggdc1pxgrayfxvxcsi8qqcknn09wvnh9ajl4ivrjijsp")))

(define-public crate-symbolic-proguard-5.7.8 (c (n "symbolic-proguard") (v "5.7.8") (d (list (d (n "proguard") (r "^1.1.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^5.7.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1z907had54aablk5hr5d2m4grfwwlf0w75mkcwys5wl0s11lmyca")))

(define-public crate-symbolic-proguard-6.0.0 (c (n "symbolic-proguard") (v "6.0.0") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.0.0") (d #t) (k 0)))) (h "07kdbdq0ymd7gbyzfzd0vnfdp7lpvy6mdk65blzh9s9930mqlsg7")))

(define-public crate-symbolic-proguard-6.0.3 (c (n "symbolic-proguard") (v "6.0.3") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.0.3") (d #t) (k 0)))) (h "1dwn4a2wmnsz379p3gd6lw9qnjg0dhv4854kkqspi5rdih71id5d")))

(define-public crate-symbolic-proguard-6.0.4 (c (n "symbolic-proguard") (v "6.0.4") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.0.4") (d #t) (k 0)))) (h "054whlfimg8nh2ajsy9h7pva8gild0nfgl2igrrsvhyfssz8xlvn")))

(define-public crate-symbolic-proguard-6.0.5 (c (n "symbolic-proguard") (v "6.0.5") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.0.5") (d #t) (k 0)))) (h "1qzvlijbkg6vsyzj2xg95dwaqazvx1msmx3lmz7d7pdxy8ywkwvm")))

(define-public crate-symbolic-proguard-6.0.6 (c (n "symbolic-proguard") (v "6.0.6") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.0.6") (d #t) (k 0)))) (h "1lxn0x1blslp95wcj9sr65syf7x8hsw6wy7a3m2l38nshfj8v2im")))

(define-public crate-symbolic-proguard-6.1.0 (c (n "symbolic-proguard") (v "6.1.0") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.1.0") (d #t) (k 0)))) (h "0mcg8c2qv6smsf36nhqn6zdmy3jsrg5dkja76i7h65wf3hsm41j3")))

(define-public crate-symbolic-proguard-6.1.3 (c (n "symbolic-proguard") (v "6.1.3") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.1.3") (d #t) (k 0)))) (h "1q2cmcwgf0wmb93s0x709c04srsc9libm72l7fgfmkj50cjkfdnz")))

(define-public crate-symbolic-proguard-6.1.4 (c (n "symbolic-proguard") (v "6.1.4") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^6.1.4") (d #t) (k 0)))) (h "099yn3z1pab3qarhp51ad8a74rpc2xrdvj52l04fikyz26k933ws")))

(define-public crate-symbolic-proguard-7.1.1 (c (n "symbolic-proguard") (v "7.1.1") (d (list (d (n "proguard") (r "^2.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.1.1") (d #t) (k 0)))) (h "105fqgf6pf0s1b5vw8hzw7prk00wl24japs0s1lhm02f2fk5160i")))

(define-public crate-symbolic-proguard-7.2.0 (c (n "symbolic-proguard") (v "7.2.0") (d (list (d (n "proguard") (r "^3.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.2.0") (d #t) (k 0)))) (h "1w6w3hqrf74g4qqpkg70lgcwibzx0qjavjpbdca3ma3nyi0xr0ma")))

(define-public crate-symbolic-proguard-7.3.0 (c (n "symbolic-proguard") (v "7.3.0") (d (list (d (n "proguard") (r "^3.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.0") (d #t) (k 0)))) (h "1gajfm0xpd8gb6w7xj046laj0gz1fibay8m55lqfgdwsjd797hx3")))

(define-public crate-symbolic-proguard-7.3.1 (c (n "symbolic-proguard") (v "7.3.1") (d (list (d (n "proguard") (r "^3.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.1") (d #t) (k 0)))) (h "108bsr51wmdmw571iwcjiwh91xwrwm7h85fvka6r60fr6ln7p8nd")))

(define-public crate-symbolic-proguard-7.3.2 (c (n "symbolic-proguard") (v "7.3.2") (d (list (d (n "proguard") (r "^3.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.2") (d #t) (k 0)))) (h "09y2x0n57gmgp507ds032dgcsaw9bspnw3cvqc03zi670hmrmizs")))

(define-public crate-symbolic-proguard-7.3.3 (c (n "symbolic-proguard") (v "7.3.3") (d (list (d (n "proguard") (r "^3.0.0") (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.3") (d #t) (k 0)))) (h "17rcy6xqgxc287lldd8rf20dkc770n6pyq7q11ksx95kr0fkaidx")))

(define-public crate-symbolic-proguard-7.3.4 (c (n "symbolic-proguard") (v "7.3.4") (d (list (d (n "proguard") (r "^4.0.0") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.4") (d #t) (k 0)))) (h "1j5kwlayg68vxhzkmwrh91j4i3fc6dwkcf6gjq6ljkmbdbc7rsaq")))

(define-public crate-symbolic-proguard-7.3.5 (c (n "symbolic-proguard") (v "7.3.5") (d (list (d (n "proguard") (r "^4.0.1") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.5") (d #t) (k 0)))) (h "1zmpq4f68d892iykm2waj7q5338icbgh7yq11xcwkjb2chy1whp4")))

(define-public crate-symbolic-proguard-7.3.6 (c (n "symbolic-proguard") (v "7.3.6") (d (list (d (n "proguard") (r "^4.0.1") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "symbolic-common") (r "^7.3.6") (d #t) (k 0)))) (h "0naqc5p48j7xn7cms4ryc10r8k4al3xadpzfgy88mzca010gbqnz")))

(define-public crate-symbolic-proguard-7.4.0 (c (n "symbolic-proguard") (v "7.4.0") (d (list (d (n "proguard") (r "^4.0.1") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "symbolic-common") (r "^7.4.0") (d #t) (k 0)))) (h "19byn8j9wr6x1v1xg1axf5q7vf539pj397m81n4gc93rk4bb5ifl")))

(define-public crate-symbolic-proguard-7.5.0 (c (n "symbolic-proguard") (v "7.5.0") (d (list (d (n "proguard") (r "^4.0.1") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "symbolic-common") (r "^7.5.0") (d #t) (k 0)))) (h "014s387y56nziij7nd6kxjkv8n5cvjgmfch2s8lvgahxx4fdvgkn")))

