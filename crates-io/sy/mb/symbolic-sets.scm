(define-module (crates-io sy mb symbolic-sets) #:use-module (crates-io))

(define-public crate-symbolic-sets-0.1.0 (c (n "symbolic-sets") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 2)) (d (n "indoc") (r "^2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "proptest-attr-macro") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proptest-attr-macro") (r "^1.0") (d #t) (k 2)))) (h "0659gmskv00bmw7hlw4cp64n4qmvhyph7ighcdqh44s21znk2z19") (s 2) (e (quote (("proptest" "dep:indoc" "dep:proptest" "dep:proptest-attr-macro"))))))

