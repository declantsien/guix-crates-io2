(define-module (crates-io sy mb symboltable) #:use-module (crates-io))

(define-public crate-symboltable-0.1.0 (c (n "symboltable") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "miette") (r "^5.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed_ids") (r "^0.2.0") (d #t) (k 0)))) (h "1jv31vw067n0cqr4cg0b66767if6gjfgx0b3fysp1f8wi6ljlz4f")))

