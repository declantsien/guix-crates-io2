(define-module (crates-io sy mb symboscript-formatter) #:use-module (crates-io))

(define-public crate-symboscript-formatter-0.0.0 (c (n "symboscript-formatter") (v "0.0.0") (h "145k0ssv731077ck5va88lpd3h87kfrlj3vpfbs6nbaj3i86jscm")))

(define-public crate-symboscript-formatter-0.6.0 (c (n "symboscript-formatter") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.6.1") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.7.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0g37ca3djhm609qa7hqbvndn5p6r36m01alwvyn18dvxin3w9yxj")))

(define-public crate-symboscript-formatter-0.6.1 (c (n "symboscript-formatter") (v "0.6.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.6.2") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.8.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0zx0d5c6k46qd4mgvra3wbhiq0vcp9z8qvwyx9kv7hz3sq340q9a")))

(define-public crate-symboscript-formatter-0.6.3 (c (n "symboscript-formatter") (v "0.6.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.1") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.9.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "07k03xm35c9dj3v7ifjnm7rc4swfc675bigmhs40hf9wzdf32cq4")))

(define-public crate-symboscript-formatter-0.6.4 (c (n "symboscript-formatter") (v "0.6.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.2") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.10.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1c3q8nb0c1bxdlkqaid6qnwa8g16g4xq2xrvdbd79ibwi0ns3cy6")))

(define-public crate-symboscript-formatter-0.6.5 (c (n "symboscript-formatter") (v "0.6.5") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.3") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.11.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1gdw7hjwbbcydh032d1q8y13ypdcvkm00m1yha6dd83kqacdb1vj")))

(define-public crate-symboscript-formatter-0.6.6 (c (n "symboscript-formatter") (v "0.6.6") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.4") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.12.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0gb5xz16k404mj3hwd30gwajryipvpjza4xg6ahgvvmc9g9v8j68")))

(define-public crate-symboscript-formatter-0.6.7 (c (n "symboscript-formatter") (v "0.6.7") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.5") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.13.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "07x0w2xyj04b81iwmmrw3pfrlra17jvg2p05f43h71fk7ckrbpw9")))

(define-public crate-symboscript-formatter-0.6.8 (c (n "symboscript-formatter") (v "0.6.8") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.6") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.14.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1xxrk413wki7br6zx48m76bqplk8rm8j5hbk9wgkxnrzzmjnxxzk")))

(define-public crate-symboscript-formatter-0.6.9 (c (n "symboscript-formatter") (v "0.6.9") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.7") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.15.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "110whrv9rn5vmkyykl9hgsvgvrvlh1f30hb743viz7kxmz4gx42q")))

(define-public crate-symboscript-formatter-0.6.10 (c (n "symboscript-formatter") (v "0.6.10") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.8") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.15.1") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0qi2jlg6c1rm546iyvfnr3bm9fkw6zbvhr9rz8rnl6vd6h6bs8rb")))

(define-public crate-symboscript-formatter-0.6.11 (c (n "symboscript-formatter") (v "0.6.11") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.10") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.15.2") (f (quote ("lexer"))) (d #t) (k 0)))) (h "11xn7chgc8czybqvcxk02gc17frvrslay724xc2i5315nlmxgxvz")))

(define-public crate-symboscript-formatter-0.6.12 (c (n "symboscript-formatter") (v "0.6.12") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.7.11") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.16.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "072yldwbbmq8nqi8h486yyl4pwnxc9zn7g7vn27s7k8fr9yhi5l9")))

(define-public crate-symboscript-formatter-0.6.14 (c (n "symboscript-formatter") (v "0.6.14") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.8.1") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.17.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0hv2f4g3vblrsyjl2i8b6f7da23ih57zl59xk5jsvsz18iv3yppl")))

(define-public crate-symboscript-formatter-0.6.15 (c (n "symboscript-formatter") (v "0.6.15") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.8.3") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.18.0") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1pyarhklhcmzg2aqy983zq21vkwcm0whlx9cb45gn1p8swfc04i5")))

(define-public crate-symboscript-formatter-0.6.16 (c (n "symboscript-formatter") (v "0.6.16") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symboscript-lexer") (r "^0.8.4") (d #t) (k 0)) (d (n "symboscript-types") (r "^0.18.1") (f (quote ("lexer"))) (d #t) (k 0)))) (h "17l7xxz4aas4f551bvwwj6aqv7qmyk6x7h7bfzwhxgssnjnny5lf")))

