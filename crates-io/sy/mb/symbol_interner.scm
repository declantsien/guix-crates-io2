(define-module (crates-io sy mb symbol_interner) #:use-module (crates-io))

(define-public crate-symbol_interner-0.1.0 (c (n "symbol_interner") (v "0.1.0") (h "0j976yrx97k85xnhllcbmwfkh7fc49s56x4l2zlszwj005pzb282")))

(define-public crate-symbol_interner-0.1.1 (c (n "symbol_interner") (v "0.1.1") (h "157l3x4grq0s0mgky0m1hsyf14qbv7wf3b25lbdk7pxbslq51nax")))

(define-public crate-symbol_interner-0.1.2 (c (n "symbol_interner") (v "0.1.2") (h "1siw4fwc579084ylbia10hb58iwr6dra9mgqavq78lv0lfnd9yjk")))

