(define-module (crates-io sy mb symbolic-testutils) #:use-module (crates-io))

(define-public crate-symbolic-testutils-3.0.1 (c (n "symbolic-testutils") (v "3.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1393j9cfv2narp361sdrw36201jmcv0rcy4h0q629cr97sn3d9bh")))

(define-public crate-symbolic-testutils-4.0.0 (c (n "symbolic-testutils") (v "4.0.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "145jl51a8yihc52z79d2rpj4kb369i9kx39ssxs00zfvkqhndk66")))

(define-public crate-symbolic-testutils-5.0.0 (c (n "symbolic-testutils") (v "5.0.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1igd5m2b9lpqrdhp53jprvdfgbnvmh8wv7i7yy5mmx9ygvy1m9dy")))

(define-public crate-symbolic-testutils-5.0.1 (c (n "symbolic-testutils") (v "5.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1qlis6h1ms9pbbnjp0wj0j5r80gwgvsibc7zsps73iz128xmxqy7")))

(define-public crate-symbolic-testutils-5.0.2 (c (n "symbolic-testutils") (v "5.0.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1c8q787lszd15wf80i3xpb4wgl5d75hnm9vfma17dg4gaykdcmd5")))

(define-public crate-symbolic-testutils-5.1.0 (c (n "symbolic-testutils") (v "5.1.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0jcl9srpxpdghj90v2jxv8nphfx4r8mjckg13gv6fhvnxjchfi2l")))

(define-public crate-symbolic-testutils-5.1.1 (c (n "symbolic-testutils") (v "5.1.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0dszcr2s5ia1gp8150pkq2wqa97d7vax627smzg9wp8sylpd04as")))

(define-public crate-symbolic-testutils-5.1.2 (c (n "symbolic-testutils") (v "5.1.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0a1q1mbzd2jmyn9nnfkx8ibrpsq033hqyqp83wna7kp61dnwmvq2")))

(define-public crate-symbolic-testutils-5.2.0 (c (n "symbolic-testutils") (v "5.2.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1dgwhm87dhn0sxhfa8lgs84f7c45d4l9268amga546z0pirmfhrd")))

(define-public crate-symbolic-testutils-5.3.0 (c (n "symbolic-testutils") (v "5.3.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1ymn2vqh4m250jfc9la9s8vf8h0j0dmnw511713qdj9xpnlrbxgg")))

(define-public crate-symbolic-testutils-5.4.0 (c (n "symbolic-testutils") (v "5.4.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1xxpn01hsnz7slamq3khjvrwrdccj7np1rkqcyd5asghdymby4g7")))

(define-public crate-symbolic-testutils-5.5.0 (c (n "symbolic-testutils") (v "5.5.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1lvp1mv5z31mvyqi1aws3sz6i8dg9rq15a0gv378h06yb71vl4yg")))

(define-public crate-symbolic-testutils-5.5.1 (c (n "symbolic-testutils") (v "5.5.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0qwp9hf9qb6y2jw8ijcbn1f3jffwzdmp4q8mzf7vi95vbhzbdqh8")))

(define-public crate-symbolic-testutils-5.5.2 (c (n "symbolic-testutils") (v "5.5.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0yki67w38n0w2q4nx278fcbhiiwi2a3x8ma6ixfypgqg00ag9f82")))

(define-public crate-symbolic-testutils-5.5.3 (c (n "symbolic-testutils") (v "5.5.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0bda8yaqxy878zl6h50jv7fdiir3bqzv0jg00qd6cnw80pvf3yh9")))

(define-public crate-symbolic-testutils-5.5.4 (c (n "symbolic-testutils") (v "5.5.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "03b6yhm5xnacl54zq1f6jyn7a0z2zbc7gzp5v6aq79b1ncsi6msl")))

(define-public crate-symbolic-testutils-5.5.5 (c (n "symbolic-testutils") (v "5.5.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1vlnj90xxrwpanq9706y9g3yq2hddsa0ik8icwr7mhs96anmybbl")))

(define-public crate-symbolic-testutils-5.5.6 (c (n "symbolic-testutils") (v "5.5.6") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0w75z3r15vkqrai454jrd9z02snv2zwp3jx86fl1sipiy5657dgd")))

(define-public crate-symbolic-testutils-5.7.0 (c (n "symbolic-testutils") (v "5.7.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "18halyxlg8z89gir60a3paxvvkvnq9hma8jw6nd7aqxacs7ki2xp")))

(define-public crate-symbolic-testutils-5.7.1 (c (n "symbolic-testutils") (v "5.7.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "15k2jyp9f28x4fqhz0vg0pnir81ck8j4ibl3l407d628xyhaiavn")))

(define-public crate-symbolic-testutils-5.7.2 (c (n "symbolic-testutils") (v "5.7.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1pnm76rcgn0r9i59qylwk7qkfr7bbl9vs4wycqp88z7cv3s4ahhh")))

(define-public crate-symbolic-testutils-5.7.3 (c (n "symbolic-testutils") (v "5.7.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1awxhx40k65n7q9xwag1amacmzjr2zdp2lp41ijm2114zvwf4l85")))

(define-public crate-symbolic-testutils-5.7.4 (c (n "symbolic-testutils") (v "5.7.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0ld4k5zhkrij6iilfmlfm0aags7zvc0axa5jlyhzl1ag3gc2aicv")))

(define-public crate-symbolic-testutils-5.7.5 (c (n "symbolic-testutils") (v "5.7.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0sjl4hpmih5s4c708jc7y4hx45qfgxhk2xh3lqvcll9ywbj0vv5p")))

(define-public crate-symbolic-testutils-5.7.6 (c (n "symbolic-testutils") (v "5.7.6") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1076x8xg7gdasm55kazfnx2f8diwcnyxvqjrb5j01a3p5j57203w")))

(define-public crate-symbolic-testutils-5.7.7 (c (n "symbolic-testutils") (v "5.7.7") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "1kj99i9lh4szc94r7ilwrybdpzw7mandmgnlp1lrknf8bpxmxifp")))

(define-public crate-symbolic-testutils-5.7.8 (c (n "symbolic-testutils") (v "5.7.8") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)))) (h "0mnw428w4cc427fz4q7p98cs6gf6pi5fhw91nz2656dybp53688l")))

