(define-module (crates-io sy mb symbolic_math) #:use-module (crates-io))

(define-public crate-symbolic_math-0.1.0 (c (n "symbolic_math") (v "0.1.0") (h "1pp7j53qniddzv6fd0q38aja2na0a7cshnyqixc0liwx31wf513z") (y #t)))

(define-public crate-symbolic_math-0.1.1 (c (n "symbolic_math") (v "0.1.1") (h "068svfnphb1kz6xm49iyy50d1i7mc4imianxijgmsz687pqrwvfd")))

(define-public crate-symbolic_math-0.1.2 (c (n "symbolic_math") (v "0.1.2") (h "1zvvgrnpaia7z8z66z0ms7kmyfvahy6zraslyb2kl8bycx3m2amk")))

