(define-module (crates-io sy mb symboscript-analyzer) #:use-module (crates-io))

(define-public crate-symboscript-analyzer-0.0.0 (c (n "symboscript-analyzer") (v "0.0.0") (h "1xl5j3gcfp2pbwm3clzn14l9fzd8dykimp99dxbh694d2n7ra6hy")))

(define-public crate-symboscript-analyzer-0.5.0 (c (n "symboscript-analyzer") (v "0.5.0") (h "1bb1hr2xr94y7ag879cyza1x7jas5z7d749h8w6bfdn9p229wis7")))

