(define-module (crates-io sy mb symbolic_expressions) #:use-module (crates-io))

(define-public crate-symbolic_expressions-0.3.5 (c (n "symbolic_expressions") (v "0.3.5") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "070dvw2rdd90cnin8i212xmzdwzqpp2b1lniw188rmfjcdv708ga") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-symbolic_expressions-0.3.6 (c (n "symbolic_expressions") (v "0.3.6") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "1klmnv2l4c8wfn77bq5q1lnhkfcg36rxc8zvqb65awfhn96bsja5") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-symbolic_expressions-0.3.7 (c (n "symbolic_expressions") (v "0.3.7") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "1hn76kckfs5r865absjyhkl3ah23b4i631qribrb2jkklx75712y") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-symbolic_expressions-0.3.8 (c (n "symbolic_expressions") (v "0.3.8") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "1mdfdr8bmaw181hvps9kzla42ip187kqr086xk2i31p72ff74q73") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-symbolic_expressions-0.3.10 (c (n "symbolic_expressions") (v "0.3.10") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "1bvh8vn83b8ibz086d28gmn9vcnybhzw23qhyg85f2k2bcad85nl") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-symbolic_expressions-0.3.11 (c (n "symbolic_expressions") (v "0.3.11") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "1m1qgfpncis7mxmp8ab4yhb2d8dqpv40msw8mp1s4zfwn8b54amy") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-symbolic_expressions-0.3.13 (c (n "symbolic_expressions") (v "0.3.13") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "0gz6gafdp0r8ll4ljrz6frqabs2jv5fyhl2bbkd9b8nzr46pmfqy") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-symbolic_expressions-0.3.14 (c (n "symbolic_expressions") (v "0.3.14") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "nom") (r "~1.2.0") (d #t) (k 0)))) (h "0iyy7fjnvzcc6x1vd1bks54f9qhfnhjdg072g122d1s73nmm4i7z") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-symbolic_expressions-0.3.15 (c (n "symbolic_expressions") (v "0.3.15") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "1hydw29v7idpl7vck2pfcvkfd0x1hx2hnhi9vksa5clwqz4lmbha")))

(define-public crate-symbolic_expressions-0.4.0 (c (n "symbolic_expressions") (v "0.4.0") (h "0siks9975dknim822ppza7yddf7jqxfwxa9wdx7i96nd8q1x4dn8")))

(define-public crate-symbolic_expressions-0.4.1 (c (n "symbolic_expressions") (v "0.4.1") (h "075xxkzcl0s1qdk5sh8z18g4klm2phdx03anil57jcx67m4k4f5m")))

(define-public crate-symbolic_expressions-0.4.2 (c (n "symbolic_expressions") (v "0.4.2") (h "0mpb1397wjgbxd5yqwm1v6lybfbrdfqb9ywrjrq4f0n9x349pj59")))

(define-public crate-symbolic_expressions-1.0.0 (c (n "symbolic_expressions") (v "1.0.0") (h "1gvzqdlyjjwn72r1dcjxnvwbjcyzhy60gsqjq7765y9wrz5488vh")))

(define-public crate-symbolic_expressions-2.0.3 (c (n "symbolic_expressions") (v "2.0.3") (d (list (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_codegen") (r "0.8.*") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "0.8.*") (o #t) (d #t) (k 0)))) (h "0kvq0mxc6hky8396dzcsqfjc4pd38h4zip1l8aznmfqvq5rfykir") (f (quote (("nightly" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-symbolic_expressions-2.0.8 (c (n "symbolic_expressions") (v "2.0.8") (d (list (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_codegen") (r "0.8.*") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "0.8.*") (o #t) (d #t) (k 0)))) (h "1b5gnk0fhwlrznc9rj9pknnr8f9wqghm05n64si3zvpbnh2jxvh6") (f (quote (("nightly" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-symbolic_expressions-3.0.0 (c (n "symbolic_expressions") (v "3.0.0") (h "091skcx5b64l8jlql1ch8qjixbyss3vgygz9g2qj99rbx939d8yg")))

(define-public crate-symbolic_expressions-3.0.2 (c (n "symbolic_expressions") (v "3.0.2") (h "11c74dwrp5cz8y5mx6qrma1cp7sdv9madfl2l5mfccfm3w6qf9w2")))

(define-public crate-symbolic_expressions-4.0.1 (c (n "symbolic_expressions") (v "4.0.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "0zs6rj9zq3h5rbc2i4j6vy12wbsv3pb44kkwdfn6407012i3bwfd")))

(define-public crate-symbolic_expressions-4.0.2 (c (n "symbolic_expressions") (v "4.0.2") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1ii1fxcjhd4fgzk2klrfmv50gj0g26jzvfgd9qkssqnfd4clpv8p")))

(define-public crate-symbolic_expressions-4.0.3 (c (n "symbolic_expressions") (v "4.0.3") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0c7xi2r0pkx8mj8gzfzyxy6vdrxflm34hgiym0wgkk7vghg1g8rg")))

(define-public crate-symbolic_expressions-4.0.4 (c (n "symbolic_expressions") (v "4.0.4") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0mjcnmmrxf3wyhrqw33h010ax3rwpj9zqn1d1knpq790qzsc5399")))

(define-public crate-symbolic_expressions-4.0.5 (c (n "symbolic_expressions") (v "4.0.5") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1dn91h79mkapbp4jc500dz05khzzpd6khp5pbxhk4zi9d2rx4dyw")))

(define-public crate-symbolic_expressions-4.0.6 (c (n "symbolic_expressions") (v "4.0.6") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0pxi9pv6kr765qwqfrn17h5kzz9zapmijhj43sbjnvnrlz2cj6y6")))

(define-public crate-symbolic_expressions-4.0.7 (c (n "symbolic_expressions") (v "4.0.7") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1y0ljqrbwlhs3x1j5r17s7x334wm9sxf2lrmjbvv655lri1vcm4g")))

(define-public crate-symbolic_expressions-4.0.8 (c (n "symbolic_expressions") (v "4.0.8") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0jdiyamgzxvn1n7yjpkssxfqvpviqvz5lhh4kln2m6028d45b950")))

(define-public crate-symbolic_expressions-4.1.0 (c (n "symbolic_expressions") (v "4.1.0") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0rrqfksz8g1225186i7lnlz8p7v6iayh0d5ciigyiggdzr7506ks")))

(define-public crate-symbolic_expressions-4.1.1 (c (n "symbolic_expressions") (v "4.1.1") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1a2p86snl2lqjcsnacahk06xc3wma34p5xm1hg46c6wb40ykl46d")))

(define-public crate-symbolic_expressions-4.1.2 (c (n "symbolic_expressions") (v "4.1.2") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1afwqzbdf4mz6xhagrwzzx9h2903dkklk2srh3b8i3dqlbbxrj8p")))

(define-public crate-symbolic_expressions-4.1.3 (c (n "symbolic_expressions") (v "4.1.3") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0lnjj2hwn6vnww8jl9k0xkb2v6zv6zjajs8impvakbmy11011903")))

(define-public crate-symbolic_expressions-4.1.4 (c (n "symbolic_expressions") (v "4.1.4") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0n70nn9rf95pzdlgir2ngqvzjrp8f8ygpn2xba9gwv9qff5pg0bg")))

(define-public crate-symbolic_expressions-4.1.5 (c (n "symbolic_expressions") (v "4.1.5") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "1jzwq671xkx3iybdq054qwrzy3pfaicxc94yh6gl291v894j7mw7")))

(define-public crate-symbolic_expressions-4.1.6 (c (n "symbolic_expressions") (v "4.1.6") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "09xxiz9y6ck0hyv9i3jh97ffv7wyh8yq5y3szlhcpg6fvbxiy6js")))

(define-public crate-symbolic_expressions-4.1.7 (c (n "symbolic_expressions") (v "4.1.7") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "0lwcb0998hqhzzpi1pzlwz8fab2f860l8n55qpl0mwwvzddaslqc")))

(define-public crate-symbolic_expressions-4.1.8 (c (n "symbolic_expressions") (v "4.1.8") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0kxngsr82f825jzwyxvq74gyjg8mnwcmcm5pdikvhrsc8knpvsbj")))

(define-public crate-symbolic_expressions-4.1.9 (c (n "symbolic_expressions") (v "4.1.9") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "16h13kjvw5p9cx7wfr2qnk7hwrdv4wzl5biic0466r82hry76zvf")))

(define-public crate-symbolic_expressions-4.1.10 (c (n "symbolic_expressions") (v "4.1.10") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1ripaylyvm44vkfpghl2v6q3a8kfl5ahkczzcnfx6wn3bfss2ylk")))

(define-public crate-symbolic_expressions-4.1.11 (c (n "symbolic_expressions") (v "4.1.11") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "19083gp37rq58w9ibd937xhg63hdgp6gb3n128ja9ik5shpi3all")))

(define-public crate-symbolic_expressions-5.0.0 (c (n "symbolic_expressions") (v "5.0.0") (h "1ll523msq0i87vj2dl7gail5qnqj6ysqpq71nw9vlvh2yzqp4p9r")))

(define-public crate-symbolic_expressions-5.0.1 (c (n "symbolic_expressions") (v "5.0.1") (h "197i2kijm6fxfj9538gl7ssvs12mxsa4kj8ckwppz0lcp3i5i4ic")))

(define-public crate-symbolic_expressions-5.0.2 (c (n "symbolic_expressions") (v "5.0.2") (h "0pqwwc7p693wiv42hl69p6zhkkjh66iklgil763ifbanrl1fgslj")))

(define-public crate-symbolic_expressions-5.0.3 (c (n "symbolic_expressions") (v "5.0.3") (h "0w9vl9975dlkn4r93mzhsmj1k4ch88mc91052lqwbiiyv0qxas3w")))

