(define-module (crates-io sy mb symbolic-il2cpp) #:use-module (crates-io))

(define-public crate-symbolic-il2cpp-8.7.1 (c (n "symbolic-il2cpp") (v "8.7.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.7.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1qldvvxibxyz7ab98rhsqkfhi79swv34cfr4j2swsp37hqh9dfqs")))

(define-public crate-symbolic-il2cpp-8.7.2 (c (n "symbolic-il2cpp") (v "8.7.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.7.2") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "10fiv5xgsw8lvvmrldmy788rxym5cvxqjfgwcck6pbr3lszhgyq5")))

(define-public crate-symbolic-il2cpp-8.7.3 (c (n "symbolic-il2cpp") (v "8.7.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.7.3") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0xsr50dvr9wsp6fx913fyax0g1dgywcdaxfb77zrdjdmi7g67xal")))

(define-public crate-symbolic-il2cpp-8.8.0 (c (n "symbolic-il2cpp") (v "8.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.8.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "102fl55iaacdc799as2919inc3xiszpi3khjgxryxvapf054d1ry")))

(define-public crate-symbolic-il2cpp-9.0.0 (c (n "symbolic-il2cpp") (v "9.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.0.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0mqw5d9g6d5iv88p4d2aj7jl3dfzhxk5x8fsky4mv2dr0494ssip")))

(define-public crate-symbolic-il2cpp-9.1.0 (c (n "symbolic-il2cpp") (v "9.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.0") (d #t) (k 2)) (d (n "object") (r "^0.28") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.1.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "087vmpv2kq4wqkidy1bcqjlqg5l2mrxy1zq5inry852w1wm4g7gc")))

(define-public crate-symbolic-il2cpp-9.1.1 (c (n "symbolic-il2cpp") (v "9.1.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.1.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.1.1") (d #t) (k 0)))) (h "1mq73qhnnbh0gzd4i71wzzafz7i9kj2k5kfwjy6ayphq1gpq56hp")))

(define-public crate-symbolic-il2cpp-9.1.2 (c (n "symbolic-il2cpp") (v "9.1.2") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.1.2") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.1.2") (d #t) (k 0)))) (h "093wjbbqh9s1zblkp063ghvdjq9pw7mpwb5jlxcmdh9f77666qkf")))

(define-public crate-symbolic-il2cpp-9.1.3 (c (n "symbolic-il2cpp") (v "9.1.3") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.1.3") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.1.3") (d #t) (k 0)))) (h "0frylf4x8jyhjq1m3n6jbp22hj7x6fbxyiibn6k3yf3ph7k9gv7x")))

(define-public crate-symbolic-il2cpp-9.1.4 (c (n "symbolic-il2cpp") (v "9.1.4") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.1.4") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.1.4") (d #t) (k 0)))) (h "0vsmr08fdrbd838q33bd0ddppdh0rn8wjd3b9lrdcprjrhra8hgj")))

(define-public crate-symbolic-il2cpp-9.2.0 (c (n "symbolic-il2cpp") (v "9.2.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.2.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.2.0") (d #t) (k 0)))) (h "06jhak5yl1fdiqvsdi2snf7sqibbivggiaqpi5lykg402y7znh2k")))

(define-public crate-symbolic-il2cpp-9.2.1 (c (n "symbolic-il2cpp") (v "9.2.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^9.2.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^9.2.1") (d #t) (k 0)))) (h "0ba74shn1zibjcdbpv18j2k5v6qzgq36zzray9iyzq45qdgl41x4")))

(define-public crate-symbolic-il2cpp-10.0.0 (c (n "symbolic-il2cpp") (v "10.0.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.0.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.0.0") (d #t) (k 0)))) (h "0f6xhs7dmd65qwl2acwydqgmygfxbbyb24iyxylasgc9zg0yk357")))

(define-public crate-symbolic-il2cpp-10.1.0 (c (n "symbolic-il2cpp") (v "10.1.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.0") (d #t) (k 0)))) (h "099nlsqmmkql8r5sbycml1vb8m27kmm13w562d1dxzh82fldskzi")))

(define-public crate-symbolic-il2cpp-10.1.1 (c (n "symbolic-il2cpp") (v "10.1.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.1") (d #t) (k 0)))) (h "1yq5az6scz1qk4i79xp354708j91dki0856r28xj0c17yx762h7m")))

(define-public crate-symbolic-il2cpp-10.1.2 (c (n "symbolic-il2cpp") (v "10.1.2") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.2") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.2") (d #t) (k 0)))) (h "1r3p0lysfmkg40jkhcrrx6l7l8j5vpkizzvjvrfr7jizf7xzds6k")))

(define-public crate-symbolic-il2cpp-10.1.3 (c (n "symbolic-il2cpp") (v "10.1.3") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.3") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.3") (d #t) (k 0)))) (h "0cwagq05igwsi5pc48m3ma87arb3dhfysbgzj2wwy943179bk15z")))

(define-public crate-symbolic-il2cpp-10.1.4 (c (n "symbolic-il2cpp") (v "10.1.4") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.4") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.4") (d #t) (k 0)))) (h "1h2av9rrpi6p14g3g7x0g0yjcbfs2ka667i0yzx3yk47idlyihmw")))

(define-public crate-symbolic-il2cpp-10.1.5 (c (n "symbolic-il2cpp") (v "10.1.5") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.1.5") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.1.5") (d #t) (k 0)))) (h "0nnxj7hj4i6lkcf52y6z3ak8mvxyg1wq2b8mhv15lja90bl5sssd")))

(define-public crate-symbolic-il2cpp-10.2.0 (c (n "symbolic-il2cpp") (v "10.2.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.2.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.2.0") (d #t) (k 0)))) (h "0f21k8n0xk71db9qw6n2zskkf6mhjz1p3m4scjw4a3didb1fw6l4")))

(define-public crate-symbolic-il2cpp-10.2.1 (c (n "symbolic-il2cpp") (v "10.2.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^10.2.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^10.2.1") (d #t) (k 0)))) (h "1m7iws9nsikpiml4c0cgd6b96yb33wf2y5m3mm401qxdxnfllp32")))

(define-public crate-symbolic-il2cpp-11.0.0 (c (n "symbolic-il2cpp") (v "11.0.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^11.0.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^11.0.0") (d #t) (k 0)))) (h "15n618kyfy2a4vg7a4y8h5cad5n5zwskdxivk7kw3z0x9rchrnq6")))

(define-public crate-symbolic-il2cpp-11.1.0 (c (n "symbolic-il2cpp") (v "11.1.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^11.1.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^11.1.0") (d #t) (k 0)))) (h "0cah6dslfq61j420d74qc59588kn63b1qdzch7ibf739q8fvbrpw")))

(define-public crate-symbolic-il2cpp-12.0.0 (c (n "symbolic-il2cpp") (v "12.0.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.0.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.0.0") (d #t) (k 0)))) (h "1zc2dc4sxbbfqgfw79d8y39bb4v5l67fzk4b2xh1knprqyi3r1a4")))

(define-public crate-symbolic-il2cpp-12.1.0 (c (n "symbolic-il2cpp") (v "12.1.0") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.0") (d #t) (k 0)))) (h "0cg3z82i389y3km8mi9f52dp40yrircx3x83j2ca1d9srf885h0w")))

(define-public crate-symbolic-il2cpp-12.1.1 (c (n "symbolic-il2cpp") (v "12.1.1") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.1") (d #t) (k 0)))) (h "05j45kkcnh0nqckqvknzy4rlqp3yjc6ydqifzmy8ak1rvllc1gdf")))

(define-public crate-symbolic-il2cpp-12.1.2 (c (n "symbolic-il2cpp") (v "12.1.2") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.2") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.2") (d #t) (k 0)))) (h "1h0g1cpbfxh38jfrgm8vcdcwirddihmnxkj9bgm6fyfpd448wyx0")))

(define-public crate-symbolic-il2cpp-12.1.3 (c (n "symbolic-il2cpp") (v "12.1.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.3") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.3") (d #t) (k 0)))) (h "182cqcvzklvj7vm6sq2vimwgazj65wk1rqg75csn86b08aq5n1r0")))

(define-public crate-symbolic-il2cpp-12.1.4 (c (n "symbolic-il2cpp") (v "12.1.4") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.4") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.4") (d #t) (k 0)))) (h "0j8pv5604dww6wbybvcsryzdqmg2cc50y5b6dhyvprkq8yqx48ax")))

(define-public crate-symbolic-il2cpp-12.1.5 (c (n "symbolic-il2cpp") (v "12.1.5") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.5") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.5") (d #t) (k 0)))) (h "13jxww1jp0s0x24gp9sm79myfmgmv2rzzw4vhhgarfq0hd9xp97z")))

(define-public crate-symbolic-il2cpp-12.1.6 (c (n "symbolic-il2cpp") (v "12.1.6") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.6") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.1.6") (d #t) (k 0)))) (h "16l9pz6nz4znj6jj9ny5bxwnakcmnwp5m7fry9i6a3jc9vqmfhg9")))

(define-public crate-symbolic-il2cpp-12.2.0 (c (n "symbolic-il2cpp") (v "12.2.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.2.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.2.0") (d #t) (k 0)))) (h "0ab932ybh7i8xwyyx48adpcj6prfgkhz0m0g0sga2lrg2pv9lpd0")))

(define-public crate-symbolic-il2cpp-12.3.0 (c (n "symbolic-il2cpp") (v "12.3.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.3.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.3.0") (d #t) (k 0)))) (h "178rk6plx3wccvd9jgg7lm484h4zzr8m89qwxrarh1x8d20zxgkm")))

(define-public crate-symbolic-il2cpp-12.4.0 (c (n "symbolic-il2cpp") (v "12.4.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.4.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.4.0") (d #t) (k 0)))) (h "00vkkqqm9d8jhamfy8p2gpsihp15dz2yfzrjpi3510dlypjavapg")))

(define-public crate-symbolic-il2cpp-12.4.1 (c (n "symbolic-il2cpp") (v "12.4.1") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.4.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.4.1") (d #t) (k 0)))) (h "1xigr90kglgwgrymx3q784kww7a3jf0c7lk23771f6l9z2qs89pd")))

(define-public crate-symbolic-il2cpp-12.5.0 (c (n "symbolic-il2cpp") (v "12.5.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.5.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.5.0") (d #t) (k 0)))) (h "0gfkivkn9mlnqbhq5zdizmfn5hrn9qkfmk5zxn6glj1g7irhc7zs")))

(define-public crate-symbolic-il2cpp-12.6.0 (c (n "symbolic-il2cpp") (v "12.6.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.6.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.6.0") (d #t) (k 0)))) (h "0zfd8cckhm6q0pfj0dl6pjlh61maswxaib036pyihd9ls9zijvqm")))

(define-public crate-symbolic-il2cpp-12.7.0 (c (n "symbolic-il2cpp") (v "12.7.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.7.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.7.0") (d #t) (k 0)))) (h "0bap4dyz9irjzwh0b6vclhlvss9nhsqdi802924sa0wfkvnj64yn")))

(define-public crate-symbolic-il2cpp-12.7.1 (c (n "symbolic-il2cpp") (v "12.7.1") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.7.1") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.7.1") (d #t) (k 0)))) (h "1d915mm3nys4hm3yw685h5mx2d9zhy0acz6x247hd9c2bjff12sx")))

(define-public crate-symbolic-il2cpp-12.8.0 (c (n "symbolic-il2cpp") (v "12.8.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.8.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^12.8.0") (d #t) (k 0)))) (h "1n9kxq0qps2kykw7cr3s9zba0jm933zsgfgwvgbyngw57hylb39g")))

