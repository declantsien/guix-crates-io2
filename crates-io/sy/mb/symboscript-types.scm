(define-module (crates-io sy mb symboscript-types) #:use-module (crates-io))

(define-public crate-symboscript-types-0.1.0 (c (n "symboscript-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0xp0s9pysz939ralf8ikmk473cpmr3q1b7q4rby9z9vzv1q9z3m1")))

(define-public crate-symboscript-types-0.2.0 (c (n "symboscript-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "09lm6gq1qjr23wa2mf68z15b0jqkzss9y28cacrpx9h9qmi2b0g7")))

(define-public crate-symboscript-types-0.3.0 (c (n "symboscript-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "01l790q0ca02cqmz13gzv0b4zld541q3319rzqyhdc1naf0zf3n2")))

(define-public crate-symboscript-types-0.3.1 (c (n "symboscript-types") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1kjafv611npd8y6bq3ff3wibm6dc2cnaxlfwcalaj4mp4jf5wd32")))

(define-public crate-symboscript-types-0.4.0 (c (n "symboscript-types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "14bvph6b9cplwihgd8h8l4ddspbazxfvfc5lgf6kdbqaq4vrqklr")))

(define-public crate-symboscript-types-0.6.0 (c (n "symboscript-types") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0dc5gh52fddxrxq1vlrjx8bk3lylxh9hxb4ncqrpb9p36l738bhp") (f (quote (("parser") ("lexer"))))))

(define-public crate-symboscript-types-0.7.0 (c (n "symboscript-types") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0bzj8k7sj648q0nyd7nhyxp463kraqjq1rvyjry73kwb2fvm54s3") (f (quote (("parser") ("lexer"))))))

(define-public crate-symboscript-types-0.8.0 (c (n "symboscript-types") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "03bphwswlq7dfrcv1gvhhiy719464592svnh1xhbjjjfcnvsxmks") (f (quote (("parser") ("lexer"))))))

(define-public crate-symboscript-types-0.9.0 (c (n "symboscript-types") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "16dj1xldncbg5skwv94inmwzbzd0109n3gdqhhib1xwrh0mjcj84") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.10.0 (c (n "symboscript-types") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1s0b0bfajzfv0d1wpqw0x9rp4rj4ga8y9w45wdwk7zwa4x8nc5zm") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.11.0 (c (n "symboscript-types") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1fpp85phq6ibk3il39h9950kmd4nqasjrwnjx9rxc3wgmhmmgznl") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.12.0 (c (n "symboscript-types") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1a4b76gd52jw94da1x9n9rqbw6rq4m77di026860d0062bqqdv52") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.13.0 (c (n "symboscript-types") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "005kczp3gpii9i9yv4ir4d8jkgpyc363290f5fzypsd22qrrsldp") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.14.0 (c (n "symboscript-types") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "07x5mbsh8lk3b45dzhcbby9f0an1pw9x6mcyqv35hrjiir8n385p") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.15.0 (c (n "symboscript-types") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0l0gx8siilsx1ycjrx1ari041fq1v2980lp3s4nfplss1yf0bjfz") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.15.1 (c (n "symboscript-types") (v "0.15.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1j5dzlp0vyxyi98fpcpmr1ijca83v07src8ygg0vis4a8fily25q") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.15.2 (c (n "symboscript-types") (v "0.15.2") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1g2l5sps82f168ym631kd6571s9ha2fpax30k6rrbs19vcd1d6py") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.16.0 (c (n "symboscript-types") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1j0wf59n4hnhq8hbi57g5fipwz1mg2z6vxvm4hn9nxsb86z8p0s5") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.17.0 (c (n "symboscript-types") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "03j86zyhl65qbl23v03ps3rdn9f0r95sfwwwnmhpr49hja1zfmn9") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.18.0 (c (n "symboscript-types") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1bkm38fni22navsg8bdq3rmfyhvb8pr66cj7agnbnwq1xm2hysv3") (f (quote (("parser") ("lexer") ("interpreter"))))))

(define-public crate-symboscript-types-0.18.1 (c (n "symboscript-types") (v "0.18.1") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "190jfywkz65ic3zsy84bnjznfd9cikj23l8iir2xmhgarxcc8imc") (f (quote (("parser") ("lexer") ("interpreter"))))))

