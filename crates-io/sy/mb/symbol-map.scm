(define-module (crates-io sy mb symbol-map) #:use-module (crates-io))

(define-public crate-symbol-map-1.0.0 (c (n "symbol-map") (v "1.0.0") (d (list (d (n "crossbeam") (r "0.2.*") (d #t) (k 2)))) (h "1gdmvknx7gxyr446fz5jq9f3qnwxw85vxc1h8idp70hivd8m7ky3")))

(define-public crate-symbol-map-1.0.1 (c (n "symbol-map") (v "1.0.1") (d (list (d (n "crossbeam") (r "0.2.*") (d #t) (k 2)))) (h "1bfl8h1bmj8bv42n4cyjycigmqva0cz3z99k0jwnjbqxghhr9654")))

(define-public crate-symbol-map-1.0.2 (c (n "symbol-map") (v "1.0.2") (d (list (d (n "crossbeam") (r "0.2.*") (d #t) (k 2)))) (h "0b2rwdfnda26j9lqzla16nj4s1whfcjxni0jm84jzw1aha8wrnb1")))

