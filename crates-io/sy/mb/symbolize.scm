(define-module (crates-io sy mb symbolize) #:use-module (crates-io))

(define-public crate-symbolize-0.1.0 (c (n "symbolize") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "0q2jqwfx11671i2rkh9laza5p4674vqxfirrwfqxqz7vzi540kg3")))

(define-public crate-symbolize-0.1.1 (c (n "symbolize") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1vvh0225hb5blkgk4bk49hjqq92847iaznd3asrv1azkldbf0cg8")))

(define-public crate-symbolize-0.1.2 (c (n "symbolize") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1nvgv9x1j3q8skdgr9d8i5min5x6ra4pxcxqg9hq1sn23gq8z6p6")))

(define-public crate-symbolize-0.1.3 (c (n "symbolize") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "02d9yy3k2kwwmrcqqayl13ixxr31parj3rnmvfjk882nbka2239r")))

(define-public crate-symbolize-0.1.4 (c (n "symbolize") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1i85wzk7kx7pcz1a0laqw8alwff9nyi8zb7hw05i0nps7hya13ys")))

(define-public crate-symbolize-0.1.5 (c (n "symbolize") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1slhxmq2qkv52xm2wh0xn19rbmghrbns3l8bkvzvfl816cb9ipg6")))

(define-public crate-symbolize-0.2.0 (c (n "symbolize") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1shv81vblq7i4a6w5kcgahpjajin6hgghzbfs7h9lfs6saxr1wwg")))

(define-public crate-symbolize-0.2.1 (c (n "symbolize") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1c3x1ii6adkqwyh9glz98kblhs5z0asw3sz2fkgypc1fjsjbv77z")))

