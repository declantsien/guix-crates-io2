(define-module (crates-io sy mb symbolizer-rs) #:use-module (crates-io))

(define-public crate-symbolizer-rs-0.1.0 (c (n "symbolizer-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "itoa") (r "^1.0.11") (d #t) (k 0)) (d (n "kdmp-parser") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "msvc-demangler") (r "^0.10") (d #t) (k 0)) (d (n "pdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("tls" "gzip"))) (k 0)))) (h "07k4lp8gvgvwi11q9lwr21vny2lky4hl3dqsph37nbxv17b6rn3p") (r "1.70")))

