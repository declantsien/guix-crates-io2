(define-module (crates-io sy mb symbols-models) #:use-module (crates-io))

(define-public crate-symbols-models-0.1.0 (c (n "symbols-models") (v "0.1.0") (d (list (d (n "sea-orm") (r "^0.7.1") (d #t) (k 0)))) (h "1ss2351bcqjqq1ys2s4n23r9j5jkqmmh0gvb3khnzvlr0ym7wcai")))

(define-public crate-symbols-models-0.1.1 (c (n "symbols-models") (v "0.1.1") (d (list (d (n "sea-orm") (r "^0.7.1") (d #t) (k 0)))) (h "07y6dr5ba44f5814hrbfl7rifn0cxxbhhh14avhrs32k1l25ly49")))

(define-public crate-symbols-models-0.1.2 (c (n "symbols-models") (v "0.1.2") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "1szk8fcpaimd30pa61bgspvx73l02bbfdxwm91gx25fazgk2brc1")))

(define-public crate-symbols-models-0.1.3 (c (n "symbols-models") (v "0.1.3") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "07yw9yy9wwlx49a0119q93abls2557rkgjicwqn647d8ffqlwmj2")))

(define-public crate-symbols-models-0.1.4 (c (n "symbols-models") (v "0.1.4") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "15v97ashza3zy02fij1i86csjkim0xivmidxl5jjf2ww8phmnpmw")))

(define-public crate-symbols-models-0.1.5 (c (n "symbols-models") (v "0.1.5") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "00l7vdkf7y707f64n5zkwixmv8wc8csl4aqf6zz12pcmvgri46fx")))

(define-public crate-symbols-models-0.1.6 (c (n "symbols-models") (v "0.1.6") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "0dk6ls2z7m12mlimp2fj1mgxk83s8bfkfqf34m22g5ydrclnisrf")))

(define-public crate-symbols-models-0.1.7 (c (n "symbols-models") (v "0.1.7") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "0af3aiywksp865niwmk0qnp94ci7q1hvin764frk3qmlz9yh0ww1")))

(define-public crate-symbols-models-0.1.8 (c (n "symbols-models") (v "0.1.8") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "1xwf4xky33svvnbi1grm59lny1hfnpph1m3q7fqk6c2sllq5g66v")))

(define-public crate-symbols-models-0.1.9 (c (n "symbols-models") (v "0.1.9") (d (list (d (n "sea-orm") (r "^0.8.0") (d #t) (k 0)))) (h "032b78x5mgc5w3a9zvlxwc6zbxdvzz9fbvfgl984canbzrm937s1")))

(define-public crate-symbols-models-0.9.0 (c (n "symbols-models") (v "0.9.0") (d (list (d (n "sea-orm") (r "^0.9.0") (d #t) (k 0)))) (h "0sljnv6li8xa0fjhsjgxrw54k306kzj0ixp5gsaaycjnybmgbg4x")))

(define-public crate-symbols-models-0.10.0 (c (n "symbols-models") (v "0.10.0") (d (list (d (n "sea-orm") (r "^0.10.2") (d #t) (k 0)))) (h "191pmd7b0cziwpnk5rn92h1jih2kj6difvm8098b6lgnm1kzcbkj")))

(define-public crate-symbols-models-0.10.5 (c (n "symbols-models") (v "0.10.5") (d (list (d (n "sea-orm") (r "^0.10.5") (d #t) (k 0)))) (h "1w07alx4bgdlqabgpsixlfz85p1x2d1p7axh872lv23j2c0a7v01")))

(define-public crate-symbols-models-0.11.0 (c (n "symbols-models") (v "0.11.0") (d (list (d (n "sea-orm") (r "^0.11.0") (d #t) (k 0)))) (h "1g8xbpm8dvx3bvysbk55cmha7z8xhyix1y2pxjx0arsgxcyclcyn")))

(define-public crate-symbols-models-0.12.0 (c (n "symbols-models") (v "0.12.0") (d (list (d (n "sea-orm") (r "^0.12.0") (d #t) (k 0)))) (h "12li0j85i2q0gnawgbqrbjxsankfvp925zqa2qw6z5df7f2vci8q")))

