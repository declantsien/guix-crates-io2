(define-module (crates-io sy mb symbol) #:use-module (crates-io))

(define-public crate-symbol-0.1.0 (c (n "symbol") (v "0.1.0") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (d #t) (k 0)))) (h "050glr4ghx7wid15dfd0zcb1mild9dwygn6gb34wf3fl9k30qwr5")))

(define-public crate-symbol-0.1.1 (c (n "symbol") (v "0.1.1") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "1a19h535y81jkbbcdgmnj66lwhjsrw9hlwghg82mqkycj20zf825")))

(define-public crate-symbol-0.1.2 (c (n "symbol") (v "0.1.2") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "1asvr6dqf5w9wvkpj8qpx8783p8hrbcjpfkcqfhq7pnsf2vslq6d")))

(define-public crate-symbol-0.1.3 (c (n "symbol") (v "0.1.3") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "0ij3i1g5wnqqj67plzgwcxrsdi5c6d4asshkb9d67fqximrnhkxy")))

(define-public crate-symbol-0.1.4 (c (n "symbol") (v "0.1.4") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "0nxv48yxfh8dzg8bk7kjhkkvjswi5v1a3ryf6pz568ncxh23lx8x")))

(define-public crate-symbol-0.1.5 (c (n "symbol") (v "0.1.5") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "1dhlq2bizcfvpfv289g0gc64m70ywfm6grks31hqwm5dxjn5s0f3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-symbol-0.1.6 (c (n "symbol") (v "0.1.6") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "0kkhjzhdn8fj8n95rqia4djvixr6b1zxbfqrpkmv8133x6d383hz") (f (quote (("std") ("default" "std"))))))

(define-public crate-symbol-0.1.7 (c (n "symbol") (v "0.1.7") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "0gx8rf3c44iwqys8dpf60ss7grnsrrspag783pg6x2phrkx5q08g") (f (quote (("std") ("default" "std"))))))

(define-public crate-symbol-0.1.8 (c (n "symbol") (v "0.1.8") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "11hw65hk4154nap7f0kybm1dgzr2m4iiivcqrdd310ksrbg4yglf") (f (quote (("std") ("default" "std"))))))

(define-public crate-symbol-0.1.9 (c (n "symbol") (v "0.1.9") (d (list (d (n "gc") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (k 0)))) (h "08s45mixihymcsxvvbaw1953w02z6nd0f4ca3asnd1r5w8x9qsbr") (f (quote (("std") ("default" "std"))))))

