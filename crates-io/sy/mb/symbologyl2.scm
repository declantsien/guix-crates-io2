(define-module (crates-io sy mb symbologyl2) #:use-module (crates-io))

(define-public crate-symbologyl2-0.1.0 (c (n "symbologyl2") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0d737w3kpc4zmgxf1kncm3b9pf2jx42zb8n2g6z897brqwfkxw00")))

(define-public crate-symbologyl2-0.1.1 (c (n "symbologyl2") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xs5gawaw11g5vax90azvkmc616496l7h20ydwr6xbrhqzqsihhr")))

