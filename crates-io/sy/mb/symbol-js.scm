(define-module (crates-io sy mb symbol-js) #:use-module (crates-io))

(define-public crate-symbol-js-0.1.0 (c (n "symbol-js") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 0)) (d (n "parse-js") (r "^0.23.0") (d #t) (k 0)))) (h "0h2kkvnank1krpgsifn82dnql77vrsflvn3lw5i7plyvmvchzp6j")))

(define-public crate-symbol-js-0.2.0 (c (n "symbol-js") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 0)) (d (n "parse-js") (r "^0.24.0") (d #t) (k 0)))) (h "0bj1bai8yl2rd6rdarpry34xlwfnlya3xgmcwjvngii0d1gsp48v")))

