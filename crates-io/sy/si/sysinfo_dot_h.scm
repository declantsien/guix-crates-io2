(define-module (crates-io sy si sysinfo_dot_h) #:use-module (crates-io))

(define-public crate-sysinfo_dot_h-0.1.0 (c (n "sysinfo_dot_h") (v "0.1.0") (h "0xpx3di0gr6yasfdr5ds7bli2slpfi6lp254k2rdfnhylw6zrpd6")))

(define-public crate-sysinfo_dot_h-0.2.0 (c (n "sysinfo_dot_h") (v "0.2.0") (h "0f01skjw4f85x4ybbn63i206p0n666rynk3y6x02bgb81p8d3j8b")))

(define-public crate-sysinfo_dot_h-0.2.1 (c (n "sysinfo_dot_h") (v "0.2.1") (h "0lq3v01qzzghkp0fvdxlhy12r4kbzjhcb81z5m7mwmwgwnbk49rz")))

