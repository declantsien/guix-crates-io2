(define-module (crates-io sy si sysinfo-web) #:use-module (crates-io))

(define-public crate-sysinfo-web-0.1.0 (c (n "sysinfo-web") (v "0.1.0") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.3") (d #t) (k 0)))) (h "0hc8mvq70ccvzhpg319dnhsx3iy1qzdmgsrkmwbnyza8kc8qrjj4")))

(define-public crate-sysinfo-web-0.1.1 (c (n "sysinfo-web") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.3") (d #t) (k 0)))) (h "02kihrlnr1npqvwszibm36d3l5chxjy5ydpr05akvh1cyib1f1l9") (f (quote (("gzip") ("default" "gzip"))))))

(define-public crate-sysinfo-web-0.1.2 (c (n "sysinfo-web") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.5.6") (d #t) (k 0)))) (h "1fl69gfnxwvh25y1ql5vkiah701qq4d7kmy3rni5q0msx36kw76y") (f (quote (("gzip" "flate2") ("default" "gzip"))))))

