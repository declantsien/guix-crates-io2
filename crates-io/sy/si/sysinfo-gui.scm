(define-module (crates-io sy si sysinfo-gui) #:use-module (crates-io))

(define-public crate-sysinfo-gui-0.1.0 (c (n "sysinfo-gui") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "1sw3zk9spjvwl2aqpvg685m0qc0lbzjlwridighjq8ad0j0cfjk2")))

(define-public crate-sysinfo-gui-0.1.1 (c (n "sysinfo-gui") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "0ga9sd4jwqhli45v8w8k36zcdhsxxd0qiwiy5xa1147g9gll2ivz")))

(define-public crate-sysinfo-gui-0.1.3 (c (n "sysinfo-gui") (v "0.1.3") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "0if2l6r6y7byrvgwchmr1sxskcpdjl0s1b7qn5xqx0i0bhnwvn5i")))

(define-public crate-sysinfo-gui-0.1.4 (c (n "sysinfo-gui") (v "0.1.4") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "15jqggqz5i9i0yv1fhz4bdg0s3m2dd9dlrimh0dakba1zx24sivq")))

(define-public crate-sysinfo-gui-0.1.5 (c (n "sysinfo-gui") (v "0.1.5") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "01lahvk0ffp4780zyy7ynl5wbhyia3ii6wbnrxydgq0vkyvl9p1a")))

(define-public crate-sysinfo-gui-0.1.6 (c (n "sysinfo-gui") (v "0.1.6") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "1snpxkxcn0zwzzx15v5pfdzsbx433p8fa1hr09jz9gz1grpsb4yb")))

(define-public crate-sysinfo-gui-0.1.7 (c (n "sysinfo-gui") (v "0.1.7") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "1qw5f0jcij1m6b6qr1j08v5f4955nkpkalpfp3a9ydag4ifi0abq")))

(define-public crate-sysinfo-gui-0.1.8 (c (n "sysinfo-gui") (v "0.1.8") (d (list (d (n "dark-light") (r "^0.2.2") (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "0b66kswdnvf3qs9n2qf0bcgj58c8j421mjdn2rn3255z4llri13p")))

(define-public crate-sysinfo-gui-0.1.9 (c (n "sysinfo-gui") (v "0.1.9") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "02f408hbdrc4wns7qwyc82zhjb7w5x8ss49wglm86b2il3qsl8da")))

(define-public crate-sysinfo-gui-0.1.10 (c (n "sysinfo-gui") (v "0.1.10") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)))) (h "1m1vq5cjsd5p4z3yrg590zmlfqh59gx320zrcwsikq3fmf2iwjd3")))

(define-public crate-sysinfo-gui-0.1.12 (c (n "sysinfo-gui") (v "0.1.12") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.10") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1kc32z357305drhw930yxqmgif15sim1mqycx95xi4b280lz58p6")))

(define-public crate-sysinfo-gui-0.1.13 (c (n "sysinfo-gui") (v "0.1.13") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0iqscp4s8azhd0h9g1y7nwnmfviy6yvc0iim3rvw1d381lsbl4x2")))

(define-public crate-sysinfo-gui-0.1.14 (c (n "sysinfo-gui") (v "0.1.14") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "fltk-grid") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1pp0d08w3vy8pjkx0v5slcw6c816yi7gfql6c2sfs0m2yvwh9wf0")))

(define-public crate-sysinfo-gui-0.1.15 (c (n "sysinfo-gui") (v "0.1.15") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1z9vbqh69njm7168qcc5ka0c6a7fs8a41m61immnmlhwlbzxzy0v")))

(define-public crate-sysinfo-gui-0.1.16 (c (n "sysinfo-gui") (v "0.1.16") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "fltk-extras") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0j6jzjd9iqfivk2z0d96bbs4ffggf9sg00m1mvv602gbsc8ir3g5")))

(define-public crate-sysinfo-gui-0.1.17 (c (n "sysinfo-gui") (v "0.1.17") (d (list (d (n "dark-light") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "fltk-extras") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0g76xjmwvn8v68j5lgvpwakrrjflmz5haxklamrh3b25gbyc5v1x")))

