(define-module (crates-io sy si sysit) #:use-module (crates-io))

(define-public crate-sysit-0.1.0 (c (n "sysit") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6z9hgq7kyjx43i60hxf4lhbmnbkb8gzy784cwnlbjcm0gc22n1")))

(define-public crate-sysit-0.1.1 (c (n "sysit") (v "0.1.1") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqlz200504va9pl2vhpy1dq05yyvpqv11mc8fjyw417ancca2wv")))

(define-public crate-sysit-0.1.2 (c (n "sysit") (v "0.1.2") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("cpu" "memory" "sensors"))) (d #t) (k 0)))) (h "0jjdsc7ydz5hnrzrxwrjrfb4grbdr2ji0wfcxfnlipqjfv00cwif")))

(define-public crate-sysit-0.1.3 (c (n "sysit") (v "0.1.3") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("cpu" "memory" "sensors"))) (d #t) (k 0)))) (h "0vncq7572ss20jmmbkgbdb2nk35jn4l3cd21l9j47yxj1zxiph3j")))

(define-public crate-sysit-0.2.0 (c (n "sysit") (v "0.2.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("cpu" "memory" "sensors"))) (d #t) (k 0)))) (h "04p7yr90kkc4k8gd8vq881n3g4rk1zhyn5bdvhrhc3g9m2hivw9b")))

(define-public crate-sysit-0.2.1 (c (n "sysit") (v "0.2.1") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("cpu" "memory" "sensors"))) (d #t) (k 0)))) (h "0zih77vbsh6725k557ajl6q78p7y93xi968rq3m5r7kym59zmmjq")))

(define-public crate-sysit-0.3.0 (c (n "sysit") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0p89akmcnfrcq4lqxcm8hnr5hzm08nki0a52h0arhy8wwsj6g3cy")))

(define-public crate-sysit-0.4.0 (c (n "sysit") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0bf7a56fwq4qhrjz9jsrsxqcnz1drj5333s47dj8x8zp78ady7q3")))

(define-public crate-sysit-0.4.1 (c (n "sysit") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0jcc0fd21zv1a00i5n0hsgs1vb4z2ivn0d04iiv9kz4alp7nxx97")))

(define-public crate-sysit-0.4.2 (c (n "sysit") (v "0.4.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "1rsbp41lvi3jydd9jv5nq3as8xbfdnxigzilcxkmycd66ka2pllv")))

(define-public crate-sysit-0.4.3 (c (n "sysit") (v "0.4.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "14saqfggcpzrbbpvrld8p2cz3mqg9zar3795bh27j81v9mhp77yl")))

(define-public crate-sysit-0.4.4 (c (n "sysit") (v "0.4.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)))) (h "0qiq4ilm14xhlg2phv9xp8mygsdn828vy9gaq451nbvclw0b301m")))

(define-public crate-sysit-0.5.0 (c (n "sysit") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.20.0") (d #t) (k 0)))) (h "0832cvj20kxj8109zx7myh742g3c2h2vpq8v29ycf34py3ziw41d")))

(define-public crate-sysit-0.6.0 (c (n "sysit") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.20.1") (d #t) (k 0)))) (h "1f5h2l7xjfph8ysgs74njc3qc78d1lh0pyqy7fhw97wnr0lnjl5m")))

(define-public crate-sysit-0.7.0 (c (n "sysit") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21") (d #t) (k 0)))) (h "12ffq58ikhjps2nz93wxwrlz5800zhb03ddrfjg3k5dxpggbq2a3")))

(define-public crate-sysit-0.8.0 (c (n "sysit") (v "0.8.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21") (d #t) (k 0)))) (h "07fbg3anak5grr9hyqpjvsdag9ylwrjwv43rrx6xk4s5v98zskd0")))

(define-public crate-sysit-0.8.1 (c (n "sysit") (v "0.8.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21") (d #t) (k 0)))) (h "1gixxglz2bn7nyi78zfk012swbjzndffsy2pxvzqnq345ypcnshc")))

(define-public crate-sysit-0.8.2 (c (n "sysit") (v "0.8.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "0903px5vwqkjsip4c2p06yraf43xa44h6agmdxld9yzjs7pa8q28")))

(define-public crate-sysit-0.9.0 (c (n "sysit") (v "0.9.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "1gdj3mlak0bfp7qqiw4310xbs8wf9f7mydzw20dq28b2anq0lscq")))

(define-public crate-sysit-0.9.1 (c (n "sysit") (v "0.9.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "1rxflncq3lmf9z87cyrhb1xm5sch3p8b9knvb8qj560q9w651ifd")))

(define-public crate-sysit-0.9.2 (c (n "sysit") (v "0.9.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "03sdssiq36yj5sihmhfbjbxsyfbl7z8j3cm2iys06dqrygs7m8ah")))

(define-public crate-sysit-0.10.0 (c (n "sysit") (v "0.10.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "1l08j9c565sqjbcx9sfdfb5lh9afpn4xi447ida4zgqvh3p2ri99")))

