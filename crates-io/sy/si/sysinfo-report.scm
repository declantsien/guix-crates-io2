(define-module (crates-io sy si sysinfo-report) #:use-module (crates-io))

(define-public crate-sysinfo-report-0.1.0 (c (n "sysinfo-report") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.12") (d #t) (k 0)))) (h "146fia3h89gwa4d6rgwjkg4pjchdh141dia8m2714m69hdkrv45g")))

(define-public crate-sysinfo-report-0.2.0 (c (n "sysinfo-report") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.12") (d #t) (k 0)))) (h "0f94svwiyj8a1fhips62iv9q1g5rydfqcr2plkmcqvv93j13nl8l")))

(define-public crate-sysinfo-report-1.0.0 (c (n "sysinfo-report") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "1z3626f2fgfimv25d8wh5wh653md5labl30cqwwyc3yjidzyz2zr")))

(define-public crate-sysinfo-report-1.1.0 (c (n "sysinfo-report") (v "1.1.0") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "1plglkzjh4p53slv1sk0yg7cbw1dw2jyd0rgff9k10wx4kbn3fbk")))

(define-public crate-sysinfo-report-1.1.1 (c (n "sysinfo-report") (v "1.1.1") (d (list (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "1fmsagkwwm5gd7bxba0bw02wxkiplfw8vzgvir8y6kbgbg9x2zjs")))

(define-public crate-sysinfo-report-1.1.2 (c (n "sysinfo-report") (v "1.1.2") (d (list (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0imyx1b35fmbnzb8dvxnzbj8jnqmaayqicsrp3qsbgsr4cf39vki")))

