(define-module (crates-io sy sw syswall_cli) #:use-module (crates-io))

(define-public crate-syswall_cli-0.1.1 (c (n "syswall_cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "syswall") (r "^0.3.0") (d #t) (k 0)))) (h "1szym3i5a4vpar0x7wadqy78kqnarfw1c2mg7bb1lsvrhzinfywk")))

(define-public crate-syswall_cli-0.1.2 (c (n "syswall_cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "syswall") (r "^0.3.1") (d #t) (k 0)))) (h "06cdxyjv7hcsdzh1hc2p76413xmndk1wdk13fkz1x5pwy6i94qw0")))

