(define-module (crates-io sy sw syswall) #:use-module (crates-io))

(define-public crate-syswall-0.3.0 (c (n "syswall") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.8") (d #t) (k 0)))) (h "1s2zmx2grvw33zjnq9lh06558dajrn5v0fnfjsx5wchr56sjhgc6")))

(define-public crate-syswall-0.3.1 (c (n "syswall") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.8") (d #t) (k 0)))) (h "1ch27xhhg66fi3z1zpwsy9dbchh16pszfpywv86clj0lhab3xnkk")))

