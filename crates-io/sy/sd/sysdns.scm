(define-module (crates-io sy sd sysdns) #:use-module (crates-io))

(define-public crate-sysdns-0.1.0 (c (n "sysdns") (v "0.1.0") (d (list (d (n "interfaces") (r "^0.0.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17f3r2np3x0mndvam6xanwj8a94905hwk30cf1hab8x2zxpi7rv1")))

