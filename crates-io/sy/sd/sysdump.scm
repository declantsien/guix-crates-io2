(define-module (crates-io sy sd sysdump) #:use-module (crates-io))

(define-public crate-sysdump-0.1.0 (c (n "sysdump") (v "0.1.0") (d (list (d (n "default-net") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.2") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)))) (h "0qsr59bjkzq7d4d5ag52i8iq7fq09ci4x0jangcmkzkigdh7n7df")))

