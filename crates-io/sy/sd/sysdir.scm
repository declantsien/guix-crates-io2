(define-module (crates-io sy sd sysdir) #:use-module (crates-io))

(define-public crate-sysdir-1.0.0 (c (n "sysdir") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1wvznnr57m0dbc509jw2mcizgafp4q3aj7vmz4hy4k6ja410rj52") (r "1.64.0")))

(define-public crate-sysdir-1.1.0 (c (n "sysdir") (v "1.1.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "19j6k76mplhk1r001w76z2icxnx11gi2ymbi50mylz66iyahv4np") (r "1.64.0")))

(define-public crate-sysdir-1.2.0 (c (n "sysdir") (v "1.2.0") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1pwlmcywa1qx42jdha26sbm4vjb6w3dqfcwz1cyfpici2w5qxhak") (r "1.64.0")))

(define-public crate-sysdir-1.2.1 (c (n "sysdir") (v "1.2.1") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "148839862fldr6vlr5w22dx5k985mgkhvg4ikrjc04phz9j7nk25") (r "1.64.0")))

(define-public crate-sysdir-1.2.2 (c (n "sysdir") (v "1.2.2") (d (list (d (n "version-sync") (r "^0.9.4") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1w1352rva864zgzqa77kv2vqd29f4fa3j3z1la52lh114swrhip1") (r "1.64.0")))

