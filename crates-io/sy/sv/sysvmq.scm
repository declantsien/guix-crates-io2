(define-module (crates-io sy sv sysvmq) #:use-module (crates-io))

(define-public crate-sysvmq-0.1.0 (c (n "sysvmq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "09wd1ciic3sskaf3m2z1ypkb06jpar28y3j5m71ac0ww4z1b28wx")))

(define-public crate-sysvmq-0.1.1 (c (n "sysvmq") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0yrm881ajbr257sdns1zp8i285wq1z65zk13y6llim1krzag8yz1")))

