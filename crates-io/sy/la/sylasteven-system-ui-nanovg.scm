(define-module (crates-io sy la sylasteven-system-ui-nanovg) #:use-module (crates-io))

(define-public crate-sylasteven-system-ui-nanovg-0.1.0 (c (n "sylasteven-system-ui-nanovg") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.1") (d #t) (k 0)))) (h "02p4zyghpjrf466li5smcbk96smcpsv79qpvk62jbr83biic6y3g")))

(define-public crate-sylasteven-system-ui-nanovg-0.1.1 (c (n "sylasteven-system-ui-nanovg") (v "0.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nanovg") (r "^1.0.2") (f (quote ("gl3"))) (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.1") (d #t) (k 0)))) (h "1skdl64px8dxnysrgj3m9hn6l24pf81qnzh7s8g2n3ggchp4k3qm")))

