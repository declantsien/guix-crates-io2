(define-module (crates-io sy la sylasteven-system-input-default) #:use-module (crates-io))

(define-public crate-sylasteven-system-input-default-0.1.0 (c (n "sylasteven-system-input-default") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.1") (d #t) (k 0)))) (h "1zp653kq32nwzzjnijk8d60iz6bvlg6bgpc27asagf1bsbwsrbri")))

