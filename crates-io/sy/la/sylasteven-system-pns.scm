(define-module (crates-io sy la sylasteven-system-pns) #:use-module (crates-io))

(define-public crate-sylasteven-system-pns-0.1.0 (c (n "sylasteven-system-pns") (v "0.1.0") (d (list (d (n "pns") (r "^0.5.1") (f (quote ("static-build"))) (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.0") (d #t) (k 0)))) (h "1zkciy38zpb3ilxn5g0vyr3smzhari17xjafxlf71wwzcq8pc09n")))

(define-public crate-sylasteven-system-pns-0.1.1 (c (n "sylasteven-system-pns") (v "0.1.1") (d (list (d (n "pns") (r "^0.5.1") (f (quote ("static-build"))) (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.1") (d #t) (k 0)))) (h "0925ihspv32by6f2pc35wqq0sx41mqvbhswm80ayidh3h935avq0")))

(define-public crate-sylasteven-system-pns-0.1.2 (c (n "sylasteven-system-pns") (v "0.1.2") (d (list (d (n "pns") (r "^0.5.2") (f (quote ("static-build"))) (d #t) (k 0)) (d (n "sylasteven") (r "^0.1.1") (d #t) (k 0)))) (h "06kmva344x4zy6c0bji55kv18j0m8i0pyk9k97ag0352w4s3g53n")))

