(define-module (crates-io sy dn sydney) #:use-module (crates-io))

(define-public crate-sydney-0.1.0 (c (n "sydney") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "014hmn87a4k7jhk610zzjq40n2v03zfbg557f0lck64c58k1wkv3")))

(define-public crate-sydney-0.1.1 (c (n "sydney") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1cd15xvrc29pkjmq66xfp7cqiglgpvbkawgkzzpfcna9m8pqs0r1")))

(define-public crate-sydney-0.1.2 (c (n "sydney") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1pw9hah78w4hkn09r3wcgzknyd9ks37xvhnj9w9xr80n1jaw3r6j")))

(define-public crate-sydney-0.1.3 (c (n "sydney") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0m7cph8sdj94q85b6x5c0gm1p1zxpbglc1yqw43m1bvsvkdpnk5j")))

(define-public crate-sydney-0.1.4 (c (n "sydney") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "016kqpqc3r7as6z6fz03ja0ad6kwmms40is0wvbvfh5a8621j0sk")))

(define-public crate-sydney-0.1.5 (c (n "sydney") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0zdvhak35b0msc5qwmmp93rwd1bzy43z1xn3wilsplw2sr5pfd4i")))

(define-public crate-sydney-0.1.6 (c (n "sydney") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "091d8fl47bvcnzb84v9zayhs1x0281k89m76wf07xvsr8yd6j39g")))

(define-public crate-sydney-0.1.7 (c (n "sydney") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0f7f13345qpcc0zwz9b80nw0cwfvr9hp2ahc4lwfz6jp8bn7bc6b")))

(define-public crate-sydney-0.1.8 (c (n "sydney") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.7") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1a5l8mcnh08fzm1hwiv97m3mcgb5jv3yr5hz27y3zdxjqyls16qn")))

(define-public crate-sydney-0.1.9 (c (n "sydney") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.7") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1w30i6wii4xkz870a1kl47bpc032m4h8pp49qcwfggnqpgb03z5c")))

(define-public crate-sydney-0.1.10 (c (n "sydney") (v "0.1.10") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.7") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "15c8h2vrg6mi1sfmacg94rnq7hys8r05mcxy9wm6zv4rjd8f6sv5")))

