(define-module (crates-io sy mp symphonia-format-riff) #:use-module (crates-io))

(define-public crate-symphonia-format-riff-0.5.4 (c (n "symphonia-format-riff") (v "0.5.4") (d (list (d (n "extended") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)))) (h "0l2zs6zl7q15jhsk9j1lahs2j29k5kkcn5bi9dzr6bwn5wivxxq5") (f (quote (("wav") ("default" "aiff" "wav") ("aiff")))) (r "1.53")))

