(define-module (crates-io sy mp symparser) #:use-module (crates-io))

(define-public crate-symparser-0.1.0 (c (n "symparser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1rjf4s8n1h5gj2npg3dh8gwd2v5clc31smfkh6v3p8nli5k34vsb")))

(define-public crate-symparser-1.0.0 (c (n "symparser") (v "1.0.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1isj1f3w41v2d8q1v4yf3grp6hz39mk24f8dwqhd19ikw7pb7yiv") (f (quote (("v6") ("v5") ("default" "v6"))))))

