(define-module (crates-io sy mp symphonia-core) #:use-module (crates-io))

(define-public crate-symphonia-core-0.1.0 (c (n "symphonia-core") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ni9hfj8qm8zswsaqbfhm4j7q90gca0hnp3572bq2myhczqf71gm")))

(define-public crate-symphonia-core-0.2.0 (c (n "symphonia-core") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pa84kfpzp6m5m1k6ynxkzj443bhlywglmy2v80wd61x8xbbjciy")))

(define-public crate-symphonia-core-0.3.0 (c (n "symphonia-core") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.6.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00ah245sgj1rqvi0mkcqni4a4l7064r46lzfmgbpwjhbyl32wx61")))

(define-public crate-symphonia-core-0.4.0 (c (n "symphonia-core") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1j84q4a9p9qa23976spxap9s6ns3fm6fzrfz65n6cjhgpsbmw4zs")))

(define-public crate-symphonia-core-0.5.0 (c (n "symphonia-core") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "079rc1ha3q037mvm1b3ljvraagqmg2kr18pqns4bc0jys9ab5p0y") (r "1.53")))

(define-public crate-symphonia-core-0.5.1 (c (n "symphonia-core") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ixkqllgd4f99lvfn36xg85hbsjqjf2lpdl9jb3vl5a1rlbn96hr") (r "1.53")))

(define-public crate-symphonia-core-0.5.2 (c (n "symphonia-core") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sc8sf86c6lyvqvnvzxwp8k6h30yh5nkcpagz6r6dy55v3i6g5bb") (r "1.53")))

(define-public crate-symphonia-core-0.5.3 (c (n "symphonia-core") (v "0.5.3") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hj1fmq6k1ik10fpbawjw1v7nfn9gcn78yycd1970ygfiyw3xizp") (r "1.53")))

(define-public crate-symphonia-core-0.5.4 (c (n "symphonia-core") (v "0.5.4") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (o #t) (k 0)))) (h "1hr2w2a217vq4lpghszmsdwxr5ilh5d1ysfm3cixbirxkrvhd0vr") (f (quote (("opt-simd-sse" "rustfft/sse") ("opt-simd-neon" "rustfft/neon") ("opt-simd-avx" "rustfft/avx") ("opt-simd" "opt-simd-sse" "opt-simd-avx" "opt-simd-neon") ("default")))) (r "1.53")))

