(define-module (crates-io sy mp symptom) #:use-module (crates-io))

(define-public crate-symptom-0.0.0 (c (n "symptom") (v "0.0.0") (h "0s8gnr2mkxmf4v1bfcwr0liw2ikwy33d5k52r76i0dwga2isc5nc") (y #t)))

(define-public crate-symptom-0.0.1 (c (n "symptom") (v "0.0.1") (d (list (d (n "gtk") (r "^0.17.1") (d #t) (k 0)))) (h "1gkhbjmdygazvm0q8dx5b1015a53l77dnmxphs5clpwnp7d9mql7") (y #t)))

(define-public crate-symptom-0.0.11 (c (n "symptom") (v "0.0.11") (d (list (d (n "gtk") (r "^0.17.1") (d #t) (k 0)))) (h "0c10pfk7n5xgx5g53w27hbrbr3n3yss7dbqaf81s5rc5pkm2kj9s")))

