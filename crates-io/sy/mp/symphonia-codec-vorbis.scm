(define-module (crates-io sy mp symphonia-codec-vorbis) #:use-module (crates-io))

(define-public crate-symphonia-codec-vorbis-0.4.0 (c (n "symphonia-codec-vorbis") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.4") (d #t) (k 0)))) (h "1yj1si92fnnzdfkw27cq324h6y1s958s8r2hl0szpvvqh1sdd7m2")))

(define-public crate-symphonia-codec-vorbis-0.5.0 (c (n "symphonia-codec-vorbis") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "1i7rp164hs1z28kjf5c1jcyxijynzzjk55jzarvy4dvbjrcw8vvl") (r "1.53")))

(define-public crate-symphonia-codec-vorbis-0.5.1 (c (n "symphonia-codec-vorbis") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "1hv73p5scac4gr4lfp8l5fvhyrpjmsp915720487x00sb91r8frj") (r "1.53")))

(define-public crate-symphonia-codec-vorbis-0.5.2 (c (n "symphonia-codec-vorbis") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.2") (d #t) (k 0)))) (h "0x3vip9cn1qvhy8wl6lac3qhvf2xms7grkhsxxy1v8mznvvxdzkx") (r "1.53")))

(define-public crate-symphonia-codec-vorbis-0.5.3 (c (n "symphonia-codec-vorbis") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.3") (d #t) (k 0)))) (h "00z295bncyyhb022z93pd13qvdcm9xz821a21hsh3ah66mz3jlrr") (r "1.53")))

(define-public crate-symphonia-codec-vorbis-0.5.4 (c (n "symphonia-codec-vorbis") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.4") (d #t) (k 0)))) (h "0c4z98b8yg2kws3pknw7ipvvca911j3y5xq7n0r6f2kanigpd62s") (r "1.53")))

