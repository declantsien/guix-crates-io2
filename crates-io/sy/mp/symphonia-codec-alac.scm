(define-module (crates-io sy mp symphonia-codec-alac) #:use-module (crates-io))

(define-public crate-symphonia-codec-alac-0.5.0 (c (n "symphonia-codec-alac") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "02l6vaxpfzyq4bj3xpmb5xh5mg89wn86kqkigr3djmacv74hqpd9") (r "1.53")))

(define-public crate-symphonia-codec-alac-0.5.1 (c (n "symphonia-codec-alac") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "00hgjmdgh08h63xlg2x052hf9k79r31p2lzkalppzw1bygb3h925") (r "1.53")))

(define-public crate-symphonia-codec-alac-0.5.2 (c (n "symphonia-codec-alac") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)))) (h "1724ly8qj3i5sjvqd5vx7aa10cjr3xgxhas5zykyzg0qjch1p7n4") (r "1.53")))

(define-public crate-symphonia-codec-alac-0.5.3 (c (n "symphonia-codec-alac") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)))) (h "1f83551nh7ac44y4xjizm7vy4kcxm5rkx45gdxkgz7hw7mvfh9rs") (r "1.53")))

(define-public crate-symphonia-codec-alac-0.5.4 (c (n "symphonia-codec-alac") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)))) (h "1wrq1s6w029bz7lqj08q87i375wvzl78nsj70qll224scik6d2id") (r "1.53")))

