(define-module (crates-io sy mp symphonia-format-wav) #:use-module (crates-io))

(define-public crate-symphonia-format-wav-0.1.0 (c (n "symphonia-format-wav") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.1") (d #t) (k 0)))) (h "0xjx4bz1nghblqjij104pfz8vqz9j1vv6w6ww99a7h11kv4s2d6g")))

(define-public crate-symphonia-format-wav-0.2.0 (c (n "symphonia-format-wav") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)))) (h "1m8j7jb5pndpgdlcbfllm5hqx8jypvpa24cw07kx3jdiydypph98")))

(define-public crate-symphonia-format-wav-0.3.0 (c (n "symphonia-format-wav") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)))) (h "0dzgby8f6q4vm3mm20kqxfhn3fdj70p2jr76za6m3shjm7cn7s4p")))

(define-public crate-symphonia-format-wav-0.4.0 (c (n "symphonia-format-wav") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)))) (h "0xcmj0162lzwj6qgz6qi9xw67h23cvsd655f37zz6ld98klqcdfs")))

(define-public crate-symphonia-format-wav-0.4.1 (c (n "symphonia-format-wav") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)))) (h "1b8x213s44xis4pb1ibnqr1a20hsxf3phm527dvadpi0nkjsb7vd")))

(define-public crate-symphonia-format-wav-0.5.0 (c (n "symphonia-format-wav") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "1h3jh0gci937xq4ajibq0ylp10a52qqrjm4kbzlybxx9frjh3ck6") (r "1.53")))

(define-public crate-symphonia-format-wav-0.5.1 (c (n "symphonia-format-wav") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "08i8pq7lkg3lllfw83sqmp0f3db0d87i2ag6f6vhb7w8lqd7g7az") (r "1.53")))

(define-public crate-symphonia-format-wav-0.5.2 (c (n "symphonia-format-wav") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)))) (h "15l2z0i7qpcfm4bwq1ij2ghw873fiby1v2c81wq3fc3bckarnrq6") (r "1.53")))

(define-public crate-symphonia-format-wav-0.5.3 (c (n "symphonia-format-wav") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)))) (h "1903xxcaqaqx5zkfhpy9jjzzrn3b758sryydpl1w09zs513n2xns") (r "1.53")))

(define-public crate-symphonia-format-wav-0.5.4 (c (n "symphonia-format-wav") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)))) (h "1zqj3c1gxymm8m45fh378fsg2ixpgxczjd0vgh7330jykbxgdw4w") (r "1.53")))

