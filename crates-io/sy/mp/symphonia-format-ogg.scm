(define-module (crates-io sy mp symphonia-format-ogg) #:use-module (crates-io))

(define-public crate-symphonia-format-ogg-0.1.0 (c (n "symphonia-format-ogg") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.1") (d #t) (k 0)))) (h "0gmh983f130mdnga9ixx8r22w31nxs9yqlgshz0l8jdz6ib89vpm")))

(define-public crate-symphonia-format-ogg-0.2.0 (c (n "symphonia-format-ogg") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.2") (d #t) (k 0)))) (h "0gami969xa13qs99v7sp6bmk5ck2iz80ybrdshw094ml2smrkqc7")))

(define-public crate-symphonia-format-ogg-0.3.0 (c (n "symphonia-format-ogg") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.3") (d #t) (k 0)))) (h "1h8q9jd4p4n8gh0fsyc95ijdfg22wjpz2prv3xp161asbkwfx628")))

(define-public crate-symphonia-format-ogg-0.4.0 (c (n "symphonia-format-ogg") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.4") (d #t) (k 0)))) (h "06d5327m4yk8a6yq7zzyiv2sbkwnjq28dz9cagndz6m7i1r3bcnp")))

(define-public crate-symphonia-format-ogg-0.5.0 (c (n "symphonia-format-ogg") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "0655k4j76zglwnkdrjpga4qmnydzs7d2ccxyklyqfw3358mbkx80") (r "1.53")))

(define-public crate-symphonia-format-ogg-0.5.1 (c (n "symphonia-format-ogg") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "0zcv7mnqqmb4cxhxr7ga8jh3pc80gxgn1wnigrh07wd0d4a78bwd") (r "1.53")))

(define-public crate-symphonia-format-ogg-0.5.2 (c (n "symphonia-format-ogg") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.2") (d #t) (k 0)))) (h "0fa49i4ma54qlbca5i2p6145y9202kdblc1ij5bcn7c7dglgcka7") (r "1.53")))

(define-public crate-symphonia-format-ogg-0.5.3 (c (n "symphonia-format-ogg") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.3") (d #t) (k 0)))) (h "13acazi7wnvnv3cxn604cnp7ixs0h216h0wa0i22si8irl6a1wcv") (r "1.53")))

(define-public crate-symphonia-format-ogg-0.5.4 (c (n "symphonia-format-ogg") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.4") (d #t) (k 0)))) (h "0cd9py2xgx211qvwl9sw8n5l5vgd55vwcmqizh0cyssii5bm18xd") (r "1.53")))

