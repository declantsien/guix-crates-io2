(define-module (crates-io sy mp symphonia-codec-pcm) #:use-module (crates-io))

(define-public crate-symphonia-codec-pcm-0.1.0 (c (n "symphonia-codec-pcm") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)))) (h "0ff23gfxfa9lqbwk996a48p0b7ix2xgwfj4jfk5g8b515fa86n4j")))

(define-public crate-symphonia-codec-pcm-0.2.0 (c (n "symphonia-codec-pcm") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)))) (h "0wnzk0b7ldgsghnfmmqp4za39r2d17p0xcxy1h1mdppcqd3z4br5")))

(define-public crate-symphonia-codec-pcm-0.3.0 (c (n "symphonia-codec-pcm") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)))) (h "1v1dsjyhkbijw39nqhlzrmi3id8zqxkhra7lghfaigl6n5hq2782")))

(define-public crate-symphonia-codec-pcm-0.4.0 (c (n "symphonia-codec-pcm") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)))) (h "1is49qjnfy541zpgzz498hnpz0nsq7i4nfky2133b6aqhxrm87ds")))

(define-public crate-symphonia-codec-pcm-0.5.0 (c (n "symphonia-codec-pcm") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "0443ryb04iz2ixxbl4b5bmlrnkdaqisg24pj7yb1zzrdc2bs24h8") (r "1.53")))

(define-public crate-symphonia-codec-pcm-0.5.1 (c (n "symphonia-codec-pcm") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "0g24133667cyv5vwfvi6v1761sv86li52pibnrmdmaslr49a9k0p") (r "1.53")))

(define-public crate-symphonia-codec-pcm-0.5.2 (c (n "symphonia-codec-pcm") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)))) (h "1jgkpp50wvpj8clmwpl1lj1d1m8jyi568x3v47rwq74rp7qakfcc") (r "1.53")))

(define-public crate-symphonia-codec-pcm-0.5.3 (c (n "symphonia-codec-pcm") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)))) (h "1any34qhsfyl4m6h9nal1k6favpgyl8avpg2ihf68sm0439gpwa7") (r "1.53")))

(define-public crate-symphonia-codec-pcm-0.5.4 (c (n "symphonia-codec-pcm") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)))) (h "16zq2s8zf0rs6070y3sfyscvm9z1riqvxcbv9plcbsy2axqad5gk") (r "1.53")))

