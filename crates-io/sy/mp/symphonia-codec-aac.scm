(define-module (crates-io sy mp symphonia-codec-aac) #:use-module (crates-io))

(define-public crate-symphonia-codec-aac-0.2.0 (c (n "symphonia-codec-aac") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)))) (h "0w9vbc7fhsjbbjhnbwflw1wd1xnj2bzssz21m74sy90r7a8q2ds9")))

(define-public crate-symphonia-codec-aac-0.3.0 (c (n "symphonia-codec-aac") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)))) (h "0gvcj6x92immabdpibhdd6y4p4gsxg9vhrl09yr05cngvhi68qwy")))

(define-public crate-symphonia-codec-aac-0.4.0 (c (n "symphonia-codec-aac") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)))) (h "13smaxgb1jadl4jyay7hixqgwaiqrjvsvmzdvlbdzdxrgsrplgdx")))

(define-public crate-symphonia-codec-aac-0.5.0 (c (n "symphonia-codec-aac") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "0s04119la073h3bbis7cdyd8px16vlh5hb41k7l1ai1sz2y1srwn") (r "1.53")))

(define-public crate-symphonia-codec-aac-0.5.1 (c (n "symphonia-codec-aac") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "0xj714r5ds585w5vh7z795ygy6ibwlcdyqw5880iphr8jjw3354y") (r "1.53")))

(define-public crate-symphonia-codec-aac-0.5.2 (c (n "symphonia-codec-aac") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)))) (h "06l7ikgffrrnd68jgdhf6mbh6cl60l1fqq13awl0bjlm0shx9kbg") (r "1.53")))

(define-public crate-symphonia-codec-aac-0.5.3 (c (n "symphonia-codec-aac") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)))) (h "178hd0qlsl1kappjgln1gyyjs326lbz0pljb5aqq8jyf4mdxggb8") (r "1.53")))

(define-public crate-symphonia-codec-aac-0.5.4 (c (n "symphonia-codec-aac") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)))) (h "0w1ga9c7m5bb11rc9bpnjb5g9bqms4x69slix3ikw3dd8nsjbgyd") (r "1.53")))

