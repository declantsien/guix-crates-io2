(define-module (crates-io sy mp symphonia-format-isomp4) #:use-module (crates-io))

(define-public crate-symphonia-format-isomp4-0.2.0 (c (n "symphonia-format-isomp4") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)))) (h "0k9bs8h3nvgdf036wq811jlqh19jn38gspsr58d4jjqm0zmhy653")))

(define-public crate-symphonia-format-isomp4-0.3.0 (c (n "symphonia-format-isomp4") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)))) (h "19bdmhk2gzb3zc61vq6vn21jdhzh17awy11gswialz1lldn6ng5z")))

(define-public crate-symphonia-format-isomp4-0.4.0 (c (n "symphonia-format-isomp4") (v "0.4.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)))) (h "1dap5yh286j74sybjsam378v1jxkpdl3hvvm81sipv7725vkmvpy")))

(define-public crate-symphonia-format-isomp4-0.5.0 (c (n "symphonia-format-isomp4") (v "0.5.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "0l8rmhg4bsk76pb9xb9p8zbsa52wgcxsjs5rjb7g62w4q4b5hcra") (r "1.53")))

(define-public crate-symphonia-format-isomp4-0.5.1 (c (n "symphonia-format-isomp4") (v "0.5.1") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "145qcyxg2jbciqdsqjy2xdibnvs034nxybp18g6dyirjlv789khx") (r "1.53")))

(define-public crate-symphonia-format-isomp4-0.5.2 (c (n "symphonia-format-isomp4") (v "0.5.2") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.2") (d #t) (k 0)))) (h "0dx4xdjw0gxxvnd4rzxigrhpxi9plv6dpa04q1gbmsmlaipqz0f1") (r "1.53")))

(define-public crate-symphonia-format-isomp4-0.5.3 (c (n "symphonia-format-isomp4") (v "0.5.3") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.3") (d #t) (k 0)))) (h "1yqyg91d8i1va4w35grwscljs96nwm8l3ikb84r20dfgwnx19pzz") (r "1.53")))

(define-public crate-symphonia-format-isomp4-0.5.4 (c (n "symphonia-format-isomp4") (v "0.5.4") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.4") (d #t) (k 0)))) (h "0i68dnhp3q7hv4i51hryw0c75i4l3fx85ffrwphhrrcpsrwg3zdb") (r "1.53")))

