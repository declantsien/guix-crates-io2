(define-module (crates-io sy mp symphonia-format-mkv) #:use-module (crates-io))

(define-public crate-symphonia-format-mkv-0.5.0 (c (n "symphonia-format-mkv") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "09i7fn9n989312n1xxdb1ng66bfrn408m6yb7r57kcw54bk546lh") (r "1.53")))

(define-public crate-symphonia-format-mkv-0.5.1 (c (n "symphonia-format-mkv") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "15iv27zlqz8j842q0xwwlncnm6mf5231j5cmw5q0rk5j601l01gl") (r "1.53")))

(define-public crate-symphonia-format-mkv-0.5.2 (c (n "symphonia-format-mkv") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.2") (d #t) (k 0)))) (h "069iixc6bdkw0zr78dz35qidbixz90b12rmdjn20g3wcxwnjzlhv") (r "1.53")))

(define-public crate-symphonia-format-mkv-0.5.3 (c (n "symphonia-format-mkv") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.3") (d #t) (k 0)))) (h "15c8sm41iqmd46nyvh3c5b87r3vyc7c33hnq8d05vlhshpy1vipm") (r "1.53")))

(define-public crate-symphonia-format-mkv-0.5.4 (c (n "symphonia-format-mkv") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.4") (d #t) (k 0)))) (h "0vrxzr95d1xk2l5jarp7k2935s5ybsyrawwkr4nqixq0l5qk9d0v") (r "1.53")))

