(define-module (crates-io sy mp symphonia-utils-xiph) #:use-module (crates-io))

(define-public crate-symphonia-utils-xiph-0.1.0 (c (n "symphonia-utils-xiph") (v "0.1.0") (d (list (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.1") (d #t) (k 0)))) (h "0kc6zpdzkvmkki244g4jfqb9lpq819rma7bbsxdxdqysj9v4y0qw")))

(define-public crate-symphonia-utils-xiph-0.2.0 (c (n "symphonia-utils-xiph") (v "0.2.0") (d (list (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)))) (h "0rwm6n7q3bwz942652zqv7xci8yszd2jwm21c71cg3gv1lxrkrf5")))

(define-public crate-symphonia-utils-xiph-0.3.0 (c (n "symphonia-utils-xiph") (v "0.3.0") (d (list (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)))) (h "0an4jiiyqskl9l7439bpaland0mcgix8wjq539dlvkqsss37sds7")))

(define-public crate-symphonia-utils-xiph-0.4.0 (c (n "symphonia-utils-xiph") (v "0.4.0") (d (list (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)))) (h "1sg1y7s607rk1akrrzyhdsqimiwwaw440jzr1cp89zs8d5n04dva")))

(define-public crate-symphonia-utils-xiph-0.5.0 (c (n "symphonia-utils-xiph") (v "0.5.0") (d (list (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "1q33lz2vdcsxck83bdzv8bvzssyhkl0s0m15dy1kg94z6m9zmbdb") (r "1.53")))

(define-public crate-symphonia-utils-xiph-0.5.1 (c (n "symphonia-utils-xiph") (v "0.5.1") (d (list (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "0camgldf3d26yadvxwsc5hdnyysyckdki52zagpqlyp7divb1jvk") (r "1.53")))

(define-public crate-symphonia-utils-xiph-0.5.2 (c (n "symphonia-utils-xiph") (v "0.5.2") (d (list (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)))) (h "0sqkawjkjrs5yfm69ijw7lm8lvh5xh4068h1vr1cn1mc6dn0ld6f") (r "1.53")))

(define-public crate-symphonia-utils-xiph-0.5.3 (c (n "symphonia-utils-xiph") (v "0.5.3") (d (list (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)))) (h "1y2ir4a5f10hp6m050lr545r883zvjznqmrmigzrmml0bdjcll54") (r "1.53")))

(define-public crate-symphonia-utils-xiph-0.5.4 (c (n "symphonia-utils-xiph") (v "0.5.4") (d (list (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)))) (h "1zhhs1p0h6wdcgcwfqpmqq07n8v2wvn50razvapr36d41xc74i28") (r "1.53")))

