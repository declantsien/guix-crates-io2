(define-module (crates-io sy mp symphonia-codec-adpcm) #:use-module (crates-io))

(define-public crate-symphonia-codec-adpcm-0.5.2 (c (n "symphonia-codec-adpcm") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)))) (h "0gkfwawnnvcljlmfsf2bfy6jn42yn12xqmrib6wnxqh58j6znp1a") (r "1.53")))

(define-public crate-symphonia-codec-adpcm-0.5.3 (c (n "symphonia-codec-adpcm") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)))) (h "0l0iqh5g2409z7gl711v58xkm9sk0l39v1qqcdxqr0axhv0ps3l7") (r "1.53")))

(define-public crate-symphonia-codec-adpcm-0.5.4 (c (n "symphonia-codec-adpcm") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)))) (h "03va885srhrzfz31jvxh2rgr9crnmmlvxmbkx4bdcz1jqgm1ykn9") (r "1.53")))

