(define-module (crates-io sy mp symphonia-bundle-mp3) #:use-module (crates-io))

(define-public crate-symphonia-bundle-mp3-0.1.0 (c (n "symphonia-bundle-mp3") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.1") (d #t) (k 0)))) (h "19q5adyz0j7krwix2k7fifnl4847r2ghawrjq01prjz7581ylhxa")))

(define-public crate-symphonia-bundle-mp3-0.2.0 (c (n "symphonia-bundle-mp3") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)))) (h "12zb8v8if9i33mra59jwibhz0b79ml50gy040a1a2rkhwyaji3q5")))

(define-public crate-symphonia-bundle-mp3-0.3.0 (c (n "symphonia-bundle-mp3") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)))) (h "1yl36909qsg4kdaiylbcxv7jszp2c55n8n2583lhdbnj2vz9dxdr")))

(define-public crate-symphonia-bundle-mp3-0.4.0 (c (n "symphonia-bundle-mp3") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)))) (h "14074njhgrcgh2p5iryrd68mgdzcxf9v7p8xfm8ldkhylv29fkgc")))

(define-public crate-symphonia-bundle-mp3-0.5.0 (c (n "symphonia-bundle-mp3") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "0qca93xv0v0r6jdmi65njxbcks806p9797bmb15j6zs4c7kclc4i") (r "1.53")))

(define-public crate-symphonia-bundle-mp3-0.5.1 (c (n "symphonia-bundle-mp3") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)))) (h "0glxjl9ps41y33qmz8mcidn8lff5ra1zd0lywg8bgqjsad9kspfv") (r "1.53")))

(define-public crate-symphonia-bundle-mp3-0.5.2 (c (n "symphonia-bundle-mp3") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)))) (h "0wrb5blf7hkylx2gchx1s9x42w2ijwxci7vrzy0q16icg9p8982m") (f (quote (("mp3") ("mp2") ("mp1") ("default" "mp1" "mp2" "mp3")))) (r "1.53")))

(define-public crate-symphonia-bundle-mp3-0.5.3 (c (n "symphonia-bundle-mp3") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)))) (h "12inws5f7cbkc0pxblwgs6xk70clmv79w6h1fdliwvslrvzdfc8g") (f (quote (("mp3") ("mp2") ("mp1") ("default" "mp1" "mp2" "mp3")))) (r "1.53")))

(define-public crate-symphonia-bundle-mp3-0.5.4 (c (n "symphonia-bundle-mp3") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)))) (h "1m062zkxq2cbwqxbm3qp4qvgpc9hm49g23vgdc4zpwghf2p2l760") (f (quote (("mp3") ("mp2") ("mp1") ("default" "mp1" "mp2" "mp3")))) (r "1.53")))

