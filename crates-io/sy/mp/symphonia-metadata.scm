(define-module (crates-io sy mp symphonia-metadata) #:use-module (crates-io))

(define-public crate-symphonia-metadata-0.1.0 (c (n "symphonia-metadata") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)))) (h "1pihkv30vpj3qkjkvh6w54izl4arda8v8jjb4m6nbff5d6blkm7d")))

(define-public crate-symphonia-metadata-0.2.0 (c (n "symphonia-metadata") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)))) (h "1rsdxhk8kk6mjcyz5p4h96n268bzdqclpy6qdv6q3cdaakb6krg8")))

(define-public crate-symphonia-metadata-0.3.0 (c (n "symphonia-metadata") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)))) (h "0p2nmsp3ln4xqx0ss19rx51dwiw61ynffdciarlgj03libikcpnv")))

(define-public crate-symphonia-metadata-0.4.0 (c (n "symphonia-metadata") (v "0.4.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)))) (h "06lvwy24kirc84r6d23ncad544525fsb6gna0plqz3d1mffmjq2j")))

(define-public crate-symphonia-metadata-0.5.0 (c (n "symphonia-metadata") (v "0.5.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "03mxf8z5inb3l532c56a42vrds8jadmchqbjp0cvkllzr5jyckph") (r "1.53")))

(define-public crate-symphonia-metadata-0.5.1 (c (n "symphonia-metadata") (v "0.5.1") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)))) (h "0ywrwmphlqjpfgs8s8bk3n6bf1js6s37nn892pp8nvjydg7immvf") (r "1.53")))

(define-public crate-symphonia-metadata-0.5.2 (c (n "symphonia-metadata") (v "0.5.2") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)))) (h "0bmvqya3x6qnxzrknmpabq33xpkm9899prqb01hn3vr368k5rlxc") (r "1.53")))

(define-public crate-symphonia-metadata-0.5.3 (c (n "symphonia-metadata") (v "0.5.3") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)))) (h "1h106cn1g5zq1sg2frb30n6s5yib5xmzcag8pdlf1l1igs9y3hw9") (r "1.53")))

(define-public crate-symphonia-metadata-0.5.4 (c (n "symphonia-metadata") (v "0.5.4") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)))) (h "0g02lhhyf6yyxm7bynx5b9fn2ha39y8fp6cfn72qj05186c2nqmw") (r "1.53")))

