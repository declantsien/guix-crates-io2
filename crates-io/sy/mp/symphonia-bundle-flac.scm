(define-module (crates-io sy mp symphonia-bundle-flac) #:use-module (crates-io))

(define-public crate-symphonia-bundle-flac-0.1.0 (c (n "symphonia-bundle-flac") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.1") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.1") (d #t) (k 0)))) (h "1hcxxgmjmpm98lphc3jqqnqpkrpapn7lqy66apymcs58sygb7a0d")))

(define-public crate-symphonia-bundle-flac-0.2.0 (c (n "symphonia-bundle-flac") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.2") (d #t) (k 0)))) (h "0armilvz2g0jnicb2dfi3ch704i4f9m3j5bq5ikyv0d3853xkkbz")))

(define-public crate-symphonia-bundle-flac-0.3.0 (c (n "symphonia-bundle-flac") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.3") (d #t) (k 0)))) (h "1bwpb4z6w02bdbqpk0ivx4fsap4qjd7ylfh7dr6gc7zzs11bwdxj")))

(define-public crate-symphonia-bundle-flac-0.4.0 (c (n "symphonia-bundle-flac") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.4") (d #t) (k 0)))) (h "00jxn9izfg1g07srhgglpqgadmzwsr88sqnnxw3mskpvyl958vhi")))

(define-public crate-symphonia-bundle-flac-0.5.0 (c (n "symphonia-bundle-flac") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "0r866h2ihpc0ljvkv31m83v6qz96289rhr7fy0nnkqjyha88ykzk") (r "1.53")))

(define-public crate-symphonia-bundle-flac-0.5.1 (c (n "symphonia-bundle-flac") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5") (d #t) (k 0)))) (h "0jlkswmcv2b92ph75768hqw4qh8rw5p37n7idmyj2aw96x9nakq4") (r "1.53")))

(define-public crate-symphonia-bundle-flac-0.5.2 (c (n "symphonia-bundle-flac") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.2") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.2") (d #t) (k 0)))) (h "1j7xgm0xpvm9yppb5s9ggas0r0lmiwvi7yb0p9qpi5h46bnxxhix") (r "1.53")))

(define-public crate-symphonia-bundle-flac-0.5.3 (c (n "symphonia-bundle-flac") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.3") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.3") (d #t) (k 0)))) (h "138574j4cg02a132jz8qh00fy07qinvv06lqz76qzcbw594b08vz") (r "1.53")))

(define-public crate-symphonia-bundle-flac-0.5.4 (c (n "symphonia-bundle-flac") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "symphonia-core") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-metadata") (r "^0.5.4") (d #t) (k 0)) (d (n "symphonia-utils-xiph") (r "^0.5.4") (d #t) (k 0)))) (h "15xxncx6gfh7jwvxvqqw4f8x9ic4bfzpyv3s77a0hwwa54s4zqvj") (r "1.53")))

