(define-module (crates-io sy st system_diskinfo) #:use-module (crates-io))

(define-public crate-system_diskinfo-0.1.0 (c (n "system_diskinfo") (v "0.1.0") (h "021zc2ikm0a1biwmih39xdd0mpd6l7295lkr61fix0dr67jqfaqd") (y #t)))

(define-public crate-system_diskinfo-0.1.1 (c (n "system_diskinfo") (v "0.1.1") (h "1l48dvxqfs5dacrniq55pn334adla0k9pqarlg9022j305k9fg3c") (y #t)))

(define-public crate-system_diskinfo-0.1.2 (c (n "system_diskinfo") (v "0.1.2") (h "18djgd710zxiq52jr0rb42w686xlx87f3c20vvpci8nsg3grdcqk")))

(define-public crate-system_diskinfo-0.1.3 (c (n "system_diskinfo") (v "0.1.3") (h "023b9mn66ma16ibd8yj96lhcz3m8mpk9pn44574pz27q9865zy55")))

(define-public crate-system_diskinfo-0.1.4 (c (n "system_diskinfo") (v "0.1.4") (h "05jd39xbfb3v3cm4bs0xndmxqxql5dk7az4md8mkaf3vhhafqw8n")))

(define-public crate-system_diskinfo-0.2.0 (c (n "system_diskinfo") (v "0.2.0") (h "1fxsgwcnym8asxs8yjr6d7avi2ak0jy1dgpqz1dhpr3jp721h9v9") (y #t)))

(define-public crate-system_diskinfo-0.2.1 (c (n "system_diskinfo") (v "0.2.1") (h "1bpmwrpypnpsd90ng8m2yidalwnkinmcgvlyblgkb7lcs2l3mwls")))

(define-public crate-system_diskinfo-0.2.2 (c (n "system_diskinfo") (v "0.2.2") (h "179ny35mk36kcysi73imh0iplk545xan4jjb4dr58krxhbgc3jng")))

