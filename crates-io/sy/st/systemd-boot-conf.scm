(define-module (crates-io sy st systemd-boot-conf) #:use-module (crates-io))

(define-public crate-systemd-boot-conf-0.1.0 (c (n "systemd-boot-conf") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.7.0") (d #t) (k 0)))) (h "0v9i7bfxakyny6gyxbyrp3dprjlilqwd87pnwqxakhpkjycpq9yi")))

(define-public crate-systemd-boot-conf-0.2.1 (c (n "systemd-boot-conf") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "146w5lck3a4rc9k52bcpfycpgfnb8kbi8972hplpa6mpwq284zwn")))

(define-public crate-systemd-boot-conf-0.2.2 (c (n "systemd-boot-conf") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c442479mm9sxw1ka7ljf4hs68ayy9qdp2jqsk6pmlv8k88s0ia5")))

