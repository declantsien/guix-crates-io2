(define-module (crates-io sy st system_error) #:use-module (crates-io))

(define-public crate-system_error-0.1.0 (c (n "system_error") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1wxwnkxy6hyfphp9hy56qr860qddq1fr2hxq6bxbi82i39cshkrw") (y #t)))

(define-public crate-system_error-0.1.1 (c (n "system_error") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1plbzwwx1psc6z7hk9nxn54g9ciz7b6z82vzvq4kwlzzgghnkrix")))

(define-public crate-system_error-0.2.0 (c (n "system_error") (v "0.2.0") (h "18rljw75jjp7cp4x6vbr3fdipd6yamisksv1wnl1m9nc1rwxlamm")))

