(define-module (crates-io sy st systemd-boot-gen) #:use-module (crates-io))

(define-public crate-systemd-boot-gen-1.0.0 (c (n "systemd-boot-gen") (v "1.0.0") (d (list (d (n "alphanumeric-sort") (r "^1.4.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pf0slyld5zcp41fq5y7cw1k9lqqlvq83qsghmvpdv19vrgbb562")))

(define-public crate-systemd-boot-gen-1.1.0 (c (n "systemd-boot-gen") (v "1.1.0") (d (list (d (n "alphanumeric-sort") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10680haf1rrgxm3wkb7njjmrxa33hs15kxf1m53qxk3jvqzvyhha")))

(define-public crate-systemd-boot-gen-1.1.1 (c (n "systemd-boot-gen") (v "1.1.1") (d (list (d (n "alphanumeric-sort") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0xpjs9m7vakvz7yzmwdj8gzwd0wbbhlfq0rwfj536pzbisw7vhb5")))

