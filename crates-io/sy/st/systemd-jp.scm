(define-module (crates-io sy st systemd-jp) #:use-module (crates-io))

(define-public crate-systemd-jp-0.1.0 (c (n "systemd-jp") (v "0.1.0") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "01jpymzr9blzwf63z1pxp7s6q2yixax56cqsjk1q3k1whava6wrc")))

(define-public crate-systemd-jp-0.1.1 (c (n "systemd-jp") (v "0.1.1") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "1hnsnvf115shky89x8s3j3cwi9q3qrprf7cjw4n9md8l6k679zzc")))

