(define-module (crates-io sy st system_alloc_stats) #:use-module (crates-io))

(define-public crate-system_alloc_stats-0.1.0 (c (n "system_alloc_stats") (v "0.1.0") (d (list (d (n "humansize") (r "^2") (o #t) (d #t) (k 0)) (d (n "num-format") (r "^0.4") (o #t) (d #t) (k 0)))) (h "127s6n43pmy9k0mf2dsvdhh2db855g1rryky0va0apf8nf7rhx98") (s 2) (e (quote (("fmt" "dep:humansize" "dep:num-format"))))))

