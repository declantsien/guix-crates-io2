(define-module (crates-io sy st system-service) #:use-module (crates-io))

(define-public crate-system-service-0.1.0 (c (n "system-service") (v "0.1.0") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j1yijngsb8dsxmifid8x30ljdi11qa8x36c0sdlcfd304y80qzr")))

(define-public crate-system-service-0.2.0 (c (n "system-service") (v "0.2.0") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1na422rpiyainvdbcbwqiljs2h16zkm91v520v7f4qw94kfyqqc4")))

(define-public crate-system-service-0.2.1 (c (n "system-service") (v "0.2.1") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00jmb2hh9czspc227f2h9zllr8a7v3cbf4jzfyamkxb9w0jb8a0j")))

(define-public crate-system-service-0.2.2 (c (n "system-service") (v "0.2.2") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11caj8lm1wpi029753y39sw07gn4jgi8ls67cc3j8rywz4mmsa5c")))

