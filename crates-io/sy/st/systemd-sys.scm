(define-module (crates-io sy st systemd-sys) #:use-module (crates-io))

(define-public crate-systemd-sys-0.1.0 (c (n "systemd-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0034zhddv894c6f4cib904hq415k7s61fj6hic5hlp6hrmqgf5j8")))

