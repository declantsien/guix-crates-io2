(define-module (crates-io sy st systemctl) #:use-module (crates-io))

(define-public crate-systemctl-0.0.1 (c (n "systemctl") (v "0.0.1") (h "19yn62dvaqj9is210fsdkr0dv7v5aq74ggcx0rfk3afn74nbgd37")))

(define-public crate-systemctl-0.1.0 (c (n "systemctl") (v "0.1.0") (h "1x1iq8572slcjfnx7lmxifx6rrrvcxpafxcs80vzl8qrq3rcs2wj")))

(define-public crate-systemctl-0.1.1 (c (n "systemctl") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "0d4zbaw5hgw4p7wp38h9kjp3rfba5lrkvxrs867jvrwlnysp7gjw")))

(define-public crate-systemctl-0.1.2 (c (n "systemctl") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1l26g8gyw4vpw5ss7b4rcgd1n664cpira8nvxmnh2ppmggd6pdp5")))

(define-public crate-systemctl-0.1.3 (c (n "systemctl") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "0xignbc56ryax4c6v1xrlrgba1jm4aczvvc77rzrcll5jxz85834")))

(define-public crate-systemctl-0.1.4 (c (n "systemctl") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "0b2p4764aiig5n7x1kwkcqvmzxx8vlw9gygf4srvmxpxnic4fay3")))

(define-public crate-systemctl-0.1.5 (c (n "systemctl") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1b3rx36xkb01lz51jsjmp6jrf3429bw6lgrjw5671km443san8fb")))

(define-public crate-systemctl-0.1.6 (c (n "systemctl") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1663hn0i0vyls69r21is7ls9cz6r1v38xig9ak8vpvpwcxxb3n0v")))

(define-public crate-systemctl-0.1.7 (c (n "systemctl") (v "0.1.7") (d (list (d (n "default-env") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1lc1qxffhswv13iy6sc3gy4xnwch5hfk6wadxpjvw04hnyx4q4yk")))

(define-public crate-systemctl-0.1.8 (c (n "systemctl") (v "0.1.8") (d (list (d (n "default-env") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1wzyf4p1xx5xgdg33d94zdqab1ll6m15g3c10jk14nmmrd3fp5l5")))

(define-public crate-systemctl-0.1.9 (c (n "systemctl") (v "0.1.9") (d (list (d (n "default-env") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "09mbyl6mpfbyq4vkw2qkfk18s0613cdyfr4m9i40cyi5xk6gxm5l")))

(define-public crate-systemctl-0.2.0 (c (n "systemctl") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "09jwnasn268lq2945j346hd6973119n6jhfpvsgfxl3dnrcr0fif")))

(define-public crate-systemctl-0.3.0 (c (n "systemctl") (v "0.3.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1kfbllr3chgwqy6yjb49zvg5j2vlfbm5g5pm5a7wsdblix7hl0nm") (f (quote (("default"))))))

(define-public crate-systemctl-0.3.1 (c (n "systemctl") (v "0.3.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1nifnpm9lgxngsab18syvpjn6p0b776pdxggc3hlndpf4jyal4yq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

