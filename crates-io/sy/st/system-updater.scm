(define-module (crates-io sy st system-updater) #:use-module (crates-io))

(define-public crate-system-updater-0.1.0 (c (n "system-updater") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0q7imw3izl7iydawpn8priy1rfn2ndhklwdrkdlrcnibfrg169n1")))

