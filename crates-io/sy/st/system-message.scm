(define-module (crates-io sy st system-message) #:use-module (crates-io))

(define-public crate-system-message-0.0.1 (c (n "system-message") (v "0.0.1") (d (list (d (n "windows") (r "^0.51") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1n5sra3c162fh4cgqmqsv6qgc43rdij16ghgh0vraacnwk5z4mvq")))

(define-public crate-system-message-0.0.2 (c (n "system-message") (v "0.0.2") (d (list (d (n "windows") (r "^0.51") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "02h400zqgldvp5l9gcm3dqva009y7wgv3m6nvpxcahsp9w4bn662")))

(define-public crate-system-message-0.0.3 (c (n "system-message") (v "0.0.3") (d (list (d (n "windows") (r "^0.51") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1yig2gsavvnm42x2pp4ayp3v4wxcn4fqk9hqb6v1gv2k8idhbss5")))

(define-public crate-system-message-0.0.4 (c (n "system-message") (v "0.0.4") (d (list (d (n "windows") (r "^0.51") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "046pr4szgl616i34zv9bsyh20y12blv2nwyfpxjq32w0dlmdbbrp")))

(define-public crate-system-message-0.0.5 (c (n "system-message") (v "0.0.5") (d (list (d (n "windows") (r "^0.51") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1h5cxj55h99gn3msgihd1cfdsmyg561jpnssh6rd3lmhzakx3vrb")))

