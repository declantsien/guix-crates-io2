(define-module (crates-io sy st systembus-notifier) #:use-module (crates-io))

(define-public crate-systembus-notifier-0.1.0 (c (n "systembus-notifier") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("user"))) (d #t) (k 0)))) (h "13p9m0r7fann4hrndvf980wkm1d8knxk5n6rkp6j82sgrap3wnys")))

(define-public crate-systembus-notifier-0.1.1 (c (n "systembus-notifier") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("user"))) (d #t) (k 0)))) (h "1ld7f6yxk3r3v1yxsn8z9q50n8ibi1a2qdaaz35gaf7wrzaz54yq")))

(define-public crate-systembus-notifier-0.1.2 (c (n "systembus-notifier") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (f (quote ("user"))) (d #t) (k 0)) (d (n "syslog") (r "^6.0.1") (d #t) (k 0)))) (h "1d27a8sp4j2ffvps3c46zz65642ax6snf1mdl17mkinngri9n1vs")))

