(define-module (crates-io sy st system_shutdown) #:use-module (crates-io))

(define-public crate-system_shutdown-1.0.0 (c (n "system_shutdown") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xjhs4r0gl50r6vk4fj4cj1klvh9ghb6grb1haflr2vdpa00hby1")))

(define-public crate-system_shutdown-1.0.1 (c (n "system_shutdown") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vbh0avv9ih763l7yz34nmsbk10dkm2fyznhyy005zvpyr9s2p4b")))

(define-public crate-system_shutdown-1.0.2 (c (n "system_shutdown") (v "1.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ifc883k8a3mny9rn50bzpzrd3p5iscqvpgkz20s2ilipdbfk1i7")))

(define-public crate-system_shutdown-2.0.0 (c (n "system_shutdown") (v "2.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15ymhx2srdqwfzpy8qjydy6f4niplcz3d81qrrmxz0n072w13pal")))

(define-public crate-system_shutdown-2.0.1 (c (n "system_shutdown") (v "2.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ikj8wc18pf1cngrfgkagq7ajjqfqac1ipv2mh953ggb3qqzwzqb")))

(define-public crate-system_shutdown-2.1.0 (c (n "system_shutdown") (v "2.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00dh5lcbp8c9zkcg9np0m1sy49kiqy86wczarnqpjzzfvc35v1fb")))

(define-public crate-system_shutdown-3.0.0 (c (n "system_shutdown") (v "3.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "processthreadsapi" "reason" "securitybaseapi" "winbase" "winerror" "winnt" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "053a80h9h100j4v8qr37x94wfs9jj4r24zdjipbxhl9mc0fhhph3")))

(define-public crate-system_shutdown-4.0.0 (c (n "system_shutdown") (v "4.0.0") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Com" "Win32_System_Threading" "Win32_System_Shutdown" "Win32_System_Power" "Win32_System_SystemServices" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "zbus") (r "^3.3.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1rrgbmbsdhrmd2p8ldd12j8plgb2cdij4vz56w7f6dilq2p5kgyp")))

(define-public crate-system_shutdown-4.0.1 (c (n "system_shutdown") (v "4.0.1") (d (list (d (n "zbus") (r "^3.7.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Com" "Win32_System_Threading" "Win32_System_Shutdown" "Win32_System_Power" "Win32_System_SystemServices" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0rncjnk8ami935j36p5cj7zcwx11rwr1b8pmnjzrlpmgc08zfrvm")))

