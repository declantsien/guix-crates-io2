(define-module (crates-io sy st system_dns) #:use-module (crates-io))

(define-public crate-system_dns-0.1.0 (c (n "system_dns") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00vq6ayaj51c9ab08har286vlzk62mdq6yvkfkn4p4dk2y2r5a0m")))

(define-public crate-system_dns-1.0.0 (c (n "system_dns") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kpai72gpzqyj7xf9sscjk4zcn4ngclakhs3dxrgv2b4a89cnavh")))

