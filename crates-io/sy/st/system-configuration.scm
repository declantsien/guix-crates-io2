(define-module (crates-io sy st system-configuration) #:use-module (crates-io))

(define-public crate-system-configuration-0.1.0 (c (n "system-configuration") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.1") (d #t) (k 0)))) (h "0n4av2azkypryhdlg122826fk6jhi75q8xmyvh5gmmdd8nc7hii6")))

(define-public crate-system-configuration-0.2.0 (c (n "system-configuration") (v "0.2.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.2") (d #t) (k 0)))) (h "176gi5h7v0gcgdndx2cy4c96axsgsfb200yslywaan98whiirscc")))

(define-public crate-system-configuration-0.3.0 (c (n "system-configuration") (v "0.3.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.3") (d #t) (k 0)))) (h "1rk2cjd0a1x5120zy109dp103v2pic29zlc381vdd7d807kw6gfz")))

(define-public crate-system-configuration-0.4.0 (c (n "system-configuration") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.7") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.4") (d #t) (k 0)))) (h "0s4l2bg1mbrn2mvqa8p3qznnay3h3gia7k25a4d0p31bg9iw0jyx")))

(define-public crate-system-configuration-0.5.0 (c (n "system-configuration") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.5") (d #t) (k 0)))) (h "1zf3704abgaknds62f6r8ghci9xx67p6a2smjqsra3j95zqq4lfp")))

(define-public crate-system-configuration-0.5.1 (c (n "system-configuration") (v "0.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.5") (d #t) (k 0)))) (h "1rz0r30xn7fiyqay2dvzfy56cvaa3km74hnbz2d72p97bkf3lfms")))

(define-public crate-system-configuration-0.6.0 (c (n "system-configuration") (v "0.6.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "system-configuration-sys") (r "^0.6") (d #t) (k 0)))) (h "0hjabiw8k1qwmj1zsf9dq39g85pcjnfq3c79fvszrd5923pcd2v5")))

