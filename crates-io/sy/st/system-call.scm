(define-module (crates-io sy st system-call) #:use-module (crates-io))

(define-public crate-system-call-0.0.0 (c (n "system-call") (v "0.0.0") (h "177z5jhz3s508yvm72nb3izwrriy23mh2cj0m9kskdn5b1hvysc6")))

(define-public crate-system-call-0.1.0 (c (n "system-call") (v "0.1.0") (h "1zrz5n2lpbhfbg3kw976j9gvz3qd6cpij5f2f3nj4q39q1aav8f4")))

(define-public crate-system-call-0.1.1 (c (n "system-call") (v "0.1.1") (h "1183q3fqwkazjlx9bmald2vzcyl2kzqgp4vfky7sprcfp0mpzsxd")))

(define-public crate-system-call-0.1.2 (c (n "system-call") (v "0.1.2") (h "130ycfbna5b8jnz72rqv20w6h3z8qq60ypzaxkqnmqvah5nbfijr")))

(define-public crate-system-call-0.1.3 (c (n "system-call") (v "0.1.3") (h "0y07shlidsljcc11al3w4wfmyy2cjqlwcnhi0lysa2i3dcykz6n0")))

