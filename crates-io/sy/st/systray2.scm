(define-module (crates-io sy st systray2) #:use-module (crates-io))

(define-public crate-systray2-0.5.0 (c (n "systray2") (v "0.5.0") (d (list (d (n "glib") (r "^0.10.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libappindicator") (r "^0.5.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.96") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1drawd76jqkz1dchpaq8a7y5anfjylivwbkdmc0fcc55j89gdjjx")))

