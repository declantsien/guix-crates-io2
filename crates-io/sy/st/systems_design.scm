(define-module (crates-io sy st systems_design) #:use-module (crates-io))

(define-public crate-systems_design-0.1.0 (c (n "systems_design") (v "0.1.0") (h "1h6nyxm44d8z6am1ayy4qbnd2854d2r5aimzsh0q68rqh8mp18fx")))

(define-public crate-systems_design-0.2.0 (c (n "systems_design") (v "0.2.0") (h "0gi7hdr5q7442xr1n9zz7979qy4gflddd97r0v5gq79hdvqrxkh5")))

