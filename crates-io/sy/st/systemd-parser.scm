(define-module (crates-io sy st systemd-parser) #:use-module (crates-io))

(define-public crate-systemd-parser-0.1.0 (c (n "systemd-parser") (v "0.1.0") (d (list (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "12wvqcxksl6p3jzm5if1mxmsncljss4ciw0m1j9zcism2c8897dd")))

(define-public crate-systemd-parser-0.1.2 (c (n "systemd-parser") (v "0.1.2") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)))) (h "15j9j3k5c3jpdrl2kharakic08736nqz0hpwllvn99zkkmiap942")))

(define-public crate-systemd-parser-0.1.3 (c (n "systemd-parser") (v "0.1.3") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)))) (h "0jv66irfdnfwm51rz8gkdyhp9bc4ixja7ici3mdm6sgn34nbk1qn")))

