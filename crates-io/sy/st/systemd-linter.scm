(define-module (crates-io sy st systemd-linter) #:use-module (crates-io))

(define-public crate-systemd-linter-0.1.0 (c (n "systemd-linter") (v "0.1.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "systemd-parser") (r "^0.1.0") (d #t) (k 0)))) (h "06q2z5z1gikryp4i8knm0ksv948a5yk6jnpk1kgavm9cigc6h9gi")))

(define-public crate-systemd-linter-0.1.2 (c (n "systemd-linter") (v "0.1.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "colored") (r "^1.0") (d #t) (k 0)) (d (n "systemd-parser") (r "^0.1") (d #t) (k 0)))) (h "0f31j7w55cyyqm4gz3r74j081l151r6hwdzq814wkgn1xj4ii2sd")))

(define-public crate-systemd-linter-0.1.3 (c (n "systemd-linter") (v "0.1.3") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "colored") (r "^1.0") (d #t) (k 0)) (d (n "systemd-parser") (r "^0.1") (d #t) (k 0)))) (h "0sglsjck5kw0n9wpyag7rwrmmjyxsjl2bnb4j547p2s98jarjdj2")))

