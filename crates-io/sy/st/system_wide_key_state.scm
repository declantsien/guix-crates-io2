(define-module (crates-io sy st system_wide_key_state) #:use-module (crates-io))

(define-public crate-system_wide_key_state-1.0.0 (c (n "system_wide_key_state") (v "1.0.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1qisdf2h1aq4pggyvdjav331qplk6v9ksw1hd4k8r4mim3nv9df7")))

(define-public crate-system_wide_key_state-1.0.1 (c (n "system_wide_key_state") (v "1.0.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0fjpgpmjbg81qshafdf9q3hc01ca107fxxaxnh6zlgqadifnaxp7")))

(define-public crate-system_wide_key_state-1.1.0 (c (n "system_wide_key_state") (v "1.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1ic4is7s1h1q9hmy41l7yggcp6vll4bgfjny22gnr6m7pi736698")))

(define-public crate-system_wide_key_state-1.1.1 (c (n "system_wide_key_state") (v "1.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "13ppv5dr7c364jn0j57h1m05rw3ybqahl3nw42q6i7cf29xa6zlk")))

(define-public crate-system_wide_key_state-1.1.2 (c (n "system_wide_key_state") (v "1.1.2") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "050g4m3ihsa0xpmnagscnkqd8bxvkmxj123i801a45whp155bwgj")))

(define-public crate-system_wide_key_state-1.1.3 (c (n "system_wide_key_state") (v "1.1.3") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0wx13lf6qalqnaw1mlcnr4i78pr2jjy78kb8vwz715h0672p4k6v")))

(define-public crate-system_wide_key_state-1.2.0 (c (n "system_wide_key_state") (v "1.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0mvw7g1djp3aam42mmqwcnzlgr90i8lb72xhbna9qgn3c5g4r9hd")))

