(define-module (crates-io sy st system-info) #:use-module (crates-io))

(define-public crate-system-info-0.1.0 (c (n "system-info") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "windows-sys") (r "^0.32.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0c97b9g1wfyp5i5z84688bbbfd4vnww8s7yk44sj8ry0m7v721ly") (f (quote (("std"))))))

(define-public crate-system-info-0.1.1 (c (n "system-info") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "windows-sys") (r "^0.32.0") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0m4hnqxys0gi105pim4lvx8w3hb9y8cx5j6gsb9s369jbqlbbvyb") (f (quote (("std"))))))

(define-public crate-system-info-0.1.2 (c (n "system-info") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "windows-sys") (r "^0.36.1") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1jm0fpbzq6qn83s8fwxna21jb0f5dy78xfhrnyy8wk7njc69wr66") (f (quote (("std"))))))

