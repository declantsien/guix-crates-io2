(define-module (crates-io sy st system-config) #:use-module (crates-io))

(define-public crate-system-config-0.1.0 (c (n "system-config") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "00zff66f3629qw0abpkm3y5v2dcacb5mgnwwbld1dxxbblb1rax7")))

(define-public crate-system-config-0.1.1 (c (n "system-config") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "12297yhxk6i5c0js8iv0sqnxxv815p7bxg840hwnzrs1abmmh3lr")))

(define-public crate-system-config-0.1.2 (c (n "system-config") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "01v6bwc82hdcbc9q6s8shrgzdg95an3w9k14j0i4xgc9vg2ildva")))

