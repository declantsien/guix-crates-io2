(define-module (crates-io sy st systemd-wake) #:use-module (crates-io))

(define-public crate-systemd-wake-0.1.0 (c (n "systemd-wake") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0knqpnknfmhp2mm7lkg1k2zcsm4wxn1r0wvsv5fpxbidlf13dyms")))

(define-public crate-systemd-wake-0.1.1 (c (n "systemd-wake") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fqgay8fyq4ms2gdjk0lbglmq4mfy8ywik2mn7vw1b2lkz012qkg")))

(define-public crate-systemd-wake-0.1.2 (c (n "systemd-wake") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ib0wcxzjdzm2n1ngn367pq931r98q7a62s2m1jwgb9wfyc7i6cy")))

