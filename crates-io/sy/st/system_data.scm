(define-module (crates-io sy st system_data) #:use-module (crates-io))

(define-public crate-system_data-0.1.0 (c (n "system_data") (v "0.1.0") (h "0fpbsdn430w0ii777v0fc2v7lc10427fwr92c8mb9gaxadwn8z56")))

(define-public crate-system_data-0.1.1 (c (n "system_data") (v "0.1.1") (h "1wbapsxv6wdsbxlhnlvfn3kr1xvdarmplr0j5jsks28wyjvf8vd8")))

