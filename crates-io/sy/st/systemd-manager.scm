(define-module (crates-io sy st systemd-manager) #:use-module (crates-io))

(define-public crate-systemd-manager-1.0.0 (c (n "systemd-manager") (v "1.0.0") (d (list (d (n "dbus") (r "^0.3.4") (d #t) (k 0)) (d (n "gdk") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "quickersort") (r "^2.0.1") (d #t) (k 0)))) (h "1dimygq6k3ggp15p9jqbsmhn1f2w73nwzk8wvkrycnmpzvhny77h")))

