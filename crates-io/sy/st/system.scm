(define-module (crates-io sy st system) #:use-module (crates-io))

(define-public crate-system-0.1.0 (c (n "system") (v "0.1.0") (h "1jncx0g6p23i2s15rp6q4d9v0ddb7bncr47g6nkva5wkpc9bbg7h")))

(define-public crate-system-0.2.0 (c (n "system") (v "0.2.0") (h "10m6sp2i4835705z1p3jhsv3xs9w5knc73vd85r0j1zgx6j6w7mq")))

(define-public crate-system-0.2.1 (c (n "system") (v "0.2.1") (h "1r1m9k4fd00z1f50d7509jyxp8x2ha2kd9gm8zldbf9idv7xh1k3")))

(define-public crate-system-0.3.0 (c (n "system") (v "0.3.0") (h "0npwwgwl832bzfn4457k9bp6gp7g7izn2y3n2dfimb3wpvh9qdji")))

(define-public crate-system-0.3.1 (c (n "system") (v "0.3.1") (h "1lmgcqbmgbl7bz6vmh03d7pxwv8vh2a5m5b7r9lmbjv9m5aig5f5")))

(define-public crate-system-0.3.2 (c (n "system") (v "0.3.2") (h "1xpdzrzinyijm3c8mml894db3m02i8ihlfg76kyxg1nyh0ymm6v8")))

(define-public crate-system-0.3.3 (c (n "system") (v "0.3.3") (h "0fn0rxcc55l9dgchvdhr699krppv5crlmxpmkj9hf5jgk2pi005z")))

