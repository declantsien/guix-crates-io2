(define-module (crates-io sy st systeroid) #:use-module (crates-io))

(define-public crate-systeroid-0.1.0-rc.1 (c (n "systeroid") (v "0.1.0-rc.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.1.0-rc.1") (d #t) (k 0)))) (h "0pqgwh501z6gm7by7991fz6cd166vqabgm58z6rrrh2mlqwx9pfi") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.1.0 (c (n "systeroid") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.1.0") (d #t) (k 0)))) (h "1lafn2j3vfyvcjjrwzzy3nfs996yhrfaha0zkmkd8s65z27yr3d5") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.1.1 (c (n "systeroid") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.1.1") (d #t) (k 0)))) (h "07aaf5ilg6j46nq5n2f2ia5isxh8q7yn0x708zvpm0x46pwj26f8") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.2.0 (c (n "systeroid") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.2.0") (d #t) (k 0)))) (h "0vsh8sfpyni61dx7qbr617mrwz82qpa0h6yxlqr8haq2rp1ab7f8") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.2.1 (c (n "systeroid") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.2.1") (d #t) (k 0)))) (h "04ds2j2ax2wdlwwfa92lwmpvwg66l5z4s2didifbp1ysh17zgks0") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.2.2 (c (n "systeroid") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.2.2") (d #t) (k 0)))) (h "0a571anyj0ir1hg4iz263nqjh4v98l4ls50ciqz7v7p53f1s3plc") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.3.0 (c (n "systeroid") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.3.0") (d #t) (k 0)))) (h "16lnv04g4psgkvhd7v1mb44zqyavf46da2pj03siqv3gr405ha51") (f (quote (("live-tests")))) (r "1.56.1")))

(define-public crate-systeroid-0.3.1 (c (n "systeroid") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "systeroid-core") (r "^0.3.1") (d #t) (k 0)))) (h "1y7vjk8vi6vnfia51gdwj7higmf0vn1habgg8q786k3r4glb6591") (f (quote (("live-tests")))) (r "1.63.0")))

(define-public crate-systeroid-0.3.2 (c (n "systeroid") (v "0.3.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.3.2") (d #t) (k 0)))) (h "1mpz6bx73hsxs8gn4khjc73bb79nq018ghp531xdnw1nay6fp2yi") (f (quote (("live-tests")))) (r "1.64.0")))

(define-public crate-systeroid-0.4.0 (c (n "systeroid") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.4.0") (d #t) (k 0)))) (h "1m6k9kn427jfzg5nv9i3iih6iwk2cx2gzs6ymjwh10hdmg25p530") (f (quote (("live-tests")))) (r "1.64.0")))

(define-public crate-systeroid-0.4.1 (c (n "systeroid") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.4.1") (d #t) (k 0)))) (h "0lkzyfvavnjjjijrlz64lj4kchzx8xmdfyd6s7j9whwlifdh33lv") (f (quote (("live-tests")))) (r "1.64.0")))

(define-public crate-systeroid-0.4.2 (c (n "systeroid") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("std"))) (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.4.2") (d #t) (k 0)))) (h "1r8mx63jim0yg4kn1xx4l42f44b8iavv982vgvif3dk5crdq0g8c") (f (quote (("live-tests")))) (r "1.64.0")))

(define-public crate-systeroid-0.4.3 (c (n "systeroid") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.4.3") (d #t) (k 0)))) (h "0k4l20g8s373ii9yd8x61jd57zgw4jfvxvpbvxfjdz1k5kiiw7l9") (f (quote (("live-tests")))) (r "1.64.0")))

(define-public crate-systeroid-0.4.4 (c (n "systeroid") (v "0.4.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "parseit") (r "^0.1.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "systeroid-core") (r "^0.4.4") (d #t) (k 0)))) (h "0hyzcnpprzwjzkib31nrgbdakn0wrj206wk1lgivcwc6fxxjj3gf") (f (quote (("live-tests")))) (r "1.64.0")))

