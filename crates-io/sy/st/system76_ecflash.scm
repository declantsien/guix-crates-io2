(define-module (crates-io sy st system76_ecflash) #:use-module (crates-io))

(define-public crate-system76_ecflash-0.1.0 (c (n "system76_ecflash") (v "0.1.0") (h "0iy5v85ghy07rkw7zn4f31il7s9vrs69f84l092fjv42rliipa6j")))

(define-public crate-system76_ecflash-0.1.1 (c (n "system76_ecflash") (v "0.1.1") (h "11i5kh84bipk6mkrsms9rs2gq5ma11x1gdfb35v5qvqljgyxpnlz")))

(define-public crate-system76_ecflash-0.1.2 (c (n "system76_ecflash") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "redox_hwio") (r "^0.1.3") (d #t) (k 2)) (d (n "serialport") (r "^3.3") (d #t) (k 2)))) (h "1z59qc9k532n8w114s9han96x1616kw645dj17grv3pmzgmx5shq")))

(define-public crate-system76_ecflash-0.1.3 (c (n "system76_ecflash") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 2)) (d (n "redox_hwio") (r "^0.1.5") (d #t) (k 2)) (d (n "serialport") (r "^4.1.0") (d #t) (k 2)))) (h "09gcc2wf60m7yr50jqs48wm49rzlcgld67fg7rx6xh61b6s2hi1v")))

