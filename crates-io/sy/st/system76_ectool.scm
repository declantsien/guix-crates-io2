(define-module (crates-io sy st system76_ectool) #:use-module (crates-io))

(define-public crate-system76_ectool-0.1.0 (c (n "system76_ectool") (v "0.1.0") (d (list (d (n "redox_hwio") (r "^0.1") (d #t) (k 0)))) (h "1m3h9xrcn7yd1nz3a5qjjdigzsp32pcgmynczfvy344hz63c6ss2")))

(define-public crate-system76_ectool-0.1.1 (c (n "system76_ectool") (v "0.1.1") (d (list (d (n "redox_hwio") (r "^0.1") (d #t) (k 0)))) (h "0av1x4gkz0647ci1y2ihva6byb2yjajg700jla3ii1jly301iw29")))

(define-public crate-system76_ectool-0.1.2 (c (n "system76_ectool") (v "0.1.2") (d (list (d (n "redox_hwio") (r "^0.1.1") (d #t) (k 0)))) (h "1gy9r6gi7wzjh9wa6vnycd2d1zyi9dxfiwswkmdiarwbjy0fr5z4") (f (quote (("stable" "redox_hwio/stable") ("default"))))))

(define-public crate-system76_ectool-0.1.3 (c (n "system76_ectool") (v "0.1.3") (d (list (d (n "redox_hwio") (r "^0.1.1") (d #t) (k 0)))) (h "1ivjhxnzxn3wpdwlgdgrclb5sj2511yppj02v6cdvsd7s0fpb7ad") (f (quote (("stable" "redox_hwio/stable") ("default"))))))

(define-public crate-system76_ectool-0.2.0 (c (n "system76_ectool") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (d #t) (k 0)))) (h "12kqc5glda6mzdry8wvnygy0fmrlacxqvxkz58cq8rd5kmq4k8kg") (f (quote (("std" "libc") ("stable" "redox_hwio/stable") ("default" "std"))))))

(define-public crate-system76_ectool-0.2.1 (c (n "system76_ectool") (v "0.2.1") (d (list (d (n "hidapi") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "15blfv545xb1pf5jchjag3iv4ykldrcjhg06xq2ac0hhs70ndh3b") (f (quote (("std" "libc") ("default" "std"))))))

(define-public crate-system76_ectool-0.2.2 (c (n "system76_ectool") (v "0.2.2") (d (list (d (n "hidapi") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0crj8lizpils5lgvpzm57789bhv1gxwx1gqxplvfb2i6lml37995") (f (quote (("std" "libc") ("default" "std"))))))

(define-public crate-system76_ectool-0.2.3 (c (n "system76_ectool") (v "0.2.3") (d (list (d (n "hidapi") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "02k1s15hdzvc48n98dy2m5h6ca81mn7z87r6plfh65z0579zsmy9") (f (quote (("std" "libc") ("default" "std"))))))

(define-public crate-system76_ectool-0.3.0 (c (n "system76_ectool") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "03d5y6l61dg4k4jlah1pkwv7jbd31mil1xza6mlxyxchsdadk08v") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi"))))))

(define-public crate-system76_ectool-0.3.1 (c (n "system76_ectool") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1a0k9lqijr97c4ksczj50r89xr655n3r344lw5advd1kawvisp00") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi"))))))

(define-public crate-system76_ectool-0.3.2 (c (n "system76_ectool") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1cxm2xzmzpz8kdcp6gmi51r912ny1sz1vryncwmws3bl3yiy89z4") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi"))))))

(define-public crate-system76_ectool-0.3.3 (c (n "system76_ectool") (v "0.3.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0zlr8y6k1dj2wmi6whaq0s2chai3b0ddf9cdm2zqzkr65s6isid3") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi"))))))

(define-public crate-system76_ectool-0.3.4 (c (n "system76_ectool") (v "0.3.4") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1x1ic89xh62f3jwb2japd9xhkj027wqm3lc16c91ryvsplylsrvy") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

(define-public crate-system76_ectool-0.3.5 (c (n "system76_ectool") (v "0.3.5") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "11fawi71k644k75nsrssz95c4njf5gh0q5aqrbbb56xc586j9vrv") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

(define-public crate-system76_ectool-0.3.6 (c (n "system76_ectool") (v "0.3.6") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-static-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0y3qza3rl4bkzvzkdfmc7rrw2bxqr71k3j0dsmq3zx8wylc2qvf7") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

(define-public crate-system76_ectool-0.3.7 (c (n "system76_ectool") (v "0.3.7") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-shared-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "183l076pg3z5wfc3y33frrx2q5f3fcnx9g364ykzs5bw9ildvn7i") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

(define-public crate-system76_ectool-0.3.8 (c (n "system76_ectool") (v "0.3.8") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.2") (f (quote ("linux-shared-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1ghycrl1yj9bpd3w2qgi992vsdszvx0f0s22f16g0qhxwvwgi8az") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

