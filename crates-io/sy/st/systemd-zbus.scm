(define-module (crates-io sy st systemd-zbus) #:use-module (crates-io))

(define-public crate-systemd-zbus-0.1.0 (c (n "systemd-zbus") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6") (d #t) (k 0)))) (h "05y4l2wh4znccvzz7wihq2swgbb4rzxarwblmy7284qv8ig6gfmb")))

(define-public crate-systemd-zbus-0.1.1 (c (n "systemd-zbus") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~3.14.0") (d #t) (k 0)))) (h "1qx7kwm0262kpbwbvmmix1zk61j1zhg72cj5x6ki8syrlrp1pfzz")))

(define-public crate-systemd-zbus-0.2.0 (c (n "systemd-zbus") (v "0.2.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~4.0.1") (d #t) (k 0)))) (h "0lcdhs2nvxssls0qscdqz60s21mf8j9l8dqwdrrwq517x012yh57")))

(define-public crate-systemd-zbus-0.3.0 (c (n "systemd-zbus") (v "0.3.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "zbus") (r "^4.1.2") (d #t) (k 0)))) (h "1a3k7ri92p32i9403lp5phi8nijf98xs11dgb70d7cngd2450ccc")))

(define-public crate-systemd-zbus-0.3.1 (c (n "systemd-zbus") (v "0.3.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "zbus") (r "^4.1.2") (d #t) (k 0)))) (h "0bibzm4r6k96ax5y0m13gx7cd07wzd11jmbn2nq952mhqz4w12m9")))

(define-public crate-systemd-zbus-0.3.2 (c (n "systemd-zbus") (v "0.3.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "zbus") (r "^4.2.0") (d #t) (k 0)))) (h "0xi999la7l6m52gj3zh7h047fkgwkm86agqgx31120x9lsk72zg1")))

