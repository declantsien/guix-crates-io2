(define-module (crates-io sy st systrayx) #:use-module (crates-io))

(define-public crate-systrayx-0.4.1 (c (n "systrayx") (v "0.4.1") (d (list (d (n "glib") (r "^0.14.8") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.14.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libappindicator") (r "^0.6.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("shellapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0hygdmzjij222cbq9mimrh3cgwxk0x66izk7cdchnh2nqvfvcnkx")))

