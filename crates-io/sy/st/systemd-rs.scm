(define-module (crates-io sy st systemd-rs) #:use-module (crates-io))

(define-public crate-systemd-rs-0.1.0 (c (n "systemd-rs") (v "0.1.0") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsystemd-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "144k00n686kzyacf0pj4c1p4mb4j1j2j3244bz13mgycgm2nwbj6")))

(define-public crate-systemd-rs-0.1.1 (c (n "systemd-rs") (v "0.1.1") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "0w8k16fgi6y47c117sa2nm8qb2hhk9aaxgm5v1669d5fgimp4z92")))

(define-public crate-systemd-rs-0.1.2 (c (n "systemd-rs") (v "0.1.2") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "1kbsayk1vxypq85isaylkj816hhs9p161r0ys21hl2z7gdk82cv0")))

(define-public crate-systemd-rs-0.1.3 (c (n "systemd-rs") (v "0.1.3") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "1dk5qf49fb384ald0xjh62j2pfw3zf2bxllhzspip3z15pxps7ki")))

(define-public crate-systemd-rs-0.1.4 (c (n "systemd-rs") (v "0.1.4") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "0rlh10spk34skc0irj5qsa0v5f4c9i6wwivqmv037larx1kjigkd")))

(define-public crate-systemd-rs-0.1.5 (c (n "systemd-rs") (v "0.1.5") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "1wl9ns6fh25n32c6hqqr7r7gcav835advpygknccw19sk11v5x9h")))

(define-public crate-systemd-rs-0.1.6 (c (n "systemd-rs") (v "0.1.6") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "1s8fxyn85k07k92scp9rk7rmjzanvdbbwlkk8az37fsgz5qzs3nb")))

(define-public crate-systemd-rs-0.1.7 (c (n "systemd-rs") (v "0.1.7") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "systemd-sys") (r "^0.1") (d #t) (k 0)))) (h "0nbvl7a624mxsm0zxbf1g0rzy7fzz5nal183hawziyli2kncsa5s")))

