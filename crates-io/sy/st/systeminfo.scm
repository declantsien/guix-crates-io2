(define-module (crates-io sy st systeminfo) #:use-module (crates-io))

(define-public crate-systeminfo-0.1.1 (c (n "systeminfo") (v "0.1.1") (d (list (d (n "humanize") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "winerror" "winreg" "ntstatus" "libloaderapi" "winuser" "processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.8.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1142q5a7lypgiam4j7pfrxqiwvg49hb52cwbh4s4802baklw9d7z") (y #t)))

