(define-module (crates-io sy st system_token_generator) #:use-module (crates-io))

(define-public crate-system_token_generator-0.1.0 (c (n "system_token_generator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n6av1c7cjx3gb6fxfcz6l8w6xy36a3aprj07daxswja4r8fnpmm")))

