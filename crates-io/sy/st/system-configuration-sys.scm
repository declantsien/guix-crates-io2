(define-module (crates-io sy st system-configuration-sys) #:use-module (crates-io))

(define-public crate-system-configuration-sys-0.1.0 (c (n "system-configuration-sys") (v "0.1.0") (d (list (d (n "core-foundation-sys") (r "^0.5") (d #t) (k 0)))) (h "1zscsxs5rrda0g7vkdh9mp854gqvsi6z88rydss8aidvz0zld2vx")))

(define-public crate-system-configuration-sys-0.2.0 (c (n "system-configuration-sys") (v "0.2.0") (d (list (d (n "core-foundation-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01phr9fyd4w1cpaz864z9pfpws7s36indqkagcvwzdcncysh4iqi")))

(define-public crate-system-configuration-sys-0.3.0 (c (n "system-configuration-sys") (v "0.3.0") (d (list (d (n "core-foundation-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16ddk13k77v091zd78y0y4h6ih1ai41vy5zmzm5hhm1di2i0dndz")))

(define-public crate-system-configuration-sys-0.4.0 (c (n "system-configuration-sys") (v "0.4.0") (d (list (d (n "core-foundation-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10ciqf1r51z2dhrl67hgzaj0vp0j3kd53jcxhy32qk60gzbzw3rm") (y #t)))

(define-public crate-system-configuration-sys-0.4.1 (c (n "system-configuration-sys") (v "0.4.1") (d (list (d (n "core-foundation-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)))) (h "02hqyrr3m3r8h0pzg6mifhph2p9xw0gs2df544kbpr6q6qa2g7i6")))

(define-public crate-system-configuration-sys-0.5.0 (c (n "system-configuration-sys") (v "0.5.0") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.49") (d #t) (k 0)))) (h "1jckxvdr37bay3i9v52izgy52dg690x5xfg3hd394sv2xf4b2px7")))

(define-public crate-system-configuration-sys-0.6.0 (c (n "system-configuration-sys") (v "0.6.0") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1i5sqrmgy58l4704hibjbl36hclddglh73fb3wx95jnmrq81n7cf")))

