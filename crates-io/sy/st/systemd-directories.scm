(define-module (crates-io sy st systemd-directories) #:use-module (crates-io))

(define-public crate-systemd-directories-0.1.0 (c (n "systemd-directories") (v "0.1.0") (h "0br29g3nd330qifklaxxkw0mimfschfmhnf8dj340gqnmlla6532")))

(define-public crate-systemd-directories-0.1.1 (c (n "systemd-directories") (v "0.1.1") (h "157kr42w3lqzq27302vjipmi335g91myv7lzakb3i5w350p9winm")))

