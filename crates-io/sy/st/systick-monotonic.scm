(define-module (crates-io sy st systick-monotonic) #:use-module (crates-io))

(define-public crate-systick-monotonic-0.1.0-alpha.0 (c (n "systick-monotonic") (v "0.1.0-alpha.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0l5flibc2852vi9d8xg7pav8n28izq3fqf39sxqcdpy4kxz7mq4j")))

(define-public crate-systick-monotonic-0.1.0-rc.1 (c (n "systick-monotonic") (v "0.1.0-rc.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-rc.1") (d #t) (k 0)))) (h "1hzhi981a69jy672vzbza72p7isbddgvrbn1imx1fgnn9xq3lkfg")))

(define-public crate-systick-monotonic-0.1.0-rc.2 (c (n "systick-monotonic") (v "0.1.0-rc.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-rc.2") (d #t) (k 0)))) (h "1r5cg1r1f54yca2lzyi690v4cm442pw6jh9qfbpmc7dhahg05kir")))

(define-public crate-systick-monotonic-1.0.0 (c (n "systick-monotonic") (v "1.0.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "1w37pzg5hfnzdcfklfl1vxj63x47368fz6lbp0sg9n478n71vrsp")))

(define-public crate-systick-monotonic-1.0.1 (c (n "systick-monotonic") (v "1.0.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "117zsn46m3pslk063n48ykxcg0b3s2qyapkrlkihlnk1bhnq5yv7")))

