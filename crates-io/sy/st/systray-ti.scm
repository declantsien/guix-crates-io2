(define-module (crates-io sy st systray-ti) #:use-module (crates-io))

(define-public crate-systray-ti-0.4.1 (c (n "systray-ti") (v "0.4.1") (d (list (d (n "glib") (r "^0.10") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libappindicator") (r "^0.5.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("shellapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1icvsgxwzv2i7m88shjsdxd5h60vmkyy9956gir7x2xacprqsgb5")))

