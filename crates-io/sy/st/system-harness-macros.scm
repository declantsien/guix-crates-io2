(define-module (crates-io sy st system-harness-macros) #:use-module (crates-io))

(define-public crate-system-harness-macros-0.2.0 (c (n "system-harness-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "13daakdgwzn3j72pswiwba72pilf4wkhljxk9p3q9af7xh09wxq6")))

(define-public crate-system-harness-macros-0.4.0 (c (n "system-harness-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1n5qhzkjbdsaznsfivi8cyws6bq1lknj7c4lpjg2cpv0jcm525nd")))

(define-public crate-system-harness-macros-0.5.0 (c (n "system-harness-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "12h4y6wrc4pmays289pqd3k9046f2nfzrw1sgcfljsi1hlzhch6r")))

(define-public crate-system-harness-macros-0.6.0 (c (n "system-harness-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0f85mfn82p13iksglkhl78a35wicqq4k8ppsmchg6q5mmvmfza50")))

