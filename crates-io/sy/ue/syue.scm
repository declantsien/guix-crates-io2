(define-module (crates-io sy ue syue) #:use-module (crates-io))

(define-public crate-syue-0.1.0 (c (n "syue") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1bg3qsairg4ls96il3dfm0f6fvac9aaklgbi5gibvk34affswf6g") (y #t)))

(define-public crate-syue-0.1.1 (c (n "syue") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0jfv2h6zwxmhy0411zz6zw4msknnaizz682xxjdgy2mv78s16idi") (y #t)))

(define-public crate-syue-0.1.2 (c (n "syue") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "180i6fyqj5z4hnip2qvkfsdh1pfwxa05gc4n2dg6xkfl3zcgib1h")))

