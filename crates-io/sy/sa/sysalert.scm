(define-module (crates-io sy sa sysalert) #:use-module (crates-io))

(define-public crate-sysalert-0.1.0 (c (n "sysalert") (v "0.1.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01br7vknjwnsgjzgcdi328mcx3158rk0wln02adc3m38b03ipdr2")))

(define-public crate-sysalert-0.1.1 (c (n "sysalert") (v "0.1.1") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "070yqq1wj3srdzqzf7n76mzblw3jhff8hl1pfznjlj8b9fh1my43")))

