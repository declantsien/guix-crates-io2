(define-module (crates-io sy nn synner) #:use-module (crates-io))

(define-public crate-synner-0.1.0 (c (n "synner") (v "0.1.0") (d (list (d (n "pnet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1idxy6vjjjdxck0z2r8mslr53vf5w8ipli9qi0yjnna5v0z868zc")))

(define-public crate-synner-0.1.1 (c (n "synner") (v "0.1.1") (d (list (d (n "pnet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_base") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1v0hdwfc1zivppxmap2dhfa1pw91d27vpls1dqnbmp0ljjc7w9a6")))

