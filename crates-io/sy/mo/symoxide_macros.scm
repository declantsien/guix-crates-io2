(define-module (crates-io sy mo symoxide_macros) #:use-module (crates-io))

(define-public crate-symoxide_macros-0.1.0 (c (n "symoxide_macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "13ir0l7kqaa9r67g2whmya0jfm84fas4chizr9sjyqmvp1w7aycw")))

