(define-module (crates-io sy na synac) #:use-module (crates-io))

(define-public crate-synac-0.1.0 (c (n "synac") (v "0.1.0") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w4hrhc38nf5ryx7jh9dcfipb5b033gkx7dlc5s0ynfmbszkl5hy")))

(define-public crate-synac-0.1.1 (c (n "synac") (v "0.1.1") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1law0pp6hd6f4i67wsk4x4f66wyz6cjqsbrlkhm4qm1qswnq7qds")))

(define-public crate-synac-0.1.2 (c (n "synac") (v "0.1.2") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lhla63v4j4zs7rfvgf7di46l8dww785mmfilfjbwwiwm98ck9rn")))

(define-public crate-synac-0.1.3 (c (n "synac") (v "0.1.3") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lj8ldljcvdw23j6xd8mq44x14xy6a2l2z2frlagycs9qz5kyjn8")))

(define-public crate-synac-0.1.4 (c (n "synac") (v "0.1.4") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19wyksf574xi6jdxyc0amk1mxzcsgvdwjdqbyl160gpv9ha8bpaj")))

(define-public crate-synac-0.2.0 (c (n "synac") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kbzciv486zknk2b2nz7yk0m6jfp4z3cifjyx518g1y0np46745n")))

(define-public crate-synac-0.2.1 (c (n "synac") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sjqv6zfb06zl3p8liam6il87gdm7dpqai3in4np6r96ifr4cswl")))

(define-public crate-synac-0.3.0 (c (n "synac") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "0xwvmkx0jq7bh6kqjjjqih6k16l32a173fqsyjrc3c1vv1kb9jg1")))

(define-public crate-synac-0.3.1 (c (n "synac") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "0i16rai4ca83zdc7xpgkfz58lqv5mfakl4jz3l2bjjckbj3k79fh")))

(define-public crate-synac-0.4.0 (c (n "synac") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "1fb4vkckj1ffkgy79axslgcf64s1ki77p4rx15606vxw75m374vz")))

(define-public crate-synac-0.5.0 (c (n "synac") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "1dzshwnv1nn7cz63v9v32jqyz4hs33l4prkk8y8h43sm4a188kyq")))

(define-public crate-synac-0.5.1 (c (n "synac") (v "0.5.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "0kq82wn8jc929r2a5pc5ba4xjscjrxns74xr076s8k3k2i4kfjjw")))

(define-public crate-synac-0.5.2 (c (n "synac") (v "0.5.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "1q8iva8jj0b614lcj8il4hibbg959d84g4v5k9x171fjnaysm263")))

(define-public crate-synac-0.5.3 (c (n "synac") (v "0.5.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "14armpxpb5l9jhyzf00msifh8r461xjfw0ny87sp5ysz41zsd4vn")))

(define-public crate-synac-0.5.4 (c (n "synac") (v "0.5.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "1wby7m7zxaj1k2p5alhjlav48qszpirrhvsz31aihkx3zgfhsdn7")))

(define-public crate-synac-0.5.5 (c (n "synac") (v "0.5.5") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.23") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "1pb6dn2f7pnym4mlzl579p49f9li5jpl9iy30lcvwaccdsik2qz3")))

(define-public crate-synac-0.6.0 (c (n "synac") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1jfyfd55wbss92ljf2gbpada6bqq8rl1li8msbid37z7hn1xdkqh") (f (quote (("tokio" "futures" "tokio-core" "tokio-io" "tokio-openssl"))))))

(define-public crate-synac-0.6.1 (c (n "synac") (v "0.6.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1f7ahb1r66p7k8nsdkdq5sb14jjmsqqavw8ycf26vrdl7p5j346h") (f (quote (("tokio" "futures" "tokio-core" "tokio-io" "tokio-openssl"))))))

(define-public crate-synac-0.6.2 (c (n "synac") (v "0.6.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1nw3h1rc1qrxni0nzzwxf3mapzzfmh9g9rarpfxr3ydi4yhxpbby") (f (quote (("tokio" "futures" "tokio-core" "tokio-io" "tokio-openssl"))))))

