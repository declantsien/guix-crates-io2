(define-module (crates-io sy na synap) #:use-module (crates-io))

(define-public crate-synap-0.1.0 (c (n "synap") (v "0.1.0") (h "1zf82zffg7vahrwx167inxxz1fy5rml7vnz51s27nyxifb0cjdvh")))

(define-public crate-synap-0.1.1 (c (n "synap") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1graivkvxrkr6rnvx3hx0bm81akvzfp7d3idh43v3m4bakw892az")))

