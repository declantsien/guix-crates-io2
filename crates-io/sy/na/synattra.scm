(define-module (crates-io sy na synattra) #:use-module (crates-io))

(define-public crate-synattra-0.1.0 (c (n "synattra") (v "0.1.0") (d (list (d (n "auto_enums") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0cmksgh9nlriv2vplwrc84d6r2mimay8xarlpfwcvs8p2px1x1")))

(define-public crate-synattra-0.1.1 (c (n "synattra") (v "0.1.1") (d (list (d (n "auto_enums") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1wxl23hhrk4dpa3ic1dssb1n331f16yp537116rrm3c0qfwcjxib")))

(define-public crate-synattra-0.2.0 (c (n "synattra") (v "0.2.0") (d (list (d (n "auto_enums") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xr79g7a6zmnc1iiwxfi0s7a5z2r7z5cf28a3ii78h1lvrc36y2y")))

(define-public crate-synattra-0.2.1 (c (n "synattra") (v "0.2.1") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qxyj339d9az5ps7fgxc9cafczaf8qdfhw5vd1ka5xcybxlxb31p")))

