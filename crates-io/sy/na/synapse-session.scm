(define-module (crates-io sy na synapse-session) #:use-module (crates-io))

(define-public crate-synapse-session-0.1.0 (c (n "synapse-session") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0q4f2mg7x1mhm5dydw0vf1gqqfvwz95pj1lk83ppg0nfydhjfd1p")))

