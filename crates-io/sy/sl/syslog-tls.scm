(define-module (crates-io sy sl syslog-tls) #:use-module (crates-io))

(define-public crate-syslog-tls-7.0.0 (c (n "syslog-tls") (v "7.0.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "0galpywsha7742lyx96msnsis3x6dn1azvh3sl4m97ik2qw3bn0n")))

(define-public crate-syslog-tls-7.0.1 (c (n "syslog-tls") (v "7.0.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "0jiqv8ijixq9145bc2bl59zcmhppzmxp4vi6b0vkwh7hg0n5jivc")))

(define-public crate-syslog-tls-7.1.0 (c (n "syslog-tls") (v "7.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "0jsm3g4bwszqjdj6ydk08nhs7gd2m7nq182kd2mm6s2xbnnr3qfm")))

(define-public crate-syslog-tls-7.1.1 (c (n "syslog-tls") (v "7.1.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)))) (h "16in8938l421g6xf4rgdzvr4v0bf80wvxp105h8nnm722f553n3d")))

