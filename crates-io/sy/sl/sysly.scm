(define-module (crates-io sy sl sysly) #:use-module (crates-io))

(define-public crate-sysly-0.0.1 (c (n "sysly") (v "0.0.1") (d (list (d (n "time") (r "^0.1.17") (d #t) (k 0)))) (h "0vbyhbpf6j8rvcfrqbnylj2l5hbaw2jajg39yamxjwks48sp08l6") (y #t)))

(define-public crate-sysly-0.0.2 (c (n "sysly") (v "0.0.2") (d (list (d (n "time") (r "^0.1.17") (d #t) (k 0)))) (h "19i08ymkap8psdsl1j40sj3cwbd31nb912cfsbm6lzmbkvcvynzv")))

(define-public crate-sysly-0.1.0 (c (n "sysly") (v "0.1.0") (d (list (d (n "time") (r "^0.1.19") (d #t) (k 0)) (d (n "unix_socket") (r "^0.1.0") (d #t) (k 0)))) (h "03qq9ml9k0il9alsi4l2cvr3mblbhqm5g9pbvlwg1i05h05sdh2a")))

(define-public crate-sysly-0.2.0 (c (n "sysly") (v "0.2.0") (d (list (d (n "time") (r "^0.1.20") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.1") (d #t) (k 0)))) (h "0xhl2j3dkr3bjzl05lbh1nk21g01avabq1hlximazg9hzjgpslix")))

(define-public crate-sysly-0.2.1 (c (n "sysly") (v "0.2.1") (d (list (d (n "time") (r "^0.1.21") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2.2") (d #t) (k 0)))) (h "15w8ncwd730xpgan3vn3y8nvnwaa2dsa13rn32dkdyxq3n4gvwsy")))

(define-public crate-sysly-0.2.2 (c (n "sysly") (v "0.2.2") (d (list (d (n "time") (r "^0.1.24") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1f61xvzyl05djh7w7fqiccrnma2l0wczk83z5mymza83pz6hcvb0")))

(define-public crate-sysly-0.2.3 (c (n "sysly") (v "0.2.3") (d (list (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.2") (d #t) (k 0)))) (h "12kq2i4mapfrw5fdicf4fwlcg77s898dmcd2l6dbvpswb8a9l1di")))

(define-public crate-sysly-0.2.4 (c (n "sysly") (v "0.2.4") (d (list (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.0") (d #t) (k 0)))) (h "1946msrh5542fwf8kg0cjzd87ah7ycvwbl3wpkcfq4daf6gvdhqq")))

