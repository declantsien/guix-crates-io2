(define-module (crates-io sy sl syslog-ng-sys) #:use-module (crates-io))

(define-public crate-syslog-ng-sys-0.2.0 (c (n "syslog-ng-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18wpwqcc868d46jggrcn1cs4g76aivdvxvqqash15z0fmmqrfz7l")))

(define-public crate-syslog-ng-sys-0.2.1 (c (n "syslog-ng-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17jxjbg0482mffrsgkh3z0alh83854slb6mp2ij2phcj8hqj7hvv")))

(define-public crate-syslog-ng-sys-0.2.2 (c (n "syslog-ng-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c8wgjqwbm3wi6pb4sfh5ign9xjw0nrzsn14l60fkdb5p9j32653")))

(define-public crate-syslog-ng-sys-0.2.3 (c (n "syslog-ng-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pvb08b2m0ccwcpyv2i9rf7qjz21wba0fhs154wac55add0zsnld")))

(define-public crate-syslog-ng-sys-0.3.0 (c (n "syslog-ng-sys") (v "0.3.0") (d (list (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16vlh0w8hls7azsxy2fdg3grnllb1gy1n2fdlq7pdp9l76gvn17i")))

