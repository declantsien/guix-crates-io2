(define-module (crates-io sy sl syslog_rfc3164) #:use-module (crates-io))

(define-public crate-syslog_rfc3164-0.1.0 (c (n "syslog_rfc3164") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "18l4pnbw4ikamcfkvqgwba2p6yawdzk2l4v1i63yypizzxkr2mm7")))

