(define-module (crates-io sy sl syslog5424) #:use-module (crates-io))

(define-public crate-syslog5424-0.1.0 (c (n "syslog5424") (v "0.1.0") (h "0s16ir53dq406b841hknb59cayamdfab4qzmgy9jvfgzrrc6shr8")))

(define-public crate-syslog5424-0.1.1 (c (n "syslog5424") (v "0.1.1") (h "1f35fc5pgvx8b9a24v4yhvp6w8clvjiardqh1w23chwyh6jcazs2")))

