(define-module (crates-io sy sl syslog_heroku) #:use-module (crates-io))

(define-public crate-syslog_heroku-0.1.0 (c (n "syslog_heroku") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "17j59nig5lis3w3nibppc4fa9fi5qj4lmrgcvmjjf6bs9bq9gvy3")))

