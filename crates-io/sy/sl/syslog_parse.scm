(define-module (crates-io sy sl syslog_parse) #:use-module (crates-io))

(define-public crate-syslog_parse-1.0.0 (c (n "syslog_parse") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0rdhnswp4afxnjx6jv6bhx7bi035jpzdir9hngz1kqz811wjd11i")))

(define-public crate-syslog_parse-1.0.2 (c (n "syslog_parse") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hj43f0xavsydqzvmm1jkl382mr07nvk9g3dzk3gbhpicjg26211")))

