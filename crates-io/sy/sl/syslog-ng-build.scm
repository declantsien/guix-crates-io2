(define-module (crates-io sy sl syslog-ng-build) #:use-module (crates-io))

(define-public crate-syslog-ng-build-0.2.0 (c (n "syslog-ng-build") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)))) (h "0dcwh151642xf3j011jirnsgidp6f2gzk45xhlffbha0znbb9j47")))

