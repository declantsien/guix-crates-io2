(define-module (crates-io sy sl syslog-c) #:use-module (crates-io))

(define-public crate-syslog-c-0.1.0 (c (n "syslog-c") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0px1z8i090yj0czl3bfc9b3pr63i6jb3jfq745xypm93jix3d27w")))

(define-public crate-syslog-c-0.1.1 (c (n "syslog-c") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "188gzw8m26j6b82nakph0x12fwdz9c3kc2i35ndazmknmldgw119")))

(define-public crate-syslog-c-0.1.2 (c (n "syslog-c") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "183nxlxzapyz81fx7br2ih5rm3x0mzwad5wn9afby68p62v7npil")))

(define-public crate-syslog-c-0.1.3 (c (n "syslog-c") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vj4hbpbzhqsyrk8wx5pflcrwjnn6ryjd99c2sm936vj7h6gxx5r")))

