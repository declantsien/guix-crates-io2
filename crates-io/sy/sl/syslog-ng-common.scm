(define-module (crates-io sy sl syslog-ng-common) #:use-module (crates-io))

(define-public crate-syslog-ng-common-0.4.0 (c (n "syslog-ng-common") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2") (d #t) (k 0)))) (h "05gpng2gfl8vp6lzpq0gwjil4gwgcgicyfqf18afnapl5893xyrr")))

(define-public crate-syslog-ng-common-0.5.0 (c (n "syslog-ng-common") (v "0.5.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0zjbbyrxkk5325pgs3nyjbv00692qmfz5l84wh2j2d0h0mc68bbc")))

(define-public crate-syslog-ng-common-0.6.0 (c (n "syslog-ng-common") (v "0.6.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0fbhzd8mx3r2l3fbs3xwd4dx4rshc8b2yg1fgidzx5g4cydswn8a")))

(define-public crate-syslog-ng-common-0.6.1 (c (n "syslog-ng-common") (v "0.6.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.2") (d #t) (k 0)))) (h "00dqkc9m1z1mphw8ymv95zsa1vp9sxdiamg13spxni9i3mgqng20")))

(define-public crate-syslog-ng-common-0.6.2 (c (n "syslog-ng-common") (v "0.6.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0lca2pw384l6hpyy5qjlwdmrp12q689x34cvg3xzxijb9p4fwf2a")))

(define-public crate-syslog-ng-common-0.6.3 (c (n "syslog-ng-common") (v "0.6.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0hlifidlg5pnc4kx7mymh0548x1m4fi942l8bkfd9pp5n3dm6lhl")))

(define-public crate-syslog-ng-common-0.6.4 (c (n "syslog-ng-common") (v "0.6.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.2") (d #t) (k 0)))) (h "1aa4jw5b2nczp4lxyb8626y1b3lzw6c0xdr6040i17l572wdkb8n")))

(define-public crate-syslog-ng-common-0.7.0 (c (n "syslog-ng-common") (v "0.7.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.2.3") (d #t) (k 0)))) (h "14yr3kx3c1nc3hc5p9gjzxa7jf0g5ck72p9rpy8bch318z1i642m")))

(define-public crate-syslog-ng-common-0.8.0 (c (n "syslog-ng-common") (v "0.8.0") (d (list (d (n "glib") (r "^0.0.8") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "syslog-ng-sys") (r "^0.3") (d #t) (k 0)))) (h "16sm5w5m1dwwgal9vlh1gqax6avljwd3kxjba5g1b36v8kn63csd") (y #t)))

