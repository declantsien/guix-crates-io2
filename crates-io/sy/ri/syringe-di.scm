(define-module (crates-io sy ri syringe-di) #:use-module (crates-io))

(define-public crate-syringe-di-0.0.1 (c (n "syringe-di") (v "0.0.1") (d (list (d (n "frunk") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "0wh2iwxan4vaar681qcfaqf5v4imb46px3m9yi7lgm1s4bqbdfpp")))

(define-public crate-syringe-di-0.0.2 (c (n "syringe-di") (v "0.0.2") (d (list (d (n "frunk") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "0hi1v58bv5wj9x6q24i9l028cysafv80mcavxkx816x5qx03s0qb")))

