(define-module (crates-io sy ri syringe) #:use-module (crates-io))

(define-public crate-syringe-0.1.0 (c (n "syringe") (v "0.1.0") (d (list (d (n "dunce") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("ntdef" "handleapi" "errhandlingapi" "tlhelp32" "processthreadsapi" "securitybaseapi" "winbase" "handleapi" "libloaderapi" "memoryapi"))) (d #t) (k 0)))) (h "0404dacjjcawvl40vsn9d8bs4rkj94ddfgrwid2qx665f5zr8ln3")))

