(define-module (crates-io sy ri syringe-di-derive) #:use-module (crates-io))

(define-public crate-syringe-di-derive-0.0.1 (c (n "syringe-di-derive") (v "0.0.1") (h "0qyiaakh7k1ckfdhxxi25gp3vinsn9m4fq2xhgpj68vg36qk5k3q")))

(define-public crate-syringe-di-derive-0.0.2 (c (n "syringe-di-derive") (v "0.0.2") (h "132irqg2ih21h353nhqxxlrhj8a7vw0zz8pwrms48llv0aq0crzp")))

