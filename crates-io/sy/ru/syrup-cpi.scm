(define-module (crates-io sy ru syrup-cpi) #:use-module (crates-io))

(define-public crate-syrup-cpi-0.1.0 (c (n "syrup-cpi") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.1.2") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0x9gmj8fhyz1nc6ml69nj7yxh9kc2jvapyvdkndlac9ynxs35slx") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-syrup-cpi-0.2.0 (c (n "syrup-cpi") (v "0.2.0") (d (list (d (n "anchor-gen") (r "^0.1.2") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0v9y4n7p0zka3mkp80x5fkhwjgb1lrnz8jv11n3ig675j4d7g9yv") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

