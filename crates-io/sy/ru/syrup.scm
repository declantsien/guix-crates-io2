(define-module (crates-io sy ru syrup) #:use-module (crates-io))

(define-public crate-syrup-0.1.0 (c (n "syrup") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.15.0") (d #t) (k 0)))) (h "0la0ys6zac91i62g4l2w6hm17k67xp0iwhgh65ysn6qspcwcxihi")))

(define-public crate-syrup-0.2.0 (c (n "syrup") (v "0.2.0") (d (list (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.10") (k 0)))) (h "1f1npbzcp3vv58asv59352758k21bbl4gha6kv9bpp3xhd06w2xj")))

