(define-module (crates-io sy sf sysfs-serde) #:use-module (crates-io))

(define-public crate-sysfs-serde-0.2.0 (c (n "sysfs-serde") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12cbv5s53y28sd8kfwh7s8p4w609h9wyl9gml9ivpifgq69pgxn9")))

(define-public crate-sysfs-serde-0.2.1 (c (n "sysfs-serde") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1zgfk094l74blyqkfgl18gx8w1li8k5h9g9c6pizfxzqj5swq9b2")))

(define-public crate-sysfs-serde-0.2.2 (c (n "sysfs-serde") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yxcjn3dlv4if2kggyvf61l4q1h63lads6b19zl2xnbrcy8kq8ac")))

(define-public crate-sysfs-serde-0.2.3 (c (n "sysfs-serde") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0sh62np8sl06ckgm5fqi9mxf4rvffp9gbljc9ay0p7bxpiw4mswm")))

