(define-module (crates-io sy sf sysfp) #:use-module (crates-io))

(define-public crate-sysfp-0.0.1 (c (n "sysfp") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "18x391dk0k4hgv0vmzm2gzh5nymrfphjm3nz8509gr40jy921n9l")))

