(define-module (crates-io sy sf sysfuss) #:use-module (crates-io))

(define-public crate-sysfuss-0.1.0 (c (n "sysfuss") (v "0.1.0") (h "1wjgxib5h30swc84jkg16afcslm7hii9nvby9ypygrs4msnglzak") (f (quote (("derive") ("default"))))))

(define-public crate-sysfuss-0.2.0 (c (n "sysfuss") (v "0.2.0") (h "0icz9n87jwlkl1fhr4nmy3bcxzndf2csb49ryspl3zdkg5cdv92g") (f (quote (("derive") ("default"))))))

(define-public crate-sysfuss-0.3.0 (c (n "sysfuss") (v "0.3.0") (h "1p0lg7zqda55jnzfxxyc0f6dzk5jw1mc8k7dyasp39hijm9awfzk") (f (quote (("derive") ("default"))))))

