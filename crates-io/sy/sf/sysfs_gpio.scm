(define-module (crates-io sy sf sysfs_gpio) #:use-module (crates-io))

(define-public crate-sysfs_gpio-0.1.0 (c (n "sysfs_gpio") (v "0.1.0") (h "1g2m1h01l5fb2f5nlkn4hyg8ipra9cq25m3rvjayfghkr45s8mbz")))

(define-public crate-sysfs_gpio-0.1.1 (c (n "sysfs_gpio") (v "0.1.1") (h "1ds48ilf7n896anj650n9jjsfv9blnpdsvm1g86602pqmls9y6mi")))

(define-public crate-sysfs_gpio-0.2.0 (c (n "sysfs_gpio") (v "0.2.0") (h "1vqsyk8sdm64q9lzdqriy7jfnkcb9kk916lkyhr0kpgr68qqbksz")))

(define-public crate-sysfs_gpio-0.2.1 (c (n "sysfs_gpio") (v "0.2.1") (h "04z3lis27414hn5hx3v9ba8ph6521c4pyf6l26q5adld75a9j7vj")))

(define-public crate-sysfs_gpio-0.3.0 (c (n "sysfs_gpio") (v "0.3.0") (d (list (d (n "nix") (r "*") (d #t) (k 0)))) (h "07favkf08qkg8qxwvv99j2przx5320ybriqrq1ifa4h4mplnqc28")))

(define-public crate-sysfs_gpio-0.3.1 (c (n "sysfs_gpio") (v "0.3.1") (d (list (d (n "nix") (r "*") (d #t) (k 0)))) (h "099h2l2m517lg74k1ym5s8fnpzinzw525bc01awmj8dpbs05is4m")))

(define-public crate-sysfs_gpio-0.3.2 (c (n "sysfs_gpio") (v "0.3.2") (d (list (d (n "nix") (r "*") (d #t) (k 0)))) (h "11wzvynld62f4g2f40sxi8rb9r6rd7042jj8a9visz1v4b2ky512")))

(define-public crate-sysfs_gpio-0.3.3 (c (n "sysfs_gpio") (v "0.3.3") (d (list (d (n "nix") (r "*") (d #t) (k 0)))) (h "1gb149hv6chyzsfigsl100x7ddvhfczdhj299bcsxfwkbl3ikrfg")))

(define-public crate-sysfs_gpio-0.4.0 (c (n "sysfs_gpio") (v "0.4.0") (d (list (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "0wqyiwrjhjvymqw58vry0is270943q4m1h999b6dh3zp9bx72q64")))

(define-public crate-sysfs_gpio-0.4.1 (c (n "sysfs_gpio") (v "0.4.1") (d (list (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1arzh82vcmq00kadjip2544fafmbay6vb7bxy5iswfjl06vaas1l")))

(define-public crate-sysfs_gpio-0.4.2 (c (n "sysfs_gpio") (v "0.4.2") (d (list (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "04gmrad1hvg2ahilriqzgaah0h5sxndhzndf2gz82cssl28mq59q")))

(define-public crate-sysfs_gpio-0.4.3 (c (n "sysfs_gpio") (v "0.4.3") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "09xahkfq8fbwl5x87iigfca5869y4sxv52n1pag5fyp65l9mmp9d")))

(define-public crate-sysfs_gpio-0.4.4 (c (n "sysfs_gpio") (v "0.4.4") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "14055kc38xv3pvvis9w481him67x8h57mfvcxdd1qb5kjpibx2c9")))

(define-public crate-sysfs_gpio-0.5.0 (c (n "sysfs_gpio") (v "0.5.0") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0xdvdqf9hrphyadbv1sf2d8f9pw55fv18yz61p8iq3qs7rmhbz4h") (f (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5.1 (c (n "sysfs_gpio") (v "0.5.1") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hx5gxacpn2vfx288y70110pc0y3rkgbyrw0r0xzwgrfipqqkp6i") (f (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5.2 (c (n "sysfs_gpio") (v "0.5.2") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06k9g6qr2rksl448fylj2fbcbjsvn9bpk725551gdqg4ds9mhzx6") (f (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5.3 (c (n "sysfs_gpio") (v "0.5.3") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0gqyd76kfq4vk6sfargf8i3rf0gv3z1qr1carra9zly7wg5g4s1x") (f (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5.4 (c (n "sysfs_gpio") (v "0.5.4") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ldmm5xa040726kkc0wpsvcgw5jijhc8ghyfyvjdh8v6hiaim5i4") (f (quote (("use_tokio" "futures" "tokio" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.6.0 (c (n "sysfs_gpio") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-ext"))) (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1izpcffcij1v844x05lf7g0bnky7nzlqhnhrbz7mc3rf447vm9yf") (f (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

(define-public crate-sysfs_gpio-0.6.1 (c (n "sysfs_gpio") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0ms149rl90qhx9c3kw0nspf3apr36sbwbcjrvbj6qngbpyyckycy") (f (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

(define-public crate-sysfs_gpio-0.6.2 (c (n "sysfs_gpio") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "05symfiy6whlzayy40v84f2ibmddm358p0zp5v36arcjpiaqr078") (f (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

