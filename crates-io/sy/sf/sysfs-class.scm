(define-module (crates-io sy sf sysfs-class) #:use-module (crates-io))

(define-public crate-sysfs-class-0.1.0 (c (n "sysfs-class") (v "0.1.0") (h "1ybryi8jyrk8b5jrkk8rkdwlkblzih3ayywrkjbdv559mkzm8z3h")))

(define-public crate-sysfs-class-0.1.1 (c (n "sysfs-class") (v "0.1.1") (h "04pnrkm7yd2lar8zdkip2a6vw9gl7s1sq8s0m6s6wmqsk0ia1va8")))

(define-public crate-sysfs-class-0.1.2 (c (n "sysfs-class") (v "0.1.2") (d (list (d (n "numtoa") (r "^0.2.3") (d #t) (k 0)))) (h "0gkg83pka9fadf12bkvlxk6h32bxk6j1i1mv0lgg0kql1rflm3ki")))

(define-public crate-sysfs-class-0.1.3 (c (n "sysfs-class") (v "0.1.3") (d (list (d (n "numtoa") (r "^0.2.3") (d #t) (k 0)))) (h "1lani118vxa29127wl8kfv1xy4flsqggdxwqg2klab3kd7wbq6sy")))

