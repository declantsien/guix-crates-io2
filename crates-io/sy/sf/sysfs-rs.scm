(define-module (crates-io sy sf sysfs-rs) #:use-module (crates-io))

(define-public crate-sysfs-rs-0.0.1 (c (n "sysfs-rs") (v "0.0.1") (h "1iy54dy8ks4j2plfiskpdc762hzg0yw9x7y1g5inw55kip3xigjv")))

(define-public crate-sysfs-rs-0.0.2 (c (n "sysfs-rs") (v "0.0.2") (h "1xmnhigmxqgbl0lc01ph8fkf3bw70cib72sx0f43vqk1arswq3qg")))

(define-public crate-sysfs-rs-0.0.3 (c (n "sysfs-rs") (v "0.0.3") (h "02vbmd0sziswkx41jqxjyw24x8rj7sgn44w869xsq0lh1rc425j1")))

(define-public crate-sysfs-rs-0.0.4 (c (n "sysfs-rs") (v "0.0.4") (h "02c5f65ydl3rfimh3iiih9lys331dcm811zfb7sz8wqbccxfqj49")))

(define-public crate-sysfs-rs-0.0.5 (c (n "sysfs-rs") (v "0.0.5") (h "1vn7h9rn08rsd2q5981xm0d3rbvicrcwsa557k16h30vb449afaa")))

(define-public crate-sysfs-rs-0.0.6 (c (n "sysfs-rs") (v "0.0.6") (h "0n920nzi0nwqk3n9cwi3r64pfvj60266q86asa8940zpah6w1shd")))

(define-public crate-sysfs-rs-0.0.7 (c (n "sysfs-rs") (v "0.0.7") (h "0jbqwybynxqahirkssjc3wrr501q9lv19byvqzv37xww50gd7gad")))

(define-public crate-sysfs-rs-0.0.8 (c (n "sysfs-rs") (v "0.0.8") (h "1rw1jvi403js0sfn1534lwqafm9pw1l8cllakinmh65340xnwkh4")))

(define-public crate-sysfs-rs-0.0.9 (c (n "sysfs-rs") (v "0.0.9") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0fbnh8hy8wc0a4p8qrmk1mhnbl76jqyvjq3s58nxn73s1cddmpk8")))

(define-public crate-sysfs-rs-0.0.10 (c (n "sysfs-rs") (v "0.0.10") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "02k0nhwi8cqflzjr2ivmijgba57ya2596xhv682ahj258wfn01x4")))

(define-public crate-sysfs-rs-0.0.11 (c (n "sysfs-rs") (v "0.0.11") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "15zf13636ac41jrbjyfvhy15d1ichj6cp9p45ipq24qdsqgzfjm4")))

(define-public crate-sysfs-rs-0.0.12 (c (n "sysfs-rs") (v "0.0.12") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1f76limx6s6bza8gj1zjkl9vb76rgnykb20apph7k461nk4g1i03")))

