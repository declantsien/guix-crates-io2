(define-module (crates-io sy sf sysfunc-byteorder) #:use-module (crates-io))

(define-public crate-sysfunc-byteorder-0.1.0 (c (n "sysfunc-byteorder") (v "0.1.0") (h "166lnpn3225aami5fmkqicrmjpsk48mn9gqyc9flbb84zkz4in9s") (f (quote (("no-core") ("enable-128") ("default")))) (y #t)))

(define-public crate-sysfunc-byteorder-0.1.1 (c (n "sysfunc-byteorder") (v "0.1.1") (h "0g5xyz4hgkq6rdx4yk0p0p020ay1nf4kym74drf2s60yrcipkh2i") (f (quote (("no-core") ("enable-128") ("default")))) (y #t)))

(define-public crate-sysfunc-byteorder-0.1.2 (c (n "sysfunc-byteorder") (v "0.1.2") (h "1ws3m7k67wjyahj2gnlhzkij6rk1zzrngxw9qapybkg5kq7d7nhp") (f (quote (("no-core") ("force-conversion") ("enable-128") ("default"))))))

