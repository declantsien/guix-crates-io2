(define-module (crates-io sy sf sysfunc-blockcipher-xtea) #:use-module (crates-io))

(define-public crate-sysfunc-blockcipher-xtea-0.1.0 (c (n "sysfunc-blockcipher-xtea") (v "0.1.0") (h "1dh6az0536kqwgx7y5fga036bddqngbm23xaz0hnaraqgadvr126")))

(define-public crate-sysfunc-blockcipher-xtea-0.1.1 (c (n "sysfunc-blockcipher-xtea") (v "0.1.1") (h "1z69r89wvgm1dzxrpv4rb7dxmrncljnjh1fy96qdx6ya40rg6zi7")))

