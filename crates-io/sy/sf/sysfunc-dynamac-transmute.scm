(define-module (crates-io sy sf sysfunc-dynamac-transmute) #:use-module (crates-io))

(define-public crate-sysfunc-dynamac-transmute-0.1.1 (c (n "sysfunc-dynamac-transmute") (v "0.1.1") (h "0jwp1c3nrqfc22m7ha66186rxckn6sz6a9h8c292fvcck3kgaajd") (f (quote (("rust-documentation") ("no-core") ("enable-std") ("default"))))))

