(define-module (crates-io sy sf sysfetch) #:use-module (crates-io))

(define-public crate-sysfetch-1.1.0 (c (n "sysfetch") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0i1vsri3qb1q8ccac82rghdv2zjvbl8m46zwgvm0sjww602n2b0l") (y #t)))

(define-public crate-sysfetch-1.2.0 (c (n "sysfetch") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "12jp8h3p3adz6y3hy10iknmg3jar8bizr57qakwynv2zvl986gvi") (y #t)))

