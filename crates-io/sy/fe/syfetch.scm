(define-module (crates-io sy fe syfetch) #:use-module (crates-io))

(define-public crate-syfetch-0.1.0 (c (n "syfetch") (v "0.1.0") (h "04g7rawqaaflh8hakvkhbqxzahw23bk01p8vkhqn2cv5bjh9kyv4")))

(define-public crate-syfetch-0.1.1 (c (n "syfetch") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0i5dzf8009b1nj5yis5c2a28nqmnchlwjm5xqhz10w6d6z3vxwmw")))

(define-public crate-syfetch-0.1.2 (c (n "syfetch") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1mqbsivcrrf535b0gxv3vbzc33bmw79mzy7n1xqcpn0mkznwifmm")))

