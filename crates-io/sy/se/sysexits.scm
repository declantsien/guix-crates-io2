(define-module (crates-io sy se sysexits) #:use-module (crates-io))

(define-public crate-sysexits-0.1.0 (c (n "sysexits") (v "0.1.0") (h "127p5sn5s6sjr1b411v7jvwrv463b7jhcdbdl2vqnb2bqlwxz4y0") (r "1.61.0")))

(define-public crate-sysexits-0.1.1 (c (n "sysexits") (v "0.1.1") (h "0pa2gqcn13q1yw6425wlacnd55s1bkdcy60ll9ysfqs0ilkwh1pc") (r "1.61.0")))

(define-public crate-sysexits-0.2.0 (c (n "sysexits") (v "0.2.0") (h "0yincvc9ma60kzqqc4sjv1i8lxc473hqziv871lbqbqrw6bc2nlj") (r "1.61.0")))

(define-public crate-sysexits-0.2.1 (c (n "sysexits") (v "0.2.1") (h "1s5lvmzvy9q7d8wh91ig5lfbz0w6i3mhad0p3rw67cbkf0a3qpwx") (r "1.61.0")))

(define-public crate-sysexits-0.2.2 (c (n "sysexits") (v "0.2.2") (h "1j69s4hdskxzh349qpsnqpvmjhiifbyrja3y90krxrvp7l0kz47j") (r "1.61.0")))

(define-public crate-sysexits-0.3.0 (c (n "sysexits") (v "0.3.0") (h "0adnw8hscy8jylkpkirjmglnnhflbwkn1nd21512l2kdckm4vhjp") (r "1.61.0")))

(define-public crate-sysexits-0.3.1 (c (n "sysexits") (v "0.3.1") (h "0kb46filxgg44wnlb90xhq3kvzspczlrh0aywb7jjin12y4r1fvh") (r "1.61.0")))

(define-public crate-sysexits-0.3.2 (c (n "sysexits") (v "0.3.2") (h "183l4vkq507l4ra2852ka933lj938jyfnirjhrgcggilqy192mn1") (r "1.61.0")))

(define-public crate-sysexits-0.3.3 (c (n "sysexits") (v "0.3.3") (h "0hyf9a2wrw8idqx7s8svbjran9lf32gck0fidn3043m90zlmylrn") (r "1.61.0")))

(define-public crate-sysexits-0.3.4 (c (n "sysexits") (v "0.3.4") (h "0wpsvi1xjbilx2y5qm07pasczq6grq31fa3fwxwfqmfw7yf6qfwi") (r "1.61.0")))

(define-public crate-sysexits-0.4.0 (c (n "sysexits") (v "0.4.0") (h "0gblrxdny2s2mm6dnmrdskh0bc2ymid8dgkgjiyyq7r7rzsfzm5w") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.4.1 (c (n "sysexits") (v "0.4.1") (h "08p8ns7wsvp1jdj7kzz9c1qd7iagk32s6rzl1lliwmg6wrl40ay0") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.5.0 (c (n "sysexits") (v "0.5.0") (h "1rb4yyx602rqaaq25nq45r6d987wi7dw0zpg6008cwlgnhfnqz0g") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.6.0 (c (n "sysexits") (v "0.6.0") (h "17s22c8d425khbxfi2ia6z6qfaibjyy4saqcnps97c49dihwkd92") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.6.1 (c (n "sysexits") (v "0.6.1") (h "190130yq60wqii72wmraxzc8j1gfg36wdvkwk2was7x3f8665myr") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.6.2 (c (n "sysexits") (v "0.6.2") (h "0s3lzb9z6yx0lh9j3n7q9jw0pa0382fqb8k51jpmgq47rwiwrbk4") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.6.3 (c (n "sysexits") (v "0.6.3") (h "0dxf46i79d8if9qi0d8ic92xv4b14y4ah3401yrd3bcgsqbc0gi8") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.0 (c (n "sysexits") (v "0.7.0") (h "0mwagmk3j2056ga8bm1fb1lfix2yj5xn3p5sjx1bkr0xgmgpwjy6") (f (quote (("std") ("default" "std")))) (y #t) (r "1.61.0")))

(define-public crate-sysexits-0.7.1 (c (n "sysexits") (v "0.7.1") (h "1zq597v05lwyp8lrjqjbspjy78qg382fi7mnl4r3ns0rzc76s96l") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.2 (c (n "sysexits") (v "0.7.2") (h "1mm3d4i3ry56j93zaicxhx0hsqv73z0gvx6rkvgr4fck9m2pqxpd") (f (quote (("std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.3 (c (n "sysexits") (v "0.7.3") (h "17a66dwwk4apnrc4rlv9vrz2y2khxd1f0zxyhsb52g35cnck49b4") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.4 (c (n "sysexits") (v "0.7.4") (h "06cjkzcm2bxsi8jxwhwg35abwafqxisprd52mn4mp5vzy0bf6kia") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.5 (c (n "sysexits") (v "0.7.5") (h "04vcdsk460x598z4w5f7rvzwbwn3lw1rkbyzskcw6r0fnjj7yn1m") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.6 (c (n "sysexits") (v "0.7.6") (h "1x8jbmsncg1wgqfyhpca04r58bz4srzqcsf0iiaqa0w9r50ljwhy") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.7 (c (n "sysexits") (v "0.7.7") (h "0mngn6dxzf53m73hrgqih01556d0w8cgxxw27kiyf44g9yzhwgc6") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.8 (c (n "sysexits") (v "0.7.8") (h "1bvrfyhi8qfckq404l3mmifxxgkq17qma4jqaq9l4rsmy9r421lm") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.9 (c (n "sysexits") (v "0.7.9") (h "0pcj2kvn64bs0061rv0r43sfyjbqsi78zii3x6lqahpkikhcfwhf") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (y #t) (r "1.61.0")))

(define-public crate-sysexits-0.7.10 (c (n "sysexits") (v "0.7.10") (h "1rp9php91drmqq2dbnig77g580il341ciyhzn1wcxm6fqsndi5q1") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.11 (c (n "sysexits") (v "0.7.11") (h "0wpr9kvv14srxj5x056235c0fwbwmyiwm2j6aa4i43bx04gq2wqh") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.12 (c (n "sysexits") (v "0.7.12") (h "1jvwqspw3s1cmvs2pi33zpqp3kj6win54scvvl2vzn0q7nkpi4bx") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.13 (c (n "sysexits") (v "0.7.13") (h "1gn2dficr7p4ip33y321spk4vzgkzwzn4v354w9g3vz3pjrk7sb2") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

(define-public crate-sysexits-0.7.14 (c (n "sysexits") (v "0.7.14") (h "10j2y6hzi349v56qmzizzr09qf4hfaiyx5jcqfk953ywliw6rf5m") (f (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (r "1.61.0")))

