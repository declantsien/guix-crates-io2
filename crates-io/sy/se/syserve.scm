(define-module (crates-io sy se syserve) #:use-module (crates-io))

(define-public crate-syserve-0.1.0 (c (n "syserve") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.5.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)))) (h "0vh3jp7zhqa6ndmyy8mxag89icsa87657fsmkqj0pd5wl6x4xhzr")))

(define-public crate-syserve-0.2.0 (c (n "syserve") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "01qzkkn29r6jr6v3860r9lrrh5rx206frfywk66fy0v003xv655f")))

(define-public crate-syserve-0.2.1 (c (n "syserve") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "18jjjra2skmgncs2apawbbf2y82iazxmci6f283ilyrv882qcjmm")))

(define-public crate-syserve-0.3.0 (c (n "syserve") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "httparse") (r "^1.3.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0ga1kc66ba2h2nq2zwnq68d0k5yfh44h3iz1wislw2p1fx5af27l")))

