(define-module (crates-io sy se sysexit) #:use-module (crates-io))

(define-public crate-sysexit-0.1.0 (c (n "sysexit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1nh1ypp5xgfgzpz94zrgz8mcql3fsiw14ccnl5rpnm0jilw4gcvh")))

(define-public crate-sysexit-0.1.1 (c (n "sysexit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0p4874nsc8bwqbiia7v2gas3c38r4m39fg8a8r0ipnq2n3qc9ln5")))

(define-public crate-sysexit-0.2.0 (c (n "sysexit") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1ad6i92nxzhqv0s07am350cr4ngsksbmfswdkan3kybf1sxr2sd2")))

