(define-module (crates-io sy zy syzygy) #:use-module (crates-io))

(define-public crate-syzygy-0.0.1 (c (n "syzygy") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1w2akyqpnvjymzv2xl6m4w11k48yp6w9ajwkgx5gpkwfry7cv7pw")))

(define-public crate-syzygy-0.0.2 (c (n "syzygy") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ypx9fkm3pv5nh6lp8vsnihrk40lysf76c9zn89sfwlnbzirwhp4")))

(define-public crate-syzygy-0.0.3 (c (n "syzygy") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0y22934vwz0cscgdpfp7m6alvk1p78z83l1mb8i21ijm68cazdma")))

(define-public crate-syzygy-0.0.4 (c (n "syzygy") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "typetag") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0w0ln5lbj1bz03fv42n1zfxfwlfxzfz2f7vjagmmhh4qy8aaa93s")))

(define-public crate-syzygy-0.0.5 (c (n "syzygy") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "typetag") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "05c1r7f2p4z9lm48brkbmw95vrvi800x2pwgj5rqa8c779dijlzq")))

(define-public crate-syzygy-0.0.6 (c (n "syzygy") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "typetag") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1j6jcygs8dg736cv3rnpsx27ngqladmy1p7nm824qkbm6mx7y9in")))

