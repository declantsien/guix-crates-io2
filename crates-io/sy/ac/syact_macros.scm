(define-module (crates-io sy ac syact_macros) #:use-module (crates-io))

(define-public crate-syact_macros-0.1.0 (c (n "syact_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "04i6w2z8b3yh8i5apwxdl5rc30q59wkb9vbhdfxwacnhnbh8kigc")))

(define-public crate-syact_macros-0.1.1 (c (n "syact_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1spzbw5w6i045n2q6my1kc7plgzdnimn40jpw9my0k9y1cpyambp")))

