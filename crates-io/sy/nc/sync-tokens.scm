(define-module (crates-io sy nc sync-tokens) #:use-module (crates-io))

(define-public crate-sync-tokens-0.1.0 (c (n "sync-tokens") (v "0.1.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cooked-waker") (r "^4.0.0") (d #t) (k 2)) (d (n "futures") (r "0.*") (d #t) (k 0)))) (h "1pghx7i6a80gbsihcnfcvk1ynzlgm3mg2ac8i20njhyaqv568x3s")))

