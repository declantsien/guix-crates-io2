(define-module (crates-io sy nc syncfast) #:use-module (crates-io))

(define-public crate-syncfast-0.0.0 (c (n "syncfast") (v "0.0.0") (h "00bpxy3235ggaw3c4mywpfgr7kfcinwwfih2674c53k5yq6ln73k")))

(define-public crate-syncfast-0.2.0 (c (n "syncfast") (v "0.2.0") (d (list (d (n "cdchunking") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (f (quote ("termcolor" "atty" "humantime"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.11") (f (quote ("io-std" "io-util" "process" "rt"))) (d #t) (k 0)))) (h "1kq8gxlbgq2kyhdb0m1qr7j74lyxw5hdlnchal91i750fb1rdbr0")))

