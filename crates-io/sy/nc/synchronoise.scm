(define-module (crates-io sy nc synchronoise) #:use-module (crates-io))

(define-public crate-synchronoise-0.1.0 (c (n "synchronoise") (v "0.1.0") (h "1mzpy8rpv0hgd1za829ddsgnyhz1d06hkqw2m6lfsldqd5z7zrz3")))

(define-public crate-synchronoise-0.2.0 (c (n "synchronoise") (v "0.2.0") (h "1ry0s8rbrrw0xzzwa8vf1nmnf2nl4b7cgicpl7hdv8d831ql5a58")))

(define-public crate-synchronoise-0.3.0 (c (n "synchronoise") (v "0.3.0") (h "09bp9k5y2a8y6x5ns2wc6cn251ald2pqmsflpqzh7ygq3g47wik0")))

(define-public crate-synchronoise-0.4.0 (c (n "synchronoise") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)))) (h "0f2lkrv4sajkh1zl2wkfgzpxbvibzkpk4qgmpa5zz08lnln0pp18")))

(define-public crate-synchronoise-1.0.0 (c (n "synchronoise") (v "1.0.0") (d (list (d (n "crossbeam-queue") (r "^0.1") (d #t) (k 0)))) (h "1szasv8xl6z3gxfq8h8wllw2mq53d71nn29achxsnfcxzh7fs5yp")))

(define-public crate-synchronoise-1.0.1 (c (n "synchronoise") (v "1.0.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)))) (h "1wnylkdf84520ks7a70fnwds2wibxmnkgqzz3j6ww9n61wwh3g1x")))

