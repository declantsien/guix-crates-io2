(define-module (crates-io sy nc synctree) #:use-module (crates-io))

(define-public crate-synctree-0.1.0 (c (n "synctree") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "onevec") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "0c6hj2mprffv4fs7y5i1k079i1rgqwihag901cagamnall4zkd3m")))

(define-public crate-synctree-0.1.1 (c (n "synctree") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "onevec") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "1l2j0bm4hzkffjrq9zws3ig662yb0jwibq8sgkd0nb6s8jlf8wfg")))

(define-public crate-synctree-0.1.2 (c (n "synctree") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "onevec") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "1wiazyga3z55d3jg5rwqj0sdrymh8b0fvh7ca9x0y5y0jgk38fn1")))

(define-public crate-synctree-0.1.3 (c (n "synctree") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "onevec") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "1qvj4559dvl4xzxib3na10kc89x6m7qavismh7k4xv527d8pkrw2")))

