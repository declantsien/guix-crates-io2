(define-module (crates-io sy nc sync-lsp-derive) #:use-module (crates-io))

(define-public crate-sync-lsp-derive-0.1.0 (c (n "sync-lsp-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1wan9ycj6ifnmwdl584d7jaly7dmqg20y2i4jrvd1dnqpzngwgr7")))

