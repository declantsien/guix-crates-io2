(define-module (crates-io sy nc sync42) #:use-module (crates-io))

(define-public crate-sync42-0.1.0 (c (n "sync42") (v "0.1.0") (d (list (d (n "biometrics") (r "^0.1.1") (d #t) (k 0)))) (h "1dfp12sbxljj4mlcxk84jv10024d5vz023iwqd2a6nl2lkypdc2n")))

(define-public crate-sync42-0.1.1 (c (n "sync42") (v "0.1.1") (d (list (d (n "biometrics") (r "^0.2") (d #t) (k 0)))) (h "108kcjdmqdqdxd0ki97xka4jvq8rbfcpbzhh3rcw6m91a8wv43ip")))

(define-public crate-sync42-0.2.0 (c (n "sync42") (v "0.2.0") (d (list (d (n "biometrics") (r "^0.3") (d #t) (k 0)))) (h "1mv13k29dsld34yj55wkqczr5gzjxj2mddjzd7xh35j727mk4v5c")))

(define-public crate-sync42-0.3.0 (c (n "sync42") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.3") (d #t) (k 0)))) (h "05asim4df26nafbkvizqp1flzmhz1693k7xx17kvwbg3hx7p17pg")))

(define-public crate-sync42-0.4.0 (c (n "sync42") (v "0.4.0") (d (list (d (n "biometrics") (r "^0.4") (d #t) (k 0)))) (h "15x35dji08ick002imcqq0c99fyq7jpsnby2s4kwl31n8sldsa2d")))

(define-public crate-sync42-0.5.0 (c (n "sync42") (v "0.5.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)))) (h "00yi7b0jkqwhj7mklvkm2kls3vcsw9pyh5qr4fw5xrqlydr6vzj6") (f (quote (("default" "binaries") ("binaries"))))))

(define-public crate-sync42-0.6.0 (c (n "sync42") (v "0.6.0") (d (list (d (n "biometrics") (r "^0.6") (d #t) (k 0)) (d (n "guacamole") (r "^0.6") (d #t) (k 2)))) (h "19rjm03p9y7ds8xjx3m66qfk0x9xggkqhvkb85v73fvn45wj7ybj") (f (quote (("default" "binaries") ("binaries"))))))

