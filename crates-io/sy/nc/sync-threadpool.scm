(define-module (crates-io sy nc sync-threadpool) #:use-module (crates-io))

(define-public crate-sync-threadpool-0.0.1 (c (n "sync-threadpool") (v "0.0.1") (h "0mwcgrsg84kym08pcda8lsyc0682nmakqck0xrdk821ql8blkn8i")))

(define-public crate-sync-threadpool-0.0.2 (c (n "sync-threadpool") (v "0.0.2") (h "1scaksyqq0skfzb4k3anh40wn0q8i2vvfpwr9y416igyxmahn2gm")))

