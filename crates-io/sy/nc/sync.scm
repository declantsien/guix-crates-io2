(define-module (crates-io sy nc sync) #:use-module (crates-io))

(define-public crate-sync-0.0.0 (c (n "sync") (v "0.0.0") (h "1kb1zmq2in191fkxy5mvsv480pqyidhp3l5a2zqgvz0yzlp06vc2")))

(define-public crate-sync-0.1.0 (c (n "sync") (v "0.1.0") (h "1pgglpdlfv8g5wn2hf39ps84gxklcg0crw2vcvljnng5df3ci1f2")))

