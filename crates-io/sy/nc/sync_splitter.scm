(define-module (crates-io sy nc sync_splitter) #:use-module (crates-io))

(define-public crate-sync_splitter-0.2.0 (c (n "sync_splitter") (v "0.2.0") (h "0arjhdr8y63c9k804rgaczr1b9fh0fq7234g1rr4b2wibarv3r3f") (y #t)))

(define-public crate-sync_splitter-0.3.0 (c (n "sync_splitter") (v "0.3.0") (h "1jaj38gmq8m181x12hjcgrqhw1a6wm3fki3m1zssrfpp3spmxf79") (y #t)))

(define-public crate-sync_splitter-0.4.0 (c (n "sync_splitter") (v "0.4.0") (d (list (d (n "rayon") (r "^0.8.2") (d #t) (k 2)))) (h "1m09gnrgw8qzj92ni6jwwkqrbw9m8aqk5yglf0d8sg4bf5g65znz")))

(define-public crate-sync_splitter-0.4.1 (c (n "sync_splitter") (v "0.4.1") (d (list (d (n "rayon") (r "^0.8.2") (d #t) (k 2)))) (h "0p4jskrfd96lx1i243pmagic9y269b2g3928bjcc3ila6ypnagh4")))

