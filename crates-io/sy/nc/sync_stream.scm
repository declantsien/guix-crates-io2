(define-module (crates-io sy nc sync_stream) #:use-module (crates-io))

(define-public crate-sync_stream-0.1.0 (c (n "sync_stream") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "mixed_array") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sync_stream_struct_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "11dais20rl327dsg8pxiqf048djx6zszx1csarb89f395i7pk19y")))

