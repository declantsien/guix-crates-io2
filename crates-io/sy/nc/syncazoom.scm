(define-module (crates-io sy nc syncazoom) #:use-module (crates-io))

(define-public crate-syncazoom-0.1.0 (c (n "syncazoom") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 1)) (d (n "cron") (r "^0.6.0") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "minreq") (r "^2.1.0") (f (quote ("https" "json-using-serde"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0mnn5c2vjx5jnp878a9ii73r14db1zmgqc33ss8gcxlxa2jbfqfz")))

