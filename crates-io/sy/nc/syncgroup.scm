(define-module (crates-io sy nc syncgroup) #:use-module (crates-io))

(define-public crate-syncgroup-0.1.0 (c (n "syncgroup") (v "0.1.0") (h "1qn213p2clz451cqwpl0vrw70qc0xcdxh108ca82pk01wmnhq1dn")))

(define-public crate-syncgroup-0.1.1 (c (n "syncgroup") (v "0.1.1") (h "0ch85bar6maq8bip9s5b52wphjp4ad4cxc7f2f04z6dp6nz3989j")))

