(define-module (crates-io sy nc synchrotron) #:use-module (crates-io))

(define-public crate-synchrotron-0.1.0 (c (n "synchrotron") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "index_queue") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 2)) (d (n "vec-arena") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0ab488rjv9ng64wv9k78s0c17wp0k6w8i58i9ncjzm162jzswc9z")))

