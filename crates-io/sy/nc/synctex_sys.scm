(define-module (crates-io sy nc synctex_sys) #:use-module (crates-io))

(define-public crate-synctex_sys-0.1.0 (c (n "synctex_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "1r086ldr1wj8mh2mz571z9afksnj3jb5aiiz23cws6d8455fisgf") (l "libsynctex")))

(define-public crate-synctex_sys-0.1.1 (c (n "synctex_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "1w8l0g2nyjmj2rby3zziil4q61j5nznq0biai10r4wj2805vfs05") (l "libsynctex")))

