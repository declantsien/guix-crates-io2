(define-module (crates-io sy nc synchronicity) #:use-module (crates-io))

(define-public crate-synchronicity-0.0.0 (c (n "synchronicity") (v "0.0.0") (h "0yin6r0z8fb44wqhsy7i88kkwmwjhca7g727gzfdcmbx2z53jm2j")))

(define-public crate-synchronicity-0.0.1 (c (n "synchronicity") (v "0.0.1") (d (list (d (n "abscissa_core") (r "^0.4.0") (d #t) (k 0)) (d (n "abscissa_core") (r "^0.4.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "07h4x9g52y3fndia3yqgcdnlpczp10rmdsqiym4kn8hlqygjl8a9") (y #t)))

