(define-module (crates-io sy nc synchronous-server) #:use-module (crates-io))

(define-public crate-synchronous-server-0.1.2 (c (n "synchronous-server") (v "0.1.2") (d (list (d (n "napi") (r "^2") (d #t) (k 0)) (d (n "napi-build") (r "^1") (d #t) (k 1)) (d (n "napi-derive") (r "^2") (d #t) (k 0)) (d (n "proxy-server") (r "^0.5.1") (f (quote ("napi"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "14j5bd28b5xrz0fnz1v98qhb3p1kxric90kc2wn5jdq4c5zsdv7p") (r "1.74")))

(define-public crate-synchronous-server-0.1.3 (c (n "synchronous-server") (v "0.1.3") (d (list (d (n "proxy-server") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1wrxzc55y47pcbzw8fvihfn7m8hbjyfyp9hgbxin84flg6mrw989") (r "1.74")))

(define-public crate-synchronous-server-0.1.4 (c (n "synchronous-server") (v "0.1.4") (d (list (d (n "proxy-server") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1wwlncp4jrdqvk0qj7cn9nidfknzgqqy21b4pdwlzbkcxihv6whh") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.5 (c (n "synchronous-server") (v "0.1.5") (d (list (d (n "proxy-server") (r "^0.5.2") (d #t) (k 0)))) (h "1fwiy618sngwf7f3nabxfrzb3w02sqsjwkmdbw1fm0zn2j9hcxz6") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.6 (c (n "synchronous-server") (v "0.1.6") (d (list (d (n "proxy-server") (r "^0.6.0") (d #t) (k 0)))) (h "1d4jy97gpy5dmpzmyf9jx9w1w43qi0dj38yqddg3fmb3s7yfj3n2") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.7 (c (n "synchronous-server") (v "0.1.7") (d (list (d (n "proxy-server") (r "^0.6.0") (d #t) (k 0)))) (h "0pjbxhg03vmqrkjd7mami01qi4pr1bb5zy87xbazfzrfxxl8qlns") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.8 (c (n "synchronous-server") (v "0.1.8") (d (list (d (n "proxy-server") (r "^0.6.1") (d #t) (k 0)))) (h "0vg2s3znmq9kymh996pg4l4rsiakspil23dr0dj529s6739hghw1") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.9 (c (n "synchronous-server") (v "0.1.9") (d (list (d (n "proxy-server") (r "^0.6.2") (d #t) (k 0)))) (h "0dhgv6chb82impib6mwwmvyr02784ivhsab14fs7b7j0g1irq11p") (f (quote (("napi" "proxy-server/napi")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.11 (c (n "synchronous-server") (v "0.1.11") (d (list (d (n "proxy-server") (r "^0.6.4") (d #t) (k 0)))) (h "1h1m2ifjniq73ynbhv3sygpcl48s7y6p6ssdpp146f3l7zlkj8nb") (f (quote (("napi" "proxy-server/napi") ("chunk_5KB" "proxy-server/chunk_5KB") ("chunk_50KB" "proxy-server/chunk_50KB") ("chunk_500KB" "proxy-server/chunk_500KB") ("chunk_1MB" "proxy-server/chunk_1MB") ("chunk_1KB" "proxy-server/chunk_1KB") ("chunk_10KB" "proxy-server/chunk_10KB") ("chunk_100KB" "proxy-server/chunk_100KB")))) (r "1.74")))

(define-public crate-synchronous-server-0.1.12 (c (n "synchronous-server") (v "0.1.12") (d (list (d (n "proxy-server") (r "^0.6.5") (d #t) (k 0)))) (h "0n6dm68hdz0znadcd1jbm58mw8kyla1znx8j2q15bc4v8wxa281b") (f (quote (("napi" "proxy-server/napi") ("chunk_5KB" "proxy-server/chunk_5KB") ("chunk_50KB" "proxy-server/chunk_50KB") ("chunk_500KB" "proxy-server/chunk_500KB") ("chunk_1MB" "proxy-server/chunk_1MB") ("chunk_1KB" "proxy-server/chunk_1KB") ("chunk_10KB" "proxy-server/chunk_10KB") ("chunk_100KB" "proxy-server/chunk_100KB")))) (r "1.74")))

