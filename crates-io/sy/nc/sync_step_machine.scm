(define-module (crates-io sy nc sync_step_machine) #:use-module (crates-io))

(define-public crate-sync_step_machine-0.1.0 (c (n "sync_step_machine") (v "0.1.0") (h "0dhgxz4d6dpj7a5zqyzzj0iid5k1zjsikprir4yw7i90yv17dixs")))

(define-public crate-sync_step_machine-0.1.1 (c (n "sync_step_machine") (v "0.1.1") (h "038ziybklkx76irilp9s9svmxkflv9sxp58mqkqsbi24r788qz78")))

(define-public crate-sync_step_machine-0.1.2 (c (n "sync_step_machine") (v "0.1.2") (h "01i1v5mpx2lzjsghcyk61ph4amgma71dsmlyizvf8p4cmhvh05yb")))

