(define-module (crates-io sy nc sync_tools) #:use-module (crates-io))

(define-public crate-sync_tools-0.1.1 (c (n "sync_tools") (v "0.1.1") (d (list (d (n "byte_buffer") (r "^0.1.2") (d #t) (k 0)) (d (n "syncpool") (r "^0.1.5") (d #t) (k 0)))) (h "1pp8iw22q376grzf7bpwrrhck0lgfqc08nn8nxkh0lhidiwjg7n0")))

