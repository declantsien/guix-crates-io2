(define-module (crates-io sy nc syncell) #:use-module (crates-io))

(define-public crate-syncell-0.0.1 (c (n "syncell") (v "0.0.1") (h "089zx2f3cj3kfllmcilpxn0rfawpa1sydivvl27hyjhjxz38wbbk")))

(define-public crate-syncell-0.1.0 (c (n "syncell") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1vvsybnhaas5p7li81riqsmyc9xb706xmg5l40cv8c48irzifdja")))

