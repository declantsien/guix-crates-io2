(define-module (crates-io sy nc sync-async-runner) #:use-module (crates-io))

(define-public crate-sync-async-runner-0.1.0 (c (n "sync-async-runner") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3.5") (f (quote ("sink"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (f (quote ("sink"))) (d #t) (k 2)) (d (n "pin-project") (r "^0.4.20") (d #t) (k 0)))) (h "05mjjp96b96m2rpvnwfh2kdp0jk2v74xchbfr3zskvlx258f0pvz")))

