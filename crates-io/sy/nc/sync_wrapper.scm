(define-module (crates-io sy nc sync_wrapper) #:use-module (crates-io))

(define-public crate-sync_wrapper-0.1.0 (c (n "sync_wrapper") (v "0.1.0") (h "019p78xcmfjlp42kjmzg97rcpbr43qar3v2206pq2l9lz4mdfx3i")))

(define-public crate-sync_wrapper-0.1.1 (c (n "sync_wrapper") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 2)))) (h "1a59lwsw52d1a64l2y1m7npfw6xjvrjf96c5014g1b69lkj8yl90")))

(define-public crate-sync_wrapper-0.1.2 (c (n "sync_wrapper") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 2)))) (h "0q01lyj0gr9a93n10nxsn8lwbzq97jqd6b768x17c8f7v7gccir0") (f (quote (("futures" "futures-core"))))))

(define-public crate-sync_wrapper-1.0.0 (c (n "sync_wrapper") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 2)))) (h "037jwlka84cxwx6yzrd9aswmlpqi5508qnmdbj4njaaf3b0rai9q") (f (quote (("futures" "futures-core"))))))

(define-public crate-sync_wrapper-1.0.1 (c (n "sync_wrapper") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 2)))) (h "150k6lwvr4nl237ngsz8fj5j78k712m4bggrfyjsidllraz5l1m7") (f (quote (("futures" "futures-core"))))))

