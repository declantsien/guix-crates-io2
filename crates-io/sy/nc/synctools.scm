(define-module (crates-io sy nc synctools) #:use-module (crates-io))

(define-public crate-synctools-0.1.0 (c (n "synctools") (v "0.1.0") (h "0ljz2qpmbllm39wa8macn0md1lj2iww4n9y3a6jiymrhy92dnlz1")))

(define-public crate-synctools-0.1.1 (c (n "synctools") (v "0.1.1") (h "18wclqp2nyv2a8w6v3sq8nl6dfkkccyf78292sxj4mg4w7v5r67x")))

(define-public crate-synctools-0.2.0 (c (n "synctools") (v "0.2.0") (h "0qbijcl9nsv1jd09j1jvq4p6zvwlibdyiy87jxrzkbsdpsbpgxqw")))

(define-public crate-synctools-0.3.0 (c (n "synctools") (v "0.3.0") (h "0912xklypnb88crw2nb5yr03hk4mv1bpxmm901ai9mcdlxp3wbb4")))

(define-public crate-synctools-0.3.1 (c (n "synctools") (v "0.3.1") (h "1bzgk09ngl65ji46p4fr44d0xq0cqk2f55dayd8jyfgk4lirzd9r")))

(define-public crate-synctools-0.4.0 (c (n "synctools") (v "0.4.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "03jabrx0zlxqsbgkwfys45jday8m381sfylhnrxvvmvqyxzwnqcp") (y #t)))

(define-public crate-synctools-0.3.2 (c (n "synctools") (v "0.3.2") (h "1f31j9zz9yg272c493cv00a7yhcird9d1jddlvqf8hrfmwqr20kd")))

(define-public crate-synctools-0.3.3 (c (n "synctools") (v "0.3.3") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "loom") (r "^0.7") (d #t) (k 2)))) (h "1cpqjlyvvqlrl7fi7ymrnvj5jc4h4ycwz7pjvq5936zxkqil7p11")))

