(define-module (crates-io sy nc sync-unsafe-cell) #:use-module (crates-io))

(define-public crate-sync-unsafe-cell-0.1.0 (c (n "sync-unsafe-cell") (v "0.1.0") (h "1ncf892j943ajd32z1lwlsdgbki70xrw8prr7fn4gl2rcm2ffpwh")))

(define-public crate-sync-unsafe-cell-0.1.1 (c (n "sync-unsafe-cell") (v "0.1.1") (h "1d5y96bzhsz740vv2mzh11119nysd08f5l9c8g5rbh42afxfrsld")))

