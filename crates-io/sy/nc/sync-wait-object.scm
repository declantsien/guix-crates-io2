(define-module (crates-io sy nc sync-wait-object) #:use-module (crates-io))

(define-public crate-sync-wait-object-0.1.0 (c (n "sync-wait-object") (v "0.1.0") (d (list (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ri1148bybxmfrd5ddih3fxpiwpvh2npndal7i4kx70mmsmkypfl")))

(define-public crate-sync-wait-object-0.2.0 (c (n "sync-wait-object") (v "0.2.0") (d (list (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0h7qzf1pjsxwzjvcygn77x5a1nfcml0bgmfj5smd89a5g2j5mjvp")))

