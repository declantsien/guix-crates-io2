(define-module (crates-io sy nc sync_file) #:use-module (crates-io))

(define-public crate-sync_file-0.1.0 (c (n "sync_file") (v "0.1.0") (h "027yv54l40dan8r9bgvhbjxjvl8fc51rag70kyfg7grfzi0nxpqm")))

(define-public crate-sync_file-0.1.1 (c (n "sync_file") (v "0.1.1") (h "0aybsprm9pw00kckasyqqd30vrfkmfj9yfir2svldd6p3ad5g2nw")))

(define-public crate-sync_file-0.1.2 (c (n "sync_file") (v "0.1.2") (h "131zbd63znhk6l0zrzvqn44qj84cr6s4maskf4y0iycmm4hvw5mm")))

(define-public crate-sync_file-0.2.0 (c (n "sync_file") (v "0.2.0") (h "1y2jkn36vmw9qbsflf8ps36qqk3piljr7gw83hr1s8alr79741nw") (r "1.51")))

(define-public crate-sync_file-0.2.1 (c (n "sync_file") (v "0.2.1") (h "1cghypdx5vvvg2j1f37hl1a6sqln2v0mmz38slh3q79ibbd9zslf") (r "1.51")))

(define-public crate-sync_file-0.2.2 (c (n "sync_file") (v "0.2.2") (h "1w1zvphv18v7h3wp4bwf956gg7dbn2wkhxny4f1xp42fhc3brvz1") (r "1.51")))

(define-public crate-sync_file-0.2.3 (c (n "sync_file") (v "0.2.3") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "0yy8sv85zcr5s6hn4nzs4cj85y0k9rq7gzjd2hs1104n4rhh79kc") (r "1.56")))

(define-public crate-sync_file-0.2.4 (c (n "sync_file") (v "0.2.4") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "04r2i8is6nnkjc0gjvp096n1z18qjmnv3qrj9z16xy6dmy9vqigm") (r "1.56")))

(define-public crate-sync_file-0.2.5 (c (n "sync_file") (v "0.2.5") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "1rc3wgi2srzrdckhlxwlnw05b0qfa7wmgm6xwpzkkpvxh9hjn5rm") (r "1.56")))

(define-public crate-sync_file-0.2.6 (c (n "sync_file") (v "0.2.6") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "1cpdn2zq8k7mcr41jg4dvs9a0737cj97x6fvv4jw7y59bawklbpr") (r "1.56")))

(define-public crate-sync_file-0.2.7 (c (n "sync_file") (v "0.2.7") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "0iphw0axfgkxb6bp4ab0k265s5x36xscj0ivn7xxbhnr8pgp56x4") (r "1.63")))

(define-public crate-sync_file-0.2.8 (c (n "sync_file") (v "0.2.8") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "05pj13372d5vg0kd3jfll62lkkj1mqss8iivvg6lrcjaq1vwdvmv") (r "1.63")))

(define-public crate-sync_file-0.2.9 (c (n "sync_file") (v "0.2.9") (d (list (d (n "wasi") (r "^0.11") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "0kl7jnxxxxr4xyaym8xvbyf68ijxgxxasjii6pxjzr0kh8ij9z4w") (r "1.63")))

