(define-module (crates-io sy nc synchronized) #:use-module (crates-io))

(define-public crate-synchronized-1.0.0 (c (n "synchronized") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "1sdczj3ivxh8f1c7yn95nfn6l4if1hmfza30s7fa28s0bybhhb8m") (f (quote (("std") ("get_point_name") ("default" "std" "get_point_name"))))))

(define-public crate-synchronized-1.0.1 (c (n "synchronized") (v "1.0.1") (d (list (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0pq8flbrys9dp3pasw37jrw4hygqdcl0f8vy88aks4c5hmy83cwl") (f (quote (("std") ("get_point_name") ("default" "std" "get_point_name"))))))

(define-public crate-synchronized-1.0.2 (c (n "synchronized") (v "1.0.2") (d (list (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "04fxnszizlx9x8z9spwpjdvw6bczgygr1a83dv56l9jv4wk304xg") (f (quote (("std") ("point") ("get_point_name") ("default" "std" "get_point_name"))))))

(define-public crate-synchronized-1.0.3 (c (n "synchronized") (v "1.0.3") (d (list (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("parking_lot" "sync"))) (o #t) (k 0)))) (h "16zjikv25bx1ipl908n1ahv0zylirrqss1kdnfzfirf8mhga1jpy") (f (quote (("std") ("point") ("get_point_name") ("default" "std" "point" "get_point_name") ("async" "tokio" "async-trait"))))))

(define-public crate-synchronized-1.0.4 (c (n "synchronized") (v "1.0.4") (d (list (d (n "async-trait") (r "^0.1.58") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("parking_lot" "sync"))) (o #t) (k 0)))) (h "0596mv6fxcmdi9n50f4qjwgwh43la9d91wqgpgk739pnfb7fl8fd") (f (quote (("std") ("point") ("get_point_name") ("default" "std" "point" "get_point_name") ("async" "tokio" "async-trait"))))))

