(define-module (crates-io sy nc synco) #:use-module (crates-io))

(define-public crate-synco-0.0.0 (c (n "synco") (v "0.0.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "specs") (r "^0.16") (d #t) (k 2)))) (h "1qw8kd1718wkwagn0l52dys3w2v3iivsi8r0h1myc428i7dgif3w")))

