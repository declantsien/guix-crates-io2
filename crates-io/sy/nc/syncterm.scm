(define-module (crates-io sy nc syncterm) #:use-module (crates-io))

(define-public crate-syncterm-0.0.1 (c (n "syncterm") (v "0.0.1") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.2") (d #t) (k 2)))) (h "13gczd7hj9vyqsngciafbnhc6jrfgr2lm7rx7d4q7vv4hfgl8im2")))

