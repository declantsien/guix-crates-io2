(define-module (crates-io sy nc sync_stream_struct_proc) #:use-module (crates-io))

(define-public crate-sync_stream_struct_proc-0.1.0 (c (n "sync_stream_struct_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1kv1gvcfs1s7b1ma380gyqlqqh7fk4fxipdf33576r5q0apdpkm3")))

