(define-module (crates-io sy nc syncme) #:use-module (crates-io))

(define-public crate-syncme-0.1.0 (c (n "syncme") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("once_cell_cache"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "118sf7rrxqc4rvg1z2ibs3id2gwk6w3sdg7pnsqrlqj4jsrz03r0")))

