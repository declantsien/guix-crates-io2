(define-module (crates-io sy nc syncmers) #:use-module (crates-io))

(define-public crate-syncmers-0.1.0 (c (n "syncmers") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 2)) (d (n "pulp") (r "^0.4.0") (d #t) (k 2)))) (h "11jihf06ifr6wkjb2ic7yr1hmydzk77rcfh8cdjbi390f9qg0nr3")))

(define-public crate-syncmers-0.1.1 (c (n "syncmers") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 2)) (d (n "pulp") (r "^0.4.0") (d #t) (k 0)))) (h "1kazxk65y6lha8554y29yzb9wd4s5ji65csrl06ryps0ppb633kz")))

(define-public crate-syncmers-0.1.3 (c (n "syncmers") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 2)) (d (n "pulp") (r "^0.4.0") (d #t) (k 0)))) (h "1qmpxlfhv5cqn1vypk4bha9dv809q662hs3a1h7akkwr8kb15f4m")))

(define-public crate-syncmers-0.1.4 (c (n "syncmers") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 2)) (d (n "pulp") (r "^0.4.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "0iyhjjd29v9ir6q2sw0fp4gz9yvckks1q3gca1pcwqpj89mnqha2")))

(define-public crate-syncmers-0.1.5 (c (n "syncmers") (v "0.1.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 2)) (d (n "pulp") (r "^0.10.4") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "05avbc0ks4ghghhllbwh7vn552g6jf8k1zzvjw6966fhpfj1my97")))

