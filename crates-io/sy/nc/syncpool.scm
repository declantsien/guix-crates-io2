(define-module (crates-io sy nc syncpool) #:use-module (crates-io))

(define-public crate-syncpool-0.1.0 (c (n "syncpool") (v "0.1.0") (h "04lr9agsly3810x1f04b1swywzfivkqvfk7hdq4wh9a8cappirp4")))

(define-public crate-syncpool-0.1.1 (c (n "syncpool") (v "0.1.1") (h "0gz3aq64ax6vax6d899s6zx62rlckn7wrx2mg27x48a363cr0fsd")))

(define-public crate-syncpool-0.1.2 (c (n "syncpool") (v "0.1.2") (h "167dp2mzndv7g3zxmhsq857ziqxldgdj92z6ml8by2fzy1y47714")))

(define-public crate-syncpool-0.1.3 (c (n "syncpool") (v "0.1.3") (h "0d9qj879rhqak1q6yv7ksf31gdkn4mnajgyklnaj86v8bf4z7w6s")))

(define-public crate-syncpool-0.1.4 (c (n "syncpool") (v "0.1.4") (h "1zq338h4kzsfi625lfxxyssx9wkc5j3vybd4s1wwkcl591kxi6hr")))

(define-public crate-syncpool-0.1.5 (c (n "syncpool") (v "0.1.5") (h "0w5zc0d0kbykksr4w70vy387qnyvxnf5g1fah75fyws87i9qjp4p")))

(define-public crate-syncpool-0.1.6 (c (n "syncpool") (v "0.1.6") (h "1gf6f420by1dr7hngaf0534bkzddczwq23irryyh7ba11z01yv17")))

