(define-module (crates-io sy nc syncbox) #:use-module (crates-io))

(define-public crate-syncbox-0.1.0 (c (n "syncbox") (v "0.1.0") (h "1hf3sgjlj5flj0hrz2kkhzwsys891i5prnmdv7hi8njgimz521y5")))

(define-public crate-syncbox-0.2.0 (c (n "syncbox") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 2)))) (h "1q2cik8bfvs2sbx5fhbsl61fwzqaablwvyhfjnllmjzddgikghlx")))

(define-public crate-syncbox-0.2.1 (c (n "syncbox") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 0)))) (h "0fagaa0k0i582ds2afgqfbjj7c3l8srz9zwgg0l3idy0l86k2hxb")))

(define-public crate-syncbox-0.2.2 (c (n "syncbox") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 0)))) (h "14xl3922fmi8clh7x72hxlmksk4fdnli0l3i4b50ppgrlv8h7scy")))

(define-public crate-syncbox-0.2.3 (c (n "syncbox") (v "0.2.3") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.21") (d #t) (k 0)))) (h "1jdjyz9pmnj4cmwa06yhhka0khbricl21dswr08yrhg7nak9i60y")))

(define-public crate-syncbox-0.2.4 (c (n "syncbox") (v "0.2.4") (d (list (d (n "env_logger") (r "^0.3.1") (d #t) (k 2)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "1iw57d91zj0jmgg6gxs19iy1jy35b746chbw1qnpmhlscmr2pg05")))

