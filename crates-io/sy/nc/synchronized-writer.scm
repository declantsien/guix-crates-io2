(define-module (crates-io sy nc synchronized-writer) #:use-module (crates-io))

(define-public crate-synchronized-writer-1.0.0 (c (n "synchronized-writer") (v "1.0.0") (h "18bpwlggw7d99mafv6l6nsias2w3yn2xzyqipv44x9g2p3qqaqan")))

(define-public crate-synchronized-writer-1.1.0 (c (n "synchronized-writer") (v "1.1.0") (h "1dwnx4y4jfyxi7q1cdff397925qgxmg3v8g6prdfxqxkxv4vkvj5")))

(define-public crate-synchronized-writer-1.1.1 (c (n "synchronized-writer") (v "1.1.1") (h "0z0rfgkyyp0i237nnk5iwcvi509232dwswwgqvz2x3p7w3fcxsib")))

(define-public crate-synchronized-writer-1.1.2 (c (n "synchronized-writer") (v "1.1.2") (h "14brwz9ykk5azff0balpqmcg8sfswhidk96g63g8igphzxczs79c")))

(define-public crate-synchronized-writer-1.1.3 (c (n "synchronized-writer") (v "1.1.3") (h "0k58i006rf1h7c9q0nk5hy7pnd4ks8vpadw3rin9az1wyz7ni8d1")))

(define-public crate-synchronized-writer-1.1.4 (c (n "synchronized-writer") (v "1.1.4") (h "1d88i5hc1blcqa5hymc3q8jal6n0bm7nflm1sqc5y8yh0isqh77h")))

(define-public crate-synchronized-writer-1.1.5 (c (n "synchronized-writer") (v "1.1.5") (h "1qa8cwd93rrxj4jmq75alxb5v3lx8l5a0l40k7fw77kia70p5h4p")))

(define-public crate-synchronized-writer-1.1.6 (c (n "synchronized-writer") (v "1.1.6") (h "0gmfcarb5dlqhlfc2jm61pnrvlzxzmbzjfkcnszdjhng70cc1f73")))

(define-public crate-synchronized-writer-1.1.7 (c (n "synchronized-writer") (v "1.1.7") (h "1r0fsvsf15aqvkx0mf0qrcl2yx3ckd6v0m8ndg8v93rw5z15gjlq")))

(define-public crate-synchronized-writer-1.1.8 (c (n "synchronized-writer") (v "1.1.8") (h "1k879aq6wc0sd7kmq6mldagb52cagxvv9dfg273afg8iqhs98w4b")))

(define-public crate-synchronized-writer-1.1.9 (c (n "synchronized-writer") (v "1.1.9") (h "1k9pssvq70w3byqi4ak1hqq69wwksivxz4b6lz9l6cv2a4v73frz")))

(define-public crate-synchronized-writer-1.1.10 (c (n "synchronized-writer") (v "1.1.10") (h "0k88nry4y061s6wicd56vhv3ycbkbdg4m8ln17xzycc0aw3nd22z")))

(define-public crate-synchronized-writer-1.1.11 (c (n "synchronized-writer") (v "1.1.11") (h "18m480g5i9mzci8jr8j24qcqp6937xjxbkdxa9q7cw8fh6h3qm6k")))

