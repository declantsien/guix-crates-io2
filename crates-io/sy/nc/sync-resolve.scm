(define-module (crates-io sy nc sync-resolve) #:use-module (crates-io))

(define-public crate-sync-resolve-0.3.0 (c (n "sync-resolve") (v "0.3.0") (d (list (d (n "idna") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "19yp9950f9846557nd1hq4klmj9q5xx4wv28dhf52mzx6yggm2bs")))

(define-public crate-sync-resolve-0.3.1 (c (n "sync-resolve") (v "0.3.1") (d (list (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1y028zk8krp1w10ijhpdnc62ki2apmba08m6z3c8q7bpgjshwx0b")))

