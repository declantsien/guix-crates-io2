(define-module (crates-io sy nc syncat-stylesheet) #:use-module (crates-io))

(define-public crate-syncat-stylesheet-0.1.0 (c (n "syncat-stylesheet") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.3") (d #t) (k 0)))) (h "0jmlf0sywb2789wg0arji8mnl22rljlkpwbl5csagpjk3ddg553y")))

(define-public crate-syncat-stylesheet-0.2.0 (c (n "syncat-stylesheet") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.3") (d #t) (k 0)))) (h "0748clafsxs49hx3bx62fwrqcimz6yrs2aybn40jj9xzvnkjjjr4")))

(define-public crate-syncat-stylesheet-0.2.1 (c (n "syncat-stylesheet") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.3") (d #t) (k 0)))) (h "0y80cjf8aqizlysyrawj1s7yx76l4v1l8sw40azy9w22cn07wlfk")))

(define-public crate-syncat-stylesheet-0.2.2 (c (n "syncat-stylesheet") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.3") (d #t) (k 0)))) (h "0wji3br6mf8gf3si7j170gj3fx5ky6viisq4kp6v05nj5rvax4kg")))

(define-public crate-syncat-stylesheet-2.0.0 (c (n "syncat-stylesheet") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "04xg2ch192z8bsw8lf1nxy52ljyggvrc1xcakq09dammdblqrv6w")))

(define-public crate-syncat-stylesheet-2.0.1 (c (n "syncat-stylesheet") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "11a7ncql1hh8nx28cm18qw6kh5ls7jr438vzc4ym8ldqhlwi01n4")))

(define-public crate-syncat-stylesheet-2.1.1 (c (n "syncat-stylesheet") (v "2.1.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "18x9wyjcnn46qzjcdqds4v42lff8n14shck4581ks5c44yflnsyq")))

(define-public crate-syncat-stylesheet-2.1.2 (c (n "syncat-stylesheet") (v "2.1.2") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "0jq6bv07cdy7b7acdbghzrix1dn7d7h1pdd3gyki7gdnfyqfdffn")))

(define-public crate-syncat-stylesheet-2.1.3 (c (n "syncat-stylesheet") (v "2.1.3") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "0i2gv7dy2zs1g7brfzgb7fwhs41c16yh7awrigdg8lb2d3qlrqbw")))

(define-public crate-syncat-stylesheet-2.1.4 (c (n "syncat-stylesheet") (v "2.1.4") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "0bxh62n4mp6a4kj752vnl899aslg1d7wznrb51594gbq789xi70n")))

(define-public crate-syncat-stylesheet-2.1.5 (c (n "syncat-stylesheet") (v "2.1.5") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "0mbszcbdpv4hivalx440z6lqq3d8km3c53l39qnibmzzlsz46hjf")))

(define-public crate-syncat-stylesheet-2.2.0 (c (n "syncat-stylesheet") (v "2.2.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6") (d #t) (k 0)))) (h "1h0a4i3dpsy4bhyhfffgiilc669zaxcz74vy4zazq012jw8l93fm")))

(define-public crate-syncat-stylesheet-2.2.1 (c (n "syncat-stylesheet") (v "2.2.1") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)))) (h "1m11dkkrz8zwnxkra56s1lwh5bkc9x0dwk8qgw6dc18k43ggvg9a")))

(define-public crate-syncat-stylesheet-2.3.0 (c (n "syncat-stylesheet") (v "2.3.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1r23gg78j5wzvjx3hrm6chrpbg00k2ab1002l7bd0575gdfgg8ay")))

(define-public crate-syncat-stylesheet-2.4.0 (c (n "syncat-stylesheet") (v "2.4.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "13nhlx9icdd0zcqnfwyciwlyhf9aygkl4crb6km25129ywx79gqi")))

(define-public crate-syncat-stylesheet-2.5.0 (c (n "syncat-stylesheet") (v "2.5.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "03bpf263vvr001fdsn1mvqwcqgmr24d8jnyqmc5v913lrl5ccfkn") (y #t)))

(define-public crate-syncat-stylesheet-3.5.0 (c (n "syncat-stylesheet") (v "3.5.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0aggcdy5ichfn0hikydr5y0j87mg7r584a59zwd8lz7a1mpd82ls")))

