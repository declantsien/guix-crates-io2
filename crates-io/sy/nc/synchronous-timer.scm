(define-module (crates-io sy nc synchronous-timer) #:use-module (crates-io))

(define-public crate-synchronous-timer-0.1.0 (c (n "synchronous-timer") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1psrq3k4hyqy78g21ifr27bkp42y3lcccb7c9jmcc1w9a0cqxnhn")))

