(define-module (crates-io sy nc sync-wait-group) #:use-module (crates-io))

(define-public crate-sync-wait-group-0.1.0 (c (n "sync-wait-group") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "081vjsnzbiizi6m5lk32gs6k9d9klkkwgv2c2jszjrb0db0yzy12") (r "1.56")))

(define-public crate-sync-wait-group-0.1.1 (c (n "sync-wait-group") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0mr5i7fmsksr2xknprw6kb1rq5qax8ix7x3bfla31ix9y1hbviqs") (r "1.56")))

