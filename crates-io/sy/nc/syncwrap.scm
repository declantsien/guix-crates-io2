(define-module (crates-io sy nc syncwrap) #:use-module (crates-io))

(define-public crate-syncwrap-0.1.0 (c (n "syncwrap") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (d #t) (k 0)))) (h "0bpgh9m625qd4n9kmn84jxwmqr1bb976lvkmn52y1kmpdf51ch3y")))

(define-public crate-syncwrap-0.2.0 (c (n "syncwrap") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0jq49h61lhqsf4ifq2xlsrxp8agg8f89wqpf02lyklqj9mz0nn7n") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-syncwrap-0.2.1 (c (n "syncwrap") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjqdksb5vizc1i78rqnna8djwir9fqx80c2i7harynpr1v187m9") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-syncwrap-0.2.2 (c (n "syncwrap") (v "0.2.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "195rmrpwf08rddn08cr54fy0av6ifcjs9fy1wia81b0x5w20cxn8") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-syncwrap-0.3.0 (c (n "syncwrap") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0bbv02a779mf90ip6aaqimmhjjvjg48jv2j2pmb69x21j8lzgi9m") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-syncwrap-0.4.0 (c (n "syncwrap") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)))) (h "13mvwdwpch1s2vliwgfscb8ipr7377pmip67hjai2iq0r4sh368l") (f (quote (("sync") ("default" "sync"))))))

