(define-module (crates-io sy nc sync_2) #:use-module (crates-io))

(define-public crate-sync_2-1.0.0 (c (n "sync_2") (v "1.0.0") (d (list (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1pabpqkz6ag11jh5ragzbpf4h0jwksmrwac76y55w70s8lw4vzgz")))

(define-public crate-sync_2-1.0.1 (c (n "sync_2") (v "1.0.1") (d (list (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0s860gv591bfr7kr5y1k76d87c4c9h4vpf99j7g58g062ddr41w2")))

(define-public crate-sync_2-1.0.2 (c (n "sync_2") (v "1.0.2") (d (list (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "15yzfa6ss0c6n71sfa37mprmnysfjg4ca211gzwxpx8siscbwc6y")))

(define-public crate-sync_2-1.0.3 (c (n "sync_2") (v "1.0.3") (d (list (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0yvf791v15qyjxs7cw95w9xp6pjx1s4q668f2k8gxjvg4pgnxsla") (y #t)))

(define-public crate-sync_2-1.0.4 (c (n "sync_2") (v "1.0.4") (d (list (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "0rp7fbpf7iwhn0gshrfgjid7dq6s99y8g5b69bdimaws5r8il4cj")))

