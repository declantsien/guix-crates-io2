(define-module (crates-io sy nc sync_cow) #:use-module (crates-io))

(define-public crate-sync_cow-0.1.0 (c (n "sync_cow") (v "0.1.0") (h "0a0zq9h139xrrzw7k8ag3q91l8h8h10fb2cj84rmsndb8v4wafiw")))

(define-public crate-sync_cow-0.1.1 (c (n "sync_cow") (v "0.1.1") (h "0k3dhxjgnm5nmzd1irg8yi3g0yf5fyhh4ascgc3chfq2n9gxnzjh")))

