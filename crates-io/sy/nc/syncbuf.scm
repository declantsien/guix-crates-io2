(define-module (crates-io sy nc syncbuf) #:use-module (crates-io))

(define-public crate-syncbuf-0.1.0 (c (n "syncbuf") (v "0.1.0") (h "10c0v0sfywg930gv447k4aak1g72rmdmkgbrz6whsaiahc7lnaxa")))

(define-public crate-syncbuf-0.2.0 (c (n "syncbuf") (v "0.2.0") (h "16aim5ykxyq5509s65s22wlngpqdk6cb7v7vfwwvkhvd9d4d5q30")))

