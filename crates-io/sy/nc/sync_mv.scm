(define-module (crates-io sy nc sync_mv) #:use-module (crates-io))

(define-public crate-sync_mv-0.1.0 (c (n "sync_mv") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 2)))) (h "0yzpsgzpdz73dfz7c2i15741dkdvm60gn8ix5c868w9i6m1fz5gn")))

(define-public crate-sync_mv-0.1.1 (c (n "sync_mv") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 2)))) (h "0n2xf86aji13m6423032jknq6lfqs9z431wi7ryj4q4r0wv2yk68")))

(define-public crate-sync_mv-0.1.2 (c (n "sync_mv") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 2)))) (h "17gsx73d8z3fapqcfz4dryavpgjn60407ch7fig6h7lp4hd02nch")))

(define-public crate-sync_mv-0.1.3 (c (n "sync_mv") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 2)))) (h "1dfarzwn3j10f8k1y9lnm6jvinm4xk0f7z26fxxy3ay20gwb2ach")))

