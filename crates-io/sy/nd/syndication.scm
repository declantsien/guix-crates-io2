(define-module (crates-io sy nd syndication) #:use-module (crates-io))

(define-public crate-syndication-0.1.0 (c (n "syndication") (v "0.1.0") (d (list (d (n "atom_syndication") (r "^0.1") (d #t) (k 0)) (d (n "rss") (r "^0.2") (d #t) (k 0)))) (h "0bar1kph3rx73wwqgwlwd364vs65wkcm4pqmyapcz8a1f1c6xsx4")))

(define-public crate-syndication-0.1.1 (c (n "syndication") (v "0.1.1") (d (list (d (n "atom_syndication") (r "^0.1") (d #t) (k 0)) (d (n "rss") (r "^0.2") (d #t) (k 0)))) (h "1b3v1i9pqvqzykm1r2vp5p537c2i13gf3svgbpzdkzjw810r5g2a")))

(define-public crate-syndication-0.2.0 (c (n "syndication") (v "0.2.0") (d (list (d (n "atom_syndication") (r "^0.1") (d #t) (k 0)) (d (n "rss") (r "^0.3") (d #t) (k 0)))) (h "0pcvj0rhikdvk91a7jd8k7w4g343nnvxd6p38jrmlhrr8l5r6kr7")))

(define-public crate-syndication-0.3.0 (c (n "syndication") (v "0.3.0") (d (list (d (n "atom_syndication") (r "^0.2") (d #t) (k 0)) (d (n "rss") (r "^0.3") (d #t) (k 0)))) (h "12myi4kap4w5viqkwcr610y5ss1wfklj2sabwlxqh0nvqpgfn38v")))

(define-public crate-syndication-0.4.0 (c (n "syndication") (v "0.4.0") (d (list (d (n "atom_syndication") (r "^0.3") (d #t) (k 0)) (d (n "rss") (r "^0.3") (d #t) (k 0)))) (h "1zjrklxp8kwvjidcl4k3y7v0bz85pimddaw743wrkd11c3239jyh")))

(define-public crate-syndication-0.5.0 (c (n "syndication") (v "0.5.0") (d (list (d (n "atom_syndication") (r "^0.6.0") (d #t) (k 0)) (d (n "rss") (r "^1.8.0") (d #t) (k 0)))) (h "0limdlvvv4pb2bxz46rgmnvf578gdlf6j0zwfjfgwja2ll0zhhf4")))

