(define-module (crates-io sy nd syndicate-schema-plugin) #:use-module (crates-io))

(define-public crate-syndicate-schema-plugin-0.1.0 (c (n "syndicate-schema-plugin") (v "0.1.0") (d (list (d (n "preserves-schema") (r "^4.992.0") (d #t) (k 0)) (d (n "syndicate") (r "^0.30.0") (d #t) (k 0)))) (h "0m8d60fi46hf98mqmim415zh75czg8qzcb474h423q4s0vn6f2jb")))

(define-public crate-syndicate-schema-plugin-0.2.0 (c (n "syndicate-schema-plugin") (v "0.2.0") (d (list (d (n "preserves-schema") (r "^4.992.0") (d #t) (k 0)) (d (n "syndicate") (r "^0.30.0") (d #t) (k 0)))) (h "0svff1nswn7b0dld1c7ymx15lzhxy4vzys1vddma2qyh66fqn8r0")))

(define-public crate-syndicate-schema-plugin-0.2.1 (c (n "syndicate-schema-plugin") (v "0.2.1") (d (list (d (n "preserves-schema") (r "^5.992") (d #t) (k 0)) (d (n "syndicate") (r "^0.31.0") (d #t) (k 0)))) (h "004ci0sjx838bf1ngk869r8qpzzf1v5i92qdzmlcgqz649703dn3")))

(define-public crate-syndicate-schema-plugin-0.3.0 (c (n "syndicate-schema-plugin") (v "0.3.0") (d (list (d (n "preserves-schema") (r "^5.993") (d #t) (k 0)) (d (n "syndicate") (r "^0.33.0") (d #t) (k 0)))) (h "0k9br4shhgm25m0ah4qqbnw8h9g9ma0vr6gp3211qg9r1llkfysw")))

(define-public crate-syndicate-schema-plugin-0.4.0 (c (n "syndicate-schema-plugin") (v "0.4.0") (d (list (d (n "preserves-schema") (r "^5.994") (d #t) (k 0)) (d (n "syndicate") (r "^0.34.0") (d #t) (k 0)))) (h "105dhg2isq1k4914s4cp5syz3kbzyyd4r5ksr0ihncbv2dym84v4")))

(define-public crate-syndicate-schema-plugin-0.5.0 (c (n "syndicate-schema-plugin") (v "0.5.0") (d (list (d (n "preserves-schema") (r "^5.994") (d #t) (k 0)) (d (n "syndicate") (r "^0.35.0") (d #t) (k 0)))) (h "0scmxlz8ld049i0xixlakak9i9hw46521r095vayfhdvbz2f73d9")))

(define-public crate-syndicate-schema-plugin-0.6.0 (c (n "syndicate-schema-plugin") (v "0.6.0") (d (list (d (n "preserves-schema") (r "^5.995") (d #t) (k 0)) (d (n "syndicate") (r "^0.36.0") (d #t) (k 0)))) (h "1vhz1yh4kgihdrqw4ckwj9xsrvv91f08aw7zsqbyg3wv4lwm83d9")))

(define-public crate-syndicate-schema-plugin-0.7.0 (c (n "syndicate-schema-plugin") (v "0.7.0") (d (list (d (n "preserves-schema") (r "^5.995") (d #t) (k 0)) (d (n "syndicate") (r "^0.37.0") (d #t) (k 0)))) (h "0g7wz758iryrgc058r2hnd3nxckdpxip3938cqg2vmi586szw3rd")))

(define-public crate-syndicate-schema-plugin-0.8.0 (c (n "syndicate-schema-plugin") (v "0.8.0") (d (list (d (n "preserves-schema") (r "^5.995") (d #t) (k 0)) (d (n "syndicate") (r "^0.39.0") (d #t) (k 0)))) (h "0kc7n2rmrj6zbv91vrh7aydp4ia5vq5yady0dx7d8sm9wrnf5y4c")))

(define-public crate-syndicate-schema-plugin-0.9.0 (c (n "syndicate-schema-plugin") (v "0.9.0") (d (list (d (n "preserves-schema") (r "^5.995") (d #t) (k 0)) (d (n "syndicate") (r "^0.40.0") (d #t) (k 0)))) (h "1fryyi68svapv2d4l2s2s6sm2lrahqjr5vzsl49fw2cad1jmdpqx")))

