(define-module (crates-io sy nd syndey) #:use-module (crates-io))

(define-public crate-syndey-0.1.0 (c (n "syndey") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "germ") (r "^0.3.3") (f (quote ("request" "ast"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0s939wj4sjjhxppq1vjdiz5bbzwlyl24zgbx5mvkd03n54fy3wbj") (y #t)))

