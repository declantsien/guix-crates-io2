(define-module (crates-io sy nd synd_authn) #:use-module (crates-io))

(define-public crate-synd_authn-0.1.1 (c (n "synd_authn") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-serde-ext") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1n13azx6l2li6x76r489rrdgm3g8hwn6hr63dplijcgsbf3f2hfs") (y #t)))

