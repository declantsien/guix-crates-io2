(define-module (crates-io sy su sysutil) #:use-module (crates-io))

(define-public crate-sysutil-0.1.0 (c (n "sysutil") (v "0.1.0") (h "0bcyixhfn9vmnym8x69dwrij24y8905ppwz2pai17827b95gcqq9")))

(define-public crate-sysutil-0.1.1 (c (n "sysutil") (v "0.1.1") (h "07fid7s3c56ln66iglvdpy9d7aw7lv6xmpvg48zmq821c3fixpz0")))

(define-public crate-sysutil-0.1.2 (c (n "sysutil") (v "0.1.2") (h "0y0ai6ilys6ys2s0wkm5zyp5hf1xi3ybjs2xagczizplcwdndmyn")))

(define-public crate-sysutil-0.2.1 (c (n "sysutil") (v "0.2.1") (h "0qbn031ygphv1byc5ypxixygbavp25f5z7w4lw28288y88f155r6")))

(define-public crate-sysutil-0.2.2 (c (n "sysutil") (v "0.2.2") (h "0c9xpy3h4ih01wfyapqswb4wpvsnn7x51d96iik6isnbpw9bwd5v")))

(define-public crate-sysutil-0.2.3 (c (n "sysutil") (v "0.2.3") (h "12vb7ndb60x4l4bd5zpz896yfinjd147d4gkp7vg7h1ccivaa1nk")))

(define-public crate-sysutil-0.2.4 (c (n "sysutil") (v "0.2.4") (h "0sp1ixilnhqprsia3ppaph7jf1lpzd0grqgaj3whfyhcz2mrcx92")))

(define-public crate-sysutil-0.2.5 (c (n "sysutil") (v "0.2.5") (h "1jjij9i42d0gy4452n08a9ppgwl1hcs1v6f9xk6jz4afc0rkzprs")))

(define-public crate-sysutil-0.2.6 (c (n "sysutil") (v "0.2.6") (h "010xjzyn6lwy86v9qh22246i0axbfsd8d09ady6pfz423461id9y")))

(define-public crate-sysutil-0.3.0 (c (n "sysutil") (v "0.3.0") (h "0nkfwv4ik8c7zw9nwdv0ynv2panar5jvlwrr9mrnxljnk5djy4ab")))

(define-public crate-sysutil-0.3.1 (c (n "sysutil") (v "0.3.1") (h "0np5nydy0wvbvqm2rnm7fv0644yq8sp8hrj5ny6g0hmzi872zknn")))

(define-public crate-sysutil-0.3.2 (c (n "sysutil") (v "0.3.2") (h "1l0r9i8gbr9wfa2nndnmjd555i3q297xfq40gfl7zyiwwshch1ba")))

(define-public crate-sysutil-0.3.3 (c (n "sysutil") (v "0.3.3") (h "0nxm1375rsspw95jcgqgifcs4qcm8kh8svsajm0d4rzv18z9ppc7")))

(define-public crate-sysutil-0.4.0 (c (n "sysutil") (v "0.4.0") (h "0v5m87ycw5292l8l737y0x280qrn4hzrg1fsszi7hd086i3kb3y1")))

(define-public crate-sysutil-0.4.1 (c (n "sysutil") (v "0.4.1") (h "0r3zmf56pwbfd6w2x7d25ic9ilbhn9cv3ziplhmprjkbyrh23hc8")))

(define-public crate-sysutil-0.4.2 (c (n "sysutil") (v "0.4.2") (h "1dph6akjg45p230ajapdzhndaq48jxi4czfznv5ldz94k0fahfq2")))

(define-public crate-sysutil-0.4.3 (c (n "sysutil") (v "0.4.3") (h "19bncfqijxwjjlh3p6kahmh1hbvw11bl4i6h6f6fvgyi7rdq6n86")))

(define-public crate-sysutil-0.4.4 (c (n "sysutil") (v "0.4.4") (d (list (d (n "rsjson") (r "^0.3.6") (d #t) (k 0)))) (h "02lyiv1wgxp492p1sl5f96cpm67pxrs0n1yirxz38ixrf34qzsln")))

