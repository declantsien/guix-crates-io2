(define-module (crates-io sy mm symmetric-interaction-calculus) #:use-module (crates-io))

(define-public crate-symmetric-interaction-calculus-0.1.0 (c (n "symmetric-interaction-calculus") (v "0.1.0") (h "0542dbpfy1gj1jdhyvxscsj5aya1q3x7p0znzc5vmvvdan16qg99")))

(define-public crate-symmetric-interaction-calculus-0.1.1 (c (n "symmetric-interaction-calculus") (v "0.1.1") (h "0hcghdyl5aw9nsk6wykgf9pq7k6hi1l9iqw7068srcfz5sfcnnk4")))

(define-public crate-symmetric-interaction-calculus-0.1.2 (c (n "symmetric-interaction-calculus") (v "0.1.2") (h "0dnzwx01biwkh9an626nhr3zy8m1qj5hxiz22lf7k97hwajvfi9p")))

(define-public crate-symmetric-interaction-calculus-0.1.3 (c (n "symmetric-interaction-calculus") (v "0.1.3") (h "0ysmx8j6i5l1k5fnnf8k916jqvyc2ad4r4iyh05lsk975mj5k75b")))

(define-public crate-symmetric-interaction-calculus-0.1.4 (c (n "symmetric-interaction-calculus") (v "0.1.4") (h "0qjyw3wwr13fga7p9ypilvd0djc6b6nzp96l2pj8r1wr8kf1ynqx")))

(define-public crate-symmetric-interaction-calculus-0.1.5 (c (n "symmetric-interaction-calculus") (v "0.1.5") (h "08mr80baq1xjysn9hayis3jc7dii6bn38y4p73x4dql66a23x8z0")))

(define-public crate-symmetric-interaction-calculus-0.1.6 (c (n "symmetric-interaction-calculus") (v "0.1.6") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)))) (h "04w97hcg2hb3lf9jj64b9lj751jf5gvvqqsbv5n46wk7r362ij1d")))

(define-public crate-symmetric-interaction-calculus-0.1.7 (c (n "symmetric-interaction-calculus") (v "0.1.7") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)))) (h "1z4wh4a3q980chykr5nhdj6vqash5yh3gllc568shvcnbkx4k6z0")))

(define-public crate-symmetric-interaction-calculus-0.1.8 (c (n "symmetric-interaction-calculus") (v "0.1.8") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)))) (h "0g8qlx88r4gsm2w987wh0a455inzaqa8raqjgxhwb3yz4xw8z0nd")))

