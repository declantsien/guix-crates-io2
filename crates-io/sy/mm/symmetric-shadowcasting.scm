(define-module (crates-io sy mm symmetric-shadowcasting) #:use-module (crates-io))

(define-public crate-symmetric-shadowcasting-0.1.0 (c (n "symmetric-shadowcasting") (v "0.1.0") (d (list (d (n "num-rational") (r "^0.3") (k 0)))) (h "0wn0rwf80lppzqd70vn771pnkj03c247dj0cndvxcmzz882vfi8p")))

(define-public crate-symmetric-shadowcasting-0.1.1 (c (n "symmetric-shadowcasting") (v "0.1.1") (d (list (d (n "num-rational") (r "^0.3") (k 0)))) (h "18m46yhlpxnqgglmfyiyr31zx4yqqi8kdy8gzg8xhlpny9h5d3sv")))

(define-public crate-symmetric-shadowcasting-0.2.0 (c (n "symmetric-shadowcasting") (v "0.2.0") (d (list (d (n "num-rational") (r "^0.3") (k 0)))) (h "1j465vfihvx2y89w8gdz9mkrdhb89g659pjcwhglknq57ca2xq5k")))

