(define-module (crates-io sy mm symmetrical-spork) #:use-module (crates-io))

(define-public crate-symmetrical-spork-0.1.0 (c (n "symmetrical-spork") (v "0.1.0") (h "1yybpg5ng7jsmvwrwa387f7pjxr5qmhijvypq3x2m9r0fblyk2xc") (f (quote (("two") ("one"))))))

(define-public crate-symmetrical-spork-0.1.1 (c (n "symmetrical-spork") (v "0.1.1") (h "1z1n6samf7ql7f8jzkb15mjbk53khkq0z5m9hscr05snwk5vgmqh") (f (quote (("two") ("one"))))))

(define-public crate-symmetrical-spork-0.1.2 (c (n "symmetrical-spork") (v "0.1.2") (h "0yza2hhb2mr46ij6gr0j4vymci4pd13lvx79v6p7v9fkrw9myikz") (f (quote (("two") ("one"))))))

