(define-module (crates-io sy mm symm_impl) #:use-module (crates-io))

(define-public crate-symm_impl-0.1.0 (c (n "symm_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0l79zzivqj6zhy8rjxc5nfar66xpa8iyrjvnbd1a6nfz0havahk1")))

(define-public crate-symm_impl-0.1.1 (c (n "symm_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02kqfgbalp55ynnvc50c24aak7akfzdqzi1h7jacx3fhdbx9b34i")))

(define-public crate-symm_impl-0.1.2 (c (n "symm_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k5165889vnb6dy4h829jv7cs06yhbd71ippf2g39djx3sd7g3lk")))

