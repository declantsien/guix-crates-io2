(define-module (crates-io sy bo sybot) #:use-module (crates-io))

(define-public crate-sybot-0.9.0-alpha (c (n "sybot") (v "0.9.0-alpha") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.12.1-alpha") (d #t) (k 0)) (d (n "syunit") (r "^0.1.5") (d #t) (k 0)))) (h "02ak87pzvxdlkbadicjzvrhcld0b99kj95ah3dc67isb7cas1jg3") (y #t)))

(define-public crate-sybot-0.10.0 (c (n "sybot") (v "0.10.0") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.13.1") (d #t) (k 0)) (d (n "syunit") (r "^0.1.5") (d #t) (k 0)))) (h "15zwm8l4l8p9s69kijiwnyiw6sa06ifrbdph3s2shrl35szl6jy8") (y #t)))

(define-public crate-sybot-0.10.1 (c (n "sybot") (v "0.10.1") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.13.1") (d #t) (k 0)) (d (n "syunit") (r "^0.1.5") (d #t) (k 0)))) (h "05f94xi8v8wx57m89xwkyzd0p4djq2rilrs8mh8n12ps7kh2vjv4")))

(define-public crate-sybot-0.10.2 (c (n "sybot") (v "0.10.2") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.13.5") (d #t) (k 0)) (d (n "syunit") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sy7w6sbc79zllrigyii7xbfnmkiwhpzv66qffqyqr6jny6p87sj")))

(define-public crate-sybot-0.10.3 (c (n "sybot") (v "0.10.3") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.13.5") (d #t) (k 0)) (d (n "syunit") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "047a618rj22k2hpzfk3vd614z8siiyszv180jmb3z20210jnaang")))

(define-public crate-sybot-0.10.4 (c (n "sybot") (v "0.10.4") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 2)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syact") (r "^0.13.7") (d #t) (k 0)) (d (n "syunit") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09j48hwsd90vm3srvkpxnrdrdfnhf88bybn2diqljm7smcxlsal9")))

