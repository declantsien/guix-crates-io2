(define-module (crates-io sy ll syllabize-es) #:use-module (crates-io))

(define-public crate-syllabize-es-0.1.0 (c (n "syllabize-es") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01f5kjjh0qzajqwbgr57yvxfalpi34hpii1d4glys2ysx1wfhhnw")))

(define-public crate-syllabize-es-0.2.0 (c (n "syllabize-es") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17297ccbc839bil4khjw8m8rc7nhynzbqs893yvibngcrkzlqjrf")))

(define-public crate-syllabize-es-0.3.0 (c (n "syllabize-es") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ci1s4n6x1wqwwda8ff9mx1dns8rxdqn8wkbvqgry1n8kfpj266y")))

(define-public crate-syllabize-es-0.3.1 (c (n "syllabize-es") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vxvz3cpk5wqlfmg1nc64glxw108a67vmkmvnxhmlbx54wb499gw")))

(define-public crate-syllabize-es-0.3.2 (c (n "syllabize-es") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1j7bsgqg15mjdjacga9gp09v096qh7fgln78l5bs482c5cv12r3l")))

(define-public crate-syllabize-es-0.3.3 (c (n "syllabize-es") (v "0.3.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1iydvbpykiqc1pws84ck00r3q29rdjg8nj6biv1j1d3zdddg4wkn")))

(define-public crate-syllabize-es-0.4.0 (c (n "syllabize-es") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bc0pi4iq02vmkqnqh3v1j5zf984yzc3mdjvv82cwwx7kmzf4khd")))

(define-public crate-syllabize-es-0.4.1 (c (n "syllabize-es") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rkxbx9q5536ddg4h9rgcd6wnnjpixrhziv2w8v1m209lzagqsal")))

(define-public crate-syllabize-es-0.4.2 (c (n "syllabize-es") (v "0.4.2") (h "1g4rf3s8njxz3v0k9amwdc8yv237xf6p2yrx4y78wzq2zlj9n6xi")))

(define-public crate-syllabize-es-0.4.3 (c (n "syllabize-es") (v "0.4.3") (h "1rvlda0i9qhfrraiyxsa3x3dzm53j05axczi6p34gy5903yrfa52")))

(define-public crate-syllabize-es-0.4.4 (c (n "syllabize-es") (v "0.4.4") (h "1yzgj8r16siiipjb1hf9gjcl7lnif7950i7jgcrgj9801khh9g9m")))

(define-public crate-syllabize-es-0.4.5 (c (n "syllabize-es") (v "0.4.5") (h "020gdygljcd2z2xcdb2r72h0yfmhr7kq7lgil1zmrxry91gc66m5")))

(define-public crate-syllabize-es-0.5.0 (c (n "syllabize-es") (v "0.5.0") (h "1242ii1lyni2hqlnjlzcw1jssil1abmsbx4k29rxpb7z5530sa1h")))

(define-public crate-syllabize-es-0.5.1 (c (n "syllabize-es") (v "0.5.1") (h "06sbkvip0n0cn8z4b7a2lrvwxmfkc1k6pc6bazndksdlxg4ygmz2")))

(define-public crate-syllabize-es-0.5.2 (c (n "syllabize-es") (v "0.5.2") (h "086jgvi3f591qx3c4s83x5hz7cdkg94jjipnax7npf8x7yi8n6lx")))

