(define-module (crates-io sy ll syllogism-macro) #:use-module (crates-io))

(define-public crate-syllogism-macro-0.1.0 (c (n "syllogism-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syllogism") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "06aw53jzb975n50y9c7295x06bwgxslmjrcxxlmch6lqcfr8b5rq")))

(define-public crate-syllogism-macro-0.1.1 (c (n "syllogism-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syllogism") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "150qg5z5scpqb1nmg4fgaz5g627k9yny0156b9l089mvprhxkks4")))

