(define-module (crates-io sy ze syze) #:use-module (crates-io))

(define-public crate-syze-0.1.0 (c (n "syze") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "14ywgd2jhlr3q0vryk8mflw3kxdr6b3vsw8k9gzgb3hr4hvk4lc7")))

(define-public crate-syze-0.1.27 (c (n "syze") (v "0.1.27") (d (list (d (n "clap") (r "^4.2.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1a2bmw4zn3z5pz0lapadn8hh1g82ldyb49h3h9n62knv0kii7x94")))

