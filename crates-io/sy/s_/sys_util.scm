(define-module (crates-io sy s_ sys_util) #:use-module (crates-io))

(define-public crate-sys_util-0.1.0 (c (n "sys_util") (v "0.1.0") (d (list (d (n "data_model") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "poll_token_derive") (r "^0.1") (d #t) (k 0)) (d (n "sync_panic") (r "^0.1") (d #t) (k 0)) (d (n "syscall_defines") (r "^0.1") (d #t) (k 0)))) (h "0xxjnkbcwqwzi5zh2bf6j055vb7sviihhh05bpbn8ip88a3dmrz2")))

