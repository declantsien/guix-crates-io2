(define-module (crates-io sy n_ syn_builder) #:use-module (crates-io))

(define-public crate-syn_builder-0.1.0 (c (n "syn_builder") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "17wdpcdzbhmbsr6wiz4j7wic72wsdwbc06x6avxpi8i772nz38br")))

(define-public crate-syn_builder-0.2.0 (c (n "syn_builder") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1xnlvs83h8p0aa4fa131zn1x6qf2vncl08axrinlypf5d35jnkj8")))

