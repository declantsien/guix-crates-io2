(define-module (crates-io sy n_ syn_query) #:use-module (crates-io))

(define-public crate-syn_query-0.1.0 (c (n "syn_query") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "00ar80cyddmy3ipwr0ajl8jasjlb2nz80wvdf25xzi7ccvqc7kcn")))

(define-public crate-syn_query-0.1.1 (c (n "syn_query") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "04ndrv16i5al8ca7hf5ncv3an4v5mb0khgxb0m6ds1wv2psn1vfv")))

(define-public crate-syn_query-0.1.2 (c (n "syn_query") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1mdw78ikhfbgxm0llhlksckxhp1hsicgxn07cg1g5pfcrq9vfswd")))

(define-public crate-syn_query-0.2.0 (c (n "syn_query") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "130885mfdhiss1a8pz66ajm5145p92l7y0sxpz1w48gfdlw7zmwr")))

