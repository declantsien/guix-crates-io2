(define-module (crates-io sy n_ syn_util) #:use-module (crates-io))

(define-public crate-syn_util-0.1.0 (c (n "syn_util") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rak1bf44dyxsrg4z61x0k7rcx4bwp2b9f7rz1l3s8hg9z1n8gff")))

(define-public crate-syn_util-0.1.1 (c (n "syn_util") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xzc5vs1ffl95jqh5gvcp2m014xj5js3jzjkprbv3n7c7qwnb3c8")))

(define-public crate-syn_util-0.2.0 (c (n "syn_util") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13hf6h20l7jji32sv8700vmd9yim20v383midgj0hv76wi8683yp")))

(define-public crate-syn_util-0.2.1 (c (n "syn_util") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lkky3i76jxqas9r8l346g5dd7wk3bvz1ddajbj3jr72xch2ypi9") (y #t)))

(define-public crate-syn_util-0.3.0 (c (n "syn_util") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18743qjbq959f77nzgr25yh20ancrrbgyyqxk2qa3ghxln5qslah")))

(define-public crate-syn_util-0.4.0 (c (n "syn_util") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fcmwlw21p6f8hii7ijxcy8lxmmrv4l2xnxb51vm8vxfd6vmmlnh") (y #t)))

(define-public crate-syn_util-0.4.1 (c (n "syn_util") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12gb40gr3czrqcka8z0izgz9yz4vmxnagy46qh6n69g0s2k5ilsl")))

(define-public crate-syn_util-0.4.2 (c (n "syn_util") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g52vqjl3v34hc82771b71nwg474cmpdb86qx5a7arbrkdaw8m37")))

