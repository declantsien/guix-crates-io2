(define-module (crates-io sy n_ syn_derive) #:use-module (crates-io))

(define-public crate-syn_derive-0.1.0 (c (n "syn_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03fvnm7by3xab49rq6c5m9nhcjwr2c7qig4pdyp0d0ka43nxb3gm")))

(define-public crate-syn_derive-0.1.1 (c (n "syn_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h47jjqwgp3j6pxvg9n6gyk87c03r5vv961x38qfxnm8slpfqi81")))

(define-public crate-syn_derive-0.1.2 (c (n "syn_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9c2cbh2fv0zkkbz0rmz0fjkwwcpxzlws9gb34z3r8bn84j7ja2")))

(define-public crate-syn_derive-0.1.3 (c (n "syn_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "04mxvrq9gpa84lcyakzrba0h8cq0293nvp536zxdgyg98jf8sxk7") (y #t)))

(define-public crate-syn_derive-0.1.4 (c (n "syn_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1gn2fk07jc7vmil6g7l1285kvz3vl8dsscv7z3k257r9qa17jjb0")))

(define-public crate-syn_derive-0.1.5 (c (n "syn_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "06w6a7cb9hk4iw485wl2b0snry5wa4phqzj52gm926jpnpnbl0cw")))

(define-public crate-syn_derive-0.1.6 (c (n "syn_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pk0bi4rqzixqxx4f9hf4k5x6cnbg0h2bbfrwsni98gr096qfa41")))

(define-public crate-syn_derive-0.1.7 (c (n "syn_derive") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1999dww2b2h2ggh3hbmp9pjbbammm22flwvqzp6jx8f4000fyvmf")))

(define-public crate-syn_derive-0.1.8 (c (n "syn_derive") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "0yxydi22apcisjg0hff6dfm5x8hd6cqicav56sblx67z0af1ha8k") (f (quote (("full" "syn/full") ("default" "full"))))))

