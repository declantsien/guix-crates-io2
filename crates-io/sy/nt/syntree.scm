(define-module (crates-io sy nt syntree) #:use-module (crates-io))

(define-public crate-syntree-0.1.0 (c (n "syntree") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pr4naxmydzhzlixfmqxd8yij20fkz2ng8x6f5b3bggqpxbviiph") (y #t)))

(define-public crate-syntree-0.1.1 (c (n "syntree") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1i7sg3j8fcrwbs3rxfi6a5qkccjfjl076vdmcwc5l0wvs0fcsrgc") (y #t)))

(define-public crate-syntree-0.1.2 (c (n "syntree") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13smnvw1pz8rp19p1isr4bzadw5j6b6m91kcdc7vpj3xxs2jbw7v") (y #t)))

(define-public crate-syntree-0.2.0 (c (n "syntree") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)))) (h "0n5y5bjwm4gz40kwh8y7pvzzvsvvh3xilnfncsx8brnhlkp19dxb") (y #t)))

(define-public crate-syntree-0.3.0 (c (n "syntree") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0vhiy2bmv7qh3nf5rsfca3j4vql08bjjfp3brlq66awlbkc7j6n3") (y #t)))

(define-public crate-syntree-0.3.1 (c (n "syntree") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ss79c4mpd5gb83m8gpz3a0l8pxxidh2ca818381f2zss5siypkn") (y #t)))

(define-public crate-syntree-0.3.2 (c (n "syntree") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0jdw2r75sxdlyn1pbyx5s163cf9xw1ik5kicld2pvqaklp19c462") (y #t)))

(define-public crate-syntree-0.4.0 (c (n "syntree") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0hs7lp1q2pfjsjdvxcpv7lhirpqapfjmdgxj4akj54dx69xpzh6x") (y #t)))

(define-public crate-syntree-0.4.1 (c (n "syntree") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1dpyn95nbi2krgsqgg2bjrndq9mzwmmq3g8zzihxm9hnwas8dv9n") (y #t)))

(define-public crate-syntree-0.4.2 (c (n "syntree") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ay61hqc8a8dxngfgniwb75g8g2g1jxsgqq0mgmqn8axl56qm8ji")))

(define-public crate-syntree-0.4.3 (c (n "syntree") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1nw7xmns7iqxxxm7q1i68amn551qb064nbvvafkxx3irnvybmv1r")))

(define-public crate-syntree-0.5.0 (c (n "syntree") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0y0gsi7qiy1kb5shd5kdrx8828dwh5zk1zlxpb8jqrai08cbcw9m")))

(define-public crate-syntree-0.6.0 (c (n "syntree") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "18nx2q36xbmwih931i0sxv4vz95k83v6vrhi9bfcsq9kwdhrhwb7")))

(define-public crate-syntree-0.7.0 (c (n "syntree") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1mp36ia56a56pq305p8m60w6w0szcnz864sncjfvcq8yshnfl838")))

(define-public crate-syntree-0.7.1 (c (n "syntree") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1yjrf0s1anjfxvk7ky16avvy3l8vq4ssdj8g4ii81l9khq7fikx5")))

(define-public crate-syntree-0.8.0 (c (n "syntree") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1gy7z3k5hjc40f6v9f0nkvvvm8pg1vf49kksn5ya7kkhvn94108g")))

(define-public crate-syntree-0.9.0 (c (n "syntree") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "152minh643vgxfikzx6f55kb1cc9f3p2h2h3rlq3b6i5wkhc7sb0")))

(define-public crate-syntree-0.9.1 (c (n "syntree") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1hx1mlixyd962hyr5vsg93dvvb2044wziyk6kv4k5n9f71h4wg1j")))

(define-public crate-syntree-0.10.0 (c (n "syntree") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0sg1fpacv34q6032i7mlrzk6527dmhzxsql8nxsys6svj7234imi")))

(define-public crate-syntree-0.10.1 (c (n "syntree") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "12jnvz9zs86x1ww2s65ii4m126r14l9ipjgcf7c952ihq0h55x96")))

(define-public crate-syntree-0.10.3 (c (n "syntree") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "13zp4k8spf144c1payzj8apphpzbh9hcz5b2ha2d1ccgdjfv4kf0")))

(define-public crate-syntree-0.11.0 (c (n "syntree") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "173hr6v9jqhvwzj2gwlf7x7smkbiz7jq07dh3naq7ww5ks4f4fh8") (f (quote (("u32")))) (y #t)))

(define-public crate-syntree-0.11.1 (c (n "syntree") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0pxqy668wvnb1ilf6p6n66pyflbzz5vxwzlz5gpqrl16aa6xl7is")))

(define-public crate-syntree-0.12.0 (c (n "syntree") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0ggl67g6ds67bh8k2jligh268521gx11k9sfspbri5hign4dvnc7") (r "1.65.0")))

(define-public crate-syntree-0.12.1 (c (n "syntree") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0m1yfaqimkcd189wk8k5fa9s51nywrgranpx6xfsgxi6zg0pcaqq") (r "1.65.0")))

(define-public crate-syntree-0.12.2 (c (n "syntree") (v "0.12.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0b1ca83dq4r7yan57z6zsk22izxa0m8cpxx6n72mj3vzxbsv2hlq") (r "1.65.0")))

(define-public crate-syntree-0.13.0 (c (n "syntree") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1p29499namfg15phasblw11wz3crnjnxmlr7jccad63g6909gi1p") (r "1.65.0")))

(define-public crate-syntree-0.13.1 (c (n "syntree") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0xmcr8fc7y1rclv5k76yc5g7sx9245j532lapclhhxmrq3kk74fa") (r "1.65.0")))

(define-public crate-syntree-0.13.2 (c (n "syntree") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "15aimw3k566qj7s8l3ha24vmg3dsx6w2qs1f66l5imwvq6sb8czm") (r "1.65.0")))

(define-public crate-syntree-0.14.0 (c (n "syntree") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "1k0mkr3wf8mckn73673c1vxxr5v8amhyqv0w3j0k8qk8yb4m0d07") (y #t) (r "1.65.0")))

(define-public crate-syntree-0.14.1 (c (n "syntree") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "0gl4zsalpvyyq943r1c2qrk1mza2k6di8k094yfy91pzz7j6bkgs") (r "1.65.0")))

(define-public crate-syntree-0.14.3 (c (n "syntree") (v "0.14.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "06f75y3danp5gjgzpzcc7lcgyi0dpc88mzyv02r0m0slpdsiqbip") (r "1.65.0")))

(define-public crate-syntree-0.14.4 (c (n "syntree") (v "0.14.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "1lg4908wy7br0gfjl4ri3ba02ma1i5aznmn8gpy9z1fzbq6kbh55") (r "1.65")))

(define-public crate-syntree-0.14.5 (c (n "syntree") (v "0.14.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "11lll0phcb94p4isg8zfilw36ipdxq13zlww643yjdcibni1k86q") (r "1.65")))

