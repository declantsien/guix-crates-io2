(define-module (crates-io sy nt synthez-codegen) #:use-module (crates-io))

(define-public crate-synthez-codegen-0.1.0 (c (n "synthez-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.1.0") (d #t) (k 0)))) (h "1wzs5zjw29qhradj5jkxmminzj5bbkpgq2wgrlkbp7wmfvn5iyjk")))

(define-public crate-synthez-codegen-0.1.1 (c (n "synthez-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.1.1") (d #t) (k 0)))) (h "1ww9g19ww4kwk9ia8miac85r7rg0s31g2y9l8724c2alv3i1fri6")))

(define-public crate-synthez-codegen-0.1.2 (c (n "synthez-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.4") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.1.1") (d #t) (k 0)))) (h "0lyw3l42yd4g9n1bzh3q66wjqjsvb522yg6xv0yphfzrf5ky87d6")))

(define-public crate-synthez-codegen-0.1.3 (c (n "synthez-codegen") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.4") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.1.3") (d #t) (k 0)))) (h "1skz9ndr6z5m00dv2ji5rnm7nn3yly8cxhm88w7d84h3685vl77d")))

(define-public crate-synthez-codegen-0.2.0 (c (n "synthez-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 2)) (d (n "syn") (r "^1.0.72") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.2") (d #t) (k 0)))) (h "0yjrzmm16iamqx5yb1hzlj1yfsffjh0f46060w7rcihflii389k9") (r "1.56")))

(define-public crate-synthez-codegen-0.3.0 (c (n "synthez-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.3") (d #t) (k 0)))) (h "19f0krq1mj1grsd3sfmsm97g7xr2rvlpphj6izs1yjcvqvn52bav") (r "1.62")))

(define-public crate-synthez-codegen-0.3.1 (c (n "synthez-codegen") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "proc-macro"))) (k 2)) (d (n "synthez-core") (r "^0.3.1") (d #t) (k 0)))) (h "0ikp5ivz8hgzrldy0dzpmsj3jyx7f7ccqyx5b0qjy5mp8insl97p") (r "1.62")))

