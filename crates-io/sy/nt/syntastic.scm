(define-module (crates-io sy nt syntastic) #:use-module (crates-io))

(define-public crate-syntastic-0.1.0 (c (n "syntastic") (v "0.1.0") (h "08ywz0k74pz1m6bjw0dhg3ryhj3h7nzr91b7f5mmgagsr73jxxz1") (y #t)))

(define-public crate-syntastic-0.2.0 (c (n "syntastic") (v "0.2.0") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xxqxahz8fi5bbd2x8c4klihc3h5a1yy827fdyrvalc0kdxw0p6h")))

(define-public crate-syntastic-0.2.1 (c (n "syntastic") (v "0.2.1") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "078j5l8qw1cldkmmqnmy6smxvjz8fwm34jgkny17v0x7n0kdqq6g")))

(define-public crate-syntastic-0.2.2 (c (n "syntastic") (v "0.2.2") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bqlxs28500lmn2vqrnvg7mk3j8g3dsakbrkqncv71r1mdf6fw99")))

(define-public crate-syntastic-0.3.0 (c (n "syntastic") (v "0.3.0") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lninb446n0f9x20b5k72dgbbc1fnbmp0vqxcygjd6q7vv568d5b")))

(define-public crate-syntastic-0.4.0 (c (n "syntastic") (v "0.4.0") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04n0wiilwpjzmcrhdb9cn6lz5yywkl7l60vpdx452bil6y209xpp")))

(define-public crate-syntastic-0.5.0 (c (n "syntastic") (v "0.5.0") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xa6sxs919yja45b7br0nr8rz7qhpl6kwkfi9sh873s288bknqcz")))

(define-public crate-syntastic-0.5.1 (c (n "syntastic") (v "0.5.1") (d (list (d (n "insta") (r "^1.3.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11z3y80jjv12pm3sd5mdkirak0lzk63lxsmy7xvrr4za46jyspf3")))

