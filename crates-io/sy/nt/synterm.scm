(define-module (crates-io sy nt synterm) #:use-module (crates-io))

(define-public crate-synterm-0.1.0 (c (n "synterm") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "01iccfrsdwjgmzf1jb3g3fdkmvdvbxlbjmw3bd5i8g29l8gbh70i")))

(define-public crate-synterm-0.1.1 (c (n "synterm") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ci00ilsm71nzbizjv5srizrg2mdfj9hff7c24n05qi0nkd0npiq")))

(define-public crate-synterm-0.1.11 (c (n "synterm") (v "0.1.11") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1q3x0230xlr1y0p7jh5pc9rl2k71j1ms7rb7lsf66pvrvakj9dvi")))

(define-public crate-synterm-0.1.111 (c (n "synterm") (v "0.1.111") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "01ph6138nj432v01gl90sz1868z5mfifbdn5fsq4al6n7c9hjql4")))

(define-public crate-synterm-0.1.12 (c (n "synterm") (v "0.1.12") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1lw1zy1jrkrvrna46i8zpkzn9m480rgx4sivvnann1f4z1mbwk5p")))

(define-public crate-synterm-0.1.121 (c (n "synterm") (v "0.1.121") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0c7lcswwal4sx1igbcs4akr99vmn751c495kcz9id5qv2dnf29yy")))

(define-public crate-synterm-0.1.2 (c (n "synterm") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ihazbvznbyxmj03cmwmp7kdcanpbgr0i8dx5csq4x1ii2xx6ay6")))

(define-public crate-synterm-0.1.21 (c (n "synterm") (v "0.1.21") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1f524gcc0zzqv29pqfgknlsa8v21raqny4gi487irxx4a9slqf52")))

(define-public crate-synterm-0.2.0 (c (n "synterm") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0lrlbfccjrdfvxfgs9q6lvp0lxlg28h830xv3gacirb5hpg18j1i")))

(define-public crate-synterm-0.2.1 (c (n "synterm") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1jh2nyjxdc10z5vjajr9ixhvx4wrhc2hdhc1a2k8vi057sj2fvys")))

(define-public crate-synterm-0.2.11 (c (n "synterm") (v "0.2.11") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0yia43qii6n246gfvx88x2r5304hml3lwdhvizyi8fcm0malmsic")))

(define-public crate-synterm-0.3.0 (c (n "synterm") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "16jas984iymar6g8dm7wbgx58i7d66hbjxlmp3i2c5gk4krs4mcg")))

(define-public crate-synterm-0.3.1 (c (n "synterm") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0n0r1lzcfxa0g2s3hrjds7x353s03z5pcshn955sh9pwzj3ypca7")))

