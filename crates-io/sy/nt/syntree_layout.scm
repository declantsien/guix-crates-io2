(define-module (crates-io sy nt syntree_layout) #:use-module (crates-io))

(define-public crate-syntree_layout-0.1.0 (c (n "syntree_layout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "syntree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml_writer") (r "^0.4") (d #t) (k 0)))) (h "0j5aggn3mpi3lb0p9kfyw1pa5rq2yrry5sd8yc6bcs2y3315sqn9")))

(define-public crate-syntree_layout-0.2.0 (c (n "syntree_layout") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "syntree") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml_writer") (r "^0.4") (d #t) (k 0)))) (h "0svmpm49lk04xzbylcd8hqhgprfqy5wxvfc7iiqgk8388l2cqkbc")))

