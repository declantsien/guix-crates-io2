(define-module (crates-io sy nt syntax-rust) #:use-module (crates-io))

(define-public crate-syntax-rust-0.1.0 (c (n "syntax-rust") (v "0.1.0") (d (list (d (n "dialect") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "111bh3qb41fzpvjhxf9fb6m9qfg5n1362gifw16qb66almzp7qjd")))

(define-public crate-syntax-rust-0.2.0 (c (n "syntax-rust") (v "0.2.0") (d (list (d (n "dialect") (r "^0.2") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "156c8xjd5755s5vv3iar798jdvf9m583vw4q4kdi8s4gd13z7c9b")))

(define-public crate-syntax-rust-0.3.0 (c (n "syntax-rust") (v "0.3.0") (d (list (d (n "dialect") (r "^0.3") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1xxrla935jl3agk5sxk124dcqdyj5f6l6d116fbzyfsjggcsybq5")))

(define-public crate-syntax-rust-0.4.0 (c (n "syntax-rust") (v "0.4.0") (d (list (d (n "dialect") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0n18xgcsv0nbli9hfhcal7r836kiwy1p3mw8l7p9nac2r4xar2ml")))

(define-public crate-syntax-rust-0.4.1 (c (n "syntax-rust") (v "0.4.1") (d (list (d (n "dialect") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1l43c2xn9b4hmmchbbqmzc6xi1vvlpfbhn8x1flmh3drrl17vq4a")))

