(define-module (crates-io sy nt synthizer-sys) #:use-module (crates-io))

(define-public crate-synthizer-sys-0.7.1 (c (n "synthizer-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "0rvarq69y17q3pafjq9fxyf8gpqmi80sa2bzhyvr7dii6fxv2vgg")))

(define-public crate-synthizer-sys-0.7.11 (c (n "synthizer-sys") (v "0.7.11") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)))) (h "1rdjlz12zpp3kqc2n8rdy1ixxm63423w36l3py0jzz10k4gxrlcv")))

(define-public crate-synthizer-sys-0.7.12 (c (n "synthizer-sys") (v "0.7.12") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)))) (h "06nn3k59x39hsfiqpc6jgg9c9vb0rr5kr2pds29whrbmcrdwf4md")))

(define-public crate-synthizer-sys-0.8.0 (c (n "synthizer-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1xlpvxkhmsr18av5qylvrlhb9jna03zfg1qha5sli7ygjpqnl01q")))

(define-public crate-synthizer-sys-0.9.0 (c (n "synthizer-sys") (v "0.9.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0i84z0alapbh1jg4rsxg7nzipak7d90wiv53ny7cb42fr2x33ayk")))

(define-public crate-synthizer-sys-0.9.1 (c (n "synthizer-sys") (v "0.9.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "181qwpfnh2riai22ri8v13al3157i68vjcr4ss626h59y1h5qsa2")))

(define-public crate-synthizer-sys-0.9.2 (c (n "synthizer-sys") (v "0.9.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "151jl68szr0pimgkpdjhbnql569310by15hfrka6clsh5fbapf1d")))

(define-public crate-synthizer-sys-0.10.0 (c (n "synthizer-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "11l9f4kg6h0jk4s26lz89ha1fjwiflr22dz9d28yfbm63lgfdzms")))

(define-public crate-synthizer-sys-0.10.1 (c (n "synthizer-sys") (v "0.10.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "17iyz4vqk161hd996yfwajmh4yzkkydvqvpaclxr99aa90qpdpzl")))

(define-public crate-synthizer-sys-0.10.2 (c (n "synthizer-sys") (v "0.10.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0afh70kph7b99ns4mb5sp7nnadk742wmfda05rd0vaq349llqd46")))

(define-public crate-synthizer-sys-0.10.3 (c (n "synthizer-sys") (v "0.10.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1piyjp6zqp1i8sfwbn972fz8r18ci5p7kgb3z55ikprgk89b6dpx")))

(define-public crate-synthizer-sys-0.10.4 (c (n "synthizer-sys") (v "0.10.4") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0bbb6y122pwrb8cz223cqd8g1naxmqszn8q2av72wqhkl86z0q6k")))

(define-public crate-synthizer-sys-0.10.5 (c (n "synthizer-sys") (v "0.10.5") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0h518gkha6g1qdrpd754qjf2zp7420if0hkch96ff3nb1i8wb8k0")))

(define-public crate-synthizer-sys-0.10.7 (c (n "synthizer-sys") (v "0.10.7") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "166sybc079xmxb1mvk7z3lz9bqjxvjm97yg95qn5acikmpjrj793")))

(define-public crate-synthizer-sys-0.10.8 (c (n "synthizer-sys") (v "0.10.8") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0z0vrxwl8b8wh6szclgr8ndykafsxm76zdnfkidp72bnp3m0y1vy")))

(define-public crate-synthizer-sys-0.10.9 (c (n "synthizer-sys") (v "0.10.9") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0ahsc2vvkyxh4lw6qjmfhbrdhhhrcp5f27iz1hhzh768iad4ybm5")))

(define-public crate-synthizer-sys-0.10.10 (c (n "synthizer-sys") (v "0.10.10") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "14p95a82p8d4rv1mciksz52rli85im7l2rarx9c8sdyw4zva0p60")))

