(define-module (crates-io sy nt syntastica-themes) #:use-module (crates-io))

(define-public crate-syntastica-themes-0.1.0 (c (n "syntastica-themes") (v "0.1.0") (d (list (d (n "syntastica") (r "^0.1.1") (d #t) (k 0)))) (h "17dcz5pn7g80q6vhr2rlki399kji582pyi46j0p6svkfq9939bpc")))

(define-public crate-syntastica-themes-0.2.0 (c (n "syntastica-themes") (v "0.2.0") (d (list (d (n "syntastica") (r "^0.2.0") (d #t) (k 0)))) (h "17wkcq6g57xrz1r3fhiby8aca3i88b9zi3rd5dzqr1xb66cv37bz")))

(define-public crate-syntastica-themes-0.3.0 (c (n "syntastica-themes") (v "0.3.0") (d (list (d (n "syntastica-core") (r "^0.3.0") (d #t) (k 0)))) (h "13y0qkfalnm0qpl0pvbrfv9lf7pi1m0s181gjalpzhfqfxj6m2ja")))

(define-public crate-syntastica-themes-0.4.0 (c (n "syntastica-themes") (v "0.4.0") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "syntastica-core") (r "^0.4.0") (k 0)))) (h "04z4hh7bafirqfs69g832jb3ldwzh02zx2kpg284kswbdz2znmsc") (f (quote (("runtime-c2rust" "syntastica-core/runtime-c2rust") ("runtime-c" "syntastica-core/runtime-c") ("default" "runtime-c")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-syntastica-themes-0.4.1 (c (n "syntastica-themes") (v "0.4.1") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "syntastica-core") (r "^0.4.1") (k 0)))) (h "0m1d04bkd7hd0hql1ydsygmfqzb8z312lyfs69jk0yfj9m6icz6b") (f (quote (("runtime-c2rust" "syntastica-core/runtime-c2rust") ("runtime-c" "syntastica-core/runtime-c") ("default" "runtime-c")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

