(define-module (crates-io sy nt synth-utils) #:use-module (crates-io))

(define-public crate-synth-utils-0.1.0 (c (n "synth-utils") (v "0.1.0") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "midi-convert") (r "^0.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0lbl2h6iyr92d8q00p2kk4416jacwlpjcx11jwawzddd16msa9nk")))

(define-public crate-synth-utils-0.1.1 (c (n "synth-utils") (v "0.1.1") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "midi-convert") (r "^0.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1d6hjliqgq18ymjmd3jqvq168j2n5acsl46009nw4pyyqsglqwx4")))

(define-public crate-synth-utils-0.2.0 (c (n "synth-utils") (v "0.2.0") (d (list (d (n "biquad") (r "^0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "midi-convert") (r "^0.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1rpgs4p66g6mfx9nmn0z3hs0v09jpbpd970q408m7rg09w2nwnyg")))

