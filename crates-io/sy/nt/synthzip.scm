(define-module (crates-io sy nt synthzip) #:use-module (crates-io))

(define-public crate-synthzip-0.1.0 (c (n "synthzip") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15qq47cdf9khsxvf38m8f874qngfsmjr37qqgf6r9kx2yzg7fphq") (f (quote (("discovery" "logging") ("default")))) (s 2) (e (quote (("logging" "dep:log"))))))

