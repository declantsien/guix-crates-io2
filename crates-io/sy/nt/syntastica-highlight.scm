(define-module (crates-io sy nt syntastica-highlight) #:use-module (crates-io))

(define-public crate-syntastica-highlight-0.3.0 (c (n "syntastica-highlight") (v "0.3.0") (d (list (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "1j8cplj7hmqxg36y9scswa61lpdxdfpqp7lwjn1idxm4fh12nbpw")))

(define-public crate-syntastica-highlight-0.4.0 (c (n "syntastica-highlight") (v "0.4.0") (d (list (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (o #t) (d #t) (k 0)) (d (n "tree-sitter-c2rust") (r "^0.20.10") (o #t) (d #t) (k 0)))) (h "1996vlvpl1hvb3mzy6v9591n8izxay1zd9n2qjmsgp67w5zr8s5g") (f (quote (("default" "runtime-c")))) (s 2) (e (quote (("runtime-c2rust" "dep:tree-sitter-c2rust") ("runtime-c" "dep:tree-sitter"))))))

(define-public crate-syntastica-highlight-0.4.1 (c (n "syntastica-highlight") (v "0.4.1") (d (list (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (o #t) (d #t) (k 0)) (d (n "tree-sitter-c2rust") (r "^0.20.10") (o #t) (d #t) (k 0)))) (h "09r3n7kmy2p9h4g5dbqqyggfcmpnhlz0y8g08fyi0namc097g4ph") (f (quote (("default" "runtime-c")))) (s 2) (e (quote (("runtime-c2rust" "dep:tree-sitter-c2rust") ("runtime-c" "dep:tree-sitter"))))))

