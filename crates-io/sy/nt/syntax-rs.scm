(define-module (crates-io sy nt syntax-rs) #:use-module (crates-io))

(define-public crate-syntax-rs-0.1.0 (c (n "syntax-rs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.3") (d #t) (k 0)))) (h "0xhwk3hfdv849qbhinax62nhsgdw1vlcdm8g96hd6r8nwykn6si3") (f (quote (("span") ("default" "char_spec" "span") ("char_spec"))))))

(define-public crate-syntax-rs-0.1.1 (c (n "syntax-rs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.3") (d #t) (k 0)))) (h "0dkv7qi9dyjxk3jr6f757clchf66hvcc4n1lm17b3z23hkqn8597") (f (quote (("span") ("default" "char_spec" "span") ("char_spec"))))))

(define-public crate-syntax-rs-1.1.1 (c (n "syntax-rs") (v "1.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.3") (d #t) (k 0)))) (h "165xiygx8ikadjfrjz05md7djnx5q9gy7rgyvwqkzmig1idad9i1") (f (quote (("span") ("default" "char_spec" "span") ("debug") ("char_spec"))))))

