(define-module (crates-io sy nt syntastica-queries) #:use-module (crates-io))

(define-public crate-syntastica-queries-0.1.0 (c (n "syntastica-queries") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syntastica-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "syntastica-parsers") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "syntastica-parsers") (r "^0.1.0") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)))) (h "01nwy2hwmkai2w7wkws26n82p8drkn4hs7aaqn74d121qfgya4b2") (f (quote (("default")))) (s 2) (e (quote (("some" "dep:syntastica-parsers" "syntastica-parsers?/some") ("most" "some" "syntastica-parsers?/most") ("all" "most" "syntastica-parsers?/all"))))))

(define-public crate-syntastica-queries-0.2.0 (c (n "syntastica-queries") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 2)))) (h "1pa0qcd5lhy0fd08arwqfcy56k5mhj0iwnp6yrisc0mip71kq30c")))

(define-public crate-syntastica-queries-0.3.0 (c (n "syntastica-queries") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1p4fv84h9ampsmbw9vbgwxmf62zi1jq88i9mpw9pc6ymn3hrcfy1")))

(define-public crate-syntastica-queries-0.4.1 (c (n "syntastica-queries") (v "0.4.1") (h "0mig3262airdhndpb5s1ib85digh6558j0w7yx3n5bn7wbxii13l")))

