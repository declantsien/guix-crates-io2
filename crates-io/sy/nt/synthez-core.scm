(define-module (crates-io sy nt synthez-core) #:use-module (crates-io))

(define-public crate-synthez-core-0.1.0 (c (n "synthez-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.3") (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "0fvxl2kjf6pir9ahkdx0la60zlf4d8bn02b25r1i1w9bppsrg00m") (f (quote (("full" "syn/full"))))))

(define-public crate-synthez-core-0.1.1 (c (n "synthez-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.3") (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "0hajm06lq5zdjdfgjb9dahfsgj7cy5kvvzl9xpmqmzmf9jf18676") (f (quote (("full" "syn/full"))))))

(define-public crate-synthez-core-0.1.2 (c (n "synthez-core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.3") (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1afg7nszy55gyvparnig3cnk8np8xx1pl0y60amflkx2zbsblcqs") (f (quote (("full" "syn/full"))))))

(define-public crate-synthez-core-0.1.3 (c (n "synthez-core") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.3") (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1bsmcdq9ylnywkbv4sydyr6n8rds87sgir4wnyv9z7ch9748z690") (f (quote (("full" "syn/full"))))))

(define-public crate-synthez-core-0.2.0 (c (n "synthez-core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.3") (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1nc98c6mjnc7s2scnax826maqpaalvyjlc06kwkj65zyi505m2xv") (f (quote (("full" "syn/full")))) (r "1.56")))

(define-public crate-synthez-core-0.3.0 (c (n "synthez-core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.4") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1c29bcr705ab8nsixg3admqymawx6bsydqxd06axn51fszphq3hl") (f (quote (("full" "syn/full")))) (r "1.62")))

(define-public crate-synthez-core-0.3.1 (c (n "synthez-core") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (k 0)) (d (n "quote") (r "^1.0.9") (k 0)) (d (n "sealed") (r "^0.5") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1sjlqqbfi009n6vgwqvw9yb27dphw2xmpkj3zljj8pj6abnadgvq") (f (quote (("full" "syn/full")))) (r "1.62")))

