(define-module (crates-io sy nt synthez) #:use-module (crates-io))

(define-public crate-synthez-0.1.0 (c (n "synthez") (v "0.1.0") (d (list (d (n "synthez-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "synthez-core") (r "^0.1.0") (d #t) (k 0)))) (h "03lzxyxyahfzpdi8rv210irzrl8w9q36v0307lsdblmsvn92zwzl") (f (quote (("full" "synthez-core/full"))))))

(define-public crate-synthez-0.1.1 (c (n "synthez") (v "0.1.1") (d (list (d (n "synthez-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "synthez-core") (r "^0.1.1") (d #t) (k 0)))) (h "19017my99xjrk1fy1x040qrcr9fi6k21khw144xql4cmm7j0p83m") (f (quote (("full" "synthez-core/full"))))))

(define-public crate-synthez-0.1.2 (c (n "synthez") (v "0.1.2") (d (list (d (n "synthez-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "synthez-core") (r "^0.1.1") (d #t) (k 0)))) (h "0kln4zis7791p6gdnbhzfw2awmyvp1qpsy6xmn92y5cjgc25cl1g") (f (quote (("full" "synthez-core/full"))))))

(define-public crate-synthez-0.1.3 (c (n "synthez") (v "0.1.3") (d (list (d (n "synthez-codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "synthez-core") (r "^0.1.3") (d #t) (k 0)))) (h "1n0wkxp6pkjj2dq4cgqrg2pdbi1liq7wdzj35vwpf5n8cjxcdwdc") (f (quote (("full" "synthez-core/full"))))))

(define-public crate-synthez-0.2.0 (c (n "synthez") (v "0.2.0") (d (list (d (n "syn") (r "^1.0.72") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "synthez-codegen") (r "^0.2") (d #t) (k 0)) (d (n "synthez-core") (r "^0.2") (d #t) (k 0)))) (h "1cccarg152v0wwj7qvywx5p5ipd8wq370r91014wbzycmk87hc83") (f (quote (("full" "syn/full" "synthez-core/full")))) (r "1.56")))

(define-public crate-synthez-0.3.0 (c (n "synthez") (v "0.3.0") (d (list (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "synthez-codegen") (r "^0.3") (d #t) (k 0)) (d (n "synthez-core") (r "^0.3") (d #t) (k 0)))) (h "1s5sppgbvnswyn7xwdyn25shiidn7cimffm319anarrgnyahksn5") (f (quote (("full" "syn/full" "synthez-core/full")))) (r "1.62")))

(define-public crate-synthez-0.3.1 (c (n "synthez") (v "0.3.1") (d (list (d (n "syn") (r "^2.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "synthez-codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "synthez-core") (r "^0.3.1") (d #t) (k 0)))) (h "13gcb5gkwcqcpskmc6ndhnz8q6n9329nsn9yws3f388h4lhc5lm3") (f (quote (("full" "syn/full" "synthez-core/full")))) (r "1.62")))

