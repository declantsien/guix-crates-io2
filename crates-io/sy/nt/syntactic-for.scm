(define-module (crates-io sy nt syntactic-for) #:use-module (crates-io))

(define-public crate-syntactic-for-0.1.0 (c (n "syntactic-for") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lwni7sfkgf1nw67cnniln43wjwx4906agsg1ajjdkmvi2dhbls3") (r "1.56")))

(define-public crate-syntactic-for-0.1.1 (c (n "syntactic-for") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mr7jjnmb4k9sdg1hxi6f2q1hkjwjf3ipb2wvhvb6cwk46id35x8") (r "1.56")))

