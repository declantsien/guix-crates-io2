(define-module (crates-io sy nt syntaxdb) #:use-module (crates-io))

(define-public crate-syntaxdb-0.1.0 (c (n "syntaxdb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "10blk0hc8pf27yvaskqjnmn6g5imk28g3pjxbi9ykkqn4p6hixdj")))

