(define-module (crates-io sy nt syntax-error) #:use-module (crates-io))

(define-public crate-syntax-error-0.0.0 (c (n "syntax-error") (v "0.0.0") (h "1qrxhrwz82099rp6i45wljc6dhp52lgy4wdqxlccqz8ksal7b0kp") (f (quote (("default"))))))

(define-public crate-syntax-error-0.0.1 (c (n "syntax-error") (v "0.0.1") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "00pqgfy5fvmh139jfq0w3i8h09sai9p6zfhcfi5ghqvsgs461p08") (f (quote (("default"))))))

(define-public crate-syntax-error-0.0.2 (c (n "syntax-error") (v "0.0.2") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0fdy540gb5qjj20kgld5iasiaanw9cjrmlx6nrk6ysr40bl4vq5b") (f (quote (("default"))))))

(define-public crate-syntax-error-0.0.3 (c (n "syntax-error") (v "0.0.3") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1n45fdapvdrqicgm92p1l1pswc646sabwwr37zxawpx6fr601y8x") (f (quote (("default"))))))

(define-public crate-syntax-error-0.0.4 (c (n "syntax-error") (v "0.0.4") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0f75gjgp2k7a8lwv76sypbrxsim56igymbfc2yqbp73n2rg5siiq") (f (quote (("default"))))))

