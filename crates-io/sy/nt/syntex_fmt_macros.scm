(define-module (crates-io sy nt syntex_fmt_macros) #:use-module (crates-io))

(define-public crate-syntex_fmt_macros-0.4.0 (c (n "syntex_fmt_macros") (v "0.4.0") (d (list (d (n "unicode-xid") (r "*") (d #t) (k 0)))) (h "0sqaash7zhdnw4mkdbgdbkngv515jxz4934cyl3nj2c064fg16rs")))

(define-public crate-syntex_fmt_macros-0.4.2 (c (n "syntex_fmt_macros") (v "0.4.2") (d (list (d (n "unicode-xid") (r "*") (d #t) (k 0)))) (h "0n9qbvf46skrq7fpiiws69zbsh6bp10jpxjy8cb1mpbwh648cz8b")))

(define-public crate-syntex_fmt_macros-0.4.3 (c (n "syntex_fmt_macros") (v "0.4.3") (d (list (d (n "unicode-xid") (r "*") (d #t) (k 0)))) (h "1z5la7d5ssf5vqiy3gs1q0mb79ygjm9kn6543nfd4y019jm0c276")))

(define-public crate-syntex_fmt_macros-0.5.0 (c (n "syntex_fmt_macros") (v "0.5.0") (d (list (d (n "unicode-xid") (r "*") (d #t) (k 0)))) (h "1h7grnlkd35lplw2pp6pv3xakwz1902qicw5mwvd2n47qjyqclsy")))

