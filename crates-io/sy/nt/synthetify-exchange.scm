(define-module (crates-io sy nt synthetify-exchange) #:use-module (crates-io))

(define-public crate-synthetify-exchange-0.1.0 (c (n "synthetify-exchange") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.16.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.16.2") (d #t) (k 0)) (d (n "pyth-anchor") (r "^0.1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thincollections") (r "^0.5.3") (d #t) (k 0)))) (h "0xwgiz2i9ycw504qscmsqcgc98lyr1csa9idwcmarrwavq6lqh32") (f (quote (("testnet") ("no-idl") ("no-entrypoint") ("mainnet") ("localnet") ("devnet") ("default" "localnet") ("cpi" "no-entrypoint"))))))

