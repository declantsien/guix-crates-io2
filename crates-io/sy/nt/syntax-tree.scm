(define-module (crates-io sy nt syntax-tree) #:use-module (crates-io))

(define-public crate-syntax-tree-0.1.0 (c (n "syntax-tree") (v "0.1.0") (h "07wdibl197wsq9jxl5r0qinx0kj3y6w6y6h2x6fad72fxp1yrkga")))

(define-public crate-syntax-tree-0.2.0 (c (n "syntax-tree") (v "0.2.0") (h "0x5kivrd85h5zmi4c107b1ycgfy42i6a8ivnzdzf829ls8mi78xj")))

(define-public crate-syntax-tree-0.3.0 (c (n "syntax-tree") (v "0.3.0") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0kd027lmnyb4fq33f89614phpgmn22wg1ybgq5p7n1qv204555hv")))

(define-public crate-syntax-tree-0.3.1 (c (n "syntax-tree") (v "0.3.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "04k3q8y2wmz51mk1wv1shzw88a0qryklc4241jfc33h3fhwn83v6")))

(define-public crate-syntax-tree-0.3.2 (c (n "syntax-tree") (v "0.3.2") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1fsfjj03cdr85ayq0bxb5l3lkl8cjvywyc3j9dgf8704kyxgnv2p")))

