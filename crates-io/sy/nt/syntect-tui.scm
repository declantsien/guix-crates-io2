(define-module (crates-io sy nt syntect-tui) #:use-module (crates-io))

(define-public crate-syntect-tui-1.0.0 (c (n "syntect-tui") (v "1.0.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0826mw19d09xd4ch3siqsn810yls06pds1s9zvk79dzj651jy7z3")))

(define-public crate-syntect-tui-1.0.1 (c (n "syntect-tui") (v "1.0.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "183wgfz0lskpv75sgdx6g1gs2ikfhkh24nwmzdrp37x06plghbin")))

(define-public crate-syntect-tui-2.0.0 (c (n "syntect-tui") (v "2.0.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "099wydvfbkggqidlwl7mylssqjsq08zy4652ik7x1ngb4vbsi3mi")))

(define-public crate-syntect-tui-3.0.0 (c (n "syntect-tui") (v "3.0.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "0zcy46fmypiivba4v3j4p0k77wwmnasvw31wz58hmsfvh77k4dp4")))

(define-public crate-syntect-tui-3.0.1 (c (n "syntect-tui") (v "3.0.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "06928sr04s9glh4mnryjccbdwdjhfkxqbf2q6x3drdqzpnla8i41")))

(define-public crate-syntect-tui-3.0.2 (c (n "syntect-tui") (v "3.0.2") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "0kvrb4v1i8wi2cz0vyvq9mwwyifcx1d8nbmm10yzkfmvxnb73zh2")))

