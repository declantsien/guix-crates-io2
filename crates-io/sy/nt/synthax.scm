(define-module (crates-io sy nt synthax) #:use-module (crates-io))

(define-public crate-synthax-0.1.0 (c (n "synthax") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "1qnqhlp4ir0vzgid3903kqdgnfjbdkg7gn9nmcs1d3xmjs8hn1w0")))

(define-public crate-synthax-0.2.0 (c (n "synthax") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "1vhnm0jwqw7i85bd5rh6iia4i17sicb4wsyd9pfsiqf3gan03p42") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.0 (c (n "synthax") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "11lakxx3nbn0cmk9nphv94hgkbw3p35bmd0r93gqpwrl15599ik2") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.1 (c (n "synthax") (v "0.3.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "0499zsqngp8fw6fz4h7x8mydpbzgf1pcicm1ds1jzqxlrznr15yy") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.2 (c (n "synthax") (v "0.3.2") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "1hfy1yzs46g395fc3zbxa979cz8a0scqnm8i6c5k4s9m1vhnsgc3") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.3 (c (n "synthax") (v "0.3.3") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "011a3s0amall1shancc74rq5vagx8azchg4ic21qxkjzm8q6ndw1") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.4 (c (n "synthax") (v "0.3.4") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "0k0sfidyc37zkbc13hcmw9zxn7v5kkakcikjyf7f012q5rji1pj3") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.5 (c (n "synthax") (v "0.3.5") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "19ylqva5a2s22mkzqfahz2fy3chp05laq799w6a9b8m31q598jkk") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.3.6 (c (n "synthax") (v "0.3.6") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)))) (h "0jpidcrc7ldqz377kbv3yv8rhqkxjc2x8i2690ix3mw44fnk03fm") (f (quote (("stable" "syntex" "syntex_syntax"))))))

(define-public crate-synthax-0.4.0 (c (n "synthax") (v "0.4.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "17rjk6lmdvym4abdqy1mzbachnpkp7xglidcqw4l1w3cvrns3y33")))

(define-public crate-synthax-0.4.1 (c (n "synthax") (v "0.4.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)))) (h "0nv0rzbwxy22wmrnnni2kf0pkidc2sahqng7ppln2n33361fb91s")))

