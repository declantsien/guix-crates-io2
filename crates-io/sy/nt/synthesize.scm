(define-module (crates-io sy nt synthesize) #:use-module (crates-io))

(define-public crate-synthesize-0.1.0 (c (n "synthesize") (v "0.1.0") (d (list (d (n "ees") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0v03zri92nbi342qfyljjva43z3ih3lmghvgbs1mgvi715g2d0ld") (y #t)))

(define-public crate-synthesize-0.1.1 (c (n "synthesize") (v "0.1.1") (d (list (d (n "ees") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1n8zapylbi08skwwmmczd00b1cgf734h4yal6la2z56nq9psha8i") (y #t)))

(define-public crate-synthesize-0.1.2 (c (n "synthesize") (v "0.1.2") (d (list (d (n "ees") (r "^0.2.5") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hikcyz7zpircj7p8ji6q4by5ijq3wgzl587z0f6qzyrpc7yls63") (y #t)))

