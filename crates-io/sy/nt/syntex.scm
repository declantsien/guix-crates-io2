(define-module (crates-io sy nt syntex) #:use-module (crates-io))

(define-public crate-syntex-0.0.0 (c (n "syntex") (v "0.0.0") (h "1gvir28786ak7687r17fw4zkb1l6b4zw6z0mdpsalp2gdhb5jnq1")))

(define-public crate-syntex-0.1.0 (c (n "syntex") (v "0.1.0") (d (list (d (n "syntex_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "096f4s8risqij4nv7xwrqhyiqdv3xsbswvfk9qnalswpf5ysm9ah")))

(define-public crate-syntex-0.3.0 (c (n "syntex") (v "0.3.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "0m1qv8sazlc7pp2wxa5gz4kf7amdsz9b78vk6jwir6f2xvsmgrkq")))

(define-public crate-syntex-0.4.0 (c (n "syntex") (v "0.4.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "0x36d7zg91w7bp75pzpysilj6qn184lc56c17yixfvhwdqkjc3s8")))

(define-public crate-syntex-0.4.1 (c (n "syntex") (v "0.4.1") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "1nvx2vqncfn8q0x7rgckz61bn9p85ndwwrhg8vy2c097v7w1jir5")))

(define-public crate-syntex-0.4.2 (c (n "syntex") (v "0.4.2") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "0x0fjdh4h1rljdwvxwjdicvd3jzd8nwvzw9bkz2vs1rb7p5klfzb")))

(define-public crate-syntex-0.4.3 (c (n "syntex") (v "0.4.3") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "0h8467a60mrw54ag9ab9f9sqhznj6vsc8a7sg03jgsfarlv134xb")))

(define-public crate-syntex-0.5.0 (c (n "syntex") (v "0.5.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "1w32ddpsdmgr7v1i4z5jhjhw7hbsyh1bsg6g6pnpf8jg7336wmii")))

(define-public crate-syntex-0.6.0 (c (n "syntex") (v "0.6.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "0wgzrdhz5ynx1gla68pcbkrxdfp4bqsvzm8z208bkhjvvn00ibm0")))

(define-public crate-syntex-0.7.0 (c (n "syntex") (v "0.7.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "1xc8grjbjy30c29dq5vj0nlp0wcgjy9agn4gpyfgr1rk0pq51h3n")))

(define-public crate-syntex-0.11.0 (c (n "syntex") (v "0.11.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "11yi73n3mm20bckzxwq3wn08ywkhlrs9d91ylz6z9pz9kbgfni67")))

(define-public crate-syntex-0.17.0 (c (n "syntex") (v "0.17.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "188cqpyaaq92x0qjxaxxi8c300v2axp3y9qcjqy1dpi9f15k4jk8")))

(define-public crate-syntex-0.22.0 (c (n "syntex") (v "0.22.0") (d (list (d (n "syntex_syntax") (r "*") (d #t) (k 0)))) (h "01l6vb040nbhnc2mggf56hjfrwi9hvxgp02hjsnlbrlgrlq4pdpw")))

(define-public crate-syntex-0.24.0 (c (n "syntex") (v "0.24.0") (d (list (d (n "syntex_syntax") (r "^0.24.0") (d #t) (k 0)))) (h "1ghj4hvnhncsb98als45y3y9cbfqyzxv6i6pkphjb0z7l5lddfmi")))

(define-public crate-syntex-0.25.0 (c (n "syntex") (v "0.25.0") (d (list (d (n "syntex_syntax") (r "^0.25.0") (d #t) (k 0)))) (h "0bxxdq0qwpz2qvhfwvb2hbz16w6xfdf7dym7qx7mha9455bl46lx")))

(define-public crate-syntex-0.26.0 (c (n "syntex") (v "0.26.0") (d (list (d (n "syntex_syntax") (r "^0.26.0") (d #t) (k 0)))) (h "0nznbnqfs1myg8k5mmxgfzd189m6iff3r6x7bc2f4ij26gmjvlbb")))

(define-public crate-syntex-0.27.0 (c (n "syntex") (v "0.27.0") (d (list (d (n "syntex_syntax") (r "^0.27.0") (d #t) (k 0)))) (h "0299xf3h3rz6lb6gn5ypc9wq3n0ggdvfjfsyry2l2c7pvn9392j7")))

(define-public crate-syntex-0.28.0 (c (n "syntex") (v "0.28.0") (d (list (d (n "syntex_syntax") (r "^0.28.0") (d #t) (k 0)))) (h "079xlr6y7qz6ra9fc3dwwm75krsa0d4dsixl6kss6fgzn08k857k")))

(define-public crate-syntex-0.29.0 (c (n "syntex") (v "0.29.0") (d (list (d (n "syntex_syntax") (r "^0.29.0") (d #t) (k 0)))) (h "0563jpm5f0k9xnqf9d9dv7qfgf5pdfvzricdiw25ii1ywr4lg9na")))

(define-public crate-syntex-0.30.0 (c (n "syntex") (v "0.30.0") (d (list (d (n "syntex_syntax") (r "^0.30.0") (d #t) (k 0)))) (h "021vndx7q085a37yvdj4xyfpyjlbcg35faxqzkckh1jgbyw47z5s")))

(define-public crate-syntex-0.30.1 (c (n "syntex") (v "0.30.1") (d (list (d (n "syntex_syntax") (r "^0.30.0") (d #t) (k 0)))) (h "19fv5nikanz4d6ww4hrkam8m5948mdkq330xi80my19ghnbgvqd9")))

(define-public crate-syntex-0.29.2 (c (n "syntex") (v "0.29.2") (d (list (d (n "syntex_syntax") (r "^0.29.0") (d #t) (k 0)))) (h "0f7978daj49xf03v3plbvn1sry5vds03m2q87w3vwpnxzjg7vwji")))

(define-public crate-syntex-0.31.0 (c (n "syntex") (v "0.31.0") (d (list (d (n "syntex_syntax") (r "^0.31.0") (d #t) (k 0)))) (h "03n91q19cpgb6g2y5cww9srizvl3fpbs5s6z7xjwhif9d61wwd55")))

(define-public crate-syntex-0.32.0 (c (n "syntex") (v "0.32.0") (d (list (d (n "syntex_syntax") (r "^0.32.0") (d #t) (k 0)))) (h "03pccyzfc5sd192lmg6nlsywmfvnsdppymk7gaxsx4i85i5hg0hb")))

(define-public crate-syntex-0.33.0 (c (n "syntex") (v "0.33.0") (d (list (d (n "syntex_syntax") (r "^0.33.0") (d #t) (k 0)))) (h "1bw876jmbv8gmvp0simc0hb5ghkbvb7m9agfprjb1wlxi386sfrr")))

(define-public crate-syntex-0.34.0 (c (n "syntex") (v "0.34.0") (d (list (d (n "syntex_syntax") (r "^0.34.0") (d #t) (k 0)))) (h "08p9plxgvjxk1559pm74xjc8yyqnh94k32s62qpdcgz0ibgl1zs6")))

(define-public crate-syntex-0.35.0 (c (n "syntex") (v "0.35.0") (d (list (d (n "syntex_syntax") (r "^0.35.0") (d #t) (k 0)))) (h "1n5ydxrgw34vlhwc3a8h7zfxqpx7365wzp7xd7hgkb6psc7r27ff")))

(define-public crate-syntex-0.36.0 (c (n "syntex") (v "0.36.0") (d (list (d (n "syntex_syntax") (r "^0.36.0") (d #t) (k 0)))) (h "16g8ga2gp1iy86yssb1mx7h24b0ma3kh0rhdqlzdbp263sz0pp31")))

(define-public crate-syntex-0.37.0 (c (n "syntex") (v "0.37.0") (d (list (d (n "syntex_errors") (r "^0.37.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.37.0") (d #t) (k 0)))) (h "0pdaqw986443fab7x8dw1smy0g3i53xib56fh6bmc2qpwrlachj6")))

(define-public crate-syntex-0.38.0 (c (n "syntex") (v "0.38.0") (d (list (d (n "syntex_errors") (r "^0.38.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (d #t) (k 0)))) (h "1rr7z3imjir6l0pcx7vz5z6zp8vc0pg28gi0zqylj8yairy08vc9")))

(define-public crate-syntex-0.39.0 (c (n "syntex") (v "0.39.0") (d (list (d (n "syntex_errors") (r "^0.39.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (d #t) (k 0)))) (h "1g8w87vkl2a3af2j1lmhmnz3g3pf28plm2a0min7axyin67v9nlc")))

(define-public crate-syntex-0.40.0 (c (n "syntex") (v "0.40.0") (d (list (d (n "syntex_errors") (r "^0.40.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.40.0") (d #t) (k 0)))) (h "10zg26dj54whidg6wmk0phvaq93b0cy8rbsgx18yfxj6x7iqvbav")))

(define-public crate-syntex-0.41.0 (c (n "syntex") (v "0.41.0") (d (list (d (n "syntex_errors") (r "^0.41.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.41.0") (d #t) (k 0)))) (h "1v3r1r0hy8d6yq8974p7xpvzr69symmz7fghfrwy6x3ihq4b84gh")))

(define-public crate-syntex-0.42.0 (c (n "syntex") (v "0.42.0") (d (list (d (n "syntex_errors") (r "^0.42.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (d #t) (k 0)))) (h "0rl09q6ypnlns97lh077bfqj2an0wi7nzdmrx4bm9mcfb7ldws6z")))

(define-public crate-syntex-0.42.1 (c (n "syntex") (v "0.42.1") (d (list (d (n "syntex_errors") (r "^0.42.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (d #t) (k 0)))) (h "1f9wi7ad9k0jw9hn05z49mb83p0jhyw9spj9wqk13qy4vxasarwh")))

(define-public crate-syntex-0.42.2 (c (n "syntex") (v "0.42.2") (d (list (d (n "syntex_errors") (r "^0.42.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (d #t) (k 0)))) (h "05z3xd1f98qfl1wwl05v0pwlh3b7d78jghgdyvjj4fiqdf5b0c0a")))

(define-public crate-syntex-0.43.0 (c (n "syntex") (v "0.43.0") (d (list (d (n "syntex_errors") (r "^0.43.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.43.0") (d #t) (k 0)))) (h "18ssl02sikiynp3d9syaa3psphlv8bcv219aba38v2sbgdwg3v34")))

(define-public crate-syntex-0.44.0 (c (n "syntex") (v "0.44.0") (d (list (d (n "syntex_errors") (r "^0.44.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (d #t) (k 0)))) (h "1d5j47wayicjvw83zff3i1qzhp26jlxgfhc7qp52nxpfsya7pww4")))

(define-public crate-syntex-0.45.0 (c (n "syntex") (v "0.45.0") (d (list (d (n "syntex_errors") (r "^0.45.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.45.0") (d #t) (k 0)))) (h "0cxzvv36yrf0z5b1i3xy7d66zmpdz7lswhsrv25ypabh04cwn6l7")))

(define-public crate-syntex-0.45.1 (c (n "syntex") (v "0.45.1") (d (list (d (n "syntex_errors") (r "^0.45.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.45.1") (d #t) (k 0)))) (h "0sz552m8if62bp98hx8awgclab45mqwimyp3ak1ph1g1jrw32ipx")))

(define-public crate-syntex-0.46.0 (c (n "syntex") (v "0.46.0") (d (list (d (n "syntex_errors") (r "^0.46.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.46.0") (d #t) (k 0)))) (h "1znk4qwrgwkv5rc6qqnz3vdy7nw4yjjfbf3lbfsdrfflqvmla33f")))

(define-public crate-syntex-0.47.0 (c (n "syntex") (v "0.47.0") (d (list (d (n "syntex_errors") (r "^0.47.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.47.0") (d #t) (k 0)))) (h "0b7ch4y8jb032bp1r18vr273fh3m5yjihfrcwl1hn3c2jan2fmv1")))

(define-public crate-syntex-0.48.0 (c (n "syntex") (v "0.48.0") (d (list (d (n "syntex_errors") (r "^0.48.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (d #t) (k 0)))) (h "1jsxhg8gnn1g0zva53vck2v1fiw7m7lh9hsh2w0jv57lg2ny5qhp")))

(define-public crate-syntex-0.49.0 (c (n "syntex") (v "0.49.0") (d (list (d (n "syntex_errors") (r "^0.49.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.49.0") (d #t) (k 0)))) (h "0iql7c7ag8vgkhxyfrklhvhgv7hw01n720b1wrgzvkl0q8gqan8l")))

(define-public crate-syntex-0.50.0 (c (n "syntex") (v "0.50.0") (d (list (d (n "syntex_errors") (r "^0.50.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.50.0") (d #t) (k 0)))) (h "182d2fxn4w8ps0i21szsrknziixy1rpl4k9q6cx751ypsyq57liv")))

(define-public crate-syntex-0.51.0 (c (n "syntex") (v "0.51.0") (d (list (d (n "syntex_errors") (r "^0.51.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.51.0") (d #t) (k 0)))) (h "1adwlycbz2wbahikqmql7l004h8gs6641y993ihdq0rlj0al22pm")))

(define-public crate-syntex-0.52.0 (c (n "syntex") (v "0.52.0") (d (list (d (n "syntex_errors") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (d #t) (k 0)))) (h "0r90y25h62i09pydcc1hiipa0xmp25a252m30x06qnp2q7l6c6rv")))

(define-public crate-syntex-0.53.0 (c (n "syntex") (v "0.53.0") (d (list (d (n "syntex_errors") (r "^0.53.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.53.0") (d #t) (k 0)))) (h "0rcdw83051glww2mqf2gq6bc3j1b67947k9ngc9kf73ly4smd3sv")))

(define-public crate-syntex-0.54.0 (c (n "syntex") (v "0.54.0") (d (list (d (n "syntex_errors") (r "^0.54.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.54.0") (d #t) (k 0)))) (h "1y230j5bx9id9yrv1h59r7h4gcwih8139p044jc7arln79am4gxv")))

(define-public crate-syntex-0.55.0 (c (n "syntex") (v "0.55.0") (d (list (d (n "syntex_errors") (r "^0.55.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.55.0") (d #t) (k 0)))) (h "0lg31ivpjii2pn09g808338kw9a55f8jqm3ch5lp4hh8g4knw62v")))

(define-public crate-syntex-0.56.0 (c (n "syntex") (v "0.56.0") (d (list (d (n "syntex_errors") (r "^0.56.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.56.0") (d #t) (k 0)))) (h "17bnkm5bqzz04maik5fs8g9rfwj0r6vdlhard7anwiqkwa6325nj")))

(define-public crate-syntex-0.57.0 (c (n "syntex") (v "0.57.0") (d (list (d (n "syntex_errors") (r "^0.57.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.57.0") (d #t) (k 0)))) (h "1km1n4fn4fv7c7ypgm266y7w176b88531nl97qdrx8906nwxcg0s")))

(define-public crate-syntex-0.58.0 (c (n "syntex") (v "0.58.0") (d (list (d (n "syntex_errors") (r "^0.58") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58") (d #t) (k 0)))) (h "1mfnqrq4f23a0ffjwjm1kz973qkdrmjkp4xdxy7yy8v38jfwrwrm")))

(define-public crate-syntex-0.58.1 (c (n "syntex") (v "0.58.1") (d (list (d (n "syntex_errors") (r "^0.58") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58") (d #t) (k 0)))) (h "03lgd36cxhc6gzaab0wqvckbhml00s6s73lk34ymf6cklymf7xd8")))

