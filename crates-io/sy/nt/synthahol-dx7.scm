(define-module (crates-io sy nt synthahol-dx7) #:use-module (crates-io))

(define-public crate-synthahol-dx7-0.1.0 (c (n "synthahol-dx7") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nb66nks8msgbrwh3a17xfnyzxybxfrj5hskicxairdxm6x4fhv2")))

(define-public crate-synthahol-dx7-0.1.1 (c (n "synthahol-dx7") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18vsw9isly60qrcpf8q3mayfy6k3qdzaza445rxdqrszgr3cy2pa")))

