(define-module (crates-io sy nt syntastica-query-preprocessor) #:use-module (crates-io))

(define-public crate-syntastica-query-preprocessor-0.4.0 (c (n "syntastica-query-preprocessor") (v "0.4.0") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "lua-pattern") (r "^0.1.2") (f (quote ("to-regex"))) (d #t) (k 0)) (d (n "rsexpr") (r "^0.2.5") (f (quote ("comments"))) (d #t) (k 0)))) (h "0bx3v8lq1i1x13mn0nnla1p4w8m3yzsbn4j73d0pix7r7xcqz666")))

(define-public crate-syntastica-query-preprocessor-0.4.1 (c (n "syntastica-query-preprocessor") (v "0.4.1") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "lua-pattern") (r "^0.1.2") (f (quote ("to-regex"))) (d #t) (k 0)) (d (n "rsexpr") (r "^0.2.5") (f (quote ("comments"))) (d #t) (k 0)))) (h "18xk9rccr0yiyqv0awnkkpq1rpsm8f98nd0iqrp7zqxlc2wqdiis")))

