(define-module (crates-io sy nt synthahol-babylon) #:use-module (crates-io))

(define-public crate-synthahol-babylon-0.3.0 (c (n "synthahol-babylon") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "uom") (r "^0.34") (f (quote ("f64" "si" "std"))) (k 0)))) (h "00s8mcjqv4pj9jchyyidf9zhy6svc773miy55xnpli76s1aphyab")))

(define-public crate-synthahol-babylon-0.3.1 (c (n "synthahol-babylon") (v "0.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "uom") (r "^0.34") (f (quote ("f64" "si" "std"))) (k 0)))) (h "0vg74a5hy5dphb5j97q02052b3yypzn7cin5wb4837jaz5ql54kg")))

