(define-module (crates-io sy nt syntaxext_lint) #:use-module (crates-io))

(define-public crate-syntaxext_lint-0.1.0 (c (n "syntaxext_lint") (v "0.1.0") (h "0l47l8myljdky5925mx2fqyvqhgvpan6gq1b1dcrxh15p5zk893r")))

(define-public crate-syntaxext_lint-0.1.1 (c (n "syntaxext_lint") (v "0.1.1") (h "1pa9zmjdl57x4wkfr2ghygwf0vgq77aijnkf17s0q0shfyq4mpjq")))

(define-public crate-syntaxext_lint-0.1.2 (c (n "syntaxext_lint") (v "0.1.2") (h "0xr4qpcs0kiwi7zbq0z898lbnnspdd9jh18k33a7macq809dm5ck")))

(define-public crate-syntaxext_lint-0.1.3 (c (n "syntaxext_lint") (v "0.1.3") (h "0w9p6wppcziszzlzv8wbhz2y1d3gbdny61qdw3vjwc5aphj0lgxq")))

(define-public crate-syntaxext_lint-0.1.4 (c (n "syntaxext_lint") (v "0.1.4") (h "0dbsczj117x83s10zkvldnc0h9fsh212lbdkb8y24k716nqkj5xi")))

(define-public crate-syntaxext_lint-0.1.5 (c (n "syntaxext_lint") (v "0.1.5") (h "1nm9aljiyysr1gk1n9pgpqwfl7rbb0rf9ic2mqr7by9fajsi6rqc")))

(define-public crate-syntaxext_lint-0.1.6 (c (n "syntaxext_lint") (v "0.1.6") (h "05j1prpsqvm4gsw3ajdq6i49f6jnxxzjg44lc6m05n3frryaahq7")))

