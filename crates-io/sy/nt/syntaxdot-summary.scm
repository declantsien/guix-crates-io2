(define-module (crates-io sy nt syntaxdot-summary) #:use-module (crates-io))

(define-public crate-syntaxdot-summary-0.3.0 (c (n "syntaxdot-summary") (v "0.3.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.7") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "0nnlwnpfl9zhni1b6fhas79bhkgs8hv73qcciq7vw28qkximb0hj")))

(define-public crate-syntaxdot-summary-0.4.0 (c (n "syntaxdot-summary") (v "0.4.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.8") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "094qx7kmc5nj3cin30ikhr4ww50by3g85dlkda3c47gx3lks9n1l")))

(define-public crate-syntaxdot-summary-0.5.0-beta.0 (c (n "syntaxdot-summary") (v "0.5.0-beta.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "0mkbikizpbhcigzgxyhg8i1ijw090gxhc53fbadybfmg36v7hk3a")))

(define-public crate-syntaxdot-summary-0.5.0-beta.1 (c (n "syntaxdot-summary") (v "0.5.0-beta.1") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "1x8a3g04nhrj6sds4096s0kx20ysbyvv3pssx5nd5ixyw14gg0sk")))

(define-public crate-syntaxdot-summary-0.5.0-beta.2 (c (n "syntaxdot-summary") (v "0.5.0-beta.2") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "11knm10j5gzppv48y2jwyz1pqasxf2k7cwjdf8jgv7z5b84ad0ca")))

(define-public crate-syntaxdot-summary-0.5.0 (c (n "syntaxdot-summary") (v "0.5.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (f (quote ("prost-derive"))) (d #t) (k 0)))) (h "008wybdjk5wbff31klkwr6cv2fg5p1p66vmrbn3hbqsp29j5xicy")))

