(define-module (crates-io sy nt synthizer) #:use-module (crates-io))

(define-public crate-synthizer-0.1.0 (c (n "synthizer") (v "0.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "shrust") (r "^0.0.7") (d #t) (k 2)) (d (n "synthizer-sys") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1559f2b9lfm4cwb4isfx7x0zjviyv0511fb2lyw01j3q4c26x620")))

(define-public crate-synthizer-0.1.1 (c (n "synthizer") (v "0.1.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "shrust") (r "^0.0.7") (d #t) (k 2)) (d (n "synthizer-sys") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17m0wga12njgi8dsa9l8hymdkqki05gh9n2n56kawx8vi459dxhg")))

(define-public crate-synthizer-0.2.0 (c (n "synthizer") (v "0.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "shrust") (r "^0.0.7") (d #t) (k 2)) (d (n "synthizer-sys") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g6m9rvlk16iv4b7d0mlc6cy8i91y7jx7b4mgmpz9lg9yd70wiw0")))

(define-public crate-synthizer-0.3.0 (c (n "synthizer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0f76j34y4p6g9m59lhhlfv13v0szpfjvwmqz6bv9i46l7kcjfsqx")))

(define-public crate-synthizer-0.3.1 (c (n "synthizer") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.9.0") (d #t) (k 0)))) (h "11prpx6miq9a3j9z4b3cb8076nk12z37pss8jnckf637q6g5yrqy")))

(define-public crate-synthizer-0.3.2 (c (n "synthizer") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.9.0") (d #t) (k 0)))) (h "0inr4zdgq9ys8s2yidv3qppi8k3gankk8r972xww4pir5fx92qvp")))

(define-public crate-synthizer-0.3.3 (c (n "synthizer") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.9.1") (d #t) (k 0)))) (h "18pyks7d8x4z39dsz106ddf6vgza4364sqvvmm0kcqlnyxbzkjrw")))

(define-public crate-synthizer-0.3.4 (c (n "synthizer") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.9.2") (d #t) (k 0)))) (h "0pf5f99i0bh5plp77pmmdj2xfikgd4wawjmzhn07a5h0ij91bsd4")))

(define-public crate-synthizer-0.4.0 (c (n "synthizer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.0") (d #t) (k 0)))) (h "0ih97vqh7m5cp707y6pg7nw8dngg37qkkb32xnvgscy835aha7af")))

(define-public crate-synthizer-0.4.1 (c (n "synthizer") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.1") (d #t) (k 0)))) (h "1mydpsmyxppybk8f7iwcymgn07fg7zlgs8p9n7k2cvy2bdbzxfgn")))

(define-public crate-synthizer-0.4.2 (c (n "synthizer") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.2") (d #t) (k 0)))) (h "18rj8x2pbvan6b7zda0xlj848w602kiq4sc467sqba8vdkkqk2b5")))

(define-public crate-synthizer-0.4.3 (c (n "synthizer") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.3") (d #t) (k 0)))) (h "0v9bhlddrk82iqy155lbc9ccvzgr1nbxkzxcwgz71w92a0kkm41p")))

(define-public crate-synthizer-0.4.4 (c (n "synthizer") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.3") (d #t) (k 0)))) (h "03irf8d7k5ia7g5rj3v63iflfagqk09gaas6y8fzq0vqprabvki5")))

(define-public crate-synthizer-0.4.5 (c (n "synthizer") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.3") (d #t) (k 0)))) (h "07p6dndvm270mnmk4k1k36bvyybb4mgi2hfyl40cry4p1yzm2bw8")))

(define-public crate-synthizer-0.4.6 (c (n "synthizer") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.4") (d #t) (k 0)))) (h "046z6zj8q8qc1p2z33afnyamb8dxgxw7psljg4m1m028dbkbhnzk")))

(define-public crate-synthizer-0.4.7 (c (n "synthizer") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.4") (d #t) (k 0)))) (h "1j30a7y7630fxblr998qclgdg1h3s4y9nifi84rcgla9vbf8n7ad")))

(define-public crate-synthizer-0.5.0 (c (n "synthizer") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.4") (d #t) (k 0)))) (h "11jb7jwij9248r3kik2xhlyyf61y9la6jf5nc2jlrblqr5mdi2mc")))

(define-public crate-synthizer-0.5.1 (c (n "synthizer") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.5") (d #t) (k 0)))) (h "0az1nldhix666n4062wr1wbqjvn2p6nc7d61c8hsvx7zl691bci1")))

(define-public crate-synthizer-0.5.2 (c (n "synthizer") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.7") (d #t) (k 0)))) (h "1dbpz4mk8jqimnpn5m3mys860fyb67cmgvjvs92mrf2shxi7pz2f")))

(define-public crate-synthizer-0.5.3 (c (n "synthizer") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.8") (d #t) (k 0)))) (h "1y1bdbad18r1d572shqic8x48nizm688ddykksdqc73d1nwn8pxx")))

(define-public crate-synthizer-0.5.4 (c (n "synthizer") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.9") (d #t) (k 0)))) (h "1p2znj68idrks6v52zhw5pi7fkhp2ay91ndjx5hkvwr70h798a7z")))

(define-public crate-synthizer-0.5.5 (c (n "synthizer") (v "0.5.5") (d (list (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.9") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)))) (h "1h9c17vfs3p7mif31wc8xxbj2aldvpy9xppy05cgg5p0nlx97mkx")))

(define-public crate-synthizer-0.5.6 (c (n "synthizer") (v "0.5.6") (d (list (d (n "asset_lru") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "synthizer-sys") (r "^0.10.10") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)))) (h "1kd02zgg73xy5llvd07n91jvxz20wdybharz7s0kza4wp72v6biw")))

