(define-module (crates-io sy nt syntex_pos) #:use-module (crates-io))

(define-public crate-syntex_pos-0.37.0 (c (n "syntex_pos") (v "0.37.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1yhiwa9y8kbmcfah7bmwc28jx1jwbxpsk8v4rm7ca9dnjxjg6rlq")))

(define-public crate-syntex_pos-0.38.0 (c (n "syntex_pos") (v "0.38.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0haqw373rhvmpkl0il9a5y28g8n73vrpq2xy5bs8jrspdax0b7ic")))

(define-public crate-syntex_pos-0.39.0 (c (n "syntex_pos") (v "0.39.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "14mygrhlakw9rh80d55a3qbwim0nn0nkrrnaxd39ycmh64v2brgh")))

(define-public crate-syntex_pos-0.40.0 (c (n "syntex_pos") (v "0.40.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1jh4l948b9fwk8kj7b6pn89vb3pvjyy5bfbgl73xdj9wwyka0dvw")))

(define-public crate-syntex_pos-0.41.0 (c (n "syntex_pos") (v "0.41.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0zm9byw03hxypn3kj5qnb97qrgp48w54c385fny22hl0f4is3wbf")))

(define-public crate-syntex_pos-0.42.0 (c (n "syntex_pos") (v "0.42.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "19kij62spjzvls0bknvcpfvy3m6gnnmykjzyc4y82l94wn49km1z")))

(define-public crate-syntex_pos-0.43.0 (c (n "syntex_pos") (v "0.43.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0is2770bchkjd1r9y4q04aj447gfxidninhis6yw09l87vkvfmkf")))

(define-public crate-syntex_pos-0.44.0 (c (n "syntex_pos") (v "0.44.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1pv450ai690ixb1qipdjs3r45gwkgrplwahjr8nhz5y5k02vyb1y")))

(define-public crate-syntex_pos-0.45.0 (c (n "syntex_pos") (v "0.45.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1gcdr4dywmqw2mavp8mc3sqjjd760xq23mw6gc95rci1c2a4g814")))

(define-public crate-syntex_pos-0.46.0 (c (n "syntex_pos") (v "0.46.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "036dhv362c4gkx5igwi74pxbknkqpi6ik3v55a48b4bimm80k5q6")))

(define-public crate-syntex_pos-0.47.0 (c (n "syntex_pos") (v "0.47.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0hab1iqk8gj3x7dwy8d34f3gvdcz2c701knjwn2w08i4lzxc58r6")))

(define-public crate-syntex_pos-0.48.0 (c (n "syntex_pos") (v "0.48.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0g49lvgg2qcnd0xld89rrh41vxabgnya9phmsxn37fadbvrdzyi5")))

(define-public crate-syntex_pos-0.49.0 (c (n "syntex_pos") (v "0.49.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1r03fxwshaswam5g7iihyqj1v1ms82bccg1lvq5n8xhybfswyxr9")))

(define-public crate-syntex_pos-0.50.0 (c (n "syntex_pos") (v "0.50.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1ny7f1s8k0wpismv2rw287xjwl6f2g4nxr2mhnn5pispa3nvsfm4")))

(define-public crate-syntex_pos-0.51.0 (c (n "syntex_pos") (v "0.51.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0ld9r5g2i9k2j05j4x77g6jdkc6hzxg41dfk9ps4y271f8nrlkxx")))

(define-public crate-syntex_pos-0.52.0 (c (n "syntex_pos") (v "0.52.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1b0mbspiby8qkrj8109kgmaqya7zfgw9f50dd3j6ii7ldaqz8plm")))

(define-public crate-syntex_pos-0.53.0 (c (n "syntex_pos") (v "0.53.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "173cnpxcbhsb48p47ds0m9972vsna9lns6i48z2nr9saxw8m9cxl")))

(define-public crate-syntex_pos-0.54.0 (c (n "syntex_pos") (v "0.54.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0azw1qmbhkixkdhw6yghnmd3ncgl5kdmm8rsqpzvkps5g4f95wwd")))

(define-public crate-syntex_pos-0.55.0 (c (n "syntex_pos") (v "0.55.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1vp0kqr6f7v7f89wl96pifbh2sfnhap6m4izy9r1jzq1r0sbmdv0")))

(define-public crate-syntex_pos-0.56.0 (c (n "syntex_pos") (v "0.56.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0sln2rsfsahgsj1pipqr2a10vc0rnrvsg5zcab65pmrsk37i0n6m")))

(define-public crate-syntex_pos-0.57.0 (c (n "syntex_pos") (v "0.57.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1vjpdma2yimxdayk9wrs6na7cb6anf6jav25xb41g27v7jmpz1kr")))

(define-public crate-syntex_pos-0.58.0 (c (n "syntex_pos") (v "0.58.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0jx2r4942nq9xfkbv4k75wr13lgj105vpgaxjz7kg7sdj152ll0y")))

(define-public crate-syntex_pos-0.58.1 (c (n "syntex_pos") (v "0.58.1") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0iqhircpr723da1g97xrrj8smqqz3gxw91cf03sckasjzri4gb8k")))

(define-public crate-syntex_pos-0.59.0 (c (n "syntex_pos") (v "0.59.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0i5shcpi5z812a1himgir9m4i51ma6fjlrn1jxq8af2sfk5hihan")))

(define-public crate-syntex_pos-0.59.1 (c (n "syntex_pos") (v "0.59.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w3465vz8z08sbcva5hdalr3vk4idp3vnx7qh730ilh3l2gndarh")))

