(define-module (crates-io sy un syunit) #:use-module (crates-io))

(define-public crate-syunit-0.1.0 (c (n "syunit") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vjfxxgsrbc74g08y0cmyqa9jl35hhal7b0lk3qpj8nmyhg64r14")))

(define-public crate-syunit-0.1.1 (c (n "syunit") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kcmw4h8xjxwkwm6pd8c1x1wgw41xicjbjvixsry0hxhvzybj35s") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1.2 (c (n "syunit") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "011hq39mb6m78siq0dz0jzi4psxm3jc9l0dhd9dynzs7kz1n59qj") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1.3 (c (n "syunit") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cp8x1sz8ixc395bs417vnb7bfjvwjdpkwc3ljzqfc5ig9m5zp4x") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1.4 (c (n "syunit") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0frbaib5hd3x7fjkp8xcdj1cs5dbqjlvqgan1ygv7nbkvw2xk031") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1.5 (c (n "syunit") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lagcdxjrn1h26rk0234zirf226qj3bvy4qbj0hwhz14cxc75wwm") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1.6 (c (n "syunit") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "102x3gbkgiirqy3md36igqqxxmfp2wz04rbi1yyaliylx2qd7jgk") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.2.0 (c (n "syunit") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1di8sblf4hb4vydxw63jfr3bjkpnhjr2v942py4cgfp0xm8f1bz6") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.2.1 (c (n "syunit") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hpzqwxilp7ibjclc2ybliwfglh8qiwbvbsrraws8w2r05874fl5") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

