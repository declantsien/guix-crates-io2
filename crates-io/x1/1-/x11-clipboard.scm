(define-module (crates-io x1 #{1-}# x11-clipboard) #:use-module (crates-io))

(define-public crate-x11-clipboard-0.1.0 (c (n "x11-clipboard") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("thread"))) (d #t) (k 0)))) (h "1j200c337vh2ysprahip0gi12sx03ryqw90p6g5j2lj2bw27hisw") (y #t)))

(define-public crate-x11-clipboard-0.1.1 (c (n "x11-clipboard") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("thread"))) (d #t) (k 0)))) (h "07bn2igg8sizgighaiq0h5zl6r9p45hm4rd2vvblkpdh1wqmb9zm") (y #t)))

(define-public crate-x11-clipboard-0.1.2 (c (n "x11-clipboard") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("thread"))) (d #t) (k 0)))) (h "16gw8cppihj1d47f466bv2qd40dglpykwx8haqmixsgyk4rf2ikb") (y #t)))

(define-public crate-x11-clipboard-0.1.3 (c (n "x11-clipboard") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("thread"))) (d #t) (k 0)))) (h "11dy2nlsd75g4alkja420llcq4p2x2hwrxx0kj5wbq6akf1lf106")))

(define-public crate-x11-clipboard-0.1.4 (c (n "x11-clipboard") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("thread"))) (d #t) (k 0)))) (h "0wr9vgwra8b6m2px4s5da16mjf55q2g4qphh8y9dkffbxnw304kk")))

(define-public crate-x11-clipboard-0.2.0 (c (n "x11-clipboard") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("thread"))) (d #t) (k 0)))) (h "1l97hcw5z7sbjkajp43k71fmd66cbr1g7cbaf3h9ayyig7cmr8vq")))

(define-public crate-x11-clipboard-0.2.1 (c (n "x11-clipboard") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("thread"))) (d #t) (k 0)))) (h "0xp1zd4kj3vg3q19zialwlv61rmjy8aqz1cvwnmd6mys09s047rw")))

(define-public crate-x11-clipboard-0.2.2 (c (n "x11-clipboard") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("thread"))) (d #t) (k 0)))) (h "1pyikg5f8bxfp668148k9msgqh4nw1biv9jc12kwq44jd73p8wrf")))

(define-public crate-x11-clipboard-0.3.0-alpha.1 (c (n "x11-clipboard") (v "0.3.0-alpha.1") (d (list (d (n "xcb") (r "^0.8") (f (quote ("thread"))) (d #t) (k 0)))) (h "1cpkljg54kq71s7q0dppxj8ljz0i1gvrvdb65mnz4mvs5wyni2p2")))

(define-public crate-x11-clipboard-0.3.0 (c (n "x11-clipboard") (v "0.3.0") (d (list (d (n "xcb") (r "^0.8") (f (quote ("thread"))) (d #t) (k 0)))) (h "019f1v0k5p8qnlhylpwsy2vq571wsyb685gzaz7myvn9hmhpqqfq")))

(define-public crate-x11-clipboard-0.3.1 (c (n "x11-clipboard") (v "0.3.1") (d (list (d (n "xcb") (r "^0.8") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "1ld8s391l7784m6gw9qv5zs7iqz3x3008sb8fzr9zckmzwqrf4ha")))

(define-public crate-x11-clipboard-0.3.2 (c (n "x11-clipboard") (v "0.3.2") (d (list (d (n "xcb") (r "^0.8") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "1lw7jp184i6p0wa3hbmswnrzvly8l5gisz2sw5kq4fd16mikaxrs")))

(define-public crate-x11-clipboard-0.3.3 (c (n "x11-clipboard") (v "0.3.3") (d (list (d (n "xcb") (r "^0.8") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "1smwyr23jns0dncm6bwv00xfxxy99bv6qlx6df7dkdcydk04kgc9")))

(define-public crate-x11-clipboard-0.4.0 (c (n "x11-clipboard") (v "0.4.0") (d (list (d (n "xcb") (r "^0.9") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "0nqdnswiyj28b1izjp5rzbc67cxpb5c8p4vh1xyndkirzs84vqqk")))

(define-public crate-x11-clipboard-0.5.0 (c (n "x11-clipboard") (v "0.5.0") (d (list (d (n "xcb") (r "^0.9") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "1xpajbj9ya87wdiw5sas6dncd7cbp13s4qzz4r3yg85wdn062w3s") (y #t)))

(define-public crate-x11-clipboard-0.5.1 (c (n "x11-clipboard") (v "0.5.1") (d (list (d (n "xcb") (r "^0.9") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "17c5yxxhknrp7y9mc7mp85ra8q4jw12c174m9yzbfr1vs2pkgsg5")))

(define-public crate-x11-clipboard-0.5.2 (c (n "x11-clipboard") (v "0.5.2") (d (list (d (n "xcb") (r "^0.9") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "0cjzbl1vd9mbl6gwwiypsqqv5ynzb33x9qsgkbjhslc0x7kar5xk")))

(define-public crate-x11-clipboard-0.5.3 (c (n "x11-clipboard") (v "0.5.3") (d (list (d (n "xcb") (r "^0.10.1") (f (quote ("thread" "xfixes"))) (d #t) (k 0)))) (h "0g35qcmy7ayp3clbqnvvqjc7h02y9q2z294868c6mj0ap2vnhc27")))

(define-public crate-x11-clipboard-0.6.0 (c (n "x11-clipboard") (v "0.6.0") (d (list (d (n "xcb") (r "^1.1.1") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "124pnczcm8r9sij4mr0641vbk62ha3gnprlbqj50dj88l444fs4w")))

(define-public crate-x11-clipboard-0.6.1 (c (n "x11-clipboard") (v "0.6.1") (d (list (d (n "xcb") (r "^1.1.1") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "1s1p2cg2i83w41kw5anfmij1l06psrh4n3ccdhz4gslgfsjnhx3a")))

(define-public crate-x11-clipboard-0.7.0 (c (n "x11-clipboard") (v "0.7.0") (d (list (d (n "x11rb") (r "^0.10.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "15a6nm78np9d7h1byw130i5vp3p8mgg1kxm454rygi0hm5mgh9q8")))

(define-public crate-x11-clipboard-0.7.1 (c (n "x11-clipboard") (v "0.7.1") (d (list (d (n "x11rb") (r "^0.10.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "0r3lgslbbdf0mb914n0f9q2pqci407r1pcddwbl7sfvc4alrl2wq")))

(define-public crate-x11-clipboard-0.8.0 (c (n "x11-clipboard") (v "0.8.0") (d (list (d (n "x11rb") (r "^0.12.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "0b4p44lnnlf9alwfk3ca1hfd2dqp9gf44w5il29kk4wqkax6hsjf")))

(define-public crate-x11-clipboard-0.8.1 (c (n "x11-clipboard") (v "0.8.1") (d (list (d (n "x11rb") (r "^0.12.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "1ps0fk1912vzy382fc8l926q8w1l8bxmw72l3kr9bwdi2l8wl6ml")))

(define-public crate-x11-clipboard-0.9.0 (c (n "x11-clipboard") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "x11rb") (r "^0.13.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "0f09v1dj29kzjqbngiarpn28njlhhfdbm3yv74c7z1vak9mpw1fn")))

(define-public crate-x11-clipboard-0.9.1 (c (n "x11-clipboard") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "x11rb") (r "^0.13.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "11zh1r18rlnflanp22n0p9lccpjzcw9l9af5ayxjpbvjwzijng31")))

(define-public crate-x11-clipboard-0.9.2 (c (n "x11-clipboard") (v "0.9.2") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "x11rb") (r "^0.13.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "11l27l15y8hadnq2rcyzsc6rl1g1rb906cm151p49mr2jfh8b1xr")))

