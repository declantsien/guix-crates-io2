(define-module (crates-io x1 #{1-}# x11-dl) #:use-module (crates-io))

(define-public crate-x11-dl-1.0.0 (c (n "x11-dl") (v "1.0.0") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "15sxm5rjnmran8x8a5a28jcircj2laavzwrvpmg7c3h7h86m76ad") (y #t)))

(define-public crate-x11-dl-1.0.1 (c (n "x11-dl") (v "1.0.1") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "03agr2p85pgzdzxn1zxk0yypa40l372d7ncr90m6pxd88gdf06zw")))

(define-public crate-x11-dl-1.1.0 (c (n "x11-dl") (v "1.1.0") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0n9z909ckd39cm908rxwk87ymz4mx2wl1iidrbbxzhpxbabrf3gw")))

(define-public crate-x11-dl-2.0.0 (c (n "x11-dl") (v "2.0.0") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "06wxispah1b65myscxqnnysydn12y9rwwp2cascvcfn67b3axzyl")))

(define-public crate-x11-dl-2.0.1 (c (n "x11-dl") (v "2.0.1") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1iwlyzmk3z5lfvcbgzqjh816ih4j384x874vhg5g4cn2nvpklph9")))

(define-public crate-x11-dl-2.1.0 (c (n "x11-dl") (v "2.1.0") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0jg959pdf1w0k18dcrz724574p62icgxdkxlyr9agamlkcxak636")))

(define-public crate-x11-dl-2.2.0 (c (n "x11-dl") (v "2.2.0") (d (list (d (n "dylib") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0gpyzxf0kvp3qvc143miivb3vpv47znq8wi83sm95virx31n3wqs")))

(define-public crate-x11-dl-2.2.1 (c (n "x11-dl") (v "2.2.1") (d (list (d (n "dylib") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "05njbz9q1gi36a3x401kfjqy3rc9hf5sqjfb02n32q5yp2vvi79l")))

(define-public crate-x11-dl-2.3.0 (c (n "x11-dl") (v "2.3.0") (d (list (d (n "dylib") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00inhbxni7r4q2rwpbpdpn266liacqm7q1pp51dfln9c7764z8hm")))

(define-public crate-x11-dl-2.3.1 (c (n "x11-dl") (v "2.3.1") (d (list (d (n "dylib") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l9a7gzgsm5l4y5755id97sdy6bc05xyns0p5dfhpmv0r93pp2rk")))

(define-public crate-x11-dl-2.4.0 (c (n "x11-dl") (v "2.4.0") (d (list (d (n "dylib") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11c5iqkgxmx2b01rf337vh1z75dpippmvza42nfp7hk5542nxbgx")))

(define-public crate-x11-dl-2.5.0 (c (n "x11-dl") (v "2.5.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ikmv3fn2w3pslivsi7xmrn0c44viljvgfqnqfnjc8ikynf0vcn8")))

(define-public crate-x11-dl-2.5.1 (c (n "x11-dl") (v "2.5.1") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a6fia65db3di8n4p6mymnlwhzh49nfb1l31akj661zhpg0iszc7")))

(define-public crate-x11-dl-2.6.0 (c (n "x11-dl") (v "2.6.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i75qwsyjf9zj1dm940810rv1qbpjnz3iabv9pahjcy3if4k7dlj")))

(define-public crate-x11-dl-2.6.1 (c (n "x11-dl") (v "2.6.1") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1583832pnijjmfgbfyjdsd65dfprp881shnhdx4gyhs5224s77mk")))

(define-public crate-x11-dl-2.7.0 (c (n "x11-dl") (v "2.7.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12hrp705rr2whlksvqzdcgng4bg73mldny26mi0z021lrq50prpq")))

(define-public crate-x11-dl-2.8.0 (c (n "x11-dl") (v "2.8.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0sqd9dsdjyancx7whkig1ifijjm97a9kw6y7iphnaxcdr6yjkk3a")))

(define-public crate-x11-dl-2.9.0 (c (n "x11-dl") (v "2.9.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "193f63xf3rf9lyqdawkm3bqwiad4qskan38akk9srykjaxbhhc1y")))

(define-public crate-x11-dl-2.10.0 (c (n "x11-dl") (v "2.10.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1654lb92f93l5nvljhg3g2l0hp0hbv7fb0znndwkh4vf16w0c50q")))

(define-public crate-x11-dl-2.11.0 (c (n "x11-dl") (v "2.11.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1pgb861cir465wr7rm74gnwadg89jpdbn8xawjys2qdqgw57yk2f")))

(define-public crate-x11-dl-2.12.0 (c (n "x11-dl") (v "2.12.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "170ki8lb6agqfg41d1ilsg3ckq22xdy3ly0s37cbv6wc6s39j7xz")))

(define-public crate-x11-dl-2.13.0 (c (n "x11-dl") (v "2.13.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "07ignqi1lnkr955hhphld7hn0bn6z0vqx30b1m22gav6rclz5y57")))

(define-public crate-x11-dl-2.14.0 (c (n "x11-dl") (v "2.14.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1y7sczf9nayy2xcc562xbazff32wrllsij6q1p3xfvqnvh650v1j")))

(define-public crate-x11-dl-2.15.0 (c (n "x11-dl") (v "2.15.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "09ili89lb8iazp8m2v633lzrsgvjbqcjdki29fnxhlh6xnrjzyl1")))

(define-public crate-x11-dl-2.16.0 (c (n "x11-dl") (v "2.16.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "02w2033k30kxnl84gih54vanhvsri3648y2w2vp55r8z0v29qh6x")))

(define-public crate-x11-dl-2.17.0 (c (n "x11-dl") (v "2.17.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0npga9zjc77wa79mqircyx7ysx69a6djypk597sbvmjzw0ivpzxj")))

(define-public crate-x11-dl-2.17.1 (c (n "x11-dl") (v "2.17.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0fzs4dfvck38yds913b1l8rjchnjwlzpqg7lhwzbg8hxp55bd13g")))

(define-public crate-x11-dl-2.17.2 (c (n "x11-dl") (v "2.17.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1670jmgqn8a1x4kglxbwkivppq206x551c43wvgxpkr864351v18")))

(define-public crate-x11-dl-2.17.3 (c (n "x11-dl") (v "2.17.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0gjb97c0f6zg331apjkvyvmq4j92p7nk5j72zw8mb7i3ldjqmrr9")))

(define-public crate-x11-dl-2.17.4 (c (n "x11-dl") (v "2.17.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0pxj4ljfiw93vm1z9jxphyrp8v08il0346dyd17nq6blxwikl2hx") (y #t)))

(define-public crate-x11-dl-2.17.5 (c (n "x11-dl") (v "2.17.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0zfcgcn7rj5q2zpf3ipi0w20f3kc2ra91x4d9h3sxqgx802m8d9j")))

(define-public crate-x11-dl-2.18.0 (c (n "x11-dl") (v "2.18.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1sgi3pjsi58akg9xl82rzl52ix5fv1rg68n4y0x18wcpnj28a7ck") (y #t)))

(define-public crate-x11-dl-2.18.1 (c (n "x11-dl") (v "2.18.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0khc6sm7nikhqdsbx7xam3d6a0jw51xjin9xpmrxal8y57lphvwn")))

(define-public crate-x11-dl-2.18.2 (c (n "x11-dl") (v "2.18.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0i8by7c3dm48h6kfmpsy1xfyk3iiwpnw4jw9nw5g28v0mw3fpjsq")))

(define-public crate-x11-dl-2.18.3 (c (n "x11-dl") (v "2.18.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1qxl0rpdjshfrjyc0sp6vv0px6krhlb27b3i772hbsjrp2n8c1cl")))

(define-public crate-x11-dl-2.18.4 (c (n "x11-dl") (v "2.18.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0n1w837xagxqgwx2880d7c9ks6l3g1kk00yd75afdaiv58sf2rdy")))

(define-public crate-x11-dl-2.18.5 (c (n "x11-dl") (v "2.18.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1y7yq4sfvv56shk4v3s7gvlrwk9d0migj622fl4i4c5klpiq3y9b")))

(define-public crate-x11-dl-2.19.0 (c (n "x11-dl") (v "2.19.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "18qzpi1f9h7qgrsbxxj148gpv9qs4rvk9gjyhq9jh1591n0ixavy")))

(define-public crate-x11-dl-2.19.1 (c (n "x11-dl") (v "2.19.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0ncc0f0cm6b0zylgl6saw5m7r8z50720nfhgkmfny6p89imr49pa")))

(define-public crate-x11-dl-2.20.0 (c (n "x11-dl") (v "2.20.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "19ly8j6gh29bvd9wjdzw46v61x4fj1xrqfdv0365lq1pq5xn50qc")))

(define-public crate-x11-dl-2.20.1 (c (n "x11-dl") (v "2.20.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1w20mh07qaacqckgbga76ggb5l5wbv0s4wzgqxrybm55cmlnslxi")))

(define-public crate-x11-dl-2.21.0 (c (n "x11-dl") (v "2.21.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0vsiq62xpcfm0kn9zjw5c9iycvccxl22jya8wnk18lyxzqj5jwrq")))

