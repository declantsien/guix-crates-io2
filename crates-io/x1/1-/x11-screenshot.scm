(define-module (crates-io x1 #{1-}# x11-screenshot) #:use-module (crates-io))

(define-public crate-x11-screenshot-0.1.0 (c (n "x11-screenshot") (v "0.1.0") (d (list (d (n "image") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "x11") (r "^2.16.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1gbildgc1akmlk8729n4m4k61hjpxnhdqvdgsmdrvlysdzfg4skj")))

(define-public crate-x11-screenshot-0.2.0 (c (n "x11-screenshot") (v "0.2.0") (d (list (d (n "image") (r "^0.21.0") (k 0)) (d (n "x11") (r "^2.16.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0b5z7l1gzhf5iv1vk05d797wq2dkrnmy52ss4ckqd8d4mjsbw1ma")))

(define-public crate-x11-screenshot-0.2.1 (c (n "x11-screenshot") (v "0.2.1") (d (list (d (n "image") (r "^0.23.12") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "113iacg3dr0cfzkk22cjvx7ryldds250hpi9wm9a5m9xkqyghspr")))

