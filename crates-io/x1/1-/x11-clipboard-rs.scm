(define-module (crates-io x1 #{1-}# x11-clipboard-rs) #:use-module (crates-io))

(define-public crate-x11-clipboard-rs-0.1.0 (c (n "x11-clipboard-rs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "03rdrvzizbjwcdgk6pfczbcb2w0zz4ylz3svz8prqibcp14f21fl")))

(define-public crate-x11-clipboard-rs-0.1.1 (c (n "x11-clipboard-rs") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1g4yd7sb186sbcm2bky9l2lngnshm0ndcdmw20w3f18vpabzskyh")))

