(define-module (crates-io x1 #{1-}# x11-keysymdef) #:use-module (crates-io))

(define-public crate-x11-keysymdef-0.2.0 (c (n "x11-keysymdef") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 1)))) (h "0n5jf74wpg7lpilsy6f96ix20xnxxira5pg7gmm5s02l6nqidx6v")))

