(define-module (crates-io x1 #{1-}# x11-input-mirror) #:use-module (crates-io))

(define-public crate-x11-input-mirror-0.1.0 (c (n "x11-input-mirror") (v "0.1.0") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0i998wa6v8xggvdikiq3ldiyd71ixjli8ygvjj8jixhhmbv2cxgp")))

(define-public crate-x11-input-mirror-0.2.0 (c (n "x11-input-mirror") (v "0.2.0") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "11rs73ndgvd6qfsbavmbhbkzjl0r5mbp11h0swsy4yyvz0xs86wy")))

(define-public crate-x11-input-mirror-0.2.1 (c (n "x11-input-mirror") (v "0.2.1") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0p4x9b6i1sg5cpb9g0v0ipzhgx5xkhgq3888f4jq179i72bk79mz")))

(define-public crate-x11-input-mirror-0.2.2 (c (n "x11-input-mirror") (v "0.2.2") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1kb9lji0fxx0kppcj6ia1065scys84qs38fkah0598ssdsyyga6j")))

(define-public crate-x11-input-mirror-0.3.0 (c (n "x11-input-mirror") (v "0.3.0") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0p6jmaab1kgfikh8in39p0f9kg2k8bhhhsvr6qzy3lgqn6srs38y") (y #t)))

(define-public crate-x11-input-mirror-0.3.1 (c (n "x11-input-mirror") (v "0.3.1") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "0ps4az6lvk7y7dkk5i7jd1rbjah0qya67c2yh2ammdr7avchnwj6")))

(define-public crate-x11-input-mirror-0.3.2 (c (n "x11-input-mirror") (v "0.3.2") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1ijg4i47bkx15w10366bw58nsgrfaws3lbqqlyaxwx5apdpx8z72")))

(define-public crate-x11-input-mirror-0.3.3 (c (n "x11-input-mirror") (v "0.3.3") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1idc7qb51cxxr2vfygf2vjqd3bxb5slv7qqinmq1aiazn3xvl3jb")))

(define-public crate-x11-input-mirror-0.3.4 (c (n "x11-input-mirror") (v "0.3.4") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "04d3hy18dmli9hdii32z4v3g4fzlgnzwyrm81zflf1b66r471v3v")))

(define-public crate-x11-input-mirror-0.3.5 (c (n "x11-input-mirror") (v "0.3.5") (d (list (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "01scifjfxb1cyxzq04mndxlc9gj543v89pczizf1h6szmpbb5wga")))

(define-public crate-x11-input-mirror-0.3.6 (c (n "x11-input-mirror") (v "0.3.6") (d (list (d (n "chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.86") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.86") (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0yf945nl6742hbymw6mq1agvfdji0y3dww4hb45y625l2spvgij3")))

