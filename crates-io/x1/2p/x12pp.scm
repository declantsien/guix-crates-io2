(define-module (crates-io x1 #{2p}# x12pp) #:use-module (crates-io))

(define-public crate-x12pp-0.1.0 (c (n "x12pp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0nxwy64fichsb0a4fh8062r8barc0fd72ws7daz6j1krb9yyvmpw")))

(define-public crate-x12pp-0.2.0 (c (n "x12pp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0ggm821py3l6lc6djj04ivf6bxyfirliry31yy8inaga6jrmmdz5")))

(define-public crate-x12pp-0.3.0 (c (n "x12pp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("wrap_help"))) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "1yv2m90wh5v1nvxga958zqi56r1w173vh5cv6hrhpbis2s6f4zk7")))

