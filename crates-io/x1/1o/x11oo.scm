(define-module (crates-io x1 #{1o}# x11oo) #:use-module (crates-io))

(define-public crate-x11oo-0.1.0 (c (n "x11oo") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0s7wfyp1vwggdkvhdyjgxf5qzk7qnxb8hzryyjml09m4wrwxczgv") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.1.1 (c (n "x11oo") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0320ysxrdp5xqvkc5p3r3k1jkhdxmihycil96xidr5pwhapjy5hd") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.1.2 (c (n "x11oo") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0qq5qwpl6gzxrlnfnzkbjic1l3xkxpwp1z19x9z76r2nmhcway5y") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.1.3 (c (n "x11oo") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1ncyasrn0f5zy2qyagjq71xxxffax17bp87c1d004clp7jk9yqf9") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.1.4 (c (n "x11oo") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0k4bzw5zr6iczjap8c8ncvghlnr85r57kx7fvz1n5grki1z865c8") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.2.0 (c (n "x11oo") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0k6nniz6aw9yy9b0y2j8x429hq6kd2kh7w8z3g4kifrk0zrvbz8w") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.2.2 (c (n "x11oo") (v "0.2.2") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0m5xda6dv1wc68dmk8zb8vrlzd2i2hz4r0djw5azj5skd08hqv4s") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.2.3 (c (n "x11oo") (v "0.2.3") (d (list (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)) (d (n "x11") (r "^2.20.1") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0r6bbaxc3rsndvmi85ivwc0xi5x3mwrrj69s1v18krv0wf1p5vq3") (f (quote (("xfixes"))))))

(define-public crate-x11oo-0.3.0 (c (n "x11oo") (v "0.3.0") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1yjmp9ciab35985y9aggmjh84nc7hsqf1f50xycvqy41dl8xw42z") (f (quote (("xfixes")))) (y #t)))

(define-public crate-x11oo-0.4.0 (c (n "x11oo") (v "0.4.0") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1dhcv44bnbvlv6lbbx78ji1p936l8g8ssbqg3b7dgwf424ncrvfp") (f (quote (("xfixes" "x11/xfixes")))) (y #t)))

(define-public crate-x11oo-0.4.1 (c (n "x11oo") (v "0.4.1") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0q8b3kn517w6q32d101llxbmpsa1qsk8l5vvgwqdm8g2kj5r7kx8") (f (quote (("xfixes" "x11/xfixes"))))))

(define-public crate-x11oo-0.4.2 (c (n "x11oo") (v "0.4.2") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0ina23gbsbckpj5lfmi1692j18yj0jfagf3lrivsmzlic78h21xv") (f (quote (("xfixes" "x11/xfixes"))))))

