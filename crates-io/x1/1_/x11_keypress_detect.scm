(define-module (crates-io x1 #{1_}# x11_keypress_detect) #:use-module (crates-io))

(define-public crate-x11_keypress_detect-0.1.0 (c (n "x11_keypress_detect") (v "0.1.0") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)))) (h "08qp91xw05599rdl5r218i6vgnsa6887frvc26vk6lqv74skm9xa") (y #t)))

(define-public crate-x11_keypress_detect-0.1.1 (c (n "x11_keypress_detect") (v "0.1.1") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)))) (h "1gdlgdjfa894f1p0gk366pjxaqzbwgc2i7gs9m6zcv96zv7xficw") (y #t)))

(define-public crate-x11_keypress_detect-0.2.1 (c (n "x11_keypress_detect") (v "0.2.1") (d (list (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1richdn8lzasvqk7ckgj7srgb1x350b9qsw3dqrwd9mvc1wmmqjg") (y #t)))

(define-public crate-x11_keypress_detect-0.2.2 (c (n "x11_keypress_detect") (v "0.2.2") (d (list (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0smryhgwxv4347mlfkhjbyxs21gv22h1x2z7ycikfm93vys8j1lg")))

