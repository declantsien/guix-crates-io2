(define-module (crates-io x1 #{2-}# x12-types-macros) #:use-module (crates-io))

(define-public crate-x12-types-macros-0.1.0 (c (n "x12-types-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1n965vs6fg1dp8rxc2iy8sd9ghizgk0c89l6lyk2w6iqsmhfwizr")))

(define-public crate-x12-types-macros-0.2.0 (c (n "x12-types-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0idrdiy6v82va5wlczvhdqxri6qndkhcif5dzdrqnlkya5g7qwk4")))

