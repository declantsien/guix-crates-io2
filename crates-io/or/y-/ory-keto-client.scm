(define-module (crates-io or y- ory-keto-client) #:use-module (crates-io))

(define-public crate-ory-keto-client-0.0.0-alpha.55 (c (n "ory-keto-client") (v "0.0.0-alpha.55") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "12w1axhx3rcqiagw1z8lhni8ldnnidjwdb12m4ik4zqy476mhq2v")))

(define-public crate-ory-keto-client-0.6.0-alpha.6 (c (n "ory-keto-client") (v "0.6.0-alpha.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11v55bryh05mrf3hpdc6rn2dasnwrmw4zvnzasghj0vqi32x09gd")))

(define-public crate-ory-keto-client-0.7.0-alpha.1 (c (n "ory-keto-client") (v "0.7.0-alpha.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0f2x7xv4vppcm41lvdrkq83c31hkjzsi84pkrn2f61lsdfqi81yv")))

(define-public crate-ory-keto-client-0.8.0-alpha.1 (c (n "ory-keto-client") (v "0.8.0-alpha.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qdxknbdnr7lkb9b066r48zvszmkzwz495qim12pwwinsa17mdn7")))

(define-public crate-ory-keto-client-0.8.0-alpha.2 (c (n "ory-keto-client") (v "0.8.0-alpha.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jr9dclfdngv27hbjrvc553lxpj3xcwjrj5y74j682qb75fa47d3")))

(define-public crate-ory-keto-client-0.9.0-alpha.0 (c (n "ory-keto-client") (v "0.9.0-alpha.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1lvvb5i1km2vvwzgbn7wmjxpjw7sjj44zzf1n98c1k8nxsgnb8hk")))

(define-public crate-ory-keto-client-0.11.0-alpha.0 (c (n "ory-keto-client") (v "0.11.0-alpha.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "08xfd91598kdx4zmxcbf8v759gm9lzcvgn0yiv4m4vnbkyxg8i29")))

