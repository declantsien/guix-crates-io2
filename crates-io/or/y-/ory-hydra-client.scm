(define-module (crates-io or y- ory-hydra-client) #:use-module (crates-io))

(define-public crate-ory-hydra-client-1.10.3 (c (n "ory-hydra-client") (v "1.10.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "08j2vpp4pyf7xaxvw61dzd8a6qsll0b6n3xvr83wwahhiv93anj8")))

(define-public crate-ory-hydra-client-1.10.5 (c (n "ory-hydra-client") (v "1.10.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0chn2y55i2ssnc3db98j735pfp868zlzcr0rck621wqxvqknnbkl")))

(define-public crate-ory-hydra-client-1.10.6 (c (n "ory-hydra-client") (v "1.10.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1k2a4x49fdn5ycacgbvhlnvdqgcq4xlfryyg201kgi2ivkgxckmr")))

(define-public crate-ory-hydra-client-1.11.4 (c (n "ory-hydra-client") (v "1.11.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1mqlrnnvwyp23s39dfgg0g4djbd665iwhqiqln7kim38pp9libdc")))

(define-public crate-ory-hydra-client-1.11.5 (c (n "ory-hydra-client") (v "1.11.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1zf6xgrwj45qh7zzlrhrm8jishb0y4hd93n8rpn74kv4d3yi6zvj")))

(define-public crate-ory-hydra-client-1.11.6 (c (n "ory-hydra-client") (v "1.11.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1n4pyi0c74gdp2rmgab07lihnr2fx8cfk1x7q8iwqj5h0088dw7c")))

(define-public crate-ory-hydra-client-1.11.7 (c (n "ory-hydra-client") (v "1.11.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1ay530var5cqicr41xg3wghvlib6dynj0k4ab9gsvy8dnn0js9l9")))

(define-public crate-ory-hydra-client-1.11.8 (c (n "ory-hydra-client") (v "1.11.8") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0fjj3yw3305gapiy60z5pc4g2fqwazk18w1iych3gvga2yx2caal")))

(define-public crate-ory-hydra-client-2.1.0 (c (n "ory-hydra-client") (v "2.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1gylzzj1lcw0lniqc0w8dpfx5q7vaq87bbp7sng5bnnkghqyb12x")))

(define-public crate-ory-hydra-client-2.1.1 (c (n "ory-hydra-client") (v "2.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1583bihanvar93xljzd3rlyllis9lg969jdhfqmb1j6zfhqqxq0z")))

(define-public crate-ory-hydra-client-2.2.0-rc.3 (c (n "ory-hydra-client") (v "2.2.0-rc.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "077lz76dhzf2lf9an4ifyxbarym2laprgz63i3adjfix7mb22z62")))

(define-public crate-ory-hydra-client-2.2.0 (c (n "ory-hydra-client") (v "2.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0gxdfbvwrha33d8xqp446019x99mjzh27cjp3ivm9a7m8zwgy3lx")))

