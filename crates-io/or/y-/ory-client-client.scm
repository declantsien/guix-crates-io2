(define-module (crates-io or y- ory-client-client) #:use-module (crates-io))

(define-public crate-ory-client-client-0.0.1-alpha.1 (c (n "ory-client-client") (v "0.0.1-alpha.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0kzxvvjklz2gbx596jl0v11ik60ka2mvhjzfl9v06225spv5mccs")))

