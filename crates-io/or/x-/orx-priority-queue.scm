(define-module (crates-io or x- orx-priority-queue) #:use-module (crates-io))

(define-public crate-orx-priority-queue-0.1.0 (c (n "orx-priority-queue") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04nqkii5xgzgqx7fz9kljl2pyqlws08vvg6grhj3x4jprinc4bx0")))

(define-public crate-orx-priority-queue-0.1.1 (c (n "orx-priority-queue") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0n7pfhxyj4hmxk4db10a69hk8hzrcbwfh7zjh4b3j5qm5ib6j8br")))

(define-public crate-orx-priority-queue-0.1.2 (c (n "orx-priority-queue") (v "0.1.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0snvykv6v0rp87gb460zwcvw6s8bq76qwz6b5ldi0jmybzx50kii")))

(define-public crate-orx-priority-queue-0.1.3 (c (n "orx-priority-queue") (v "0.1.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mlm0jlyflw3qy92kzi4d2fkgp6ncw5iwzgb0ivify23729132c4")))

(define-public crate-orx-priority-queue-0.1.4 (c (n "orx-priority-queue") (v "0.1.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c2dwjx07lljwasp9qi7abr1r38xpqzjabfrgabz07nmch8p6j2z")))

(define-public crate-orx-priority-queue-0.1.5 (c (n "orx-priority-queue") (v "0.1.5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02shvbnjrss7ijfr5p7ai086is2xyq7xzi5g8i2vc1fpaxmc4pdw")))

(define-public crate-orx-priority-queue-0.1.6 (c (n "orx-priority-queue") (v "0.1.6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "104471qqgp910bj3grmrxnj7lyhdrr8h599dfw3h04s969c53bf4")))

(define-public crate-orx-priority-queue-0.1.7 (c (n "orx-priority-queue") (v "0.1.7") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k0rw3jml5saklpyl5qmchyglv4mvf9v4l176vqgsarz9vlvhzkz")))

(define-public crate-orx-priority-queue-0.2.0 (c (n "orx-priority-queue") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0axwdsvzkjjrh8cy0kbcj9x2xsn5shbp66saqcfsykqzpabx7l64")))

(define-public crate-orx-priority-queue-0.3.0 (c (n "orx-priority-queue") (v "0.3.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nig4pdw2nw2qyyfy0bxhwi0l0js65vn823ciwh7jnmar979s4z0")))

(define-public crate-orx-priority-queue-0.3.1 (c (n "orx-priority-queue") (v "0.3.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15g5ip048j97vfrhl570pnkz7a2x3s6gggx6592nldn602jw8jkb")))

(define-public crate-orx-priority-queue-0.3.2 (c (n "orx-priority-queue") (v "0.3.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0is57dv22r3c9ygj5ih3ss2j6bwlk142w6y9g1bi7dzsy922vwzq")))

(define-public crate-orx-priority-queue-0.3.3 (c (n "orx-priority-queue") (v "0.3.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0m4wgg1pd2wf0da5wxsw7q5fmwpbby3zfc2h18kxbj6z2z5z3bn6")))

(define-public crate-orx-priority-queue-0.4.0 (c (n "orx-priority-queue") (v "0.4.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gpqwnlaamxgxii3ysc47gzji4zn9g5hqn7x9g1ghs6xiz2nk4bn") (f (quote (("std") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.0.0 (c (n "orx-priority-queue") (v "1.0.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0hyv036jj413j0pddwmjw3anf7x9fw7f4fy4wpcvicszsrj8adc9") (f (quote (("std") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.0 (c (n "orx-priority-queue") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0p83m4jjn1c5sncidbixci7cxa1j7yz5m9jsc6gg6q9cnv9bhx9v") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.1 (c (n "orx-priority-queue") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0ca0py2s8apk42hq9ck31flrjdwxjmvi5aw1vll401aypka3nnh7") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.2 (c (n "orx-priority-queue") (v "1.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0s7c4vrj5xdi3hixhpng4ym7jgpacj942xbf8yvhjf4pd4yv4c70") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.3 (c (n "orx-priority-queue") (v "1.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1xikgjr97kdky3lrqp29297sv319la33yj3fd2xdafscg49dsphc") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.4 (c (n "orx-priority-queue") (v "1.1.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1ss0x2nfw1yh3miwlfhscp3q5s57y4salrbkazv9xdjlapsjlwjc") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

(define-public crate-orx-priority-queue-1.1.5 (c (n "orx-priority-queue") (v "1.1.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "priority-queue") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1rnpk38xjckxdgm60wc4zfh5r8s9wpfdccyc8kbkai4dkjschv4r") (f (quote (("std") ("impl_priority_queue" "priority-queue") ("default" "std"))))))

