(define-module (crates-io or x- orx-closure) #:use-module (crates-io))

(define-public crate-orx-closure-0.0.1 (c (n "orx-closure") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1alignndr78kfcsjvjcpbhmccvzkx6gw9436nlridnv21zxqlqw6")))

(define-public crate-orx-closure-0.1.0 (c (n "orx-closure") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0xh39yqqwmkrkavj9dg9sxfisa816rf38prhr7x9rjnn6wh745qk")))

(define-public crate-orx-closure-0.1.1 (c (n "orx-closure") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1pwxk3iy0i0pkfs9qvdf6ahjf1fxcdn9ryfdvmjzczwgg5l2rfzh")))

(define-public crate-orx-closure-0.1.2 (c (n "orx-closure") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1npbviqg3nzh9bj7zq5l7gd3l5lk42045fx7v6x0az33rbdlr2sw")))

(define-public crate-orx-closure-0.1.3 (c (n "orx-closure") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ijw58nyp41ikssfg1bkjcxiwh3b5q6j5hd0720ds5iqfq918gas")))

(define-public crate-orx-closure-0.1.4 (c (n "orx-closure") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "182j4ngclwv3zjkggv53r6ylbn2xqmvc9qrpgx76l55s1bv8758z")))

