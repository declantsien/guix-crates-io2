(define-module (crates-io or x- orx-pinned-vec) #:use-module (crates-io))

(define-public crate-orx-pinned-vec-0.1.0 (c (n "orx-pinned-vec") (v "0.1.0") (h "1pdb73hy1z3a5wv4jfvphpwfbld27vpbsxvr9z5yhcnx04jj0wsd")))

(define-public crate-orx-pinned-vec-0.1.1 (c (n "orx-pinned-vec") (v "0.1.1") (h "0vnzf0aqggmnp5l2dz9z83c3wbchx40g3by3zfiq1vdm4pik0zfn")))

(define-public crate-orx-pinned-vec-0.2.0 (c (n "orx-pinned-vec") (v "0.2.0") (h "1vn934xqrp536cli5q8vg3glnh3aa9aaaz6hkca4y2pxv721my9m")))

(define-public crate-orx-pinned-vec-0.2.1 (c (n "orx-pinned-vec") (v "0.2.1") (h "19wf5dp2551chcismx24ap1gyky36criwbndbhp2038mf0g91sln")))

(define-public crate-orx-pinned-vec-0.3.0 (c (n "orx-pinned-vec") (v "0.3.0") (h "0crh640rpykhy1rmkxyzfihprkrwq1r4niqhny2lf3mrmyd3r6hb")))

(define-public crate-orx-pinned-vec-0.3.1 (c (n "orx-pinned-vec") (v "0.3.1") (h "0ln9h3f2m5pkky1vzz2621ra1nfgcb442p4k9wy4icpimq4a59hr")))

(define-public crate-orx-pinned-vec-0.4.0 (c (n "orx-pinned-vec") (v "0.4.0") (h "04wzd5n2nywqvla20gjyiwp81m0rm2i73lsvg0h5vlwklvf88liq")))

(define-public crate-orx-pinned-vec-0.4.1 (c (n "orx-pinned-vec") (v "0.4.1") (h "02hcvw0difcsdfgs7ih22i05v895pzqvn49fc995sbi4z2d9rgcy")))

(define-public crate-orx-pinned-vec-0.4.2 (c (n "orx-pinned-vec") (v "0.4.2") (h "0sc2w05h4ramnwhjc41xygbx1p2va419zwhj60f71mn5019nh3xk")))

(define-public crate-orx-pinned-vec-0.4.3 (c (n "orx-pinned-vec") (v "0.4.3") (h "0fdk716ij0fahl740685sv81ms2a0xd2av6yz0819lypz128lb8b")))

(define-public crate-orx-pinned-vec-0.5.0 (c (n "orx-pinned-vec") (v "0.5.0") (h "0jpnjhk1l4lr3vxbdjwh2b2380p24lsa18pgkqsdj1jrsq7c3a1p")))

(define-public crate-orx-pinned-vec-0.5.1 (c (n "orx-pinned-vec") (v "0.5.1") (h "1pvqs0hcws3sjabw25ny4s0jw374wly16sjvb6fd4fa4y8kk2pl6")))

(define-public crate-orx-pinned-vec-0.5.2 (c (n "orx-pinned-vec") (v "0.5.2") (h "1bgjgm5igwcawdgg8yjd5a3j8wwv2m75ppkjy4bxpz3jz6wi6j2s")))

(define-public crate-orx-pinned-vec-0.5.3 (c (n "orx-pinned-vec") (v "0.5.3") (h "0b4yc7frlkyvl2hf1ijszncqqdcmvigs7gl40jl4smmlsigq9bzj")))

(define-public crate-orx-pinned-vec-1.0.0 (c (n "orx-pinned-vec") (v "1.0.0") (h "1in3x2b930dl0br9ipwrw1p0vqpgnixhsix7idn7fknn8qf1jx06")))

(define-public crate-orx-pinned-vec-1.0.1 (c (n "orx-pinned-vec") (v "1.0.1") (h "1fw9v2wyhcrjngq21w3zmwxk4xz9pgzpc74fhh8vdhf96sh5qcfx")))

(define-public crate-orx-pinned-vec-2.0.0 (c (n "orx-pinned-vec") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "120mh91fmdz59zm7255k3jls7cjqns2n9ljg4ckkz42rmrwd162z")))

(define-public crate-orx-pinned-vec-2.0.1 (c (n "orx-pinned-vec") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0qvq6gkcy4j0fi1jbisyy6i26d2dxgazi0wrs7w2mgrn64zf0p24")))

(define-public crate-orx-pinned-vec-2.1.0 (c (n "orx-pinned-vec") (v "2.1.0") (h "00y0wgs5908sq92pkh2hdskh6rvin7af8lwxqvg224g64zh35l2p")))

(define-public crate-orx-pinned-vec-2.1.1 (c (n "orx-pinned-vec") (v "2.1.1") (h "1imd7zpa8mxhcpwx14rmd7jgdimf9f5zd5vk6dkmq6m42jdzjwv2")))

(define-public crate-orx-pinned-vec-2.2.0 (c (n "orx-pinned-vec") (v "2.2.0") (h "0ggvf6ihl4alhmia7hmj57k142sadlmqrwq7fwp7v5pg9id8122a")))

(define-public crate-orx-pinned-vec-2.3.0 (c (n "orx-pinned-vec") (v "2.3.0") (h "1vx5mr98xnaqsx8qw5dzabs2vrdyx3wn8wcczb7zwp5jkh49z2av")))

(define-public crate-orx-pinned-vec-2.4.0 (c (n "orx-pinned-vec") (v "2.4.0") (h "1kvsy81zir47amrrc87p76mbj9savsqq0l8yingn9pbd5vwqi520")))

(define-public crate-orx-pinned-vec-2.5.0 (c (n "orx-pinned-vec") (v "2.5.0") (h "0cpqpxj45a30239hy4mb41ciy30av7vmhgwgs0slk7llmjdlld2z")))

(define-public crate-orx-pinned-vec-2.6.0 (c (n "orx-pinned-vec") (v "2.6.0") (h "13lzvsi6694c2hgdyai9a7zd9i0c06p4ygy0pw2nyvdhc10nzwcj")))

(define-public crate-orx-pinned-vec-2.7.0 (c (n "orx-pinned-vec") (v "2.7.0") (h "0my8srq3ivn3vyggsj6h3h4pk9hhcgbs4q5yyjirqc5bs0xszq6x")))

(define-public crate-orx-pinned-vec-2.8.0 (c (n "orx-pinned-vec") (v "2.8.0") (h "1j49dbmalw0a44afh2agkk5a8dzxqy2hvwk1lcr4nzyc6mzvjkar")))

(define-public crate-orx-pinned-vec-2.9.0 (c (n "orx-pinned-vec") (v "2.9.0") (h "0fj96x3igksvbqnxs1xny9dkqjdrziwqhhg1s8rh4wviwd2jdgks")))

(define-public crate-orx-pinned-vec-2.10.0 (c (n "orx-pinned-vec") (v "2.10.0") (h "0z0kxpg5lzy1mc9q52ka6ams9ram7qzidd0zn1s0fplsr6dfqj0k")))

