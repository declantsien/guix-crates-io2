(define-module (crates-io or x- orx-split-vec) #:use-module (crates-io))

(define-public crate-orx-split-vec-0.1.0 (c (n "orx-split-vec") (v "0.1.0") (h "1jm0kwjh91gdwdmvm1mx9whxyq0kdbh8j2241iya4c3119jv25yn")))

(define-public crate-orx-split-vec-0.1.1 (c (n "orx-split-vec") (v "0.1.1") (h "1hazz925shl893d12mr47f10qriljkc66vpwqqy9zjg4kmqklqkc")))

(define-public crate-orx-split-vec-0.1.2 (c (n "orx-split-vec") (v "0.1.2") (h "1nhsrrs14rwplp75r7xyzyrpiphbf2jgj743aaffry6ykra00qbh")))

(define-public crate-orx-split-vec-0.1.3 (c (n "orx-split-vec") (v "0.1.3") (h "1jhdn9s17ss3806wx5v31xkbxfkwghsz6r43p2rrqvjkkswzwwls")))

(define-public crate-orx-split-vec-0.1.4 (c (n "orx-split-vec") (v "0.1.4") (h "15ldvi3sa20snvafhmgflvflf1gxhakmj8mm11vrlhxpkh9x1yjc")))

(define-public crate-orx-split-vec-0.1.5 (c (n "orx-split-vec") (v "0.1.5") (h "1xnxbdjflkizcj60wrv3jh4pac6vg3sdd8zimj739v6sh3b8d85z")))

(define-public crate-orx-split-vec-0.1.6 (c (n "orx-split-vec") (v "0.1.6") (h "1dq9s6xppnhdznki27ah7sxhsxk638g3l7270pmdsn1vv9dw7sls")))

(define-public crate-orx-split-vec-0.1.7 (c (n "orx-split-vec") (v "0.1.7") (h "0sgxlcgxs3kp6zk9v1rr5b5rrl4wk5qm84i00dxzf8flxij6jmm8")))

(define-public crate-orx-split-vec-0.2.0 (c (n "orx-split-vec") (v "0.2.0") (h "0n92j79prl16wrkisv1k7fqvydj2cl1wr6598k2jbsxx1riadf4h")))

(define-public crate-orx-split-vec-0.2.1 (c (n "orx-split-vec") (v "0.2.1") (h "1n653hcvpz0q62z2rgkgn4b0x0yvpx4mz9ckcdiiag6gibq1zxwv")))

(define-public crate-orx-split-vec-0.2.2 (c (n "orx-split-vec") (v "0.2.2") (h "0x1glpzn99jbb0laqqn4yb7h8j5w41570bi9r0qw8a236vqk09vh")))

(define-public crate-orx-split-vec-0.2.3 (c (n "orx-split-vec") (v "0.2.3") (h "1wlppv7mg5vz8s2b73nbydb3d45i5jb9rl499d86ghki2jy2fjfa")))

(define-public crate-orx-split-vec-0.3.0 (c (n "orx-split-vec") (v "0.3.0") (h "1qhjgcl34rvharngxipiq6wfjpi1lgy9h5g4cbs89cb3ki9z4434")))

(define-public crate-orx-split-vec-0.5.0 (c (n "orx-split-vec") (v "0.5.0") (h "1vx1x8acmx01lxs9jxxib7z2yaxavckm6rb5cm88ir1m8m4syxvh")))

(define-public crate-orx-split-vec-0.6.0 (c (n "orx-split-vec") (v "0.6.0") (d (list (d (n "orx-pinned-vec") (r "^0.1.0") (d #t) (k 0)))) (h "1nr1q604h8x4qa750wsms90j3032krj2mw77nc4xnc5zll5s5x1q")))

(define-public crate-orx-split-vec-0.6.1 (c (n "orx-split-vec") (v "0.6.1") (d (list (d (n "orx-pinned-vec") (r "^0.1.1") (d #t) (k 0)))) (h "0rqhgg8ily25np71qa03g1bqxzj4ry9c9hvv30icpy7xhfgiq8fg")))

(define-public crate-orx-split-vec-0.6.2 (c (n "orx-split-vec") (v "0.6.2") (d (list (d (n "orx-pinned-vec") (r "^0.1.1") (d #t) (k 0)))) (h "1nkh7gb4b2an9w0ffh8n80v7g5hzc01rdbgrwvdzcxybxg88ckwg")))

(define-public crate-orx-split-vec-0.7.0 (c (n "orx-split-vec") (v "0.7.0") (d (list (d (n "orx-pinned-vec") (r "^0.3.1") (d #t) (k 0)))) (h "18ymcymc0qa0l8d8671pk9hjnzgr6y5ip82nzi8acjjrw3xkj4n9")))

(define-public crate-orx-split-vec-0.7.1 (c (n "orx-split-vec") (v "0.7.1") (d (list (d (n "orx-pinned-vec") (r "^0.4.1") (d #t) (k 0)))) (h "0smxjncyxnp6lsiljz5cvdaav1kdaqjg4b9bzmp1vnvmfqyk2jv8")))

(define-public crate-orx-split-vec-0.7.2 (c (n "orx-split-vec") (v "0.7.2") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "0z73r9b3mbhli12m2j7s3l3s11x6r2mfns7x92cg5y57qdpcj2wp")))

(define-public crate-orx-split-vec-0.7.3 (c (n "orx-split-vec") (v "0.7.3") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "18320pw2rxxsnxvcawbri3bz6fmq7x5pgwkgf4qryd6jsw17kayq")))

(define-public crate-orx-split-vec-0.7.4 (c (n "orx-split-vec") (v "0.7.4") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1xlrqck7b74v3l0fmi42pyyx5il4ifh1ibcjcmc04f5w2qlz68vd")))

(define-public crate-orx-split-vec-0.8.0 (c (n "orx-split-vec") (v "0.8.0") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1yzh84b7368gm8vd4z7a0jyf2vwhb2wvz14yk6gxah0iin9qvkkp")))

(define-public crate-orx-split-vec-0.8.1 (c (n "orx-split-vec") (v "0.8.1") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1cf06m8l2p4lql5gcd9qph0q333wlc6pmvzl42jc8vq4038li7g3")))

(define-public crate-orx-split-vec-0.8.2 (c (n "orx-split-vec") (v "0.8.2") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1d6ypa3wz7fldqbsrzskf3i0akcgy5y9f1m0zzjsbsgvlv652h64")))

(define-public crate-orx-split-vec-0.9.0 (c (n "orx-split-vec") (v "0.9.0") (d (list (d (n "orx-fixed-vec") (r "^0.3.2") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "0c3h86mavih0i91jvzyxmzw8w67ra9jn7vk2vihly4dfnqh3gpyd")))

(define-public crate-orx-split-vec-0.9.1 (c (n "orx-split-vec") (v "0.9.1") (d (list (d (n "orx-fixed-vec") (r "^0.3.3") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.4.3") (d #t) (k 0)))) (h "0bqk3p6y96g0mrqrqhhphkhcq3r98pr9mr8wbl3rmdh7ygd1cb49")))

(define-public crate-orx-split-vec-0.9.2 (c (n "orx-split-vec") (v "0.9.2") (d (list (d (n "orx-fixed-vec") (r "^0.3.3") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1blsn6iwxqn43mdy7ykj849i3mj4y7ipb7zqfw2pa0np2nxzvs92")))

(define-public crate-orx-split-vec-0.9.3 (c (n "orx-split-vec") (v "0.9.3") (d (list (d (n "orx-fixed-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "1n400p60v7ijqq5280ykxl05xprbrh2iw4g3vfrnvm6b7wb20mxa")))

(define-public crate-orx-split-vec-0.9.4 (c (n "orx-split-vec") (v "0.9.4") (d (list (d (n "orx-fixed-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "0nazhhv9192j1jybm5a269s1cczpxaqvmlj0jh1ixxrnsl5d4sj9")))

(define-public crate-orx-split-vec-0.9.5 (c (n "orx-split-vec") (v "0.9.5") (d (list (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)))) (h "1q5hvf0xq326d2x98qjd0l441lyw9rvdjalr4rxcy2x89df4b5ip")))

(define-public crate-orx-split-vec-1.0.0 (c (n "orx-split-vec") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "11kikhmvswazfdckcv9vlwqlrjys2p1fvxpa5c68qhfr90f4nc82")))

(define-public crate-orx-split-vec-1.0.1 (c (n "orx-split-vec") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1nfmcvrysnz88v5k8fyz6iqi66875b2lsw4qkn6xhyfnwwi6012d")))

(define-public crate-orx-split-vec-1.0.2 (c (n "orx-split-vec") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0d15980v58rpqvmmlsw1wnqni61bp2n3a2l55p7lq0sa5rkr35r8")))

(define-public crate-orx-split-vec-1.0.3 (c (n "orx-split-vec") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0bbmr52pysw4hhsgfbjzikb63q19y7x48ch4q57ij9kwmcfmnivj")))

(define-public crate-orx-split-vec-1.1.0 (c (n "orx-split-vec") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-fixed-vec") (r "^0.4") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1m4mgrncm24in3cciwchq9bi2554bdlrmfyi74s3wf9hd4mgn6s9")))

(define-public crate-orx-split-vec-1.1.1 (c (n "orx-split-vec") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0a19izghwh95qf9jfa9aa0qsqrlqaf2wzynqmaq0s0ga440wlrdm")))

(define-public crate-orx-split-vec-1.2.0 (c (n "orx-split-vec") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1v588x4m5rdcgrwixghfk9vz2i0s3qd9hnzvkkwm10w3inc32c4z")))

(define-public crate-orx-split-vec-1.2.1 (c (n "orx-split-vec") (v "1.2.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "09pdd200hlvbh08fbhwjg6p59r05y976s9q5p2vxdjmx4sli4cgb")))

(define-public crate-orx-split-vec-1.3.0 (c (n "orx-split-vec") (v "1.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1lwj56dxqyhaisf7k9iwphhsyk6r0hqk23bm4cbji8x03vfjyx3b")))

(define-public crate-orx-split-vec-2.0.0 (c (n "orx-split-vec") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1cf8fijx1zkc0myhzm96mpz2l7k30jf0ab6mf4lgln92s4zpyk3h")))

(define-public crate-orx-split-vec-2.0.1 (c (n "orx-split-vec") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1rg28zy817y621p77zk36zvjgl26fvj3906plmchfi7g997hshz7")))

(define-public crate-orx-split-vec-2.1.0 (c (n "orx-split-vec") (v "2.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1mcc82qgcs2d0j2dx3ichik7pscvpr4slkwd2nxdnrwn9s7gzgvh")))

(define-public crate-orx-split-vec-2.1.1 (c (n "orx-split-vec") (v "2.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0zjwi9g23bayycnjng56djivpc15dmj9wsxq72fc8spa9r75bsra")))

(define-public crate-orx-split-vec-2.2.0 (c (n "orx-split-vec") (v "2.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0mc5hgrf26lvidkp1viyh71v001cf88sr1nf09d7jlk2al23zgjn")))

(define-public crate-orx-split-vec-2.3.0 (c (n "orx-split-vec") (v "2.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1yp18472zxkns1syisqbffh5pskkm6r6mf6y21sw9x5paxabd0rb")))

(define-public crate-orx-split-vec-2.4.0 (c (n "orx-split-vec") (v "2.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1pza9ayblp7arw85v3rxd7fqzp5fsaqav7xwdmhl174m2fgyky14")))

(define-public crate-orx-split-vec-2.5.0 (c (n "orx-split-vec") (v "2.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "12xxmy0f1gn9qgiz543hv3pmqxghyk40nd7wxnmwxid30prlcqqw")))

(define-public crate-orx-split-vec-2.6.0 (c (n "orx-split-vec") (v "2.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0rqrcjpg1517jhzhm7c64yghmphhcjbqwkalxpqyk0zxfp3hp2ah")))

(define-public crate-orx-split-vec-2.7.0 (c (n "orx-split-vec") (v "2.7.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0jfwa1h5bbslbxi6lalf9inafvhd86fz4zm8l9pq15cadnn9vqkz")))

(define-public crate-orx-split-vec-2.7.1 (c (n "orx-split-vec") (v "2.7.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0g03zcp9g9qar87vd5vsbd1bvi73n945n6qr9kxvqhid9wci0qcf")))

(define-public crate-orx-split-vec-2.8.0 (c (n "orx-split-vec") (v "2.8.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0ji2v4q3q8s78ba46dvqkvx19vav1y6ibq0909l7g7pq8qm9155a")))

(define-public crate-orx-split-vec-2.9.0 (c (n "orx-split-vec") (v "2.9.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "140n0ckj6zihq94lrfxx5lxqbd1srj9qiymrxpllmc5c6gd1cqnh")))

(define-public crate-orx-split-vec-2.10.0 (c (n "orx-split-vec") (v "2.10.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "14397rdzzyvkmf3pi1wybndnn02inrxrxwgfr9h78vq6kmil03r1")))

(define-public crate-orx-split-vec-2.11.0 (c (n "orx-split-vec") (v "2.11.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0kg5z2qr4q270d4s0i9kc1q2i3p6cbnk70qkczmk0gixdc4f6zl0")))

(define-public crate-orx-split-vec-2.12.0 (c (n "orx-split-vec") (v "2.12.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "04v5lf3aq83riyh1g5nfb2pqqp9gqnsg70gq874gcw45ghyik8c0")))

