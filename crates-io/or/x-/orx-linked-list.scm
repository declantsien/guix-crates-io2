(define-module (crates-io or x- orx-linked-list) #:use-module (crates-io))

(define-public crate-orx-linked-list-0.1.0 (c (n "orx-linked-list") (v "0.1.0") (d (list (d (n "orx-imp-vec") (r "^0.9.0") (d #t) (k 0)))) (h "093cjbq2mgdzpbk63lknhz28vfbqrpn5xppvjxbgsxqx5l3sj3pk")))

(define-public crate-orx-linked-list-0.1.1 (c (n "orx-linked-list") (v "0.1.1") (d (list (d (n "orx-imp-vec") (r "^0.9.0") (d #t) (k 0)))) (h "0pq91s629ids3p7n85aa73nh0pvgs9l1mj1nmd1cc8vi50gcsivi")))

(define-public crate-orx-linked-list-0.1.2 (c (n "orx-linked-list") (v "0.1.2") (d (list (d (n "orx-imp-vec") (r "^0.9.0") (d #t) (k 0)))) (h "1lphwhy38blhps29azkr0x71m6ja9phywp4g4lzk4aa3g4x2rjr9")))

(define-public crate-orx-linked-list-0.2.0 (c (n "orx-linked-list") (v "0.2.0") (d (list (d (n "orx-imp-vec") (r "^0.9.1") (d #t) (k 0)))) (h "0jc9r73v8jc9blj5wzhdqsga0rdbwlsc74qqnd29w5pp32wbnyai")))

(define-public crate-orx-linked-list-0.3.0 (c (n "orx-linked-list") (v "0.3.0") (d (list (d (n "orx-imp-vec") (r "^0.9.1") (d #t) (k 0)))) (h "02nlsdgi0sgkd4fdcll8hji1qfygcbqpld71w3yykxzmjp41nx0s")))

(define-public crate-orx-linked-list-0.4.1 (c (n "orx-linked-list") (v "0.4.1") (d (list (d (n "orx-imp-vec") (r "^0.9.1") (d #t) (k 0)))) (h "0pni0gbabh82z639qg24kck1ayw726hi56kn84nwq78pby1x9pva")))

(define-public crate-orx-linked-list-0.4.2 (c (n "orx-linked-list") (v "0.4.2") (d (list (d (n "orx-imp-vec") (r "^0.9.2") (d #t) (k 0)))) (h "0xz497fi3sxywyan3g2hksb97n0dzarxl12ndf9acfmw5i59hnkh")))

(define-public crate-orx-linked-list-0.5.0 (c (n "orx-linked-list") (v "0.5.0") (d (list (d (n "orx-imp-vec") (r "^0.9.3") (d #t) (k 0)))) (h "0ss0ykxpmap7mxv5a63mnwa58bcm2m2kr3ys5ibz59af0lfaivhq")))

(define-public crate-orx-linked-list-0.6.0 (c (n "orx-linked-list") (v "0.6.0") (d (list (d (n "orx-imp-vec") (r "^0.9.4") (d #t) (k 0)))) (h "1ni9akilksa5a4xvwhpdkv0dny2rqmdfbf3ql6krav36c2j65zwl")))

(define-public crate-orx-linked-list-0.6.1 (c (n "orx-linked-list") (v "0.6.1") (d (list (d (n "orx-imp-vec") (r "^0.9.5") (d #t) (k 0)))) (h "1022kf6by5ykrw8ris8d9m4jk3pgl91dmpsawgmjxgj4mry7q2hf")))

(define-public crate-orx-linked-list-0.7.0 (c (n "orx-linked-list") (v "0.7.0") (d (list (d (n "orx-imp-vec") (r "^0.9.6") (d #t) (k 0)))) (h "0jcdq42bbmj4cnxgxfbji292yb32qks2vwivc77d6h0y8nkib637")))

(define-public crate-orx-linked-list-0.8.0 (c (n "orx-linked-list") (v "0.8.0") (d (list (d (n "orx-imp-vec") (r "^0.9.6") (d #t) (k 0)))) (h "1dqfi6csi5n2i7dy81yi8fxbvmqa57r9ga9qwd9alqnykzvj413j")))

(define-public crate-orx-linked-list-0.8.1 (c (n "orx-linked-list") (v "0.8.1") (d (list (d (n "orx-imp-vec") (r "^0.9.7") (d #t) (k 0)))) (h "0rbg61fmrq0xfq0krx9mjyxbk8caxnjjx3ysm2rjk2gawpib6c6g")))

(define-public crate-orx-linked-list-0.8.2 (c (n "orx-linked-list") (v "0.8.2") (d (list (d (n "orx-imp-vec") (r "^0.9.7") (d #t) (k 0)))) (h "1dxjwzhax2np729sknplw77vvczlyj1ya9laqgp6i4znfbn4vk6m")))

(define-public crate-orx-linked-list-0.8.3 (c (n "orx-linked-list") (v "0.8.3") (d (list (d (n "orx-imp-vec") (r "^0.9.7") (d #t) (k 0)))) (h "1ls69rsnsrk2f1pjc6f1ggk4j3rj0sn6lzz5xgn0g96jgvpm4chn")))

(define-public crate-orx-linked-list-0.8.4 (c (n "orx-linked-list") (v "0.8.4") (d (list (d (n "orx-imp-vec") (r "^0.9.7") (d #t) (k 0)))) (h "0mjfl0ksr784aqividi4i4w9q6d486bwwmmpwac3fi5hkrmknpl4")))

(define-public crate-orx-linked-list-0.8.5 (c (n "orx-linked-list") (v "0.8.5") (d (list (d (n "orx-imp-vec") (r "^0.9.7") (d #t) (k 0)))) (h "0p9zvxass0nqq36bg0alhw71qix4wbl1h2qjnqpb1br4z28prz8f")))

(define-public crate-orx-linked-list-1.0.0 (c (n "orx-linked-list") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-imp-vec") (r "^1.0") (d #t) (k 0)) (d (n "orx-split-vec") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0aq253vpds28cmgalr4xgimn9njdb0s37zxjc107vkjqyidm3090")))

(define-public crate-orx-linked-list-2.0.0 (c (n "orx-linked-list") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.0") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "14m3jy1nm6wlxc1i55f4l0y25jlfc4bs87akmnxmi8zyx9xswmcg")))

(define-public crate-orx-linked-list-2.0.1 (c (n "orx-linked-list") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.0") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "07afhcv0jy01k1q7hbpb4a2yyhdy5zkhjyilkwr7v2cp3nyfrmhp")))

(define-public crate-orx-linked-list-2.0.2 (c (n "orx-linked-list") (v "2.0.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.1") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "0cf6ka1550npdmhb65a8jy0lxj9ml71hsw8iv88jhprk1n6j680i")))

(define-public crate-orx-linked-list-2.0.3 (c (n "orx-linked-list") (v "2.0.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.1") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "15bdz31fq6npz00xj311ilrs7i1dbb547b28dybw4brfar1mj0gx")))

(define-public crate-orx-linked-list-2.1.0 (c (n "orx-linked-list") (v "2.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.2") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1xsci98f5w7kzks468nm6jx6vkdgn91i481mkg1s2vp4yqi6n6kn")))

(define-public crate-orx-linked-list-2.2.0 (c (n "orx-linked-list") (v "2.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.3") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1s2pp6qh41i27qha5lvnrd2k8jkg9ds3ph44dil5lvlk6lzvic90")))

(define-public crate-orx-linked-list-2.3.0 (c (n "orx-linked-list") (v "2.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-selfref-col") (r "^1.4") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "0azpkslh80w3n02xammr124b9aa18v328xi4l9758vwa202mqfyz")))

(define-public crate-orx-linked-list-2.4.0 (c (n "orx-linked-list") (v "2.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.7") (d #t) (k 0)) (d (n "orx-selfref-col") (r "^1.5") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "0cc289j5zmzbqnlkv2b8rc8xgfkiq23z4y2py58g25alqzadi5rm")))

