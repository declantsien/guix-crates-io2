(define-module (crates-io or x- orx-fixed-vec) #:use-module (crates-io))

(define-public crate-orx-fixed-vec-0.1.0 (c (n "orx-fixed-vec") (v "0.1.0") (d (list (d (n "orx-pinned-vec") (r "^0.1.0") (d #t) (k 0)))) (h "046mw3g64xilakvp01hsii3msapp8zk63d9h5zwiyddngkhkjbga")))

(define-public crate-orx-fixed-vec-0.1.1 (c (n "orx-fixed-vec") (v "0.1.1") (d (list (d (n "orx-pinned-vec") (r "^0.1.1") (d #t) (k 0)))) (h "05gmrpkcd40ywylw4xsl7hgb817xrwsczvapaw79j620csn5zdwc")))

(define-public crate-orx-fixed-vec-0.2.0 (c (n "orx-fixed-vec") (v "0.2.0") (d (list (d (n "orx-pinned-vec") (r "^0.3.1") (d #t) (k 0)))) (h "0ap4vfidrqlkp4cjhzmldr2z4d4r9nh3ykbyal9lfi9a568r6hg1")))

(define-public crate-orx-fixed-vec-0.2.1 (c (n "orx-fixed-vec") (v "0.2.1") (d (list (d (n "orx-pinned-vec") (r "^0.3.1") (d #t) (k 0)))) (h "0y1ma28g5b41y8ik8sg87ja7ilzqqa0sqjxly3k25zplr3f79zfx")))

(define-public crate-orx-fixed-vec-0.3.0 (c (n "orx-fixed-vec") (v "0.3.0") (d (list (d (n "orx-pinned-vec") (r "^0.4.1") (d #t) (k 0)))) (h "0lz4slrdspislfx6d5vl81yswvlm1z5vn4ghp51yf9wl8zhzvn8c")))

(define-public crate-orx-fixed-vec-0.3.1 (c (n "orx-fixed-vec") (v "0.3.1") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1acwg4c7qc5mzji8wqapb9j9r7cwv394f8n7hpwbw2q9f3rnc906")))

(define-public crate-orx-fixed-vec-0.3.2 (c (n "orx-fixed-vec") (v "0.3.2") (d (list (d (n "orx-pinned-vec") (r "^0.4.2") (d #t) (k 0)))) (h "1amsj1ga6mx5856fpsrj16vkrglfi023y5bhh3i7zk3l2g360ffa")))

(define-public crate-orx-fixed-vec-0.3.3 (c (n "orx-fixed-vec") (v "0.3.3") (d (list (d (n "orx-pinned-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1fqd1x5xz0mw1svpix02bfl4kv8ia98mxpq315ad5z9zby5g5sr8")))

(define-public crate-orx-fixed-vec-0.4.0 (c (n "orx-fixed-vec") (v "0.4.0") (d (list (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "06bdznciymd0m7kq9fnw4jch8jkdlz01mday2gr6n554lwg356fn")))

(define-public crate-orx-fixed-vec-0.4.1 (c (n "orx-fixed-vec") (v "0.4.1") (d (list (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "0hh139y2jf5bq1iglykihwxqrpfd1h0s676qw7avkwcc7iavkl6a")))

(define-public crate-orx-fixed-vec-0.4.2 (c (n "orx-fixed-vec") (v "0.4.2") (d (list (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "19c8q1gapzfaxhdmj8j4p8izavgy65591b699nygab8vmfjfsvzj")))

(define-public crate-orx-fixed-vec-0.4.3 (c (n "orx-fixed-vec") (v "0.4.3") (d (list (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "0bl9awn9brivfy4wrjkw30gj07k3f5v1kpd6azn7knbcq47d96hy")))

(define-public crate-orx-fixed-vec-0.4.4 (c (n "orx-fixed-vec") (v "0.4.4") (d (list (d (n "orx-pinned-vec") (r "^0.5.1") (d #t) (k 0)))) (h "0lzhhw98n587302k7c09v7wb4ppmsm2wnndlmpi6sczyh8846h9s")))

(define-public crate-orx-fixed-vec-0.4.5 (c (n "orx-fixed-vec") (v "0.4.5") (d (list (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)))) (h "1xhmkr91zsbpz45s74qp01x3nksldra4aph7065svxy977mbrs2w")))

(define-public crate-orx-fixed-vec-0.4.6 (c (n "orx-fixed-vec") (v "0.4.6") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0cbb9vms6kajqlkkbrm48249cm9rkv9cshf1ak5k24j2jcvrnlgf")))

(define-public crate-orx-fixed-vec-1.0.0 (c (n "orx-fixed-vec") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1sshvs6mad5qxbk5l0zmnsf3k8656243km1cqna716p8h3n5n4i4")))

(define-public crate-orx-fixed-vec-1.0.1 (c (n "orx-fixed-vec") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "03yivp5rsnhv68jgaqagv9d9kg0vnv2m3mwi44rin47ym7xw0gpq")))

(define-public crate-orx-fixed-vec-1.0.2 (c (n "orx-fixed-vec") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1nqn14b0i80s6wiwbhn9vsc6f54xslqpsz8n8ss7c887lfaavcdj")))

(define-public crate-orx-fixed-vec-2.0.0 (c (n "orx-fixed-vec") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0rz7cm1myz9lpsn9zffap8xj89rkdh4jkzvb2a2vchp4gxzc1814")))

(define-public crate-orx-fixed-vec-2.0.1 (c (n "orx-fixed-vec") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "07h043pc1ri3lki67bsaik2f87fvsmgwkmafi71adizwzji5018s")))

(define-public crate-orx-fixed-vec-2.1.0 (c (n "orx-fixed-vec") (v "2.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "15ab6gm95j3liyjni0gvdk0gnw6cb4a42zifgi64v625rq28cp4c")))

(define-public crate-orx-fixed-vec-2.1.1 (c (n "orx-fixed-vec") (v "2.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1r09hzjzws3a6lgy4sgqx88aimqlz90irja9dj00ijszywmf60id")))

(define-public crate-orx-fixed-vec-2.2.0 (c (n "orx-fixed-vec") (v "2.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1wcjgya4j8n9lvmzz40b7qyzb18injm57r57mjfliy3i9nn42w8x")))

(define-public crate-orx-fixed-vec-2.3.0 (c (n "orx-fixed-vec") (v "2.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1qcyyjlhaknmkzwlc4smjb3c59kjwiw65hf4cj9zxz8i5vi9g77j")))

(define-public crate-orx-fixed-vec-2.4.0 (c (n "orx-fixed-vec") (v "2.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0rcjp49v1q1dk799psy8p9zw3a14xhg638cbc3c2c795s8ngvpa8")))

(define-public crate-orx-fixed-vec-2.5.0 (c (n "orx-fixed-vec") (v "2.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1b6ksx91gbc2wc83ma36yyzv10fac1gszggkfxzqwcggafzggr6f")))

(define-public crate-orx-fixed-vec-2.6.0 (c (n "orx-fixed-vec") (v "2.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1398g29vnfa6y21nvapnp5msx6k2iaid0z7pch80lahpydmhxqmx")))

(define-public crate-orx-fixed-vec-2.6.1 (c (n "orx-fixed-vec") (v "2.6.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1w4i40jfr2vb7ssj895da7nraq87gjbakp234fs20mmf51akvcnx")))

(define-public crate-orx-fixed-vec-2.7.0 (c (n "orx-fixed-vec") (v "2.7.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0gahi1mf079594n17sjfxdqa3zjv68wk15mcazkwr1n9bx6rkrzj")))

(define-public crate-orx-fixed-vec-2.8.0 (c (n "orx-fixed-vec") (v "2.8.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "02qakza5ryqqwkl832q0w4ckdjrl9gayr20nbaja6y0f0yqqnc61")))

(define-public crate-orx-fixed-vec-2.9.0 (c (n "orx-fixed-vec") (v "2.9.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "02jh663pjlfj8rcqirsfkvbg6n9pckv28gw5zhg0q5vb11l2gr20")))

(define-public crate-orx-fixed-vec-2.10.0 (c (n "orx-fixed-vec") (v "2.10.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "02hq2a8c5l690nysys03p8njl0mafv2z2gvhh579ljhqd8jgm6g0")))

