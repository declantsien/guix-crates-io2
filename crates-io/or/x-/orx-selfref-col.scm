(define-module (crates-io or x- orx-selfref-col) #:use-module (crates-io))

(define-public crate-orx-selfref-col-1.0.0 (c (n "orx-selfref-col") (v "1.0.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1n60mac3fzl3ld9hmaspiciip6grvq1f3685g1qp27zinlh5p9xd")))

(define-public crate-orx-selfref-col-1.0.1 (c (n "orx-selfref-col") (v "1.0.1") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "170s25xnwli2g9qbvbpgs0bgnhkgirwzyn9417fhyvd69n25xya7")))

(define-public crate-orx-selfref-col-1.1.0 (c (n "orx-selfref-col") (v "1.1.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "119kh68cvv5z4d3sk9j48w43flmyixfkqn4w1yw647yjhbafhla4")))

(define-public crate-orx-selfref-col-1.1.1 (c (n "orx-selfref-col") (v "1.1.1") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "06dmrmsp8flraw9x9rybp731niqsqlgay5pa7gaw5j33m2ac9ij2")))

(define-public crate-orx-selfref-col-1.1.2 (c (n "orx-selfref-col") (v "1.1.2") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "022lcjkyj6yif1jnjl02kkswrp159wl8y7gmbifszhkp98p5zgs3")))

(define-public crate-orx-selfref-col-1.1.3 (c (n "orx-selfref-col") (v "1.1.3") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "14n8pd9askzackmbhwz1h7r8dlz2szh38ir8bbypi0nzavc361ll")))

(define-public crate-orx-selfref-col-1.2.0 (c (n "orx-selfref-col") (v "1.2.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "15gl4l1hdy65gz5lrfcyb8m3wjs28whmp8y08jvvw8jl2iycs86m")))

(define-public crate-orx-selfref-col-1.3.0 (c (n "orx-selfref-col") (v "1.3.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.3") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1j35jd2br6qk2mral5rq9n1w22js9jv6z6a8cs9yfg86iq2pgay5")))

(define-public crate-orx-selfref-col-1.4.0 (c (n "orx-selfref-col") (v "1.4.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-split-vec") (r "^2.7") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1yn8sy07mi4pfzw4fgiizmf8qyp4pxbplm2s5d13cf151b64mcjf")))

(define-public crate-orx-selfref-col-1.5.0 (c (n "orx-selfref-col") (v "1.5.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "orx-pinned-vec") (r "^2.7") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.8") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "0d1qfk5nsys7k7984ns2aib9p9vpclw6plb2cg8f2shmm78vichk")))

