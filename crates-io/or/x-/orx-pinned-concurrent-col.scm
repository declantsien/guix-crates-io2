(define-module (crates-io or x- orx-pinned-concurrent-col) #:use-module (crates-io))

(define-public crate-orx-pinned-concurrent-col-1.0.0 (c (n "orx-pinned-concurrent-col") (v "1.0.0") (d (list (d (n "orx-fixed-vec") (r "^2.8") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^2.8") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.9") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0pn4vdplaxbxsx4i18rl3bg3q5l2rz5vpvqajr2w85rhdpasdw06")))

(define-public crate-orx-pinned-concurrent-col-1.1.0 (c (n "orx-pinned-concurrent-col") (v "1.1.0") (d (list (d (n "orx-fixed-vec") (r "^2.8") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^2.8") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.9") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0gvq5piwz9cwzqnrm3jzl9gbrbml8mq6wsjkln3852mijgk68z61")))

(define-public crate-orx-pinned-concurrent-col-1.2.0 (c (n "orx-pinned-concurrent-col") (v "1.2.0") (d (list (d (n "orx-fixed-vec") (r "^2.9") (d #t) (k 0)) (d (n "orx-pinned-vec") (r "^2.9") (d #t) (k 0)) (d (n "orx-split-vec") (r "^2.11") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "08lha27ilbfd03g10x4xz80vzvxqldgnl3sfib1j27n96x9l5jlc")))

