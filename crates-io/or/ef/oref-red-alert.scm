(define-module (crates-io or ef oref-red-alert) #:use-module (crates-io))

(define-public crate-oref-red-alert-1.0.0 (c (n "oref-red-alert") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cfs78bpvap3ynwr0a37yvd771c47r2nq7jw7bny8cprhx36mf89") (y #t)))

(define-public crate-oref-red-alert-1.0.1 (c (n "oref-red-alert") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qvgxlvpib7x21c2cl667z513c9za6kppwnl9xsnhd897xyv7q8y")))

