(define-module (crates-io or na ornament) #:use-module (crates-io))

(define-public crate-ornament-0.1.0 (c (n "ornament") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02a8602lmjmljvmlk6585gcq6g6r75k9rwir4aigdn4mqbnxbcsq") (f (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2.0 (c (n "ornament") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lmmf8jb6x2dfgafzgfvi14i82f60v4awpb87s2gd199vf9ji8c6") (f (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2.1 (c (n "ornament") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zdmvv3hm4qmxi845b3a1awq971nf22v3m0vcmc3f6k9m75y750x") (f (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2.2 (c (n "ornament") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15am8zjkvry9vr40i6h2xas8sl03fpxbpjzxf80wv22rv3ndhncr") (f (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

