(define-module (crates-io or o- oro-config-derive) #:use-module (crates-io))

(define-public crate-oro-config-derive-0.2.0 (c (n "oro-config-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1q7y31c452jj3dcrj2f7cs8yy8qlmkj26zgrpl4ix8b6i72y77l1")))

(define-public crate-oro-config-derive-0.2.1 (c (n "oro-config-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0wf21fl243jwgax70v9dfq4hg3jggx5i665k20l2vm81jqcnr116")))

(define-public crate-oro-config-derive-0.2.2 (c (n "oro-config-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "13y361d0yr0m4z7f1ndcvms00jfip77q6y404ahkvy2ax5mx8j46")))

(define-public crate-oro-config-derive-0.3.3 (c (n "oro-config-derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0whhab41wvx285pzd5zqadi1qlkrrhzw26skn2ishbravm1r2c4p")))

(define-public crate-oro-config-derive-0.3.4 (c (n "oro-config-derive") (v "0.3.4") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "19lslj4nbij1dg6ds34ddla1l1mv4g349fd4209cdwa6iby9y4f8")))

(define-public crate-oro-config-derive-0.3.5 (c (n "oro-config-derive") (v "0.3.5") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1gk2bh7hhr3s03xlg420whlrsdfnc84vsq185x40fwqn7gj305hp")))

(define-public crate-oro-config-derive-0.3.6 (c (n "oro-config-derive") (v "0.3.6") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1rqxf9awjgf0snbrqllwfby7sb3viv331j148ay58dwd1lbfgd75")))

(define-public crate-oro-config-derive-0.3.7 (c (n "oro-config-derive") (v "0.3.7") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "12bi1lid097y7ymcml804nszq4czyvk3himcy1s1jc3abax9spvm")))

(define-public crate-oro-config-derive-0.3.8 (c (n "oro-config-derive") (v "0.3.8") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1nzyymy325dizc05g6dwrmi3x4kzv9lvk314cakrmx26cwh1k7zm")))

(define-public crate-oro-config-derive-0.3.9 (c (n "oro-config-derive") (v "0.3.9") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1c72x37m54aj0x0k9k1ys5h7q4b1p8nxyv09i51ams7vx56r4zp0")))

(define-public crate-oro-config-derive-0.3.10 (c (n "oro-config-derive") (v "0.3.10") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0wzsjks0b4mvq6043vmb8yzqcpqvj2ia7vvynaqv1s9gwxqs5bhg")))

(define-public crate-oro-config-derive-0.3.11 (c (n "oro-config-derive") (v "0.3.11") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1svdvzmbiy2sxhsh5kk6kvxsq6m9rq84jj763226d8p7wkfrbr5y")))

(define-public crate-oro-config-derive-0.3.12 (c (n "oro-config-derive") (v "0.3.12") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1vndxq50b34jsrll1qlcg2lzhsxxg09bc9i0kk0s4wlmqqy2bfny")))

(define-public crate-oro-config-derive-0.3.13 (c (n "oro-config-derive") (v "0.3.13") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "13y9cwknhgvr1y9fcgbnjbzfqs15c9zzmg5zfqx6djglrhddysyf")))

(define-public crate-oro-config-derive-0.3.14 (c (n "oro-config-derive") (v "0.3.14") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "075wbb3d25vz28czpl66pqxzqlnxm4b7rjd48kj3z6shfc4ps8qx")))

(define-public crate-oro-config-derive-0.3.15 (c (n "oro-config-derive") (v "0.3.15") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "14xn112c30dnawsp16slqgjnj1ypsgfdsmmbbdw9b7px8khf70wa")))

(define-public crate-oro-config-derive-0.3.16 (c (n "oro-config-derive") (v "0.3.16") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0glv4c0aylbhpjcl1kxkzzbx6g82mzghcsms8y304za7v0vy8b4l") (r "1.67.1")))

