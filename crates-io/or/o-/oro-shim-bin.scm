(define-module (crates-io or o- oro-shim-bin) #:use-module (crates-io))

(define-public crate-oro-shim-bin-0.3.14 (c (n "oro-shim-bin") (v "0.3.14") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0zr7vg117baxiy92cg1gpsd9337ns2dini0bg8dyf6j6svw9hsqb")))

(define-public crate-oro-shim-bin-0.3.15 (c (n "oro-shim-bin") (v "0.3.15") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0brvg485cv07gy1ipwprxayy0h2vhi1faz38f6c858rqb8dz1bq8")))

(define-public crate-oro-shim-bin-0.3.16 (c (n "oro-shim-bin") (v "0.3.16") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0pfadb5946g8rizpm639clzx8qgwx008qh7kpl5ngf3j3qiv8qni") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.17 (c (n "oro-shim-bin") (v "0.3.17") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0n1392bs5ffjsybkna9if5k2lbqk63afirkj728yr3jqvazb2mwi") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.18 (c (n "oro-shim-bin") (v "0.3.18") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "06ls3n4933gwj6d2di8sqqk4v7wwn95x33adr1nyqdf5cw73dw62") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.19 (c (n "oro-shim-bin") (v "0.3.19") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0n3xgimjx4fk8sss5n5mrdy3qg31r6psnc90kmfasqjzrr2z0dpl") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.20 (c (n "oro-shim-bin") (v "0.3.20") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0wkd80wwbwdw7xjvvjq15dyz7jpqk2fk3mvwjciwgk6qqah1dxzg") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.22 (c (n "oro-shim-bin") (v "0.3.22") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1ims2wyf9p5lbhg98ypinja5cmi2bs2sb5kiv4li46sjwlxbxn5v") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.23 (c (n "oro-shim-bin") (v "0.3.23") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "11za3qpmwvd012fqfnrmhzzjbs4q9qyimvpmrn35731cdz6yavh3") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.24 (c (n "oro-shim-bin") (v "0.3.24") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0b49gqig8qrr2dljysz1bvvf1fz79dwryzx33zfwk2s3dsi7vf2w") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.25 (c (n "oro-shim-bin") (v "0.3.25") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1ikdmk0qh4flmgfpdsp623cpfx76j8gv1421drfk7yrjznf0jwdc") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.26 (c (n "oro-shim-bin") (v "0.3.26") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1rvyxhb7dxahwnfj3fixsiya9v8myy2zk835s56vy179nz0gx88s") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.27 (c (n "oro-shim-bin") (v "0.3.27") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0bck26jyki1zd37gba8zlqqsr93dzg6sg28zqm2x2nycmi0m5675") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.28 (c (n "oro-shim-bin") (v "0.3.28") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1saa5z2allnywv9zd0c5s3f3il9wrldwlrf61jy4a895rsh7bwky") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.29 (c (n "oro-shim-bin") (v "0.3.29") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "16g8c89fi70d20pi1nxyapqs9zrh958qx2vg4909k56irgdagnr1") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.30 (c (n "oro-shim-bin") (v "0.3.30") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0v14yndp8jcm25q3jmgfhkbv69mzm29v6lrj3wrfhpkb5whzfb7p") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.31 (c (n "oro-shim-bin") (v "0.3.31") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0m1l9jd6pl0psk9n5napi1aajz03wf578rlib0hy44nk91g2whc9") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.32 (c (n "oro-shim-bin") (v "0.3.32") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1dmb0b31f0wbkwcxqk2pcd1pb7cbwcrfkav9fjh0nr67mby26wf3") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.33 (c (n "oro-shim-bin") (v "0.3.33") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0r0x7bbxk8myclz97xnkd6a6ccishjqh5ba046dqn2agz1xkgpgy") (r "1.67.1")))

(define-public crate-oro-shim-bin-0.3.34 (c (n "oro-shim-bin") (v "0.3.34") (d (list (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "161vmj693pfkil1hg266i8lxgnbla8pf0f5z851yr2vn47hbd02l") (r "1.67.1")))

