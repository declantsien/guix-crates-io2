(define-module (crates-io or o- oro-pretty-json) #:use-module (crates-io))

(define-public crate-oro-pretty-json-0.3.20 (c (n "oro-pretty-json") (v "0.3.20") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1gh875b58cn9ml5746ycn7w2ajijfa0y93lwnmi9jkycp9wz306p") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.22 (c (n "oro-pretty-json") (v "0.3.22") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1700bpfjdqa7d058brdv57rgyml7kp8xnjifrmg434cls1fmlqhr") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.23 (c (n "oro-pretty-json") (v "0.3.23") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "12yzbpiv7kbysvizzw6v3bn3a58ws9n8ggdpmzrr2p0a82g79xhv") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.24 (c (n "oro-pretty-json") (v "0.3.24") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1byxqqyd8j78sb57g7zv6bl0qc51w79m491rn3xkk4vsv0qjq61f") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.25 (c (n "oro-pretty-json") (v "0.3.25") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "012s2zmvbd3yj3mkzkw7qmkbrcnlfdz2k5sjaawmlw84vyya56pn") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.26 (c (n "oro-pretty-json") (v "0.3.26") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "069pf3x7a06v6ml086gd0igsclxv9cf7z0v122x12pqfsv1l8b0q") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.27 (c (n "oro-pretty-json") (v "0.3.27") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0fv919fpcv2bpddb01mqm2jm7j44rbns5sxlpin8mpigkcn4pgxk") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.28 (c (n "oro-pretty-json") (v "0.3.28") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1zj02q7vqsxkjaabnq5lwav9l0gy5mwykj0kbdh4gbgdms6wpvdy") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.29 (c (n "oro-pretty-json") (v "0.3.29") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0mhdp3fqzk51rzqfqlix6pas1n4yrzxv3hm6ask61zr1iraifd8s") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.30 (c (n "oro-pretty-json") (v "0.3.30") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1nk0kbqxy205jx2l002fig2mnzaaxw1iykqki0k34zavf9sjnqwn") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.31 (c (n "oro-pretty-json") (v "0.3.31") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "06swr03ql9wbwgxiz5mg2s0v0c6qk02y0kspfn7szirazj60q2xk") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.32 (c (n "oro-pretty-json") (v "0.3.32") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1c0fn78rn0qkm8qxxjng26a90bqlmf7q3y7gyzf8i67nqrbjw645") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.33 (c (n "oro-pretty-json") (v "0.3.33") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "17qgnz8ykfb73m64ql15l3v1n5lfvcm0abd51syyj7ba1vc28rqp") (r "1.67.1")))

(define-public crate-oro-pretty-json-0.3.34 (c (n "oro-pretty-json") (v "0.3.34") (d (list (d (n "serde_json") (r "^1.0.93") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1n4hjk2g9qfymzg9y2w58hdr8m7lz7cng4iyk532z7zcvsfsz06y") (r "1.67.1")))

