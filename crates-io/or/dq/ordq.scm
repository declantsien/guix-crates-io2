(define-module (crates-io or dq ordq) #:use-module (crates-io))

(define-public crate-ordq-0.1.0 (c (n "ordq") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)))) (h "1r1v76b4fm45ks404wsd1kvf61nfla483q8khfkdgbhlnq3680qd")))

(define-public crate-ordq-0.2.0 (c (n "ordq") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1aigxwnd4g9aq1gcbx2mr9raxc75fdjnkgmz9an8qnb1p02mwlvf")))

