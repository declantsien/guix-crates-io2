(define-module (crates-io or dl ordlebot) #:use-module (crates-io))

(define-public crate-ordlebot-0.1.0 (c (n "ordlebot") (v "0.1.0") (d (list (d (n "comfy-table") (r "^6.1.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "mersenne-twister-m") (r "^0.1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "roaring") (r "^0.10.1") (d #t) (k 0)))) (h "0amfw2ssw4jjlnlzffpy76knyq0b74rrs4vvd7gl270dag0115cy")))

