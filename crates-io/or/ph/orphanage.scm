(define-module (crates-io or ph orphanage) #:use-module (crates-io))

(define-public crate-orphanage-0.0.1 (c (n "orphanage") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("functions"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "16aw6jsi3ylk0f689sjsafhpajv79scdvq4d0mvdy780y65xsgfx") (r "1.64")))

(define-public crate-orphanage-0.0.2 (c (n "orphanage") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.77") (o #t) (d #t) (k 0)) (d (n "killswitch") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("functions"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (o #t) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "net" "time"))) (o #t) (d #t) (k 0)))) (h "01g40psbr6swgyid420wg9ggiqqvg4jhk39j1mhwjpy2x0ghyhdl") (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait" "dep:killswitch") ("rusqlite" "dep:rusqlite" "dep:sha2")))) (r "1.64")))

