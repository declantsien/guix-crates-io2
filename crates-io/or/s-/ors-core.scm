(define-module (crates-io or s- ors-core) #:use-module (crates-io))

(define-public crate-ors-core-0.0.1 (c (n "ors-core") (v "0.0.1") (h "01vl17xb1k26pagcv47binj8nq796ial7nxv0933b8j5zlx5wk79") (y #t)))

(define-public crate-ors-core-0.0.2 (c (n "ors-core") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0gawa8bxj3jmhgjkw287bxglmxbal93jgd2s9q5sxpcldy4rq39s") (y #t)))

