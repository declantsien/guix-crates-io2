(define-module (crates-io or ec orecc-front) #:use-module (crates-io))

(define-public crate-orecc-front-0.1.0 (c (n "orecc-front") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0n8f810knaxgjz6yci2czcl06q0mkk1nf26ygnjnw4gg71dr89cd") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.1 (c (n "orecc-front") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0gngzw04944481jyfwdwma19c37f2z618y5kgdf2sl7p9dgv79vz") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.2 (c (n "orecc-front") (v "0.1.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "1pp8bvmxrzbrcips7b4xn5ibnvnfa8zml6dmp5w7rrrwlb9qznwg") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.3 (c (n "orecc-front") (v "0.1.3") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "1072n4n5b7xxhddcp3d9yamki6k0kaanmwv214z5g0wy5jm20pmc") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.4 (c (n "orecc-front") (v "0.1.4") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "095mshj12clp69r47cc2cczzk487ij1x95s7z8hh4h69qsc18spm") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.5 (c (n "orecc-front") (v "0.1.5") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0gjmi7jxnx76lpkz65wp5p86fadqy2rljgnskpspdrp8hv35j8rz") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.6 (c (n "orecc-front") (v "0.1.6") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0x0amg1yzgvhb7sdrly6fivqh20afnql2p60yi22k6xk1mz235b3") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.7 (c (n "orecc-front") (v "0.1.7") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "1d4irb7qrfjvqkyzhwlwy2y8fqb0pw9dy1g7fyh4jpbmb3v74s62") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.8 (c (n "orecc-front") (v "0.1.8") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0z0f03zlla3kj0gsxmkm4887a2nlbj8z3lpic74vcrvjgbk1b6g0") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

(define-public crate-orecc-front-0.1.9 (c (n "orecc-front") (v "0.1.9") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.11") (o #t) (d #t) (k 0)))) (h "0mlcgvaj78wingzn69dcnpr7h8yhslmmyai2ifdsf8ms9pfkym1r") (s 2) (e (quote (("xid" "dep:unicode-ident"))))))

