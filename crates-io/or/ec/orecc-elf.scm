(define-module (crates-io or ec orecc-elf) #:use-module (crates-io))

(define-public crate-orecc-elf-0.0.1 (c (n "orecc-elf") (v "0.0.1") (h "094w26d89x4n39li2gwayi613rwdyy2g4w2gw4195zhb67dhv1ih")))

(define-public crate-orecc-elf-0.0.2 (c (n "orecc-elf") (v "0.0.2") (d (list (d (n "target-lexicon") (r ">=0.12.0") (o #t) (d #t) (k 0)))) (h "02ahm7x086z9ldlcsi4kblhyidxncjxhd1d70lcy5v4fidd03s8q") (s 2) (e (quote (("target-lexicon" "dep:target-lexicon"))))))

