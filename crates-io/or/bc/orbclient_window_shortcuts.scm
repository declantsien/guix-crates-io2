(define-module (crates-io or bc orbclient_window_shortcuts) #:use-module (crates-io))

(define-public crate-orbclient_window_shortcuts-0.1.0 (c (n "orbclient_window_shortcuts") (v "0.1.0") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)))) (h "1fgpndd197jmhqss2bgkp4lzpgdmamcpal2nw8bd5pbrb6bvngwp")))

(define-public crate-orbclient_window_shortcuts-0.1.1 (c (n "orbclient_window_shortcuts") (v "0.1.1") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)))) (h "08l15qpd4qirmwkajj1gbnni01iy5mhi8flm69229a0pzn941bzr")))

(define-public crate-orbclient_window_shortcuts-0.1.2 (c (n "orbclient_window_shortcuts") (v "0.1.2") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)))) (h "1zx1s0a2zyap8h6akrxwpa9wh43yrp3yyz2kl96l0csjnk91k0ii")))

(define-public crate-orbclient_window_shortcuts-0.1.3 (c (n "orbclient_window_shortcuts") (v "0.1.3") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)))) (h "1cf3i62qb4d48zyi6988dgdq67wswmc0fybdhr8980vq3blc9hh9")))

(define-public crate-orbclient_window_shortcuts-0.1.4 (c (n "orbclient_window_shortcuts") (v "0.1.4") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)))) (h "0sfjgvsa2hfmfv15r0axp1bqxpambimm0zz9cvg6ibg4h8pcqgd1")))

(define-public crate-orbclient_window_shortcuts-0.1.5 (c (n "orbclient_window_shortcuts") (v "0.1.5") (d (list (d (n "orbclient") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lhyrslgn5hxqs3vba8aqkdh6n04r35pd69xjjkfbac5b9fy841j")))

