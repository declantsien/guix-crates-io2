(define-module (crates-io or -r or-rs) #:use-module (crates-io))

(define-public crate-or-rs-0.1.0 (c (n "or-rs") (v "0.1.0") (h "1yan7b10aw7k3xrhc1plp6mh98cqmpkz1y1hax8z9kzilsw2qav0") (f (quote (("unstable_feature") ("default")))) (r "1.60")))

(define-public crate-or-rs-0.1.1 (c (n "or-rs") (v "0.1.1") (h "1a52igm2n55jgrgz03nrcng8if69i1mzgpa699pk39b1dkir18n0") (f (quote (("unstable_feature") ("default")))) (r "1.60")))

(define-public crate-or-rs-0.1.2 (c (n "or-rs") (v "0.1.2") (h "1xh0qvj7yyr5gij5zm6q3bih62629w4h0k08bzckh1rmlm18f1cz") (f (quote (("unstable_feature") ("default")))) (r "1.60")))

