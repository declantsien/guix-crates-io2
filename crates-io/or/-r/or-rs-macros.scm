(define-module (crates-io or -r or-rs-macros) #:use-module (crates-io))

(define-public crate-or-rs-macros-0.1.0 (c (n "or-rs-macros") (v "0.1.0") (d (list (d (n "colored") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qmsqwf9bxv366hgqsafqwh96fkbsk8rq05v2085gn393fpqwv3v") (f (quote (("macro_error_debugging" "colored" "proc-macro2/span-locations") ("default" "proc-macro2")))) (r "1.60")))

(define-public crate-or-rs-macros-0.1.1 (c (n "or-rs-macros") (v "0.1.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pmv020srf25vsamh0xbay8dd46zjscakkz8y1x8piw8f2p82q32") (f (quote (("macro_error_debugging" "colored" "proc-macro2/span-locations") ("default" "proc-macro2")))) (r "1.60")))

