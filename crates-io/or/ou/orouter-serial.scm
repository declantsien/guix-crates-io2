(define-module (crates-io or ou orouter-serial) #:use-module (crates-io))

(define-public crate-orouter-serial-0.1.0 (c (n "orouter-serial") (v "0.1.0") (d (list (d (n "base16") (r "^0.2") (k 0)) (d (n "cobs") (r "^0.1") (k 0)) (d (n "defmt") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "1pnhv6zybkiax633m3qcf4k74mqvj2wpcxbqv4pxy5zxfq27i5zf") (f (quote (("std") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default" "std"))))))

(define-public crate-orouter-serial-0.1.1 (c (n "orouter-serial") (v "0.1.1") (d (list (d (n "base16") (r "^0.2") (k 0)) (d (n "cobs") (r "^0.1") (k 0)) (d (n "defmt") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "1dpb4i22mfgmdplkhzm5vfhl6q4kckiyhkkss1r04mvjhp1ynygq") (f (quote (("std") ("defmt-impl" "defmt" "heapless/defmt-impl") ("default" "std"))))))

