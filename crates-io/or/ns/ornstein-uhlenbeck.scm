(define-module (crates-io or ns ornstein-uhlenbeck) #:use-module (crates-io))

(define-public crate-ornstein-uhlenbeck-0.1.0 (c (n "ornstein-uhlenbeck") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 0)))) (h "00zs5yxhbhkj81741imhsc57i48pf8swvvdd475s1kpk4cgc8n45")))

