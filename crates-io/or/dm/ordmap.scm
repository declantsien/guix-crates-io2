(define-module (crates-io or dm ordmap) #:use-module (crates-io))

(define-public crate-ordmap-1.0.0 (c (n "ordmap") (v "1.0.0") (h "19zvhkhfv109blyj8sxw1ncnrh66k4dimvrmnicwnvz4scb58s47")))

(define-public crate-ordmap-1.1.0 (c (n "ordmap") (v "1.1.0") (h "16xp89m6ld4c4ixqb4gx463v35akvphn089k1yj4f2jfgrnxws30")))

