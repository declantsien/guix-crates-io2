(define-module (crates-io or gd orgdt) #:use-module (crates-io))

(define-public crate-orgdt-0.1.0 (c (n "orgdt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zff731zxvc2a8ibzlnim3f0aqyc6631sah3h425pkndm4gynqv6")))

(define-public crate-orgdt-0.1.1 (c (n "orgdt") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00l9amkwjmdz0fcmds979myn1mgb9lhpijxyzsxf9bmx73qigif2")))

