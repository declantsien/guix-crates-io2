(define-module (crates-io or go orgora) #:use-module (crates-io))

(define-public crate-orgora-0.1.0 (c (n "orgora") (v "0.1.0") (d (list (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "107ql40r3axjw0dphqy8mlys09xbgq7asic6jvljjlgb71422h20")))

(define-public crate-orgora-0.1.1 (c (n "orgora") (v "0.1.1") (d (list (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0841pzysfs720sp0qkxxn6pw5cwzr72543gc3jx3qnzgdphqkbgv")))

