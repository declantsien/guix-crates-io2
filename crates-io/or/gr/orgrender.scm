(define-module (crates-io or gr orgrender) #:use-module (crates-io))

(define-public crate-orgrender-0.1.0 (c (n "orgrender") (v "0.1.0") (d (list (d (n "orgize") (r "^0.9.0") (d #t) (k 0)))) (h "01rra0d2x9c8wjqczsp63zmmb6x14jln5b82ni2ddx6a64vhvmg2")))

(define-public crate-orgrender-1.0.0 (c (n "orgrender") (v "1.0.0") (d (list (d (n "orgize") (r "^0.9.0") (d #t) (k 0)))) (h "0bjnd0j91aqfzi7xqq9iaa11mjgxz4lipz323d9qvl1ampil5c3r")))

