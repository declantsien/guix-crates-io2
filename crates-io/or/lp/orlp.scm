(define-module (crates-io or lp orlp) #:use-module (crates-io))

(define-public crate-orlp-0.0.0 (c (n "orlp") (v "0.0.0") (h "1n4rbxdsy8cbi9j3g9dqzc79rij7wprg2vf72y793qak7334nnf5")))

(define-public crate-orlp-0.1.0 (c (n "orlp") (v "0.1.0") (h "0i5kpkpcksk8l4nwgmhnl2jdzgl4xx03qp4nfckw51wc8jr1mq1g") (f (quote (("unstable") ("std") ("default" "std"))))))

