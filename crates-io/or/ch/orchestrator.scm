(define-module (crates-io or ch orchestrator) #:use-module (crates-io))

(define-public crate-orchestrator-0.1.0 (c (n "orchestrator") (v "0.1.0") (h "0cpvya7f0fhj74gqxnxnq2xcac3nrycz3sch3j0qissgvbj8kppj")))

(define-public crate-orchestrator-0.2.0 (c (n "orchestrator") (v "0.2.0") (h "1bfwi95mvvls1jrswmcy9p5c1lmn3vdsb66y7xahymbwamfsrps9")))

(define-public crate-orchestrator-0.2.2 (c (n "orchestrator") (v "0.2.2") (h "0yj0pd3nzdlaayklh2py713708vi5gwy0gxmmcv27m7x3yzlvksv")))

