(define-module (crates-io or di ordinal-type) #:use-module (crates-io))

(define-public crate-ordinal-type-0.1.0 (c (n "ordinal-type") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)))) (h "0b625dlbjak5gbq0bj9nqjp104dzcvijy57hcai15zd2w24v7v7y")))

(define-public crate-ordinal-type-0.2.0 (c (n "ordinal-type") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1f41d8zmyqy8v9yb6qbdn0lyfl44kqxg67a07nil072axj536lks")))

(define-public crate-ordinal-type-0.3.0 (c (n "ordinal-type") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "12vy3cnyfb6lh353dj7akgir4x5k5293z7sfwwdy8fm3fcd9mrp6")))

