(define-module (crates-io or di ordinalizer) #:use-module (crates-io))

(define-public crate-ordinalizer-0.1.0 (c (n "ordinalizer") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hbwmm4yra6sdlv6ll0kgs93xayw8jrhdagnrzni3pp3kchwjcgn")))

