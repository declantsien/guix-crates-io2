(define-module (crates-io or di ordist) #:use-module (crates-io))

(define-public crate-ordist-0.1.0 (c (n "ordist") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0kx0kqw49qgcr44mc86hsq9cc3kx518x03jiv39nr7kh247y2jhf")))

(define-public crate-ordist-0.1.1 (c (n "ordist") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1s3v6yp2cqk8vk3in4hrivsdzylxiffrns4a8lcha0a3igvdj2kn")))

(define-public crate-ordist-0.2.0 (c (n "ordist") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i179ri3zbnzr1w70n7m7n8da5ycd659z29jj4nsi8ygnmxjii1b")))

