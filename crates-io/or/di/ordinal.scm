(define-module (crates-io or di ordinal) #:use-module (crates-io))

(define-public crate-ordinal-0.1.0 (c (n "ordinal") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "11y6kqql1skgm80hhwdbg26w126f74aj6w5fhllqjk00dpkzr739")))

(define-public crate-ordinal-0.2.0 (c (n "ordinal") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "11nj95fbjmk3qdna7d2qrnz3v8276vls3d0jn5l6d86sm4r5rp33")))

(define-public crate-ordinal-0.2.1 (c (n "ordinal") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1gvygr5x675jvspwx0i9sa7pr9szp4kn3kxr5dhnhr6y193w9gfb")))

(define-public crate-ordinal-0.2.2 (c (n "ordinal") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1crmvz0cqxmgjrpac7ckhg0ya6dj0wp1q5jsf74f1a91rgj1cx4a")))

(define-public crate-ordinal-0.2.3 (c (n "ordinal") (v "0.2.3") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "02sclvxymbzyakk6dmcmsjfasrmxgfcs7j289d2sgvd870clahvp")))

(define-public crate-ordinal-0.3.0 (c (n "ordinal") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1hwvhlwchnkrk95z2kq728dvkdjjgdkabzwyy45i57x3zhcgf58i")))

(define-public crate-ordinal-0.3.1 (c (n "ordinal") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1kqhqpwd2f29fc5j8w8ddzi24kxwinaagb5c5wi6mr8fihd0s85r")))

(define-public crate-ordinal-0.3.2 (c (n "ordinal") (v "0.3.2") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "10qhhll61kd6vq3zc0l9ad8b4wiip20fnzbdf22qk7bfyhq1a368")))

