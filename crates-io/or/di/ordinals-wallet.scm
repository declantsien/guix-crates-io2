(define-module (crates-io or di ordinals-wallet) #:use-module (crates-io))

(define-public crate-ordinals-wallet-0.0.1 (c (n "ordinals-wallet") (v "0.0.1") (h "16vqxdpnwc2j3qm0ncb047nj74d5l47q6qb648ll0l9j3bg0pqsv")))

(define-public crate-ordinals-wallet-0.0.2 (c (n "ordinals-wallet") (v "0.0.2") (h "17z81cvipibf3wbiyqakkrnzlp93822qas5blr37mazvn4bmqw8l")))

