(define-module (crates-io or di ordi) #:use-module (crates-io))

(define-public crate-ordi-0.1.0 (c (n "ordi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc") (r "^0.17.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rusty-leveldb") (r "^2.0.0") (d #t) (k 0)) (d (n "seek_bufread") (r "^1.2.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "024x21g0jb7sjy8cbxq7klpw9p84cjbz9x68krf20hkacz7f4a1l")))

