(define-module (crates-io or c- orc-format) #:use-module (crates-io))

(define-public crate-orc-format-0.1.0 (c (n "orc-format") (v "0.1.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)))) (h "0m8pqcj1lhid8gl4izmg0sf7bvrnky3npgwjfa1hfb2pbwr4y2lf")))

(define-public crate-orc-format-0.1.1 (c (n "orc-format") (v "0.1.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)))) (h "0pp7aqyzd8icjw4fslnqcnbn8kjidf65bsqg2v56civhq4xhvph2")))

(define-public crate-orc-format-0.2.0 (c (n "orc-format") (v "0.2.0") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)))) (h "0k03fdlqfsjhf0yslq0cvn6nc8vhcvp8m58b2v7jan3im1ycgmg1")))

(define-public crate-orc-format-0.3.0 (c (n "orc-format") (v "0.3.0") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)))) (h "02qzqhy1zx9bmylvkmbjrc2mxyddjgn2sqiwwd7kr9zh2p7jsaj0")))

