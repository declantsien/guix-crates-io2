(define-module (crates-io or c- orc-rs) #:use-module (crates-io))

(define-public crate-orc-rs-0.1.3 (c (n "orc-rs") (v "0.1.3") (h "0jb9drqsam5821wngq88a3nh364r5a7vnwbalkqb99i8n92cb2p7")))

(define-public crate-orc-rs-0.1.4 (c (n "orc-rs") (v "0.1.4") (h "1qzbsw9chhc4f70n2gp5dkn2dwb42ybr4x4pvswmj0dbwmw83661")))

(define-public crate-orc-rs-0.1.5 (c (n "orc-rs") (v "0.1.5") (h "0xazac1hf2syhnik7jhnfh2hvqvfsif0v005gr0k190hkciiyn6c")))

