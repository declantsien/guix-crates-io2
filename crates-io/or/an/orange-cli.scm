(define-module (crates-io or an orange-cli) #:use-module (crates-io))

(define-public crate-orange-cli-0.1.0 (c (n "orange-cli") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "serialport") (r "^3.3") (k 0)))) (h "0gmxdhhbrf8jdlv1p8xp8n8jvnr4kvlsd6ipdr4f4wpccd1sc9rn")))

