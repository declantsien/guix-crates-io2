(define-module (crates-io or an orange) #:use-module (crates-io))

(define-public crate-orange-0.1.0 (c (n "orange") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "11cyh6v4plypzq3f8wfh48xgzn3hazw15p5vy80av0i3sa1q9apv")))

(define-public crate-orange-0.1.1 (c (n "orange") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0nihh60f48a6y4vrrcmjdqwpwili68m016rd796dx2rmh8jcgrbr")))

(define-public crate-orange-0.1.2 (c (n "orange") (v "0.1.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1f669k2ab4m5acm3gx36lw9431jsilxcr2mhvmipq0973hjy1a53")))

(define-public crate-orange-0.1.3 (c (n "orange") (v "0.1.3") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "15r1gzsnkq9fjr6ykjk8miafzl5v5hgijhmpzx50isy2rp95phk1")))

