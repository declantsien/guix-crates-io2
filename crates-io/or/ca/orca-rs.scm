(define-module (crates-io or ca orca-rs) #:use-module (crates-io))

(define-public crate-orca-rs-0.1.1 (c (n "orca-rs") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.14") (d #t) (k 0)))) (h "12f1dl41rkb4443ph4kqa29bydf701692igdigh7i37hx23108vv") (f (quote (("participant_obstacles") ("default" "participant_obstacles"))))))

(define-public crate-orca-rs-0.1.2 (c (n "orca-rs") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1mfh4jd68sw1b0p72z5y5p43j0b8v6an92ksv3ky7srn6d3g6zj0") (f (quote (("participant_obstacles") ("default" "participant_obstacles"))))))

(define-public crate-orca-rs-0.2.0 (c (n "orca-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1g78jjb8lhq30d9s29wqwj16dnjzyi4zmfhmjcgd45s41g82r9fs") (f (quote (("participant_obstacles") ("default" "participant_obstacles"))))))

(define-public crate-orca-rs-0.2.1 (c (n "orca-rs") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1zk25mjkx3hkx7q4q4fky5g8ghslc7g4grvjkdrb2f1pbpagf5rs") (f (quote (("participant_obstacles") ("default" "participant_obstacles"))))))

(define-public crate-orca-rs-0.3.0 (c (n "orca-rs") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1l3nff12ipb3bxfnccsf6m3lh50qkk58ivqv81d275a6crx4z39h") (f (quote (("participant_obstacles") ("default" "participant_obstacles"))))))

