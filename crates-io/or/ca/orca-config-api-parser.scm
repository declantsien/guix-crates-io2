(define-module (crates-io or ca orca-config-api-parser) #:use-module (crates-io))

(define-public crate-orca-config-api-parser-0.1.0 (c (n "orca-config-api-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1kb81438l2byk6spn4yjadsb84yis1mikrzgrw9aia24w4d34ncp")))

