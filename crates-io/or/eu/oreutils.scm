(define-module (crates-io or eu oreutils) #:use-module (crates-io))

(define-public crate-oreutils-0.0.1 (c (n "oreutils") (v "0.0.1") (d (list (d (n "exa") (r "^0.8.0") (d #t) (k 0)))) (h "1zshjq42bpm4bg78n0pb2pq9cpjgr17z1cjcwsvbrgf08rnw77mx")))

(define-public crate-oreutils-0.0.2 (c (n "oreutils") (v "0.0.2") (d (list (d (n "env_proxy") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "1h2gh00lgmsqlwxxl0mm3d9izav2bxbl9jw5ra4l55p1d70sy1mp")))

(define-public crate-oreutils-0.0.3 (c (n "oreutils") (v "0.0.3") (d (list (d (n "env_proxy") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "1hsk1i751fkbsbd8z9pmxjnyrcl0z6ijhrkg4qx74minzjs3izs2")))

