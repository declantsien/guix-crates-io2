(define-module (crates-io or de ordes) #:use-module (crates-io))

(define-public crate-ordes-0.1.0 (c (n "ordes") (v "0.1.0") (d (list (d (n "ordes-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "1y8yyyn3dv3vqib40w0gpvh3km7i9sblkii0s936gk8abpz2agbf") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.2.0 (c (n "ordes") (v "0.2.0") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "0ic70vrf5awnh0d63x3gp6pj8h76kg7zvyqpcrmddmvv95agy8sl") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.2.1 (c (n "ordes") (v "0.2.1") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "04j2qb8d1sy5s1zl3ranyzn59cb3cwlx92hckspvzz6hgpd6wx5n") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.2.2 (c (n "ordes") (v "0.2.2") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "06rqig9xfmp38zfp9lhvzkr2gfy35hfj8g5agmrdzf167wqn00a8") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.2.3 (c (n "ordes") (v "0.2.3") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "1yyn8f63qf1bvaz6fdvik986y1l93rmddljcf2mzy30vw9pai2xw") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.2.4 (c (n "ordes") (v "0.2.4") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "17kkm6731p827rl7rl73hy79w4pkmwjxkpqm07y4hk546ik0xkr7") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.3.0 (c (n "ordes") (v "0.3.0") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "0dkvxw7jq4qhx82msijgwbakcp84r3b4w8x9krq2yyrdki79mzyj") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.3.1 (c (n "ordes") (v "0.3.1") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1qj9q6nrjyrzapj2yi5jn86zxhqpgc4957kshzq8vsnqghhcbr2v") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.3.2 (c (n "ordes") (v "0.3.2") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "07wg60vq87lb679c27gbxd067hpp7sbxn91xk7pn3bgwhi9m4z26") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.3.3 (c (n "ordes") (v "0.3.3") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "0h8fp5m3fhwslsbisilycxmcffmh07zz1c2z08l19kb7wi1r3ckl") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

(define-public crate-ordes-0.3.4 (c (n "ordes") (v "0.3.4") (d (list (d (n "ordes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "0163kblpw392vpgs695h274wsxkz8qwrmgg38jb7hygii5labp5g") (f (quote (("len_all" "len_32_64" "len_64_128" "len_128_256" "len_256_512" "len_512_1024") ("len_64_128") ("len_512_1024") ("len_32_64") ("len_256_512") ("len_128_256") ("const_generics"))))))

