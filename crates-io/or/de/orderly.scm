(define-module (crates-io or de orderly) #:use-module (crates-io))

(define-public crate-orderly-0.1.0 (c (n "orderly") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "0vf0y9g4bxylk6s6yd1i6sm452jg4jfkyijaf2zjrqg5hm6ms92k")))

(define-public crate-orderly-0.2.0 (c (n "orderly") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "05m5r1sjv08bgj7lx4yx45ysr3q2wn9kr2vbagx96had35fwg3nr")))

(define-public crate-orderly-0.3.0 (c (n "orderly") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "1l2qfs0yd140bb7iffv0j4q5wmn0djl5f7cw90q0kwyf78d3pkfg")))

(define-public crate-orderly-0.4.0 (c (n "orderly") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "10d1q4zp2fvx03fjcvlzpr7793hmmsmaydi0k5zwxg43bd7iy908")))

(define-public crate-orderly-0.5.0 (c (n "orderly") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "12qp9sg7ng3w3y97nm2ma2ygaild5gsc9zghlfnrgaqhnfv5y8qm")))

(define-public crate-orderly-0.5.1 (c (n "orderly") (v "0.5.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "1gk0l15l7rnh6pjp090ryxplnfdnhi6n1irsdgljsrizn2rcrja2")))

