(define-module (crates-io or de ordes-macros) #:use-module (crates-io))

(define-public crate-ordes-macros-0.1.0 (c (n "ordes-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05ddnbr032qilizi8s3ap8l518jac02y930hikz59c6qn25lbgdh")))

(define-public crate-ordes-macros-0.2.0 (c (n "ordes-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dsb647f0ypx9l3scmcrsl1wjqzlxrfdw07bkfg6m3ivrxh8yvgs")))

(define-public crate-ordes-macros-0.2.1 (c (n "ordes-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0g6npa3hm19m00d854g692b88i8j1kx1yw18g87zca6zw57s9yaa")))

