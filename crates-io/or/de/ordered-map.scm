(define-module (crates-io or de ordered-map) #:use-module (crates-io))

(define-public crate-ordered-map-0.1.0 (c (n "ordered-map") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "13qd5pzl00ln3pgxhq2ccqprpg645k9s94g6msf21lbrfk0p1iy5")))

(define-public crate-ordered-map-0.1.1 (c (n "ordered-map") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "1czils76mlss1x0a57gzlbknrpff25pzd01v4bnxwp22g10305qz")))

(define-public crate-ordered-map-0.3.1 (c (n "ordered-map") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "1d9q1257dh9k5n7mkk3javly2lh5l3kw6c8sb2d61rd8p6ywgfx4")))

(define-public crate-ordered-map-0.3.2 (c (n "ordered-map") (v "0.3.2") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "17gva13pvxlsqi7z9may4jjw09j33p3vw6jgwaw52x11khkigfs2")))

(define-public crate-ordered-map-0.4.0 (c (n "ordered-map") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "07x2h9s8vz5jnx4knywlx9arhigphzmqmhzjyqjrsss76zp22xl4")))

(define-public crate-ordered-map-0.4.1 (c (n "ordered-map") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "0gg1qiqpx3zkn6afxxl1j3c3zsscvwlvr0n76xjp3smb9sj2x1w4")))

(define-public crate-ordered-map-0.4.2 (c (n "ordered-map") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)))) (h "0w1rjy7xh7k7jc2clp0d5sspqs4zw4zvn78m9fi1m0bcl2jg9j3a")))

