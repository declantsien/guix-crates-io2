(define-module (crates-io or de ordered_vec) #:use-module (crates-io))

(define-public crate-ordered_vec-0.1.0 (c (n "ordered_vec") (v "0.1.0") (h "0cnxk8va1fds3dc5d84gvz2jmah9brmk77b42m1lvz4gx8k9kq9h")))

(define-public crate-ordered_vec-0.2.0 (c (n "ordered_vec") (v "0.2.0") (h "06sab7rpdfagl523g00vmf5m2icr1rpf0zfawwjdgr8fi2901fkv")))

