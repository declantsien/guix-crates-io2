(define-module (crates-io or de ordered-stream) #:use-module (crates-io))

(define-public crate-ordered-stream-0.0.1 (c (n "ordered-stream") (v "0.0.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1cfc4mgsl29ij9g27hfxlk51jcg35kdv2ldapl46xzdckq2hqqs4")))

(define-public crate-ordered-stream-0.1.0 (c (n "ordered-stream") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1qhyic5mflxqaac0y3qd1bdzx412c485ksg31qd07rjdbxdw3xnd")))

(define-public crate-ordered-stream-0.1.1 (c (n "ordered-stream") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1vsy52r4wiz3cpj0fhg77d9imngvfw2ykfr4hj6mw94b062f6k03")))

(define-public crate-ordered-stream-0.1.2 (c (n "ordered-stream") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1shqm170sp1nbmphiyh18yfgs2rcqa9gdyaq2fn94viwsycqrjh1")))

(define-public crate-ordered-stream-0.1.3 (c (n "ordered-stream") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.25") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "06iqrz7rpjd8lc6r1sv1sn9ixj39l5fdw8minz9vsbg4yfirpsyl")))

(define-public crate-ordered-stream-0.1.4 (c (n "ordered-stream") (v "0.1.4") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.25") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)))) (h "140c0h0ap30mcfhdk2xy1q9iqyb450wh11dglshh2y6vmjyj82in")))

(define-public crate-ordered-stream-0.2.0 (c (n "ordered-stream") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.25") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)))) (h "0l0xxp697q7wiix1gnfn66xsss7fdhfivl2k7bvpjs4i3lgb18ls")))

