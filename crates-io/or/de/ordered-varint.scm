(define-module (crates-io or de ordered-varint) #:use-module (crates-io))

(define-public crate-ordered-varint-0.0.0-reserve.0 (c (n "ordered-varint") (v "0.0.0-reserve.0") (h "1ba76dhcsbia8fbw4hip9scz53gqzlz6ins85y81c3l42d4f6s1f")))

(define-public crate-ordered-varint-0.1.0-dev.0 (c (n "ordered-varint") (v "0.1.0-dev.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "12nzivlgqgyqm3ycxfyjyv7g8aqad7d89iwlk147xwqra5xhx0nz")))

(define-public crate-ordered-varint-0.1.0-dev.1 (c (n "ordered-varint") (v "0.1.0-dev.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "070116shwx4p8bzb5a4ng6s7cd3qryj7mxjfp23qr3bq704pd6v1")))

(define-public crate-ordered-varint-1.0.0 (c (n "ordered-varint") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01qp3ymjdl69ff837x309balip1wrz4ffmwwr3nc0xq5b38yq2jv")))

(define-public crate-ordered-varint-1.0.1 (c (n "ordered-varint") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "16ivcab9404dx415qy8gz9kgjnj6dxkfqn4sz3giyrqlybx2wa36")))

(define-public crate-ordered-varint-2.0.0 (c (n "ordered-varint") (v "2.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0k5kkmp17h5lcslhi1rhfrg1xwdqzrci5nkbf80ixbabmcc9zk79")))

