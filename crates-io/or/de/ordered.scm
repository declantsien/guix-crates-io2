(define-module (crates-io or de ordered) #:use-module (crates-io))

(define-public crate-ordered-0.1.0 (c (n "ordered") (v "0.1.0") (h "1pk3m6dmyypl1swmyl0n4ppblyhjvxa66vkc3sww3q9x1z4znjlh")))

(define-public crate-ordered-0.1.1 (c (n "ordered") (v "0.1.1") (h "1viq0kgx6h2r19vh70sh0gl2y0xlq529q63vw8k646aczsajlr2d") (r "1.56.1")))

(define-public crate-ordered-0.2.0 (c (n "ordered") (v "0.2.0") (h "0r9v7d8zfvmnnzv6j8jgx5g3ymbz8k6mfqbmzych6irfqm41za6s") (r "1.56.1")))

(define-public crate-ordered-0.2.1 (c (n "ordered") (v "0.2.1") (h "0q5dissydmlkyniaw5vd6ymf7g2bmdmjm9viz2x4glcb1842xsq3") (r "1.56.1")))

(define-public crate-ordered-0.2.2 (c (n "ordered") (v "0.2.2") (h "16wm1gw49fmlw8gwdj1ay7q2j947q6pipqyasn5va2za7m9l41kz") (r "1.56.1")))

