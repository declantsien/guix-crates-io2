(define-module (crates-io or de order-stat) #:use-module (crates-io))

(define-public crate-order-stat-0.1.0 (c (n "order-stat") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1wapgqghz182qsgqpqp7dzdf1xgn58wnvclf71hcs5l5b7fskwqr") (f (quote (("experimental"))))))

(define-public crate-order-stat-0.1.1 (c (n "order-stat") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0p9l1r98ihsv3rr6a2ld8xgbcj7ggimx76lnxxfzxhmpbsk780kv") (f (quote (("experimental")))) (y #t)))

(define-public crate-order-stat-0.1.2 (c (n "order-stat") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vg7y8r7pzrb9hzws9khgjqscd290y7s6zl01hr3nj78h87ai2m4") (f (quote (("experimental"))))))

(define-public crate-order-stat-0.1.3 (c (n "order-stat") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1sq5844wv1mibn9i6fw4flinzq7zy2v1j5xz9l9n2dkx27akb9gg") (f (quote (("unstable"))))))

