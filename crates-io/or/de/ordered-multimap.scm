(define-module (crates-io or de ordered-multimap) #:use-module (crates-io))

(define-public crate-ordered-multimap-0.1.0 (c (n "ordered-multimap") (v "0.1.0") (d (list (d (n "dlv-list") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.2") (d #t) (k 0)))) (h "1696r9rcqzgw4jvi44xrmpab8y1vfg55lb5p8w6c8rq812k66h8g") (y #t)))

(define-public crate-ordered-multimap-0.2.0 (c (n "ordered-multimap") (v "0.2.0") (d (list (d (n "dlv-list") (r "^0.2.0") (d #t) (k 0)))) (h "1lmk013pfsnlh9zl2hc53km0rwh9mn09bkgjbn2si682dlwgrik9")))

(define-public crate-ordered-multimap-0.2.1 (c (n "ordered-multimap") (v "0.2.1") (d (list (d (n "dlv-list") (r "^0.2.1") (d #t) (k 0)))) (h "07ddyicdrw1ykpgh1jh39kfn164zrdnawp340w1qg5hmiwdfbf10")))

(define-public crate-ordered-multimap-0.2.2 (c (n "ordered-multimap") (v "0.2.2") (d (list (d (n "dlv-list") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.3.0") (d #t) (k 0)))) (h "0ibkzybijqp49l3qls9l9x3wg60yf0ncd4q9rpx9w9ph33jx2bi0")))

(define-public crate-ordered-multimap-0.2.3 (c (n "ordered-multimap") (v "0.2.3") (d (list (d (n "dlv-list") (r "^0.2.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.0") (d #t) (k 0)))) (h "1k9p4ra46ij9cyjv7sqpqsryc6bxzk4gagl7854hgrpwn204p6vn")))

(define-public crate-ordered-multimap-0.2.4 (c (n "ordered-multimap") (v "0.2.4") (d (list (d (n "dlv-list") (r "^0.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 0)))) (h "0n53fbc1ddi9vkpcwfplz2l4mb0pbhva5y3c1vsyzmcrcxy993z8")))

(define-public crate-ordered-multimap-0.3.0 (c (n "ordered-multimap") (v "0.3.0") (d (list (d (n "dlv-list") (r "^0.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)))) (h "0swvl3j0g1vga4nm8skw3bw7x3iy50gqji2k23amkly20s2xz9l7")))

(define-public crate-ordered-multimap-0.3.1 (c (n "ordered-multimap") (v "0.3.1") (d (list (d (n "dlv-list") (r "^0.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1194q7sb2d6chbllsn7237dhhvx04iqr3sq0ii16w1pcv5x2qrqw")))

(define-public crate-ordered-multimap-0.4.0 (c (n "ordered-multimap") (v "0.4.0") (d (list (d (n "dlv-list") (r "^0.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1crdby4x1m8n3lg82s5kdclpbz23hkd7841jcx560jzsx5fplrd4")))

(define-public crate-ordered-multimap-0.4.1 (c (n "ordered-multimap") (v "0.4.1") (d (list (d (n "dlv-list") (r "^0.2.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "01jb8f9d45s50naqsk3ribjlrvxnrkwl7ic28kbyf2hawwlgl3cr")))

(define-public crate-ordered-multimap-0.4.2 (c (n "ordered-multimap") (v "0.4.2") (d (list (d (n "dlv-list") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0l7z0lcmjk1wf4iskxq0wgx32dw1l6fxzrvlin5nzldaq1gnqivv")))

(define-public crate-ordered-multimap-0.4.3 (c (n "ordered-multimap") (v "0.4.3") (d (list (d (n "dlv-list") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0jljv1257pfyf855jlwwas5mqkzk40b9lqfx40f73qbpf7ildmyc")))

(define-public crate-ordered-multimap-0.5.0 (c (n "ordered-multimap") (v "0.5.0") (d (list (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.4.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "19f67zalagl34jx4lvdywmgksmbj58inknbkkf73vhqalawcn82s")))

(define-public crate-ordered-multimap-0.6.0 (c (n "ordered-multimap") (v "0.6.0") (d (list (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.13.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "03k9jx3irxldq4hp462ld25fcr03xcycd2sc73jl9rwqivqarn2f") (f (quote (("std" "dlv-list/std") ("default" "std"))))))

(define-public crate-ordered-multimap-0.7.0 (c (n "ordered-multimap") (v "0.7.0") (d (list (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "1f8xqcyypm0pqxgw54ff0691g97k6m45qfhlqq6ld9khcpk4xn04") (f (quote (("std" "dlv-list/std") ("default" "std"))))))

(define-public crate-ordered-multimap-0.7.1 (c (n "ordered-multimap") (v "0.7.1") (d (list (d (n "coverage-helper") (r "^0.2.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "13ri7lq4w2ky9f3q848bpjqkg6sydyzr2q1y6z1g05675z1aimm4") (f (quote (("std" "dlv-list/std") ("default" "std")))) (y #t)))

(define-public crate-ordered-multimap-0.7.2 (c (n "ordered-multimap") (v "0.7.2") (d (list (d (n "coverage-helper") (r "^0.2.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "0fgaaqa5q21fn39lqrsvsbzj31c9lc80j7hvybw2wzwnpclg0m8d") (f (quote (("std" "dlv-list/std") ("default" "std")))) (y #t) (r "1.64.0")))

(define-public crate-ordered-multimap-0.7.3 (c (n "ordered-multimap") (v "0.7.3") (d (list (d (n "coverage-helper") (r "^0.2.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "0ygg08g2h381r3zbclba4zx4amm25zd2hsqqmlxljc00mvf3q829") (f (quote (("std" "dlv-list/std") ("default" "std")))) (r "1.64.0")))

(define-public crate-ordered-multimap-0.7.4 (c (n "ordered-multimap") (v "0.7.4") (d (list (d (n "coverage-helper") (r "^0.2.0") (d #t) (k 2)) (d (n "dlv-list") (r "^0.5") (k 0)) (d (n "hashbrown") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "1jcijbq7zj1nb7p6fbfqrm8v2vch2w6g6a276svzrc2x9rxq58if") (f (quote (("std" "dlv-list/std") ("default" "std")))) (y #t) (r "1.71.1")))

