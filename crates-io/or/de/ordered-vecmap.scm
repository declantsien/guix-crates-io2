(define-module (crates-io or de ordered-vecmap) #:use-module (crates-io))

(define-public crate-ordered-vecmap-0.0.1 (c (n "ordered-vecmap") (v "0.0.1") (h "1iblc03559873k5hf6d7blb7di68baqaqpkwvg2axhylb2l1qjqz") (y #t)))

(define-public crate-ordered-vecmap-0.1.0 (c (n "ordered-vecmap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1vw6p293k6vkjhrjprzdmh4b28r0hl4szgl8badbb95ca83qkkn1")))

(define-public crate-ordered-vecmap-0.2.0 (c (n "ordered-vecmap") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("alloc"))) (o #t) (k 0)))) (h "08nxlk5c65pnqg2pxn3560cackwhg2ynhq4slyz6an21pxc4yliw")))

