(define-module (crates-io or de ordered_hash_map) #:use-module (crates-io))

(define-public crate-ordered_hash_map-0.1.0 (c (n "ordered_hash_map") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "16rp92nfzadrjizc3v47i77f662dlhln4nd3zd8g026lcdwsn5m6") (r "1.65.0")))

(define-public crate-ordered_hash_map-0.2.0 (c (n "ordered_hash_map") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1ajfqwnmb8s5i2v299prgp3pf9p4slwmhfg6vcjv4vqm2w29jhb9") (r "1.65.0")))

(define-public crate-ordered_hash_map-0.3.0 (c (n "ordered_hash_map") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1fqzb9symy8qygf5v7xv117kx9wsyp6fjvxn5avw6nf24zh0y39r") (r "1.65.0")))

(define-public crate-ordered_hash_map-0.3.1 (c (n "ordered_hash_map") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "03p1xpkjn26rm94j9zqdpz3yhkgkfxvlhv3wbfps26h702s10lzp") (r "1.65.0")))

(define-public crate-ordered_hash_map-0.4.0 (c (n "ordered_hash_map") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0xn1ww62fl5izfkfnq0jihngma0kg0j7922ahnylml3dpwi5y3mb") (r "1.65.0")))

