(define-module (crates-io or de ordered-streams) #:use-module (crates-io))

(define-public crate-ordered-streams-0.1.2 (c (n "ordered-streams") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1pqj84czad6a6qmn5f14y1lgvyx7m08n61mqkdwqw4x8bvyw62b9")))

(define-public crate-ordered-streams-99.99.99 (c (n "ordered-streams") (v "99.99.99") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0cvvx4y5jf3v0zrp7kbl1kln1bnqklhafs9qy78k4zj5xcy77gvw")))

