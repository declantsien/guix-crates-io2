(define-module (crates-io or de ordered-locks) #:use-module (crates-io))

(define-public crate-ordered-locks-0.1.0 (c (n "ordered-locks") (v "0.1.0") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1378cgh833xb3l360q99rl2js5clj5blc7hvx95psai1lniqyf03")))

(define-public crate-ordered-locks-0.1.1 (c (n "ordered-locks") (v "0.1.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "05b4l2j23fzbn90af8492l99vf9q2bfahq2p0iz9fhgzvq108c0s")))

(define-public crate-ordered-locks-0.2.0 (c (n "ordered-locks") (v "0.2.0") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0yc40715skzin72hki42k118i53d5sqgi2vi83b5k82rzaxi65j4")))

(define-public crate-ordered-locks-0.2.1 (c (n "ordered-locks") (v "0.2.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1r2l8g2v4pxxzddk4lg1jbp6drw2qbvbd0m9rdq6168yjs6v8y12")))

