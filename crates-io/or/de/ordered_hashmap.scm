(define-module (crates-io or de ordered_hashmap) #:use-module (crates-io))

(define-public crate-ordered_hashmap-0.0.1 (c (n "ordered_hashmap") (v "0.0.1") (h "1ifdfzbvpl2c35g5cx3fk6ka1l892fy1wxaf7xyl7b4zy58w4nil")))

(define-public crate-ordered_hashmap-0.0.2 (c (n "ordered_hashmap") (v "0.0.2") (h "136h10za9zy9ci4pnbba86ig6bcyrsck6kq408rw3fljq8gip87x")))

(define-public crate-ordered_hashmap-0.0.3 (c (n "ordered_hashmap") (v "0.0.3") (h "0njckh4nv7chd03h3n32dnigb18jq5sv7j3r91b9saa2sacs87nb")))

(define-public crate-ordered_hashmap-0.0.4 (c (n "ordered_hashmap") (v "0.0.4") (h "0ps0hlwnvx68g6x7bl3zny3apjf1wwisr66yh66y9jbynva8bakp")))

