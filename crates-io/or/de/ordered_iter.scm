(define-module (crates-io or de ordered_iter) #:use-module (crates-io))

(define-public crate-ordered_iter-0.0.1 (c (n "ordered_iter") (v "0.0.1") (h "0q6m5aiv2h6mr6d0i64cm0khmhz4p0m555bzk6cn2c9vwg0fdkq9")))

(define-public crate-ordered_iter-0.1.0 (c (n "ordered_iter") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "vec_map") (r "^0.3") (d #t) (k 0)))) (h "0b1ndpf1wq3kxmpyi5xrzz5n05w93gc2p2mysr2k7jp6jsqkjhid") (f (quote (("nightly"))))))

(define-public crate-ordered_iter-0.1.1 (c (n "ordered_iter") (v "0.1.1") (d (list (d (n "bit-set") (r "~0.5") (d #t) (k 0)) (d (n "bit-vec") (r "~0.5") (d #t) (k 0)) (d (n "vec_map") (r "~0.8") (d #t) (k 0)))) (h "1gabyv1bcbhky81y9xiw6hxd6p74f44qwpg5wx2bydc3x4qp93rw") (f (quote (("nightly"))))))

(define-public crate-ordered_iter-0.1.2 (c (n "ordered_iter") (v "0.1.2") (d (list (d (n "bit-set") (r "~0.5") (d #t) (k 0)) (d (n "bit-vec") (r "~0.6") (d #t) (k 0)) (d (n "vec_map") (r "~0.8") (d #t) (k 0)))) (h "1zh0j68xdgdb205rpwbd3fg66gl8ikqkkz5krgdah8ys1iqv5p68") (f (quote (("nightly"))))))

