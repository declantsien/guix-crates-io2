(define-module (crates-io or de order-maintenance) #:use-module (crates-io))

(define-public crate-order-maintenance-0.1.0 (c (n "order-maintenance") (v "0.1.0") (d (list (d (n "slotmap") (r "^1.0.6") (k 0)))) (h "1svj3pfij7p6xdv7q00f9p9gydhbm0ip5v2dlnrk34fd2sd65dpd")))

(define-public crate-order-maintenance-0.1.1 (c (n "order-maintenance") (v "0.1.1") (d (list (d (n "slotmap") (r "^1.0.6") (k 0)))) (h "0g7n6smyw47x7vch1mqy1spanv5cjp5nincs8aq5h1ggsq65l6r5")))

