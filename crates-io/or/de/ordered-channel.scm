(define-module (crates-io or de ordered-channel) #:use-module (crates-io))

(define-public crate-ordered-channel-1.0.0 (c (n "ordered-channel") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (o #t) (d #t) (k 0)))) (h "13byzl2q390a5n0jz79ykfr4770v28kq6pr72xy8pvmp30r6xmld") (r "1.60")))

(define-public crate-ordered-channel-1.1.0 (c (n "ordered-channel") (v "1.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (o #t) (d #t) (k 0)))) (h "1vb49561g90axdwbmgh14c3kmbdnyxvcyljz2a8cnx09r9lwa2sg") (r "1.60")))

