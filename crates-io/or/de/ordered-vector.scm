(define-module (crates-io or de ordered-vector) #:use-module (crates-io))

(define-public crate-ordered-vector-0.1.0 (c (n "ordered-vector") (v "0.1.0") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "1mmvp2l5m9cn427mjvqpd7r127vxn8dqfhalps8kh3g4cxf6rp16")))

(define-public crate-ordered-vector-0.1.1 (c (n "ordered-vector") (v "0.1.1") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "07xpir38lmwlawp7rh77n43izr3j09ifzbjvq5wcf8i6hwxr3a71")))

(define-public crate-ordered-vector-0.1.2 (c (n "ordered-vector") (v "0.1.2") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "0ay17xj2rv788ww961npyz6nirfng4rswg9bg71fm0gp6rh9n26z")))

(define-public crate-ordered-vector-0.1.3 (c (n "ordered-vector") (v "0.1.3") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "0jby3wcq5agx19dan57zhm9lv3w5n4hi9v4f5mbnx72yk16h4cfc")))

(define-public crate-ordered-vector-0.1.4 (c (n "ordered-vector") (v "0.1.4") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "1j0ff53hi7nlmwi88xmldbhw8cqzm6m2jg4hha5kvg3nmqh1zl79")))

(define-public crate-ordered-vector-0.1.5 (c (n "ordered-vector") (v "0.1.5") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)))) (h "096qgsk3n7a3y1l4kfhv0zixvscrkcrcv5akgcb8qvcy78z457p3")))

