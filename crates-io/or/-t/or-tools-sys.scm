(define-module (crates-io or -t or-tools-sys) #:use-module (crates-io))

(define-public crate-or-tools-sys-9.7.0 (c (n "or-tools-sys") (v "9.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.18") (d #t) (k 1)))) (h "0xfa17j5vlbdn2zn31an0wdlyl6lhr98lya47jza5dswabssiasn") (f (quote (("solver-xpress") ("solver-scip") ("solver-highs") ("solver-glpk") ("solver-cplex") ("solver-coinor") ("default")))) (l "ortools") (r "1.73")))

