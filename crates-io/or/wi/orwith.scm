(define-module (crates-io or wi orwith) #:use-module (crates-io))

(define-public crate-orwith-0.1.0 (c (n "orwith") (v "0.1.0") (h "0bk7hr2idm720jjaw8lj3cqwdgxikd76cdd17kbgw36191qpa693")))

(define-public crate-orwith-0.1.1 (c (n "orwith") (v "0.1.1") (h "0pazwjbn60vmh4gmdcb5avl408gr3v8chsq3gf3dpkb5hpn6pk07")))

(define-public crate-orwith-0.1.2 (c (n "orwith") (v "0.1.2") (h "0jahgbxq61h3ngbyq2dxpnh7pgvkpj72vdqpdh3a4071mwyrmhqz")))

(define-public crate-orwith-0.1.3 (c (n "orwith") (v "0.1.3") (h "0b6f2khin5hbzal8ig5c9b90205viglrlapmzhc958lghvq2mika")))

