(define-module (crates-io or as oras) #:use-module (crates-io))

(define-public crate-oras-0.0.1 (c (n "oras") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "oci-distribution") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)))) (h "0pacr27gwx971lybfaxjpgai5r062jn7b9afj5ipg7gfdp7c93bg")))

