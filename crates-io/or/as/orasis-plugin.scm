(define-module (crates-io or as orasis-plugin) #:use-module (crates-io))

(define-public crate-orasis-plugin-0.1.0 (c (n "orasis-plugin") (v "0.1.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1yz0xb2ghf1z4hvb2pxsgp1x3i54bvjk72x2ahrlwv3pbf46v7f9")))

(define-public crate-orasis-plugin-0.1.1 (c (n "orasis-plugin") (v "0.1.1") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1r3vb1s2qa1c31mhlshcrvl8cx47952r7vri1iyxdyy28sl82fn3")))

(define-public crate-orasis-plugin-0.1.2 (c (n "orasis-plugin") (v "0.1.2") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0qrmlanwm5zskg6049yg573365cbld3aifqzf7vb5c13r76c6kz4")))

(define-public crate-orasis-plugin-0.1.3 (c (n "orasis-plugin") (v "0.1.3") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "05mhzyrb2c42kdnkzw7d9l1nlhbjkpf8jvl1ddx86zmjvs3zvz4y")))

(define-public crate-orasis-plugin-0.2.0 (c (n "orasis-plugin") (v "0.2.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0w9x8a896g7v6776wm48qzr81p34dx7nxx5840lyqjz1rgn98m2r")))

(define-public crate-orasis-plugin-0.3.0 (c (n "orasis-plugin") (v "0.3.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0p9s9n6an2xqbzxzcm1rpc153zib4rqc14kqnqqpzsjmbfmfhr2j")))

(define-public crate-orasis-plugin-0.4.0 (c (n "orasis-plugin") (v "0.4.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0yj2nw2vwym473zs910xdx9yam3bws0w35xqiihdx9p1dhk1rk0k")))

(define-public crate-orasis-plugin-0.5.0 (c (n "orasis-plugin") (v "0.5.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0pray16iakfkmz89aqgcshnildr3sakawd879vhz01fqj65p8n61")))

(define-public crate-orasis-plugin-0.6.0 (c (n "orasis-plugin") (v "0.6.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1q061nqapggi8r0kwh87pxsing71661ggm71vyj86x08gmfcwrr5")))

(define-public crate-orasis-plugin-0.7.0 (c (n "orasis-plugin") (v "0.7.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0ng41fn0bim78pszm8bj6s5drh5s918p3g08bqhvfn9fiz7w7hhi")))

(define-public crate-orasis-plugin-0.8.0 (c (n "orasis-plugin") (v "0.8.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0mz942fz5xn3ln4lil07gahss8nr2mrw1rddj0bgqscax3wqyqkl")))

(define-public crate-orasis-plugin-0.9.0 (c (n "orasis-plugin") (v "0.9.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "028szs7pwzcm6aic0y1nsyz9p7sfjqb3jycw55kf8hlf2s8wjlrg")))

(define-public crate-orasis-plugin-0.10.0 (c (n "orasis-plugin") (v "0.10.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "13mqb6ckbzm1cdfq1972di39lq9wfx1gdias2vy2wsr6x5wq4azy")))

(define-public crate-orasis-plugin-0.11.0 (c (n "orasis-plugin") (v "0.11.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1i18zm9g49q1lpipqv92v7dfcacyajb9i4813z3qg6h27da0p38c")))

(define-public crate-orasis-plugin-0.12.0 (c (n "orasis-plugin") (v "0.12.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "0hk9b2pbax85cm2gcql3b6n3wqvap2j32d36c7ihyzn67474fp2a")))

(define-public crate-orasis-plugin-0.12.1 (c (n "orasis-plugin") (v "0.12.1") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1zbhp8qm3hj71l8xfqqfbhsnc5bgpsspbkawj3h4xlig26qyf8x3")))

(define-public crate-orasis-plugin-0.13.0 (c (n "orasis-plugin") (v "0.13.0") (d (list (d (n "termion") (r "~1.5.3") (d #t) (k 0)))) (h "1d2kqb744lz9x5iamikzfg97krm6pl9q4v0xiwzfxxh9n1nzd7nh")))

