(define-module (crates-io or do ordoo) #:use-module (crates-io))

(define-public crate-ordoo-0.1.0 (c (n "ordoo") (v "0.1.0") (h "1m48464ff94n47dxsm1qz0mf0ach8jr221hm61a8f2pgrf78fbcb")))

(define-public crate-ordoo-0.1.1 (c (n "ordoo") (v "0.1.1") (h "08dfqrq70mm3d618243m0w8g93bw04c68lqa6v42ica5bzl9cqcq")))

