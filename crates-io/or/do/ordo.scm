(define-module (crates-io or do ordo) #:use-module (crates-io))

(define-public crate-ordo-0.1.0 (c (n "ordo") (v "0.1.0") (h "1y2vdnc7bffy0caprkqd3g69nizacyjl0p0zy3n5503237kb5mm8")))

(define-public crate-ordo-0.1.1 (c (n "ordo") (v "0.1.1") (h "0hspq06hx8ga4xzx41lz51ba4z5k567yb1jwkc5s62qnidyi4l00")))

