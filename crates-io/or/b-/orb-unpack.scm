(define-module (crates-io or b- orb-unpack) #:use-module (crates-io))

(define-public crate-orb-unpack-0.1.0 (c (n "orb-unpack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1rrixms3prabv464vwv919r0hl7y7b8ww4zipxfjl5m6fwn7lipq")))

(define-public crate-orb-unpack-0.1.1 (c (n "orb-unpack") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "02w07p11byd07cgxp9nzxy5gdq6dw9drib51vx9j0v3p0nfjilqr")))

(define-public crate-orb-unpack-0.1.2 (c (n "orb-unpack") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dir-diff") (r "^0.3.2") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0j49widwr28zrm6an42mipvhxivfr7cvbd4g8aqpsj8ki11ddpca")))

