(define-module (crates-io or b- orb-network-manager) #:use-module (crates-io))

(define-public crate-orb-network-manager-0.11.2 (c (n "orb-network-manager") (v "0.11.2") (d (list (d (n "ascii") (r "^0.8") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "dbus") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1avwjlqxn6hjngzzzssswliq1zj0dk3bprh707714vx0agq6i3h2")))

(define-public crate-orb-network-manager-0.11.3 (c (n "orb-network-manager") (v "0.11.3") (d (list (d (n "ascii") (r "^0.8") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "dbus") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0i87gbl76vkzsy73a1h9rnpn18rafvslxwwhrglmfrl5hff7xbkp")))

