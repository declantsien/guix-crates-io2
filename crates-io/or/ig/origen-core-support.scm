(define-module (crates-io or ig origen-core-support) #:use-module (crates-io))

(define-public crate-origen-core-support-0.1.0-dev.0 (c (n "origen-core-support") (v "0.1.0-dev.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1k6vn1q5iap0g90938zma93d1y0qx0qvj411chmikjfirx2594w2")))

(define-public crate-origen-core-support-0.1.0-dev.1 (c (n "origen-core-support") (v "0.1.0-dev.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1d8qdfzq0gb97biw07jsfqgb4ynq4cm0yifa7nk00mkbc7zsbyma")))

