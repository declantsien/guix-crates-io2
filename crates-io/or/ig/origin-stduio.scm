(define-module (crates-io or ig origin-stduio) #:use-module (crates-io))

(define-public crate-origin-stduio-0.1.0 (c (n "origin-stduio") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1.101") (f (quote ("mem"))) (d #t) (k 0)) (d (n "origin") (r "^0.11.2") (f (quote ("origin-all" "origin-start"))) (k 0)) (d (n "rustix") (r "^0.38.9") (f (quote ("stdio"))) (k 0)) (d (n "rustix-dlmalloc") (r "^0.1.0") (f (quote ("global"))) (d #t) (k 0)) (d (n "rustix-futex-sync") (r "^0.1.2") (f (quote ("atomic_usize"))) (d #t) (k 0)))) (h "0ydnyf9rjgh7qcn72fcl7fna45f52gaiqiibgc0g2px1rv4qp3g1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

