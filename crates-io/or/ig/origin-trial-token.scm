(define-module (crates-io or ig origin-trial-token) #:use-module (crates-io))

(define-public crate-origin-trial-token-0.1.0 (c (n "origin-trial-token") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k9a06cbcq5l1j23ddqk82fbwc1p1jw8867cjr93b80d7ydbqjqs")))

(define-public crate-origin-trial-token-0.1.1 (c (n "origin-trial-token") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l25xbdpcr592s21f4s3n4aplc49h0lx02hfmdrgsbhxl7y61jwl")))

