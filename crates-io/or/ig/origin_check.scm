(define-module (crates-io or ig origin_check) #:use-module (crates-io))

(define-public crate-origin_check-0.1.0 (c (n "origin_check") (v "0.1.0") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0pf1dd2h15wz7kw1nxs4803k2c6kx8zv7zhgh3b2lv425a1sj2a2")))

(define-public crate-origin_check-0.1.1 (c (n "origin_check") (v "0.1.1") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1k4ap84h99s0w7xqr3g2zpwbasc21wazsaxbcxm80r9fa31c35h1")))

(define-public crate-origin_check-0.2.0 (c (n "origin_check") (v "0.2.0") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tower-test") (r "^0.4") (d #t) (k 2)))) (h "0cl6y8kj1y5s5fk4xhnnbkmay6b95zmrm9mialb8a641vx96m2sr") (f (quote (("default" "tower-layer"))))))

(define-public crate-origin_check-0.2.1 (c (n "origin_check") (v "0.2.1") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tower-test") (r "^0.4") (d #t) (k 2)))) (h "1vz2vpddam0l629cwk1l9nhcspyr4x0pyg7fzrv05jsspv0s7ydh") (f (quote (("default" "tower-layer"))))))

(define-public crate-origin_check-0.2.2 (c (n "origin_check") (v "0.2.2") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tower-test") (r "^0.4") (d #t) (k 2)))) (h "12nlr92mwiqhim66z59kik2868rk3shicvqcn09rmj8m4id0bdvx") (f (quote (("default" "tower-layer"))))))

