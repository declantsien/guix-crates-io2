(define-module (crates-io or iz orizentic) #:use-module (crates-io))

(define-public crate-orizentic-1.0.0 (c (n "orizentic") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "uuid") (r "^0.6.5") (f (quote ("v4"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1.5") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "18znb9llgd6pvn55rj61x3avgcj5nysvsvmdrllv3lq2j2fszx28")))

