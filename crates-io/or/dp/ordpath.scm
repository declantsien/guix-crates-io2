(define-module (crates-io or dp ordpath) #:use-module (crates-io))

(define-public crate-ordpath-0.1.0 (c (n "ordpath") (v "0.1.0") (h "0g6yby7qfg7mcv3ilvxm9pixvadsxc1ih01vk2m9k70xacifg3g2")))

(define-public crate-ordpath-0.2.0 (c (n "ordpath") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1n3pf5dh5av4j9555d0gjgp2q5xsrwhgjz4hc0dpihc70x14kz7y")))

