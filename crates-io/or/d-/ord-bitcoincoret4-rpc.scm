(define-module (crates-io or d- ord-bitcoincoret4-rpc) #:use-module (crates-io))

(define-public crate-ord-bitcoincoret4-rpc-0.17.2 (c (n "ord-bitcoincoret4-rpc") (v "0.17.2") (d (list (d (n "bitcoint4-private") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincoret4-rpc-json") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yix8dznglmxfcc2rn1zwzs8ap4syqj8pwv2zc2nsg99g3lnq9gc")))

