(define-module (crates-io or d- ord-qtumcore-rpc) #:use-module (crates-io))

(define-public crate-ord-qtumcore-rpc-0.17.1-qtum (c (n "ord-qtumcore-rpc") (v "0.17.1-qtum") (d (list (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-qtumcore-rpc-json") (r "^0.17.1-qtum") (d #t) (k 0)) (d (n "qtum") (r "^0.30.0-qtum") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "qtum-private") (r "^0.1.0-qtum") (d #t) (k 0)) (d (n "qtum_hashes") (r "^0.12.0-qtum") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r5fvy9m0pyy1lxi69jyf0hcx66a8wcbfzjr4lhrs4g0pwjzbya9")))

