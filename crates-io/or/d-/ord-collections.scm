(define-module (crates-io or d- ord-collections) #:use-module (crates-io))

(define-public crate-ord-collections-0.1.0 (c (n "ord-collections") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13vjsnmacxnn2jr23wallxdjyn723c27ghzfzcam4nldpzd5f5sd") (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

(define-public crate-ord-collections-0.1.1 (c (n "ord-collections") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18i4yg9vsc3d60g7vb7f9fxsqfsvpq7h9iiz27xp86fzvyqfhkdk") (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

(define-public crate-ord-collections-0.1.2 (c (n "ord-collections") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ncs42nr5p4pqcnyzdldrvhsk48s3pavilkszqqq7sljikfsak77") (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

