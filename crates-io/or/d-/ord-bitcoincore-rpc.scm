(define-module (crates-io or d- ord-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-ord-bitcoincore-rpc-0.16.0 (c (n "ord-bitcoincore-rpc") (v "0.16.0") (d (list (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pk6zbfk6pw6qz4c37pi9s7vlmlwqivr8r5z3wfba6d3yadmilin")))

(define-public crate-ord-bitcoincore-rpc-0.16.2 (c (n "ord-bitcoincore-rpc") (v "0.16.2") (d (list (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g2crgxzw8zqlripwyf9327zyppn15n21zjmxxnv87c0akr6p2fq")))

(define-public crate-ord-bitcoincore-rpc-0.16.3 (c (n "ord-bitcoincore-rpc") (v "0.16.3") (d (list (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "19hik97yvzwckmyg5wpmvrbglgw0vjyhvqnlrlqqhnyisdxviaxd")))

(define-public crate-ord-bitcoincore-rpc-0.16.4 (c (n "ord-bitcoincore-rpc") (v "0.16.4") (d (list (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1sddgfkdib0z3jhnh81hdn4024bmlc0222r6mjg4l263q5hha18f")))

(define-public crate-ord-bitcoincore-rpc-0.16.5 (c (n "ord-bitcoincore-rpc") (v "0.16.5") (d (list (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1f4q3lw67mw8f44j6kbhnpf9p14wx15y4l2iv0fp78hlb96saxya")))

(define-public crate-ord-bitcoincore-rpc-0.17.0 (c (n "ord-bitcoincore-rpc") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0jl1i7nwjja4khjymyzryif780m6ja3qc5bnvmyi549kilaw3qzc")))

(define-public crate-ord-bitcoincore-rpc-0.17.1 (c (n "ord-bitcoincore-rpc") (v "0.17.1") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gjsb7jzvrlhg16v2wh2snba9y8r72rj1q48w36hcra6glls8mrx")))

(define-public crate-ord-bitcoincore-rpc-0.17.2 (c (n "ord-bitcoincore-rpc") (v "0.17.2") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "ord-bitcoincore-rpc-json") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1n7l9z363qnkj4a7gmspgizfnv53miw5jqc6a4fj1mv8kpv25dhn")))

