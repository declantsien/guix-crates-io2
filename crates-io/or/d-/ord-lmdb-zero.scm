(define-module (crates-io or d- ord-lmdb-zero) #:use-module (crates-io))

(define-public crate-ord-lmdb-zero-0.4.5 (c (n "ord-lmdb-zero") (v "0.4.5") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "gcc") (r "=0.3.39") (d #t) (k 2)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "=0.2.18") (d #t) (k 2)) (d (n "ord-liblmdb-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "=0.3.15") (d #t) (k 2)) (d (n "supercow") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "=0.3.4") (d #t) (k 2)))) (h "02dz4jd05jdjwk9r9nf2326ahy8v6kzaq9ifywcm2qa6rsznn67n")))

