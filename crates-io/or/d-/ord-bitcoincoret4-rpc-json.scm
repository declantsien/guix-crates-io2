(define-module (crates-io or d- ord-bitcoincoret4-rpc-json) #:use-module (crates-io))

(define-public crate-ord-bitcoincoret4-rpc-json-0.17.1 (c (n "ord-bitcoincoret4-rpc-json") (v "0.17.1") (d (list (d (n "bitcoint4") (r "^0.30.2") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoint4-private") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "038zy04yqc80y4bnfgn2p7m621lihaa9mq5r6hb09pgqgmshzbfb")))

