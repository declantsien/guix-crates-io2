(define-module (crates-io or d- ord-lmdb-rs) #:use-module (crates-io))

(define-public crate-ord-lmdb-rs-0.7.7 (c (n "ord-lmdb-rs") (v "0.7.7") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ord-liblmdb-sys") (r "^0.2.3") (d #t) (k 0)))) (h "1k2biw8dzmlnhais051yg67pfca8s94q0h2m9cpzzfff7qk4pvkv")))

