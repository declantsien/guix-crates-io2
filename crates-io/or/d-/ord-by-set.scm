(define-module (crates-io or d- ord-by-set) #:use-module (crates-io))

(define-public crate-ord-by-set-1.0.0 (c (n "ord-by-set") (v "1.0.0") (h "1l67zvbvipyk6ff05nkfw30pwgbld253jyawckr0fhxhx45irml0")))

(define-public crate-ord-by-set-1.0.1 (c (n "ord-by-set") (v "1.0.1") (h "03c0gxljc3rjdk3i6a915bs36pwwljv6sqxzm1zpq63jlbzm7qm3")))

(define-public crate-ord-by-set-1.0.2 (c (n "ord-by-set") (v "1.0.2") (h "1mdl0pfzqapq7b7jpa3ddcd1af5qhc8zvwsklgry0xwb9aw862yr")))

(define-public crate-ord-by-set-1.0.3 (c (n "ord-by-set") (v "1.0.3") (h "0gd5y5y597cza9rq6in0phb429zgbs75mm5dhi3zzr773pagvx9c")))

