(define-module (crates-io or d- ord-bitcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-ord-bitcoincore-rpc-json-0.16.1 (c (n "ord-bitcoincore-rpc-json") (v "0.16.1") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1f7iydcdja1zqkadsdlsn86psp6j5ydkn0hw1iy9r0dnwf0hzs6d")))

(define-public crate-ord-bitcoincore-rpc-json-0.16.2 (c (n "ord-bitcoincore-rpc-json") (v "0.16.2") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "180zcy5w1hr5yc0yw4bfx873h2isa5qbavjgsj1lppppm5nrnn3h")))

(define-public crate-ord-bitcoincore-rpc-json-0.16.3 (c (n "ord-bitcoincore-rpc-json") (v "0.16.3") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vd8ax11ycim9jvrv4jisyvk3qgwyjiyggwp3ag8gwhd23ws075z")))

(define-public crate-ord-bitcoincore-rpc-json-0.16.4 (c (n "ord-bitcoincore-rpc-json") (v "0.16.4") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1piq00jir43944fs1v7h6x5mc2idp1wqix9jdx2g60gixgk4k6r7")))

(define-public crate-ord-bitcoincore-rpc-json-0.16.5 (c (n "ord-bitcoincore-rpc-json") (v "0.16.5") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yw44a5x6a315yhhhafa43grppybjy1dx9r1yb9m17yrivnirf0q")))

(define-public crate-ord-bitcoincore-rpc-json-0.17.0 (c (n "ord-bitcoincore-rpc-json") (v "0.17.0") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08d32cgfg27kwfqpqx1i1vmdnahvllxlijykc6a7b5fc16snxx25")))

(define-public crate-ord-bitcoincore-rpc-json-0.17.1 (c (n "ord-bitcoincore-rpc-json") (v "0.17.1") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cxdg546a1kfvpp0m45pdwhjp29ls1rf6ygs4yy7biqqz6451csv")))

