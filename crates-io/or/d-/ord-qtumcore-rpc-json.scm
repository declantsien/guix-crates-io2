(define-module (crates-io or d- ord-qtumcore-rpc-json) #:use-module (crates-io))

(define-public crate-ord-qtumcore-rpc-json-0.17.1-qtum (c (n "ord-qtumcore-rpc-json") (v "0.17.1-qtum") (d (list (d (n "qtum") (r "^0.30.0-qtum") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "qtum-private") (r "^0.1.0-qtum") (d #t) (k 0)) (d (n "qtum_hashes") (r "^0.12.0-qtum") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i68dgj2nz2b8hbvhhbhacln2n1bxwl216xrbiv4m02r6ww1f8lw")))

