(define-module (crates-io or mi ormio) #:use-module (crates-io))

(define-public crate-ormio-0.1.0 (c (n "ormio") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "08jlrda221b1bfgxcyry1dy1niw0k6rv15qg94vww29pcrgjc6nc")))

