(define-module (crates-io or m_ orm_macro_derive) #:use-module (crates-io))

(define-public crate-orm_macro_derive-0.1.0 (c (n "orm_macro_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "169is8z70v0c49zkp7kidy09gb12qw577xhkd2388xn00rx14hja")))

(define-public crate-orm_macro_derive-0.1.1 (c (n "orm_macro_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "008gz5ralca4mmanc01aphayszfqfa65mvzxs1rbimf6ngyhsigv")))

(define-public crate-orm_macro_derive-0.1.2 (c (n "orm_macro_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1px0zpfbbb3r0r5ck2lj09wi124xkkbamc0np7z778g5r0ybykpd")))

(define-public crate-orm_macro_derive-1.0.0 (c (n "orm_macro_derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0wx944hm1szcgi5bgxjj4i5lb1d9di7jninz4yajbvicf3h6w03n")))

(define-public crate-orm_macro_derive-1.0.1 (c (n "orm_macro_derive") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "158ga54aid6igdgnnxgdliifvq9yd00vzhklziar0dvn99f6s78k")))

(define-public crate-orm_macro_derive-1.1.1 (c (n "orm_macro_derive") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "12mkj0a89wfavx7yz2dv54qf6n4zsnsi84621amv5qgmagc2bwy3")))

(define-public crate-orm_macro_derive-1.1.2 (c (n "orm_macro_derive") (v "1.1.2") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1mcs9s8p5bbdwkdzjnkzkpx34gi3ab4z0rdx5kf0vjiq1llchvmy")))

(define-public crate-orm_macro_derive-1.1.3 (c (n "orm_macro_derive") (v "1.1.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "17gl0c8hjzq8c5iyabqmgdv1mr5v1gkln0v4s1r13angkiw7zg06")))

(define-public crate-orm_macro_derive-1.2.0 (c (n "orm_macro_derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1brzx2gcdpjba95d1yzji79h06dm6myym3lfy5rks32flm37m85r") (f (quote (("sqlite") ("postgres") ("mysql") ("default" "postgres")))) (y #t)))

(define-public crate-orm_macro_derive-1.2.1 (c (n "orm_macro_derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1mr74wy051zd2if5c5rwfdw77aczw75ds7racy7c5i2agq7z6jml") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-orm_macro_derive-1.3.0 (c (n "orm_macro_derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "05x0rjg7mhy9h18fbh2ig65czzgfzgiyki06xyap8bz12sg3xl02") (f (quote (("sqlite") ("postgres") ("mysql"))))))

