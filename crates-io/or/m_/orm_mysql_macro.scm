(define-module (crates-io or m_ orm_mysql_macro) #:use-module (crates-io))

(define-public crate-orm_mysql_macro-0.2.0 (c (n "orm_mysql_macro") (v "0.2.0") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1aarksgsnkwf92zxkxk4qgbp421c4i3347i8rvb04mrj2s510wsv")))

(define-public crate-orm_mysql_macro-1.0.0 (c (n "orm_mysql_macro") (v "1.0.0") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08rkqkm212ivd617b7l0p82y399yhw90b86hwb7g0k504x465fa8")))

(define-public crate-orm_mysql_macro-1.1.0 (c (n "orm_mysql_macro") (v "1.1.0") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15ln66a4hrn1mh064wy08ap1xhjbvdzvllxipqss25igbflq26v4")))

(define-public crate-orm_mysql_macro-1.2.0 (c (n "orm_mysql_macro") (v "1.2.0") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13nm04jkknkh3wbviiqx5m7vc1l160j07cq12srq5ka6bq5kg9w4") (r "1.75")))

(define-public crate-orm_mysql_macro-1.2.1 (c (n "orm_mysql_macro") (v "1.2.1") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1df6r5gvxkkdr5qpxpl6699q07lg889srn1sakhxc3mxrhz22gbc") (r "1.75")))

(define-public crate-orm_mysql_macro-1.2.2 (c (n "orm_mysql_macro") (v "1.2.2") (d (list (d (n "common_uu") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0hwjpd6hpv2mk79n59xg2ldkc3zqpjvmk8wjrprs0d4i4lxwwjar") (r "1.75")))

