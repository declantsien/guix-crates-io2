(define-module (crates-io or m_ orm_macro) #:use-module (crates-io))

(define-public crate-orm_macro-0.1.0 (c (n "orm_macro") (v "0.1.0") (d (list (d (n "orm_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1khma0ss3412m70001qd98fnhh42c2rq5f1m9csaqn5bcagaz8g1")))

(define-public crate-orm_macro-0.1.1 (c (n "orm_macro") (v "0.1.1") (d (list (d (n "orm_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1zfki6bgf90cik6xn9jbyadi088vbpv1i053df0a4vgyd28ldzj7")))

(define-public crate-orm_macro-0.1.2 (c (n "orm_macro") (v "0.1.2") (d (list (d (n "orm_macro_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0i6vi6x1p0qmli3djza4i74l41mdbhfrm7hp15jznsjcdqdrdjyv")))

(define-public crate-orm_macro-0.1.3 (c (n "orm_macro") (v "0.1.3") (d (list (d (n "orm_macro_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0nsqmms5pk0x4q2d89fap050brh54l7v6akizy8hdhhy25cxmdqx")))

(define-public crate-orm_macro-1.0.0 (c (n "orm_macro") (v "1.0.0") (d (list (d (n "orm_macro_derive") (r "^1.0.0") (d #t) (k 0)))) (h "15dm4xq0y661mlbffmkm1k3cbs92brfhb2c95yzdfmwyrnrmdnbg")))

(define-public crate-orm_macro-1.0.1 (c (n "orm_macro") (v "1.0.1") (d (list (d (n "orm_macro_derive") (r "^1.0.1") (d #t) (k 0)))) (h "0vjld2lx2c02imsviy7pljj8njhnd81hb80myc0il02kgawvcmdp")))

(define-public crate-orm_macro-1.1.1 (c (n "orm_macro") (v "1.1.1") (d (list (d (n "orm_macro_derive") (r "^1.1.1") (d #t) (k 0)))) (h "0lxcabxikylz9qwrhsz0s89n7rxyl4z81ssqj73rpkg20skxjzh8")))

(define-public crate-orm_macro-1.1.2 (c (n "orm_macro") (v "1.1.2") (d (list (d (n "orm_macro_derive") (r "^1.1.2") (d #t) (k 0)))) (h "1y31kir41dp1qfg6y48vnd1nbfqn7c0sbncynvr4xx2az37ry37j")))

(define-public crate-orm_macro-1.1.3 (c (n "orm_macro") (v "1.1.3") (d (list (d (n "orm_macro_derive") (r "^1.1.3") (d #t) (k 0)))) (h "1j0542sv5n9j15ih2ar9jj4f1faq7m7gkvcskrwzh145pkd83a67")))

(define-public crate-orm_macro-1.2.0 (c (n "orm_macro") (v "1.2.0") (d (list (d (n "orm_macro_derive") (r "^1.2.0") (d #t) (k 0)))) (h "1vxi9pc2ay5xzk95q7jd644zmpsndxfnwrir3prxqhahv137ywq9") (f (quote (("sqlite") ("postgres") ("mysql") ("default" "postgres")))) (y #t)))

(define-public crate-orm_macro-1.2.1 (c (n "orm_macro") (v "1.2.1") (d (list (d (n "orm_macro_derive") (r "^1.2.1") (d #t) (k 0)))) (h "054apl0xp8863z0l60sb6j2nmyq6zc1bh767705bi42x96yz3qi8")))

(define-public crate-orm_macro-1.3.0 (c (n "orm_macro") (v "1.3.0") (d (list (d (n "orm_macro_derive") (r "^1.3.0") (d #t) (k 0)))) (h "0wwz5y0i8a5pnbhd1hy857m752cn3gy546fqcizfy22ghff6rvr0")))

