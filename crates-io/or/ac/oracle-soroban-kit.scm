(define-module (crates-io or ac oracle-soroban-kit) #:use-module (crates-io))

(define-public crate-oracle-soroban-kit-0.1.8 (c (n "oracle-soroban-kit") (v "0.1.8") (d (list (d (n "soroban-kit") (r "^0.1.8") (f (quote ("oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0wk5grpz8n5ys14rc01bk7p6s2arfsyvny1a057qv9q4v250b6r0")))

(define-public crate-oracle-soroban-kit-0.1.9 (c (n "oracle-soroban-kit") (v "0.1.9") (d (list (d (n "soroban-kit") (r "^0.1.9") (f (quote ("oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1x5pv1hj0h166w97zi8m54w4k7c6j871l4rjb312y3wy50w6i20w")))

(define-public crate-oracle-soroban-kit-0.1.10 (c (n "oracle-soroban-kit") (v "0.1.10") (d (list (d (n "soroban-kit") (r "^0.1.10") (f (quote ("oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1m656xfm53z2syk4qfcdajcjfk00hv0lpq7njx0v00qlwmbgwzy5")))

(define-public crate-oracle-soroban-kit-0.1.11 (c (n "oracle-soroban-kit") (v "0.1.11") (d (list (d (n "soroban-kit") (r "^0.1.11") (f (quote ("oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.3.2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.3.2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1hgxxjjmnqrm876c6pdyqv742q8mhwwdlrw3cigb9drylbrxl6ng")))

