(define-module (crates-io or ac oracletraits) #:use-module (crates-io))

(define-public crate-oracletraits-0.1.0 (c (n "oracletraits") (v "0.1.0") (d (list (d (n "ink") (r "^4.0.1") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ink_e2e") (r "^4.0.1") (d #t) (k 2)))) (h "1k4b8hr1x5gwj89d21sg4kxxfz66l06ba99igky4bk2gdqqk3l3c") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("e2e-tests") ("default" "std"))))))

