(define-module (crates-io or ac oracle) #:use-module (crates-io))

(define-public crate-oracle-0.0.1 (c (n "oracle") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0w4ndnq92kpjj5kgfpyqy7524fs3y7igj53pnn9k9119rh0ps0py") (y #t)))

(define-public crate-oracle-0.0.2 (c (n "oracle") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "08zda7g6bcq03zv7qy689bk9xryzrlvm0n7rnc59d76b3zk5cp0n")))

(define-public crate-oracle-0.0.3 (c (n "oracle") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0rw552fcwnxvg9jx6y1rm6q9qj5bi3cj6n6lc6f43h4i8dvx4jyz")))

(define-public crate-oracle-0.0.4 (c (n "oracle") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "13s31429jkd99gn8szn9i1k36hlzry93ndb5j2h4z56lmhm11g5s") (f (quote (("restore-deleted"))))))

(define-public crate-oracle-0.0.5 (c (n "oracle") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0xsgqjgdvrkqbzfw4njw9i3xkdlvian28rp2208pgzgspkj7v8p2") (f (quote (("restore-deleted"))))))

(define-public crate-oracle-0.0.6 (c (n "oracle") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0vkfw17n7k887xb2jkff1lw3jj62qcxz65p2f2c2n6jypl7rymg3") (f (quote (("restore-deleted"))))))

(define-public crate-oracle-0.0.7 (c (n "oracle") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "087j4nrycr2qzc4afksgq50a051knkfdaj31gqp0gw3n96zbgdp9")))

(define-public crate-oracle-0.0.8 (c (n "oracle") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "1zrrhclqw0swi7qgj5dn66z4nkhvm815l2hyn21dsyi41p6w0ywf")))

(define-public crate-oracle-0.1.0 (c (n "oracle") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0gqghiz50v0k1afvpaqm67iaqlm1y3fyqlgcgb757rm48w0vzh9a")))

(define-public crate-oracle-0.1.1 (c (n "oracle") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "00wvlmrgw7a9s1b5fwd6iy2y3cf4mcr5l6pxbbm177qzvhs3y6gw")))

(define-public crate-oracle-0.1.2 (c (n "oracle") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "1ldbihscnaz9ygkchnkhsnzbq3s3fi7pardr4s2cyxgsxrj4j3wk") (y #t)))

(define-public crate-oracle-0.2.0 (c (n "oracle") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "1005n4khq4wjb4ilkg5if7dpygq241y978kx2gp16ybdfhva2n04")))

(define-public crate-oracle-0.2.1 (c (n "oracle") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "01whkwbmwfwnavh7snh3lr80dr0nkhg1cw93dj7lcpnz5kdxjsba")))

(define-public crate-oracle-0.2.2 (c (n "oracle") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "1b6q2ynn5x92x7694a2cjnyvm749lwxalazwkjc8ffjmc9hq85c9")))

(define-public crate-oracle-0.3.0 (c (n "oracle") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0m9v2addqlknm7pbhk7dyjdgg1xwqnrwll6j2a4cyjhd22kyhscb")))

(define-public crate-oracle-0.3.1 (c (n "oracle") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0vmqm4br99r6lx0i1v7sy67c2xx3bck6l1vxmmf0mnffr64f1qzm")))

(define-public crate-oracle-0.3.2 (c (n "oracle") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "0ai54mc3b8m93b4cy0rq306w0hk440zy5ls1z0sxww924wwlsni3")))

(define-public crate-oracle-0.3.3 (c (n "oracle") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "19cg3h86rfr32r5l5ppb8dm0r0spnfvd6mi78i10i43lh6c95cn8")))

(define-public crate-oracle-0.4.0 (c (n "oracle") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "117y6ifv9r0pj619x2nm91ak7zakj9czvrasp97nblnjr1fkskhs")))

(define-public crate-oracle-0.5.0 (c (n "oracle") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1sg5af2v0373r8j5gwg1rakihjw2p4hd8cy1bxrzmcvsdhd3mjfs") (y #t)))

(define-public crate-oracle-0.5.1 (c (n "oracle") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1jzxwavfph0lrd7bjk5fsrk56jskfjnilvr39lbyq3lcy4z6prds")))

(define-public crate-oracle-0.5.2 (c (n "oracle") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0z0qh8zgfx8bp19nq11cdq6kbfdl8gkbjs5z09d7ddaf8yargbv9")))

(define-public crate-oracle-0.5.3 (c (n "oracle") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1yhvprrv9s17y6z5rf09ffhb4cyvijq9fah03cm94la69w5mv30p")))

(define-public crate-oracle-0.5.4 (c (n "oracle") (v "0.5.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0yma13s2nvayab44qz90lwa40hiyqmsvywpfnlj24mdwl96av1cy")))

(define-public crate-oracle-0.5.5 (c (n "oracle") (v "0.5.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "0i69plzlikxvh0lq165x89hf7wnd3bwv9rhxr3a7yfvs1dqdmkmv") (f (quote (("aq_unstable"))))))

(define-public crate-oracle-0.5.6 (c (n "oracle") (v "0.5.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "1s7xp0wc70sxnhjyrc1diwr8h5bkini4zkjapbs4frdjz9nmc53s") (f (quote (("aq_unstable")))) (r "1.54.0")))

(define-public crate-oracle-0.5.7 (c (n "oracle") (v "0.5.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "00shb0vkgjdqi69h76p2c8ilacpsymvszw5ydw0ymfhzmws07s6g") (f (quote (("stmt_without_lifetime") ("aq_unstable")))) (r "1.54.0")))

(define-public crate-oracle-0.6.0 (c (n "oracle") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (o #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "0xrhq7g1m45145j90z881m51260f2b4pwn2x4383ch23qlg80c9x") (f (quote (("aq_unstable")))) (r "1.60.0")))

