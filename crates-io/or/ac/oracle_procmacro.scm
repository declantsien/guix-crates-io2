(define-module (crates-io or ac oracle_procmacro) #:use-module (crates-io))

(define-public crate-oracle_procmacro-0.1.0 (c (n "oracle_procmacro") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g90j61f4lcgy094jvw5k3fzfk33dxf4jxsfm12g2839q06b3lwp")))

(define-public crate-oracle_procmacro-0.1.1 (c (n "oracle_procmacro") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00b5gyp3935zhakkp1l5mjmqg95dbcilx1cpjnk5i31frlq195dj")))

(define-public crate-oracle_procmacro-0.1.2 (c (n "oracle_procmacro") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "09xgv31j5w4jdmji685y4ajlh42bkljd62041mmfazfm44s7y95d")))

