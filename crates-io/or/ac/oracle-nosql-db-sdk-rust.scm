(define-module (crates-io or ac oracle-nosql-db-sdk-rust) #:use-module (crates-io))

(define-public crate-oracle-nosql-db-sdk-rust-0.1.0 (c (n "oracle-nosql-db-sdk-rust") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v0y0acv7jql70ksc8psvxd1fnjfq2hfzbwpl3f8d1sb4zn3xjbb")))

(define-public crate-oracle-nosql-db-sdk-rust-0.2.0 (c (n "oracle-nosql-db-sdk-rust") (v "0.2.0") (d (list (d (n "cgo") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gaq4icf1dzi4185nk13cywld4c0ki4cav9vip8k2nvrcp2mkf3z")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.0 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.0") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "064qhfadzbbps0kab70c7wrf15kprrqlb3rx6sfzxkrvjiz9xal6")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.1 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.1") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dxapnc56rvardf2nl2bkv0mqsg9nciz552w3kmvis85mwlmh81f")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.2 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.2") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17ibix5lsi7khns8xac1fzvdlmj13crjqqvmjzzxi4ms784fbkpy")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.3 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.3") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05pdg8ic1gbadfbkvfrfc1l7i61981i12lg970rqnpc13cb8lihx")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.4 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.4") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nqphi8lmr5h7b7rwx516lyf5nkifxxx27zrccxb42ndc1332c30")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.5 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.5") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vhg8nwrkd48mfwixsnlhpw1nxsab7vli9r0hzq7mdfkfpkyf644")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.6 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.6") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ngazqz0fcip8q31b2jqndayz3jjmaxpjsbl0d4vzakzh4zm6nlk")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.7 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.7") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03x3qs19g8dpa8hfbix1ywx4f31bggsnpc9gmp71xqk5b2kdpqvv")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.8 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.8") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bqc4k0nim1z6w7sz020y84p2kczzhblijnhcyi3sn0dfq8r0xg5")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.10 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.10") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00p101m2ciwq59lbl6glvj6fgbbwxb01b28b5jrydnh1xx914i50")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.11 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.11") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zkrkfbnh4divra1qf4d0skxl7l9lm6gb7dhy4k82p77pabm0wyx")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.12 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.12") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bgahvn35wici7y47vyykf656sgncpsvdbxm11pyfzjrp74mxahm")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.13 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.13") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gfdgqyjjmi5gfd0zx1pnmhbz3dqjsqx1a11sq64pkxqvmqrfvgq")))

(define-public crate-oracle-nosql-db-sdk-rust-0.3.14 (c (n "oracle-nosql-db-sdk-rust") (v "0.3.14") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sbg2agrx3lwkbk6s3nq6c4dgzxm0wgr3gsf60wp7dgjrkxim52l")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.0 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.0") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1rafndz39ndid1gjgsi2rbmjk9npv7bp54dgy29nfrhfzkgxgp7x")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.2 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.2") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "161wgwifvn79b29hcf6lfs01b6mqh55w6fyyq32271bvw5q4wj25")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.3 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.3") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1zmdjmfmy8pf2382jf02ccrfj5vyvv9439z9mrn82d7y7f9ci1vp")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.4 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.4") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1ibc1wkjplyilv5jhcbidzgvrmqwayy1vci00rnszy58kb16q0lf")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.5 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.5") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jbzfgyjlrijkmlny2wynzk1qcxlj2gxxd7q1f7lh6mb67mkmwzs")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.6 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.6") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hh9dmpp5znnns2q4il952j74z7y0h7vvfbqmldqn2g1nmg17wr0")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.7 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.7") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gvsyv6zp5si0j20fxqyf6aazd0w45g2sg4m34dpqw4gfrqj19gh")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.8 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.8") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j5lg7ygfbh30dv759zh4w3zjqaz2c5lxy1nwridqxgap4hb3wid")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.9 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.9") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ramz10kl6rdcbvfgwqlvw5qq8na4vcb2zhw3lamw0yrzrkp65lb")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.10 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.10") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0waxqc2n1v05rb8764ghslccy3p6ia9dhpwlrzkzylfh5cjna2l5")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.11 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.11") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j3l129lny8pywhz9hc9d7jdglvnqr9i6bil39sbd9hq3jcpqbf5")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.12 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.12") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vjn3x3nkrpfr83zcsdsh3qpgzv1diw14vhlw1xz5m9c3nb63shw")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.13 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.13") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1n1v7mm7bm70bjc5l49cqf3j5qlwwr8fy70wkg9rss5xkn0cwkwj")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.15 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.15") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v4v1rk9lazijvk88l7d685hiw34sgf9wipd76lm3hmf4yfrdpzi")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.16 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.16") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qvdx2mdzmdp6abpzdyshdc2lw9fkg8l3733f0sd3yg19ljjkcgy")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.17 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.17") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xkdpmnzngfhsvwr78ifn0skk9sfssy1jky96lyzkjbbhq8qf7k7")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.18 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.18") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lw4kp21j1ydnv40g8c2a98ypqjf25s4fv4acik18zwiq7k0ypig")))

(define-public crate-oracle-nosql-db-sdk-rust-0.4.19 (c (n "oracle-nosql-db-sdk-rust") (v "0.4.19") (d (list (d (n "cgo_oligami") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gfa3dfr7gjcwwj57iv53jx83xlxa1k95hqdyy6n2ylgpbysfm0v")))

