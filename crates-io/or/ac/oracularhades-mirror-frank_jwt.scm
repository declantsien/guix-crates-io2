(define-module (crates-io or ac oracularhades-mirror-frank_jwt) #:use-module (crates-io))

(define-public crate-oracularhades-mirror-frank_jwt-3.1.3 (c (n "oracularhades-mirror-frank_jwt") (v "3.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "18sm3zdd0frh2ysb77szmhwarnpy65ib19ssgralisvqgr874cid")))

(define-public crate-oracularhades-mirror-frank_jwt-3.1.4 (c (n "oracularhades-mirror-frank_jwt") (v "3.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0k1wjzyz5airpxn91mmpr3ys30k8qffhn4r6w82msmfv3farwrjw")))

