(define-module (crates-io or ia orias) #:use-module (crates-io))

(define-public crate-orias-0.1.0 (c (n "orias") (v "0.1.0") (h "12j0bws334hx26c1n4mb2790la2b6q9mjz2qdhvbc3nia9h63gfd")))

(define-public crate-orias-0.1.1 (c (n "orias") (v "0.1.1") (h "1awswavv0psgr0b07kk4q3dv7igf4qhibcyqij6pkmkf645wnk67")))

