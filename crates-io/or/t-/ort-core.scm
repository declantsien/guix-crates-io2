(define-module (crates-io or t- ort-core) #:use-module (crates-io))

(define-public crate-ort-core-0.2.0 (c (n "ort-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "time"))) (d #t) (k 0)))) (h "0g54vn548scpazkj28jwkb79fjad75h52cqxixmxb3akfb96y7h0") (f (quote (("deser" "serde" "bytes/serde"))))))

