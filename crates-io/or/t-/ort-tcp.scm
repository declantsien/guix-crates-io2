(define-module (crates-io or t- ort-tcp) #:use-module (crates-io))

(define-public crate-ort-tcp-0.2.0 (c (n "ort-tcp") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "drain") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "ort-core") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "net" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s63cp48g2xb37g01m9ki3c57lwhhjhzvsf0f5npy7xavq9kxm5w")))

