(define-module (crates-io or t- ort-http) #:use-module (crates-io))

(define-public crate-ort-http-0.2.0 (c (n "ort-http") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "drain") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "client" "server" "tcp"))) (d #t) (k 0)) (d (n "ort-core") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fdgpqpkbpp27bv1ylwxcz3mbihmmnwgjkahlybqxfvqqj0d78df")))

