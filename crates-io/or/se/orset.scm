(define-module (crates-io or se orset) #:use-module (crates-io))

(define-public crate-orset-0.1.0 (c (n "orset") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "00ksk4gxwngc3chw17rvv0p0f79qgzmrqqpwnv0rz36pcfykn955")))

(define-public crate-orset-0.2.0 (c (n "orset") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p32bxj1csj5fhqj5zxp1gyqayxidg461a4czfckrgmrz868cfj0")))

