(define-module (crates-io or tb ortb_v2_5) #:use-module (crates-io))

(define-public crate-ortb_v2_5-0.1.7 (c (n "ortb_v2_5") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)))) (h "1565azq7hq8llbn414mlnzzmqajvhnfgs4mrf4f0702viv3s0bwx")))

