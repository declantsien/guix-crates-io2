(define-module (crates-io or fa orfail) #:use-module (crates-io))

(define-public crate-orfail-0.1.0 (c (n "orfail") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18xf55i4gjbdy92fqnhq6dk5x0z6ki3pxwpaf3dl2pcls389516j")))

(define-public crate-orfail-0.1.1 (c (n "orfail") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r0aqx79c03k06zkih6lcavx59rj7vavmx5m0as084jvs45alc0a")))

(define-public crate-orfail-0.1.2 (c (n "orfail") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fc3nwdibynpdxks4ha6bqzqpv4mb0g0izvybc0gbm0871cplpmz")))

(define-public crate-orfail-0.1.3 (c (n "orfail") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hylw60y9jdwcqlwwcaga18h6sswkz9wlwdi6k4n1vp8vlj3asb3")))

(define-public crate-orfail-1.0.0 (c (n "orfail") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1raia366yha5ff7z4na8imn7yw6gvv5py1iy6k9zhb42n2xs5w6c")))

(define-public crate-orfail-1.1.0 (c (n "orfail") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bj4ygq95ynp8pr72jn257g5whw26cwv1fcz43491lnx1wm238mb")))

