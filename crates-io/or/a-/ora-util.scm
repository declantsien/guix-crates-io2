(define-module (crates-io or a- ora-util) #:use-module (crates-io))

(define-public crate-ora-util-0.1.0 (c (n "ora-util") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0s9dbfg8clx2bzdj8x0f3wi0fj7qdp9kilf39abxk5fgchgrc5zd")))

(define-public crate-ora-util-0.1.1 (c (n "ora-util") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "19yzfgkqc9kak9dk8y099fa6iha9x5pl7vbl731jxpp6hr436gn5")))

