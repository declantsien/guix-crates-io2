(define-module (crates-io or a- ora-macros) #:use-module (crates-io))

(define-public crate-ora-macros-0.1.0 (c (n "ora-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1q8izsspiw67m2010klnyf7jj5l6nrpkmwx0dcw1vswvn1abx0bs")))

