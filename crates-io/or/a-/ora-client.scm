(define-module (crates-io or a- ora-client) #:use-module (crates-io))

(define-public crate-ora-client-0.1.0 (c (n "ora-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "1lbc5r59rp19l4356ncpqafvmpmffdk9g6n92sqc8p7id228lpgl")))

(define-public crate-ora-client-0.2.0 (c (n "ora-client") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "1cpgzawywmlr5ra1z404ryzjkn65x80pdjv4i95gsp4pz5gm0nhr")))

(define-public crate-ora-client-0.2.1 (c (n "ora-client") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "0c4r88a0qmjwyxxpg3ng1b9cyp05ppwn61k5gcx2i5m51qywdf9r")))

(define-public crate-ora-client-0.3.0 (c (n "ora-client") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ora-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "1zxc8jcwj1sc3fgcy49rzrdv7i26v8fp8chayjwd11c91zkpafmc")))

