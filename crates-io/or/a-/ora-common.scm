(define-module (crates-io or a- ora-common) #:use-module (crates-io))

(define-public crate-ora-common-0.1.0 (c (n "ora-common") (v "0.1.0") (d (list (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde" "serde-well-known"))) (d #t) (k 0)))) (h "0g1zq6wnzs4r6map0y2qxwxgn1dj39l89qmsw6kv520hjs64xfv5")))

(define-public crate-ora-common-0.1.1 (c (n "ora-common") (v "0.1.1") (d (list (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde" "serde-well-known"))) (d #t) (k 0)))) (h "1vl8rhkwpjnnhxml9qvqh7qyykp3d5n95kblkcndysbjssfzfsdv")))

(define-public crate-ora-common-0.1.2 (c (n "ora-common") (v "0.1.2") (d (list (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde" "serde-well-known"))) (d #t) (k 0)))) (h "0y31mfwkbwd80cvsi4kl9drgarl2gn6fkzcsyyxybhi7r4lzsqb2")))

