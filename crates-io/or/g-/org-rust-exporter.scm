(define-module (crates-io or g- org-rust-exporter) #:use-module (crates-io))

(define-public crate-org-rust-exporter-0.1.0 (c (n "org-rust-exporter") (v "0.1.0") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-rust-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1mck9sbw79zmhjaqa15a1i8hrxl5ghqsqg0q058ak6fwjagdirq7")))

(define-public crate-org-rust-exporter-0.1.1 (c (n "org-rust-exporter") (v "0.1.1") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-parser") (r "^0.1.1") (d #t) (k 0) (p "org-rust-parser")) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "14mp1faf9ckrq5asmnw6h88fdvnfrjm6v8pnnm00mmfhnlbh9qyy") (r "1.70")))

(define-public crate-org-rust-exporter-0.1.2 (c (n "org-rust-exporter") (v "0.1.2") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-parser") (r "^0.1.2") (d #t) (k 0) (p "org-rust-parser")) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0zk602h4d0jnjhn8azhxklzz252z3c0yd9pzh2lkw4f7pmrn68jg") (r "1.74")))

(define-public crate-org-rust-exporter-0.1.3 (c (n "org-rust-exporter") (v "0.1.3") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-parser") (r "^0.1.2") (d #t) (k 0) (p "org-rust-parser")) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0mzv576ayca1zdcmf9qsn26nhyy7bczqi7czia0w85r45b1pfmzg") (r "1.74")))

(define-public crate-org-rust-exporter-0.1.4 (c (n "org-rust-exporter") (v "0.1.4") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-parser") (r "^0.1.3") (d #t) (k 0) (p "org-rust-parser")) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0lgpvn6md8p59zf7iyirwym1c4l56cvgj73npff0zzz17slkja81") (r "1.74")))

(define-public crate-org-rust-exporter-0.1.5 (c (n "org-rust-exporter") (v "0.1.5") (d (list (d (n "latex2mathml") (r "^0.2.3") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "org-parser") (r "^0.1.3") (d #t) (k 0) (p "org-rust-parser")) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0n3pn07paa0kh38yxv4bzn0pkdaahsdb8l47l3mm1h6ypg53yvp7") (r "1.74")))

