(define-module (crates-io or g- org-tangle) #:use-module (crates-io))

(define-public crate-org-tangle-0.1.0 (c (n "org-tangle") (v "0.1.0") (h "1bl8znszv3prjd376s5fsmcrm6n3qh2yqcwv9nd7q7zbw9xh2yq6") (y #t)))

(define-public crate-org-tangle-0.1.1 (c (n "org-tangle") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "org-parser") (r "^0.1") (d #t) (k 0)))) (h "1g7kbpkmlzmj676jsp0iyh8nyg7zf335qhn6idxmwzq9hkrb2311") (y #t)))

(define-public crate-org-tangle-0.1.2 (c (n "org-tangle") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "1kkr0zah7ajv8s913lpxwqfxipmjfrgn7k04iz7jpq46a5qf7k5m") (y #t)))

(define-public crate-org-tangle-0.1.3 (c (n "org-tangle") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "07zl9jqq7348gn6k29ql1i6zfs3ldpaqvs47fv8inhglqm6crg1v") (y #t)))

(define-public crate-org-tangle-0.1.4 (c (n "org-tangle") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "17al19jm59x2xz5lm3ji796fqkrjb8nyc8n2nr6lrgm1shbmf5ih") (y #t)))

(define-public crate-org-tangle-0.1.5 (c (n "org-tangle") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "org-tangle-engine") (r "^0.1") (d #t) (k 0)))) (h "0qsjfzp3zn5328d285646d94kbaw7a06ix3206mw0d92lbnwbhaw") (y #t)))

