(define-module (crates-io or g- org-parser) #:use-module (crates-io))

(define-public crate-org-parser-0.1.0 (c (n "org-parser") (v "0.1.0") (h "0fzaggfxhfsrlssk0d1ivzxn0x86gpnxj86pcwvf3biy7467h1lf") (y #t)))

(define-public crate-org-parser-0.1.1 (c (n "org-parser") (v "0.1.1") (h "12x7s5jafxrnqh6mfchr1sbyrqlzk4w8ycn4v22z2h8w1xq66z4i") (y #t)))

