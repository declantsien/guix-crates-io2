(define-module (crates-io or g- org-tangle-engine) #:use-module (crates-io))

(define-public crate-org-tangle-engine-0.1.0 (c (n "org-tangle-engine") (v "0.1.0") (d (list (d (n "org-parser") (r "^0.1") (d #t) (k 0)))) (h "1kliga19fxfkk6ph47yq3n63kwv3ymf9z33cb4xv0grd4b3qwp0h") (y #t)))

(define-public crate-org-tangle-engine-0.1.1 (c (n "org-tangle-engine") (v "0.1.1") (d (list (d (n "org-parser") (r "^0.1") (d #t) (k 0)))) (h "0ig463w8y5qcsw5bp81n6b1w7msj5sbnzzcwcafmzwwmsbya0r9j") (y #t)))

(define-public crate-org-tangle-engine-0.1.2 (c (n "org-tangle-engine") (v "0.1.2") (h "1a2xszcnlp23k95b1zq3bl0p9jv5wjrlx50d25q8n1f0j8bvwf1l") (y #t)))

(define-public crate-org-tangle-engine-0.1.3 (c (n "org-tangle-engine") (v "0.1.3") (h "0gqn21ahhmj7v2ml2gnifw2aafap8jf367qw2mmzbnfv1wbp67ch") (y #t)))

(define-public crate-org-tangle-engine-0.1.4 (c (n "org-tangle-engine") (v "0.1.4") (h "1k8qcyjpyq2crjmg981d1n4prr395xp8nxq2cdsi61agw5a9rk7b") (y #t)))

(define-public crate-org-tangle-engine-0.1.5 (c (n "org-tangle-engine") (v "0.1.5") (h "0dh6mv44m0rzvni7k53d0wz062nb7sbdsr8zfs3qnadj1sjbgm92") (y #t)))

(define-public crate-org-tangle-engine-0.1.7 (c (n "org-tangle-engine") (v "0.1.7") (h "156dx5aznxwd3qban55b5ml5m47i2yk832pl86hwl7inm0m7m44c") (y #t)))

(define-public crate-org-tangle-engine-0.1.8 (c (n "org-tangle-engine") (v "0.1.8") (h "0qbkni4136iz9dwwfn7jjrx1nrcg8a42zp09nr7q9c0ksn6ay78y") (y #t)))

(define-public crate-org-tangle-engine-0.1.9 (c (n "org-tangle-engine") (v "0.1.9") (h "07a5p00khlglh8iam4dqrq24acf4gvqnhxbfws52jh2crfh7gj77") (y #t)))

