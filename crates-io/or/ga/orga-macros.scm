(define-module (crates-io or ga orga-macros) #:use-module (crates-io))

(define-public crate-orga-macros-0.1.0 (c (n "orga-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0dy0mm16jj4jd6f67yh7y5i9aisqnifmxjbp6cj2bg92s73dldir")))

(define-public crate-orga-macros-0.2.0 (c (n "orga-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0r21gyn4lbx4fys4lbkqwz5s19c2i5m5xszddr3cc3as4nls3x9j")))

(define-public crate-orga-macros-0.2.1 (c (n "orga-macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkn7df27pb58hmqzhyd3l20173ia39prbfp4rmnacvvz09drxgq")))

(define-public crate-orga-macros-0.2.2 (c (n "orga-macros") (v "0.2.2") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0p622lph3ff45q1yp9v95jqra4rm2gxzliwhj83ish9c9ndjmrb7")))

(define-public crate-orga-macros-0.2.3 (c (n "orga-macros") (v "0.2.3") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1b0gxzibrwfrfa8pf51n9jqgbwk1mddjl1jf51bglsnwl80a3dzr")))

