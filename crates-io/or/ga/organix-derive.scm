(define-module (crates-io or ga organix-derive) #:use-module (crates-io))

(define-public crate-organix-derive-0.1.0 (c (n "organix-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "0vfbi8l8slisdx4m3q1zr6m5b073vxqnrxyn0zngihbp4miiz6q9")))

