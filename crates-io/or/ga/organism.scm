(define-module (crates-io or ga organism) #:use-module (crates-io))

(define-public crate-organism-0.1.0 (c (n "organism") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0nv1l68fjy6fz41ixz5jmb6l0wjak8rvi2jrb7hzx9kfgzlv72fm")))

(define-public crate-organism-0.1.1 (c (n "organism") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1p2pw4alv5kzpwrgc8zdf9cn4mdrx79vvz2aah0jh614dz8cb8xa")))

