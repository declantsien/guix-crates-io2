(define-module (crates-io or ga organisationsnummer) #:use-module (crates-io))

(define-public crate-organisationsnummer-1.0.0 (c (n "organisationsnummer") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "personnummer") (r "^3.1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1h1xs763jjjvxflj2x9smy180zr1zgnjic7pj853h0g39sx5c5s2")))

(define-public crate-organisationsnummer-1.0.1 (c (n "organisationsnummer") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "personnummer") (r "^3.1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0q8phqxvjr1y4ym9cwyfv3ys4g8psiciqyz8cw8z1mqq93b4q4h2")))

