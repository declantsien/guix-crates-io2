(define-module (crates-io or th orthrus-core) #:use-module (crates-io))

(define-public crate-orthrus-core-0.2.0 (c (n "orthrus-core") (v "0.2.0") (d (list (d (n "der") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.8") (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (o #t) (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0sn01nph6wddpd0c7l0bxm2gk8awi9akgvmd99wv0g8pxi6vfgdd") (f (quote (("std") ("default" "std") ("certificate" "der" "x509-cert")))) (s 2) (e (quote (("time" "dep:time"))))))

