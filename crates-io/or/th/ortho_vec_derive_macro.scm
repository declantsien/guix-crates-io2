(define-module (crates-io or th ortho_vec_derive_macro) #:use-module (crates-io))

(define-public crate-ortho_vec_derive_macro-0.1.0 (c (n "ortho_vec_derive_macro") (v "0.1.0") (d (list (d (n "ortho_vec_derive_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18hajvhkbb8ai733rvm5n0jxi6vwk3sd61s0jym9hx6cvdbc11xz")))

