(define-module (crates-io or th orthanc-plugin-bindings) #:use-module (crates-io))

(define-public crate-orthanc-plugin-bindings-0.1.0 (c (n "orthanc-plugin-bindings") (v "0.1.0") (h "1fdlacf0nck0ksgzhxd3fqzmcpw0vxcj58379m2b5jwyk3q0pcii")))

(define-public crate-orthanc-plugin-bindings-0.1.1 (c (n "orthanc-plugin-bindings") (v "0.1.1") (h "0336wsf9npxgcayprb69hlcyhc4yiy7zvmf9p8gphpgrw2icjnky")))

