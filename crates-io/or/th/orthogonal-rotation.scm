(define-module (crates-io or th orthogonal-rotation) #:use-module (crates-io))

(define-public crate-orthogonal-rotation-0.0.5 (c (n "orthogonal-rotation") (v "0.0.5") (h "071fsjx56fjza0cyjzmz8jncmp7sjrxw3mnkis0rpsm4nx2ikmy2") (y #t)))

(define-public crate-orthogonal-rotation-0.0.6 (c (n "orthogonal-rotation") (v "0.0.6") (h "0zpasqrk9k4wmzzxdzlzk2ca34m7dqp5spd1hx07syj7jnf9m9s3")))

(define-public crate-orthogonal-rotation-0.0.7 (c (n "orthogonal-rotation") (v "0.0.7") (h "0wbmi8399rxy53pf155an3vviq7dbpxy27rf4y7sz2zcy50hvfq6")))

(define-public crate-orthogonal-rotation-0.0.8 (c (n "orthogonal-rotation") (v "0.0.8") (h "0wbr0jdhxfrygdr295yk6pw1jf10j1pkrginn8wdpd5rl46q0c0z")))

(define-public crate-orthogonal-rotation-0.0.9 (c (n "orthogonal-rotation") (v "0.0.9") (h "161cvacgk5f6qsjs96fp2fcikjrrdnlals0hssakj8rgh6262d8k")))

(define-public crate-orthogonal-rotation-0.0.10 (c (n "orthogonal-rotation") (v "0.0.10") (h "0bmv3v6p5x716ldnh6f8g11j8ihiq662ffdqv4ib96c1a9w7kbj0")))

