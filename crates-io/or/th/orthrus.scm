(define-module (crates-io or th orthrus) #:use-module (crates-io))

(define-public crate-orthrus-0.1.0 (c (n "orthrus") (v "0.1.0") (d (list (d (n "clap") (r "~3.2") (d #t) (k 0)) (d (n "orthrus-yaz0") (r "^0.1.0") (d #t) (k 0)))) (h "1xsh456jg29200l8rk6nn3l0mvmr719xgbivskidvj3v41y4i9dd")))

(define-public crate-orthrus-0.2.0 (c (n "orthrus") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argp") (r "^0.3") (k 0)) (d (n "enable-ansi-support") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "orthrus-core") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "orthrus-ncompress") (r "^0.2") (d #t) (k 0)) (d (n "orthrus-panda3d") (r "^0.1") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "0bz7qmxncjydm1wh72gv0cipmqjf8rszb6qm60zvbpxlghn95jxv")))

