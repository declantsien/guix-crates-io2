(define-module (crates-io or th orthrus-ncompress) #:use-module (crates-io))

(define-public crate-orthrus-ncompress-0.2.0 (c (n "orthrus-ncompress") (v "0.2.0") (d (list (d (n "orthrus-core") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (k 0)))) (h "01zbbnbxr80h79xa2ssb225a0j6dbznpfkjyki6j1qs3xw99q2vp") (f (quote (("std") ("default" "std"))))))

