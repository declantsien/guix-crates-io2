(define-module (crates-io or th ortho_vec_derive) #:use-module (crates-io))

(define-public crate-ortho_vec_derive-0.1.0 (c (n "ortho_vec_derive") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ortho_vec_derive_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "ortho_vec_derive_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pw9dkhfzhjrss5kyvfkn726g8jzkapwwrif0ja3pmmrikmcvgcl") (f (quote (("default"))))))

