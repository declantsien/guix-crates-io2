(define-module (crates-io or th orthrus-panda3d) #:use-module (crates-io))

(define-public crate-orthrus-panda3d-0.1.0 (c (n "orthrus-panda3d") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "der") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "orthrus-core") (r "^0.2.0") (f (quote ("certificate" "time"))) (d #t) (k 0)) (d (n "snafu") (r "^0.8") (k 0)))) (h "11ily57rm1nas79fz77yvzkda6l9729pfmxxdriqzx9c9l4jjvch") (f (quote (("std" "snafu/std") ("signature" "der") ("default" "std"))))))

