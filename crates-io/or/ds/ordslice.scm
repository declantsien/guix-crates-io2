(define-module (crates-io or ds ordslice) #:use-module (crates-io))

(define-public crate-ordslice-0.1.0 (c (n "ordslice") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1mxm35nd4bjf27fmly1wqwn99my03vfil2gzhi34ypd8l1hd0lfj")))

(define-public crate-ordslice-0.2.0 (c (n "ordslice") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0x02718apcr08h7cljcq5rk77pnv70xk6j89350jgliqa89ydr84")))

(define-public crate-ordslice-0.3.0 (c (n "ordslice") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0950m13dnh1dzw1059fw65l7c2jyqimax03xrclnhdz4vg1yw86x")))

