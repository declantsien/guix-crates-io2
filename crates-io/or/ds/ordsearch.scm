(define-module (crates-io or ds ordsearch) #:use-module (crates-io))

(define-public crate-ordsearch-0.1.0 (c (n "ordsearch") (v "0.1.0") (d (list (d (n "prefetch") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0qv934xbkjicvx6mfyd3q4afn6rlwg7az0vl0f7ci93nc51ar67d") (f (quote (("default") ("bench"))))))

(define-public crate-ordsearch-0.2.0 (c (n "ordsearch") (v "0.2.0") (d (list (d (n "prefetch") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "197m5pk50is6qz1jpqp7vzswfsma6h863l270qvpyaldcc3x8lwn") (f (quote (("nightly" "prefetch") ("default"))))))

(define-public crate-ordsearch-0.2.1 (c (n "ordsearch") (v "0.2.1") (h "19i0bwhrzmk8y4kppv8n0rsp9z9h1kjwczcz7mf7if42hcw4a7l1") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.2 (c (n "ordsearch") (v "0.2.2") (h "0dm0yxkd1n96abp2ppclglx33cx4z835mp2yi471dwa4v63g6g30") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.3 (c (n "ordsearch") (v "0.2.3") (h "0hxnddp5fndb3a0vpqwcqb2xbmn6q6gzjwfh484l2s82bzskrkqd") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.4 (c (n "ordsearch") (v "0.2.4") (h "1kf6ay5smc46k22igb0hdvazzfh9j2d516qa64lid420z0gj3amh") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.5 (c (n "ordsearch") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (t "cfg(any())") (k 0)) (d (n "serde") (r "^1.0.100") (o #t) (d #t) (t "cfg(any())") (k 0)))) (h "1ribzzkfhk80bg36fv647x4dsgaxba6imfpkrn5vc40spgyswkhz") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.6 (c (n "ordsearch") (v "0.2.6") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (t "cfg(any())") (k 0)) (d (n "serde") (r "^1.0.100") (o #t) (d #t) (t "cfg(any())") (k 0)))) (h "0xkxmdfkj63nqycvyrchqq3vp84hg89aq7mhm76i476bgjg4achj") (f (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2.7 (c (n "ordsearch") (v "0.2.7") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (t "cfg(any())") (k 0)) (d (n "serde") (r "^1.0.100") (o #t) (d #t) (t "cfg(any())") (k 0)))) (h "1xks1n1w66m5m4316az6ls30yllfimw4rhsyhca4k5v90p8rdcpc") (f (quote (("nightly") ("default"))))))

