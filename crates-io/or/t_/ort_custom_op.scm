(define-module (crates-io or t_ ort_custom_op) #:use-module (crates-io))

(define-public crate-ort_custom_op-0.1.0 (c (n "ort_custom_op") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1la5hy89hyk3xglsl679qx7yay19bp6g09vp2ld2snxqlmbdx8d8")))

(define-public crate-ort_custom_op-0.1.1 (c (n "ort_custom_op") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "071kyiwxwjp6y83mdrkizkw0nc8dd3rnzdjyzsf32sb2xh2fcxpw")))

(define-public crate-ort_custom_op-0.2.0 (c (n "ort_custom_op") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0cq1y8z4610ivs42gjajwf5yzg5yxqf4xaxvcjihvgn5lc19h1k3")))

(define-public crate-ort_custom_op-0.3.0 (c (n "ort_custom_op") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1x8wr99s07yhrrqd9zh12813afp9vyvx8gw3d79sxpiw59aww7an")))

(define-public crate-ort_custom_op-0.4.0 (c (n "ort_custom_op") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0b8kf2mgm5ijn1vbsvgnw41kcid9kkfgvxrxs7h793961hm675n4")))

(define-public crate-ort_custom_op-0.4.1 (c (n "ort_custom_op") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1a0d13b6vva22jz5pyhvsv1iargq9vm3ws9vvsj1zxpas2lzbrkb")))

(define-public crate-ort_custom_op-0.5.0 (c (n "ort_custom_op") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "186fpj5wwlpypbcbknisdh4bd02wgpgysc3grychm63w2p5bvl3g")))

(define-public crate-ort_custom_op-0.5.1 (c (n "ort_custom_op") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "11f1v72d25rganqvf8smzx61ip98p7yfiii38d6sb26ipkczkwvk")))

(define-public crate-ort_custom_op-0.6.0 (c (n "ort_custom_op") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0f0p0sf2zlw2wdbsl7s4vyvgv2cqyq8sla91zkbby94g3kb3x0s3")))

(define-public crate-ort_custom_op-0.7.0 (c (n "ort_custom_op") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "02czw7xvwfgxv4ycw4sfhhzc3fsylabnnwpqwl1kj5rbgf40m2pd")))

