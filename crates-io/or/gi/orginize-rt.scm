(define-module (crates-io or gi orginize-rt) #:use-module (crates-io))

(define-public crate-orginize-rt-0.9.0 (c (n "orginize-rt") (v "0.9.0") (d (list (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0v1y07xdvj3mhlf5id8xgc7kmddvr5pnc4rk1as5bq1c2si1kfrw")))

