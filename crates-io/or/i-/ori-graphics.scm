(define-module (crates-io or i- ori-graphics) #:use-module (crates-io))

(define-public crate-ori-graphics-0.1.0-alpha.0 (c (n "ori-graphics") (v "0.1.0-alpha.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "13jznr1fpa2gy24zsa0jwh8dx9mlmg1qh3wx5i5ilh5lzg38x262")))

(define-public crate-ori-graphics-0.1.0-alpha.1 (c (n "ori-graphics") (v "0.1.0-alpha.1") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1pp9fy9fw5bi1aaijj8zspp2ywiky90brzc26dn6hm76ijghirlc")))

