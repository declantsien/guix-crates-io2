(define-module (crates-io or i- ori-winit) #:use-module (crates-io))

(define-public crate-ori-winit-0.1.0-alpha.0 (c (n "ori-winit") (v "0.1.0-alpha.0") (d (list (d (n "ori-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-graphics") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-wgpu") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "winit") (r "^0.28.0") (d #t) (k 0)))) (h "16rxk861ljxs9ad7x8w423kj8pm1spdkylyqpwvnlbq2ar0k5yrv") (f (quote (("wgpu" "ori-wgpu") ("default"))))))

