(define-module (crates-io or i- ori-wgpu) #:use-module (crates-io))

(define-public crate-ori-wgpu-0.1.0-alpha.0 (c (n "ori-wgpu") (v "0.1.0-alpha.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ori-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-graphics") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "wgpu_glyph") (r "^0.19") (d #t) (k 0)))) (h "1avmhs3ijyzqs4l7p8bjafixmhdw2jpc4hc2ylvnypbhizdpp9jm")))

(define-public crate-ori-wgpu-0.1.0-alpha.1 (c (n "ori-wgpu") (v "0.1.0-alpha.1") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ori-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-graphics") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "wgpu_glyph") (r "^0.19") (d #t) (k 0)))) (h "11m3w6v5ydzbzp6wphr67mln0d51mz5l4fpv8ranxazj8m1fsiyr")))

