(define-module (crates-io or ml orml-oracle-rpc) #:use-module (crates-io))

(define-public crate-orml-oracle-rpc-0.4.0 (c (n "orml-oracle-rpc") (v "0.4.0") (d (list (d (n "codec") (r "^2.0.0") (d #t) (k 0) (p "parity-scale-codec")) (d (n "jsonrpc-core") (r "^15.0.0") (d #t) (k 0)) (d (n "jsonrpc-core-client") (r "^15.0.0") (d #t) (k 0)) (d (n "jsonrpc-derive") (r "^15.0.0") (d #t) (k 0)) (d (n "orml-oracle-rpc-runtime-api") (r "^0.4.0") (d #t) (k 0)) (d (n "sp-api") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-blockchain") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^3.0.0") (d #t) (k 0)))) (h "0xd0kfa8d07k3p3s3slxswf4pcyhqq2clyqsjaqbc563aq2whxsz")))

