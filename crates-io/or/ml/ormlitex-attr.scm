(define-module (crates-io or ml ormlitex-attr) #:use-module (crates-io))

(define-public crate-ormlitex-attr-0.17.1 (c (n "ormlitex-attr") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlmo") (r "^0.16") (d #t) (k 0)) (d (n "structmeta") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00va4qlz2wjnj5lfw50fdvkn2wnz61xfkf22kznmpykmb952mk3m")))

