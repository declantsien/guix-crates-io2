(define-module (crates-io or ml ormlib_derive) #:use-module (crates-io))

(define-public crate-ormlib_derive-0.2.1 (c (n "ormlib_derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xmj9b26jip1479m4qmpdwkraisl7kkhyzm4yi3dsyml11ymy7mx") (y #t)))

(define-public crate-ormlib_derive-0.2.2 (c (n "ormlib_derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f1ynqjasvp731fpa8gky7m5r7kj4csghqw83sy12bly9y9cphwb") (y #t)))

(define-public crate-ormlib_derive-0.2.3 (c (n "ormlib_derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ssf3sg1lj5cm7b0p0w318l31iq7hfaq0lnf45l0zm6ib5c2771h") (y #t)))

(define-public crate-ormlib_derive-0.2.4 (c (n "ormlib_derive") (v "0.2.4") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cd6z5ifggzz0i5gwll27kk4gkkjxdnqcv3l3wlh5p81m73pfbk5") (y #t)))

(define-public crate-ormlib_derive-0.2.5 (c (n "ormlib_derive") (v "0.2.5") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17hn5nc3cvpp4rb9fplgblslhvzzfz0q4kxbpc5z7qrm6iif1wcx") (y #t)))

(define-public crate-ormlib_derive-0.2.6 (c (n "ormlib_derive") (v "0.2.6") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pwq3hp91zjwcj9541b9rsj333fvrhda7bbz7hynw2iazivinykd") (y #t)))

(define-public crate-ormlib_derive-0.2.7 (c (n "ormlib_derive") (v "0.2.7") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p02gk7p29cyjhm8jkrgrd06si0vy4qqvvmxphc9i01mgkb4ca7c") (y #t)))

(define-public crate-ormlib_derive-0.2.8 (c (n "ormlib_derive") (v "0.2.8") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.2.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nhd5a87gp51jb687bmf8a59nvghgw8kbp15cgswsr33rmzgjsi3") (y #t)))

(define-public crate-ormlib_derive-0.3.0 (c (n "ormlib_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n7kkc4x4x2k2hxa9i47kwffp1p5l0yjynfh7xaykva4njv1gyv0") (y #t)))

(define-public crate-ormlib_derive-0.3.1 (c (n "ormlib_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3y8xz4ibkg635x2vfjjrwmwsbsh1a0lq9s2mnikifqsq8bxz69") (y #t)))

(define-public crate-ormlib_derive-0.3.3 (c (n "ormlib_derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16b8nh5r7525h1ddv7ki502v67d95g2j5qgbazy7v4vpgy96g9al") (y #t)))

(define-public crate-ormlib_derive-1.0.0 (c (n "ormlib_derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c977cj8yd1gpmyl3zsah80hzavjpygn9j966wl42ky8yxbyhk5v") (y #t)))

(define-public crate-ormlib_derive-1.0.1 (c (n "ormlib_derive") (v "1.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zgicxn9ajmqh98rm2z1y2ng93c86cn8izk5fgpys3qsdnd4g0s0") (y #t)))

(define-public crate-ormlib_derive-1.0.2 (c (n "ormlib_derive") (v "1.0.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qz0xhsidab77wv6x092s3j68a7x7gkkzal826zzgbyag2fkzwp7") (y #t)))

(define-public crate-ormlib_derive-2.0.1 (c (n "ormlib_derive") (v "2.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pmgmx8d457c4aka7axgf9az4l5spac9zap29jw3ac934ciyrgj6") (y #t)))

(define-public crate-ormlib_derive-2.0.2 (c (n "ormlib_derive") (v "2.0.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ormlib") (r "^2.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rdrx39gbyr47cmd55b1i4ii0a8kqs2djb3jz82c5nqai8hwv5xy") (y #t)))

