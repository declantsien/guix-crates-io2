(define-module (crates-io or ml orml-build-script-utils) #:use-module (crates-io))

(define-public crate-orml-build-script-utils-0.0.0 (c (n "orml-build-script-utils") (v "0.0.0") (h "0l3x9rcmnz87kwyn9c330ja008im8m1yj8iwnybkdi2g6bq5w3ri") (y #t)))

(define-public crate-orml-build-script-utils-0.6.7 (c (n "orml-build-script-utils") (v "0.6.7") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1n198zmir6wyp2gy0c3npnyzg7k55djs90qzy2f9d623pn5bhafv")))

(define-public crate-orml-build-script-utils-0.7.0 (c (n "orml-build-script-utils") (v "0.7.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1f1vxn4j0nyraffqll08csqfrkmgrmirdrj0yw9j9pckkgax28vj")))

(define-public crate-orml-build-script-utils-0.8.0 (c (n "orml-build-script-utils") (v "0.8.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mhzaicpikr9bb070b9hy3jnyndr4iixmg6v9cawbc0h5xwdq542")))

(define-public crate-orml-build-script-utils-0.9.0 (c (n "orml-build-script-utils") (v "0.9.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kw9yssxxfs7qfy5fcphhad1sffhi7c37ql454dby9swrkqjp8v0") (y #t)))

(define-public crate-orml-build-script-utils-0.9.1 (c (n "orml-build-script-utils") (v "0.9.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0a89c4c7mhk5m8v4chfzv9v27336dr8lmbxlbhajcmcd3rlvgdqv")))

(define-public crate-orml-build-script-utils-0.10.0 (c (n "orml-build-script-utils") (v "0.10.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ijp5dnggn78facc04h00vb0vpbjl8klli142m2hv25krn7g4kx5")))

