(define-module (crates-io or ml orml-oracle-runtime-api) #:use-module (crates-io))

(define-public crate-orml-oracle-runtime-api-0.6.1 (c (n "orml-oracle-runtime-api") (v "0.6.1") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "116z9mx0w29zcyz7l6557m3g7nv3xyby3wis40y4fbrq7b2zkzyf") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.6.3 (c (n "orml-oracle-runtime-api") (v "0.6.3") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "110cmijfafri0pg6m9fjl2sc84csqpw525jlan47k3g3q8s0mdy2") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.6.4 (c (n "orml-oracle-runtime-api") (v "0.6.4") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "1mvag5545drb410crvvd441grby9w5baqi5hs7c6dzjfsiy85090") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.6.5 (c (n "orml-oracle-runtime-api") (v "0.6.5") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "0a908d8i497hcz37f48rnpignnqffpni5xjfdaxs264rwcpqyh16") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.6.6 (c (n "orml-oracle-runtime-api") (v "0.6.6") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "1xyrhx8yvmzsmifrjs8z2jp6yyj7ny9m7jd5xjbqgssa2adl94bg") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.6.7 (c (n "orml-oracle-runtime-api") (v "0.6.7") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-std") (r "^12.0.0") (k 0)))) (h "0wdhrswnp814a19rdsh7jf06r6kasy4wp74nbjl3hwfvdslafczh") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.7.0 (c (n "orml-oracle-runtime-api") (v "0.7.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^26.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0im7yjl05rrcy5pp4ph422znbiq2id1djxajidmc7fkybiz0hlfj") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.8.0 (c (n "orml-oracle-runtime-api") (v "0.8.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^27.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0zn74wg7jgca358kh22gaszgx3gzp6imbi9dskh3bn9yn0296lr9") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.9.0 (c (n "orml-oracle-runtime-api") (v "0.9.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^27.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0f9kml1by0q1p0fi6gbfmy7s89mw91pnz6vbrjwhv91bmfjsh7vs") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std")))) (y #t)))

(define-public crate-orml-oracle-runtime-api-0.9.1 (c (n "orml-oracle-runtime-api") (v "0.9.1") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^29.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "0dslipjafw7vx6cqki570p68inhk0c9869p5ch6y7szsadanqbwg") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-orml-oracle-runtime-api-0.10.0 (c (n "orml-oracle-runtime-api") (v "0.10.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("derive"))) (k 0)) (d (n "sp-api") (r "^30.0.0") (k 0)) (d (n "sp-std") (r "^14.0.0") (k 0)))) (h "1qvyakjq34g4k3c8n8fg2yljim1yxsqjq3vyz9mm5mdmcm74l8a1") (f (quote (("std" "parity-scale-codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

