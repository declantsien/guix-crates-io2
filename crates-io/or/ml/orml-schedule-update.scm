(define-module (crates-io or ml orml-schedule-update) #:use-module (crates-io))

(define-public crate-orml-schedule-update-0.0.1 (c (n "orml-schedule-update") (v "0.0.1") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-alpha.6") (k 0)) (d (n "frame-system") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.6") (k 0)))) (h "0sb0w9mj4sivarawfj6s2xbfnnhqrrjfykn4632y6dv0y0r8zbzq") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-std/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-orml-schedule-update-0.1.1 (c (n "orml-schedule-update") (v "0.1.1") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-rc2") (k 0)) (d (n "frame-system") (r "^2.0.0-rc2") (k 0)) (d (n "orml-traits") (r "^0.1.1") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc2") (k 0)) (d (n "sp-std") (r "^2.0.0-rc2") (k 0)))) (h "1g1hc8400b8ni6n6xik6c9n3grgy0kbnvqi1xajib39sfcchgrav") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-std/std" "sp-runtime/std" "orml-traits/std") ("default" "std"))))))

