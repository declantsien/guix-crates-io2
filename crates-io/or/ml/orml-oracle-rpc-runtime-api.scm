(define-module (crates-io or ml orml-oracle-rpc-runtime-api) #:use-module (crates-io))

(define-public crate-orml-oracle-rpc-runtime-api-0.4.0 (c (n "orml-oracle-rpc-runtime-api") (v "0.4.0") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "0wzb65phk14l25nggvsqvhblyv663biwgia9h2sy2zhsm7v1jqlw") (f (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

