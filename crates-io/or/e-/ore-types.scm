(define-module (crates-io or e- ore-types) #:use-module (crates-io))

(define-public crate-ore-types-0.0.1 (c (n "ore-types") (v "0.0.1") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0izg8dklzgc590kiyl76ah084v4xymddvflf84ckl4cixn402zn4")))

(define-public crate-ore-types-0.0.2 (c (n "ore-types") (v "0.0.2") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ga4hnbv7ybwzfxq4iwbwvqpn7x0m04vg902h47accfsr6rvrfx8")))

(define-public crate-ore-types-0.0.3 (c (n "ore-types") (v "0.0.3") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pwi1h5150wzqwnwrvbxjxdgn47bd2k0wdmdqcs5flwqg8q1pqfj")))

(define-public crate-ore-types-0.0.4 (c (n "ore-types") (v "0.0.4") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q1fizdxi6nnnapshc1lbx8bgfx1abhgahm4d756pk9zr7n6grqk")))

(define-public crate-ore-types-0.0.5 (c (n "ore-types") (v "0.0.5") (d (list (d (n "postgres-types") (r "^0.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0py8griqs2rgy27pvxxh1vgdrlwl9j81a86sgpkbsdjbnpmvyqxc")))

