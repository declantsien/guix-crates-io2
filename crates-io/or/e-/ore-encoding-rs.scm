(define-module (crates-io or e- ore-encoding-rs) #:use-module (crates-io))

(define-public crate-ore-encoding-rs-0.1.0 (c (n "ore-encoding-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0irj7x217z8dyhgydnv192fh93zrccq2v6ybg0v1va8v62ib2zwn")))

(define-public crate-ore-encoding-rs-0.23.3 (c (n "ore-encoding-rs") (v "0.23.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.9") (d #t) (k 0)))) (h "1hhnzm1p3x9whrf8y7qr30bqnkd4mmkc2d2bvwnww17bz3sab2l2")))

