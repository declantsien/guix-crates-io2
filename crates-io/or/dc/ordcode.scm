(define-module (crates-io or dc ordcode) #:use-module (crates-io))

(define-public crate-ordcode-0.2.2 (c (n "ordcode") (v "0.2.2") (d (list (d (n "serde") (r "1.*") (o #t) (k 0)) (d (n "serde_bytes") (r "0.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*") (d #t) (k 2)))) (h "1ag258diqqdhyvxy1vzcw58h0ndbk1rqwpfmd86allwvvqwckazl") (f (quote (("std" "serde/std") ("default" "std" "serde"))))))

