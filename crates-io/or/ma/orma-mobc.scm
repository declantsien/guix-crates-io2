(define-module (crates-io or ma orma-mobc) #:use-module (crates-io))

(define-public crate-orma-mobc-0.1.3 (c (n "orma-mobc") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "mobc") (r "^0.5.3") (d #t) (k 0)) (d (n "orma") (r "= 0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.10") (f (quote ("full"))) (d #t) (k 2)))) (h "0grw17qzv5j5zg32j5kbcjvimwijpkwj44kzgw5m4dr7yz52xmd1")))

(define-public crate-orma-mobc-0.1.4 (c (n "orma-mobc") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "mobc") (r "^0.5.3") (d #t) (k 0)) (d (n "orma") (r "= 0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "0nmxm3yszi69f6i1ba0im2sfmih1b27gfwg5ydk5azdiwb37dyhw")))

(define-public crate-orma-mobc-0.2.0 (c (n "orma-mobc") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "mobc") (r "^0.5.3") (d #t) (k 0)) (d (n "orma") (r "= 0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "1zz50w0i22hydpryghj5z1whd3g144jykmxqx3zbwxzs5m6x1hpm")))

(define-public crate-orma-mobc-0.3.0 (c (n "orma-mobc") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.29") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "mobc") (r "^0.5.7") (d #t) (k 0)) (d (n "orma") (r "= 0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("full"))) (d #t) (k 2)))) (h "0hfxbwcri21k6rsklhg3y6mlkz62giygdbfsj86456cg2ibbslmm")))

(define-public crate-orma-mobc-0.3.1 (c (n "orma-mobc") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "mobc") (r "^0.5.7") (d #t) (k 0)) (d (n "orma") (r "=0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1d45k9vlbwjxdyldsvljg96yvgmzak4fql0dxajimq49xyk1mzdg")))

(define-public crate-orma-mobc-0.3.2 (c (n "orma-mobc") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "mobc") (r "^0.5.12") (d #t) (k 0)) (d (n "orma") (r "=0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0nx6nb9ikjxw3d1av3fwnhiipni61c8nvybm9gcs8vp9b4y8m48j")))

(define-public crate-orma-mobc-0.3.3 (c (n "orma-mobc") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "mobc") (r "^0.5.12") (d #t) (k 0)) (d (n "orma") (r "=0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0mp1cbg0wi6cfblpw61z1ih8ymdc6bj5nsy2x2m073g58r9bagcz")))

