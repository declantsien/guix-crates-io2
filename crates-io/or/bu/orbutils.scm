(define-module (crates-io or bu orbutils) #:use-module (crates-io))

(define-public crate-orbutils-0.1.0 (c (n "orbutils") (v "0.1.0") (d (list (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "1sd6v39v867m1vysnlmx9kh9xl7i3kiqw7fyykc01kll9m2s49ws")))

(define-public crate-orbutils-0.1.1 (c (n "orbutils") (v "0.1.1") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "088lbqfgyh02283zzfzpzf5kdwfailg1ifbdgr1rc7marax3np4x")))

(define-public crate-orbutils-0.1.2 (c (n "orbutils") (v "0.1.2") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0lkfvf289pnbb1g71q3wjcj8prkkmznljgh8njmjbhz22682i6iv")))

(define-public crate-orbutils-0.1.3 (c (n "orbutils") (v "0.1.3") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0jx5q8lr4pmzl3pnvyz3bpwf1r2zjnacqqq8v96mkhn7r3n2c1g9")))

(define-public crate-orbutils-0.1.4 (c (n "orbutils") (v "0.1.4") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0riycsaj7q2rljrjld927d8lz4r99wb1q71g4bqjcr1rf8bvj0xw")))

(define-public crate-orbutils-0.1.5 (c (n "orbutils") (v "0.1.5") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "114lhxmn78yy5fxz72d690q73fhvpxyv9yz7w71f9plhsigv6g1k")))

(define-public crate-orbutils-0.1.6 (c (n "orbutils") (v "0.1.6") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0vvhdg3p9zj2is8fxypa7dgbdxy0qsyh3hz8fvp9pmiiiyiyxagw")))

(define-public crate-orbutils-0.1.7 (c (n "orbutils") (v "0.1.7") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0x7f9jqij14issjcny7rz2wv8g6862z178fmv838cpgrvb74wwn7")))

(define-public crate-orbutils-0.1.8 (c (n "orbutils") (v "0.1.8") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "1wqg99j8jmwnfl6diwq1z9p4wzwzq9xj08xqy12m90z1028q3xz2")))

(define-public crate-orbutils-0.1.9 (c (n "orbutils") (v "0.1.9") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "1ph3yip6pvzbfk4dfd00iv24610r84fjs2ssjd9bfg3k10liz8i3")))

(define-public crate-orbutils-0.1.10 (c (n "orbutils") (v "0.1.10") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbtk") (r "^0.1") (d #t) (k 0)))) (h "0k4w507q4zr3sdqcx0gs3x7p6apa6f9rpaqwagmg5gazw7s7jglb")))

