(define-module (crates-io or d_ ord_by_key) #:use-module (crates-io))

(define-public crate-ord_by_key-0.1.0 (c (n "ord_by_key") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18ixxrkyac657ddkhc319r5zqgrgpr12xxkw8sm1vpwg4pawykic") (y #t)))

(define-public crate-ord_by_key-0.1.1 (c (n "ord_by_key") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01smw90rxiy4cdila0sglf2w1ay45varxnjggji5ag03nnwbng6l") (y #t)))

(define-public crate-ord_by_key-0.1.2 (c (n "ord_by_key") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6zqwp977j0k94rq5kmavy6iqgcarr8w260d319x8by7fkdxakb")))

(define-public crate-ord_by_key-0.1.3 (c (n "ord_by_key") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "085xswdmhxypx71373z835zysyhbyjj3rgx4kdiy8gxl9svjdss2")))

