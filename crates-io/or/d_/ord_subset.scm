(define-module (crates-io or d_ ord_subset) #:use-module (crates-io))

(define-public crate-ord_subset-1.0.0 (c (n "ord_subset") (v "1.0.0") (h "1w2mhsr8klv787p1xb44pbgka2hcz3gcwblpghcv2drwdcbryq5b")))

(define-public crate-ord_subset-1.1.0 (c (n "ord_subset") (v "1.1.0") (h "0l5icd3myh71llz3029k07janj983jfwy4559lyny40krc8csmam")))

(define-public crate-ord_subset-1.2.0 (c (n "ord_subset") (v "1.2.0") (h "13xbdj13mqwhbkby6r60gxr625wq7g8gxayv1i5kv33qnkxk2001")))

(define-public crate-ord_subset-2.0.0 (c (n "ord_subset") (v "2.0.0") (h "1f7qy0d46lppbaap089bj8dwdvflyakfrh8gr1z0zixsn564x5bh")))

(define-public crate-ord_subset-2.1.0 (c (n "ord_subset") (v "2.1.0") (h "1s050kgy8v3lvabcbkkin2hhin1pbnjj0kr9r04wcjw2fp1bif5x")))

(define-public crate-ord_subset-3.0.0 (c (n "ord_subset") (v "3.0.0") (h "0qyh5b3yh3p9w3y2fs3l4s3p2f28awmkl1r7wwr0sbk60brvrprx") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-ord_subset-3.1.0 (c (n "ord_subset") (v "3.1.0") (h "0h0w6a8wj4qinnddbqdak99jaygz32wn03skxnxabqbn8kifcrdw") (f (quote (("unstable") ("unchecked_ops" "ops") ("std") ("ops") ("default" "std"))))))

(define-public crate-ord_subset-3.1.1 (c (n "ord_subset") (v "3.1.1") (h "1vvb6zmz279nb59dki7kbsvixbk8zpg2gxvgcpsjfnxg9ik19knp") (f (quote (("unstable") ("unchecked_ops" "ops") ("std") ("ops") ("default" "std"))))))

