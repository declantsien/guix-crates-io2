(define-module (crates-io or d_ ord_by) #:use-module (crates-io))

(define-public crate-ord_by-0.1.0 (c (n "ord_by") (v "0.1.0") (h "0f395cfjbh23i489ai1h2mivax45kn9hrhj13h0pb90va5kxgyw7")))

(define-public crate-ord_by-0.1.1 (c (n "ord_by") (v "0.1.1") (h "0g9y91blqznql4izpm1idrwiyv89kqxl9nlc56xhh1gv90v9j3vb")))

