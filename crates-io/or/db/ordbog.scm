(define-module (crates-io or db ordbog) #:use-module (crates-io))

(define-public crate-ordbog-0.1.0 (c (n "ordbog") (v "0.1.0") (d (list (d (n "float-ord") (r "^0.3.1") (d #t) (k 0)) (d (n "float_next_after") (r "^0.1.5") (d #t) (k 2)) (d (n "http_req") (r "^0.7.2") (d #t) (k 2)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "zip") (r "^0.5.12") (d #t) (k 2)))) (h "1j5ijyi8b3ngvjpziikkhalwsfqcn6bm6mbjmkliawfny2jx6r32")))

