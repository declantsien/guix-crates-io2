(define-module (crates-io or io orion-cli) #:use-module (crates-io))

(define-public crate-orion-cli-0.1.0-alpha (c (n "orion-cli") (v "0.1.0-alpha") (h "0yiw02485r37hp1rzznjisw61bfd2szdi2c6dy15d2q3p3zq9lhz") (y #t)))

(define-public crate-orion-cli-0.1.1-alpha (c (n "orion-cli") (v "0.1.1-alpha") (h "13r64dgl5k3byyri7n07sq8g23c2vzf1cwq7c1cqkzri09khipnj") (y #t)))

(define-public crate-orion-cli-0.1.0 (c (n "orion-cli") (v "0.1.0") (d (list (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "1b9niif5h9qpn3a5aj98r7dcg6q8j85w4k843ldkr58mrf6mff94") (y #t)))

