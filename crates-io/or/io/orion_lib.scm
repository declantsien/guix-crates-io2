(define-module (crates-io or io orion_lib) #:use-module (crates-io))

(define-public crate-orion_lib-0.1.0 (c (n "orion_lib") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "04cv22y8xvir395nb2knlmvfs8ca95lifyszrsn62vir900i4izi")))

(define-public crate-orion_lib-1.0.0 (c (n "orion_lib") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)))) (h "0kn85wn2a11abmpl9y8zfid7nwx5hfwrvj4mclgjfx706zbp23l2")))

(define-public crate-orion_lib-2.0.0 (c (n "orion_lib") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)))) (h "1g6ka86875z9n42iqw91sx8mvns3yvrl1hy9mlbda9k56vz7ski3")))

(define-public crate-orion_lib-3.0.0 (c (n "orion_lib") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)))) (h "0jycdwm2axz0y42gnbp1n36byzzsrmd4mid7724j6hx4jfw74mwk")))

(define-public crate-orion_lib-4.0.0 (c (n "orion_lib") (v "4.0.0") (d (list (d (n "logos") (r "^0.14.0") (d #t) (k 0)))) (h "0qnrvivp99khf98mjcwjvq71538w2wap634ccdkahqb0as6ykzl3")))

(define-public crate-orion_lib-5.0.0 (c (n "orion_lib") (v "5.0.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 1)) (d (n "lalrpop") (r "^0.20.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.2") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1dq7pb2vx9s0s305l60cbvmizazsclq2hd9z80ynhj018cp2x3k7")))

(define-public crate-orion_lib-6.0.0 (c (n "orion_lib") (v "6.0.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lalrpop") (r "^0.20.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.2") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0h49f1w1x196f00rbc4jdxzk834f4039250k8bjiab03c025cxw7")))

