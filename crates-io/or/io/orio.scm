(define-module (crates-io or io orio) #:use-module (crates-io))

(define-public crate-orio-0.1.0 (c (n "orio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1ikd0nr9j5lf33hz982qllmvsglck4r1yr0ymjy2rmbi8162xa7n") (f (quote (("derive" "orio-derive") ("default" "derive"))))))

(define-public crate-orio-0.2.0 (c (n "orio") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0x8ba1qq0vfrvkr704wyhn4z863lqlbwzyv536jyvbd9ndnadhnw") (f (quote (("derive" "orio-derive") ("default" "derive"))))))

(define-public crate-orio-0.3.0 (c (n "orio") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "19kfh749k92zf0wpr747b668hr26jasx5lqrfydx856z753pm185") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.3.1 (c (n "orio") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1sy5lr7lad6h4fwmkmr9ix7pi9j2f031d5sjd6lkd5vwaw15nzdl") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.4.0 (c (n "orio") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0pac5an6wz211cgvnk52vxmssix4q4agf7zc061kij4gb8nwkb02") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.4.1 (c (n "orio") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "09i7zirmka7w8ciw2aksywmdf284n8d7ck4hb3xxyid6pfqh7nd1") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.4.2 (c (n "orio") (v "0.4.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "058jq19ipwfbdkbsicfrcgjb5bhkkrgbnxi34hw9g54avmfl7cc6") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.5.0 (c (n "orio") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "15ja4215k28q6wjxj20ssgghpiwywd35i83603vjqf8kxfdn5s7g") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.5.1 (c (n "orio") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1yv2mg4gfvbpw6157gyi0kxkixchb2npghmr947p07jrzmcxddj9") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.5.2 (c (n "orio") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1vr9xnhcakmpq8mmfxcrxanjpnd9sg9rjzqfp7z1z098nqpasxnv") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

(define-public crate-orio-0.5.3 (c (n "orio") (v "0.5.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "orio-derive") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "00bqz3k55p8b6p82r5x7g7jbvbvdpd2imhdck6kxvznk71lk7nkq") (f (quote (("usize") ("derive" "orio-derive") ("default" "derive" "usize"))))))

