(define-module (crates-io or io orion_cfmt) #:use-module (crates-io))

(define-public crate-orion_cfmt-0.1.0 (c (n "orion_cfmt") (v "0.1.0") (d (list (d (n "cfmt-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1bd8c91lgcz9spldln2xhpxlgm33m102zh1iyqc666l90qx3mj7m")))

(define-public crate-orion_cfmt-0.1.1 (c (n "orion_cfmt") (v "0.1.1") (d (list (d (n "cfmt-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1f7jqgpb6yinxx1zyhfbz64z4fsa5rxyfymk2y3dqdjk8dmdgm93")))

(define-public crate-orion_cfmt-0.1.2 (c (n "orion_cfmt") (v "0.1.2") (d (list (d (n "cfmt-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1w4hw11qy7gsmgva2zjpq7lsy7dsbg18j978xhn17xybfcg6f3sb")))

