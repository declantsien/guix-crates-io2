(define-module (crates-io or io orion-async) #:use-module (crates-io))

(define-public crate-orion-async-0.1.0 (c (n "orion-async") (v "0.1.0") (d (list (d (n "orion-async-macros") (r "^0.1") (d #t) (k 0)))) (h "06qvscdz82dhkh8wijaar5x3bhdlc5m0z2py6ckkkr240w6c8y8w")))

(define-public crate-orion-async-0.1.1 (c (n "orion-async") (v "0.1.1") (d (list (d (n "orion-async-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0fmyfx1qcrb7p3l7b1pwm420pc36w00jfq1d1xmgzrkdy06zrl1d")))

