(define-module (crates-io or io orion-async-macros) #:use-module (crates-io))

(define-public crate-orion-async-macros-0.1.0 (c (n "orion-async-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kq1kqb50zy8csrk0fmi28bj030ax3x6l0qqp19jp2x69qjc5n2c")))

(define-public crate-orion-async-macros-0.1.1 (c (n "orion-async-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01yzfwil4psiwqzk91qyrpi1dfssmpfa9cmbbd9nvqcargmm36sx")))

