(define-module (crates-io or io orio-derive) #:use-module (crates-io))

(define-public crate-orio-derive-0.1.0 (c (n "orio-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zd3s9a4ypk423i33ybli2p5nlsram0n7j5n963b87phks7wf444")))

(define-public crate-orio-derive-0.1.1 (c (n "orio-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1irjdzzvi6n0l547z183qj4shdyqlsb9k07h7vv4fgkzwyka5404")))

(define-public crate-orio-derive-0.2.0 (c (n "orio-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jd7ry99mmv7mn01fpva3ahy6ivssagxkn7s9nspl5raj23cwvcy")))

(define-public crate-orio-derive-0.3.0 (c (n "orio-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ik53brrs5qxzfw5pw70y7xwhxcgddnhi6imf790z4ja88n9zpz1")))

(define-public crate-orio-derive-0.3.1 (c (n "orio-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zsb86rpliv9m7kspq9g1vgsr0shmkjbv9p0jlp6bwb83gabfqpf")))

(define-public crate-orio-derive-0.4.0 (c (n "orio-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rwzyvzlpqabcxrvhhrcfk5y0jb79jlkvmmz96ls7qnsxa5gqb4b")))

(define-public crate-orio-derive-0.5.1 (c (n "orio-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yfmbyzqxallqc55bdzcxsgb0ch497cy30sv1544czmnv42ys4sr")))

