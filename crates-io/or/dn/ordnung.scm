(define-module (crates-io or dn ordnung) #:use-module (crates-io))

(define-public crate-ordnung-0.0.0 (c (n "ordnung") (v "0.0.0") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)))) (h "0npqb07q3f3lyq8qclzlwcxy059lyk4r4p24kj21sd3bljzbyvcs")))

(define-public crate-ordnung-0.0.1 (c (n "ordnung") (v "0.0.1") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 2)))) (h "0lan4si2ah1w13g6rkpqq587saal5ypsi8clq4msg6h182i4352y")))

