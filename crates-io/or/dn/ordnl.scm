(define-module (crates-io or dn ordnl) #:use-module (crates-io))

(define-public crate-ordnl-1.0.0 (c (n "ordnl") (v "1.0.0") (h "1g1k8yj493mhhi1my9kbvd6wa9yndl7zvzwbqk5k9igq3zqi714z")))

(define-public crate-ordnl-1.0.1 (c (n "ordnl") (v "1.0.1") (h "00cqh4q7avr3srmln6qmn25jyj53irm7y4h786c9pzd66clvyrvk")))

(define-public crate-ordnl-1.0.2 (c (n "ordnl") (v "1.0.2") (h "1gdfn1j5jnrlhlrazrzbvwhc5mxgixqww6snlran4qvbi7f0z4ps")))

(define-public crate-ordnl-1.0.3 (c (n "ordnl") (v "1.0.3") (h "17z05dlyaywcjp2431x661f94phv08lldc7cqlvnx474j2y3lmbh")))

