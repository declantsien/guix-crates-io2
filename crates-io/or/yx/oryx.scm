(define-module (crates-io or yx oryx) #:use-module (crates-io))

(define-public crate-oryx-0.1.0 (c (n "oryx") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "simpler_timer") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "09c3b2904lgh6p5sraqhrbzivdgjfvm6ka444yl9jidjkdx1lf45")))

