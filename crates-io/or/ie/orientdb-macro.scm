(define-module (crates-io or ie orientdb-macro) #:use-module (crates-io))

(define-public crate-orientdb-macro-0.1.0 (c (n "orientdb-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwaj88pjlarhp20w7178q8rlgbvziyfik349lj8cawhin35yf56")))

(define-public crate-orientdb-macro-0.2.0 (c (n "orientdb-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0lqclh3v9i2w71xzykm99ch4wb22nsdzhfmv1knqy41hql5yvw10")))

