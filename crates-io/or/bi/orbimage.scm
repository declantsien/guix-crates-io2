(define-module (crates-io or bi orbimage) #:use-module (crates-io))

(define-public crate-orbimage-0.1.2 (c (n "orbimage") (v "0.1.2") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0icg8hkqdgpvxavz4pva2m7nvkrcc8xvjjwyczj7k3b1f62vnrj5") (f (quote (("default" "png"))))))

(define-public crate-orbimage-0.1.3 (c (n "orbimage") (v "0.1.3") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "004vja76z7cbfbh3asdf9wzpwb1lgxybh7l0fqk2rh00a97kxsdl") (f (quote (("default" "png"))))))

(define-public crate-orbimage-0.1.4 (c (n "orbimage") (v "0.1.4") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0yyg1j9nf2cwndzcm15dikims9k7qfg83v9v5177dbgrrlpl0yb4") (f (quote (("default" "png"))))))

(define-public crate-orbimage-0.1.5 (c (n "orbimage") (v "0.1.5") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0zji7xas4sskff8n2y76qjq08w91asrm3s32lqzrk2mxmq037dzq") (f (quote (("default" "png"))))))

(define-public crate-orbimage-0.1.6 (c (n "orbimage") (v "0.1.6") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0lyqwjr1pfv22hlxlg9np2n9cgdvgfryvgqvi929li64626fmnvx") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.7 (c (n "orbimage") (v "0.1.7") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "1dbz280bqvma4m7jzl9bpk5shjqn5kz71sq7cnvr5jvwcn0d5vy9") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.8 (c (n "orbimage") (v "0.1.8") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "116rwdjicylj7azif1bmw2a5rcs52lv4p0ly7x1rggsmaf2agrbp") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.9 (c (n "orbimage") (v "0.1.9") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0cja5i172i0a0l03x3q706ckk58qz4zgwl2zmw108jc21g51pa2b") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.10 (c (n "orbimage") (v "0.1.10") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0asc6iax25xjvlmb0kqc0v9hrv5xjrgy7crghfv47vvjlffpl5s2") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.11 (c (n "orbimage") (v "0.1.11") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)))) (h "0z6yzg1nmaq2m945bnpjf6fq0jmkrwfd3sgkrfxwhc3fb93k3l80") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.12 (c (n "orbimage") (v "0.1.12") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)) (d (n "resize") (r "^0.2") (d #t) (k 0)))) (h "0kvvk9qb0szw711ic4p8fpkd73mcqp5dsbjlrjpn5gi799k3imhx") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.13 (c (n "orbimage") (v "0.1.13") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)) (d (n "resize") (r "^0.2") (d #t) (k 0)))) (h "16xf4z45wm8vm2hd5av2nszyk5jsr76k2j3kxsqwh3ikhvwv4zvv") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.14 (c (n "orbimage") (v "0.1.14") (d (list (d (n "jpeg-decoder") (r "^0.1") (o #t) (k 0)) (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.5") (o #t) (k 0)) (d (n "resize") (r "^0.2") (d #t) (k 0)))) (h "06mmb8r3q8qi5z2n6lafn5cvf5w26p8almrn5grrldl82nha5wsv") (f (quote (("jpg" "jpeg-decoder") ("default" "bmp" "jpg" "png") ("bmp"))))))

(define-public crate-orbimage-0.1.15 (c (n "orbimage") (v "0.1.15") (d (list (d (n "image") (r "^0.12") (d #t) (k 0)) (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "resize") (r "^0.2") (d #t) (k 0)))) (h "0vqsh772v9a2f90ayi25n6bxdcm23rdmkywxjiicb8zpgrh33c63")))

(define-public crate-orbimage-0.1.16 (c (n "orbimage") (v "0.1.16") (d (list (d (n "image") (r "^0.12") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.13") (d #t) (k 0)) (d (n "resize") (r "^0.2") (d #t) (k 0)))) (h "1qbwjx3n7npvj8idmn36ac4j54cj35nkjmbhm0jk6b0cdh2g982b")))

(define-public crate-orbimage-0.1.17 (c (n "orbimage") (v "0.1.17") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.20") (d #t) (k 0)) (d (n "resize") (r "^0.3.0") (d #t) (k 0)))) (h "09647133s2xzkm0vxfcn96rkd2a0rvmnfahjpw9a75xp2ab3zzir")))

