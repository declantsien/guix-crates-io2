(define-module (crates-io or bi orbitc) #:use-module (crates-io))

(define-public crate-orbitc-0.1.0 (c (n "orbitc") (v "0.1.0") (d (list (d (n "c-emit") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0q2zi72q40ybz1q7d8ran5i78vn7200sf9i1g3vc5xfbqrg79xp0")))

(define-public crate-orbitc-0.2.0 (c (n "orbitc") (v "0.2.0") (d (list (d (n "c-emit") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1mzzfs99x5sgnjmn6pqz2z1dqjb0xajr2q4gxpm0ncdnwmkib7hh")))

(define-public crate-orbitc-0.3.0 (c (n "orbitc") (v "0.3.0") (d (list (d (n "c-emit") (r "^1.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1f4dg3gjpv55zb7cmddvpgpiv6kfdxdjar7315pj9k72smcai3yd")))

(define-public crate-orbitc-1.0.0 (c (n "orbitc") (v "1.0.0") (d (list (d (n "c-emit") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1b9yswzzspxw4x9px90q3m6s96p235sj29apxfq2z3nrzxrinagh")))

(define-public crate-orbitc-1.1.0 (c (n "orbitc") (v "1.1.0") (d (list (d (n "c-emit") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0f50n0ik76h3z0ig77vak61d6fwa2csxa8sal189dis0ibda2d5y")))

