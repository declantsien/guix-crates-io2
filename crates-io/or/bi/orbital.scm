(define-module (crates-io or bi orbital) #:use-module (crates-io))

(define-public crate-orbital-0.1.0 (c (n "orbital") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10bwm6mp2yr5ny3b3hjja7r7cgrm9sn9an4h91ixvkkw79i60lsa")))

