(define-module (crates-io or bi orbis_encoder) #:use-module (crates-io))

(define-public crate-orbis_encoder-0.1.0 (c (n "orbis_encoder") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p19hycwaschdc2k01vnj7faqxn2ccznkq5460cx5mkdmrrpxcc2")))

