(define-module (crates-io or bt orbtk-proc-macros) #:use-module (crates-io))

(define-public crate-orbtk-proc-macros-0.3.0-alpha1 (c (n "orbtk-proc-macros") (v "0.3.0-alpha1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "15g62lywqpc6i92niiyr10l3b6kjpp7v97dmjv34i1xg1m0gc373") (f (quote (("debug"))))))

(define-public crate-orbtk-proc-macros-0.3.1-alpha2 (c (n "orbtk-proc-macros") (v "0.3.1-alpha2") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "02nb5z1l2mn63lyzhy20dmzhdka5ifym2swiaznwz6yn06bbvaxz") (f (quote (("debug"))))))

(define-public crate-orbtk-proc-macros-0.3.1-alpha3 (c (n "orbtk-proc-macros") (v "0.3.1-alpha3") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rza9kfvvbkphjrsp6jwma5zbvj3xq3ikkic85zcs51m5i3smgvc") (f (quote (("debug"))))))

(define-public crate-orbtk-proc-macros-0.3.1-alpha4 (c (n "orbtk-proc-macros") (v "0.3.1-alpha4") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03qmi4wwri7cx6h540zyqfbazrqqm20iw2wk37d0ybajrlljx5b7") (f (quote (("debug"))))))

