(define-module (crates-io or bt orbtk-simple-modal) #:use-module (crates-io))

(define-public crate-orbtk-simple-modal-0.1.0 (c (n "orbtk-simple-modal") (v "0.1.0") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbtk") (r "^0.2") (d #t) (k 0)))) (h "1mwac9ggxgb2dx2b6fax9imwmfisvk06p5n4h2p255f70wg5and6")))

