(define-module (crates-io or bt orbtk-tree) #:use-module (crates-io))

(define-public crate-orbtk-tree-0.3.0-alpha1 (c (n "orbtk-tree") (v "0.3.0-alpha1") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)))) (h "189jmvq7li69fnf98dlgalhnmlq74bc4b5rnwxpb4bx9lk755k21")))

(define-public crate-orbtk-tree-0.3.1-alpha2 (c (n "orbtk-tree") (v "0.3.1-alpha2") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)))) (h "176b9hs2m5v8wqr50vy5b50lh6nwxlgnlpdnzz1dq96vyb6lcgh8")))

(define-public crate-orbtk-tree-0.3.1-alpha3 (c (n "orbtk-tree") (v "0.3.1-alpha3") (d (list (d (n "dces") (r "^0.3") (d #t) (k 0)))) (h "1j1vb49dmsi5kgxqdzk4p95x3vm753fh23w6acrharlr3cg0awsb")))

