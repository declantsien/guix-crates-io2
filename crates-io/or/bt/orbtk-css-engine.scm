(define-module (crates-io or bt orbtk-css-engine) #:use-module (crates-io))

(define-public crate-orbtk-css-engine-0.3.0-alpha1 (c (n "orbtk-css-engine") (v "0.3.0-alpha1") (d (list (d (n "cssparser") (r "^0.17.0") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.0-alpha1") (d #t) (k 0)))) (h "1jxwj0wcbv1a03vig9aqy00gn64fhnv5l3jvvy8v9pdc4pk51cwq")))

(define-public crate-orbtk-css-engine-0.3.1-alpha2 (c (n "orbtk-css-engine") (v "0.3.1-alpha2") (d (list (d (n "cssparser") (r "^0.17.0") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha2") (d #t) (k 0)))) (h "069fkwi32cs2nna5mghijha29p3aamkalx24sfsnkcmvsdm49lib")))

