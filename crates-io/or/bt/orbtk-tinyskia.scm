(define-module (crates-io or bt orbtk-tinyskia) #:use-module (crates-io))

(define-public crate-orbtk-tinyskia-0.3.1-alpha4 (c (n "orbtk-tinyskia") (v "0.3.1-alpha4") (d (list (d (n "image") (r "^0.23") (f (quote ("ico"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha4") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "smallvec") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tiny-skia") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1p25z92bs6zsp3fbzgkc6f2g41i5na53fkjsvlpqr115b50vfkai")))

