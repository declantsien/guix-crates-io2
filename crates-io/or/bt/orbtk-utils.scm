(define-module (crates-io or bt orbtk-utils) #:use-module (crates-io))

(define-public crate-orbtk-utils-0.3.0-alpha1 (c (n "orbtk-utils") (v "0.3.0-alpha1") (h "1hnxx95gwgw54nfp2k02wqahcxk5ll0p7kw7gl02diq0rkkficsv")))

(define-public crate-orbtk-utils-0.3.1-alpha2 (c (n "orbtk-utils") (v "0.3.1-alpha2") (h "1phvv9d586c33sa6k7rzvrbn7wwqqy8fi9q8b5kxbq7p1kp4vqzj")))

(define-public crate-orbtk-utils-0.3.1-alpha3 (c (n "orbtk-utils") (v "0.3.1-alpha3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0i8b62fj8kinw3jzp9wsfqfl7142l22zijhmvb67ywdcgcqlvqgy")))

(define-public crate-orbtk-utils-0.3.1-alpha4 (c (n "orbtk-utils") (v "0.3.1-alpha4") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("add" "constructor" "from" "mul"))) (k 0)) (d (n "lexical-core") (r "^0.7") (d #t) (k 0)) (d (n "phf") (r "^0.8") (k 0)) (d (n "phf_codegen") (r "^0.8") (k 1)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yz9h3xbasxpam81bp7v2hq9nxfsms38grxqkhz8rm6lan8bjdv3")))

