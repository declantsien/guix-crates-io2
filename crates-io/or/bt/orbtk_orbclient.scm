(define-module (crates-io or bt orbtk_orbclient) #:use-module (crates-io))

(define-public crate-orbtk_orbclient-0.3.1-alpha4 (c (n "orbtk_orbclient") (v "0.3.1-alpha4") (d (list (d (n "image") (r "^0.23") (f (quote ("ico"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.32") (k 0)) (d (n "orbtk-tinyskia") (r "^0.3.1-alpha4") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle"))) (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "1c6sknkqrv0zl3rvf5id0bnr2x8naqiq3agl22ykf79j6xfs7bxg") (f (quote (("log") ("bundled" "sdl2/bundled" "sdl2/static-link" "orbclient/bundled"))))))

