(define-module (crates-io or bt orbtk-theming) #:use-module (crates-io))

(define-public crate-orbtk-theming-0.3.1-alpha3 (c (n "orbtk-theming") (v "0.3.1-alpha3") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gnnsglf5xncif70mp6x1i9587icsc6f2dxn0xy3nik261xwp4fz")))

