(define-module (crates-io or bt orbtk) #:use-module (crates-io))

(define-public crate-orbtk-0.0.1 (c (n "orbtk") (v "0.0.1") (d (list (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (k 0)))) (h "04hkw2sw4lxjdzgclzmfc5glg05i6265sd8w9wz27185xxgf8kfc")))

(define-public crate-orbtk-0.0.2 (c (n "orbtk") (v "0.0.2") (d (list (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (k 0)))) (h "0rq4lqxxidxwb791njjfw6w5vm8031gxgd218c2xqif10403jwqq")))

(define-public crate-orbtk-0.0.3 (c (n "orbtk") (v "0.0.3") (d (list (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (k 0)))) (h "1cry6jzshqs9nz0rnsjbm3p30jcjlzqjfsia4lasmjlpyf9y61qi")))

(define-public crate-orbtk-0.0.4 (c (n "orbtk") (v "0.0.4") (d (list (d (n "sdl2") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)))) (h "0b0bijx241db0sl9z6zv2krwm2sf7wac2i794wmfaf5yrag1qfhz")))

(define-public crate-orbtk-0.0.5 (c (n "orbtk") (v "0.0.5") (d (list (d (n "sdl2") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)))) (h "136r1alhlfnmkm1nx36yrldyi5c8r8xkhll288pgmzlzc7af36jj")))

(define-public crate-orbtk-0.0.6 (c (n "orbtk") (v "0.0.6") (d (list (d (n "sdl2") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)))) (h "17qrd9wmc6jpvh6555bklgcxnyhn2awvj987bfdgmkh08bppirlp")))

(define-public crate-orbtk-0.0.7 (c (n "orbtk") (v "0.0.7") (d (list (d (n "orbclient") (r "^0.0.1") (d #t) (k 0)))) (h "1kbprnnh1qpfn15nmxw4bamr70np1rlaaz1155iz0q46rs8r4lwk")))

(define-public crate-orbtk-0.1.0 (c (n "orbtk") (v "0.1.0") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)))) (h "01l2nkg6w5b59mgz0763b6r08qk156xnjb5w052fnz32w04v45nw")))

(define-public crate-orbtk-0.1.1 (c (n "orbtk") (v "0.1.1") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)))) (h "18x2v07x6567pbwlffpmifb9hn57l5lznqkwvvlyq44d9w9rga67")))

(define-public crate-orbtk-0.1.2 (c (n "orbtk") (v "0.1.2") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)))) (h "11py5jq8y1qx771rqiipw6f4qmvyywxsw9wsx5b7pbq5kd4naq10")))

(define-public crate-orbtk-0.1.5 (c (n "orbtk") (v "0.1.5") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0hmj9f9hkzvp4k8wznil1lw3g6wg345aqhvdpi70nbvi5gxbj225")))

(define-public crate-orbtk-0.1.6 (c (n "orbtk") (v "0.1.6") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1paxr02vq80y4r5vmp4g7f6w02xx8ify87p02bjq1gf9qzkcqlwb")))

(define-public crate-orbtk-0.1.7 (c (n "orbtk") (v "0.1.7") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "04yj2ray7kvc2jmi2cyzwk06ijc6h9xsna96k41vfarqfmxay9hi")))

(define-public crate-orbtk-0.1.8 (c (n "orbtk") (v "0.1.8") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0lch2gd1d5mdcgkxsn0g91nb9pf8f4xj6jyyzhr6ha2zxszbwpbc")))

(define-public crate-orbtk-0.1.9 (c (n "orbtk") (v "0.1.9") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "02ykwllwfni1c7mmqv9r6kfv1hb3hbd4i0m9xc2555341b5zr9yf")))

(define-public crate-orbtk-0.2.0 (c (n "orbtk") (v "0.2.0") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0831ps2jp7iql52b1j1mgid8kfilkdsacb280687r4k38j7fgsbj")))

(define-public crate-orbtk-0.2.1 (c (n "orbtk") (v "0.2.1") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "03c3vr7crkwixxqsrianfkfdhr0nx1k2gzjg6js04smx58vjhm3v")))

(define-public crate-orbtk-0.2.2 (c (n "orbtk") (v "0.2.2") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0k5i0l1w3gq0wrmr55604ssh2qmkp93yl245v3hcmgkx2903q9kg")))

(define-public crate-orbtk-0.2.3 (c (n "orbtk") (v "0.2.3") (d (list (d (n "orbclient") (r "^0.1") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1lfmf4gv25iqhv7i1smfjvzjzpdkp225bx1k9f9iskpk75mf5f6l")))

(define-public crate-orbtk-0.2.4 (c (n "orbtk") (v "0.2.4") (d (list (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "062sxab2vcg9sljbfpbk57kg4nj0ni472y8f68z7w6c8yd8hslvw")))

(define-public crate-orbtk-0.2.5 (c (n "orbtk") (v "0.2.5") (d (list (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1awdbziav3493kx994nd6mfvdrpd8qv6j26416l81fjhylc8rww5")))

(define-public crate-orbtk-0.2.6 (c (n "orbtk") (v "0.2.6") (d (list (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0jsh8rrzasn9y40qqz55n7ais3kkapvnnz9z2slzfrjrww0zfmid")))

(define-public crate-orbtk-0.2.7 (c (n "orbtk") (v "0.2.7") (d (list (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1bipq0m8mi8na954jzjid8rg2vvxkcjhbixs9gvbq0gczfyr9mra")))

(define-public crate-orbtk-0.2.8 (c (n "orbtk") (v "0.2.8") (d (list (d (n "orbclient") (r "^0.2") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0jymd38xzsn1mpngx5vifz8ydy5lnmb7wc0m9z3idd2nwk6lzzf6")))

(define-public crate-orbtk-0.2.9 (c (n "orbtk") (v "0.2.9") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "117i15y8h1sxrpl1n61s46fnf6ldjn781fakwwipnhsd237kk6d0")))

(define-public crate-orbtk-0.2.10 (c (n "orbtk") (v "0.2.10") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "02xsk2ipqs8b9fymhfzkdcvk1k58y1vxgg2w72ccqwspcrkhqfnh")))

(define-public crate-orbtk-0.2.11 (c (n "orbtk") (v "0.2.11") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "09545ifdkk8fyhs6zw6pfxawd45hq1qjwmakzynyk1pwan1dg2yj")))

(define-public crate-orbtk-0.2.12 (c (n "orbtk") (v "0.2.12") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "10ds1c24azfagy7p3b0mdggqa1q4bclajnssq36mvk33hn7cc5sx")))

(define-public crate-orbtk-0.2.14 (c (n "orbtk") (v "0.2.14") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1fws0m048l46h577bccb8vl5ybmibh6f7irzmdppcx5nzij8r0v3")))

(define-public crate-orbtk-0.2.15 (c (n "orbtk") (v "0.2.15") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0c7s31xicc3klljw1psf54695p7bv22fm3claf8p7hpvxcfwg6r7")))

(define-public crate-orbtk-0.2.16 (c (n "orbtk") (v "0.2.16") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1x149ac960s9vix0fmkhkb30npavf1l2rcc84jfcfmk6x201cpv2")))

(define-public crate-orbtk-0.2.17 (c (n "orbtk") (v "0.2.17") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "0np9qjicy518fih8arqi2r3hw4j4hb1iar4irfx3lw37gbymwby0")))

(define-public crate-orbtk-0.2.18 (c (n "orbtk") (v "0.2.18") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "04b9yz2g8y1z6pi83fzgfgrh2mcp5i5pfx6kg4shc6ja7w2qc0xf")))

(define-public crate-orbtk-0.2.19 (c (n "orbtk") (v "0.2.19") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1wgr3clba9p07i340n87d77ayqxri6ccfvfj5n97l43ld2n51jib")))

(define-public crate-orbtk-0.2.20 (c (n "orbtk") (v "0.2.20") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1zs59w3d505gynaf9f5qydcal74pg6hk2g3wdzwrnf4yd2661x8r")))

(define-public crate-orbtk-0.2.21 (c (n "orbtk") (v "0.2.21") (d (list (d (n "orbclient") (r "^0.3") (d #t) (k 0)) (d (n "orbfont") (r "^0.1") (d #t) (k 0)) (d (n "orbimage") (r "^0.1") (d #t) (k 0)))) (h "1g1r92jg7690856r2w67r5wpi7if47xi4071fdh4ka5lf7jwqsfx")))

(define-public crate-orbtk-0.2.22 (c (n "orbtk") (v "0.2.22") (d (list (d (n "orbclient") (r "^0.3.11") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.15") (d #t) (k 0)))) (h "14d6qjzf4f36h1y0m55rg92wy0pb8w6li5gs56k41k406n3a9dn9")))

(define-public crate-orbtk-0.2.23 (c (n "orbtk") (v "0.2.23") (d (list (d (n "orbclient") (r "^0.3.11") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.15") (d #t) (k 0)))) (h "140cyyvchbriid99f0hn2j7r1g9jdyrm5fmsp675mjfw085apcp2")))

(define-public crate-orbtk-0.2.24 (c (n "orbtk") (v "0.2.24") (d (list (d (n "orbclient") (r "^0.3.11") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.15") (d #t) (k 0)))) (h "0fn4cawp49zc71z3hqrbzgln8a4v9chpix0ax3pym2n1zf3z0606")))

(define-public crate-orbtk-0.2.25 (c (n "orbtk") (v "0.2.25") (d (list (d (n "orbclient") (r "^0.3.11") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.15") (d #t) (k 0)))) (h "06wv3by1n7y8ra1minbniciavkw4z9cnyh462h5baw83qpmkmvw9")))

(define-public crate-orbtk-0.2.26 (c (n "orbtk") (v "0.2.26") (d (list (d (n "orbclient") (r "^0.3.11") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.15") (d #t) (k 0)))) (h "0hz44abmbn8krhxkjxv24hvc9bimp9llas9ay54gbzdqc04viphz")))

(define-public crate-orbtk-0.2.27 (c (n "orbtk") (v "0.2.27") (d (list (d (n "cssparser") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.13") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.16") (d #t) (k 0)))) (h "0my2215b5havphj89gh4q7a9zil0ylk5x1dxryy3c9hylbwph0q4")))

(define-public crate-orbtk-0.2.28 (c (n "orbtk") (v "0.2.28") (d (list (d (n "cssparser") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.13") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.16") (d #t) (k 0)))) (h "1zbw8hflyfnf7hpacrnjrbx298nccfb4zihgfjy9wiy1c8fwiifl")))

(define-public crate-orbtk-0.2.29 (c (n "orbtk") (v "0.2.29") (d (list (d (n "cssparser") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.13") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.16") (d #t) (k 0)))) (h "115nr8j36h4vhhqsfzz7jkqnr79xp5bjc5wnznqs0iacq8isk76k")))

(define-public crate-orbtk-0.3.0 (c (n "orbtk") (v "0.3.0") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)) (d (n "euc") (r "^0.4.2") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "vek") (r "^0.9.10") (d #t) (k 2)))) (h "0ksh5si5cnypiavk30x5cq4zix8h48airnhdxxh778m37cy7w309") (f (quote (("debug" "orbtk-api/debug")))) (y #t)))

(define-public crate-orbtk-0.3.0-alpha-1 (c (n "orbtk") (v "0.3.0-alpha-1") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)) (d (n "euc") (r "^0.4.2") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "vek") (r "^0.9.10") (d #t) (k 2)))) (h "0r956jnjwkw0yy2q9glc5w3xb86wl5a8pca3x44g8wf2dinvlam9") (f (quote (("debug" "orbtk-api/debug")))) (y #t)))

(define-public crate-orbtk-0.3.1-alpha-1 (c (n "orbtk") (v "0.3.1-alpha-1") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)) (d (n "euc") (r "^0.4.2") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "vek") (r "^0.9.10") (d #t) (k 2)))) (h "0rwsdcw5w4ly7ziz098hb3lij7gfmrimv7rdkhqhnx1p7khpj41g") (f (quote (("debug" "orbtk-api/debug")))) (y #t)))

(define-public crate-orbtk-0.3.1-alpha1 (c (n "orbtk") (v "0.3.1-alpha1") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)) (d (n "euc") (r "^0.4.2") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.0-alpha1") (d #t) (k 0)) (d (n "vek") (r "^0.9.10") (d #t) (k 2)))) (h "0l356qw7f1fyb5yflibfx48hbzsji3jba4vw4mr65a07afqkfgfs") (f (quote (("debug" "orbtk-api/debug"))))))

(define-public crate-orbtk-0.2.31 (c (n "orbtk") (v "0.2.31") (d (list (d (n "cssparser") (r "^0.16.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.27") (d #t) (k 0)) (d (n "orbfont") (r "^0.1.8") (d #t) (k 0)) (d (n "orbimage") (r "^0.1.17") (d #t) (k 0)))) (h "1jm1jg07ah0gjz44bdm0dp3w1r2b11s2wiv10gzxxv069vs1pr5p")))

(define-public crate-orbtk-0.3.1-alpha2 (c (n "orbtk") (v "0.3.1-alpha2") (d (list (d (n "dces") (r "^0.2") (d #t) (k 0)) (d (n "euc") (r "^0.4.3") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.1-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "vek") (r "^0.9.12") (d #t) (k 2)))) (h "0rmhvp0iixjxcxxil3mv4vvm0bzxynhc16hwj09fmjg4v96l3fg8") (f (quote (("debug" "orbtk-api/debug"))))))

(define-public crate-orbtk-0.3.1-alpha3 (c (n "orbtk") (v "0.3.1-alpha3") (d (list (d (n "dces") (r "^0.3") (d #t) (k 0)) (d (n "euc") (r "^0.5.0") (d #t) (k 2)) (d (n "orbtk-api") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-render") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-shell") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-theme") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-theming") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-tree") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.1-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 2)) (d (n "vek") (r "^0.10.2") (d #t) (k 2)))) (h "0s9k044xwv5fpgkv8s8cw6sgqmwlpq1bcbispljyn24mxp0b99mv") (f (quote (("pathfinder" "orbtk-shell/pfinder" "orbtk-render/pfinder") ("log" "orbtk-shell/log") ("debug" "orbtk-api/debug"))))))

(define-public crate-orbtk-0.3.1-alpha4 (c (n "orbtk") (v "0.3.1-alpha4") (d (list (d (n "dces") (r "^0.3.1") (d #t) (k 0)) (d (n "euc") (r "^0.5.0") (d #t) (k 2)) (d (n "orbtk-core") (r "^0.3.1-alpha4") (k 0)) (d (n "orbtk-proc-macros") (r "^0.3.1-alpha4") (d #t) (k 0)) (d (n "orbtk-tinyskia") (r "^0.3.1-alpha4") (k 0)) (d (n "orbtk-utils") (r "^0.3.1-alpha4") (d #t) (k 0)) (d (n "orbtk-widgets") (r "^0.3.1-alpha4") (k 0)) (d (n "orbtk_orbclient") (r "^0.3.1-alpha4") (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 2)) (d (n "vek") (r "^0.12.1") (f (quote ("rgb" "rgba"))) (k 2)))) (h "00yf2scylpasvnrc9mpjz2j5asmw10m5nybcs569nr5d67zi0yfk") (f (quote (("log" "orbtk_orbclient/log") ("debug" "orbtk-core/debug") ("bundled" "orbtk_orbclient/bundled"))))))

