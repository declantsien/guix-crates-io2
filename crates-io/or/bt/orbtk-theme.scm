(define-module (crates-io or bt orbtk-theme) #:use-module (crates-io))

(define-public crate-orbtk-theme-0.3.0-alpha1 (c (n "orbtk-theme") (v "0.3.0-alpha1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.0-alpha1") (d #t) (k 0)))) (h "0a6vmgcm1vra9q0q9vb8sgg9bg6xn5r70a6jbdfl70srpp9w2fxn")))

(define-public crate-orbtk-theme-0.3.1-alpha2 (c (n "orbtk-theme") (v "0.3.1-alpha2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "orbtk-css-engine") (r "^0.3.1-alpha2") (d #t) (k 0)))) (h "0hbs4pfwbvvplkvhxni7pvpr493ig2f92klgin5sgk59z21pga5b")))

(define-public crate-orbtk-theme-0.3.1-alpha3 (c (n "orbtk-theme") (v "0.3.1-alpha3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "orbtk-theming") (r "^0.3.1-alpha3") (d #t) (k 0)))) (h "0x0mpm2bjxrpigzz3pv8jcg655b1r93avgbqp4inh2hxpl1mpfcw")))

