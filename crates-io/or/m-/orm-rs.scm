(define-module (crates-io or m- orm-rs) #:use-module (crates-io))

(define-public crate-orm-rs-0.0.0 (c (n "orm-rs") (v "0.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1m5w417qvzar2yx5r2x0m6xdr9mlfbxs98kj1v0v46hr79ynxlpl")))

