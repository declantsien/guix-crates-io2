(define-module (crates-io or ci orcid) #:use-module (crates-io))

(define-public crate-orcid-0.1.0 (c (n "orcid") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l8hnl3b6jf5z9fw0qjka6jivlv8n1qidlln79vwm3s2988ij4w6")))

(define-public crate-orcid-0.1.1 (c (n "orcid") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "195c336229qcnqmrfyifd3c7k00zsv96svy7kcggqq0g1shy625q")))

(define-public crate-orcid-0.1.2 (c (n "orcid") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zkfgih4qlgy47i6byrqpwq173l89bif2ns536ichjwqgxs5i4dl")))

(define-public crate-orcid-0.1.3 (c (n "orcid") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mvikgixyd3571n9wfaigjzsmr2r6r0naw2inpbpancjxfn0iprp")))

(define-public crate-orcid-0.1.4 (c (n "orcid") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1w73zwsqsdwmzg57728rwkfkmki8l0d22fm9l7qalm0hmfsysra6")))

