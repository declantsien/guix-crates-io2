(define-module (crates-io or ao orao-fuel-vrf) #:use-module (crates-io))

(define-public crate-orao-fuel-vrf-0.1.5 (c (n "orao-fuel-vrf") (v "0.1.5") (d (list (d (n "fuel-types") (r "^0.47.1") (d #t) (k 0)) (d (n "fuels") (r "^0.55") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0l9078q1pw5scpbidzfxvd868zw5a65s4szy52v12z89sgidwqp5") (r "1.65")))

(define-public crate-orao-fuel-vrf-0.1.6 (c (n "orao-fuel-vrf") (v "0.1.6") (d (list (d (n "fuel-types") (r "^0.47.1") (d #t) (k 0)) (d (n "fuels") (r "^0.55") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0lc72qyyhz3m6srmipbx40awcpsqpcxi66ijz75p5v8ab2cm3g63") (r "1.65")))

