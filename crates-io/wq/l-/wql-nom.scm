(define-module (crates-io wq l- wql-nom) #:use-module (crates-io))

(define-public crate-wql-nom-0.1.0 (c (n "wql-nom") (v "0.1.0") (d (list (d (n "bcrypt") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.121") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1r5ipq5073a0261vy3wxfqz3220dwwrmhz9pxiigkyfps014nwid")))

