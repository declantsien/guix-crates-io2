(define-module (crates-io mo ok mook) #:use-module (crates-io))

(define-public crate-mook-0.1.0 (c (n "mook") (v "0.1.0") (h "1mh9x2yq3qp5n13nx5gpyfhjxmbsmwlbkqmw3wx2xdbl2j0iik7c")))

(define-public crate-mook-0.1.1 (c (n "mook") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "tindercrypt") (r "^0.3.2") (d #t) (k 0)))) (h "0m0sgpc0p381hlhn1x45n3x9k9ifda47znpky2pmllqf5l4ij0c6")))

