(define-module (crates-io mo t- mot-rs) #:use-module (crates-io))

(define-public crate-mot-rs-0.0.1 (c (n "mot-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1vqp4b5yx17a13hh4g9xzgbnysd77yp07b8v9p1wbkv51r37lznk")))

(define-public crate-mot-rs-0.0.2 (c (n "mot-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0bs20fwykj66fkdqzpm5z4iaplsx7kj90cp6nfb11gb896xd7jcj")))

(define-public crate-mot-rs-0.0.3 (c (n "mot-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0qz871x93pxf5a98f6qarmywxb2n6n98i3vx3yg7pzbk4cd7wn30")))

(define-public crate-mot-rs-0.0.4 (c (n "mot-rs") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0fbm8bcab1jsvjvs6cgzp6cqhgnah8hg8rk07i452j50i2rzi499")))

(define-public crate-mot-rs-0.0.5 (c (n "mot-rs") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "18r9w556rkfwm04jl60flwwhbdz2q5lr8iy1943vbclqhczrbmk9")))

(define-public crate-mot-rs-0.0.6 (c (n "mot-rs") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "03wl7infkpw310apkffann4hvhng77i7b5n1k26fjjba5n8vvz8a")))

(define-public crate-mot-rs-0.0.7 (c (n "mot-rs") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1y0j6g032g1r29q4qgdwf6gr8q2x5181gc11yyi33bb2yp4ys4lz")))

(define-public crate-mot-rs-0.0.8 (c (n "mot-rs") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1kgkycybp21h98df0m172pdq7r39r7kdy1k9bzg31big393yz6v7")))

(define-public crate-mot-rs-0.0.9 (c (n "mot-rs") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0wm8m0xdja0px2b20ljd3nlvc7q8wflkgn2b961sp5c84m5ap64n")))

(define-public crate-mot-rs-0.0.10 (c (n "mot-rs") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1yn3ydkzdr8549pwazvhl38bq6g89v93l5sxhfzdfdq7w5qkv5h1")))

(define-public crate-mot-rs-0.0.11 (c (n "mot-rs") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1qky2rzgfyr1zrirr5bd5s0qxg221q32275zl45mh3qhbbzv4chi")))

(define-public crate-mot-rs-0.0.12 (c (n "mot-rs") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0zxdrzda2mhxy2jx4s16hxqxqvi5k3q7y36246b2h5s4wp1gcra6")))

(define-public crate-mot-rs-0.1.0 (c (n "mot-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0p1x6dkysm5yykgpi6v0jva7nw2r0lk9lygww2rqyyvjsz8aarjr")))

(define-public crate-mot-rs-0.1.1 (c (n "mot-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "kalman-rust") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0yhrnl1g98simwc7cyrj4sbm2g8addp148pl13db7afd2i162ccs")))

