(define-module (crates-io mo v- mov-cli-rs) #:use-module (crates-io))

(define-public crate-mov-cli-rs-1.0.0 (c (n "mov-cli-rs") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fzf-wrapped") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "13ap1b0xm2v3g8abz22cb8kiis38625qzbhbdwcrxh9dkdjns2lw")))

(define-public crate-mov-cli-rs-1.0.1 (c (n "mov-cli-rs") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fzf-wrapped") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "12kzv6b326ylayy1dipcldsd9d4vnmzq8mixz1kd2x25rmsdrx0d")))

