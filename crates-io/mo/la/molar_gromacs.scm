(define-module (crates-io mo la molar_gromacs) #:use-module (crates-io))

(define-public crate-molar_gromacs-0.1.0 (c (n "molar_gromacs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1r4v0gb0akz8sbyb3wlky1wc46vngzy34aq0zi9xabhd83jylz31") (l "stdc++")))

(define-public crate-molar_gromacs-0.2.0 (c (n "molar_gromacs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0aszg6gvn9flrcki1b720cj89vf93sfb1vmyg65y8nl6qsnymz2f") (l "stdc++")))

(define-public crate-molar_gromacs-0.2.1 (c (n "molar_gromacs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "02qrs9nwb3mi7qwsih26faxv1v5zfjdikc45h1nc2n1ck920rshz") (l "stdc++")))

