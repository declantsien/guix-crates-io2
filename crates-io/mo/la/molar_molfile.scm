(define-module (crates-io mo la molar_molfile) #:use-module (crates-io))

(define-public crate-molar_molfile-0.1.0 (c (n "molar_molfile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1pqpyq17v4926lnaging1vd8zzzmgfwv3sk3dnapmjk2sszkc0cv")))

(define-public crate-molar_molfile-0.1.1 (c (n "molar_molfile") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0mcd4b07f1b6v3s9d672srp7ad9zrb7ln9g4hpc459wngsgiv9dd")))

(define-public crate-molar_molfile-0.1.2 (c (n "molar_molfile") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0dn1v3f6rvyvgvzwxxf74mwnjlqxlbczy2r4v7fbipb5s9qnjd8y")))

