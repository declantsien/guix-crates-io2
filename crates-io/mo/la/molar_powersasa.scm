(define-module (crates-io mo la molar_powersasa) #:use-module (crates-io))

(define-public crate-molar_powersasa-0.1.0 (c (n "molar_powersasa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ar95fsxvmf2r32r0n6zlisyc8cyp9wsfmc5cm3hka9m5m2qj15m")))

(define-public crate-molar_powersasa-0.1.1 (c (n "molar_powersasa") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1y74li1j8k2a0cjrgaxmb80q9kmrbqb05hhrafpgjpf2z52xyqhy")))

(define-public crate-molar_powersasa-0.1.2 (c (n "molar_powersasa") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0arjilfhzaawgz362lk0wcl94hp1ijc9wbgzba8ligmpnxg86aip")))

(define-public crate-molar_powersasa-0.1.3 (c (n "molar_powersasa") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0rr8m2vw5psxr0jp29gb0s0gi7507k2rnydwbvicx6pyqrjk6mis")))

