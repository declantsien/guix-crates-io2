(define-module (crates-io mo la molar_xdrfile) #:use-module (crates-io))

(define-public crate-molar_xdrfile-0.1.0 (c (n "molar_xdrfile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1m14kzf0iiwbzkkfm52icz288ibg51dc6v9x21w2p8jdzwshv355")))

(define-public crate-molar_xdrfile-0.1.1 (c (n "molar_xdrfile") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1kbwd10n47l09npgkixmsf3ahw1lpl9cwvy94bwaf6aig7m0fqwz")))

