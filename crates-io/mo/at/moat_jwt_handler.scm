(define-module (crates-io mo at moat_jwt_handler) #:use-module (crates-io))

(define-public crate-moat_jwt_handler-0.1.0 (c (n "moat_jwt_handler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1s18xq9j8g599vwbfw6x969kdda4sa0yrf16i90pb3iq26npz2yb")))

(define-public crate-moat_jwt_handler-0.2.0 (c (n "moat_jwt_handler") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0ii2gc91n8ysvv8j9qg79ygxaml567nyfbjdg71k5f4pq7awghrx")))

(define-public crate-moat_jwt_handler-1.0.0 (c (n "moat_jwt_handler") (v "1.0.0") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1vqs6lmpj4hwd67vvci2ymj6r208rhhh0wk5rnhn7l5xipk7bq1p")))

