(define-module (crates-io mo rx morx-tools) #:use-module (crates-io))

(define-public crate-morx-tools-0.1.0 (c (n "morx-tools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "morx") (r "^0.1") (d #t) (k 0)) (d (n "tch") (r "^0.7") (d #t) (k 0)))) (h "00h46k1p53wb8y67k0sgh5f2yp82vns90pz6jaqwd1s07v3yilkk")))

