(define-module (crates-io mo dp modpow) #:use-module (crates-io))

(define-public crate-modpow-1.0.0 (c (n "modpow") (v "1.0.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1nzxn8pmri8hrh4kxvl7x0hzrwj7vw05g2hccn4zj68ri89f6mfh")))

(define-public crate-modpow-1.0.1 (c (n "modpow") (v "1.0.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "14xj422fi7mrji5ykrb7wnz6s4p9qfpa8nj13275z61nvm05cw10")))

