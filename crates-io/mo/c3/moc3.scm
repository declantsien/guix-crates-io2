(define-module (crates-io mo c3 moc3) #:use-module (crates-io))

(define-public crate-moc3-0.1.0 (c (n "moc3") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pzxhnqcbnzqcczq3bldwsacwl8fnnwai4ngcxipqvcwv1izrmz1")))

