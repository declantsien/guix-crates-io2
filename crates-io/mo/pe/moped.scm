(define-module (crates-io mo pe moped) #:use-module (crates-io))

(define-public crate-moped-0.1.0 (c (n "moped") (v "0.1.0") (d (list (d (n "async-mpd") (r "^0.2.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "moped-shared") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.10.0") (d #t) (k 0)))) (h "0hm5rkr7ppf2xhbkybwiqhv88q2dsll5k4hha56j127k12irv7dg")))

