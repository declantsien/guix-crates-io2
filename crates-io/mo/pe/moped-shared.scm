(define-module (crates-io mo pe moped-shared) #:use-module (crates-io))

(define-public crate-moped-shared-0.1.0 (c (n "moped-shared") (v "0.1.0") (d (list (d (n "async-mpd") (r "^0.2.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18b83szh2zii4a4d6m00wjshhydjr47sd7fi5b8gjnz448hd4l3j")))

