(define-module (crates-io mo pe moped-backend) #:use-module (crates-io))

(define-public crate-moped-backend-0.1.0 (c (n "moped-backend") (v "0.1.0") (d (list (d (n "async-mpd") (r "^0.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tide") (r "^0.9.0") (d #t) (k 0)))) (h "14zp85crw89bs8dxv84s7dgvrivhykk1p3ngwjb6kflckcwqkivn")))

