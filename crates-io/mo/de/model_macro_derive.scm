(define-module (crates-io mo de model_macro_derive) #:use-module (crates-io))

(define-public crate-model_macro_derive-0.1.0 (c (n "model_macro_derive") (v "0.1.0") (d (list (d (n "model_macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "116b92qs1ylcgnzqh4n02zby15h5fmxx8qdfbac0v1w7lcp58m9x")))

(define-public crate-model_macro_derive-0.2.0 (c (n "model_macro_derive") (v "0.2.0") (d (list (d (n "model_macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sj26s7hxn6d2xpar5lakrh5j0rcdbf35knn5a7i3nv8sh4cf3q5")))

(define-public crate-model_macro_derive-0.2.1 (c (n "model_macro_derive") (v "0.2.1") (d (list (d (n "model_macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07b87jr4ai77nywbfyp8lpai3m44v027rz9yq1kznxgbyilnnzvh")))

(define-public crate-model_macro_derive-0.2.2 (c (n "model_macro_derive") (v "0.2.2") (d (list (d (n "model_macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r8x8150ynf18mqi26i5qhp27r366g2rav411nzb4dl8i7pq6xj8")))

(define-public crate-model_macro_derive-0.2.3 (c (n "model_macro_derive") (v "0.2.3") (d (list (d (n "model_macro") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01awfhi926cfxf8ccsm0ryn3i4pr07y74gmn85y6s5g1sd7i4x7d")))

