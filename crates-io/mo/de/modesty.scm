(define-module (crates-io mo de modesty) #:use-module (crates-io))

(define-public crate-modesty-0.1.0 (c (n "modesty") (v "0.1.0") (h "148q0prc30g893frjynxrlmj9xl0wxy1ci8nnzn6s2nvf1khr1mz")))

(define-public crate-modesty-0.1.1 (c (n "modesty") (v "0.1.1") (h "1lj679hv368c24pz6pr2pnjbzfl3kb5jlzwpv5qd7s1mr9pjj3pg")))

