(define-module (crates-io mo de model-mapper-macros) #:use-module (crates-io))

(define-public crate-model-mapper-macros-0.1.0 (c (n "model-mapper-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "macro-field-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pav45qnhcif9dyc9fxkxmzm3c3brk2jqzdb9wv6nclqlqyjndzp")))

(define-public crate-model-mapper-macros-0.2.0 (c (n "model-mapper-macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "macro-field-utils") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0y41kmwq36rrmgjapx5pvh7d61b6a2g0lzhjn01pf1vmzpnkk8da")))

(define-public crate-model-mapper-macros-0.2.1 (c (n "model-mapper-macros") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "macro-field-utils") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ms86hr28llvkhyidzgycijajfps84na42h8nx9c4zzzc0npr4ix")))

