(define-module (crates-io mo de model3d) #:use-module (crates-io))

(define-public crate-model3d-0.1.0 (c (n "model3d") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1w2y0kkarbdbmmy1d3grb612jh16w62rigm1x9s5i3yx1lr7mb8j") (y #t)))

(define-public crate-model3d-0.0.1 (c (n "model3d") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17") (d #t) (k 2)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "wgpu") (r "^0.13") (d #t) (k 2)) (d (n "winit") (r "^0.27") (d #t) (k 2)))) (h "10wlhvllk8nbc5b32vhbwvly1gqk60938dnclri1jl1nvxhqgh8y")))

