(define-module (crates-io mo de modesetting) #:use-module (crates-io))

(define-public crate-modesetting-0.0.1 (c (n "modesetting") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.7") (d #t) (k 0)) (d (n "gcc") (r "^0.3.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0i6lv5p05ci5gwwzwp4xawnbqs4rgg6azyi7rjfxv5yqylr5vdw8")))

(define-public crate-modesetting-0.0.2 (c (n "modesetting") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.7") (d #t) (k 0)) (d (n "gcc") (r "^0.3.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1ydhmy8jharfrp0c2b6n17jzwk5d0qaprrb434y6xldy20xvaxi4")))

(define-public crate-modesetting-0.0.3 (c (n "modesetting") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.7") (d #t) (k 0)) (d (n "gcc") (r "^0.3.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0k52pw28p4siyiczip28y46l905l6afa0a2jrk2sccwjzvv1k8b5")))

(define-public crate-modesetting-0.1.0 (c (n "modesetting") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "1mmy54gj8ksghwvbg2w18645c1fvc3vxxsm6y96803wqpzyw2l8d")))

(define-public crate-modesetting-0.2.0 (c (n "modesetting") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "007z2c45pzgl0zaap4xkjxy9k7ky1pa2bgxvkx0nba1yh3p6a1vc")))

(define-public crate-modesetting-0.2.1 (c (n "modesetting") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0aizg1cbs87kqid3xdpflijxdxfs3lfxsldkwjm8cils2yxic9fj")))

(define-public crate-modesetting-0.2.2 (c (n "modesetting") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "178i662dghxifiddxifi2s7z9bdlr564d235nhl41k5sg0wc583j")))

