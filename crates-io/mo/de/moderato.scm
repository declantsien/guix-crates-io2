(define-module (crates-io mo de moderato) #:use-module (crates-io))

(define-public crate-moderato-0.1.0 (c (n "moderato") (v "0.1.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 2)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.6") (d #t) (k 0)))) (h "0k41rpp1ph6wl8518d6x826580d94dhwy2ph44qyfhpbkhn544aq") (y #t)))

