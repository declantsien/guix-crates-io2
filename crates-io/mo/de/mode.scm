(define-module (crates-io mo de mode) #:use-module (crates-io))

(define-public crate-mode-0.1.0 (c (n "mode") (v "0.1.0") (h "0qv6cszvds111ps08cwdrvnfr1q6ygpqssj8fqklxx88036fx6vd")))

(define-public crate-mode-0.1.1 (c (n "mode") (v "0.1.1") (h "0j5al5ci63nbsls7zn2wyjxcsrki0vv6p96nxvxy2v0xzck0yv77")))

(define-public crate-mode-0.2.0 (c (n "mode") (v "0.2.0") (h "1b5ih3a8306dxfhmv6mzjiszpfv37qr0hr7lcpnwq65jgk0ps4pc")))

(define-public crate-mode-0.2.1 (c (n "mode") (v "0.2.1") (h "1glfbrq6s5a3l4l97ml8jz64q0fp4fi2cidj2fd077f5vrzq61gp")))

(define-public crate-mode-0.2.2 (c (n "mode") (v "0.2.2") (h "1i9hg7ji15smc10n482zkjsrjcjqrlmr4sq3dnxs4mh3k09nbs6j")))

(define-public crate-mode-0.2.3 (c (n "mode") (v "0.2.3") (h "0y5f461bijwj54qmaa2lfy52qg29x4fb0lqwi92sir4n0f2jrvhg")))

(define-public crate-mode-0.2.4 (c (n "mode") (v "0.2.4") (h "064627bm6ngqyy9ry5brajm9nkrv0ssiyw380i6xkfaj3qp60kq1")))

(define-public crate-mode-0.3.0 (c (n "mode") (v "0.3.0") (h "09ri7gwbbafc5z2i41vhir0wypmsm0l11fa27gf5db46i0hxivbp")))

(define-public crate-mode-0.4.0 (c (n "mode") (v "0.4.0") (h "0yr9h3015whiwl0szhlb1ap2is0mp5jvw8d3aqllm0ip6z471k68")))

(define-public crate-mode-0.4.1 (c (n "mode") (v "0.4.1") (h "1jiwcylv6z5ygyvj72dicrgzhrg4yzyjp2i83nnsf58zg995bll6")))

