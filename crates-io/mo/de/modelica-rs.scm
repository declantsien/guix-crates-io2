(define-module (crates-io mo de modelica-rs) #:use-module (crates-io))

(define-public crate-modelica-rs-0.1.0 (c (n "modelica-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "1zswfc5r3d0hcmdhaa2i0y52j8zr7srhllzkz3g41p1fc3slhlsn")))

(define-public crate-modelica-rs-0.1.1 (c (n "modelica-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "16jv7qk6hvs9a7lkzi5c7xd6dn9lgbcrwdzgsqz1xhs8l4y639z2")))

(define-public crate-modelica-rs-0.1.2 (c (n "modelica-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "0qlljl9fybrmjhqpgkzlf5krqkg5szrrigpy6gm4qkhywnbh66xh")))

(define-public crate-modelica-rs-0.1.3 (c (n "modelica-rs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "18s7xmivx7i0n77zr90rdq2i3flykmn9lfk08lkv9wmhcsh1jqix")))

(define-public crate-modelica-rs-0.1.4 (c (n "modelica-rs") (v "0.1.4") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.5") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "git2") (r "^0.18.1") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hl5qwws0xnlyffikjaxm6vrqgv37k5v7bzfbl9l8qr8qvpkbpr4")))

(define-public crate-modelica-rs-0.1.5 (c (n "modelica-rs") (v "0.1.5") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.5") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "git2") (r "^0.18.1") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10329lbfcz0jgfxivlq8kfaclpm1vs95sqz8l18s33d12md7gibj")))

