(define-module (crates-io mo de modelassetlib-native) #:use-module (crates-io))

(define-public crate-modelassetlib-native-0.1.0 (c (n "modelassetlib-native") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "17w2m9kn2hc84qd7cabhgi58kczcy4cfgh2wyfnxj43x5miylqjn") (y #t)))

(define-public crate-modelassetlib-native-0.1.1 (c (n "modelassetlib-native") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1llm1dja4dilbcdk5jszc8zlz9dzym76k2m7gqz0siikx4nis2cn")))

