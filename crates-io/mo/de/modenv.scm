(define-module (crates-io mo de modenv) #:use-module (crates-io))

(define-public crate-modenv-0.1.0 (c (n "modenv") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "06g5h1s0jgslvdshijdmcb8vmiaz4dadyn17fp92nglxkvjm5cpw")))

(define-public crate-modenv-0.2.0 (c (n "modenv") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1vihzizg177a9pb57gxqvhk4qwlksid3mvrm0gv1w3l5kwp396f9")))

(define-public crate-modenv-0.3.0 (c (n "modenv") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1vppk84fw4i30ah5cj6h0xqn8m8pl1bji650bs4h0dyjbjpfq0v8")))

(define-public crate-modenv-0.3.1 (c (n "modenv") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "0v5h1jwi1lq35rqdimrvd7qihm9bghl7fqnmp4dg75vvnj8kl3ix")))

(define-public crate-modenv-0.3.8 (c (n "modenv") (v "0.3.8") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "0gddp4d1syb8qj4mik0492f5gfi0bzy1lv1zqbb99jjldqqvqn0y")))

(define-public crate-modenv-0.3.9 (c (n "modenv") (v "0.3.9") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1yla8sinjmjwyf9gf6ganwzbr8w5rgfqra2xgb9611rqkyfk0c0p")))

(define-public crate-modenv-0.4.1 (c (n "modenv") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1jrkjl117nr4adpjrj4iznlsfk70ag69gbhrhb3zjs9x9g5aayas")))

(define-public crate-modenv-0.4.2 (c (n "modenv") (v "0.4.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1a5fg03rv8cfzxv00xz0z5bhnmm9x9xdc1phxdsyvfks6i7jr584")))

(define-public crate-modenv-0.5.0 (c (n "modenv") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)))) (h "1vqk4r8b2z7fig063g7l40agb2w4znnnziing8lqybcw5hnbbyxp")))

(define-public crate-modenv-0.5.1 (c (n "modenv") (v "0.5.1") (d (list (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "0w1vbbaqln4zbdav7lybdji6hlgg7zn0832yr3lxlf4y0954ll4l") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.6.1 (c (n "modenv") (v "0.6.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "0mw4lvw21zxrf3jgbwkv2q8j3pjx5ik016269l4cr2dwsnzfrywq") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.0 (c (n "modenv") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1m94pwlr36mrh4gvbn0cdy3rwb2npk0id2s3446i9qa6gjw9i9ka") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.1 (c (n "modenv") (v "0.7.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1j211ngr6zlhdmc9bs26l6jlvfn3h7rxnhxkc8v50mqcg92n2iva") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.2 (c (n "modenv") (v "0.7.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1hb642wq0llw81la30h9ma1j6bh85b1w3cdyx9kif323yzriirv7") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.3 (c (n "modenv") (v "0.7.3") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1i4n2ddf8l1sgiw1f0nx9byhpb3g22w0y5jhlp1nyz89z1rl196i") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.4 (c (n "modenv") (v "0.7.4") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1dfkn2x452qvmrmnd25cgajbmgaigisq0ddidl7g23la0cb5b28y") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.5 (c (n "modenv") (v "0.7.5") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "0cxwq3ac009sn0p1i0bpn6jhpjlfx6sfamh6jrrxnl4ylm16in63") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.7.6 (c (n "modenv") (v "0.7.6") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1h5msngcfyznm8cj24qyr6y8fkcqizhlx7jk6bzfy7zzp2y0jhzd") (f (quote (("push" "reqwest" "serde" "serde_json"))))))

(define-public crate-modenv-0.8.0 (c (n "modenv") (v "0.8.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "098qnmqnyqkxxgnnx49rb7ppkk7zcx0fghz8dzv8slv7ndj957c7")))

(define-public crate-modenv-0.8.1 (c (n "modenv") (v "0.8.1") (d (list (d (n "clap") (r "=3.1") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "14k52ad2dpf5k4f3lsx8sys87ri24mpmw8y0v4lk82s5455mcnsr")))

(define-public crate-modenv-0.8.2 (c (n "modenv") (v "0.8.2") (d (list (d (n "clap") (r "=3.1") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1l7j81yvs3iikdsrns4ac36z4fzdim60kbz1138i89wk3wrc0k2g")))

(define-public crate-modenv-0.9.0 (c (n "modenv") (v "0.9.0") (d (list (d (n "clap") (r "=3.1") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "0xkavj8plqsk2gsd524hvciqnd4w2wrfm7a4gy9z6lwbby6zh9ii")))

(define-public crate-modenv-0.9.1 (c (n "modenv") (v "0.9.1") (d (list (d (n "clap") (r "=3.1") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "0849s2d3khmf6xbqnl1jmyz6gn8pc5prg4gnkfsrpv3bvsrw29n3")))

(define-public crate-modenv-0.10.0 (c (n "modenv") (v "0.10.0") (d (list (d (n "clap") (r "=3.1") (f (quote ("color" "env" "suggestions"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1yrmk73ijl9cs182765k2a2d6bd8j5whr7fpv3yzh3cnzncnsymw")))

(define-public crate-modenv-0.10.1 (c (n "modenv") (v "0.10.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("color" "env" "suggestions" "derive"))) (d #t) (k 0)) (d (n "env2") (r "^0.3.1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1cg1shjlhh2b1dg49j28hcy67nshj83cwdgifsj9ic7aj7yvbnsy")))

(define-public crate-modenv-0.10.2 (c (n "modenv") (v "0.10.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("color" "env" "suggestions" "derive"))) (d #t) (k 0)) (d (n "env2") (r "^0.3.1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "13b56k9n14avdv7jyyb7ls6z6l9wakgfjxyl4dmrcr78gaz8f7d0")))

(define-public crate-modenv-0.10.3 (c (n "modenv") (v "0.10.3") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("color" "env" "suggestions" "derive"))) (d #t) (k 0)) (d (n "env2") (r "^0.3.1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "06yh3znakx73q46jrwwck94yji87ffmv7y82npm8a1rcg2s3rk7f")))

