(define-module (crates-io mo de modelizer) #:use-module (crates-io))

(define-public crate-modelizer-0.1.0 (c (n "modelizer") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "1r22kl8aqvi254lypqpgadwy22fwrvzx2r7r8hhgvyjnny9mbccx")))

(define-public crate-modelizer-0.1.1 (c (n "modelizer") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "1ldhi9r713skjqxcvi2y33kyv7ahrjsingmwi79i4plvrzjjdj3f")))

(define-public crate-modelizer-0.1.2 (c (n "modelizer") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "0p6zdgcidy01k8k2nvnwda6grvrylnhjcvflr4lqh7rp04xdm95z")))

