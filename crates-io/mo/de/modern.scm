(define-module (crates-io mo de modern) #:use-module (crates-io))

(define-public crate-modern-0.1.0 (c (n "modern") (v "0.1.0") (h "0xkl51l68nvym2bwdw6g0rhdfih6m6cfx8g35xs5lg4syfb01vxl")))

(define-public crate-modern-0.1.1 (c (n "modern") (v "0.1.1") (h "0lknk8grybfl0rc5ddsy05ybdgank5y6vykk1xlpikglmscmk2p0")))

