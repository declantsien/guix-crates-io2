(define-module (crates-io mo de models-proc-macro) #:use-module (crates-io))

(define-public crate-models-proc-macro-0.1.0 (c (n "models-proc-macro") (v "0.1.0") (d (list (d (n "models-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0r6p50l3mm5pnvjbdm403nb8f274g5g80l6ivjp6d20xmwipm6ha") (f (quote (("helpers"))))))

(define-public crate-models-proc-macro-0.1.1 (c (n "models-proc-macro") (v "0.1.1") (d (list (d (n "models-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "15y26dij9a7ilc64pa3fhh4rjqk0bhg4kkar77zn8cjill4p58ib") (f (quote (("helpers"))))))

