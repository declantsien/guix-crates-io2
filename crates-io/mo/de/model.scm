(define-module (crates-io mo de model) #:use-module (crates-io))

(define-public crate-model-0.0.1 (c (n "model") (v "0.0.1") (d (list (d (n "deterministic") (r "^0.0.6") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "16rzyxyyyhba8p7lncb03jwi0cy48qvirygdblkndc3akv9fxnx2")))

(define-public crate-model-0.0.2 (c (n "model") (v "0.0.2") (d (list (d (n "deterministic") (r "^0.0.6") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1hgn1yq3hszs04jf5pwd7zyxsfhz975ajyrr7fwbr1xshpyy9981")))

(define-public crate-model-0.0.3 (c (n "model") (v "0.0.3") (d (list (d (n "deterministic") (r "^0.0") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "147dwzp0k333l5rqf45nfjd9nipz11b1awcb5nyfchjxcnf04g5i")))

(define-public crate-model-0.0.4 (c (n "model") (v "0.0.4") (d (list (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.7") (d #t) (k 0)))) (h "1j7rbvmgmwi6jx6z6ipm4nvnk37ic4p8b5qd6irr1kn17p7id60c")))

(define-public crate-model-0.1.0 (c (n "model") (v "0.1.0") (d (list (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 0)))) (h "1yybs76lljf2l1bs1my1xdcf0r8gk0lpapmvfkc2qc9p2cr8b9m7")))

(define-public crate-model-0.1.2 (c (n "model") (v "0.1.2") (d (list (d (n "permutohedron") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 0)))) (h "0kx6hy5i1fn2qs4x6hpng9jixpm68g83vm24z8bqqscr317yinb6")))

