(define-module (crates-io mo de modernized_vigenere) #:use-module (crates-io))

(define-public crate-modernized_vigenere-0.1.0 (c (n "modernized_vigenere") (v "0.1.0") (h "03vjq0qd0snd68j0xsg9qr916z3nmmbhs77qimaqivmawzscxig6")))

(define-public crate-modernized_vigenere-0.1.1 (c (n "modernized_vigenere") (v "0.1.1") (h "0c48svn7qvlvc1jdvvfcnybpv47vjrx283lwdm4wgp38cwn17b03")))

(define-public crate-modernized_vigenere-0.1.2 (c (n "modernized_vigenere") (v "0.1.2") (h "0r4vyabb57l0mr7n06jvh27p9v9id5crdhbzm67y6g75psw0i4pc")))

