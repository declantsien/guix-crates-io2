(define-module (crates-io mo de model-mapper) #:use-module (crates-io))

(define-public crate-model-mapper-0.1.0 (c (n "model-mapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "model-mapper-macros") (r "=0.1.0") (d #t) (k 0)))) (h "19ixzkxgj5bv3hxiwh8jk43ih515qqb9x6hhm1nrbmg16lvp05k2") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-model-mapper-0.2.0 (c (n "model-mapper") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "model-mapper-macros") (r "=0.2.0") (d #t) (k 0)))) (h "0akn8m8ivb2g1x8s2fy0644fc115zjpkldic3l6wly1fwqlxr3w1") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-model-mapper-0.2.1 (c (n "model-mapper") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "model-mapper-macros") (r "=0.2.1") (d #t) (k 0)))) (h "1pdkakpgvl2srzrcw82pnbalybxia6cq7wd0w6xsf5x1shabykiw") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

