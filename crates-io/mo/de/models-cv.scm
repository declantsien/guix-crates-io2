(define-module (crates-io mo de models-cv) #:use-module (crates-io))

(define-public crate-models-cv-0.1.0 (c (n "models-cv") (v "0.1.0") (d (list (d (n "gltf") (r "^1.2") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1glmqhhxv2mccdiid5lb3ygqlj4m3s0n6nfxbf0m8zh6bpl9whkf")))

