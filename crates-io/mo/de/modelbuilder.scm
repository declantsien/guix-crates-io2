(define-module (crates-io mo de modelbuilder) #:use-module (crates-io))

(define-public crate-modelbuilder-0.0.1 (c (n "modelbuilder") (v "0.0.1") (d (list (d (n "candle-core") (r "^0.3.1") (f (quote ("cuda"))) (d #t) (k 0) (p "candle-core")) (d (n "candle-nn") (r "^0.3.1") (d #t) (k 0)) (d (n "candle-transformers") (r "^0.3.1") (d #t) (k 0)) (d (n "hf-hub") (r "^0.3.2") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sbq01ayj8mf83c5iqwyv6z4acvwlslaivzilyj35v5d80mqv5k9")))

