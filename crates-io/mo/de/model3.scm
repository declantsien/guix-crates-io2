(define-module (crates-io mo de model3) #:use-module (crates-io))

(define-public crate-model3-0.1.0 (c (n "model3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1v8gpmkp0c9cdz0pj2c3ghv8zb6ng9zn34ck5pn1lv3hqbrzns09")))

(define-public crate-model3-0.2.0 (c (n "model3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16nj6imh2b24ycf0nczviki2zwpnnjpiqn6m5ryypkxcks8w8c0y")))

