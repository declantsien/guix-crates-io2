(define-module (crates-io mo de models-parser) #:use-module (crates-io))

(define-public crate-models-parser-0.1.0 (c (n "models-parser") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 2)))) (h "01sfj2mglkzxzds5qyxzd72sskhnw34bgviq0jp2vqahxv3acbfq") (f (quote (("std") ("json_example" "serde_json" "serde") ("default" "std"))))))

(define-public crate-models-parser-0.2.0 (c (n "models-parser") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 2)))) (h "0xjp5az7hl2m20dazqngad2sj069w0h0zbvs8h0c5846v404bfal") (f (quote (("std") ("json_example" "serde_json" "serde") ("default" "std"))))))

