(define-module (crates-io mo de modelfox_macro) #:use-module (crates-io))

(define-public crate-modelfox_macro-0.8.0 (c (n "modelfox_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04dyvr077sz3s65crgrf4ih30pvxviiskcc6cc97znikycjg2irn")))

