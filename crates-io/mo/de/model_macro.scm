(define-module (crates-io mo de model_macro) #:use-module (crates-io))

(define-public crate-model_macro-0.1.0 (c (n "model_macro") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)))) (h "1r2jd88y32rwyimzbny334pc0ii0gd01ylsw5cxswgrw8swf6z6f")))

(define-public crate-model_macro-0.2.1 (c (n "model_macro") (v "0.2.1") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)))) (h "0fp0fylk6ybspmc37ir511a7m3r271mmmi6kh5fmg43cvs7948sh")))

(define-public crate-model_macro-0.2.2 (c (n "model_macro") (v "0.2.2") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p9ac084yg8pby376ighx3pqyihfdmz058c4idmkbrr5pbmy1r74")))

(define-public crate-model_macro-0.2.3 (c (n "model_macro") (v "0.2.3") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k0jdnwjmc9vqcds9rrwkqj64gl1b8rh8i8wvih51933nd67fbk4")))

(define-public crate-model_macro-0.2.4 (c (n "model_macro") (v "0.2.4") (d (list (d (n "bevy_reflect") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zkn7c2w5fnkbghwflvvdqziv3ja7584vjr9vw625y24xwhkj50i")))

