(define-module (crates-io mo de modern-terminal) #:use-module (crates-io))

(define-public crate-modern-terminal-0.1.0 (c (n "modern-terminal") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ihahvw02cka6iy7rf657hh39ybihzx52mdbakql7q3dcjpk1bgq")))

(define-public crate-modern-terminal-0.2.0 (c (n "modern-terminal") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ybfvnj5fd3jr3wi8h3axpk3gw8zmhbj8dijg8hiwgfk2hcfif6c")))

(define-public crate-modern-terminal-0.3.0 (c (n "modern-terminal") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dbwdwjq5k8psz0jlal0j1y6rc14an995r0nrsmxghdfi41m6lw2")))

(define-public crate-modern-terminal-0.4.0 (c (n "modern-terminal") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hvkxbyqw1bf147xwfkvqkdln2ymj7nlhqp57z8agg0xlxb29lzb")))

(define-public crate-modern-terminal-0.5.0 (c (n "modern-terminal") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "14176xsckglzj0l8l94vlhrj9hciv2q7zr7zdhypkxqkgmnkf3m1")))

(define-public crate-modern-terminal-0.6.0 (c (n "modern-terminal") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "0xib59i771ini0h81nhv6ca3m5bdgv0yjdlkpq1834r0a87iiihx")))

(define-public crate-modern-terminal-0.7.0 (c (n "modern-terminal") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "14by82rrgbdq86hpnx7gsc2vaasrkaiz6r3rr17ymmycz13as093")))

