(define-module (crates-io mo de modexp) #:use-module (crates-io))

(define-public crate-modexp-0.1.0 (c (n "modexp") (v "0.1.0") (d (list (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "01wikqibqdrr1f8pb0zlavvr2d4c3fq0spm690ixvvxy276nsxvr")))

(define-public crate-modexp-0.2.0 (c (n "modexp") (v "0.2.0") (d (list (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1fjdlkfs0yb5jswzzsig3234rzsvrwf8r3cm6jzgqklcnbm1x5vg")))

(define-public crate-modexp-0.2.1 (c (n "modexp") (v "0.2.1") (d (list (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0xb8cv7is0256rinswfjnkn3c6i4salg9pkam077nlryjdyyq08c")))

(define-public crate-modexp-0.2.2 (c (n "modexp") (v "0.2.2") (d (list (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "19xsraiafmg75w1f5nda4i7wgjh51dwg24i9z62k5bpwlz1xcnw2")))

