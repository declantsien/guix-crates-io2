(define-module (crates-io mo gu mogul) #:use-module (crates-io))

(define-public crate-mogul-0.1.0 (c (n "mogul") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "16b3fpx79llbqdpk2y5r4ffasb0g9n6l837f9a5wj7igf52ashrj")))

(define-public crate-mogul-0.1.1 (c (n "mogul") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1m43pjc5dxm2xswbr23r1rnvnqnzpra275kq0d20fpi6nzfac89p")))

