(define-module (crates-io mo gu moguls) #:use-module (crates-io))

(define-public crate-moguls-0.1.0 (c (n "moguls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "15jgx87w39nx2j76cysmhm4svk50amjrq3vh5qw2fvbx500684q3") (y #t)))

(define-public crate-moguls-0.1.1 (c (n "moguls") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1lnh9slgwl3ks7ca9rya9k8jgh6vkp1wzzk81mij2k3kw04bgczh")))

