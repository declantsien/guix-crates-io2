(define-module (crates-io mo le molehill) #:use-module (crates-io))

(define-public crate-molehill-0.1.0 (c (n "molehill") (v "0.1.0") (h "16grwv8dwgv441x9ncz7sibxd5mmyr02rjw79rvq2mlavwdwly86") (y #t)))

(define-public crate-molehill-0.1.1 (c (n "molehill") (v "0.1.1") (h "0fmp1xkr9f6g7w6ahsah6ak7gffg6lxa7hi64l2znj1krgqjvl4b") (y #t)))

(define-public crate-molehill-0.2.0 (c (n "molehill") (v "0.2.0") (h "16l4c20c50a0h80431cmgkwxdi0hzh9kcif8rnkh1paxncp39b69") (y #t)))

(define-public crate-molehill-0.2.1 (c (n "molehill") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1.0.5") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 2)))) (h "0r78jj5pzvj67hyi99890x15xq1slan6wnn3knq8gw03q67qv442")))

(define-public crate-molehill-0.2.2 (c (n "molehill") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^1.0.5") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 2)))) (h "1p91023mrgpr80fxpzgllfrs35afrw8pw4h6gh7xvhi06nrj26bc")))

