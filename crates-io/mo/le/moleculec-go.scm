(define-module (crates-io mo le moleculec-go) #:use-module (crates-io))

(define-public crate-moleculec-go-0.1.0 (c (n "moleculec-go") (v "0.1.0") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.5.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "0wxgj4l2n197kr4rs5ygxhr88ijq22ma124x6ls3lq8psc2955nj") (y #t)))

(define-public crate-moleculec-go-0.1.1 (c (n "moleculec-go") (v "0.1.1") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "100yi2s55k89glwqcd3fwksa08liy14csifyvcv5qackackjn4hm") (y #t)))

(define-public crate-moleculec-go-0.1.2 (c (n "moleculec-go") (v "0.1.2") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "09yqc04n1q7g5rvjin895k2ly4kmabpsh9rk6dcjcizzdjg4k2h9") (y #t)))

(define-public crate-moleculec-go-0.1.3 (c (n "moleculec-go") (v "0.1.3") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "1g4hbm5fwpplvl8s11pvzzak2z1jf0ysrn78k35i3fcy2chmwz1i") (y #t)))

(define-public crate-moleculec-go-0.1.4 (c (n "moleculec-go") (v "0.1.4") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "14ax1n985xl4hmp9x97hrf67s5y6vf9f68mg9afmsxb4fx58z19c")))

(define-public crate-moleculec-go-0.1.5 (c (n "moleculec-go") (v "0.1.5") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "1ni7yxmhdnv2yy0s5nwkh7hb2widjhs0n6qwn8zy4bg78m12cgb2")))

(define-public crate-moleculec-go-0.1.6 (c (n "moleculec-go") (v "0.1.6") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "17d6ng3xn6b09zxx6mdrqg5dhgc2hfl8dk8k6pv84vhr3llil13r")))

(define-public crate-moleculec-go-0.1.7 (c (n "moleculec-go") (v "0.1.7") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "10phbqgiwbn7npywplccyzds48j9nmdcaw1j4kyaa8dvx5pc37hz")))

(define-public crate-moleculec-go-0.1.8 (c (n "moleculec-go") (v "0.1.8") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "1gl2j9x305jxq6ny0igkgcyd733xy6gmssy60kiiwjhakbnx3y8f")))

(define-public crate-moleculec-go-0.1.9 (c (n "moleculec-go") (v "0.1.9") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "0hbhddg1y8gh0rg6i5zbmiq5jvy52grs0n5lxq5mnfgc269la8mi")))

(define-public crate-moleculec-go-0.1.10 (c (n "moleculec-go") (v "0.1.10") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("yaml" "cargo"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "18q6x6rd4pcvqsligl2azvy6n0cm0npb0ga1qbkbgb6v5zxc8174")))

(define-public crate-moleculec-go-0.1.11 (c (n "moleculec-go") (v "0.1.11") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "^0.7.5") (f (quote ("compiler-plugin"))) (d #t) (k 0)))) (h "1flr8rq1fdxzpdk5hqxcrb2h7w82jrv37nsmc17bnk3psr97vb2m")))

