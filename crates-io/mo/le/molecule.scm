(define-module (crates-io mo le molecule) #:use-module (crates-io))

(define-public crate-molecule-0.0.1 (c (n "molecule") (v "0.0.1") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)))) (h "1ha01czgpaki6797qndaj2gd7damp45fj8zqkyplmc2j61ihk698")))

(define-public crate-molecule-0.0.2 (c (n "molecule") (v "0.0.2") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)))) (h "1r795nbcqm2k6yq21cgryvzcc7kq3pkmchvx11nzkcbsfbipp65c")))

(define-public crate-molecule-0.0.3 (c (n "molecule") (v "0.0.3") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)))) (h "0d5h13ddfqw1samrbcqhwdskqq5zqapbi39mybq2yf75sc8491s8")))

(define-public crate-molecule-0.0.4 (c (n "molecule") (v "0.0.4") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)))) (h "1i9irq7kmxgwqpkcazydc0kbimryr9a7vja759fkvcm6ikj4ki3j")))

(define-public crate-molecule-0.0.5 (c (n "molecule") (v "0.0.5") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.3") (d #t) (k 0)))) (h "0ynwvkkwmmgknf9lvmxsyfhj5w500z51hj8lys6s99s7cqqrisc8")))

(define-public crate-molecule-0.1.0 (c (n "molecule") (v "0.1.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.3") (d #t) (k 0)))) (h "1bp0h4cbqzrp1s6nifnlwxvalqrrcgjc8j8b4y0s0ac3fxklpjg4")))

(define-public crate-molecule-0.2.0 (c (n "molecule") (v "0.2.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.3") (d #t) (k 0)))) (h "0y7kksway82jl6c9mdij4192fh01hpd6r77y1mjx7zchr48z9p6a")))

(define-public crate-molecule-0.2.5 (c (n "molecule") (v "0.2.5") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.3") (d #t) (k 0)))) (h "0cp4jf2nfdvw5czrqnpmixsw4x2pxk55hidi5p1jcz20rmryl2av")))

(define-public crate-molecule-0.3.0 (c (n "molecule") (v "0.3.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.3") (d #t) (k 0)))) (h "12rynycbh3bw9rhj1akfzznf3n7la1ncmzr1rjpng43dzzki4qq4")))

(define-public crate-molecule-0.3.1 (c (n "molecule") (v "0.3.1") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.4") (d #t) (k 0)))) (h "1ygs1dic0qgsj2wihjzsplqhflr334cnysqkzpdlgrymb177yh3n")))

(define-public crate-molecule-0.4.0 (c (n "molecule") (v "0.4.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "faster-hex") (r "~0.4") (d #t) (k 0)))) (h "0am8w01id7gliv7n8bs1lxzz6c93gzjs3dsaw3bi4gp1jfxij1ms")))

(define-public crate-molecule-0.5.0 (c (n "molecule") (v "0.5.0") (d (list (d (n "bytes") (r "~0.5") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "faster-hex") (r "~0.4") (o #t) (d #t) (k 0)))) (h "14ydnzrhx75cr3vpij81qkmw7nqhzsmldx7dj61w57p2k6dzk4jl") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.6.0 (c (n "molecule") (v "0.6.0") (d (list (d (n "bytes") (r "~0.5") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "faster-hex") (r "~0.4") (o #t) (d #t) (k 0)))) (h "161rcvgrfba1bj5qk71wa2l8myc9kp73qmpmw9bmk7i1ab67cgv6") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.6.1 (c (n "molecule") (v "0.6.1") (d (list (d (n "bytes") (r "~0.5") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "~0.1") (d #t) (k 0)) (d (n "faster-hex") (r "~0.4") (o #t) (d #t) (k 0)))) (h "1m9g7id5yia0r3z3440mc48nyxbfa3y89ifg4xscqpbz7nxcwrrh") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.7.0 (c (n "molecule") (v "0.7.0") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "14swv12bm6sia4jwpa6l5mf97i92xrbrgdkwzk0q3hfavc3ahd0j") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.7.1 (c (n "molecule") (v "0.7.1") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "00mzlby11pc2bybbnw824mxh1zfvwnd0k9jn5kkirmfhyi3i33f5") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.8.0-alpha (c (n "molecule") (v "0.8.0-alpha") (d (list (d (n "bytes") (r "^1.0.0") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "09almbs84d0a9jhpb3lldnykak5cabdcxk63zaissbigzd56r862") (f (quote (("std" "bytes/std" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.7.2 (c (n "molecule") (v "0.7.2") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0pszscwsvymx5zwrw06ikhrszvbcm5i98y3mv6nlw8i2chbn6pxy") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.7.3 (c (n "molecule") (v "0.7.3") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "008csb3pg07bdjlhrfphnphj217ki0d8rhniszdbs1m009n2gj7d") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.7.4 (c (n "molecule") (v "0.7.4") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0mxfzvsk0w5w1yl3vlji9whxr7hfl58dmasd8lg8gzdda8s99i4y") (f (quote (("std" "bytes" "faster-hex") ("default" "std")))) (y #t)))

(define-public crate-molecule-0.7.5 (c (n "molecule") (v "0.7.5") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "18yz96kr1qyhzp197q2jj1850g33vf5lrzvi1yj2wpsymdkrgzfl") (f (quote (("std" "bytes" "faster-hex") ("default" "std"))))))

(define-public crate-molecule-0.8.0 (c (n "molecule") (v "0.8.0") (d (list (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0c1p1zdc3f1gx6caxr3ajml9jz882g5ln40fb75g9gfhziz1rzkf") (f (quote (("std" "bytes/std" "faster-hex") ("default" "std") ("bytes_vec"))))))

