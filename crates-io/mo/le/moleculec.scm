(define-module (crates-io mo le moleculec) #:use-module (crates-io))

(define-public crate-moleculec-0.0.1 (c (n "moleculec") (v "0.0.1") (d (list (d (n "codegen") (r "= 0.0.1") (f (quote ("lang-all"))) (k 0) (p "molecule-codegen")))) (h "0wk62yvpa09wnm46yd65vb7mvjf2bcqyh68yaj64kw4h6vpd6lsd")))

(define-public crate-moleculec-0.0.2 (c (n "moleculec") (v "0.0.2") (d (list (d (n "codegen") (r "= 0.0.2") (f (quote ("lang-all"))) (k 0) (p "molecule-codegen")))) (h "1lkf5jr9gpgyj9czwaz4v26i1qsmx0ln4pigzgxal1ivj9gcqg2q")))

(define-public crate-moleculec-0.0.3 (c (n "moleculec") (v "0.0.3") (d (list (d (n "codegen") (r "= 0.0.3") (f (quote ("lang-all"))) (k 0) (p "molecule-codegen")))) (h "19kwwyvd7qqqvrhjrkfmqzdjxk6pdfx5y310i95xrkrr8pd55syh")))

(define-public crate-moleculec-0.0.4 (c (n "moleculec") (v "0.0.4") (d (list (d (n "codegen") (r "= 0.0.4") (d #t) (k 0) (p "molecule-codegen")))) (h "0da37nfrnzy44r0ydy31538vimxpggr0v4kys5fzv2xn9is9q3is")))

(define-public crate-moleculec-0.0.5 (c (n "moleculec") (v "0.0.5") (d (list (d (n "codegen") (r "= 0.0.5") (d #t) (k 0) (p "molecule-codegen")))) (h "08ypycl0hppf2rxz1xcrr7s9hb0grny8rb6z4zfld7m05r2m699a")))

(define-public crate-moleculec-0.1.0 (c (n "moleculec") (v "0.1.0") (d (list (d (n "codegen") (r "= 0.1.0") (d #t) (k 0) (p "molecule-codegen")))) (h "1jvc7ksavdqvn1r0l2r9vj726gpwiskid8dg8k7x4gr8hcfgjrbj")))

(define-public crate-moleculec-0.2.0 (c (n "moleculec") (v "0.2.0") (d (list (d (n "codegen") (r "= 0.2.0") (d #t) (k 0) (p "molecule-codegen")))) (h "0n85aig7vjx8p7dhczyhnbw0ngqg7kl1cw3lsqj51hhv7c1m79xg")))

(define-public crate-moleculec-0.2.1 (c (n "moleculec") (v "0.2.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.2.1") (d #t) (k 0) (p "molecule-codegen")))) (h "1gxh8316n6lrkz103rkj7z4ma9v1xj65w6myrn5h8zhqvzvdm9ms") (y #t)))

(define-public crate-moleculec-0.2.2 (c (n "moleculec") (v "0.2.2") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.2.2") (d #t) (k 0) (p "molecule-codegen")))) (h "1alv4knvjkha94i1z0s9y762vn489n131hwkwvldkyypryqa17rp")))

(define-public crate-moleculec-0.2.3 (c (n "moleculec") (v "0.2.3") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.2.3") (d #t) (k 0) (p "molecule-codegen")))) (h "1p9d3b2w8hwjmfx3v94azi4swkx7c4xwwp30d7xdqnsqqbjzm30p")))

(define-public crate-moleculec-0.2.4 (c (n "moleculec") (v "0.2.4") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.2.4") (d #t) (k 0) (p "molecule-codegen")))) (h "1ixsagxdxknwbh7m6b8fhcybi8cr56din4rmhl0i9rr8233lfwp7")))

(define-public crate-moleculec-0.2.5 (c (n "moleculec") (v "0.2.5") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.2.5") (d #t) (k 0) (p "molecule-codegen")))) (h "1ym1fgkyfsrbxk1fn6fmxa96fs9qyigp28q4g86wfwr944p6ficj")))

(define-public crate-moleculec-0.3.0 (c (n "moleculec") (v "0.3.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.3.0") (d #t) (k 0) (p "molecule-codegen")))) (h "0z4a79xb3x7x4q0gxrpgy0whvnf80d0j1d035jjsvbnryq187c7m")))

(define-public crate-moleculec-0.3.1 (c (n "moleculec") (v "0.3.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.3.1") (d #t) (k 0) (p "molecule-codegen")))) (h "1l04di6v6fkxnla2z5ldg727abda4vx25k9130fak4fg4cc94m6k")))

(define-public crate-moleculec-0.4.0 (c (n "moleculec") (v "0.4.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.4.0") (d #t) (k 0) (p "molecule-codegen")))) (h "0wwq2vl5zpl8mcf7kv6pwk460l45jij8vxnyiagnm5kp3krf3c1m")))

(define-public crate-moleculec-0.4.1 (c (n "moleculec") (v "0.4.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.4.1") (d #t) (k 0) (p "molecule-codegen")))) (h "0rjb2k3ax36qkpg4y9vxgyxx4mrpkx5skfz61gk0lwnr1k7wcfzx")))

(define-public crate-moleculec-0.4.2 (c (n "moleculec") (v "0.4.2") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "codegen") (r "= 0.4.2") (d #t) (k 0) (p "molecule-codegen")))) (h "17qdd5ivl8g07qf4plwhbqbhavfm9r5dbgfn2d4hbhssghf2jg16")))

(define-public crate-moleculec-0.5.0 (c (n "moleculec") (v "0.5.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "= 0.5.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "~3.1") (d #t) (k 0)))) (h "06114yxrig9q5zcbd2a9jsbqgfqx29y3vg4346jgfb4axz4qcq48")))

(define-public crate-moleculec-0.6.0 (c (n "moleculec") (v "0.6.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "= 0.6.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "~3.1") (d #t) (k 0)))) (h "0vbcj3mssnxzx1kxh8la69qr1qn7s23ffkmjhdlwjh1ffp1zx9iq")))

(define-public crate-moleculec-0.6.1 (c (n "moleculec") (v "0.6.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.6.1") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "~3.1") (d #t) (k 0)))) (h "0sy83nf7r81gnirrd1wi8rqcrjpy665h6rlygd609qrlsgk0wmvn")))

(define-public crate-moleculec-0.7.0 (c (n "moleculec") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "1xfnf2569lmigxcg6w2c9g06yjlnxirjyawwnx9wddrqcjcvlqw4")))

(define-public crate-moleculec-0.7.1 (c (n "moleculec") (v "0.7.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.1") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "11n47h7p68ah82k21fvsblp8bxrl2g3ismblxm18cx37kc0ynsy3")))

(define-public crate-moleculec-0.7.2 (c (n "moleculec") (v "0.7.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.2") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "1m8q2c0qdrsnlzqlnlg0fq6ysdjw6701dhrwl46y347lkmmw0v3h")))

(define-public crate-moleculec-0.7.3 (c (n "moleculec") (v "0.7.3") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.3") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "1rbzjx8jipz6i5zwq3yjilm51ba1pa8w8bxgppf0mhp8nprgba63")))

(define-public crate-moleculec-0.7.4 (c (n "moleculec") (v "0.7.4") (d (list (d (n "clap") (r "^3") (f (quote ("yaml" "cargo"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.4") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "18yma83y2fbknh37i24i94wqqbm7vv5zs6snhg1axbgi1ws15kph") (y #t)))

(define-public crate-moleculec-0.7.5 (c (n "moleculec") (v "0.7.5") (d (list (d (n "clap") (r "^3") (f (quote ("yaml" "cargo"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.7.5") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "1dlhgp49kbh9zqh2frihvbl64i4hpkhb4w5z722q0vj5bg5pgix5")))

(define-public crate-moleculec-0.8.0 (c (n "moleculec") (v "0.8.0") (d (list (d (n "clap") (r "^3") (f (quote ("yaml" "cargo"))) (d #t) (k 0)) (d (n "molecule-codegen") (r "=0.8.0") (f (quote ("compiler-plugin"))) (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "0hr0frz2zdq6834qfysnmyyg9wsagrif8gjdpv4xpvixkcfw2f2c")))

