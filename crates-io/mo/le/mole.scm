(define-module (crates-io mo le mole) #:use-module (crates-io))

(define-public crate-mole-0.1.0 (c (n "mole") (v "0.1.0") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.1") (d #t) (k 0)))) (h "0sackmg3w5kr85bjzr9ccg30ryvijls5q2hxh57r2kp1gvhsh2nm")))

(define-public crate-mole-0.1.1 (c (n "mole") (v "0.1.1") (d (list (d (n "glam") (r "^0.22.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.1") (d #t) (k 0)))) (h "0fnpna37ibkhbsfvbi7ildvx18ik66hbi5bp45v0himvl0yl78cz")))

