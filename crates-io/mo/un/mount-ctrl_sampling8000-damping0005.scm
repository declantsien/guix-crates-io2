(define-module (crates-io mo un mount-ctrl_sampling8000-damping0005) #:use-module (crates-io))

(define-public crate-mount-ctrl_sampling8000-damping0005-0.1.0 (c (n "mount-ctrl_sampling8000-damping0005") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xjqky9pzalam46d2wjqj6acwphhxvwphl1a451j3r2zazm7lyjb")))

