(define-module (crates-io mo un mount-api) #:use-module (crates-io))

(define-public crate-mount-api-0.1.0 (c (n "mount-api") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0cfxri8866r07v3lrw3gkxfwy551a1z3jhsj8yvnxcnq97lr8cc1")))

(define-public crate-mount-api-0.1.1 (c (n "mount-api") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "057hvnlks80lddkr38wirz1w8i2vdij82jkaazcnn8ah2f6kxx94")))

