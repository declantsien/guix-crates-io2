(define-module (crates-io mo un mount) #:use-module (crates-io))

(define-public crate-mount-0.0.2 (c (n "mount") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1v8w1whq03mqmav1icngvyxqb4msdrygf0579y2mm97xkzqvxl5h")))

(define-public crate-mount-0.0.3 (c (n "mount") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0l2nz2czkfg1vym3q1xyhrkb1gfszf502q254x87mfsfix684ygh")))

(define-public crate-mount-0.0.4 (c (n "mount") (v "0.0.4") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1ysj6vcmv2pccjk9kh6xfyncnq739s54lc0ygbnl334zpnglv08v")))

(define-public crate-mount-0.0.5 (c (n "mount") (v "0.0.5") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01x2scdrmjhfgl213jizy8krsicx7p0l1mx2lck2s5d51sry7hxx")))

(define-public crate-mount-0.0.6 (c (n "mount") (v "0.0.6") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "02l0g7xi6lsg8cylvabgw1calbgv2iifgb0rj1lm42w1s0yq8smp")))

(define-public crate-mount-0.0.7 (c (n "mount") (v "0.0.7") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "sequence_trie") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0gwmky4wfrj6albvzy6c6xr4l5569aaf7py30mkxvkf12siwcbfj")))

(define-public crate-mount-0.0.8 (c (n "mount") (v "0.0.8") (d (list (d (n "iron") (r "^0.1") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "16f67n8xryic4j96h0xh3sdp5s48ln85jypa5hssns5d34dxr9qd")))

(define-public crate-mount-0.0.9 (c (n "mount") (v "0.0.9") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "1bvalsp06jvy4cxzl3cknkxsvjk9cjgg2kf7qr8d8108ihdkwp65")))

(define-public crate-mount-0.0.10 (c (n "mount") (v "0.0.10") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)))) (h "0167i7cayhyjd4xh7y5n25kj57is9a462rb2596zfw2s4spid5nc")))

(define-public crate-mount-0.1.0 (c (n "mount") (v "0.1.0") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)))) (h "0ffx4nrhi3id2fxmlx6pc8m673d9868qh1iggl412dgz8h66764h")))

(define-public crate-mount-0.2.0 (c (n "mount") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)))) (h "10mxlrfhydbp6lzx3r6l2g12wzfrbzwksrmsqzdpxglnrjavq55q")))

(define-public crate-mount-0.2.1 (c (n "mount") (v "0.2.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.0") (d #t) (k 0)))) (h "1wn1yrngwaff2lirqrmmfk5mr6c12y6w0q2ivlfam8sxvwgfy665")))

(define-public crate-mount-0.3.0 (c (n "mount") (v "0.3.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.2") (d #t) (k 0)))) (h "0mj2h9x7gwvcaxzkzrw03q99zl6g2x1lrz02jn4rdl1wj8qmf91j")))

(define-public crate-mount-0.4.0 (c (n "mount") (v "0.4.0") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.3") (d #t) (k 0)))) (h "02vg71z11p4yb2rkxaw9qjgrzxlcqk2pxbrfbz3ziaj1540hcp72")))

