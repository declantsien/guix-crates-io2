(define-module (crates-io mo un mount_dir) #:use-module (crates-io))

(define-public crate-mount_dir-0.0.1 (c (n "mount_dir") (v "0.0.1") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "win_subst") (r "^0.0.2") (d #t) (k 0)))) (h "0z5y4pw4v6diradwskzx9xfd81v5fzc2hnji08prjqzcnyzwaihk")))

(define-public crate-mount_dir-0.0.2 (c (n "mount_dir") (v "0.0.2") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "win_subst") (r "^0.0.2") (d #t) (k 0)))) (h "036k1xlmr9hjk54m57hdz92a6zm1ixp0gv7ckicwcmz9nbdvr72p")))

(define-public crate-mount_dir-0.0.3 (c (n "mount_dir") (v "0.0.3") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "win_subst") (r "^0.0.2") (d #t) (k 0)))) (h "1rfnkzhvgbm514773kcjxzrfkl60j2ydj5206cs1r99iqnmqqmn1")))

(define-public crate-mount_dir-0.0.4 (c (n "mount_dir") (v "0.0.4") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "win_subst") (r "^0.0.3") (d #t) (k 0)))) (h "190bn3rg2ydnsqhnx54ibyfy4fdcfwgh6kfgdrm6v4bmn5nplkzr")))

