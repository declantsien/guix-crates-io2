(define-module (crates-io mo un mount-ctrl) #:use-module (crates-io))

(define-public crate-mount-ctrl-0.1.0 (c (n "mount-ctrl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "dosio") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "simulink-rs") (r "^0.1.0") (d #t) (k 0)))) (h "047v6klvwwlxbf8ciaqa5xqwwmp1jh1crad4fpnvg1rl39443bac") (f (quote (("pdr") ("default" "pdr") ("cdr"))))))

(define-public crate-mount-ctrl-0.1.1 (c (n "mount-ctrl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "dosio") (r "^0.1.3") (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "simulink-rs") (r "^0.1.0") (d #t) (k 0)))) (h "0wzhdzmgalcp25i13dj5crzrbdrjhlgn64cz4dp47va1dlc1fjwi") (f (quote (("pdr") ("dos-prqt" "dosio/prqt") ("default" "pdr" "dosio/default") ("cdr"))))))

(define-public crate-mount-ctrl-0.2.0 (c (n "mount-ctrl") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)))) (h "097cxsyz6wmyp20cbkpd7f0798pfsazchafzp17ybsfc3gw6g8k0") (f (quote (("pdr") ("default" "pdr") ("cdr"))))))

(define-public crate-mount-ctrl-0.2.1 (c (n "mount-ctrl") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)))) (h "1kgbfrzwliyy6ma5h2kqn0xsqsadpjf3p6wqfpapypv2fwd38201") (f (quote (("pdr") ("default" "pdr") ("cdr"))))))

(define-public crate-mount-ctrl-0.3.0 (c (n "mount-ctrl") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "13fgmbsn694gp80a5l2hgsdc4ms0890rg1w0yxh219vqlvd2mpnh")))

(define-public crate-mount-ctrl-1.0.0 (c (n "mount-ctrl") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "sampling1000-damping002") (r "^0.1.0") (o #t) (d #t) (k 0) (p "mount-ctrl_sampling1000-damping002")) (d (n "sampling8000-damping0005") (r "^0.1.0") (o #t) (d #t) (k 0) (p "mount-ctrl_sampling8000-damping0005")) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d4b5klplqv5pgldnblwwcc4fx0ri0q5x94008pmm09l3ca3jpsk") (f (quote (("s8000d0005" "sampling8000-damping0005") ("s1000d002" "sampling1000-damping002"))))))

