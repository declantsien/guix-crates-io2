(define-module (crates-io mo un mountpoints) #:use-module (crates-io))

(define-public crate-mountpoints-0.1.0 (c (n "mountpoints") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.93") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "fileapi" "winerror" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0dyb6jbvq3wjxwqdjsh30ld1w96yn9kfav0l43qdbk9w4282hi8g")))

(define-public crate-mountpoints-0.1.1 (c (n "mountpoints") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.93") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "fileapi" "winerror" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "05nblan5qrnmppay5q7cn00fb0kv4x1m8xarq83pi197n2d5zwrq")))

(define-public crate-mountpoints-0.2.0 (c (n "mountpoints") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.93") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "fileapi" "winerror" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "166bqsmibjvyf4dmhgw8pw3g2b36hgf9a5p9lllnl2jk7g5366nh")))

(define-public crate-mountpoints-0.2.1 (c (n "mountpoints") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.93") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "fileapi" "winerror" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1icfxfcfrgyql4lmba1n8al780zdscswfmwsv3nprc16abmf741h")))

