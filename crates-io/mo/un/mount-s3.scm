(define-module (crates-io mo un mount-s3) #:use-module (crates-io))

(define-public crate-mount-s3-0.0.0 (c (n "mount-s3") (v "0.0.0") (h "1i711r0ns2b8f666wgi8zgwz0dhj6ffvgc5lk2260b3k8iy3kdc7")))

(define-public crate-mount-s3-0.0.1 (c (n "mount-s3") (v "0.0.1") (h "1dqq7ijd2i5prqaswhxwnyscjwbrijksg8fk1f64jljdcl8bxr99")))

