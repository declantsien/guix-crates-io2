(define-module (crates-io mo un mountpoint-s3) #:use-module (crates-io))

(define-public crate-mountpoint-s3-0.0.0 (c (n "mountpoint-s3") (v "0.0.0") (h "0cbs34mzl2x952j6s95bbzkblnw9jmhzsrkcz6wf9sgjm1sk1kw0")))

(define-public crate-mountpoint-s3-0.0.1 (c (n "mountpoint-s3") (v "0.0.1") (h "0mbzv6b4k1l51yhn9jwcjqkqmv8wqpbqp5zxyc2llzv89n9hy0x1")))

