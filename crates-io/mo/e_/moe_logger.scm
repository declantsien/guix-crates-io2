(define-module (crates-io mo e_ moe_logger) #:use-module (crates-io))

(define-public crate-moe_logger-0.1.0 (c (n "moe_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio-uring") (r "^0.1.0") (d #t) (k 0)))) (h "1q96s6xfg9ijxnhlnl04s0cqg8iqlh170xw7v682y59w96fqbaf1")))

(define-public crate-moe_logger-0.2.0 (c (n "moe_logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio-uring") (r "^0.1.0") (d #t) (k 0)))) (h "1wjwwk1ld13x2n1pgj40d8kczihbjl081yawzx32qvx9yyy47zkg")))

