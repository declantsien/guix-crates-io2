(define-module (crates-io mo dd moddef) #:use-module (crates-io))

(define-public crate-moddef-0.1.0 (c (n "moddef") (v "0.1.0") (h "19gw5d1ac76hx7ldblbjz6dqhzf15a1xd3qdrpkm0036vk44m2iy")))

(define-public crate-moddef-0.2.0 (c (n "moddef") (v "0.2.0") (h "0wxzd7y6a0cb4dgc9hkdvxh6swwq35y5iysryshbrl1xdw6s9wm8")))

(define-public crate-moddef-0.2.1 (c (n "moddef") (v "0.2.1") (h "0m5494kq3xmic1wbmh2qd7x0mnqkqbb8mi89nbgivr256abck742")))

(define-public crate-moddef-0.2.2 (c (n "moddef") (v "0.2.2") (h "1hp5qajcp0dplzlw9mnr4iy1l8smjdhlhnshq7qn66q64bvhxljy")))

