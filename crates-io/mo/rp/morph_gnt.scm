(define-module (crates-io mo rp morph_gnt) #:use-module (crates-io))

(define-public crate-morph_gnt-0.1.0 (c (n "morph_gnt") (v "0.1.0") (d (list (d (n "bible_ref") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.27.0") (d #t) (k 0)))) (h "1rabix6mw7id9ajijy2y9bhmnf9xz641xrw37y5qvs36blfam82q")))

(define-public crate-morph_gnt-0.1.1 (c (n "morph_gnt") (v "0.1.1") (d (list (d (n "bible_ref") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.27.0") (d #t) (k 0)))) (h "04dq3gbgih4pkb1r1r9gr6m3q3y540p34y1jagr3bnjw4ahw7i9k")))

(define-public crate-morph_gnt-0.1.2 (c (n "morph_gnt") (v "0.1.2") (d (list (d (n "bible_ref") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.27.0") (d #t) (k 0)))) (h "1ha8z7gnv6wjhv455lz80y9pqwy8k3d5iqsijzigmyz2swkhkvc9")))

