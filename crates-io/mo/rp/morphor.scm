(define-module (crates-io mo rp morphor) #:use-module (crates-io))

(define-public crate-morphor-0.0.0 (c (n "morphor") (v "0.0.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1jw0rk1pfpna7k3vsbk00328aci2l2vayh9vwgd7frpwiwi9px49") (y #t)))

