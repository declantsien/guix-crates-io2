(define-module (crates-io mo rp morphism) #:use-module (crates-io))

(define-public crate-morphism-0.0.1 (c (n "morphism") (v "0.0.1") (h "1q8xynpmnp0qb5jqsdrpxqj6fz78alifpddz63kxbx4pkqbhpx2p")))

(define-public crate-morphism-0.2.0 (c (n "morphism") (v "0.2.0") (h "1xk66j79gl46nazhy5llaw4wxbv2irfsdhq0l359l25rcmyv7bsh")))

(define-public crate-morphism-0.2.1 (c (n "morphism") (v "0.2.1") (h "0h30ns9i0p4frnva95mkpcdz2kdjp6859nzvjmnvac1233wbsrf6")))

(define-public crate-morphism-0.2.2 (c (n "morphism") (v "0.2.2") (h "1bxzf92gzdp5xl39f8s3bahsszp23rk4gagfxnrbh8ybmnvbqfky")))

(define-public crate-morphism-0.3.0 (c (n "morphism") (v "0.3.0") (h "1vfzdsddlc59nhsggjd6lp40gi46prs3vgkb6gjrdha2lyks9a1r")))

(define-public crate-morphism-0.4.0 (c (n "morphism") (v "0.4.0") (h "1ar84i5b66kcrp9xkc5wvqa3yry0wpf1aml2kz23ispfp2g5dps3")))

(define-public crate-morphism-0.4.1 (c (n "morphism") (v "0.4.1") (h "1azy7asc225z73srq6vl46qyq45ch0ybylgg5h157p6pj4gzszil")))

