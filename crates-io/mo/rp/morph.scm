(define-module (crates-io mo rp morph) #:use-module (crates-io))

(define-public crate-morph-0.1.0 (c (n "morph") (v "0.1.0") (d (list (d (n "clap") (r "^2.12.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.4.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.4.0") (d #t) (k 0)) (d (n "tremor") (r "^0.1.2") (d #t) (k 0)))) (h "04p2kdrjw9ch0ylbdw02jjxlg2k7nq3pj7w14gh6kzgjp1m14vp5")))

