(define-module (crates-io mo rp morp) #:use-module (crates-io))

(define-public crate-morp-0.1.0 (c (n "morp") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0s6gfrbm89yz2cyqd6xk1bn9i1w1s3w7wi4j0sixbzcrksxhricp")))

