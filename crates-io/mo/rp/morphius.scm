(define-module (crates-io mo rp morphius) #:use-module (crates-io))

(define-public crate-morphius-1.0.0 (c (n "morphius") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mexprp") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0djhpjhisz8h0gsjq0kd8nb5fhmgj1j8mrylgd29xsv8cr0n0bn8")))

