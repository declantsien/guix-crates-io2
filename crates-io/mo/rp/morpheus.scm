(define-module (crates-io mo rp morpheus) #:use-module (crates-io))

(define-public crate-morpheus-0.1.0 (c (n "morpheus") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.0-alpha.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "0cry3k2f7srzxs9fzb1gjl7sm49k8vs01l3g46ai2ijk9qnyada9")))

