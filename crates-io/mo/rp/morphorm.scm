(define-module (crates-io mo rp morphorm) #:use-module (crates-io))

(define-public crate-morphorm-0.1.0 (c (n "morphorm") (v "0.1.0") (h "08hn9650ip3c02m2i9ikgqv5crcmyxhhdqnlsfk5f4p0z81hca74")))

(define-public crate-morphorm-0.2.0 (c (n "morphorm") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0s77ddypqirbrj5i81gwy5qfz6lmrabsy9c1qfkl8nn5pdlj1lly")))

(define-public crate-morphorm-0.3.0 (c (n "morphorm") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "femtovg") (r "^0.2.2") (k 2)) (d (n "glutin") (r "^0.27.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "winit") (r "^0.25") (k 2)))) (h "0qqiqg0gnwp4vfxj04aq7lmaqwqybzfpm3dc3jnm083p9ankzws3") (f (quote (("rounding"))))))

(define-public crate-morphorm-0.6.0 (c (n "morphorm") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "femtovg") (r "^0.7.1") (k 2)) (d (n "glutin") (r "^0.30.10") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.6") (k 2)))) (h "0nclylcmzjn9b4wqjmkph4w84zz3ynjm53cpykiyg9j57sk3z543")))

(define-public crate-morphorm-0.6.1 (c (n "morphorm") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "femtovg") (r "^0.7.1") (k 2)) (d (n "glutin") (r "^0.30.10") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.6") (k 2)))) (h "02avx26prl7sjw387h9120bihzn21paavsxfqkj6p95z3rv9r8i5") (y #t)))

(define-public crate-morphorm-0.6.2 (c (n "morphorm") (v "0.6.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "femtovg") (r "^0.7.1") (k 2)) (d (n "glutin") (r "^0.30.10") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.6") (k 2)))) (h "04jpjz8d6vs0hbvc8r06ii6qrv8w1xgsqlb0nxspvvghfcblpg7a")))

(define-public crate-morphorm-0.6.3 (c (n "morphorm") (v "0.6.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "femtovg") (r "^0.7.1") (k 2)) (d (n "glutin") (r "^0.30.10") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.6") (k 2)))) (h "0kn0bxsnnx623w55ay8sfa6w2zi9bir4w71y80pwyyclnvp5j1ax")))

(define-public crate-morphorm-0.6.4 (c (n "morphorm") (v "0.6.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "femtovg") (r "^0.7.1") (k 2)) (d (n "glutin") (r "^0.30.10") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.6") (k 2)))) (h "0rikwdlg95cp7lnqnym2sj5mic452fh7qryl1sp9i23cn9wvk3v1")))

