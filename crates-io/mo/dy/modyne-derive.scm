(define-module (crates-io mo dy modyne-derive) #:use-module (crates-io))

(define-public crate-modyne-derive-0.1.0 (c (n "modyne-derive") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0inr1kggq32qsvxbhw5aw22jrhv1a02qq2sjqv4jrhd23xyycgbk") (r "1.66.1")))

(define-public crate-modyne-derive-0.2.0 (c (n "modyne-derive") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0ddligvihx6mmhqgfwj4p6rm1ysfd8fa15ijf7853m8gvr428gah") (r "1.67.0")))

(define-public crate-modyne-derive-0.2.1 (c (n "modyne-derive") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "00p96541mfwqkrf24aa25ayhzp0a3i7bc7q1015gy7dva786rnjc") (r "1.67.0")))

(define-public crate-modyne-derive-0.3.0 (c (n "modyne-derive") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1jmpplsvfbfggs9vsndzd9ncyxcqwfp0ldym3dm7sy917q71a51w") (r "1.68.0")))

