(define-module (crates-io mo sh moshimoshi) #:use-module (crates-io))

(define-public crate-moshimoshi-0.1.0 (c (n "moshimoshi") (v "0.1.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "17csxpij89sv2gsgl3aw27n1ba8fh6khbdlapya4vx92r02zs8yz")))

(define-public crate-moshimoshi-0.2.0 (c (n "moshimoshi") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)))) (h "1fa198nhaagmyy0ibjl0b1hfichkwvh8g525ifvi8ln1r7mdarid")))

