(define-module (crates-io mo sh mosh-server-upnp) #:use-module (crates-io))

(define-public crate-mosh-server-upnp-0.0.1 (c (n "mosh-server-upnp") (v "0.0.1") (d (list (d (n "get_if_addrs") (r "^0.4.0") (d #t) (k 0)) (d (n "igd") (r "^0.5.1") (d #t) (k 0)))) (h "1wqkkq6rv47q2wy89lxm0i0lklr3xiilqmry556apl671yi1k8xy")))

(define-public crate-mosh-server-upnp-0.1.0 (c (n "mosh-server-upnp") (v "0.1.0") (d (list (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.7.0") (d #t) (k 0)))) (h "10mm7x378k621aq79nfwsqsl491k7gbc949hi0wxc6f80nj7ygz9")))

(define-public crate-mosh-server-upnp-0.2.0 (c (n "mosh-server-upnp") (v "0.2.0") (d (list (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.11.0") (d #t) (k 0)))) (h "0r70vrivw1kp298ryivnsvkslf2kdlvhhr56ix8gvpq2l34p5c3y")))

