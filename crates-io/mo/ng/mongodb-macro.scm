(define-module (crates-io mo ng mongodb-macro) #:use-module (crates-io))

(define-public crate-mongodb-macro-0.1.0 (c (n "mongodb-macro") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mongodb") (r ">=2") (d #t) (k 0)))) (h "0cwv2cv7wndlf3rflyacjqsz5yr5hf2yp8sl8dbj88hnvizf223l")))

(define-public crate-mongodb-macro-0.2.0 (c (n "mongodb-macro") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "0q5sfn89k0irwh8q03xr96dbiirkkg47sh256hg6j6dhrljl5g28")))

(define-public crate-mongodb-macro-0.3.0 (c (n "mongodb-macro") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "0qgsrjrgzxm2y74lvm9qyszdi6xr0vc745arpf2xinknjy6lclhb")))

(define-public crate-mongodb-macro-1.0.0 (c (n "mongodb-macro") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "0k7ij1pbvk8vsld7rg75g1pfbiqb6ndqbsk2hxwk8qnjz7wdq44x")))

(define-public crate-mongodb-macro-1.0.1 (c (n "mongodb-macro") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "1lfs24wkxczzjbsz6n95hjgyk6yskigjscmhdcfqxcca1nmqlsq7")))

(define-public crate-mongodb-macro-1.0.2 (c (n "mongodb-macro") (v "1.0.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "0wxg7y5kazr8d8iz1nbqv1rrf86bc2kq42vhm64nzg4zr9qz619y")))

(define-public crate-mongodb-macro-1.0.3 (c (n "mongodb-macro") (v "1.0.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "mongodb") (r ">=2") (d #t) (k 2)))) (h "1yhdzr51d0959aps6gzhr3742kkwsnd17sbpf8gdafpb7i6djc88")))

(define-public crate-mongodb-macro-1.0.4 (c (n "mongodb-macro") (v "1.0.4") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mongodb") (r ">=2") (d #t) (k 2)) (d (n "nested-env-parser") (r "^1.1.0") (d #t) (k 0)))) (h "05msqc6z4iy1mavbzyswikms90px6fv53bhjyr5v3gri9vs9icnz")))

