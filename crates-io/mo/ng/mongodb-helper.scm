(define-module (crates-io mo ng mongodb-helper) #:use-module (crates-io))

(define-public crate-mongodb-helper-0.1.0 (c (n "mongodb-helper") (v "0.1.0") (d (list (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ps381gx9xj7429lgij81njw0xr7jssam2265dgfr6wndyfh21qd")))

(define-public crate-mongodb-helper-0.1.1 (c (n "mongodb-helper") (v "0.1.1") (d (list (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "192qvxrm8ljvpfm7varck3qwdiplgvlkr6kb41if33xf72wyx3hn")))

(define-public crate-mongodb-helper-0.1.2 (c (n "mongodb-helper") (v "0.1.2") (d (list (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "00q1gf5s77rcmz1sra55jsrvp0vqpd85q21ldi80g9jy3my74qwl")))

(define-public crate-mongodb-helper-0.1.3 (c (n "mongodb-helper") (v "0.1.3") (d (list (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "01kj3rvfzdsss8q7vg4ghnjvksylf9i9vbvi9q3p8g9ah6djdqi3")))

(define-public crate-mongodb-helper-0.1.4 (c (n "mongodb-helper") (v "0.1.4") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0qb08lz29gd01a4dx2n94v2595yrag5rcvvq6zspckps0cspj3cr")))

(define-public crate-mongodb-helper-0.1.5 (c (n "mongodb-helper") (v "0.1.5") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "12y42zzdsk8dls9fkyjj71kql8s8239rgzr894xjy581399i35i5")))

(define-public crate-mongodb-helper-0.1.6 (c (n "mongodb-helper") (v "0.1.6") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-mongodb") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1k6j9xsnc3xvjy3kpn8288z7ralvn2c307w5skl18xm8s1g10qbc")))

(define-public crate-mongodb-helper-0.1.7 (c (n "mongodb-helper") (v "0.1.7") (d (list (d (n "bson") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-alpha") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0an2zpzq7csfasynpz8r6g5psr60cf52biciqh69hi406ng5a0ad") (y #t)))

