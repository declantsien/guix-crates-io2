(define-module (crates-io mo ng mongocrypt-sys) #:use-module (crates-io))

(define-public crate-mongocrypt-sys-0.1.0+1.6.1 (c (n "mongocrypt-sys") (v "0.1.0+1.6.1") (d (list (d (n "bson") (r "^2.2.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1g8vz47f84wwpqqdf1k8mb2lk1v070vg6salafq5m3a74379hdvp") (l "mongocrypt")))

(define-public crate-mongocrypt-sys-0.1.1+1.8.0-alpha0 (c (n "mongocrypt-sys") (v "0.1.1+1.8.0-alpha0") (d (list (d (n "bson") (r "^2.6.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0ig9q65rmfbm6y6zyxcq4kadhfckmidjh5720lnzsva31ls4m6dw") (l "mongocrypt")))

(define-public crate-mongocrypt-sys-0.1.2+1.8.0-alpha1 (c (n "mongocrypt-sys") (v "0.1.2+1.8.0-alpha1") (d (list (d (n "bson") (r "^2.6.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "1abaqqpz4lcaxgn781ny3ni04c6ncvsdzqjzvrnrhqjknpjgn53a") (l "mongocrypt")))

