(define-module (crates-io mo ng mongo_indexed) #:use-module (crates-io))

(define-public crate-mongo_indexed-0.1.0 (c (n "mongo_indexed") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "1r01m0y47nsbvaa0a4qa3nzyz19jyxbbnw7abnnqmfr4ah6v00ra")))

(define-public crate-mongo_indexed-0.1.1 (c (n "mongo_indexed") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "0j2rcglh6hxbsgn7h7fraagjbgj9yak9xnvjyi7dbiqnclzj7q0s")))

(define-public crate-mongo_indexed-0.2.0 (c (n "mongo_indexed") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "0p4kh7bvzhlnjncw9cp8zgfh76y4lj61hcksxcl09m79inj5jxag")))

(define-public crate-mongo_indexed-0.2.1 (c (n "mongo_indexed") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0y70j9wyzc4axrw4c24v654k1r3qr4bng08jwbhh9jcib5psawyr")))

(define-public crate-mongo_indexed-0.2.2 (c (n "mongo_indexed") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "04vmkdhj7a6alfjrwq0pkz3hwdyiwl2g96b83rsw92jaq0d5gkgh")))

(define-public crate-mongo_indexed-0.3.0 (c (n "mongo_indexed") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "mongo_indexed_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)))) (h "1nvagvr61ng3ylc3niycwzgjn969q4i6y1lqwbba1336vihpk0ry")))

