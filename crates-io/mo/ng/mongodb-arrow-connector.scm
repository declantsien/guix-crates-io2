(define-module (crates-io mo ng mongodb-arrow-connector) #:use-module (crates-io))

(define-public crate-mongodb-arrow-connector-0.1.0 (c (n "mongodb-arrow-connector") (v "0.1.0") (d (list (d (n "arrow") (r "^0.16.0") (d #t) (k 0)) (d (n "bson") (r "^0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)))) (h "1czy1qf64kn0cdacsczswrqny4rnndzd4xbwzg6c5cqxw8dvbbcz")))

(define-public crate-mongodb-arrow-connector-0.2.0 (c (n "mongodb-arrow-connector") (v "0.2.0") (d (list (d (n "arrow") (r "^1.0") (k 0)) (d (n "bson") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mongodb") (r "^1.0") (f (quote ("sync"))) (k 0)))) (h "11xh930k8g9z8hwlkwzxc1npx8zgf8gd99qsj0i6gl0dcyv017kd")))

(define-public crate-mongodb-arrow-connector-0.3.0 (c (n "mongodb-arrow-connector") (v "0.3.0") (d (list (d (n "arrow") (r "^4.0") (k 0)) (d (n "bson") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mongodb") (r "^1.0") (f (quote ("sync"))) (k 0)))) (h "037pvc7rqf3zp2l5rf77gi1wbyfb2m75mf7z5c8ks75v4ikv0fmi")))

(define-public crate-mongodb-arrow-connector-0.4.0 (c (n "mongodb-arrow-connector") (v "0.4.0") (d (list (d (n "arrow") (r "^5.0") (k 0)) (d (n "arrow") (r "^5.0") (f (quote ("csv"))) (k 2)) (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0pc8vn25caacs60arws09y72sl2pagkimn1lnh0zws9n0k2nzajy")))

(define-public crate-mongodb-arrow-connector-0.5.0 (c (n "mongodb-arrow-connector") (v "0.5.0") (d (list (d (n "arrow") (r "^6") (k 0)) (d (n "arrow") (r "^6") (f (quote ("csv"))) (k 2)) (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "03rhaq4dhxk6abkj2ycgwmny29022i7jsdfhhi0vzpmnyrblp6vd")))

(define-public crate-mongodb-arrow-connector-0.6.0 (c (n "mongodb-arrow-connector") (v "0.6.0") (d (list (d (n "arrow") (r "^11") (k 0)) (d (n "arrow") (r "^11") (f (quote ("csv"))) (k 2)) (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "073cjr9v3vfm91hx614si3x7ngziql0zprjz6ib4c3wwdx2ijm2v")))

(define-public crate-mongodb-arrow-connector-0.7.0 (c (n "mongodb-arrow-connector") (v "0.7.0") (d (list (d (n "arrow") (r ">=7, <13") (k 0)) (d (n "arrow") (r ">=7, <13") (f (quote ("csv"))) (k 2)) (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0hnaa4d97vc2vvjndngq4sfabrjvsc3zwda4y712qf230w0s8nag")))

