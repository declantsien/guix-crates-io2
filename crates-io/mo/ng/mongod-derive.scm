(define-module (crates-io mo ng mongod-derive) #:use-module (crates-io))

(define-public crate-mongod-derive-0.1.0 (c (n "mongod-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rqmb87f9ywxmk6g6lh0dzj8d2armln952qmww3kqdckzhydxg2n")))

(define-public crate-mongod-derive-0.1.1 (c (n "mongod-derive") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c77nvp1gyz6kasy4nvk2qsiq601k2jacrziy5gpcsnmds6papm5")))

(define-public crate-mongod-derive-0.1.2 (c (n "mongod-derive") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x3c26qk96fkmzwrcc28bmgl8myqmnasny7icdpzc0ayp1i6h7nj")))

(define-public crate-mongod-derive-0.2.0 (c (n "mongod-derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yfrs94vv7grvikpvlghlhiz5iibba7xjrw1gm81cwp0wwqnahhn")))

(define-public crate-mongod-derive-0.2.1 (c (n "mongod-derive") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k2fpyrlyiw8l8x4blaignxl6rjl1lzdan33c6xbx1y5ccgysq3w")))

(define-public crate-mongod-derive-0.2.2 (c (n "mongod-derive") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1117fsvfifj05lahyagjija93wf2jdjmrazx0hasyj5qa2sg6slw")))

(define-public crate-mongod-derive-0.3.0 (c (n "mongod-derive") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13l7bgqdk8m51miqmdvb1lz7z0p8br61afszas91xi742qc6gnnp")))

(define-public crate-mongod-derive-0.3.1 (c (n "mongod-derive") (v "0.3.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h1wgc5d03hxg2gjpndkdb00ivg8a89yl7rffb3c2vk69c6b1f7s")))

(define-public crate-mongod-derive-0.3.2 (c (n "mongod-derive") (v "0.3.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cn5f667h6667bh5x1zc78cmq5sb01wjpr5i4rw8yaw5k5qjq1dx")))

(define-public crate-mongod-derive-0.3.3 (c (n "mongod-derive") (v "0.3.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nch0ligis7j1ynp888p2rg6a1daj9gjbqyh370j395kwb70zs9n")))

(define-public crate-mongod-derive-0.3.4 (c (n "mongod-derive") (v "0.3.4") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r7s65pbbnd9r24iilm1m8278s4dzs1qs1glsl1gm42qjjrb49xr")))

(define-public crate-mongod-derive-0.3.5 (c (n "mongod-derive") (v "0.3.5") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ciiyns6rnp4hkkgjgnqnb0fcapll6s0ixki3lrhpcjm9lbzm3vm")))

