(define-module (crates-io mo ng mongo-lock-async) #:use-module (crates-io))

(define-public crate-mongo-lock-async-0.1.0 (c (n "mongo-lock-async") (v "0.1.0") (d (list (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "110d72ksp96i7kmwqi7mk2xcmzjxfg5iw7f0bp1xn17lc68lc9f1") (y #t)))

(define-public crate-mongo-lock-async-0.2.0 (c (n "mongo-lock-async") (v "0.2.0") (d (list (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1m3abz6262dkypyc89qgw82b8fa7zh2gdvhbscp1xmg1r0sikv8v") (y #t)))

(define-public crate-mongo-lock-async-0.2.1 (c (n "mongo-lock-async") (v "0.2.1") (d (list (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1azc7wrkq36a183mk3w3sxcrbcfcljfj1dyancs119hq7x9ch7nd")))

(define-public crate-mongo-lock-async-0.2.2 (c (n "mongo-lock-async") (v "0.2.2") (d (list (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1lhrggfpq7029k9122msq5n2r1dn8gyrzh5wc0vfskwpk7k21dyq")))

(define-public crate-mongo-lock-async-0.3.0 (c (n "mongo-lock-async") (v "0.3.0") (d (list (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "15zr7zmlmg03iigbybjal4vqbj9fbxia6d7plpc5jfmlif15iz6q")))

