(define-module (crates-io mo ng mongodb-atlas-admin) #:use-module (crates-io))

(define-public crate-mongodb-atlas-admin-0.0.0 (c (n "mongodb-atlas-admin") (v "0.0.0") (h "14wc82q588q5wjk6snc2ksrg9zdvxdjibkrisfykn9v113s1s6qh")))

(define-public crate-mongodb-atlas-admin-0.0.1 (c (n "mongodb-atlas-admin") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "digest_auth") (r "^0.3") (f (quote ("http"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1qfyls71mcbiq09vwb8r8gnb7iclis80ndvbmsv77zvwj6m12a68")))

