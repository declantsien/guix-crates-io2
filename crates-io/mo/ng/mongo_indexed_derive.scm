(define-module (crates-io mo ng mongo_indexed_derive) #:use-module (crates-io))

(define-public crate-mongo_indexed_derive-0.1.0 (c (n "mongo_indexed_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0q3y69df2sz3bilg8v69krb0i1i04xmz11w7kwk9jn56c69n5in0")))

(define-public crate-mongo_indexed_derive-0.2.0 (c (n "mongo_indexed_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0j58m4mq1xdp5ccjjiiqgr2mxb0pfm2jjjhijslj7br7c6la4zkk")))

(define-public crate-mongo_indexed_derive-0.2.2 (c (n "mongo_indexed_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03lrx5nj5f7nhdpc3hrn1wx451bj49mphqvmhxiql7p014s78a6s")))

(define-public crate-mongo_indexed_derive-0.3.0 (c (n "mongo_indexed_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1s647msq4lnjg9z0bqcb15sw2122fnswqxviv1wdp2jwnsags6rb")))

