(define-module (crates-io mo ng mongo-leaf) #:use-module (crates-io))

(define-public crate-mongo-leaf-0.1.0 (c (n "mongo-leaf") (v "0.1.0") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mongo-c-sys") (r "^1.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1m9b6mkjip324d4hfl4r69qzmfhn02p3x03hv8q26gk8vxm9vhl3")))

(define-public crate-mongo-leaf-0.1.1 (c (n "mongo-leaf") (v "0.1.1") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mongo-c-sys") (r "^1.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0r90dw7my2mcdill0d4935r2frw1iix518xxjj8vnxk3x8ibhz79")))

(define-public crate-mongo-leaf-0.1.2 (c (n "mongo-leaf") (v "0.1.2") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mongo-c-sys") (r "^1.16.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0wzf8d5km3zrfqf6flrw66da0zq1533sfyvpgcq4xwif9v5lcq7x")))

(define-public crate-mongo-leaf-0.1.3 (c (n "mongo-leaf") (v "0.1.3") (d (list (d (n "bson") (r "^0.14.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mongo-c-sys") (r "^1.16.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0qr2n34yns8b3lim9bg3gsj1fi60g274d8hm4ic3wwvjrd20pcc2")))

