(define-module (crates-io mo ng mongoloid) #:use-module (crates-io))

(define-public crate-mongoloid-0.1.0 (c (n "mongoloid") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "bson") (r "^0.10") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "0f9wm0vr5p0hj860pzgkrn837bxxws13v65d055nwb0c31s3fvyf")))

(define-public crate-mongoloid-0.1.1 (c (n "mongoloid") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "bson") (r "^0.10") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "16bpaxpnprd5i750612ifn65riy9ssvhlrfrjw60njr78w2fhjy6")))

