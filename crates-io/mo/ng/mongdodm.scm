(define-module (crates-io mo ng mongdodm) #:use-module (crates-io))

(define-public crate-mongdodm-0.9.2 (c (n "mongdodm") (v "0.9.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2") (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (d #t) (k 2)))) (h "0a2bxq3asph935zpvp9ng3p3f88j2f2czplf20r56lriyqv5qgbs") (f (quote (("tokio-runtime" "mongodb/tokio-runtime") ("default" "tokio-runtime") ("chrono-0_4" "mongodb/bson-chrono-0_4") ("async-std-runtime" "mongodb/async-std-runtime")))) (y #t)))

