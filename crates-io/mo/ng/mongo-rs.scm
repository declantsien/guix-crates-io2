(define-module (crates-io mo ng mongo-rs) #:use-module (crates-io))

(define-public crate-mongo-rs-0.1.1 (c (n "mongo-rs") (v "0.1.1") (d (list (d (n "bson") (r "^0.1.0") (d #t) (k 0)) (d (n "mongo-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0nf8fc9jdwfpvq3kxmw8zg483cpc9ri5290ckq7j982f9m84ab17")))

