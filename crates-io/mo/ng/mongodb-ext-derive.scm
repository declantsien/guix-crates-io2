(define-module (crates-io mo ng mongodb-ext-derive) #:use-module (crates-io))

(define-public crate-mongodb-ext-derive-0.1.0 (c (n "mongodb-ext-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11h9cnwcx6lq3ddhnp2k5zsqdl0jxgslz0i5920vng6w6qqxz8wq")))

(define-public crate-mongodb-ext-derive-0.1.1 (c (n "mongodb-ext-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17b7wl51zls2i485n9xr3ns61mr93849162ndb8i9iw0r2771ylv")))

(define-public crate-mongodb-ext-derive-1.1.1 (c (n "mongodb-ext-derive") (v "1.1.1") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pxpcqgksvqwnsq400a8bpa97hsqz50r5c94mdrzqn9dx4npx97r")))

(define-public crate-mongodb-ext-derive-1.1.2 (c (n "mongodb-ext-derive") (v "1.1.2") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08bki3frss95bvzj28mx12253a8y8ipa6pblwdsfq0xxpzr25h7z")))

