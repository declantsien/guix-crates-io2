(define-module (crates-io mo ng mongo-sys) #:use-module (crates-io))

(define-public crate-mongo-sys-0.1.0 (c (n "mongo-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0qby8x9gd8vr7zd8107xsgkvwh0rmjd6z58pf1qpbph94bps78sp")))

(define-public crate-mongo-sys-0.1.1 (c (n "mongo-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "1a46g73kq9w3xx4hgi27ba6id20qfxvswr2prgayl1n3gz0fprvh")))

