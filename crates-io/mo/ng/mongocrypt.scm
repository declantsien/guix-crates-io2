(define-module (crates-io mo ng mongocrypt) #:use-module (crates-io))

(define-public crate-mongocrypt-0.1.0 (c (n "mongocrypt") (v "0.1.0") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "mongocrypt-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "04cbz43djn1mgvz8kyr3prybhskc3d34h9jlj1czqqcxypgx63m0") (f (quote (("compile_fail"))))))

(define-public crate-mongocrypt-0.1.1 (c (n "mongocrypt") (v "0.1.1") (d (list (d (n "bson") (r "^2.6.0") (d #t) (k 0)) (d (n "mongocrypt-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0cyhyiva5sap3pdv4lj3z083clgbfvqnfdnhl33hqq3i7lp0f9v5") (f (quote (("compile_fail"))))))

(define-public crate-mongocrypt-0.1.2 (c (n "mongocrypt") (v "0.1.2") (d (list (d (n "bson") (r "^2.6.0") (d #t) (k 0)) (d (n "mongocrypt-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0rvxhac1b62s31gk0j5rij0pb8dv8skzzdnxn97dvyp6qvsii9g4") (f (quote (("compile_fail"))))))

(define-public crate-mongocrypt-0.1.3 (c (n "mongocrypt") (v "0.1.3") (d (list (d (n "bson") (r "^2.10.0") (d #t) (k 0)) (d (n "mongocrypt-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "080vvqdvarpfkmm8sys0mk8822mqf6qrqq3hgax26w80y7jcjbpj") (f (quote (("compile_fail"))))))

