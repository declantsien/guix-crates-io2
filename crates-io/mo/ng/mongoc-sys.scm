(define-module (crates-io mo ng mongoc-sys) #:use-module (crates-io))

(define-public crate-mongoc-sys-1.3.0 (c (n "mongoc-sys") (v "1.3.0") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "0b3lxalnss9702ig84afb5fnwaa15wvlnvm2gjk04gll0w5al5fk")))

(define-public crate-mongoc-sys-1.3.1 (c (n "mongoc-sys") (v "1.3.1") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "1r0hxqffh3dlc29ym99q6hgzjgzlhfrn0mrvflwz9w1dprssfxxs")))

(define-public crate-mongoc-sys-1.3.4 (c (n "mongoc-sys") (v "1.3.4") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "04vknqhn7513pwvgs43gahf0g76caz482ydyrh954wyx8szf5dfp")))

(define-public crate-mongoc-sys-1.3.5 (c (n "mongoc-sys") (v "1.3.5") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "187kv4w2pc5rx9v33qh068s61j4688fb60qzd7d44w2vjf6lq6xz")))

(define-public crate-mongoc-sys-1.3.6 (c (n "mongoc-sys") (v "1.3.6") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "1bwgffhf215pxzscdhb3mncbwm183z6fak7vbk5rcckcw2j91668")))

(define-public crate-mongoc-sys-1.4.0 (c (n "mongoc-sys") (v "1.4.0") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "1qwism9vfkrrfmcbmr6p2skln46sjkcbz2c8s8yj6l538lcqibjh")))

(define-public crate-mongoc-sys-1.5.3 (c (n "mongoc-sys") (v "1.5.3") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "1lcr9prfgr7hh346gf7lbx2hfk3ry4i33201xd40iqqplhvg0ps8")))

(define-public crate-mongoc-sys-1.6.3 (c (n "mongoc-sys") (v "1.6.3") (d (list (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "> 0.7.0") (d #t) (k 0)))) (h "0ysxxln6hywfa85sdwg7g1n85r382qnnp4vwfjcmdkn56x7kf3n6")))

(define-public crate-mongoc-sys-1.6.3-1 (c (n "mongoc-sys") (v "1.6.3-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1w9kjqxqj8905jngmbrccrqwvxysnxz3qsik0m08l84slqay76iw")))

(define-public crate-mongoc-sys-1.6.3-2 (c (n "mongoc-sys") (v "1.6.3-2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1vsrj9x49dfmrlnhhcipk1yvnnkq6rwm9xhwf9vaqxvy74mxfv8n")))

(define-public crate-mongoc-sys-1.6.3-3 (c (n "mongoc-sys") (v "1.6.3-3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "05m0f3xdh0ky8ww9sbapbn7r8524a6wa56ix3ga2lx1rscz4i897")))

(define-public crate-mongoc-sys-1.6.4 (c (n "mongoc-sys") (v "1.6.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0vx5pli711116nhmky26lsvwc1fw6417y3m4d6irrffplnf2n66x")))

(define-public crate-mongoc-sys-1.8.0 (c (n "mongoc-sys") (v "1.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1j0lrk45ks8866l4ppmxy1xafxrpb1gv5dx076vn662kp379xd22")))

(define-public crate-mongoc-sys-1.8.0-0 (c (n "mongoc-sys") (v "1.8.0-0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ifm9h1xlp83yqdsjxn4ndwk6wxc1v10fjv26b8ppjxglhkl4qrb")))

(define-public crate-mongoc-sys-1.8.2-0 (c (n "mongoc-sys") (v "1.8.2-0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n7fzs0sr8aadvy7ckm0sc4dz9z524hyi29vw9wh9c06z3hv2w7i")))

(define-public crate-mongoc-sys-1.8.2-1 (c (n "mongoc-sys") (v "1.8.2-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ljpfbkkfzr159c5s2qg96yi9gnfi101lzyippry46alpjkhma9m")))

(define-public crate-mongoc-sys-1.17.4 (c (n "mongoc-sys") (v "1.17.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pk8frqkh0xl536kfrrzm79f53fimk2ama9j3z1mlgxrk23qjrai") (l "mongoc")))

(define-public crate-mongoc-sys-1.17.4-1 (c (n "mongoc-sys") (v "1.17.4-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1j66ijcd4b0mx93gqklayyfhicy7rk9415q8msbmw7cqrsmr0hxx") (l "mongoc")))

(define-public crate-mongoc-sys-1.17.4-2 (c (n "mongoc-sys") (v "1.17.4-2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ajbfvs3l6g9nk71z726czp50wpkvbcwvlk9aviiddfl9jxklsp2") (l "mongoc")))

(define-public crate-mongoc-sys-1.17.5 (c (n "mongoc-sys") (v "1.17.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qcwpmas3qi1r7sddwspi67qc3fvfyaawicdxk7ybixan8hl8nr2") (l "mongoc")))

(define-public crate-mongoc-sys-1.17.5-1 (c (n "mongoc-sys") (v "1.17.5-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d5ir57qqivxnaq2c7sf7f1b8mz65zh4s75qq8nd6i20lkx69ff6") (l "mongoc")))

(define-public crate-mongoc-sys-1.22.0 (c (n "mongoc-sys") (v "1.22.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qgd5aqhbsn9gvw2mv022arn3mv8dl58q024y3mslc1qpqhzc01b") (l "mongoc")))

(define-public crate-mongoc-sys-1.22.0-1 (c (n "mongoc-sys") (v "1.22.0-1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09f10cjwy6bf0zsb7wk2p7vcir2xj59p5m1w03cris33wwls5zf7") (l "mongoc")))

(define-public crate-mongoc-sys-1.22.0-2 (c (n "mongoc-sys") (v "1.22.0-2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zlk7xnlmnhh240r9x2x0dg6m3dsnh4xyb2mjhngx9izdpihhyxh") (l "mongoc")))

