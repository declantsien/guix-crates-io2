(define-module (crates-io mo ng mongodb-language-model) #:use-module (crates-io))

(define-public crate-mongodb-language-model-0.1.0 (c (n "mongodb-language-model") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0f81swq1ipznixcp22hdrfblvnj0cxr7rdw57bfrb66gcg6fsnzv")))

(define-public crate-mongodb-language-model-0.1.1 (c (n "mongodb-language-model") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "13n73h0i25anfgllny1zlr0fjw8vbfv0m3hr7znm0cjy8s8gpxwm")))

(define-public crate-mongodb-language-model-0.1.2 (c (n "mongodb-language-model") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16ipxvl4q2330l127dn0m79x2h7l4w3qfdghmhrmd3zpxr2qqi8l")))

(define-public crate-mongodb-language-model-0.1.3 (c (n "mongodb-language-model") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "05cvd8hi5410n1q4j4d8fdw9v5s6n2y4lzjhq4szwrfcf6mv1rip") (y #t)))

(define-public crate-mongodb-language-model-0.1.4 (c (n "mongodb-language-model") (v "0.1.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0ay9g89rx112ymzqadwv1vg85x9dh31g67ywphxf6rckhn245r4y")))

(define-public crate-mongodb-language-model-0.1.5 (c (n "mongodb-language-model") (v "0.1.5") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0ch4s0620b9iss5bfng41h37xz5r8xc7734rya4m52ln8d70zsgg")))

(define-public crate-mongodb-language-model-0.1.6 (c (n "mongodb-language-model") (v "0.1.6") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1dcid5jj58nhs3ylwm3falfmynhv9lxf43srqdlsvl0y6nqh1am4")))

