(define-module (crates-io mo ng mongo-lock) #:use-module (crates-io))

(define-public crate-mongo-lock-0.1.0 (c (n "mongo-lock") (v "0.1.0") (h "0c801ciz64byq65lcnnqq0syfffwj2am9j4r3bml21jcvz2lxs4d") (y #t)))

(define-public crate-mongo-lock-0.2.1 (c (n "mongo-lock") (v "0.2.1") (d (list (d (n "mongodb") (r "^2") (f (quote ("sync"))) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1lzjw3y03267y1lbclr734qrfgkm7s3p6lcg13xxrrj9bc3ydfig")))

(define-public crate-mongo-lock-0.2.2 (c (n "mongo-lock") (v "0.2.2") (d (list (d (n "mongodb") (r "^2") (f (quote ("sync"))) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1if2cw8js2nwvgcb873x8x7b2q7wa1isgynl9dlccf2b9hqk973x")))

(define-public crate-mongo-lock-0.3.0 (c (n "mongo-lock") (v "0.3.0") (d (list (d (n "mongodb") (r "^2") (f (quote ("sync"))) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "0wfc1kzg8yv5ngcim1g1vynhmb8n144xm96bqbjwcwqpqhjrp251")))

