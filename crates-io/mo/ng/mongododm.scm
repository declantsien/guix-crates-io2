(define-module (crates-io mo ng mongododm) #:use-module (crates-io))

(define-public crate-mongododm-0.9.2 (c (n "mongododm") (v "0.9.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2") (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (d #t) (k 2)))) (h "1j68x76y90h25x4z0fzv41s9ggx8z6gzihiiwi7q621rhb6n31c3") (f (quote (("tokio-runtime" "mongodb/tokio-runtime") ("default" "tokio-runtime") ("chrono-0_4" "mongodb/bson-chrono-0_4") ("async-std-runtime" "mongodb/async-std-runtime"))))))

