(define-module (crates-io mo ng mongo_oplog) #:use-module (crates-io))

(define-public crate-mongo_oplog-0.0.1 (c (n "mongo_oplog") (v "0.0.1") (d (list (d (n "bson") (r "0.3.*") (d #t) (k 0)) (d (n "env_logger") (r "0.3.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "mongo_driver") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0pvlvdhb3ywdh8gax11w99v5cdxhh963i55k7gvxz7wbp79y88ch")))

