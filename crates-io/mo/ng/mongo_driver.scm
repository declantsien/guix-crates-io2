(define-module (crates-io mo ng mongo_driver) #:use-module (crates-io))

(define-public crate-mongo_driver-0.1.0 (c (n "mongo_driver") (v "0.1.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.0") (d #t) (k 0)))) (h "17sg2mwqjly84lxs64c2v2lyrg4idzg9ka0f3wpj4jjqs1jsrsq1")))

(define-public crate-mongo_driver-0.2.0 (c (n "mongo_driver") (v "0.2.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.1") (d #t) (k 0)))) (h "029bwjxxgy6rpbwxsg6d6kj8lkl2dm213qyjdg91bif0j50xf6is")))

(define-public crate-mongo_driver-0.2.1 (c (n "mongo_driver") (v "0.2.1") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.1") (d #t) (k 0)))) (h "00bpv3ihpz99bms6wlhvf53gzp15wx3yw66kc0g4hlg9pspmy0ig")))

(define-public crate-mongo_driver-0.2.2 (c (n "mongo_driver") (v "0.2.2") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.1") (d #t) (k 0)))) (h "1ivpv5kcnqziddw9088cympwdkn9zxh3ph01lk9v8gramxz4mhfs")))

(define-public crate-mongo_driver-0.2.3 (c (n "mongo_driver") (v "0.2.3") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.4") (d #t) (k 0)))) (h "1qpn3za7f085qhzlk8cbgi7z7ygb781bgrg0a2hmc5fdnj9vf6l4")))

(define-public crate-mongo_driver-0.2.4 (c (n "mongo_driver") (v "0.2.4") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.5") (d #t) (k 0)))) (h "1li616wcr3jlffl7vwx6g4jkg090l7fbrry4yah33yfaiq4b6y3h")))

(define-public crate-mongo_driver-0.3.0 (c (n "mongo_driver") (v "0.3.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.3.6") (d #t) (k 0)))) (h "171c4kg8djb82ajr5qf33ami4109nh7h76hqw37d080hzxnhl1pb")))

(define-public crate-mongo_driver-0.4.0 (c (n "mongo_driver") (v "0.4.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.4.0") (d #t) (k 0)))) (h "1sczq8fjyym8vpbbz1wkdfxz6hpcz7zymdryf85qmc648df1dbas")))

(define-public crate-mongo_driver-0.5.0 (c (n "mongo_driver") (v "0.5.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.5.3") (d #t) (k 0)))) (h "0dhbagiwz261qby5q1nin9vijg49a787zdxvmarxi3jjwd6c46w8")))

(define-public crate-mongo_driver-0.6.0 (c (n "mongo_driver") (v "0.6.0") (d (list (d (n "bson") (r "> 0.1.0") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "> 0.2.0") (d #t) (k 0)) (d (n "log") (r "> 0.3.0") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.3") (d #t) (k 0)))) (h "1m0a9160q252mxmlk4cqjp0ivxx1nxqr6rijnjsnvlhl9lggis9g")))

(define-public crate-mongo_driver-0.7.0 (c (n "mongo_driver") (v "0.7.0") (d (list (d (n "bson") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.3") (d #t) (k 0)))) (h "095jc1395w8ccsvzalgpfny34qnansjy14jkd98z8dv6jkzy7242")))

(define-public crate-mongo_driver-0.7.1 (c (n "mongo_driver") (v "0.7.1") (d (list (d (n "bson") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "> 0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.3") (d #t) (k 0)))) (h "1al3yjmj09wsg65gx7sr6sicih37rm8l1ickdcph36zd5a13mxap")))

(define-public crate-mongo_driver-0.7.2 (c (n "mongo_driver") (v "0.7.2") (d (list (d (n "bson") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "= 0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.3") (d #t) (k 0)))) (h "09887bvlns89baqwxrx2rqlfi89pd4s2d6k0d9ldrh2qzsjwxldh")))

(define-public crate-mongo_driver-0.8.0 (c (n "mongo_driver") (v "0.8.0") (d (list (d (n "bson") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.3-1") (d #t) (k 0)))) (h "1sjacir3mf43m0rhawj96q42ssqsx5wp9dhdr2zw0l65l2c652kx")))

(define-public crate-mongo_driver-0.9.0 (c (n "mongo_driver") (v "0.9.0") (d (list (d (n "bson") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.6.4") (d #t) (k 0)))) (h "0znh6mbq90vskcsp2cfznjjyhn8g5cwhkqv104dcb3x952qbyc3p")))

(define-public crate-mongo_driver-0.10.0 (c (n "mongo_driver") (v "0.10.0") (d (list (d (n "bson") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.8.0") (d #t) (k 0)))) (h "1y0ry9xrwshn7svk805gf04y675qmf14xdd96svdxd3cih20mzhk")))

(define-public crate-mongo_driver-0.11.0 (c (n "mongo_driver") (v "0.11.0") (d (list (d (n "bson") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.8.0-0") (d #t) (k 0)))) (h "136smxqvvczkl2zvz4hawph9i7w8a7mh3dnq96f4x06ry1ldl8wh")))

(define-public crate-mongo_driver-0.12.0 (c (n "mongo_driver") (v "0.12.0") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.8.2-0") (d #t) (k 0)))) (h "05yw9fr2qhqmlpx43w198hfnk6r4n8q1xyl4afwhrxan36ac2baw")))

(define-public crate-mongo_driver-0.12.1 (c (n "mongo_driver") (v "0.12.1") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.8.2-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "07dkyrlia0zw2zy1n0p5b2acf7nf3l4ymnp5wrii2ywbjzp2nhyq")))

(define-public crate-mongo_driver-0.13.0 (c (n "mongo_driver") (v "0.13.0") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.17.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "12h1wz08yrxahkmvpxvi6wnl049v7x77r77y78ikwv6dclj70027")))

(define-public crate-mongo_driver-0.13.1 (c (n "mongo_driver") (v "0.13.1") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.17.4-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fk06fibd26lgadvrkdva15jd206gr564ivp05z8zpwndkbv1zli")))

(define-public crate-mongo_driver-0.13.2 (c (n "mongo_driver") (v "0.13.2") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.17.4-2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "157jywwwgvr029rhilmq5h7aqrwjn5a7p1xhdzxlif7syjmzpabb")))

(define-public crate-mongo_driver-0.13.3 (c (n "mongo_driver") (v "0.13.3") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.17.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sbzc74px6inawpywm4wmafqia397k9dc7582i2yfkw2cdsvcg83")))

(define-public crate-mongo_driver-0.13.4 (c (n "mongo_driver") (v "0.13.4") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "^1.17.5-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1cfmxbgq8x0lfvirad1k0dnzrgxdg0lppsd5npczl57qd3wxqjag")))

(define-public crate-mongo_driver-0.13.5 (c (n "mongo_driver") (v "0.13.5") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "=1.17.5-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zf568w03ddhvbhmcqqqklk1j9wiqzm39ry6fg9hbb41b5jx3m8b")))

(define-public crate-mongo_driver-0.13.6 (c (n "mongo_driver") (v "0.13.6") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "=1.17.5-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fqc5rfbif5fg55942xqpvjxwivgc4bgfr1zqaadjxg2zz84jazx")))

(define-public crate-mongo_driver-0.14.0 (c (n "mongo_driver") (v "0.14.0") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "=1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yl7m6b9k8girnhfdmckm2sqya3pbvwswj5804lgs5fy41bpffn1")))

(define-public crate-mongo_driver-0.14.1 (c (n "mongo_driver") (v "0.14.1") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "=1.22.0-1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kjxibkb3fz38g32v0x3r8174p0867386aygxqjflb6f3081b2yc")))

(define-public crate-mongo_driver-0.14.2 (c (n "mongo_driver") (v "0.14.2") (d (list (d (n "bson") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mongoc-sys") (r "=1.22.0-2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0z4l49nj19jfp8bsv6aa0k24z6a6il56ci0y6r6ynw8mbky8k2p8")))

