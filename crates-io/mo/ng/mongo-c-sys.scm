(define-module (crates-io mo ng mongo-c-sys) #:use-module (crates-io))

(define-public crate-mongo-c-sys-1.16.2 (c (n "mongo-c-sys") (v "1.16.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "0b0l8g3zyn3c5dw08vp4dgv0rnmvhsdrj8c8f6bmfyrn2x4wj2px")))

(define-public crate-mongo-c-sys-1.16.3 (c (n "mongo-c-sys") (v "1.16.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 1)))) (h "0jp1z1xjwpiy7gk03ma9mg50ivsg0r1086nk5dxynajfzf8nj2pc")))

