(define-module (crates-io mo ng mongo-tracing) #:use-module (crates-io))

(define-public crate-mongo-tracing-0.1.0 (c (n "mongo-tracing") (v "0.1.0") (d (list (d (n "mongodb") (r "2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "tracing") (r "0.1.*") (d #t) (k 0)))) (h "0q915y5a0l9dlccbgxd802rraw9hln5iqny6pc29fjw0igx1c2lm")))

(define-public crate-mongo-tracing-0.1.1 (c (n "mongo-tracing") (v "0.1.1") (d (list (d (n "mongodb") (r "2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "tracing") (r "0.1.*") (d #t) (k 0)))) (h "08f320p0idyx7b7mdrcv0048pgz64gzf6yif5lnz0nqa0fcrvgq9")))

(define-public crate-mongo-tracing-0.1.2 (c (n "mongo-tracing") (v "0.1.2") (d (list (d (n "mongodb") (r "2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "tracing") (r "0.1.*") (d #t) (k 0)))) (h "1q02f4cmgivm5lx1lykni0m1imj9iibicsanl7a113rxwwi7aqwi")))

