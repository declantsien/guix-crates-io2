(define-module (crates-io mo ng mongo_rub) #:use-module (crates-io))

(define-public crate-mongo_rub-0.0.2 (c (n "mongo_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0qh69bjcwvjvglr6mr6qc853c8ri28ck2gc5mr6qv9i1jm9jhn70")))

