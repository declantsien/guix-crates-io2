(define-module (crates-io mo ch mochi-lang) #:use-module (crates-io))

(define-public crate-mochi-lang-0.1.0 (c (n "mochi-lang") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "11rr5l0zj4f5vrjc85b6cac17gmkx9d3f6j850kx4xyqpfpa3wf1")))

(define-public crate-mochi-lang-0.1.1 (c (n "mochi-lang") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1m216r4ry8aw2hgpnpcjcfh3x4ccghwwrndpjxf71kn21vcc7cvf")))

