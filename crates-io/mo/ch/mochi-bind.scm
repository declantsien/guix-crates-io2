(define-module (crates-io mo ch mochi-bind) #:use-module (crates-io))

(define-public crate-mochi-bind-0.0.1 (c (n "mochi-bind") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0sps4spj99c6m9li6jh8nfdwlsncivhizp5qipna2dihrwgvinpq")))

