(define-module (crates-io mo ch mocha) #:use-module (crates-io))

(define-public crate-mocha-0.1.0 (c (n "mocha") (v "0.1.0") (d (list (d (n "actix-http") (r "^0.2.10") (d #t) (k 2)) (d (n "actix-web") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)))) (h "1p7kh6qdqmji92vlj05k8yhcpxx9casgqqasl2clj3rv14n4ighv") (y #t)))

(define-public crate-mocha-0.1.1 (c (n "mocha") (v "0.1.1") (h "0zvqfhp0np28pqln5fqyixv913gh5ciq9sqbgd6q5klkv81b234z")))

