(define-module (crates-io mo ch mocha_audio) #:use-module (crates-io))

(define-public crate-mocha_audio-0.1.0 (c (n "mocha_audio") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.81") (d #t) (k 1)))) (h "00qnypwf9nmqwzqmahvs8ckmcfn0a3mh06s16i527dkfvk23s5jj")))

(define-public crate-mocha_audio-0.1.1 (c (n "mocha_audio") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.81") (d #t) (k 1)))) (h "00hb1h4l90znzm0l296iymzzssxq2n4ib37dda01rh343snc579v")))

