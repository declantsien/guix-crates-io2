(define-module (crates-io mo ch mochi) #:use-module (crates-io))

(define-public crate-mochi-0.0.0 (c (n "mochi") (v "0.0.0") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.9.0") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0xbx1l1hhs23vfzwi62bg382r9ipbyzrb3jq0bqmv9smd18pqfza")))

(define-public crate-mochi-0.0.1 (c (n "mochi") (v "0.0.1") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.9.0") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1l5ksrhf6hpsmzn7j6w5zwlz9ai6x6j777qz3wi079lx27bz2i6b")))

(define-public crate-mochi-0.0.2 (c (n "mochi") (v "0.0.2") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.9.0") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0i0qcwfnzqb821csyh9nrk1gygsyx7v2mlbd1prh6bbc0773r98j")))

(define-public crate-mochi-0.0.3 (c (n "mochi") (v "0.0.3") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.9.0") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "032jp2spj8xvy16npk1yh6qhsfjymfw9b9p6w36w1fzc7r1cjkbr")))

(define-public crate-mochi-0.0.5 (c (n "mochi") (v "0.0.5") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.9.0") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (d #t) (k 0)))) (h "0swcb32xc9h355q9b048znlq8f24xhgqlqxd7x9b0wjbybzvviap")))

