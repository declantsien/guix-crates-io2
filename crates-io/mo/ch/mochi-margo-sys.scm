(define-module (crates-io mo ch mochi-margo-sys) #:use-module (crates-io))

(define-public crate-mochi-margo-sys-0.1.0 (c (n "mochi-margo-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d8x0n6kccppip71p8y42syjq7f93ss1yilgx7bwvyrv0gvc7xsj") (l "margo")))

