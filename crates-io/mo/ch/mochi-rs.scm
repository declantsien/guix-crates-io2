(define-module (crates-io mo ch mochi-rs) #:use-module (crates-io))

(define-public crate-mochi-rs-0.0.1 (c (n "mochi-rs") (v "0.0.1") (d (list (d (n "dlmalloc") (r "^0.2.4") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "mochi-bind") (r "^0.0.1") (d #t) (k 0)))) (h "1zihjcx5yf712xghnkivxb4j27ngmynfls5brwq4vrx2zm29q7db") (f (quote (("extractors") ("default" "dlmalloc")))) (y #t)))

(define-public crate-mochi-rs-0.0.2 (c (n "mochi-rs") (v "0.0.2") (d (list (d (n "dlmalloc") (r "^0.2.4") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "mochi-bind") (r "^0.0.1") (d #t) (k 0)))) (h "1rfd29yhas7ivfsnxls9jwvm93fv9nb1gb7la8rw1y6q8ivcfza6") (f (quote (("extractors") ("default" "dlmalloc"))))))

