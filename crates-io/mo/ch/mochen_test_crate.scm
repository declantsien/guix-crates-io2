(define-module (crates-io mo ch mochen_test_crate) #:use-module (crates-io))

(define-public crate-mochen_test_crate-0.1.0 (c (n "mochen_test_crate") (v "0.1.0") (h "0n48v9lamrr0n334kzdbb43wsr57s50anc75y817g2zjv4fcfixn")))

(define-public crate-mochen_test_crate-0.1.1 (c (n "mochen_test_crate") (v "0.1.1") (h "0rygq5dg3xwpaphb641h9cl6w4sgghwa803swq3n5nfav6h0p83g")))

