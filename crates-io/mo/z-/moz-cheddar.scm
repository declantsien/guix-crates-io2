(define-module (crates-io mo z- moz-cheddar) #:use-module (crates-io))

(define-public crate-moz-cheddar-0.4.0 (c (n "moz-cheddar") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.42.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.3.2") (d #t) (k 0)))) (h "1p8i6gzsxswb6zh2f9n7im7r4pf22x0v3q1qxh8h74jck147v78d") (f (quote (("with-syntex" "syntex_errors" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-moz-cheddar-0.4.1 (c (n "moz-cheddar") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.3.2") (d #t) (k 0)))) (h "1av320whs86x39wbk2q5grfal2489p1wazmmlcd78r19x3xqisp2") (f (quote (("with-syntex" "syntex_errors" "syntex_syntax") ("default" "with-syntex"))))))

(define-public crate-moz-cheddar-0.4.2 (c (n "moz-cheddar") (v "0.4.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "05ahc8zaf3vppnzzad9lgjjwba4cmcwa21cijn6z61hbv3d9zcj3") (f (quote (("with-syntex" "syntex_errors" "syntex_syntax") ("default" "with-syntex"))))))

