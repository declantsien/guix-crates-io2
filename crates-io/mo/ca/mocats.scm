(define-module (crates-io mo ca mocats) #:use-module (crates-io))

(define-public crate-mocats-0.1.0 (c (n "mocats") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04snip55a2m17s08pvzqikb2ylcfcbh4cmd30iwys0pv0ifl28w3")))

(define-public crate-mocats-0.2.0 (c (n "mocats") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fg9iqcw87l581nqhqg06rp9kbg6limg7qjg57rmp2lpq4b126rd")))

(define-public crate-mocats-0.2.1 (c (n "mocats") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b2zdfwwy458amhn8s8bikxpfqzh4dp5442v9l2kdymid8n5cbhz")))

