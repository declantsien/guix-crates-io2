(define-module (crates-io mo gr mogrify) #:use-module (crates-io))

(define-public crate-mogrify-0.1.0 (c (n "mogrify") (v "0.1.0") (d (list (d (n "mogrify_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1bhhpxxpcqsqadk55gkzpiqbqrsz27mqhq5minvps9iyc5icsjva")))

(define-public crate-mogrify-0.1.1 (c (n "mogrify") (v "0.1.1") (d (list (d (n "mogrify_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1rryzpdf7sbvadivj5cdnnk3g0x7di4q9p462r41dg6290vshx7c")))

