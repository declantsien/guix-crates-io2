(define-module (crates-io mo gr mogrify_derive) #:use-module (crates-io))

(define-public crate-mogrify_derive-0.1.0 (c (n "mogrify_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08fc1mrw0jida6838dsjm7jf5radpr37dhny1z7v2ydljjdw7063")))

(define-public crate-mogrify_derive-0.1.1 (c (n "mogrify_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1nm9w9q31lkfh4878pwlsa3vg06nbw8xkhfbs67x71926b6pmqsp")))

