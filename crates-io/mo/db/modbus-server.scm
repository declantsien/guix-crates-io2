(define-module (crates-io mo db modbus-server) #:use-module (crates-io))

(define-public crate-modbus-server-0.1.0 (c (n "modbus-server") (v "0.1.0") (d (list (d (n "modbus-iiot") (r "^0.2.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)))) (h "1mj1s6akpdz76fwbgpblw4z3dnlg4qlnxfvycrkn2bxqy9kvak0k")))

