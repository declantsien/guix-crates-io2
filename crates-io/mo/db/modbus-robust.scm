(define-module (crates-io mo db modbus-robust) #:use-module (crates-io))

(define-public crate-modbus-robust-0.1.0 (c (n "modbus-robust") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 0)))) (h "0prd5zpi46f1m1ydd8r058zjz7z0m9ar5vsirjfswj4xwbr5vysf")))

