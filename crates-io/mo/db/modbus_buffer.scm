(define-module (crates-io mo db modbus_buffer) #:use-module (crates-io))

(define-public crate-modbus_buffer-0.0.1 (c (n "modbus_buffer") (v "0.0.1") (h "1v71ipgbfkfxwgz3jb0xwv45hn66vb30hnj8qd1gjjvg6svkkfmm")))

(define-public crate-modbus_buffer-0.0.2 (c (n "modbus_buffer") (v "0.0.2") (h "0jf4sl04h1hq18ksrm9s85k4qiq0vcsz10scr2nxphl3gv7ivlz5")))

