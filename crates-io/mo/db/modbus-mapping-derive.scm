(define-module (crates-io mo db modbus-mapping-derive) #:use-module (crates-io))

(define-public crate-modbus-mapping-derive-0.1.1 (c (n "modbus-mapping-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b7j7xa8xs2r0863na0ia3rnph65n9bldk9jj435g60i8cmwn82s") (y #t) (r "1.75.0")))

(define-public crate-modbus-mapping-derive-0.1.2 (c (n "modbus-mapping-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jasrr0ckyv6fih94vlqkn6dbjg9hv3w3wg4dmlldfamwa6z5k1k") (y #t) (r "1.75.0")))

(define-public crate-modbus-mapping-derive-0.1.3 (c (n "modbus-mapping-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "076z0rbsw84sh126dshwdlc2smddln0wgba3s09zvyvgx9npcci0") (r "1.75.0")))

(define-public crate-modbus-mapping-derive-0.2.0 (c (n "modbus-mapping-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bb8b4bz54yrbp47hqdvd247py3fvwmphl55k0678w53w2p8j2r9") (r "1.75.0")))

