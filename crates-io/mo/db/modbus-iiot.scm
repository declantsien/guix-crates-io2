(define-module (crates-io mo db modbus-iiot) #:use-module (crates-io))

(define-public crate-modbus-iiot-0.1.0 (c (n "modbus-iiot") (v "0.1.0") (h "1lr00xcmbgj64vmw4lvvh9h2c09vzn2zyh939jf344c13s0lkwfc")))

(define-public crate-modbus-iiot-0.1.1 (c (n "modbus-iiot") (v "0.1.1") (h "1v06xsc6axqc21vv5mdvbsqbyypf2py1lz4g8h5fd0c0ypajyy34")))

(define-public crate-modbus-iiot-0.2.0 (c (n "modbus-iiot") (v "0.2.0") (h "1nimkgmhjirzsyq5rrapkmmg97qip08jhq3by667slnyabqwcs8z")))

