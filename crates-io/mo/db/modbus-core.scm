(define-module (crates-io mo db modbus-core) #:use-module (crates-io))

(define-public crate-modbus-core-0.0.0 (c (n "modbus-core") (v "0.0.0") (h "10rbmrsp7g20phcz0kjmjdzv49j7bry9mmgi6bilb736a3nnzmgp")))

(define-public crate-modbus-core-0.1.0 (c (n "modbus-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0iz2rq2x68hi3khmqzwpa61i6vb46pf5b559lw77d6lpf7nk50la") (f (quote (("tcp") ("std" "byteorder/std") ("rtu") ("default" "tcp" "rtu")))) (r "1.65")))

