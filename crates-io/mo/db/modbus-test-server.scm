(define-module (crates-io mo db modbus-test-server) #:use-module (crates-io))

(define-public crate-modbus-test-server-0.0.1 (c (n "modbus-test-server") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)))) (h "06b1v9pa0j6difz23fw0656sv9dxkh4fm10q5x5si3fqvgq6ppqj") (f (quote (("modbus-server-tests") ("default"))))))

(define-public crate-modbus-test-server-0.0.2 (c (n "modbus-test-server") (v "0.0.2") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)))) (h "03c551rlvh2mcigc3qqqrmz4n57fvk4h58pswfykvj9x3pxxj73i") (f (quote (("modbus-server-tests") ("default"))))))

(define-public crate-modbus-test-server-0.0.3 (c (n "modbus-test-server") (v "0.0.3") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)))) (h "1zcpnp8f05bc7105f60fv3fqq5rgpxy5fs9yadiimixbqigdxphc") (f (quote (("modbus-server-tests") ("default"))))))

(define-public crate-modbus-test-server-0.0.4 (c (n "modbus-test-server") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "06kz1lkx7271ql6cbwjdi8qzw8anz707n97ba80y63q9gnc797mg") (f (quote (("modbus-server-tests") ("default"))))))

