(define-module (crates-io mo fo mofo) #:use-module (crates-io))

(define-public crate-mofo-0.1.0 (c (n "mofo") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "15scpa3h7jvgzm7qz7a9s84pw8ry3a1r3qjclx5n3vgsfmfs4bxh")))

(define-public crate-mofo-0.1.1 (c (n "mofo") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "0wpqijwh2rcqadjzmq1ah9nspwf6la6a90akn1ba1jqgcnx7lbcr")))

(define-public crate-mofo-0.1.2 (c (n "mofo") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "127hkn04l56c24dr4xhx74h7x99r8nglsml77ymnanzd5hdfhai8")))

(define-public crate-mofo-0.1.3 (c (n "mofo") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jkwk18papwx399yzdnw35bj90vbrqx5ad97zizsvmn3klisyx6z")))

(define-public crate-mofo-0.1.4 (c (n "mofo") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0pagbidsyp9pdf361l8wyva5bgdcdfb540j8w8napyjy0yzynqwv")))

(define-public crate-mofo-0.1.5 (c (n "mofo") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "11f8n43sgzbip7y55msl4agpfg7hxnkav39fcgq6ffym9v4wn8zb")))

(define-public crate-mofo-0.2.0 (c (n "mofo") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0aww21yd6j7rnjf24w8hrw2lhslbg588q3kfvi0rsy8q5kq4wklg")))

(define-public crate-mofo-0.2.1 (c (n "mofo") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "12499a8kzfagizm6hi8f5p0aqxk7vjk8iikmch95dr5ra384zmck")))

(define-public crate-mofo-0.2.2 (c (n "mofo") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1r95zqp7ymw0d9gnx4lapgx8hsm5lxl615399l4rai9gdsy791i5")))

