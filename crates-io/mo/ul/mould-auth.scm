(define-module (crates-io mo ul mould-auth) #:use-module (crates-io))

(define-public crate-mould-auth-0.0.7 (c (n "mould-auth") (v "0.0.7") (d (list (d (n "authorize") (r "^0.1.2") (d #t) (k 0)) (d (n "mould") (r "^0.0.17") (d #t) (k 0)))) (h "1fdiaapdcn43snlg1g0hkhbvgy1nm78dacfrq2nwz6rzvhw5v86r")))

(define-public crate-mould-auth-0.0.8 (c (n "mould-auth") (v "0.0.8") (d (list (d (n "identify") (r "^0.1.0") (d #t) (k 0)) (d (n "mould") (r "^0.0.17") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "1dalxk91l02zwbbi1g7i9ymcpnm5krjmksdq1x2swr5hjy9zrriy")))

(define-public crate-mould-auth-0.0.9 (c (n "mould-auth") (v "0.0.9") (d (list (d (n "identify") (r "^0.1.0") (d #t) (k 0)) (d (n "mould") (r "^0.0.18") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "1zzmbw7sigric7p044l1jgpxn614bas4573jwq9mwr71z056frjz")))

(define-public crate-mould-auth-0.0.10 (c (n "mould-auth") (v "0.0.10") (d (list (d (n "identify") (r "^0.1.0") (d #t) (k 0)) (d (n "mould") (r "^0.0.19") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "1n9b7dnmk83m3nhj2qycz691nl46gsgvsfapis3mkbfpqlk8li68")))

(define-public crate-mould-auth-0.0.11 (c (n "mould-auth") (v "0.0.11") (d (list (d (n "mould") (r "^0.0.20") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "03jq90bdng35xn9h9f3pr7nc5nrcy8bqn1cabz8ikhbbgcqx29vy")))

