(define-module (crates-io mo ul mould) #:use-module (crates-io))

(define-public crate-mould-0.0.1 (c (n "mould") (v "0.0.1") (d (list (d (n "hyper") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12") (d #t) (k 0)))) (h "1vcc4rvdizdcg8m4d5aq27b703hapc4y3c6sv7jigack2fb5sc3v")))

(define-public crate-mould-0.0.2 (c (n "mould") (v "0.0.2") (d (list (d (n "hyper") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12") (d #t) (k 0)))) (h "0j6v7b0mf9hrfzwb6000s0sa3b4ynfxnffnk7mcwgw3rfpynp0v8")))

(define-public crate-mould-0.0.4 (c (n "mould") (v "0.0.4") (d (list (d (n "hyper") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12") (d #t) (k 0)))) (h "01x1wvw8ym54phxj0jmiw3d3ycdadakqnkbj2jjqsihw1syvgm69")))

(define-public crate-mould-0.0.5 (c (n "mould") (v "0.0.5") (d (list (d (n "hyper") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12") (d #t) (k 0)))) (h "07aki9gh4qpl8wapn2sv71y2wm1mp0bgqq89wmpcqa9hh6fq6zv0")))

(define-public crate-mould-0.0.6 (c (n "mould") (v "0.0.6") (d (list (d (n "hyper") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "websocket") (r "~0.12") (d #t) (k 0)))) (h "05g0c3i180n8m3l2639ivvrgha95y3a80gang7bkp214xkhh6ljq")))

(define-public crate-mould-0.0.8 (c (n "mould") (v "0.0.8") (d (list (d (n "hyper") (r "~0.7.0") (d #t) (k 0)) (d (n "json_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)) (d (n "websocket") (r "~0.15.0") (d #t) (k 0)))) (h "0ykd7f6cr3rpvy62rzf1v3pd89ys64ah1brxffnpp7k0fzqkdz9n")))

(define-public crate-mould-0.0.9 (c (n "mould") (v "0.0.9") (d (list (d (n "hyper") (r "~0.9.1") (d #t) (k 0)) (d (n "json_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.0") (d #t) (k 0)))) (h "0cvnsaakc3cvms4q14fqv6zyxb9f3311brag45wy27rj6sdp9gpy")))

(define-public crate-mould-0.0.10 (c (n "mould") (v "0.0.10") (d (list (d (n "hyper") (r "~0.9.1") (d #t) (k 0)) (d (n "json_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.0") (d #t) (k 0)))) (h "06zwr7n68afww84a3m6637vsrllfnqy8dvd3h8hzg7nimbvxnh67")))

(define-public crate-mould-0.0.11 (c (n "mould") (v "0.0.11") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "json_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "01vnqbk9s8b9v0gk6zz1aplhk1hpga28nfwmlf4bc5vhhysaz5xq")))

(define-public crate-mould-0.0.12 (c (n "mould") (v "0.0.12") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "json_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "052s22008sg85cw1kbcb083ycp5wrmf2inn81fps7wk8m2vryg9d")))

(define-public crate-mould-0.0.13 (c (n "mould") (v "0.0.13") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1v67902vym3k8ljzjkqvn10v4d0fav1g7gd7syz6l2y11kryli3j")))

(define-public crate-mould-0.0.15 (c (n "mould") (v "0.0.15") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1gfkq4nab1cbkqnr5cwavy4x9zbbxyc06355yhavr438mcncm42m") (y #t)))

(define-public crate-mould-0.0.16 (c (n "mould") (v "0.0.16") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1kdlw8cksls01r5fz9zw4y24qizw7z3apk95p8xpgq540lck7l24") (y #t)))

(define-public crate-mould-0.0.17 (c (n "mould") (v "0.0.17") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1x9llmkixkvlb78k2p5kvy41ihrrpafyq6amgl76x7sdq8k13qqn")))

(define-public crate-mould-0.0.18 (c (n "mould") (v "0.0.18") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1rz1s6d9f4w2520xqlsfmmrznsm4lcl18im4xniibax7h9hqklc3")))

(define-public crate-mould-0.0.19 (c (n "mould") (v "0.0.19") (d (list (d (n "hyper") (r "~0.9.10") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "~0.17.1") (d #t) (k 0)))) (h "1zkim7vkgays6wiipms7s06ar0dsalgsmsh8ryym927108v0y08k")))

(define-public crate-mould-0.0.20 (c (n "mould") (v "0.0.20") (d (list (d (n "hyper") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1kv2znq88kfd96i2h8bznxyxpi6qjdwrw6qnyafyhsykz2jx60zb") (f (quote (("wsmould" "hyper" "websocket") ("iomould") ("default" "iomould" "wsmould"))))))

(define-public crate-mould-0.0.21 (c (n "mould") (v "0.0.21") (d (list (d (n "hyper") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "01p5cvai5qmgsm4qpi1lfcagr0xw6sxwcww14y455c4pr5apy3qz") (f (quote (("wsmould" "hyper" "websocket") ("iomould") ("default" "iomould" "wsmould"))))))

(define-public crate-mould-0.0.22 (c (n "mould") (v "0.0.22") (d (list (d (n "hyper") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "websocket") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1dacg4jf01fgsr5zhql6zk44ry1i6lbwsi4sqgnkrz9mslm38apx") (f (quote (("wsmould" "hyper" "websocket") ("iomould") ("default" "iomould" "wsmould"))))))

