(define-module (crates-io mo ul mould-nfd) #:use-module (crates-io))

(define-public crate-mould-nfd-0.0.6 (c (n "mould-nfd") (v "0.0.6") (d (list (d (n "mould") (r "^0.0.17") (d #t) (k 0)) (d (n "nfd") (r "^0.0.4") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "0ry8g96l9fikhpksxdl2vz3dlks9hnr2mjpxwh8jh3sbhya41bls")))

(define-public crate-mould-nfd-0.0.7 (c (n "mould-nfd") (v "0.0.7") (d (list (d (n "mould") (r "^0.0.18") (d #t) (k 0)) (d (n "nfd") (r "^0.0.4") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "1gg5xhnabcb03wpjgb58xh16i2lwbfjfbh8gsih7m63x08b9hvqn")))

(define-public crate-mould-nfd-0.0.8 (c (n "mould-nfd") (v "0.0.8") (d (list (d (n "mould") (r "^0.0.19") (d #t) (k 0)) (d (n "nfd") (r "^0.0.4") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "1h55alqybrx176mqrfqx36qfiqf2cbam7y4yzqqn60zp6hpkyr2h")))

