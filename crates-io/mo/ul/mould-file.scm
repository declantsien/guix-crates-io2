(define-module (crates-io mo ul mould-file) #:use-module (crates-io))

(define-public crate-mould-file-0.0.6 (c (n "mould-file") (v "0.0.6") (d (list (d (n "mould") (r "^0.0.17") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "0z6wavrw92bhc4a7mr7hns7avq9nz5wg0m73cdhic8f7dfvwfz69")))

(define-public crate-mould-file-0.0.7 (c (n "mould-file") (v "0.0.7") (d (list (d (n "mould") (r "^0.0.18") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "0cipmf3hb7ph825wpb9ikbl20iqlv23gzb5bgwlixaxsk8yg8l0q")))

(define-public crate-mould-file-0.0.8 (c (n "mould-file") (v "0.0.8") (d (list (d (n "mould") (r "^0.0.19") (d #t) (k 0)) (d (n "permission") (r "^0.1.0") (d #t) (k 0)))) (h "00f1i7nv2wv233jq1gaklffiz5lyxwca5wms2s6fwp6a2irniivg")))

