(define-module (crates-io mo na monad) #:use-module (crates-io))

(define-public crate-monad-0.0.1 (c (n "monad") (v "0.0.1") (d (list (d (n "free") (r "*") (d #t) (k 0)) (d (n "monad_macros") (r "*") (d #t) (k 0)) (d (n "tailrec") (r "*") (d #t) (k 0)))) (h "01vk7avx9g7jd85l2xsi9x85wx0w8rwa3zicj55050p83hj1hxga")))

(define-public crate-monad-0.0.2 (c (n "monad") (v "0.0.2") (d (list (d (n "free") (r "*") (d #t) (k 0)) (d (n "monad_macros") (r "*") (d #t) (k 0)) (d (n "tailrec") (r "*") (d #t) (k 0)))) (h "0mivv6a72w4kcfchv8n2vqpgyqzyp2d2jjsw25iaxmbi7318b3ps")))

