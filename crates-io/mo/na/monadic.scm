(define-module (crates-io mo na monadic) #:use-module (crates-io))

(define-public crate-monadic-0.1.0 (c (n "monadic") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "095m38ws4c69ip1r74hzlka4jiyb6m8vwkfq6q4wnpj23jr9qzyl")))

(define-public crate-monadic-0.1.1 (c (n "monadic") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0zcpg0i2im28p7hpwwvylv6qhdbmc07hg7gar28ac72g9cn9r3qd")))

(define-public crate-monadic-0.1.2 (c (n "monadic") (v "0.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0nfdpa2gpny47zqpqbm9w32n5vxhyl1995v61sqnw29idq9jw09c")))

(define-public crate-monadic-0.1.3 (c (n "monadic") (v "0.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0nkyi6yv7xzw2akyzyllb5waryz7my1nl6cpvjxz622gzaipck66")))

(define-public crate-monadic-0.1.4 (c (n "monadic") (v "0.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0394f9blypag670zb3jijz0hwlsxygyd0fjqi121jf21glymy88c")))

(define-public crate-monadic-0.1.5 (c (n "monadic") (v "0.1.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "16f87c3ckz5dmc88rpiiasln5i52r4ppchfh2r0vjidxz2y39r3p")))

(define-public crate-monadic-0.1.6 (c (n "monadic") (v "0.1.6") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1w6y9pk3j0x2l3bjjmfppjly6z5s262nwjrcbq72llrwwbd9lv7i")))

(define-public crate-monadic-0.1.7 (c (n "monadic") (v "0.1.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "160gmg6jj03divqg666jgl7yibly3gg1hwnab0kxzpphkgr27xn3")))

(define-public crate-monadic-0.1.8 (c (n "monadic") (v "0.1.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1w78jqsabsvqa7krz84s3rxqxw0vlfja9i2h93b0jpdkp9q4ybyh")))

(define-public crate-monadic-0.1.9 (c (n "monadic") (v "0.1.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0dli9611a5fxn0n01i24jm70r5ri878x9kk2bv5vza6849mz8fbl")))

(define-public crate-monadic-0.1.10 (c (n "monadic") (v "0.1.10") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0q4397vzijmys6bbqbrhzmnbba9h24fmcdyicisgk66si2d76s81")))

(define-public crate-monadic-0.1.11 (c (n "monadic") (v "0.1.11") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1pa2fjh85ziacv8r5mj9aml9ngpfx6fq48xgavw9f4pm8dmd9xqj")))

(define-public crate-monadic-0.1.12 (c (n "monadic") (v "0.1.12") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0dhv59f62q1zigfikcz86b7nl6x5319y1q8q7zndpl2m3wwzf9i4")))

(define-public crate-monadic-0.1.13 (c (n "monadic") (v "0.1.13") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1p9qkzzd0y7zbfgf7jmpsc57d1w42s3qjy8cq7r19g21w6xc2vf0")))

(define-public crate-monadic-0.1.14 (c (n "monadic") (v "0.1.14") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1sji26n9lahqj8a3nbwiksrdq5mrcx5d9ypn8p9pnkasighh5ss6")))

(define-public crate-monadic-0.1.15 (c (n "monadic") (v "0.1.15") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1izj9mqm87cc842zdkgixj7wxqfdqc8a9zmmnb5701hrjcabjnzg")))

(define-public crate-monadic-0.1.16 (c (n "monadic") (v "0.1.16") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1104wrb6w1qqk9bcv6qyarqvrba06r8fizidahaxy6znhlpj1h4r")))

(define-public crate-monadic-0.2.0 (c (n "monadic") (v "0.2.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0nw2hzq2rkxahs737mrsgck0xvnk3ggzfh16a2ni34bxs9dy0qc8")))

(define-public crate-monadic-0.2.1 (c (n "monadic") (v "0.2.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0fvjmd0q3qx4kz6vr9ka8pb6j4h6zlddv3x7pvdff9pd3ia4fsa1")))

(define-public crate-monadic-0.3.0 (c (n "monadic") (v "0.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "0rpx793lyw8ig6cg0jg6y26zkgajd6almqr9lid6a8am0vwsh4n2")))

(define-public crate-monadic-0.3.1 (c (n "monadic") (v "0.3.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)))) (h "1pyid8d4d9550j0vmfk3sixvcmk716lhi11mzll1d0cjdjf8fp39")))

(define-public crate-monadic-0.3.2 (c (n "monadic") (v "0.3.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1zvdl27wzgs4rxxafahx9kv8z5b6arjyx0ajsn6y44qpqxl23mr3")))

(define-public crate-monadic-0.3.3 (c (n "monadic") (v "0.3.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "0z60xhsb8s0mbj0xvbzkg1z2zv6ayp9b5v3s8qlqgnqmrszrra1b")))

(define-public crate-monadic-0.3.4 (c (n "monadic") (v "0.3.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "0wvfsap7wbqs7h2nyak8k3wfdr62xg3n683fdln3gh3ar9d6jlz7")))

(define-public crate-monadic-0.3.5 (c (n "monadic") (v "0.3.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1q5fgq7s912h1bmvr8kag48r4fh2f9pb3p72d4mkwn60k7yn7ypy")))

(define-public crate-monadic-0.3.6 (c (n "monadic") (v "0.3.6") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "02wnw3y3di1ac9mw1l2xc3qs12djlbap4cbl7szsj1xqb9li4xgw")))

(define-public crate-monadic-0.3.7 (c (n "monadic") (v "0.3.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "13v4gn7r2h6clvw6rj07qqx9nqhsdg5r742vr991m293qwpy8wl6")))

(define-public crate-monadic-0.3.8 (c (n "monadic") (v "0.3.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1y0127x7ay05bir76b38b6pksvryanvb7rvfjgf13mdq5ww93m1i")))

(define-public crate-monadic-0.3.9 (c (n "monadic") (v "0.3.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "0k23hr6b1fmqn21bfyf5ip7lgfpafq2br0cpnx7d4cazgdjqfz1v")))

(define-public crate-monadic-0.3.10 (c (n "monadic") (v "0.3.10") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "11in99dlms0jyjq5xpz8ip1g3rbn6akm48f1i8pyr64w3d7n6yd4")))

(define-public crate-monadic-0.3.11 (c (n "monadic") (v "0.3.11") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1viym5bgnj9ica8vm10i9ywf83k2cczsb4mqx3brgp6qh3c7qm35")))

(define-public crate-monadic-0.3.12 (c (n "monadic") (v "0.3.12") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1aa3kj2pl7h2769m3xhrydd6gq3v53i2dn3g61ihpwg989z6vv79")))

(define-public crate-monadic-0.3.13 (c (n "monadic") (v "0.3.13") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1la1iw8b4cb1fhvx4b7lln9bpdcvwqp99la7lj3r13jw17gjjici")))

(define-public crate-monadic-0.3.14 (c (n "monadic") (v "0.3.14") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "1lgd8w50dgjk5g06rv0b8xmh0vldqd8gb3qf5378lzq9g5p4dhw3")))

(define-public crate-monadic-0.4.0 (c (n "monadic") (v "0.4.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "16cxyzhv837n133qi879f5vdhib2dz6h3azxyb5xgb8gcr41iakp")))

(define-public crate-monadic-0.4.1 (c (n "monadic") (v "0.4.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)))) (h "0sv3pn3idn405a88ir0m2r99464bm0qjwbqv9jazcz61zw62aw8r")))

(define-public crate-monadic-0.4.2 (c (n "monadic") (v "0.4.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "09znvcbxswmz1zayc6mhhkdr403a0hx3hmyxmhfj0p7pp2dp40w5")))

(define-public crate-monadic-0.4.3 (c (n "monadic") (v "0.4.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "08cplrsa5xwrqgw1jqk9h646fl86qvx19xwy1dzhzkvxlin8by3b")))

(define-public crate-monadic-0.4.4 (c (n "monadic") (v "0.4.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0bczvkkfb998cwv51gh9890py3l6knbvlg6ryzfx4kgnahjjnbg0")))

(define-public crate-monadic-0.4.5 (c (n "monadic") (v "0.4.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1y2l9ldak5c49fhwpfj3shyimwcf6hyf1zzh2wvvciqmz9zwy91i")))

(define-public crate-monadic-0.4.6 (c (n "monadic") (v "0.4.6") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0l5x4k728hh3dn8wwrmiqmaf3n6f671hbkskx15pmpnqjfiwci4g")))

(define-public crate-monadic-0.4.7 (c (n "monadic") (v "0.4.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "14077ak3sphc9kh6x3i7ski4qjnsch14azksg3cyy2g1ilfybsf1")))

(define-public crate-monadic-0.4.8 (c (n "monadic") (v "0.4.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1ji646myhmpci58fpp6rsjz7vvwx4ihhf5cvgdp8ndw33ga2qv85")))

(define-public crate-monadic-0.4.9 (c (n "monadic") (v "0.4.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1dmrilvvjww3c4rl82h63dch3l7ay8r2phqdzqmkmchyvbh10cr6")))

(define-public crate-monadic-0.4.10 (c (n "monadic") (v "0.4.10") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1w7cy6z07qlx6f2f1njcybi1gnzbx7q83h9lbnxqdqhrm3znn08m")))

(define-public crate-monadic-0.5.0 (c (n "monadic") (v "0.5.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0jyiqgr13dbihy686d7flp7w1ixckb14mjykmlzinwg6rdjd3gwg")))

(define-public crate-monadic-0.5.1 (c (n "monadic") (v "0.5.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1n4wzc5p0i2bn0nsw7vpr3qhx4jhhyc0mgbzv3041lz642ckij6x")))

(define-public crate-monadic-0.5.2 (c (n "monadic") (v "0.5.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "07y3yx9j7227ax25pfl763nj3qx3w29yvznqn4aa7r76bqw3jsm5")))

(define-public crate-monadic-0.5.3 (c (n "monadic") (v "0.5.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "14nsr7a4wkkb9dcdni5c1lfci3w231q2nry71sqqgp2qpgrixv4f") (f (quote (("writer_trans") ("writer") ("state_trans") ("state") ("reader_trans") ("reader") ("default" "reader" "reader_trans" "writer" "writer_trans" "state" "state_trans"))))))

(define-public crate-monadic-0.5.4 (c (n "monadic") (v "0.5.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1af6v4xaa26lzc57yaw67cj0gpc4rkjml78yswvhfppl60p0asg6") (f (quote (("writer_trans") ("writer") ("state_trans") ("state") ("reader_trans") ("reader") ("default" "reader" "reader_trans" "writer" "writer_trans" "state" "state_trans"))))))

(define-public crate-monadic-0.5.5 (c (n "monadic") (v "0.5.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "partial_application") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1f2x6b701kh4sxhwn25xl7dsgckg812336s01q939hllg1cln4ni") (f (quote (("writer_trans") ("writer") ("state_trans") ("state") ("reader_trans") ("reader") ("default" "reader" "reader_trans" "writer" "writer_trans" "state" "state_trans"))))))

