(define-module (crates-io mo na monal-zeus) #:use-module (crates-io))

(define-public crate-monal-zeus-1.0.0 (c (n "monal-zeus") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mustache") (r "^0.9.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "05xxyj7zrsdj3fi1ldc1qizb7bsi8papz9vlzcjwda38m1fxc6rd")))

