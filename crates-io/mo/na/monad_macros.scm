(define-module (crates-io mo na monad_macros) #:use-module (crates-io))

(define-public crate-monad_macros-0.0.0 (c (n "monad_macros") (v "0.0.0") (h "0320crp6k3xy8y6mlv4zp1d0sbysy2d75a3vg0gl746fql38pmij")))

(define-public crate-monad_macros-0.0.1 (c (n "monad_macros") (v "0.0.1") (h "07hnjyldfn8c7bvv5zhzbnfqlniz8c16mjyifw270al1r7kxz4wc")))

