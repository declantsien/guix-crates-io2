(define-module (crates-io mo ne monero-generators) #:use-module (crates-io))

(define-public crate-monero-generators-0.1.0 (c (n "monero-generators") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("std"))) (d #t) (k 0)) (d (n "dalek-ff-group") (r "^0.1.4") (d #t) (k 0)) (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1gnq3dkmg8a8kpp7z9jkr6x0rbpq84rwj512sirhi5z47gkya9mw")))

(define-public crate-monero-generators-0.1.1 (c (n "monero-generators") (v "0.1.1") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("std"))) (d #t) (k 0)) (d (n "dalek-ff-group") (r "^0.1.4") (d #t) (k 0)) (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "115qpj5zycfxzqdz0r4vzzzzzm3zkc8yya1zklxixfk58d9fxg4z")))

(define-public crate-monero-generators-0.2.0 (c (n "monero-generators") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("std"))) (d #t) (k 0)) (d (n "dalek-ff-group") (r "^0.2") (d #t) (k 0)) (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "19q31vq6mnaqak5h7iadnd7r9hhbkcj9jvfi3psc268sd5p4w39q")))

(define-public crate-monero-generators-0.3.0 (c (n "monero-generators") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("std"))) (d #t) (k 0)) (d (n "dalek-ff-group") (r "^0.3") (d #t) (k 0)) (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "10scdaknjaw7n6vxhn846w2ivi60hbs54pyhp1pv6bx3fpngh08v")))

