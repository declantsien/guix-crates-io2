(define-module (crates-io mo ne moneyman_cli) #:use-module (crates-io))

(define-public crate-moneyman_cli-0.1.0 (c (n "moneyman_cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "moneyman") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4") (d #t) (k 0)))) (h "159c2h5lkn4151s4ad8bjj3zyw2nrsqawm3w6bsjdjjpmmxlkjha")))

(define-public crate-moneyman_cli-0.1.1 (c (n "moneyman_cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "moneyman") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4") (d #t) (k 0)))) (h "1nw8n12x9l1f72smnjnl3xf06k3gj9x9c1mzjdy3iglm6ch6g9v7")))

(define-public crate-moneyman_cli-0.1.2 (c (n "moneyman_cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "moneyman") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4") (d #t) (k 0)))) (h "0sn9vkrljnvnc0j267yp102g9fwckiz171p9fmnlcbsfs4qxpjp3")))

