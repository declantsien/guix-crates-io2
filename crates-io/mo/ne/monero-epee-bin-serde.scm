(define-module (crates-io mo ne monero-epee-bin-serde) #:use-module (crates-io))

(define-public crate-monero-epee-bin-serde-1.0.0 (c (n "monero-epee-bin-serde") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_with") (r "^1") (d #t) (k 2)))) (h "0q1r3mb1fz4248ldwh5pglavrr81xjq92hldp6ag4l7ib995pghk")))

(define-public crate-monero-epee-bin-serde-1.0.1 (c (n "monero-epee-bin-serde") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "monero") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_with") (r "^1") (d #t) (k 2)))) (h "07ij6wza5m5bzgwr8zgw1wdcry4dwj3r3gjkr7b1zfzmgrzkz2hz")))

