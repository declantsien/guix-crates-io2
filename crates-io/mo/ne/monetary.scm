(define-module (crates-io mo ne monetary) #:use-module (crates-io))

(define-public crate-monetary-0.0.0 (c (n "monetary") (v "0.0.0") (d (list (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 0)))) (h "18b8k9fhjsa69xjwsdj5wisb4wddj2w6h1mifrfpmb038b3225dp")))

(define-public crate-monetary-0.0.1 (c (n "monetary") (v "0.0.1") (d (list (d (n "cosmwasm-std") (r ">=1, <3") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g2sbbdz4zf5f3kb8yxcp9jdqa789lihza4z0i3hfjnb3j1p5ys8") (f (quote (("default" "serde"))))))

(define-public crate-monetary-0.0.2 (c (n "monetary") (v "0.0.2") (d (list (d (n "cosmwasm-std") (r ">=1, <3") (f (quote ("std"))) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1663dc31a2das9x4nrkl9s5di0qxplpb0j9bb6sf4k5f7djyvkkx") (f (quote (("default" "serde" "schemars"))))))

(define-public crate-monetary-0.0.3 (c (n "monetary") (v "0.0.3") (d (list (d (n "cosmwasm-std") (r ">=1, <3") (f (quote ("std"))) (d #t) (k 0)) (d (n "monetary-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a8m0s31jzvw7hy33azqkv4s21bxzpljazg0yhxyaqrjyxipmhc7") (f (quote (("default" "serde" "schemars")))) (s 2) (e (quote (("serde" "dep:serde" "monetary-macros/serde") ("schemars" "dep:schemars" "monetary-macros/schemars"))))))

