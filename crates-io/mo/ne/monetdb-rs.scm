(define-module (crates-io mo ne monetdb-rs) #:use-module (crates-io))

(define-public crate-monetdb-rs-0.1.0 (c (n "monetdb-rs") (v "0.1.0") (h "1a5kjgszpcva97y2srcls0yynnc7lv6vs3c5hrcdw1mpkwqdcjaf")))

(define-public crate-monetdb-rs-0.1.1 (c (n "monetdb-rs") (v "0.1.1") (h "0a5bda4vv3x9zc7immakmmdjaic9fbarfi456k729sw43yg3gixz")))

