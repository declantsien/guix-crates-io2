(define-module (crates-io mo ne monero_client) #:use-module (crates-io))

(define-public crate-monero_client-0.0.1 (c (n "monero_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "monero") (r "^0.6.0") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0n58gdc2xbr2ccsi2h8xmi4iy6w3kwwpcykac6g3jcqak0iilgp5")))

