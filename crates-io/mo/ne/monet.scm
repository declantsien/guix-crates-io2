(define-module (crates-io mo ne monet) #:use-module (crates-io))

(define-public crate-monet-0.1.0 (c (n "monet") (v "0.1.0") (d (list (d (n "iso4217") (r "^0.3") (d #t) (k 0)))) (h "0whyg1w9p7l9qpid7a8alc6fxar970sz9a2jinrb5xih8nz590cn")))

(define-public crate-monet-0.1.1 (c (n "monet") (v "0.1.1") (d (list (d (n "iso4217") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15a6l8vpblqy6s9lkrim8qw2jqnlgnf309s4j040mkfnfmaqpxd1") (f (quote (("serialize" "serde") ("default"))))))

