(define-module (crates-io mo ne moneta) #:use-module (crates-io))

(define-public crate-moneta-0.1.0 (c (n "moneta") (v "0.1.0") (d (list (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "1xzfhfdavlngv1268ay1q1xsw83mzry6h0rfwl68ldmc13x30a4z") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-moneta-0.1.1 (c (n "moneta") (v "0.1.1") (d (list (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "0r3mc8q2maxm4vp8kpbjrmzabysip5zfmyqrqzc5lz5s2v98mjhn") (f (quote (("std") ("default" "std"))))))

(define-public crate-moneta-0.2.0 (c (n "moneta") (v "0.2.0") (d (list (d (n "fpdec") (r "^0.5.2") (d #t) (k 0)) (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "17gzff5flf3cd1bsxijgxf2p737kh2i3x2q0xg1fb0x7vypbnl5x") (f (quote (("std") ("default" "std"))))))

(define-public crate-moneta-0.3.0 (c (n "moneta") (v "0.3.0") (d (list (d (n "fpdec") (r "^0.5.2") (d #t) (k 0)) (d (n "fpdec-core") (r "^0.5") (d #t) (k 0)) (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "0mg98s50zks93nv1i8hv075x4npn2gk42kgyza5nl42dwqvpgds2") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-moneta-0.3.1 (c (n "moneta") (v "0.3.1") (d (list (d (n "fpdec") (r "^0.5.2") (d #t) (k 0)) (d (n "fpdec-core") (r "^0.5") (d #t) (k 0)) (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "0kxp7q0dd6laqkdspqzccm0nj2jh2547rpl87xinkry3if0mdxfh") (f (quote (("std") ("default" "std"))))))

(define-public crate-moneta-0.3.2 (c (n "moneta") (v "0.3.2") (d (list (d (n "fpdec") (r "^0.5.2") (d #t) (k 0)) (d (n "fpdec-core") (r "^0.5") (d #t) (k 0)) (d (n "quantities") (r "^0.9.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "1n1bv68hx0dydx8i8zlm9fzqsxjgn07rxlvrdr48j9yilvix5bbn") (f (quote (("std") ("default" "std"))))))

(define-public crate-moneta-0.3.3 (c (n "moneta") (v "0.3.3") (d (list (d (n "fpdec") (r "^0.7.0") (d #t) (k 0)) (d (n "fpdec-core") (r "^0.6.4") (d #t) (k 0)) (d (n "quantities") (r "^0.12.0") (f (quote ("fpdec"))) (d #t) (k 0)))) (h "1pm5ga0b1p14jx0300fgmvii3a0178zlrkb26zyvanjxhvv0ljmx") (f (quote (("std") ("default" "std"))))))

(define-public crate-moneta-0.4.0 (c (n "moneta") (v "0.4.0") (d (list (d (n "fpdec") (r "^0.7.0") (d #t) (k 0)) (d (n "fpdec-core") (r "^0.6.4") (d #t) (k 0)) (d (n "quantities") (r "^0.12.0") (f (quote ("fpdec"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sl92s04rdxxylm2aair130d1xl59p1yxmjxhvjah59cc48wj5d3") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "quantities/serde"))))))

