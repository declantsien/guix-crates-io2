(define-module (crates-io mo ne monero-lmdb-rkv) #:use-module (crates-io))

(define-public crate-monero-lmdb-rkv-0.1.0 (c (n "monero-lmdb-rkv") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lmdb-rkv-sys") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zxvl7mr44h5p90d5b98yl6ac3c3lc1vshsii8zcyn38z37fnc61") (f (quote (("with-fuzzer-no-link" "lmdb-rkv-sys/with-fuzzer-no-link") ("with-fuzzer" "lmdb-rkv-sys/with-fuzzer") ("with-asan" "lmdb-rkv-sys/with-asan") ("default"))))))

