(define-module (crates-io mo ne monero-burn) #:use-module (crates-io))

(define-public crate-monero-burn-0.1.0 (c (n "monero-burn") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "monero") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0fxrnk3v3yx4913dg4rvandfzjl2vib1ckicybsvz1cgigv7f9xq")))

(define-public crate-monero-burn-0.2.0 (c (n "monero-burn") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "monero") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1z29vv01g5i1ids3fx4025c24hg03pbfw4qxi5bza4i4z5qf41rf")))

(define-public crate-monero-burn-0.3.0 (c (n "monero-burn") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "monero") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1rff5wsvvr3y993y0fs52y3rhw4ny8q41sjarns8mq9b8fyvpqkl")))

(define-public crate-monero-burn-0.4.0 (c (n "monero-burn") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "monero") (r "^0.7.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0pxgsv4ig29vpzv0zc8wyidm41nh4586wr74rbnv61l57hrx39i3")))

(define-public crate-monero-burn-0.4.1 (c (n "monero-burn") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "monero") (r "^0.7.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0lpq3lzzk01jg44nqf15wcll4gfvgvxbkb92jclwv603las8ri32")))

(define-public crate-monero-burn-0.4.2 (c (n "monero-burn") (v "0.4.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "monero") (r "^0.7.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "16fwrijgklrscrr7afyckpl9kfkfbq7jl9s50j22wnqsp2f1c34j")))

