(define-module (crates-io mo ne moneta_fn_macros) #:use-module (crates-io))

(define-public crate-moneta_fn_macros-0.2.3 (c (n "moneta_fn_macros") (v "0.2.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0f9akicfb749g55cigvmmz9gln7l859vljb89anahsj5cwc2shdn") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.4 (c (n "moneta_fn_macros") (v "0.2.4") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "02586wii31mfzdx0aic0r5a9granickzny25bxpchmhpvm5qmndc") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.5 (c (n "moneta_fn_macros") (v "0.2.5") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1wrmsja6970pc6h2vbf1524ix3r9pdg1qxa4q6wz6pqxfhwyjfif") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.6 (c (n "moneta_fn_macros") (v "0.2.6") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1zy7xwqj8jkgkbz1ncgpn62y0p9z2w24m7wwjbwrz4kw9729kpvn") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.7 (c (n "moneta_fn_macros") (v "0.2.7") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0dk70h4747bbzvjd267bvnzr94v4g6vmdcmaqsf326a1i544cfki") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.8 (c (n "moneta_fn_macros") (v "0.2.8") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1sa3wwdrvpdcaq3lwsq2fiiha120sd5ii08xk45gmvgbvsqll4nn") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.9 (c (n "moneta_fn_macros") (v "0.2.9") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0cng6n873dkiaks6vlqbdx1m03qhfd24y1grdcp71ncghrzrw4nj") (f (quote (("visible") ("trace") ("time") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.10 (c (n "moneta_fn_macros") (v "0.2.10") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q3l0jxi00m9fhv196fbxm7wpczd0mwcf3k3hqv53ycaalgf3s60") (f (quote (("visible") ("trace") ("time") ("depth") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.11 (c (n "moneta_fn_macros") (v "0.2.11") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0sf5nqbfg28bp8v52kfbvhj5mqbk8fd0ckkjs0l296vhkig5gwzv") (f (quote (("visible") ("trace") ("time") ("depth") ("count") ("cache"))))))

(define-public crate-moneta_fn_macros-0.2.12 (c (n "moneta_fn_macros") (v "0.2.12") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0dvjidk9x5xpp4r60rj5xpg06npfmzz7f8hsylrpx0dmf1r533ad") (f (quote (("visible") ("trace") ("time") ("depth") ("count") ("cache"))))))

