(define-module (crates-io mo ne monero-p2p) #:use-module (crates-io))

(define-public crate-monero-p2p-0.1.0 (c (n "monero-p2p") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "epee-serde") (r "^0.1.2") (d #t) (k 0)) (d (n "monero") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ahs99rsrrml2cm17apljax9bpp8iigv0xv35vj83kcrjb1rdgb4") (y #t)))

