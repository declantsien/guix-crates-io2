(define-module (crates-io mo ne monee) #:use-module (crates-io))

(define-public crate-monee-0.0.1 (c (n "monee") (v "0.0.1") (h "14czxz5c57a5dq1i8mqqashnva23dxqxjbpak9whpaalfsgi9saa")))

(define-public crate-monee-0.0.2 (c (n "monee") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0l5x8y4d2yil4fxlbzkghmcpbvsqxa50kwjg9ll8jw2mk2kcqz6m")))

(define-public crate-monee-0.0.3 (c (n "monee") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j23l7brzp06is4bj8fvxfghv8gxss3r0byfa146l7ybaww0wifw")))

(define-public crate-monee-0.0.4 (c (n "monee") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0shm7236agnd5ccmqak8c7fqkxwrnrg91aklpf69p71mh91z8cc5")))

(define-public crate-monee-0.0.5 (c (n "monee") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1nyj356rcvvjgph4pnbv9sbbwjv5n4k4w9andnjjq2v14rz5s60h")))

(define-public crate-monee-0.0.6 (c (n "monee") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0skyg1im6957kqbhaf37h4l1lqv0p996bn3wbzm4s1pdc1k9cp11")))

(define-public crate-monee-0.0.7 (c (n "monee") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "012bn6zcmkggj77lgrygnzm1chxx73cwdvif39630zq6yz17a233")))

