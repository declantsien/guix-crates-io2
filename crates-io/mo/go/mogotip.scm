(define-module (crates-io mo go mogotip) #:use-module (crates-io))

(define-public crate-mogotip-0.1.0 (c (n "mogotip") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0j0z2ryz88ladgzzdxdjam61iadpcm5s6817j1jgwzv85y8gn0qs")))

(define-public crate-mogotip-0.2.0 (c (n "mogotip") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16a1n3i396jsz075hhhilmjz5br860g3pqh645svqmcah7ysjk0z")))

(define-public crate-mogotip-0.3.0 (c (n "mogotip") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01816agq379g6kw3rrr641cnp2k61ij1q79nbqvdvc3j4jcpy1yr")))

