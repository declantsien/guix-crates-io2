(define-module (crates-io mo q_ moq_lambda) #:use-module (crates-io))

(define-public crate-moq_lambda-0.1.0 (c (n "moq_lambda") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fr9ybsrrm1qlmdiwhwbf8f50368khcnvfsm3s97py7sx19nzwb1")))

(define-public crate-moq_lambda-0.1.1 (c (n "moq_lambda") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13kmzpa0vx4v4jqkfyxxnigpjjnvf8myr11hmd0w4b3730qxms5v")))

(define-public crate-moq_lambda-0.2.0 (c (n "moq_lambda") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hc626m477dn5s6ndnxa4fnc7ibvbja98d05wq0apmx4sykyngc5")))

