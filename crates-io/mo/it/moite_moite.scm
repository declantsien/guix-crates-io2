(define-module (crates-io mo it moite_moite) #:use-module (crates-io))

(define-public crate-moite_moite-0.1.0 (c (n "moite_moite") (v "0.1.0") (h "1l4p5sl0clrkgh6icqzvwirkghmr75ky10db5r4cvs0bv6zpf14m")))

(define-public crate-moite_moite-0.2.0 (c (n "moite_moite") (v "0.2.0") (h "1bx14klafi3m6ck6cn48r11nl4lgr9p2ngpf2vy2qbp1c56akdgf")))

