(define-module (crates-io mo n- mon-artist) #:use-module (crates-io))

(define-public crate-mon-artist-0.1.0 (c (n "mon-artist") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.12.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tango") (r "^0.5.0") (d #t) (k 1)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "07qnpgpyl2g2x1ksx3kqbwxf32470m4z5f3cijrspiddvckd1222")))

(define-public crate-mon-artist-0.1.1 (c (n "mon-artist") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.13") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tango") (r "^0.6") (d #t) (k 1)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "0a5scq64zcxrgxkrgjh9ww9b0l0ryzjmvaffj9zlp6v2z7l4wkkf")))

(define-public crate-mon-artist-0.1.3 (c (n "mon-artist") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.13") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tango") (r "^0.6") (d #t) (k 1)) (d (n "treexml") (r "^0.3.0") (d #t) (k 0)))) (h "08vp1wlanbw196jqnvilvf5iwdnv849mzwmcjlv2fdq00whrxdsx")))

