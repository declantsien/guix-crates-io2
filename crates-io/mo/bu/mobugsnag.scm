(define-module (crates-io mo bu mobugsnag) #:use-module (crates-io))

(define-public crate-mobugsnag-0.1.0 (c (n "mobugsnag") (v "0.1.0") (d (list (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0r8yz7z9hd95530hk956lbh1klcymn05hij5qhdl5ildsg24284g")))

(define-public crate-mobugsnag-0.1.1 (c (n "mobugsnag") (v "0.1.1") (d (list (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0vf8cf0ykk64wls71lglk4b7r4qxikxdvfakr6mmszx79r67ppl2")))

(define-public crate-mobugsnag-0.1.2 (c (n "mobugsnag") (v "0.1.2") (d (list (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1g6blcgr9xwidaadxmk8jianr2p1r70bxf5izr7mvjkjr1vsfbjv")))

