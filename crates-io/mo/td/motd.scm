(define-module (crates-io mo td motd) #:use-module (crates-io))

(define-public crate-motd-0.1.0 (c (n "motd") (v "0.1.0") (h "0a98ivl7f1alqswsw4avmpkq47i8fj9l0xpjbdzj6f0ncnqk16xa")))

(define-public crate-motd-0.2.0 (c (n "motd") (v "0.2.0") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)) (d (n "which") (r "^6") (d #t) (k 1)))) (h "17gw7qy68pz05xwicn0a2jv6cksw3s7p1fphn7a1xvc871xk5cd8")))

(define-public crate-motd-0.2.1 (c (n "motd") (v "0.2.1") (d (list (d (n "dlopen2") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (o #t) (d #t) (k 0)) (d (n "which") (r "^6") (d #t) (k 1)))) (h "024llj5nz4w1xzn4vbn3cm9paycrh11a01sz568js7iqq0w30zcf") (f (quote (("default" "socall")))) (s 2) (e (quote (("socall" "dep:lazy_static" "dep:libc" "dep:pam-sys" "dep:tempfile" "dep:serde_json" "dep:dlopen2" "dep:walkdir"))))))

