(define-module (crates-io mo nz monzilla_rs) #:use-module (crates-io))

(define-public crate-monzilla_rs-1.0.0 (c (n "monzilla_rs") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "command-group") (r "^2.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "easy-error") (r "^1.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "notify-debouncer-mini") (r "^0.2.1") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0ng3qpizrizxw31z7kamlv35y6p7jmyh5l62lx3cypjgqicwp5pf")))

