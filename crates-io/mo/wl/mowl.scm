(define-module (crates-io mo wl mowl) #:use-module (crates-io))

(define-public crate-mowl-1.0.0 (c (n "mowl") (v "1.0.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "13zmssw3d8dda44ba94ka04s82g8r8i4dhyy1i89879dwyxgksn2")))

(define-public crate-mowl-1.0.1 (c (n "mowl") (v "1.0.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "0ky1kq1vg9i57zrzqiizxspdcpsnb2hb2flcxr407js6k81h3k2v")))

(define-public crate-mowl-1.0.2 (c (n "mowl") (v "1.0.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "0ang4nz0pjyhh3qas2kvv0ksz14d6n9hfl2y1499z1f6vbl4slsx")))

(define-public crate-mowl-1.0.3 (c (n "mowl") (v "1.0.3") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "0sah7zy4qqhhdd0n4wm34zswlqhnr04p9caapxc307ma0y3lrlr3")))

(define-public crate-mowl-1.1.0 (c (n "mowl") (v "1.1.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)))) (h "16xmdshgx36b1w47vpg8p53ldaqdpn8hg479c4rbjf8f1r1zd3f2")))

(define-public crate-mowl-1.2.0 (c (n "mowl") (v "1.2.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "10lpjldyj0j9vqfdv5lzyk8srrilb6b3fwyhkj2ilw3lkwg283zn")))

(define-public crate-mowl-1.3.0 (c (n "mowl") (v "1.3.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0") (d #t) (k 0)))) (h "0b72jja5x3zg0jzggirgn9k6g3y9ijfflw7n4qggyw5x60iac8n4")))

(define-public crate-mowl-2.0.0 (c (n "mowl") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0kas5x2a9nhcky95p7zd5mlqb04vlzar6pg4mcag80pmks7f8azx")))

(define-public crate-mowl-2.1.0 (c (n "mowl") (v "2.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 0)))) (h "1v1x4sc833j6435ixhdrzxl0gkiai6ll8q49xq1w266z310gcyf0")))

(define-public crate-mowl-2.1.1 (c (n "mowl") (v "2.1.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.2.25") (d #t) (k 0)))) (h "02jd4z5m40xcbi4afvl9g09zpnipdkm45qj1r8819dir36xpx31l")))

