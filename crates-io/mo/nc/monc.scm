(define-module (crates-io mo nc monc) #:use-module (crates-io))

(define-public crate-monc-0.1.0 (c (n "monc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "monk-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "ritelinked") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1bcdgyb7w24s8bhjrn9092xkzm4kil8ppkyzi2h8pm35lyzwykwv")))

