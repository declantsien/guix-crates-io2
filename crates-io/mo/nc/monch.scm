(define-module (crates-io mo nc monch) #:use-module (crates-io))

(define-public crate-monch-0.1.0 (c (n "monch") (v "0.1.0") (h "0zsn0w15sszj7v43kl3wjnipbf7l2lnd9cyf6m6lqyybw44z1xh0")))

(define-public crate-monch-0.2.0 (c (n "monch") (v "0.2.0") (h "1f7yf2yhj1fvskyqar5hg1qflcn53qpjwn1lwmlq40b9gbr3f19m")))

(define-public crate-monch-0.2.1 (c (n "monch") (v "0.2.1") (h "1i7hx8bx2y5ddac890xi5vx84wvrhk8zf00pnfdm5nysmn1f5qn5")))

(define-public crate-monch-0.3.0 (c (n "monch") (v "0.3.0") (h "113v5v53kdxb4m93fpjw5zc29d490rx64limslgx28xa35n3vz7d")))

(define-public crate-monch-0.4.0 (c (n "monch") (v "0.4.0") (h "1yksyqpc8i0hq17zy2h1813a7sxkk9pgaa8h7bfbk9f9xp1y2ggi")))

(define-public crate-monch-0.4.1 (c (n "monch") (v "0.4.1") (h "0vv4kz8wwlcm1p05ffqfx5w2wbai9whrmb497g5xm35bjamw280i")))

(define-public crate-monch-0.4.2 (c (n "monch") (v "0.4.2") (h "1q2h240mciaqs2ln7rvhndwhjfhp3px4azzjgss1lbi3gpff2wxv")))

(define-public crate-monch-0.4.3 (c (n "monch") (v "0.4.3") (h "1hybn617653895f9qp9j7zlismvaxih10gy57nnmwbds8y4ah6a5")))

(define-public crate-monch-0.5.0 (c (n "monch") (v "0.5.0") (h "1shy00250d4ck00c6ilmbnmsg2mnk79qn4qkxbn2l54qzwrinb5m")))

