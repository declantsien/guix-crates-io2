(define-module (crates-io mo dt modtype) #:use-module (crates-io))

(define-public crate-modtype-0.1.0 (c (n "modtype") (v "0.1.0") (d (list (d (n "modtype_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1biaclcw36c974c7g305811yfk14ca775mqs5say2pggcps2q0c7")))

(define-public crate-modtype-0.1.1 (c (n "modtype") (v "0.1.1") (d (list (d (n "modtype_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "06dhy5jm7im04gzs3bg4s2yp2r6wa1fxipn4z8nydvdm59h1xzyf")))

(define-public crate-modtype-0.2.0 (c (n "modtype") (v "0.2.0") (d (list (d (n "modtype_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0vai91nvnf5gw110b839w78mynj817ngiabdma2pp4slcd2d3a9i")))

(define-public crate-modtype-0.3.0 (c (n "modtype") (v "0.3.0") (d (list (d (n "modtype_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1f7mgbs8hrhcv702j0dkiw419008p341qii1b2lg9jkvbrlgzzcg")))

(define-public crate-modtype-0.4.0 (c (n "modtype") (v "0.4.0") (d (list (d (n "modtype_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 2)))) (h "1xsdpsimwa2khnx9fn23i3y8lpl82dbbd0vzn92057875h1vc4sn")))

(define-public crate-modtype-0.5.0 (c (n "modtype") (v "0.5.0") (d (list (d (n "modtype_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 2)))) (h "19anlybvclcak0dsxpqv76jcynbc37ihhhcb99aw35nsdip665sq")))

(define-public crate-modtype-0.6.0 (c (n "modtype") (v "0.6.0") (d (list (d (n "modtype_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 2)))) (h "04w67akzx458053l0g4bsgf6is34p561rnwdk4dlyrqgqqr1zxvv")))

(define-public crate-modtype-0.7.0 (c (n "modtype") (v "0.7.0") (d (list (d (n "modtype_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "= 0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 2)))) (h "0jsy8f1rwva9xgxd8jc0f501kmk7z164zjm2xbc8pjg87ylgysvw")))

