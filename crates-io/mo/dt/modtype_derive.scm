(define-module (crates-io mo dt modtype_derive) #:use-module (crates-io))

(define-public crate-modtype_derive-0.1.0 (c (n "modtype_derive") (v "0.1.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z8jvyd7dp2zmsaslzfpw7f8ry9qk75v6xpad9xxd391pb80zi6l")))

(define-public crate-modtype_derive-0.1.1 (c (n "modtype_derive") (v "0.1.1") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a0i1svyiv12y0x269v2884z89dhhrzv7qi3qls4saflzklvaryz")))

(define-public crate-modtype_derive-0.2.0 (c (n "modtype_derive") (v "0.2.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10jcc50s6lxvqmwbb3amxkm9cj5k50zymz0yf2yxs09m7abp1f34")))

(define-public crate-modtype_derive-0.3.0 (c (n "modtype_derive") (v "0.3.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lsb2nb5nrra4jwin96yzqxrncnr4xsxv2kr0vd983h0aj5ybw4w")))

(define-public crate-modtype_derive-0.4.0 (c (n "modtype_derive") (v "0.4.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03shph78k7ifjscc4wd7ib09vbqxaqal5by1rfdg5wqk9l7ny956")))

(define-public crate-modtype_derive-0.5.0 (c (n "modtype_derive") (v "0.5.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i3qap9yypi9179v55j27f53v03b4jfi5cvci2zhnqgcycvwnxmr")))

(define-public crate-modtype_derive-0.6.0 (c (n "modtype_derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "modtype") (r "0.*") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sfhs2nig6z0x1nfm1fc8kn553s4dc02gr27r6yra11k4ykkigvm")))

(define-public crate-modtype_derive-0.7.0 (c (n "modtype_derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "modtype") (r "0.*") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hd9cq8bdzs8xkaan1vllf2xjykv7c465rb5058sjfsd4glypxdj")))

