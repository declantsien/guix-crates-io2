(define-module (crates-io mo se mosek) #:use-module (crates-io))

(define-public crate-mosek-0.0.1 (c (n "mosek") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ya3ycjdfmhlxl0kb2pcshafsd18ixsaacs0kc54ry3q5ki24bb8") (y #t)))

(define-public crate-mosek-0.1.0 (c (n "mosek") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zfapb2wrbrl1h887pyh6dhpwz8dqx78wdrwcyr4775f760z1kxq")))

(define-public crate-mosek-0.2.0 (c (n "mosek") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wmg6vq2gzyp17nbnfs9rha6gb0zp419xixb768b6996lilg04jz")))

(define-public crate-mosek-10.0.0-beta.0 (c (n "mosek") (v "10.0.0-beta.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cjfchiwjzbyal7zrbvfsnskqyivdjkwrzxm77pndniphb1fgs0c")))

(define-public crate-mosek-10.0.0-beta.1 (c (n "mosek") (v "10.0.0-beta.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10j0yrp14dci5kr23j47wmrsz3dzxvvgy82x07ay68s0cr5qjw7i")))

(define-public crate-mosek-10.0.0 (c (n "mosek") (v "10.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13ilqb8a11pkqxqqhvqdql88hn6dl2hd9ppjznpzf9xx61fl5z58")))

(define-public crate-mosek-10.0.1 (c (n "mosek") (v "10.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y8dkd4bsjk7dkqq5wffl0phhhwncq1qkivk5skivzfn9prn6m59")))

(define-public crate-mosek-10.0.2 (c (n "mosek") (v "10.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01wxp32y835wjlvwb516ynqrmx6wby1k1fixylfsich99lbg1pws")))

(define-public crate-mosek-10.0.3 (c (n "mosek") (v "10.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "070mc89s667qr918qyxw4yhg1zl5ycrs1zkfh71rp9bg0dj1w6ah")))

(define-public crate-mosek-10.1.0-beta1 (c (n "mosek") (v "10.1.0-beta1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "065dnglzzz8wx8zzmbggi4x72klnqaa4d2my7mm0a23a4yvy93ha") (r "1.59")))

(define-public crate-mosek-10.1.0-beta2 (c (n "mosek") (v "10.1.0-beta2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10fiachagcpw968f2ca60a0qr1ss8w28ycmw8dclhyl0j49j1n2f") (r "1.59")))

(define-public crate-mosek-10.1.0 (c (n "mosek") (v "10.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04yd4288qiry02014xb826ld5mm76v4i68gqgvwwpgnimjn75wh0") (r "1.59")))

(define-public crate-mosek-10.1.1 (c (n "mosek") (v "10.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gr99s06g2y10c71hxsrcj58qgnzxggx4qvq7g13ysi6drxrfnb5") (r "1.59")))

(define-public crate-mosek-10.1.2 (c (n "mosek") (v "10.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q77n53wabz3klxflk6p60qbdsan6n47dpfjhn7svlmlqfxma8p6") (r "1.59")))

(define-public crate-mosek-10.1.3 (c (n "mosek") (v "10.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h3rvwgzfvrx6600z8rv3da2x1apavbrn5540095rl2fb2d5la01") (r "1.59")))

(define-public crate-mosek-10.1.4 (c (n "mosek") (v "10.1.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bkf7yd7ldsx98magwpjk4brs87v6aq5lnpnh8h4j9yvby4sby8m") (r "1.59")))

(define-public crate-mosek-10.1.5 (c (n "mosek") (v "10.1.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nrijkvw5rw5xlpi1xphz5zbaxfdnrh1zb9pi55f0glkjmzcqr1d") (r "1.59")))

