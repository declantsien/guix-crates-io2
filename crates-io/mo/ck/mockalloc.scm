(define-module (crates-io mo ck mockalloc) #:use-module (crates-io))

(define-public crate-mockalloc-0.1.0 (c (n "mockalloc") (v "0.1.0") (d (list (d (n "mockalloc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0qn9v8iq8fmwflrg1ayhkw2c6vxrqzkpzn5xvjb2n2sg1ka9d1q7")))

(define-public crate-mockalloc-0.1.1 (c (n "mockalloc") (v "0.1.1") (d (list (d (n "mockalloc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1skh0csl8rrkczk6l3fxdhdc56gp0vzh557d4jbimpis62bnshhk")))

(define-public crate-mockalloc-0.1.2 (c (n "mockalloc") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mockalloc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "151al48sb5mil1ybfj3h3iyl0810yigmyb1ail1apchnc5496v2s") (f (quote (("tracing" "backtrace"))))))

