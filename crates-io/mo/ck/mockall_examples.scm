(define-module (crates-io mo ck mockall_examples) #:use-module (crates-io))

(define-public crate-mockall_examples-0.1.0 (c (n "mockall_examples") (v "0.1.0") (d (list (d (n "downcast") (r "^0.10") (d #t) (k 0)) (d (n "fragile") (r "^0.3") (d #t) (k 0)) (d (n "mockall") (r "= 0.1.0") (d #t) (k 0)) (d (n "predicates-tree") (r "^1.0") (d #t) (k 0)))) (h "0a1zi75698k1ncw6hs459f7ynf9p5p5hz1m8b0cavzg1ck81p4f4") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.2.0 (c (n "mockall_examples") (v "0.2.0") (d (list (d (n "downcast") (r "^0.10") (d #t) (k 0)) (d (n "fragile") (r "^0.3") (d #t) (k 0)) (d (n "mockall") (r "= 0.2.0") (d #t) (k 0)) (d (n "predicates-tree") (r "^1.0") (d #t) (k 0)))) (h "0w8wa31151l8x9xkhb760vcg20xlq52c6isqkshn45g7380c1mbb") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.3.0 (c (n "mockall_examples") (v "0.3.0") (d (list (d (n "mockall") (r "^0.3.0") (d #t) (k 0)))) (h "1mhk0c42hsfjaxsz4rvd5038yqyp3s4s1mh8frimsdf7kh6bl8wj") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.4.0 (c (n "mockall_examples") (v "0.4.0") (d (list (d (n "mockall") (r "^0.4.0") (d #t) (k 0)))) (h "0dl367nx9c71b3s9bwpkmjzmpwp3z14m6pnbpdy3hiabmkx3zz20") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.5.0 (c (n "mockall_examples") (v "0.5.0") (d (list (d (n "mockall") (r "= 0.5.0") (d #t) (k 0)))) (h "0wsardjk1i69n8vqqfp7m23hnn0qd86rmj265by5bpqxip33jy9w") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.5.1 (c (n "mockall_examples") (v "0.5.1") (d (list (d (n "mockall") (r "= 0.5.1") (d #t) (k 0)))) (h "0dkja0df18869farb1bcj3ynazjmimj8wz84jshg05hk0phqjn6s") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.5.2 (c (n "mockall_examples") (v "0.5.2") (d (list (d (n "mockall") (r "= 0.5.2") (d #t) (k 0)))) (h "0lvjii64x6vqpslsx12c8dbyg5j50sc0k5kzk38qcpr4fic089lj") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.6.0 (c (n "mockall_examples") (v "0.6.0") (d (list (d (n "mockall") (r "= 0.6.0") (d #t) (k 0)))) (h "14rp91h39dx372jp0ajjxq6xxljf81rg3r0nfh3j9r2123bw2rv4") (f (quote (("nightly-docs"))))))

(define-public crate-mockall_examples-0.7.0 (c (n "mockall_examples") (v "0.7.0") (d (list (d (n "mockall") (r "= 0.7.0") (d #t) (k 0)))) (h "1x4i56z2wgqmp45gdx8km2i91w8a3mnl6n531c118ilzzzwz6qmq") (f (quote (("nightly"))))))

(define-public crate-mockall_examples-0.7.1 (c (n "mockall_examples") (v "0.7.1") (d (list (d (n "mockall") (r "= 0.7.1") (d #t) (k 0)))) (h "0wgyyyawkrv35rwi4qkc2zaxz7ri39b2ayraj0arvlkc7hbg4yb0") (f (quote (("nightly"))))))

(define-public crate-mockall_examples-0.7.2 (c (n "mockall_examples") (v "0.7.2") (d (list (d (n "mockall") (r "=0.7.2") (d #t) (k 0)))) (h "020wx49c56lza975nmqxx1r1cv9kbpa7mipxs7vc50xa8srzhdkj") (f (quote (("nightly"))))))

(define-public crate-mockall_examples-0.7.3 (c (n "mockall_examples") (v "0.7.3") (d (list (d (n "mockall") (r "=0.7.2") (d #t) (k 0)))) (h "1278icp5iljgf64fzi5cf6ipaxd85zfajgwhwbjxwp005l9cqm75") (f (quote (("nightly"))))))

