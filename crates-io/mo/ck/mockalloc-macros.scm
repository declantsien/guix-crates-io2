(define-module (crates-io mo ck mockalloc-macros) #:use-module (crates-io))

(define-public crate-mockalloc-macros-0.1.0 (c (n "mockalloc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0rgvsq34i9k0w7yclz894ibi5dxxwn21vhr2ywal3dnsmv6msmi3")))

