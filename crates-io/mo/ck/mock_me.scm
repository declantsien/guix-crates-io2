(define-module (crates-io mo ck mock_me) #:use-module (crates-io))

(define-public crate-mock_me-0.2.0 (c (n "mock_me") (v "0.2.0") (d (list (d (n "mock_me_test_context") (r "^0.1.0") (d #t) (k 0)))) (h "1f4ba4jgcp11zd8c4pca0cxzngkfkzqyv1n7c3hfv5xhyf1jpciz")))

(define-public crate-mock_me-0.2.2 (c (n "mock_me") (v "0.2.2") (d (list (d (n "mock_me_test_context") (r "^0.1.1") (d #t) (k 0)))) (h "1p9abiq06xjzvdkfxhgv5qkhzz1gpvqdxsz631giljd868y5d2nw")))

(define-public crate-mock_me-0.2.3 (c (n "mock_me") (v "0.2.3") (d (list (d (n "mock_me_test_context") (r "^0.1.2") (d #t) (k 0)))) (h "1ng68qk2rmwasjw3gkk2c6a77vadkf0iyfpmfyw7bj98cgq31qf5")))

