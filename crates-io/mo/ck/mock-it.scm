(define-module (crates-io mo ck mock-it) #:use-module (crates-io))

(define-public crate-mock-it-0.1.0 (c (n "mock-it") (v "0.1.0") (d (list (d (n "table-test") (r "^0.2.0") (d #t) (k 2)))) (h "0li5rl5kqidhjd5z46q11wrhx80hxz3550zf6qkylpzd4c9256px")))

(define-public crate-mock-it-0.2.0 (c (n "mock-it") (v "0.2.0") (d (list (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0isszhna2blmb1wqk12s4n3vpn9q3fryp9b71a2z8c292n81k1pk")))

(define-public crate-mock-it-0.2.1 (c (n "mock-it") (v "0.2.1") (d (list (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "16hka73j2blqq8ppkw2fgnxi3y7j4bq825v220l2qdb5xg5gvdcv")))

(define-public crate-mock-it-0.3.0 (c (n "mock-it") (v "0.3.0") (d (list (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0xzgx5p82jfm4mcc4w7d5br60img02p7mpli73rbbkns7k73lk8l")))

(define-public crate-mock-it-0.3.1 (c (n "mock-it") (v "0.3.1") (d (list (d (n "mock-it_codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "0j4g2sc76lzalh5qyavjn47jlgk6q32j7zi40cy5szqb5cn1lywb")))

(define-public crate-mock-it-0.4.0 (c (n "mock-it") (v "0.4.0") (d (list (d (n "mock-it_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)))) (h "1x84sjr3m8adl2dsgqqnjxqd3cfmxkfiqxkv222ilnqhbmc0i9zg")))

(define-public crate-mock-it-0.5.0 (c (n "mock-it") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0cy2j34zgky6qrndjmy9vbp0y0261y0sw9cdxqshwyjg79w4ryz9")))

(define-public crate-mock-it-0.5.1 (c (n "mock-it") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i5a7wjrwmfswfzvjb3jsj7wxlk7rzl3pcanrj5vldcgj1yfldg2")))

(define-public crate-mock-it-0.6.0 (c (n "mock-it") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0pvkmhb9lmp2vygr93xch7icn4fx8rrjln065s2hpc7jz6flx5w2")))

(define-public crate-mock-it-0.7.0 (c (n "mock-it") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1ydq2804bimfvqyyxyfxb6q30l8v2sx186wbd3y62j9cr9103l3m")))

(define-public crate-mock-it-0.8.0 (c (n "mock-it") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1z7s2rs866izz249x5jb07r1mkhjbwliandl09jnk1429k7vdj5g")))

(define-public crate-mock-it-0.8.1 (c (n "mock-it") (v "0.8.1") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.8.1") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0g87486kv8zdy46f82v5yd7iga28cvq6vb2i1lcbwil9ki8q867d")))

(define-public crate-mock-it-0.9.0 (c (n "mock-it") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.5") (d #t) (k 2)) (d (n "mock-it_codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "table-test") (r "^0.2.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1brdbpqcmablklkymrm33ij5whkvka10lr2fmqbz2r25b8jgcycw")))

