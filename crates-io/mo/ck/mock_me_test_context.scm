(define-module (crates-io mo ck mock_me_test_context) #:use-module (crates-io))

(define-public crate-mock_me_test_context-0.1.0 (c (n "mock_me_test_context") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0qjambxiy6pq2g0651d264p1021h1synhjxzc2vnzccmldb2qrh6")))

(define-public crate-mock_me_test_context-0.1.1 (c (n "mock_me_test_context") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1v76vy1ifx534bah7kvscv46mg72bkv2xpwqw1f7liny6ak5w9wh")))

(define-public crate-mock_me_test_context-0.1.2 (c (n "mock_me_test_context") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0s80kmh4xg95l0wznzd3hak97dhkb2wz41iivgyd0ika5qjp5hap")))

