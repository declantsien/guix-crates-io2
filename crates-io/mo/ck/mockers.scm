(define-module (crates-io mo ck mockers) #:use-module (crates-io))

(define-public crate-mockers-0.1.0 (c (n "mockers") (v "0.1.0") (d (list (d (n "mockers_macros") (r "^0.1.0") (d #t) (k 2)))) (h "0ib5hjc0fn8x7m8a1q2q51qram7swg2jqf3b1ssvndswnn7ahjji")))

(define-public crate-mockers-0.2.0 (c (n "mockers") (v "0.2.0") (d (list (d (n "mockers_macros") (r "^0.2.0") (d #t) (k 2)))) (h "12ic9xgy4cdnmc7nmw35jw1lc049s9grdhijk7p6f0b0b9fcyiag")))

(define-public crate-mockers-0.2.1 (c (n "mockers") (v "0.2.1") (d (list (d (n "mockers_macros") (r "^0.2.1") (d #t) (k 2)))) (h "03ys0dg23mlnq7zyxw28r5cql6rh12xqljr472nlmddspf8ba3v8")))

(define-public crate-mockers-0.2.2 (c (n "mockers") (v "0.2.2") (d (list (d (n "mockers_macros") (r "^0.2.2") (d #t) (k 2)))) (h "0grajh5nsgh4schsm5zjicimd7c4zi4y15543hmsd1qn7ikjsigy")))

(define-public crate-mockers-0.3.0 (c (n "mockers") (v "0.3.0") (d (list (d (n "mockers_macros") (r "^0.3.0") (d #t) (k 2)))) (h "0kkqavr89pdgl6d6m9zdkrwi203d2nps8x6g2sm9dbxf0pwrh6gc")))

(define-public crate-mockers-0.4.0 (c (n "mockers") (v "0.4.0") (d (list (d (n "mockers_macros") (r "^0.4.0") (d #t) (k 2)))) (h "0ngdf99r9h0sakinl4wxilsvvjna4gl03x2lh7g8nmsxzf0l3vvc")))

(define-public crate-mockers-0.4.1 (c (n "mockers") (v "0.4.1") (d (list (d (n "mockers_macros") (r "^0.4.1") (d #t) (k 2)))) (h "1c5mfmy4czjlim6a99ckqv7ag3a45j7wwa1yhlr9z7d8nzp0agik")))

(define-public crate-mockers-0.4.2 (c (n "mockers") (v "0.4.2") (d (list (d (n "mockers_macros") (r "^0.4.2") (d #t) (k 2)))) (h "04mmc0dfbylzli39zxgdyd3nbpfizxirp19sysmx5x1ywh24wwgk")))

(define-public crate-mockers-0.4.3 (c (n "mockers") (v "0.4.3") (d (list (d (n "mockers_macros") (r "^0.4.3") (d #t) (k 2)))) (h "1mmp935z0dpc2djsgy43pgb8blsyqznhhzvkxkf15w74i0j4aj1z")))

(define-public crate-mockers-0.4.4 (c (n "mockers") (v "0.4.4") (d (list (d (n "mockers_macros") (r "^0.4.4") (d #t) (k 2)))) (h "1kkqb4z7y1slg0c0s46h7bvb6nk4car908baprvfbdsiqpbgzrsw")))

(define-public crate-mockers-0.4.5 (c (n "mockers") (v "0.4.5") (d (list (d (n "mockers_macros") (r "^0.4.5") (d #t) (k 2)))) (h "03yik4qhvcymg29hy2jmc8w625pq8kgwy1bdl21pbsgwyd78b965")))

(define-public crate-mockers-0.4.6 (c (n "mockers") (v "0.4.6") (d (list (d (n "mockers_macros") (r "^0.4.6") (d #t) (k 2)))) (h "09lcymd3ny7csrsm153h55xzkg0y3y5xlmkxqdl7nnl9rqn950cj")))

(define-public crate-mockers-0.4.7 (c (n "mockers") (v "0.4.7") (d (list (d (n "mockers_macros") (r "^0.4.7") (d #t) (k 2)))) (h "0919c25vvkv005db1iss10cgngh8fbwf93v5m8bkjdvwv20205g2")))

(define-public crate-mockers-0.4.8 (c (n "mockers") (v "0.4.8") (d (list (d (n "mockers_macros") (r "^0.4.8") (d #t) (k 2)))) (h "16d7d1mlfcd94wzr3kzp58m25788k8r41297qhs57458c9z5pqf6")))

(define-public crate-mockers-0.5.1 (c (n "mockers") (v "0.5.1") (d (list (d (n "mockers_macros") (r "^0.5.1") (d #t) (k 2)))) (h "1ra7ys5pffi2622gyaa7fiy4nhgsrdwacwmj95v83ly2y9m0isg9") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.2 (c (n "mockers") (v "0.5.2") (d (list (d (n "mockers_macros") (r "^0.5.2") (d #t) (k 2)))) (h "14mffdm5v9cdbrlvzpmna9d9rr8arj1hasrhyz26c1az7dpwmy95") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.3 (c (n "mockers") (v "0.5.3") (d (list (d (n "mockers_macros") (r "^0.5.3") (d #t) (k 2)))) (h "16q2nppviy0a2cfa2m5gqr1bqn9rl83yx17z7i91ydris2l32mih") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.4 (c (n "mockers") (v "0.5.4") (d (list (d (n "mockers_macros") (r "^0.5.4") (d #t) (k 2)))) (h "1by9r071jxv344mnkcp6qx39p4gfi5p1ky1fv1blrpwlvbsklhh4") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.5 (c (n "mockers") (v "0.5.5") (d (list (d (n "mockers_macros") (r "^0.5.5") (d #t) (k 2)))) (h "05wqq4nsg1xq93i92b2w91g931f93kld4r5vypgn98yqan5cy5v9") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.6 (c (n "mockers") (v "0.5.6") (d (list (d (n "mockers_macros") (r "^0.5.6") (d #t) (k 2)))) (h "1vdjnghl84b6vw9f7rwkvs56az2qbhj4kbiwzwzk1v7cp8ks8k5v") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.7 (c (n "mockers") (v "0.5.7") (d (list (d (n "mockers_macros") (r "^0.5.7") (d #t) (k 2)))) (h "1cmgi3izw8r9ri5fdqsib02pn057k3ls775yd17xw9398ijzd0hr") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.8 (c (n "mockers") (v "0.5.8") (d (list (d (n "mockers_macros") (r "^0.5.8") (d #t) (k 2)))) (h "0ph586irxqiynffa935l9c0r8is7l69m3sfisx0r9frnak8z8khi") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.9 (c (n "mockers") (v "0.5.9") (d (list (d (n "mockers_macros") (r "^0.5.9") (d #t) (k 2)))) (h "02rd3pd0k5srw3x2cxqk5qjsri40jxm0abygdzaki6x7hjvxqq34") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.10 (c (n "mockers") (v "0.5.10") (d (list (d (n "mockers_macros") (r "^0.5.10") (d #t) (k 2)))) (h "0rhkvjqwks5dfd8jkhcv4hw8x3gbx8vidxz8na7kmb7mpms61bzj") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.5.11 (c (n "mockers") (v "0.5.11") (d (list (d (n "mockers_macros") (r "^0.5.11") (d #t) (k 2)))) (h "10j24mjbzk8ld3b6w25ccr5m3l426n2i1c63sr735jpn9ybgxz4r") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.6.0 (c (n "mockers") (v "0.6.0") (d (list (d (n "mockers_macros") (r "^0.6.0") (d #t) (k 2)))) (h "07xp2fwhabgmnckl194mxp7983vbx0sph1yzaf9j1i6qs9xgikp7") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.6.1 (c (n "mockers") (v "0.6.1") (d (list (d (n "mockers_macros") (r "^0.6.1") (d #t) (k 2)))) (h "04vkp9xw5big4rdk6nhln10qpqw261ylq0snc6nb828p2426i9bk") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.6.2 (c (n "mockers") (v "0.6.2") (d (list (d (n "mockers_macros") (r "^0.6.2") (d #t) (k 2)))) (h "06dbrsw98b3alg7fmi7zmr0qxby23zz4g29anzv30s1xjv6l6832") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.6.3 (c (n "mockers") (v "0.6.3") (d (list (d (n "mockers_macros") (r "^0.6.3") (d #t) (k 2)))) (h "1rx7hjndbnv8cb5rq7x884hd92sy628cr7am5yraamfhkgkqi3as") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.7.0 (c (n "mockers") (v "0.7.0") (d (list (d (n "mockers_macros") (r "^0.7.0") (d #t) (k 2)))) (h "06xp4g2ndxy3y9h9fmvc1lww8iipmkxk00f6aniwvnkidqvav8vk") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.7.1 (c (n "mockers") (v "0.7.1") (d (list (d (n "mockers_macros") (r "^0.7.1") (d #t) (k 2)))) (h "1nm8d1f3m69djqs7jinkcsff4zkr16jx5f6q4lsrhdd8blmnanba") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.8.0 (c (n "mockers") (v "0.8.0") (d (list (d (n "mockers_macros") (r "^0.8.0") (d #t) (k 2)))) (h "1nrchqj2nmsdhi90xvxqnc3rm0wljyj0dj3wxzpbr1d55a6kpzv6") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.9.0 (c (n "mockers") (v "0.9.0") (d (list (d (n "mockers_macros") (r "^0.9.0") (d #t) (k 2)))) (h "1rg5cxsmxy9ra4vgia9rh0r53bmdpg7z7brn5y6pr1bkylp4j08r") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.9.1 (c (n "mockers") (v "0.9.1") (d (list (d (n "mockers_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1s6zxr6qhxh2949ygw0kf0ydjykdzryifi6k40xd610k0795qp5i") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.9.2 (c (n "mockers") (v "0.9.2") (d (list (d (n "mockers_macros") (r "^0.9.2") (d #t) (k 2)))) (h "0mcxdjhw7agjrh0bi5w81y7kqcwjhjm6h9k9s1g1m4yg7slz49mn") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.9.3 (c (n "mockers") (v "0.9.3") (d (list (d (n "mockers_macros") (r "^0.9.3") (d #t) (k 2)))) (h "1wm51pj6a10c67acff0006plm623fdx1vr6q7sjqxfacchch3p0f") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.9.4 (c (n "mockers") (v "0.9.4") (d (list (d (n "mockers_macros") (r "^0.9.4") (d #t) (k 2)))) (h "1w0nkgankfdlayalk2pf94b8h0hq82fnkn39d6c948iwnj1yc2v1") (f (quote (("nightly") ("default" "nightly") ("debug" "mockers_macros/debug"))))))

(define-public crate-mockers-0.10.0 (c (n "mockers") (v "0.10.0") (h "1xqbs3w6r3k2nrqg1k668zfnjp8740294qcn3gg5r985ha4kmlqq") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.10.1 (c (n "mockers") (v "0.10.1") (h "0nwcwd4xpmfcdccrg4crlcs1824nmfc30yn18xkji31hc7wmsw33") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.10.2 (c (n "mockers") (v "0.10.2") (h "1z8zphx3d6hllwl4mmi3362l20s0d6ni5763n2cyy3q4c6ljx3ln") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-mockers-0.11.0 (c (n "mockers") (v "0.11.0") (h "1zc4llmq9prqh1b1r4zw815f0s57cfd9vmp5a58hkmcjq1vypnny")))

(define-public crate-mockers-0.11.1 (c (n "mockers") (v "0.11.1") (h "1nz14rz72p98zqlkdhchjw7ifiwpczhjdnj3xihnc7ahcv7v4xy6")))

(define-public crate-mockers-0.11.2 (c (n "mockers") (v "0.11.2") (h "05s021wy4k1bnd1p8sgndr2f5l5dp19y6l70ljcl5xljma073z4n")))

(define-public crate-mockers-0.12.0 (c (n "mockers") (v "0.12.0") (h "1ls9pr1wzv2mg4s4849lhxng0g1m98l3sxl7hq6vp30dpa8n5wzs")))

(define-public crate-mockers-0.12.1 (c (n "mockers") (v "0.12.1") (d (list (d (n "mockers_derive") (r "^0.12.1") (d #t) (k 2)))) (h "05mzpy6zbm03yhv6md1s0wfziqpidwfcvjqh4qrs7wb15h2mnn7h") (f (quote (("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.12.2 (c (n "mockers") (v "0.12.2") (d (list (d (n "mockers_derive") (r "^0.12.2") (d #t) (k 2)))) (h "01y9pwxc90cf4hpx0154l7k2y5781ifhmr7z12zg1z8866h39f0j") (f (quote (("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.13.0 (c (n "mockers") (v "0.13.0") (d (list (d (n "mockers_derive") (r "^0.13.0") (d #t) (k 2)))) (h "0iq49ylx0vs1qr26rlpmykfqrfjk5iyzds40yq93zr1ajsshpahc") (f (quote (("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.13.1 (c (n "mockers") (v "0.13.1") (d (list (d (n "mockers_derive") (r "^0.13.1") (d #t) (k 2)))) (h "0pl4dxac2jbhz74cmyzbp2i8y1ykzrma28iynhnjwrzsm7mdd38h") (f (quote (("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.13.2 (c (n "mockers") (v "0.13.2") (d (list (d (n "mockers_derive") (r "^0.13.2") (d #t) (k 2)))) (h "0hi0pfxai8lyxzr6blvj7vqpfy5fd1vdsdd41jy3h2mlsid3qlc9") (f (quote (("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.13.3 (c (n "mockers") (v "0.13.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.13.3") (d #t) (k 2)) (d (n "select-rustc") (r "^0.1.2") (d #t) (k 0)))) (h "18grb0lcwv3nh4fr1mw3sa6mv7ryf8gmfq6nwfsrc3m7nhsgr2nb") (f (quote (("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.13.4 (c (n "mockers") (v "0.13.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.13.4") (d #t) (k 2)) (d (n "select-rustc") (r "^0.1.2") (d #t) (k 0)))) (h "0cpd436qyrh2990pxyi4b61aqnz2fll85q357sznmqflhid0p59p") (f (quote (("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.20.0 (c (n "mockers") (v "0.20.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.20.0") (d #t) (k 2)) (d (n "select-rustc") (r "^0.1.2") (d #t) (k 0)))) (h "1az8dsf344chch3yjm2fm2xvhfj7is26jakkq87yjx8ywkvf4d0m") (f (quote (("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.21.0 (c (n "mockers") (v "0.21.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.21.0") (d #t) (k 2)) (d (n "select-rustc") (r "^0.1.2") (d #t) (k 0)))) (h "1jbfni3kn75s9r7bzg1lvbbwcmz1zbqzyh4n56yywq86awa6vwwb") (f (quote (("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.22.0 (c (n "mockers") (v "0.22.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.22.0") (d #t) (k 2)))) (h "1x5gw5a2k3hgklzlz0s5l6ryw0vgdbqlc79vzsg6wcd2a2jc6wrj") (f (quote (("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

(define-public crate-mockers-0.23.0 (c (n "mockers") (v "0.23.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "mockers_derive") (r "^0.23.0") (d #t) (k 2)) (d (n "no_deadlocks") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "07dkqycd3c6bdavfarf04qp8rqph7gx66ihzwf7np673v969a9ff") (f (quote (("use_no_deadlocks" "no_deadlocks") ("nightly" "mockers_derive/nightly") ("debug" "mockers_derive/debug"))))))

