(define-module (crates-io mo ck mock4rs) #:use-module (crates-io))

(define-public crate-mock4rs-0.1.0 (c (n "mock4rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mxa3q27vw052qw1lzscrna97y3cbmrn3s2i4hmzjqlf7mzv2lpk")))

(define-public crate-mock4rs-0.1.1 (c (n "mock4rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c4xhidgzxycxzz1z9rwgrm8fmvw9wxi6fz83sqmnrk3iw6ik4j4")))

