(define-module (crates-io mo ck mock_derive) #:use-module (crates-io))

(define-public crate-mock_derive-0.3.0 (c (n "mock_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0hh84ghs443rdz4wsbbd5xi97nsqbq0pil3r6cpvk7npw0dc3jjj")))

(define-public crate-mock_derive-0.3.1 (c (n "mock_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0z86ashjnky768l8c5xhqzp37llgbnvw39h1hw64l48c9nmnxq4l")))

(define-public crate-mock_derive-0.4.0 (c (n "mock_derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5w40s09389b1wp9fw1iy9wl3wydnv1vc1m9wknzcsqbn1m929q")))

(define-public crate-mock_derive-0.4.1 (c (n "mock_derive") (v "0.4.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1skbazyhqdfwp17w4hsbphi6w4krb887v40hz0f4bb5w4lm1nxbv")))

(define-public crate-mock_derive-0.5.0 (c (n "mock_derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "05n4pzmxipy3vha1arcvqxa3yhyygb166r2nl5m3qa7200dz7w95")))

(define-public crate-mock_derive-0.6.0 (c (n "mock_derive") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0qzwdlqz709fsbi0n43mmpb33i6r67nbl8vw9rl777nwldadpqd3")))

(define-public crate-mock_derive-0.6.1 (c (n "mock_derive") (v "0.6.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1hrzbyi8bc23fmd5c35i500673dsza95lk4djpydp8pssxnggdy6")))

(define-public crate-mock_derive-0.7.0 (c (n "mock_derive") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0pi1r5h3my1n3cdm981pc40y01l6r4n4nz45y8nzsrp6xw18kr25")))

(define-public crate-mock_derive-0.8.0 (c (n "mock_derive") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full"))) (d #t) (k 0)))) (h "09m37yk99irbn0i4cpn5qb7zk4v55vbdb6dr2vvm6j887bqrbzvj")))

