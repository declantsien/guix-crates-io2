(define-module (crates-io mo ck mock-syn-macros) #:use-module (crates-io))

(define-public crate-mock-syn-macros-0.1.5 (c (n "mock-syn-macros") (v "0.1.5") (d (list (d (n "mock-syn-common") (r "^0.1.5") (d #t) (k 0)) (d (n "mock-syn-common") (r "^0.1.5") (f (quote ("test"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ahl8i664jzrs5xq14brvkali45ngsb4nmgyqyq0n6czigsnkc6j")))

(define-public crate-mock-syn-macros-0.1.6 (c (n "mock-syn-macros") (v "0.1.6") (d (list (d (n "mock-syn-common") (r "^0.1.6") (d #t) (k 0)) (d (n "mock-syn-common") (r "^0.1.6") (f (quote ("test"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a43k6vhl582hgkzkz3c5kl3ifv9swww0ns6fs08awwjk36axh9f")))

