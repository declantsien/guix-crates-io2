(define-module (crates-io mo ck mock-it_codegen) #:use-module (crates-io))

(define-public crate-mock-it_codegen-0.1.1 (c (n "mock-it_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w2q8ph9aig0nj43ziicyrykkwda384kqvk8jj6p2kmi3z4fh3m9")))

(define-public crate-mock-it_codegen-0.2.0 (c (n "mock-it_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ciz09av7xpb573almaaaqvsdvyyl7ira9dimw6mrv0d9aq9n37q")))

(define-public crate-mock-it_codegen-0.3.0 (c (n "mock-it_codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "019d29v60g5n9dvbqi6nwmiw8l8dsg5qg9sp1vxp8vhpxw7bnblg")))

(define-public crate-mock-it_codegen-0.4.0 (c (n "mock-it_codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15kqgfvk3igrx161nnwp306s2h97wvbfdbhfabjv64ihgfs7jcgv")))

(define-public crate-mock-it_codegen-0.6.0 (c (n "mock-it_codegen") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0njn9jj2j57si9spl0zy1h1z79nqlcfs6fyljxgwhb62il85chzk")))

(define-public crate-mock-it_codegen-0.7.0 (c (n "mock-it_codegen") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rn2px78d0565gp1v0hqrs6d4h7nqwv6s7g41d0ripjr331y5xpl")))

(define-public crate-mock-it_codegen-0.8.0 (c (n "mock-it_codegen") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07vja3n6whgmdysj2ziml8mic26zjc2b33hs5r3szflh3wjf64jy")))

(define-public crate-mock-it_codegen-0.8.1 (c (n "mock-it_codegen") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vf2cgjfzxqqikci242vcj97iirczr4w5y15xrqkh4f23gd1wzpb")))

(define-public crate-mock-it_codegen-0.9.0 (c (n "mock-it_codegen") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13z170dc7kr1lzp5x432yb92kzgh29sfb7n5ffvr744hgkp8ga6q")))

