(define-module (crates-io mo ck mockem) #:use-module (crates-io))

(define-public crate-mockem-0.1.0 (c (n "mockem") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "010lmcpw0cq0cd1bwcg1m87vaam382vz3kh6lrxd475m5isn5kb4") (r "1.56")))

(define-public crate-mockem-0.1.1 (c (n "mockem") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1vvcw1ndzb7nfxl4avw7g66bgbfg9g25dya9yw5j3dx7i03zabkd") (r "1.56")))

(define-public crate-mockem-0.2.0 (c (n "mockem") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1gz9hwkwxrlvxy2xyrdnsnql7r3h289j9836njhjakgb967s77al") (r "1.56")))

(define-public crate-mockem-0.2.1 (c (n "mockem") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "137bhbmvsjw4if5if1vhd5a8vk5s22p2maakq9n16n2ajbsjgilq") (r "1.56")))

(define-public crate-mockem-0.2.2 (c (n "mockem") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1rqx2sks8b95d2yp8r8s3xy6n5g4g11mqr10ldhmwb76ysa6cyal") (r "1.56")))

(define-public crate-mockem-0.2.3 (c (n "mockem") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockem-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1x1p49g00nd5gkf3y2hsghlb3f6rsdlpsi25iygn6dk7rb577b7f") (r "1.56")))

