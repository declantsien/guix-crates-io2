(define-module (crates-io mo ck mock-numeric-condition) #:use-module (crates-io))

(define-public crate-mock-numeric-condition-0.8.8 (c (n "mock-numeric-condition") (v "0.8.8") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "10d913my8wpqnzckv6wd6ccnja1908qp21mqzmyb69y1lbr0qr82") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

(define-public crate-mock-numeric-condition-0.8.9 (c (n "mock-numeric-condition") (v "0.8.9") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "04r5p1p5ic2zpq46kbcsi3zvhpg4z0m7wzanlifkcik2fqmkjwlx") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

