(define-module (crates-io mo ck mock-rs) #:use-module (crates-io))

(define-public crate-mock-rs-0.1.0 (c (n "mock-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vsz73a1smnlxbk6vwgh4331k5v8nvv3f7s016j59r5wbsxmmkc7")))

(define-public crate-mock-rs-0.2.0 (c (n "mock-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0j98dq146l2ml40las2rpmmkq3qfb0xfvzayk2ifk37k1v341yc1")))

(define-public crate-mock-rs-0.3.0 (c (n "mock-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mhb2i0l0gffc0cdpzr5jgdvdgbg85f5hmi5n3pcah8br8bmqjh8")))

