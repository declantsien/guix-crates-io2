(define-module (crates-io mo ck mocktopus) #:use-module (crates-io))

(define-public crate-mocktopus-0.1.0 (c (n "mocktopus") (v "0.1.0") (d (list (d (n "mocktopus_macros") (r "^0.1.0") (d #t) (k 0)))) (h "17g5zpcprjcca5sgm5pzi8al47lkkphxng73jzpyv6kq22i910f6") (y #t)))

(define-public crate-mocktopus-0.1.1 (c (n "mocktopus") (v "0.1.1") (d (list (d (n "mocktopus_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0qm2q1crrzz6pg5wr7x85bp9r6hzk2q4f58hppd4w958pv5gpicx")))

(define-public crate-mocktopus-0.1.2 (c (n "mocktopus") (v "0.1.2") (d (list (d (n "mocktopus_macros") (r "= 0.1.2") (d #t) (k 0)))) (h "0qcy1fznny5h24knjk2il0z0kz9b6h6ai3pfdrc6i9ikapnhr3r9")))

(define-public crate-mocktopus-0.2.0 (c (n "mocktopus") (v "0.2.0") (d (list (d (n "mocktopus_macros") (r "= 0.2.0") (d #t) (k 0)))) (h "02l8wkws67m5k3a6sxv7gampwyya5agp165fj79q0cv00qx6cq02")))

(define-public crate-mocktopus-0.2.1 (c (n "mocktopus") (v "0.2.1") (d (list (d (n "mocktopus_macros") (r "= 0.2.0") (d #t) (k 0)))) (h "0qlc4a9684x3y2hwwc1v8rglds0ja88v4xnxa1s8whxlb1l7bcw5")))

(define-public crate-mocktopus-0.3.0 (c (n "mocktopus") (v "0.3.0") (d (list (d (n "mocktopus_macros") (r "= 0.3.0") (d #t) (k 0)))) (h "0wimkzpp7w685jidh3rxrjs42019ch729xxpbkayg1g2gkd00w2a")))

(define-public crate-mocktopus-0.3.1 (c (n "mocktopus") (v "0.3.1") (d (list (d (n "mocktopus_macros") (r "= 0.3.1") (d #t) (k 0)))) (h "1l3izbmn9bwfky4xq21ghym3fbqy8wspqim1b3m3fvah40i40p4l")))

(define-public crate-mocktopus-0.3.2 (c (n "mocktopus") (v "0.3.2") (d (list (d (n "mocktopus_macros") (r "= 0.3.2") (d #t) (k 0)))) (h "00xjsbr365sgyp0q8jkpal1m2mwn5zs8cgy1hzvxfjddw0lnhv3m")))

(define-public crate-mocktopus-0.3.3 (c (n "mocktopus") (v "0.3.3") (d (list (d (n "mocktopus_macros") (r "= 0.3.3") (d #t) (k 0)))) (h "1scx25gkn53r0yhhxm6djy0hip2jg3ypqfdr20mw4hbqagan42zn")))

(define-public crate-mocktopus-0.3.4 (c (n "mocktopus") (v "0.3.4") (d (list (d (n "mocktopus_macros") (r "= 0.3.4") (d #t) (k 0)))) (h "07n866im5gjjqd8j3l0vks0ak069gnpxkmmfgpn15sdfqjv9jhcq")))

(define-public crate-mocktopus-0.3.5 (c (n "mocktopus") (v "0.3.5") (d (list (d (n "mocktopus_macros") (r "= 0.3.5") (d #t) (k 0)))) (h "0bgd4id3nlzljrgj1xpwfbl7wn8zynnhy2615631dp8ilabr9pyk")))

(define-public crate-mocktopus-0.3.6 (c (n "mocktopus") (v "0.3.6") (d (list (d (n "mocktopus_macros") (r "= 0.3.6") (d #t) (k 0)))) (h "0fg1clbcq3sq22wmsvfwzym7rym57k2aw4zip7z7l5axzwm8hvdy")))

(define-public crate-mocktopus-0.3.7 (c (n "mocktopus") (v "0.3.7") (d (list (d (n "mocktopus_macros") (r "= 0.3.7") (d #t) (k 0)))) (h "0pw749b8skf5fw163s2bngsz72d9vqlfakcqfim9nn4fc40nksyv")))

(define-public crate-mocktopus-0.3.8 (c (n "mocktopus") (v "0.3.8") (d (list (d (n "mocktopus_macros") (r "= 0.3.8") (d #t) (k 0)))) (h "160hmh0130r41xjp17wrfhr9qdm7bzgfs1ng7v3hkls81zryl6c9")))

(define-public crate-mocktopus-0.4.0 (c (n "mocktopus") (v "0.4.0") (d (list (d (n "mocktopus_macros") (r "= 0.4.0") (d #t) (k 0)))) (h "0lsjp8x1vddjpqw80gyipcs0jbpsiyxgljvzmizfg533j2d10lsi")))

(define-public crate-mocktopus-0.4.1 (c (n "mocktopus") (v "0.4.1") (d (list (d (n "mocktopus_macros") (r "= 0.4.0") (d #t) (k 0)))) (h "1b6v77b3h0ck9qghal7k91xsyn1ck5w22nkvlqjmy65vl64inxwb")))

(define-public crate-mocktopus-0.5.0 (c (n "mocktopus") (v "0.5.0") (d (list (d (n "mocktopus_macros") (r "= 0.5.0") (d #t) (k 0)))) (h "13fd68aj0wjg5dd7w3kgp92d56xzm7q80w1514ar2fv13mhxwfrn")))

(define-public crate-mocktopus-0.6.0 (c (n "mocktopus") (v "0.6.0") (d (list (d (n "mocktopus_macros") (r "= 0.6.0") (d #t) (k 0)))) (h "0kacai41ibgbfflygyln0nq0k2xx564n3kiz3zzs0sjhic51qk7l")))

(define-public crate-mocktopus-0.7.0 (c (n "mocktopus") (v "0.7.0") (d (list (d (n "mocktopus_macros") (r "= 0.7.0") (d #t) (k 0)))) (h "1j2fgw8ypwwg1ffmdfz1x1pbsqkxfzq5xggkn3wgm00k2g12diks")))

(define-public crate-mocktopus-0.7.1 (c (n "mocktopus") (v "0.7.1") (d (list (d (n "mocktopus_macros") (r "= 0.7.0") (d #t) (k 0)))) (h "1v019llk431cnh71cj3y79n07lgaj06i0wn3av6p11yhi49lnpsk")))

(define-public crate-mocktopus-0.7.2 (c (n "mocktopus") (v "0.7.2") (d (list (d (n "mocktopus_macros") (r "= 0.7.0") (d #t) (k 0)))) (h "11pbs35fypxnawd09130w6jgnqd5wkpjdr4509s81wcrz2x5l8nf")))

(define-public crate-mocktopus-0.7.3 (c (n "mocktopus") (v "0.7.3") (d (list (d (n "mocktopus_macros") (r "= 0.7.3") (d #t) (k 0)))) (h "1mca88rflrqf4pxbnp7h4hpnxk7b5lxs4zxx2v09a74cbwkw5pf3")))

(define-public crate-mocktopus-0.7.4 (c (n "mocktopus") (v "0.7.4") (d (list (d (n "mocktopus_macros") (r "= 0.7.4") (d #t) (k 0)))) (h "1fark9vpkgyyxi3wkhyxjzdzmv5ny70v4l1xxmfhj0s744gpbw2p") (y #t)))

(define-public crate-mocktopus-0.7.5 (c (n "mocktopus") (v "0.7.5") (d (list (d (n "mocktopus_macros") (r "= 0.7.5") (d #t) (k 0)))) (h "07v818aw6gh6mka5g97ww12bvhi3fi42719j8rjl5l9ib7ch5f3b")))

(define-public crate-mocktopus-0.7.6 (c (n "mocktopus") (v "0.7.6") (d (list (d (n "mocktopus_macros") (r "=0.7.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0d9szg7jjhhjsibh651x7bxj3sgvhmxm77z2lf5sgfazv41dyjj0")))

(define-public crate-mocktopus-0.7.7 (c (n "mocktopus") (v "0.7.7") (d (list (d (n "mocktopus_macros") (r "^0.7.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0n7kwmg72d1isp0h1racq9bzvs43gr99xihbi5l7ap0iaal5gjpj")))

(define-public crate-mocktopus-0.7.8 (c (n "mocktopus") (v "0.7.8") (d (list (d (n "mocktopus_macros") (r "^0.7.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1y8ng8yzmbh1q5wsl261ml62kgz7yjyrr71kdhkgrwdivd2bx3fc")))

(define-public crate-mocktopus-0.7.9 (c (n "mocktopus") (v "0.7.9") (d (list (d (n "mocktopus_macros") (r "^0.7.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0c0vfnipih0w5v9xn5cig9l6d1sf277n7nc660giclxdqsdwr0ky")))

(define-public crate-mocktopus-0.7.10 (c (n "mocktopus") (v "0.7.10") (d (list (d (n "mocktopus_macros") (r "^0.7.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1asc5n91p5ls8fj8df088k7j8866vh4rd3kykwnfrymj4inb5kda")))

(define-public crate-mocktopus-0.7.11 (c (n "mocktopus") (v "0.7.11") (d (list (d (n "mocktopus_macros") (r "^0.7.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1hypvxkm3wsmsp8siasv9vhsx5n8z2bg6vlwrdja0qfdprdlmrfi")))

(define-public crate-mocktopus-0.8.0 (c (n "mocktopus") (v "0.8.0") (d (list (d (n "mocktopus_macros") (r "^0.7.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0dwm0mii13yl9a7pd46pjbl2xhxrqhrpbxhwai9a5zi12rd0skqy")))

