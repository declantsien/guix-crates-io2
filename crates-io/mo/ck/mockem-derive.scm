(define-module (crates-io mo ck mockem-derive) #:use-module (crates-io))

(define-public crate-mockem-derive-0.1.0 (c (n "mockem-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0s8063lba1l7hkj4sd122p7y69bwzfr3q3s1906lqcw0yx9rqd71") (r "1.56")))

(define-public crate-mockem-derive-0.2.0 (c (n "mockem-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0fdgxszsvb2bjzfzh5n2l2yjp015s9ysnywsy03kqr00wqf1hkdd") (r "1.56")))

(define-public crate-mockem-derive-0.2.1 (c (n "mockem-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrcqs8n4icd9xa46xz38x8zfx4dlbbsj3fb0q4z6h8nqw2bx6rc") (r "1.56")))

