(define-module (crates-io mo ck mockingbird) #:use-module (crates-io))

(define-public crate-mockingbird-0.0.1 (c (n "mockingbird") (v "0.0.1") (d (list (d (n "harper") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p13jpymfqgm5vkbyspbxiv41cmjwfbg9q9mdyd6f6n7x8y9z5bi")))

(define-public crate-mockingbird-0.0.2 (c (n "mockingbird") (v "0.0.2") (d (list (d (n "harper") (r "^0.0.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3.2") (d #t) (k 0)))) (h "1w61zmsl2cjang1qby4qxa9nb09z0vq8q9awyxlkd0zbha3fmlcg")))

