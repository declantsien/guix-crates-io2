(define-module (crates-io mo ck mockers_macros) #:use-module (crates-io))

(define-public crate-mockers_macros-0.1.0 (c (n "mockers_macros") (v "0.1.0") (h "18r831r98rgbwm7lpi8z72hanwgcqvzz39zwd0bpmx0m594dq0pw")))

(define-public crate-mockers_macros-0.2.0 (c (n "mockers_macros") (v "0.2.0") (h "0wnrc5v6jxzz4gpkybydjn5vvdl6lhjanq6y8yk1rjkj1zxvzwmq")))

(define-public crate-mockers_macros-0.2.1 (c (n "mockers_macros") (v "0.2.1") (h "0gz50397ihmjh8q0bmik6z036vcvblah96r18x0vkmxlfin4f0hv")))

(define-public crate-mockers_macros-0.2.2 (c (n "mockers_macros") (v "0.2.2") (h "0118cvw5a5aqigqbn8i4ahjk1w7k433cyqvrs7ww9l8a1b5r2mzd")))

(define-public crate-mockers_macros-0.3.0 (c (n "mockers_macros") (v "0.3.0") (h "1gid2kdc9smdcndvy7rrxzihmg1h4ddzc5m1rwrxsfxcvals0hj2")))

(define-public crate-mockers_macros-0.4.0 (c (n "mockers_macros") (v "0.4.0") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "0srghrwi7cxij69j0x4jqw8jxgkl341vlzsss5clak3png3slgpw")))

(define-public crate-mockers_macros-0.4.1 (c (n "mockers_macros") (v "0.4.1") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "1n5ik7k6r5jpnh5zb283gwi8305ghv2b6chmb365ibgp2hcas216")))

(define-public crate-mockers_macros-0.4.2 (c (n "mockers_macros") (v "0.4.2") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "1l5093rfj2q801x5hm2r0pahg54pjsmd3n7zz0mvkfcqjp57cl3q")))

(define-public crate-mockers_macros-0.4.3 (c (n "mockers_macros") (v "0.4.3") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "1m4qsmbdpi6vhkw20kyg4xmsf1y4bsq73fr0l5mvi6qchjvk6p30")))

(define-public crate-mockers_macros-0.4.4 (c (n "mockers_macros") (v "0.4.4") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "0a31dgz633f49k0kiv6pgw31qsymrmlw3a28qli85lii2h7nvl0s")))

(define-public crate-mockers_macros-0.4.5 (c (n "mockers_macros") (v "0.4.5") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "0p95nk9kxzn1ybxf0w5lgk9sr2pll0vnj6pxipgvlaidafi4a2dw")))

(define-public crate-mockers_macros-0.4.6 (c (n "mockers_macros") (v "0.4.6") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "1ay0z8f5sikj24i7lk4w47mmna3kfiarl0ijk5l00w53y5r4h4yp")))

(define-public crate-mockers_macros-0.4.7 (c (n "mockers_macros") (v "0.4.7") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "13is29mdqypr204wjzwh1rky7ap8na4q1hnbbwkp7qdqdpshdnq4")))

(define-public crate-mockers_macros-0.4.8 (c (n "mockers_macros") (v "0.4.8") (d (list (d (n "itertools") (r "^0.4.13") (d #t) (k 0)))) (h "192hikdff1457bdf8mpn088vi7nkjza2qkmqxpvldh18dxdnpqz6")))

(define-public crate-mockers_macros-0.5.1 (c (n "mockers_macros") (v "0.5.1") (d (list (d (n "mockers_codegen") (r "^0.5.1") (d #t) (k 0)))) (h "1q5bsqs4zva4cbmassqqldky7wwhiqhm2bjkhgh2fikhxk2brfxp") (f (quote (("debug"))))))

(define-public crate-mockers_macros-0.5.2 (c (n "mockers_macros") (v "0.5.2") (d (list (d (n "mockers_codegen") (r "^0.5.2") (d #t) (k 0)))) (h "1ci127jcp6q4zzha9mnzzbxm74fyszyhg9zx3by3qww3mya4igvr") (f (quote (("debug"))))))

(define-public crate-mockers_macros-0.5.3 (c (n "mockers_macros") (v "0.5.3") (d (list (d (n "mockers_codegen") (r "^0.5.3") (d #t) (k 0)))) (h "1wdb5qch3rdw0y10qdqq5c2lsv4pc8q52d700hppd1jswblfqaa0") (f (quote (("debug"))))))

(define-public crate-mockers_macros-0.5.4 (c (n "mockers_macros") (v "0.5.4") (d (list (d (n "mockers_codegen") (r "^0.5.4") (d #t) (k 0)))) (h "1bimnjy6s0hd0mc7arj6ih8liljc4ff2bmcdfk5sw7v43ahmnfzx") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.5 (c (n "mockers_macros") (v "0.5.5") (d (list (d (n "mockers_codegen") (r "^0.5.5") (d #t) (k 0)))) (h "03rfhlc4m82l2ml0yw4g7ybcx5nq4nshi5nq4ppgq9ns6dksz6f1") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.6 (c (n "mockers_macros") (v "0.5.6") (d (list (d (n "mockers_codegen") (r "^0.5.6") (d #t) (k 0)))) (h "18c51jwrzc3hk5vwmzfwhrphb1wx47zdfm4rgci9jiaf35w3j9rh") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.7 (c (n "mockers_macros") (v "0.5.7") (d (list (d (n "mockers_codegen") (r "^0.5.7") (d #t) (k 0)))) (h "0siyrqqvhgxjvg6gv4fahf7izbrziaxjn3yafr9yc4bbnznpgh0x") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.8 (c (n "mockers_macros") (v "0.5.8") (d (list (d (n "mockers_codegen") (r "^0.5.8") (d #t) (k 0)))) (h "0w4hvgb8v0fzacmfddwl057b8rjg44idfd387z90yiz86qcxfpgl") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.9 (c (n "mockers_macros") (v "0.5.9") (d (list (d (n "mockers_codegen") (r "^0.5.9") (d #t) (k 0)))) (h "1yrjy72vh44605ikbcq82g5wzyzx6fb3k3sghhmlmv583nlbina9") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.10 (c (n "mockers_macros") (v "0.5.10") (d (list (d (n "mockers_codegen") (r "^0.5.10") (d #t) (k 0)))) (h "0f3j3ispbd08fwwrpgdx6zgxh1f9i30x04mgxm0x6qan878hr8is") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.5.11 (c (n "mockers_macros") (v "0.5.11") (d (list (d (n "mockers_codegen") (r "^0.5.11") (d #t) (k 0)))) (h "019ipgdl8glv992wzdl5k0yxj62i43njqxncdfdr871w4sjgrfwb") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.6.0 (c (n "mockers_macros") (v "0.6.0") (d (list (d (n "mockers_codegen") (r "^0.6.0") (d #t) (k 0)))) (h "01qpnvbs9ir140j1jfac1vpmd7j2n6wkx9j3b5px4xq147sw45q1") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.6.1 (c (n "mockers_macros") (v "0.6.1") (d (list (d (n "mockers_codegen") (r "^0.6.1") (d #t) (k 0)))) (h "0psy8jqfngj7pihnki553vizb7jv0yd304qpg4haazq1yn4p03fd") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.6.2 (c (n "mockers_macros") (v "0.6.2") (d (list (d (n "mockers_codegen") (r "^0.6.2") (d #t) (k 0)))) (h "16wm72dlv4dn4jw4a6ncpzdwf9xwfhkbcv05j24aw73cqbky424g") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.6.3 (c (n "mockers_macros") (v "0.6.3") (d (list (d (n "mockers_codegen") (r "^0.6.3") (d #t) (k 0)))) (h "1xrj2p1y4y2rcbv8r3lw8jsim3cyvglf77vgzlgd1bb6w0fkpfyl") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.7.0 (c (n "mockers_macros") (v "0.7.0") (d (list (d (n "mockers_codegen") (r "^0.7.0") (d #t) (k 0)))) (h "18inz54r4xzrb2bbcc1gpm7gkz98p1cpwqr74d26pssyad3mjnzy") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.7.1 (c (n "mockers_macros") (v "0.7.1") (d (list (d (n "mockers_codegen") (r "^0.7.1") (d #t) (k 0)))) (h "07gmhzb01sa2ayf4vzaxh2qfz7wn9jjb5lqpamf68g0x11p4hhz0") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.8.0 (c (n "mockers_macros") (v "0.8.0") (d (list (d (n "mockers_codegen") (r "^0.8.0") (d #t) (k 0)))) (h "1fp3bkz3dhkxifj9bi55j7r19cvh8lh5y2xd1cizihcr4g927c38") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.9.0 (c (n "mockers_macros") (v "0.9.0") (d (list (d (n "mockers_codegen") (r "^0.9.0") (d #t) (k 0)))) (h "0rlkrsllly381a077lx65jgxfl5xfwylhbjmfyyfkg5hn2rf903c") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.9.1 (c (n "mockers_macros") (v "0.9.1") (d (list (d (n "mockers_codegen") (r "^0.9.1") (d #t) (k 0)))) (h "1j8prvdxnbflvck7qgg10334w4lxfw78k3h7brwh9366d7x1w8lj") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.9.2 (c (n "mockers_macros") (v "0.9.2") (d (list (d (n "mockers_codegen") (r "^0.9.2") (d #t) (k 0)))) (h "1p3ay5ikq9vip5w41v3fq2mk0iyaswzihanm24dx8icn4iv845ih") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.9.3 (c (n "mockers_macros") (v "0.9.3") (d (list (d (n "mockers_codegen") (r "^0.9.3") (d #t) (k 0)))) (h "1nbdijg3i1z34dqdsr1pvv0432hb5hxi7idn7dlvjg9057zmbr3p") (f (quote (("debug" "mockers_codegen/debug"))))))

(define-public crate-mockers_macros-0.9.4 (c (n "mockers_macros") (v "0.9.4") (d (list (d (n "mockers_codegen") (r "^0.9.4") (d #t) (k 0)))) (h "0ridc2ay4swaiqs0jh2lal3hrc4hnfgz15gfx80qh1wsf1lzmzlj") (f (quote (("debug" "mockers_codegen/debug"))))))

