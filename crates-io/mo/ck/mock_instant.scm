(define-module (crates-io mo ck mock_instant) #:use-module (crates-io))

(define-public crate-mock_instant-0.1.0 (c (n "mock_instant") (v "0.1.0") (h "10m4v0rawl5mylagl67xd7r3jzvws4xgj9n8hz3p8hqng4ayk43i")))

(define-public crate-mock_instant-0.1.1 (c (n "mock_instant") (v "0.1.1") (h "0198rig2lvzlf6m37a8qghclmns0yd31csqpyyrjhfbzqfpvvbv2")))

(define-public crate-mock_instant-0.2.0 (c (n "mock_instant") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1ch8yl98nz2p0ridqq36k83zrv9xrwxqb253djqpf0741anmcspv") (f (quote (("sync" "once_cell") ("default"))))))

(define-public crate-mock_instant-0.2.1 (c (n "mock_instant") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0vg0kmz96zazjdq57l57nm24mc2in57y090ywcq827xq8fi2jzki") (f (quote (("sync" "once_cell") ("default"))))))

(define-public crate-mock_instant-0.3.0 (c (n "mock_instant") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17") (o #t) (d #t) (k 0)))) (h "0vvbk7s5g6y7dn8rksb3s6zq2y2w0cwb0fasxfgv8yfvx864wwsw") (f (quote (("sync" "once_cell") ("default")))) (y #t)))

(define-public crate-mock_instant-0.3.1 (c (n "mock_instant") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17") (o #t) (d #t) (k 0)))) (h "1v140pa6dvvil1m1ijsbvjsnw0pnq4f765hmifw0ch3chkg586kc") (f (quote (("sync" "once_cell") ("default"))))))

(define-public crate-mock_instant-0.3.2 (c (n "mock_instant") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.17") (o #t) (d #t) (k 0)))) (h "180yr3i44a98w1mj36dd8xmym33rbzndpj0j1g13di52n8g8crlk") (f (quote (("sync" "once_cell") ("default"))))))

(define-public crate-mock_instant-0.4.0 (c (n "mock_instant") (v "0.4.0") (h "1v0ml51yzf9sivxl69ads0kgqb3w61sl03klj13m4r953526cdbc")))

