(define-module (crates-io mo ck mock-store) #:use-module (crates-io))

(define-public crate-mock-store-0.1.0 (c (n "mock-store") (v "0.1.0") (d (list (d (n "modql") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0sn653raxvrsjsbbl6pgd6p5890hxkcc1cxclwx3r3ravm2sni2v")))

(define-public crate-mock-store-0.2.0 (c (n "mock-store") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "modql") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10kq9hpwkac7jjlmi7vshrw0mdp8qfs6h8qb04bml6lh3abnlvci")))

