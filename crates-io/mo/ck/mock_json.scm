(define-module (crates-io mo ck mock_json) #:use-module (crates-io))

(define-public crate-mock_json-0.1.0 (c (n "mock_json") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "073zfl8gpwfl41b9kfn0bli21f2nxbia9gx18iqfcjgc0yv6w412")))

(define-public crate-mock_json-0.1.1 (c (n "mock_json") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b1fpc93lzni7jhixcm65xc3zzdxvfwiwb4ksi2nv7dr9122nmk5")))

(define-public crate-mock_json-0.1.2 (c (n "mock_json") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1z2pk1zqcfzh267n3hfdlyxi11y4jcr88py5qi7z0q1pkz327fxz")))

(define-public crate-mock_json-0.1.3 (c (n "mock_json") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1m9zax1bpjx9b6xgjbv4363rxc2f0h3yh63p12427cv07qvas9v4")))

(define-public crate-mock_json-0.1.4 (c (n "mock_json") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hfiyx788pnid0jrdgfmvshvcralrgn3h3yq9s1lld38zws3iirw")))

(define-public crate-mock_json-0.1.5 (c (n "mock_json") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1k1agpwbdrm5d04klqh8jb9p9kycajgzrp0gk5a5g1wg3n29bmgm")))

(define-public crate-mock_json-0.1.6 (c (n "mock_json") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hd6g8j7340bgzk382sf66pqada8iwhanm5mpz4h7q08fhq09hr5")))

(define-public crate-mock_json-0.1.7 (c (n "mock_json") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "057f28w4z9pd1aiiml32scngqlfykvg6ama5hyz49pd2wg5dxb0i")))

(define-public crate-mock_json-0.1.8 (c (n "mock_json") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jlz6694w15wb6ms9wxn0m1gxfmhfz5ayj3d3yr0c9l0lggwxnfj")))

