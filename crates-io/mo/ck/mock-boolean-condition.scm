(define-module (crates-io mo ck mock-boolean-condition) #:use-module (crates-io))

(define-public crate-mock-boolean-condition-0.8.8 (c (n "mock-boolean-condition") (v "0.8.8") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "1yiy0a6p1sjbj9i7sfqxay3abn1z8lap54m8m9l3v5rjsmddp828") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

(define-public crate-mock-boolean-condition-0.8.9 (c (n "mock-boolean-condition") (v "0.8.9") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "084lj9g21j7fbaq2g2s7agfr3p76rf4ks61rfxhcqy1bpm34b377") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

