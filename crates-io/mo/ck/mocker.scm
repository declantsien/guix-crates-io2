(define-module (crates-io mo ck mocker) #:use-module (crates-io))

(define-public crate-mocker-0.1.0 (c (n "mocker") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0fc82d40x5g9h7rg3gd2gg7b2g4r5rq4h0fz2bi6w5sgkzzb2npf")))

(define-public crate-mocker-0.1.1 (c (n "mocker") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0xmwf1h1p4k4nhmlmwiwf9v1zl9yq9pwbppadqb96rba4kmykzx0")))

(define-public crate-mocker-0.1.2 (c (n "mocker") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0babpxbhd8b8yqjqdx8n1yq8bqdjjjck2v2car59h5xy0l2swa7a")))

(define-public crate-mocker-0.1.3 (c (n "mocker") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1pvrq2hgblw1r6a8fpvbsirgrirrdf93i20r5r7qcpibbj5ns5hn")))

(define-public crate-mocker-0.1.4 (c (n "mocker") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0blvri5mpjc88dmn88h56x3rgzx9pl5d9mfmigmb8xjz4xp9h31p")))

(define-public crate-mocker-0.1.5 (c (n "mocker") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0za51aykrq9gdj2ppvcr7sz55wkc6f4w4g2w9fya0hvc3j61fka4")))

