(define-module (crates-io mo ck mock-ellipticoin) #:use-module (crates-io))

(define-public crate-mock-ellipticoin-0.1.1 (c (n "mock-ellipticoin") (v "0.1.1") (d (list (d (n "ellipticoin") (r "^0.1.4") (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.12") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.12") (d #t) (k 0)))) (h "155whyif2k3dhwp6lrqhsj02xmnyj619zi5k68h3khljiay1s88a")))

