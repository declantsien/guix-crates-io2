(define-module (crates-io mo ck mock-syn) #:use-module (crates-io))

(define-public crate-mock-syn-0.1.0 (c (n "mock-syn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkah2i6dbgfspnv5i8wqmcl163nf4jw0n3d753603fjpfygk7bb")))

(define-public crate-mock-syn-0.1.1 (c (n "mock-syn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q7qld0rq85206dy7kp2p0r3rk0i24yza9z2b84q42wjdnhx46li")))

(define-public crate-mock-syn-0.1.2 (c (n "mock-syn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0rnfk0sg55q7i9npk1ralkbv47337myjqk6ciwz2y4fmjdffw3")))

(define-public crate-mock-syn-0.1.3 (c (n "mock-syn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04n1cdb2n9iz63cf2cnyp3dypf4apnk61298p38cp03m91qnkvvz")))

(define-public crate-mock-syn-0.1.4 (c (n "mock-syn") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0acrq85n6i8fgggdmxqxysbwxv5jhxwqvjq51qgncpggb9142xcc")))

(define-public crate-mock-syn-0.1.5 (c (n "mock-syn") (v "0.1.5") (d (list (d (n "mock-syn-common") (r "^0.1.5") (d #t) (k 0)) (d (n "mock-syn-common") (r "^0.1.5") (f (quote ("test"))) (d #t) (k 2)) (d (n "mock-syn-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 2)))) (h "1dw8pg59vnq2b9j22f5afdja4fslwfb7qhfkwzfxwymn6j5cbxw8")))

(define-public crate-mock-syn-0.1.6 (c (n "mock-syn") (v "0.1.6") (d (list (d (n "mock-syn-common") (r "^0.1.6") (d #t) (k 0)) (d (n "mock-syn-common") (r "^0.1.6") (f (quote ("test"))) (d #t) (k 2)) (d (n "mock-syn-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 2)))) (h "0l5zdr2y66qjr72skj9a098yhdc2ms3dpkd2jxfslpmli2k3m1s5")))

