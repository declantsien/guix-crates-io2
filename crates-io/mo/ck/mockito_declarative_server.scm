(define-module (crates-io mo ck mockito_declarative_server) #:use-module (crates-io))

(define-public crate-mockito_declarative_server-0.1.0 (c (n "mockito_declarative_server") (v "0.1.0") (d (list (d (n "mockito") (r "^0.25.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0lzg9p00xq5w428j29azwd3iadfzzx0imi1xn4svqmzsnnbwyl25")))

(define-public crate-mockito_declarative_server-0.2.0 (c (n "mockito_declarative_server") (v "0.2.0") (d (list (d (n "mockito") (r "^0.25.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "029x2sh2v9zjilz0xwidaj5rwn0ilglsjdqir0rn8yk93r02vyb1")))

(define-public crate-mockito_declarative_server-0.3.0 (c (n "mockito_declarative_server") (v "0.3.0") (d (list (d (n "mockito") (r "^0.25.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1q8v4hknqlh5lac8phb66bmbjhpwd1cjfr6d2jvi11xhdk16c0k6")))

