(define-module (crates-io mo ck mocktave) #:use-module (crates-io))

(define-public crate-mocktave-0.1.0 (c (n "mocktave") (v "0.1.0") (d (list (d (n "bollard") (r "^0.14.0") (f (quote ("buildkit"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "15jqj16b5374jrpjnf0y95qwf8bzkmn6npgwswmgzh2fqv178g6h")))

(define-public crate-mocktave-0.1.1 (c (n "mocktave") (v "0.1.1") (d (list (d (n "bollard") (r "^0.14.0") (f (quote ("buildkit"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1rhiyk7whhl2a8gly2xsrzbnzf7pwhdhhwj6x4i7yz1c2jl4cxpm")))

(define-public crate-mocktave-0.1.2 (c (n "mocktave") (v "0.1.2") (d (list (d (n "bollard") (r "^0.14.0") (f (quote ("buildkit"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (d #t) (k 0)))) (h "1kx29h8m6cmqk70hnhl7zx7g4m2wf1mnsdnc8y9gdyin3jdb5yqi")))

(define-public crate-mocktave-0.1.3 (c (n "mocktave") (v "0.1.3") (d (list (d (n "bollard") (r ">=0.14.0") (f (quote ("buildkit"))) (o #t) (d #t) (k 0)) (d (n "futures-util") (r ">=0.3.0") (o #t) (d #t) (k 0)) (d (n "human_regex") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "regex") (r ">=1.7.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.7.0") (o #t) (d #t) (k 0)))) (h "1xbgi57p5xm8k39mg8vwpsd3q70m4pahlwd00b75la0y4ibn9rv8") (f (quote (("docker" "bollard" "futures-util" "tokio") ("default" "docker"))))))

(define-public crate-mocktave-0.1.4 (c (n "mocktave") (v "0.1.4") (d (list (d (n "bollard") (r ">=0.14.0") (f (quote ("buildkit"))) (o #t) (d #t) (k 0)) (d (n "decompress") (r ">=0.6.0") (o #t) (d #t) (k 1)) (d (n "futures-util") (r ">=0.3.0") (o #t) (d #t) (k 0)) (d (n "human_regex") (r "^0.3.0") (d #t) (k 0)) (d (n "minreq") (r ">=2.6.0") (f (quote ("json-using-serde" "https"))) (o #t) (d #t) (k 1)) (d (n "regex") (r ">=1.7.0") (d #t) (k 0)) (d (n "serde_yaml") (r ">=0.9.0") (o #t) (d #t) (k 1)) (d (n "tokio") (r ">=1.7.0") (o #t) (d #t) (k 0)))) (h "1p9ndn42vvybqahnjvy60gbbdrhvby9isvnw8ah2v6sxfwmlhjza") (f (quote (("docker" "bollard" "futures-util" "tokio") ("default" "docker"))))))

