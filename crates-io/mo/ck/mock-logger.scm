(define-module (crates-io mo ck mock-logger) #:use-module (crates-io))

(define-public crate-mock-logger-0.1.0 (c (n "mock-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1f5j9vyhqsmjvr462pxzrm2yn5rsn8ahf2bnjwjdisdariprwc55")))

(define-public crate-mock-logger-0.1.1 (c (n "mock-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1vv0wp1y54lw96qkh0zaf2dp650a808crq3jdaxcjmzd3r58qybf")))

(define-public crate-mock-logger-0.1.2 (c (n "mock-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1v9gzampvxnrqvz65rljzr305hhwilnspwi9ypdxcgwcc7sp97gx")))

(define-public crate-mock-logger-0.1.3 (c (n "mock-logger") (v "0.1.3") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0f86xcfaf4h72h2bjf30lgbbywywqnxig3vp0qq8kafh4kapxlhm")))

