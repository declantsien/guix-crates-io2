(define-module (crates-io mo ck mockstream) #:use-module (crates-io))

(define-public crate-mockstream-0.0.1 (c (n "mockstream") (v "0.0.1") (h "0lh5gjqsh646qgf70q3z7g6nvh5g9m20r2mngkcrs84iyg6v7lz8")))

(define-public crate-mockstream-0.0.2 (c (n "mockstream") (v "0.0.2") (h "14hmfpi62p3as713r2jnm1jyq71b4gpsaljr6vyzg2llr341ishi")))

(define-public crate-mockstream-0.0.3 (c (n "mockstream") (v "0.0.3") (h "0bmsjqj2jxssgvkv2lp08yajh8ql3lvbqd1pn5iv8m6jr70f1frm")))

