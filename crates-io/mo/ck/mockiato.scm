(define-module (crates-io mo ck mockiato) #:use-module (crates-io))

(define-public crate-mockiato-0.1.0 (c (n "mockiato") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.14") (d #t) (k 2)) (d (n "mockiato-codegen") (r "^0.1") (d #t) (k 0)))) (h "0kmlm0cljw79cjzxwqd9nivvrh24zgvl3qs91l8rgzgbqr1ymall")))

(define-public crate-mockiato-0.2.0 (c (n "mockiato") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3.14") (d #t) (k 2)) (d (n "mockiato-codegen") (r "^0.2.0") (d #t) (k 0)))) (h "06mg8s20536npja0cl009vrzrxqhdnn5yg9qxiv3nbl6l4jhhgnq")))

(define-public crate-mockiato-0.2.1 (c (n "mockiato") (v "0.2.1") (d (list (d (n "mockiato-codegen") (r "^0.2.1") (d #t) (k 0)))) (h "05331vq6akz2xkwp3j2lx7yra7jwwfdg0k44ydc97zpfcrcz34bj")))

(define-public crate-mockiato-0.3.0 (c (n "mockiato") (v "0.3.0") (d (list (d (n "mockiato-codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "1lrf1k4bxg1qfb3vxzwzjz2a9anw3nflhggzva2ackj8w25vfwb4")))

(define-public crate-mockiato-0.4.0 (c (n "mockiato") (v "0.4.0") (d (list (d (n "mockiato-codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0w6ilsh11n7syhwmyi9xc8saakv4idr90hjpklaw3sa8kxa25317")))

(define-public crate-mockiato-0.4.1 (c (n "mockiato") (v "0.4.1") (d (list (d (n "mockiato-codegen") (r "^0.4.1") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "136mrp9h1nv011zgmf9wb57nagd9bc0z3qqgml84yv2aiwjwp7m5") (y #t)))

(define-public crate-mockiato-0.4.2 (c (n "mockiato") (v "0.4.2") (d (list (d (n "mockiato-codegen") (r "^0.4.2") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0pgyaf12j3cfdbqc6vvwy3hvyy18pmpc8w0d1brxg4h83a52xrvw")))

(define-public crate-mockiato-0.5.0 (c (n "mockiato") (v "0.5.0") (d (list (d (n "mockiato-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0fb13h81lasb9cxzj4mzzmyhjw1rjand7x4wmvc5iasc4lixbxbs")))

(define-public crate-mockiato-0.5.1 (c (n "mockiato") (v "0.5.1") (d (list (d (n "mockiato-codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0qxpa2n648y2ag304bc5qfak55jvv03qg3vzb5mnh68vp86nxmvw")))

(define-public crate-mockiato-0.6.0 (c (n "mockiato") (v "0.6.0") (d (list (d (n "mockiato-codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "01qbnkb4f2lkn32i8bxnxhncf8lrlsqji0w3fnjr3vqi7lc0f4jy")))

(define-public crate-mockiato-0.6.1 (c (n "mockiato") (v "0.6.1") (d (list (d (n "mockiato-codegen") (r "^0.6.1") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0z87qk9cwd6m0c8gkw6xr3hrkkqgj2r7i3ly3cpwdn4dwgx1km51")))

(define-public crate-mockiato-0.6.2 (c (n "mockiato") (v "0.6.2") (d (list (d (n "mockiato-codegen") (r "^0.6.2") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "0r0xkhr9b339qg41ay4g98bxc3hn0d7cdnbbhblm6fs7ws4hrsac")))

(define-public crate-mockiato-0.6.3 (c (n "mockiato") (v "0.6.3") (d (list (d (n "mockiato-codegen") (r "^0.6.3") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2") (d #t) (k 0)))) (h "01n85s1qrnrsifkhn4msggyl36zfnv2qrzbw4rh34kj4q3lmvgmv")))

(define-public crate-mockiato-0.7.0 (c (n "mockiato") (v "0.7.0") (d (list (d (n "mockiato-codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1ppgk03qm4m1j4phrf65a6qwvf4ag5yhfzc47ksdn4q8a4nxc3kf")))

(define-public crate-mockiato-0.8.0 (c (n "mockiato") (v "0.8.0") (d (list (d (n "mockiato-codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "nameof") (r "^1.0.2") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0s8y2xbh6j259y1z0p7r4ikzpv9f845kkfwc0m0gla3g8r4d997i")))

(define-public crate-mockiato-0.9.0 (c (n "mockiato") (v "0.9.0") (d (list (d (n "mockiato-codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0wgrrav87hicqq0j7hakrr7ggmqvn1i624jxp2grpnywzalp76x2")))

(define-public crate-mockiato-0.9.1 (c (n "mockiato") (v "0.9.1") (d (list (d (n "mockiato-codegen") (r "^0.9.1") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1cvnp48b5csp492h6k7wqg75dmbd9zbcc6wifgwibrbwbz16iy7i")))

(define-public crate-mockiato-0.9.2 (c (n "mockiato") (v "0.9.2") (d (list (d (n "mockiato-codegen") (r "^0.9.2") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "03i2x1gc8h9ximzszkyvnink5pk2wl3pd1dwmq5m67iccjspvkiw") (y #t)))

(define-public crate-mockiato-0.9.3 (c (n "mockiato") (v "0.9.3") (d (list (d (n "mockiato-codegen") (r "^0.9.3") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "14sfs4z4jwwk5c7maa9gxvjjywqj6scxmbcka3xgxkd52l8d90wz") (y #t)))

(define-public crate-mockiato-0.9.4 (c (n "mockiato") (v "0.9.4") (d (list (d (n "mockiato-codegen") (r "^0.9.4") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1s9s9h3sndf5ql73bk1d0xg236zc1vhlgp21xf4l361y6c14j21g")))

(define-public crate-mockiato-0.9.5 (c (n "mockiato") (v "0.9.5") (d (list (d (n "mockiato-codegen") (r "^0.9.5") (d #t) (k 0)) (d (n "nameof") (r "^1.1.0") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0g7qpk6mldrnik88zwwkz1yshx16csfwdk2sj6dph95zq7q89arj")))

