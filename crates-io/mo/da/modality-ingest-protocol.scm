(define-module (crates-io mo da modality-ingest-protocol) #:use-module (crates-io))

(define-public crate-modality-ingest-protocol-0.1.0 (c (n "modality-ingest-protocol") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.13.1") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "modality-api") (r "^0.1") (f (quote ("client"))) (d #t) (k 0)))) (h "1lps93wjx4wcihjp8jfi560j8n0lfx1cma6a0w9844msqyq6s2pg") (f (quote (("default" "client") ("client"))))))

(define-public crate-modality-ingest-protocol-0.1.1 (c (n "modality-ingest-protocol") (v "0.1.1") (d (list (d (n "minicbor") (r "^0.13.1") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "modality-api") (r "^0.1") (f (quote ("client"))) (d #t) (k 0)))) (h "0wnk7p98f6dp0ga0qn9mf093v0n4gprwpwzi4ywn8m3jbnfjv4w6") (f (quote (("default" "client") ("client"))))))

(define-public crate-modality-ingest-protocol-0.2.0 (c (n "modality-ingest-protocol") (v "0.2.0") (d (list (d (n "minicbor") (r "^0.13.1") (f (quote ("std" "derive" "std" "derive"))) (d #t) (k 0)) (d (n "modality-api") (r "^0.2.0") (f (quote ("client"))) (d #t) (k 0)))) (h "0rhh2q3np38cy7rp2zqpih78f1bqjvj419addwrqcjjg63pll6h2") (f (quote (("default" "client") ("client"))))))

(define-public crate-modality-ingest-protocol-0.3.0 (c (n "modality-ingest-protocol") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.13.1") (f (quote ("std" "derive" "std" "derive"))) (d #t) (k 0)) (d (n "modality-api") (r "^0.3.0") (f (quote ("client"))) (d #t) (k 0)))) (h "03ss8ydwrhi8lb748pwmdnqg4bz3y06js8i0pbgxmh0z14c791s2") (f (quote (("default" "client") ("client"))))))

