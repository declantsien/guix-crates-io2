(define-module (crates-io mo da modark) #:use-module (crates-io))

(define-public crate-modark-0.5.1 (c (n "modark") (v "0.5.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "tl") (r "^0.7") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1si4ivxjhq9layhmkhg49ajc5icglwcssb2i6hgj53iff9rzax5a")))

(define-public crate-modark-0.5.2 (c (n "modark") (v "0.5.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "escaper") (r "^0.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "tl") (r "^0.7") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "1jnkkj8lfl4zm62nrw38j7d97rhqaj5dd7ja2d8pbnxxlblpb11n")))

