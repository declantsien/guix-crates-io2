(define-module (crates-io mo uc mouce) #:use-module (crates-io))

(define-public crate-mouce-0.1.0 (c (n "mouce") (v "0.1.0") (h "10k94aq6c34gzvbf4gn8rwpxql6pv5k19z9n9mf9v99247wyv6mb")))

(define-public crate-mouce-0.1.1 (c (n "mouce") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hmfvlcs4r74l52nrlirx1aaga2psl8jm129lb8nx8s2wy3ixm8w") (f (quote (("build-binary" "clap"))))))

(define-public crate-mouce-0.1.2 (c (n "mouce") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gvj7r7milqvh1ykllliniq054wjl84q802w0hkvv26jjacj0ga8") (f (quote (("build-binary" "clap"))))))

(define-public crate-mouce-0.1.3 (c (n "mouce") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10247m3shw8gycd3m1ls75v9b46xga1qgdm6qsrvmaxzky2ihgx2") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.1.4 (c (n "mouce") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v7vr20d7fm6hy0mwxqfmai3prjk7833581pxbcgfs2s5zn7al5p") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.1.5 (c (n "mouce") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j79kvgnmw608w56bfnmwb3z4qxcx4a00rrzb0i1q9fqd1i3a4xd") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.2.0 (c (n "mouce") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "19k9cvx2bdqpx2g0i2yh8q4ynz7xx3cy5raa9cfx1y1id9amdmy6") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.2.1 (c (n "mouce") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "0k2zxyyf1gzqza72k3gsvfjqnsf628ph762p538cn7qkwxvyflyg") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.2.2 (c (n "mouce") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "0nisy4dz44m139hpc8r1pkarf77i1rp8g56vk58raqp1abfrk44i") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.2.3 (c (n "mouce") (v "0.2.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1701jl86bddyvjs05pz3amd2r1vcz07wh90yyhx15rfk3ldq3yll") (f (quote (("cli" "clap"))))))

(define-public crate-mouce-0.2.4 (c (n "mouce") (v "0.2.4") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1i5my4p3ajrf53im7iz2rf5qlmkbgn8akq9znp72jngbh00vq04z") (f (quote (("x11") ("default" "x11") ("cli" "clap"))))))

(define-public crate-mouce-0.2.41 (c (n "mouce") (v "0.2.41") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "02g26jcwddmqj0yrznyhnpbvpfj5qr0wdgj6pkbkxagvklrmn33r") (f (quote (("x11") ("default" "x11") ("cli" "clap"))))))

(define-public crate-mouce-0.2.42 (c (n "mouce") (v "0.2.42") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "197vj5y6bpf2z36r7hix0z9314gj2lhd7r0biq76bdmxp94kpxp9") (f (quote (("x11") ("default" "x11") ("cli" "clap"))))))

(define-public crate-mouce-0.2.43 (c (n "mouce") (v "0.2.43") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "142vmss77vhn7jkxf84dal9sc8pc03cfh97f24g3mvlhsnhdjcsa") (f (quote (("x11") ("default" "x11") ("cli" "clap"))))))

(define-public crate-mouce-0.2.44 (c (n "mouce") (v "0.2.44") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)))) (h "1srawccwp1iny4awk0qjlqxnzhj4hc06prjr1kqzqwabizl1w9jm") (f (quote (("x11") ("default" "x11") ("cli" "clap"))))))

