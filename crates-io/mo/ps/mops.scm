(define-module (crates-io mo ps mops) #:use-module (crates-io))

(define-public crate-mops-0.1.0 (c (n "mops") (v "0.1.0") (h "0nkhjhixd40nsnap3siaqb66djvyd5wnbya6cpp13lil7mkpgddh") (y #t)))

(define-public crate-mops-0.1.1 (c (n "mops") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.10") (d #t) (k 0)) (d (n "azure_identity") (r "^0.11") (d #t) (k 0)) (d (n "azure_security_keyvault") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1bpxl3y6bnj1c9xj3ccgzvxnr1qh74yfyl7ik137wdm2p05dnhmx")))

(define-public crate-mops-0.1.2 (c (n "mops") (v "0.1.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.10") (d #t) (k 0)) (d (n "azure_identity") (r "^0.11") (d #t) (k 0)) (d (n "azure_security_keyvault") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1n7np9bs8fj5hxm1ixkc9fmyy2ziyjkgmswwnbwsks5m4bfip7jm")))

