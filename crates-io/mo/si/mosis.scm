(define-module (crates-io mo si mosis) #:use-module (crates-io))

(define-public crate-mosis-0.1.0 (c (n "mosis") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vxndf3z5746zmxwg1fh54736i4bz5c8bzcrs5dklnlrfnsdwmbd")))

(define-public crate-mosis-0.2.0 (c (n "mosis") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a3lsb0xh02p9iqmwgrrrc3wxvfj35n4jigjg95n9im28jgiyzsw") (y #t)))

(define-public crate-mosis-0.2.1 (c (n "mosis") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z5da15mvbf0ddqlglb0vwkmr824sa6phcnsxzz6hamy3p5x0val")))

