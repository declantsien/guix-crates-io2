(define-module (crates-io mo ol moolah) #:use-module (crates-io))

(define-public crate-moolah-0.1.0 (c (n "moolah") (v "0.1.0") (d (list (d (n "claims") (r "^0.7") (d #t) (k 2)) (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dqpg16z2hq13w31cfjpgmmbhqw0ivqvgwilhahyzwkabqcyw4y1")))

(define-public crate-moolah-0.1.1 (c (n "moolah") (v "0.1.1") (d (list (d (n "claims") (r "^0.7") (d #t) (k 2)) (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mn7xqsnpxfnvcbsxfgyjbsv3wi8nanwszw9ms4bz2xkqzc5i4in")))

(define-public crate-moolah-0.1.2 (c (n "moolah") (v "0.1.2") (d (list (d (n "claims") (r "^0.7") (d #t) (k 2)) (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w2xhk5a9clnk7yylj0inycs1xfd3kfhnk7mm31wk6b6rddn9w0l")))

(define-public crate-moolah-0.1.3 (c (n "moolah") (v "0.1.3") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0migd1bd1y6yy8sji6i33ldi3znmhhxxwsyidh9dq9nvgll68in6")))

(define-public crate-moolah-0.1.4 (c (n "moolah") (v "0.1.4") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wxr70pw1g49zyvxcrx4bkpk3cshh1vp0vms8kiwj0ycry0g4j9m")))

(define-public crate-moolah-0.1.5 (c (n "moolah") (v "0.1.5") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (k 2)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)))) (h "12p9wj464zmxyrw300mhz3b0qnqbayygkhilndravvjynm9hqfm6")))

(define-public crate-moolah-0.1.7 (c (n "moolah") (v "0.1.7") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (k 2)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y71nrzdzcd2vs5w7ba1hfzc1df0cyaqp03f6750arrb835kwabl")))

(define-public crate-moolah-0.1.8 (c (n "moolah") (v "0.1.8") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (k 2)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01g4fbdfmac5asslcy4yfcziy7sdqafs0nj3708lzv4dypvi9bqs")))

(define-public crate-moolah-0.1.9 (c (n "moolah") (v "0.1.9") (d (list (d (n "mudra") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (k 2)) (d (n "rust_decimal") (r "^1.34") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nhpxzz69ywkh05girpf8a5il166699ril88w8id1c7sxf9wcg9b")))

