(define-module (crates-io mo sq mosquitto-client) #:use-module (crates-io))

(define-public crate-mosquitto-client-0.1.2 (c (n "mosquitto-client") (v "0.1.2") (h "1356q16pqsw2400nj0a0cvs4cq8fp4yilkdk6pbcgkjfz6gq3wfs")))

(define-public crate-mosquitto-client-0.1.3 (c (n "mosquitto-client") (v "0.1.3") (h "119h6xmlipsqfw1kk12a1zzapwd97h76zbxsanm8dx59sda72rcs")))

(define-public crate-mosquitto-client-0.1.4 (c (n "mosquitto-client") (v "0.1.4") (h "1dlppxs4lkh5zav2d0yjckysj5y2lms0fjf0iy6q8p1567zqbpra")))

(define-public crate-mosquitto-client-0.1.5 (c (n "mosquitto-client") (v "0.1.5") (h "0ij8394yh8nwjqrn9q574vhaan8nd9kswbm3jni5v44zm01yandz")))

