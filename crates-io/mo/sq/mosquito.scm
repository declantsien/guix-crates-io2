(define-module (crates-io mo sq mosquito) #:use-module (crates-io))

(define-public crate-mosquito-0.1.0 (c (n "mosquito") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lwhhzbnxhp0rmj49rv9i96q7x959nda2fv1vwlgchy4qhlwpaam")))

(define-public crate-mosquito-0.1.1 (c (n "mosquito") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hb5kn84qjm8vnhfs1f10qy2yx679xn7bk1l52r968d8a5zhmnwi")))

(define-public crate-mosquito-0.1.2 (c (n "mosquito") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0swv1v5gcbxgd75f8xmjn6qkk57hpn5ipf1qm9fqrj74b2pq4837")))

