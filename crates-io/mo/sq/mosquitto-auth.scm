(define-module (crates-io mo sq mosquitto-auth) #:use-module (crates-io))

(define-public crate-mosquitto-auth-0.1.0 (c (n "mosquitto-auth") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mosquitto-io") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql") (r "^23.0.1") (d #t) (k 0)))) (h "1ik6nlzhxg0fxwn2wj5zfy3mrrhhll0aws67p8ipqrqc8is4jivd")))

