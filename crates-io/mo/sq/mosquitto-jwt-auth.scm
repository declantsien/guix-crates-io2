(define-module (crates-io mo sq mosquitto-jwt-auth) #:use-module (crates-io))

(define-public crate-mosquitto-jwt-auth-0.2.0 (c (n "mosquitto-jwt-auth") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)))) (h "0jii5rj4h0jrca3b9y0g8h9w52c85ghn3xhs3c8alh4xil12785q")))

