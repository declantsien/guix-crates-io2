(define-module (crates-io mo sq mosquitto-plugin) #:use-module (crates-io))

(define-public crate-mosquitto-plugin-0.1.0 (c (n "mosquitto-plugin") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06vxhx156slrs4v77df4sly6qc9kydvyh39q7pp1441dvyl6d89s")))

(define-public crate-mosquitto-plugin-0.1.1 (c (n "mosquitto-plugin") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zlc83dj5484dmk8rc2x5hxhqdxjyxawk9yaqnjlgccyh1yjwv8s")))

(define-public crate-mosquitto-plugin-1.0.1 (c (n "mosquitto-plugin") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s5jzv03nzc8jgwlwk5vd0nhfq3i9bcgxag7bbcw7nbcigigcfxk")))

(define-public crate-mosquitto-plugin-1.0.2-alpha.0 (c (n "mosquitto-plugin") (v "1.0.2-alpha.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jk02k806s7w2ncv773cz1bbf3yqik7qklj1k59jq53pxgx8p6cc")))

(define-public crate-mosquitto-plugin-1.1.0 (c (n "mosquitto-plugin") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fhvm1qngmhf0fzjaxxd89s5n100dm0ky978sizck2w4gs1lpzf7")))

(define-public crate-mosquitto-plugin-1.2.0 (c (n "mosquitto-plugin") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pdh6q22gch5sp05gxbz5c7x48g5pwsz6c8mrcfj6s1kb7skg07l")))

(define-public crate-mosquitto-plugin-1.3.0 (c (n "mosquitto-plugin") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00cpl8m5d0b7d43bavgn9a1igjwmzr6f555q2xmpy5k9ydx150k2") (y #t)))

(define-public crate-mosquitto-plugin-2.0.0 (c (n "mosquitto-plugin") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "196644cmb91f3mkhy16vihi7qasdiifac162jmn2wdw2k88prw12")))

(define-public crate-mosquitto-plugin-2.1.0 (c (n "mosquitto-plugin") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ckgca1jg03gq7p50pqbzkzv1jswrhmslfiy0idmisbmsqyfdqw1")))

(define-public crate-mosquitto-plugin-2.1.1 (c (n "mosquitto-plugin") (v "2.1.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18xnkv7c68r09kkibmc0cffjizdmgsn04qfpipd625ajndw68g2m")))

(define-public crate-mosquitto-plugin-2.1.2 (c (n "mosquitto-plugin") (v "2.1.2") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zv6bcmaz1rvs53wkqdbpqal93qanjg1sbchwwz9azk3pw4g3fxn")))

(define-public crate-mosquitto-plugin-2.1.3 (c (n "mosquitto-plugin") (v "2.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.64") (f (quote ("runtime"))) (k 1)))) (h "0z6lm2lbyjcngc4v5y4fhi4amaq1hazvxp5s4nnx1wgr8h5fvd4m")))

