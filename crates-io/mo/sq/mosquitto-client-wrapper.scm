(define-module (crates-io mo sq mosquitto-client-wrapper) #:use-module (crates-io))

(define-public crate-mosquitto-client-wrapper-0.1.6 (c (n "mosquitto-client-wrapper") (v "0.1.6") (h "0qvrzsdshd5lfi2jb1h3vv94zv72kk4425a9h2kbhb2ypmyk98mf")))

(define-public crate-mosquitto-client-wrapper-0.2.0 (c (n "mosquitto-client-wrapper") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0dpixsip0i1dvn06f59j8b0rxmrfqvi9vxhlmirnm716ddh2d2yl") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.1 (c (n "mosquitto-client-wrapper") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0fdccj49kjdjqfghikzwwpd7c4zxhjp2hf1pr41nvl3k3r01y4x3") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.2 (c (n "mosquitto-client-wrapper") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "05bd08zlm982716vskqzddfhllsj8smq5my75sy9ic5pyazhfdlk") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.3 (c (n "mosquitto-client-wrapper") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1bpaxshvp90cqh1i4bjp2cp6zcp6wb1h1p08gmnw2yvrm73brj5x") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.4 (c (n "mosquitto-client-wrapper") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "09zx7i609b46zajb1i9kibzkfbkh518k6i5naj55rnsjizq6s353") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.5 (c (n "mosquitto-client-wrapper") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0rbzfll8vw18j0dikghw06k8a1ydjxh5ij9hkm9w1x57jhg3677m") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.6 (c (n "mosquitto-client-wrapper") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "14h469ciir4h1gviz7p4b0xxy7p03f9rdwi5w3zn3qwhhxz1lbkw") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.2.7 (c (n "mosquitto-client-wrapper") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0p8942pfv8lnxrj4vdml6lkasmjm1c3fplgxh3qg5zm7bkb9992s") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.3.0 (c (n "mosquitto-client-wrapper") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0x0bkqh1f4hvla583ziqca56b3bqkvifg175g4j0gkh6ifin9gw3") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

(define-public crate-mosquitto-client-wrapper-0.3.1 (c (n "mosquitto-client-wrapper") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "08yr101014gafw40xh3kbagwmazlnaj8642zc47zv2wly9jvpigb") (f (quote (("default" "bundled") ("bundled" "build_bindgen" "cmake" "anyhow") ("build_bindgen" "bindgen"))))))

