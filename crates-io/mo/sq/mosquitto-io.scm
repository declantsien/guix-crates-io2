(define-module (crates-io mo sq mosquitto-io) #:use-module (crates-io))

(define-public crate-mosquitto-io-0.1.0 (c (n "mosquitto-io") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1pyr7m1n04yydzbxhpp1bd81kd2z93qrs2q9c1hr7zm3l6fdrk58") (l "mosquitto")))

(define-public crate-mosquitto-io-0.1.1 (c (n "mosquitto-io") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1yhr2asc9j7akz1a2hcyvc7kg635i2g7n490b4sw2631yqxwqpli") (l "mosquitto")))

