(define-module (crates-io mo og moogle) #:use-module (crates-io))

(define-public crate-moogle-0.0.1 (c (n "moogle") (v "0.0.1") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "1rdw60yv7g92hvafxv3zw9s2ihjr1cyja3xrsyxlg1h3vfq4dhzr")))

(define-public crate-moogle-0.0.2 (c (n "moogle") (v "0.0.2") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "186wan59sqlb6yrakr9fkadnwkvigz8xnvrk546k2p99z0fhr4gj")))

(define-public crate-moogle-0.0.3 (c (n "moogle") (v "0.0.3") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "128z2lxqp5h8d3fs72vslr8abpa3s9b276xxrfvdanp8032lfq96")))

(define-public crate-moogle-0.0.4 (c (n "moogle") (v "0.0.4") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "07q9h3fwnv8fq0v0i2jq5c5ffpnlxbzag63755j8y9fyw9y7p14j")))

(define-public crate-moogle-0.0.5 (c (n "moogle") (v "0.0.5") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "0rh2ik37yrp9nfldbhc9g4rf7kjkrq3llc4ks7xvy8cssrjndkyn")))

(define-public crate-moogle-0.0.6 (c (n "moogle") (v "0.0.6") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "14ba541a6mq1d7s09r4h2zbsaphvj845p2im00s0ibn1h7lx5wly")))

(define-public crate-moogle-0.0.7 (c (n "moogle") (v "0.0.7") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "09qsj55325f9zb7qvc4dd2hpjhdacjafmmcbx5phi8km9jwm9vwq")))

(define-public crate-moogle-0.0.8 (c (n "moogle") (v "0.0.8") (d (list (d (n "auto_enums") (r "^0.7") (d #t) (k 0)))) (h "127m8mr7z27d0m0pmx8rjdnvqfw15rd7q7sjddmnll7sfw3134lz")))

(define-public crate-moogle-0.0.9 (c (n "moogle") (v "0.0.9") (h "152a5nv8xi33jxdixq84rf7chp7bs1w6m2fv8jav81fp9ly88shm")))

(define-public crate-moogle-0.1.0 (c (n "moogle") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1zg4rljw1k627phhw5y9xabzgl3y1d8f7xdbmqhf5xh8fibcz8zv")))

(define-public crate-moogle-0.1.1 (c (n "moogle") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0sa2sf2x8jy8dzm0caqn8kvw3w7xhlil83l6m9m3hzppshqwqs3v")))

(define-public crate-moogle-0.1.2 (c (n "moogle") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "10bb1w1gwfn0gbbp4rf44xcl7bvka9qx7q63gsqy1lz7kl1jhjmj")))

(define-public crate-moogle-0.1.3 (c (n "moogle") (v "0.1.3") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "110ijxhln1y51diyzp05rhpp04qwnvk9d8w7fj1az7x4q996vz9h")))

(define-public crate-moogle-0.2.0 (c (n "moogle") (v "0.2.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1yzqx7l3v8kjn2rxr829id1058mrhr5bjypqs27y5yzljjnjbg4y")))

(define-public crate-moogle-0.2.1 (c (n "moogle") (v "0.2.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0g1qpnfp2aq6dkpndr302qmvg9gwlxrypfsbqbz7vpbyrav6qcr8")))

(define-public crate-moogle-0.3.0 (c (n "moogle") (v "0.3.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "04lswjv7mpsrxpfa91lza63arrpjr6386033mlvmljqphb416fxq")))

(define-public crate-moogle-0.3.1 (c (n "moogle") (v "0.3.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1094qcf6q1p7w938yvzxbxpv9ycjk2cbp5rmh52d543l7pfzm685") (f (quote (("serde1" "serde"))))))

(define-public crate-moogle-0.4.0 (c (n "moogle") (v "0.4.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yggm71gsdiayywh255a21bq06z5xgw2984c50v7rx726zndzvnr") (f (quote (("serde1" "serde"))))))

(define-public crate-moogle-0.4.1 (c (n "moogle") (v "0.4.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16g5ih0y7sa1ihw5g91rc04w71vjcbk9bjrd2i7kc3s8sb5g27yj") (f (quote (("serde1" "serde"))))))

(define-public crate-moogle-0.4.2 (c (n "moogle") (v "0.4.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xvasfd1z2r3v8xb9s80immfc1yackpr5p73srbifr3l02p52qgd") (f (quote (("serde1" "serde"))))))

(define-public crate-moogle-0.4.3 (c (n "moogle") (v "0.4.3") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n8qxslb3fq7dk2irmmarihqf46ym514rqqzsgfvgjpd7p3k4426") (f (quote (("serde1" "serde"))))))

(define-public crate-moogle-0.4.4 (c (n "moogle") (v "0.4.4") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1h702gzqymc090sjhxq9p082ydk8iv79nfmydcns5y5qwkjaqy3v") (f (quote (("serde1" "serde"))))))

