(define-module (crates-io mo fm mofmt) #:use-module (crates-io))

(define-public crate-mofmt-0.4.0 (c (n "mofmt") (v "0.4.0") (d (list (d (n "moparse") (r "^0.1.4-rc4") (d #t) (k 0)))) (h "0ywhqnvq27mdz8qschf1gknv54dzbzfqh4jvpq14sj679lap7xrr")))

(define-public crate-mofmt-0.4.1 (c (n "mofmt") (v "0.4.1") (d (list (d (n "moparse") (r "^0.1.4") (d #t) (k 0)))) (h "0mxr6mh2cmhcb7pnpgh77r20qpdz41vxq8sx52y92cz07vgsdil5")))

(define-public crate-mofmt-0.4.2 (c (n "mofmt") (v "0.4.2") (d (list (d (n "moparse") (r "^0.1.4") (d #t) (k 0)))) (h "0373i60qm3am48mw0x094s8vzlpwfj779n5x5jl18nzj61gpbms2")))

(define-public crate-mofmt-0.5.0 (c (n "mofmt") (v "0.5.0") (d (list (d (n "moparse") (r "^0.1.4") (d #t) (k 0)))) (h "0lwni2vh2np6lf76n01rn2n386v69f18np2afcxxxpglklk1j7lq")))

(define-public crate-mofmt-0.5.1 (c (n "mofmt") (v "0.5.1") (d (list (d (n "moparse") (r "^0.1.6") (d #t) (k 0)))) (h "168l5xv4s5sz6ksbxw2z0g67bl66hcc4n5giayw29cvipnfp9pfh")))

