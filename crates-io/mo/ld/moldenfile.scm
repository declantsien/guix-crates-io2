(define-module (crates-io mo ld moldenfile) #:use-module (crates-io))

(define-public crate-moldenfile-0.1.0 (c (n "moldenfile") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.7") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1918czxzdb2h3mq01qsr7r2jirakya210ml7ijrb6lfj8crqsqys")))

