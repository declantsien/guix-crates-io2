(define-module (crates-io mo ld mold2d) #:use-module (crates-io))

(define-public crate-mold2d-0.0.1 (c (n "mold2d") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.3") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (k 0)))) (h "030gqhwy2y95ixkihjfwwpyynqx8h2rdabwpm5hardppsi55lhyz")))

(define-public crate-mold2d-0.0.2 (c (n "mold2d") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_image") (r "^0.3") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.9") (d #t) (k 0)))) (h "0vndjn0jphkljqigd0pinyhlp7j8hsfsai0cc0wa4bw87q0flw5m")))

