(define-module (crates-io mo du modular_math) #:use-module (crates-io))

(define-public crate-modular_math-0.1.0 (c (n "modular_math") (v "0.1.0") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "0sm9ph1lpmxy8qcr7xb54f4jyw9pjwq2l6xbjc83p9fkffr2f3y9")))

(define-public crate-modular_math-0.1.1 (c (n "modular_math") (v "0.1.1") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "1049x3fi0sj7wadk2wbmmyns8jvrkalhbfmyhjpix39rpwviajrk")))

(define-public crate-modular_math-0.1.2 (c (n "modular_math") (v "0.1.2") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "1bbsr7g8vgs64bqlmk6zcczkrg31gmji48sf04dkbk667l14crxh")))

(define-public crate-modular_math-0.1.3 (c (n "modular_math") (v "0.1.3") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "07qm73di4gxbw1p48igb9vcxbs8hjzvg2l4500sq3s8dydb3jpap")))

(define-public crate-modular_math-0.1.4 (c (n "modular_math") (v "0.1.4") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "1gnbgg4qzbxqqjwhb8z4sg9k45pcavwm3faahcs561gcbvf97b9w")))

(define-public crate-modular_math-0.1.5 (c (n "modular_math") (v "0.1.5") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)))) (h "1p2x9k524bbp0k53lvy3g0gdm0ywv36h64ass30pkj8lahcpa5h0")))

(define-public crate-modular_math-0.1.6 (c (n "modular_math") (v "0.1.6") (d (list (d (n "primitive-types") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14q0xh95qdjq5fx0q72drgszhlfp6ip8qq9349fxx8fcwn4n1ffi")))

