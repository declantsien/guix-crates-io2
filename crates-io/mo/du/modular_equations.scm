(define-module (crates-io mo du modular_equations) #:use-module (crates-io))

(define-public crate-modular_equations-1.0.0 (c (n "modular_equations") (v "1.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11i92s8hfjdrnrlwfqx7ijdc2lx77a5jpqqqy8jrxl2y4z3xvc4y")))

(define-public crate-modular_equations-1.0.1 (c (n "modular_equations") (v "1.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vsj5w67f9v8w0z8isj28qkaj9hl47y49n745c6v02yqlz80xk42")))

(define-public crate-modular_equations-1.0.2 (c (n "modular_equations") (v "1.0.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vs6mnxg415v0nxddcak78ki26bn7ybjabr20bf09jcypmn7zgb4")))

(define-public crate-modular_equations-1.0.3 (c (n "modular_equations") (v "1.0.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1600l6xjrs9k5nyl526kgplxv8k0xb4kqy44097wzdfqarslixpc")))

(define-public crate-modular_equations-1.0.4 (c (n "modular_equations") (v "1.0.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bl3i6lpsqxqiavl5nas24plinih8496r7f0wvx1x1ynf6zv54z8")))

(define-public crate-modular_equations-1.0.5 (c (n "modular_equations") (v "1.0.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1n8qw6w7qx04hi2w9gz86xapky3pi9ax7rq8a0cf0safsbidvmgd")))

