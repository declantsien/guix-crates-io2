(define-module (crates-io mo du modular-bitfield-impl) #:use-module (crates-io))

(define-public crate-modular-bitfield-impl-0.1.0 (c (n "modular-bitfield-impl") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1gfdx5ra3s07ihyq7r756n3hai7c1x6aqpwb8d503lv3psn6vpk9")))

(define-public crate-modular-bitfield-impl-0.2.0 (c (n "modular-bitfield-impl") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1bg8lzpm5ijrig680bwsc06fhl01216xs1cfd63vwsxg4n0xh8l3")))

(define-public crate-modular-bitfield-impl-0.3.0 (c (n "modular-bitfield-impl") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0ym6403qw5nxaqr8d4b3h9x902fv09f8kqai0s5bqn9njc2v24v0")))

(define-public crate-modular-bitfield-impl-0.4.0 (c (n "modular-bitfield-impl") (v "0.4.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1svl8krc2xrqjqyp12yf4m4rp0732xpn0fr37lvcv2kmdvy0cb")))

(define-public crate-modular-bitfield-impl-0.5.0 (c (n "modular-bitfield-impl") (v "0.5.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0rh53zrz7dlmvafrcldx0fbmak43mvj229cwbzg70yilv2h48z0w")))

(define-public crate-modular-bitfield-impl-0.6.0 (c (n "modular-bitfield-impl") (v "0.6.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "094qx9pxzlsr4hp95n2p6pj0j66582i4h3ls8ppvrygl5rj5ynlw")))

(define-public crate-modular-bitfield-impl-0.7.0 (c (n "modular-bitfield-impl") (v "0.7.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mpv7nkn77y2hg4x0wkggc8c10iiiwbkq2q6xpiwbbhkmv7s3m15")))

(define-public crate-modular-bitfield-impl-0.8.0 (c (n "modular-bitfield-impl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6kqf81bhkg5b40ifdpv3f5p6z5xfa1pw5sw4dwb6bgh9sb1gci")))

(define-public crate-modular-bitfield-impl-0.9.0 (c (n "modular-bitfield-impl") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w0jyhhywd0gqw6752x04v5h5dshnvkxipiv43qzw3xc30al3mw9")))

(define-public crate-modular-bitfield-impl-0.10.0 (c (n "modular-bitfield-impl") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l40zxbv00swczi7vx5px29asxyc5rvqxrs5fjqq4cay1c3fd0sr")))

(define-public crate-modular-bitfield-impl-0.11.0 (c (n "modular-bitfield-impl") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kis1wwqmygnx61ppx0w09m4aia5prclkskjn381ywwz7ghlmixl")))

(define-public crate-modular-bitfield-impl-0.11.1 (c (n "modular-bitfield-impl") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a6i9d9ind2srqg5xhwwv8qb5g1jgm9mvfshgbxdzl09816rpzir")))

(define-public crate-modular-bitfield-impl-0.11.2 (c (n "modular-bitfield-impl") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12dpljzpnx5yfmlk9amb2cm0c6ikxi8adiidvrlbqgk0frq5yzas")))

