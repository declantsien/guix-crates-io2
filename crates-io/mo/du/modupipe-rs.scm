(define-module (crates-io mo du modupipe-rs) #:use-module (crates-io))

(define-public crate-modupipe-rs-0.0.1 (c (n "modupipe-rs") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1699xz1mjbdn04jwl5zv6yfrl0gxawqypd7l2k0zy2kjyjcxv1qw") (y #t)))

(define-public crate-modupipe-rs-0.0.2 (c (n "modupipe-rs") (v "0.0.2") (h "1y0jg2xbzvvykh8bbql3xvx2vhsx1ygv0rg48xsbvr25lxqvp5g5") (y #t)))

