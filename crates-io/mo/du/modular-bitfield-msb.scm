(define-module (crates-io mo du modular-bitfield-msb) #:use-module (crates-io))

(define-public crate-modular-bitfield-msb-0.11.2 (c (n "modular-bitfield-msb") (v "0.11.2") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-msb-impl") (r "^0.11.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pfa0v7np30f7mhxwyapjsbqykhi20rfwfg7hq0bn9h1bckw9y54")))

