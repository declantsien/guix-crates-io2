(define-module (crates-io mo du modular-bitfield) #:use-module (crates-io))

(define-public crate-modular-bitfield-0.1.0 (c (n "modular-bitfield") (v "0.1.0") (d (list (d (n "modular-bitfield-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nz2lvbw6gsr7fanr1hjx7csbrs78r2g9107gmdkdmxzzzl5s1b6")))

(define-public crate-modular-bitfield-0.1.1 (c (n "modular-bitfield") (v "0.1.1") (d (list (d (n "modular-bitfield-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yjxyri4rsizlzh6ps7g0zqlcnqb094ii16fkz39mpm6497nfz5p")))

(define-public crate-modular-bitfield-0.2.0 (c (n "modular-bitfield") (v "0.2.0") (d (list (d (n "modular-bitfield-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rvbra27c9dkwlw41r4yir2b06wh6272kr5wzacz25x6440pamrr")))

(define-public crate-modular-bitfield-0.3.0 (c (n "modular-bitfield") (v "0.3.0") (d (list (d (n "modular-bitfield-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ika0sjphmcbsmvd4p74phs0l8iszsnj4nac5kjvvm941k1h12d6")))

(define-public crate-modular-bitfield-0.4.0 (c (n "modular-bitfield") (v "0.4.0") (d (list (d (n "modular-bitfield-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0kiabc5qkcr9cyq40kplaxgvgms0p29pa6g6m155m512md5lgbdh")))

(define-public crate-modular-bitfield-0.5.0 (c (n "modular-bitfield") (v "0.5.0") (d (list (d (n "modular-bitfield-impl") (r "^0.5.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ql4120v67l5gd99nj1p16bh2jjfhgrrycfbzq60v35yc0ljympq")))

(define-public crate-modular-bitfield-0.6.0 (c (n "modular-bitfield") (v "0.6.0") (d (list (d (n "modular-bitfield-impl") (r "^0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yzxisw8l2rbywwl0fq8qj5fbswnj7mgp28z2z3zl78c9b005kng")))

(define-public crate-modular-bitfield-0.7.0 (c (n "modular-bitfield") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19xrxng62yc37vjcagxr0760mmaf29lg7sxgsh3zqld067zbib5f")))

(define-public crate-modular-bitfield-0.8.0 (c (n "modular-bitfield") (v "0.8.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.8") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0nq6bif40rshin1zrh2lq2wfybvwnhdkp7rw1kxx15l06blh39p2")))

(define-public crate-modular-bitfield-0.9.0 (c (n "modular-bitfield") (v "0.9.0") (d (list (d (n "modular-bitfield-impl") (r "^0.9") (d #t) (k 0)))) (h "0lyrmcbr1ms66482zci036y6kqyrykm33p20pbay5km2fzm07fv5")))

(define-public crate-modular-bitfield-0.10.0 (c (n "modular-bitfield") (v "0.10.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1j5a46fnz9hkscv8r7pdvcvwaqdq065qfv9hgj08pbf534b5ybpz")))

(define-public crate-modular-bitfield-0.11.0 (c (n "modular-bitfield") (v "0.11.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rp114dd5vq484hafqa4jl6ygl3wzg58f7zlpr97qjszhsqd8pwq")))

(define-public crate-modular-bitfield-0.11.1 (c (n "modular-bitfield") (v "0.11.1") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.11.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1xp555f75s6fr5cpm5nb62bspz0iz9h2j02si30780549051x2aw")))

(define-public crate-modular-bitfield-0.11.2 (c (n "modular-bitfield") (v "0.11.2") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "modular-bitfield-impl") (r "^0.11.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0x4aygjbv3xdyiywwwkgcbafjyqzya0x5fdkkr7irb04hfx7jgd5")))

