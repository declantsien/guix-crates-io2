(define-module (crates-io mo du modupipe) #:use-module (crates-io))

(define-public crate-modupipe-0.0.1 (c (n "modupipe") (v "0.0.1") (h "0g9ifwkxa93rf146bbpqny4cxsmagwiw54bfhfn2l4fzd6xsapcr") (y #t)))

(define-public crate-modupipe-0.1.0 (c (n "modupipe") (v "0.1.0") (h "0a4ahdpf3yd46rsn4rsl11m7v6ilmyhawnw3gnwjn78fzgqarhcw")))

