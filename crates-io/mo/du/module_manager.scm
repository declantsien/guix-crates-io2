(define-module (crates-io mo du module_manager) #:use-module (crates-io))

(define-public crate-module_manager-0.1.0 (c (n "module_manager") (v "0.1.0") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "1n4i2y6y7f81bsmjriydsmi8ivfiah1p0v3vpak2lz26ifkdb074") (y #t)))

(define-public crate-module_manager-0.2.0 (c (n "module_manager") (v "0.2.0") (d (list (d (n "module_manager_derive") (r "^0.2.0") (d #t) (k 0)))) (h "182fk29y9wphr1b8kzf94fmqbg59mmp9mm9icqkf76fl7d2pk9qz") (y #t)))

(define-public crate-module_manager-0.2.1 (c (n "module_manager") (v "0.2.1") (d (list (d (n "module_manager_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0wgq0df86bk24llp9gvryfv1cj74ymwzz076065frmcjy20h112j") (y #t)))

(define-public crate-module_manager-0.2.2 (c (n "module_manager") (v "0.2.2") (d (list (d (n "module_manager_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1l40jla84gylpxxmy691gz3dv37i5p89fgv3vni96fw7xcdik0v6") (y #t)))

(define-public crate-module_manager-0.2.3 (c (n "module_manager") (v "0.2.3") (d (list (d (n "module_manager_derive") (r "^0.2.1") (d #t) (k 0)))) (h "007dgbpr7lyg2pmhmx9shvvridjvy92nqzi7p1x893lkxnf9r27x") (y #t)))

