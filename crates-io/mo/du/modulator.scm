(define-module (crates-io mo du modulator) #:use-module (crates-io))

(define-public crate-modulator-0.1.0 (c (n "modulator") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1cafpnd1byzfys7x9pl73f5gn41asfidnnpzx34ffg0snhf1hxjq")))

(define-public crate-modulator-0.3.0 (c (n "modulator") (v "0.3.0") (d (list (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06lj66qjsn80324q58m0qbqvp1r3xp4pmcfwjmxp6nwla6phzg8p")))

(define-public crate-modulator-0.4.0 (c (n "modulator") (v "0.4.0") (d (list (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q14s85zswpqn2hypaz5kvy6k0df8d9fjzriffv5by9d16rn1fmy")))

