(define-module (crates-io mo du modulo) #:use-module (crates-io))

(define-public crate-modulo-0.1.0 (c (n "modulo") (v "0.1.0") (h "0fnfgfby02z6f87g1ishg24kgyrbc3s029b79cf77jk3qxkj4bk8")))

(define-public crate-modulo-0.1.1 (c (n "modulo") (v "0.1.1") (h "1rzsfl316gdah3142y90zi7a7rvpdm7gs43xnhw65cax1fhxmzxn")))

(define-public crate-modulo-0.1.2 (c (n "modulo") (v "0.1.2") (h "1a5dm8bcb0jhksmyks9mp5hi1pd01v5wlb4sxwml1y0xmiy49nsk")))

