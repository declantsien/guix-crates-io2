(define-module (crates-io mo du module-utils) #:use-module (crates-io))

(define-public crate-module-utils-0.2.0 (c (n "module-utils") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "module-utils-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-proxy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kxh917y0ycagm29fibrya602f1s1rf07m5valwwgcs5flgpb29h")))

