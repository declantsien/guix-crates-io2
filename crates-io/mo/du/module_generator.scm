(define-module (crates-io mo du module_generator) #:use-module (crates-io))

(define-public crate-module_generator-0.1.0 (c (n "module_generator") (v "0.1.0") (h "1jgi6hcfpqi51yz2ls7h17ndlz0zgs18yznnn92ch3j1ayvdj5nk")))

(define-public crate-module_generator-0.1.1 (c (n "module_generator") (v "0.1.1") (h "1ah1f2420myrr7nwdqkn6sakcq7b4acmbd0454h9yxgx5jdzmwfq")))

(define-public crate-module_generator-0.1.2 (c (n "module_generator") (v "0.1.2") (h "0ll55q2zz96vdd3fv48f00dv752cv92avls07gb1pggpfn5a5xr2")))

(define-public crate-module_generator-0.1.3 (c (n "module_generator") (v "0.1.3") (h "0lpn6mcb4a3pl3bwiy7c31l3kw1aicx52s8h7mzpj91xrq2j4c26")))

(define-public crate-module_generator-0.1.4 (c (n "module_generator") (v "0.1.4") (h "1xfw3cyqs6q86a08604qvvq4fh62rpxkdwpmxnskqfl303z5apvr")))

(define-public crate-module_generator-0.2.0 (c (n "module_generator") (v "0.2.0") (h "1z9qmj44x0rdvjhd2y8micz36ip7pllg21cbnv1jfx44w731kwph")))

(define-public crate-module_generator-0.2.1 (c (n "module_generator") (v "0.2.1") (h "11z6zpp3kj5im32vl7ngjifmx417kzg3mq9aafd6bhqi975jqfd7")))

(define-public crate-module_generator-0.2.2 (c (n "module_generator") (v "0.2.2") (h "19p2p314sfm04rzgv697fhj1gvsb8yg2arlc4rvrrjv0hx1yvkn9")))

(define-public crate-module_generator-0.2.3 (c (n "module_generator") (v "0.2.3") (h "1jrf9m7sw2gk7n14gqxdq6vffdmrnhjmkn3yxsma7v22ldra4kwr")))

