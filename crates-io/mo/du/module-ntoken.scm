(define-module (crates-io mo du module-ntoken) #:use-module (crates-io))

(define-public crate-module-ntoken-0.1.0 (c (n "module-ntoken") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zpfiragmvp86kw3mz6pa0ssb4xqaxgba7y02za7vmx2gli9rnjf")))

