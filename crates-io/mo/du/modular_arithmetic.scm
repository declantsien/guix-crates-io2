(define-module (crates-io mo du modular_arithmetic) #:use-module (crates-io))

(define-public crate-modular_arithmetic-0.1.0 (c (n "modular_arithmetic") (v "0.1.0") (h "09w0vqbbpifif4w24hgp3kda1ifzn3x8vslqf8d05r8gdichi067")))

(define-public crate-modular_arithmetic-0.2.0 (c (n "modular_arithmetic") (v "0.2.0") (h "1rysr3yk70qn07pzg960x6hgqzkmqidvcy5yc55s502978512p8l")))

