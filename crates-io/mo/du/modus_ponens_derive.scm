(define-module (crates-io mo du modus_ponens_derive) #:use-module (crates-io))

(define-public crate-modus_ponens_derive-0.1.0 (c (n "modus_ponens_derive") (v "0.1.0") (d (list (d (n "modus_ponens") (r "^0.11.0") (d #t) (k 0)))) (h "17rv4q14makk4sh4i57sg2wh29gx620q5b0vi7lhqihkwd1ixc9y")))

(define-public crate-modus_ponens_derive-0.1.2 (c (n "modus_ponens_derive") (v "0.1.2") (d (list (d (n "modus_ponens") (r "^0.11.4") (d #t) (k 0)))) (h "04myj3gsdid023qxibvg8rwg63f7lgw9z28qsbbr6yvkwig13yy0")))

(define-public crate-modus_ponens_derive-0.1.3 (c (n "modus_ponens_derive") (v "0.1.3") (d (list (d (n "modus_ponens") (r "^0.11.5") (d #t) (k 0)))) (h "0z49897pvsw3j1l7jz9318g3xv57a06i0hzbrxmb0cpsxazgxm6g")))

