(define-module (crates-io mo du module_manager_derive) #:use-module (crates-io))

(define-public crate-module_manager_derive-0.2.0 (c (n "module_manager_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqrplfz421gg48szbpv2yd0qgs23ljd1nic28nz3r7n9vbnxhfi")))

(define-public crate-module_manager_derive-0.2.1 (c (n "module_manager_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0k64xcd9jzwrd7afwyhxim1p50yrp70yih6gijf3vzqiyxpwsq")))

