(define-module (crates-io mo du module-utils-macros) #:use-module (crates-io))

(define-public crate-module-utils-macros-0.2.0 (c (n "module-utils-macros") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 2)) (d (n "pingora-core") (r "^0.2.0") (d #t) (k 2)) (d (n "pingora-proxy") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^2.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "18g07nr52v211ll5lpjcv8jn7z0f6pbj4wj9cid00mcq52fyn5cj")))

