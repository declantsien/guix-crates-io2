(define-module (crates-io mo du modulo-n-tools) #:use-module (crates-io))

(define-public crate-modulo-n-tools-0.1.0 (c (n "modulo-n-tools") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "13dq8xc9k0kkamn15gv0i9wm3nm7yczn9pj0mkxpn1ylpix7qmjb")))

(define-public crate-modulo-n-tools-0.1.1 (c (n "modulo-n-tools") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "1bi31q2mgwqgacs6nhjamh8nn9vigrrg19y1xxg11s5lhgqyqv0j")))

(define-public crate-modulo-n-tools-0.2.0 (c (n "modulo-n-tools") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "1khbxd66102ihppqy39fqzyj3370fy94h36hw8jnxzlr52c78hrd")))

