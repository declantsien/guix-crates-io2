(define-module (crates-io mo sc moscato) #:use-module (crates-io))

(define-public crate-moscato-0.1.0 (c (n "moscato") (v "0.1.0") (d (list (d (n "pinot") (r "^0.1.5") (d #t) (k 0)))) (h "1mahf9alydaxdfam2lpf26f8hpfnapgf2b439d42qs3n6cz69hf8")))

(define-public crate-moscato-0.1.1 (c (n "moscato") (v "0.1.1") (d (list (d (n "pinot") (r "^0.1.5") (d #t) (k 0)))) (h "13d30fpg26rnmacj32dnz6fjfccx8qfhi7daa4wjh6hgwfy0k12p")))

(define-public crate-moscato-0.1.2 (c (n "moscato") (v "0.1.2") (d (list (d (n "pinot") (r "^0.1.5") (d #t) (k 0)))) (h "0r3w815434q4a01alaj7d0dfy4f5v74dnklw19sk3i5jr36zcwl3")))

