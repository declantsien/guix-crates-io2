(define-module (crates-io mo rt mortem) #:use-module (crates-io))

(define-public crate-mortem-0.1.0 (c (n "mortem") (v "0.1.0") (h "1nr9900hlgq3j2d8361ig3zafkg36ph5rjz6fszym37c6y2df548")))

(define-public crate-mortem-0.2.0 (c (n "mortem") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1zvb49xni7yh8rlk1xh5w2c0jc9yfcwfp1gs2fww1d099y5jvwys")))

(define-public crate-mortem-0.2.1 (c (n "mortem") (v "0.2.1") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("fmt"))) (d #t) (k 2)))) (h "1pdj8zvv2c64ihkbsshmwcb25qfx7jvvbabq92sklw2h50p4fgga") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-mortem-0.3.0 (c (n "mortem") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("fmt"))) (d #t) (k 2)))) (h "1bl59ys8iy93pia7cwwpyd1711whlhxbpp6akl74x3s6365w2yyq") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

