(define-module (crates-io mo rt morton) #:use-module (crates-io))

(define-public crate-morton-0.1.0 (c (n "morton") (v "0.1.0") (h "0iirfvjrnrhwzy3z9wyr0fyvs365x1kl5lcp7wfw15g40mjrgi13")))

(define-public crate-morton-0.1.1 (c (n "morton") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "08bik7vyindpvlcvy4288llh7yn6q71584spzlmjw1dnwig144xn")))

(define-public crate-morton-0.1.2 (c (n "morton") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0i68pfrwbhqs4ba08fy53awh1vcp2bfzylq0mgbm56nxdjl65w1b")))

(define-public crate-morton-0.2.0 (c (n "morton") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1wma1kvqg95ry86im40zjxvy4d4ahkgcx55pzq1rp41381j4nnl9")))

(define-public crate-morton-0.3.0 (c (n "morton") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "170q9j0dmgl6pm1frgdi156s8jdpbyz029p7l3h89nx2k55cvlxi")))

