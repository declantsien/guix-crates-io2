(define-module (crates-io mo rt mortar_distance) #:use-module (crates-io))

(define-public crate-mortar_distance-0.1.0 (c (n "mortar_distance") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4.2") (d #t) (k 0)))) (h "1zfylx46wh8w6vdv9i2fhyzfq1aj8ais9ddzx5lx1f6dqqpgldr7")))

