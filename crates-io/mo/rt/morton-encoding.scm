(define-module (crates-io mo rt morton-encoding) #:use-module (crates-io))

(define-public crate-morton-encoding-1.0.0 (c (n "morton-encoding") (v "1.0.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jhlf9gi62ihbglxby2h9p4cv2pvq7p6yrwqsj7vdsm1g4zvvdxs")))

(define-public crate-morton-encoding-1.0.1 (c (n "morton-encoding") (v "1.0.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "19f8iyywqlv7pyg6fff9kp4dhg8pmbhnpi92qgp9hzagyzxhgdj9") (f (quote (("std") ("no_std") ("default" "std"))))))

(define-public crate-morton-encoding-2.0.0 (c (n "morton-encoding") (v "2.0.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ayzapp7nh0l0snig9kzch67xcvfg24yrb17zc0jv30la6i20zxf") (f (quote (("std") ("default" "std"))))))

(define-public crate-morton-encoding-2.0.1 (c (n "morton-encoding") (v "2.0.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1j1ng4idqjds3nspx4b58qhhslkknkij302rljccsy55j8yrav7n") (f (quote (("std") ("default" "std"))))))

