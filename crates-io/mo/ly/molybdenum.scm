(define-module (crates-io mo ly molybdenum) #:use-module (crates-io))

(define-public crate-molybdenum-0.1.0 (c (n "molybdenum") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "19wnnha66m3975irdv1x3xs4i2mk2pmji94pw4lh0612nw8hxvjm")))

(define-public crate-molybdenum-0.1.1 (c (n "molybdenum") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "11q0i9wk3s06lg2mm1b4rgajnlj2xz7ml5b77w9nhyx26ais1vlw")))

(define-public crate-molybdenum-0.1.2 (c (n "molybdenum") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1wxcai9rqdc8w7k382gfkc22wyn46hazjbcj1c4z3midifka323p")))

(define-public crate-molybdenum-0.1.3 (c (n "molybdenum") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0grcnvsgs597v75b2l8f3s9aw5ag16li81hafl88gfl32mayaxqd")))

(define-public crate-molybdenum-0.1.4 (c (n "molybdenum") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "01ikgf8mr1y8ar1v2djh1y13bznr3iq82m1l90s1xl5cwx1n6d29")))

(define-public crate-molybdenum-0.1.5 (c (n "molybdenum") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1x812cmrgnp7li0qd8ggpbz66sjdclv2n9c3mw7ddyig1dla0fgp")))

(define-public crate-molybdenum-0.1.6 (c (n "molybdenum") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0s40gb7r6g0zyjpfi39jh47ih708zl2w60nxy3r0d1a35h0j6msa")))

(define-public crate-molybdenum-0.1.7 (c (n "molybdenum") (v "0.1.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0r4ilzrdq0kg802ijdc08n76j8vvg1wafjx3c6g8kkpr943m5in0")))

(define-public crate-molybdenum-0.1.8 (c (n "molybdenum") (v "0.1.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "037276yr7rhal4nvf87y0kny18iiibp4acmn9rxsq83q240kpbyc")))

(define-public crate-molybdenum-0.1.9 (c (n "molybdenum") (v "0.1.9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0jhs3aizqxaa7vfgb10k7xsw5mylzmjcag5g41f2n7lyr8n6b5nk")))

