(define-module (crates-io mo or moore-neighborhood) #:use-module (crates-io))

(define-public crate-moore-neighborhood-0.1.0 (c (n "moore-neighborhood") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0hziffwz0s8ydvl7ghjr1v33rsi2j0fkycqqyl19njwqygswwg6f")))

(define-public crate-moore-neighborhood-0.2.0 (c (n "moore-neighborhood") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "1m6n44jaqz69d61xxypailbkl2hzkbypdmbscg62g557nmfxc565") (f (quote (("std") ("default" "std"))))))

