(define-module (crates-io mo or moore-vhdl-syntax) #:use-module (crates-io))

(define-public crate-moore-vhdl-syntax-0.1.0 (c (n "moore-vhdl-syntax") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1qip154g9968rffqvn9z8q57xr7dc723x0ijbpb29qikj8v8r3am")))

(define-public crate-moore-vhdl-syntax-0.2.0 (c (n "moore-vhdl-syntax") (v "0.2.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "043jdk9rdfwd702ld14rb74ip5vshnsln03zx0a5199mnkba4hns")))

(define-public crate-moore-vhdl-syntax-0.5.0 (c (n "moore-vhdl-syntax") (v "0.5.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1sq19c4ki4k3m5imbc74q5lv44zy3z3wakk5sqn8cyhc536ha843")))

(define-public crate-moore-vhdl-syntax-0.6.0 (c (n "moore-vhdl-syntax") (v "0.6.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1zl8ga9qg60lqrxa1m5sbd4qkm616cjdkl872kawifjxmqr352br")))

(define-public crate-moore-vhdl-syntax-0.7.0 (c (n "moore-vhdl-syntax") (v "0.7.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "0ripkx2qnk5n8djvn8p0nb6yi40wakz4y0jwa7fbp79rzw9268k0")))

(define-public crate-moore-vhdl-syntax-0.8.0 (c (n "moore-vhdl-syntax") (v "0.8.0") (d (list (d (n "moore-common") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0jrds9p7zycs7kgf8kfgpmnx8x0s1p65spcfs9fq97320kngz2nx")))

(define-public crate-moore-vhdl-syntax-0.9.0 (c (n "moore-vhdl-syntax") (v "0.9.0") (d (list (d (n "moore-common") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1mf7ifbiwyb4hqy5vx1lfwz1rb94r8nx0g868gc9b533l1lhly53")))

(define-public crate-moore-vhdl-syntax-0.10.0 (c (n "moore-vhdl-syntax") (v "0.10.0") (d (list (d (n "moore-common") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "10v1y7x9pchb4g4kgacg5rjvg73z6kshfy9ddny8k4160xrkgdw2")))

(define-public crate-moore-vhdl-syntax-0.11.0 (c (n "moore-vhdl-syntax") (v "0.11.0") (d (list (d (n "moore-common") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "086dawhc34xx1ri6z908w4zim5cahlh3nlvpqzxiqpr5rz7gxnjp")))

(define-public crate-moore-vhdl-syntax-0.12.0 (c (n "moore-vhdl-syntax") (v "0.12.0") (d (list (d (n "moore-common") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "14qlk3z3vkcy8ajvhjahikyi4s3cdw8f7l0k54r2xkiyr3yb144w")))

(define-public crate-moore-vhdl-syntax-0.12.1 (c (n "moore-vhdl-syntax") (v "0.12.1") (d (list (d (n "moore-common") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "125zbq92acj4wyh1sn5q7m3v3alrc8zvrc8bcilwyskz3mx4adry")))

(define-public crate-moore-vhdl-syntax-0.13.0 (c (n "moore-vhdl-syntax") (v "0.13.0") (d (list (d (n "moore-common") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1czsj492rkzm3zfzdiyg90pf6hrk7vvbw7m9bxqavxsrx079vkzr")))

(define-public crate-moore-vhdl-syntax-0.13.1 (c (n "moore-vhdl-syntax") (v "0.13.1") (d (list (d (n "moore-common") (r "^0.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1nnswv70685pl7ch552mxvfnn6xmxz0kw1mw28wykajh0pb747vz")))

(define-public crate-moore-vhdl-syntax-0.13.2 (c (n "moore-vhdl-syntax") (v "0.13.2") (d (list (d (n "moore-common") (r "^0.13.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0cvhdhfia29fm4075lffkdsq12vwx97fign3412mg9zk08ygf2dc")))

(define-public crate-moore-vhdl-syntax-0.14.0 (c (n "moore-vhdl-syntax") (v "0.14.0") (d (list (d (n "moore-common") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0d5xhhsah78sli37ih8alx082y39hs8iijz9w9h30nj7vb8sg9hp")))

