(define-module (crates-io mo or moore-svlog-syntax) #:use-module (crates-io))

(define-public crate-moore-svlog-syntax-0.1.0 (c (n "moore-svlog-syntax") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1c22ajvni1zpzxqmri8qyh92jh7msa9skfwly66b5g9pci4slrwr")))

(define-public crate-moore-svlog-syntax-0.2.0 (c (n "moore-svlog-syntax") (v "0.2.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1d4gs5k9arlc533mm7jb3nz70a315x3zaqsichh4jm42xcw29g9z")))

(define-public crate-moore-svlog-syntax-0.5.0 (c (n "moore-svlog-syntax") (v "0.5.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1g2xzywcsdam40src1rz626x870lk9v9khm4pmzgzhc3k3ikafwq")))

(define-public crate-moore-svlog-syntax-0.6.0 (c (n "moore-svlog-syntax") (v "0.6.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "moore-common") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "0r8935d2akdqcr0bxgn66kjnjl08gkfsdk715xgfph74l1f47zkh")))

(define-public crate-moore-svlog-syntax-0.7.0 (c (n "moore-svlog-syntax") (v "0.7.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0hiq4i6ch48vgpsf63vhzsi9qfrfbh8rhvxl10xwiyw5vd4f022f")))

(define-public crate-moore-svlog-syntax-0.8.0 (c (n "moore-svlog-syntax") (v "0.8.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.8.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0cs2pj5nm8ynd8vgjpxdjrx3gxp13c3g13rj2v9pfxmn0ainszip")))

(define-public crate-moore-svlog-syntax-0.9.0 (c (n "moore-svlog-syntax") (v "0.9.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.9.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1yxakllqpv9phq3n8msd24gl7pcshxzfhk2d8d27wb35gxyvz881")))

(define-public crate-moore-svlog-syntax-0.10.0 (c (n "moore-svlog-syntax") (v "0.10.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.10.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1v7x6rd28f8lfr0cm4izss4ga8rifdfx1v47hl6icfz93d743yj1")))

(define-public crate-moore-svlog-syntax-0.11.0 (c (n "moore-svlog-syntax") (v "0.11.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.11.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1l48ynf3d0b6bvzl39nlljif20y6z91j7lxh5h2vp76i0n2sd8ap")))

(define-public crate-moore-svlog-syntax-0.12.0 (c (n "moore-svlog-syntax") (v "0.12.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.12.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1h1nfqjwajc065r8lvd1h7aaf8vak3lnw3pmjlwqgm0jc2q98mls")))

(define-public crate-moore-svlog-syntax-0.12.1 (c (n "moore-svlog-syntax") (v "0.12.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.12.1") (d #t) (k 0)) (d (n "moore-derive") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "08ipdn4d0dy2xs6pssz81hhm1p19f3l4lnxjbi3cw2bq2m0hcw67")))

(define-public crate-moore-svlog-syntax-0.13.0 (c (n "moore-svlog-syntax") (v "0.13.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.13.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0qiidranhm1iy557avak7jy3bm1ja94y2rxdqd43dnsg7nj3n44p")))

(define-public crate-moore-svlog-syntax-0.13.1 (c (n "moore-svlog-syntax") (v "0.13.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.13.1") (d #t) (k 0)) (d (n "moore-derive") (r "^0.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0sgnw13gx9vn74ivhrbjafprd573agh19xm70cwkk24b7jnbbhvf")))

(define-public crate-moore-svlog-syntax-0.13.2 (c (n "moore-svlog-syntax") (v "0.13.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.13.2") (d #t) (k 0)) (d (n "moore-derive") (r "^0.13.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0v9cbrrgvjlpav63hid80359da8ndp9jwdrinfbci4ki3a2lxlz6")))

(define-public crate-moore-svlog-syntax-0.14.0 (c (n "moore-svlog-syntax") (v "0.14.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "moore-common") (r "^0.14.0") (d #t) (k 0)) (d (n "moore-derive") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0943vy70yg0yk1yl4is8zvq8fm4nvrj9p55wdgbknb04i12pqjgw")))

