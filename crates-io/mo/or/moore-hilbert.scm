(define-module (crates-io mo or moore-hilbert) #:use-module (crates-io))

(define-public crate-moore-hilbert-0.1.0 (c (n "moore-hilbert") (v "0.1.0") (d (list (d (n "moore-hilbert-sys") (r "^0.1.0") (d #t) (k 0)))) (h "13hgcg88mn1aj3h6bh9hs4h6vb5yb6p5wh0nqmrka6px5fg1rzcf")))

(define-public crate-moore-hilbert-0.1.1 (c (n "moore-hilbert") (v "0.1.1") (d (list (d (n "moore-hilbert-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0fs8r7dnk9aszx1m6lmi8p1wh8sw78hbv4ihckxddkzq2g5yd94i")))

