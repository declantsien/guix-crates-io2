(define-module (crates-io mo or moore-hilbert-sys) #:use-module (crates-io))

(define-public crate-moore-hilbert-sys-0.1.0 (c (n "moore-hilbert-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gkm3mb433j8zgij97p4fxal0s6jn4f4hg14xdscgdx09pfs3q8i")))

