(define-module (crates-io mo or moore-derive) #:use-module (crates-io))

(define-public crate-moore-derive-0.8.0 (c (n "moore-derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0y4zsigrs249di6yf2l2981j6anc848vc5slply1amnrsx09zgyy")))

(define-public crate-moore-derive-0.9.0 (c (n "moore-derive") (v "0.9.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0x6vahhv41ffi8vd62hg2vvidlfqw1qhym3d63hfbxw6qv659d9z")))

(define-public crate-moore-derive-0.10.0 (c (n "moore-derive") (v "0.10.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0s34qw81vj0vnhb425dnq32z07in084skjh7xav3gpngbqv9n1v3")))

(define-public crate-moore-derive-0.11.0 (c (n "moore-derive") (v "0.11.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "18kpydi6x0i9vmp4hgcqh3crlf3azd98lpqv63rv08ydir1wv168")))

(define-public crate-moore-derive-0.12.0 (c (n "moore-derive") (v "0.12.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0786gcjx5c0jb94xwgmm6lykdb33payrwzizh02kv9g38mi4p9xh")))

(define-public crate-moore-derive-0.12.1 (c (n "moore-derive") (v "0.12.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1qi3fpdzw6v9q58b2ghg18w0dn898xz39irh592w01ay5787pg4h")))

(define-public crate-moore-derive-0.13.0 (c (n "moore-derive") (v "0.13.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1p9x8j55gra3sgswhf69d7dcnhipk8ddzkadg09wpcwnn7p68zmf")))

(define-public crate-moore-derive-0.13.1 (c (n "moore-derive") (v "0.13.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1gflhjlvmpv6fqs4mf08jw8f27sgw57nf1mga5hr6xjfigl7grdd")))

(define-public crate-moore-derive-0.13.2 (c (n "moore-derive") (v "0.13.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1qmq7k4ijpismajnxjmhj2rrrgdffk2glwq4ch9w5zgdicxx0b7k")))

(define-public crate-moore-derive-0.14.0 (c (n "moore-derive") (v "0.14.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1hsgrspzmcbwwi7l0ri9077i26x4cc4ga22kw0yjjiykw1c6c8jl")))

