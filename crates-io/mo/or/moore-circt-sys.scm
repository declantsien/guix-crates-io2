(define-module (crates-io mo or moore-circt-sys) #:use-module (crates-io))

(define-public crate-moore-circt-sys-0.14.0 (c (n "moore-circt-sys") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w3jadjlbxq568pp750jyd0s02wkwj960ycj2pcpdk7scb9zmx8i")))

