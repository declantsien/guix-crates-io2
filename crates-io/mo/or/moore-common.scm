(define-module (crates-io mo or moore-common) #:use-module (crates-io))

(define-public crate-moore-common-0.1.0 (c (n "moore-common") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1irlvz3pn0mnfpbiamrk4s192g4iprawhwjlg9xca0rdzqgbpf8l")))

(define-public crate-moore-common-0.2.0 (c (n "moore-common") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "1dn0334bvjqvbkq9ysnb8sl5lbavpb7mk3yn692xcj8z60q98wr3")))

(define-public crate-moore-common-0.5.0 (c (n "moore-common") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "12vm7z1hj6wbd1lmazz6l09f0iwzajbc8020r5v18hg910hqny0x")))

(define-public crate-moore-common-0.6.0 (c (n "moore-common") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "0mjbch91pva5m9aqmq605qkh374ck4rdnz3l6l7ngxi6y5v0qklj")))

(define-public crate-moore-common-0.7.0 (c (n "moore-common") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "1drbyd0pfqm4nlqshmc9dp3wcjbdnvx8268mjbcy91r4bxkfjnv1")))

(define-public crate-moore-common-0.8.0 (c (n "moore-common") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0v6dacgph60s1qd1yng7rv7i53iaagd1v4i1xzj2b6hsm7662aky")))

(define-public crate-moore-common-0.9.0 (c (n "moore-common") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1vah93rccgvildpc5ll399yrnjpwia5hjhi8k7p47wh8k6dhmdrm")))

(define-public crate-moore-common-0.10.0 (c (n "moore-common") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0w5v2p1khhlp4r8zlyaqnpq9qrl9wk2gk0l1zdvbaj16w19wbvi7")))

(define-public crate-moore-common-0.11.0 (c (n "moore-common") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1n4ia96yxgybwrvvfh433fx8bi9izjkfgd77b0ph1cydp6vykfzm")))

(define-public crate-moore-common-0.12.0 (c (n "moore-common") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1apxyfqy8gbnjjyxqf76h6xjhggl0cn7ja3cvfdx9422bybnfwha")))

(define-public crate-moore-common-0.12.1 (c (n "moore-common") (v "0.12.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1cdfn1pysyryhs8k9i0i803q1lf0hnsd4cm62bmfvpvzmr1whgjm")))

(define-public crate-moore-common-0.13.0 (c (n "moore-common") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0d6d6i4vj26jwyjc8m1b1k5gbg7pr7b6mjpprh25xs8pmhikqgcr")))

(define-public crate-moore-common-0.13.1 (c (n "moore-common") (v "0.13.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0s45g2lpfal7g4qc6zx2h4nbg9w4gpy8mizhbrqvnkjqa374a1vm")))

(define-public crate-moore-common-0.13.2 (c (n "moore-common") (v "0.13.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1p9flg55rjnmpgf5sva4qglcbavbapj496dzx0g1xlb3sg4svwq1")))

(define-public crate-moore-common-0.14.0 (c (n "moore-common") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1lmrsrmfzvskx0nmnss6wajb4xcg3l0qx7jsiis7b7clc3daaixw")))

