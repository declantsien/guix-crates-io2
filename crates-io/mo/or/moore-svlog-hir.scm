(define-module (crates-io mo or moore-svlog-hir) #:use-module (crates-io))

(define-public crate-moore-svlog-hir-0.1.0 (c (n "moore-svlog-hir") (v "0.1.0") (d (list (d (n "moore-common") (r "^0.1") (d #t) (k 0)) (d (n "moore-svlog-syntax") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2.0.12") (d #t) (k 0)))) (h "1102b55yz081hs04c8iskgjq6iw67pl40zg02kn1i834carm1pq8")))

