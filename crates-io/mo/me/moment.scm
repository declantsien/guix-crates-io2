(define-module (crates-io mo me moment) #:use-module (crates-io))

(define-public crate-moment-0.1.0 (c (n "moment") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "00fhni8zdgcp3kh5wqgj531n7m2dzyxzkd6kw7br0nrz84dpxpvf")))

