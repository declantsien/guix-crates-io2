(define-module (crates-io mo me momen) #:use-module (crates-io))

(define-public crate-momen-0.1.0 (c (n "momen") (v "0.1.0") (d (list (d (n "atomic-wait") (r "^1.0.1") (d #t) (k 0)) (d (n "clone_all") (r "^0.1.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "06w9g6bqvz9s8fqsvpdpyzxyxkfixgkcsqpwzc46qha0gicvgwa7")))

(define-public crate-momen-0.1.1 (c (n "momen") (v "0.1.1") (d (list (d (n "atomic-wait") (r "^1.0.1") (d #t) (k 0)) (d (n "clone_all") (r "^0.1.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "1kq8mxjdf95h5050gvsbysdla8mqabsslbhmy63b8vp91hk0vg7b")))

(define-public crate-momen-0.2.0 (c (n "momen") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "atomic-wait") (r "^1.0.1") (d #t) (k 0)) (d (n "clone_all") (r "^0.1.1") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "0c452cljy55mbiji2xykq4rhgy27q37b0rbhbhncahnjh1145nir")))

