(define-module (crates-io mo me momento-protos) #:use-module (crates-io))

(define-public crate-momento-protos-0.0.1 (c (n "momento-protos") (v "0.0.1") (d (list (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "111442m2ngzxxp11q1q5kkf43i61b9wi342s2zandvjfan6f44cp")))

(define-public crate-momento-protos-0.0.2 (c (n "momento-protos") (v "0.0.2") (d (list (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "0bki2lciy9pyzpw7685skhx72cb5gs631zww2zdd8njx4yj60sbr")))

(define-public crate-momento-protos-0.0.3 (c (n "momento-protos") (v "0.0.3") (d (list (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "00fs3klmys4q19xld50afsxc2vwd54j8ciw7wcpw51px5p6v1lnl")))

(define-public crate-momento-protos-0.17.2 (c (n "momento-protos") (v "0.17.2") (d (list (d (n "protobuf") (r "^3") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "1c2jhvdcy1hxk1pwcrwd4np9xqqqjvpzscn4zwgmzaz7zfcwj7df")))

(define-public crate-momento-protos-0.17.3 (c (n "momento-protos") (v "0.17.3") (d (list (d (n "protobuf") (r "^3") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "1952nzsb0dzq3bpms49dy83wjqhsgnxf5v9kj9mfr4f5864m7pqv")))

(define-public crate-momento-protos-0.18.0 (c (n "momento-protos") (v "0.18.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "19icrmv48k7a4j75d37px63w1aqr3p0np8ibgs5ka87bj5hyap77")))

(define-public crate-momento-protos-0.18.1 (c (n "momento-protos") (v "0.18.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1lpvana3gh1bxh2pk14jp32q7ph97bkaqyqadlaabbb6k39ssva2")))

(define-public crate-momento-protos-0.19.0 (c (n "momento-protos") (v "0.19.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1cq6jssfbxn737p4v5j11lhiph02k0l7h3xys8b5n2a3ykqyvmsx")))

(define-public crate-momento-protos-0.20.0 (c (n "momento-protos") (v "0.20.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1j1i1d1fhjn26ypcy00d7gbapmyk58h1isy9ynjnsq4ilgd8wafw")))

(define-public crate-momento-protos-0.20.1 (c (n "momento-protos") (v "0.20.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0iy5h20hxqf2h0ff9bp5bazb125kbscmk3j36bm8g7p1iahc7l5i")))

(define-public crate-momento-protos-0.21.0 (c (n "momento-protos") (v "0.21.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1bdbmifa1cag5c35jfkp80ixv7qv7sg38gmlikklv1jbrl3ly7sy")))

(define-public crate-momento-protos-0.22.0 (c (n "momento-protos") (v "0.22.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0xjzq8dsxx5iqpwmj4bv073zg5r3kn73risbiggg2kklavc6hwd8")))

(define-public crate-momento-protos-0.23.0 (c (n "momento-protos") (v "0.23.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0hqqzsqxhnhvq4j209wksgzvs0dm5hdmsjhq3g9nzr8qk46z4crg")))

(define-public crate-momento-protos-0.23.1 (c (n "momento-protos") (v "0.23.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0daam9vp5yklfh5r9sgyspj4sbadapcxpgss0nxg26cmm5w3k44q")))

(define-public crate-momento-protos-0.23.2 (c (n "momento-protos") (v "0.23.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "05dnddi2mzmykc13sn9pa0gqn1y1lnzxgyalrrvmrc43a3yilx19")))

(define-public crate-momento-protos-0.24.0 (c (n "momento-protos") (v "0.24.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0l1k5m8y7fvpx3a0aqgwynfc018zvbzmlnb79qb4ymn3va0a8hz1")))

(define-public crate-momento-protos-0.25.0 (c (n "momento-protos") (v "0.25.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0lnwzmhnybx7j425bwaq0dxsip0k2q2jxixy728qj94dh654xpf4")))

(define-public crate-momento-protos-0.26.0 (c (n "momento-protos") (v "0.26.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "17aw9yxcaqmd9qi82zslgqj6gm2brrgbbvw70jhs3wrxm2p4121c")))

(define-public crate-momento-protos-0.27.0 (c (n "momento-protos") (v "0.27.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "17dz6w8j8fjqi7v7anpk5p8zf59zyzif784rnxyk2dykr5alwrxv")))

(define-public crate-momento-protos-0.27.1 (c (n "momento-protos") (v "0.27.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0ir1wggpk14xndlcygshymjgxlrlvlicyzmakpv8r13csxywr0hp")))

(define-public crate-momento-protos-0.27.2 (c (n "momento-protos") (v "0.27.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "18vxbjkdcjrqcqv062v22xl3p14j5cnvzvf675jj8hp2vi4vhc7c")))

(define-public crate-momento-protos-0.28.0 (c (n "momento-protos") (v "0.28.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "01pfm9s87vl61m5xy06pb4nc6wsw067k3c02kvkjmq7hjspfl81w")))

(define-public crate-momento-protos-0.28.1 (c (n "momento-protos") (v "0.28.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1549v185blqkss893jkab9350rv7ipgjmfzwj8xfjjb8lnbqgzz7")))

(define-public crate-momento-protos-0.28.2 (c (n "momento-protos") (v "0.28.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0ccvbsjdg68cd2npaxhanla65yhw8gcjwv2gv42lqhrc7fv40di2")))

(define-public crate-momento-protos-0.28.3 (c (n "momento-protos") (v "0.28.3") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0zs01796xdlz4yicaki3a59r534f51c2c3z2nxswhwp1mhifgl9b")))

(define-public crate-momento-protos-0.28.4 (c (n "momento-protos") (v "0.28.4") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0q07hgbpw2z3ivqq945djnhdlgww93bk39kq9pvxh58vbfn8p5zi")))

(define-public crate-momento-protos-0.29.0 (c (n "momento-protos") (v "0.29.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1vafvmnjf839dfzqg93i9zqbr01x83qbmwds87nhqv24vwdxpjar")))

(define-public crate-momento-protos-0.29.1 (c (n "momento-protos") (v "0.29.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0c8zpprkvdv4qikyxrqqqa3m3r6lqm60hjahlg76j8sphv0jp0sg")))

(define-public crate-momento-protos-0.30.0 (c (n "momento-protos") (v "0.30.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1czmsayqfb134kvcxm1i6m6lyj2rlrviasan5mybd52nlbsv2n30")))

(define-public crate-momento-protos-0.30.1 (c (n "momento-protos") (v "0.30.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0s83dk6c584gfa86s70vmw3jy8qmf1066xh8qn5agqj8lm5csaz1")))

(define-public crate-momento-protos-0.30.2 (c (n "momento-protos") (v "0.30.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0pazcay2n547h3avxna8l53xyx05sjqlg265gpcxgc4llfy3dk8p")))

(define-public crate-momento-protos-0.30.3 (c (n "momento-protos") (v "0.30.3") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "19rqrx8m8vv7l5dmsr6iipb7p93ylw5bjkrwpii71l9wgq93qhvb")))

(define-public crate-momento-protos-0.31.0 (c (n "momento-protos") (v "0.31.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0qy3ycp0jsihr5p2g62nxf3f3z38kkx0v7kiwv3pm4wv6fgnkb0p")))

(define-public crate-momento-protos-0.31.1 (c (n "momento-protos") (v "0.31.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0pyjwzr1srsywdfhjbjl3yscwnbzljwbq2h8b02ljwzl3ikhvzyh")))

(define-public crate-momento-protos-0.32.0 (c (n "momento-protos") (v "0.32.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1yy964na1p1lg90vynv4n77sbxgfn17n8y4zp0vh7hyglwfwcpyb")))

(define-public crate-momento-protos-0.32.1 (c (n "momento-protos") (v "0.32.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1pp48v80k38x9d24ja2j07gkp23nrvbpb0bix8iri5jjmvbrsqnv")))

(define-public crate-momento-protos-0.33.0 (c (n "momento-protos") (v "0.33.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0idcdiyqsgq26bzf7c2wsk9bfrm3w46xf5wp651qpypl579lpf7r")))

(define-public crate-momento-protos-0.34.0 (c (n "momento-protos") (v "0.34.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1mka65h22nfa9dgimmfh4x64ink54rr51ppx4mnwm0sy9l39xvyh")))

(define-public crate-momento-protos-0.35.0 (c (n "momento-protos") (v "0.35.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0p9jrzhqap2cwkj6dskal5j670c3b1i0pd9md8624hirifpw68wa")))

(define-public crate-momento-protos-0.36.0 (c (n "momento-protos") (v "0.36.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0mcaaq2ckgzww13rilqm76ls616k0f8vbq3r91njfxld6i8znxy5")))

(define-public crate-momento-protos-0.37.0 (c (n "momento-protos") (v "0.37.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0hvs5xl432v3l05hnlz2rxr18yqyxcaf6fjkfk6pknqiyp87gl7l")))

(define-public crate-momento-protos-0.38.0 (c (n "momento-protos") (v "0.38.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1czfamfxfdga266q68bfljg41dn5lbrws079lyb3jz3m9jd1sr09")))

(define-public crate-momento-protos-0.39.0 (c (n "momento-protos") (v "0.39.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0hcplrz18ipabbhg5fd58jm3d665zd97xzvr5isg4j0fi6nil0cb")))

(define-public crate-momento-protos-0.40.0 (c (n "momento-protos") (v "0.40.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "16f88s0brk4wnzvwhh5rn7vqm7asdkq1cdffxami7krxyjnpyr72")))

(define-public crate-momento-protos-0.40.1 (c (n "momento-protos") (v "0.40.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1yqs6vzvslfw1kjrdqkvahmrmpv0dwm8w0vzpa05ajjpi8wg33px")))

(define-public crate-momento-protos-0.41.0 (c (n "momento-protos") (v "0.41.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1gr147xyx0hg0y24j9kb6qzpm6rg5k0mfyrkxldc3m76pzwxk5fz")))

(define-public crate-momento-protos-0.41.1 (c (n "momento-protos") (v "0.41.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1il6ib0cmjj400m3wcmpd0bwq9j5vibdnfkcgvyl1gnmy1786c06")))

(define-public crate-momento-protos-0.42.0 (c (n "momento-protos") (v "0.42.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0yb1g02hi5gpyf2a93v23fky9hg9r67apwg9ig4vn88qj4c2r18r")))

(define-public crate-momento-protos-0.42.1 (c (n "momento-protos") (v "0.42.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0nr914c23abpd53a2j1dv6vdf3mf9z0gwz38vwng6b1nmhh55ald")))

(define-public crate-momento-protos-0.42.2 (c (n "momento-protos") (v "0.42.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0r4vng0qlvsirnzv4mrz503brs4ajwx6fn3vg12a83gsc7ahyl8a")))

(define-public crate-momento-protos-0.42.3 (c (n "momento-protos") (v "0.42.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "180xh4ixl4nac7m6373ks62lhjylwr0ggl9dkp6vha9fvj9hh2cn")))

(define-public crate-momento-protos-0.42.4 (c (n "momento-protos") (v "0.42.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1j5cgcanrnq12010iwrv5p0rv2q8qdqv6c6r0bc9njw5619agc37")))

(define-public crate-momento-protos-0.42.5 (c (n "momento-protos") (v "0.42.5") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1656vz2iwdpfqn7cwfrhrwdbalczaznlxgrsw948ick07y94ypgp")))

(define-public crate-momento-protos-0.43.0 (c (n "momento-protos") (v "0.43.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0n2x7ykxnq20hf4cyh4lh1vmik5v99sxs1xl3z8gzfm14y3gsdfc")))

(define-public crate-momento-protos-0.44.0 (c (n "momento-protos") (v "0.44.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0a56lgd96afwyvj4mh837nwmy2v8ib0v3pkyfvzay35j6qmfzfn1")))

(define-public crate-momento-protos-0.44.1 (c (n "momento-protos") (v "0.44.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0044jyxwz4wg6pry6c4vxndxjybm900fs0skd8m474hd5rhi7a6x")))

(define-public crate-momento-protos-0.44.2 (c (n "momento-protos") (v "0.44.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1igvc7zqpl459459rvdin7wfxf8cliq12hyap1f6yiaks8ifcdch")))

(define-public crate-momento-protos-0.45.0 (c (n "momento-protos") (v "0.45.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1irprq5dk582n879c9xdm9a7z819zwv672013f6qrfvqsdwl3ia1")))

(define-public crate-momento-protos-0.46.0 (c (n "momento-protos") (v "0.46.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "103cgvfyl2xz5wa8a3wwydj2vx92vyx5cyjphssiij76g26mclf4")))

(define-public crate-momento-protos-0.47.0 (c (n "momento-protos") (v "0.47.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1ki16dwzczjxwd6wmx19wadpjvawwsw4bhfgck8l04d2r1byq6x4")))

(define-public crate-momento-protos-0.47.1 (c (n "momento-protos") (v "0.47.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "00pncfxg27hyvv2lcfl6jcnknndf32xmp2wks17v1h8lcjxk41xx")))

(define-public crate-momento-protos-0.48.0 (c (n "momento-protos") (v "0.48.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "15wlzxkffk5j556icd8y57nahsx9yzim64325295f2w53ll7hsid")))

(define-public crate-momento-protos-0.49.0 (c (n "momento-protos") (v "0.49.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1699q04gcyfrmnxxr8a14fns2ny8n687i1fn0jiczfs2m3gncikj")))

(define-public crate-momento-protos-0.49.1 (c (n "momento-protos") (v "0.49.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0rjppkdpkzdzyj4djphyy2r23da8h2dkd138s2wnl967gwijg1q1")))

(define-public crate-momento-protos-0.49.2 (c (n "momento-protos") (v "0.49.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1pxjxjyva320hwc1hbibj0fydpnx6s1649qjw27g04q0s0pqa0fs")))

(define-public crate-momento-protos-0.50.0 (c (n "momento-protos") (v "0.50.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1y98zpa2cyj5mgc5y3ph23da0f9ily83y7x4zrrfxd9ks4n0slwj")))

(define-public crate-momento-protos-0.51.0 (c (n "momento-protos") (v "0.51.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1dqlpa8bcj872n644zkhbmzdkrhw74ha7m68rds6rax83cjlpg7x")))

(define-public crate-momento-protos-0.51.1 (c (n "momento-protos") (v "0.51.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "13rxj8vzm6zkrxxslxj9wqd6brnmip0x2n73i06lha3p87bix5cf")))

(define-public crate-momento-protos-0.52.0 (c (n "momento-protos") (v "0.52.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "08v5avzg0vklqdzg0nrrg4wvaljdwa5qqjn6s2jqi9cq29nxzg2l")))

(define-public crate-momento-protos-0.53.0 (c (n "momento-protos") (v "0.53.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1q27cx8njjb4vvw9knw8sgpnaa1yvxywr11j0bkc90ldqqv3mrgh")))

(define-public crate-momento-protos-0.53.1 (c (n "momento-protos") (v "0.53.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1jmy0870l9pq693d7m54x63bggcx7yqkfky2413rbas51nh9s52f")))

(define-public crate-momento-protos-0.54.0 (c (n "momento-protos") (v "0.54.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "17hyar87mkgpnhl9wxpkbi54q5dfz736wx874yfi9dri6d95kqbc")))

(define-public crate-momento-protos-0.53.2 (c (n "momento-protos") (v "0.53.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0br6k7dr0xlpqy6p43s309kylq4scyysmk1mggsnwf6l55lm1gqp")))

(define-public crate-momento-protos-0.54.1 (c (n "momento-protos") (v "0.54.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0w4yjkk8kidxs1g6qcfdzjg90ds16q4dghiz4gz12dlwj3fz1qwl")))

(define-public crate-momento-protos-0.54.2 (c (n "momento-protos") (v "0.54.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0gxl157l5grxbxlhmsswpp61mqa68gng836i4q8j8n040qajdmm4")))

(define-public crate-momento-protos-0.54.3 (c (n "momento-protos") (v "0.54.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1197dff651mcjxml0d77na4pp9rgsr7a8ykmhzx357r9gkcv919a")))

(define-public crate-momento-protos-0.55.0 (c (n "momento-protos") (v "0.55.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1zhhaak592xfjjpd2qssh0sy50bg800m4w61ys1r7mnmgm8v8yd7")))

(define-public crate-momento-protos-0.56.0 (c (n "momento-protos") (v "0.56.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "13v1jpj54c9yrriwszw1j5w6cjk8a0z0qjl27dszp92139cs5nzs")))

(define-public crate-momento-protos-0.57.0 (c (n "momento-protos") (v "0.57.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1caz370kkswcg00gikj6khl9ql58iynd6iwhli0kdajyxkhrh7i2")))

(define-public crate-momento-protos-0.58.0 (c (n "momento-protos") (v "0.58.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1jkpv6416hlgcgvbxvcaw9rrdjim9hsrl6cc86njy6y77ann82zy")))

(define-public crate-momento-protos-0.59.0 (c (n "momento-protos") (v "0.59.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0wydqgkl3x2x8j36m1fx15fjlc6iaqshv57dza530nmsz7gisgzc")))

(define-public crate-momento-protos-0.60.0 (c (n "momento-protos") (v "0.60.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1laxi5zljzqrq5sdb839pgzaaqll18x2pa34bv7sh1pm2gc8szaa")))

(define-public crate-momento-protos-0.60.1 (c (n "momento-protos") (v "0.60.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1rd4k8mppwdn4ah0g84km3zzvhmlx65zn0lgcz1d8p9dsiyi1x15")))

(define-public crate-momento-protos-0.60.2 (c (n "momento-protos") (v "0.60.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1l08p656p3mmbcl7r5iwwqakb48ar69l0msa4ircjz3gbv7kchnz")))

(define-public crate-momento-protos-0.60.3 (c (n "momento-protos") (v "0.60.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "11l80bc36fm8gm4jhgbh08filk1f1cv2if8q4vjdw7iqrwlv8l32")))

(define-public crate-momento-protos-0.60.4 (c (n "momento-protos") (v "0.60.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0d1s2hjc48yx3f41dip5f1n1689qvzdszi5g5adycjad3b7d4dbg")))

(define-public crate-momento-protos-0.61.0 (c (n "momento-protos") (v "0.61.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1a0k13chwal5yvz7z5dwqj7kfv2j6hbb7rnd19ha3qqfmgpawd8g")))

(define-public crate-momento-protos-0.61.1 (c (n "momento-protos") (v "0.61.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1rn645mcy74nfvdll2xnbwgmvvjhqvg1kywrrxk6dr9wmvywcfkd")))

(define-public crate-momento-protos-0.61.2 (c (n "momento-protos") (v "0.61.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1b2ibmsgq74p1q1iy6ibkk950061giyd1in7c5qgx81hl1hdsvzm")))

(define-public crate-momento-protos-0.62.0 (c (n "momento-protos") (v "0.62.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0cvrw27qrvanfbnq4gggi6bwq9199hpydc2xj3ml58lm3dalmvki")))

(define-public crate-momento-protos-0.62.1 (c (n "momento-protos") (v "0.62.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "07kg16jvc5n3bsnaf40pywi4pb1ldvmvdsd2dpsci4sb39ddwsqd")))

(define-public crate-momento-protos-0.62.2 (c (n "momento-protos") (v "0.62.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0h1370cah62l2r1fgfcsb1arwv18w3aw2yvkvv23xzqh7plmsa07")))

(define-public crate-momento-protos-0.62.3 (c (n "momento-protos") (v "0.62.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1dy16dwv08gpv5xpvllkdi11njbkxkf08khlpy87d3nxc8rb65wf")))

(define-public crate-momento-protos-0.63.0 (c (n "momento-protos") (v "0.63.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "03cj6yqvyi6wipmm3n1haimyjvq6qryhr9xwh8hmzjk46ya9v52h")))

(define-public crate-momento-protos-0.64.0 (c (n "momento-protos") (v "0.64.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "08dly4dkym17k03ym129ivjjqmixqfbm874fs6q72a5gbgrbjaa6")))

(define-public crate-momento-protos-0.64.1 (c (n "momento-protos") (v "0.64.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "09yw79hh58rw4h45b3vj9sm686ym3fk1jy3550nm2p42z3pss8jm")))

(define-public crate-momento-protos-0.65.0 (c (n "momento-protos") (v "0.65.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0jslml6zdmj84l86ynab759nz9z95kwkwyjm8r62yv2v5ygk22k1")))

(define-public crate-momento-protos-0.65.1 (c (n "momento-protos") (v "0.65.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0i2bh286qlfbkbmdp0x527nbi7aiia7hjwg2jz2bj7zbk3kd1bxz") (y #t)))

(define-public crate-momento-protos-0.65.4 (c (n "momento-protos") (v "0.65.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0h3di7wifzihakk4fgniysvdlq5v0gh56a1fxynb2804ayalk72r")))

(define-public crate-momento-protos-0.65.5 (c (n "momento-protos") (v "0.65.5") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "12cnvw6x36w4p7awwins5lm8g91ws55zldb1qsaap1aqqxihv43k")))

(define-public crate-momento-protos-0.66.0 (c (n "momento-protos") (v "0.66.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1rjcf5asacbaq44m7qvv05n2n26dngxhq406bq92r1a51ry8mrvk")))

(define-public crate-momento-protos-0.67.0 (c (n "momento-protos") (v "0.67.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1va57y14jg2rs11dzjxsyyc3vkp2z1sc7q4dgb9aaqcinnflnpgi")))

(define-public crate-momento-protos-0.68.0 (c (n "momento-protos") (v "0.68.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1zywrpn02phyjz7wqhycm7rajvfknsjg9hbq2521rqgz2cfd5jr0")))

(define-public crate-momento-protos-0.68.1 (c (n "momento-protos") (v "0.68.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1in888wj5zr7l4sg9hkch5fikwigjk1ii5vz5mcr7h1h2fwfjj3l")))

(define-public crate-momento-protos-0.69.0 (c (n "momento-protos") (v "0.69.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1spk5r0mscgb42pl2dxnx95iija9kf0d5g3cdly1zdgcy03dm8y5")))

(define-public crate-momento-protos-0.69.1 (c (n "momento-protos") (v "0.69.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0z66kd5lrgbpgix6pckpy3sp1w26m0w3hhvfxp1w876b32b4a5xi")))

(define-public crate-momento-protos-0.70.0 (c (n "momento-protos") (v "0.70.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0b5al1d59n1qrn493alx0ffk474w3xm3fbnwanvml8pkyn55nyhx")))

(define-public crate-momento-protos-0.71.0 (c (n "momento-protos") (v "0.71.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1pzfbm0q5r276i7xjq098r5z5q6krw84s9ibkyvq12mwvhkb9n01")))

(define-public crate-momento-protos-0.71.1 (c (n "momento-protos") (v "0.71.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1x4x5hfxwf9f8257jabbrhpbi1xp2sfca84hclkdb215nfw6nsgr")))

(define-public crate-momento-protos-0.72.0 (c (n "momento-protos") (v "0.72.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0s6ja8jvnzwnq159l5dhvrzcqrc199srpkvfqk9q1mf6dqin2pkz")))

(define-public crate-momento-protos-0.72.1 (c (n "momento-protos") (v "0.72.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "11z9ylm5skyvy20w0slgdfvfmsvpz1v5ds5ygqk5nni053q2yzfv")))

(define-public crate-momento-protos-0.72.2 (c (n "momento-protos") (v "0.72.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0z03f73gf18j58mpd5rcfmbdi1qb754caf42lgwp8xamh7am1wrf")))

(define-public crate-momento-protos-0.73.0 (c (n "momento-protos") (v "0.73.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "18sis2kifqp8zaschp100ldjak8drwrhs33nfx3zwd4rqm5ijfiw")))

(define-public crate-momento-protos-0.74.0 (c (n "momento-protos") (v "0.74.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0cfcz9x1z4fc6is1v4a770s0ni79z92lhk4agagd34hqznsp6v3c")))

(define-public crate-momento-protos-0.75.0 (c (n "momento-protos") (v "0.75.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0a1b4a35jw1vbfcl7hf1waxfr8is716xgaihzvc242fzsrsbcqpk")))

(define-public crate-momento-protos-0.76.0 (c (n "momento-protos") (v "0.76.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "136bwaqq9bgdxjbsgyx64s3r87dslx621a23gv9ya68ws5k7zk17")))

(define-public crate-momento-protos-0.76.1 (c (n "momento-protos") (v "0.76.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0zbs59nan7bjrmww9gb8y2wakmkzipfvpl0n7s1nm8fwr3x95y33")))

(define-public crate-momento-protos-0.76.2 (c (n "momento-protos") (v "0.76.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0wzfxx9m3hmj77jmvaghqgh6w3bn06p41wa96w3whg8rkvgg7n7l")))

(define-public crate-momento-protos-0.77.0 (c (n "momento-protos") (v "0.77.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0czyykjp1nd6ccbpf1m06h4sjmw7vfswnwd9cjwib4mxak2lzwmy")))

(define-public crate-momento-protos-0.78.0 (c (n "momento-protos") (v "0.78.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1dd18ag6jbkn3rx3ik04wrms08b5mvnzgrhxwmz6dq85z3213nq8")))

(define-public crate-momento-protos-0.78.1 (c (n "momento-protos") (v "0.78.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1dn6can2w8vqfbhgblykfn2pgc3gpzmkzqjrlphj2q2qp5h6rjmz")))

(define-public crate-momento-protos-0.78.2 (c (n "momento-protos") (v "0.78.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0d6gcxwic93zby7nna8qim179rxmw8sschzxd7asqr9j445kpgkl")))

(define-public crate-momento-protos-0.79.0 (c (n "momento-protos") (v "0.79.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "17wz6adirq9ka4mid6x4i7fvs8n7qi45frr0rfn5kxn95nz78g5r")))

(define-public crate-momento-protos-0.80.0 (c (n "momento-protos") (v "0.80.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0isij40j8hx9v5755fmx8fc98yg6mlpv029wz5jdfkz3r7pq7526")))

(define-public crate-momento-protos-0.80.1 (c (n "momento-protos") (v "0.80.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "07sicdz6nyrfm2akb7f4c4783mf62vqyckbyyfh8c7aqsirgnvv3")))

(define-public crate-momento-protos-0.80.2 (c (n "momento-protos") (v "0.80.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1p6x8ivl7cj2izb8ll696rja1ldydv9l3dsvhcqx9ajy0i5021y8")))

(define-public crate-momento-protos-0.81.0 (c (n "momento-protos") (v "0.81.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0an1aa36gx9q57jb6pniks476lyb5zajibsy5fj272vgxdphlcyk")))

(define-public crate-momento-protos-0.81.1 (c (n "momento-protos") (v "0.81.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "06dp83s1rr9911y3fg812ap9nnhim8xz45zavwr2qp9pg5pm7d52")))

(define-public crate-momento-protos-0.81.2 (c (n "momento-protos") (v "0.81.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0mm91hbfja5zigzkmp32l3m04h2br00j9q70r81sb76kwzsfr6vd")))

(define-public crate-momento-protos-0.82.0 (c (n "momento-protos") (v "0.82.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "15jzklyhprd4dp9xc0ygmv1qbxbxmnir7sam4zjyhw281r7wc9ns")))

(define-public crate-momento-protos-0.82.1 (c (n "momento-protos") (v "0.82.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "00zm66pp5j4hl5hsp220av33sw6g0kgd2ibpx62jyqmp92klp51h")))

(define-public crate-momento-protos-0.82.2 (c (n "momento-protos") (v "0.82.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1riymb2r3ifdq16s6qa34rlckzly53yk77n6cb5i6l4j6b0isjmm")))

(define-public crate-momento-protos-0.82.3 (c (n "momento-protos") (v "0.82.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1czq01ik1m0bq2260682kvdgfw19ixiy8pq9wbzgwh29j0qpfcqj")))

(define-public crate-momento-protos-0.82.4 (c (n "momento-protos") (v "0.82.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1r9hn30i3pzx7mnpq746lghaa4csi1hskkl7mvw113lskxsh1pk5")))

(define-public crate-momento-protos-0.82.5 (c (n "momento-protos") (v "0.82.5") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "19fqar12c1dhg2jiwp74qfx662akfhhf8bi3vkzsdrzqbvpca3wr")))

(define-public crate-momento-protos-0.83.0 (c (n "momento-protos") (v "0.83.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1h0z0jifryqfq7g94lrshp64bhgsmmxvrqyyphaahw58yydayw8d")))

(define-public crate-momento-protos-0.84.0 (c (n "momento-protos") (v "0.84.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0r4xprbmqgwjpjfn451mx6qnd94mrflrwyk9xkl472jdjkg23pbk")))

(define-public crate-momento-protos-0.84.1 (c (n "momento-protos") (v "0.84.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "08ria62w1ljf8zwdjrgrrah0qgnnkd30246dxz3vh6qa8vbkgn35")))

(define-public crate-momento-protos-0.84.2 (c (n "momento-protos") (v "0.84.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1g8ay2l8645nq0lq21ln13lww7q9sq5nr96n16mvhk6vxr92m8s5")))

(define-public crate-momento-protos-0.85.0 (c (n "momento-protos") (v "0.85.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "027kk12fr2mb0fncydivs7qczzjppbff2j2wvf5y81k7bhm7k7yj")))

(define-public crate-momento-protos-0.86.0 (c (n "momento-protos") (v "0.86.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1dwv5gvlz711wq0gl3pmszphhmg8dpxy8ix3y5vdsi7kfyvxzavk")))

(define-public crate-momento-protos-0.87.0 (c (n "momento-protos") (v "0.87.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0wylxqrgnk9m25swyijw0kfd0v56hwkqaad1dgsz67zz830ric2v")))

(define-public crate-momento-protos-0.88.0 (c (n "momento-protos") (v "0.88.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0iyr7c58svhx0da20iapzri9p1vci1hrqhpgb884hn2jxfnj7d3c")))

(define-public crate-momento-protos-0.88.1 (c (n "momento-protos") (v "0.88.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0jc41x02sq7hhnjpn1p6pa8kk61dkgp7fya1mcjdjmnqsivrr47g")))

(define-public crate-momento-protos-0.89.0 (c (n "momento-protos") (v "0.89.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0sxs41b1qqgxpgyi42swi5jbjwp5swnxqfk5ymprka20zf8xkqh7")))

(define-public crate-momento-protos-0.90.0 (c (n "momento-protos") (v "0.90.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1c78bvjfvds7gxvzz8d57dhg5aslab7c528cdwsw750ja2lz87bf")))

(define-public crate-momento-protos-0.91.0 (c (n "momento-protos") (v "0.91.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "012s43g6w76pdv9gbr612rgjhd2dplkm82nk3b465wyyj4s6cy90")))

(define-public crate-momento-protos-0.91.1 (c (n "momento-protos") (v "0.91.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0hnqgk632191m38dic8rp13vsmh4x5hzjd11b7g6gibp9hqiddx9")))

(define-public crate-momento-protos-0.91.2 (c (n "momento-protos") (v "0.91.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "09vk2l7qdkvd0nrcw9lv7yddanjl3fdz3syss2scbn2jb5n0ysqp")))

(define-public crate-momento-protos-0.91.3 (c (n "momento-protos") (v "0.91.3") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0q4fgjzhpv0y8va1lvsd2fpp0230s8g2f3gagxabdnnxjj7jf0m6")))

(define-public crate-momento-protos-0.91.4 (c (n "momento-protos") (v "0.91.4") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0xlq45sdaqhgcrxw7dq1s9ml3n3q8mm714g9bq0smx4mwwps29vi")))

(define-public crate-momento-protos-0.92.0 (c (n "momento-protos") (v "0.92.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "18wjvw00a4siz9fgz7pqg14x4x0j87p22vpx5v359q9cx295f5yr")))

(define-public crate-momento-protos-0.93.0 (c (n "momento-protos") (v "0.93.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0f22y4dl5jrldnzk9qpvb0awqx20vq61kr5gmnfdzg4jkjam70ky")))

(define-public crate-momento-protos-0.94.0 (c (n "momento-protos") (v "0.94.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0d3xigr4grlk5xazqz6f9q7frjpy730ggxghaigwgwxa76y6lh0x")))

(define-public crate-momento-protos-0.94.1 (c (n "momento-protos") (v "0.94.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0gifp5hc7f2y419mvf4mvk9iap97z81hi54cx7s1qx7c4xyz3dcw")))

(define-public crate-momento-protos-0.95.0 (c (n "momento-protos") (v "0.95.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1642d3g85r6flsn3mnhcv97i8x5a9p11xhplnanszv5gfs8z4dga")))

(define-public crate-momento-protos-0.96.0 (c (n "momento-protos") (v "0.96.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "09wns21kx4fmlr73pqz8hb2jld219f0jiqlzy0485d53ymbjz17b")))

(define-public crate-momento-protos-0.97.0 (c (n "momento-protos") (v "0.97.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1ylr0snx2yzqvvsl01kinjdk2b232gz76f4akhgmr640ngwxs60r")))

(define-public crate-momento-protos-0.97.1 (c (n "momento-protos") (v "0.97.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1143w6yg6b9qjvxgrc2dx3780m7va474dyhfz6q2f170cg99xyp0")))

(define-public crate-momento-protos-0.98.0 (c (n "momento-protos") (v "0.98.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "01yvbp39mrm435i5696aigy1121i024f2wkqg1d12imw9iinyql0")))

(define-public crate-momento-protos-0.98.1 (c (n "momento-protos") (v "0.98.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0ga3pni0krp3rvms0vv4hl8p1swzlr7y42lwdp4q7pak2cq8w99q")))

(define-public crate-momento-protos-0.99.0 (c (n "momento-protos") (v "0.99.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0n707j77vlnf7pkpzgvkq8s59y1m0616p8641l0y9dx5g51w272p")))

(define-public crate-momento-protos-0.100.0 (c (n "momento-protos") (v "0.100.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1pkwy13pyg9pqnmlk7qsrfisp3ngbpdz884ncqjmq1549x1wih53")))

(define-public crate-momento-protos-0.100.1 (c (n "momento-protos") (v "0.100.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0y5hsx85c9ni526ahvhv8jqqsnsiyp4ljnsc08wnqdniwzryg4zy")))

(define-public crate-momento-protos-0.101.0 (c (n "momento-protos") (v "0.101.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "07ary5h716hz5rvbzi0ghphmdwhfascwzxx3krnmxjb2i1i7h6q9")))

(define-public crate-momento-protos-0.102.0 (c (n "momento-protos") (v "0.102.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1n36qijchhl6fra7q7y7pwlm964xy53a28578ip7nj6ffdnjkpq6")))

(define-public crate-momento-protos-0.102.1 (c (n "momento-protos") (v "0.102.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0yz053xa6yb1pgxjw119w1nf9b2r1xm3j2sh6qjhrshz3pjlsf7f")))

(define-public crate-momento-protos-0.102.2 (c (n "momento-protos") (v "0.102.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0vv8b4l5bcbz84g6xl1acbv818hxrah733dhpp6gf5583sbdvsl9")))

(define-public crate-momento-protos-0.103.0 (c (n "momento-protos") (v "0.103.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1f6f64b17jg8cnkjb96h2as4q91w7qw0rl7sc5l8pgm4vayw9z6h")))

(define-public crate-momento-protos-0.104.0 (c (n "momento-protos") (v "0.104.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "00gqlxqbqx2fphkrwlykl1p4v5b7an8511x8r6b1x2is45yzdgxi")))

(define-public crate-momento-protos-0.105.0 (c (n "momento-protos") (v "0.105.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1aikj6x17r85s2yb553affy7hs8kq6r9hjnq8aajbzz94v7cxd7n")))

(define-public crate-momento-protos-0.105.1 (c (n "momento-protos") (v "0.105.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1ha7mpf36x7jrmwz8cm5cg1x008qvvhmwx8pp4layy8fi6f5j9rx")))

(define-public crate-momento-protos-0.105.2 (c (n "momento-protos") (v "0.105.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1094s2gdsm86vb6hib5279d4vsdrnzhwl4zaxp3sqcvjc36ld47a")))

(define-public crate-momento-protos-0.105.3 (c (n "momento-protos") (v "0.105.3") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "16lwn3bpxbfjv8swqsqsic97z4vfzp4li7qdnvhb542ly5zmsz6g")))

(define-public crate-momento-protos-0.106.0 (c (n "momento-protos") (v "0.106.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1wvi9iqimfckrj0c02nv1sidr574x5ablvhh4dxxxmfjgwd7zylb")))

(define-public crate-momento-protos-0.106.1 (c (n "momento-protos") (v "0.106.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1h3l9ffigmk0p1l048j3j1xxdasxp7gmvigwajkfqvbv7dv603c9")))

(define-public crate-momento-protos-0.106.2 (c (n "momento-protos") (v "0.106.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "18xw1z9s83h9mgxw61jq6f2qyl1x6ckv3k1kary9hdvy02j7jjpq")))

(define-public crate-momento-protos-0.107.0 (c (n "momento-protos") (v "0.107.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0m40lz14csd37x14svcvk9fqvs8msb1r8kwnq2gxilv32b15lp0i")))

(define-public crate-momento-protos-0.107.1 (c (n "momento-protos") (v "0.107.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "15fkffvb098b1qpy9iy4nw6i06kj7pzabzwmmcra3yw96biqqvm7")))

(define-public crate-momento-protos-0.107.2 (c (n "momento-protos") (v "0.107.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1gi8dkc2aqfykb7cms27k9gwzc0ff5iw10ds2n9am3x7bvh4f3wy")))

(define-public crate-momento-protos-0.107.3 (c (n "momento-protos") (v "0.107.3") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0ipl19y4nzbfdd4ny5fr99gf4aw0mpjx2hg6x5djlbzaaqqwvi1h")))

(define-public crate-momento-protos-0.107.4 (c (n "momento-protos") (v "0.107.4") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "11x5wqdjg5q0v2wd3m2j8l48b2sc8ji98pyghn0c4azbg3zj259c")))

(define-public crate-momento-protos-0.108.0 (c (n "momento-protos") (v "0.108.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0n8gpdciyd3k9b5s5dlfbfr3iiv73ir0hbdrb6333zjkcjpyq52a")))

(define-public crate-momento-protos-0.108.1 (c (n "momento-protos") (v "0.108.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1a5hnl51n5wq6m9s8m6k1ykb81payrdmk1xlgll0kbkhqdxvzgjc")))

(define-public crate-momento-protos-0.108.2 (c (n "momento-protos") (v "0.108.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0clgwm2vkchl1lacgfqgxgkr27gmjk2cd6c10s8iy0ipy05yzwls")))

(define-public crate-momento-protos-0.109.0 (c (n "momento-protos") (v "0.109.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0isg9dqyz62z40sy5jh2q8x9n7wv6wkd163pnpwz8p3ik5j3i4bi")))

(define-public crate-momento-protos-0.110.0 (c (n "momento-protos") (v "0.110.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1fr88six233ybfknc4ifc9p3izx9m4b55vfxfbmdahlb1x18vcl0")))

(define-public crate-momento-protos-0.110.1 (c (n "momento-protos") (v "0.110.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0p5aaj13bbpvq71lg2iscin9x29flsd0zp97vwr6k77lq0x5iv73")))

(define-public crate-momento-protos-0.110.2 (c (n "momento-protos") (v "0.110.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1mya3p3rpf4c2cb5rpp9dyha4c196n5yrvqlmddn7f95j4za0v0l")))

