(define-module (crates-io mo me moment-rs) #:use-module (crates-io))

(define-public crate-moment-rs-0.1.0 (c (n "moment-rs") (v "0.1.0") (h "07vaaf9ah7hpcqxjbhvxjd8ycbnrvm6kyciwvwb88a31a3x0fj0a")))

(define-public crate-moment-rs-0.1.1 (c (n "moment-rs") (v "0.1.1") (h "0baajdgn08ajmhpjqrim1zrhncplm1vgnvdrgy0lq65mnvx5ki5y")))

