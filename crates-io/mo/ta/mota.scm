(define-module (crates-io mo ta mota) #:use-module (crates-io))

(define-public crate-mota-0.0.1 (c (n "mota") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("compat" "thread-pool"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j3xpab2jd2al9fvyyffs73psc6r9chz80vi1nsfccclgbphkr30")))

