(define-module (crates-io mo ri mori_parallel) #:use-module (crates-io))

(define-public crate-mori_parallel-0.1.0 (c (n "mori_parallel") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.8") (d #t) (k 0)))) (h "14z95qdpmfc1jpqz9lfwg3dfi1ysdlp73gixhg4ckjqzqxxqzrsq")))

(define-public crate-mori_parallel-0.1.1 (c (n "mori_parallel") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.8") (d #t) (k 0)))) (h "0k0brwb9qygl644j41rdl6b22m9565pqsd3kywqcm64s7sgn63j5")))

(define-public crate-mori_parallel-0.1.2 (c (n "mori_parallel") (v "0.1.2") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.8") (d #t) (k 0)))) (h "098kksxpgy2qsvm8k9i1i3mn5nkiyzgpx2sh054ivv2i1c87cxa6")))

(define-public crate-mori_parallel-0.1.3 (c (n "mori_parallel") (v "0.1.3") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.8") (d #t) (k 0)))) (h "18943nrrgqmwws1f46ip9q93scimhb3x8yg42abwlrnl90b31nn9")))

