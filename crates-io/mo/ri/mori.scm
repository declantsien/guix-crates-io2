(define-module (crates-io mo ri mori) #:use-module (crates-io))

(define-public crate-mori-0.1.0 (c (n "mori") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)))) (h "0bgv1f5plzy0xgnc5l6i3radjammnf79ca6baw9197glsakzl86v")))

(define-public crate-mori-0.1.1 (c (n "mori") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)))) (h "0jgbyv6cvj90srhg3jlzcxac1b7nzysf0bcqlqj9ycjxapcjkr8x")))

(define-public crate-mori-0.1.2 (c (n "mori") (v "0.1.2") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)))) (h "0dacvm1iqwv9zsnazv4pi77nrj6z2qac0zarx2lqc7vd8xv607k7")))

(define-public crate-mori-0.2.0 (c (n "mori") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.0") (f (quote ("approx"))) (d #t) (k 0)))) (h "0a33ysi6dvfppk5cqh0v7lpvxj54v730m8ryfnk8cyn3cjbj4y9d") (f (quote (("parallel" "ndarray/rayon"))))))

