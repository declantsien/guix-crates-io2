(define-module (crates-io mo s- mos-alloc) #:use-module (crates-io))

(define-public crate-mos-alloc-0.1.0-beta.1 (c (n "mos-alloc") (v "0.1.0-beta.1") (d (list (d (n "ufmt") (r "^0") (d #t) (k 2)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)))) (h "1cj21n9c1lm5g0xzgbwsbrv0d8i43wsp15hnn5w6rv9lrbrxacmc")))

(define-public crate-mos-alloc-0.1.0-beta.2 (c (n "mos-alloc") (v "0.1.0-beta.2") (d (list (d (n "ufmt") (r "^0") (d #t) (k 2)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)))) (h "1n87s74nwrsv3krgsapdv5mgkzj2pqlcm44hd9plrh6fr2nk1z4x")))

(define-public crate-mos-alloc-0.1.0 (c (n "mos-alloc") (v "0.1.0") (d (list (d (n "ufmt") (r "^0") (d #t) (k 2)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)))) (h "0a0ml0czmhfjwgb5ip8zmr9gdsvkhxnnnbvfnazm53xkqby3a4s4")))

(define-public crate-mos-alloc-0.2.0 (c (n "mos-alloc") (v "0.2.0") (d (list (d (n "ufmt") (r "^0") (d #t) (k 2)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)))) (h "11v3vfgjk1dlqc6v4s6nhnzjsfv3x5m84irxpgwp6923axx96fd0")))

