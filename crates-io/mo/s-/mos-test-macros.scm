(define-module (crates-io mo s- mos-test-macros) #:use-module (crates-io))

(define-public crate-mos-test-macros-0.0.1 (c (n "mos-test-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "00k60yckxr7g6c7cy182j9c8rr61m17pvy7hny6ihwz3wa0azyl8")))

