(define-module (crates-io mo ho moho) #:use-module (crates-io))

(define-public crate-moho-0.0.1 (c (n "moho") (v "0.0.1") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)) (d (n "serde") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_yaml") (r ">= 0.6") (d #t) (k 0)))) (h "1anr5c8s001kcskq02hjai2c2nwdk7ddg5i6ncbk68x0zrzgfmx7")))

(define-public crate-moho-0.0.2 (c (n "moho") (v "0.0.2") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)) (d (n "serde") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_yaml") (r ">= 0.6") (d #t) (k 0)))) (h "004kgygqyj0dfwjzs0phis5qj4n7n43pzb4r86jsa59x74sjw4wa")))

(define-public crate-moho-0.0.3 (c (n "moho") (v "0.0.3") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)) (d (n "serde") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_yaml") (r ">= 0.6") (d #t) (k 0)))) (h "1pb8ffsp458dwzgpz26xvmby3z3bsacl0hf8y06hy11aj0inji03")))

(define-public crate-moho-0.0.4 (c (n "moho") (v "0.0.4") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)) (d (n "serde") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_derive") (r ">= 0.9") (d #t) (k 0)) (d (n "serde_yaml") (r ">= 0.6") (d #t) (k 0)))) (h "0074nvlfk5xhrcn4p63g7a8h37yrvwby6050h88qgsx3mpmm3sk7")))

(define-public crate-moho-0.0.5 (c (n "moho") (v "0.0.5") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)))) (h "15m12akb5ijjmsnz1612x47rvghf254z47wa2az1jkzl8yw0vmv9")))

(define-public crate-moho-0.0.6 (c (n "moho") (v "0.0.6") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image"))) (k 0)))) (h "1mra8rf0ly7k1da3gajyhlkwkwmnxk42q2pl7xxb59y6x74lmyfr")))

(define-public crate-moho-0.0.7 (c (n "moho") (v "0.0.7") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image" "ttf"))) (k 0)))) (h "016z1lswrj4pzy4fazd7rabgmj8qx7qwz4dsgk7qaj16c52igl8k")))

(define-public crate-moho-0.0.8 (c (n "moho") (v "0.0.8") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image" "ttf"))) (k 0)))) (h "0a03xjhr5vzq61qabwfgp2vjncah3kln152cwjy9l75prgh6asz0")))

(define-public crate-moho-0.0.9 (c (n "moho") (v "0.0.9") (d (list (d (n "error-chain") (r ">= 0.9") (d #t) (k 0)) (d (n "glm") (r ">= 0.2.2") (d #t) (k 0)) (d (n "num-traits") (r ">= 0.1.36") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.29") (f (quote ("image" "ttf"))) (k 0)))) (h "0icf9zvd0bigh1x462kv7prqkl5nv8p5hql1f46cdm940gyjvv61")))

