(define-module (crates-io mo bo mobot-derive) #:use-module (crates-io))

(define-public crate-mobot-derive-0.1.0 (c (n "mobot-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0a7il0alw13mvfd9051dcns6c01f17aizlq3gmljif84ldx9i6jp")))

(define-public crate-mobot-derive-0.1.1 (c (n "mobot-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0yz5lpvhdlwpq1dmsb7f5falxmx7z83kv9q8cnlz895frm928ckv")))

(define-public crate-mobot-derive-0.1.2 (c (n "mobot-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1zi9wqfwzp5rfypfbwnj3w21czcjah4iwfdqrg99wdl753a0jy5v")))

(define-public crate-mobot-derive-0.1.3 (c (n "mobot-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0fdbmgly06zrhmclv6sndp8higcmylffxyc3yavp4cqgfs0g7d2m")))

