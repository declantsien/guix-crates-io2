(define-module (crates-io mo re more-iter) #:use-module (crates-io))

(define-public crate-more-iter-0.1.0 (c (n "more-iter") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "0wpnph1nla56zk2c3gg51iqx4389ai1hj5pm4823mfcciampspdc")))

(define-public crate-more-iter-0.1.1 (c (n "more-iter") (v "0.1.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1psw88snqkkp3kaj88ahngjyzh4ylbjh7jb1yg6di4vrfx6hrk69")))

(define-public crate-more-iter-0.1.2 (c (n "more-iter") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "10pd4bicmyhys3ygkf7cngm7hx9s2b759k5wi8pjmin7zy8143zn")))

