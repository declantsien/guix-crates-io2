(define-module (crates-io mo re more-di-macros) #:use-module (crates-io))

(define-public crate-more-di-macros-1.0.0 (c (n "more-di-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1kvk6ajak01kq08h9wnx9smlmd10g6ppfhzy4mh44s5sjq1p6yj4")))

(define-public crate-more-di-macros-1.0.1 (c (n "more-di-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1arr1ydbnvsh1fqkfbk2sn1jy7sz17h6hdbyncwincslqpk4wk8i")))

(define-public crate-more-di-macros-2.0.0 (c (n "more-di-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0d9vidr54x3r6wrmgfbis8ffj8wfq9ci1847h22vkmhfmi8ba8xx")))

(define-public crate-more-di-macros-2.1.0 (c (n "more-di-macros") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0m72kf57i2v5gkhhr22ah2p74mrwxwf76il62jmwgvmlj3m8fn6z")))

(define-public crate-more-di-macros-3.0.0 (c (n "more-di-macros") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1ajsmsjkn9hjjpyfd6281zv02hnj2dn6bpldi8an4dm4wpjrc3yr") (f (quote (("default") ("alias" "toml" "serde"))))))

(define-public crate-more-di-macros-3.1.0 (c (n "more-di-macros") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1qsc95v7jnkm9vgzqb0d2c8k0m6v9fmxzkk3nz26zjyx9kfxa12b") (f (quote (("default") ("alias" "toml" "serde"))))))

