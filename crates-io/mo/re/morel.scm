(define-module (crates-io mo re morel) #:use-module (crates-io))

(define-public crate-morel-0.1.0 (c (n "morel") (v "0.1.0") (h "05ig9z9mjk07j8qqmar03wm2v2dlxlihx18cavdln3s2x474vrrf")))

(define-public crate-morel-0.2.0 (c (n "morel") (v "0.2.0") (h "0jjn2rf0pglhch43p80nhikgl90qh1nlmdvv98sarvxpn959smyc")))

(define-public crate-morel-0.3.0 (c (n "morel") (v "0.3.0") (h "16n0d12m979fpcgiha5g225r8g6gja6byrwn44mhv9hg1w8lxh9c")))

