(define-module (crates-io mo re more-itertools) #:use-module (crates-io))

(define-public crate-more-itertools-0.1.0 (c (n "more-itertools") (v "0.1.0") (h "01jhqxzb8xq6f7jf5h2f7mb096w5zl950zmnbixbbsab4ipn5633")))

(define-public crate-more-itertools-0.1.1 (c (n "more-itertools") (v "0.1.1") (h "0myspbgsmgpavbxqk8jwwvk73w9qcmailcmgp2v5jilh04znwhqs")))

(define-public crate-more-itertools-0.1.2 (c (n "more-itertools") (v "0.1.2") (h "1k5wp80kx4xgg120wj2zwc1pvdphcxqrccj0vq5ydpaihzvb4yy1")))

(define-public crate-more-itertools-0.1.3 (c (n "more-itertools") (v "0.1.3") (h "0r0fhvxbjcd5sbwvk8d8zw681qmhmcs21vp9ci7z4c2gqpfp5mij")))

(define-public crate-more-itertools-0.1.4 (c (n "more-itertools") (v "0.1.4") (h "00w0vjz6m23ksrr726kb5fy5v1faganw2b6afy5473lzp53j12yy")))

(define-public crate-more-itertools-0.1.5 (c (n "more-itertools") (v "0.1.5") (h "1mw3j4xckavlmkwrmrwbivy2m4ia8crcis9xjnafb4i0dvv5fnvz")))

(define-public crate-more-itertools-0.1.6 (c (n "more-itertools") (v "0.1.6") (h "13q0n8ra38fvgkrsipj16x35y0y5wn97fi1qra8nchgnskhbb2ka")))

