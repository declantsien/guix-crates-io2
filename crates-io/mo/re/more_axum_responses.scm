(define-module (crates-io mo re more_axum_responses) #:use-module (crates-io))

(define-public crate-more_axum_responses-0.1.0 (c (n "more_axum_responses") (v "0.1.0") (d (list (d (n "axum-core") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)))) (h "0ccrpkrmf1m354kn3xi8yf438a77szjpf6cpw5hbns4gz49cljvm")))

