(define-module (crates-io mo re more-syn-types) #:use-module (crates-io))

(define-public crate-more-syn-types-0.1.0 (c (n "more-syn-types") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qxpfsfvzq9s2mh0cbfa1qi952jwhms6b0x9ad9s5k1w2mr92l75") (f (quote (("extra-traits" "syn/extra-traits") ("executing") ("default" "quote" "extra-traits" "executing"))))))

