(define-module (crates-io mo re more-asserts) #:use-module (crates-io))

(define-public crate-more-asserts-0.1.0 (c (n "more-asserts") (v "0.1.0") (h "12cs7ywgryw7mpg35c5p93y0dgig5wa145dba0xqxm75hxpdj8dz")))

(define-public crate-more-asserts-0.2.0 (c (n "more-asserts") (v "0.2.0") (h "0dl3ilaf5r8dyg6gh457xkg8ns062h4skpqf4lwx9y7l537clszm")))

(define-public crate-more-asserts-0.2.1 (c (n "more-asserts") (v "0.2.1") (h "0f2jy8wcwhlr1679g5fr4wqz6hqnmcasir349nk3x0l8rygypsqd")))

(define-public crate-more-asserts-0.2.2 (c (n "more-asserts") (v "0.2.2") (h "12b3fypg2sr4qmxy6wiyx6k9sdg573f5ij98cdmbrg00whnyqhvq")))

(define-public crate-more-asserts-0.3.0 (c (n "more-asserts") (v "0.3.0") (h "04lpq4397rm39w6qayc4zqy0xlwwsfy9arfzx5d67q675mrzhx2l")))

(define-public crate-more-asserts-0.3.1 (c (n "more-asserts") (v "0.3.1") (h "0zj0f9z73nsn1zxk2y21f0mmafvz7dz5v93prlxwdndb3jbadbqz")))

