(define-module (crates-io mo re moretypes) #:use-module (crates-io))

(define-public crate-moretypes-0.1.0 (c (n "moretypes") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1lh93xkpf6n3jp646dgs4isdkl7zwc8pk0yndypf80k91fzmrk69")))

(define-public crate-moretypes-0.1.1 (c (n "moretypes") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0082r64kymi7gx3rz0ni83r26x443jcrw76gdhjh53972bqv0z3x")))

(define-public crate-moretypes-0.1.2 (c (n "moretypes") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0qpyfb491kk3lkd8qk8m2260sz3x6k94crixfizpw5k7592zdpj2")))

(define-public crate-moretypes-0.1.3 (c (n "moretypes") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1kx0aidnx6l6gg2bkjk1h5f4nxggc4f27xwf4hj2nhlm0xvkbjpf")))

