(define-module (crates-io mo re more-changetoken) #:use-module (crates-io))

(define-public crate-more-changetoken-1.0.0 (c (n "more-changetoken") (v "1.0.0") (h "1v0n9ggkgx6f7yygq8lm0jms66bfr0snnzv7szpdm9mcvddv0b1y")))

(define-public crate-more-changetoken-2.0.0 (c (n "more-changetoken") (v "2.0.0") (d (list (d (n "notify") (r "^6.1") (o #t) (d #t) (k 0)))) (h "0f7b9x8wxp0zkm4jzjlnsniwfrnzrl5cs3xp4wmbyb3zy1r8vgh9") (f (quote (("fs" "notify"))))))

