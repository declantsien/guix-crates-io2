(define-module (crates-io mo re more-fs) #:use-module (crates-io))

(define-public crate-more-fs-0.1.0 (c (n "more-fs") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1z604yci2wdd1pm160g65lpjfgl48v0k17cbfhbig0bapfx5rrcc") (y #t)))

(define-public crate-more-fs-0.2.0 (c (n "more-fs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "02cfis421f2z3anfp050a3gf4scq5l1kn0c1bcm1vwz1qzppvgyd") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-more-fs-0.2.1 (c (n "more-fs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "16jr5yidnn6n0lspkkpsh16saggjb6icaf4975my3ldrndjcqa21") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-more-fs-0.2.2 (c (n "more-fs") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0xwvbrlkvdzkfkbsa6kfa41sihg9sm306fmynhgrrxzlaqbs0gi9") (f (quote (("default" "rayon")))) (y #t)))

(define-public crate-more-fs-0.2.3 (c (n "more-fs") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1h5pk8lbgj5yka1z07raj10s1xcz620dhv2lm76rb7bpclw6gm1r") (f (quote (("default" "rayon")))) (y #t)))

