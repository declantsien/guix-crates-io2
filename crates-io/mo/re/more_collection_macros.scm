(define-module (crates-io mo re more_collection_macros) #:use-module (crates-io))

(define-public crate-more_collection_macros-0.1.0 (c (n "more_collection_macros") (v "0.1.0") (h "1vnlcligkld32fmcnqbfl4wrlf7wg23d10swgnjbpd7p1rmgh7dj")))

(define-public crate-more_collection_macros-0.2.0 (c (n "more_collection_macros") (v "0.2.0") (h "19hff5fwclsxbwlzw5267ig6cirw6p2nwvqllrf8mw3b68mkrf0z")))

(define-public crate-more_collection_macros-0.2.1 (c (n "more_collection_macros") (v "0.2.1") (h "0n0vb9hwdk0z8bh2hsa4ifzwgfj414jfh8ahgkdz8la45afhlrms")))

(define-public crate-more_collection_macros-0.2.2 (c (n "more_collection_macros") (v "0.2.2") (h "0b356q0pfryjrmkg29ai198p6qp9fw1kpq3l485ybfd4nkc4cdhh")))

