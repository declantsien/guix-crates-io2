(define-module (crates-io mo re more-cargo) #:use-module (crates-io))

(define-public crate-more-cargo-0.1.0 (c (n "more-cargo") (v "0.1.0") (h "11kgqswgr2nw4p57v1bigcxw0jlkvm6zfpggxn4g3npmk3z17kd4")))

(define-public crate-more-cargo-0.2.0 (c (n "more-cargo") (v "0.2.0") (h "1av6svr2g21xq2j9sld1mx8h2yajlyfbw4kx0rx73n335lqhmf4a")))

