(define-module (crates-io mo re more-sync) #:use-module (crates-io))

(define-public crate-more-sync-0.1.0 (c (n "more-sync") (v "0.1.0") (h "0d1jw93bxh7x42zxfgdyszlvcv76jahps1dgl7z4bjganlz36kfn")))

(define-public crate-more-sync-0.1.1 (c (n "more-sync") (v "0.1.1") (h "14qfj0ac174czsqgjg7qv5mhpfd22ak63rn2a608sa0hmzc8h98p")))

(define-public crate-more-sync-0.1.2 (c (n "more-sync") (v "0.1.2") (h "0rrndggx6s3wn8976aym2cqwsgr4bw0ac060p56j8a3cjya8jrq5")))

(define-public crate-more-sync-0.1.3 (c (n "more-sync") (v "0.1.3") (h "04lsw4lmjy2zdy3ggdsxvbfp0aid0lx7lbsp6j06dgc47nch3khh")))

(define-public crate-more-sync-0.1.4 (c (n "more-sync") (v "0.1.4") (h "14m2xphavv2dk5ssxrf36np47fbv9ifbvikci3i9a2wkkwq8n8r9")))

(define-public crate-more-sync-0.1.5 (c (n "more-sync") (v "0.1.5") (h "1s4r7k3884qpf3jrcliflwa642vhi7a55f4ff54j4sinxmp5yb8s")))

