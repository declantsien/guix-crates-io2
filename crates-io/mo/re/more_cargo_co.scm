(define-module (crates-io mo re more_cargo_co) #:use-module (crates-io))

(define-public crate-more_cargo_co-0.1.0 (c (n "more_cargo_co") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0l2wd9g9wcvz2d3gc3x703di2m35npffcw744xf5zq8d7jfyq0xm")))

(define-public crate-more_cargo_co-0.1.1 (c (n "more_cargo_co") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0xfi5cpfr8mhcgrqqfgchq54c330l5gaahhmgxbg2cc8zz5ynynw") (y #t)))

