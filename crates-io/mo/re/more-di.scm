(define-module (crates-io mo re more-di) #:use-module (crates-io))

(define-public crate-more-di-1.0.0 (c (n "more-di") (v "1.0.0") (d (list (d (n "more-di-macros") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "091qkdhx67b3r6w80rj18zwqkq1h6hp9avqlam8kdmbx5brlzsv4") (f (quote (("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-1.0.1 (c (n "more-di") (v "1.0.1") (d (list (d (n "more-di-macros") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "1bm3l1g5chyqy6zlncmfmqk5b5nvjr1wr71zjq3cw1v62v7qscgs") (f (quote (("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-2.0.0 (c (n "more-di") (v "2.0.0") (d (list (d (n "more-di-macros") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "1ad93qd1zlain0g936prwmdxywyi18jmng5bs5zri84davp6vxwy") (f (quote (("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-2.0.1 (c (n "more-di") (v "2.0.1") (d (list (d (n "more-di-macros") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "0nzmqan2hkw0ficzcrkqc8lpbdv6lg1y7vkxg75rcmzqnpcxv73i") (f (quote (("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-2.1.0 (c (n "more-di") (v "2.1.0") (d (list (d (n "more-di-macros") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "0fjxvcyz8wp6pfcp2qq66h5xx23841q1fc8k7hw3ri7qgf2fly5l") (f (quote (("lazy") ("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-2.1.1 (c (n "more-di") (v "2.1.1") (d (list (d (n "more-di-macros") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("once"))) (k 0)))) (h "0lwl724jjm7l1sprg4kgwpx8hym6y482s4hw9l3qqqc0vb70w5vb") (f (quote (("lazy") ("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-2.1.2 (c (n "more-di") (v "2.1.2") (d (list (d (n "more-di-macros") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("once"))) (k 0)))) (h "13pgd1zwz28f6hd44jcq5fpsfj29g5hcnm98vbw642dfkpabvcar") (f (quote (("lazy") ("inject" "more-di-macros") ("default" "builder" "inject") ("builder") ("async"))))))

(define-public crate-more-di-3.0.0 (c (n "more-di") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "more-di-macros") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("once"))) (k 0)))) (h "1qisn0kxpp02labqa20iwfqbzjs0n1n2qr8j0xlr9br0cakcyspv") (f (quote (("lazy") ("inject" "more-di-macros") ("fmt" "colored") ("default" "builder" "inject") ("builder") ("async") ("alias" "more-di-macros/alias"))))))

(define-public crate-more-di-3.1.0 (c (n "more-di") (v "3.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "more-di-macros") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("once"))) (k 0)))) (h "12gv5mgpq1z4g730sfras43fjqyrds8fd4biir5jq0d2249n2qzj") (f (quote (("lazy") ("inject" "more-di-macros") ("fmt" "colored") ("default" "builder" "inject") ("builder") ("async") ("alias" "more-di-macros/alias"))))))

