(define-module (crates-io mo c- moc-rs) #:use-module (crates-io))

(define-public crate-moc-rs-0.1.0 (c (n "moc-rs") (v "0.1.0") (h "13a4v88k7iilcihkz94g0j25qzswja5xy82qkr40q074s0y59chx")))

(define-public crate-moc-rs-0.1.1 (c (n "moc-rs") (v "0.1.1") (h "13kx1a1sf0rk547s875jlbhz8bz4zynhfbr2f8240mgcmc0zkkax")))

(define-public crate-moc-rs-0.1.2 (c (n "moc-rs") (v "0.1.2") (h "0y5g4wgaiji6f90al1cikf7d3qn9n508sq8q3xvjr60zsxwa5rgy")))

(define-public crate-moc-rs-0.1.3 (c (n "moc-rs") (v "0.1.3") (h "0i4q4yna3ahf6xbpvps16khfsk6ni5yrryd3ayp4f3ciqpbbx8jn")))

