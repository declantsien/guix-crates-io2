(define-module (crates-io mo s6 mos6502_assembler) #:use-module (crates-io))

(define-public crate-mos6502_assembler-0.1.0 (c (n "mos6502_assembler") (v "0.1.0") (d (list (d (n "mos6502_model") (r "^0.1") (d #t) (k 0)))) (h "0pk36lyxcrihcik4brs3apkjb9icqr11rb573627qw7ysy3nf9lw")))

(define-public crate-mos6502_assembler-0.2.0 (c (n "mos6502_assembler") (v "0.2.0") (d (list (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "1big9mjhmv8ahkgvm5wn667q2qk8yz8m05dpk9dhsc8780b7af86")))

(define-public crate-mos6502_assembler-0.3.0 (c (n "mos6502_assembler") (v "0.3.0") (d (list (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "0q3dcz3ds0qwb4i7s7h1fmiaydk18lbxqn7krsggfgmsw57zz7nc")))

