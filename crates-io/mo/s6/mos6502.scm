(define-module (crates-io mo s6 mos6502) #:use-module (crates-io))

(define-public crate-mos6502-0.0.1 (c (n "mos6502") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0vzr9f89dlx940zrxwvzc6l5vnqd52nx9qwxjak2zqxwv0l9r53l")))

(define-public crate-mos6502-0.2.0 (c (n "mos6502") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.3") (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0mkz2y9f8im6g7d15q14bd5xka6ajm7k48mx9zzb21azqnjfa05y")))

(define-public crate-mos6502-0.3.0 (c (n "mos6502") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "0vq1hg9xq993q88l2j084vg5x8c93zpsdkk0p1mxrbb44wvdc7dk")))

(define-public crate-mos6502-0.4.0 (c (n "mos6502") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "14bxpwvnrq439460w9dfplmy8p8bm26sc5ggdi7m3g1xvfjb55i7") (f (quote (("default" "decimal_mode") ("decimal_mode"))))))

(define-public crate-mos6502-0.5.0 (c (n "mos6502") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1wckxzm7xwah157f1jy90xbqxyah2qvjw4j1wvvfsamcdhjc7zxp") (f (quote (("default" "decimal_mode") ("decimal_mode"))))))

