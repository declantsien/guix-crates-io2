(define-module (crates-io mo s6 mos6502_model) #:use-module (crates-io))

(define-public crate-mos6502_model-0.1.0 (c (n "mos6502_model") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0bjzfja982sbbp08rk5jpy3s3nadvyh201h6f0pkr9s3dsw6wsx8") (f (quote (("serialize" "serde"))))))

(define-public crate-mos6502_model-0.2.0 (c (n "mos6502_model") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16647hw2alf6yg5z47hp3ahbadvy6176hrckyhpbjl3nqszw5369") (f (quote (("serialize" "serde"))))))

(define-public crate-mos6502_model-0.2.1 (c (n "mos6502_model") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "08xwmhbyssywdf4llaqg7vcx13mwxraadda3k096xbrp75l7ab0h") (f (quote (("serialize" "serde"))))))

