(define-module (crates-io mo lt molt-argparse-procmacro) #:use-module (crates-io))

(define-public crate-molt-argparse-procmacro-0.1.0 (c (n "molt-argparse-procmacro") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10kjlppf5zr7sakhv1n4m0dpb33xw7rcjwaq2sq7s04h6d5hlgg4")))

(define-public crate-molt-argparse-procmacro-0.1.1 (c (n "molt-argparse-procmacro") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gdx5clymhiaznqr2x0x3iwad46wl43m93f0b9nzrvg0zliq3pw2")))

