(define-module (crates-io mo lt moltenvk_deps) #:use-module (crates-io))

(define-public crate-moltenvk_deps-0.1.0 (c (n "moltenvk_deps") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1494m25bdsq0af699bc5x70f8jpn0b6chlrrdfiscfbd1xf9v05d")))

(define-public crate-moltenvk_deps-0.1.1 (c (n "moltenvk_deps") (v "0.1.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "02vyclk7lrmr5ymv5z79zqcgjr116bslbk01bccnilbhsgf510r9")))

(define-public crate-moltenvk_deps-0.1.2 (c (n "moltenvk_deps") (v "0.1.2") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0g2m4gci9x22m77dkwq4lcnn34i6nxh1fx1ga2mg9acsvvwlm021")))

(define-public crate-moltenvk_deps-0.1.3 (c (n "moltenvk_deps") (v "0.1.3") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0wd9lmxygpriq1wr8rmnpdfaa5nrgkf10m5k9rmj971x1a896g8x")))

(define-public crate-moltenvk_deps-0.1.4 (c (n "moltenvk_deps") (v "0.1.4") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0bj7svh8psq3p13m1h40j18w5d1lcgpsk68axqa04ccaxyyzrbjn") (y #t)))

(define-public crate-moltenvk_deps-0.1.5 (c (n "moltenvk_deps") (v "0.1.5") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1l74ff1cqwvxl4wizf1ak6mryv6k3inlv21drwmgd9074wyvyzzz")))

(define-public crate-moltenvk_deps-0.1.6 (c (n "moltenvk_deps") (v "0.1.6") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "13bc95f9q9pr5k6n5019qkdb99x5l00ccc7bm505l6b1ddcadr1f")))

(define-public crate-moltenvk_deps-0.1.7 (c (n "moltenvk_deps") (v "0.1.7") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0m3wgn2i796dr2lsmmw4qgja5vzmjzd3h4n72dx23pk8gakkin0w")))

(define-public crate-moltenvk_deps-0.1.8 (c (n "moltenvk_deps") (v "0.1.8") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_perm") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "172z5zcnxkv80v2bsfb195n92lc3j5znkmdgx8vmwj1h34bdlma8")))

