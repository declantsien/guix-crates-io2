(define-module (crates-io mo lt moltenvk-sys) #:use-module (crates-io))

(define-public crate-moltenvk-sys-0.1.0 (c (n "moltenvk-sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 1)))) (h "09km6xwbynfdbwl765kwryszv2n073d1ij3xqlrij2wy5hyngifj")))

(define-public crate-moltenvk-sys-0.1.1 (c (n "moltenvk-sys") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 1)))) (h "1vr3a3y2aifgakaq60cmvc711ms2g8jm7rasxqmfxkjk92ykabmc")))

(define-public crate-moltenvk-sys-0.1.2 (c (n "moltenvk-sys") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 1)))) (h "0l5fi2j6hh85i1gd297vdhxsb9nv8aazimfjhb9icw6j29g28y0i")))

