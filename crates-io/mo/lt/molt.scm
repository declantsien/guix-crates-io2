(define-module (crates-io mo lt molt) #:use-module (crates-io))

(define-public crate-molt-0.1.0 (c (n "molt") (v "0.1.0") (h "1kaaslcr3w5rd5zvpazv6yvvg257jmrlnskzzkv49fbw7m6yj6ra")))

(define-public crate-molt-0.1.1 (c (n "molt") (v "0.1.1") (h "133cy2c7p2pfbjylckdz4nlwawyy305c6wfc9k10cr46vkvsrzbc")))

(define-public crate-molt-0.2.0 (c (n "molt") (v "0.2.0") (h "036bkk0hyja652jpq8p8pldhqm9240klwgpkf0ln5xjrxbc516bx")))

(define-public crate-molt-0.2.1 (c (n "molt") (v "0.2.1") (h "12jbbms5l58kc31wfnmzk6hhy97304grlka500msxz1xdxlyrbz6")))

(define-public crate-molt-0.2.2 (c (n "molt") (v "0.2.2") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)))) (h "07a1f9nm9hdyxagacfbbh90xk3r770bf5wn7z1nbw1dni1m9s0yr")))

(define-public crate-molt-0.3.0 (c (n "molt") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)))) (h "1jhj36i2mgczbx5sqidw70jl3n0hjwzw0xk686fb3r565rb0csa0")))

(define-public crate-molt-0.3.1 (c (n "molt") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)))) (h "1g0nwm9wkdj5b1g9kkwmkxyr2a9x2qdxw6dqbbb786r6qqkhvzzn")))

