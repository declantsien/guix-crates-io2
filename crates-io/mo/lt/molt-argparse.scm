(define-module (crates-io mo lt molt-argparse) #:use-module (crates-io))

(define-public crate-molt-argparse-0.1.0 (c (n "molt-argparse") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "molt") (r "^0.3.1") (d #t) (k 0)) (d (n "molt-argparse-procmacro") (r "^0.1.0") (d #t) (k 0)))) (h "09xh290mkfghlfc97xnnwh365qngr6jmwz79dpnq1qn3mqcvvprg")))

(define-public crate-molt-argparse-0.1.1 (c (n "molt-argparse") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "molt-argparse-procmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "molt-ng") (r "^0.3.2") (d #t) (k 0)))) (h "1sdspyxsxbna4f72hyxx4fxcld3ji1hapwy8qh8q6w3yp545dag8")))

