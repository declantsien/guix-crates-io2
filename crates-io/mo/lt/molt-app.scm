(define-module (crates-io mo lt molt-app) #:use-module (crates-io))

(define-public crate-molt-app-0.1.0 (c (n "molt-app") (v "0.1.0") (d (list (d (n "molt") (r "^0.1.0") (d #t) (k 0)) (d (n "molt-shell") (r "^0.1.0") (d #t) (k 0)))) (h "0rv6rbr8j3w0y37n8pixc8yj88pmm75z0h4jj0nrjk310yswkpa9")))

(define-public crate-molt-app-0.2.0 (c (n "molt-app") (v "0.2.0") (d (list (d (n "molt") (r "^0.2.0") (d #t) (k 0)) (d (n "molt-shell") (r "^0.2.0") (d #t) (k 0)))) (h "1wj3vsxr799fvcdil24a6wdqbdx3s6cwh52sqb0zvzc3c544n2v7")))

(define-public crate-molt-app-0.2.1 (c (n "molt-app") (v "0.2.1") (d (list (d (n "molt") (r "^0.2.1") (d #t) (k 0)) (d (n "molt-shell") (r "^0.2.1") (d #t) (k 0)))) (h "0zwixaj0qv4x5xa31dzp1nbzzbhz59l572r9d2yh8dflzzq9053q")))

(define-public crate-molt-app-0.2.2 (c (n "molt-app") (v "0.2.2") (d (list (d (n "molt") (r "^0.2.2") (d #t) (k 0)) (d (n "molt-shell") (r "^0.2.2") (d #t) (k 0)))) (h "09fnhcbciy1h011slb8mliib2jslxwlcdvrpz4b5mznamql1x3zr")))

(define-public crate-molt-app-0.3.0 (c (n "molt-app") (v "0.3.0") (d (list (d (n "molt") (r "^0.3.0") (d #t) (k 0)) (d (n "molt-shell") (r "^0.3.0") (d #t) (k 0)))) (h "0n99bqi1h5wls1pkfcjad1j4k291garwnkr7iii9hyld3yv4ac62")))

(define-public crate-molt-app-0.3.1 (c (n "molt-app") (v "0.3.1") (d (list (d (n "molt") (r "^0.3.1") (d #t) (k 0)) (d (n "molt-shell") (r "^0.3.1") (d #t) (k 0)))) (h "1lksqi8h737dd8h6m3kl272x6hsc95ryl1vr9w172i64sjs29zbg")))

