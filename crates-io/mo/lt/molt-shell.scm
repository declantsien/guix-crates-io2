(define-module (crates-io mo lt molt-shell) #:use-module (crates-io))

(define-public crate-molt-shell-0.1.0 (c (n "molt-shell") (v "0.1.0") (d (list (d (n "molt") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "04d8dirhaqv1gildknhsiyqgrzy1s9rvn8x5bl6d739lncjfx4bn")))

(define-public crate-molt-shell-0.1.1 (c (n "molt-shell") (v "0.1.1") (d (list (d (n "molt") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "12y45211mdhdk8db0kbkali7gxh1sxrisg5l4lj97zli6dgr058d")))

(define-public crate-molt-shell-0.2.0 (c (n "molt-shell") (v "0.2.0") (d (list (d (n "molt") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "18l6k5p7k0dxlc2d2sfj57mn0sy96lpl7kg3nd09xn7ni2fa8jq5")))

(define-public crate-molt-shell-0.2.1 (c (n "molt-shell") (v "0.2.1") (d (list (d (n "molt") (r "^0.2.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "1d0shg77fhb0fzqp4pnni7llq0nigbawn437g1pmjhn8xj4mp12h")))

(define-public crate-molt-shell-0.2.2 (c (n "molt-shell") (v "0.2.2") (d (list (d (n "molt") (r "^0.2.2") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "02ymrldv315ycn8dn2fr2hn0v3ar09psbcy47flrnv40s7kfgmz5")))

(define-public crate-molt-shell-0.3.0 (c (n "molt-shell") (v "0.3.0") (d (list (d (n "molt") (r "^0.3.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "0b22k2kqx0s7lfchk3k5sj7vp0qhb907ng3z5li2z03n9bsck9s5")))

(define-public crate-molt-shell-0.3.1 (c (n "molt-shell") (v "0.3.1") (d (list (d (n "molt") (r "^0.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "1311kfzqklwc7liyj1sf89xlf888dwhn82wc4si8byz1vgmji59a")))

