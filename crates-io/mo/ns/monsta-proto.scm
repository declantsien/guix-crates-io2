(define-module (crates-io mo ns monsta-proto) #:use-module (crates-io))

(define-public crate-monsta-proto-0.0.1 (c (n "monsta-proto") (v "0.0.1") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (d #t) (k 1)))) (h "1w9248v0j9la4w2mg4351f8hxvhv6g8bmnn0ngb3wdw01niriyvf")))

