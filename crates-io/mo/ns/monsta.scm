(define-module (crates-io mo ns monsta) #:use-module (crates-io))

(define-public crate-monsta-0.0.0 (c (n "monsta") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (d #t) (k 1)))) (h "0xirgn7fw24bbgai8k9j97qb5jprd6hdr5pgzs1aag28izph2wd8")))

(define-public crate-monsta-0.0.1 (c (n "monsta") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monsta-server") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)))) (h "102haahkj55ar184r4rd353p8759bz8plz86kyssvpr6fpvka0a1")))

