(define-module (crates-io mo ns monsta-client) #:use-module (crates-io))

(define-public crate-monsta-client-0.0.1 (c (n "monsta-client") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "monsta-proto") (r "^0.0.1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)))) (h "1v3r9vwc7n4l3dgwgncv4rh1ipv3nnlwqsr9zg7p4dryjz00xwzz")))

