(define-module (crates-io mo ns monsta-server) #:use-module (crates-io))

(define-public crate-monsta-server-0.0.1 (c (n "monsta-server") (v "0.0.1") (d (list (d (n "monsta-proto") (r "^0.0.1") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)))) (h "1c61x2akbyax0iarx6sim5js6q885x8ab4rgz5m6ygk8fbp5j711")))

