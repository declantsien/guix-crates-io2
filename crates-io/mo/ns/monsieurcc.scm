(define-module (crates-io mo ns monsieurcc) #:use-module (crates-io))

(define-public crate-monsieurcc-0.1.0 (c (n "monsieurcc") (v "0.1.0") (d (list (d (n "http") (r "^0.2.5") (d #t) (k 2)) (d (n "mockito") (r "^0.30.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rstest") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1mpph5zdj9z74h1z04rn1k30liaw9sxqfwa8q5abyavdisb0r0yh")))

