(define-module (crates-io mo ns monstermaker_core) #:use-module (crates-io))

(define-public crate-monstermaker_core-0.1.0 (c (n "monstermaker_core") (v "0.1.0") (d (list (d (n "hashable_rc") (r "^0.1.2") (d #t) (k 0)))) (h "022fjdpkh44ssc5sfg25swxzkhgzbs42qk76437qbzgxlncp0q08")))

(define-public crate-monstermaker_core-0.2.0 (c (n "monstermaker_core") (v "0.2.0") (d (list (d (n "hashable_rc") (r "^0.1.2") (d #t) (k 0)))) (h "1mkspii0fvhzhfqp96mkh8x1z5bnpzxb7srsigvxv7fvp2w35q7v") (f (quote (("default" "bestiary") ("bestiary"))))))

(define-public crate-monstermaker_core-1.0.0 (c (n "monstermaker_core") (v "1.0.0") (h "1b027zscalvw7m9m4bwr5sxlpjy85mjhynbmxcy1j2r1nybmkd7j") (f (quote (("default" "bestiary") ("bestiary"))))))

(define-public crate-monstermaker_core-2.0.0 (c (n "monstermaker_core") (v "2.0.0") (d (list (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0i6bahva2srqkg0xsmamc0c98s11zfy6d4qvpxqzaxahzy3910lc") (f (quote (("default" "bestiary") ("bestiary"))))))

(define-public crate-monstermaker_core-2.0.1 (c (n "monstermaker_core") (v "2.0.1") (d (list (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0bzhhn0hgshzkw68p6xpqq9z31hz5d97aylmyq7pzysi6zkkmj2d") (f (quote (("default" "bestiary") ("bestiary"))))))

(define-public crate-monstermaker_core-3.0.0 (c (n "monstermaker_core") (v "3.0.0") (h "14zjh4ynwsz53m02axipd5ii3pablwzhzdmqfx1hk00x26njmq6m")))

(define-public crate-monstermaker_core-3.1.0 (c (n "monstermaker_core") (v "3.1.0") (h "0x3j0fwaqh9nr780l9jj7daj66rqid65ydvxww8ayw4in4mrmskn")))

