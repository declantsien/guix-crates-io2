(define-module (crates-io mo ns monster_ugi) #:use-module (crates-io))

(define-public crate-monster_ugi-0.0.1 (c (n "monster_ugi") (v "0.0.1") (d (list (d (n "monster_chess") (r "^0.0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hvkmcwjpzv60xsc83xvhkik6a1kbns6zzrsjadjxf1ridr479v2")))

(define-public crate-monster_ugi-0.0.2 (c (n "monster_ugi") (v "0.0.2") (d (list (d (n "monster_chess") (r "^0.0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "106zkna13blfwl0hwm69r0nsipbj1cw1qwm0lgykjqvrr9dmvg8w")))

(define-public crate-monster_ugi-0.0.3 (c (n "monster_ugi") (v "0.0.3") (d (list (d (n "monster_chess") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1inzxndkvvi8qjrcm0b1fn1136jhh6f8wv6w320gpislws561760")))

(define-public crate-monster_ugi-0.0.4 (c (n "monster_ugi") (v "0.0.4") (d (list (d (n "monster_chess") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zrhkkxvaq3cddqirsk1s32qwsqsxcamf03ifjl0y1gdmkncx517")))

(define-public crate-monster_ugi-0.0.5 (c (n "monster_ugi") (v "0.0.5") (d (list (d (n "monster_chess") (r "^0.0.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "002j7gabirjd49xki6366bn8l53cj0zf5y4w7f9wkvp7k2afqa5q")))

(define-public crate-monster_ugi-0.0.6 (c (n "monster_ugi") (v "0.0.6") (d (list (d (n "monster_chess") (r "^0.0.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bwmvpnj50ir0h8cfv0pg1g8v4j7h440n8fl3wvrqxm1vb6rz4wk")))

(define-public crate-monster_ugi-0.0.7 (c (n "monster_ugi") (v "0.0.7") (d (list (d (n "monster_chess") (r "^0.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "156difb91l44qcjl9yqmdjnx4j88rlx7aivgwnwb7ys6w694wqbd")))

(define-public crate-monster_ugi-0.0.8 (c (n "monster_ugi") (v "0.0.8") (d (list (d (n "monster_chess") (r "^0.0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xvficnzbcckpxbg4mf956h3il0k2fs6ansif8dl88barpkq0lx4")))

(define-public crate-monster_ugi-0.0.9 (c (n "monster_ugi") (v "0.0.9") (d (list (d (n "monster_chess") (r "^0.0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wbyd61ksgjvfqlfnaywwazsialxpmf667spd1yr1x69bvx7j4ya")))

(define-public crate-monster_ugi-0.0.10 (c (n "monster_ugi") (v "0.0.10") (d (list (d (n "monster_chess") (r "^0.0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kbx030ml0bf38j6l9z14278ypy2a9dxv6v15lrvfh7v30lx79y8")))

(define-public crate-monster_ugi-0.0.11 (c (n "monster_ugi") (v "0.0.11") (d (list (d (n "monster_chess") (r "^0.0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05x84vhddkzhh7xnx0dig90jyp72i7n5bbz71iayrlq0m26m6my7")))

(define-public crate-monster_ugi-0.0.12 (c (n "monster_ugi") (v "0.0.12") (d (list (d (n "monster_chess") (r "^0.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "036377pnzba3q0hkd8ii5y8mgq5z0pffff47q4zw86af4q0gdb8h")))

(define-public crate-monster_ugi-0.0.13 (c (n "monster_ugi") (v "0.0.13") (d (list (d (n "monster_chess") (r "^0.0.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1741mmbj6ndmlqfy3wsbcdjif3c57k9637f7ld4isshx1vddj76j")))

(define-public crate-monster_ugi-0.0.14 (c (n "monster_ugi") (v "0.0.14") (d (list (d (n "monster_chess") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x4yfwf9lx9vbbl0r91f1mmb4q78wj6fk21h0lgpad2an0kg9qvy")))

(define-public crate-monster_ugi-0.0.15 (c (n "monster_ugi") (v "0.0.15") (d (list (d (n "monster_chess") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09bg1nf0fi1jrqqamxcbwi2qczd89ad7ngw2by639gkv9h5053gi")))

(define-public crate-monster_ugi-0.0.16 (c (n "monster_ugi") (v "0.0.16") (d (list (d (n "monster_chess") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06dzwiwhglcsx12kmindgc8zkpq6j1d9j8gx2rh8hg4l1ljc5rji")))

(define-public crate-monster_ugi-0.0.17 (c (n "monster_ugi") (v "0.0.17") (d (list (d (n "monster_chess") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qx0dwv7pahr6kfzcvc4d4hxphih2xqhbw65rlgh5si9vpcvwbs1")))

(define-public crate-monster_ugi-0.0.18 (c (n "monster_ugi") (v "0.0.18") (d (list (d (n "monster_chess") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05z539flfck2k9s41b605bslcilr8xrrv323yzf9dihlbw6hq5wm")))

(define-public crate-monster_ugi-0.0.19 (c (n "monster_ugi") (v "0.0.19") (d (list (d (n "monster_chess") (r "^0.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10l8c6cjm98sdz59804k8wkf9xdpr789nigkz2gqfcr3j1diw4vb")))

(define-public crate-monster_ugi-0.0.20 (c (n "monster_ugi") (v "0.0.20") (d (list (d (n "monster_chess") (r "^0.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08y6sm967cvmhw5ag0w329287kmqky1fps5h6b6z1w855gbjv0kr")))

