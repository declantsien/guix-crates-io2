(define-module (crates-io mo ns monstrio) #:use-module (crates-io))

(define-public crate-monstrio-0.1.0 (c (n "monstrio") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "multi_reader") (r "^0.1") (d #t) (k 0)))) (h "0q2wfzrb11i8zswy7i7snlyzfgaaagiyrg3cmd1kfjq7m4lmafd1") (f (quote (("default"))))))

(define-public crate-monstrio-0.1.1 (c (n "monstrio") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "multi_reader") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "0p7hqmxgfdsl2djzdq0nq20a8szym73mz7hcwm363mqcqk6kz6z6") (f (quote (("default"))))))

(define-public crate-monstrio-0.1.2 (c (n "monstrio") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "multi_reader") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "1zvddp2zfbb1frdycg8xd26am5ma9z772knal6xnhq6zwj81wyfb") (f (quote (("default"))))))

