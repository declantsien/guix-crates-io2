(define-module (crates-io mo ns monster_maker_core) #:use-module (crates-io))

(define-public crate-monster_maker_core-0.1.0 (c (n "monster_maker_core") (v "0.1.0") (d (list (d (n "hashable_rc") (r "^0.1.2") (d #t) (k 0)))) (h "0h235011273marbhgzcj0sicbqm2hkfwynray1srqkhspjig47fj") (y #t)))

(define-public crate-monster_maker_core-0.1.1 (c (n "monster_maker_core") (v "0.1.1") (d (list (d (n "hashable_rc") (r "^0.1.2") (d #t) (k 0)))) (h "0lsizrih852iaby0fmzfqkx5ndh5bvij2x950n4gsr34v5xcspp1") (y #t)))

(define-public crate-monster_maker_core-0.1.2 (c (n "monster_maker_core") (v "0.1.2") (d (list (d (n "hashable_rc") (r "^0.1.2") (d #t) (k 0)))) (h "1an1slicfkjnjnv7ypjnlmk8qr4p55lyhy0zazgh36c8dyi01c77") (y #t)))

