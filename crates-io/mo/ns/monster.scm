(define-module (crates-io mo ns monster) #:use-module (crates-io))

(define-public crate-monster-0.1.0 (c (n "monster") (v "0.1.0") (h "0ykf2fpb4f1ggvcmi393g12jxkc0iscykpd9yalk8zd8rkkqrj7v")))

(define-public crate-monster-0.1.1 (c (n "monster") (v "0.1.1") (h "1j4h2krxh1lgqqwj87h5k6jv6ar91arz4sbvln1adpp50ss7r9cv")))

(define-public crate-monster-0.1.2 (c (n "monster") (v "0.1.2") (h "12gy0yyh6p2zaiak98jrq0ssz8azzfm3qhr7gsj4m89kqfp8c3hh")))

(define-public crate-monster-0.1.3 (c (n "monster") (v "0.1.3") (h "0zc87j6s27681j1ymcvywachada2ymn4civgz151fnxixzivdy64")))

(define-public crate-monster-0.1.5 (c (n "monster") (v "0.1.5") (h "1nyrq4qfgjlalkj1r3zdw80q4sg3mrk46fx6ffjdvdm4g8y4acg8")))

(define-public crate-monster-0.1.6 (c (n "monster") (v "0.1.6") (h "17q13aypjr3wg8d2wcmcjfpwlv0n4lqvyrcdc5xv9ghg8w9kbz8q") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.7 (c (n "monster") (v "0.1.7") (h "1y1iwavaxa14gyfy93d62n6rh1whazpp28cl48cxswyfkjacx1hz") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.8 (c (n "monster") (v "0.1.8") (h "1nxffv2rh1ssf1i1mzr1rvs4bm2j0ih6rkswzw4z1by46k5g0kl9") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.9 (c (n "monster") (v "0.1.9") (h "1pjm36njm0vcayhdy1p3kfwqq6xkcz3j8a0iikhln7hk6mvbkkyf") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.10 (c (n "monster") (v "0.1.10") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "06g3lmx7hhkbpq8bjj43s5lkzrrlmdkdb40iq6q7wfd1qzgcnhbs") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.11 (c (n "monster") (v "0.1.11") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "172drwfs2nhv0awscpf7wg9kdm5as3hpd1mk5bf0079f25zh8sil") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.12 (c (n "monster") (v "0.1.12") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "1an3hq6krhnzp106hk3ap6lbj9cjx1hrrqc7rwcplc4rcq6wk33l") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.13 (c (n "monster") (v "0.1.13") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0ryrimizyf70sz6wa7l3c6z3a23ya7473vvi9mrx6c6kha9bb9mw") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.14 (c (n "monster") (v "0.1.14") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "19868h0gk89mgx6y1611pgi6gr3idwsx4x1n4j0w52brvq6xxmsi") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.15 (c (n "monster") (v "0.1.15") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "011sj5qcj7gg28gnr3pclj1rfbw9phrw133pmghqmrdfn0x4gp6f") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.16 (c (n "monster") (v "0.1.16") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0adlys9m10mi8iskn9kr55n06avqmk2h09i8v0fw65xv34kz8h9l") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.17 (c (n "monster") (v "0.1.17") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0qy418cf03i7c2agsrk4wwv4hvhkhpgdgzi1pvdkh4mrya2dy9c2") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.18 (c (n "monster") (v "0.1.18") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0hfybg2w51sz6ps2j1jphhrhmkz31yf29fj8xjm6asf1kx07mvlq") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.19 (c (n "monster") (v "0.1.19") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0gm2613i2n62n4v5bkfapw3sx905i5iwhxizdn6w15xv386hmign") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.20 (c (n "monster") (v "0.1.20") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0csmfplilxzzfkl66f9n8lfzvi7ix87bc64grbjmxbkx0pkm7b6i") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.21 (c (n "monster") (v "0.1.21") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "169mf4d62r5dql749r0mpyq3bw71p444cmaq2zjr2iw96ysyl3v3") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.22 (c (n "monster") (v "0.1.22") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "1a8s6xhvmdj7f7gwmhrdp5yl8mfpgvfbka98zw801fpidc06ry4c") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.23 (c (n "monster") (v "0.1.23") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0saig3sf8zwx2gv1h0ynm9zaxz2yvxriyrcrxx3szmcnmvk0dg41") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.24 (c (n "monster") (v "0.1.24") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0sa1xipgbk4iacv44j15fjgs7rkwy5gj267ms4wghs23yvbml7kr") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.25 (c (n "monster") (v "0.1.25") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "040qm5yvravcwkr6pxbbwmmk1lmk36xx7acsabi1arbrpmhp6z92") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.26 (c (n "monster") (v "0.1.26") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "1nqznca9scdylazlxay6yf06h93p09zf2s1fayzrw10sw8l86qz5") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.27 (c (n "monster") (v "0.1.27") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "0r6cp4dq4njqyd1g096a5g05snhvdr99jbyd928cirka0jlcwhcb") (f (quote (("unstable") ("default"))))))

(define-public crate-monster-0.1.28 (c (n "monster") (v "0.1.28") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 2)))) (h "1k260k853l3p57dj5slrn1agyzf7xml95hh2wm56yvzszlvn2a9v") (f (quote (("unstable") ("default"))))))

