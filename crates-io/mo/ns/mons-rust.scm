(define-module (crates-io mo ns mons-rust) #:use-module (crates-io))

(define-public crate-mons-rust-0.1.0 (c (n "mons-rust") (v "0.1.0") (h "1l6qjs6ddzhlrxzq9m9ig6j9d4vni5ilx2n7pngfzblmq23jd1sp")))

(define-public crate-mons-rust-0.1.5 (c (n "mons-rust") (v "0.1.5") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1sl7fgqaam2nw5rfk5d60qhgxacm5s4ppmd1j1q95jw04nl4vfxx")))

