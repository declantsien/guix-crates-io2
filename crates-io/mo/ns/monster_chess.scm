(define-module (crates-io mo ns monster_chess) #:use-module (crates-io))

(define-public crate-monster_chess-0.0.1 (c (n "monster_chess") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0ki12g1yvrsrc0jlkyhvhkd1679clfwk242b1bx96f51783xrv1n")))

(define-public crate-monster_chess-0.0.2 (c (n "monster_chess") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1p276ffkpq2xl72z9nx7x3x3bvj2mla3l35cwl5rris9imfdjy04")))

(define-public crate-monster_chess-0.0.3 (c (n "monster_chess") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "11g2514h4pc9ifzwynkc7ry3qms9p9yqamxqb0qvdh6gx5rbbjvy")))

(define-public crate-monster_chess-0.0.4 (c (n "monster_chess") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "13d7r9jgbjnlq9pwqjhapqqarn99482i8dkzlwnixyn85x2zr0xi")))

(define-public crate-monster_chess-0.0.5 (c (n "monster_chess") (v "0.0.5") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1v5m7j1knf3qg9nggncmdibhzymnv1rd2nkidamwn1k1bsa1qpbp")))

(define-public crate-monster_chess-0.0.6 (c (n "monster_chess") (v "0.0.6") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0hyrzjh5cxw1s6bwjsyq3p4irpns06vwgl3pr58q9hyp6ana0s55")))

(define-public crate-monster_chess-0.0.7 (c (n "monster_chess") (v "0.0.7") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "14jng5nkz2zp312dph66g0j19glm54970m5b9zzhw86g364s4b1v")))

(define-public crate-monster_chess-0.0.8 (c (n "monster_chess") (v "0.0.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "09xpj2lwj1d9ccxrnh5iqbsgbd3j415g7hg4m4vw1fdbd8chv3gb")))

(define-public crate-monster_chess-0.0.9 (c (n "monster_chess") (v "0.0.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1biv5vb8mgbipp8dmil9kg3a54smm86737z6lkwavcznaw9cra7f")))

(define-public crate-monster_chess-0.0.10 (c (n "monster_chess") (v "0.0.10") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1w037zrz43463ka11ksync09ak59ms096dcb5h5xr4fgkz64kf5k")))

(define-public crate-monster_chess-0.0.11 (c (n "monster_chess") (v "0.0.11") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0ycix0jjldrhb9hi1yw901px0iwa8h5hkh9h9cck6gmnp46ssfbs")))

(define-public crate-monster_chess-0.0.12 (c (n "monster_chess") (v "0.0.12") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1x648xl49b9hh7hjrdrlqqhp4gx5rnvlb2hslxjjyzq931gc1r7s")))

(define-public crate-monster_chess-0.0.13 (c (n "monster_chess") (v "0.0.13") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0kdifzaapy8lz6f1rdvc992bkl8wmj435r8kldxv0bp5irr5ak9b")))

(define-public crate-monster_chess-0.0.14 (c (n "monster_chess") (v "0.0.14") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1ghxcky8y9xbfaaw9cgm1s4g3gax6hnbd8ysnjw2bhl0vhnv8bh3")))

(define-public crate-monster_chess-0.0.15 (c (n "monster_chess") (v "0.0.15") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "00cswp70d2z6i2bw85s1209rnr2anvhcl87msmk26w629rdv4f63")))

(define-public crate-monster_chess-0.0.16 (c (n "monster_chess") (v "0.0.16") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "00p8lwvmgb4ai3i6f4v5gfk03qw0fbzyj1x5ly48ysakdklk06ng")))

(define-public crate-monster_chess-0.0.17 (c (n "monster_chess") (v "0.0.17") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1cwx6p9q1v12x1vnyp264qrjx3ni20ynj9cp87hp9qva8na0mba9")))

(define-public crate-monster_chess-0.0.18 (c (n "monster_chess") (v "0.0.18") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0l58m4cb7q75ib0b0jd14zf37frrrjh950w6rzjlh0ljnnv8962c")))

(define-public crate-monster_chess-0.0.19 (c (n "monster_chess") (v "0.0.19") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0x3pc73z20zbxnam7jw7qx2cpi19zkhadrx8z7l1whsyc7r624s5")))

(define-public crate-monster_chess-0.0.20 (c (n "monster_chess") (v "0.0.20") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1h80mg6wgk0qal7p70rnjshvqnn70s1pkb5n1mz0v1azagp57yk9")))

(define-public crate-monster_chess-0.0.21 (c (n "monster_chess") (v "0.0.21") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "01287j0cjjgnpiz77wkz6y6135va9rv53h23y1jawnra6nykjbdg")))

(define-public crate-monster_chess-0.0.22 (c (n "monster_chess") (v "0.0.22") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0brmwg86bpa199q5z5gjxbg93yzr3vf7d66ddv9ykyphm0r0vm7a")))

(define-public crate-monster_chess-0.0.23 (c (n "monster_chess") (v "0.0.23") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "00mzjqi1v0pmi9nlza95hzyc9m7d7wqv11dfiixq40r1p8qzydls")))

(define-public crate-monster_chess-0.0.24 (c (n "monster_chess") (v "0.0.24") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1zy24b2lxqgnfrszkcmyjq749z192ki9cxsprlfawcmv94zw4z3w")))

