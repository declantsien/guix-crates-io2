(define-module (crates-io mo nt month) #:use-module (crates-io))

(define-public crate-month-0.0.0 (c (n "month") (v "0.0.0") (h "14r2z1qpz974v8zcnc6hkvlxc79h1v1xcxk82s6v7fldnmh41x2a")))

(define-public crate-month-0.1.0 (c (n "month") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j6vykwqx9428d5s284m4fx4f5hpdvhr7jv3qgn946jk25f7zman") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-month-0.1.1 (c (n "month") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lnv4g04s75dn7kw5g2lisxpz2amgq7rb0q176l2q593n2i1hjlr") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-month-0.2.0 (c (n "month") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "138r6i5fllz4mr29bcpkpk02rfjwbr7r3j21yldi4bwadh78grl6") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-month-0.3.0 (c (n "month") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gkb8by07igybr465cxxlsw4llzd5mbhlr375274z4l2664xwxx9") (f (quote (("with-serde" "serde") ("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

