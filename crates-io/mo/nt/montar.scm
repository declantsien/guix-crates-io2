(define-module (crates-io mo nt montar) #:use-module (crates-io))

(define-public crate-montar-0.1.0 (c (n "montar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "07bg7kb2fbkickyx0pa892xwy31q0kpjhv8qp96wzkgh91r9hn14")))

