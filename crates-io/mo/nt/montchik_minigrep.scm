(define-module (crates-io mo nt montchik_minigrep) #:use-module (crates-io))

(define-public crate-montchik_minigrep-0.1.0 (c (n "montchik_minigrep") (v "0.1.0") (h "0cwrl96dbqys2m2drkwgjk1i5y8mpqr9vdzngzw6jqkfr0v2a6a6")))

(define-public crate-montchik_minigrep-0.1.1 (c (n "montchik_minigrep") (v "0.1.1") (h "1lhfgmim6r7c2blb689ss72jv51h35a4q2kyzd2w1p6vn0w8kd06")))

