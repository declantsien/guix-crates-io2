(define-module (crates-io mo nt montage) #:use-module (crates-io))

(define-public crate-montage-0.1.0 (c (n "montage") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thread") (r "^0.1.0") (d #t) (k 0)))) (h "1lkbga0swv827l4mw3sslcrxssxchr9xdmnr8619nhahhghn1b13")))

