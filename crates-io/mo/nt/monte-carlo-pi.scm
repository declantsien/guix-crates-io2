(define-module (crates-io mo nt monte-carlo-pi) #:use-module (crates-io))

(define-public crate-Monte-Carlo-Pi-0.1.0 (c (n "Monte-Carlo-Pi") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f5glkd6csiq9pz7k5kpvxakzaj4cv90yaz290727jmdyjsj446z")))

(define-public crate-Monte-Carlo-Pi-0.1.1 (c (n "Monte-Carlo-Pi") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07xnzm5h50xgsccnbjda8pbnv2jag0ibmh8vksy2arkdcn36ri1d")))

