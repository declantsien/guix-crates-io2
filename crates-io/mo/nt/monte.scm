(define-module (crates-io mo nt monte) #:use-module (crates-io))

(define-public crate-monte-0.1.0 (c (n "monte") (v "0.1.0") (h "0w2hgq8893lypd1hcvmdnfzyia2s1mh7n19w0n667dhpbib177gs")))

(define-public crate-monte-0.1.1 (c (n "monte") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1fxna6ypcxqaqdf3xkr2pd3jk20m185qdb38y30bxk4isj8zlhmg")))

(define-public crate-monte-0.1.2 (c (n "monte") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19zv27vwxjsvjn0h3lagrwcw3207wyl7blq5spricm31pvginac9")))

(define-public crate-monte-0.1.3 (c (n "monte") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0bj89qmhaynf7z1qpnfmg9pxqxyq38i633ln51nd29r0j9dpqbj9")))

(define-public crate-monte-0.1.4 (c (n "monte") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0r4zmdq8crz0407ndwhqd7pzvz3sby93y4sg3mcxqrm92s9bkw16")))

