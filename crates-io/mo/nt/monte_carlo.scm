(define-module (crates-io mo nt monte_carlo) #:use-module (crates-io))

(define-public crate-monte_carlo-0.1.0 (c (n "monte_carlo") (v "0.1.0") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "166w57yzlv2bdlakj2jr4wmq0h3abpv18mfv74zm658ypbap768r")))

(define-public crate-monte_carlo-0.1.1 (c (n "monte_carlo") (v "0.1.1") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "0j9y7g24pyijv701z42qgf72jnw5jrf749h7r5z7w5iwfn9hfwlx")))

