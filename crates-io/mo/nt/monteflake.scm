(define-module (crates-io mo nt monteflake) #:use-module (crates-io))

(define-public crate-monteflake-0.1.0 (c (n "monteflake") (v "0.1.0") (h "150vnqm7p0zz94m64nlzybbkqc8sikvx3ydy25jx5fc6yv75j2qg")))

(define-public crate-monteflake-0.1.1 (c (n "monteflake") (v "0.1.1") (h "13wz6rg8p5dq34x139xznbp094p4nvsda40pw6gmr4lv9m6crvyx")))

(define-public crate-monteflake-0.1.2 (c (n "monteflake") (v "0.1.2") (h "0hhxrxfj5z06mxnbvf3z7al9q59cfgcvajpl87cm7041lba8nh94")))

(define-public crate-monteflake-0.1.3 (c (n "monteflake") (v "0.1.3") (h "0cbq2j4wxxygwk2wzcd10jjr0vsgp0dbcywfig8h9dgping1dbim")))

