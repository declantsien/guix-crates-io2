(define-module (crates-io mo xi moxi) #:use-module (crates-io))

(define-public crate-moxi-0.1.0 (c (n "moxi") (v "0.1.0") (d (list (d (n "addr2line") (r "^0.20.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1c3vb2k775jis6z8phmqjrjinvp0p5an37d2ykz9cqzzqghhyvpz")))

