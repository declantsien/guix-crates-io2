(define-module (crates-io mo xi moxie-web) #:use-module (crates-io))

(define-public crate-moxie-web-0.1.0 (c (n "moxie-web") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.5") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "moxie") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.27") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "Event" "EventTarget" "HtmlElement" "Node" "Text" "Window"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0wy87hfjx8ig15dnsvhahaq49i3xd0lq8vdyzvfk1v8f17y0qw87") (f (quote (("default-features" "console_error_panic_hook" "wee_alloc"))))))

