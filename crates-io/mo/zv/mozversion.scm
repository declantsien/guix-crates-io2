(define-module (crates-io mo zv mozversion) #:use-module (crates-io))

(define-public crate-mozversion-0.1.0 (c (n "mozversion") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "0sdiny4mp0bnygyp425xqr9npxncz3xwsar433ij8xavk0lrcidr")))

(define-public crate-mozversion-0.1.1 (c (n "mozversion") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "1w3260s3pqggp0aqrggx46plcgjiwk2xvq1151hlmjdq9lhfmb3n")))

(define-public crate-mozversion-0.1.2 (c (n "mozversion") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "09lgy3fyvc6570rp4gx9qn8hrbxyg6aa9d1y1mb12djm6l0s9cwz")))

(define-public crate-mozversion-0.1.3 (c (n "mozversion") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.2") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "0qaq52z0xc6y1ybdxmb559pmj6fcs20amr5c4gl19yhw0g4i4x2r")))

(define-public crate-mozversion-0.2.0 (c (n "mozversion") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "1zj8hx7fzy7022dnlliy64wlg7fisqzl7dvd4wa6mslbfa158a9g")))

(define-public crate-mozversion-0.2.1 (c (n "mozversion") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "07kc30b5fhcawf6c7z006h4xwgvq01hdya1mw01qyhavfqclcz5w")))

(define-public crate-mozversion-0.3.0 (c (n "mozversion") (v "0.3.0") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "1rkc3w7h96f1kra6vi5412l7m716xv3ik68n48jscg13sw8y3nh6")))

(define-public crate-mozversion-0.4.0 (c (n "mozversion") (v "0.4.0") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0adhaxlci5yd6x98a5nnqkiksljbv5xhhgqqwphk65ifvd3x1rh4")))

(define-public crate-mozversion-0.4.1 (c (n "mozversion") (v "0.4.1") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "1dqw346xc52qgd5ff6nc8x9bylhb0w23fs3wqrybx1vi4pi96g6k")))

(define-public crate-mozversion-0.4.2 (c (n "mozversion") (v "0.4.2") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "04hk4gpi5r7ba4f3hfzjc2y8d222i410phg11pwd7hci7fqhf1z4")))

(define-public crate-mozversion-0.4.3 (c (n "mozversion") (v "0.4.3") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "1k037ng3fj6jypgs1m93wwbgb41cv4iwd13vz67lqiz18b7zjvr3")))

(define-public crate-mozversion-0.5.0 (c (n "mozversion") (v "0.5.0") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1wfiqif4bb40sc3h16jn3mh24spc22d3qy4qi06a9rwml331l8iy")))

(define-public crate-mozversion-0.5.1 (c (n "mozversion") (v "0.5.1") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1mklqnvgmc6hg6hkadzlm6g2fv666ly6h6xsw8yzllnc6iprj72y")))

(define-public crate-mozversion-0.5.2 (c (n "mozversion") (v "0.5.2") (d (list (d (n "regex") (r "^1") (f (quote ("perf" "std"))) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13li5a7wcbc0862rqkadadj709fp56dkcav9v9p085ii86saiw5b")))

