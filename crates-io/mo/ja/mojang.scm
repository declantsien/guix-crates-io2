(define-module (crates-io mo ja mojang) #:use-module (crates-io))

(define-public crate-mojang-0.1.0 (c (n "mojang") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "tinyjson") (r "^2.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (d #t) (k 0)))) (h "0nvfpw84hxqz2p1rxd0la8bxg9r4piihk289xpwhybmgrcbr32r9") (f (quote (("timeout"))))))

