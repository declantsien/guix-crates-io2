(define-module (crates-io mo ja mojang-leveldb) #:use-module (crates-io))

(define-public crate-mojang-leveldb-0.1.0 (c (n "mojang-leveldb") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1d236jlbswnfbmr2i9z2ijwfdcpb54n3y47mmr2hk9pbw02vshvv")))

(define-public crate-mojang-leveldb-0.2.0 (c (n "mojang-leveldb") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "193nadncrn6dv0lwdnd497j48fwi7rycczmmna26n0rgvzjw9mzf")))

