(define-module (crates-io mo ru morus) #:use-module (crates-io))

(define-public crate-morus-0.1.0 (c (n "morus") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 2)))) (h "1507bqvz7zhrsaxi9nv3pnk18j890sngfjgvajkcy7m9p3rr1gli") (f (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1.1 (c (n "morus") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 2)))) (h "096p7si5qd2v39rzhfl02jvljglhfiwia1pvj50986z5h84qyk9c") (f (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1.2 (c (n "morus") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 2)))) (h "12vf0i2nb2mhz88f550zb390353z2ph45pnax54ffj2gz7vs45by") (f (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1.3 (c (n "morus") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 2)) (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 2)))) (h "0qr9ndylrkpyvbydz8rbjgrax483izim1q4pcb2370f5a38im9s1") (f (quote (("std") ("default" "std"))))))

