(define-module (crates-io mo rq morq) #:use-module (crates-io))

(define-public crate-morq-0.1.0 (c (n "morq") (v "0.1.0") (h "025h3i08z3v92mbjx1fsc6kgvsfwpbrjszyw9h4zhac5wdxyx1ay")))

(define-public crate-morq-0.2.0 (c (n "morq") (v "0.2.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0g8nal095bcxfdd1p5i1rr6020y0hrnvf3xrxckh5hnw2kyaisly")))

(define-public crate-morq-0.2.1 (c (n "morq") (v "0.2.1") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0n6p4j338ynf1p4k4khhrc11cdxcd2587r9zy2y3swxd9m2x88vv")))

(define-public crate-morq-0.2.2 (c (n "morq") (v "0.2.2") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0rvw3z53d74na5z7w957bd2mq1vgvcjnvz598jafg5x5rsa5jn8p")))

(define-public crate-morq-0.3.0 (c (n "morq") (v "0.3.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "1x2kkj2wnl5b3siidmy2cw2j7q40bfrgjjcsshrhv8ns8hv8xy01")))

