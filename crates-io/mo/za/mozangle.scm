(define-module (crates-io mo za mozangle) #:use-module (crates-io))

(define-public crate-mozangle-0.1.0 (c (n "mozangle") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "10bpsszczc2m4b04y65zawryd8zfmqx6b5x6d21pr2lyxf7wym7w") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.1 (c (n "mozangle") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0hlhmxflhwphpm823nl4hv3d6jn0pv8gkn8d10dnjfsfh2xy3yf6") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.2 (c (n "mozangle") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1lwrlqm4pxnhhya6dn5gh41d614hlh99kivaigi167hkwviw19ga") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.3 (c (n "mozangle") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "14j050xmxbi5j9gr85br9yfpq7kl7k3bgkl2n3p0x81r5m7nx4ad") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.4 (c (n "mozangle") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "17z5dyawi89cl12a9yckn0hbqlrraxdrm0rnjx8vzlwb92sxjhbv") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.5 (c (n "mozangle") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1i633bzv596nx06h95ab7ykmckh4c6i1fykh5s09gg4wypjh8kxx") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.6 (c (n "mozangle") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1alksm7drlg9ynvmli33408574x5fzvl0x1spfcg85r9g7k8618z") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.1.7 (c (n "mozangle") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "gl_generator") (r "^0.9.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1rz00q2b4fprixdj4cxv1wd22k86i7m46byc4pg0zayg865a3a25") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.0 (c (n "mozangle") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "gl_generator") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "02651kxdzpl45w9db5lbv6861rzcsknklimwrxqnmm9g47jirfn9") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.1 (c (n "mozangle") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "gl_generator") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "06dp9x5vxh1p1xwygdw50b34l1j2s0v5061h1maw7vldqxysh1n3") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.2 (c (n "mozangle") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "gl_generator") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "13b1iaiihh9jax6bqpy7cnrw33kn1kq21rqdb2svz0wbrj001h8x") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.3 (c (n "mozangle") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.5") (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1rr4v9x2i8n70p1aic9jbklds788z91vn87ixgb514w00w9qcv61") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.4 (c (n "mozangle") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0jml2nq7wdg7a9dr91142cxvc72jclh9h7d9kjvaghg3ym22yvrp") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.5 (c (n "mozangle") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1iysgxwlcadz7w5h2qwazq319yibldw3xwbldapgz912csflchq6") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.6 (c (n "mozangle") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0ghjdaw2fzafqx3kb52wq3maj75fqq07naq3hdrsrvjdxyqg6shi") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.2.7 (c (n "mozangle") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0lcngxzbj803qwfxxg1rf257cmf28ckdy3ar8pmn5wxn0rd1p9km") (f (quote (("egl" "gl_generator"))))))

(define-public crate-mozangle-0.3.0 (c (n "mozangle") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0aslrbbidz8y05jx7jh0i69srj24bnngn8xqlj1f1d3jxsbasf0v") (f (quote (("egl" "gl_generator") ("build_dlls"))))))

(define-public crate-mozangle-0.3.1 (c (n "mozangle") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "05yc0y9ljqiyla5napglnikizm9v8fch8zfj52zgz7vw199jcj5p") (f (quote (("egl" "gl_generator") ("build_dlls"))))))

(define-public crate-mozangle-0.3.2 (c (n "mozangle") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1qp8yl74891qz253brssn857xkiw3csk57280918kjnvbs93hx6p") (f (quote (("egl" "gl_generator") ("build_dlls"))))))

(define-public crate-mozangle-0.3.3 (c (n "mozangle") (v "0.3.3") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1738ilkamx3j3m9520rk24672hkwry8xr45wm9ggn025l5ypk67g") (f (quote (("egl" "gl_generator") ("build_dlls"))))))

(define-public crate-mozangle-0.3.4 (c (n "mozangle") (v "0.3.4") (d (list (d (n "cc") (r "^1.0.5") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0z35f8pri49slba1p62ljp7shba95yqmz2srdx1m9q45dvyl9sdb") (f (quote (("egl" "gl_generator") ("build_dlls"))))))

(define-public crate-mozangle-0.3.5 (c (n "mozangle") (v "0.3.5") (d (list (d (n "cc") (r "^1.0.74") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.13.1") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "184fc5g8fpl4xjrhq19jn2f156zv9vj86ycrlyl5hrkgl2xq4j89") (f (quote (("egl" "gl_generator") ("build_dlls" "egl" "gl_generator"))))))

(define-public crate-mozangle-0.3.6 (c (n "mozangle") (v "0.3.6") (d (list (d (n "cc") (r "^1.0.74") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1mf7cg2hjpwsamjrjhk5mwc3gz03xzqv6cgaxjbnwzxhfslaa7rs") (f (quote (("egl" "gl_generator") ("build_dlls" "egl" "gl_generator"))))))

(define-public crate-mozangle-0.4.0 (c (n "mozangle") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cc") (r "^1.0.74") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0dfafziwylcnsq6z9am6xyfrgcx3963mc7ki0yq0cypfl1w0hljh") (f (quote (("egl" "gl_generator") ("build_dlls" "egl" "gl_generator"))))))

(define-public crate-mozangle-0.5.0 (c (n "mozangle") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.68") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cc") (r "^1.0.74") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1wxbsr2zj57fd2qvyy6v83d1mgsqgzxvhmd1v81brr50di0yvddf") (f (quote (("egl" "gl_generator" "libz-sys") ("build_dlls" "egl" "libz-sys"))))))

(define-public crate-mozangle-0.5.1 (c (n "mozangle") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime" "which-rustfmt"))) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dlopen") (r "^0.1") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0jvy0ski04pyfn0fgihs01w28rn3shisr55g6lw94y2jlv8zfjmi") (f (quote (("egl" "gl_generator" "libz-sys") ("dynamic_lib") ("build_dlls" "egl" "libz-sys"))))))

