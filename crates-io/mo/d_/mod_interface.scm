(define-module (crates-io mo d_ mod_interface) #:use-module (crates-io))

(define-public crate-mod_interface-0.1.0 (c (n "mod_interface") (v "0.1.0") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1kyq5v12wpx7s33r25icgichv8a1f2fsh5jrwm1nk5dwp6ga1rqj")))

(define-public crate-mod_interface-0.1.1 (c (n "mod_interface") (v "0.1.1") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1j906hjjjfimrwga33b89d9zx11w0wjzlfs1wcmgnv0c63b5rg2k")))

(define-public crate-mod_interface-0.1.2 (c (n "mod_interface") (v "0.1.2") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "173qvabk8fh3qwairq5ixg6drpcvir7rrawa2ncmibzh42c65avp")))

(define-public crate-mod_interface-0.1.3 (c (n "mod_interface") (v "0.1.3") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1my9ag5khf4gcyp6z6z3i6p993bmwya3v1dcgw0l98agknbr3vvg")))

(define-public crate-mod_interface-0.1.4 (c (n "mod_interface") (v "0.1.4") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0l8gbaimjik6ck6jg007hmy8w06kw7rvw9p97k62xnscqn2xggb4") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.5 (c (n "mod_interface") (v "0.1.5") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "12rh56d8sb12b36l2x2b958nbwz6gdh2i32j1dxqmj9lv6ivy71r") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.6 (c (n "mod_interface") (v "0.1.6") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1hq41w6m89jbsq3y77i8n0x2mcknn5rqndxw9jjrybz45fvyjiwl") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.7 (c (n "mod_interface") (v "0.1.7") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0qil14b6wy0gzxvi8y614gsg3kzhqymhklqhszzkpzbrka7qqabj") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.8 (c (n "mod_interface") (v "0.1.8") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1wrzd5fw6jzicx0v359dascdf8dajpcb3zclawjlkwcgv9v6yfm2") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.9 (c (n "mod_interface") (v "0.1.9") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1dd86875kdxd3i2gpklshvk56kr95jm3j6cr60yph64xbczrdy92") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.10 (c (n "mod_interface") (v "0.1.10") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0ixs2vp680c2vyiahh24ipj4iy57h6rvyykx0433r6rz7f8bwv9f") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.13 (c (n "mod_interface") (v "0.1.13") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1vhs3pp5hjxkmwj68fqpwndw4xxp9p2fahrjj0nkhxcvxdsabw3l") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.15 (c (n "mod_interface") (v "0.1.15") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0x0xriq9zyd7ymxrhcczhw23njjpj33cvqd9nfywj1mmmm79p5z2") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.1.16 (c (n "mod_interface") (v "0.1.16") (d (list (d (n "mod_interface_meta") (r "~0.1") (d #t) (k 0)) (d (n "mod_interface_runtime") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0pwspa02sw4z4scyyjpqswxnkklafq3r7hbg9wm3s06rcxbvbmrm") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mod_interface-0.3.0 (c (n "mod_interface") (v "0.3.0") (d (list (d (n "mod_interface_meta") (r "^0.3.0") (d #t) (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "05sn98j2bhckl5jinsjjc953ql02ci6vp84il5jigz80iadmkzbz") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.4.0 (c (n "mod_interface") (v "0.4.0") (d (list (d (n "mod_interface_meta") (r "^0.3.0") (k 0)) (d (n "test_tools") (r "^0.3.0") (d #t) (k 2)))) (h "0f6m7k9kcmgfv0w8r0s0bbzl0gqysplvp9gajzl1knhnl3n81jr6") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.5.0 (c (n "mod_interface") (v "0.5.0") (d (list (d (n "mod_interface_meta") (r "~0.5.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1s5gwblapqf4rwv9xr89dnmfhgx1av1y9pkv1hx0priw9d5bjyfp") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.6.0 (c (n "mod_interface") (v "0.6.0") (d (list (d (n "mod_interface_meta") (r "~0.6.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1jvszjffl682r6jrhh6d0xhwcpj70wp1f8dj6rs2jckwlai7hfvx") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.7.0 (c (n "mod_interface") (v "0.7.0") (d (list (d (n "mod_interface_meta") (r "~0.7.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "06rrdz6qhy0xf4l400n5xg726qh213206alhrrp2kb6vc6szn9zs") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.8.0 (c (n "mod_interface") (v "0.8.0") (d (list (d (n "mod_interface_meta") (r "~0.8.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "14aicmd76qb2sc65w87ghkvwq4xvpgwpbs0minm577h932vgzach") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.9.0 (c (n "mod_interface") (v "0.9.0") (d (list (d (n "mod_interface_meta") (r "~0.9.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0s3sjw7wf70zy0zylslkkjv8z2nz2rm5v6la73ly0p0z6npdc1j9") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.10.0 (c (n "mod_interface") (v "0.10.0") (d (list (d (n "mod_interface_meta") (r "~0.10.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0ddpjjkfcqkvqr933skjzxqpjlc53799prmyny1kjsk8wdgnpw6l") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.11.0 (c (n "mod_interface") (v "0.11.0") (d (list (d (n "mod_interface_meta") (r "~0.11.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "07lbhbl5xz9fb5q2qayx9bz8x8ac95536yqhlj3sadzncyyzgpgp") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.12.0 (c (n "mod_interface") (v "0.12.0") (d (list (d (n "mod_interface_meta") (r "~0.12.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "142qh6zqj2akjajmki9m5mijnx8qgfds1h89npky6pbxfxhm3s1q") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.13.0 (c (n "mod_interface") (v "0.13.0") (d (list (d (n "mod_interface_meta") (r "~0.13.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0bmrkjifigf76wv34g8n1l4588x60nwlscl06vm5gl0d11c4rqrx") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.14.0 (c (n "mod_interface") (v "0.14.0") (d (list (d (n "mod_interface_meta") (r "~0.14.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0b33ws7mqbq44k39qpj9g76c5x8rnwfkzzx2w9b1m7ybwvyqrr57") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.15.0 (c (n "mod_interface") (v "0.15.0") (d (list (d (n "mod_interface_meta") (r "~0.15.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1gn2mivm18jcgcr24dx6iqnnyxp6c4blh4b2xj0asj6jzsczxb1l") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.16.0 (c (n "mod_interface") (v "0.16.0") (d (list (d (n "mod_interface_meta") (r "~0.16.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1cvl5gcq4avqlr0p4aihrhns9i3n3k67r0q5w77pycvdga5j0chj") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.17.0 (c (n "mod_interface") (v "0.17.0") (d (list (d (n "mod_interface_meta") (r "~0.17.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1hjq9amw206416n9ip83pmmmspqipshv490ymgprhyaklzs1hgcy") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.18.0 (c (n "mod_interface") (v "0.18.0") (d (list (d (n "mod_interface_meta") (r "~0.18.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1wf2kw90w79f7kg6mf09x32fki8zh7pry4p7704pl2d7b70r0pd8") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

(define-public crate-mod_interface-0.19.0 (c (n "mod_interface") (v "0.19.0") (d (list (d (n "mod_interface_meta") (r "~0.19.0") (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)))) (h "1jfx02r4m5nwg5ymvv99lb5idnzq4bxph92jnm88lsw3iqjzla62") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "mod_interface_meta/enabled") ("default" "enabled"))))))

