(define-module (crates-io mo d_ mod_language) #:use-module (crates-io))

(define-public crate-mod_language-0.1.0 (c (n "mod_language") (v "0.1.0") (h "0frikfgbqqd0s5v99zhmggr9y3633ib907lycxs4dj1vmsivjgsq") (y #t)))

(define-public crate-mod_language-0.1.2 (c (n "mod_language") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "mod_utilities") (r "^0.1.2") (d #t) (k 0)))) (h "117qhbcllq44yxk6p1hx08n3b8dzih37bw24cvdnm43qs1k7map3") (y #t)))

(define-public crate-mod_language-0.1.21 (c (n "mod_language") (v "0.1.21") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mod_utilities") (r "^0.1.2") (d #t) (k 0)))) (h "1xsfg5azyz7hv3kk4pshk30f2b4lcb6j92zfwrprqqxjr521ah2v") (y #t)))

(define-public crate-mod_language-0.1.22 (c (n "mod_language") (v "0.1.22") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mod_utilities") (r "^0.1.2") (d #t) (k 0)))) (h "1mgjw4la1lkghp6y3j5py31wsjjzc7bbjmq4qyn4x2sinn95g1pd") (y #t)))

