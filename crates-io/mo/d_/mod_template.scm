(define-module (crates-io mo d_ mod_template) #:use-module (crates-io))

(define-public crate-mod_template-0.1.0 (c (n "mod_template") (v "0.1.0") (d (list (d (n "mod_template_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1qi7zrxfykk04nrgrnkdk457hfj1ndhd4h3xfcxpj9b6c8nhyrk7")))

(define-public crate-mod_template-0.1.1 (c (n "mod_template") (v "0.1.1") (d (list (d (n "mod_template_macros") (r "^0.1.1") (d #t) (k 0)))) (h "11sajz65q86n8xbwvg637n8xhbk6hci8l7yrm3sb0i462mapxqc3")))

(define-public crate-mod_template-0.2.0 (c (n "mod_template") (v "0.2.0") (d (list (d (n "mod_template_macros") (r "^0.2.0") (d #t) (k 0)))) (h "150m08d2dj752inrkmdi066ja5hq8bfcsc8kklhr22y4vvs54l42")))

(define-public crate-mod_template-0.3.0 (c (n "mod_template") (v "0.3.0") (d (list (d (n "mod_template_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1d2wqsxh2fd1lkzaqfxd11dkbnf909pm1xszv7j4bijn4qf7vxzf")))

