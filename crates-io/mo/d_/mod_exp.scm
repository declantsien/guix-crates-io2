(define-module (crates-io mo d_ mod_exp) #:use-module (crates-io))

(define-public crate-mod_exp-0.1.0 (c (n "mod_exp") (v "0.1.0") (d (list (d (n "num") (r "*") (k 0)))) (h "07kz5jmqlxccdyih2x9x2flfngwjj3jklfxcyxbcg07khkvgijzv")))

(define-public crate-mod_exp-1.0.0 (c (n "mod_exp") (v "1.0.0") (d (list (d (n "num") (r "*") (k 0)))) (h "1abjxi8bc6fdyxhhm8qhy239ipkw4f1zvyyykqw3y6w2gpamhd7y")))

(define-public crate-mod_exp-1.0.1 (c (n "mod_exp") (v "1.0.1") (d (list (d (n "num") (r "^0.2.0") (k 0)))) (h "1c6qn118n804p94bv88knhzifjl1klszdchp2j6wz7zpdankyjmv")))

