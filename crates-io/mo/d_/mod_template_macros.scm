(define-module (crates-io mo d_ mod_template_macros) #:use-module (crates-io))

(define-public crate-mod_template_macros-0.1.0 (c (n "mod_template_macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 2)))) (h "0maj31qbjgk81zdcxhdzhszq8cfa3rg9lnjrbgcjmxbigycrilqq")))

(define-public crate-mod_template_macros-0.1.1 (c (n "mod_template_macros") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 2)))) (h "15pk51zlbw8yhljis4gb21clrqs539xcgpxkgszz1nahmm9jwgsz")))

(define-public crate-mod_template_macros-0.2.0 (c (n "mod_template_macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 2)))) (h "1wsm58fvyj0w5rkmx466674w1mqyivlmmjkh1ak0dm14pm1k5482")))

(define-public crate-mod_template_macros-0.3.0 (c (n "mod_template_macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 2)))) (h "0fddkggzb27szl63cbg6q8khq05m0n3xsq1aifi5ylzj5vbm7zaf")))

