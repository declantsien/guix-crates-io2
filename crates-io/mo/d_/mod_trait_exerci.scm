(define-module (crates-io mo d_ mod_trait_exerci) #:use-module (crates-io))

(define-public crate-mod_trait_exerci-0.1.0 (c (n "mod_trait_exerci") (v "0.1.0") (h "0sdalqixcb3m483bq2y3dq4323ymi36abdhx4k39c5iw84mj9cfx")))

(define-public crate-mod_trait_exerci-0.1.1 (c (n "mod_trait_exerci") (v "0.1.1") (h "1l062bi91za0q5akkkqllvrzd18dwaxv83nwizr1kvb1v54ljxv5")))

(define-public crate-mod_trait_exerci-0.1.2 (c (n "mod_trait_exerci") (v "0.1.2") (h "1adxmslji5aa4ciw8a9sma33mv4wlzkyxni62xmhxyki74jx6963")))

(define-public crate-mod_trait_exerci-0.2.0 (c (n "mod_trait_exerci") (v "0.2.0") (h "09ajgj5p5a4ydvclppc6zv8harr4lxqhwwwrb0xzyv7q2462sy32")))

(define-public crate-mod_trait_exerci-0.2.1 (c (n "mod_trait_exerci") (v "0.2.1") (h "0xglipyq10wqm1df49ng3aj01k6h4mgnakza11vxs7isfiq9n8rb")))

