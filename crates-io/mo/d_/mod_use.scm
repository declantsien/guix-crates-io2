(define-module (crates-io mo d_ mod_use) #:use-module (crates-io))

(define-public crate-mod_use-0.1.0 (c (n "mod_use") (v "0.1.0") (h "1vj2rpv40pcg5n38xq8q3phzvn2xj7dy9d8gdb60m7bbdx1xj4kz")))

(define-public crate-mod_use-0.2.0 (c (n "mod_use") (v "0.2.0") (h "05w5458vfw9f1dfsi5b95da4i9zzqs88zjxa2cki536vriwq4p1z")))

(define-public crate-mod_use-0.2.1 (c (n "mod_use") (v "0.2.1") (h "0f9mcfr0cxhki6rk7gmkd0m885mgfd3l4d9zbcpiry9c565fjpnr")))

