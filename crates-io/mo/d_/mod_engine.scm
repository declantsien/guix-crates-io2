(define-module (crates-io mo d_ mod_engine) #:use-module (crates-io))

(define-public crate-mod_engine-0.1.0 (c (n "mod_engine") (v "0.1.0") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (f (quote ("ttf" "image" "gfx" "mixer"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1irrh30w914187jv4fvgwrygm7jhwkniv2q8lsb2q6jqr4g6fsk5") (y #t)))

