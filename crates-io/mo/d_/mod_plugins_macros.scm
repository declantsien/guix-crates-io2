(define-module (crates-io mo d_ mod_plugins_macros) #:use-module (crates-io))

(define-public crate-mod_plugins_macros-0.1.0 (c (n "mod_plugins_macros") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0rkq97sa782ain0yg0syxjpk6h1g76wkmaylxsnrq011i4svv08a")))

