(define-module (crates-io mo d_ mod_path) #:use-module (crates-io))

(define-public crate-mod_path-0.1.0 (c (n "mod_path") (v "0.1.0") (h "1nyd8d4vnz9jdp6iv08yr00wclax87w8gya8xyk7in7id0siy3sf")))

(define-public crate-mod_path-0.1.1 (c (n "mod_path") (v "0.1.1") (h "0kk309vlx93lj14nv4aghmv22mnbl72rvj2ja7w3j33d7djkq7zf")))

(define-public crate-mod_path-0.1.2 (c (n "mod_path") (v "0.1.2") (h "0xwsbg52c026cbqymi30009k0yphrw49pp5li48zsjrnl512qs31")))

(define-public crate-mod_path-0.1.3 (c (n "mod_path") (v "0.1.3") (h "050c5l7hk6kfp8llg42ychka1zhp2rzvpigma1lfp6dryr6kx4cw")))

(define-public crate-mod_path-0.1.4 (c (n "mod_path") (v "0.1.4") (h "0q6irm0qgyx38b4zmw85bm4pw1qvy1i8i5mfjvb9dx2gbldrpnxw")))

(define-public crate-mod_path-0.1.5 (c (n "mod_path") (v "0.1.5") (h "0yqgij2m866drcaq5wyrj81ic93aslv4cjqqbwdsxdqy9b8c5497")))

(define-public crate-mod_path-0.1.6 (c (n "mod_path") (v "0.1.6") (h "1b6pwa1lskvzbc67ra4w4i60f3ianm3s1klfkqjy1dp5hig11isl")))

