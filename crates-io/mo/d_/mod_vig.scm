(define-module (crates-io mo d_ mod_vig) #:use-module (crates-io))

(define-public crate-mod_vig-0.1.0 (c (n "mod_vig") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "modernized_vigenere") (r "^0.1") (d #t) (k 0)))) (h "1qr65m4d9c40lnqah1d3lgizzi3gbzz8dxasg9zmfh9wx42yl708")))

(define-public crate-mod_vig-0.1.1 (c (n "mod_vig") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "modernized_vigenere") (r "^0.1") (d #t) (k 0)))) (h "19hb5c56z55vmz4p49xrcl21w5ickxaqxdq5x52b1vjw38djmjzq")))

(define-public crate-mod_vig-0.1.2 (c (n "mod_vig") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "modernized_vigenere") (r "^0.1") (d #t) (k 0)))) (h "0ij1j3ns3ilq762535bik0p86v47syspwhkqh0y4f7zsmr2wdcwk")))

