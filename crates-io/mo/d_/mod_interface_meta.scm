(define-module (crates-io mo d_ mod_interface_meta) #:use-module (crates-io))

(define-public crate-mod_interface_meta-0.1.0 (c (n "mod_interface_meta") (v "0.1.0") (d (list (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "12hsfy8yidmqay7nak5wdr9syx9wh9b992fjma0y12b1vx4h3jf3")))

(define-public crate-mod_interface_meta-0.1.1 (c (n "mod_interface_meta") (v "0.1.1") (d (list (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "0bx8drzbh05q3wyczsa5pzrc814apb6abyy8rnx1kxb92ppsks3z")))

(define-public crate-mod_interface_meta-0.1.2 (c (n "mod_interface_meta") (v "0.1.2") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "013prg39czq1lnq8iba9iy5g19hz227ck4lb3c2ania0sggfj4rf") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.3 (c (n "mod_interface_meta") (v "0.1.3") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "05548i342fh8fvialmjpvjwzw6rpz7f4r75jvmxhvcf13ysxirrx") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.4 (c (n "mod_interface_meta") (v "0.1.4") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "0ajl73264z0383m2c2v06bkq69bjp0n5nfqnnpp1crw52vmgkcb2") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.5 (c (n "mod_interface_meta") (v "0.1.5") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "1pzylxmi9fm664qldsh3cvgqsy98k6y7yla5ndkq6hnxk3841yhi") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.7 (c (n "mod_interface_meta") (v "0.1.7") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "0hfmja8v22fsjcms06ckh7nhwbcb2q7bsq8b6lm3h260a8dmgbdz") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.8 (c (n "mod_interface_meta") (v "0.1.8") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "1nfrrnbivn1mchwwkr0hkk4jvarpw9ivjx1bqs9bjfa7y870q8x1") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.9 (c (n "mod_interface_meta") (v "0.1.9") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "194sw2i54mwy1ngq7wdhh7wyhnxgwwg07mhypmi63g5s13drr39h") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.1.11 (c (n "mod_interface_meta") (v "0.1.11") (d (list (d (n "derive_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "0bylnf23khjmrg85gnlbjl4ymar4y4vnnqlaa1w38cgcfy6aqm4q") (f (quote (("full") ("default"))))))

(define-public crate-mod_interface_meta-0.2.0 (c (n "mod_interface_meta") (v "0.2.0") (d (list (d (n "derive_tools") (r "^0.2.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "0k1hfnqaf07rpbkda3gna2z7mywqfln0pcizgpigffvy74bhsnm0") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.3.0 (c (n "mod_interface_meta") (v "0.3.0") (d (list (d (n "derive_tools") (r "^0.2.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "0rvh5jdqdvlp9kx56bhsvpl1bll74824vzhb5r1nznff5c7yh9id") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.5.0 (c (n "mod_interface_meta") (v "0.5.0") (d (list (d (n "derive_tools") (r "~0.7.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.3.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0fgsqvsq6ismbd54nm0kz95nwkvbr45m4xb869klmiak0q4aln3f") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.6.0 (c (n "mod_interface_meta") (v "0.6.0") (d (list (d (n "derive_tools") (r "~0.7.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.3.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1x70a7x1kja0amxlz5cs86rn3x778bjlsca8m31x7cnmqlrihyxm") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.7.0 (c (n "mod_interface_meta") (v "0.7.0") (d (list (d (n "derive_tools") (r "~0.9.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.4.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "03dfyypw0f3jjr4nc22xgw10wf4jvl4slxkyhnm352cs2q57i6ik") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.8.0 (c (n "mod_interface_meta") (v "0.8.0") (d (list (d (n "derive_tools") (r "~0.10.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.4.0") (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1g1nd4rxpsy74r34llw6rffwz4f1p1wvwgavfrqj12rbqp1hvqri") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.9.0 (c (n "mod_interface_meta") (v "0.9.0") (d (list (d (n "derive_tools") (r "~0.11.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.5.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1412nbzza7nppl1lmnsgg3j4mzwd8ygv6jzrhm1i1hwg3bzs2d0v") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.10.0 (c (n "mod_interface_meta") (v "0.10.0") (d (list (d (n "derive_tools") (r "~0.12.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.6.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0fayzbh7lckl4zridswp0hymyd3kkshh83wzp2rxnn5dz88vfjpz") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.11.0 (c (n "mod_interface_meta") (v "0.11.0") (d (list (d (n "derive_tools") (r "~0.13.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.8.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1hc0smmiqw302ml08d8rxh24g3133pq29ws0wy5dj1wpyy9grkj5") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.12.0 (c (n "mod_interface_meta") (v "0.12.0") (d (list (d (n "derive_tools") (r "~0.14.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.16.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0id20pfsa2cy9na9lbpxcg65sxbdf76ww884n544ch82rizbw7q7") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.13.0 (c (n "mod_interface_meta") (v "0.13.0") (d (list (d (n "derive_tools") (r "~0.15.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.18.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "14l941jrb5br9yn5xpzvlyq2kijfgbz1w20s7yphhvwikbginyxh") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.14.0 (c (n "mod_interface_meta") (v "0.14.0") (d (list (d (n "derive_tools") (r "~0.17.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.19.0") (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0r2089m6ldkbyw6ycs6w7pik9a1gsr59y0sqvn6339q9sa1164qn") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.15.0 (c (n "mod_interface_meta") (v "0.15.0") (d (list (d (n "derive_tools") (r "~0.18.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.20.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "062v5ja1j6h56z4ix60xw8dgzcr7dmz188sna65rih8lz86qc5r5") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.16.0 (c (n "mod_interface_meta") (v "0.16.0") (d (list (d (n "derive_tools") (r "~0.19.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.21.0") (k 0)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "14wcbd3j6cfsb793j7a9jag56m8l85cq83kma2ypnhg3h6cxgzih") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.17.0 (c (n "mod_interface_meta") (v "0.17.0") (d (list (d (n "derive_tools") (r "~0.20.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.23.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1j1hycj9gk3mjvalicqkiaxd6vvm6gxqshs2f42k6g2viyg5xmhr") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.18.0 (c (n "mod_interface_meta") (v "0.18.0") (d (list (d (n "derive_tools") (r "~0.21.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.24.0") (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "08bffbw3aqdjxffhanb7zgdm618i263i2r7j6cvmbv3sldiibygg") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mod_interface_meta-0.19.0 (c (n "mod_interface_meta") (v "0.19.0") (d (list (d (n "derive_tools") (r "~0.22.0") (f (quote ("enabled" "enabled" "derive_is_variant"))) (k 0)) (d (n "macro_tools") (r "~0.24.0") (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)))) (h "1w5b1a2glvfavgwp4lbfm30cjqd4ha13f4paqac1b7wmzgblf7nm") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

