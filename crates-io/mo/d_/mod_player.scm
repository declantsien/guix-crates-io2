(define-module (crates-io mo d_ mod_player) #:use-module (crates-io))

(define-public crate-mod_player-0.1.0 (c (n "mod_player") (v "0.1.0") (d (list (d (n "cpal") (r "^0.8.2") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)))) (h "0gdy2vq8sr6411cap6vjp69hccrfj45ivmcmhys3gqyh8mzw1cs5")))

(define-public crate-mod_player-0.1.1 (c (n "mod_player") (v "0.1.1") (d (list (d (n "cpal") (r "^0.8.2") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)))) (h "15b1k9n9bxk8y4xcp3y654rr8n3ih53m6l7jpks2m1fh67wms28d")))

(define-public crate-mod_player-0.1.2 (c (n "mod_player") (v "0.1.2") (d (list (d (n "cpal") (r "^0.8.2") (d #t) (k 2)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1h2g376mrmfyac728j0k97ks4vpnnajf2mbnqwj9674750cvav7h")))

(define-public crate-mod_player-0.1.3 (c (n "mod_player") (v "0.1.3") (d (list (d (n "cpal") (r "^0.8.2") (d #t) (k 2)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "18j4psvi13xfc5jyy30baj2wnhi7q4rlf47as30372l274a564m1")))

(define-public crate-mod_player-0.1.4 (c (n "mod_player") (v "0.1.4") (d (list (d (n "cpal") (r "^0.8.2") (d #t) (k 2)) (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0ai77hxajajzylqnkfv2wnwqjzidzlpvn0rg5aad97k4g4mg2ryp")))

