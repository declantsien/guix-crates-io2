(define-module (crates-io mo d_ mod_plugins) #:use-module (crates-io))

(define-public crate-mod_plugins-0.1.0 (c (n "mod_plugins") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.1") (d #t) (k 0)) (d (n "mod_plugins_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "mod_plugins_resources") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0slgnihgjijrmyq416yvb646g8xqwpg0pqhzxw2z65grq8c20l3v")))

