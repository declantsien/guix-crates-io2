(define-module (crates-io mo ir moira) #:use-module (crates-io))

(define-public crate-moira-0.1.0 (c (n "moira") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "15jaink8nrxgfrhqpkpxia4q9877jyf0c04jm5lbznqg2sanx492")))

(define-public crate-moira-0.2.0 (c (n "moira") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1xp9cpsn9wbq62ab3hd9b3x605j5qji5680rf3zcgm8cfl2sqlwk")))

