(define-module (crates-io mo ir moiradb) #:use-module (crates-io))

(define-public crate-moiradb-0.2.0 (c (n "moiradb") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.24") (f (quote ("full"))) (d #t) (k 0)))) (h "0mzq6ppklhg4mvwms61cxprk5a3m66q0q1isa6pg62z7dhysph87")))

