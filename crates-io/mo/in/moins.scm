(define-module (crates-io mo in moins) #:use-module (crates-io))

(define-public crate-moins-0.1.0 (c (n "moins") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0kqk7cisv4cl7i3r73kg5mxxkqmkxqnafggaham8crw52k9km9j1")))

(define-public crate-moins-0.1.1 (c (n "moins") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "09b0wdy22syx7s0mh54b4fsa1zsqgv6aha30g47d1i3ni73b0d84")))

(define-public crate-moins-0.1.2 (c (n "moins") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "08dfff7qal61ddd9f6s19qlsq9lw1xhzzx1hld8a3daqlknyfr78")))

(define-public crate-moins-0.4.0 (c (n "moins") (v "0.4.0") (d (list (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "07hkkg4fvdc4xj57cih7d3lk3dwqbharbih4cch9v2ik9v5m40bc")))

(define-public crate-moins-0.5.0 (c (n "moins") (v "0.5.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1va9i3cpkm6zq2fsc4jds8kgbg7gnzjlf1r1aqr2dmlnl18nrqkr")))

