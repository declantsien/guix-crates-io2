(define-module (crates-io mo rd mordhau-loadout-parser) #:use-module (crates-io))

(define-public crate-mordhau-loadout-parser-0.1.0 (c (n "mordhau-loadout-parser") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0a8gqf154h4sy4za4pckl9ibvdrrw3yi4qw4xpxm2azyw8s0yhfr")))

