(define-module (crates-io mo ka moka-cht) #:use-module (crates-io))

(define-public crate-moka-cht-0.5.0 (c (n "moka-cht") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 2)) (d (n "lock_api") (r "^0.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)))) (h "1q8s5qmi6nwglp5ydjk2ssfnq7a6ily979kmvvpzxg2xaza9d0il") (f (quote (("num-cpus" "num_cpus") ("default" "num-cpus"))))))

(define-public crate-moka-cht-0.4.2 (c (n "moka-cht") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 2)) (d (n "lock_api") (r "^0.3.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 2)))) (h "1spy0dz6k1af3djhmqi3x7sij4x9q2fichj8hb8ldw6za8zlp7l2") (f (quote (("num-cpus" "num_cpus") ("default" "num-cpus"))))))

