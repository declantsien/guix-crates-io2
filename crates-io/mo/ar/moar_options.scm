(define-module (crates-io mo ar moar_options) #:use-module (crates-io))

(define-public crate-moar_options-0.1.0 (c (n "moar_options") (v "0.1.0") (h "03vgfrw5nh5iz7fp28jpywcv4i7xap0x1c2c7hy3mnx4xjhgiwwk")))

(define-public crate-moar_options-0.1.1 (c (n "moar_options") (v "0.1.1") (h "0f11rbyr31z4im5xghaikn41bn9048fb9p1nscbznyr4p3khkc7p")))

