(define-module (crates-io mo p- mop-bindings) #:use-module (crates-io))

(define-public crate-mop-bindings-0.0.8 (c (n "mop-bindings") (v "0.0.8") (d (list (d (n "js-sys") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "mop") (r "^0.0.8") (d #t) (k 0)) (d (n "mop-common-deps") (r "^0.0.8") (d #t) (k 0)) (d (n "mop-structs") (r "^0.0.8") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.29") (o #t) (d #t) (k 0)))) (h "1w0qx1sg87v4blqgg915i8fxvsx63jlc62r5fl6l93h33psahzky") (f (quote (("wasm" "js-sys" "mop-common-deps/rand" "mop-common-deps/std" "mop-common-deps/wasm-bindgen1" "mop-structs/rand" "mop-structs/std" "mop/std" "wasm-bindgen") ("default" "wasm"))))))

