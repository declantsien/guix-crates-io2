(define-module (crates-io mo p- mop-common-defs) #:use-module (crates-io))

(define-public crate-mop-common-defs-0.0.8 (c (n "mop-common-defs") (v "0.0.8") (h "09zrqjxvc2a9wn7q30kpjbbzpb40znxqzl24rx9iyk42n2z0v81a") (f (quote (("parallel") ("default"))))))

(define-public crate-mop-common-defs-0.0.9 (c (n "mop-common-defs") (v "0.0.9") (h "0gh0d2xxqvlnccsy591vjccx2nm6ncx3aqi2l4579vxfw53dzklf") (f (quote (("parallel") ("default"))))))

(define-public crate-mop-common-defs-0.0.10 (c (n "mop-common-defs") (v "0.0.10") (h "18prpbs73lrm3hslx5jwy566k8ld159a7pn0abxlpx9j1hcpj61h") (f (quote (("parallel") ("default"))))))

(define-public crate-mop-common-defs-0.1.0 (c (n "mop-common-defs") (v "0.1.0") (h "0nm0p87imhhcw2hrrssziixskf76ghgk9h8mc7xlc8fk8a0bjgd7") (f (quote (("with_futures") ("std") ("default"))))))

(define-public crate-mop-common-defs-0.2.0 (c (n "mop-common-defs") (v "0.2.0") (h "1736kybh8rjdjpqqg8zpsppjl02919bpzxpr4pml1kjbkfbb3l2a") (f (quote (("with_futures") ("std") ("default"))))))

