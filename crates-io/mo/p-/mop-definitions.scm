(define-module (crates-io mo p- mop-definitions) #:use-module (crates-io))

(define-public crate-mop-definitions-0.0.2 (c (n "mop-definitions") (v "0.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "mop") (r "^0.0.2") (d #t) (k 0)) (d (n "mop-common") (r "^0.0.2") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vwyy7mwp5kmf13vdn2i05kwxlvhda11iv77g8qy2dfjgbf8fsif")))

