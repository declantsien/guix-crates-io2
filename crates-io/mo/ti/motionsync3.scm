(define-module (crates-io mo ti motionsync3) #:use-module (crates-io))

(define-public crate-motionsync3-0.1.0 (c (n "motionsync3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bvyymhv4dfh17ha5kmmni694f342spq9jnr1i6adi75a3wb8w3b")))

(define-public crate-motionsync3-0.2.0 (c (n "motionsync3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0zamws624k086kv09kn1l8bjbhvf1k3ixri1zjdnkn2ka1l1gyyn")))

