(define-module (crates-io mo ti motion-planning-cli) #:use-module (crates-io))

(define-public crate-motion-planning-cli-0.1.0-alpha.1 (c (n "motion-planning-cli") (v "0.1.0-alpha.1") (d (list (d (n "motion-planning") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1hgx3a7crwafj90xz9ngwv8z1c1in2nlf0m7wrkwzpj434f92q3y")))

(define-public crate-motion-planning-cli-0.1.0-alpha.5 (c (n "motion-planning-cli") (v "0.1.0-alpha.5") (d (list (d (n "motion-planning") (r "^0.1.0-alpha.5") (d #t) (k 0)))) (h "0mv27sc53fzp5clci7hryhg31vgzl94qfpfk3zc3x9w07gm70ydf")))

