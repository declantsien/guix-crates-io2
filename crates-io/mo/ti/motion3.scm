(define-module (crates-io mo ti motion3) #:use-module (crates-io))

(define-public crate-motion3-0.1.0 (c (n "motion3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f4mny359wchfm8s7f3qh8jx35ni60728qn35a0j5wkyrpczn7k6")))

(define-public crate-motion3-0.2.0 (c (n "motion3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1ab13yayzwqaiji0wrlczs04mdhzhmbycaw672dqjmq4083i01dd")))

