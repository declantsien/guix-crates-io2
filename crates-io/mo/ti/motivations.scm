(define-module (crates-io mo ti motivations) #:use-module (crates-io))

(define-public crate-motivations-1.0.0 (c (n "motivations") (v "1.0.0") (h "006wf6q7df10848wdg5qyh61j8nj9sy7p4b2nbm3s9yr25ffi5kn")))

(define-public crate-motivations-1.0.1 (c (n "motivations") (v "1.0.1") (h "0bkbcjl72ydn684x7l1jyxxfng8wwrdffr711vxcss34c8azdb95")))

(define-public crate-motivations-1.0.2 (c (n "motivations") (v "1.0.2") (h "0wzlcqlgwmpp72sb21wk4cvfzkqwb7qj1qwg61ggzfzj9wfja3wv")))

(define-public crate-motivations-1.1.2 (c (n "motivations") (v "1.1.2") (h "1z0z7xah6n2rz4sr4wvsam55fnkaxxmwa08i79wq39d6zyqd0plp")))

