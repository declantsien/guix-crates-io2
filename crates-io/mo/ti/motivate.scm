(define-module (crates-io mo ti motivate) #:use-module (crates-io))

(define-public crate-motivate-0.1.0 (c (n "motivate") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "ureq") (r "^1.5.5") (d #t) (k 0)))) (h "15qydnxdvsbbk306hl4s072h8qn75gyx9k16is6cpiqjbj4v2fi6")))

