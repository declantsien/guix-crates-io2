(define-module (crates-io mo ti motion-planning) #:use-module (crates-io))

(define-public crate-motion-planning-0.1.0-alpha.1 (c (n "motion-planning") (v "0.1.0-alpha.1") (h "12alpw3mni74j1myivn6pkcnxqma04gbi58ycb5gk0wnbx9xzpzq")))

(define-public crate-motion-planning-0.1.0-alpha.5 (c (n "motion-planning") (v "0.1.0-alpha.5") (h "0j3ahpw35mqvf79vx9fpz44ar651vkmp5bhf1hdmbvpzprbp56ri")))

