(define-module (crates-io mo oe mooeye) #:use-module (crates-io))

(define-public crate-mooeye-0.2.0 (c (n "mooeye") (v "0.2.0") (d (list (d (n "ggez") (r "^0.9.0-rc0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "0k1gcysi8xsassfpji9dyshkmdjvazxba5j0kmcdldsxh1pm7h06")))

(define-public crate-mooeye-0.2.1 (c (n "mooeye") (v "0.2.1") (d (list (d (n "ggez") (r "^0.9.0-rc0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "0q8xca0k5sbbxcvcrspkyb6nl9q57bdcma637xbac8537znpi6jd")))

(define-public crate-mooeye-0.3.0 (c (n "mooeye") (v "0.3.0") (d (list (d (n "ggez") (r "^0.9.0-rc0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "1n1f0xncc6vjmi7hv32f2xaqjj5n44ydd0jfdnhrkb9vr9kxp91c")))

(define-public crate-mooeye-0.3.1 (c (n "mooeye") (v "0.3.1") (d (list (d (n "ggez") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "0n3s22rh4x0y5ljqp5s42pc5fc9bdgjmkr7167yvwm1r5s9lclks")))

(define-public crate-mooeye-0.3.2 (c (n "mooeye") (v "0.3.2") (d (list (d (n "ggez") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "0095v729x29qbhyyiin8qczgr2vjmnaw6y3sjdpgyik5ix530f2f")))

(define-public crate-mooeye-0.4.0 (c (n "mooeye") (v "0.4.0") (d (list (d (n "ggez") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "15z9waqqgd0r0sifa6h53j2m4ac9lfvafppmziw2acs2xk67s6nd")))

(define-public crate-mooeye-0.4.1 (c (n "mooeye") (v "0.4.1") (d (list (d (n "ggez") (r "=0.9.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "0s85lhcv8b6nrcn8rdj2iaqw4zgbpg2lipzklvl536cj9i1vr8ds")))

