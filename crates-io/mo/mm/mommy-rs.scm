(define-module (crates-io mo mm mommy-rs) #:use-module (crates-io))

(define-public crate-mommy-rs-0.1.0 (c (n "mommy-rs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1w4c5lsmwn666xgr3l76p3jnhw2hald2zgq0gjqp4xqi5qwclmlg")))

