(define-module (crates-io mo st mostinefficientsha) #:use-module (crates-io))

(define-public crate-mostinefficientsha-0.1.0 (c (n "mostinefficientsha") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.16") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)))) (h "0p18p1cwdv1zs2d0a7jijz1y9d1799y5dfkq7ircfbs1jbl4h8yl")))

