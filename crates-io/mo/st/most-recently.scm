(define-module (crates-io mo st most-recently) #:use-module (crates-io))

(define-public crate-most-recently-0.1.0 (c (n "most-recently") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "0hdvbvbs8rkjdldcd5dk509lq5q69dmjlr9id42kn312y09x2ah9")))

(define-public crate-most-recently-0.2.0 (c (n "most-recently") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "082868mf49ai181an751p4qlgsdq0zrc3dqnrvr4b4yrz2glkqyq")))

