(define-module (crates-io mo th mothra) #:use-module (crates-io))

(define-public crate-mothra-0.1.0 (c (n "mothra") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0k8fnhbk0iyb78k113b6yw297pm2z63fv3k81ifar6y46psdzdwh")))

(define-public crate-mothra-0.2.0 (c (n "mothra") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tabular") (r "^0.1.2") (d #t) (k 0)))) (h "00iy4h1ysyia2nb1cp39dzasj22wlgdav7h47q1haf0bgslalmv4")))

(define-public crate-mothra-0.3.0 (c (n "mothra") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tabular") (r "^0.1.2") (d #t) (k 0)))) (h "0kxwxczza8ybkxqmyiivxmr3d6qig1yg5hqgzfnnmlvz9wdk73dr")))

