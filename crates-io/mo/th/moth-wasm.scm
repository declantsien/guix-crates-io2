(define-module (crates-io mo th moth-wasm) #:use-module (crates-io))

(define-public crate-moth-wasm-1.0.0 (c (n "moth-wasm") (v "1.0.0") (d (list (d (n "lmfu") (r "^1.3.0") (d #t) (k 0)) (d (n "moth-wasm-macros") (r "^1.0.0") (d #t) (k 0)))) (h "108vwljx89rv1rph569axhpafbhamlmvadp0xim18q2j6j7g5qrr")))

