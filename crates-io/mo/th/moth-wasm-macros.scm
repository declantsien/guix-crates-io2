(define-module (crates-io mo th moth-wasm-macros) #:use-module (crates-io))

(define-public crate-moth-wasm-macros-1.0.0 (c (n "moth-wasm-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pcvbm9dh2ax8knyz1gsnvj8kbpghd5p62fgb2vgy304ar7kxqw1")))

