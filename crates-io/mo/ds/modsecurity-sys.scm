(define-module (crates-io mo ds modsecurity-sys) #:use-module (crates-io))

(define-public crate-modsecurity-sys-0.1.1-alpha.1 (c (n "modsecurity-sys") (v "0.1.1-alpha.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1b7kf6wvc83ljypcqnxprnvb43jfvgwa5bb22vfyqsvws9cc9clq")))

(define-public crate-modsecurity-sys-0.1.1-alpha.2 (c (n "modsecurity-sys") (v "0.1.1-alpha.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1k4yq782sv8257dnn93cd7n8w6g7sbrbn34g5zp5c67afpjgf4xx") (r "1.78.0")))

(define-public crate-modsecurity-sys-0.1.1-alpha.3 (c (n "modsecurity-sys") (v "0.1.1-alpha.3") (d (list (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0qn35vgz6kbzrq2ss5qd07dpa1bkgjyhldjylgb9khwyc1wrzagd")))

(define-public crate-modsecurity-sys-0.1.1 (c (n "modsecurity-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1z4q7503cfyi8wlaqhlrjhdid3xddxx51pv02av4v74c3ppn9k0l")))

(define-public crate-modsecurity-sys-0.1.2 (c (n "modsecurity-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "152yw6wyz2w4w5780c15izblczw6bm58p9j2mv9ra7kayyjz3x1g")))

