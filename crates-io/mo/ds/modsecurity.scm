(define-module (crates-io mo ds modsecurity) #:use-module (crates-io))

(define-public crate-modsecurity-0.1.1-alpha.2 (c (n "modsecurity") (v "0.1.1-alpha.2") (d (list (d (n "modsecurity-sys") (r "^0.1.1-alpha.2") (d #t) (k 0)))) (h "1c5ld9350zw6ibrvv8pa1asgi4hi5hnb5a0xkv4hli29alia8049") (r "1.78.0")))

(define-public crate-modsecurity-0.1.1-alpha.3 (c (n "modsecurity") (v "0.1.1-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modsecurity-sys") (r "^0.1.1-alpha.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1sv5faxxzvdgm27ygx3qmqip2m5lkw66vp5j58f84x1h2vl0qi78")))

(define-public crate-modsecurity-0.1.1 (c (n "modsecurity") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modsecurity-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1g6ydkp3smsz2zphq8v7wwkbxw1ikz81bymibv0bkmygzb0imkv1")))

(define-public crate-modsecurity-0.1.2 (c (n "modsecurity") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modsecurity-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12d383vcqgps6fg38ar05ay8hcxmn0r9fqvz5bklw7r20njbk194")))

(define-public crate-modsecurity-0.1.3 (c (n "modsecurity") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modsecurity-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1x0nk3kamihnkpb2w7apga1ycjpzy87bwix670cjhaxzba4mh2sg")))

(define-public crate-modsecurity-0.1.4 (c (n "modsecurity") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modsecurity-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gkbpip2jgrj6h8qriwp52q8hlhbia781g2q57qx7hn0pfmdmd2h")))

