(define-module (crates-io mo ds modsecurity-rs) #:use-module (crates-io))

(define-public crate-modsecurity-rs-0.1.0 (c (n "modsecurity-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 1)))) (h "11z1axlq1hwrclvkcgrgs9jy8zrv16lkaw8khyrsmkpaa3l6svc5") (l "modsecurity")))

(define-public crate-modsecurity-rs-0.1.1 (c (n "modsecurity-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 1)))) (h "1l7jrj14gq22i6a026yrrh4zw1cwvv957c8mmg2m4xmhxf7h1kif") (l "modsecurity")))

(define-public crate-modsecurity-rs-0.1.2 (c (n "modsecurity-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 1)))) (h "0ga6v8jbfn067lxg1afxzdahz9kl076417v83m43sx7vgknlnsxh") (l "modsecurity")))

(define-public crate-modsecurity-rs-0.1.3 (c (n "modsecurity-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 1)))) (h "1ksn9cyv6y27ca09p5k5ml2fb42x2v4w8yr1c85gjprkm15hcfgv") (l "modsecurity")))

(define-public crate-modsecurity-rs-0.1.4 (c (n "modsecurity-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26") (d #t) (k 1)) (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 1)))) (h "19ckj3mf8yv7pnpipv8vvrwb0jmz7fy77p85nd3189xqd61601hp") (l "modsecurity")))

