(define-module (crates-io mo nd mondrian) #:use-module (crates-io))

(define-public crate-mondrian-1.0.0 (c (n "mondrian") (v "1.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)) (d (n "svg") (r "^0.8") (d #t) (k 0)))) (h "177s264rynkj8np04lfkspljm4hn8bkhv8fnyhva48baly626cly") (f (quote (("default"))))))

(define-public crate-mondrian-1.1.0 (c (n "mondrian") (v "1.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)) (d (n "svg") (r "^0.8") (d #t) (k 0)))) (h "0ksnmxwj7jwqkfbmhn8liv2mlj6d3pls0kza9i8ly44lakd5w4ai") (f (quote (("default"))))))

