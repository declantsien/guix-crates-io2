(define-module (crates-io mo nd mond) #:use-module (crates-io))

(define-public crate-mond-0.1.0 (c (n "mond") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q9na2q0b4bzbl88glpljg6l6gfw3xg6is420rh8lcnkv4rkvfjm")))

