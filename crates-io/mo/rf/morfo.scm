(define-module (crates-io mo rf morfo) #:use-module (crates-io))

(define-public crate-morfo-0.1.0 (c (n "morfo") (v "0.1.0") (h "1z04dj6aa87q3jjgf9mh6ahs9d16cbf3ydsk3pnfq4mgs85z11sc")))

(define-public crate-morfo-0.2.0 (c (n "morfo") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0w3n6w8yms7qapx8qr4ibzhhhyirgjdcrb4n8lknrgqdg5zi6dwd")))

