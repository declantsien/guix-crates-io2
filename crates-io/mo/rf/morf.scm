(define-module (crates-io mo rf morf) #:use-module (crates-io))

(define-public crate-morf-0.1.0 (c (n "morf") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "chacha20") (r "^0.9") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "hkdf") (r "^0.12") (k 0)) (d (n "hmac") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "x25519-dalek") (r "^2") (f (quote ("static_secrets"))) (k 0)))) (h "1gvhc5m79dj42p6fx5fnv2yibbfyc17cpqw4h271nxw34gl2qpfp") (y #t)))

(define-public crate-morf-0.2.0 (c (n "morf") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "blake3") (r "^1") (k 0)) (d (n "chacha20") (r "^0.9") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "constant_time_eq") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "x25519-dalek") (r "^2") (f (quote ("static_secrets"))) (k 0)))) (h "02z59jaxx85jmmas0718bmm1a6qv208hd6lbms0i6srbhvfm5wm4")))

