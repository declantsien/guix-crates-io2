(define-module (crates-io mo bc mobc-arangors) #:use-module (crates-io))

(define-public crate-mobc-arangors-0.1.0 (c (n "mobc-arangors") (v "0.1.0") (d (list (d (n "arangors") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.5.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1r9p06pwlj5adr67hg818hk3v8k4irbkl775axhh2x40vr94w8yz")))

(define-public crate-mobc-arangors-0.2.0 (c (n "mobc-arangors") (v "0.2.0") (d (list (d (n "arangors") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.5.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "0ljqpib34g842p11izyfmdmf4fbk0d689d073ncfrlmxc03jqrww")))

(define-public crate-mobc-arangors-0.2.1 (c (n "mobc-arangors") (v "0.2.1") (d (list (d (n "arangors") (r ">=0.4.3") (o #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mobc") (r "^0.5.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "0w2xyqw64kk8hwlq8lf6phjchb49gva22jjl8hydkdcni60xaaji") (f (quote (("surf" "arangors/rocksdb" "arangors/surf_async") ("reqwest" "arangors/rocksdb" "arangors/reqwest_async") ("default" "reqwest"))))))

(define-public crate-mobc-arangors-0.2.2 (c (n "mobc-arangors") (v "0.2.2") (d (list (d (n "arangors") (r ">=0.4.3") (o #t) (k 0)) (d (n "futures") (r ">=0.3") (d #t) (k 0)) (d (n "mobc") (r ">=0.5.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1fd168g27dvk119y3lgrmp0b4h8dl1j21dksykwkijij3cqwv262") (f (quote (("surf" "arangors/rocksdb" "arangors/surf_async") ("reqwest" "arangors/rocksdb" "arangors/reqwest_async") ("default" "reqwest"))))))

