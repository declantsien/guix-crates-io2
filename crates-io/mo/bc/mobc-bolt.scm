(define-module (crates-io mo bc mobc-bolt) #:use-module (crates-io))

(define-public crate-mobc-bolt-0.0.0 (c (n "mobc-bolt") (v "0.0.0") (h "1bzbk8g6hzfyzj22a410grvil7x5xpmp36yz3jdp14386gh5nmla")))

(define-public crate-mobc-bolt-0.1.0 (c (n "mobc-bolt") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "bolt-client") (r "^0.10.0") (f (quote ("tokio-stream"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (f (quote ("alloc"))) (k 2)) (d (n "mobc") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.0") (f (quote ("compat"))) (d #t) (k 0)))) (h "1qrbhr782f0gr6x8c9fpb26b16633nl8x24xcnm5q8wms0mjivgk")))

(define-public crate-mobc-bolt-0.2.0 (c (n "mobc-bolt") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "bolt-client") (r "^0.11.0") (f (quote ("tokio-stream"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (f (quote ("alloc"))) (k 2)) (d (n "mobc") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("compat"))) (d #t) (k 0)))) (h "07pxqfhfkgcc0a9j88sj31zdmww23s01r4z08jnyak70w8pwvaz5")))

