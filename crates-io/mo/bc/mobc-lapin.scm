(define-module (crates-io mo bc mobc-lapin) #:use-module (crates-io))

(define-public crate-mobc-lapin-0.5.0 (c (n "mobc-lapin") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^1.2") (d #t) (k 0)) (d (n "mobc") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^0.1") (d #t) (k 2)))) (h "0fzhrr2xhbhv0lcnd7iljcb0v1df9ihnpyjifjs0ik8ch44bpxzq") (y #t)))

(define-public crate-mobc-lapin-0.5.1 (c (n "mobc-lapin") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^1.2") (d #t) (k 0)) (d (n "mobc") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^0.1") (d #t) (k 2)))) (h "1kndfmxn0a0pzhnhdvjnxlwh95sdlkwmf6f94v8qpk4qmdvdhffg")))

(define-public crate-mobc-lapin-0.5.2 (c (n "mobc-lapin") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^1.4") (d #t) (k 0)) (d (n "mobc") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^0.1") (d #t) (k 2)))) (h "1sf8sj18sxir5jyiykczdgc12zqhjc807qxq2akzss4js359ayhf")))

(define-public crate-mobc-lapin-0.6.0 (c (n "mobc-lapin") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^1.6") (d #t) (k 0)) (d (n "mobc") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^0.3") (d #t) (k 2)))) (h "0iqnpfm8vs7yl0x8fpbg9irrxamvy84fxam9g1vzmljpg9xspy3g")))

(define-public crate-mobc-lapin-0.7.0 (c (n "mobc-lapin") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^1.6") (d #t) (k 0)) (d (n "mobc") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^1.0") (d #t) (k 2)))) (h "09q155zq36szh5g4xpwj8pkl1vljdxyvhq8jz5a03hybvaz2023w")))

(define-public crate-mobc-lapin-0.8.0 (c (n "mobc-lapin") (v "0.8.0") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^2.1") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^2.0") (d #t) (k 2)) (d (n "tokio-executor-trait") (r "^2.1") (d #t) (k 2)))) (h "1yw38223mjm2sg9lrhqqhvfpxwnwyad4ps27sd957inmlc3gcl6d")))

(define-public crate-mobc-lapin-0.8.1 (c (n "mobc-lapin") (v "0.8.1") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "lapin") (r "^2.3") (d #t) (k 0)) (d (n "mobc") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-amqp") (r "^2") (d #t) (k 2)) (d (n "tokio-executor-trait") (r "^2") (d #t) (k 2)))) (h "08xjq11x5a0b1jsbj5mifcbl5y9prnbsv94i3cw5rdr4yfl6yr2p")))

