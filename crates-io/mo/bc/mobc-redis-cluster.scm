(define-module (crates-io mo bc mobc-redis-cluster) #:use-module (crates-io))

(define-public crate-mobc-redis-cluster-0.1.0 (c (n "mobc-redis-cluster") (v "0.1.0") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 2)) (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "mobc") (r "^0.7.2") (d #t) (k 0)) (d (n "redis") (r "^0.20.1") (d #t) (k 0)) (d (n "redis_cluster_async") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 2)))) (h "1dqckkv2cn6q78jnimpj16a627kf6lahqmlviis1xqmx5yki2k76")))

(define-public crate-mobc-redis-cluster-0.1.1 (c (n "mobc-redis-cluster") (v "0.1.1") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 2)) (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "mobc") (r "^0.7.2") (d #t) (k 0)) (d (n "redis") (r "^0.20.1") (d #t) (k 0)) (d (n "redis_cluster_async") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 2)))) (h "0chvlkv619j1ga9lgx285h39lyzd5ib42ji9dvnly6ar5bhpg8qw")))

