(define-module (crates-io mo bc mobc-diesel) #:use-module (crates-io))

(define-public crate-mobc-diesel-1.0.0 (c (n "mobc-diesel") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "diesel") (r "^1.4.5") (f (quote ("sqlite" "postgres"))) (d #t) (k 2)) (d (n "mobc") (r "^0.5.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "macros" "sync"))) (d #t) (k 2)))) (h "11fksvpk4a5qirbmrhrn9q7gxwhrkpm2rwbr6034hs82d8w0fyzy")))

