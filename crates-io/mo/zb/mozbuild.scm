(define-module (crates-io mo zb mozbuild) #:use-module (crates-io))

(define-public crate-mozbuild-0.0.0 (c (n "mozbuild") (v "0.0.0") (h "1c8aghbgrr7f4s19lhm6q9w9w3x1yqjp0agafbw8s49949lvfxq9")))

(define-public crate-mozbuild-0.1.0 (c (n "mozbuild") (v "0.1.0") (h "05l6w86bcga9hl734ynqwfklw35sz23z7y2c45sp53945yp70fch")))

