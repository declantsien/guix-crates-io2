(define-module (crates-io mo zi mozilla-ca-certs) #:use-module (crates-io))

(define-public crate-mozilla-ca-certs-0.1.3 (c (n "mozilla-ca-certs") (v "0.1.3") (d (list (d (n "nss-certdata-parser") (r "^0.6.2") (d #t) (k 1)) (d (n "untrusted") (r "^0.3.1") (o #t) (d #t) (k 1)) (d (n "webpki") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "1kfkyr070smraw2sncbpqxrskbf8ayyzx8z3q26sgmg0dhacv06g") (f (quote (("use_webpki" "webpki" "untrusted") ("default" "use_webpki"))))))

