(define-module (crates-io mo zi mozilla-readability) #:use-module (crates-io))

(define-public crate-mozilla-readability-0.1.1 (c (n "mozilla-readability") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0vm1hpzqjfl1ny4gfn2gwdr4bngq5zz9jzp0dcc14p3g3ycjjybl")))

