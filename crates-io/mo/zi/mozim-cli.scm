(define-module (crates-io mo zi mozim-cli) #:use-module (crates-io))

(define-public crate-mozim-cli-0.1.0 (c (n "mozim-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("color" "auto-color" "humantime"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mozim") (r "^0.2") (d #t) (k 0)) (d (n "nispor") (r "^1.2.10") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)))) (h "1r61jv66xm3c7jb7fm0ckacsgr0910mnv6d30fx4ms2r4xg8jknq")))

(define-public crate-mozim-cli-0.1.1 (c (n "mozim-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("color" "auto-color" "humantime"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mozim") (r "^0.2") (d #t) (k 0)) (d (n "nispor") (r "^1.2.10") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)))) (h "0g0d3xrigbv9ia5qg1s5v5mhp6ncyq82a9zvprvc8m5dkzjmd8ph")))

(define-public crate-mozim-cli-0.2.2 (c (n "mozim-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("color" "auto-color" "humantime"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mozim") (r "^0.2.2") (d #t) (k 0)) (d (n "nispor") (r "^1.2.10") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)))) (h "0xk85lpdbrakj2k6vlvgkv0dnigjb1ah9srqwbssw6qawlhi38an")))

