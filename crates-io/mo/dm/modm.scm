(define-module (crates-io mo dm modm) #:use-module (crates-io))

(define-public crate-modm-0.1.0 (c (n "modm") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 0)))) (h "0gdlrl8g9kn0aaxmqcjh2616yjzpng81qa1bk72563iv5rfvy0gk")))

(define-public crate-modm-0.1.1 (c (n "modm") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1nxyv89y3l28fdk4s683z7gyzflrnsffgzjq90hhmz5zbig9wjhj")))

