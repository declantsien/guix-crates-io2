(define-module (crates-io mo dm modmon) #:use-module (crates-io))

(define-public crate-modmon-0.1.0 (c (n "modmon") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04yyhfif1yzy0capz9vlx56a6ml24lqpx7dm519sx2ijij8m7flq")))

