(define-module (crates-io mo wn mownstr) #:use-module (crates-io))

(define-public crate-mownstr-0.1.0 (c (n "mownstr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "083wy24yxcgmf8zz6rcs8ndc73pzqcn48grk0lyjydpsgkbcxglf")))

(define-public crate-mownstr-0.1.1 (c (n "mownstr") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "112grspy6dqir2ldnmmssxg06bi32da8mzq87bqh1lnvh5w40ail")))

(define-public crate-mownstr-0.1.2 (c (n "mownstr") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05h59a2d0aha7pnya9xqvkjajpp172h4bmg53456ry5h855qym8m")))

(define-public crate-mownstr-0.1.3 (c (n "mownstr") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cx5rk564ikkxgxcpchfgmbvjabsq6zmmqrk9srrs2l5z7wcvakn")))

(define-public crate-mownstr-0.2.0 (c (n "mownstr") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0wwws0rq8r4hfgr44ykq77p76gpw5lv6kczcid2l808a7xa0cxy9")))

(define-public crate-mownstr-0.2.1 (c (n "mownstr") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "16258iyvvh1shh6il5vx78l1qhychp6hrcgz1jrdidcjc7lmri4b")))

