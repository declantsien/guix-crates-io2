(define-module (crates-io mo wn mown) #:use-module (crates-io))

(define-public crate-mown-0.1.0 (c (n "mown") (v "0.1.0") (h "11478i98hnbwggni0d7shmi3pcl963yw6z4qywa4dm7n4s75rwy2")))

(define-public crate-mown-0.1.1 (c (n "mown") (v "0.1.1") (h "18hh6bsncjcwlfnavmky3cyhz6brn40xlihhbk53zprqcb9skxv1")))

(define-public crate-mown-0.2.0 (c (n "mown") (v "0.2.0") (h "14hnnpmvvq2q66jnspg0fdh6805cahp859cr4lfhfrcrh3cfg97n")))

(define-public crate-mown-0.2.1 (c (n "mown") (v "0.2.1") (h "1r2mqdqia5svhw7cnwp59l5bnmzg43yas7kxapywdin7sc9dj7cc")))

(define-public crate-mown-0.2.2 (c (n "mown") (v "0.2.2") (h "0znrrgli003x46s175x81l281xjair422jzpqgqxnzmips5psqp7")))

(define-public crate-mown-1.0.0 (c (n "mown") (v "1.0.0") (h "1fcda3k0adbpgmx2fvmkkms5wr03ak8zjdkmyf3xc6r4xs65aq0l")))

