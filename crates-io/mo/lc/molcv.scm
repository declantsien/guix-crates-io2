(define-module (crates-io mo lc molcv) #:use-module (crates-io))

(define-public crate-molcv-0.1.0 (c (n "molcv") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)))) (h "10xni6czp6lb3ykhn52q5kklcavpmzzia167iz5d8iiy74d95dn3")))

