(define-module (crates-io mo lc molc) #:use-module (crates-io))

(define-public crate-molc-0.1.0 (c (n "molc") (v "0.1.0") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "16583cjddcqj45xgjyqg16m16l9jisq9mjs3zi97bjiimfd6d6x1")))

(define-public crate-molc-0.2.0 (c (n "molc") (v "0.2.0") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0zn5wjj8nfkmpmxgsds0qwr3qpdszyq5jaw200v5qr76y1v2nli8")))

(define-public crate-molc-0.2.1 (c (n "molc") (v "0.2.1") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1aif1mfhkfmmmfzix453jvxklxpylbhj0d38xrlj33d8cwnj31cl")))

(define-public crate-molc-0.2.2 (c (n "molc") (v "0.2.2") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1m8jbrs0d8wpgjpz8xq4d2s2qvx75vxd84cwax6rhipw7g8c0rmj")))

(define-public crate-molc-0.2.3 (c (n "molc") (v "0.2.3") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1sdccanp8r4rimg8h00l1f037vg8rrkwcrinrz8xj0x3yshm5p7b")))

(define-public crate-molc-0.2.4 (c (n "molc") (v "0.2.4") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "17rd6rrns0wd64pw5b9mr9rxq8pd0jxj96cgx472dzw8w0lfh785")))

(define-public crate-molc-0.2.5 (c (n "molc") (v "0.2.5") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1fn73bj7p4ws91vflmkajl4815i0r36j8gzpvkk9akcjmdspmpsd")))

(define-public crate-molc-0.2.6 (c (n "molc") (v "0.2.6") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0hhqh1z0lfa708i80lk61bggiflk7cqlbl3yp5b858fvq0aigd3n")))

(define-public crate-molc-0.2.7 (c (n "molc") (v "0.2.7") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "15chcvjswj0cf4r92kisnwxlgp904njrnjhzy8bvddrkq0bkps4l") (f (quote (("debug"))))))

(define-public crate-molc-0.2.8 (c (n "molc") (v "0.2.8") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1rpf2pr9i3v28zx203nw5x0d4pnnvz27girfhpfmfpyr1ajqi0ai") (f (quote (("debug"))))))

(define-public crate-molc-0.3.0 (c (n "molc") (v "0.3.0") (d (list (d (n "lsp-types") (r "^0.93.2") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1a686djnk1sc9l9afbr1kllyl8rynzh48gj1qqvfcazfvzqgb125") (f (quote (("debug"))))))

