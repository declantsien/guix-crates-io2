(define-module (crates-io mo lc molconv) #:use-module (crates-io))

(define-public crate-molconv-0.2.0 (c (n "molconv") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0rhx2jbfb33alr3psg7hvix8dyhrvqsblnnw821qmwlibkplz8cn")))

