(define-module (crates-io mo lr molrs) #:use-module (crates-io))

(define-public crate-molrs-0.1.0 (c (n "molrs") (v "0.1.0") (h "1nhrlb73rxba8qsp0xl3wd64ghrq5vaf1jl4yhy88sybndah9kws")))

(define-public crate-molrs-0.1.1 (c (n "molrs") (v "0.1.1") (d (list (d (n "pertable") (r "^0.1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "159mv8y2i1mpqrjkf7xrcjl6wc00991ls1f558q37pym1ij58a87")))

