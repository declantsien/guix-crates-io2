(define-module (crates-io mo te mote) #:use-module (crates-io))

(define-public crate-mote-0.1.0 (c (n "mote") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rgb") (r "^0.7.0") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)))) (h "0b109vfxn2qg627djqnc667cw52kybb4abhrcjf25azqk58y97cv")))

(define-public crate-mote-0.1.1 (c (n "mote") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rgb") (r "^0.7.0") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)))) (h "039q0fg07qq26viif6cid89pv37d1kfjmdysby98ipv4r840fngi")))

(define-public crate-mote-0.1.2 (c (n "mote") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rgb") (r "^0.7.0") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)))) (h "0vpx141zw6p7jca1r0s23w4mx8n3wk27si9va7jnn7qwlh7x5qqm")))

