(define-module (crates-io mo te moteconnection) #:use-module (crates-io))

(define-public crate-moteconnection-0.1.0 (c (n "moteconnection") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "clap") (r "^2.33.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "14hvvfhjh1idnghl1xsfhllf1f05hg8z4pnn6i049dkihihaxr8k")))

(define-public crate-moteconnection-0.2.0 (c (n "moteconnection") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "clap") (r "^2.33.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cmx7ikkf9ww54b0jw62p6ha0v3q53011r8rhk0p5j2qf0wpdqb2")))

(define-public crate-moteconnection-0.3.0 (c (n "moteconnection") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "clap") (r "^2.33.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fis6qzjc3p25rghqy6hr5an2b5infa3sbih90pafh7nzdrq8pjj")))

(define-public crate-moteconnection-0.3.1 (c (n "moteconnection") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "clap") (r "^2.33.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wibrcjrbfz62vgwia7sd3jcm3y7gh303abzna40d92zbkk2ymlp")))

