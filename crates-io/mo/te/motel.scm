(define-module (crates-io mo te motel) #:use-module (crates-io))

(define-public crate-motel-0.1.0 (c (n "motel") (v "0.1.0") (d (list (d (n "chitchat") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cool-id-generator") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0qsml7nqf4yxa72rbv3il6g1jhm587b31w00n403s9b7fiv82paq")))

