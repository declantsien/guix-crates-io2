(define-module (crates-io mo to motorola_68000_cpu) #:use-module (crates-io))

(define-public crate-motorola_68000_cpu-0.1.0 (c (n "motorola_68000_cpu") (v "0.1.0") (h "0irhbj10xy34v23rds5zgm09qdbyl4ri0a1scnlz6kgsb9960gvl") (y #t)))

(define-public crate-motorola_68000_cpu-0.1.1 (c (n "motorola_68000_cpu") (v "0.1.1") (h "0a1nhg50zgvmpx96w66mgwfp8nyn0kgbmnhr1kf2dmqlvxg5q17i")))

(define-public crate-motorola_68000_cpu-0.1.2 (c (n "motorola_68000_cpu") (v "0.1.2") (h "0lblpvz65y2sqxf4jm7xiwg9y8lj74i7w70nspfkrmnrwc40h80x")))

