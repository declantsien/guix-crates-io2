(define-module (crates-io mo to motore) #:use-module (crates-io))

(define-public crate-motore-0.0.0 (c (n "motore") (v "0.0.0") (h "0z5fxdh9k815ysgank824dda9yszjwam39lx6551wkcwhrm0d6vn")))

(define-public crate-motore-0.1.0 (c (n "motore") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "motore-macros") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "0a536v0v3iwk677krvir55skrzm935hcdxc9xczp5ny1k9k4vpwy")))

(define-public crate-motore-0.2.0 (c (n "motore") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "motore-macros") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "0mvzhvsl6n59frjmnzr1j2v4286vcjyv90y9xz6vfb047iinjaip")))

(define-public crate-motore-0.2.1 (c (n "motore") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "motore-macros") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "00xbzll4km54si25bb31kw0jrvr5ds3h635mbmp6nparzs9wm720")))

(define-public crate-motore-0.3.0 (c (n "motore") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "motore-macros") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "15w2h5bcx7kmdbb5sjb76pcm00jlbr3ilsx2j7d8afgplanxak1q") (s 2) (e (quote (("tower" "dep:tower"))))))

(define-public crate-motore-0.3.1 (c (n "motore") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "motore-macros") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0h9mccjscik7sz52dlxg7gygkp79x32yacj56fc0kmf7pyxjl5jc") (s 2) (e (quote (("tower" "dep:tower"))))))

(define-public crate-motore-0.3.2 (c (n "motore") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "motore-macros") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1mz7b0zglvsy97nq0sqg7b4746c9yk696a3xjvb06n7d6pj4ryj1") (s 2) (e (quote (("tower" "dep:tower"))))))

(define-public crate-motore-0.3.3 (c (n "motore") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "motore-macros") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1i53qfmrwhka2zcfgvyw2mhn79jq0201gziw9ikazglxyrpl23hi") (s 2) (e (quote (("tower" "dep:tower"))))))

(define-public crate-motore-0.4.0 (c (n "motore") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "motore-macros") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1a85fjv7c0ywsd1c1lqbbsy68zfg1kkdp7frnnnggd3g4m0hcy9f") (f (quote (("service_send" "motore-macros/service_send") ("default" "service_send")))) (s 2) (e (quote (("tower" "dep:tower"))))))

(define-public crate-motore-0.4.1 (c (n "motore") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 2)) (d (n "motore-macros") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (d #t) (k 0)))) (h "12jh0pbips8hsmc3cac4f86ypchh46msic8n1k50c5p65cx2hfwq") (f (quote (("service_send" "motore-macros/service_send") ("default" "service_send")))) (s 2) (e (quote (("tower" "dep:tower")))) (r "1.77.0")))

