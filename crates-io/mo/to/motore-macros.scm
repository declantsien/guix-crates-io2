(define-module (crates-io mo to motore-macros) #:use-module (crates-io))

(define-public crate-motore-macros-0.1.0 (c (n "motore-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15bhj31sjr7ja2wih38ab1hjglpyaazxp4dsk6j9srvvrw7y3y86")))

(define-public crate-motore-macros-0.1.1 (c (n "motore-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05r8miz48ahr8449pc4wgjd8x6qh63y16lgdwkksiz04lngppsjc")))

(define-public crate-motore-macros-0.3.0 (c (n "motore-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0045xndc49q6cwrzryn0cgjb6iik8mpq0w7qbc63dy0f9fphb104")))

(define-public crate-motore-macros-0.3.1 (c (n "motore-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8nazjqm3afj78y05fm7v5g0yfakh95vyqdnpfknybf8h8lxn8n")))

(define-public crate-motore-macros-0.4.0 (c (n "motore-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bsx1nad5p87gbbxwvkjx9cniavz66c732wrfpdfyj164m9xyfjg") (f (quote (("service_send") ("default"))))))

(define-public crate-motore-macros-0.4.1 (c (n "motore-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqpm8iv8q2fn0z9dkn1gsd5gy6qqy4nxnvdrxl4shfqdyksnh5h") (f (quote (("service_send") ("default")))) (r "1.77.0")))

