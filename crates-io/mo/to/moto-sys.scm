(define-module (crates-io mo to moto-sys) #:use-module (crates-io))

(define-public crate-moto-sys-0.1.0 (c (n "moto-sys") (v "0.1.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1fwjnb3l762zxi9x1hz0vrz8m2qlchci0rg9pymj58varflzl2z3") (f (quote (("userspace") ("rustc-dep-of-std" "alloc" "compiler_builtins" "core" "userspace") ("default" "userspace"))))))

(define-public crate-moto-sys-0.1.1 (c (n "moto-sys") (v "0.1.1") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0x44hxjdkwvc5j8zzk0slbgr2rjal7l7fnadsa892gmhcv3hjxx9") (f (quote (("userspace") ("rustc-dep-of-std" "alloc" "compiler_builtins" "core" "userspace") ("default" "userspace"))))))

(define-public crate-moto-sys-0.2.0 (c (n "moto-sys") (v "0.2.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "136cs4dpjsdhsm85sl2klkbpddrd91a7pb8k7l8qhg6v471d1h3f") (f (quote (("userspace") ("rustc-dep-of-std" "alloc" "compiler_builtins" "core" "userspace") ("default" "userspace"))))))

