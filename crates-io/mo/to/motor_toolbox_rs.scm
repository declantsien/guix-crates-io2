(define-module (crates-io mo to motor_toolbox_rs) #:use-module (crates-io))

(define-public crate-motor_toolbox_rs-0.1.0 (c (n "motor_toolbox_rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "16522jzr55ns132p0a906qk4s2ga7x6h8hp1dbc0bg89wjkk3znk")))

(define-public crate-motor_toolbox_rs-0.2.0 (c (n "motor_toolbox_rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r8v6lvnw7rpy9wl3l1w2wrrd9kbf986hv2gn74lmx8wij2b4fq0")))

(define-public crate-motor_toolbox_rs-0.2.1 (c (n "motor_toolbox_rs") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zc6a7cqj5g9mdy1dblynqdyrvlirzlzg5wypw23w51yya5p5z5y")))

