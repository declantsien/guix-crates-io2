(define-module (crates-io mo to motoyuki_json) #:use-module (crates-io))

(define-public crate-motoyuki_json-0.1.0 (c (n "motoyuki_json") (v "0.1.0") (d (list (d (n "ajson") (r "^0.2") (d #t) (k 0)))) (h "01qw58ad5rxxcbylij2a5gfxai78b2jgg4126gsn4iw4vdjkxd23")))

(define-public crate-motoyuki_json-0.1.1 (c (n "motoyuki_json") (v "0.1.1") (d (list (d (n "ajson") (r "^0.2") (d #t) (k 0)))) (h "1b03hc8vw99mrn1glqq2zx3f2d3pbrpfawab2xxydybg8zm13ghn")))

