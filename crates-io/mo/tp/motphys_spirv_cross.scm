(define-module (crates-io mo tp motphys_spirv_cross) #:use-module (crates-io))

(define-public crate-motphys_spirv_cross-0.23.2 (c (n "motphys_spirv_cross") (v "0.23.2") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1l388p4f0951bbyvh0fd43dz71175fnlhf946lpgak4kqlj7n0d3") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-motphys_spirv_cross-0.23.3 (c (n "motphys_spirv_cross") (v "0.23.3") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1ggg8yd5nsd6vj7mv8n4iwrwcr63iirk34yp03hf1gjilb9sxar1") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

