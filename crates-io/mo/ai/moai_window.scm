(define-module (crates-io mo ai moai_window) #:use-module (crates-io))

(define-public crate-moai_window-0.0.1 (c (n "moai_window") (v "0.0.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.51.0") (d #t) (k 0)))) (h "0fr4ghxgpcg71lyd9gfc04pxcsmlsna3vj915a6h7i9axxrad9bw")))

