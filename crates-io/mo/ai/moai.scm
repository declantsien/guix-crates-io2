(define-module (crates-io mo ai moai) #:use-module (crates-io))

(define-public crate-moai-0.0.0 (c (n "moai") (v "0.0.0") (h "0n70ynp2p1hiqvg3gddgav41p7lb509i2nb6zs3zm5hzfvsq214f")))

(define-public crate-moai-0.0.1 (c (n "moai") (v "0.0.1") (d (list (d (n "moai_core") (r "^0.0.1") (d #t) (k 0)) (d (n "moai_window") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "glfw") (r "^0.51.0") (d #t) (k 2)))) (h "05m75ln47m5mykbb713mqxmjxrcv3w0bas5r8fi20wvvvybdpajj") (f (quote (("default" "moai_window"))))))

(define-public crate-moai-0.0.2 (c (n "moai") (v "0.0.2") (d (list (d (n "moai_core") (r "^0.0.2") (d #t) (k 0)) (d (n "moai_window") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "glam") (r "^0.23.0") (d #t) (k 2)) (d (n "glfw") (r "^0.51.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 2)))) (h "0yasfpd17vgjzvrv4qa19cvb1702firfhz1ymnvrf5rp9r15fbcp") (f (quote (("default" "moai_window"))))))

