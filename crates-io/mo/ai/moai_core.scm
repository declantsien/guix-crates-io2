(define-module (crates-io mo ai moai_core) #:use-module (crates-io))

(define-public crate-moai_core-0.0.1 (c (n "moai_core") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1x7wfy4kcvj9a58hh0kb86kpc2pbxc9yh16y1cz7qzv7m7zgaqrd")))

(define-public crate-moai_core-0.0.2 (c (n "moai_core") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0kipg32y0ykb438z6jyjw9dxsv9g7316r42rgl0982jadjngjjn4")))

