(define-module (crates-io mo d- mod-exp-unsigned) #:use-module (crates-io))

(define-public crate-mod-exp-unsigned-0.1.0 (c (n "mod-exp-unsigned") (v "0.1.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "10yqg9jgj4phi7slx9ynmh88214rkk7cjrnhcxfkjxj9qg7k0539")))

(define-public crate-mod-exp-unsigned-0.1.1 (c (n "mod-exp-unsigned") (v "0.1.1") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "1wpixap341gqxgc672c3fr68yxfw8bq4pm5d50nnp6rgb6ilnffj")))

