(define-module (crates-io mo d- mod-rs-migrator) #:use-module (crates-io))

(define-public crate-mod-rs-migrator-0.1.0 (c (n "mod-rs-migrator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env" "color"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lznarylcgiad54xd0809jagxnxq2jrxg260sfqwnvd3jq9a8987")))

(define-public crate-mod-rs-migrator-0.2.0 (c (n "mod-rs-migrator") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env" "color"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0xazqij3r4rhhgghf03q249knbfk8q5dzk52bkbpc3scwrvmy57l")))

