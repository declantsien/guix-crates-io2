(define-module (crates-io mo d- mod-sdk) #:use-module (crates-io))

(define-public crate-mod-sdk-0.1.0 (c (n "mod-sdk") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qkdp307ap7w39w43vamx0dhiqsxhayn53g83fvbjf5p5cscipf3")))

(define-public crate-mod-sdk-0.1.1 (c (n "mod-sdk") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16m8qkqn9ndxz34cxklnk4g7xfzkqwg92vbj5jgbl44dqd16w7sl")))

(define-public crate-mod-sdk-0.1.2 (c (n "mod-sdk") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07h8nxlhxbpwz1xp54qqvx10l7dggrr2hr48pv8c9giibrcxcshq")))

(define-public crate-mod-sdk-0.1.3 (c (n "mod-sdk") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13f4jhjjf0fiyhwgskhv0wabn95bhsi728i8k4yx7bf7dxsv47fk")))

(define-public crate-mod-sdk-0.1.4 (c (n "mod-sdk") (v "0.1.4") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sg9ks180fjmsqdjrw72yhlm038yx2kgig719i9gyv64f7n592xw")))

(define-public crate-mod-sdk-0.1.5 (c (n "mod-sdk") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02y66945wy0m94b1xawx5mpgpr36hlvcfb82nsbn935f6xiny16f")))

(define-public crate-mod-sdk-1.0.0 (c (n "mod-sdk") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16g5qdjar8nrnmz8lydn037n3ynd2kn1qagis4md166vmgn61bp1")))

(define-public crate-mod-sdk-1.0.1 (c (n "mod-sdk") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16avvkmwsigz7z93jgjvp7d4bi21zk3s3nq64cp139rzpafycf21")))

(define-public crate-mod-sdk-1.0.2 (c (n "mod-sdk") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sjj41pqvrkqjyb94sgnmqyrv8waf4vxv7xziwhn568ga8fy4fyn")))

(define-public crate-mod-sdk-1.0.3 (c (n "mod-sdk") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xsmbinscgk2ybj7j47r6dfpfgd7rkfpkpl3s2gz10kqii9p1hnl")))

(define-public crate-mod-sdk-1.0.4 (c (n "mod-sdk") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "167ljzypx03gz47f4m9jb119q27429hri1dis7hcr46cfwv38a4d")))

