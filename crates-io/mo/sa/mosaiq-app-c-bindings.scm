(define-module (crates-io mo sa mosaiq-app-c-bindings) #:use-module (crates-io))

(define-public crate-mosaiq-app-c-bindings-0.2.7 (c (n "mosaiq-app-c-bindings") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "09a4nir0d5xynjfdyvrf7ryx0pvm60a5cmyzpnyp0z1j8d41qyjf") (y #t)))

(define-public crate-mosaiq-app-c-bindings-0.2.8 (c (n "mosaiq-app-c-bindings") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1sbgqdvhwggskb4yds7gqrh8irwkp3g6akwf0c5ly50kk6861mb3") (y #t)))

(define-public crate-mosaiq-app-c-bindings-0.2.9 (c (n "mosaiq-app-c-bindings") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "14dd8nnlx1h4dn9jf9aabp5n50ryk2crad06pa3cz64x1533pkf8") (y #t)))

(define-public crate-mosaiq-app-c-bindings-0.2.10 (c (n "mosaiq-app-c-bindings") (v "0.2.10") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0cvxmpcmywmplcw73viz704kdzc8kzxbx5qas642rciwys72c3vv") (y #t)))

