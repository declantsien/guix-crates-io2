(define-module (crates-io mo sa mosaic) #:use-module (crates-io))

(define-public crate-mosaic-0.1.0 (c (n "mosaic") (v "0.1.0") (d (list (d (n "ggez") (r "^0.4.3") (d #t) (k 0)) (d (n "mint") (r "^0.5.0") (d #t) (k 0)))) (h "137s260h9sf38ng6cnph9qwxndmidibb5s7f45flz94l7zsnv6nr") (y #t)))

