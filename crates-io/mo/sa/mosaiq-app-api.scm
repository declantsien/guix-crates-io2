(define-module (crates-io mo sa mosaiq-app-api) #:use-module (crates-io))

(define-public crate-mosaiq-app-api-0.1.3 (c (n "mosaiq-app-api") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "1fsqkdp14arfxq1n42nmpyxr6jzd6wnwq8196bs1yblwplp8yk8y") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

(define-public crate-mosaiq-app-api-0.1.4 (c (n "mosaiq-app-api") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "0z1sbcb6l1g07lidzsskaqcfnnv398glpr9fplrklnwir23jjk6i") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

(define-public crate-mosaiq-app-api-0.1.5 (c (n "mosaiq-app-api") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "0jidbl1vj5v4j8266144ydja5qdi0qqrsnkxhq38vn68dq47p8xv") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

(define-public crate-mosaiq-app-api-0.1.6 (c (n "mosaiq-app-api") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "1igm6gp5pjamyl41s81ndqph9i82lxlvkw2y1kc48c7bv46ndg3n") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

(define-public crate-mosaiq-app-api-0.1.8 (c (n "mosaiq-app-api") (v "0.1.8") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "1610rzai1krymdxbyn1yx9s7l11s4xg2a7h7nqmbhzsa1yj1bb1b") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

(define-public crate-mosaiq-app-api-0.1.9 (c (n "mosaiq-app-api") (v "0.1.9") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-macros") (r "^0.1") (d #t) (k 0)))) (h "1sybdyj11rbf924jchqakrdwf4hpnz55a413cqk01qm6pf82918d") (f (quote (("testing" "byteorder") ("default")))) (y #t)))

