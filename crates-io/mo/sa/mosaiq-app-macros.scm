(define-module (crates-io mo sa mosaiq-app-macros) #:use-module (crates-io))

(define-public crate-mosaiq-app-macros-0.1.2 (c (n "mosaiq-app-macros") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "019912z96qqhg0rhi2s6x5qsbdr4fpav7z5110aj9cbha0xgkyxf") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.3 (c (n "mosaiq-app-macros") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nhx322c5pnlc0934n9iwrayi89diqb8avhmj0k2gj3fx5fm66n4") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.4 (c (n "mosaiq-app-macros") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dm35nzskvf261305xqzbdiyib8q1hn2j0vkkg6c9cixz3b41gmm") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.5 (c (n "mosaiq-app-macros") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jq4lp0hf4jhsn4rqi0li7lwxv8mr1vxk3g4gd4yqhwpa7v756fy") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.6 (c (n "mosaiq-app-macros") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b28rqvqcgpbvb90p9hrkrcwl6l5x52s8z7wy8892p2a837l13j9") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.7 (c (n "mosaiq-app-macros") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "062iib593n24zkl6sy9kc6fi85sdf7i6wdfnmi87cb5jjq2ybvil") (y #t)))

(define-public crate-mosaiq-app-macros-0.1.8 (c (n "mosaiq-app-macros") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mosaiq-app-c-bindings") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfsaazqy741bcmax7vr21rjnrpx88jpkpdj7p2zq2y5zz54fiih") (y #t)))

