(define-module (crates-io mo sa mosaic-tile) #:use-module (crates-io))

(define-public crate-mosaic-tile-0.1.0 (c (n "mosaic-tile") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k3cfypbw448r1iqnjg9ycyqnwqphyp5jlapq7q1pngj9isj2jnr")))

(define-public crate-mosaic-tile-0.1.1 (c (n "mosaic-tile") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jzxkcxq6m8vkwikzpd7rxfv78c4fasnsx5z1hcqxj9nn370wz6x")))

(define-public crate-mosaic-tile-0.2.1 (c (n "mosaic-tile") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kh62z5bwpa2gkgizjb8j1px5rdibmi5jpbxd1jaixgy3p5l4i1a")))

(define-public crate-mosaic-tile-0.2.2 (c (n "mosaic-tile") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09phm6rf1lf7fpmh16w56425fyxrf9qw55gshc5gznmi0282i4b8")))

(define-public crate-mosaic-tile-0.3.0 (c (n "mosaic-tile") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q9k1zk2ayk3jm45yylmcfg43wp5xpm9adyvbcav1j2bk10yvkfw")))

(define-public crate-mosaic-tile-0.3.1 (c (n "mosaic-tile") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z3v719xrw0r2vwsnwr6irw6inw0c2g4rwca07bc69bj6vh129qc")))

(define-public crate-mosaic-tile-0.4.0 (c (n "mosaic-tile") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bw4cxxzv9fxi6q14bggcw38zi472x2jj0l5bfd5482q2ai362k7")))

