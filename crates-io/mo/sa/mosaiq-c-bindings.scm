(define-module (crates-io mo sa mosaiq-c-bindings) #:use-module (crates-io))

(define-public crate-mosaiq-c-bindings-0.2.4 (c (n "mosaiq-c-bindings") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0qaybcvkxczsay5dc9wc1pz7wyvl3273qsg31mshjif55vlm153b") (y #t)))

(define-public crate-mosaiq-c-bindings-0.2.5 (c (n "mosaiq-c-bindings") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "10ppq785dsy52ahqxzil2d4cjwd94lp4v4fbwpl48dwkcp93jqz0") (y #t)))

(define-public crate-mosaiq-c-bindings-0.2.6 (c (n "mosaiq-c-bindings") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "02rmqiggnilbbny7l3y23rzh73mj9gdgkn8sb9zd4bhj03yavdqk") (y #t)))

