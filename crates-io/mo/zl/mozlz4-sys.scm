(define-module (crates-io mo zl mozlz4-sys) #:use-module (crates-io))

(define-public crate-mozlz4-sys-0.1.0 (c (n "mozlz4-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07ll3chz7ns4v3cy4j2vqhd7a1p95frlzx7cijky46vs5v6ghqms") (l "lz4")))

