(define-module (crates-io mo zl mozlz4) #:use-module (crates-io))

(define-public crate-mozlz4-0.2.0 (c (n "mozlz4") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "mozlz4-sys") (r "^0.1") (d #t) (k 0)))) (h "15h5prq5ynqs3giry4yk6w2pdkn6pdvp3av5ly9876pg25akdwpj")))

(define-public crate-mozlz4-0.3.0 (c (n "mozlz4") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "mozlz4-sys") (r "^0.1") (d #t) (k 0)))) (h "1yzzhp5l0y30h7lha4s3k3izx1z4s8icasf6py6wxn7fq7vd88mm")))

(define-public crate-mozlz4-0.3.1 (c (n "mozlz4") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "mozlz4-sys") (r "^0.1") (d #t) (k 0)))) (h "1p73x9cwing8h1drcw58f0csv9zwxfmgw16jn615v2kgmfq6hjkh")))

