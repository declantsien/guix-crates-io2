(define-module (crates-io mo ve movement) #:use-module (crates-io))

(define-public crate-movement-0.1.0 (c (n "movement") (v "0.1.0") (h "0wlwraa3fkyygadz90zjgfjvi3bw5cy7srdfxw1gb5pbiv76020i")))

(define-public crate-movement-0.2.0 (c (n "movement") (v "0.2.0") (h "0m04r88s8xq9mmk8sgbly4s3f29001c9lh74r6xgcwcb08xhjila")))

