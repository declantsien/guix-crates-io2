(define-module (crates-io mo ve movecell) #:use-module (crates-io))

(define-public crate-movecell-0.1.0 (c (n "movecell") (v "0.1.0") (h "0hj5m6snnz9ln100rnzij2mfk665lciqjkh8slldnqav41v4vq76")))

(define-public crate-movecell-0.1.1 (c (n "movecell") (v "0.1.1") (h "1fpdqdb1r5vmf2jvcqs3y0ky299k258qnj3lijb3hfifmmspk1qb")))

(define-public crate-movecell-0.2.0 (c (n "movecell") (v "0.2.0") (h "1wa1a9hk6dc54wicg6dlbdiz2k1wlvc5s1yk68q83rp9cvvfqplk")))

