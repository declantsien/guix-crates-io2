(define-module (crates-io mo ve moveref) #:use-module (crates-io))

(define-public crate-moveref-0.0.0 (c (n "moveref") (v "0.0.0") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1ml1kqiv2j469gkar0yzwr4jr87w07gb1dcwfgsz8pxhwv2is4y6") (f (quote (("std" "alloc" "tracing/std") ("default" "std" "cxx") ("debug" "tracing") ("cxx" "cxx/c++20") ("alloc"))))))

(define-public crate-moveref-0.0.1 (c (n "moveref") (v "0.0.1") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1cr8a3hqb2afp1680ngf91k2lipa42knqxil3yclmnf9ll67605n") (f (quote (("valgrind") ("std" "alloc" "tracing/std") ("default" "std" "cxx") ("debug" "tracing") ("cxx" "cxx/c++20") ("alloc"))))))

(define-public crate-moveref-0.0.2 (c (n "moveref") (v "0.0.2") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0fxdl0dpgyclii2g3df5bqafy0nl6hdkhd36nz5fwfjh0dldm1h6") (f (quote (("valgrind") ("std" "alloc" "tracing/std") ("default" "std" "cxx") ("debug" "tracing") ("cxx" "cxx/c++20") ("alloc"))))))

