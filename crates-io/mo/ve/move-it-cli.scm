(define-module (crates-io mo ve move-it-cli) #:use-module (crates-io))

(define-public crate-move-it-cli-0.1.0 (c (n "move-it-cli") (v "0.1.0") (d (list (d (n "move-it") (r "^0.1") (d #t) (k 0)))) (h "0bfxg95241azg6lgk8m2klpn3rc01lj56nf9pgwy1mvm6l7hfqps")))

(define-public crate-move-it-cli-0.1.1 (c (n "move-it-cli") (v "0.1.1") (d (list (d (n "move-it") (r "^0.1") (d #t) (k 0)))) (h "0mwn0wi7xjzjxa2ak04jqypwkgi450v62cmwd6lfgv7kid0rlb9c")))

(define-public crate-move-it-cli-0.2.0 (c (n "move-it-cli") (v "0.2.0") (d (list (d (n "clap") (r "~3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "move-it") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "0.2.*") (f (quote ("full"))) (d #t) (k 0)))) (h "05fvhhi2a3f2c52dipgvqmnvd9qs8yq045cd5g1bravxy3mhrwsl")))

(define-public crate-move-it-cli-0.2.1 (c (n "move-it-cli") (v "0.2.1") (d (list (d (n "clap") (r "~3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "move-it") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "0.2.*") (f (quote ("full"))) (d #t) (k 0)))) (h "18q2vbnq421f5zzd5wg9cgn31wllz1rb97m16fxiqkcpjqf00nv2")))

(define-public crate-move-it-cli-0.2.2 (c (n "move-it-cli") (v "0.2.2") (d (list (d (n "clap") (r "~3.0.0-beta.1") (d #t) (k 0)) (d (n "clap_generate") (r "~3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "move-it") (r "^0.2.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "0.2.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1y5r3svl94wp5lv485gznf1mq42cvg2cn3ydljy627f7c2fxl62x")))

(define-public crate-move-it-cli-0.2.3 (c (n "move-it-cli") (v "0.2.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "move-it") (r "^0.2.3") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "0.2.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqrf700rr7b5gh4q14j3rnc429xjm7blnv90yxiwcaj105wj436")))

(define-public crate-move-it-cli-0.2.4 (c (n "move-it-cli") (v "0.2.4") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "move-it") (r "^0.2.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "1.19.*") (f (quote ("full"))) (d #t) (k 0)))) (h "12dqdmv048hbq15kc6zll0l19m1xqfkk3fiwznxmzhgqjlky74xq")))

