(define-module (crates-io mo ve movey-core) #:use-module (crates-io))

(define-public crate-movey-core-0.0.1 (c (n "movey-core") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "utils") (r "^0.0.1") (d #t) (k 0) (p "movey-utils")))) (h "1zzjlzv0mhskvcc8gyn68z7hh34mmsm5lja09pgh3nzx2ydfcwjn")))

(define-public crate-movey-core-0.0.2 (c (n "movey-core") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "utils") (r "^0.0.2") (d #t) (k 0) (p "movey-utils")))) (h "05aip892y319hk2ikhv0k56lqv8hdg34xrn65c9zp4c4cissff2p")))

