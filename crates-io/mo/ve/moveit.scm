(define-module (crates-io mo ve moveit) #:use-module (crates-io))

(define-public crate-moveit-0.1.0 (c (n "moveit") (v "0.1.0") (h "0lxdwl8gb2dm3nr29bqwhd6as18gh554xlbwmz9v9hgi0rqbjlkz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.2.0 (c (n "moveit") (v "0.2.0") (h "0n5f9p59gk5877cgg4hls93bxmv4rljvhm6m965cji19r4pi9xrk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.3.0 (c (n "moveit") (v "0.3.0") (h "0mqg8wff6vdilipl0f3000q20i197mqzlz45fxpi7llybcxdlmv0") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.4.0 (c (n "moveit") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0xjnjkd3x8jaxz3n66bicfms7pwpsv0n3l28d72wyszsvrm3v0xx") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.5.0 (c (n "moveit") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1dfwk69w87pk8775npff5r1bnqs5cpp86ajpmn5z08nxl645jpc1") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.5.1 (c (n "moveit") (v "0.5.1") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03836nimff7ifzj5ras3w35f5npwjdm75grmgm817073wkzmdmyp") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-moveit-0.6.0 (c (n "moveit") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fpg5i8xyz8262h7lfl2nljsmmz4wc5xp9kzcjyzfvnb0i937mw7") (f (quote (("default" "alloc") ("alloc"))))))

