(define-module (crates-io mo ve move-ir-compiler-transactional-tests) #:use-module (crates-io))

(define-public crate-move-ir-compiler-transactional-tests-0.1.4 (c (n "move-ir-compiler-transactional-tests") (v "0.1.4") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "0h0bpa18n6z8lgagzkbnb82rfna9b32z0hrc7zsmzc841d9p11gd") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.1.5 (c (n "move-ir-compiler-transactional-tests") (v "0.1.5") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "04vq763gix3wrj9zcn4l1nav6dii4mzy6lm26x5056bqiqi51qh9") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.1.6 (c (n "move-ir-compiler-transactional-tests") (v "0.1.6") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "0pa4xhlksgyf5bndvisjj7gpgg81mblj3yilq5q3klbnn381aba7") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.2.0 (c (n "move-ir-compiler-transactional-tests") (v "0.2.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "1dxn9kv805mamgki2k2cr84dwrgqmbv07s4qz9rnpskqx438qj2n") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.2.1 (c (n "move-ir-compiler-transactional-tests") (v "0.2.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "04angirj3vzss4slijxdlxl2xzr48h1vjngm01bnaqg2p0j6ki36") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.3.0 (c (n "move-ir-compiler-transactional-tests") (v "0.3.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "1m2bnp0a29kb3q6rhsd4gzax7vvmpc36qqm2yipwpzy5qnq3f99k") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.3.1 (c (n "move-ir-compiler-transactional-tests") (v "0.3.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "053jlqw0y589x55mhd866l3w51y0cwim4pvilj0anb692g6a2ysz") (y #t)))

(define-public crate-move-ir-compiler-transactional-tests-0.3.2 (c (n "move-ir-compiler-transactional-tests") (v "0.3.2") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "0cvggswx8ywngkg1lnaajlfxif6l0l5qqw0fr4scjjip47jqfjq2") (y #t)))

