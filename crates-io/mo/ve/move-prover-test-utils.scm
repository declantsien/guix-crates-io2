(define-module (crates-io mo ve move-prover-test-utils) #:use-module (crates-io))

(define-public crate-move-prover-test-utils-0.1.0 (c (n "move-prover-test-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0z2lj3lk04hah287fl8h56sy7jwzvkjf4m92z617h27kyhzp4xyv") (y #t)))

(define-public crate-move-prover-test-utils-0.1.1 (c (n "move-prover-test-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1z97h5z4k5m1lfq579970qll2vhaq0siv0gcdwahb419pgag82lk") (y #t)))

(define-public crate-move-prover-test-utils-0.1.2 (c (n "move-prover-test-utils") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "036j39wmc6c5k8f46ax82z2cwp6m50vf96cx5kyl50z2kz5nf3cc") (y #t)))

(define-public crate-move-prover-test-utils-0.1.4 (c (n "move-prover-test-utils") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0cn4b030a0v9jsbfqa0ca4axm9gs14fxll8m6lggs2nq638ay434") (y #t)))

(define-public crate-move-prover-test-utils-0.1.5 (c (n "move-prover-test-utils") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1j2bb3awpwr809a47c90ws3cgrwma1gkxwwrjimncgjy42hpz81r") (y #t)))

(define-public crate-move-prover-test-utils-0.1.6 (c (n "move-prover-test-utils") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.1.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "171xsdpqm15j6h2n96sn50iprw3vkbdspz27b59w72binw1l7kry") (y #t)))

(define-public crate-move-prover-test-utils-0.2.0 (c (n "move-prover-test-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.2.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0qk0gkyjmkbvpwps13isvbyima01np724bla6jsi9in462q3j57g") (y #t)))

(define-public crate-move-prover-test-utils-0.2.1 (c (n "move-prover-test-utils") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.2.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1mnxm9qlfjdvswnksnggvzlrqvsf5114k5fz84xj875b3as0i3zh") (y #t)))

(define-public crate-move-prover-test-utils-0.3.0 (c (n "move-prover-test-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0ljpw1g9kym3c5jjs1lq25c5gxh77p2iabzk6ipxnqy60mhn1z5s") (y #t)))

(define-public crate-move-prover-test-utils-0.3.1 (c (n "move-prover-test-utils") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0dypm0r1d5f1j5k7blwcfg57la0hbn99cdgghs2synhqrdi7s7l9") (y #t)))

(define-public crate-move-prover-test-utils-0.3.2 (c (n "move-prover-test-utils") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-command-line-common") (r "^0.3.0") (d #t) (k 0)) (d (n "prettydiff") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "09dn95vkzdldgsjxnljqf12g74vbcpcbcgskb6010y3y05qps2zs") (y #t)))

