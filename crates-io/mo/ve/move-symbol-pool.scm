(define-module (crates-io mo ve move-symbol-pool) #:use-module (crates-io))

(define-public crate-move-symbol-pool-0.1.0 (c (n "move-symbol-pool") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0s2jfgk8hqrk0gkls9f6f6r07lsl4bp21b9gkc3zg9hlxw7klg8x") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.1.1 (c (n "move-symbol-pool") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1ikml8dv70xllhdhbb2f65da81msbrwb5dhpqfk6c2rj3navgb79") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.1.2 (c (n "move-symbol-pool") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "15125mkl1ads504qcif5ak6gmskp8d3asw9lagph4dqk5bq0m2dg") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.1.4 (c (n "move-symbol-pool") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0fnl6dmvwg0gw9mcjbx812xqnr3ql5k0q09vnqrlxv7ix4w05s7l") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.1.5 (c (n "move-symbol-pool") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0p71q7yrvs737j34q38bpsv5yqcjqd29zvrcf2h0mhbmaw899acf") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.1.6 (c (n "move-symbol-pool") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1kkzq2swhaj40hcsjjdi93ywgw2lqy2433fx2cy82nnj8pf6zkiy") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.2.0 (c (n "move-symbol-pool") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0dxa6r6yv8mghv2b1kgxizds9y8yz7iszlxm56rjzgyx7l5ljssi") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.2.1 (c (n "move-symbol-pool") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "043rqb79zypblkhlz1900fyx5x8x7k5rgc3crsyyrvkqrswisw3f") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.3.0 (c (n "move-symbol-pool") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "078yisbmpgb7mz4imq7lq4mg66xnqbfaz0cbm1pw3lchswy9n74m") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.3.1 (c (n "move-symbol-pool") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0hk219rkq9c8n24gigfbixmjib0qqhhlwblfli11pi1pbmrdy6k2") (f (quote (("default")))) (y #t)))

(define-public crate-move-symbol-pool-0.3.2 (c (n "move-symbol-pool") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1n8l899qvfd60awxb0r0vr245j3xcsqg3ijcxysxkbxnaywz50k5") (f (quote (("default")))) (y #t)))

