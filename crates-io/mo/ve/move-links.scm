(define-module (crates-io mo ve move-links) #:use-module (crates-io))

(define-public crate-move-links-0.1.0 (c (n "move-links") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "0kw7lsy66d01vd6acx7prlxw2h2dmsm88750llgip49hxnr9820q")))

(define-public crate-move-links-0.1.1 (c (n "move-links") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "1626grrp3dy7fsz1d4ysajr46c244pvg3q42vrm5ab4a332d1b96")))

