(define-module (crates-io mo ve move-compiler-transactional-tests) #:use-module (crates-io))

(define-public crate-move-compiler-transactional-tests-0.1.4 (c (n "move-compiler-transactional-tests") (v "0.1.4") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "1p510kjqiljkmb84r2ps1al9pnpr4ssqv5jlhs500fzs3fldr2cr") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.1.5 (c (n "move-compiler-transactional-tests") (v "0.1.5") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "0iqk8smdiwd9xfdhl9r5yk516agpih1y7kk4cc981j2ws6v0i933") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.1.6 (c (n "move-compiler-transactional-tests") (v "0.1.6") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "0wzp1x9i3drqvbl51adiklk05bbd3q53iv4ay38as4wfiqxr3w0x") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.2.0 (c (n "move-compiler-transactional-tests") (v "0.2.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "126cbkbkfg84bv0y9sycj5d54i76charww85xh6dcq0sa4lcf9wi") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.2.1 (c (n "move-compiler-transactional-tests") (v "0.2.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "0y7q2wp6zlbbnhl7nd5af7imjh3g2js116qjnn3klcac03lbb7hn") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.3.0 (c (n "move-compiler-transactional-tests") (v "0.3.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "05l2j2hxgf8v4zci2jlhyd4sbi249rakhza48dgpjshq5g3fpqhn") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.3.1 (c (n "move-compiler-transactional-tests") (v "0.3.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "04hw2ywwcc6wmdy7zpysnxsqzymn1c7miylfi46a6s912drar311") (y #t)))

(define-public crate-move-compiler-transactional-tests-0.3.2 (c (n "move-compiler-transactional-tests") (v "0.3.2") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "0i17rrjjaj4sr9ib6n92zk0ij7b6wl7danpgz4ya9c0kzd1vygka") (y #t)))

