(define-module (crates-io mo ve move-vm-transactional-tests) #:use-module (crates-io))

(define-public crate-move-vm-transactional-tests-0.1.4 (c (n "move-vm-transactional-tests") (v "0.1.4") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "1cm6c7xasm18j5f6mqsqprnlhjqrlrr5fn72mc490c0wdkjbfhvl") (y #t)))

(define-public crate-move-vm-transactional-tests-0.1.5 (c (n "move-vm-transactional-tests") (v "0.1.5") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "1gnwkafmcxm3rdw86arfrq24gdkvh1iin5dxwagph9b8vydlws52") (y #t)))

(define-public crate-move-vm-transactional-tests-0.1.6 (c (n "move-vm-transactional-tests") (v "0.1.6") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "1bvlxl4901fqk8ch0nfkh7c474fdz63p196qn952zra1kh9y42pq") (y #t)))

(define-public crate-move-vm-transactional-tests-0.2.0 (c (n "move-vm-transactional-tests") (v "0.2.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "1mc58lv1589fgz15psmvkh3wswna5yp17d6y14da05dr0aiv0ysw") (y #t)))

(define-public crate-move-vm-transactional-tests-0.2.1 (c (n "move-vm-transactional-tests") (v "0.2.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "1g4nf8xmphrcsg9b9y5i24b68w0mwidsnyg8fay327yi1mlrxjz0") (y #t)))

(define-public crate-move-vm-transactional-tests-0.3.0 (c (n "move-vm-transactional-tests") (v "0.3.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "0qyg4fzyxa76smm6c7vvfa6r0dxh22m9gq86m9cqk1zilfna03m0") (y #t)))

(define-public crate-move-vm-transactional-tests-0.3.1 (c (n "move-vm-transactional-tests") (v "0.3.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "1g3szlajph5nnvgac47h3j592aksi5cpfl9am6fxbp53w91cv7f8") (y #t)))

(define-public crate-move-vm-transactional-tests-0.3.2 (c (n "move-vm-transactional-tests") (v "0.3.2") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "0v99avn8rcygk0zfi6wjb3sszbv7iymhlk2wfsbgakx793ipqkxa") (y #t)))

