(define-module (crates-io mo ve move-bytecode-utils) #:use-module (crates-io))

(define-public crate-move-bytecode-utils-0.1.1 (c (n "move-bytecode-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "0h7czm31vkk2cl6707iiz5drgi93nx2s61mrhiyykmfwwvnrzw2l") (y #t)))

(define-public crate-move-bytecode-utils-0.1.2 (c (n "move-bytecode-utils") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "1ga1yy5kwlwc2gxm1piyskdi66pdlf3pb5x7j135qm9144lfn9n5") (y #t)))

(define-public crate-move-bytecode-utils-0.1.4 (c (n "move-bytecode-utils") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "1bz78jlz7ws1f26ggh4j0znbw3jwh1k0k2chfbjqs1pj6wv9gkki") (y #t)))

(define-public crate-move-bytecode-utils-0.1.5 (c (n "move-bytecode-utils") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "0pzinsbr442c425iygjkyi18w954pzq5bn1yv73vpmw9lbxbp95a") (y #t)))

(define-public crate-move-bytecode-utils-0.1.6 (c (n "move-bytecode-utils") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "1jd5qd3v7kldsizaadcngv3p35inf0h3bw05mjziapllh5ka3lq1") (y #t)))

(define-public crate-move-bytecode-utils-0.2.0 (c (n "move-bytecode-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "0q2am4is4k3ayriayd1axxip9x9czsvxf06mvr8pxjqbfxf35908") (y #t)))

(define-public crate-move-bytecode-utils-0.2.1 (c (n "move-bytecode-utils") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "0fr6d91gjxwk5m2kyk3yzf0z9g1lzwp7x7p2s26l9kss94wxjidp") (y #t)))

(define-public crate-move-bytecode-utils-0.3.0 (c (n "move-bytecode-utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "0fwmnb7c363czkmdcl964rrp0rcikx7z9xqw4xfaqahs63pla2hj") (y #t)))

(define-public crate-move-bytecode-utils-0.3.1 (c (n "move-bytecode-utils") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "1sz5gwbq06hmwyljpiq0dkxd8av03kynw8w5jawr9i8wqn8sfib2") (y #t)))

(define-public crate-move-bytecode-utils-0.3.2 (c (n "move-bytecode-utils") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.2") (d #t) (k 0)))) (h "1r8vm54qq6qwnsizrfpazjda1z3mdp9dgh7wpqlwz5jcgv4k25ff") (y #t)))

