(define-module (crates-io mo ve move-idl-schema) #:use-module (crates-io))

(define-public crate-move-idl-schema-0.3.0 (c (n "move-idl-schema") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-idl") (r "^0.3.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12xpndx5b6h4zn626cb9cygqqfly8aqcwry099bnf3m7jkbcwnb1") (f (quote (("default") ("address32" "move-idl/address32") ("address20" "move-idl/address20"))))))

(define-public crate-move-idl-schema-0.4.0 (c (n "move-idl-schema") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "move-idl") (r "^0.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jz10bmk1av9m13hiq0i5bbipmyswkm6gq6bmhipirkfhdr5a9d6") (f (quote (("default") ("address32" "move-idl/address32") ("address20" "move-idl/address20"))))))

