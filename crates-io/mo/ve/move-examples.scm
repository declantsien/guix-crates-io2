(define-module (crates-io mo ve move-examples) #:use-module (crates-io))

(define-public crate-move-examples-0.1.0 (c (n "move-examples") (v "0.1.0") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1zs3864skivq077w6mnjmk6jcj5xahv83qx9vigpv8yl47x8ib1j") (y #t)))

(define-public crate-move-examples-0.1.1 (c (n "move-examples") (v "0.1.1") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1lx3fn9v2l5izlal3810krgwg85jyxwm9dfffdm1nfnvzy085bk8") (y #t)))

(define-public crate-move-examples-0.1.2 (c (n "move-examples") (v "0.1.2") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1dm31ymg2r9bc54iw00s09i10gixdc2zn27n0ncksvfxk3ghic39") (y #t)))

(define-public crate-move-examples-0.1.3 (c (n "move-examples") (v "0.1.3") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.0") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0x3rciyqg5jc9ywb36kw1zwa755knj473781qi60r8skg5bdij5k") (y #t)))

(define-public crate-move-examples-0.1.4 (c (n "move-examples") (v "0.1.4") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.4") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17dsa2l4y3r7gkwrry1451lv2ara1pwz4dc5ja29w63qc2wyg11m") (y #t)))

(define-public crate-move-examples-0.1.6 (c (n "move-examples") (v "0.1.6") (d (list (d (n "aptos-types") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.4") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1bg1kwism0110cxgr88pkj452v3l0j10b6yq1rwz024z0rhkls2r") (y #t)))

(define-public crate-move-examples-0.1.7 (c (n "move-examples") (v "0.1.7") (d (list (d (n "aptos-types") (r "^0.1.7") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.1.7") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.1.7") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1v3fisa1m0wabg6vhg79876iff3ni5ykqq1hsyr11j1i58p87alr") (y #t)))

(define-public crate-move-examples-0.2.1 (c (n "move-examples") (v "0.2.1") (d (list (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.2.1") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0mh041i62riy6hxirlj1jkcwa2qs86syqhd3blgs0080x90nqygm") (y #t)))

(define-public crate-move-examples-0.2.2 (c (n "move-examples") (v "0.2.2") (d (list (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.2.1") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0mmqlba6b828k4y3mq7mnfn3qv1zpp3b5pnr8gy10jpl6sppqk06") (y #t)))

(define-public crate-move-examples-0.2.6 (c (n "move-examples") (v "0.2.6") (d (list (d (n "aptos-gas") (r "^0.2.6") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.2.1") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0n282yqra674ql78nyzjxmddszf77asj5flp5nvjhsp04wkk5n2h") (y #t)))

(define-public crate-move-examples-0.2.7 (c (n "move-examples") (v "0.2.7") (d (list (d (n "aptos-gas") (r "^0.2.6") (d #t) (k 0)) (d (n "aptos-types") (r "^0.2.1") (d #t) (k 0)) (d (n "aptos-vm") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "move-deps") (r "^0.2.1") (f (quote ("table-extension"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0kqs7r6jy8v2pian6p9zvypwckws48nilqdcbgzmkx0r17k2wfn2") (y #t)))

