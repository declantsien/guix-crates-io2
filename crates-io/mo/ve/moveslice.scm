(define-module (crates-io mo ve moveslice) #:use-module (crates-io))

(define-public crate-moveslice-1.0.0 (c (n "moveslice") (v "1.0.0") (h "093xjjbah7kcl3viwh0r7vhvqkxb1ch00nd55g76lyh7bkkxcdz5")))

(define-public crate-moveslice-1.0.1 (c (n "moveslice") (v "1.0.1") (h "0n8z4d8bjiixnq03mm7dsz2jdw6zdc1qpkswpr684mrd1j3pz7y1")))

(define-public crate-moveslice-1.0.2 (c (n "moveslice") (v "1.0.2") (h "1a2fx4abv4p1cyhcgmds6bca68hdq9w79hj6lls5m3wfwg4j03y2")))

(define-public crate-moveslice-1.0.3 (c (n "moveslice") (v "1.0.3") (h "1hzbjpkn8hrkirx3qrd5k8q7cmw1cdbvd928wg09yb9kbc6dmsjn")))

(define-public crate-moveslice-1.1.0 (c (n "moveslice") (v "1.1.0") (h "0pdnblscmrlkp6m1dv8sanwkdaj5wxv4b37xpjgj2qx7kzlwapq1") (y #t)))

(define-public crate-moveslice-2.0.0 (c (n "moveslice") (v "2.0.0") (h "0i5im6shr32fkjlhb8vphp60syzb1x3mfq0kbd4s4yg4vklaz4k7")))

(define-public crate-moveslice-2.0.1 (c (n "moveslice") (v "2.0.1") (h "01nrfy9zz699901ncc7pjv8rkc40biccbwq6wvsg6jchjgww1xi3")))

