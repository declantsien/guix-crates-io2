(define-module (crates-io mo pa moparse) #:use-module (crates-io))

(define-public crate-moparse-0.1.0 (c (n "moparse") (v "0.1.0") (h "1lhgv6va7ndwq05bag8g1ipv5055l64adx5y5xay18d6frggvyqn") (y #t)))

(define-public crate-moparse-0.1.1-rc1 (c (n "moparse") (v "0.1.1-rc1") (h "1aaddd4bdd5gxl7xih1dzd3y8da6ilgra2l35w2iv0wsm3xw7if1")))

(define-public crate-moparse-0.1.1 (c (n "moparse") (v "0.1.1") (h "0jqlrfi4xil48v8kwgvfsyw2scdnjapzyznkxx85acrcgfqcj7j7")))

(define-public crate-moparse-0.1.2-rc1 (c (n "moparse") (v "0.1.2-rc1") (h "15qd93p1zlx4b21c3mfvkszhm6z4bvd5xbcgz0zpsmp33kr9wwvi")))

(define-public crate-moparse-0.1.2-rc2 (c (n "moparse") (v "0.1.2-rc2") (h "0b274m71vlnmd5li85nfirx3f0dq4ak59yz0znhrzgnrzm2cd9ql")))

(define-public crate-moparse-0.1.2-rc3 (c (n "moparse") (v "0.1.2-rc3") (h "1gp936iixidlcg95cnzqjgdx91h6yc5q3qa49m25ygi1iqgckmlq")))

(define-public crate-moparse-0.1.2 (c (n "moparse") (v "0.1.2") (h "1ll03g6shnxc5vf0yax18bys005di66pi3gqwjkjb87bmflqjcf1")))

(define-public crate-moparse-0.1.3 (c (n "moparse") (v "0.1.3") (h "03c0f3n9q85wxcrfnwfymsa1yrnnkjm47lq0315hq914282nkmwc")))

(define-public crate-moparse-0.1.4-rc1 (c (n "moparse") (v "0.1.4-rc1") (h "0pk3i7cl7cf0sfvd5fdbfwr7pcnf6lcpvb6nzgl2hmz635nn1kab")))

(define-public crate-moparse-0.1.4-rc2 (c (n "moparse") (v "0.1.4-rc2") (h "1pvb1gzxvk7znzwfnih7afrl0xszc86bhnbmxv36lsyqsrj7j82a")))

(define-public crate-moparse-0.1.4-rc3 (c (n "moparse") (v "0.1.4-rc3") (h "12bf75g06is3rzaca05nr22b116qw4rn681m1wz1gb3na2p9bvn4")))

(define-public crate-moparse-0.1.4-rc4 (c (n "moparse") (v "0.1.4-rc4") (h "032qfchg06d6z5qkkikf91hdq6fbzv4122kn8ayg6lkx82ay98mk")))

(define-public crate-moparse-0.1.4 (c (n "moparse") (v "0.1.4") (h "09zw1l0dw2z30ykbs2wb60jbrbasyg5sz3abx2c4vbryg5fkmdvy")))

(define-public crate-moparse-0.1.5-rc1 (c (n "moparse") (v "0.1.5-rc1") (h "08ickkijgf8806xfjr6vx3c4m5iab586rr85jjpnm22raiv3k4yj")))

(define-public crate-moparse-0.1.5-rc2 (c (n "moparse") (v "0.1.5-rc2") (h "0crwcwr286k0mz4sssyc6lnab27najm9byqvvs0xs425kv683if3")))

(define-public crate-moparse-0.1.6 (c (n "moparse") (v "0.1.6") (h "1iky2796is1hgxfxplgvz1qk1sfmyja5fajs97zjn9jhgwk8i19i")))

