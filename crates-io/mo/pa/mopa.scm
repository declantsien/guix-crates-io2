(define-module (crates-io mo pa mopa) #:use-module (crates-io))

(define-public crate-mopa-0.1.0 (c (n "mopa") (v "0.1.0") (h "139m7c62kdkx6n5xclcjfxmlvkkbmql5v6d4jbc768ar335vafk1")))

(define-public crate-mopa-0.1.1 (c (n "mopa") (v "0.1.1") (h "0xd7qvycca7mi186y3yd2ag97413zdmbnfxgnpw8yjz26yrz6m9v")))

(define-public crate-mopa-0.1.2 (c (n "mopa") (v "0.1.2") (h "06cxfk7l6syw1mrknry96dp3y2k8sfxyjkaqf596wimkpqcrk22x")))

(define-public crate-mopa-0.1.3 (c (n "mopa") (v "0.1.3") (h "07nxl9yrxbp7b8qv6x6nlhb0c7wl6k2j9p40xqqch8ccwgpm3lrc")))

(define-public crate-mopa-0.1.4 (c (n "mopa") (v "0.1.4") (h "07c4gmhqmrm2wq9km2mjrjc3iz87wxds5r0499wgxd3nd9dzk7cb")))

(define-public crate-mopa-0.1.5 (c (n "mopa") (v "0.1.5") (h "1gx0c09nw1y39sbwa6kzdwh7cjfz5az65d1r0lxmg85vil48q5b9")))

(define-public crate-mopa-0.1.6 (c (n "mopa") (v "0.1.6") (h "0qm1xmmc27bg2524xnrf138n13a1pxp9m2w7n9zivzxpff7b3s9f")))

(define-public crate-mopa-0.1.7 (c (n "mopa") (v "0.1.7") (h "0xb3rd8z3fnb33n4d5mxrwnyd5w9wiz6zfc2l3xfpy5ccg414l3y") (y #t)))

(define-public crate-mopa-0.1.8 (c (n "mopa") (v "0.1.8") (h "085cn1ivc7z1fc6nknh5m4zjw40scis7mxcn0dd8s9vc5zq0hh4z")))

(define-public crate-mopa-0.2.0 (c (n "mopa") (v "0.2.0") (h "1w3brvc7dgjd3zd5xmy182rvpdldq9nqz6sl1mzjny76c43dsdsd") (f (quote (("no_std"))))))

(define-public crate-mopa-0.2.1 (c (n "mopa") (v "0.2.1") (h "0h5l90ks0aq3l6wyhd33j2bq96jjcvqk1pcw68j0j4c1r4cmczgp") (f (quote (("no_std"))))))

(define-public crate-mopa-0.2.2 (c (n "mopa") (v "0.2.2") (h "05grm5s996fmjs7w2bq6lwrq969gwn1knba6aw7j6v15f41791d7") (f (quote (("no_std_examples" "no_std") ("no_std"))))))

