(define-module (crates-io mo eb moebius-tools) #:use-module (crates-io))

(define-public crate-moebius-tools-0.1.2 (c (n "moebius-tools") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("rustc-serialize" "serde" "unstable-locales"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive" "serde_derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1b73mhg51w8hdm9wax5yppzlma007d9p8kikahcjf9n8nz4r40dx")))

