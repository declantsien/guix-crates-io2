(define-module (crates-io mo gg mogglo-javascript) #:use-module (crates-io))

(define-public crate-mogglo-javascript-0.1.1-rc.1 (c (n "mogglo-javascript") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0b1kz2mk2cm498apd5a4l149r1w6x3mkwkcf8yfsy9lx7bw3a7bz")))

(define-public crate-mogglo-javascript-0.1.1-rc.2 (c (n "mogglo-javascript") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "0qsyjrc4bbs3n2q6grca5clfrbvb5ka26kvxpyw9i1xnpvmh1ba9")))

(define-public crate-mogglo-javascript-0.1.1-rc.3 (c (n "mogglo-javascript") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "1pwrw627rdsjqzg56kiy88jinczic1ix4wx2x4d06nk9ivln5y70")))

(define-public crate-mogglo-javascript-0.1.1 (c (n "mogglo-javascript") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20") (d #t) (k 0)))) (h "13bjqwfwgdmcqznvszjcprd9jhn76fgm62r4cmwz1kjal8j91rn8")))

