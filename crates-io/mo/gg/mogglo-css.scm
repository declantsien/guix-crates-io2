(define-module (crates-io mo gg mogglo-css) #:use-module (crates-io))

(define-public crate-mogglo-css-0.1.0 (c (n "mogglo-css") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "0l882zw0kjzpx44d27kw9pgj1pmz1iw52vny7dzfvjspj0af2fjz")))

(define-public crate-mogglo-css-0.1.1-rc.1 (c (n "mogglo-css") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "0w4h2l2wv4g77slkdi94j6npc8g9qsl46h93dxifk5ixg39v7m8r")))

(define-public crate-mogglo-css-0.1.1-rc.2 (c (n "mogglo-css") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "05afivmgppz7gfk5rgq6wy929qy62qq5vh3yqrliscp4vmzh35jq")))

(define-public crate-mogglo-css-0.1.1-rc.3 (c (n "mogglo-css") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "08rpwyl8sdbpssv26kcsqkbvd2d13y3qdd36zvdj52i5sg6blpjp")))

(define-public crate-mogglo-css-0.1.1 (c (n "mogglo-css") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-css") (r "^0.19") (d #t) (k 0)))) (h "1ckn9qrky3wp8fv9vywqpgwn05c00w61p572259vkdfjf0i53724")))

