(define-module (crates-io mo gg mogglo-typescript) #:use-module (crates-io))

(define-public crate-mogglo-typescript-0.1.1-rc.2 (c (n "mogglo-typescript") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "1iwnklgbjbvic29brijpcl7g5rdlmyyal3a07ciaapwa5l0dkmfy")))

(define-public crate-mogglo-typescript-0.1.1-rc.3 (c (n "mogglo-typescript") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "15g1z8914mjxwwsfv1g0ggnr0s93vjdjlfnaqal6n3yvn4hc3ihf")))

(define-public crate-mogglo-typescript-0.1.1 (c (n "mogglo-typescript") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20") (d #t) (k 0)))) (h "1aalsxrzq9mfvg5n54wyrxiz39sc7clxgvay0sld00hi3wp4s095")))

