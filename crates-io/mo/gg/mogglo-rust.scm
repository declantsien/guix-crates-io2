(define-module (crates-io mo gg mogglo-rust) #:use-module (crates-io))

(define-public crate-mogglo-rust-0.1.1-rc.1 (c (n "mogglo-rust") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1dbm5pb56dkvcmwr00nwjcby05zfhrqhi9sr9ajg00cd8fhy17av")))

(define-public crate-mogglo-rust-0.1.1-rc.2 (c (n "mogglo-rust") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1bqkxg27p7q128qq03cy4v0z75dvw7gf4xcbcs1xprlmc519hdsx")))

(define-public crate-mogglo-rust-0.1.1-rc.3 (c (n "mogglo-rust") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "1i8rqrxd9fw9n4fb9zcnz5q0kg793vx0gvnbbj2bwlzf1qri7kmn")))

(define-public crate-mogglo-rust-0.1.1 (c (n "mogglo-rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.20") (d #t) (k 0)))) (h "03jj1iv0cy5pbsxnj1h1yl4p77lzd5wvw821ya4cwrk5f39zi3ym")))

