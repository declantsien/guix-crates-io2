(define-module (crates-io mo gg mogglo-html) #:use-module (crates-io))

(define-public crate-mogglo-html-0.1.1-rc.1 (c (n "mogglo-html") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "07y2q38cmnp88m7acp3s8dsgpabplbb9sz6jw57avfmgi3s7nfh2")))

(define-public crate-mogglo-html-0.1.1-rc.2 (c (n "mogglo-html") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "1dzzbmcm8m5yd013y3qv7zpcahrldwk5231a7hcb7d6qnfj2xfxq")))

(define-public crate-mogglo-html-0.1.1-rc.3 (c (n "mogglo-html") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "12dz7vq7zz4df2xz8lp8w6gwvssls4ilk805y6g6vfskh02pdpm6")))

(define-public crate-mogglo-html-0.1.1 (c (n "mogglo-html") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-html") (r "^0.19") (d #t) (k 0)))) (h "199sl4pmnb00inh9b0km8619qld3kp1ik8lsb5ilzcmrhhsv9s47")))

