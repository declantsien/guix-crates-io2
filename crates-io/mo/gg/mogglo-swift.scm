(define-module (crates-io mo gg mogglo-swift) #:use-module (crates-io))

(define-public crate-mogglo-swift-0.1.1-rc.2 (c (n "mogglo-swift") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-swift") (r "^0.3") (d #t) (k 0)))) (h "190d3ycklf3vgd3i97jd831h17ri3g2ahk9af4m2g643h30ablbq")))

(define-public crate-mogglo-swift-0.1.1-rc.3 (c (n "mogglo-swift") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-swift") (r "^0.3") (d #t) (k 0)))) (h "01k7xxbqciv82wf1qyzvcy0j1cj3820ddbqh801vgjl2x7g58cqm")))

(define-public crate-mogglo-swift-0.1.1 (c (n "mogglo-swift") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-swift") (r "^0.3") (d #t) (k 0)))) (h "0bab21dw9qbv8wp58lzacsgznc6h0nihq00pfb9970xgfr845794")))

