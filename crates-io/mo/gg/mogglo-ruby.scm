(define-module (crates-io mo gg mogglo-ruby) #:use-module (crates-io))

(define-public crate-mogglo-ruby-0.1.1-rc.1 (c (n "mogglo-ruby") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-ruby") (r "^0.20") (d #t) (k 0)))) (h "0jf1pqxgyybc0w929dk0rd0312jawn4snxwy3h04fjn7gnfz3paq")))

(define-public crate-mogglo-ruby-0.1.1-rc.2 (c (n "mogglo-ruby") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-ruby") (r "^0.20") (d #t) (k 0)))) (h "1j374vj8kjnvlix7c747gim9vrridllg3qnl5ixs3n7bxx2qd3dl")))

(define-public crate-mogglo-ruby-0.1.1-rc.3 (c (n "mogglo-ruby") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-ruby") (r "^0.20") (d #t) (k 0)))) (h "1scj6i7mw67jk7khv957k1blki5zszmrj7fx2kmsy7nrlifim15x")))

(define-public crate-mogglo-ruby-0.1.1 (c (n "mogglo-ruby") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-ruby") (r "^0.20") (d #t) (k 0)))) (h "0h7zk45wnsdayfwykkwkybn9kanacmzlpdj0jdp7q1fkf7x33rr9")))

