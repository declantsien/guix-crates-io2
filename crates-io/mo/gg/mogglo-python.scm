(define-module (crates-io mo gg mogglo-python) #:use-module (crates-io))

(define-public crate-mogglo-python-0.1.1-rc.1 (c (n "mogglo-python") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20") (d #t) (k 0)))) (h "1wd7mxywm5ck06pxgv4a6lvf9fj776vbqp1kfxkk8qjc5793mjm1")))

(define-public crate-mogglo-python-0.1.1-rc.2 (c (n "mogglo-python") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20") (d #t) (k 0)))) (h "1ggc482vwnvdglhyq6s4hb3bb86gz51j6sf2l0wqqrx3wxq0ik2f")))

(define-public crate-mogglo-python-0.1.1-rc.3 (c (n "mogglo-python") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20") (d #t) (k 0)))) (h "1i9p8rma0h7jxxivvl42ww35zmpjc2dxs61s0qnc92l8naarqaxl")))

(define-public crate-mogglo-python-0.1.1 (c (n "mogglo-python") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-python") (r "^0.20") (d #t) (k 0)))) (h "0zrdbqh8av0qhj9i9cbdh78gia0hf40498wap8ja8vbqrzsddp4f")))

