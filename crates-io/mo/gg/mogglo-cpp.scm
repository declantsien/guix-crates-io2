(define-module (crates-io mo gg mogglo-cpp) #:use-module (crates-io))

(define-public crate-mogglo-cpp-0.1.0 (c (n "mogglo-cpp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20") (d #t) (k 0)))) (h "0zqgigvymizcr3slklf07814mhxy8rkqca9yffjxlxpgn4bsma4i")))

(define-public crate-mogglo-cpp-0.1.1-rc.1 (c (n "mogglo-cpp") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20") (d #t) (k 0)))) (h "0075cadmri3z09gp5ysbkqz64i42pv772b6ydd1l80fhn2ncmjqb")))

(define-public crate-mogglo-cpp-0.1.1-rc.2 (c (n "mogglo-cpp") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20") (d #t) (k 0)))) (h "0z3y06fg0xcs3gbv7qbljc7vc8wp4mvmaxahx1n540naiairij4w")))

(define-public crate-mogglo-cpp-0.1.1-rc.3 (c (n "mogglo-cpp") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20") (d #t) (k 0)))) (h "0phphcc3zlapmdk5nvbwhzp2h1svm11dfqinyfh30frmp3n7nzbd")))

(define-public crate-mogglo-cpp-0.1.1 (c (n "mogglo-cpp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20") (d #t) (k 0)))) (h "121w9zjz8xs0cr3bry0lc0w289ps4xcibh42dcwj0j4wavnklxjk")))

