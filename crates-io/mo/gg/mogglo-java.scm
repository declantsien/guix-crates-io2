(define-module (crates-io mo gg mogglo-java) #:use-module (crates-io))

(define-public crate-mogglo-java-0.1.0 (c (n "mogglo-java") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)))) (h "10ccqi9pj9n8hlsszz08rjrlcpm4v7y6jvqlsxdmqvnjrzd5d7qk")))

(define-public crate-mogglo-java-0.1.1-rc.1 (c (n "mogglo-java") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)))) (h "0rhhwmdv1c188czwml03y2z396kraqp4yf6l8y1vwi9rss42vgq2")))

(define-public crate-mogglo-java-0.1.1-rc.2 (c (n "mogglo-java") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)))) (h "0w9fi0l2p2b60pddvaw1cqwkibsxkndpny2j33xqw0f3qhbddrnv")))

(define-public crate-mogglo-java-0.1.1-rc.3 (c (n "mogglo-java") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)))) (h "06lipjzbaxdivccx1j0cdfp1y5fkj1329z7rg2cd4pfp8mr85vwi")))

(define-public crate-mogglo-java-0.1.1 (c (n "mogglo-java") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-java") (r "^0.20") (d #t) (k 0)))) (h "1i4am2c02krfchvxgm9yni42qlgkdlgpi4ff98xsik2a5fbsjl9j")))

