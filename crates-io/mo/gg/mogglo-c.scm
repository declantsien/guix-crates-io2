(define-module (crates-io mo gg mogglo-c) #:use-module (crates-io))

(define-public crate-mogglo-c-0.1.0 (c (n "mogglo-c") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.0") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1m1pnny1qrmj21kr17d6f8zs5v6vpp6mz1dczzf7mhrgqj1d3475")))

(define-public crate-mogglo-c-0.1.1-rc.1 (c (n "mogglo-c") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "1kbi9qah3kx52cylb6pan6lxpw2wrcbi9nq0a9ra8rxjijyw89g9")))

(define-public crate-mogglo-c-0.1.1-rc.2 (c (n "mogglo-c") (v "0.1.1-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.2") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "089hlvz5q1dgk79mgw92z9wy5hn9q18hx0427yrcl8i2v49agmsp")))

(define-public crate-mogglo-c-0.1.1-rc.3 (c (n "mogglo-c") (v "0.1.1-rc.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1-rc.3") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "17p1rjxfi36yca0xj4c7j3gqqhvgfvbikr0h40imxfx0rdn1vvz0")))

(define-public crate-mogglo-c-0.1.1 (c (n "mogglo-c") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mogglo") (r "^0.1.1") (f (quote ("cli"))) (d #t) (k 0)) (d (n "tree-sitter-c") (r "^0.20") (d #t) (k 0)))) (h "13pwqf0vim3k3hgrflir9ygj0zvc88jn5knnawknn2akbzn6nwf2")))

