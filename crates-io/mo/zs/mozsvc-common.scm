(define-module (crates-io mo zs mozsvc-common) #:use-module (crates-io))

(define-public crate-mozsvc-common-0.1.0 (c (n "mozsvc-common") (v "0.1.0") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1yk7ynkmjsa1xy2nbi7mkdly8bijfdikvkzd3q0pw7dhfvaqc8vk")))

(define-public crate-mozsvc-common-0.1.1 (c (n "mozsvc-common") (v "0.1.1") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0bpbmicjivg71s9h9847lyyw7pxc7xgplgbh5wpb3bfn5qcybpzg")))

(define-public crate-mozsvc-common-0.2.0 (c (n "mozsvc-common") (v "0.2.0") (d (list (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1pzcc2p3rzc225s87qmmmlz0n5vps5vj5xq9lj4cl8zdgn6a7ysi")))

