(define-module (crates-io mo zp mozprofile) #:use-module (crates-io))

(define-public crate-mozprofile-0.1.0 (c (n "mozprofile") (v "0.1.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0dyaanbmw43mdjhalynif0q1brvagrmv8l457yv22j7m85wxj1w0")))

(define-public crate-mozprofile-0.2.0 (c (n "mozprofile") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "109wr3fhyi1dnsj3847y2ndir53g32adkihxnbb0bad0m487q5q1")))

(define-public crate-mozprofile-0.2.1 (c (n "mozprofile") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0ra1m0hl19and50l0q2ifvlcc1k9nki69xd2cna98zdskwidx0a9")))

(define-public crate-mozprofile-0.3.0 (c (n "mozprofile") (v "0.3.0") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0zndqyf5xpj72iycwghp6alvm2bgghnijwxhr3qzph0xvsxvh5qs")))

(define-public crate-mozprofile-0.4.0 (c (n "mozprofile") (v "0.4.0") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0b492arp8jkk31cijzxcddlzvkpzx4ira5yid4m0s2300zincak5")))

(define-public crate-mozprofile-0.5.0 (c (n "mozprofile") (v "0.5.0") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "1q6bpkq3h2ri3w04rral4z7p23m3r4dq9ki0a3w9hjkaig0kqicz")))

(define-public crate-mozprofile-0.6.0 (c (n "mozprofile") (v "0.6.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "077a6931zr3zhnlhcpwv8f4l291n6q6z3f5gl9n898rg5s8gw5az")))

(define-public crate-mozprofile-0.7.0 (c (n "mozprofile") (v "0.7.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "13r2l4mv7zgd7pa0cp956199qjg547r3kdqbxiqzrn3f7680p5jq")))

(define-public crate-mozprofile-0.7.1 (c (n "mozprofile") (v "0.7.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "180gl0b7pg3gjcv6wnpkkq4nr91b0xvc9c2mp1fbz8d47j6hlfym")))

(define-public crate-mozprofile-0.7.2 (c (n "mozprofile") (v "0.7.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "18fldl5xqmmpxszpqyilpc8yhvqhfdr3kbbfxm2v26aksb31kl30")))

(define-public crate-mozprofile-0.7.3 (c (n "mozprofile") (v "0.7.3") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "175kx6dj3wcjnxs36fnbyhic051bvqs3isy4y6lh38m9sw7mriq9")))

(define-public crate-mozprofile-0.8.0 (c (n "mozprofile") (v "0.8.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0lkna46w0xh57lsc0ym6kvdjcyh4pmplvjnkvswjv83lvcf2vb65")))

(define-public crate-mozprofile-0.9.0 (c (n "mozprofile") (v "0.9.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1h919pjbdnvllh895jqgx01y0ixp57wwzyang9sz8p0n7k70scnx")))

(define-public crate-mozprofile-0.9.1 (c (n "mozprofile") (v "0.9.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0acmd1x57v364qw0zx5f4z99r08w0xdvpbr7cghwqcqxhshm88p2")))

(define-public crate-mozprofile-0.9.2 (c (n "mozprofile") (v "0.9.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pavjkag5llvg4nckjgmr2v8hz3jpyvflxk5bz7fk65m7fahvxwg")))

