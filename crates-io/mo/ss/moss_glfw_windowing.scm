(define-module (crates-io mo ss moss_glfw_windowing) #:use-module (crates-io))

(define-public crate-moss_glfw_windowing-0.1.0 (c (n "moss_glfw_windowing") (v "0.1.0") (d (list (d (n "glfw") (r "^0.41.0") (d #t) (k 0)) (d (n "moss_windowing") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)))) (h "0a71bnjsvjrm6akcfh99vimdg40rf3jyln688id7wkncrhd8mr6a") (y #t)))

