(define-module (crates-io mo ss moss-oxide) #:use-module (crates-io))

(define-public crate-moss-oxide-0.1.0 (c (n "moss-oxide") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1d544z329a5m9sf3q565847pdmjl1yld8wqg6fjwcwfchr4knqa2")))

(define-public crate-moss-oxide-0.2.0 (c (n "moss-oxide") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1ydfbh2z1p71z0hwrac83zywdagz6wnlyj8mrxas6bvdj19bhg5h")))

(define-public crate-moss-oxide-0.3.0 (c (n "moss-oxide") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0nljf5r8x6lrfh17awlsfl6fps2iqdlffibl413s5gmx80zh1wx3")))

