(define-module (crates-io mo ss mossy) #:use-module (crates-io))

(define-public crate-mossy-0.1.0 (c (n "mossy") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "175inf1h1d0wdpgvix125ai1mlx87hmj6a0a9krsmprpd7xjjq77")))

(define-public crate-mossy-0.1.1 (c (n "mossy") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10pijbga93jrk53nl4iac9cgca26r5715x2xi7r6nq60s2c5b8xm")))

(define-public crate-mossy-0.0.1 (c (n "mossy") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "037bx6mniv29nmss4il4ql73nb5s5h0h8qr5swgmiamhw8w8hrxs")))

(define-public crate-mossy-0.1.2 (c (n "mossy") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cfamrpm86051qkzh8ywxq6cjj0z5s4r96lbv8rq60paqrc7hpkd")))

