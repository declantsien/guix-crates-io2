(define-module (crates-io mo ss moss-trimmer) #:use-module (crates-io))

(define-public crate-moss-trimmer-0.1.0 (c (n "moss-trimmer") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lm6b9clyczqiikjfqfdq8i7r51gh0z2vvb5avp6m1nvxqfkkbbi")))

(define-public crate-moss-trimmer-1.0.1 (c (n "moss-trimmer") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m9aca0l0qcd9rjh0fra6mpfkg6sbrjdqmkffs6kby9bfgspap6b")))

(define-public crate-moss-trimmer-1.0.2 (c (n "moss-trimmer") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1viv1rjsh4hkfx5m3773kxbyid9nwx30185qf2av8rakn9j0cqlh")))

(define-public crate-moss-trimmer-1.0.3 (c (n "moss-trimmer") (v "1.0.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19skzsqzimnqfclc5rza3db6394n6324ycp7sb8x7n4raw8ayz30")))

(define-public crate-moss-trimmer-1.1.0 (c (n "moss-trimmer") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ylzhs00n3hmil1fgyawllky9i3i4g6hqawalf8ya51d17d1rgjh")))

(define-public crate-moss-trimmer-1.1.1 (c (n "moss-trimmer") (v "1.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "190nxr12rl310m4msbiw5b11qg6c1x59anwbhr9v427xaj288r44")))

