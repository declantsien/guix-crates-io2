(define-module (crates-io mo ss moss) #:use-module (crates-io))

(define-public crate-moss-0.0.1 (c (n "moss") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "1wkja54f1pyxg321kxn3l3lb3rgg6hzzq58w783b86w54zhymg47")))

(define-public crate-moss-0.0.2 (c (n "moss") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "0qb1g8a3avgb8qh63rrb7q13hc318yl8r52i20inh3n8lxdcj5j9")))

(define-public crate-moss-0.0.3 (c (n "moss") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)))) (h "0skxqahkhnijas5nr3vcnyqzm56aym1fvq8k2ipk9rwypjv5c9y7") (f (quote (("math-la") ("long-gmp") ("default" "long-gmp" "math-la"))))))

(define-public crate-moss-0.0.4 (c (n "moss") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0lgsz12vpzpfx0fz1z6v4smbhh2vsmslxhmc6flb6l3cx0hrsybk") (f (quote (("math-sf") ("math-la") ("long-none") ("long-gmp") ("la") ("graphics") ("default" "long-gmp" "la" "math-la" "math-sf" "graphics"))))))

