(define-module (crates-io mo ss moss-fmt) #:use-module (crates-io))

(define-public crate-moss-fmt-0.1.0 (c (n "moss-fmt") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "15ylann16byljzsqhzy1dmms34bbw0q5jgq9b6gsjhwsi4hyn36k")))

(define-public crate-moss-fmt-0.2.0 (c (n "moss-fmt") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "19a577h0w29ll1916fn48zc7xf4y7j3crjp52blyi23bgh6kprw1")))

