(define-module (crates-io mo ss mossfets-game-of-life) #:use-module (crates-io))

(define-public crate-mossfets-game-of-life-0.7.0 (c (n "mossfets-game-of-life") (v "0.7.0") (d (list (d (n "rayon") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0z3ajdlym92rbdv36bk26c6gfa9wpcpdyzsxqiwrgsniyj37mknl") (f (quote (("default" "advanced_threading" "advanced_hashing")))) (s 2) (e (quote (("advanced_threading" "dep:rayon") ("advanced_hashing" "dep:rustc-hash"))))))

(define-public crate-mossfets-game-of-life-0.8.0 (c (n "mossfets-game-of-life") (v "0.8.0") (d (list (d (n "rayon") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "06i9w9gw78ab267x3wrrp0ms7mwq9jdbfrh6a4qwzqg05gshlj00") (f (quote (("default" "advanced_threading" "advanced_hashing")))) (s 2) (e (quote (("advanced_threading" "dep:rayon") ("advanced_hashing" "dep:rustc-hash"))))))

