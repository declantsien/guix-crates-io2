(define-module (crates-io mo on moonshine-spawn) #:use-module (crates-io))

(define-public crate-moonshine-spawn-0.1.0 (c (n "moonshine-spawn") (v "0.1.0") (d (list (d (n "bevy") (r "0.12.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.12.*") (d #t) (k 0)))) (h "142sd5dh1j9ypjqf34xs539iyvy3n4mzz9hk1bza1ja68nvx75xd")))

(define-public crate-moonshine-spawn-0.1.1 (c (n "moonshine-spawn") (v "0.1.1") (d (list (d (n "bevy") (r "0.12.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.12.*") (d #t) (k 0)))) (h "075myv9zk63xqg090bsfy239591drar8mlpbkcjpgk2ivbbcxrbm")))

(define-public crate-moonshine-spawn-0.1.2 (c (n "moonshine-spawn") (v "0.1.2") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "0g9n1pv366yw57z1pwsi97714740464iiha04rq3kjp18l7j8kg2")))

(define-public crate-moonshine-spawn-0.2.0 (c (n "moonshine-spawn") (v "0.2.0") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "10ql64krh6p3kahkajgkvr7l3946593qqllw88dc5bxbz6zk6ar1")))

(define-public crate-moonshine-spawn-0.2.1 (c (n "moonshine-spawn") (v "0.2.1") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "069a1nq6ms05drymandygcafspycn8xx1x6qxva17gw1miif3590")))

