(define-module (crates-io mo on moonshine-object) #:use-module (crates-io))

(define-public crate-moonshine-object-0.1.0 (c (n "moonshine-object") (v "0.1.0") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_core") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "moonshine-kind") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-util") (r "0.2.*") (d #t) (k 0)))) (h "1kj611avyg724qcd7wyxhl84nsmz2775f186szpghcv4wh7d9icg")))

(define-public crate-moonshine-object-0.1.1 (c (n "moonshine-object") (v "0.1.1") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_core") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)) (d (n "moonshine-kind") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-util") (r "0.2.*") (d #t) (k 0)))) (h "1alc41lv4rv27lbmmsy8c9lbd01bm7rxiq01shqb2ra5a9szwa2l")))

(define-public crate-moonshine-object-0.1.2 (c (n "moonshine-object") (v "0.1.2") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_core") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)) (d (n "moonshine-kind") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-util") (r "0.2.*") (d #t) (k 0)))) (h "0kf5xnwyhrv43yi8lccry3y434szzxr6ykaawmca5l56h6i38yv8")))

