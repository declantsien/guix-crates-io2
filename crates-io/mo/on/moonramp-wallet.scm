(define-module (crates-io mo on moonramp-wallet) #:use-module (crates-io))

(define-public crate-moonramp-wallet-0.1.22 (c (n "moonramp-wallet") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("random" "serialization"))) (d #t) (k 0)) (d (n "moonramp-entity") (r "^0.1") (o #t) (d #t) (k 0)))) (h "179nsxw0gkh7wqk1yj6mai8f1137jbdxgcb8rrqzxgdhma6sv5dp") (f (quote (("entity" "moonramp-entity") ("default" "all-currencies") ("bitcoin" "moonramp-core/crypto-currency-bitcoin") ("all-currencies" "bitcoin"))))))

