(define-module (crates-io mo on moondancer-pac) #:use-module (crates-io))

(define-public crate-moondancer-pac-0.0.0 (c (n "moondancer-pac") (v "0.0.0") (d (list (d (n "critical-section") (r "=1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "=0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "=0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "=0.1.3") (d #t) (k 0)))) (h "0kw0r0xmai33fp2qyly1a3grvhjwkxxapaip8k6rbkviz2srh2i3") (f (quote (("vexriscv") ("rt" "riscv-rt") ("minerva") ("default" "vexriscv")))) (y #t) (r "1.68")))

(define-public crate-moondancer-pac-0.0.1 (c (n "moondancer-pac") (v "0.0.1") (d (list (d (n "critical-section") (r "=1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "=0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "=0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "=0.1.3") (d #t) (k 0)))) (h "18k2xq919smp31apf1hl0f00a5zblk56905l1jka0gb876j6xhry") (f (quote (("vexriscv") ("rt" "riscv-rt") ("minerva") ("default" "vexriscv")))) (r "1.68")))

