(define-module (crates-io mo on moonboot-macros) #:use-module (crates-io))

(define-public crate-moonboot-macros-0.1.0 (c (n "moonboot-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m3gx8zzvsq4ddnmbqhcv32iqkrm4sd547b8fhpg4rxs9y82caxx")))

(define-public crate-moonboot-macros-0.1.1 (c (n "moonboot-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g1v1wavv8sjgc9dggrrmh56nsznm0kqbiwgwy4ywmdadxxlprxb")))

(define-public crate-moonboot-macros-0.1.2 (c (n "moonboot-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xz4rra61bbw74jsnil25i0f27d2lirlhihj8a18s5f30j38wnz1")))

