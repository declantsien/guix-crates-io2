(define-module (crates-io mo on moonramp-sale) #:use-module (crates-io))

(define-public crate-moonramp-sale-0.1.22 (c (n "moonramp-sale") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "moonramp-entity") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "moonramp-lunar") (r "^0.1.0") (d #t) (k 0)) (d (n "moonramp-wallet") (r "^0.1") (f (quote ("entity" "all-currencies"))) (d #t) (k 0)))) (h "0xinws0mrspl081bc4d3kav4z844wl3ijjmf5zpazl7dxxhilscc") (f (quote (("entity" "moonramp-entity"))))))

