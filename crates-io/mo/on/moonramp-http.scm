(define-module (crates-io mo on moonramp-http) #:use-module (crates-io))

(define-public crate-moonramp-http-0.1.22 (c (n "moonramp-http") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("lib" "http"))) (d #t) (k 0)) (d (n "moonramp-encryption") (r "^0.1") (d #t) (k 0)) (d (n "moonramp-entity") (r "^0.1") (d #t) (k 0)) (d (n "moonramp-migration") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)))) (h "03qmb75kw06rzrpgfdqqba4r73m5p87zcvf4s256lj8i6wl7djg5")))

