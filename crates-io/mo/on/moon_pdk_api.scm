(define-module (crates-io mo on moon_pdk_api) #:use-module (crates-io))

(define-public crate-moon_pdk_api-0.0.1 (c (n "moon_pdk_api") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.3.1") (d #t) (k 0)))) (h "1rylsg00y3js4zrkj79xy3dy00jf30lq3h64rjyivl2y76mjqp10")))

(define-public crate-moon_pdk_api-0.0.2 (c (n "moon_pdk_api") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.3.2") (d #t) (k 0)))) (h "0s2x6q5fgxh7hk3ijv2zp58i82mn8qw4awx36f194s247krn17zn")))

(define-public crate-moon_pdk_api-0.0.4 (c (n "moon_pdk_api") (v "0.0.4") (d (list (d (n "moon_common") (r "^0.0.3") (d #t) (k 0)) (d (n "moon_config") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.1") (d #t) (k 0)))) (h "085dgyk51bm5kfi6q8d3v4qmxwa8frzg0ppxnv6gnn4k0z962nic")))

(define-public crate-moon_pdk_api-0.0.5 (c (n "moon_pdk_api") (v "0.0.5") (d (list (d (n "moon_common") (r "^0.0.4") (d #t) (k 0)) (d (n "moon_config") (r "^0.0.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.5") (d #t) (k 0)))) (h "1g1zijql5f3ks048zvzbd3i94qchj1s7dgxkh09sp185jhqgw33i")))

(define-public crate-moon_pdk_api-0.0.6 (c (n "moon_pdk_api") (v "0.0.6") (d (list (d (n "moon_common") (r "^0.0.5") (d #t) (k 0)) (d (n "moon_config") (r "^0.0.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.5.1") (d #t) (k 0)))) (h "1cc0vwr5av8sgrjysmvf6176lhjls8v10bzwfwbrpvvp4h467pvc")))

