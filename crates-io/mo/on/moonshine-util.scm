(define-module (crates-io mo on moonshine-util) #:use-module (crates-io))

(define-public crate-moonshine-util-0.1.0 (c (n "moonshine-util") (v "0.1.0") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)))) (h "1wy431lqfqxc5rpgfw74zh9rsk4j10ydpwgd71ha5nh9k7bdbnms")))

(define-public crate-moonshine-util-0.2.0 (c (n "moonshine-util") (v "0.2.0") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)))) (h "1kqx1msgc5my0jdvbjqfkdqzj7g8wcpzka2fir72wq0xrqxpn3r4")))

(define-public crate-moonshine-util-0.2.1 (c (n "moonshine-util") (v "0.2.1") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)))) (h "0mi9cpl5i7vkwfkwv04zgshm9iaqlasicx3dpg48l49kqhgwr9sn") (y #t)))

(define-public crate-moonshine-util-0.2.2 (c (n "moonshine-util") (v "0.2.2") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)))) (h "0an7pk86dnsn4q42zjbanrfb3gm2xf9l4f2s2171ikqm06rgcgcr")))

(define-public crate-moonshine-util-0.2.3 (c (n "moonshine-util") (v "0.2.3") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_hierarchy") (r "0.13.*") (d #t) (k 0)))) (h "1a4qv2b97bp5qpb9anyg7kg2zvdy4mz8lg7qj9i7w2xra3p17yyq")))

