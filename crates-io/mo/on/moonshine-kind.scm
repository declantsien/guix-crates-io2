(define-module (crates-io mo on moonshine-kind) #:use-module (crates-io))

(define-public crate-moonshine-kind-0.1.0 (c (n "moonshine-kind") (v "0.1.0") (d (list (d (n "bevy") (r "0.11.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.11.*") (d #t) (k 0)))) (h "15salngaw27v9x6m5ziykj0jrhm6rwmw14sccl5qzi53i00sjnws")))

(define-public crate-moonshine-kind-0.1.1 (c (n "moonshine-kind") (v "0.1.1") (d (list (d (n "bevy") (r "0.12.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.12.*") (d #t) (k 0)))) (h "1laqr9linh5gc6rh12phkk69w1ibxv3x6jm4fcis3bmhrnyv8di0")))

(define-public crate-moonshine-kind-0.1.2 (c (n "moonshine-kind") (v "0.1.2") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "1p7w8d7rkgndggfgn77jvrsd8rmls1l7jmcvgpnbhs10qcnblv1f")))

(define-public crate-moonshine-kind-0.1.3 (c (n "moonshine-kind") (v "0.1.3") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "1ks61wlfhkz22wdblrpkz35v344ihsrnma6ivlfk00dzfgg9lh54")))

(define-public crate-moonshine-kind-0.1.4 (c (n "moonshine-kind") (v "0.1.4") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "07vsri6sw8v4vai261xlm837xj6qvidj7wdigd70la729hj7msnl")))

(define-public crate-moonshine-kind-0.1.5 (c (n "moonshine-kind") (v "0.1.5") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "0kwc4pix1v7crh191izxg6lcvq9zx7g46c691zszjyp2gh05ni4k")))

(define-public crate-moonshine-kind-0.1.6 (c (n "moonshine-kind") (v "0.1.6") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "1hk435axy05pq3sazaqrravqj8fc8w8ac7x36zwpisqbz57q8hqx")))

