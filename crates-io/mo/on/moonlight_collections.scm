(define-module (crates-io mo on moonlight_collections) #:use-module (crates-io))

(define-public crate-moonlight_collections-0.1.0 (c (n "moonlight_collections") (v "0.1.0") (h "0f87idjpzc7nk1agpj994nvpp6lxvnm250i4zqwyvs8cq286zscb")))

(define-public crate-moonlight_collections-0.1.1 (c (n "moonlight_collections") (v "0.1.1") (h "19rgk6m117c5b3xidad0x8y2h0gkm7z9gcwkqjhbkykbm83sn12y")))

