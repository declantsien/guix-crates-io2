(define-module (crates-io mo on moonramp-lunar-macro) #:use-module (crates-io))

(define-public crate-moonramp-lunar-macro-0.1.22 (c (n "moonramp-lunar-macro") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "0r1r4v5d75dv0nmghrgygz32mn5p99wks797i89p5kyrc5fflqyg")))

