(define-module (crates-io mo on moon_math) #:use-module (crates-io))

(define-public crate-moon_math-0.1.0 (c (n "moon_math") (v "0.1.0") (h "1pnsp6jq6qzv9j2nnrqlj4jlz3vabzdlskkjlfz78rk8ig4a9500")))

(define-public crate-moon_math-0.1.1 (c (n "moon_math") (v "0.1.1") (h "0dngsl0bc74hsdpk169awhln1wgy44s1r5xwc81mi7ljyqml711i")))

(define-public crate-moon_math-0.1.2 (c (n "moon_math") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)))) (h "0pa5zqm75byapg7gz98qvp5b3inli5way5qq5cw2lf3cz1h7w4f4")))

(define-public crate-moon_math-0.1.3 (c (n "moon_math") (v "0.1.3") (d (list (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)))) (h "12y26zjfwwa50hadvizzcs2v1cq2l17a81yavzz0lv5r9l750lh6")))

