(define-module (crates-io mo on moonlander-gp) #:use-module (crates-io))

(define-public crate-moonlander-gp-0.1.0 (c (n "moonlander-gp") (v "0.1.0") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0a3x0f1z7iaf9nz5bgal8056i0wsnk744paign13zarp20rj2f8f")))

(define-public crate-moonlander-gp-0.1.1 (c (n "moonlander-gp") (v "0.1.1") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0djn61v8p5w0k2rn5h2m2kyfkwlrwg7srfs11iy8bx3rg8rd682l")))

