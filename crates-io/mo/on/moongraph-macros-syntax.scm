(define-module (crates-io mo on moongraph-macros-syntax) #:use-module (crates-io))

(define-public crate-moongraph-macros-syntax-0.1.0 (c (n "moongraph-macros-syntax") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "079cs26ilgy0y4slivbmlb4yq8vi62hvvxyqnn7w4b69mj3j0488")))

