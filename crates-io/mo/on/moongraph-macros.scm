(define-module (crates-io mo on moongraph-macros) #:use-module (crates-io))

(define-public crate-moongraph-macros-0.1.0 (c (n "moongraph-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19nb3y70vn7la4mx53f401n14bj5ljvhphq419d2674abbhr2x0f")))

(define-public crate-moongraph-macros-0.1.1 (c (n "moongraph-macros") (v "0.1.1") (d (list (d (n "moongraph") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1icvcp98nw29pjda2qf7xnfs34cp3c37ssghrvkps54d492vv40p")))

(define-public crate-moongraph-macros-0.1.2 (c (n "moongraph-macros") (v "0.1.2") (d (list (d (n "moongraph") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15kzrla7m436sy8hnwli4vf36bmy7vhc3xyq10799mryn2xhgjaz")))

(define-public crate-moongraph-macros-0.1.3 (c (n "moongraph-macros") (v "0.1.3") (d (list (d (n "moongraph") (r "^0.4") (d #t) (k 2)) (d (n "moongraph-macros-syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ddmyhsj6n3dddl0ls6kpjfj33kfcq1gqcm3isbc65d99lk6gmy5")))

