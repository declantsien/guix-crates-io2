(define-module (crates-io mo on moonramp-lunar) #:use-module (crates-io))

(define-public crate-moonramp-lunar-0.1.22 (c (n "moonramp-lunar") (v "0.1.22") (d (list (d (n "moonramp-lunar-core") (r "^0.1") (d #t) (k 0)) (d (n "moonramp-lunar-macro") (r "^0.1") (d #t) (k 0)))) (h "13f0qzb6sl986ankxwfzcnzan3kvc6qisynmzlh283qvidjhzf16")))

