(define-module (crates-io mo on moonfire-ffmpeg) #:use-module (crates-io))

(define-public crate-moonfire-ffmpeg-0.0.1 (c (n "moonfire-ffmpeg") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cstr") (r "^0.2.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1ynfl41l9fc9dncd132011x6ixc8r370nwv5nnvpgvga9f03bsmw") (f (quote (("swscale"))))))

(define-public crate-moonfire-ffmpeg-0.0.2 (c (n "moonfire-ffmpeg") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cstr") (r "^0.2.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0dd1330myihrg7f1wilzk7phrnbw0xmxc70rk63mf0a255x0pf6c") (f (quote (("swscale")))) (r "1.49.0")))

