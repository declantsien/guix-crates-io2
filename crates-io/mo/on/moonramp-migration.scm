(define-module (crates-io mo on moonramp-migration) #:use-module (crates-io))

(define-public crate-moonramp-migration-0.1.22 (c (n "moonramp-migration") (v "0.1.22") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "moonramp-core") (r "^0.1") (f (quote ("async-core" "crypto" "sql"))) (d #t) (k 0)) (d (n "moonramp-encryption") (r "^0.1") (d #t) (k 0)) (d (n "moonramp-entity") (r "^0.1") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.8.0") (k 0)))) (h "0523xdgh0x4zqy35zpm7ss2s7wd7j0q4j53qqjja3ajl9pnf6nrf") (f (quote (("testing"))))))

