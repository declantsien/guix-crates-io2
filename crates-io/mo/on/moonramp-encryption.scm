(define-module (crates-io mo on moonramp-encryption) #:use-module (crates-io))

(define-public crate-moonramp-encryption-0.1.22 (c (n "moonramp-encryption") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("lib" "sql"))) (d #t) (k 0)) (d (n "moonramp-entity") (r "^0.1") (d #t) (k 0)))) (h "0v8z6m81qsqj394402mzm2vbn5jvih4p9n25gcnqy1bdfiyk6271") (f (quote (("testing" "moonramp-entity/testing"))))))

