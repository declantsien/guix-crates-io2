(define-module (crates-io mo on moonshine-core) #:use-module (crates-io))

(define-public crate-moonshine-core-0.1.0 (c (n "moonshine-core") (v "0.1.0") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "moonshine-kind") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-object") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-save") (r "0.3.*") (d #t) (k 0)) (d (n "moonshine-spawn") (r "0.2.*") (d #t) (k 0)) (d (n "moonshine-util") (r "0.2.*") (d #t) (k 0)))) (h "0z61jliwnkgqly6ys4prn79ylqbrjzmcyb7dccwsb71wc654cf7g")))

(define-public crate-moonshine-core-0.1.1 (c (n "moonshine-core") (v "0.1.1") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "moonshine-kind") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-object") (r "0.1.*") (d #t) (k 0)) (d (n "moonshine-save") (r "0.3.*") (d #t) (k 0)) (d (n "moonshine-spawn") (r "0.2.*") (d #t) (k 0)) (d (n "moonshine-util") (r "0.2.*") (d #t) (k 0)))) (h "042218akcq0g24641sr78snhiqh7lcvdz458v2gkg89j3l71y9v9")))

