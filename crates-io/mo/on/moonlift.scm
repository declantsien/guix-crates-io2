(define-module (crates-io mo on moonlift) #:use-module (crates-io))

(define-public crate-moonlift-0.1.0-alpha.0 (c (n "moonlift") (v "0.1.0-alpha.0") (d (list (d (n "cranelift") (r "^0.98.1") (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.98.1") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.98.1") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.98.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.46") (d #t) (k 0)))) (h "0if4yppck6cmp5lm5s3ap4i0xnr057f4k1i05wrsb0nirp04rz4w") (r "1.65")))

