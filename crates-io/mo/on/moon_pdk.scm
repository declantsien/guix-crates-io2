(define-module (crates-io mo on moon_pdk) #:use-module (crates-io))

(define-public crate-moon_pdk-0.0.1 (c (n "moon_pdk") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("std" "error-context" "derive"))) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "moon_pdk_api") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.1.0") (d #t) (k 0)))) (h "0vvr72gyy1ajpvv3pryqqa700rlhpggsy91lsqclxgkap4jll69l")))

(define-public crate-moon_pdk-0.0.2 (c (n "moon_pdk") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("std" "error-context" "derive"))) (k 0)) (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "moon_pdk_api") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.1.0") (d #t) (k 0)))) (h "1q6cd8grhjjm7qgwx21xz0njrhw898i3qpb6ai8d07791j2p1nd4")))

(define-public crate-moon_pdk-0.0.4 (c (n "moon_pdk") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("std" "error-context" "derive"))) (k 0)) (d (n "extism-pdk") (r "^1.0.1") (d #t) (k 0)) (d (n "moon_pdk_api") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.1") (d #t) (k 0)))) (h "1wqgf5da1kqz1a81r5w0lm07m86xwf4gi8qjvqq3b6brysdmdd97")))

(define-public crate-moon_pdk-0.0.5 (c (n "moon_pdk") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("std" "error-context" "derive"))) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "moon_pdk_api") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.2.4") (d #t) (k 0)))) (h "0v0dhypik4m8y159ga01gmdxqm4hacsirl9d0nkqzwcyqfx8cgdx")))

(define-public crate-moon_pdk-0.0.6 (c (n "moon_pdk") (v "0.0.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("std" "error-context" "derive"))) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "moon_pdk_api") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_pdk") (r "^0.3.1") (d #t) (k 0)))) (h "1kcivxm38kbw9n97icidc2js91vpm0i6r48y0fcmxdrsxla3h7ii")))

