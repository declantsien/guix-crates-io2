(define-module (crates-io mo on moonshine-behavior) #:use-module (crates-io))

(define-public crate-moonshine-behavior-0.1.0 (c (n "moonshine-behavior") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "bevy_app") (r "0.10.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.10.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.10.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.10.*") (d #t) (k 0)))) (h "1pg8qvqyqzzwzgnmilqihp5hpbbv6yrnlm1ixd46cv8lv4hfxqrc")))

(define-public crate-moonshine-behavior-0.1.1 (c (n "moonshine-behavior") (v "0.1.1") (d (list (d (n "bevy") (r "0.11.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.11.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.11.*") (d #t) (k 0)))) (h "1v9fplxw4sbzxixp2z3132qsfswwxa56vxkb08zk4c3nlxjqzcds")))

(define-public crate-moonshine-behavior-0.1.2 (c (n "moonshine-behavior") (v "0.1.2") (d (list (d (n "bevy") (r "0.12.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.12.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.12.*") (d #t) (k 0)))) (h "1cin2ybczpcfn0s5457yhp93k4g0cwsfr6fnxs9q1wpnz7a3gd67")))

(define-public crate-moonshine-behavior-0.1.3 (c (n "moonshine-behavior") (v "0.1.3") (d (list (d (n "bevy") (r "0.13.*") (d #t) (k 2)) (d (n "bevy_app") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_ecs") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_reflect") (r "0.13.*") (d #t) (k 0)) (d (n "bevy_utils") (r "0.13.*") (d #t) (k 0)))) (h "1b9rr2z9yhrzj3is36dn4njpm0agkrfpcxfmhaclncwzn0wb4g11")))

