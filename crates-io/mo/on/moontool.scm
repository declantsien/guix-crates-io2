(define-module (crates-io mo on moontool) #:use-module (crates-io))

(define-public crate-moontool-0.0.1 (c (n "moontool") (v "0.0.1") (d (list (d (n "time") (r "^0.3") (f (quote ("std" "local-offset"))) (d #t) (k 0)))) (h "02rmpn7424z91wshkxfjv35w7pqvzz6wsrb7d41xn1s9bh7gi05i")))

(define-public crate-moontool-0.1.0 (c (n "moontool") (v "0.1.0") (d (list (d (n "time") (r "^0.3") (f (quote ("std" "parsing" "local-offset"))) (d #t) (k 0)))) (h "07qg1gagspnpcdddrv0ydsjgjnj35kkiyf6cxi1g07n5lvi5cksh")))

(define-public crate-moontool-0.2.0 (c (n "moontool") (v "0.2.0") (d (list (d (n "time") (r "^0.3") (f (quote ("std" "parsing" "local-offset"))) (d #t) (k 0)))) (h "1s89mrjhi562l33mym1dd7slyfsjn8mi5czm1gww1s2v6k0d2z1k")))

(define-public crate-moontool-0.3.0 (c (n "moontool") (v "0.3.0") (d (list (d (n "textcanvas") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std" "parsing" "local-offset"))) (d #t) (k 0)))) (h "0cswmdszy45ngk4b9jkqjivfcr8abqj2py12nzlxpg1ipwkbyzya") (f (quote (("canvas" "textcanvas"))))))

