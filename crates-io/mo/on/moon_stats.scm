(define-module (crates-io mo on moon_stats) #:use-module (crates-io))

(define-public crate-moon_stats-0.1.0 (c (n "moon_stats") (v "0.1.0") (h "1mrq9m21m3q1ws10ds7j5zahcma1a55plam7nkyg3c3l88qc6cz2") (y #t)))

(define-public crate-moon_stats-0.1.1 (c (n "moon_stats") (v "0.1.1") (h "1fi2z1qn2ww2hzxmj54srmz8yakgflbxabj8h2q05i9hhbry8i0m")))

(define-public crate-moon_stats-0.1.2 (c (n "moon_stats") (v "0.1.2") (h "0mnz1pyr4gazdr13vf544aqxwsr4nsdzy8v1hxgm65ynv3jjlvq9")))

