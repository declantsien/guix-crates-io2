(define-module (crates-io mo on moonramp-rpc) #:use-module (crates-io))

(define-public crate-moonramp-rpc-0.1.22 (c (n "moonramp-rpc") (v "0.1.22") (d (list (d (n "jsonrpsee") (r "^0.14.0") (f (quote ("macros" "server"))) (k 2)) (d (n "moonramp-core") (r "^0.1") (f (quote ("lib" "http" "jsonrpc"))) (d #t) (k 0)))) (h "0x8kc1qd78jdqnfhkm1dk71i9j5vi2lchxxvyfbv6ikrm4p0rghs")))

