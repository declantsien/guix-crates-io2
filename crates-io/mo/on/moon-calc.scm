(define-module (crates-io mo on moon-calc) #:use-module (crates-io))

(define-public crate-moon-calc-1.0.0 (c (n "moon-calc") (v "1.0.0") (h "04ivbcjkikgzvx98wz2l84h7dzifr14jqr6imn4y6jmcl1x9d50k")))

(define-public crate-moon-calc-1.0.1 (c (n "moon-calc") (v "1.0.1") (h "1ijwm04rgpjqy0l607hi58vjba75xy5dc518xxs1wi5rf3kjycyl")))

