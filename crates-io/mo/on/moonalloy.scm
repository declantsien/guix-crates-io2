(define-module (crates-io mo on moonalloy) #:use-module (crates-io))

(define-public crate-moonalloy-0.1.0 (c (n "moonalloy") (v "0.1.0") (h "0304jv4kp34x1ham7hf1yx64hy6b33kplymk7ib0klzgbvch19p1")))

(define-public crate-moonalloy-0.1.1 (c (n "moonalloy") (v "0.1.1") (h "1kwilcjnrkgwcl3hd671zim9il01d9ykyq1y59011rzkvpnlzksn")))

(define-public crate-moonalloy-0.2.0 (c (n "moonalloy") (v "0.2.0") (h "1li56fxrg3cindx3cpzd4dwdpl0xp6psqr1yf1ghmjbn8xddpkmz")))

(define-public crate-moonalloy-0.3.0 (c (n "moonalloy") (v "0.3.0") (h "1jqphj8f9h642mysj7nl437lsjsjqbibiz832qzyjrnxsn5m29an")))

(define-public crate-moonalloy-0.3.1 (c (n "moonalloy") (v "0.3.1") (h "1ybldnai3rjdqpwizzhj3pan43v7zjzl7qvphwafvnsniixh3nx2")))

(define-public crate-moonalloy-0.3.2 (c (n "moonalloy") (v "0.3.2") (h "0gcvhp3avmywd4fksd84j5pfibfxz86b8l6f5vp4hibzl55s5rvx")))

(define-public crate-moonalloy-0.3.3 (c (n "moonalloy") (v "0.3.3") (h "0zkchxhv0i2019fw7vvc1k4ccjlv5ayqq0xagj76xfg6w8wcnvpr") (y #t)))

(define-public crate-moonalloy-0.3.4 (c (n "moonalloy") (v "0.3.4") (h "1vhjgsngq1f59c6jch0xs5bwrq8nyjry753sfkn6zc4y616hzz2l")))

