(define-module (crates-io mo on moonramp-program) #:use-module (crates-io))

(define-public crate-moonramp-program-0.1.22 (c (n "moonramp-program") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("lib" "wasm"))) (d #t) (k 0)) (d (n "moonramp-gateway") (r "^0.1") (d #t) (k 0)) (d (n "moonramp-lunar") (r "^0.1") (d #t) (k 0)))) (h "17rbal6inivdgsiywyq1kvwnfg4zsm8glng00sm7zj5ax7x2gkqf")))

