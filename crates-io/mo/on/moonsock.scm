(define-module (crates-io mo on moonsock) #:use-module (crates-io))

(define-public crate-moonsock-0.1.0 (c (n "moonsock") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0rf1v7ydka0mx56563zvdy2vy960lb2pinz5787vb72fhxy13li2")))

(define-public crate-moonsock-0.1.1 (c (n "moonsock") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1n43i33mzwrwvarlg6n22nz9xvq6zbddwz9khif1z2khs2540m5b")))

(define-public crate-moonsock-0.1.2 (c (n "moonsock") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0my8kjk6zxa2rpydmmk22h7nf18pas9j19mvijh7mxh189zbl1n7")))

