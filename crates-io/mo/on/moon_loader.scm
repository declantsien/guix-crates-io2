(define-module (crates-io mo on moon_loader) #:use-module (crates-io))

(define-public crate-moon_loader-0.1.0 (c (n "moon_loader") (v "0.1.0") (h "0mxsxzkv0f7pk2p802vb4cfdlkv94xn8d1c9p921gc0r9ml15hy1")))

(define-public crate-moon_loader-0.1.1 (c (n "moon_loader") (v "0.1.1") (h "0g1943lpfywxd78qikgckqdcanr10l85c817dkk967fd65kaik85")))

