(define-module (crates-io mo on moonramp-gateway) #:use-module (crates-io))

(define-public crate-moonramp-gateway-0.1.22 (c (n "moonramp-gateway") (v "0.1.22") (d (list (d (n "moonramp-core") (r "^0.1") (f (quote ("http" "serialization" "wasm"))) (d #t) (k 0)) (d (n "moonramp-lunar") (r "^0.1.0") (d #t) (k 0)))) (h "1yqdmjzd96af9b2ac7mxdzmhihx39h01sllp6sxr449n3wniqz93") (f (quote (("default" "all-currencies") ("bitcoin" "moonramp-core/crypto-currency-bitcoin") ("all-currencies" "bitcoin"))))))

