(define-module (crates-io mo on moon-phases) #:use-module (crates-io))

(define-public crate-moon-phases-0.1.0 (c (n "moon-phases") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "0h1n0cr1b0zpvwvgaykf6wkbhi87jr9dcjapqbaw42faqc575k0f")))

(define-public crate-moon-phases-0.2.0 (c (n "moon-phases") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "1v4rsgs336v0z6ijx4hcnwimvfgh70s5vkyxs8kway4vfb3gpsx6")))

(define-public crate-moon-phases-0.3.0 (c (n "moon-phases") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "100jawc9dg4navicn3kqqpra2r2s6727bkkkypisfa6ywmbv5kj3")))

(define-public crate-moon-phases-0.3.1 (c (n "moon-phases") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "14dz6xj6d2c1fxrpjhw7z8sw5mbgs9gcsg3wl275pirlhhirnf2h")))

(define-public crate-moon-phases-0.3.2 (c (n "moon-phases") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "1b82a9hyjyf5ca230zhz25ni5m9wyi6a479wa0ccpayhlfvgl35z")))

(define-public crate-moon-phases-0.3.3 (c (n "moon-phases") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "human-date-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "moon-phase") (r "^0.1.0") (d #t) (k 0)))) (h "1kyf1xzpxnp82kjakv9aaiazggn17m0qqfa8l6hmnqk7ci6amgi4")))

