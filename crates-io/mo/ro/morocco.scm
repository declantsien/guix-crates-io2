(define-module (crates-io mo ro morocco) #:use-module (crates-io))

(define-public crate-morocco-0.1.0 (c (n "morocco") (v "0.1.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.25") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.25") (d #t) (k 0)) (d (n "rusoto_kms") (r "^0.25") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "15sq3w6gp6997w084fl3hm9wm7gfp9pp0g6b9il0lw7n74qdf9li")))

