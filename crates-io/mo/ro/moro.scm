(define-module (crates-io mo ro moro) #:use-module (crates-io))

(define-public crate-moro-0.1.0 (c (n "moro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1l10xymz9yg1nrwksy7gy9n362nn7pb0icm5lx5vg2ga5lqbm7y4")))

(define-public crate-moro-0.1.1 (c (n "moro") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1hqc5f1885g5h98xsszbya0fy1f2k62grfpb560jxbw9mm570hm1")))

(define-public crate-moro-0.2.0 (c (n "moro") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0xdf1rry0x2czbrnwk8klaqs77lq57ypx66sz6g1gz0h3qbck5nv")))

(define-public crate-moro-0.3.0 (c (n "moro") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0bxs2hmfgsa07kpxg5bi775hqsmv6j94xa368wmybl8hcmjmh1wn")))

(define-public crate-moro-0.4.0 (c (n "moro") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1xid2cf0n2hk0xni6mi0pqkngqqhd0hm2g6vpwlpb7iip1sccwl4")))

