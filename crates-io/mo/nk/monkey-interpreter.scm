(define-module (crates-io mo nk monkey-interpreter) #:use-module (crates-io))

(define-public crate-monkey-interpreter-0.3.0 (c (n "monkey-interpreter") (v "0.3.0") (d (list (d (n "monkey-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1z5c1iwr91sm3whl32lwg997awgh8lm1gpammmwyinbd63vmb4fw")))

(define-public crate-monkey-interpreter-0.0.4 (c (n "monkey-interpreter") (v "0.0.4") (d (list (d (n "monkey-parser") (r "=0.0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "14vf865z0fjd84gcmfb7bmh2b25m4fsqfbngfj2dy6hcwil59qww") (y #t)))

(define-public crate-monkey-interpreter-0.4.0 (c (n "monkey-interpreter") (v "0.4.0") (d (list (d (n "monkey-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0kj766dmmj1cgnbnsg9kja8zc3zmrp0i7d4v77fkcv7xgd7kfmb4")))

(define-public crate-monkey-interpreter-0.4.1 (c (n "monkey-interpreter") (v "0.4.1") (d (list (d (n "monkey-parser") (r "^0.4.1") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0pqvka8j0a3whm4i9qn5jbxsmwinij0z99cglhspshlv2rcy7yxh")))

(define-public crate-monkey-interpreter-0.5.0 (c (n "monkey-interpreter") (v "0.5.0") (d (list (d (n "monkey-parser") (r "=0.5.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1mzgg73j77j4yb99mk2vi4v6q2l29ajz0rwgsaly2i8cnmmbmf06")))

(define-public crate-monkey-interpreter-0.6.0 (c (n "monkey-interpreter") (v "0.6.0") (d (list (d (n "monkey-parser") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1gzzx7qajf96mh3g7jawfl82if5fhdah6nk7cfhs2ddbpfdmgi74")))

(define-public crate-monkey-interpreter-0.7.0 (c (n "monkey-interpreter") (v "0.7.0") (d (list (d (n "monkey-object") (r "^0.7.0") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "04d1qvzykvlkfalmiir362qrhw8sry79br82h7cszcy1amnsl9hq")))

(define-public crate-monkey-interpreter-0.8.0 (c (n "monkey-interpreter") (v "0.8.0") (d (list (d (n "monkey-object") (r "^0.8.0") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.8.0") (d #t) (k 0)))) (h "07w2768fms76zmvhjlw1i964kz0r5shd2qinkqh06629kjp8b72p")))

(define-public crate-monkey-interpreter-0.9.1 (c (n "monkey-interpreter") (v "0.9.1") (d (list (d (n "monkey-object") (r "^0.9.1") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.9.1") (d #t) (k 0)))) (h "1hqpd3h3vlzc926ymnjmhkd9ddbvhj6ikz7w7w6dk8krfa2z5a6y")))

