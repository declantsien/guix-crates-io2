(define-module (crates-io mo nk monkey-printer) #:use-module (crates-io))

(define-public crate-monkey-printer-0.1.0 (c (n "monkey-printer") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lgqjpiir96y6x3vhdw0rv05w1w41dfikcqac4x514rb0ziinihg")))

(define-public crate-monkey-printer-0.1.1 (c (n "monkey-printer") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17dc26j2gzfnqkdvp999fya40qzg8majcarqmkpkw9y6avw0cg4z")))

(define-public crate-monkey-printer-0.1.2 (c (n "monkey-printer") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5rl7n2y956jsc68i53lf5qcib0ah8q0zcbd2h9q46lmrmss68h")))

(define-public crate-monkey-printer-0.1.3 (c (n "monkey-printer") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s05cxk65rpcmx2snmqn28ib8rk7xp3q29hz2x7ch4szwh4pk2zh")))

(define-public crate-monkey-printer-0.1.4 (c (n "monkey-printer") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01z680lxay0j2xfg27ha8x95fiy85fbsrf5fld4p10z26di61b5w")))

