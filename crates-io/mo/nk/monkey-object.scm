(define-module (crates-io mo nk monkey-object) #:use-module (crates-io))

(define-public crate-monkey-object-0.6.0 (c (n "monkey-object") (v "0.6.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "monkey-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1mj0czlf0rxg8xgbvi8cb9357pjrk3ydh2n4g2ysa0gsk1akhjgd")))

(define-public crate-monkey-object-0.7.0 (c (n "monkey-object") (v "0.7.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "monkey-parser") (r "^0.7.0") (d #t) (k 0)))) (h "0p4vj1cgmksq035f4f8q29sp3s7bagfgr365dc39hq67yagpwbas")))

(define-public crate-monkey-object-0.8.0 (c (n "monkey-object") (v "0.8.0") (d (list (d (n "insta") (r "^1.7.2") (d #t) (k 2)) (d (n "monkey-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)))) (h "1j6ch12c90dcrnaklaypfirz49xcc4hw9kmb0xwxiyd1yrrxd4ms")))

(define-public crate-monkey-object-0.9.1 (c (n "monkey-object") (v "0.9.1") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.9.1") (d #t) (k 0)))) (h "0m7706mbpknyb52fx5j8k85z0wg4jh3igkzpypq2v019wcqlhjgc")))

