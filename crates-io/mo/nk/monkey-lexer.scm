(define-module (crates-io mo nk monkey-lexer) #:use-module (crates-io))

(define-public crate-monkey-lexer-0.1.0 (c (n "monkey-lexer") (v "0.1.0") (d (list (d (n "cargo-insta") (r "^1.3.0") (d #t) (k 2)) (d (n "insta") (r "^1.3.0") (d #t) (k 2)))) (h "0ql92snalssffsq2r0vg6z882g53h0x5j3airkadmcm1z3arpl59")))

(define-public crate-monkey-lexer-0.2.0 (c (n "monkey-lexer") (v "0.2.0") (d (list (d (n "cargo-insta") (r "^1.3.0") (d #t) (k 2)) (d (n "insta") (r "^1.3.0") (d #t) (k 2)))) (h "0sldk4m6lspahwqpqxdf9c5brhrpn9znq3i8q34fchg31ikzhbv9")))

(define-public crate-monkey-lexer-0.3.0 (c (n "monkey-lexer") (v "0.3.0") (d (list (d (n "cargo-insta") (r "^1.3.0") (d #t) (k 2)) (d (n "insta") (r "^1.3.0") (d #t) (k 2)))) (h "0m7g102akjrz8nrrfscjzk9fd32wr89lkjvxj4im8pl6x8l2q56w")))

(define-public crate-monkey-lexer-0.0.4 (c (n "monkey-lexer") (v "0.0.4") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zm7qy3wc1rlw0gp7b6771cnpidhargdqk514fc1papb07vp8g5q") (y #t)))

(define-public crate-monkey-lexer-0.4.0 (c (n "monkey-lexer") (v "0.4.0") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06jcmqn234412p7j7h0r7ph1zbj292wrly9i5zgv236agw6fgnpv")))

(define-public crate-monkey-lexer-0.4.1 (c (n "monkey-lexer") (v "0.4.1") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v66mjk6v879zvpza85jibk3aywa56pawm2q5y7sbhmsmqpmmv1f")))

(define-public crate-monkey-lexer-0.5.0 (c (n "monkey-lexer") (v "0.5.0") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07hz1vn59wiabqi8k89b3pz1h105jbilf5iy6azms8vm6yx1n6m4")))

(define-public crate-monkey-lexer-0.6.0 (c (n "monkey-lexer") (v "0.6.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xnyan8grcpj0f141cqq3hr97qgvxi9jll6j6j0y52gvxhwyy179")))

(define-public crate-monkey-lexer-0.7.0 (c (n "monkey-lexer") (v "0.7.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "016d1a7hixi08w6qpfgacjvzz14n2kwqqpnc2wbmlsrzy1y9q581")))

(define-public crate-monkey-lexer-0.8.0 (c (n "monkey-lexer") (v "0.8.0") (d (list (d (n "insta") (r "^1.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cqvhx73122y8d1agzsnjggmr0ma1q3yl8j6cr1x2nyx732k6145")))

(define-public crate-monkey-lexer-0.9.1 (c (n "monkey-lexer") (v "0.9.1") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02jgprkicb1m6xs9wbfr7qpf864gnxbr4y469w98c6r4r64ak2l2")))

