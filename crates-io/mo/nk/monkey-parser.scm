(define-module (crates-io mo nk monkey-parser) #:use-module (crates-io))

(define-public crate-monkey-parser-0.1.0 (c (n "monkey-parser") (v "0.1.0") (d (list (d (n "monkey-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "0jm05rjwn66xzkbfmmb0hhb65j55lmvzrm12g6w2bv44036ak67m")))

(define-public crate-monkey-parser-0.2.0 (c (n "monkey-parser") (v "0.2.0") (d (list (d (n "monkey-lexer") (r "^0.2.0") (d #t) (k 0)))) (h "1blw0q2b834izf7sjpjphli8iplxnsx51f7vggg42rsk5hi6ipvj")))

(define-public crate-monkey-parser-0.3.0 (c (n "monkey-parser") (v "0.3.0") (d (list (d (n "monkey-lexer") (r "^0.3.0") (d #t) (k 0)))) (h "14rfq3gwz7z16mnsyb1d2nlww3bfy66c3qa2zpjidiymr00zwbvd")))

(define-public crate-monkey-parser-0.0.4 (c (n "monkey-parser") (v "0.0.4") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "monkey-lexer") (r "=0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nhzvbm9gi676ccvc6w948p4rmnigq8m7dxj2zs1k5cb5v2xnrr8") (y #t)))

(define-public crate-monkey-parser-0.4.0 (c (n "monkey-parser") (v "0.4.0") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02gfmy30qgy0jsamz81cdhb5y839qx6mal58glfjrqghfslflmq2")))

(define-public crate-monkey-parser-0.4.1 (c (n "monkey-parser") (v "0.4.1") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07r432rmv94nbkrivkfbgxqwh492032d7341gwgw5dvz6xjcmlnx")))

(define-public crate-monkey-parser-0.5.0 (c (n "monkey-parser") (v "0.5.0") (d (list (d (n "insta") (r "^1.3.0") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mh1mpi3lyxsfmdhjdkj7wll1wdvxdr7nmq0righfy5xi6p9650r")))

(define-public crate-monkey-parser-0.6.0 (c (n "monkey-parser") (v "0.6.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c46mllryd28zmsih06vql2j3nvf8i7zashwqd8gjvy4wkm261zd")))

(define-public crate-monkey-parser-0.7.0 (c (n "monkey-parser") (v "0.7.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1007q0lyz71mkk2vwgwpc1q9j2sddpf111sy4yz6b7bb456v1qyd")))

(define-public crate-monkey-parser-0.8.0 (c (n "monkey-parser") (v "0.8.0") (d (list (d (n "insta") (r "^1.7.2") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02imr81s46prc6r36v5zhmhvydx39yybd837l6pm45n5ivx6hs8g")))

(define-public crate-monkey-parser-0.9.1 (c (n "monkey-parser") (v "0.9.1") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "monkey-lexer") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zql0ybrwkxhw1qkgv34qqfb9npx9g4jx0hkvv1zz7vf4pzkzsjh")))

