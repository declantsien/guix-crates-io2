(define-module (crates-io mo nk monkey-evaluator) #:use-module (crates-io))

(define-public crate-monkey-evaluator-0.1.0 (c (n "monkey-evaluator") (v "0.1.0") (d (list (d (n "monkey-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0s85hw385yllp16jx8dvm293yjzmn1li4kdd1sm8kvbpq5frbxbc")))

(define-public crate-monkey-evaluator-0.2.0 (c (n "monkey-evaluator") (v "0.2.0") (d (list (d (n "monkey-parser") (r "^0.2.0") (d #t) (k 0)))) (h "107w8w4bpl2w4dhdzh6aygwphaz0k6my2r2ws0mkz6ikdn57khwz")))

(define-public crate-monkey-evaluator-0.3.0 (c (n "monkey-evaluator") (v "0.3.0") (d (list (d (n "monkey-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "01yf9nqr3d5mkldnakfjgvqwbn4gazkzz9051xzkpby2d5xp9sr5")))

