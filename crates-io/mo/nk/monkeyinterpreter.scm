(define-module (crates-io mo nk monkeyinterpreter) #:use-module (crates-io))

(define-public crate-monkeyinterpreter-0.1.0 (c (n "monkeyinterpreter") (v "0.1.0") (h "0y7lmljmn0rj75hymq8a6pca7ld2fj4gic12bhm7c1fn54w3n8k0")))

(define-public crate-monkeyinterpreter-0.2.0 (c (n "monkeyinterpreter") (v "0.2.0") (h "0kmjw7gnk7l9dcccfj7x7my102hf1zby91a50bbbfq39p0wyara2")))

