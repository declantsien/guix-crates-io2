(define-module (crates-io mo nk monkey-compiler) #:use-module (crates-io))

(define-public crate-monkey-compiler-0.8.0 (c (n "monkey-compiler") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "monkey-object") (r "^0.8.0") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0l5wsl3w7dbhjhy3j5yymv22p420isinbi131psyjni9dplbcb7y")))

(define-public crate-monkey-compiler-0.9.1 (c (n "monkey-compiler") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "monkey-object") (r "^0.9.1") (d #t) (k 0)) (d (n "monkey-parser") (r "^0.9.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "178h937hby9l5i05db66nddja6rbyzd2ivay1rri8ybr5hfiz26r")))

