(define-module (crates-io mo nk monkey_test) #:use-module (crates-io))

(define-public crate-monkey_test-0.1.0 (c (n "monkey_test") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "1qr1s32q9gv5pmrxq0nfcc2fbz77qyikqvzlb6ndr95z5is8qn9y")))

(define-public crate-monkey_test-0.1.1 (c (n "monkey_test") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "1lla059f63p5ri7xl7s989x599pxc9h47n4wkk65pb8vx3hpvzd9")))

(define-public crate-monkey_test-0.2.0 (c (n "monkey_test") (v "0.2.0") (d (list (d (n "min_max_traits") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "0xjiwdm7cwsxx5d7j6cjsbln96qii04whczaxqgdw6cqvw6za9s1")))

(define-public crate-monkey_test-0.3.0 (c (n "monkey_test") (v "0.3.0") (d (list (d (n "min_max_traits") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "0bhmikzyz5dbfd6x0dlp2zy0c8nkk3465qpa840hp46m3gmypj80")))

(define-public crate-monkey_test-0.4.0 (c (n "monkey_test") (v "0.4.0") (d (list (d (n "min_max_traits") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "0d6wv9ld5klfvlndm5ilcaj9vjxn2hvnxap8qblhvcwki0gsspsz")))

(define-public crate-monkey_test-0.5.0 (c (n "monkey_test") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "min_max_traits") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "1dirvyfwask0j3acmklz5l5klb0wwsb13zz6d2w62wx1wph1skl4")))

(define-public crate-monkey_test-0.6.0 (c (n "monkey_test") (v "0.6.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "1yrf2g5byqplvnsbgnnpnf0mwzg5rpgsg3pnigw86pgbvqa4frk9")))

(define-public crate-monkey_test-0.7.0 (c (n "monkey_test") (v "0.7.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "0gbdyx53j665ph5wnds6p2pm0cwgv3lvv6sb6rq68qxckrdp9qid")))

(define-public crate-monkey_test-0.7.1 (c (n "monkey_test") (v "0.7.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "121b1ybblcbqdflw5d1qcqbf6l76y5nmr28dfv1hfcv0l0hp17zv")))

(define-public crate-monkey_test-0.7.2 (c (n "monkey_test") (v "0.7.2") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "panic-message") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)))) (h "1b0ak8gaxl7m2hs9m89zd7zz9g08qgxzhfxd1m4gcgfvlsv0lahl")))

