(define-module (crates-io mo od moodle_client) #:use-module (crates-io))

(define-public crate-moodle_client-0.1.0 (c (n "moodle_client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("tokio-rustls" "rustls-tls" "json" "cookies"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "vergen") (r "^8.2.5") (f (quote ("build" "cargo" "git" "gitcl" "rustc" "si"))) (d #t) (k 1)))) (h "1rr8n8f6scc9qjx1y4y9gcifjl4732j167fay7rnh1lb18arfhg3")))

