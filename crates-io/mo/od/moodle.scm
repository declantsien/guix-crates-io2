(define-module (crates-io mo od moodle) #:use-module (crates-io))

(define-public crate-moodle-0.0.0 (c (n "moodle") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("tokio-rustls" "rustls-tls" "json" "cookies"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "vergen") (r "^8.2.5") (f (quote ("build" "cargo" "git" "gitcl" "rustc" "si"))) (d #t) (k 1)))) (h "122s89d6m5kkn3v0x8qmxqzaidabnc8spzb35skhkdamhhkzcggy")))

(define-public crate-moodle-0.1.0 (c (n "moodle") (v "0.1.0") (d (list (d (n "moodle_api") (r "^0.1.0") (d #t) (k 0)) (d (n "moodle_client") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "vergen") (r "^8.2.5") (f (quote ("build" "cargo" "git" "gitcl" "rustc" "si"))) (d #t) (k 1)))) (h "02sv3dxwy6d7hdxvimq6mr0a1glj4zcsv5fxhc2s4arqms6w1q0s")))

