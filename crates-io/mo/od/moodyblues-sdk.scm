(define-module (crates-io mo od moodyblues-sdk) #:use-module (crates-io))

(define-public crate-moodyblues-sdk-0.1.0 (c (n "moodyblues-sdk") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16afgvlxj97iaxppcbqrf7rnpah49isakjiplavns4fvrynx466l")))

(define-public crate-moodyblues-sdk-0.1.1 (c (n "moodyblues-sdk") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02ws9krkixgzh05wr7pncap8hifbic8qnzicqkpp81nwvwwqyjpr")))

(define-public crate-moodyblues-sdk-0.2.0 (c (n "moodyblues-sdk") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13w00ryqx6qd8jgj7s613fak5vwd0q806m00khfq3iwkhjzb664l")))

(define-public crate-moodyblues-sdk-0.3.0 (c (n "moodyblues-sdk") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lk46aan00pp4a5m9fbsfv8wabjqy5m3qggjbhny385cj9jn7y3a")))

