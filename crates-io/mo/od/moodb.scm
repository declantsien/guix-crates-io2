(define-module (crates-io mo od moodb) #:use-module (crates-io))

(define-public crate-moodb-0.1.0 (c (n "moodb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1arir48r91n86aqjffv94a2fik5qmp6x722yynl82dzvfp62y79k") (y #t)))

(define-public crate-moodb-0.1.1 (c (n "moodb") (v "0.1.1") (h "0msw6sjiahk777pizfc2d6zyaqbq0qn7nihf1jm707717xhc7j4r")))

