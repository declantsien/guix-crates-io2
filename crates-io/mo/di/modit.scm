(define-module (crates-io mo di modit) #:use-module (crates-io))

(define-public crate-modit-0.1.0 (c (n "modit") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 2)))) (h "1cffk59f7b7vv0vfdx45l53ckvrsi6qik2s2h8cax0rjwwsa5j9k")))

(define-public crate-modit-0.1.1 (c (n "modit") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 2)))) (h "0sagh8yif4a89frghls6q8bd7jc2dkmk66lq6mknkxbv79mwhami")))

(define-public crate-modit-0.1.2 (c (n "modit") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 2)))) (h "1ln97p28mn5q2jms3lkvi20zz45nkjh2n2qdmq0srks1j7lsg39k")))

(define-public crate-modit-0.1.3 (c (n "modit") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 2)))) (h "037g8fg7hy4m0cycrq7d43zxv6a41afhfyp0wcx4qn40amaldqaj")))

(define-public crate-modit-0.1.4 (c (n "modit") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 2)))) (h "0g612spp8s7m3lrkbiixpvh5ryxl8bsjwi8ma3l0i0m8325868gs")))

