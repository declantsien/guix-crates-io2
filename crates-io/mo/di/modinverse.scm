(define-module (crates-io mo di modinverse) #:use-module (crates-io))

(define-public crate-modinverse-0.1.0 (c (n "modinverse") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.32") (d #t) (k 0)))) (h "1y1qpnans6ia8qd0dc2qwdpzrx10vdx31s1629kqlf4vb0jkf06f")))

(define-public crate-modinverse-0.1.1 (c (n "modinverse") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0cv6f07is62pgvksbsyc6zasfrllwli5l1p7csa1mk28y5vzaqig")))

