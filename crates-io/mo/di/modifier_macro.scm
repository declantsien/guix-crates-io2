(define-module (crates-io mo di modifier_macro) #:use-module (crates-io))

(define-public crate-modifier_macro-0.1.0 (c (n "modifier_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ix91b6frrw8lqv757q4al182im2zlayhsbf6ir4s0nqiv5cfpir")))

(define-public crate-modifier_macro-0.1.1 (c (n "modifier_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03k0ql9dd0abxbhycy161wlznpj6vp1q1f0ddv8gszjzy8yfbnsc")))

(define-public crate-modifier_macro-0.1.2 (c (n "modifier_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bx4clkx1ll0xjx8rba7nn0xvibgpn4am998bv3iq8qqm8q2ymxn")))

