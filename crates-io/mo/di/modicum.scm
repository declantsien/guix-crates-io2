(define-module (crates-io mo di modicum) #:use-module (crates-io))

(define-public crate-modicum-0.1.0 (c (n "modicum") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1aki3glhd7gc2aa1cl65vn99mlsxpqbkspxrb6inkkwr2b2skmx5")))

