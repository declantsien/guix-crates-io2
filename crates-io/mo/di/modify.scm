(define-module (crates-io mo di modify) #:use-module (crates-io))

(define-public crate-modify-0.1.0 (c (n "modify") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (d #t) (k 0)))) (h "0b9sa5pf0yzmvai9g562w57k5m2g8lhq3jnk82vh7qr669zaicd5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-modify-0.1.1 (c (n "modify") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (d #t) (k 0)))) (h "04snbzm3kqrhac9d4a28irn1qbg9d74xwkfbn7k5y7ph1hl1s3d3") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-modify-0.1.2 (c (n "modify") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1aj6s0qjl46qfx9d0giaq6zrby61kycfkihzhhixlvh5jmj0m7j3") (f (quote (("default" "serde" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json") ("serde" "dep:serde"))))))

(define-public crate-modify-0.1.3 (c (n "modify") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1b3n8sgdhjcz20ibppyha2aqbc897zza99amwjhwd4ij8c3ig8kx") (f (quote (("default" "serde" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json") ("serde" "dep:serde"))))))

