(define-module (crates-io mo di modio-mqttd) #:use-module (crates-io))

(define-public crate-modio-mqttd-0.1.1 (c (n "modio-mqttd") (v "0.1.1") (d (list (d (n "argh") (r "~0.1.5") (d #t) (k 0)) (d (n "confy") (r "~0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "~0.9") (f (quote ("atty"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rumqttd") (r "~0.9.0") (k 0)))) (h "17f0ygyf0an2fx4gfd9zb97i3i234rnw17x5mvjzkn0jhf7q51lg")))

