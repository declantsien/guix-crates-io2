(define-module (crates-io mo di modifier) #:use-module (crates-io))

(define-public crate-modifier-0.0.1 (c (n "modifier") (v "0.0.1") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "0af8r70vvyb88ms6mhqfjh52f7gx9ln9qww4bwcl95ib4dn76dg7")))

(define-public crate-modifier-0.0.2 (c (n "modifier") (v "0.0.2") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "18fkk3qck7yr9f8vsd4hls61flpyn4n888fagw9b8zbcx86psnx7")))

(define-public crate-modifier-0.0.3 (c (n "modifier") (v "0.0.3") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1jc09z0an66d82slcmy5nc8aiaxyb8cqbsnm00s61zf8i28prs9k")))

(define-public crate-modifier-0.0.4 (c (n "modifier") (v "0.0.4") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0mm7amghj8s4rb5d191d8gx27qjdz9ch8mnvwmmj4ndbvw56mir0")))

(define-public crate-modifier-0.0.5 (c (n "modifier") (v "0.0.5") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "05hnx87p1l0hi9mxkyi3hqxli0aqh2qr22qvh9rq370bkqlz6768")))

(define-public crate-modifier-0.0.6 (c (n "modifier") (v "0.0.6") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0ibsg0sa64sf21mjpzrrvm1kh2q63isih3jdajanl1gwhjr0d0x5")))

(define-public crate-modifier-0.0.7 (c (n "modifier") (v "0.0.7") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0992wgpr9zswijy2zn8y7b0rxjg0h67189krawvk4sxsgl01xbgv")))

(define-public crate-modifier-0.0.8 (c (n "modifier") (v "0.0.8") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0s1b9l255n82vsjc6j36m7waclrdyqrpdmwj6irpy28ca008yxmr")))

(define-public crate-modifier-0.0.9 (c (n "modifier") (v "0.0.9") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0mazfjp000wqj99ph4xk6qzqypc7klkhc4w8ycccz4f4dr8ldyzw")))

(define-public crate-modifier-0.1.0 (c (n "modifier") (v "0.1.0") (h "0n3fmgli1nsskl0whrfzm1gk0rmwwl6pw1q4nb9sqqmn5h8wkxa1")))

