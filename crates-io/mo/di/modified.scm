(define-module (crates-io mo di modified) #:use-module (crates-io))

(define-public crate-modified-0.1.0 (c (n "modified") (v "0.1.0") (h "1fbs2qrlclmzn6i6pnw7aqg5pjkminf1qnxch3bsa1xn6wfb2v8r")))

(define-public crate-modified-0.1.1 (c (n "modified") (v "0.1.1") (h "0skc6qa034sd4y6drzi626fip1q9cdkm6i44b33mdd8dsr9paxbz")))

(define-public crate-modified-0.1.2 (c (n "modified") (v "0.1.2") (h "1xh626wcl8hddmhg95v02y41hrkkqc4s86x8kf22sp37zcywb2pb")))

