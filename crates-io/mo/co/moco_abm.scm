(define-module (crates-io mo co moco_abm) #:use-module (crates-io))

(define-public crate-moco_abm-0.1.0 (c (n "moco_abm") (v "0.1.0") (h "01y5m7v0ap0kj5kpa4gijvabfw3wyfm349q28hbzl5csrpgm2z2z")))

(define-public crate-moco_abm-0.1.1 (c (n "moco_abm") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "069rqdyyh227lv0y79mxf59nxykkqq4yg4l6qcnjmx110dy637n8")))

(define-public crate-moco_abm-0.1.2 (c (n "moco_abm") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1akm2nm01cvg2dfv7kzi801k99958g96g0hchxgawaq109k1w9nb")))

(define-public crate-moco_abm-0.2.0 (c (n "moco_abm") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15rx6w2kdh0d8nw7pp6hyzynpgdh6apmjzjclxmdvjgjmn0dawpw")))

