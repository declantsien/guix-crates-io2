(define-module (crates-io mo co mocopi_parser) #:use-module (crates-io))

(define-public crate-mocopi_parser-0.1.0 (c (n "mocopi_parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "06jd280cy6r10hjvcl8sgb92bqp6yl6mr9rhvms80wf1csgq5yk1")))

(define-public crate-mocopi_parser-0.2.0 (c (n "mocopi_parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0gir6zdy3rf1ddwzcgq83g64kzldwchn0c5jnzsnxw3961p0clai")))

(define-public crate-mocopi_parser-0.3.0 (c (n "mocopi_parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nkqgrigzpslrlkxdiy8hkhndv7g3rw7b3fjly8jc0m6rbavld67")))

(define-public crate-mocopi_parser-0.3.1 (c (n "mocopi_parser") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q4jy3vj3lp0k8y1jm8b28rdiszncyfrshgz76gf04rpxqr5jfhn")))

