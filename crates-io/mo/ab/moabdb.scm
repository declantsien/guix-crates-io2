(define-module (crates-io mo ab moabdb) #:use-module (crates-io))

(define-public crate-moabdb-0.1.0 (c (n "moabdb") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "polars") (r "^0.25.1") (f (quote ("parquet"))) (d #t) (k 0)) (d (n "prost") (r "^0.11.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.3") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.2") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1ml6am8zvmfal84g4a9hv16w591cn2p36wqhp2czxqnl81qq6xcx")))

(define-public crate-moabdb-0.1.1 (c (n "moabdb") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "polars") (r "^0.25.1") (f (quote ("parquet"))) (d #t) (k 0)) (d (n "prost") (r "^0.11.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.3") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.2") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "0rm0y78plk05yskwp674zckcqlrc82lxa3dk1ggi9vi1pgpwfx3g")))

