(define-module (crates-io mo tt mottle) #:use-module (crates-io))

(define-public crate-mottle-0.1.0 (c (n "mottle") (v "0.1.0") (d (list (d (n "json") (r "^1.0.61") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "1fpvf0fn8pws310ni08fq322ibws8b23ng5fffz9249zgimnvd2d")))

(define-public crate-mottle-0.1.1 (c (n "mottle") (v "0.1.1") (d (list (d (n "json") (r "^1.0.61") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "0znq3cri8nq0113l7hgfaca0znlji73xn31ai2ksc5pvnzgm6bvz")))

(define-public crate-mottle-0.1.2 (c (n "mottle") (v "0.1.2") (d (list (d (n "json") (r "^1.0.61") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "0vz0gs5863wv5kmjvlf0xqgvsk9bgpgp4lcz7lxsss225d0vhbvy")))

(define-public crate-mottle-0.1.3 (c (n "mottle") (v "0.1.3") (d (list (d (n "json") (r "^1.0.61") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "19pl8nvhd927i3flxgwkkn1330jswzsfyyv0ryhjp88a65rl8v7w")))

(define-public crate-mottle-0.2.0 (c (n "mottle") (v "0.2.0") (d (list (d (n "json") (r "^1.0.61") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "10k2qdf3k8p3i7mqh1cr2ahg8wp2s0ips939vwqhw2c6sy93d0sp")))

(define-public crate-mottle-0.3.0 (c (n "mottle") (v "0.3.0") (d (list (d (n "json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0) (p "serde_json")) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tincture") (r "^0.5.0") (d #t) (k 0)))) (h "137gfvl6wxz3x7113058gxgv2kx5rwdhm1jxxfsgb17j9rkncgj2")))

(define-public crate-mottle-0.4.0 (c (n "mottle") (v "0.4.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h7nk4fwvgw2k1l37m4bhzvk7f2ypsrwnd7fxxfigyckmaqh8s46")))

(define-public crate-mottle-0.5.0 (c (n "mottle") (v "0.5.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "indexmap") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gaxwvcp23j1d97r3rms4p82lkxr8pv0i7hzi3xdbwa8j4q5wc9x")))

