(define-module (crates-io mo va movable) #:use-module (crates-io))

(define-public crate-movable-0.1.0 (c (n "movable") (v "0.1.0") (h "0p6jdpp3bwx7xmzrbhycci1kqhd1qi1rdw76461csmqw20jcf0rx")))

(define-public crate-movable-0.1.1 (c (n "movable") (v "0.1.1") (h "0b7inl75mwxb45c2anc7lw9bmffbv669ms127ak4a3r5czm9zlig")))

