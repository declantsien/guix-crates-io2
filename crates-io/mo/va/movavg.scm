(define-module (crates-io mo va movavg) #:use-module (crates-io))

(define-public crate-movavg-1.0.0 (c (n "movavg") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1z9p0ga7sd0jwfk2wjq3a34vzr0y3vxm5fmg896xl463gbk3ba46")))

(define-public crate-movavg-1.1.0 (c (n "movavg") (v "1.1.0") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1qk40aig2d1ysz0z7nnlc6k4qahgq88y25p7r3kzsk5xmjm2gci5")))

(define-public crate-movavg-2.0.0 (c (n "movavg") (v "2.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "0qpv5hg44fxkdwq0820aaf5qwygqfd6pc05iz90f6yz6bwm349qf") (f (quote (("std") ("default" "std"))))))

(define-public crate-movavg-2.1.0 (c (n "movavg") (v "2.1.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1ds6gzm4dbj53v6yikxxc6gaik4wkbv496c9nkdara75jdp69zxh") (f (quote (("std") ("fastfloat") ("default" "std"))))))

(define-public crate-movavg-2.2.0 (c (n "movavg") (v "2.2.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.0") (k 0)))) (h "1frvf6pylqq6db915icc77q9y140xlqy5v3cd66dgajfvf58whv2") (f (quote (("std") ("fastfloat") ("default" "std"))))))

(define-public crate-movavg-2.3.0 (c (n "movavg") (v "2.3.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "10k9489va441dbd3vpdhp5bhbd6h0rz7vb2w563zqa4grnpk2q8h") (f (quote (("std") ("fastfloat") ("default" "std"))))))

