(define-module (crates-io mo rg morg) #:use-module (crates-io))

(define-public crate-morg-0.1.0 (c (n "morg") (v "0.1.0") (d (list (d (n "allaudiotags") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rargsxd") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0pkihci4kf2sv42f28xh56hbvblmaklnr6jxkhx4cdy3qgzszwsw")))

(define-public crate-morg-0.1.1 (c (n "morg") (v "0.1.1") (d (list (d (n "allaudiotags") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rargsxd") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wgr1n2kdsibm80907il3jsa5w18xg8ysjz3p2m0abd9wws1q6j0")))

