(define-module (crates-io mo zj mozjs_sys) #:use-module (crates-io))

(define-public crate-mozjs_sys-0.1.0 (c (n "mozjs_sys") (v "0.1.0") (h "1mw3wn5p3snhy1lz0pfw8kzkln4a11k3v3307yl2vlsncgzlb0g0")))

(define-public crate-mozjs_sys-0.50.0 (c (n "mozjs_sys") (v "0.50.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1syh5agzd4da6y45pv4lwrfgffmcq670j6cr8pa2ql3nkzgj87pg") (f (quote (("promises") ("debugmozjs"))))))

(define-public crate-mozjs_sys-0.50.1 (c (n "mozjs_sys") (v "0.50.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0sh0vv475m09sa6qdy4qc32jvq1w6kc5w9ahxk2n84sv28m7j6p6") (f (quote (("promises") ("debugmozjs"))))))

(define-public crate-mozjs_sys-0.51.0 (c (n "mozjs_sys") (v "0.51.0") (d (list (d (n "bindgen") (r "^0.36.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1ziwlrjcbq0crjwicx0gkh0f6fxrj68f7d7rcmw8j49jlgc21yzi") (f (quote (("promises") ("debugmozjs"))))))

(define-public crate-mozjs_sys-0.51.1 (c (n "mozjs_sys") (v "0.51.1") (d (list (d (n "bindgen") (r "^0.36.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1wqi3p7jdv0svpfjqqc69fqbirz7vw8dp0cm9h4bpd6rsbgnfjx6") (f (quote (("promises") ("debugmozjs"))))))

(define-public crate-mozjs_sys-0.51.2 (c (n "mozjs_sys") (v "0.51.2") (d (list (d (n "bindgen") (r "^0.36.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0k254pvrccfwsy0hy7v3hfbb65zkz419kjy0gpi4sg66ngjry5ly") (f (quote (("promises") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.51.3 (c (n "mozjs_sys") (v "0.51.3") (d (list (d (n "bindgen") (r "^0.36.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0g3a8202d75ragq5l83z4z3zl4v2cw7fvxmfjm7r1gfkqr3ji8xl") (f (quote (("promises") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.51.4 (c (n "mozjs_sys") (v "0.51.4") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1bwyghgnw42zbq2s7mmzcggpmi6yfzfkwxzzccd8hr4fy6447zbl") (f (quote (("promises") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.60.0 (c (n "mozjs_sys") (v "0.60.0") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0x4ncq3c7anfxvpr0fdhqdyldfn90i8abadmpyfpi5cmm2aja10h") (f (quote (("debugmozjs"))))))

(define-public crate-mozjs_sys-0.60.1 (c (n "mozjs_sys") (v "0.60.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1mxfdhwd70g4i9ibf7jp1hmcxc3w78q2p50s3aav53cg0rrfjm8j") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.0 (c (n "mozjs_sys") (v "0.61.0") (d (list (d (n "bindgen") (r "^0.39") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1jbh1ws4q5ddr1pi8v7szphngghilcf3fmaxsw4dqw93187ppw0z") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.1 (c (n "mozjs_sys") (v "0.61.1") (d (list (d (n "bindgen") (r "^0.39") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0nqsny7f5szxaaqw561gclc0aqvdsn8wgz7nwi7m52xys3fdj387") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.2 (c (n "mozjs_sys") (v "0.61.2") (d (list (d (n "bindgen") (r "^0.39") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0lhy7il9zaq5vwbqysfkjz85ji77ih6g14674qx6y8krrhl6s9r8") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.3 (c (n "mozjs_sys") (v "0.61.3") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1srwdv7y6v929xlc6ijx8n6ww8ax8an94yj02wgg0xl4iak63d5a") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.4 (c (n "mozjs_sys") (v "0.61.4") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0dg8vjv1v3dpdpvbihhm4x2j46blyh5b72d4s8vvl18hxw3igvj5") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.5 (c (n "mozjs_sys") (v "0.61.5") (d (list (d (n "bindgen") (r "^0.44") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "12b6xfbxp7v4a94aigc0afbqz16w8fgg90pdqv19sifhn3pzscgw") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.6 (c (n "mozjs_sys") (v "0.61.6") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1d8wf4iiimjc04jk2d0scvnyakvg3w4mgp5z4qsp4s2d4g70y3b1") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.7 (c (n "mozjs_sys") (v "0.61.7") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0ppd1pp9mwwbvgr98q9w2vf3nsa60pmrgmzfypgf504mx5jlm6zk") (f (quote (("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.8 (c (n "mozjs_sys") (v "0.61.8") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0nxgg0n7xw4hv9l1hj4zxy864ypz4a4mfbg9mqahmjk4m36v9c5c") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.9 (c (n "mozjs_sys") (v "0.61.9") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "150d4gp96m1xsaf2d3xa2vddhyh1gfmrl12x8n4l7j6lqwp0knb4") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.10 (c (n "mozjs_sys") (v "0.61.10") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1wcsk2hwwizjd7nyry1iprhiy6310c78kjgmhpz437xwqh9svk8x") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.11 (c (n "mozjs_sys") (v "0.61.11") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "12cnr37nswpkz6abmpm7d3kg3d7mrg95c4hfnfad6sgw70vmvar2") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.12 (c (n "mozjs_sys") (v "0.61.12") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0j3z75wz5qcrgnam5c81gz3qi86574kbnh78rmanahw33h8qf21p") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.61.13 (c (n "mozjs_sys") (v "0.61.13") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1fl9s5a8yna1hc7j5bv4hajdbhz4m5n8q0jy605ygwscah15bly7") (f (quote (("profilemozjs") ("debugmozjs")))) (l "mozjs")))

(define-public crate-mozjs_sys-0.67.1 (c (n "mozjs_sys") (v "0.67.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0lal37yimxvxpb9gpn8mjfbgzcd22bh0in4rdprk3qf8ix84z815") (f (quote (("uwp") ("profilemozjs") ("debugmozjs")))) (l "mozjs")))

