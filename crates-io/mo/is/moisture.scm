(define-module (crates-io mo is moisture) #:use-module (crates-io))

(define-public crate-moisture-0.1.0 (c (n "moisture") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c6pd831kln472mg0m3jbmm31wfkmy1z6wz604picmgrjpn1476s")))

