(define-module (crates-io mo rs morsec) #:use-module (crates-io))

(define-public crate-morsec-0.1.0 (c (n "morsec") (v "0.1.0") (h "0nbpy5s0kgqmsqmchzzki7nhrqimxvbifk4cry26xg18b85jnbid")))

(define-public crate-morsec-0.1.1 (c (n "morsec") (v "0.1.1") (h "1m2zksdzsh2qbpplnv4lz3hq7w8nkb4l1rcxv6mz2lciifrfvrj5")))

(define-public crate-morsec-0.1.2 (c (n "morsec") (v "0.1.2") (h "1n496dyx1jdv4mqpn8h2xg4dkpzhix2wk26cf20rdibw534q1g7a") (f (quote (("std") ("default" "std"))))))

