(define-module (crates-io mo rs morse-nostd) #:use-module (crates-io))

(define-public crate-morse-nostd-0.1.1 (c (n "morse-nostd") (v "0.1.1") (h "194v2q1hrbjhc3cz0fr1igb3z274maaps712cy3d8vmwryjhy6h0")))

(define-public crate-morse-nostd-0.1.2 (c (n "morse-nostd") (v "0.1.2") (h "1qfll84hmkaa76mgdasclx7g43vs72g7z7yyw92x74v1nd69m54r")))

