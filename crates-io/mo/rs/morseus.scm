(define-module (crates-io mo rs morseus) #:use-module (crates-io))

(define-public crate-morseus-0.1.0 (c (n "morseus") (v "0.1.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0g2m9k9d1k7a0iz3fxjqd7w09sdw47d6849yp8qfrcv2m58migx5")))

(define-public crate-morseus-0.1.1 (c (n "morseus") (v "0.1.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1yqcln852160m5qp2zmchx3x4js4lc41wlibhnym7bw844ck8jvn")))

(define-public crate-morseus-0.1.11 (c (n "morseus") (v "0.1.11") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "13jcpsmad271g4iikzbislzrbk11crp1ii9cgvww06mxws0i0ah1")))

(define-public crate-morseus-0.1.12 (c (n "morseus") (v "0.1.12") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0h2mkx0jgcfpp1a3jfra9ifhammzpjxq63yl6khc3l753p4rb9jc")))

