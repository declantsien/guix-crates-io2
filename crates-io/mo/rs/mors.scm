(define-module (crates-io mo rs mors) #:use-module (crates-io))

(define-public crate-mors-0.1.0 (c (n "mors") (v "0.1.0") (h "0gl71jars1pqx59gz1qn5cbaw6czvl0vmzclj77s5k1qfnqa0sg0")))

(define-public crate-mors-0.1.1 (c (n "mors") (v "0.1.1") (h "1iim9p8jzrdqwslqfmmdad3lk41dmc5j3n3i0p0pah22fhy6a138")))

