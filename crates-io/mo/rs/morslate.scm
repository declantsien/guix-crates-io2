(define-module (crates-io mo rs morslate) #:use-module (crates-io))

(define-public crate-morslate-1.0.0 (c (n "morslate") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (d #t) (k 0)) (d (n "morse-nostd") (r "^0.1.2") (d #t) (k 0)))) (h "194apwyik64n4609frb5ydksl6wl26dyvgx79aqg7xmxppq78w8m")))

