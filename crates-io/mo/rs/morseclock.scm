(define-module (crates-io mo rs morseclock) #:use-module (crates-io))

(define-public crate-morseclock-0.1.0 (c (n "morseclock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1grps9g10xnhm55p71ff37bzhsjhvikmfgyb8dp34pfq8mkwldhk")))

(define-public crate-morseclock-0.2.0 (c (n "morseclock") (v "0.2.0") (h "1351gzz2yj4jh9dqkrznwvklwly6xg9msnv2ja82bdqsvxn7jk85")))

