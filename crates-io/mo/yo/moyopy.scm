(define-module (crates-io mo yo moyopy) #:use-module (crates-io))

(define-public crate-moyopy-0.1.6 (c (n "moyopy") (v "0.1.6") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "moyo") (r "^0.1.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jp1xq8hnp6vv8cpxdq8q0r97z4l18ccbfyh1ryzwp7p00qzbl9c")))

(define-public crate-moyopy-0.1.7 (c (n "moyopy") (v "0.1.7") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "moyo") (r "^0.1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1202inrcsw8dwl4dz57hs615xm1d82prjhsm0x2wijndjpiqrxhv")))

