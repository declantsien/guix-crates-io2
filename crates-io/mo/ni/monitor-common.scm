(define-module (crates-io mo ni monitor-common) #:use-module (crates-io))

(define-public crate-monitor-common-0.1.0 (c (n "monitor-common") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0bmy7j8n6j48qr5xpgviw4ic80sx6d7x67nkk9m4k82kv9asgfzd")))

(define-public crate-monitor-common-0.2.0 (c (n "monitor-common") (v "0.2.0") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1wysn5bn15j6dx6zyk0awybzx6pvjfzi7bwffzgv1lz5dyhbm4gz")))

(define-public crate-monitor-common-0.2.1 (c (n "monitor-common") (v "0.2.1") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "18afvs0sa68j4fpyb0rqxw6mmaz2zc9avccxl72nzkk2sx1x77y4")))

(define-public crate-monitor-common-0.2.2 (c (n "monitor-common") (v "0.2.2") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1z70i1n5ddba68vkyaybfqjxskcwhdzm57x72dab5n58v08lzp50")))

(define-public crate-monitor-common-0.2.3 (c (n "monitor-common") (v "0.2.3") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "17sj23riy32kxci23hn4jfymhcfz7ljzlrdnggad6474garnls6a")))

(define-public crate-monitor-common-0.2.4 (c (n "monitor-common") (v "0.2.4") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1j7901w6sm1zkxn9ky4nmlgnxkx3k2vjxl9zkqd012qz83h5506f")))

(define-public crate-monitor-common-0.2.5 (c (n "monitor-common") (v "0.2.5") (d (list (d (n "rust_decimal") (r "^1.25.0") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "03bj8ad0j8vy6yaxyzb3kdii47f91cshrj5j5wn91vxasayd5dm7")))

(define-public crate-monitor-common-0.2.6 (c (n "monitor-common") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0dfg27hzz2qj52625mi8y00dhy53w072gxwa21hgpccjszadj3yd")))

(define-public crate-monitor-common-0.2.7 (c (n "monitor-common") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1l2cg4avb4740w5583vs7j4gh9yddab0r8vbcl4r26q50nixqwqx")))

(define-public crate-monitor-common-0.2.8 (c (n "monitor-common") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.40") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1b9v1nm59daq1livvb1q7qis3lczsa15k7mvc4jd9y419d11phna")))

