(define-module (crates-io mo ni moniker-derive) #:use-module (crates-io))

(define-public crate-moniker-derive-0.1.0 (c (n "moniker-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0nvsarjq7v504363r1hpm2harv70rsvgsy8drsxdbifh43f7qpgd")))

(define-public crate-moniker-derive-0.2.0 (c (n "moniker-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "1jh5m0dv33lj6qwfypv1q7xv52721ap0ymgw0nghww9x64v7z6ra")))

(define-public crate-moniker-derive-0.2.1 (c (n "moniker-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0s1czhjx1ls1kl4592y6yyqqgg4y6gl8qchvcmk76yg6p9alll0b")))

(define-public crate-moniker-derive-0.2.2 (c (n "moniker-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0pn296c3fmc0ri1kgw6m7ikz7iq102f2m55x47vwjq0j8pdr5gzd")))

(define-public crate-moniker-derive-0.2.3 (c (n "moniker-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "1vpdkymyvnq93va8avsgg0haw9k681z3fxk7wav51fz71fidwn50")))

(define-public crate-moniker-derive-0.3.0 (c (n "moniker-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0dan8h74qqyhhfasays7f8d2nfgfa5axfpx1p1382dj8wxf0dlw9")))

(define-public crate-moniker-derive-0.3.1 (c (n "moniker-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0gimhh6jqgayvqvpxn80c3dxdc50wq8bafpicz2944dh3q0n7xy5")))

(define-public crate-moniker-derive-0.3.2 (c (n "moniker-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "11aiyd2cndc5f342m9pbl926sq9kadynysm1k14w1j60ir81vj7n")))

(define-public crate-moniker-derive-0.4.0 (c (n "moniker-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "1in53ygfkc1b9hbn9f9lq95yr9y8jb0yibqcncravaj96pcqzyxy")))

(define-public crate-moniker-derive-0.5.0 (c (n "moniker-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1vb2c0ihv49n5agilxd4hagrn7bwhmxvjh128g8pgdidgdb6ih1s")))

