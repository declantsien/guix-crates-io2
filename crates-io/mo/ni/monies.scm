(define-module (crates-io mo ni monies) #:use-module (crates-io))

(define-public crate-monies-0.0.1 (c (n "monies") (v "0.0.1") (d (list (d (n "rust_decimal") (r "^1.9.0") (k 0)) (d (n "rust_decimal_macros") (r "^1.9.0") (d #t) (k 0)))) (h "00hpn6dspb2qshy3ya25ld1q3l30wwyxz3dq613l7y95bkpbs2jr") (f (quote (("iso") ("default" "iso") ("crypto"))))))

(define-public crate-monies-0.0.2 (c (n "monies") (v "0.0.2") (d (list (d (n "rust_decimal") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1b34ni0a8dvks737msnldw57cdgyraidn8q7khbj24153gcxlzan")))

(define-public crate-monies-0.0.3 (c (n "monies") (v "0.0.3") (d (list (d (n "rust_decimal") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0b5v095jbbynkfmyw2ziynss3xcywizxxvlpk6g6p1m4hsp5v1mk")))

(define-public crate-monies-0.0.4 (c (n "monies") (v "0.0.4") (d (list (d (n "rust_decimal") (r "^1.10") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "187dgydzl1b05arqpynzjz70p83r2xv83iqy4acpqmv25hlrfwa3")))

