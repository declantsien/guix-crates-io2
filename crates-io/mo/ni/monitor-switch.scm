(define-module (crates-io mo ni monitor-switch) #:use-module (crates-io))

(define-public crate-monitor-switch-0.2.0 (c (n "monitor-switch") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "ddc-hi") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "macro-attr") (r "^0.2") (d #t) (k 0)))) (h "1w0aas761i8vjmyqg7xjs1271pwsy0gckjhzwwr9plinjn9d8mda")))

