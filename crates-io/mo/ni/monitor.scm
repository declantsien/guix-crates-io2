(define-module (crates-io mo ni monitor) #:use-module (crates-io))

(define-public crate-monitor-0.0.1 (c (n "monitor") (v "0.0.1") (d (list (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "1lkpmw27f7r46fk8d06aw6wm933a28cb945bihfd9yi8b7mj24hg")))

(define-public crate-monitor-0.0.2 (c (n "monitor") (v "0.0.2") (d (list (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "0v4dzn8xs7c36rgklcfljwrkxisp1g0d4ijjwcr8yb1fp2i151b1")))

(define-public crate-monitor-0.0.3 (c (n "monitor") (v "0.0.3") (d (list (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "03m827irasmpkcvj6ghcfi6pji9i43c35dchi7cyxrd5ap0mq8lg")))

(define-public crate-monitor-0.1.0 (c (n "monitor") (v "0.1.0") (h "0cc9hq7jxrbkyn1120jzwvpywk7056zcq2v0m09zqbn9j51qnl06")))

