(define-module (crates-io mo ni moniclock) #:use-module (crates-io))

(define-public crate-moniclock-0.1.0 (c (n "moniclock") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "profileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rawqic0v1w4b8i1fnfp9c1y70gr2j0jdbfz2lrhc10wih9fzfzc")))

