(define-module (crates-io mo ni monitor_client_rs) #:use-module (crates-io))

(define-public crate-monitor_client_rs-0.0.1 (c (n "monitor_client_rs") (v "0.0.1") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0ynp22idanh1kl5r7k6qyyj5v3614cb322ivz38cxd0p58g9rg0q")))

(define-public crate-monitor_client_rs-0.0.2 (c (n "monitor_client_rs") (v "0.0.2") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0al9ikalb329pni6ial2hwih5q7xjmhw4alqix7sv4lyj8n7mjv9")))

(define-public crate-monitor_client_rs-0.0.3 (c (n "monitor_client_rs") (v "0.0.3") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0ljqp6sx3373c5irmwabajp3g1gn7iircxlydjzhc2hyn8pkjmk4")))

(define-public crate-monitor_client_rs-0.0.4 (c (n "monitor_client_rs") (v "0.0.4") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "01pb9mwmls175n23mdhcrvk7ap0paqqbdw2kz8l6yyr56d660i6d")))

(define-public crate-monitor_client_rs-0.0.5 (c (n "monitor_client_rs") (v "0.0.5") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0mw3cdrxmyqdqkb146ml7jvbyz756rqlfcdrhykv9cd2xci1bspk")))

(define-public crate-monitor_client_rs-0.0.6 (c (n "monitor_client_rs") (v "0.0.6") (d (list (d (n "mungos") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1vrdjfv30gk2z37rnhv5912f4mj89rg993674sbsi3g7875i13mn")))

