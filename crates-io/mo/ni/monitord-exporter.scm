(define-module (crates-io mo ni monitord-exporter) #:use-module (crates-io))

(define-public crate-monitord-exporter-0.1.0 (c (n "monitord-exporter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "monitord") (r "^0.3.0") (d #t) (k 0)) (d (n "prometheus_exporter") (r "^0.8.4") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "13xc9v6k1lcfr0bf5qzv9qy0msa7v1zvl80hz4f470fnwpaw15lz")))

