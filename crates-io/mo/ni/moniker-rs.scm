(define-module (crates-io mo ni moniker-rs) #:use-module (crates-io))

(define-public crate-moniker-rs-1.0.0 (c (n "moniker-rs") (v "1.0.0") (h "10b4akjhninaqnnbq95y198p51ykyrm829fwrv8hjsnaq059d9rf")))

(define-public crate-moniker-rs-1.0.1 (c (n "moniker-rs") (v "1.0.1") (h "1ybav9gfbgj4ypkn9d55yc6zrzc63sx31cdgn2k9rrwg22mk6diq") (f (quote (("moby") ("default" "moby" "animal") ("animal"))))))

