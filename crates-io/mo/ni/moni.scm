(define-module (crates-io mo ni moni) #:use-module (crates-io))

(define-public crate-moni-0.1.0 (c (n "moni") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0f3jh8sfiphqryd674390xkl7krgmyb8789cq1qdfyb5iyz9nykk")))

(define-public crate-moni-0.2.0 (c (n "moni") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1qs2lqlx3j30qqly92n22w1slwncwaw1nfpv6kn24nw3zpysxb5s")))

(define-public crate-moni-0.2.1 (c (n "moni") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0cmpf253z9fiii9knqyyp6ias0zy0yk2bxiw2rmk4i6hlwhw3r71")))

