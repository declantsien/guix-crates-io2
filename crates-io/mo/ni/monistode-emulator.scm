(define-module (crates-io mo ni monistode-emulator) #:use-module (crates-io))

(define-public crate-monistode-emulator-0.1.0 (c (n "monistode-emulator") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1xifacixyp08ak3hpqsj1433lrizkcwcwkxzwz6gldka33irr1s9")))

(define-public crate-monistode-emulator-0.1.1 (c (n "monistode-emulator") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "09djyzxy7k3lvrrcxfwxqw7jymv2pign3h7l9s2b24dkxnfdjbys")))

(define-public crate-monistode-emulator-0.1.2 (c (n "monistode-emulator") (v "0.1.2") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "05iydx1ydrhgann8zl3a32pbx0y7s1b09gzhwzldgvzsy6r43hdj")))

(define-public crate-monistode-emulator-0.1.3 (c (n "monistode-emulator") (v "0.1.3") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1bkix958k3mdm1lcn3pj3kbm5mg3ysv95j91q45w9ma5akyf7iqz")))

(define-public crate-monistode-emulator-0.1.4 (c (n "monistode-emulator") (v "0.1.4") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "01q0n64g7vpwin22rn3253wy72rdrliznmf7y6njrnlf9yp80k9k")))

(define-public crate-monistode-emulator-0.1.5 (c (n "monistode-emulator") (v "0.1.5") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "0lv68gwzzdaw2g68g741xn77m11azvi4cwyys7nn63m5r37gwg5s")))

(define-public crate-monistode-emulator-0.1.6 (c (n "monistode-emulator") (v "0.1.6") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "0lh375s1gq1vvmq11gkkdvi3frkfv3v9x76wq17skfnyrzyhamhp")))

(define-public crate-monistode-emulator-0.1.7 (c (n "monistode-emulator") (v "0.1.7") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "08lx6wnd9c1z11pnzgjyjk5cpjhykqzsqk39h0hga8sbhqx38mf3")))

(define-public crate-monistode-emulator-0.1.8 (c (n "monistode-emulator") (v "0.1.8") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1ayymhva4jcz9x2113d1k16k733x1h9vaccvxhysr10ypx5g6agv")))

(define-public crate-monistode-emulator-0.1.9 (c (n "monistode-emulator") (v "0.1.9") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "13ldnxppqzvlag8839x10vnjyn4wfnkr68kl9rwn97r2x3aljrzl")))

(define-public crate-monistode-emulator-0.1.10 (c (n "monistode-emulator") (v "0.1.10") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1jfxxp4fmka02jwf02jc952ahvc3ai5zjqam4hilrj1665rbpk25")))

