(define-module (crates-io mo ni monistode-emulator-cli) #:use-module (crates-io))

(define-public crate-monistode-emulator-cli-0.1.0 (c (n "monistode-emulator-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.0") (d #t) (k 0)))) (h "1m5cpn4q544bxg6ss65d5x2rn1pij0y672yrmj1pwd0jfnnwp5kr")))

(define-public crate-monistode-emulator-cli-0.1.1 (c (n "monistode-emulator-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.0") (d #t) (k 0)))) (h "1r1xk20glq01z06a8k0ngg9jlkc5rcjps4kkjbd894cx4944cnvd")))

(define-public crate-monistode-emulator-cli-0.1.2 (c (n "monistode-emulator-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.0") (d #t) (k 0)))) (h "080s0lc82rglgr9ny4iccj3bzj56iyyr8cxb14bjl82qnkb6g3qz")))

(define-public crate-monistode-emulator-cli-0.1.3 (c (n "monistode-emulator-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.6") (d #t) (k 0)))) (h "07nfkk77m5vqdhz47kfa0wzckzgjmjfxhy0r09b1g8mn0z762qdm")))

(define-public crate-monistode-emulator-cli-0.1.4 (c (n "monistode-emulator-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.8") (d #t) (k 0)))) (h "0bmdljhr0pwdzfa1459wh3p3zysciw4dfz2fvw7ip0dzzpp5g3z8")))

(define-public crate-monistode-emulator-cli-0.1.5 (c (n "monistode-emulator-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.9") (d #t) (k 0)))) (h "155yy39sckjy4zsha3dcfy6hpfv3nfblw9d98dbx6df8y8nf239k")))

(define-public crate-monistode-emulator-cli-0.1.6 (c (n "monistode-emulator-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "monistode-emulator") (r "^0.1.10") (d #t) (k 0)))) (h "1xmki4ab6kj6yv70zpxjgzn6cran9vdn6137kh1v3a5xgrayk6bh")))

