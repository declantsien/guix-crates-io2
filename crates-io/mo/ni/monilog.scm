(define-module (crates-io mo ni monilog) #:use-module (crates-io))

(define-public crate-monilog-0.1.0 (c (n "monilog") (v "0.1.0") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "1azmngarkw5z47n7cx09mslcdjlz3nnkszrkapipfgdm29wnzh8c")))

(define-public crate-monilog-0.1.1 (c (n "monilog") (v "0.1.1") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "17225iq3w8a129z4lpxfb49xsyp3pi63da1z7xvabp1n4h6f9wm1")))

