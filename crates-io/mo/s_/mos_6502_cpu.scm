(define-module (crates-io mo s_ mos_6502_cpu) #:use-module (crates-io))

(define-public crate-mos_6502_cpu-0.1.0 (c (n "mos_6502_cpu") (v "0.1.0") (h "1mvfjadx5841qqsb7cvczad9i3ab1gjdyjm1avll7jqwzf0q9m37") (y #t)))

(define-public crate-mos_6502_cpu-0.1.1 (c (n "mos_6502_cpu") (v "0.1.1") (h "1212fzin1d5a8vq41zj0bqqvbpqirijppjvwb1cqfz89xwmkw224")))

(define-public crate-mos_6502_cpu-0.1.2 (c (n "mos_6502_cpu") (v "0.1.2") (h "0c49cgjha897s2gfzafw6hg56zlxx4szfwib8lmz1ncmnvw8as9s")))

