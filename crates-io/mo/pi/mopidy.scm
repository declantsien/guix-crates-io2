(define-module (crates-io mo pi mopidy) #:use-module (crates-io))

(define-public crate-mopidy-0.0.0 (c (n "mopidy") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01psd0hnbzxghw8ihazzkv47x8ifmsks3aqxq6mkg40qavb2vnc9")))

