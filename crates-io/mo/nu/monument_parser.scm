(define-module (crates-io mo nu monument_parser) #:use-module (crates-io))

(define-public crate-monument_parser-1.0.0 (c (n "monument_parser") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1kig419qj5kn128zcn9wirwmrddi434i5zfzlbp8imhi7hzwr8bq")))

(define-public crate-monument_parser-1.0.1 (c (n "monument_parser") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "11wbip9xr5j3w7xczmw52drwxralfh1qz524vvhmdzd67d3d7wky")))

