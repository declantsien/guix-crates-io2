(define-module (crates-io mo rr morristown) #:use-module (crates-io))

(define-public crate-morristown-0.1.0 (c (n "morristown") (v "0.1.0") (h "175747b7nxylh74mjxg3b7dlq3aj90bml5lan7jkd0j51hbqa4w7")))

(define-public crate-morristown-0.1.1 (c (n "morristown") (v "0.1.1") (h "09cg3cvy7x4kmx1xnphvkc03b8vhhfsjs6fk36kyfvj39png13lx")))

(define-public crate-morristown-0.1.2 (c (n "morristown") (v "0.1.2") (h "13fwyr3nrf5r16r541q549pyph9dr3p7qv0jn22sw37xa3j42b20")))

(define-public crate-morristown-0.1.3 (c (n "morristown") (v "0.1.3") (h "1szxw2zsgx9d5p2wvqdq3l7jzqbhss211fc7a6sa75xn0ihnhmz0")))

(define-public crate-morristown-0.1.4 (c (n "morristown") (v "0.1.4") (h "1xi5yw5d4ksyh52j17nvnilhyyhs05s5f4i554fbjw33v4gijfzq")))

