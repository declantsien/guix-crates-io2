(define-module (crates-io mo rr morrsse) #:use-module (crates-io))

(define-public crate-morrsse-0.1.0 (c (n "morrsse") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0wsxzl4ndcz537zmiq6807x8hlq6cxldkljnn2qp12kdwcfrzy0y")))

(define-public crate-morrsse-0.1.1 (c (n "morrsse") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "1plg8h6nmpwvfdh8gbxhlmz1h1hwwic258jsjql3jylb726z9m03")))

