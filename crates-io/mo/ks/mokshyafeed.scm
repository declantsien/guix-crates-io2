(define-module (crates-io mo ks mokshyafeed) #:use-module (crates-io))

(define-public crate-mokshyafeed-0.1.0 (c (n "mokshyafeed") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "solana-client") (r "^1.7.1") (d #t) (k 2)) (d (n "solana-program") (r "^1.8.2") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.7.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mn6r4cyx2r4xg2121zyqwf6j4x7pd00lrap3ywks9gimqs9m9ks")))

