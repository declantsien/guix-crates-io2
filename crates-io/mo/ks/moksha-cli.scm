(define-module (crates-io mo ks moksha-cli) #:use-module (crates-io))

(define-public crate-moksha-cli-0.2.1 (c (n "moksha-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "moksha-core") (r "^0.2.1") (d #t) (k 0)) (d (n "moksha-wallet") (r "^0.2.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1yvd0f4nrq19f1l3blvja59xs1413krylns36h5w6df8h7fi1kc2")))

