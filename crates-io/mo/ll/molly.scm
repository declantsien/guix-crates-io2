(define-module (crates-io mo ll molly) #:use-module (crates-io))

(define-public crate-molly-0.1.0 (c (n "molly") (v "0.1.0") (h "0h6xk0ldzfd8f005f450nqrqwrvjx5pvq6i7kpxyf5hnhki2pd8k") (y #t)))

(define-public crate-molly-0.2.0-alpha (c (n "molly") (v "0.2.0-alpha") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chemfiles") (r "^0.10.41") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "xdrfile") (r "^0.3.0") (d #t) (k 2)))) (h "0w878fwp3b4pvfy1fl1x6cbqn4a8z0gxs92jj9hfkfk7s15ys652")))

(define-public crate-molly-0.2.1-alpha (c (n "molly") (v "0.2.1-alpha") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chemfiles") (r "^0.10.41") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "xdrfile") (r "^0.3.0") (d #t) (k 2)))) (h "15bm5wkqfnmsk08zmnn33yz7p1h94y0dh4y2yn8bw3vh3swbw9a8")))

(define-public crate-molly-0.2.2-alpha (c (n "molly") (v "0.2.2-alpha") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chemfiles") (r "^0.10.41") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "xdrfile") (r "^0.3.0") (d #t) (k 2)))) (h "07ygzpkwqda5jq5pyx31bc2wv700jydakyzaqjn6yvxsnl8yq9i3") (r "1.74.1")))

