(define-module (crates-io mo z_ moz_cbor) #:use-module (crates-io))

(define-public crate-moz_cbor-0.1.0 (c (n "moz_cbor") (v "0.1.0") (h "0f7jf0lnnr275f2rvvqwqc6xfiaxwrr6x67rwvcil1203rnwj3cz") (f (quote (("default"))))))

(define-public crate-moz_cbor-0.1.1 (c (n "moz_cbor") (v "0.1.1") (h "0769w8av9ak5jynxksaq54yhrcn30wbgznr2f46rkmbz11bjmj10") (f (quote (("default"))))))

(define-public crate-moz_cbor-0.1.2 (c (n "moz_cbor") (v "0.1.2") (h "1sd17yjb9p42nw4fps0cv0f45fhvg5mvjxw9gir7w6sr60ib2cr1") (f (quote (("default"))))))

