(define-module (crates-io mo os mooshroom-macros) #:use-module (crates-io))

(define-public crate-mooshroom-macros-0.1.0 (c (n "mooshroom-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1fjcn73nj344q97ks8mi8i99z6shwxyg5fmpiml6kvv0ccg8p8c7")))

