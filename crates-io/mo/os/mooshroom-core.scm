(define-module (crates-io mo os mooshroom-core) #:use-module (crates-io))

(define-public crate-mooshroom-core-0.1.0 (c (n "mooshroom-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mrhzddv2igdlndawvlg3f131fwl3hvw0p000qqvb1hqcjbkxmx8")))

