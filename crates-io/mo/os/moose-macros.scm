(define-module (crates-io mo os moose-macros) #:use-module (crates-io))

(define-public crate-moose-macros-0.2.1 (c (n "moose-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trim-margin") (r "~0.1") (d #t) (k 2)) (d (n "trybuild") (r "~1.0") (d #t) (k 2)))) (h "15ipsk7f5x9c9nvwin23bgzj3hyri6vl6ixdmydmpq5p3p6n80aa")))

