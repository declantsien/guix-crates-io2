(define-module (crates-io mo us mouse-rs) #:use-module (crates-io))

(define-public crate-mouse-rs-0.1.0 (c (n "mouse-rs") (v "0.1.0") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "003flp593jr46iw0waiiz6mjnah1jl6n9hn4n0ka97fz7kgim0kb")))

(define-public crate-mouse-rs-0.1.1 (c (n "mouse-rs") (v "0.1.1") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "00r0frzfygwwr2pyxra2bqd8igid54jlp9gsxmq8j2alxyn3l3ja")))

(define-public crate-mouse-rs-0.1.2 (c (n "mouse-rs") (v "0.1.2") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "18csk2gvcw3wf77jydwhqnci948xpnfb6g729m7ziymx8j1r60qf")))

(define-public crate-mouse-rs-0.1.3 (c (n "mouse-rs") (v "0.1.3") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "1q2fhdddwwf96gqx818f5mnbh9j8zl6djxqc99liyl5hsqidpcw3")))

(define-public crate-mouse-rs-0.1.4 (c (n "mouse-rs") (v "0.1.4") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "0jkmi3nnd95rvbcxrwnp35wkbhhqgcv179si95lnryx12m3jxg0w")))

(define-public crate-mouse-rs-0.2.0 (c (n "mouse-rs") (v "0.2.0") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "1dlm05hc4abp8fhggrfyyfli22snn6z7kxn8lrcv489hvvqxgzwc")))

(define-public crate-mouse-rs-0.2.1 (c (n "mouse-rs") (v "0.2.1") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "0l5s5qsspcff7fb523q1whivz7y1r00f7jxj6kgsw2k3898adp7d")))

(define-public crate-mouse-rs-0.2.2 (c (n "mouse-rs") (v "0.2.2") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (k 0)))) (h "0byxl57alrbfxlb7zbsragwf2nw3sbl90jp222rm19q665838f42")))

(define-public crate-mouse-rs-0.3.0 (c (n "mouse-rs") (v "0.3.0") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1jgc4b5r454mg7jkqh2w5y4dkzql1ra2g4n327k19wkr04cjyb2g")))

(define-public crate-mouse-rs-0.3.1 (c (n "mouse-rs") (v "0.3.1") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1naz906p0pcvka2rsm8xriwvbagjv8ryxn837dvv3jrfcimkfxhg")))

(define-public crate-mouse-rs-0.3.2 (c (n "mouse-rs") (v "0.3.2") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "09r53iifbangq8ygcgs6jw6q49q3qfn4l08j3nkssnb6kgwibyb4")))

(define-public crate-mouse-rs-0.4.0 (c (n "mouse-rs") (v "0.4.0") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "time") (r "^0.2.21") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1vfskk1f3im2viii79mny91a4rz7kjdbljvi2f3zq78y33ifmjnf")))

(define-public crate-mouse-rs-0.4.1 (c (n "mouse-rs") (v "0.4.1") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "time") (r "^0.2.21") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "021fp8glgvf0wc5kyf5ddc0xvmk5yasavkdx34nf8c00a6knw34b")))

(define-public crate-mouse-rs-0.4.2 (c (n "mouse-rs") (v "0.4.2") (d (list (d (n "core-graphics") (r "^0.22.1") (f (quote ("highsierra"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "time") (r "^0.2.21") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1gqjbafw1h6h3v5c1l5mjbfh9zihjfq0cma2bbwwbnfah8l9b81h")))

