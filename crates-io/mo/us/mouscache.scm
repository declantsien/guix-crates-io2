(define-module (crates-io mo us mouscache) #:use-module (crates-io))

(define-public crate-mouscache-0.2.0 (c (n "mouscache") (v "0.2.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1ijb2dknmyyil7srm9vbpm6yaw9l2cbpn77spxparrgkyql3n628")))

(define-public crate-mouscache-0.3.0 (c (n "mouscache") (v "0.3.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1m6px26y7k01a2j2jjsjy8lkc3x04can9mhgg9z0w1v5r3h153bn")))

(define-public crate-mouscache-0.3.1 (c (n "mouscache") (v "0.3.1") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "03q54yd68s60n9p46hg9h368ns9wd3xhx4mp9s0fsq8d44aqjz1z")))

(define-public crate-mouscache-0.3.2 (c (n "mouscache") (v "0.3.2") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0y4hkmh3whr6p9f7n67bcsz9axgrxwyxmyh9p7442my2h0b2h5qz")))

(define-public crate-mouscache-0.3.3 (c (n "mouscache") (v "0.3.3") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1zfp0ahdnz3pn0lmbkbdxg04dawnk9n9q4nfhxs0lz4a0nyhd09c")))

(define-public crate-mouscache-0.3.4 (c (n "mouscache") (v "0.3.4") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0wa9my73zwwcz41wr842d5hs46ka2wa48dbn15z1yy6s7fa7555w")))

(define-public crate-mouscache-0.3.5 (c (n "mouscache") (v "0.3.5") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0dz2rv6s9ikyfzy18pj146zg1rf9pw82if0binckip0l4vginhbr")))

(define-public crate-mouscache-0.3.6 (c (n "mouscache") (v "0.3.6") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "19xvmcpilhp61nq20p96fcfyki5ql8vvd5nrzjhrcq07pbx6j50h")))

(define-public crate-mouscache-0.4.0 (c (n "mouscache") (v "0.4.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0ynzypd7zg112i071g591g2f5k17bwxxnrb7smcki4jsx56rglrm") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.1 (c (n "mouscache") (v "0.4.1") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1cghxd37n7hgcqf5bzi986y3cmhgbkiryz9s068v8565hw06im4q") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.2 (c (n "mouscache") (v "0.4.2") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0qphqsqnqc3wi07ag8wj0gv4f7jm9lyc3k0pyyh1vsmp8n3nlg6c") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.3 (c (n "mouscache") (v "0.4.3") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0yrcc10zq6gvnqbvgq6kkmxpnzpxi5g1fgkwscqpqbickn1ydgqf") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.4 (c (n "mouscache") (v "0.4.4") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1rfn3rvlv58gcj5j7862f40b8xfyf8h35151jzqb80si0nn46p0i") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.5 (c (n "mouscache") (v "0.4.5") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1zrfngr46h4qshmr6pwxm794h31jl96azbx8a296giqpigk8f1jv") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.6 (c (n "mouscache") (v "0.4.6") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_redis") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "18yyqi5b3ix2ng2nq2g6np7w2fwvymbb10978wcvl4qq793nkpq4") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.7 (c (n "mouscache") (v "0.4.7") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "07dxidjjaxc8jwqb2mzns4865mz7hldng90d5wqvg6s692bxsq2p") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.8 (c (n "mouscache") (v "0.4.8") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1wmlssxl22q36xf4y87n7yxqj4r7m9d986igxvgyvpj7v1bbdvsz") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.4.9 (c (n "mouscache") (v "0.4.9") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0fp2pziipkdw7358wpzg9ahqa806v7hkgcvv3pkkzkyf8gcx50kw") (f (quote (("hashset") ("default"))))))

(define-public crate-mouscache-0.5.0 (c (n "mouscache") (v "0.5.0") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "0g35j99cflnzncdyd6b4s9rzjr4ik13n196kdcjrc8687zd6169s")))

(define-public crate-mouscache-0.5.1 (c (n "mouscache") (v "0.5.1") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1mcyj60bn0py15hhfvkvy1jkg9f6bysafywdyj4rmsy5vc4qgxpx")))

(define-public crate-mouscache-0.5.2 (c (n "mouscache") (v "0.5.2") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)))) (h "1zz1k15949986nm6hi105n6jimxd2v3m3vhd3v97azmzjv40r6a3")))

(define-public crate-mouscache-0.5.3 (c (n "mouscache") (v "0.5.3") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)))) (h "0xxyf91wvvciqb8hhiakgs94yv9wk2m03fn87jnwr5j1r678xhhd")))

(define-public crate-mouscache-0.5.4 (c (n "mouscache") (v "0.5.4") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)))) (h "0bnxzid5s8rdn3d8hz26xz532g8jppnhnq6zy9d36li8a9j9kigs")))

(define-public crate-mouscache-0.5.5 (c (n "mouscache") (v "0.5.5") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)))) (h "1jni205n0hwi016l92rz4jdaxmvjjgg3aqbgjrphlixzd4ixjqzp")))

(define-public crate-mouscache-0.5.6 (c (n "mouscache") (v "0.5.6") (d (list (d (n "dns-lookup") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)))) (h "03gw935w6fcyi8fr04pqmih5mgh44y7khwyz7hx0g9glrlan6id1")))

