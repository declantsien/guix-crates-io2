(define-module (crates-io mo us mousemode) #:use-module (crates-io))

(define-public crate-mousemode-0.1.0 (c (n "mousemode") (v "0.1.0") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "1s5m6zj902ngxc1pl9dixl9fx5f737scrwzh7jp69jyv57wwp1b8") (y #t)))

(define-public crate-mousemode-0.1.1 (c (n "mousemode") (v "0.1.1") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "04fm5dwh0ykimshfv1n343xnqfxyich80v4ypighmrkpac70zzmb") (y #t)))

(define-public crate-mousemode-0.1.2 (c (n "mousemode") (v "0.1.2") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "08igis09jn0zgi0zzkai72qqpwww8n1kvb78h7bg7hqydynd22xs") (y #t)))

(define-public crate-mousemode-0.1.3 (c (n "mousemode") (v "0.1.3") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "1zr5b4r3rsqvw1cv3za688kdak57wx8x3l7xh8p5ysh1v8l2b1cd") (y #t)))

(define-public crate-mousemode-0.1.4 (c (n "mousemode") (v "0.1.4") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "13g5lpv63x9pxp50c2i1k46l0h6ahnh7ymlbyirnkx12gs1vvmkq") (y #t)))

(define-public crate-mousemode-0.1.5 (c (n "mousemode") (v "0.1.5") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "1p68fk8wqipa375870aqbs5pxxx4k781al0cxyyl2893kx3wnxwl") (y #t)))

(define-public crate-mousemode-0.1.6 (c (n "mousemode") (v "0.1.6") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "09a61lw8v5z76gr0b7b0lpsc63affwpcbq921ajqgaqnz4rd0pri") (y #t)))

(define-public crate-mousemode-0.1.7 (c (n "mousemode") (v "0.1.7") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "1v7vv6mc92x86ksmylssa14cdjglb6ycapc60fxi7iv4wvxfyks4") (y #t)))

(define-public crate-mousemode-0.1.8 (c (n "mousemode") (v "0.1.8") (d (list (d (n "device_query") (r "^0.2.8") (d #t) (k 0)) (d (n "mouse-rs") (r "^0.4") (d #t) (k 0)))) (h "142kl58s56sv3szg465ad9k3y25gn5m0hqikiqnsyjx0b0y1asdl")))

