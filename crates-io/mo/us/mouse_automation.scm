(define-module (crates-io mo us mouse_automation) #:use-module (crates-io))

(define-public crate-mouse_automation-0.1.0 (c (n "mouse_automation") (v "0.1.0") (h "1pabgj9m8xv4xh9qjf4qlzr9f7iirk519albss2dmbcm201pyhja")))

(define-public crate-mouse_automation-0.1.1 (c (n "mouse_automation") (v "0.1.1") (h "1knf59hk275sl5f0n72mll55wysi4w27hf315jqjr8i3y827h9rr")))

(define-public crate-mouse_automation-0.1.2 (c (n "mouse_automation") (v "0.1.2") (h "14mgb89v2d7c6b57qfn3j2amcy96grk01k1fkam2kmxlln0gdnzw")))

(define-public crate-mouse_automation-0.1.3 (c (n "mouse_automation") (v "0.1.3") (h "0lr3x96a1vfdgn69rl3m868wa4q7v8ncqs53fkxrwpr6ppw1yyyd")))

