(define-module (crates-io mo us mouscache_derive) #:use-module (crates-io))

(define-public crate-mouscache_derive-0.2.0 (c (n "mouscache_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ddc8bqvplf6c5cd374ipck2dx9m6lk8ydag51ax3vbrk57rj8il")))

(define-public crate-mouscache_derive-0.3.0 (c (n "mouscache_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a0vxlsbn1wb4r0aqif53xh63az3pkgkn8vy2yp87j0a2pgcrm76")))

(define-public crate-mouscache_derive-0.3.1 (c (n "mouscache_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sml24va52d45hr25rv5pks9j3hyr8hv5s1c8gz6b1d7hgks7j1r")))

(define-public crate-mouscache_derive-0.3.2 (c (n "mouscache_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "02p1q4kp7yfbgi4zk6n1hc4v03z3adkkld52m5gbk5943c9lpp8k")))

(define-public crate-mouscache_derive-0.3.3 (c (n "mouscache_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yw8s3bkgdf07c8dw6d90jcxbxhqd8wh0nv3y4sqal7f83dpkiak")))

(define-public crate-mouscache_derive-0.4.0 (c (n "mouscache_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1livgzsfv6xbz2ivascgfrbklmrai850lww7rg2irxygva777mvp")))

