(define-module (crates-io mo us mousse) #:use-module (crates-io))

(define-public crate-mousse-0.1.0 (c (n "mousse") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex_generate") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1qbgrsvnl7k5cip4rk9dkbdwv450m47j3xjd55lzx9qcq5vdf14a")))

(define-public crate-mousse-0.1.1 (c (n "mousse") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fantoccini") (r "^0.17") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex_generate") (r "^0.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1a5w728ibnhs1q638icn7v4gb57pr5p5djif84f169b4cgbym6zj")))

