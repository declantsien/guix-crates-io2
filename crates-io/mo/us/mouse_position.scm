(define-module (crates-io mo us mouse_position) #:use-module (crates-io))

(define-public crate-mouse_position-0.1.0 (c (n "mouse_position") (v "0.1.0") (d (list (d (n "core-graphics") (r "^0.22.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0lkqxl1x50mh8xymqihj2cz406my6zryaj37jy2ddbrjd1cwhc0n")))

(define-public crate-mouse_position-0.1.1 (c (n "mouse_position") (v "0.1.1") (d (list (d (n "core-graphics") (r "^0.22.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1hwiciy86nj12adj3b5xm5kir1cxr0vpp82ga687qz56wklwrxdp")))

(define-public crate-mouse_position-0.1.3 (c (n "mouse_position") (v "0.1.3") (d (list (d (n "core-graphics") (r "^0.22.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1p9fq7dd8wivsv0f6mbjbzxa34kgpbad9v4f2lysy5yq7an75lpz")))

