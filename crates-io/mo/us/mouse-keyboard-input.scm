(define-module (crates-io mo us mouse-keyboard-input) #:use-module (crates-io))

(define-public crate-mouse-keyboard-input-0.2.0 (c (n "mouse-keyboard-input") (v "0.2.0") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "udev") (r "^0.7.0") (d #t) (k 0)))) (h "1nzc24bmgn09r34pwgqnw4qczj3zq6vfr5ghghxc8jyshvivqblw")))

(define-public crate-mouse-keyboard-input-0.2.1 (c (n "mouse-keyboard-input") (v "0.2.1") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "udev") (r "^0.7.0") (d #t) (k 0)))) (h "0dpn2a1zz879cw6vxh468vhibvc2b91wkv5yhm2cw701ay71zymy")))

(define-public crate-mouse-keyboard-input-0.2.3 (c (n "mouse-keyboard-input") (v "0.2.3") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0l1d437nrqb0lrhqd3rbmzcpk0k31v8j2wbdgkx1b4g22k7pfyiz")))

(define-public crate-mouse-keyboard-input-0.3.1 (c (n "mouse-keyboard-input") (v "0.3.1") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "010l6ngl1ssqqfkvmcqr9grh2xlm269wwfchwspbgnxnnrlfdxgb")))

(define-public crate-mouse-keyboard-input-0.3.4 (c (n "mouse-keyboard-input") (v "0.3.4") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0jn0v176nhbm8zqfakd7a2fy0bvyvcvhs27am8kj0wfs2bmmfars")))

(define-public crate-mouse-keyboard-input-0.3.5 (c (n "mouse-keyboard-input") (v "0.3.5") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "16n1b4ar5hmsjhn52c4xlr194p6mlr3kgsbigfc343p33ca4fs0c")))

(define-public crate-mouse-keyboard-input-0.3.7 (c (n "mouse-keyboard-input") (v "0.3.7") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1pj6fwj7wpp113lhcglyfdpxcvcwcmpvgdqqvkaj5xz9ld851pf5")))

(define-public crate-mouse-keyboard-input-0.4.1 (c (n "mouse-keyboard-input") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1qmsdmr51i08wc5qjqz0jvph43lj51gkb19809r72j7m3g68jzbd")))

(define-public crate-mouse-keyboard-input-0.5.1 (c (n "mouse-keyboard-input") (v "0.5.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "10w44dx2l2jl07w8r16qf8yy4px7wfcp9wp3zyxdifgda4gmr6bk") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.5.2 (c (n "mouse-keyboard-input") (v "0.5.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "19w00d9p3h012mgzdyp5djyzjkzhc74zaciw39w06gqhaxa723ww") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.6.1 (c (n "mouse-keyboard-input") (v "0.6.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "13g1sywcbxk6fgh8l02cynai85z0kjpfd7ywcmmhxqsiwxii902i") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.7.1 (c (n "mouse-keyboard-input") (v "0.7.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "1nx9hnyp43pvw5kpvc2bd05r3598r6a3k3mk81xzncnif0w8637g") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.7.2 (c (n "mouse-keyboard-input") (v "0.7.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "0x35fns3dirhb2gadvrfsl7gdd7kpm3jkbi7s169ia5jb3h1vjhb") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.7.3 (c (n "mouse-keyboard-input") (v "0.7.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "1smw68c4ywhw63hj717s645jxysaialrf30d4xfg2hi4lwc9pxyv") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

(define-public crate-mouse-keyboard-input-0.8.1 (c (n "mouse-keyboard-input") (v "0.8.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (d #t) (k 0)))) (h "0r50dn9yli3p63gws3fbcs9ll2kgq4vi1f02wa99030byiwq72yj") (f (quote (("default" "auto-acquire-permissions") ("auto-acquire-permissions"))))))

