(define-module (crates-io mo us mousepage) #:use-module (crates-io))

(define-public crate-mousepage-0.1.0 (c (n "mousepage") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "inputbot") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "touchpage") (r "^0.2.0") (d #t) (k 0)))) (h "07z835jss4p3rv615z0h5bd8nxwx4dnnm306kplbi78067kwr429")))

(define-public crate-mousepage-0.2.0 (c (n "mousepage") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "inputbot") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.41") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "touchpage") (r "^0.2.2") (d #t) (k 0)))) (h "10h9vaxfqsr0kqhb71gsrkb9hn0564fj0d12jc6xix5fhn9ww189")))

(define-public crate-mousepage-0.2.1 (c (n "mousepage") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "inputbot") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.41") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "touchpage") (r "^0.2.2") (d #t) (k 0)))) (h "0f0zp2k8mrdylxdz966ln57vimpywb99qd4fp535f37kbi7bmaly")))

(define-public crate-mousepage-0.3.0 (c (n "mousepage") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "inputbot") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.41") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-lexpr") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "touchpage") (r "^0.2.2") (d #t) (k 0)))) (h "0p3il813ayxir9srqkpx4vbyv2s2il25p5bfiqfdnq24nnb28zp5")))

