(define-module (crates-io mo us moustafinator) #:use-module (crates-io))

(define-public crate-moustafinator-0.1.0 (c (n "moustafinator") (v "0.1.0") (h "1bc40jizpf33rdvsn2ndilkkbk4cy371lyb9ndxwxvhjb4lpy3s9") (y #t)))

(define-public crate-moustafinator-0.1.1 (c (n "moustafinator") (v "0.1.1") (h "1shmzpzvhk3jjp6j6w92drm1nfbij3z9h612yj1krmm29d0ib11c") (y #t)))

