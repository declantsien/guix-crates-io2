(define-module (crates-io mo ba moba) #:use-module (crates-io))

(define-public crate-moba-0.1.0 (c (n "moba") (v "0.1.0") (h "0qjj6wjvnp87xb9kk5xp6kc7sqxj8kz5zh52xg3r116awq17dc38")))

(define-public crate-moba-0.1.1 (c (n "moba") (v "0.1.1") (h "0fmpwxkif0h0zzd1fndgx61zdqcy267p7psmi010n3s8m18klm98")))

