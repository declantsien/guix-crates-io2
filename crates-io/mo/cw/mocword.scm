(define-module (crates-io mo cw mocword) #:use-module (crates-io))

(define-public crate-mocword-0.1.0 (c (n "mocword") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1impmlhlx4l0fpj9n8fani4spni4jd1c227vmqsg6svbals1iqa2")))

(define-public crate-mocword-0.2.0 (c (n "mocword") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0fgm41n5xg10jcqm0sdd73d1q3lwcb4ijw5nvdcz3m1bx8vxbgiw")))

