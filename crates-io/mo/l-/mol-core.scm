(define-module (crates-io mo l- mol-core) #:use-module (crates-io))

(define-public crate-mol-core-0.1.0 (c (n "mol-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1glhpixdggxjvd9ikgpymn1j9mi0676wbcllg36q7pyq4anc85y9")))

(define-public crate-mol-core-0.2.0 (c (n "mol-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "18d2g7kqpx3adqvf91pv1mwqkyy6wwhcqjybdy9k5sv2qgbdpgqp")))

(define-public crate-mol-core-0.3.0 (c (n "mol-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "mol-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1dh51vq0yw3aw0cq2hzk7vxaa0dlg492crvkl1rd07pdijcrm4qy") (f (quote (("derive" "mol-derive") ("default"))))))

