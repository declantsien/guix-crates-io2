(define-module (crates-io mo rk mork-message-derive) #:use-module (crates-io))

(define-public crate-mork-message-derive-0.1.0 (c (n "mork-message-derive") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0si0dgdiwlr31bq5xspy37mlnxfdj8qcvmxch6h1sk8r0ihilnn6")))

