(define-module (crates-io mo bi mobile-entry-point) #:use-module (crates-io))

(define-public crate-mobile-entry-point-0.1.0 (c (n "mobile-entry-point") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "113mipiqc5lmmzq32ij6dd9nkhjvbacij9z5physyjinwy32j2ix")))

(define-public crate-mobile-entry-point-0.1.1 (c (n "mobile-entry-point") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "19m1nswwnhv54w1jzc4p0dvkwysd888cmk0w8y1nachq02lzbgl1")))

