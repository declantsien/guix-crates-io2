(define-module (crates-io mo bi mobi-sys) #:use-module (crates-io))

(define-public crate-mobi-sys-0.1.0 (c (n "mobi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.44.0") (d #t) (k 1)))) (h "0dpzcilb2sndp3i3bpc22hfh8y3h4fsx87j97qcv4gafkzinx4g5") (l "libmobi")))

(define-public crate-mobi-sys-0.1.1 (c (n "mobi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.44.0") (d #t) (k 1)))) (h "0a8drh6xlv9hkg3vxbp3g4ncmw8jjsxjpva3j247mfv1fls2vcna") (l "libmobi")))

(define-public crate-mobi-sys-0.1.2 (c (n "mobi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.44.0") (d #t) (k 1)))) (h "13yndlk7ni4cjlg78pgllqckwr65y4zdin1hn3lq7ggz703fwpza") (l "libmobi")))

