(define-module (crates-io mo bi mobilenet_v2) #:use-module (crates-io))

(define-public crate-mobilenet_v2-0.1.0 (c (n "mobilenet_v2") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.16.0") (d #t) (k 0)))) (h "0yh0kgdpghfz7v1hjbhcab1kc616mmij0r8vhxn1yz5g3gan8zvj")))

