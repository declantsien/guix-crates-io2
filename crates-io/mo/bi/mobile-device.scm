(define-module (crates-io mo bi mobile-device) #:use-module (crates-io))

(define-public crate-mobile-device-0.1.0 (c (n "mobile-device") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wnj5j2vbsx10wq26ig3vmmj1lwr9ajwsf3v9w4i7k81p8b0szxy")))

