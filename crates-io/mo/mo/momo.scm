(define-module (crates-io mo mo momo) #:use-module (crates-io))

(define-public crate-momo-0.2.1 (c (n "momo") (v "0.2.1") (d (list (d (n "watt") (r "^0.1") (d #t) (k 0)))) (h "0zm6lv6lmgn3rcvvgii4zwlnz5vwvcgrrz0kqiyxl666m07b6hw7")))

(define-public crate-momo-0.2.2 (c (n "momo") (v "0.2.2") (d (list (d (n "watt") (r "^0.4") (d #t) (k 0)))) (h "0sv97gcbagxfxalq34xdlv006s16xpk9rnlvl0lql1csa43ygyb7")))

