(define-module (crates-io mo mo momoa) #:use-module (crates-io))

(define-public crate-momoa-3.0.0 (c (n "momoa") (v "3.0.0") (d (list (d (n "glob") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "01rp9a9dsrlwhggmb04dzrvdzh9ddm7py5p4f2ns8xbn7al0523f")))

(define-public crate-momoa-3.0.1 (c (n "momoa") (v "3.0.1") (d (list (d (n "glob") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1pv6kla2sv3gb5rswf81k1isfqyql5fjdgznyl4ds1wcnqxkpiki")))

