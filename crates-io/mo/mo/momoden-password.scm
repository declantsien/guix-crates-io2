(define-module (crates-io mo mo momoden-password) #:use-module (crates-io))

(define-public crate-momoden-password-0.1.0 (c (n "momoden-password") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1sics89l7ajk17f3bm81rdh599ba7mq94y096r9xdg3w01piv8gg")))

