(define-module (crates-io mo rn morningtourust) #:use-module (crates-io))

(define-public crate-morningtourust-0.1.0 (c (n "morningtourust") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "15cvjlv0l6g2k7asfyfm8pp2hhy2l4y37v5c48zfy4zc6ykdqabl")))

(define-public crate-morningtourust-0.2.0 (c (n "morningtourust") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)))) (h "1ixg689z89cbkciwvpi7wwpix5zq1lgqj8pf0ds5vjq2zx0hsmcs")))

