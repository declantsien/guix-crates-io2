(define-module (crates-io mo zr mozrunner) #:use-module (crates-io))

(define-public crate-mozrunner-0.1.0 (c (n "mozrunner") (v "0.1.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mozprofile") (r "^0.1.0") (d #t) (k 0)))) (h "1v16b1rwnw5d0cp870izj20fd7v70xjv48y4vdy63fij8fan1hym")))

(define-public crate-mozrunner-0.2.0 (c (n "mozrunner") (v "0.2.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2.0") (d #t) (k 0)))) (h "1nfaiwnnyyxgk4g44vm49hpmz1pk9bs0xiayz5ji1q32mc4mggy0")))

(define-public crate-mozrunner-0.3.0 (c (n "mozrunner") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2") (d #t) (k 0)))) (h "0lwc8vp2gf38gf2lr8q3pdr66bvvw0k6iyc8nldimwzxblhwvn70")))

(define-public crate-mozrunner-0.3.1 (c (n "mozrunner") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0ja59p03zcjlcgi4ssawl3bnzn2baql3ifygy9nak4cd3d9i7ca6")))

(define-public crate-mozrunner-0.3.2 (c (n "mozrunner") (v "0.3.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1rxs77wn0y0qml1vjrvy50rdxl4airbnn2h3czixvbdp4k2fi6wr")))

(define-public crate-mozrunner-0.3.3 (c (n "mozrunner") (v "0.3.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "180dnlgcb5kl67bnpdxn7h124ynbj0arbl9b5c3bkl2cam3q21fh")))

(define-public crate-mozrunner-0.3.4 (c (n "mozrunner") (v "0.3.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "00hzg6z5m0rjyawkqxq68clx573yp885p2n45dnk4c3ldrs4mici")))

(define-public crate-mozrunner-0.4.0 (c (n "mozrunner") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0y2kx0xyvm6bqq2r5zh18cpq9zr0x9cf1ysds3b5la7w561ics55")))

(define-public crate-mozrunner-0.4.1 (c (n "mozrunner") (v "0.4.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "07riy8n888y2q0jp4i83xx3qn39m1dj8k6jdlfcp6wraycga5rk8")))

(define-public crate-mozrunner-0.4.2 (c (n "mozrunner") (v "0.4.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "10mcsyr0klj5kgacsiwfcmcd6vxk33ij94hism500v47m8z137gb")))

(define-public crate-mozrunner-0.5.0 (c (n "mozrunner") (v "0.5.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.3.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "02v0d61q4jqsdz05k0266mxrj250shb8ngw63k86b90zd353gw4g")))

(define-public crate-mozrunner-0.6.0 (c (n "mozrunner") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "11kdyh8jh3h6d5syqzjjflaxd1f3gx892gim4vgmg6n0g751y8ha")))

(define-public crate-mozrunner-0.6.1 (c (n "mozrunner") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1n1nisgffwx8s57h5s297mbj15jp1ryw6gw4kxakx6mfflskkmys")))

(define-public crate-mozrunner-0.7.0 (c (n "mozrunner") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.3.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "15qx3wjkaa27y1r899dqmirzxn99sck1kai62irda5xzq8s64j3q")))

(define-public crate-mozrunner-0.8.0 (c (n "mozrunner") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.4") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1sbkmass5j4x4gyr1vhsrpmk3p54gaff8jlv5rj19k091g3l97h9")))

(define-public crate-mozrunner-0.9.0 (c (n "mozrunner") (v "0.9.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.5.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1m2qcal3jimr574k8g5xfdzaf3yqqc18k90xrrmmq85p8ixzqa9h")))

(define-public crate-mozrunner-0.10.0 (c (n "mozrunner") (v "0.10.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.6.0") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03mrfwc8c0y8riprfnm8d7pmqigjqqp7a64n95ypmmz1vgbngzf3")))

(define-public crate-mozrunner-0.11.0 (c (n "mozrunner") (v "0.11.0") (d (list (d (n "dirs") (r "^2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.7") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0r4s4pp9mjddadaifvf90z4j8b4zxl9zfr2vp2imvr4bm0kypg2g")))

(define-public crate-mozrunner-0.12.0 (c (n "mozrunner") (v "0.12.0") (d (list (d (n "dirs") (r "^2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.7") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03lgbmjjmhba33rpcia6kki9zj0kxszlvfday9ylazlpa9lmqlgy")))

(define-public crate-mozrunner-0.12.1 (c (n "mozrunner") (v "0.12.1") (d (list (d (n "dirs") (r "^2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.7.1") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "12ddm0v2igrds5xwnvpgj9pziafg9awvlz12lbn669jvf48vdizh")))

(define-public crate-mozrunner-0.13.0 (c (n "mozrunner") (v "0.13.0") (d (list (d (n "dirs") (r "^2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.7") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1r4990y2x3vamc0y0psxwrm9icywrawq2jzj04z1s3v1xbhq0qyz")))

(define-public crate-mozrunner-0.14.0 (c (n "mozrunner") (v "0.14.0") (d (list (d (n "dirs") (r "^2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.8") (d #t) (k 0)) (d (n "plist") (r "^0.5") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16fhbyg3bms2wq12bppbdlls2z70xv2xcw1ml37qzwicljady6an")))

(define-public crate-mozrunner-0.15.0 (c (n "mozrunner") (v "0.15.0") (d (list (d (n "dirs") (r "^4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.9") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wlf773wqg6i6s754q4affqvi9a7gvbqzpk7nyvn2imlbafjzhk9")))

(define-public crate-mozrunner-0.15.1 (c (n "mozrunner") (v "0.15.1") (d (list (d (n "dirs") (r "^4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.9") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0pvfgw7acs767jr1knnhqabwpwi54dhpkk0vd9c6qpr3ynk2b28f")))

(define-public crate-mozrunner-0.15.2 (c (n "mozrunner") (v "0.15.2") (d (list (d (n "dirs") (r "^4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mozprofile") (r "^0.9") (d #t) (k 0)) (d (n "plist") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1w7wkxqgswm0g162qcp91ps10q0sl02f971rm73c8r03v1q46xvq")))

