(define-module (crates-io mo ji mojimoji-rs) #:use-module (crates-io))

(define-public crate-mojimoji-rs-0.1.0 (c (n "mojimoji-rs") (v "0.1.0") (h "189f4gbw6nz1m0if85q0k9dr468ih16qnl980nf8jzypl9z3and5")))

(define-public crate-mojimoji-rs-0.1.1 (c (n "mojimoji-rs") (v "0.1.1") (h "0mnblcqvw2wg4n13v5xjq0yh63szqmg68phd98gjg4lj9flf3cav")))

