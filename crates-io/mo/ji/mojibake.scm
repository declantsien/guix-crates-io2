(define-module (crates-io mo ji mojibake) #:use-module (crates-io))

(define-public crate-mojibake-0.1.0 (c (n "mojibake") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "17305lspsbfh6vp1lxj1rnqljwv5h6x746s0qkipcsbkvhpwlkvm") (y #t)))

(define-public crate-mojibake-0.2.0 (c (n "mojibake") (v "0.2.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1dmx6wi57imynd4wzk0zkh5gcmc1qz1shcxvyzshw045wyjmwp8g")))

(define-public crate-mojibake-0.2.1 (c (n "mojibake") (v "0.2.1") (d (list (d (n "phf") (r "^0.11.1") (k 0)) (d (n "phf") (r "^0.11.1") (k 1)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "14m38dfyn7v5wdnxazaphc4zfrxwbmcahvb5pbssqnspqjwska69")))

