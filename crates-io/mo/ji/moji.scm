(define-module (crates-io mo ji moji) #:use-module (crates-io))

(define-public crate-moji-0.1.0 (c (n "moji") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("default" "derive"))) (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "emoji") (r "^0.2.1") (d #t) (k 0)))) (h "0f4i6wkl5m1lfzp8gj7xkg5sm97367f9l7139nshi8fd9pj15ldm")))

