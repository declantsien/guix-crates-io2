(define-module (crates-io mo vi moving_gc_arena) #:use-module (crates-io))

(define-public crate-moving_gc_arena-0.1.0 (c (n "moving_gc_arena") (v "0.1.0") (h "0x0kr6ix8ysf1n723hk1xm6kfl4yifi5q7yjply0nh1a9zgkp56y")))

(define-public crate-moving_gc_arena-0.1.1 (c (n "moving_gc_arena") (v "0.1.1") (h "0vppgnqzyl2wkq247f6a97d8qmmv4shndmqw9p8dv9k8bd3sm3l4")))

(define-public crate-moving_gc_arena-0.2.0 (c (n "moving_gc_arena") (v "0.2.0") (h "1i38bc6mn6aql9d3sc3dgjnrzpj4p216lqkcxmb6fy37z6fz372b") (f (quote (("debug-arena"))))))

(define-public crate-moving_gc_arena-0.2.1 (c (n "moving_gc_arena") (v "0.2.1") (h "0cwqkw1rngrxf20884awax8419r15j6ycag0rr93r9mi9w9ppack") (f (quote (("packed-headers") ("debug-arena"))))))

(define-public crate-moving_gc_arena-0.2.2 (c (n "moving_gc_arena") (v "0.2.2") (h "1lmyiy9bkhvsymclipp5ljy3pjgr0y0i9h1s018dff4rasq8xnd0") (f (quote (("packed-headers") ("debug-arena"))))))

(define-public crate-moving_gc_arena-0.2.3 (c (n "moving_gc_arena") (v "0.2.3") (h "0g1chax4ckadzjam0786jldbh93gqz15slhr2i5salfnza8qwxd4") (f (quote (("packed-headers") ("debug-arena"))))))

(define-public crate-moving_gc_arena-0.2.4 (c (n "moving_gc_arena") (v "0.2.4") (h "1jbbmx48f48dcz0brng1awlvrz2nfbzk478iv20z86iv3fviy1ap") (f (quote (("packed-headers") ("debug-arena"))))))

(define-public crate-moving_gc_arena-0.2.5 (c (n "moving_gc_arena") (v "0.2.5") (h "06grkb58bwa53adj0y5i3c50bygv96hy7n8s2nsqf35kwaqx9j9c") (f (quote (("packed-headers") ("debug-arena")))) (y #t)))

(define-public crate-moving_gc_arena-0.2.6 (c (n "moving_gc_arena") (v "0.2.6") (h "0cr8nzzkc0fm7mrfxb83ypdgwmcs53hj2dwb35w2yjwixg6z0cy6") (f (quote (("packed-headers") ("debug-arena")))) (y #t)))

(define-public crate-moving_gc_arena-0.3.0 (c (n "moving_gc_arena") (v "0.3.0") (h "0a387vbwbwiny1kkpaw6bdnc59hy6g72022ki3sd8s5v6p68zk1n") (f (quote (("packed-headers") ("debug-arena")))) (y #t)))

(define-public crate-moving_gc_arena-0.3.1 (c (n "moving_gc_arena") (v "0.3.1") (h "1xhijcvb8n1znfj9rfxzj9y3kgpvzpvxzbv6yds834f25ndy74b4") (f (quote (("packed-headers") ("debug-arena")))) (y #t)))

(define-public crate-moving_gc_arena-0.3.2 (c (n "moving_gc_arena") (v "0.3.2") (h "069q07jyf1sjjcbfwiyfd545srdxfw1lpz9q1ay7g5hvddjsg61i") (f (quote (("packed-headers") ("debug-arena"))))))

(define-public crate-moving_gc_arena-0.3.3 (c (n "moving_gc_arena") (v "0.3.3") (h "0zha1004kgcf343mibx8b6ccf7vkdafd312dqdbqmnwykv4cy778") (f (quote (("packed-headers") ("debug-arena"))))))

