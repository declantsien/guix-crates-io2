(define-module (crates-io mo vi moving_avg) #:use-module (crates-io))

(define-public crate-moving_avg-0.1.0 (c (n "moving_avg") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)))) (h "05rd6qi2bpvkyl8pp8w6l72bs47cif9kw0cwnzpc11wkqm5k0k0s")))

(define-public crate-moving_avg-0.2.0 (c (n "moving_avg") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1yzzv9azfszlcdnlh5kam17dvp73njnwhdl7d7bnwrqpv0m4zyf5")))

