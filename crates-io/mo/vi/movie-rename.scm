(define-module (crates-io mo vi movie-rename) #:use-module (crates-io))

(define-public crate-movie-rename-1.2.1 (c (n "movie-rename") (v "1.2.1") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "load_file") (r "^1.0.1") (d #t) (k 0)) (d (n "tmdb") (r "^3.0.0") (d #t) (k 0)) (d (n "torrent-name-parser") (r "^0.11.0") (d #t) (k 0)))) (h "0xcb45bn95q3qy3yc60hwg0yih5j77fjcj2af656y0n44pjs4l6b")))

(define-public crate-movie-rename-2.0.0 (c (n "movie-rename") (v "2.0.0") (d (list (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "load_file") (r "^1.0.1") (d #t) (k 0)) (d (n "tmdb-api") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "torrent-name-parser") (r "^0.12.1") (d #t) (k 0)))) (h "1x229l5m2qw8010vi7dzqz2mxghxj42z0r020jdr5k55zxw3fyac")))

