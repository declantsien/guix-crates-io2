(define-module (crates-io mo vi moving-least-squares-image) #:use-module (crates-io))

(define-public crate-moving-least-squares-image-0.1.0 (c (n "moving-least-squares-image") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "moving-least-squares") (r "^0.1.0") (d #t) (k 0)))) (h "0m8m70b7icnzvkg2a7vz47czrxbgnk23gijm60hbr91m41m49h2k")))

(define-public crate-moving-least-squares-image-0.1.1 (c (n "moving-least-squares-image") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "moving-least-squares") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (o #t) (d #t) (k 0)))) (h "1x9y7yjjzw95n3qwm2kz3ciaigysi0f1zqwlpnd390qd7c652qjr")))

