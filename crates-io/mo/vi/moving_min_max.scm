(define-module (crates-io mo vi moving_min_max) #:use-module (crates-io))

(define-public crate-moving_min_max-0.1.0 (c (n "moving_min_max") (v "0.1.0") (h "0q250ma90l3dvxydy5n9j6mcjp6yhwp6p92xhd81v0y21g648sfz")))

(define-public crate-moving_min_max-0.1.1 (c (n "moving_min_max") (v "0.1.1") (h "1b2apvrplvq8pjkih0hncq16gm98lcpnjgns8wkaq804hxsz2sqg")))

(define-public crate-moving_min_max-0.1.2 (c (n "moving_min_max") (v "0.1.2") (h "0h2b8xvdzdka5fbyv027xlmhkqizbvd8cyh29vsi4qaqpy3a9x80")))

(define-public crate-moving_min_max-0.1.3 (c (n "moving_min_max") (v "0.1.3") (h "0vbinbxd5jc6f13dnvd0hr2h6ys7wghiz016ws3firnw2m47rqw3")))

(define-public crate-moving_min_max-0.1.4 (c (n "moving_min_max") (v "0.1.4") (h "0r5f5570b8lgyw4f7ri6cm9134bmmymzvlq05l89in8l9gindjs8")))

(define-public crate-moving_min_max-0.1.5 (c (n "moving_min_max") (v "0.1.5") (h "07hah41ih0v08k69khwi2d2lvqnjffmpn3fibmd2gyl3isw14bvl")))

(define-public crate-moving_min_max-1.0.0 (c (n "moving_min_max") (v "1.0.0") (h "1ywj6l3bb0h7mag8rkhjr820qjrrhi4m53wng6r63x16a3krppvp")))

(define-public crate-moving_min_max-1.1.0 (c (n "moving_min_max") (v "1.1.0") (h "0s4qf2c753wmhkjynm863qg6xnsm5gj2rdjg8vdry9n1vxpzm23h")))

(define-public crate-moving_min_max-1.2.0 (c (n "moving_min_max") (v "1.2.0") (h "10nhd7bfsslgsgz1xrhj014bbx2ml4c8xsqjmdiqlf43yi3aqihb")))

(define-public crate-moving_min_max-1.3.0 (c (n "moving_min_max") (v "1.3.0") (h "15ivspv4cqymhfrzba53ggw0wvz120wxv0cr3bl90ly6359zmp16")))

