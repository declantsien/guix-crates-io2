(define-module (crates-io mo vi movie) #:use-module (crates-io))

(define-public crate-movie-0.1.0 (c (n "movie") (v "0.1.0") (d (list (d (n "movie_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "movie_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1r6d2z4l8cvqbs4c3gbz6aqksrcrh8wmj8n6qsmixag8p69k7kzq")))

(define-public crate-movie-0.1.1 (c (n "movie") (v "0.1.1") (d (list (d (n "movie_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "movie_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1mdnsysn1bkjsyy6y6g4avbalc73fzmbw6js2k84cw4v0g2k9z6d")))

(define-public crate-movie-0.1.2 (c (n "movie") (v "0.1.2") (d (list (d (n "movie_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "movie_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1xf8llh35gp3snzzfpgvm67z7pqslnngk4aqnhxvvy39pn1ph898")))

