(define-module (crates-io mo vi movingai) #:use-module (crates-io))

(define-public crate-movingai-0.1.0 (c (n "movingai") (v "0.1.0") (h "19fvpnsh4z9z66cjl9fkyivkh6v04619g5901mhdq9nq3cca5x4v")))

(define-public crate-movingai-0.2.0 (c (n "movingai") (v "0.2.0") (h "1ib52y6iia4l7im284kckwkxhkgkq2fmvxh938b8vrz7w338b77f")))

(define-public crate-movingai-0.3.0 (c (n "movingai") (v "0.3.0") (h "1k0vr7gsip7lq6srnvrw5s504y8zjrn3zpmlrnrs19qsa3wy9i95")))

(define-public crate-movingai-0.4.0 (c (n "movingai") (v "0.4.0") (h "1b55g41bka1vrxy1khx1rzq9s8m56ccibnjw9vqijxjbg4nxwzsa")))

(define-public crate-movingai-0.5.0 (c (n "movingai") (v "0.5.0") (h "187bmdxqs3w33ka2k8cqx2j3z5bbpvnqfrjmbcbaw7292r2yy6c9")))

(define-public crate-movingai-0.5.1 (c (n "movingai") (v "0.5.1") (h "0kx6av83zcmwb2l5zgz1ddqab3ig7189y49h72f503danizpykna")))

(define-public crate-movingai-0.6.0 (c (n "movingai") (v "0.6.0") (h "1j0km0pa3di288bxdzj8a4l4hj4flnzq439c5h940pidk1gnxzhy")))

(define-public crate-movingai-0.7.0 (c (n "movingai") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yjjzzj8azyj7cybdn95i6wcysd647xj5yasnvk31x94bgvl3yhk")))

(define-public crate-movingai-0.8.0 (c (n "movingai") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00sb6zn7ghi5cq2brxsk8l9qh18vx545vlhvm1nmk7fpi0py1flw")))

(define-public crate-movingai-1.0.0 (c (n "movingai") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1kdh28kb2l8yswping44hy2lvpszfrc7m3nl3cm5zhwblwww0sxr")))

(define-public crate-movingai-1.1.0 (c (n "movingai") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0jr0m81g23862ldlnf3ix3b9f8shaa6i4hh7ac2jh3y34xjwgqjg")))

(define-public crate-movingai-1.1.1 (c (n "movingai") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 2)))) (h "1izix90xn30nid2f7d7fawd6swqyr9bsqvqnqk25babv0ijdzcs3")))

(define-public crate-movingai-1.2.0 (c (n "movingai") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "14bb7ra2if31b4h1gscycaikycfaxrfzs9nhx7052954x3my7wsp")))

(define-public crate-movingai-1.3.0 (c (n "movingai") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0innclbmpfxpbja7wf417rmw3zl9pipmrgqxc6x6414dypfbkl5i")))

(define-public crate-movingai-1.3.1 (c (n "movingai") (v "1.3.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "053r2ac9wi319lx9k30sak59m2j7g0j8g1rmj4a8w4frvpkk4rgl")))

