(define-module (crates-io mo df modfile) #:use-module (crates-io))

(define-public crate-modfile-1.3.2 (c (n "modfile") (v "1.3.2") (h "1z0zpqj12bfwbb7fykl90xvl3nv61iss42nh20g5rc16v1a1c9w6")))

(define-public crate-modfile-1.3.4 (c (n "modfile") (v "1.3.4") (h "117p2ls7k7f0mlsji0fvqgpg4fvf5fd11bkfyx4wjjd1j4sw57ih")))

(define-public crate-modfile-1.4.0 (c (n "modfile") (v "1.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m2sfd6gi33c430z6skxn4c5g3l73hmphnb04palhnasr5i7yh9k") (y #t)))

(define-public crate-modfile-1.4.5 (c (n "modfile") (v "1.4.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vfzy9235rmbd7lcr6ivcnff1yc6pcd9j28pma6m2vrsprdfry1g") (y #t)))

(define-public crate-modfile-1.4.6 (c (n "modfile") (v "1.4.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0apnigay6v476zfc8fdbynsimwmbla7dpn3vb45w9313lncm8lz1")))

