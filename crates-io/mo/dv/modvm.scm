(define-module (crates-io mo dv modvm) #:use-module (crates-io))

(define-public crate-modVM-0.2.0 (c (n "modVM") (v "0.2.0") (h "1ygpvnhi6qa57vszk9b4k5m5049hmxhmr59w7ddqwb03nc2nrmmf")))

(define-public crate-modVM-0.2.1 (c (n "modVM") (v "0.2.1") (h "1rl1if50c1drqb8qzy861mbmalwdhb4620535rhpz6slylg6q22d")))

(define-public crate-modVM-0.3.0 (c (n "modVM") (v "0.3.0") (h "19pksf6ml3wb8ysayr7gng9c4ms4vgm4jasfdxz9spy0gqdrgpj3")))

