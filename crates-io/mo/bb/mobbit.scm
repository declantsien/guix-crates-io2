(define-module (crates-io mo bb mobbit) #:use-module (crates-io))

(define-public crate-mobbit-0.1.0 (c (n "mobbit") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1jkzj6qkv3348xxnpdnppfnh0nlfk7rrcbf4n3xirrbsyzpqjbp2")))

(define-public crate-mobbit-0.1.1 (c (n "mobbit") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.11.2") (d #t) (k 0)))) (h "1x5w1cbj3s3ksd3z1r9hbfgxm98xwls88k4pbx1m390pwrljxlm9")))

(define-public crate-mobbit-0.2.0 (c (n "mobbit") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)))) (h "03hzfrv76pygrq550qfh7jidr3jkncnxjdzsx5d4wnlr7qqpw9fw")))

