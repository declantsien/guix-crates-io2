(define-module (crates-io mo no monostate-impl) #:use-module (crates-io))

(define-public crate-monostate-impl-0.1.0 (c (n "monostate-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1nl9158lqpg08czldhfhs2kqis8b7bplaa6mx36qq4prb12b8imm") (r "1.56")))

(define-public crate-monostate-impl-0.1.1 (c (n "monostate-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1g0wsjfjhiaa3nrlq72qdclclnrsvxagxfkiwcry2knh4rrn8h38") (r "1.56")))

(define-public crate-monostate-impl-0.1.2 (c (n "monostate-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "094abj7n9jj5g4rgi8499rgrs22nww8yyab1yi5z2a6sv8f17i5g") (r "1.56")))

(define-public crate-monostate-impl-0.1.3 (c (n "monostate-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "0n89v401f4zfk1nbfwlnqi6jgx8xjq5kw6y4qnm4fnljrks10b9k") (r "1.56")))

(define-public crate-monostate-impl-0.1.4 (c (n "monostate-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "0vyz22n5qdihwjjkkiqqfy1px3fli1r84p6ba6wg7ph6iy0y2gfz") (r "1.56")))

(define-public crate-monostate-impl-0.1.5 (c (n "monostate-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1aqnajxdax6sgqmjipcvmlam0w5dl0r6dq2bhfmg3wprjxyjmg6r") (r "1.56")))

(define-public crate-monostate-impl-0.1.6 (c (n "monostate-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "02sks8xx6q1mfdpnnxxkcx3jka4q90lx72w4iqgz2a20w79sv5c7") (r "1.56")))

(define-public crate-monostate-impl-0.1.7 (c (n "monostate-impl") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1ayixh27b6km3d0xrcw6vp3gafn94y0h4rxvsj16qndn6w9cv4pp") (r "1.56")))

(define-public crate-monostate-impl-0.1.8 (c (n "monostate-impl") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0xqkswir3ywdkx29748cqgf9k62gpz648jyb5w7jmrpx20p4sbz7") (r "1.56")))

(define-public crate-monostate-impl-0.1.9 (c (n "monostate-impl") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1kmx5m9qjh3ahplh445lzg9d59ssffn2x0mc1j06lgallp01f5rp") (r "1.56")))

(define-public crate-monostate-impl-0.1.10 (c (n "monostate-impl") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0g23ij5jp7007dxl4md79a6jziwqdryxi6x07pnrnhfs6jlq472k") (r "1.56")))

(define-public crate-monostate-impl-0.1.11 (c (n "monostate-impl") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1km6kc6yxvpsxciaj02zar8cx1sq142s6jn6saqn77h7165dd1pn") (r "1.56")))

(define-public crate-monostate-impl-0.1.12 (c (n "monostate-impl") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "19h5660c68v54g4y1k6mm964icvjwpzdm3f8rq89qykppnxpqc5z") (r "1.60")))

(define-public crate-monostate-impl-0.1.13 (c (n "monostate-impl") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1q3lxbfzpqcsy30gpyqkb2yppqzjj6ags6niflsi4kzdfnwn9km7") (r "1.60")))

