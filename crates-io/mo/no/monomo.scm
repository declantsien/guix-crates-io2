(define-module (crates-io mo no monomo) #:use-module (crates-io))

(define-public crate-monomo-0.1.0 (c (n "monomo") (v "0.1.0") (d (list (d (n "monomo_macros") (r "^0.1.0") (d #t) (k 0)))) (h "11h8h5zw3b1a7qlgrcihjxlwr541vfrb7z6plh094hd9mn8wlnl4")))

(define-public crate-monomo-0.1.1 (c (n "monomo") (v "0.1.1") (d (list (d (n "monomo_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0hhvfzy9vmqcf54cr59rc0b1j5095a9bxxfg3fi9nanzkv4w3wz1")))

(define-public crate-monomo-0.1.2 (c (n "monomo") (v "0.1.2") (d (list (d (n "monomo_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0s7zgagwwxygfgdsqpnz1lxifzqc29hpq2r69wz8pwvl6mfqkldj")))

(define-public crate-monomo-0.1.3 (c (n "monomo") (v "0.1.3") (d (list (d (n "monomo_macros") (r "^0.1.1") (d #t) (k 0)))) (h "13b3d1qwcrabwipq9bzj5mzpqhvs40gb0cl98abjkq427m84zl0k")))

