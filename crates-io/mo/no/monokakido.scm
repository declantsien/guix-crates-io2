(define-module (crates-io mo no monokakido) #:use-module (crates-io))

(define-public crate-monokakido-0.1.0 (c (n "monokakido") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6") (k 0)))) (h "1zb5fzdf1fxwf151jk7s7nhfj9507lzhmxm6h8yq0n06c7bg5qlx")))

(define-public crate-monokakido-0.2.0 (c (n "monokakido") (v "0.2.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6") (k 0)))) (h "1k0ndz2ly5ifikpzcrldlrn9m8lglbhc9s5hxfcvjlv70lpiprww")))

(define-public crate-monokakido-0.3.0 (c (n "monokakido") (v "0.3.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7") (k 0)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "146dz5f3ayz9cak8kvm4mh0ihb043273qbjnmv0dckpz1j2rsjpy")))

(define-public crate-monokakido-0.3.1 (c (n "monokakido") (v "0.3.1") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7") (k 0)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1f4kxshw2jdi0rjdww5g230mj0x7kljnhqjra4r9jvc8lrmkp7yc")))

(define-public crate-monokakido-0.3.2 (c (n "monokakido") (v "0.3.2") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7") (k 0)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1wc2f4b87ka0ps3dx6kzfdic1aq9zckr97qgx7yykddn3kjklbg8")))

