(define-module (crates-io mo no monocypher) #:use-module (crates-io))

(define-public crate-monocypher-0.1.0 (c (n "monocypher") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "monocypher-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0mxwfc9114ivymz41y20lhlnhg3637id6vk8plchwhplag6f3nlc") (y #t)))

(define-public crate-monocypher-0.1.1 (c (n "monocypher") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "monocypher-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0sk8gy3m2ym0f3z6dxmqy6lkdjfb8jcnm2yp1rvcr58pf1c98qk9") (y #t)))

(define-public crate-monocypher-0.2.0 (c (n "monocypher") (v "0.2.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^2.0.5") (d #t) (k 0)))) (h "0g7x67qh8x2b7p11a5xs7rsjs2q91d1iis1c4w8ycdgbiym2sihs") (y #t)))

(define-public crate-monocypher-0.3.0 (c (n "monocypher") (v "0.3.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^2.0.5") (d #t) (k 0)))) (h "04r63d2ghag26v3jnlys6cwbmbsp5lp4wfbm12nn00j2wfaa036m") (y #t)))

(define-public crate-monocypher-0.4.0 (c (n "monocypher") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^3.1.1") (d #t) (k 0)))) (h "1vkmqkksw7jcp2wkndfn4ljr4fj8hxg78wc9rrs29nzpqjwhp9wx") (y #t)))

(define-public crate-monocypher-0.4.1 (c (n "monocypher") (v "0.4.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^3.1.1") (d #t) (k 0)))) (h "1lkjd0bi8cgvccrnkz8p485iq6dhypi9azpmkiafsxs9lxa9rmyv") (y #t)))

(define-public crate-monocypher-0.4.2 (c (n "monocypher") (v "0.4.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^3.1.1") (d #t) (k 0)))) (h "01yqch8n8i07zidwjf3gg9fgp6h7dn8sgwzyir310yixddcfhxk1")))

(define-public crate-monocypher-0.4.3 (c (n "monocypher") (v "0.4.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^4.0.2") (d #t) (k 0)))) (h "13mmmcc2cnssf2f4raa2kxqs86vhfm60w5rsw99mhxc5d5nycx41") (f (quote (("ed25519" "monocypher-sys/ed25519") ("default"))))))

(define-public crate-monocypher-0.4.4 (c (n "monocypher") (v "0.4.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "monocypher-sys") (r "^4.0.2") (d #t) (k 0)))) (h "1wyv6bdq06497sqwpk2yjmvvhpr6svjv9vzgi22b9mpwzz304cj5") (f (quote (("ed25519" "monocypher-sys/ed25519") ("default"))))))

