(define-module (crates-io mo no mono-clock) #:use-module (crates-io))

(define-public crate-mono-clock-0.1.0 (c (n "mono-clock") (v "0.1.0") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)))) (h "1bzibzfx2dwhpqfvn045k56lhpnx50dyp6y90wvapyamsf15y9fr")))

(define-public crate-mono-clock-0.1.1 (c (n "mono-clock") (v "0.1.1") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)))) (h "1a3686kdih9nlbffi74smyarq1vvvxnnrnd9qzzaks086f7dbyg1")))

