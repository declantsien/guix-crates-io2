(define-module (crates-io mo no monomo_macros) #:use-module (crates-io))

(define-public crate-monomo_macros-0.1.0 (c (n "monomo_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0fqk6bifgmaln4hhf1awdpa56cgp9jm6hciv2df8zqadwzqjvf7k")))

(define-public crate-monomo_macros-0.1.1 (c (n "monomo_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0kkp72kb77y8gb6s639nmhg4zszcbl4npqqsff4c1344lyf9b6qp")))

