(define-module (crates-io mo no monolib) #:use-module (crates-io))

(define-public crate-monolib-0.2.1 (c (n "monolib") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0032y7h4cp2nmwyhflljzzibixpnnaq23zd69s0fy5s31k76v7k7")))

(define-public crate-monolib-0.2.2 (c (n "monolib") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pq6grh43j5hkhj5s1fhhbviwrmaazwq64ly5bzdwxs3x0qkqzlb")))

(define-public crate-monolib-0.3.0 (c (n "monolib") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vh3djr2a1lz4k97g6a8ylmibffi1hxc87gm5g2wx9z2rz4yiiac")))

(define-public crate-monolib-0.4.0 (c (n "monolib") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lonelyradio_types") (r "^0.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "1jarl108zd5s093fcw8q409n2j8wpywwgzsvab9d667zysmkcwj5")))

