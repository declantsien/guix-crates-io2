(define-module (crates-io mo no monoxide) #:use-module (crates-io))

(define-public crate-monoxide-0.0.1 (c (n "monoxide") (v "0.0.1") (d (list (d (n "bson") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "09hw4jgyk34wcm86w2k044l945018dzk02plyapfvg8bki3y6szw")))

(define-public crate-monoxide-0.0.2 (c (n "monoxide") (v "0.0.2") (d (list (d (n "bson") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1cmsy60pkkyvfhrqnwy35vny21prsj99hzk2wsvvafblcldnpx0k")))

(define-public crate-monoxide-0.0.4 (c (n "monoxide") (v "0.0.4") (d (list (d (n "bson") (r "^1.2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "mongodb") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0fdjvchqhyfvi891z0qk407hrb30c7b3kl48a3l3imnfxjw7zr8s")))

