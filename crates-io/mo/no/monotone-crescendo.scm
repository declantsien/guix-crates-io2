(define-module (crates-io mo no monotone-crescendo) #:use-module (crates-io))

(define-public crate-monotone-crescendo-1.0.0 (c (n "monotone-crescendo") (v "1.0.0") (h "163k7a1mysihfa25kk3yrkpcflr6y802f01b8dw57f29z91kqlkg")))

(define-public crate-monotone-crescendo-1.0.1 (c (n "monotone-crescendo") (v "1.0.1") (h "05d4n0wdavxci4fnlrxhmxi566xdk5ch30rc76gzmrly1lvdfp7n")))

(define-public crate-monotone-crescendo-1.0.2 (c (n "monotone-crescendo") (v "1.0.2") (h "05gjy767hsrkgar5k3dy12pqdvjax9nhhx9s2p3sidn8d4h2fp6k")))

(define-public crate-monotone-crescendo-1.0.3 (c (n "monotone-crescendo") (v "1.0.3") (h "1fm7xdf9rnqgs283bfh2qlm4ahgvq9z7bphq5yz0fdbbhphbqscs")))

(define-public crate-monotone-crescendo-1.0.4 (c (n "monotone-crescendo") (v "1.0.4") (h "136k9m8f27760nwh7y83k7p3lmgx49r5vv8gkdmwa1z8hyp5i09w")))

