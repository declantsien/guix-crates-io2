(define-module (crates-io mo no monoio-route) #:use-module (crates-io))

(define-public crate-monoio-route-0.1.0 (c (n "monoio-route") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "http-body-util") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "monoio") (r "^0.2.3") (d #t) (k 2)) (d (n "monoio-compat") (r "^0.2.2") (f (quote ("hyper"))) (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0j7ypvvz3vj0xzvr984jwczc2jxhlf3wl7jpzmdf73hrzc2qcyqn")))

(define-public crate-monoio-route-0.1.1 (c (n "monoio-route") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "http-body-util") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "monoio") (r "^0.2.3") (d #t) (k 2)) (d (n "monoio-compat") (r "^0.2.2") (f (quote ("hyper"))) (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1gsi4pwpqpahickb8lsmjnzf3ia9zs2jrdvf1lkdy0hi2hqvq0kl")))

