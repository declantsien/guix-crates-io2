(define-module (crates-io mo no monoio-codec) #:use-module (crates-io))

(define-public crate-monoio-codec-0.0.1 (c (n "monoio-codec") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.0.6") (k 0)) (d (n "monoio") (r "^0.0.6") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "0a6zkhxh0hdil9dw7a3y12g78wfvr8sjx2cp6yywqgc5x18q6wlx")))

(define-public crate-monoio-codec-0.0.3 (c (n "monoio-codec") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.0.8") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.0.8") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "01ly21071vpgcizix1sir525bhmp4kjzgcp69vd2x2x13w02ns32")))

(define-public crate-monoio-codec-0.0.4 (c (n "monoio-codec") (v "0.0.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.1.2") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.1.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1gs4r12lavmir2045362gp5adydba0knjdnd1pbw2zmax4ar3hp9")))

(define-public crate-monoio-codec-0.0.5 (c (n "monoio-codec") (v "0.0.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.1.2") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.1.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "0s07ay1k0znylnpqp9pbimmqp2ywmjvnh02vi53jlsmrs5az3g2w")))

(define-public crate-monoio-codec-0.1.0 (c (n "monoio-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.1.4") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.1.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "04bimf4r6h46aq8868g9c2zrk6hchawq4akhydkki3a8ibinv4jy")))

(define-public crate-monoio-codec-0.2.0 (c (n "monoio-codec") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.1.6") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.1.6") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1f3zy287kdf8cyxxhbx90x089lrgwvnf39nfwfdndayigsxwiixd")))

(define-public crate-monoio-codec-0.3.0 (c (n "monoio-codec") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "0kcp2m2108wn4074zk0vysm570l7z1v4lcwdsad7m4hfix0fszp2")))

(define-public crate-monoio-codec-0.3.1 (c (n "monoio-codec") (v "0.3.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1k1y73lh1vbziqyf9m5ydnw4rxb9cdx8yg81zwal8qxp72ycnr1p")))

(define-public crate-monoio-codec-0.0.0 (c (n "monoio-codec") (v "0.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (d #t) (k 0)) (d (n "monoio-codec") (r "^0.3.0") (d #t) (k 0)))) (h "1f4rnr2qrdcy5h454hgxgyq55lb1anxya58wiggwz47pnwa0vfxv") (y #t)))

(define-public crate-monoio-codec-0.3.2 (c (n "monoio-codec") (v "0.3.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "00zv9f1h9qs9653id5n9ymlpc66hcp51rm144f5i8gqj7i5ap4yc")))

(define-public crate-monoio-codec-0.3.3 (c (n "monoio-codec") (v "0.3.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1halrviydzi4d2i33v75fw8pp6zf4m9h1qc7zn7y4balwrfk6c68")))

(define-public crate-monoio-codec-0.3.4 (c (n "monoio-codec") (v "0.3.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("bytes"))) (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "085njmmncwvwl0f80bln6z7swdascfddyq3xgsl6dxyjfwjybh2q")))

