(define-module (crates-io mo no monotron-synth) #:use-module (crates-io))

(define-public crate-monotron-synth-0.1.0 (c (n "monotron-synth") (v "0.1.0") (d (list (d (n "libpulse-binding") (r "^2.2") (d #t) (k 2)) (d (n "libpulse-simple-binding") (r "^2.2") (d #t) (k 2)))) (h "0iz0rxmdhs8ivz7ag193gvbdv580l140pl6a4q4j95k6cln9gnwz")))

(define-public crate-monotron-synth-0.2.0 (c (n "monotron-synth") (v "0.2.0") (d (list (d (n "libpulse-binding") (r "^2.2.3") (k 2)) (d (n "libpulse-simple-binding") (r "^2.2.3") (k 2)))) (h "01g45zkxqglr08qbch1awy8xwgrz2hj4alk2zxhnmv9nwzl8g2yg")))

