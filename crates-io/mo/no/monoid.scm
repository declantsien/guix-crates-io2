(define-module (crates-io mo no monoid) #:use-module (crates-io))

(define-public crate-monoid-0.0.1 (c (n "monoid") (v "0.0.1") (h "1ix84cflm1xr2w9v9ry3mfag4h39hc2xjfa1wqj73w03y1qv6hlh")))

(define-public crate-monoid-0.0.2 (c (n "monoid") (v "0.0.2") (h "02cj137r6vr3r5z3bgbsq659xcp527nvzl3kg2l3mkq1cg6jm403")))

(define-public crate-monoid-0.0.3 (c (n "monoid") (v "0.0.3") (h "1j5l12ml9rx85fzl3pj0sva67cmy8xprcxz4v1b0pmn1fg62ly4n")))

(define-public crate-monoid-0.0.4 (c (n "monoid") (v "0.0.4") (h "0yz1f8rv40zw0r1zw98i632jnmxkxa1icnpvnzfkz60609ghg0ql")))

(define-public crate-monoid-0.0.5 (c (n "monoid") (v "0.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)))) (h "03l7rar7ikwbdjzs7gmx4c6xclya3n7aab8k0ihsy5g68pv4clm8")))

