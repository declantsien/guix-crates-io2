(define-module (crates-io mo no monoterm) #:use-module (crates-io))

(define-public crate-monoterm-0.1.0 (c (n "monoterm") (v "0.1.0") (d (list (d (n "filterm") (r "^0.1") (d #t) (k 0)))) (h "1bas4jx973q3j6caljkf19g3n36sk6qy5ri8v6szvv04rcadk55n")))

(define-public crate-monoterm-0.1.1 (c (n "monoterm") (v "0.1.1") (d (list (d (n "filterm") (r "^0.2") (d #t) (k 0)))) (h "14qj57vr5sv903rbnqd8zn6ixfv1zfyqppg7r67mwn4fzdnwdzh4")))

(define-public crate-monoterm-0.1.2 (c (n "monoterm") (v "0.1.2") (d (list (d (n "filterm") (r "^0.2") (d #t) (k 0)))) (h "05kpnzd48nzb3734qypgckk062p7g9i677fs55ykq78jrhkiaxl3")))

(define-public crate-monoterm-0.1.3 (c (n "monoterm") (v "0.1.3") (d (list (d (n "filterm") (r "^0.2") (d #t) (k 0)))) (h "18gsddzyr69fafg3d4sp6njnwrvinf6fngkbzsryn3fhibnp1g3l")))

(define-public crate-monoterm-0.1.4 (c (n "monoterm") (v "0.1.4") (d (list (d (n "filterm") (r "^0.2") (d #t) (k 0)))) (h "1rk7rfaskv37ljlvy02insppmr73nazng2w1bb39qwrs0c3ww816")))

(define-public crate-monoterm-0.1.5 (c (n "monoterm") (v "0.1.5") (d (list (d (n "filterm") (r "^0.3") (d #t) (k 0)))) (h "0vfv5dw9m9plsrsqfkc9lcrmgsj6dn2y66ah278pfawq0czw30wb")))

(define-public crate-monoterm-0.1.6 (c (n "monoterm") (v "0.1.6") (d (list (d (n "filterm") (r "^0.4") (d #t) (k 0)))) (h "1rh4dn0dj7x98r7hap3fc07461i96q3dh1km882zax56gdcbdigk") (r "1.63")))

(define-public crate-monoterm-0.1.7 (c (n "monoterm") (v "0.1.7") (d (list (d (n "filterm") (r "^0.4") (d #t) (k 0)))) (h "1milza90i3iqzny54al7sllwydjx68m6ffxi8dwrfgz0v2aq2747") (r "1.63")))

