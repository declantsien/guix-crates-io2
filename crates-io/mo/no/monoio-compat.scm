(define-module (crates-io mo no monoio-compat) #:use-module (crates-io))

(define-public crate-monoio-compat-0.0.3 (c (n "monoio-compat") (v "0.0.3") (d (list (d (n "local-sync") (r "^0.0.5") (d #t) (k 2)) (d (n "monoio") (r "^0.0.3") (k 0)) (d (n "monoio") (r "^0.0.3") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1fx3lqxm3v5cw4r3wq2nf5rd5426j2k214q87h09iwmgap0ngmrn")))

(define-public crate-monoio-compat-0.0.4 (c (n "monoio-compat") (v "0.0.4") (d (list (d (n "local-sync") (r "^0.0.5") (d #t) (k 2)) (d (n "monoio") (r "^0.0.3") (k 0)) (d (n "monoio") (r "^0.0.3") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0mjvsdwfhip1mspkvj6jnfgsv4hh0ywrs793m7ad04zfy8nlf650")))

(define-public crate-monoio-compat-0.0.5 (c (n "monoio-compat") (v "0.0.5") (d (list (d (n "local-sync") (r "^0.0.5") (d #t) (k 2)) (d (n "monoio") (r "^0.0.4") (k 0)) (d (n "monoio") (r "^0.0.4") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0q3si0x8ihpqnjkzlvj8yq9srr88m6kp98paqns4f6l0jvn59jmb")))

(define-public crate-monoio-compat-0.0.6 (c (n "monoio-compat") (v "0.0.6") (d (list (d (n "monoio") (r "^0.0.5") (k 0)) (d (n "monoio") (r "^0.0.5") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1xl5vh245iqjvs7j7jlaz5nadivgj7a47wrx8gb0a78s83bd6nry")))

(define-public crate-monoio-compat-0.0.8 (c (n "monoio-compat") (v "0.0.8") (d (list (d (n "monoio") (r "^0.0.8") (k 0)) (d (n "monoio") (r "^0.0.8") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0iykszf1m34i9j8v3mja3hsnyg11zgvaks9b13v3f9d0ixd4n2gs")))

(define-public crate-monoio-compat-0.0.9 (c (n "monoio-compat") (v "0.0.9") (d (list (d (n "monoio") (r "^0.0.9") (k 0)) (d (n "monoio") (r "^0.0.9") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1p9k004d4rf1a99cgpbbm4cfz655cg869hkpxfblz6va2whik6pf")))

(define-public crate-monoio-compat-0.1.0 (c (n "monoio-compat") (v "0.1.0") (d (list (d (n "monoio") (r "^0.1.0") (k 0)) (d (n "monoio") (r "^0.1.0") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1c71cws9c5vrv8hw5cdmsifibziqnzr4vnindjnbsscs9alm41qs")))

(define-public crate-monoio-compat-0.1.1 (c (n "monoio-compat") (v "0.1.1") (d (list (d (n "monoio") (r "^0.1.0") (k 0)) (d (n "monoio") (r "^0.1.0") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1l9h3jk7qb3m9rwj3nq403gyiy0hz88s0qsj5rhb71gr6xplayyc")))

(define-public crate-monoio-compat-0.1.2 (c (n "monoio-compat") (v "0.1.2") (d (list (d (n "monoio") (r "^0.1.0") (k 0)) (d (n "monoio") (r "^0.1.0") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "18yjbkrgmqlpi7610gvp2wc0licms4m3kggd6v78jdcgikg4phzc")))

(define-public crate-monoio-compat-0.2.0 (c (n "monoio-compat") (v "0.2.0") (d (list (d (n "monoio") (r "^0.2.0") (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0cwzd6vylw3y6saj5v1i0ysy9xjxc8nd7a81qlp09isal3y46q11")))

(define-public crate-monoio-compat-0.2.1 (c (n "monoio-compat") (v "0.2.1") (d (list (d (n "hyper") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "monoio") (r "^0.2.0") (k 0)) (d (n "monoio") (r "^0.2.0") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1lyi1555lh359isr69y83jwydfj4zbs8acwg5n6rlkkbcnyjbald") (s 2) (e (quote (("hyper" "dep:hyper" "dep:pin-project-lite" "monoio/poll-io"))))))

(define-public crate-monoio-compat-0.2.2 (c (n "monoio-compat") (v "0.2.2") (d (list (d (n "hyper") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "monoio") (r "^0.2.3") (f (quote ("legacy"))) (k 0)) (d (n "monoio") (r "^0.2.3") (f (quote ("async-cancel" "macros"))) (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reusable-box-future") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0v5fqppf3xm33ic22j1zfd5sz80in5f3rhgs56cbp4l7bq4pgcxa") (f (quote (("unstable" "monoio/unstable")))) (s 2) (e (quote (("hyper" "dep:hyper" "dep:pin-project-lite" "monoio/poll-io"))))))

