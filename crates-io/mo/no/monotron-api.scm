(define-module (crates-io mo no monotron-api) #:use-module (crates-io))

(define-public crate-monotron-api-0.1.0 (c (n "monotron-api") (v "0.1.0") (h "008l871wznydlh7bz4k0vqnb5kpaxgcg0h0cmcxbfvrif0rh6nwx")))

(define-public crate-monotron-api-0.2.0 (c (n "monotron-api") (v "0.2.0") (h "02ypff5b16hzhiwb919g0kpi7kl26wfl5m50x3jwqq4yaxalcdxl")))

(define-public crate-monotron-api-0.2.1 (c (n "monotron-api") (v "0.2.1") (h "19l7rniy20ddl0lzj4rrcs2jrhd8bwibzvqj3j5y4msjjzj21ckz")))

(define-public crate-monotron-api-0.2.2 (c (n "monotron-api") (v "0.2.2") (h "15w872ij97x7r5zdndrna719zracryxvr2hblf2rrw1imrbkhpd3")))

