(define-module (crates-io mo no monoio-quiche) #:use-module (crates-io))

(define-public crate-monoio-quiche-0.0.1 (c (n "monoio-quiche") (v "0.0.1") (d (list (d (n "local-sync") (r "^0.1.0") (d #t) (k 0)) (d (n "monoio") (r "^0.1.0") (d #t) (k 0)) (d (n "quiche") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0sy7731yi8mawb90s321j0qmc2llbjp6z8gmd6sgwn96j2bbgwnj")))

