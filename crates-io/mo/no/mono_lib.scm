(define-module (crates-io mo no mono_lib) #:use-module (crates-io))

(define-public crate-mono_lib-0.1.0 (c (n "mono_lib") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0bxcv891zfgp4y23h1jpiay0wfvxxs6ksh962bvn1bd1ir4q0b70")))

(define-public crate-mono_lib-0.1.1 (c (n "mono_lib") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0vm2s7i6gs6p17y16a6vnszm1zgigyx64zckmndagsmr24bgq9s3")))

(define-public crate-mono_lib-0.1.2 (c (n "mono_lib") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0hmfzkg5r1vj8icjsaqjvk2ajp95rnzgk9y58q6smasnjiiyhhbq")))

(define-public crate-mono_lib-0.1.3 (c (n "mono_lib") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0gixjjgk1zxhagl4m97gmz845l8dwhzcb578x6kyqisnil4v5q1d")))

(define-public crate-mono_lib-0.1.4 (c (n "mono_lib") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0vbjgm55m2fshk86xvlnlmm1vapxjabqml31ksdz1m4nn7lcg57q")))

(define-public crate-mono_lib-0.1.5 (c (n "mono_lib") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0viv2r381xlbnaa6fzymnagnvrcjhknb0jz4lq7ivbkp3izxmw3f")))

(define-public crate-mono_lib-0.1.6 (c (n "mono_lib") (v "0.1.6") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 0)))) (h "0jccgb00sv9j92fb2lkb4m84v33n331ir0djwk6hgj7v9w1133k9")))

