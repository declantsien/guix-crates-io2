(define-module (crates-io mo no monopiped) #:use-module (crates-io))

(define-public crate-monopiped-0.1.0 (c (n "monopiped") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("sink" "std"))) (k 0)) (d (n "mini-monocypher") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "0b2krnq6syrw4p6kl4lcwrz3cr7hdadg2dk9lqh2z9hrlws9lr4p")))

