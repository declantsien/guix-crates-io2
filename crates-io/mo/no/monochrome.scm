(define-module (crates-io mo no monochrome) #:use-module (crates-io))

(define-public crate-monochrome-0.1.0 (c (n "monochrome") (v "0.1.0") (d (list (d (n "magpie") (r "^0.1.0") (d #t) (k 0)))) (h "1n64yn3nb41113j4qgsv2lq7sl9268ifqzwanfcz5r5zfchwkqss")))

(define-public crate-monochrome-0.2.0 (c (n "monochrome") (v "0.2.0") (d (list (d (n "magpie") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0f2dn82d50f1ghlwkvs6f1lp3gf7mh63l16ig9kh779nar80fncv")))

(define-public crate-monochrome-0.3.0 (c (n "monochrome") (v "0.3.0") (d (list (d (n "magpie") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "17kxgbb2fsfld9rp6sirrqbxfggs6wqn6qx3hga7wb330ql0h89p")))

(define-public crate-monochrome-0.4.0 (c (n "monochrome") (v "0.4.0") (d (list (d (n "magpie") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)))) (h "1zwwyys9is0hb295qldp9ahxlawgsjx70cc4kcqnfic4x8bpfpn6")))

(define-public crate-monochrome-0.5.0 (c (n "monochrome") (v "0.5.0") (d (list (d (n "magpie") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.0") (d #t) (k 0)))) (h "0rrw7j2sdqmwadjcjicw703wihq067mrpz24p6g9vyx7iw9njhn4")))

(define-public crate-monochrome-0.5.1 (c (n "monochrome") (v "0.5.1") (d (list (d (n "magpie") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.0") (d #t) (k 0)))) (h "07vifn5y290r3dpjqx7p3vdibcn7y4ndrqph3g259j5sivij529d")))

