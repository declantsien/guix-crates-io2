(define-module (crates-io mo no monotonic-clock) #:use-module (crates-io))

(define-public crate-monotonic-clock-0.0.0 (c (n "monotonic-clock") (v "0.0.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "0nps7k740iw3jams531hbzayk77qc6g02apvhbg8xwrjm3fwj357") (y #t)))

(define-public crate-monotonic-clock-0.0.1 (c (n "monotonic-clock") (v "0.0.1") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "07gg4l373c9yypqbf2z7wmg56kl90x9cw4a0fzwn5wmp6z63c8ym") (y #t)))

(define-public crate-monotonic-clock-0.0.2 (c (n "monotonic-clock") (v "0.0.2") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "0kcczwqdq6vqp2rdf6d1ficdjfp1vr6vsj9c8r8sn2i3d6yp1wx5") (y #t)))

(define-public crate-monotonic-clock-0.0.3 (c (n "monotonic-clock") (v "0.0.3") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "1n9mv6i0vyryz73fxschh4rjvn05siax3rxfqa6qilr0cpbsb0xr") (y #t)))

(define-public crate-monotonic-clock-0.0.4 (c (n "monotonic-clock") (v "0.0.4") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "0lqdsj693qzdggpw9794s62k9f8gxwrgh5hw0m8h2dhcabsnxhbv") (y #t)))

(define-public crate-monotonic-clock-0.0.5 (c (n "monotonic-clock") (v "0.0.5") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "1djz4r0favg4varkhk6rcxk95nskd47c74k199d9fw2zhr9kf9a4") (y #t)))

(define-public crate-monotonic-clock-0.0.6 (c (n "monotonic-clock") (v "0.0.6") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "1z2cmz5i7hrlxf48l2dijny9qpv69p2qqrqcd7kmq1aljra6lf16")))

(define-public crate-monotonic-clock-0.0.7 (c (n "monotonic-clock") (v "0.0.7") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)))) (h "1slxcssksm0m19h86mcgjwkvnhzpcvs2iwy4bh2l1h9z0k039z76")))

