(define-module (crates-io mo no monofold) #:use-module (crates-io))

(define-public crate-monofold-0.1.0 (c (n "monofold") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "00fm95vf26rrr4ryzfxpzk1yyhb7alh8dn8acf6p5xjwbb41lfg3")))

(define-public crate-monofold-1.0.0 (c (n "monofold") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "0rm42kkkzcjra95p0hffg8g7a42pv40jz3n39z9ibn6zv6p6v1ya")))

(define-public crate-monofold-1.0.1 (c (n "monofold") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)))) (h "0xmi5zg61h9gi1ikicy1w0iwwb0644lfq9qb7d3b9givmyr822mn")))

