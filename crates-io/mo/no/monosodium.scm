(define-module (crates-io mo no monosodium) #:use-module (crates-io))

(define-public crate-monosodium-0.1.0-pre.0 (c (n "monosodium") (v "0.1.0-pre.0") (d (list (d (n "bindgen") (r "^0.47") (o #t) (d #t) (k 1)))) (h "1x075q565a5qxp15h1zcgp9vkvc1ss60907rvk49hzaky1p1d7pb") (f (quote (("fresh" "bindgen") ("default"))))))

(define-public crate-monosodium-0.1.0-pre.1 (c (n "monosodium") (v "0.1.0-pre.1") (d (list (d (n "bindgen") (r "^0.48") (o #t) (d #t) (k 1)))) (h "0b08yh5ahm3sq97wwbxzjpr8m3rb3ygldrhdys58dgfk2gap40ll") (f (quote (("fresh" "bindgen") ("default"))))))

