(define-module (crates-io mo no monocypher-sys) #:use-module (crates-io))

(define-public crate-monocypher-sys-1.0.0 (c (n "monocypher-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.32.3") (d #t) (k 1)))) (h "0khqkj6xxzkbpz9j0y4si06n8n17hqg7db7sqb0xplv9m8s06kby")))

(define-public crate-monocypher-sys-2.0.5 (c (n "monocypher-sys") (v "2.0.5") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)))) (h "1lvy9f45db2i5fkb7lyki1ry8gv3v1ggxhhxrzxqvidmyfkcq17k")))

(define-public crate-monocypher-sys-3.1.1 (c (n "monocypher-sys") (v "3.1.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)))) (h "1cw35i3jn15iqfm4q3idmxk3w1n1h2anqp44fzvaqyxbzxmdd4xi")))

(define-public crate-monocypher-sys-4.0.2 (c (n "monocypher-sys") (v "4.0.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15hwbzgww76q3adhzg2m26x6i8x7nm820amf0pfbiry9zj5ba4fw") (f (quote (("ed25519") ("default"))))))

