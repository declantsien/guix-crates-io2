(define-module (crates-io mo no mono-macro) #:use-module (crates-io))

(define-public crate-mono-macro-0.1.0 (c (n "mono-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1psw298sl3bi99dk4gxwy12di3c967c9qkjwf59nwjnx9yir7p6v")))

(define-public crate-mono-macro-0.1.1 (c (n "mono-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07ds3jh0ivr4j77cl57i3sngjipy6yxsa5i794p6718zciziqk6s")))

(define-public crate-mono-macro-0.1.2 (c (n "mono-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05dxhy6ys98dj19lyiq0yqk5vwcm426p0knrqijz2dksw82ijk9h")))

(define-public crate-mono-macro-0.1.3 (c (n "mono-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m0h40fvqg6h7x4rvjakqig2cak8g7hb14f8w5bf7zyb3qyd2dzc")))

