(define-module (crates-io mo no monotonic-time) #:use-module (crates-io))

(define-public crate-monotonic-time-1.0.0 (c (n "monotonic-time") (v "1.0.0") (h "18dzdnsjvd7ji9p9nfavbc9kyl8qa10plkdhw8f737v4hfh3f4z1") (f (quote (("std") ("no_std") ("default" "no_std"))))))

(define-public crate-monotonic-time-1.0.1 (c (n "monotonic-time") (v "1.0.1") (h "0065q32zgkpfq37wqr3bmhwqg0xvjspv1r0y7ajv9y1a6gfjhjz4") (f (quote (("std") ("no_std") ("default" "no_std"))))))

(define-public crate-monotonic-time-1.0.2 (c (n "monotonic-time") (v "1.0.2") (h "1a9ldsxdpbpkqqw1fbp9cd4yls526b6p48j2l1qymnf8gaxs585y") (f (quote (("std") ("no_std") ("default" "no_std"))))))

