(define-module (crates-io mo no mono-rs) #:use-module (crates-io))

(define-public crate-mono-rs-0.1.0 (c (n "mono-rs") (v "0.1.0") (h "0ca5wpzw6xmrm60ifvgxihxikhpqjabvv90dvqj1px69aarcaxfd") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.1 (c (n "mono-rs") (v "0.1.1") (h "09nk8fl3cdbmmjy1w2l8hdxrqp5502ii0dx88cd8pc42pksld9s8") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.2 (c (n "mono-rs") (v "0.1.2") (h "10kdprlzcnsqxf2bmlxslly3s43iidb3nyzwb3lnv0c3xq5carb8") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.3 (c (n "mono-rs") (v "0.1.3") (h "1nmnz0cisxav31h9any72lrslghhm6pxak55is4xrb99qama2bqf") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.4 (c (n "mono-rs") (v "0.1.4") (h "024f45rbx6j977ggp4dsz9bnjh2z7aplmwnz9z6s3d2hs78vandr") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.5 (c (n "mono-rs") (v "0.1.5") (h "1c7aa7bz5ibpmisr1pch9lfm92yy33kk00fkmxz6cv4k7zp8m5mx") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.6 (c (n "mono-rs") (v "0.1.6") (h "0z0jh1h1zgnciz3p9kdfcxkqmwvh767andsslvb5q92y3vp90wja") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.7 (c (n "mono-rs") (v "0.1.7") (h "171bbymk1jrck61fshlxz2vnrr3ar2pazy1plbzp0kvagrlb2i0b") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.8 (c (n "mono-rs") (v "0.1.8") (h "0l9k199qvwvslfalqkw44ncfd6s76c5vvma5xjxhb7n60j47zv4y") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.9 (c (n "mono-rs") (v "0.1.9") (h "0z9facw77h3skdmvi0z9jql4n9rc3zkmdrk9lbk1n1n2wkvsm36i") (l "mono-2.0")))

(define-public crate-mono-rs-0.1.10 (c (n "mono-rs") (v "0.1.10") (h "0a41saldplp4niwiv7xigdnmbs4p6i5ln1i29463g8wjbm3gc36y") (l "mono-2.0")))

