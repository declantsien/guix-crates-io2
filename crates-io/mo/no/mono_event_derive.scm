(define-module (crates-io mo no mono_event_derive) #:use-module (crates-io))

(define-public crate-mono_event_derive-0.1.0 (c (n "mono_event_derive") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "19ab3fa3kgb2q8nqzgv0jfajx2gx8gk4gc93qdxpi1hi48fscdkk")))

(define-public crate-mono_event_derive-0.2.0 (c (n "mono_event_derive") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1yx9ms7x1q6zsgnwwmhil0wgjmc3ys3h9kspjcmlmg8x7vyjqfk0")))

(define-public crate-mono_event_derive-0.3.0 (c (n "mono_event_derive") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "00jfxyfm7qakgm8kc3v1ndy3dnsjpf1835gs8x9wsyn2g8qpwg3j")))

(define-public crate-mono_event_derive-0.4.0 (c (n "mono_event_derive") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1r62201i929snsmx9z9x921s3cycfwhzn739v5g03rd50c6qh8p6")))

(define-public crate-mono_event_derive-0.5.0 (c (n "mono_event_derive") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0366rslbm49bvk916yjs3zylvyk17j18ibkxwip10k0kj5klk186")))

(define-public crate-mono_event_derive-0.6.0 (c (n "mono_event_derive") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0285kfsiybbi3q5v7fvimy9i6jfycnr3saadymwhcqmrciqagnwc")))

