(define-module (crates-io mo no monotonic_solver) #:use-module (crates-io))

(define-public crate-monotonic_solver-0.1.0 (c (n "monotonic_solver") (v "0.1.0") (h "1a8pc7253yscknisig8m1cfks87s0982fdq2scnb7jldxw8qnfxz")))

(define-public crate-monotonic_solver-0.2.0 (c (n "monotonic_solver") (v "0.2.0") (h "07q6dx133mxfn6prggjpckpfazcs87iqd2zka308na1lqr0gxkx0")))

(define-public crate-monotonic_solver-0.2.1 (c (n "monotonic_solver") (v "0.2.1") (h "0l4hp3samgvcwk6pd25w6dmkncari95z4iiha4b0xc4rsrvrb58h")))

(define-public crate-monotonic_solver-0.3.0 (c (n "monotonic_solver") (v "0.3.0") (h "1vl7ihlmzlbffyrpr18mn223nlcnpv6y1wm43dh8xvnfr64w6n83")))

(define-public crate-monotonic_solver-0.4.0 (c (n "monotonic_solver") (v "0.4.0") (h "03jgmya94ypwyhfzw6lwrm46pwxzijn1v51g0zs9c8a9wbwc1jqk")))

(define-public crate-monotonic_solver-0.5.0 (c (n "monotonic_solver") (v "0.5.0") (h "0gg2101m93ry27pi6w121wqxh1jr2x4rgrha2xf0v4vrwgq6fk7b")))

