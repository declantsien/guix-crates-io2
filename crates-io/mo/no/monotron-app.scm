(define-module (crates-io mo no monotron-app) #:use-module (crates-io))

(define-public crate-monotron-app-0.5.0 (c (n "monotron-app") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "sdl2") (r "^0.32") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "vga-framebuffer") (r "^0.7.5") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)))) (h "1b1ndwxvy2hsmhf48ac9mdx9yrgjw9w5868197qv0pv1y8lj0y25") (f (quote (("print-panic"))))))

