(define-module (crates-io mo no monochord) #:use-module (crates-io))

(define-public crate-monochord-0.1.0 (c (n "monochord") (v "0.1.0") (h "10s02xkx960vw4ykv312bw7psm1b701jybkp584c3dvrdi19fq02")))

(define-public crate-monochord-0.1.1 (c (n "monochord") (v "0.1.1") (h "0rrv2z3rs37vnsi9lh57248ijy7xd44gywp7nmgw2ccp9s6n9jdg")))

