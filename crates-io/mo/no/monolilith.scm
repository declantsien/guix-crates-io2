(define-module (crates-io mo no monolilith) #:use-module (crates-io))

(define-public crate-monolilith-1.0.0 (c (n "monolilith") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 1)) (d (n "kagero") (r "^0.3.0") (f (quote ("printer" "runner"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "08g67c6bxscyn0jw8pzvksw1mlww52x8v7mr2b98890v9qmyszlv") (y #t)))

(define-public crate-monolilith-1.0.1 (c (n "monolilith") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 1)) (d (n "kagero") (r "^0.3.0") (f (quote ("printer" "runner"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0hv7s0v0j4pzqkzacxs1r2mwqb9s91yc08v9zl039m5kppczr8bj") (y #t)))

(define-public crate-monolilith-1.0.2 (c (n "monolilith") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 1)) (d (n "kagero") (r "^0.3.0") (f (quote ("printer" "runner"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1rlmbbmgkninj9av99vpzlaaiha5pv105hp2pqxw99x50vhjqpiz") (y #t)))

(define-public crate-monolilith-1.1.0 (c (n "monolilith") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 1)) (d (n "kagero") (r "^0.3.0") (f (quote ("printer" "runner"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1gixjjk5li95p33zk73xp5c65gx6qmf8ysjvpq5lzgxb74ajjazw") (y #t)))

