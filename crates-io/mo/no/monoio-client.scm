(define-module (crates-io mo no monoio-client) #:use-module (crates-io))

(define-public crate-monoio-client-0.1.0 (c (n "monoio-client") (v "0.1.0") (d (list (d (n "local-sync") (r "^0.1.0") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x7jdcnzjf08hlb7xrpnha8h87r3zgz1llapf0brz92mmy4mrsnn")))

