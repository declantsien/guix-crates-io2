(define-module (crates-io mo no mono_display) #:use-module (crates-io))

(define-public crate-mono_display-0.1.0 (c (n "mono_display") (v "0.1.0") (d (list (d (n "bdf") (r "^0.5.5") (d #t) (k 0)) (d (n "font-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "rppal") (r "^0.7.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (o #t) (d #t) (k 0)))) (h "0401w9vhmhr4khy0qk11jjipwvdgl8227fip6168lfnzry1h7nm7") (f (quote (("default"))))))

