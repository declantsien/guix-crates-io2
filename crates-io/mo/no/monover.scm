(define-module (crates-io mo no monover) #:use-module (crates-io))

(define-public crate-monover-0.1.0 (c (n "monover") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kfmcgfl4iy6wfzfl1k4k72gaq5fjq1p9jkpdrvak2l921pjbvw2") (f (quote (("test-cleanup") ("default" "test-cleanup"))))))

