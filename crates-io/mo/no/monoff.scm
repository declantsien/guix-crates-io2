(define-module (crates-io mo no monoff) #:use-module (crates-io))

(define-public crate-monoff-0.4.1 (c (n "monoff") (v "0.4.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "17x70fjx5fvmkwmsfr3zlc1ccf63kwadzm0askf5wd2ix9pfkr2a")))

(define-public crate-monoff-0.4.2 (c (n "monoff") (v "0.4.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0mp5hxd7jr4l3qpgm4kri1gbnfpgyny62p0hpvn1fmymbi6shdc9")))

(define-public crate-monoff-0.4.3 (c (n "monoff") (v "0.4.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "01b4afkbbicgqw6xrk7ix5k0ncapi5c7ljssd3shhvi250s6xy4w")))

(define-public crate-monoff-0.4.4 (c (n "monoff") (v "0.4.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "1wcayp3kg2slr07sm5a360qdzl6sdklnmkkkgif9fjr10x7pz8bc")))

(define-public crate-monoff-0.4.5 (c (n "monoff") (v "0.4.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0ca8zr4yifssfxw6s038dlaf0vfa8qfggvnym6xhbagfdksymmmj")))

(define-public crate-monoff-0.4.6 (c (n "monoff") (v "0.4.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "1pz5mph8cjpzx9a9xlp9p7b3gblhnhydz6v63ikvnnbgm6j0ykac")))

