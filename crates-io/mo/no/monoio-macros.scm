(define-module (crates-io mo no monoio-macros) #:use-module (crates-io))

(define-public crate-monoio-macros-0.0.2 (c (n "monoio-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqaiydazv2z1jk5r7qp7hihn1y06bvlff9dll3nq6rqas35a9b3")))

(define-public crate-monoio-macros-0.0.3 (c (n "monoio-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15czm8ala26l6cihsfv3awbdi5cs76zbyafs33nb34612hhrsp68")))

(define-public crate-monoio-macros-0.1.0 (c (n "monoio-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k08a925wmbw9902iimwxys34lqk25gadwkw6f49wgb1d5g5yshp")))

