(define-module (crates-io mo no monostate) #:use-module (crates-io))

(define-public crate-monostate-0.1.0 (c (n "monostate") (v "0.1.0") (d (list (d (n "monostate-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11bs0hf67fiwhkdpfzl07glglyzaavx9pafhzxflypqkn6qhmnf2") (r "1.56")))

(define-public crate-monostate-0.1.1 (c (n "monostate") (v "0.1.1") (d (list (d (n "monostate-impl") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07z7bcqg6p8wrxfdiys5y47zdwdg5801jyz839mrl6aj8h3ncps8") (r "1.56")))

(define-public crate-monostate-0.1.2 (c (n "monostate") (v "0.1.2") (d (list (d (n "monostate-impl") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07xz7iqqxl9jwqh793331r5kfva11vg9fwddvp5zn8ndcv0jk3nd") (r "1.56")))

(define-public crate-monostate-0.1.3 (c (n "monostate") (v "0.1.3") (d (list (d (n "monostate-impl") (r "=0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bgisgjpnp03nmdcrdjsds7v2nqpll51nrrmh0bm2s5dc5dl2h8q") (r "1.56")))

(define-public crate-monostate-0.1.4 (c (n "monostate") (v "0.1.4") (d (list (d (n "monostate-impl") (r "=0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0309agpypcipfybdjqk8hdpmk176xbjyschwkv4z4wkh7rs4z6v9") (r "1.56")))

(define-public crate-monostate-0.1.5 (c (n "monostate") (v "0.1.5") (d (list (d (n "monostate-impl") (r "=0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dhjs1bsgrhgabwy5fbl0781ra8dy98hxai254vdgrm6hfwrilvd") (r "1.56")))

(define-public crate-monostate-0.1.6 (c (n "monostate") (v "0.1.6") (d (list (d (n "monostate-impl") (r "=0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12mrbfnx3jnb3x7zmkbycnpwrg3jahjx5l7n4hgdyddcy41vfc02") (r "1.56")))

(define-public crate-monostate-0.1.7 (c (n "monostate") (v "0.1.7") (d (list (d (n "monostate-impl") (r "=0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xq5hhizc4zi8kaq3418g86n3hagc6diyavk06sswwvghfj9vcyq") (r "1.56")))

(define-public crate-monostate-0.1.8 (c (n "monostate") (v "0.1.8") (d (list (d (n "monostate-impl") (r "=0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "06vbic5hcazgsydn1c9mk8qbnhdvwg9c6cva094gchi8h2l5fgrz") (r "1.56")))

(define-public crate-monostate-0.1.9 (c (n "monostate") (v "0.1.9") (d (list (d (n "monostate-impl") (r "=0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1vp8434xj6yh61pas6zxz8hd8q8s698yq3bi3aqwcgh9i2p71wqm") (r "1.56")))

(define-public crate-monostate-0.1.10 (c (n "monostate") (v "0.1.10") (d (list (d (n "monostate-impl") (r "=0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1503h7bwdgkg6w6mkv3bm86nmrqdr3h998ism7nz03ga40wf2174") (r "1.56")))

(define-public crate-monostate-0.1.11 (c (n "monostate") (v "0.1.11") (d (list (d (n "monostate-impl") (r "=0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0xchz8cs990g7g5f8jjybjnyi9xnhykiq44gl97p5rbh3hgjm347") (r "1.56")))

(define-public crate-monostate-0.1.12 (c (n "monostate") (v "0.1.12") (d (list (d (n "monostate-impl") (r "=0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1gzqi8gplj9nrgnz8pc9j0gv8iq182y1m9rnw0qrvim4ik6zy3x2") (r "1.60")))

(define-public crate-monostate-0.1.13 (c (n "monostate") (v "0.1.13") (d (list (d (n "monostate-impl") (r "=0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "07hfvh2202477mx1ff47b6f04gihqcdrmdndv10x0b2msw3q880d") (r "1.60")))

