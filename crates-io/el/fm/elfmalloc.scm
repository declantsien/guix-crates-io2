(define-module (crates-io el fm elfmalloc) #:use-module (crates-io))

(define-public crate-elfmalloc-0.1.0 (c (n "elfmalloc") (v "0.1.0") (d (list (d (n "bagpipe") (r "^0.1.0") (d #t) (k 0)) (d (n "bsalloc") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mmap-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5") (d #t) (k 0)))) (h "09p52y4q2rq1nfdgawcdby3wvqlywklpn46xlw6h2hj2dkz8dw8d") (f (quote (("use_default_allocator") ("print_stats" "nightly") ("prime_schedules" "bagpipe/prime_schedules") ("no_lazy_region") ("nightly") ("local_cache") ("huge_segments" "bagpipe/huge_segments"))))))

