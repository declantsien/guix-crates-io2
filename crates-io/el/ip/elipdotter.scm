(define-module (crates-io el ip elipdotter) #:use-module (crates-io))

(define-public crate-elipdotter-0.1.0 (c (n "elipdotter") (v "0.1.0") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "03cqzifndk8c8p431zk0in3h3d73j0jgszh6z3gzakxp8kagxqgy") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.1.1 (c (n "elipdotter") (v "0.1.1") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "1a6nzcn037ycsz52fbd7261nh935wn8pwng6hkpan13k8k3pv9i7") (r "1.56")))

(define-public crate-elipdotter-0.2.0 (c (n "elipdotter") (v "0.2.0") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "0alzfffm2smjdgr7yhfff4lh03lms6iv0m2f2h4j2pp3fi5yzv3b") (r "1.56")))

(define-public crate-elipdotter-0.3.0 (c (n "elipdotter") (v "0.3.0") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "05qxivpl6mlbhwg4fm80phrbpcrnklp03vm4c16gb15pdnf3ijcz") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.3.1 (c (n "elipdotter") (v "0.3.1") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0ykff6gq161ddvgca7gfil977vdijrj5wq2xlm2h73g5p7s9pxwc") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.3.2 (c (n "elipdotter") (v "0.3.2") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0v3xrarypg334jpg9r045ngi8x7pz5ra3yx7fvzppfdr6frp8lf8") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.3.3 (c (n "elipdotter") (v "0.3.3") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0kwki0cl828yb2gv0nkfgm0khz10f26zkmps6ng008wshic8kc8k") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.3.4 (c (n "elipdotter") (v "0.3.4") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "01xkpb74ysl2n35apxibyb6lpg2r457rprfsiz7him9q7k9gq5qg") (y #t) (r "1.56")))

(define-public crate-elipdotter-0.3.5 (c (n "elipdotter") (v "0.3.5") (d (list (d (n "iter-set") (r "^2.0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "0ps142mbycxpirf3mqx3kka32y15pyv31pj5azhs9can1rxdffbk") (r "1.56")))

