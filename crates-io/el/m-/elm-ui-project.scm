(define-module (crates-io el m- elm-ui-project) #:use-module (crates-io))

(define-public crate-elm-ui-project-0.1.0 (c (n "elm-ui-project") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "0i1ps1r1bc4qzrf584schdlj5lrdgvgbp22f6whxhgfd20xmm4r4")))

(define-public crate-elm-ui-project-0.1.1 (c (n "elm-ui-project") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "06d7d9hvh6hxawgxv0lhwaqwl1a2nml5vjzcv6b14ff0fc91n0cd")))

(define-public crate-elm-ui-project-0.1.2 (c (n "elm-ui-project") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "1x878mp12z5plcymi65dlqr16lw332ksfrl5c4bzfhrm4m9b4b5q")))

(define-public crate-elm-ui-project-0.1.3 (c (n "elm-ui-project") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "0abhpqzy5n4yn4b8xlmvd0safa75aywl9rj31nrmddzagx32z4wg")))

(define-public crate-elm-ui-project-0.1.4 (c (n "elm-ui-project") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "19l40zacpg80glm5w0xzszl2mmaa2yapqkkaqz8ca7sak0rbhfwk")))

(define-public crate-elm-ui-project-0.1.5 (c (n "elm-ui-project") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "03x9ir9pph4lpkxaav5w96vx8llrv4n1vbrzpqp0yn3h9ywjqscn")))

(define-public crate-elm-ui-project-0.1.6 (c (n "elm-ui-project") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.10") (k 0)))) (h "1d4d3d8smffy27a1ld6kklf5vffhk1lr42ysrwy7mfpxd3r3cl7n")))

