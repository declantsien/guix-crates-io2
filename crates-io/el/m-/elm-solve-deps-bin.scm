(define-module (crates-io el m- elm-solve-deps-bin) #:use-module (crates-io))

(define-public crate-elm-solve-deps-bin-0.1.1 (c (n "elm-solve-deps-bin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "elm-solve-deps") (r "^0.1.1") (d #t) (k 0)) (d (n "pubgrub") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (d #t) (k 0)))) (h "1ny3hccl2gg8n8bxs6yk4bjwj9hhvrk93np6ngwpgvk6l69g4p0c")))

