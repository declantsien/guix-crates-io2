(define-module (crates-io el m- elm-offline) #:use-module (crates-io))

(define-public crate-elm-offline-0.0.0 (c (n "elm-offline") (v "0.0.0") (h "0qh92wz0vn1r011b76wp0gp0bvr50xjxxm8l212jqxd9ixawd9hs")))

(define-public crate-elm-offline-0.0.1 (c (n "elm-offline") (v "0.0.1") (h "1kwymzlhwbfy4n3cf9fvfkhd658wkqng3p9la3cwrk82lhybxpm2")))

