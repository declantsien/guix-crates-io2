(define-module (crates-io el m- elm-solve-deps) #:use-module (crates-io))

(define-public crate-elm-solve-deps-0.1.0 (c (n "elm-solve-deps") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "pubgrub") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (d #t) (k 2)))) (h "1jn98piyi3rq6mfi1kcllq31qbn6pp1r0q34zpwkfizsxpll6v1y")))

(define-public crate-elm-solve-deps-0.1.1 (c (n "elm-solve-deps") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "pubgrub") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (d #t) (k 2)))) (h "1s0h0gdcak7cq9rcbfzg9n63c9lkzaxnsa2a7g6nwdijghsyrryn")))

