(define-module (crates-io el iz eliza_error) #:use-module (crates-io))

(define-public crate-eliza_error-0.99.0 (c (n "eliza_error") (v "0.99.0") (h "1q895ixz36m27d45kzpa8w13m3i9m94r1nf9wda6vdaspd5pb2xp")))

(define-public crate-eliza_error-0.99.1 (c (n "eliza_error") (v "0.99.1") (h "0cbw4fxsncdxrwvwdcazr3w5cpji446szq3q65y6xqkjl7c3f47s")))

