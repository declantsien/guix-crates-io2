(define-module (crates-io el fr elfredo) #:use-module (crates-io))

(define-public crate-elfredo-0.1.0 (c (n "elfredo") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "cargo-binutils") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "goblin") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1xvbk3lphqpcydwv9n8nk0xfcaj0f6msp8hi9bp70s4zcvr2pbjb")))

(define-public crate-elfredo-0.1.1 (c (n "elfredo") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "cargo-binutils") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "goblin") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1dyn10xzblc1p7ddzdvzv9rni1k0kaxqvlv99ldpyc4m3hqpjz1w")))

