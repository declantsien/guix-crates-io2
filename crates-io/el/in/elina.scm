(define-module (crates-io el in elina) #:use-module (crates-io))

(define-public crate-elina-0.1.0 (c (n "elina") (v "0.1.0") (d (list (d (n "elina-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x1q0p63f69q9xrcmhjv0gmzc5fv3p5b6b0m8lf4317acdpncy8i")))

(define-public crate-elina-0.1.1 (c (n "elina") (v "0.1.1") (d (list (d (n "elina-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1ljp7my18ni23hzc7gi3zbgblwfdy56zd48qlsrbir9v2pa1xq6h")))

(define-public crate-elina-0.2.0 (c (n "elina") (v "0.2.0") (d (list (d (n "elina-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1iibi828375x63fj0l3rvlq7b94hviwrvj599lkm5k2p3awrp2d0")))

(define-public crate-elina-0.3.0 (c (n "elina") (v "0.3.0") (d (list (d (n "elina-sys") (r "^0.1.1") (d #t) (k 0)))) (h "09333vnflga76rpmmsg5vs7fa5lwppk1r4l47sxl0lnj0ipw8iwm")))

(define-public crate-elina-0.3.1 (c (n "elina") (v "0.3.1") (d (list (d (n "elina-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1dcrpj0rhx4gxc5b7dfwwa2ah5fa5ra5bgfrznibpx82f9c1k0fw")))

