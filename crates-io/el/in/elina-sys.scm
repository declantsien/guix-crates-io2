(define-module (crates-io el in elina-sys) #:use-module (crates-io))

(define-public crate-elina-sys-0.1.0 (c (n "elina-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "1ykyfsxg0f4kasg1zbvd26pma1jkrrgwazixmn58v4hv8ca3q8xr")))

(define-public crate-elina-sys-0.1.1 (c (n "elina-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "043y5yywg7ilsh8xk2n640r6qxhkpxzv6a5dybcnj2iijsmdcjbd")))

