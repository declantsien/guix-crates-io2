(define-module (crates-io el fx elfx86exts) #:use-module (crates-io))

(define-public crate-elfx86exts-0.1.0 (c (n "elfx86exts") (v "0.1.0") (d (list (d (n "capstone3") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6") (d #t) (k 0)))) (h "064va95id3malrys1r4smlnzjg24zd7hqpavszk95lypb9f10dm5")))

(define-public crate-elfx86exts-0.2.0 (c (n "elfx86exts") (v "0.2.0") (d (list (d (n "capstone") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "object") (r "^0.7") (d #t) (k 0)))) (h "1hgsp7sz38kk92d3fiwilflf2zqd8ycywdvq1sg00rwwvwycvkg9")))

(define-public crate-elfx86exts-0.3.0 (c (n "elfx86exts") (v "0.3.0") (d (list (d (n "capstone") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.10") (d #t) (k 0)))) (h "1l2j8w8p3pz0f4jmk44pfvidz4bpc95z2k7c3393vinxzc7mfqpv")))

(define-public crate-elfx86exts-0.4.0 (c (n "elfx86exts") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "capstone") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.18") (d #t) (k 0)))) (h "0pm9c37bglr327fzhwq92z3gs56xd59yyz13rzbl8smb6v38lxf4")))

(define-public crate-elfx86exts-0.4.3 (c (n "elfx86exts") (v "0.4.3") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "capstone") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.21") (d #t) (k 0)))) (h "0s183v1a9zbb3njjndqh3skfs4q1548bnnkrsdwjc968xcd8l5pb")))

(define-public crate-elfx86exts-0.5.0 (c (n "elfx86exts") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "capstone") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.27") (d #t) (k 0)))) (h "0lwjp1wjzdy39sbsxdgv60a817qvx7y6j5qrkdd6gs2qc18swcdq")))

(define-public crate-elfx86exts-0.6.0 (c (n "elfx86exts") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "capstone") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)))) (h "1vi9qanabmw0y3d84l2m8i5jancfbx1irjx0mdk93n1zz348cl0h")))

(define-public crate-elfx86exts-0.6.1 (c (n "elfx86exts") (v "0.6.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "capstone") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)))) (h "0nminvpf8rmf12lvcwvjd2k2vv3sysdj3yzvk5cv87xrmff3vanx")))

(define-public crate-elfx86exts-0.6.2 (c (n "elfx86exts") (v "0.6.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "capstone") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escargot") (r "^0.5") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)))) (h "0a0lrxx1g6z2d2bmvp29r0mhr8c6d0w8xr0yvf1i1vnykllrgdqk")))

