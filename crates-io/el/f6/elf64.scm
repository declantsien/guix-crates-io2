(define-module (crates-io el f6 elf64) #:use-module (crates-io))

(define-public crate-elf64-0.1.0 (c (n "elf64") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1fw96nx1v2crqp9yshichsq2v79siszy3344x964hdfbibnr2g1y")))

(define-public crate-elf64-0.1.1 (c (n "elf64") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "02i51l56b2d9l1p5iicw6b021ckfcva0p0chbdw7dc2wfj87fnm3")))

(define-public crate-elf64-0.1.2 (c (n "elf64") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "097vwfss0zfm2c93myjhkzx25kj1ly61fx1q9w6sys0l7g1aiyl0")))

