(define-module (crates-io el ic elicit) #:use-module (crates-io))

(define-public crate-elicit-0.7.0 (c (n "elicit") (v "0.7.0") (h "0bmgza6j2p9sd1w33sz2cpd8gmc3grpknxj2w3kpsz13qkq3wkds")))

(define-public crate-elicit-0.7.1 (c (n "elicit") (v "0.7.1") (h "1wa81bqri0x3vb6ydyz7m1dxnjm66m0qfax36r3whhq2k28h86rb") (y #t)))

(define-public crate-elicit-0.7.1+nightly (c (n "elicit") (v "0.7.1+nightly") (h "0p04w8401i8lm6fwqz7baqcwjxy4n40b7smi2airxv1j06mh148a") (y #t)))

(define-public crate-elicit-0.7.2 (c (n "elicit") (v "0.7.2") (h "013lnbkb8wkwkqdr9n2r0rzw1fw064ydjl82kcx8m65c09dbgj4a")))

(define-public crate-elicit-0.7.3 (c (n "elicit") (v "0.7.3") (h "0kv7f2zg5sjsm4ymnl58a6qk5nclckb1xpizjqjr2kl98c5mkdvk")))

(define-public crate-elicit-0.7.4 (c (n "elicit") (v "0.7.4") (h "16sb5b6w51s1vsak2bwh9m0dp4bpp9m5dpyciaw6ygpw8i6y0dwg")))

(define-public crate-elicit-0.7.5 (c (n "elicit") (v "0.7.5") (h "0xqgl6cm445pan97s4qzbbqzb32gs9sk852f1lmqdz5sdi61bfrs")))

(define-public crate-elicit-0.7.6 (c (n "elicit") (v "0.7.6") (h "11cpimccfjsh3jz1882nw0zqfzxdxs86vgab71swp9cjjs1rzgm8")))

(define-public crate-elicit-0.7.8 (c (n "elicit") (v "0.7.8") (h "0cj3gggi4ba57v915rcxcnycpmdp3pm53g9gzs2c302y0r4m3j7g")))

(define-public crate-elicit-0.7.10 (c (n "elicit") (v "0.7.10") (h "093vacnpadjsmr4s4241hzccxq9wfjgsiymqm062rnvv57y62dmf")))

(define-public crate-elicit-0.7.11 (c (n "elicit") (v "0.7.11") (h "0zgl1x45vp1ck7blykq361iq0b84g3qjfy7f7in5g1pjhh7g9jnf")))

(define-public crate-elicit-0.8.0 (c (n "elicit") (v "0.8.0") (h "1m6nm77yd2hip5nm04pwpff4gpi1i48pkmqip65h3yiivpk1y3ra")))

(define-public crate-elicit-0.8.1 (c (n "elicit") (v "0.8.1") (h "0x38krn6372yryb8yqnzcl5cb453r2z6358njfy7504ry6f9hdk0")))

(define-public crate-elicit-0.9.0 (c (n "elicit") (v "0.9.0") (d (list (d (n "elicit_macro") (r "^0.1") (k 0)))) (h "1bg80lh51v7v1f5yfbm8qgxyp5ri6lxpdm8r6dvfwmydpj4bahqy")))

(define-public crate-elicit-0.9.1 (c (n "elicit") (v "0.9.1") (d (list (d (n "elicit_macro") (r "^0.1") (k 0)))) (h "01pz02q3x13v2mnb919gdj4zl0hgmvkxcbni7c7vdhi0zpb5gf4a")))

(define-public crate-elicit-0.10.0 (c (n "elicit") (v "0.10.0") (d (list (d (n "elicit_macro") (r "^0.2") (k 0)))) (h "1837w72ncx612n1r9pdixjdq3dmkjxqiwp3hv0pv3hp6qw5hcvv1")))

(define-public crate-elicit-0.11.0 (c (n "elicit") (v "0.11.0") (d (list (d (n "elicit_macro") (r "^0.2") (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (k 0)))) (h "1ln1x55l6ia0djxs8v3n4m63vx129i9d9ls63gmyjn7v7m6ppaf8") (f (quote (("default")))) (s 2) (e (quote (("parking_lot" "elicit_macro/parking_lot" "dep:parking_lot"))))))

