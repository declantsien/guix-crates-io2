(define-module (crates-io el ai elain) #:use-module (crates-io))

(define-public crate-elain-0.1.0 (c (n "elain") (v "0.1.0") (h "1has9v97dy3labwxk6678sfn77mymljb9cmsy8nah14g1kxsga7w")))

(define-public crate-elain-0.2.0 (c (n "elain") (v "0.2.0") (h "00i7iv7gr62zrr5d0lwzrjdyvk1lcmckp1ip7hgf165ysjhiw3bf")))

(define-public crate-elain-0.3.0 (c (n "elain") (v "0.3.0") (h "0wgpyy0m48vdnrip0f4x1h8w4bp2lxgy8pqk78qwhrbxmr7hj8im")))

