(define-module (crates-io el ai elaine) #:use-module (crates-io))

(define-public crate-elaine-0.1.0 (c (n "elaine") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)))) (h "05qmwrsld0q684p4rg6ml3vpmgjip4zgg1p2a2h7j5dqnvpx09s7")))

(define-public crate-elaine-0.1.1 (c (n "elaine") (v "0.1.1") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)))) (h "15vd66cmsp436c1awrspl36g4jz3gdhy3924d25mxqx8g7wj5adw")))

(define-public crate-elaine-0.2.0 (c (n "elaine") (v "0.2.0") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)))) (h "1cniqagmgpdr5p8zxq7h60xvnskcwcmsplfrxdbi2fxmw0qbddbf")))

(define-public crate-elaine-1.0.0 (c (n "elaine") (v "1.0.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 0)))) (h "05cp7m6sk8q6xwxpnp2pc62pmf5xm3pnk4xhyydlkr7az7n5yl22")))

(define-public crate-elaine-1.1.0 (c (n "elaine") (v "1.1.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 0)))) (h "1hzc89v0c8ly4mgjyxkjl1566h9vcghvgsj7jawwra95v1xl66aa")))

(define-public crate-elaine-1.2.0 (c (n "elaine") (v "1.2.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 0)))) (h "0p0mv3v3kkyqxmwnh2xdz2ich5xisil6mws4ffswhwhjfniv81db")))

