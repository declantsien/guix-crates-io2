(define-module (crates-io el ik elikar_scene) #:use-module (crates-io))

(define-public crate-elikar_scene-0.1.0 (c (n "elikar_scene") (v "0.1.0") (d (list (d (n "elikar") (r "^0.1") (d #t) (k 0)) (d (n "gltf") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.16") (d #t) (k 0)) (d (n "xecs") (r "^0.5") (d #t) (k 0)))) (h "0w8248wfpzrpsnxhiymljdlrgs3yqgwvicym7a86akqggym4ddxa")))

