(define-module (crates-io el ik elikoga-ical-rs) #:use-module (crates-io))

(define-public crate-elikoga-ical-rs-0.1.0 (c (n "elikoga-ical-rs") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "174a5m0s22ccdm9z7v0xhf68h0k4sm2r11hyr03hvgw1fc56g611")))

(define-public crate-elikoga-ical-rs-0.1.1 (c (n "elikoga-ical-rs") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rg7fvi97vviz7fh6n73pihc1mw0v8xcdz7x97qvps17f9wb5mni")))

(define-public crate-elikoga-ical-rs-0.2.0 (c (n "elikoga-ical-rs") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ns1c237h785zw9bmdv7cfw67xmma4gljrhggg30r1sljhi1yg76")))

(define-public crate-elikoga-ical-rs-0.2.1 (c (n "elikoga-ical-rs") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1imy46ixx7c8l4vghz4d985pa7vb1418lmxfk6j7j386azj5d44i")))

(define-public crate-elikoga-ical-rs-0.2.2 (c (n "elikoga-ical-rs") (v "0.2.2") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05036rp3bdsvq4cr2462ir9pk92a3kc492pgdyxajlb00vxxks3q")))

(define-public crate-elikoga-ical-rs-0.2.3 (c (n "elikoga-ical-rs") (v "0.2.3") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d9891g4wxld3gnrf2d6ypx6yl7qi1qkgxn868nl9fi9z2acx9n5")))

(define-public crate-elikoga-ical-rs-0.2.4 (c (n "elikoga-ical-rs") (v "0.2.4") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z7fmrbr6pdd849a674chq0cj3cccsk70hf8j7xyn1qwig7i96q6")))

