(define-module (crates-io el ik elikar_egui) #:use-module (crates-io))

(define-public crate-elikar_egui-0.1.0 (c (n "elikar_egui") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.16") (d #t) (k 0)) (d (n "elikar") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "xecs") (r "^0.4") (d #t) (k 0)))) (h "0brai2awxxx2bsnpnxi875widijg4a0hjhbj3hmb42jyfzcbmpxx")))

(define-public crate-elikar_egui-0.1.1 (c (n "elikar_egui") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.16") (d #t) (k 0)) (d (n "elikar") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "xecs") (r "^0.5") (d #t) (k 0)))) (h "0azkhzgfmwamxyx3cisvbrmm70nrprnhlipfn3jm5bhvn77k8x75")))

