(define-module (crates-io el m_ elm_rusty) #:use-module (crates-io))

(define-public crate-elm_rusty-0.1.0 (c (n "elm_rusty") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "10ll0mgcdcpymybwwrjmnswqf7m3ya5720124bnnphbb2l66i9c9")))

