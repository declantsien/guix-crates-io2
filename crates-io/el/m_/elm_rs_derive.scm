(define-module (crates-io el m_ elm_rs_derive) #:use-module (crates-io))

(define-public crate-elm_rs_derive-0.1.0 (c (n "elm_rs_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sy13xwmdcsf2axxbjh7n3n9xwfb0icxhlx4w5z80h98fv3j6sjh") (f (quote (("serde") ("query") ("json")))) (r "1.56")))

(define-public crate-elm_rs_derive-0.2.0 (c (n "elm_rs_derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "15zjdr4yd9ar7dix0k574irpj5l1q8pqff8my17mlq0991mmgsnv") (f (quote (("serde") ("query") ("json") ("default")))) (r "1.56")))

(define-public crate-elm_rs_derive-0.2.1 (c (n "elm_rs_derive") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1k001ckips85yv950g1zdwpl8cgb3ilgmdr3xa33l3qyrffvy4r4") (f (quote (("serde") ("query") ("json") ("default")))) (r "1.56")))

(define-public crate-elm_rs_derive-0.2.2 (c (n "elm_rs_derive") (v "0.2.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1gs06ndwswj98hvr3ajmgb4bdw9gxh187r8a72jmq8g2m9y6agib") (f (quote (("serde") ("query") ("json") ("default")))) (r "1.56")))

