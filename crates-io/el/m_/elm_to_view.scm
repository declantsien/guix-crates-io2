(define-module (crates-io el m_ elm_to_view) #:use-module (crates-io))

(define-public crate-elm_to_view-0.1.0 (c (n "elm_to_view") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ff097qm50gxq88r7gb92kx4paj43khqm9ffdvmd09ymc2n5i24n")))

(define-public crate-elm_to_view-0.2.0 (c (n "elm_to_view") (v "0.2.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hdalcvfwikarjb234l1zkini4hc38y03qyg5qmzw98hr6gr2g84")))

(define-public crate-elm_to_view-0.3.0 (c (n "elm_to_view") (v "0.3.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1k8r0hpn4vh2v4kfi29kp8v44mpmzwx331piv9zqlkwwa2nj3hk5")))

(define-public crate-elm_to_view-0.4.0 (c (n "elm_to_view") (v "0.4.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0l791n8cwa5wfgi2r98jr9f1xaa126qg1li0hf5lnkczk8ci8z4w")))

(define-public crate-elm_to_view-0.5.0 (c (n "elm_to_view") (v "0.5.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "003iqzzxqq28h8g60bhplmxjg17vrzvi2fk4swc762ilhqd7han1")))

(define-public crate-elm_to_view-0.6.0 (c (n "elm_to_view") (v "0.6.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1l4j62iqwc4kb0x9mhaqdxqjc6rwrmkn1wxvp25hyj49j3nm4ynp")))

(define-public crate-elm_to_view-0.6.1 (c (n "elm_to_view") (v "0.6.1") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1isy7jas4av3pmd4sca6mhjzdrvvdqjrs8gjmfp0x0p2m864vyhn")))

(define-public crate-elm_to_view-0.6.2 (c (n "elm_to_view") (v "0.6.2") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17y90cwvh9nipzvlx6prj6r4sjigahil1g2x63jda2qm7m1ff3sk")))

(define-public crate-elm_to_view-0.6.3 (c (n "elm_to_view") (v "0.6.3") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0s6dv0yxl2gy2r3b7nd1lpbyivn7hb85x2v4r7k8h76lad93fvh9")))

(define-public crate-elm_to_view-0.6.4 (c (n "elm_to_view") (v "0.6.4") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "027agk5996x51ji5ixk5vgs74d8dv11cjc0gl426lbv9nc560c1m")))

(define-public crate-elm_to_view-0.6.5 (c (n "elm_to_view") (v "0.6.5") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03n98i9bgvsv78xmyafymj2q3514fbqb3dvz33s69wx0i0n42p23")))

(define-public crate-elm_to_view-0.6.6 (c (n "elm_to_view") (v "0.6.6") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kd5jdz7dvkhzx8w4i6l8i3zrf1gq9rk1lz1p68mclviiafjha78")))

(define-public crate-elm_to_view-0.6.7 (c (n "elm_to_view") (v "0.6.7") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04birf2jsalrkjmabmks2a4mzr806p7xjkpmdr82w98b3hcly74x")))

(define-public crate-elm_to_view-0.7.0 (c (n "elm_to_view") (v "0.7.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "15c5fra5b6f4mkgpjl9z4xd7hdwbc0gdf3hxwqy7arfj1zrbjvk9")))

(define-public crate-elm_to_view-0.7.1 (c (n "elm_to_view") (v "0.7.1") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p01fa27y4af7mfx12xvycda48c2f4g9kfp7a25xla6y80rz68iv")))

(define-public crate-elm_to_view-0.7.2 (c (n "elm_to_view") (v "0.7.2") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ibw60aqcc3clajhwh6g6a4m4r135gv1li1gf0yk0c5jfxn6wgi8")))

(define-public crate-elm_to_view-0.7.3 (c (n "elm_to_view") (v "0.7.3") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08zzlimaqyizzl14h95z4aqcn588zkg4qcjjqb7ybad9lia8vi7c")))

(define-public crate-elm_to_view-0.7.4 (c (n "elm_to_view") (v "0.7.4") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1pag096qnx2lz9k8azyfy7mpxfxnvq7infwbmcyl1qiaccdiaxpv")))

(define-public crate-elm_to_view-0.7.5 (c (n "elm_to_view") (v "0.7.5") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1nznrbak0r5cl0sjfkdi5r6g3w1rms7nfy9pkx1r0is0y60ai3pg")))

(define-public crate-elm_to_view-0.7.6 (c (n "elm_to_view") (v "0.7.6") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lwdw2li1aqharasjzzkbp4bgpv8awlm5l72hsdxrl9v0bslg5gg")))

(define-public crate-elm_to_view-0.7.7 (c (n "elm_to_view") (v "0.7.7") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dfiis7ghvwrfcwnp431s64lhqa77738lq99fkl3fliq7gsm2jc2")))

(define-public crate-elm_to_view-0.7.77 (c (n "elm_to_view") (v "0.7.77") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fckach0xq5ix5c6xs5dj47vmdqjqhbd2qg0bxdxaiznrpvxrd98")))

(define-public crate-elm_to_view-0.7.8 (c (n "elm_to_view") (v "0.7.8") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11m0vr6dz50hb81r18q2ay9ca16d8kf2d8y73633455hgs53j08d")))

(define-public crate-elm_to_view-0.7.9 (c (n "elm_to_view") (v "0.7.9") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wb5i5gaw12qibj17c1wdjpg5dsdnmx5168mgk6glyw8xmhs69hq")))

(define-public crate-elm_to_view-0.7.91 (c (n "elm_to_view") (v "0.7.91") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "100hlcqi3y69r8hk1z3f9l5wy4adambvs91qbyf63z0d2jjj1qny")))

(define-public crate-elm_to_view-0.7.92 (c (n "elm_to_view") (v "0.7.92") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0np3rfpidfr4zs175cinmm4kn0q7wdnlrgqmp67h3lzs2p08gcyn")))

(define-public crate-elm_to_view-0.7.93 (c (n "elm_to_view") (v "0.7.93") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0whjqmd6g79xz02fgrpqj51nskg59wsdb717zrhgq1rikf9m58da")))

(define-public crate-elm_to_view-0.7.94 (c (n "elm_to_view") (v "0.7.94") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08dhjn94qj8fmkf7vp1zi4yrjic66z79wbxk16w4dwhvsq6lz9cn")))

(define-public crate-elm_to_view-0.7.95 (c (n "elm_to_view") (v "0.7.95") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1idsz5dgni2xz79dgwry86h2gf3x30q5if4vlk7371vpsfd1idd0")))

(define-public crate-elm_to_view-0.8.0 (c (n "elm_to_view") (v "0.8.0") (d (list (d (n "leptos") (r "^0.4") (f (quote ("csr"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16wi1fjijfzciyl9hjcjynyk3m5gw68si07f77b71i5kmv43j6v6")))

