(define-module (crates-io el is elise-gc) #:use-module (crates-io))

(define-public crate-elise-gc-0.1.0 (c (n "elise-gc") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "pin-cell") (r "^0.1.1") (d #t) (k 0)))) (h "0lp9b606zh557hfyp4vkxzhs3b2b3h638zrcia4b1zp4cvh5jn5g")))

(define-public crate-elise-gc-0.1.1 (c (n "elise-gc") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "pin-cell") (r "^0.1.1") (d #t) (k 0)))) (h "0s43sg4h5cqd2wmkyvw0l0s5q5b68v9py1hd6iywplmxpcpnnhwz")))

