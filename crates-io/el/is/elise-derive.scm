(define-module (crates-io el is elise-derive) #:use-module (crates-io))

(define-public crate-elise-derive-0.1.0 (c (n "elise-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0i7ak3dg1dnvmiy1pkfak561ycjpaz4wlv5gyc0q021xi6h9qrh2")))

(define-public crate-elise-derive-0.1.1 (c (n "elise-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "0x7ixfdz6n285appb16m22w6ysld4d4zm0idm5hlsyk9djliwx5r")))

