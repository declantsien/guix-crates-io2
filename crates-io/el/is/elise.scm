(define-module (crates-io el is elise) #:use-module (crates-io))

(define-public crate-elise-0.1.0 (c (n "elise") (v "0.1.0") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0) (p "elise-derive")) (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "gc") (r "^0.1.0") (d #t) (k 0) (p "elise-gc")) (d (n "pin-cell") (r "^0.1.1") (d #t) (k 0)))) (h "0brphnm0qdg6fvggzypm249m2ihzr55hgyzkm1r0c03gvwzd4ka7")))

(define-public crate-elise-0.1.1 (c (n "elise") (v "0.1.1") (d (list (d (n "derive") (r "^0.1.1") (d #t) (k 0) (p "elise-derive")) (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "gc") (r "^0.1.1") (d #t) (k 0) (p "elise-gc")) (d (n "pin-cell") (r "^0.1.1") (d #t) (k 0)))) (h "1pq83pcp3jags3r7fis6nqy7njrr9z26scdms8w5vsppcfrlnkb0")))

