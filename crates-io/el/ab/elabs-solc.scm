(define-module (crates-io el ab elabs-solc) #:use-module (crates-io))

(define-public crate-elabs-solc-0.1.0 (c (n "elabs-solc") (v "0.1.0") (h "11pijiq3dzprdzmbqw5ll8dzv7w8xvwv7dkviq51c454wj9bwgmc")))

(define-public crate-elabs-solc-0.1.1 (c (n "elabs-solc") (v "0.1.1") (h "1kykrbgcnsnh1s00d2nngzyql3sz8mv4qv564434knw3iiv6wlsa")))

