(define-module (crates-io el ab elabs-k256) #:use-module (crates-io))

(define-public crate-elabs-k256-0.1.0 (c (n "elabs-k256") (v "0.1.0") (d (list (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0vd3qcbj1kmal5qnq8c4g3j0wgzbwjm3zw7rvc7bps7lbgw6gdj0") (y #t)))

(define-public crate-elabs-k256-0.1.1 (c (n "elabs-k256") (v "0.1.1") (d (list (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "01bb7w8n12hgmax5vkgj42ykmll9w4aq2rzyspb3pp822y9bj8pz")))

