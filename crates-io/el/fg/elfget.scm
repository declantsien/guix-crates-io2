(define-module (crates-io el fg elfget) #:use-module (crates-io))

(define-public crate-elfget-0.1.0 (c (n "elfget") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "14lnn8vp14514ln8k8wlql394cgx75fqk761nnyl0z4hhjpifn6i") (y #t)))

(define-public crate-elfget-0.1.1 (c (n "elfget") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1ygczifw3mlj2nshkhzmnprq98lr7fk8lcnnf25m5xcfifrdrgqi") (y #t)))

(define-public crate-elfget-0.1.3 (c (n "elfget") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "037kwrchc27zfh0cz47wmv57i08p40ix011asjj7vryas3l0f45x")))

