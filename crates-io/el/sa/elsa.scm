(define-module (crates-io el sa elsa) #:use-module (crates-io))

(define-public crate-elsa-0.1.0 (c (n "elsa") (v "0.1.0") (h "09lv3xrhrrsnh1h170x7zhn2x2gwmzilibbij5ayz9nd0425ja4d")))

(define-public crate-elsa-0.1.1 (c (n "elsa") (v "0.1.1") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "076hpd3bi8a2l5wym5f6vscrir3jy6jw7pqbdhxssivglvfb5q89")))

(define-public crate-elsa-0.1.2 (c (n "elsa") (v "0.1.2") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1w6cys257qa71ydpngjj2s8p4v5c2zzj4vfp3702g6jdivvj4phh")))

(define-public crate-elsa-0.1.3 (c (n "elsa") (v "0.1.3") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1n7zyafn75z8xdsl8pqbbkymfk0yv4hr9gdwbwmsfqkry8aqmkbw")))

(define-public crate-elsa-1.0.0 (c (n "elsa") (v "1.0.0") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1nhwhl1mh4217n2capc868f98wd53w04ngavyijkwnh7rh1q0sw9")))

(define-public crate-elsa-1.0.1 (c (n "elsa") (v "1.0.1") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1qa9vydqfdc43nwzkwifhwmhxyjgw8fg2dw05zldm5jc2154wws6")))

(define-public crate-elsa-1.0.2 (c (n "elsa") (v "1.0.2") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "17g490yrk1aqd128jcyxzprlc526hkciahs1n2gws3y51byhr61z")))

(define-public crate-elsa-1.1.0 (c (n "elsa") (v "1.1.0") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "04ch2d4b0kxdm8wm1qxmgvca845j9n2qmb2ynb2laqwny9rayzxp")))

(define-public crate-elsa-1.2.0 (c (n "elsa") (v "1.2.0") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1kgnjr72ngr5aisqilalljwr0zwl91vjgh1d5jnfs9r6dps726l2")))

(define-public crate-elsa-1.2.1 (c (n "elsa") (v "1.2.1") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1p2zqx7s1h60kg7hiyanadb0ckpi4jx0d7ssnnlv1kacmh38rww3")))

(define-public crate-elsa-1.2.2 (c (n "elsa") (v "1.2.2") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1xwadgvcgmiyymdldfj76vp2s8nni3pjkh50g9v0v7ndc3ffrs6w")))

(define-public crate-elsa-1.3.2 (c (n "elsa") (v "1.3.2") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1rvqsv710ss3gnway47xwb7cf5ssrng8mcc64g5qb53whpi15bqd")))

(define-public crate-elsa-1.4.0 (c (n "elsa") (v "1.4.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "19bn30qn87fml954fq8lm8pgg84dcl1fqfxb03f6lx85c8ll90w4")))

(define-public crate-elsa-1.5.0 (c (n "elsa") (v "1.5.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1931mps1zqyza4nzf8qh2d53rq4pq3469881i15hsff3bjxi2yny")))

(define-public crate-elsa-1.6.0 (c (n "elsa") (v "1.6.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1064wpcvsl5b4phf9mcnclw7ck9ml8cx8s0hn27yrdwxjcglb2rl")))

(define-public crate-elsa-1.7.0 (c (n "elsa") (v "1.7.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "02bf031qna560519vz8f0ad8qmlfr55gmaj0hbb4hsbbxlimsjrb")))

(define-public crate-elsa-1.8.0 (c (n "elsa") (v "1.8.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0fs5s2q47pcbrznmwgm1ir8n4rjrhn16j69rd2i9knxfqg1pfh7p")))

(define-public crate-elsa-1.7.1 (c (n "elsa") (v "1.7.1") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1va7kkc8n05xljz7n80njiyy6ckd208abjldwsd4v9xhzcayd3w4")))

(define-public crate-elsa-1.8.1 (c (n "elsa") (v "1.8.1") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "10v2y8mv31jbh9723jfc6m137qsdlsv17gcm0516x1g8vjlarq5m")))

(define-public crate-elsa-1.9.0 (c (n "elsa") (v "1.9.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1501g4abn4g4mh2cf9bwa64s8i9lrhzi7bbn8xz4xd2n6mppckvi")))

(define-public crate-elsa-1.10.0 (c (n "elsa") (v "1.10.0") (d (list (d (n "indexmap") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "041yf5a6mm2kck1hbapwvp39408c4f8cprd2h90j2zgm9np733nr") (f (quote (("default")))) (s 2) (e (quote (("indexmap" "dep:indexmap"))))))

