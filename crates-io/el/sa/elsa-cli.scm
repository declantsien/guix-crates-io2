(define-module (crates-io el sa elsa-cli) #:use-module (crates-io))

(define-public crate-elsa-cli-0.1.0 (c (n "elsa-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1bik6c11m67krzgh170cqnk4i1jaacr8nhgdmhlx7ilfly1r2917") (y #t)))

(define-public crate-elsa-cli-0.2.0 (c (n "elsa-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1n1s042rg5z6ffh0bkvfaxinjqi6rzl4ydjij1ih8lw61rgnl78g")))

(define-public crate-elsa-cli-1.0.0 (c (n "elsa-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "122zri8b88gwvy23l486dayzqzy569ybxl53v5im5kxwwbmpiyfn")))

