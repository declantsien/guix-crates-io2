(define-module (crates-io el ro elrond-wasm-module-users) #:use-module (crates-io))

(define-public crate-elrond-wasm-module-users-0.9.8 (c (n "elrond-wasm-module-users") (v "0.9.8") (d (list (d (n "elrond-wasm") (r ">=0.9.8, <0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r ">=0.9.8, <0.10.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r ">=0.9.8, <0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r ">=0.9.8, <0.10.0") (o #t) (d #t) (k 0)))) (h "0dnifvvsk3gyxh0z4i60iqxz8wiy4ch0ypxgw5q8sc3l7ai353ls") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.0 (c (n "elrond-wasm-module-users") (v "0.10.0") (d (list (d (n "elrond-wasm") (r "^0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1jp02snsq4810m0dl9z0df4pvfg557j4mfaw853qjv25cmjl7lhn") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.1 (c (n "elrond-wasm-module-users") (v "0.10.1") (d (list (d (n "elrond-wasm") (r "^0.10.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1kvfv1qz9ia976kgkfzi90cas13959vxvklwjw22y4yg3mh05qfm") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.2 (c (n "elrond-wasm-module-users") (v "0.10.2") (d (list (d (n "elrond-wasm") (r "^0.10.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "0xzqwkzmy5fqkhacr3212j68m4gdn129szz5im4dwwapilvkpzns") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.3 (c (n "elrond-wasm-module-users") (v "0.10.3") (d (list (d (n "elrond-wasm") (r "^0.10.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "09pm1096432vdxvgpz44011fs9f136vnyrl6m4mins2fz6w4xc2z") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.4 (c (n "elrond-wasm-module-users") (v "0.10.4") (d (list (d (n "elrond-wasm") (r "^0.10.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.4") (o #t) (d #t) (k 0)))) (h "1l9kkn4wv5ff8myh89avywh1bk5zxzrd54dbmafh3584bwsvw0pb") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.10.5 (c (n "elrond-wasm-module-users") (v "0.10.5") (d (list (d (n "elrond-wasm") (r "^0.10.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.5") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.5") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.5") (o #t) (d #t) (k 0)))) (h "16j5fshxll4xlvasjpbnrv78wwvnrc6kd5hz3mj83bnp7dzxih3l") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.11.0 (c (n "elrond-wasm-module-users") (v "0.11.0") (d (list (d (n "elrond-wasm") (r "^0.11.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.11.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0amcgjnyhjfb9yzwzb7k3vvm721a1dvm77g26s1jb8gvlljaqfcp") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.12.0 (c (n "elrond-wasm-module-users") (v "0.12.0") (d (list (d (n "elrond-wasm") (r "^0.12.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.12.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "19623sbnhd1wnkbi0gxrhpk8p7shbv2rzgb7dvibp7v91f88fmj8") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.13.0 (c (n "elrond-wasm-module-users") (v "0.13.0") (d (list (d (n "elrond-wasm") (r "^0.13.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.13.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.13.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1rgvk9fg1zpxs6mhnbzvfjyslygykp8zj2hzjz31sbkk9k2mgjw8") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.14.0 (c (n "elrond-wasm-module-users") (v "0.14.0") (d (list (d (n "elrond-wasm") (r "^0.14.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1i6lbnszhnfwrkiwmph16kiw2zr2xw7w1kg6nmsd7l3gbcfdbvdf") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.14.1 (c (n "elrond-wasm-module-users") (v "0.14.1") (d (list (d (n "elrond-wasm") (r "^0.14.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0rivh6bq1m5xy065m7hxcx7097n7m175ayzq19m716vn2bljvvl9") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.14.2 (c (n "elrond-wasm-module-users") (v "0.14.2") (d (list (d (n "elrond-wasm") (r "^0.14.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.2") (o #t) (d #t) (k 0)))) (h "0w5463zgnd5wkdnbvfxk54fyvk60i7bv8lxxp2jxl241qsrkcjnj") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.15.0 (c (n "elrond-wasm-module-users") (v "0.15.0") (d (list (d (n "elrond-wasm") (r "^0.15.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.15.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.15.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "1hi93x4v9gs8rr4h6585gfmm78gyrkd1c0is3sfsa2dvfijj72kx") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.15.1 (c (n "elrond-wasm-module-users") (v "0.15.1") (d (list (d (n "elrond-wasm") (r "^0.15.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.15.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.15.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "1i2n1x1jcnf6gmjq07j10h1pmcc0i2mq43pszaazslrnbzk9fvmq") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.16.0 (c (n "elrond-wasm-module-users") (v "0.16.0") (d (list (d (n "elrond-wasm") (r "^0.16.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1jkkxzj8jrzn7mc3c79npr0v5v5qivhcfkvjv63wnvdva96pcxk8") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.16.1 (c (n "elrond-wasm-module-users") (v "0.16.1") (d (list (d (n "elrond-wasm") (r "^0.16.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "0mjl4r8hnhyjq0x79hh7jdizwbzfa416s4d6zgpr0fxwq6rphwfv") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.16.2 (c (n "elrond-wasm-module-users") (v "0.16.2") (d (list (d (n "elrond-wasm") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.2") (o #t) (d #t) (k 0)))) (h "0pm293lxs6wr29c9306j5yq5csc4r7d3k1n18vgw7f5qsn37p5ik") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.17.0 (c (n "elrond-wasm-module-users") (v "0.17.0") (d (list (d (n "elrond-wasm") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "1si6s30w1sbyw6s3y3s5pwjd1vfs12nmw39qjirx7fvbfzxpplls") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.17.1 (c (n "elrond-wasm-module-users") (v "0.17.1") (d (list (d (n "elrond-wasm") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.1") (o #t) (d #t) (k 0)))) (h "05dxhaz7xw6pybigsp2i5741b7wilx8kdx5cy0qdnf4yswa6h7im") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.17.2 (c (n "elrond-wasm-module-users") (v "0.17.2") (d (list (d (n "elrond-wasm") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.2") (o #t) (d #t) (k 0)))) (h "0hyw152s0dmbrzspsvd0w2gqgr51q6zz7233fwx2v514lq9b5j7l") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.17.3 (c (n "elrond-wasm-module-users") (v "0.17.3") (d (list (d (n "elrond-wasm") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.3") (o #t) (d #t) (k 0)))) (h "1h53ywgl38ckgj1bkjrxqcac970dwkdbjmrhi14lcg01h9s4c5si") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.17.4 (c (n "elrond-wasm-module-users") (v "0.17.4") (d (list (d (n "elrond-wasm") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.4") (o #t) (d #t) (k 0)))) (h "179147713k3v270zghcjspda268h4qvzqhhbm4gnayjyygqqm5pn") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.18.0 (c (n "elrond-wasm-module-users") (v "0.18.0") (d (list (d (n "elrond-wasm") (r "^0.18.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0xhj2yfdip998bjhl0ddq82f4vx4sv4zzk0afj24d3abnn22d8nf") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.18.1 (c (n "elrond-wasm-module-users") (v "0.18.1") (d (list (d (n "elrond-wasm") (r "^0.18.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "1y45n5bi55rcmqv1jwpv78x0mqz9zwp8mkp1gqpmayvz67nrkhkd") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.18.2 (c (n "elrond-wasm-module-users") (v "0.18.2") (d (list (d (n "elrond-wasm") (r "^0.18.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.2") (o #t) (d #t) (k 0)))) (h "17rgkpiz9wb70xv8p9w1lsbz5wdv4795gcg4qscxsnrhhh981f6r") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.19.0 (c (n "elrond-wasm-module-users") (v "0.19.0") (d (list (d (n "elrond-wasm") (r "^0.19.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "038ld7nk102mfkjl19nacsnz6m04gr6gipqkdr4dh9j82148f7lm") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.19.1 (c (n "elrond-wasm-module-users") (v "0.19.1") (d (list (d (n "elrond-wasm") (r "^0.19.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1y4fl1f3znr6i8zlckrsxav0wzzjybi47q5k10v705wp2611yqc7") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.20.0 (c (n "elrond-wasm-module-users") (v "0.20.0") (d (list (d (n "elrond-wasm") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "11zh7cra4jx5vq973gqyjwdc58pr94ialpy317n6vhix3jzhlp6y") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.20.1 (c (n "elrond-wasm-module-users") (v "0.20.1") (d (list (d (n "elrond-wasm") (r "^0.20.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "1cdlxzgidm0h3dpiyr39cfk02cb96yyissqm86f8rl3v7frvxv7z") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.21.0 (c (n "elrond-wasm-module-users") (v "0.21.0") (d (list (d (n "elrond-wasm") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "1zaygazgz8b0pr3l6k8pwda7q59m6ffvyaa1crf6a1q2l6ias6p2") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.21.1 (c (n "elrond-wasm-module-users") (v "0.21.1") (d (list (d (n "elrond-wasm") (r "^0.21.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "0rm9xryn8ga6779vy8nlyqz1plc5dsr3vi21r35vx647wwga9a3w") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.21.2 (c (n "elrond-wasm-module-users") (v "0.21.2") (d (list (d (n "elrond-wasm") (r "^0.21.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.2") (o #t) (d #t) (k 0)))) (h "1q8521sz6yz31vij8fnxh1kwcsm1mi8519spakwbvc6w4bdqg5r0") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-users-0.22.0 (c (n "elrond-wasm-module-users") (v "0.22.0") (d (list (d (n "elrond-wasm") (r "^0.22.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.0") (d #t) (k 2)))) (h "0dvpl5hvnp12kvadvwyf9fyqbgpyy29y31wb5kf8rvddcmvrd2m1")))

(define-public crate-elrond-wasm-module-users-0.22.1 (c (n "elrond-wasm-module-users") (v "0.22.1") (d (list (d (n "elrond-wasm") (r "^0.22.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.1") (d #t) (k 2)))) (h "0jrnhgzrq0kcgygl2cwflcmh3j7vkkvhngsgbpq6bb4mdb61w676")))

(define-public crate-elrond-wasm-module-users-0.22.2 (c (n "elrond-wasm-module-users") (v "0.22.2") (d (list (d (n "elrond-wasm") (r "^0.22.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.2") (d #t) (k 2)))) (h "15g9l7cn1dd8b6g0937mbyfv1dgr6rrkzdjhb9caz71753m3jz1s")))

(define-public crate-elrond-wasm-module-users-0.22.3 (c (n "elrond-wasm-module-users") (v "0.22.3") (d (list (d (n "elrond-wasm") (r "^0.22.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.3") (d #t) (k 2)))) (h "0k4ypl8v7asmj48hlxndiwba74gf75zk0x6gazaza1s9ap9mg57x")))

(define-public crate-elrond-wasm-module-users-0.22.4 (c (n "elrond-wasm-module-users") (v "0.22.4") (d (list (d (n "elrond-wasm") (r "^0.22.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.4") (d #t) (k 2)))) (h "12w2ca7mhwdsyb3jdgkvn30jwbavszsvaj6vxphbx7j31xva67js")))

(define-public crate-elrond-wasm-module-users-0.22.5 (c (n "elrond-wasm-module-users") (v "0.22.5") (d (list (d (n "elrond-wasm") (r "^0.22.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.5") (d #t) (k 2)))) (h "0fc7h86gwyjkgr0vncycy3axq395xcgbi0bwgqckssxx38f0anvr")))

(define-public crate-elrond-wasm-module-users-0.22.6 (c (n "elrond-wasm-module-users") (v "0.22.6") (d (list (d (n "elrond-wasm") (r "^0.22.6") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.6") (d #t) (k 2)))) (h "0ysdiadj0xd3rrd2m6llqmb0ah65zdw4qp8zfvqzwlyh1wk11mlb")))

(define-public crate-elrond-wasm-module-users-0.22.7 (c (n "elrond-wasm-module-users") (v "0.22.7") (d (list (d (n "elrond-wasm") (r "^0.22.7") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.7") (d #t) (k 2)))) (h "0lapacxi6454wd6r0864w5fk14q01hp5w2nw5y091rqhqi9qikkx")))

(define-public crate-elrond-wasm-module-users-0.22.8 (c (n "elrond-wasm-module-users") (v "0.22.8") (d (list (d (n "elrond-wasm") (r "^0.22.8") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.8") (d #t) (k 2)))) (h "0lywb4yyyi4mhxp5ykkpp3ay3jbyb10m9m4j9g7kdqfiy0m01iwg")))

(define-public crate-elrond-wasm-module-users-0.22.9 (c (n "elrond-wasm-module-users") (v "0.22.9") (d (list (d (n "elrond-wasm") (r "^0.22.9") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.9") (d #t) (k 2)))) (h "0y8zy95gdi9k0kbv13bccnzsg74agnprjn210vqy33mr3mjfsr69")))

(define-public crate-elrond-wasm-module-users-0.22.10 (c (n "elrond-wasm-module-users") (v "0.22.10") (d (list (d (n "elrond-wasm") (r "^0.22.10") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.10") (d #t) (k 2)))) (h "1ncvpjbs8q6jr2h0b4wrdpvl03gnsblmywvzjsvbkmy8vv7pyxlf")))

(define-public crate-elrond-wasm-module-users-0.22.11 (c (n "elrond-wasm-module-users") (v "0.22.11") (d (list (d (n "elrond-wasm") (r "^0.22.11") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.11") (d #t) (k 2)))) (h "0cjc06v0ifzmmx95n2b4xpzfp37b0nci2vrs596axhvyb6m810c8")))

(define-public crate-elrond-wasm-module-users-0.23.0 (c (n "elrond-wasm-module-users") (v "0.23.0") (d (list (d (n "elrond-wasm") (r "^0.23.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.0") (d #t) (k 2)))) (h "006bir6c92zxrgrw697bdm771bzd3ic08afk7j6nqras0599pshv")))

(define-public crate-elrond-wasm-module-users-0.23.1 (c (n "elrond-wasm-module-users") (v "0.23.1") (d (list (d (n "elrond-wasm") (r "^0.23.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.1") (d #t) (k 2)))) (h "12q058azk704jc9ff3pa8ilv4f0av884rdjsjasmmq6qvgj0xi4k")))

(define-public crate-elrond-wasm-module-users-0.24.0 (c (n "elrond-wasm-module-users") (v "0.24.0") (d (list (d (n "elrond-wasm") (r "^0.24.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.24.0") (d #t) (k 2)))) (h "0133cw7ihn2wkkbirf69ps2r4z5c5sv96hfgh0jz2rbxdjca49fq")))

(define-public crate-elrond-wasm-module-users-0.25.0 (c (n "elrond-wasm-module-users") (v "0.25.0") (d (list (d (n "elrond-wasm") (r "^0.25.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.25.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.25.0") (d #t) (k 0)))) (h "0qcz97g60n40yyyhj06z5rllxyqvn90y2jg0wvz7y6fra71332rh")))

