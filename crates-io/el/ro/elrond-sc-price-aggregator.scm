(define-module (crates-io el ro elrond-sc-price-aggregator) #:use-module (crates-io))

(define-public crate-elrond-sc-price-aggregator-0.34.1 (c (n "elrond-sc-price-aggregator") (v "0.34.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.34.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.34.1") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.34.1") (d #t) (k 0)))) (h "1dblnm99ibkpgwgrkpibxpvjk182gxrkgssfaj7gfny6pnb8rcn3")))

(define-public crate-elrond-sc-price-aggregator-0.35.0 (c (n "elrond-sc-price-aggregator") (v "0.35.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.35.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.35.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.35.0") (d #t) (k 0)))) (h "0xwqwsbghj29hbz0w697bwnygh4fq9psbivj35c7fxr69nfnxdyr")))

(define-public crate-elrond-sc-price-aggregator-0.36.0 (c (n "elrond-sc-price-aggregator") (v "0.36.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.36.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.36.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.36.0") (d #t) (k 0)))) (h "0ivdcljq3srmp9ciyc2qzlvify8nlfkn2lscm4z5vb5g28wl1v00")))

(define-public crate-elrond-sc-price-aggregator-0.36.1 (c (n "elrond-sc-price-aggregator") (v "0.36.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.36.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.36.1") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.36.1") (d #t) (k 0)))) (h "1n6nkwiw8h7gz252czpvm46cfknms13242gjfbbjz0d0i8yb1qhs")))

(define-public crate-elrond-sc-price-aggregator-0.37.0 (c (n "elrond-sc-price-aggregator") (v "0.37.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.37.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.37.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.37.0") (d #t) (k 0)))) (h "1pjdsfxf7wp6l3y77g2841nzpnqgq4zs0v7chxmdvbmk13d0yxvn")))

(define-public crate-elrond-sc-price-aggregator-0.38.0 (c (n "elrond-sc-price-aggregator") (v "0.38.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.38.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.38.0") (d #t) (k 0)))) (h "1agylmpzxm3zkskpk65w1sw4aszifc1zbyspvx98xaybjn72ksbx")))

