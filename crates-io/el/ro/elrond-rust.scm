(define-module (crates-io el ro elrond-rust) #:use-module (crates-io))

(define-public crate-elrond-rust-0.1.0 (c (n "elrond-rust") (v "0.1.0") (d (list (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.2.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "ureq") (r "^1.4.1") (f (quote ("json"))) (d #t) (k 0)))) (h "19avpzxxrh06n8avb0f19kbjd9bz41w60w0faig9fww8xc30bbia")))

