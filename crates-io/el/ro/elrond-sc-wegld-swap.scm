(define-module (crates-io el ro elrond-sc-wegld-swap) #:use-module (crates-io))

(define-public crate-elrond-sc-wegld-swap-0.34.1 (c (n "elrond-sc-wegld-swap") (v "0.34.1") (d (list (d (n "elrond-wasm") (r "^0.34.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.34.1") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.34.1") (d #t) (k 0)))) (h "0rvq4whzrzsd0qs5dzad6zy00msxrkc8s1v8xi6vv25i7n5girxi")))

(define-public crate-elrond-sc-wegld-swap-0.35.0 (c (n "elrond-sc-wegld-swap") (v "0.35.0") (d (list (d (n "elrond-wasm") (r "^0.35.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.35.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.35.0") (d #t) (k 0)))) (h "04szfchi581k7ww3s49s5ghiajq41cma64nb5fl2raysirjy6836")))

(define-public crate-elrond-sc-wegld-swap-0.36.0 (c (n "elrond-sc-wegld-swap") (v "0.36.0") (d (list (d (n "elrond-wasm") (r "^0.36.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.36.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.36.0") (d #t) (k 0)))) (h "17mcsaccbd1rzr9n18vavvzpp5ckf2banlk8hcnfq3x13bzy57qg")))

(define-public crate-elrond-sc-wegld-swap-0.36.1 (c (n "elrond-sc-wegld-swap") (v "0.36.1") (d (list (d (n "elrond-wasm") (r "^0.36.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.36.1") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.36.1") (d #t) (k 0)))) (h "1imki5gixammndv12zf7b2bldbr478bzw66kqjsiq70pnccpzjdv")))

(define-public crate-elrond-sc-wegld-swap-0.37.0 (c (n "elrond-sc-wegld-swap") (v "0.37.0") (d (list (d (n "elrond-wasm") (r "^0.37.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.37.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.37.0") (d #t) (k 0)))) (h "0rr5b9b6g6dcal3zd4yvjl33r919zjskga5i8hwggrjnw8i766bv")))

(define-public crate-elrond-sc-wegld-swap-0.38.0 (c (n "elrond-sc-wegld-swap") (v "0.38.0") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.38.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.38.0") (d #t) (k 0)))) (h "14bs1j6dmbb2znpzjkvlrr9ss537jps6srd6vk8cmp7v6sa1pq4r")))

