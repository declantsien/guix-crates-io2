(define-module (crates-io el ro elrond-codec) #:use-module (crates-io))

(define-public crate-elrond-codec-0.1.0 (c (n "elrond-codec") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "190wkamv64mkjc74h0smik8z4xb2ma0f1ywmjm1arlzrj0z6s1nb")))

(define-public crate-elrond-codec-0.1.1 (c (n "elrond-codec") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0qsbg8q8r3fh05ffjg97f6gvrkgkcz8fc505nc3zdn174mkxdmi3")))

(define-public crate-elrond-codec-0.1.2 (c (n "elrond-codec") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "14pvzzpnlp29m3zdm5zfvh48fh0s34lq9qgn1zjn199cm8af06l5")))

(define-public crate-elrond-codec-0.1.3 (c (n "elrond-codec") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0f5nj08nk0wcli32g82qixfihn8pzif4vh6qr4hxjahksv918kap")))

(define-public crate-elrond-codec-0.2.0 (c (n "elrond-codec") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0yimls5qjvr92slyxhj7187vnghqxycl5489ncmnapx29smf0r05")))

(define-public crate-elrond-codec-0.3.0 (c (n "elrond-codec") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "146qk7zfj7y4hg2nnigawv7ib7fmbi2lsypyx7dgxra3aixh8dwb")))

(define-public crate-elrond-codec-0.3.1 (c (n "elrond-codec") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1sykj0f8iqi1z69rqwwygmsa1gdx1v3km5k8nn81waxblf0hsjn6")))

(define-public crate-elrond-codec-0.3.2 (c (n "elrond-codec") (v "0.3.2") (d (list (d (n "arrayvec") (r ">=0.5.1, <0.6.0") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "wee_alloc") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "0ixsnk0c1540phfzjsibxrccqb6499bgdra4dx6r3hyka9nr82xj")))

(define-public crate-elrond-codec-0.4.0 (c (n "elrond-codec") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hr0j98py5gg3id9xynl19pwaj1wki1ivblxybyp19w3mdxpfwfs") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.4.1 (c (n "elrond-codec") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hrxrfh1fmy4kz475q3r91vgh51la9a7nzwjsd39wvywb6cnl5vy") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.4.2 (c (n "elrond-codec") (v "0.4.2") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0fxmfzqdinb5d6cjirchshzbqppzlircg1kh3bg2ax3q98dn720j") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.5.0 (c (n "elrond-codec") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0qgbhfm4wh3c8gqahp0h9fa7xg9inprcsqmsqaxycccggryrhyy1") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.5.1 (c (n "elrond-codec") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.1") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0xnrc559k5zwvwccgfn5lf1740841zsdwi8m10nx09fll59ixnb8") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.5.2 (c (n "elrond-codec") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1j4gjznqs7rb78hqja9yr9mpa38nllsvicgc1gmq4lw9xjy1gk84") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.5.3 (c (n "elrond-codec") (v "0.5.3") (d (list (d (n "arrayvec") (r "^0.5.2") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "^0.5.3") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0a77f6p1gy6rp2mfj981s00p0jxi8pw8shfmg7p5f7lrd5gwq995") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.6.0 (c (n "elrond-codec") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.6.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.6.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0rr2sy9wxglqfdhbl7yfh8kpadmydznd44jnfn8n22z8634j6ajz") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.7.0 (c (n "elrond-codec") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.7.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.7.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1zi9k2qz0bhghqyrw5mmgk7adzhhil8q5g6vk1dwbyrgdh0caa58") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.8.0 (c (n "elrond-codec") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zkd3gh4pr7b5hdf51msrsbpd5srp55sswxyqzax4h3gjkzzdhxr") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.8.1 (c (n "elrond-codec") (v "0.8.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.1") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.1") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "13s6lsbfg6gfjk0x72w0nhah480y6md9aila3hmy3zqix142n29i") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.8.2 (c (n "elrond-codec") (v "0.8.2") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.2") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1z84mqjlxpf3d8sirmi7r4sgsjihg1iwh1pvnqdxabvinwnqq2i1") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.8.3 (c (n "elrond-codec") (v "0.8.3") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.3") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.3") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "16ybdqfqarx0xwfq7j6187dprh8km8alzsmqnzvhywjz1yn93iv2") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.8.4 (c (n "elrond-codec") (v "0.8.4") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.4") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.4") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "170qgi7iwymapr4w9arn7vvkw9w586nh7zb50if94q211nw2j0jq") (f (quote (("derive" "elrond-codec-derive")))) (y #t)))

(define-public crate-elrond-codec-0.8.5 (c (n "elrond-codec") (v "0.8.5") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.8.5") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.8.5") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1wqzg1jqznbi01a8svpx7qajb0g3wgh440m5a3az76zljc346d7k") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.9.0 (c (n "elrond-codec") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.9.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.9.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hb398ri3c7bnbkllbqf5ggx2nczfngrhyhm4vd156lnghmmzn6n") (f (quote (("derive" "elrond-codec-derive"))))))

(define-public crate-elrond-codec-0.10.0 (c (n "elrond-codec") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.10.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.10.0") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1rkwy6ddbqynq5a50hjnl8bxj27kybncjffbl33bfcc4gnkps15c") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.11.0 (c (n "elrond-codec") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.11.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.11.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "00py3s2z6km7lb1cb4q6q9i6nlq7lz0h959x7fyqn9zan77abz0l") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.12.0 (c (n "elrond-codec") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.12.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.12.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1sbfh7hg6m03c9n68m1930rziqibf6a3dxh04p8g8mxaifcgdfvg") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.13.0 (c (n "elrond-codec") (v "0.13.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.13.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.13.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0djlbr9cb2l4fjssf501ybwm4crqihdj9sdr30w559diijz4j55x") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.14.0 (c (n "elrond-codec") (v "0.14.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.14.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.14.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0iswg16ww6imacgxxp3x27zn8b7jn36flb6zw4b4ipb432awb19c") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.15.0 (c (n "elrond-codec") (v "0.15.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.15.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.15.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0k2494ylkpbp5gvd5c4gahhg9wj47if33z5ilp1b6jchnzk6hyb8") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

(define-public crate-elrond-codec-0.16.0 (c (n "elrond-codec") (v "0.16.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "elrond-codec-derive") (r "=0.16.0") (o #t) (d #t) (k 0)) (d (n "elrond-codec-derive") (r "=0.16.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "098l3r4bpx699mfgcjs41w2nvd49763975vhxq2qd3vsmn516ks6") (f (quote (("derive" "elrond-codec-derive") ("alloc"))))))

