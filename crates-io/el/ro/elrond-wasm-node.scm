(define-module (crates-io el ro elrond-wasm-node) #:use-module (crates-io))

(define-public crate-elrond-wasm-node-0.1.0 (c (n "elrond-wasm-node") (v "0.1.0") (d (list (d (n "elrond-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hw95h4sl2l3qz4dsxqr5jzdsgg83fzxbn1md0zyzyd3assws56w")))

(define-public crate-elrond-wasm-node-0.1.1 (c (n "elrond-wasm-node") (v "0.1.1") (d (list (d (n "elrond-wasm") (r "^0.1.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0a8cj1j61dzjsaw2bjx2fwp5zv3s9mlnq1cyl4ncihvknq2cgxql")))

(define-public crate-elrond-wasm-node-0.2.0 (c (n "elrond-wasm-node") (v "0.2.0") (d (list (d (n "elrond-wasm") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0cw15ysf31xkwrknhjl3mf9q03xysdaav282pwp7w9wp4w9kq2f0")))

(define-public crate-elrond-wasm-node-0.3.0 (c (n "elrond-wasm-node") (v "0.3.0") (d (list (d (n "elrond-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "05xygydpvdr929yrvza03bw8jrsn13y0dfahxv8kv3mz3qsphdzv")))

(define-public crate-elrond-wasm-node-0.3.2 (c (n "elrond-wasm-node") (v "0.3.2") (d (list (d (n "elrond-wasm") (r "^0.3.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1bmcs2v7sgxxv3yps4hvxgwz1lykn0c5qxs689l14f9pbgrml2xx")))

(define-public crate-elrond-wasm-node-0.4.0 (c (n "elrond-wasm-node") (v "0.4.0") (d (list (d (n "elrond-wasm") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "08dhknww9fdvbmdb07v1z9a8km1nfimga45wifbgj29f0paz8sag")))

(define-public crate-elrond-wasm-node-0.4.1 (c (n "elrond-wasm-node") (v "0.4.1") (d (list (d (n "elrond-wasm") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "09aps6wv4payfb1nawdqvwf9vp75njwddqdqr2fi2pyd6bqmy4df")))

(define-public crate-elrond-wasm-node-0.4.2 (c (n "elrond-wasm-node") (v "0.4.2") (d (list (d (n "elrond-wasm") (r "^0.4.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0xf18xcpnkld3gqpf57zks5rwy3xsydqig1zy8npjipawly6y6lc")))

(define-public crate-elrond-wasm-node-0.4.3 (c (n "elrond-wasm-node") (v "0.4.3") (d (list (d (n "elrond-wasm") (r "^0.4.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0dm6v5ny07fy9g40xn9da6lb8166mazv9n615vkn2x24xc4d259f")))

(define-public crate-elrond-wasm-node-0.4.4 (c (n "elrond-wasm-node") (v "0.4.4") (d (list (d (n "elrond-wasm") (r "^0.4.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1gh44kwdc813bk42gq5g69jb20lim4pf7fh5cshvy679bypxpcvl")))

(define-public crate-elrond-wasm-node-0.4.5 (c (n "elrond-wasm-node") (v "0.4.5") (d (list (d (n "elrond-wasm") (r "^0.4.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0191r5p7nwzq1p81k2c1dqf1843l3mz8is1kg2y5x5afw0lgafd4")))

(define-public crate-elrond-wasm-node-0.4.6 (c (n "elrond-wasm-node") (v "0.4.6") (d (list (d (n "elrond-wasm") (r "^0.4.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "06233s27i57lky9ddjkh2vmsjjcacx31wrzdg566az14dh24ld5q")))

(define-public crate-elrond-wasm-node-0.5.0 (c (n "elrond-wasm-node") (v "0.5.0") (d (list (d (n "elrond-wasm") (r "^0.5.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0w4jlhf4xw1fs6n1n24r1kv6lkpsyvhdhpgl4k4bl3ba60pli01d")))

(define-public crate-elrond-wasm-node-0.5.1 (c (n "elrond-wasm-node") (v "0.5.1") (d (list (d (n "elrond-wasm") (r "^0.5.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0aa038rb6clmb4s153g8a1wr8n25hnv1mzgg3xrkly2fhjasc0b5")))

(define-public crate-elrond-wasm-node-0.5.2 (c (n "elrond-wasm-node") (v "0.5.2") (d (list (d (n "elrond-wasm") (r "^0.5.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "03nghhd7112canf5lz3f57gqycw21wbxbrkbqsfsi3sz5f7yvb37")))

(define-public crate-elrond-wasm-node-0.5.3 (c (n "elrond-wasm-node") (v "0.5.3") (d (list (d (n "elrond-wasm") (r "^0.5.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "009903ck6knhk23sh7xp0n2h4gbcgx3y6fgng8pwsp5bcbs07caj")))

(define-public crate-elrond-wasm-node-0.5.4 (c (n "elrond-wasm-node") (v "0.5.4") (d (list (d (n "elrond-wasm") (r "^0.5.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0wys8sc6avsg40fv74h4qs1gp7mmn6qglnxwjbclq6ppb81hxnng")))

(define-public crate-elrond-wasm-node-0.5.5 (c (n "elrond-wasm-node") (v "0.5.5") (d (list (d (n "elrond-wasm") (r "^0.5.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0pwz5hxwwxzwhlxinw108423xx2alp0ixgm9hybbx3za1b6c1f53")))

(define-public crate-elrond-wasm-node-0.6.0 (c (n "elrond-wasm-node") (v "0.6.0") (d (list (d (n "elrond-wasm") (r "^0.6.0") (d #t) (k 0)))) (h "1nv8291wzwn6y3jfagqimlar2plilxj3r529q5xvk8wmx53absky")))

(define-public crate-elrond-wasm-node-0.6.1 (c (n "elrond-wasm-node") (v "0.6.1") (d (list (d (n "elrond-wasm") (r "^0.6.1") (d #t) (k 0)))) (h "07z0y9phnkl2l23y74146i10qvdwp4wxhmi54gn3q8r08bf9sdhy")))

(define-public crate-elrond-wasm-node-0.6.2 (c (n "elrond-wasm-node") (v "0.6.2") (d (list (d (n "elrond-wasm") (r "^0.6.2") (d #t) (k 0)))) (h "1wnxpfyw02h0mzccx31l8810kcbxfk3zjvb42d5hlhyn7zdm0ipp")))

(define-public crate-elrond-wasm-node-0.7.0 (c (n "elrond-wasm-node") (v "0.7.0") (d (list (d (n "elrond-wasm") (r "^0.7.0") (d #t) (k 0)))) (h "1nxnc5jnsvpr4iasvp909cibcmn3xiva23wzknkkjddn7lm0dlak")))

(define-public crate-elrond-wasm-node-0.7.1 (c (n "elrond-wasm-node") (v "0.7.1") (d (list (d (n "elrond-wasm") (r "^0.7.1") (d #t) (k 0)))) (h "0invmbcnl4p05jy7a5yj3dsy8912hxnq2nfimxx52yd1q75aimz0")))

(define-public crate-elrond-wasm-node-0.7.2 (c (n "elrond-wasm-node") (v "0.7.2") (d (list (d (n "elrond-wasm") (r "^0.7.2") (d #t) (k 0)))) (h "104pk5cks9sgg9ncpnid23y6wfvaj0ns3yk305asfdp3zqqlh3m1")))

(define-public crate-elrond-wasm-node-0.8.0 (c (n "elrond-wasm-node") (v "0.8.0") (d (list (d (n "elrond-wasm") (r "^0.8.0") (d #t) (k 0)))) (h "0d3qwwzb75a23j7k403lcnmq96b0gmcbagks5d3rsf3g16j4m5xc") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.0 (c (n "elrond-wasm-node") (v "0.9.0") (d (list (d (n "elrond-wasm") (r "^0.9.0") (d #t) (k 0)))) (h "0llixqi6sxks7wc8ds5vn8lxz3sxi2xbifidssbkcx8i3xwp27fq") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.1 (c (n "elrond-wasm-node") (v "0.9.1") (d (list (d (n "elrond-wasm") (r "^0.9.1") (d #t) (k 0)))) (h "0ci10c7vrwzgai96a50n8xk4yi6a1wqa7j6684a3hmkn2ni2j3wz") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.2 (c (n "elrond-wasm-node") (v "0.9.2") (d (list (d (n "elrond-wasm") (r "^0.9.2") (d #t) (k 0)))) (h "1a3s11ds939cw9q3lrrifnlc71cj4lfcj61wb43hy3acw631al4d") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.3 (c (n "elrond-wasm-node") (v "0.9.3") (d (list (d (n "elrond-wasm") (r "^0.9.3") (d #t) (k 0)))) (h "1ffk3z3pfjzjz4n2li1jfxihi7s9icli786caz79dgr0wych2y11") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.4 (c (n "elrond-wasm-node") (v "0.9.4") (d (list (d (n "elrond-wasm") (r "^0.9.4") (d #t) (k 0)))) (h "107v7mag8x8bwxn3caa1zn8ipq2jgxhgms3vlccd4kcd6qx92928") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.5 (c (n "elrond-wasm-node") (v "0.9.5") (d (list (d (n "elrond-wasm") (r "^0.9.5") (d #t) (k 0)))) (h "0l5yb1assnmv6rsn824g2xzgn3x3bf0yydv0q848g9hy1r86pmih") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.6 (c (n "elrond-wasm-node") (v "0.9.6") (d (list (d (n "elrond-wasm") (r "^0.9.6") (d #t) (k 0)))) (h "0z1z362l3kbxxkd9y978h3nn8qi9hiay2fg72pp1hc56w7hj4hlg") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.7 (c (n "elrond-wasm-node") (v "0.9.7") (d (list (d (n "elrond-wasm") (r "^0.9.7") (d #t) (k 0)))) (h "1dc89rzzbvmiyiy17fmmfb7v7wwirhlpw1q70kh9ax0w4nyir3j4") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.9.8 (c (n "elrond-wasm-node") (v "0.9.8") (d (list (d (n "elrond-wasm") (r ">=0.9.8, <0.10.0") (d #t) (k 0)))) (h "18awlh4as2r8mi2v708r7w63mgnb91rwzaivk9lc8b08rxadkrcs") (f (quote (("small-int-ei"))))))

(define-public crate-elrond-wasm-node-0.10.0 (c (n "elrond-wasm-node") (v "0.10.0") (d (list (d (n "elrond-wasm") (r "^0.10.0") (d #t) (k 0)))) (h "1ql22pzsrd3cwkan2r1a30akmapm4p1avn69i4nri4c1xn19xvb1")))

(define-public crate-elrond-wasm-node-0.10.1 (c (n "elrond-wasm-node") (v "0.10.1") (d (list (d (n "elrond-wasm") (r "^0.10.1") (d #t) (k 0)))) (h "17hq6wfhzw5hylmb77qazpmbpcwv06zjy46csqqc7y5bagsgyznn")))

(define-public crate-elrond-wasm-node-0.10.2 (c (n "elrond-wasm-node") (v "0.10.2") (d (list (d (n "elrond-wasm") (r "^0.10.2") (d #t) (k 0)))) (h "1006dzzp5il91r06rf5hjb7ad35z6cg0xg6y0hypml6nszrlx996")))

(define-public crate-elrond-wasm-node-0.10.3 (c (n "elrond-wasm-node") (v "0.10.3") (d (list (d (n "elrond-wasm") (r "^0.10.3") (d #t) (k 0)))) (h "0mk8if53pg1y03q96gc2zn8wsxs1rgbdfc29axl6z3aznkniipxj")))

(define-public crate-elrond-wasm-node-0.10.4 (c (n "elrond-wasm-node") (v "0.10.4") (d (list (d (n "elrond-wasm") (r "^0.10.4") (d #t) (k 0)))) (h "18c4mf2174diyxqbnmjynjg5d24ffv0vpv3a88nfa4mi6skl5h84")))

(define-public crate-elrond-wasm-node-0.10.5 (c (n "elrond-wasm-node") (v "0.10.5") (d (list (d (n "elrond-wasm") (r "^0.10.5") (d #t) (k 0)))) (h "142k3d3ypzjr4gxwf5wbfy3c3rj7nj53dk57hcsbdssxqwlnpbwq")))

(define-public crate-elrond-wasm-node-0.11.0 (c (n "elrond-wasm-node") (v "0.11.0") (d (list (d (n "elrond-wasm") (r "^0.11.0") (d #t) (k 0)))) (h "1ab1xyxqyc397vcyzi8y4z988r3pj69a4flx1jsgih262k1zgq16")))

(define-public crate-elrond-wasm-node-0.12.0 (c (n "elrond-wasm-node") (v "0.12.0") (d (list (d (n "elrond-wasm") (r "^0.12.0") (d #t) (k 0)))) (h "1ppyg4alqjrwnamh0hf0hxa326cj5rr8mrpx5dkqw29fv956rvcr")))

(define-public crate-elrond-wasm-node-0.13.0 (c (n "elrond-wasm-node") (v "0.13.0") (d (list (d (n "elrond-wasm") (r "^0.13.0") (d #t) (k 0)))) (h "07d73rswaihkzjmajwrvgp0w2al6y8n5061bd8an48135vdlwjww")))

(define-public crate-elrond-wasm-node-0.14.0 (c (n "elrond-wasm-node") (v "0.14.0") (d (list (d (n "elrond-wasm") (r "^0.14.0") (d #t) (k 0)))) (h "09z618kp6ijrvqhrgwyrvlqnrbybw1vr1awagazk9q3jjhdxnmn7")))

(define-public crate-elrond-wasm-node-0.14.1 (c (n "elrond-wasm-node") (v "0.14.1") (d (list (d (n "elrond-wasm") (r "^0.14.1") (d #t) (k 0)))) (h "14kizwpr8n841nk0r8nbj60fhdcbln0bfgb05vh9cr9qvc8qb488")))

(define-public crate-elrond-wasm-node-0.14.2 (c (n "elrond-wasm-node") (v "0.14.2") (d (list (d (n "elrond-wasm") (r "^0.14.2") (d #t) (k 0)))) (h "1pk6h90igcdhwi85zb7l7n00xpxq75pxpg4ck3rvwjh2jk1jvzs3")))

(define-public crate-elrond-wasm-node-0.15.0 (c (n "elrond-wasm-node") (v "0.15.0") (d (list (d (n "elrond-wasm") (r "^0.15.0") (d #t) (k 0)))) (h "1cf0n9kc6jfhq5nwvaki8ln13pyvvn077y9kq0xhap8flcg564k3")))

(define-public crate-elrond-wasm-node-0.15.1 (c (n "elrond-wasm-node") (v "0.15.1") (d (list (d (n "elrond-wasm") (r "^0.15.1") (d #t) (k 0)))) (h "1164wlbj6ia0bsjpkhlbxvl3ynvmwpissnmdszm59430c1v52y9z")))

(define-public crate-elrond-wasm-node-0.16.0 (c (n "elrond-wasm-node") (v "0.16.0") (d (list (d (n "elrond-wasm") (r "^0.16.0") (d #t) (k 0)))) (h "0q9zwpdkv1ssknvqslzz6sqlslx1mg23sd5kpq6k3wd041z8rgr5")))

(define-public crate-elrond-wasm-node-0.16.1 (c (n "elrond-wasm-node") (v "0.16.1") (d (list (d (n "elrond-wasm") (r "^0.16.1") (d #t) (k 0)))) (h "17c2mwg8lz6mr3bnagzi016jbwfjil9y383rnkbzhrmx4g96b3qx")))

(define-public crate-elrond-wasm-node-0.16.2 (c (n "elrond-wasm-node") (v "0.16.2") (d (list (d (n "elrond-wasm") (r "^0.16.2") (d #t) (k 0)))) (h "0czvkfap6i3pz6xyqzjl53m8qryh2jw2zccwn9sjkw5b0mwh5aa1")))

(define-public crate-elrond-wasm-node-0.17.0 (c (n "elrond-wasm-node") (v "0.17.0") (d (list (d (n "elrond-wasm") (r "^0.17.0") (d #t) (k 0)))) (h "19izh9rv54ic02d386l5gvpcb4ywdv8n9zpn2f83xzss4igvdwqr")))

(define-public crate-elrond-wasm-node-0.17.1 (c (n "elrond-wasm-node") (v "0.17.1") (d (list (d (n "elrond-wasm") (r "^0.17.1") (d #t) (k 0)))) (h "0z82v1qmh3pd5bw4h151cf8c8a6z71yg3y3ayxzgcj24na3l2cdr")))

(define-public crate-elrond-wasm-node-0.17.2 (c (n "elrond-wasm-node") (v "0.17.2") (d (list (d (n "elrond-wasm") (r "^0.17.2") (d #t) (k 0)))) (h "1iqn4wcc71flvx1csbzmyjw6a04i3z0msf9pg435mgb49qs4d2a1")))

(define-public crate-elrond-wasm-node-0.17.3 (c (n "elrond-wasm-node") (v "0.17.3") (d (list (d (n "elrond-wasm") (r "^0.17.3") (d #t) (k 0)))) (h "1x8276ay63v5k69295hin5rbkcdgg9qvph076vf1ygc0wc28aib7")))

(define-public crate-elrond-wasm-node-0.17.4 (c (n "elrond-wasm-node") (v "0.17.4") (d (list (d (n "elrond-wasm") (r "^0.17.4") (d #t) (k 0)))) (h "1qngmxgb2vpwb4hp5k0z4sqk9h5hwgyqlssdvckxxdwjx09z86ch")))

(define-public crate-elrond-wasm-node-0.18.0 (c (n "elrond-wasm-node") (v "0.18.0") (d (list (d (n "elrond-wasm") (r "^0.18.0") (d #t) (k 0)))) (h "1fqkh9j45fplc5jzcq0rhf3yldv75fblp3w2klbdnx9a92h0069s")))

(define-public crate-elrond-wasm-node-0.18.1 (c (n "elrond-wasm-node") (v "0.18.1") (d (list (d (n "elrond-wasm") (r "^0.18.1") (d #t) (k 0)))) (h "1wc469s79fqcad657fs4a3qfaksqidxwszkrs5970a7xl1qp7651")))

(define-public crate-elrond-wasm-node-0.18.2 (c (n "elrond-wasm-node") (v "0.18.2") (d (list (d (n "elrond-wasm") (r "^0.18.2") (d #t) (k 0)))) (h "0821ac78964jxvwb6kw18d1llavsb2vjmnz8cac6hgqksf96rcsh")))

(define-public crate-elrond-wasm-node-0.19.0 (c (n "elrond-wasm-node") (v "0.19.0") (d (list (d (n "elrond-wasm") (r "^0.19.0") (d #t) (k 0)))) (h "15qzjjk2s01m1bpymyx676rfkw2npxkab2smgv56lrmh61j8qkix") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.19.1 (c (n "elrond-wasm-node") (v "0.19.1") (d (list (d (n "elrond-wasm") (r "^0.19.1") (d #t) (k 0)))) (h "0fyfvhndh2l6vkbcmfnp1fmby2xmi8gh5w6wi5kipkg75rhlva4j") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.20.0 (c (n "elrond-wasm-node") (v "0.20.0") (d (list (d (n "elrond-wasm") (r "^0.20.0") (d #t) (k 0)))) (h "1cl1l4znza0vz1ia1xkvr00r3a8gq5hqzw5ammsm4l8c8rlc9dj7") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.20.1 (c (n "elrond-wasm-node") (v "0.20.1") (d (list (d (n "elrond-wasm") (r "^0.20.1") (d #t) (k 0)))) (h "03ssv08k1ci576v62c0gbkxpdkfmfwq7hgk6gzpfrlbzsp7lpkry") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.21.0 (c (n "elrond-wasm-node") (v "0.21.0") (d (list (d (n "elrond-wasm") (r "^0.21.0") (d #t) (k 0)))) (h "185nisc4zylfahp436ap08dxh4hqfqqdbjbmaacn26anh0804p09") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.21.1 (c (n "elrond-wasm-node") (v "0.21.1") (d (list (d (n "elrond-wasm") (r "^0.21.1") (d #t) (k 0)))) (h "01pxpmvg08rfy43cgm3lhs493z8y4bz9s16y95jq79vwrdlinfgm") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.21.2 (c (n "elrond-wasm-node") (v "0.21.2") (d (list (d (n "elrond-wasm") (r "=0.21.2") (d #t) (k 0)))) (h "1kzy04bpa3fgnbfiqvk2m6f05pbkpwvkn0jkrmb3rn5mmxsx9lnx") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.0 (c (n "elrond-wasm-node") (v "0.22.0") (d (list (d (n "elrond-wasm") (r "=0.22.0") (d #t) (k 0)))) (h "1rmy7w3zvnywg9qdv22f8xagd0lviig344nal9c0s2z6b1r7cw3k") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.1 (c (n "elrond-wasm-node") (v "0.22.1") (d (list (d (n "elrond-wasm") (r "=0.22.1") (d #t) (k 0)))) (h "0casrzmphr09ahc5fpc9ynsicg87bfnhj7ynv79x3mkp3yflcbsh") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.2 (c (n "elrond-wasm-node") (v "0.22.2") (d (list (d (n "elrond-wasm") (r "=0.22.2") (d #t) (k 0)))) (h "1fc5vfr7mdzrjlqsyh391r46jjgs9y6i1fda707ds5flzq2wjmz4") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.3 (c (n "elrond-wasm-node") (v "0.22.3") (d (list (d (n "elrond-wasm") (r "=0.22.3") (d #t) (k 0)))) (h "1q1vmq1yaiisq0v7c0ligwb1cb7w2x0pyprzygap2bqc7h4yry0y") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.4 (c (n "elrond-wasm-node") (v "0.22.4") (d (list (d (n "elrond-wasm") (r "=0.22.4") (d #t) (k 0)))) (h "1dssgh21fkslb5vcb079r7sp507sbwbbaryb04kbc1837fklxbk5") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.5 (c (n "elrond-wasm-node") (v "0.22.5") (d (list (d (n "elrond-wasm") (r "=0.22.5") (d #t) (k 0)))) (h "1y53kjsa4g54v0fapsd5nlsjk9bzkq4j9d5vni5nm9vmfr9iv07r") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.6 (c (n "elrond-wasm-node") (v "0.22.6") (d (list (d (n "elrond-wasm") (r "=0.22.6") (d #t) (k 0)))) (h "0l0vg4zlk3rj6c4n0zhwbkrfpp0pgql30x6q64fzzagfh7kk8xpa") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.7 (c (n "elrond-wasm-node") (v "0.22.7") (d (list (d (n "elrond-wasm") (r "=0.22.7") (d #t) (k 0)))) (h "0xh5wl0b1g526azbvk9hzq4v5hcfbyrwjagn273cxkj9pxjn9qw2") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.8 (c (n "elrond-wasm-node") (v "0.22.8") (d (list (d (n "elrond-wasm") (r "=0.22.8") (d #t) (k 0)))) (h "1k4il7v720vn84pbc8lajb018v7rdrjc1vsd9iafwzykbbm8rdgc") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.9 (c (n "elrond-wasm-node") (v "0.22.9") (d (list (d (n "elrond-wasm") (r "=0.22.9") (d #t) (k 0)))) (h "0zlqnsgnv0k7whzzs1f917wwjxymzmgh2sq0an9nby9bc960g27f") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.10 (c (n "elrond-wasm-node") (v "0.22.10") (d (list (d (n "elrond-wasm") (r "=0.22.10") (d #t) (k 0)))) (h "170h758fpj9yziqkgb9r9mx7y1aqqnr1y5i3c56b529ss7rcv90h") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.22.11 (c (n "elrond-wasm-node") (v "0.22.11") (d (list (d (n "elrond-wasm") (r "=0.22.11") (d #t) (k 0)))) (h "0n1svxxs5gqqacgaiifn76h7n1frhi130rnvn7g8i38ipsbdghgh") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.23.0 (c (n "elrond-wasm-node") (v "0.23.0") (d (list (d (n "elrond-wasm") (r "=0.23.0") (d #t) (k 0)))) (h "1qiffw88qryh7mj8ix14pkq1fvd4ir11z4ka4wsprrgbzk60qb54") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.23.1 (c (n "elrond-wasm-node") (v "0.23.1") (d (list (d (n "elrond-wasm") (r "=0.23.1") (d #t) (k 0)))) (h "1my31zbksh8f6yy05swykdwg1ic4r0mk8llnd9adyw1l6ffxkmaw") (f (quote (("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.24.0 (c (n "elrond-wasm-node") (v "0.24.0") (d (list (d (n "elrond-wasm") (r "=0.24.0") (d #t) (k 0)))) (h "0459qag0rqa3q8lvbfzkgjjw306c9hc1id0ygynnpxn6kx8n40g0") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.25.0 (c (n "elrond-wasm-node") (v "0.25.0") (d (list (d (n "elrond-wasm") (r "=0.25.0") (d #t) (k 0)))) (h "0livvrjbi6klrk1fd0zdh78gqyv9a8pq1a6d7dm430m230lc8b96") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.26.0 (c (n "elrond-wasm-node") (v "0.26.0") (d (list (d (n "elrond-wasm") (r "=0.26.0") (d #t) (k 0)))) (h "1kcba8r1ncy9lwchbrv3qy1ycjp407p1wi8hf7b87f179yxc0iaf") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.27.0 (c (n "elrond-wasm-node") (v "0.27.0") (d (list (d (n "elrond-wasm") (r "=0.27.0") (d #t) (k 0)))) (h "1y06jhsdy22z84p9ssg6j35gbd0q7m9frj11fxpp55j842jbv4xs") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.27.1 (c (n "elrond-wasm-node") (v "0.27.1") (d (list (d (n "elrond-wasm") (r "=0.27.1") (d #t) (k 0)))) (h "0aq6zfi6043qgm5nxrb8zw96sx2l3c75kmfbg15f05q326l7h688") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.27.2 (c (n "elrond-wasm-node") (v "0.27.2") (d (list (d (n "elrond-wasm") (r "=0.27.2") (d #t) (k 0)))) (h "1x67lhdbahb52wcc5n5fbv6n0h809l6bp536bimfchhc9kdvw5w9") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.27.3 (c (n "elrond-wasm-node") (v "0.27.3") (d (list (d (n "elrond-wasm") (r "=0.27.3") (d #t) (k 0)))) (h "1b0xmfd0jl08p2q8j7r2gnjz31bmrbfj7xgjyin5p7xs3jn2lxj6") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.27.4 (c (n "elrond-wasm-node") (v "0.27.4") (d (list (d (n "elrond-wasm") (r "=0.27.4") (d #t) (k 0)))) (h "1wy5h7ph6j68f50q8lpi4r1g6npf13p4av3k7nm270v3mr8hjwsz") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.28.0 (c (n "elrond-wasm-node") (v "0.28.0") (d (list (d (n "elrond-wasm") (r "=0.28.0") (d #t) (k 0)))) (h "15s3axnhdb71hhnznk74334h9nhw9fy2jrbn6ashlxmib94s0mdd") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.29.0 (c (n "elrond-wasm-node") (v "0.29.0") (d (list (d (n "elrond-wasm") (r "=0.29.0") (d #t) (k 0)))) (h "15v2kxyg6n7g9rwfhbjc1d1bab046y5r67hpa3y4akxr7rdpdnr5") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.29.2 (c (n "elrond-wasm-node") (v "0.29.2") (d (list (d (n "elrond-wasm") (r "=0.29.2") (d #t) (k 0)))) (h "0iv74sibi750n5g9nk9871w7hh5n1h50d4r749x7x6pazpibx0zx") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.29.3 (c (n "elrond-wasm-node") (v "0.29.3") (d (list (d (n "elrond-wasm") (r "=0.29.3") (d #t) (k 0)))) (h "18jcxgk1ka27mgaq50sxhp51af5fcl3v4f4jqw58lncrnwbqxbdd") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("unmanaged-ei"))))))

(define-public crate-elrond-wasm-node-0.30.0 (c (n "elrond-wasm-node") (v "0.30.0") (d (list (d (n "elrond-wasm") (r "=0.30.0") (d #t) (k 0)))) (h "1s0h22lawl796idybxf6y901r7nf2ilp92mp7h45xqaz511chgr0") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged") ("ei-1-1" "elrond-wasm/ei-1-1"))))))

(define-public crate-elrond-wasm-node-0.31.0 (c (n "elrond-wasm-node") (v "0.31.0") (d (list (d (n "elrond-wasm") (r "=0.31.0") (d #t) (k 0)))) (h "0p2qg07cd1r2grrfr1bxj0wl1vwpv3j0mxw0xa645fiwg545bsn1") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged"))))))

(define-public crate-elrond-wasm-node-0.31.1 (c (n "elrond-wasm-node") (v "0.31.1") (d (list (d (n "elrond-wasm") (r "=0.31.1") (d #t) (k 0)))) (h "0zr06xxgwx19fcczbqwp7b0srq83wb29qldcvnlqx1mfnhn4p3wk") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged"))))))

(define-public crate-elrond-wasm-node-0.32.0 (c (n "elrond-wasm-node") (v "0.32.0") (d (list (d (n "elrond-wasm") (r "=0.32.0") (d #t) (k 0)))) (h "1qpfjycvjzq650xqdi0w2qammdlynv51rihh5gp2zscg8z24j4wg") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.33.0 (c (n "elrond-wasm-node") (v "0.33.0") (d (list (d (n "elrond-wasm") (r "=0.33.0") (d #t) (k 0)))) (h "1mpjzq61znz41ybyiz8dcdmjjah03l2ashm639yqc1v03w7yg224") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.33.1 (c (n "elrond-wasm-node") (v "0.33.1") (d (list (d (n "elrond-wasm") (r "=0.33.1") (d #t) (k 0)))) (h "081m7n7kckbm1kqlzb3373md52wjq9sa5i63s3wpsms8v25rwczn") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.34.0 (c (n "elrond-wasm-node") (v "0.34.0") (d (list (d (n "elrond-wasm") (r "=0.34.0") (d #t) (k 0)))) (h "0nlm02d9ccm170d0fzvmz1k5r18a61h1xwnr5r9d8jrbwka5yj8b") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.34.1 (c (n "elrond-wasm-node") (v "0.34.1") (d (list (d (n "elrond-wasm") (r "=0.34.1") (d #t) (k 0)))) (h "0y4j70sn1wps9djg685173hqmw7wbh0bq02jkjrcvxw5gqbn9h4p") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.35.0 (c (n "elrond-wasm-node") (v "0.35.0") (d (list (d (n "elrond-wasm") (r "=0.35.0") (d #t) (k 0)))) (h "16sfgqibd8nl3k1bkihzyyjhg9z0mhq9gqdcc1gpz70d132hj381") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.36.0 (c (n "elrond-wasm-node") (v "0.36.0") (d (list (d (n "elrond-wasm") (r "=0.36.0") (d #t) (k 0)))) (h "1q60xky1ygdpkv9zbwvd6xgzj3p3b58lnd2wqcdi1zik06iz3ng9") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.36.1 (c (n "elrond-wasm-node") (v "0.36.1") (d (list (d (n "elrond-wasm") (r "=0.36.1") (d #t) (k 0)))) (h "16rgvys46bkx7m5cnndlx86b3syi20cs1862vf37z5pdmk0x9d2c") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.37.0 (c (n "elrond-wasm-node") (v "0.37.0") (d (list (d (n "elrond-wasm") (r "=0.37.0") (d #t) (k 0)))) (h "1kh5qhbgaaaiplln1mjs2c4q5dkcwxyv87w0rirw5jxl6qvy9fi8") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-elrond-wasm-node-0.38.0 (c (n "elrond-wasm-node") (v "0.38.0") (d (list (d (n "elrond-wasm") (r "=0.38.0") (d #t) (k 0)))) (h "0wg5bfz5n0xwns2gi9vclaxklgs8h0lhsr9s7m2hz5z36ki31a6s") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

