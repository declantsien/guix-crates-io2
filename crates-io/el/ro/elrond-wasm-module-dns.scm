(define-module (crates-io el ro elrond-wasm-module-dns) #:use-module (crates-io))

(define-public crate-elrond-wasm-module-dns-0.16.2 (c (n "elrond-wasm-module-dns") (v "0.16.2") (d (list (d (n "elrond-wasm") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.2") (o #t) (d #t) (k 0)))) (h "17871ag2vayv8plda44b5jr5nqghfwgm5gzwj7bsbsjgiiyxlfpc") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.17.0 (c (n "elrond-wasm-module-dns") (v "0.17.0") (d (list (d (n "elrond-wasm") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "1a3p0xn14hfwbrp3y0ph06rwg3i2kkyx14wwj166wbcmw99wyais") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.17.1 (c (n "elrond-wasm-module-dns") (v "0.17.1") (d (list (d (n "elrond-wasm") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.1") (o #t) (d #t) (k 0)))) (h "1lrmqn49ymw3hj03i0da18600fn2inv8d13w7p250kck32d9f0cw") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.17.2 (c (n "elrond-wasm-module-dns") (v "0.17.2") (d (list (d (n "elrond-wasm") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.2") (o #t) (d #t) (k 0)))) (h "1fbjv91p2fxxmmyiqbr93vym24dsfbfsyxx3ayf3ryifl5ld9wva") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.17.3 (c (n "elrond-wasm-module-dns") (v "0.17.3") (d (list (d (n "elrond-wasm") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.3") (o #t) (d #t) (k 0)))) (h "0k7wsap2m8vmilx3i5iv1wjc5f4bpwj6qb68nnj7rzgkl7mgrbs0") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.17.4 (c (n "elrond-wasm-module-dns") (v "0.17.4") (d (list (d (n "elrond-wasm") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.4") (o #t) (d #t) (k 0)))) (h "05pcqv1kw9yam8p7211c4ssdb49g9iw3f0wxbw7x0s22mnj5mpdn") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.18.0 (c (n "elrond-wasm-module-dns") (v "0.18.0") (d (list (d (n "elrond-wasm") (r "^0.18.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1jiaii003zki1v9ll4vxxg8w8nc157ywrx1wrwz7wypglfdyfja4") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.18.1 (c (n "elrond-wasm-module-dns") (v "0.18.1") (d (list (d (n "elrond-wasm") (r "^0.18.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "0vs9d9gna0rhrvbrkx9m6b913ipbmkj4x2kw4r4rmlx1nqa8r1cy") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.18.2 (c (n "elrond-wasm-module-dns") (v "0.18.2") (d (list (d (n "elrond-wasm") (r "^0.18.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.2") (o #t) (d #t) (k 0)))) (h "0y6m7jdgv8f5llqvfmfbm3mzgrvbz6r8zarvhv1mpg0829xclsq1") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.19.0 (c (n "elrond-wasm-module-dns") (v "0.19.0") (d (list (d (n "elrond-wasm") (r "^0.19.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0wnlmb5k8r13myf8baasy1302fspn2qwxdvwqhnwgrrx9a4hdav8") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.19.1 (c (n "elrond-wasm-module-dns") (v "0.19.1") (d (list (d (n "elrond-wasm") (r "^0.19.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1al6nh26in647zk7l46m5p6gz9skvkzbvkm9yi7byy1lnxba9arh") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.20.0 (c (n "elrond-wasm-module-dns") (v "0.20.0") (d (list (d (n "elrond-wasm") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "03hbcp70w6jhhlylhlqx6w9i2pxf5d0hcbqky8dhl4z6alv05rs3") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.20.1 (c (n "elrond-wasm-module-dns") (v "0.20.1") (d (list (d (n "elrond-wasm") (r "^0.20.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "1vnpi239v5n5dymzs71avlcswdc13zbnrbf3yzi6x1y8gwm43cz2") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.21.0 (c (n "elrond-wasm-module-dns") (v "0.21.0") (d (list (d (n "elrond-wasm") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0dkndhqc04djfhkz0b07j1vks2lmf2h63brbmdq99k301v48d3jp") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.21.1 (c (n "elrond-wasm-module-dns") (v "0.21.1") (d (list (d (n "elrond-wasm") (r "^0.21.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "150alcmyn8gwjlakx4vlgx5h6fs3a9qzgv05qa737lx1hvl39jra") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.21.2 (c (n "elrond-wasm-module-dns") (v "0.21.2") (d (list (d (n "elrond-wasm") (r "^0.21.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.2") (o #t) (d #t) (k 0)))) (h "1fvlf86q7lb6ri0bi40y1m59hgkdh3156dwm3y0l91vd48kclbzi") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-dns-0.22.0 (c (n "elrond-wasm-module-dns") (v "0.22.0") (d (list (d (n "elrond-wasm") (r "^0.22.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.0") (d #t) (k 2)))) (h "1d0s3qf7whajn2ywmdpw6va9s74dx678qv7iigywrzkwpw7w8zdg")))

(define-public crate-elrond-wasm-module-dns-0.22.1 (c (n "elrond-wasm-module-dns") (v "0.22.1") (d (list (d (n "elrond-wasm") (r "^0.22.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.1") (d #t) (k 2)))) (h "14rhgxi47jvyzvy4qyx3dkdh9da0cma9pnzmf35xiimp3avx9mpm")))

(define-public crate-elrond-wasm-module-dns-0.22.2 (c (n "elrond-wasm-module-dns") (v "0.22.2") (d (list (d (n "elrond-wasm") (r "^0.22.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.2") (d #t) (k 2)))) (h "0261crv0k8d113zl7nrkjs0mdksknay9wmfcaxyffckyym8q8sn1")))

(define-public crate-elrond-wasm-module-dns-0.22.3 (c (n "elrond-wasm-module-dns") (v "0.22.3") (d (list (d (n "elrond-wasm") (r "^0.22.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.3") (d #t) (k 2)))) (h "1z65lgacr2kmxqcksajsk7xj6bp7x3j6g2pscpnrmw2x5i17ichh")))

(define-public crate-elrond-wasm-module-dns-0.22.4 (c (n "elrond-wasm-module-dns") (v "0.22.4") (d (list (d (n "elrond-wasm") (r "^0.22.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.4") (d #t) (k 2)))) (h "1gxrk2hnqiy289gaci68d4l0hfcwl9vc93xriaylfykdx6f517qh")))

(define-public crate-elrond-wasm-module-dns-0.22.5 (c (n "elrond-wasm-module-dns") (v "0.22.5") (d (list (d (n "elrond-wasm") (r "^0.22.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.5") (d #t) (k 2)))) (h "06lhivl602hbx4cr52w9k6nf650623czb62v0c0dxckc035888cd")))

(define-public crate-elrond-wasm-module-dns-0.22.6 (c (n "elrond-wasm-module-dns") (v "0.22.6") (d (list (d (n "elrond-wasm") (r "^0.22.6") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.6") (d #t) (k 2)))) (h "13wkf1nz6bckl217sh857xxcb816bvvq1g4cgv6wr215y30hhn3d")))

(define-public crate-elrond-wasm-module-dns-0.22.7 (c (n "elrond-wasm-module-dns") (v "0.22.7") (d (list (d (n "elrond-wasm") (r "^0.22.7") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.7") (d #t) (k 2)))) (h "0f5393796izamrvrccv51bc09kvix3xpk13jdnx2a7x7y6rijbbi")))

(define-public crate-elrond-wasm-module-dns-0.22.8 (c (n "elrond-wasm-module-dns") (v "0.22.8") (d (list (d (n "elrond-wasm") (r "^0.22.8") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.8") (d #t) (k 2)))) (h "1d1a1bmsr458dj2klfpsb0k48rqjf53r24fmi4w0a8p8d0yn08jk")))

(define-public crate-elrond-wasm-module-dns-0.22.9 (c (n "elrond-wasm-module-dns") (v "0.22.9") (d (list (d (n "elrond-wasm") (r "^0.22.9") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.9") (d #t) (k 2)))) (h "0zl6a87b0w2d2yq0h613c1irnxm6x46wjhj4w20xrbr3yp6x9vnr")))

(define-public crate-elrond-wasm-module-dns-0.22.10 (c (n "elrond-wasm-module-dns") (v "0.22.10") (d (list (d (n "elrond-wasm") (r "^0.22.10") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.10") (d #t) (k 2)))) (h "1k4pi3i6zd6qb2mxfhgzhklsy950pglhaspqi8sni8splsk7x4da")))

(define-public crate-elrond-wasm-module-dns-0.22.11 (c (n "elrond-wasm-module-dns") (v "0.22.11") (d (list (d (n "elrond-wasm") (r "^0.22.11") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.11") (d #t) (k 2)))) (h "02r07vkn6mvw4y9psnwp3gz402qzkw2vabra6bifdk68kqkwqk86")))

(define-public crate-elrond-wasm-module-dns-0.23.0 (c (n "elrond-wasm-module-dns") (v "0.23.0") (d (list (d (n "elrond-wasm") (r "^0.23.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.0") (d #t) (k 2)))) (h "1rrndbhvbddy7z343r11946kx8shznwd3qx5dm7a0v4hbjfw3klx")))

(define-public crate-elrond-wasm-module-dns-0.23.1 (c (n "elrond-wasm-module-dns") (v "0.23.1") (d (list (d (n "elrond-wasm") (r "^0.23.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.1") (d #t) (k 2)))) (h "1vckghs04drm007r09lcb6nm0nn6ilr3gpmjiwwvwz7hnvib4cqf")))

(define-public crate-elrond-wasm-module-dns-0.24.0 (c (n "elrond-wasm-module-dns") (v "0.24.0") (d (list (d (n "elrond-wasm") (r "^0.24.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.24.0") (d #t) (k 2)))) (h "0ra5fwra91k9bfcjp4bgm1vnbc0wv6dc4821xnr3w1vqfnmmxzq7")))

(define-public crate-elrond-wasm-module-dns-0.25.0 (c (n "elrond-wasm-module-dns") (v "0.25.0") (d (list (d (n "elrond-wasm") (r "^0.25.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.25.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.25.0") (d #t) (k 0)))) (h "12c4bml95slv728npq2n6mk6p6zszbds0w6yagrckawl96y3x2gz")))

