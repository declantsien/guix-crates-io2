(define-module (crates-io el ro elrond-wasm-output) #:use-module (crates-io))

(define-public crate-elrond-wasm-output-0.6.0 (c (n "elrond-wasm-output") (v "0.6.0") (d (list (d (n "elrond-wasm-node") (r "^0.6.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0xdd26gwnkx84z55lix5gm2vjx2dp1rk25n9afg0cr5hd5mx7yxx") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.6.1 (c (n "elrond-wasm-output") (v "0.6.1") (d (list (d (n "elrond-wasm-node") (r "^0.6.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "12ggxz4grch9f8q6z9xwgy496z0gwlk2is3n3hi3xpcflpln1rdp") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.6.2 (c (n "elrond-wasm-output") (v "0.6.2") (d (list (d (n "elrond-wasm-node") (r "^0.6.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0bj6yxqllrqbxx5npx6sb3jshgh1f78vycannxzv7i0j7lsfxd2j") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.7.0 (c (n "elrond-wasm-output") (v "0.7.0") (d (list (d (n "elrond-wasm-node") (r "^0.7.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "01xqr2rgq1yki8rsghw51yhy9w24v32hc59jiysiz8wchzag42ax") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.7.1 (c (n "elrond-wasm-output") (v "0.7.1") (d (list (d (n "elrond-wasm-node") (r "^0.7.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "13hh416w8hr2p3f4h7aw5xmv1qw34xdch0a2fqkfl7cdy183xpif") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.7.2 (c (n "elrond-wasm-output") (v "0.7.2") (d (list (d (n "elrond-wasm-node") (r "^0.7.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1akjivhdwnr5fg71ykj9mpayr7bbcqzpgk4nl9dqq6zcdfcfks2c") (f (quote (("wasm-output-mode"))))))

(define-public crate-elrond-wasm-output-0.8.0 (c (n "elrond-wasm-output") (v "0.8.0") (d (list (d (n "elrond-wasm-node") (r "^0.8.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0gzb19l197g57i43drnx8z203d44l8gv4j27v3khbaj2basb9khd") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.0 (c (n "elrond-wasm-output") (v "0.9.0") (d (list (d (n "elrond-wasm-node") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "06v1m7qfd6dpf1f4nxxw65qscs1jmx4y5vjwcmsrpprf40pg09xj") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.1 (c (n "elrond-wasm-output") (v "0.9.1") (d (list (d (n "elrond-wasm-node") (r "^0.9.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1dfki9zj89y6rm2b0vaaw3nzca1902mpsyyy0lxi2iq8k69flnnw") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.2 (c (n "elrond-wasm-output") (v "0.9.2") (d (list (d (n "elrond-wasm-node") (r "^0.9.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "11qzdk7x1sqxpmwcxlzcinnax1yah9kwp9pjnxfn05nc2v04aghg") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.3 (c (n "elrond-wasm-output") (v "0.9.3") (d (list (d (n "elrond-wasm-node") (r "^0.9.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hjjfx972pqgsddc1lmkg21is9fzakwnbf4qk7m01h6fw98n9h9h") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.4 (c (n "elrond-wasm-output") (v "0.9.4") (d (list (d (n "elrond-wasm-node") (r "^0.9.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1wb1svybipj7x9sh1wqsbp642pzy9bfs0kkp0f7mm69inp132fmy") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.5 (c (n "elrond-wasm-output") (v "0.9.5") (d (list (d (n "elrond-wasm-node") (r "^0.9.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "16h045b23s0h97xzp12q068ickx3c25ajs190vbqdxac70924svz") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.6 (c (n "elrond-wasm-output") (v "0.9.6") (d (list (d (n "elrond-wasm-node") (r "^0.9.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0w3kbh97abzla3qkxf562p6459p5m4nw6jxgz2vangripyk12sqg") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.7 (c (n "elrond-wasm-output") (v "0.9.7") (d (list (d (n "elrond-wasm-node") (r "^0.9.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1jsy263rqqf5hxfcvpiyild2rk3vzd4ldqms3nwqfafpz7a72c76") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.9.8 (c (n "elrond-wasm-output") (v "0.9.8") (d (list (d (n "elrond-wasm-node") (r ">=0.9.8, <0.10.0") (d #t) (k 0)) (d (n "wee_alloc") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "1zq674z8qz4bhzgvv0hqncgy4a86vmhm0z49lz2l0x0zi3b2iwh5") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.0 (c (n "elrond-wasm-output") (v "0.10.0") (d (list (d (n "elrond-wasm-node") (r "^0.10.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hrq3hfbpjflvjpc4mj91236fzq1bgpymcc63v0f92szpsa76njh") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.1 (c (n "elrond-wasm-output") (v "0.10.1") (d (list (d (n "elrond-wasm-node") (r "^0.10.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0w566qr5d0zmffpi2cvj684x5g69xfrpcajhxfms4jwn92mbb4yz") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.2 (c (n "elrond-wasm-output") (v "0.10.2") (d (list (d (n "elrond-wasm-node") (r "^0.10.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "15glr8sysyrni4by6rch4gllzja8i85n6hjrgsv6sv16lgly6g7x") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.3 (c (n "elrond-wasm-output") (v "0.10.3") (d (list (d (n "elrond-wasm-node") (r "^0.10.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1b4b2vcf2wz1aybijqqchpnqwlzmyb0pk9bw8976ck8jqmkmrxpb") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.4 (c (n "elrond-wasm-output") (v "0.10.4") (d (list (d (n "elrond-wasm-node") (r "^0.10.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "19sxyb1m0c4qw76ph293mkg7yqjk7p9gr9k3hy691fvpgqa5p3yf") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.10.5 (c (n "elrond-wasm-output") (v "0.10.5") (d (list (d (n "elrond-wasm-node") (r "^0.10.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0vrd1khbcd3g4fadw9d5r47f0fh2fjx27lc8rlsb7b3mh9lvjv2l") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.11.0 (c (n "elrond-wasm-output") (v "0.11.0") (d (list (d (n "elrond-wasm-node") (r "^0.11.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0p6adx8w1bqd2p8162j3f778yzikrvbv7v6zm7bkm3mrfz5yg2sj") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.12.0 (c (n "elrond-wasm-output") (v "0.12.0") (d (list (d (n "elrond-wasm-node") (r "^0.12.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1bd2nd26b69akx97l8fwqxx2wampmwq55mmd0rxgd2i1s28042v2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.13.0 (c (n "elrond-wasm-output") (v "0.13.0") (d (list (d (n "elrond-wasm-node") (r "^0.13.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0q2b2ji79sif0wa7xzxwhx6lidja023pkc0h0x0avcsfxbxivdgj") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.14.0 (c (n "elrond-wasm-output") (v "0.14.0") (d (list (d (n "elrond-wasm-node") (r "^0.14.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1iqrcig4q2yaa8142c0jz10lyy246xamy1h837l20ll53yd47d75") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.14.1 (c (n "elrond-wasm-output") (v "0.14.1") (d (list (d (n "elrond-wasm-node") (r "^0.14.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0dsmawj1iv51g34gl808lyp4alxnavxbbql8ibqhx3rj0xyrjc6q") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.14.2 (c (n "elrond-wasm-output") (v "0.14.2") (d (list (d (n "elrond-wasm-node") (r "^0.14.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0yivwz5i1pjh99ssfq3xscsp9bpb5hli3jwa07hkv2l1gqb6dvv7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.15.0 (c (n "elrond-wasm-output") (v "0.15.0") (d (list (d (n "elrond-wasm-node") (r "^0.15.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1kfhp6zlnhjikiral81sw2pcby5zhcp8f89vbyhprvjva4djzx1x") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.15.1 (c (n "elrond-wasm-output") (v "0.15.1") (d (list (d (n "elrond-wasm-node") (r "^0.15.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "07zqrzbcrh57cd3ssxgkibn01jp5fpinwvc1f8vdlja0bdcsm9i9") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.16.0 (c (n "elrond-wasm-output") (v "0.16.0") (d (list (d (n "elrond-wasm-node") (r "^0.16.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "13cx8kwplar2dmm1hj9ww4y5l2421g8i6jxzg9rpabawi2ryc2zx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.16.1 (c (n "elrond-wasm-output") (v "0.16.1") (d (list (d (n "elrond-wasm-node") (r "^0.16.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0nm4i3lv2mg83iidbrprzq4cqn2k3nz8wggsmslxwkd1839vmqv7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.16.2 (c (n "elrond-wasm-output") (v "0.16.2") (d (list (d (n "elrond-wasm-node") (r "^0.16.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "19mkmlg7iwwyp6ywmvrkx0ikhqyk18s6r7dz6b569qig27gphfk5") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.17.0 (c (n "elrond-wasm-output") (v "0.17.0") (d (list (d (n "elrond-wasm-node") (r "^0.17.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0i81kls8r8lha2li3q3yb5kka4n1akl9xv54acirpl2rk4y1pm17") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.17.1 (c (n "elrond-wasm-output") (v "0.17.1") (d (list (d (n "elrond-wasm-node") (r "^0.17.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1gnmbmsyy641fdnayk28gxxfdzvqwv0v49i68a70hkg5nfa0avwx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.17.2 (c (n "elrond-wasm-output") (v "0.17.2") (d (list (d (n "elrond-wasm-node") (r "^0.17.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "155sim5algyrngm9ibkspi56w6ra3npikz6iv0gyhsg3iirwijph") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.17.3 (c (n "elrond-wasm-output") (v "0.17.3") (d (list (d (n "elrond-wasm-node") (r "^0.17.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hjwjhyhx3is1vn52knivkp48790fgzvbplxny02kdahhj8r21qx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.17.4 (c (n "elrond-wasm-output") (v "0.17.4") (d (list (d (n "elrond-wasm-node") (r "^0.17.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0rxk4vq3x3x8y8s9ggnksr05b29hq8p98snhjs46bhd2vla9lpma") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.18.0 (c (n "elrond-wasm-output") (v "0.18.0") (d (list (d (n "elrond-wasm-node") (r "^0.18.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1zszn4cnc0wccc16vpdwn975a00wrgvshjj5l7ivbz137cgxsn32") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.18.1 (c (n "elrond-wasm-output") (v "0.18.1") (d (list (d (n "elrond-wasm-node") (r "^0.18.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1gz4s7vp4np179bw50hi4bp93f0427qj7k8z2pmgyd6y7ain8aa2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.18.2 (c (n "elrond-wasm-output") (v "0.18.2") (d (list (d (n "elrond-wasm-node") (r "^0.18.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "17gkmghi9wcjxgpqc97vyfh15rxdbadldcjnv6r7fda25rk0knjl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.19.0 (c (n "elrond-wasm-output") (v "0.19.0") (d (list (d (n "elrond-wasm-node") (r "^0.19.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1p5gf9l8wn01sh02ilk5p7abcyhiprr13xg5v4xrll0wh048k1v8") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.19.1 (c (n "elrond-wasm-output") (v "0.19.1") (d (list (d (n "elrond-wasm-node") (r "^0.19.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1a2zigpaalqfzadb1q59f4h4475hgvqagcp618cbirgqk8znhpv7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.20.0 (c (n "elrond-wasm-output") (v "0.20.0") (d (list (d (n "elrond-wasm-node") (r "^0.20.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0f057spf9g7zx82szk39gifzdpi16x9473jqjbfycv4a0qgazl8r") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.20.1 (c (n "elrond-wasm-output") (v "0.20.1") (d (list (d (n "elrond-wasm-node") (r "^0.20.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1yjmbyxnav9z556w85qaahsjqafm8x4pr4995j0q42wb8ia5m9dl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.21.0 (c (n "elrond-wasm-output") (v "0.21.0") (d (list (d (n "elrond-wasm-node") (r "^0.21.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0wp1zrvdzhyzi508ax9gqns8vaj7ski1835iaw6rg8cfhwmj1r62") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.21.1 (c (n "elrond-wasm-output") (v "0.21.1") (d (list (d (n "elrond-wasm-node") (r "^0.21.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0ma68hmmqkm73zcbapv5qjlkmq48gmb8n9jww5ic3zh0r73ah95n") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.21.2 (c (n "elrond-wasm-output") (v "0.21.2") (d (list (d (n "elrond-wasm-node") (r "=0.21.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zdlqfzmrpwrv1izm036vj4pi9q7955478h943474cax7fzysg8d") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.0 (c (n "elrond-wasm-output") (v "0.22.0") (d (list (d (n "elrond-wasm-node") (r "=0.22.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1xl7jb7r8f2pdi3z48wqbaqbvqj572gxf71gn46gvpnzyi6s5zmx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.1 (c (n "elrond-wasm-output") (v "0.22.1") (d (list (d (n "elrond-wasm-node") (r "=0.22.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0b55zlmwccg1lny7m0mi5fnkvbyxfff1f1yvv9ls4kw80riq10dz") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.2 (c (n "elrond-wasm-output") (v "0.22.2") (d (list (d (n "elrond-wasm-node") (r "=0.22.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1k7czrgwbhr3sv966lqnl7vbx23g15ik6baqwhj0n5rrr9vd58ki") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.3 (c (n "elrond-wasm-output") (v "0.22.3") (d (list (d (n "elrond-wasm-node") (r "=0.22.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0a1dyciv0q8w8n76bkrh2vwbc6czydrvkjnrc67c0vj2q63sn8i3") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.4 (c (n "elrond-wasm-output") (v "0.22.4") (d (list (d (n "elrond-wasm-node") (r "=0.22.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "09mfa29rzah19ka6d543iib3z8f8zs9a9ahhvpvjxym5bnk76185") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.5 (c (n "elrond-wasm-output") (v "0.22.5") (d (list (d (n "elrond-wasm-node") (r "=0.22.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0z2hrxwyh5i0s8hwp91li3fv13m9wmhis5bxr7gffrhxljxxpn73") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.6 (c (n "elrond-wasm-output") (v "0.22.6") (d (list (d (n "elrond-wasm-node") (r "=0.22.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fpy47bvdqi323dfpj24wga0mrvkh09pag8lcp6lj3cp4wi6c55j") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.7 (c (n "elrond-wasm-output") (v "0.22.7") (d (list (d (n "elrond-wasm-node") (r "=0.22.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "08g5qwy8ng8xj5x6j9387f9ln14wnw9x8vmqbzwx7qqnyvi7y4if") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.8 (c (n "elrond-wasm-output") (v "0.22.8") (d (list (d (n "elrond-wasm-node") (r "=0.22.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0fc86yvr3q87c19y10idvw9p19bdhmi636f2rgi3929nszhlba1y") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.9 (c (n "elrond-wasm-output") (v "0.22.9") (d (list (d (n "elrond-wasm-node") (r "=0.22.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hldjk3hpqrwsdb79012jcxrki357gbw49h4lxl4hy5wvs8j9cz4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.10 (c (n "elrond-wasm-output") (v "0.22.10") (d (list (d (n "elrond-wasm-node") (r "=0.22.10") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0m5ckgwzl8h5kl997kqqnva8h7f0gvnxdi2gdia8k1f117ybl5m7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.22.11 (c (n "elrond-wasm-output") (v "0.22.11") (d (list (d (n "elrond-wasm-node") (r "=0.22.11") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "03cs23jp0prxwczfh0ir4yj9q1hl7vkqsdnmz6mhrb7p99z6wdrl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.23.0 (c (n "elrond-wasm-output") (v "0.23.0") (d (list (d (n "elrond-wasm-node") (r "=0.23.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1w29d1c8dz9ibp005h19wxg9lvbz57fw813hp5rpzhrrswmcl938") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.23.1 (c (n "elrond-wasm-output") (v "0.23.1") (d (list (d (n "elrond-wasm-node") (r "=0.23.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0qbkdih4nysz2fiwnzyv0nxnmxz8aap2i6l21ykd23vvjv7lv0qk") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.24.0 (c (n "elrond-wasm-output") (v "0.24.0") (d (list (d (n "elrond-wasm-node") (r "=0.24.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0snnrrl2dx37ha9nn37097xilvkg016q4apsb40wybblwmg8z26m") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.25.0 (c (n "elrond-wasm-output") (v "0.25.0") (d (list (d (n "elrond-wasm-node") (r "=0.25.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0pm5g38k8gafp4nvdmbmvd0xyznn3dy82wcdgfjari8fpqk8b372") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.26.0 (c (n "elrond-wasm-output") (v "0.26.0") (d (list (d (n "elrond-wasm-node") (r "=0.26.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "19hiw71v66zhhxmn6wl6g1i64zppc4lq5yin5b5qqj801yxy0yj5") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.27.0 (c (n "elrond-wasm-output") (v "0.27.0") (d (list (d (n "elrond-wasm-node") (r "=0.27.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "16ynf2n91j9l201scbwzbpq7kyzh6f53ik0zxca8krm6024k3h3z") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.27.1 (c (n "elrond-wasm-output") (v "0.27.1") (d (list (d (n "elrond-wasm-node") (r "=0.27.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0yjiqgsjxmia2d33d50xf8aw0w6d8n27hq6q9by30llxffhi05zl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.27.2 (c (n "elrond-wasm-output") (v "0.27.2") (d (list (d (n "elrond-wasm-node") (r "=0.27.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0w04zy50p3z639g074igz3l6nvb2y5y4h19hwa5w6d2f72flys4d") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.27.3 (c (n "elrond-wasm-output") (v "0.27.3") (d (list (d (n "elrond-wasm-node") (r "=0.27.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "10a53444qdqkghg8x2l0z06nw42hj5rxlp2cxm8di3ls0siqcrb4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.27.4 (c (n "elrond-wasm-output") (v "0.27.4") (d (list (d (n "elrond-wasm-node") (r "=0.27.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0x83s3qsk4h3ag0srcaq6d16zq97b47jw03wx2nm7is9fg7vjry4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.28.0 (c (n "elrond-wasm-output") (v "0.28.0") (d (list (d (n "elrond-wasm-node") (r "=0.28.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0f3zm31j63d5qnqbg6v9b3ymsw8c23hcpy5p4avbhjq0y7p586ra") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.29.0 (c (n "elrond-wasm-output") (v "0.29.0") (d (list (d (n "elrond-wasm-node") (r "=0.29.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "16s59k9sww6w8bgnp6bma3ixf6h8czv6j6mdd4qypzdrncmyw7qa") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.29.2 (c (n "elrond-wasm-output") (v "0.29.2") (d (list (d (n "elrond-wasm-node") (r "=0.29.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1mn604c8qnhavmhmhmg0dhcvb8slydpm60kslw4n710y28jx3mcs") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.29.3 (c (n "elrond-wasm-output") (v "0.29.3") (d (list (d (n "elrond-wasm-node") (r "=0.29.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1q6fgz5qrqwxgqgr9fvrgppx88lkn2vl809ypgm3iqjbawa27hd8") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.30.0 (c (n "elrond-wasm-output") (v "0.30.0") (d (list (d (n "elrond-wasm-node") (r "=0.30.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1jl4zsqf5m1vki7682bjiva5az868w6cqmhi3k6a7l0iz2him1c7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.31.0 (c (n "elrond-wasm-output") (v "0.31.0") (d (list (d (n "elrond-wasm-node") (r "=0.31.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0d5vdfhq76n5pm9gs40sb82k22vdblnxdhjvqbsk3syj01yil3il") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.31.1 (c (n "elrond-wasm-output") (v "0.31.1") (d (list (d (n "elrond-wasm-node") (r "=0.31.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0acv60h85srmwxblpf5zyyn9mf5vp7zy56qsam52i7ss9kmf58g5") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.32.0 (c (n "elrond-wasm-output") (v "0.32.0") (d (list (d (n "elrond-wasm-node") (r "=0.32.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1r8i5fw2cldfzwjv9h9rj5njv8wsdgixdg9lgb9ry03nfwvdfrgr") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.33.0 (c (n "elrond-wasm-output") (v "0.33.0") (d (list (d (n "elrond-wasm-node") (r "=0.33.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1j4ngibyd4w4g37y22ldww203cgdfn2azpr0viikp8zadlzh2f06") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.33.1 (c (n "elrond-wasm-output") (v "0.33.1") (d (list (d (n "elrond-wasm-node") (r "=0.33.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1qjbwq8i46a6gizdgzmwvk5l0zabyrmjr4lxm5xv0m5hhcsjz94x") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.34.0 (c (n "elrond-wasm-output") (v "0.34.0") (d (list (d (n "elrond-wasm-node") (r "=0.34.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1di7bsk1mq12fz4khr3ngad0975a65bvhnglpyzpw8xwy307jyjc") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.34.1 (c (n "elrond-wasm-output") (v "0.34.1") (d (list (d (n "elrond-wasm-node") (r "=0.34.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0a1nw812bkd7i9gy7b9zb3rwk2zq86big022wasglyd759r9k453") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.35.0 (c (n "elrond-wasm-output") (v "0.35.0") (d (list (d (n "elrond-wasm-node") (r "=0.35.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0syqph2d04h3rglwpwm0p9kx4fcs1bs64akh450v6bqxifk8810m") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.36.0 (c (n "elrond-wasm-output") (v "0.36.0") (d (list (d (n "elrond-wasm-node") (r "=0.36.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0apnda266fxybxqkmw165vlpf3xhw82qja485v2xdhx4sxjy3zkp") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.36.1 (c (n "elrond-wasm-output") (v "0.36.1") (d (list (d (n "elrond-wasm-node") (r "=0.36.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "04fs1kblzhq64cn81k4rz5m88lykljhc8k67l3yzfgp42ljw1a1j") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.37.0 (c (n "elrond-wasm-output") (v "0.37.0") (d (list (d (n "elrond-wasm-node") (r "=0.37.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "04942fxbn93ycy4civw2bmyvihagmcawlhd6xmxac2myxpa9xwhh") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-elrond-wasm-output-0.38.0 (c (n "elrond-wasm-output") (v "0.38.0") (d (list (d (n "elrond-wasm-node") (r "=0.38.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1drqqkszmmsirrpdj89xgbc5zsr1s1dvnlgbnrhcnpd2jkd4mi3v") (f (quote (("wasm-output-mode") ("panic-message"))))))

