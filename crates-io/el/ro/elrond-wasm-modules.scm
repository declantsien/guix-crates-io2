(define-module (crates-io el ro elrond-wasm-modules) #:use-module (crates-io))

(define-public crate-elrond-wasm-modules-0.25.0 (c (n "elrond-wasm-modules") (v "0.25.0") (d (list (d (n "elrond-wasm") (r "^0.25.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.25.0") (d #t) (k 2)))) (h "08hp9dh88y79x3fgvi3cbmrcjrnwl5dy7gmqw66f081b9ld5qapz")))

(define-public crate-elrond-wasm-modules-0.26.0 (c (n "elrond-wasm-modules") (v "0.26.0") (d (list (d (n "elrond-wasm") (r "^0.26.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.26.0") (d #t) (k 2)))) (h "1qv2xqhph0x0pxv1y9qr2lmvbl8qxcwxnhsgy6mbply5wgnqdprb")))

(define-public crate-elrond-wasm-modules-0.27.0 (c (n "elrond-wasm-modules") (v "0.27.0") (d (list (d (n "elrond-wasm") (r "^0.27.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.27.0") (d #t) (k 2)))) (h "11kac3xc4k3z4xcpn3w8rd64jckmfwldaihx9gzkfhjij5vk90dj")))

(define-public crate-elrond-wasm-modules-0.27.1 (c (n "elrond-wasm-modules") (v "0.27.1") (d (list (d (n "elrond-wasm") (r "^0.27.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.27.1") (d #t) (k 2)))) (h "0w3hpkp7d4fmdp9i4j4s0340vh1pblnvqzl1fmy0psis5mn3nxns")))

(define-public crate-elrond-wasm-modules-0.27.2 (c (n "elrond-wasm-modules") (v "0.27.2") (d (list (d (n "elrond-wasm") (r "^0.27.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.27.2") (d #t) (k 2)))) (h "00gahfzc01zmd9n36d97wmna19610annqy5mij9a81a49l9pzz1a")))

(define-public crate-elrond-wasm-modules-0.27.3 (c (n "elrond-wasm-modules") (v "0.27.3") (d (list (d (n "elrond-wasm") (r "^0.27.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.27.3") (d #t) (k 2)))) (h "04c61x0xbjrppxav2wbaysqmcj01rdpdxcb208pliq8xs7r10psk")))

(define-public crate-elrond-wasm-modules-0.27.4 (c (n "elrond-wasm-modules") (v "0.27.4") (d (list (d (n "elrond-wasm") (r "^0.27.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.27.4") (d #t) (k 2)))) (h "1zydv0f61ah0zcgcxnlpgr050flpn5agc8hy7vrrx5qkylpfhz7z")))

(define-public crate-elrond-wasm-modules-0.28.0 (c (n "elrond-wasm-modules") (v "0.28.0") (d (list (d (n "elrond-wasm") (r "^0.28.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.28.0") (d #t) (k 2)))) (h "0dkr64xvk476vh3ks3qpzvv7ca35nnx2311xvwi4m8wn9ircx5fl")))

(define-public crate-elrond-wasm-modules-0.29.0 (c (n "elrond-wasm-modules") (v "0.29.0") (d (list (d (n "elrond-wasm") (r "^0.29.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.29.0") (d #t) (k 2)))) (h "1f9as3pcmlq6saj350qfkkgcs18fvsinp5wnszagj3dqzmxsdg4q")))

(define-public crate-elrond-wasm-modules-0.29.1 (c (n "elrond-wasm-modules") (v "0.29.1") (d (list (d (n "elrond-wasm") (r "^0.29.0") (d #t) (k 0)))) (h "1xbihf1ydy9qdpcch0qqbmc54q0i9z62vfgw3ihgd2rilxhv7x72")))

(define-public crate-elrond-wasm-modules-0.29.2 (c (n "elrond-wasm-modules") (v "0.29.2") (d (list (d (n "elrond-wasm") (r "^0.29.2") (d #t) (k 0)))) (h "0gbk1jsb9ix236h2llcr3fz9g8vvbwqx88wn18lw3pi1ih6ql0v4")))

(define-public crate-elrond-wasm-modules-0.29.3 (c (n "elrond-wasm-modules") (v "0.29.3") (d (list (d (n "elrond-wasm") (r "^0.29.3") (d #t) (k 0)))) (h "14xb6rjf6640spjnvcmxbvl1mwdvssv02sgs93jq32mnrvjgzi43")))

(define-public crate-elrond-wasm-modules-0.30.0 (c (n "elrond-wasm-modules") (v "0.30.0") (d (list (d (n "elrond-wasm") (r "^0.30.0") (d #t) (k 0)))) (h "015yw6980kd5x8j7jzxizi873k4ahf92m4hx04ix4xbwzqn0kdi1") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.31.0 (c (n "elrond-wasm-modules") (v "0.31.0") (d (list (d (n "elrond-wasm") (r "^0.31.0") (d #t) (k 0)))) (h "0j5mij63yi3b80i155gbnz02frci5hf6n2bk94pf0hc8mjkd18ac") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.31.1 (c (n "elrond-wasm-modules") (v "0.31.1") (d (list (d (n "elrond-wasm") (r "^0.31.1") (d #t) (k 0)))) (h "0v0rmpiaf96i2q44182k5gpxcgfpcc1rdakp5z0i4v5cly7rc39a") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.32.0 (c (n "elrond-wasm-modules") (v "0.32.0") (d (list (d (n "elrond-wasm") (r "^0.32.0") (d #t) (k 0)))) (h "0py0ljq6kg87r0dx51sbbivba7r5c5bxjd8jf0zly67gnrmlb5x5") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.33.0 (c (n "elrond-wasm-modules") (v "0.33.0") (d (list (d (n "elrond-wasm") (r "^0.33.0") (d #t) (k 0)))) (h "1ss9fjx3rqvsy7cxyxx838y3ikzl7vcrpnycaanrpn99cff4xzp7") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.33.1 (c (n "elrond-wasm-modules") (v "0.33.1") (d (list (d (n "elrond-wasm") (r "^0.33.1") (d #t) (k 0)))) (h "0zx9a5d1bwvd0p05l25rh9jqralq3yr9cf56s1lpidcnh9y1g1pn") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.34.0 (c (n "elrond-wasm-modules") (v "0.34.0") (d (list (d (n "elrond-wasm") (r "^0.34.0") (d #t) (k 0)))) (h "04pcpwhi7vm564kvv4bfs0n7hb7f9ib9iq3g9n4qdg685qwas1n2") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.34.1 (c (n "elrond-wasm-modules") (v "0.34.1") (d (list (d (n "elrond-wasm") (r "^0.34.1") (d #t) (k 0)))) (h "0sj9fpwpyp2rpikm2in4gp95scv2l9427mnsqxzk7xajskn19zw6") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.35.0 (c (n "elrond-wasm-modules") (v "0.35.0") (d (list (d (n "elrond-wasm") (r "^0.35.0") (d #t) (k 0)))) (h "1imrr296wxrczdsc618ik84xxr5g506hxqyylczgqj79727dg6dy") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.36.0 (c (n "elrond-wasm-modules") (v "0.36.0") (d (list (d (n "elrond-wasm") (r "^0.36.0") (d #t) (k 0)))) (h "04j4gndc5mc3jicrlp43liywzp0jc0384yn2s5gp494rq6j24vx9") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.36.1 (c (n "elrond-wasm-modules") (v "0.36.1") (d (list (d (n "elrond-wasm") (r "^0.36.1") (d #t) (k 0)))) (h "0isas119kyggiid8vifbv6nsvgd3ra1pq6fb4sv26kq6mvbkv3w6") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.37.0 (c (n "elrond-wasm-modules") (v "0.37.0") (d (list (d (n "elrond-wasm") (r "^0.37.0") (d #t) (k 0)))) (h "0myym0sn4kr51lr0sq8p26bbh6llbnwfjfx3996iimrnna7jp7lm") (f (quote (("alloc" "elrond-wasm/alloc"))))))

(define-public crate-elrond-wasm-modules-0.38.0 (c (n "elrond-wasm-modules") (v "0.38.0") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "1kbqmmw0pmdlfdmxahfsnnhkngj38clw6sigmmwp2n2s6v94sv8s") (f (quote (("alloc" "elrond-wasm/alloc"))))))

