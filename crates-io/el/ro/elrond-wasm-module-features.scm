(define-module (crates-io el ro elrond-wasm-module-features) #:use-module (crates-io))

(define-public crate-elrond-wasm-module-features-0.6.0 (c (n "elrond-wasm-module-features") (v "0.6.0") (d (list (d (n "elrond-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.6.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.6.0") (d #t) (k 0)))) (h "0y1m7xkwgby2sx2nhiqx71mjsij9ijasbhcyak9jl86rid9zvs4r")))

(define-public crate-elrond-wasm-module-features-0.6.1 (c (n "elrond-wasm-module-features") (v "0.6.1") (d (list (d (n "elrond-wasm") (r "^0.6.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.6.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.6.1") (d #t) (k 0)))) (h "095p5zql6rqfdydvi84qvzdxvrg8ag9pd7m4qniysiz8pzvfbfmj")))

(define-public crate-elrond-wasm-module-features-0.6.2 (c (n "elrond-wasm-module-features") (v "0.6.2") (d (list (d (n "elrond-wasm") (r "^0.6.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.6.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.6.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.6.2") (d #t) (k 0)))) (h "116ynj8gbzm78rsfslxaawqfji61i70jxzn3cy3ni8vijvw485dp")))

(define-public crate-elrond-wasm-module-features-0.7.0 (c (n "elrond-wasm-module-features") (v "0.7.0") (d (list (d (n "elrond-wasm") (r "^0.7.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.7.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "08nqkdk5ar997dqq5il554i4k6d4jc0vmv4x716n5k0yj114h5kz") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.7.1 (c (n "elrond-wasm-module-features") (v "0.7.1") (d (list (d (n "elrond-wasm") (r "^0.7.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.7.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.7.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "1vmhzbjrai0ap0c2kyhbj0jp62cwm3hl1xd1lzk8w8hi81ql8qaz") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.7.2 (c (n "elrond-wasm-module-features") (v "0.7.2") (d (list (d (n "elrond-wasm") (r "^0.7.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.7.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "1a777n0989ypzc9r24hla2axj9gpb9q3lh7wpqpwlnf59dfpj63g") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.8.0 (c (n "elrond-wasm-module-features") (v "0.8.0") (d (list (d (n "elrond-wasm") (r "^0.8.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.8.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "01qvn86142x2bi6djjw7mbjx7hlhwfhgswkhrymcqwlxliiyhs6v") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.0 (c (n "elrond-wasm-module-features") (v "0.9.0") (d (list (d (n "elrond-wasm") (r "^0.9.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0qajg2nsz8vdb1m5qm4d1h28df3whwrgc9p8v1kpdpqjg6msr01g") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.1 (c (n "elrond-wasm-module-features") (v "0.9.1") (d (list (d (n "elrond-wasm") (r "^0.9.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.1") (o #t) (d #t) (k 0)))) (h "0c6bigzm380h4gz29p5mvn53hnkzvh5rmya81ia2p45gwhchz0jh") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.2 (c (n "elrond-wasm-module-features") (v "0.9.2") (d (list (d (n "elrond-wasm") (r "^0.9.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "1izx19vx77wvbgy2gxn61ipzg96n73lbsb81x09i0vslmc7w62aj") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.3 (c (n "elrond-wasm-module-features") (v "0.9.3") (d (list (d (n "elrond-wasm") (r "^0.9.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "1izdi50j76scxwml8a4bsd5hx4jdz26fypjy6hv0rymlp5nhirvd") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.4 (c (n "elrond-wasm-module-features") (v "0.9.4") (d (list (d (n "elrond-wasm") (r "^0.9.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.4") (o #t) (d #t) (k 0)))) (h "14a63nyb74pg02acf6g4v6qf35shcrx95cqk80m96n57fr5iz8l5") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.5 (c (n "elrond-wasm-module-features") (v "0.9.5") (d (list (d (n "elrond-wasm") (r "^0.9.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.5") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.5") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "0spr91dbq65lvcvhbvak6v66ljmdy4wxcd5rwhlgvs39zml80y8d") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.6 (c (n "elrond-wasm-module-features") (v "0.9.6") (d (list (d (n "elrond-wasm") (r "^0.9.6") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.6") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.6") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.6") (o #t) (d #t) (k 0)))) (h "12adks02di8andm9wphm05r3q6qi6qx64cr0rkqwxrqkag42dqj2") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.7 (c (n "elrond-wasm-module-features") (v "0.9.7") (d (list (d (n "elrond-wasm") (r "^0.9.7") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.9.7") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.9.7") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.9.7") (o #t) (d #t) (k 0)))) (h "1bn51zqrw3f9yq62sjpd2xxdpkm30xydr88awzn6s81l76j0d7ds") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.9.8 (c (n "elrond-wasm-module-features") (v "0.9.8") (d (list (d (n "elrond-wasm") (r ">=0.9.8, <0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r ">=0.9.8, <0.10.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r ">=0.9.8, <0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r ">=0.9.8, <0.10.0") (o #t) (d #t) (k 0)))) (h "1pa9iccybx8pf8dxznlcll6vlmmcizapjvn56rgbfjj2vprh709k") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.0 (c (n "elrond-wasm-module-features") (v "0.10.0") (d (list (d (n "elrond-wasm") (r "^0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1kqhkjslcvn9lgljdr5jngyrwx5qkrzkg8x1pdcwj75mr81w4mvq") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.1 (c (n "elrond-wasm-module-features") (v "0.10.1") (d (list (d (n "elrond-wasm") (r "^0.10.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1yzfip13rz92zl1jd6nkx8s5s01zg79kfln9s7nj86mics8xdarg") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.2 (c (n "elrond-wasm-module-features") (v "0.10.2") (d (list (d (n "elrond-wasm") (r "^0.10.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "02b7rkf20gkbmbczphn53imjyvirxzamnnl8l66kx5qr7d8gmx7p") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.3 (c (n "elrond-wasm-module-features") (v "0.10.3") (d (list (d (n "elrond-wasm") (r "^0.10.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "01zc1a5piwb3l49hpxzcdvz7xl5280hj6dik4vqakr7zp6zh16in") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.4 (c (n "elrond-wasm-module-features") (v "0.10.4") (d (list (d (n "elrond-wasm") (r "^0.10.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.4") (o #t) (d #t) (k 0)))) (h "189g1kh28a5867a7194k0mhfmdkvzw56cn0512gn961r57wghr0a") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.10.5 (c (n "elrond-wasm-module-features") (v "0.10.5") (d (list (d (n "elrond-wasm") (r "^0.10.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.10.5") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.10.5") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.10.5") (o #t) (d #t) (k 0)))) (h "0gzs5g1f13cgpzaj5qik5c12d37mjbm0ki6w624mwr4lpdg51m6b") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.11.0 (c (n "elrond-wasm-module-features") (v "0.11.0") (d (list (d (n "elrond-wasm") (r "^0.11.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.11.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0hs81bwbbzzh6350g50338zk3baj9v6x0p8il2ly0sih3v3xn1i1") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.12.0 (c (n "elrond-wasm-module-features") (v "0.12.0") (d (list (d (n "elrond-wasm") (r "^0.12.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.12.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0i2sg41if17ip5c2y04dv3bk86mv2hja16xrczniqqkdlzcspxm9") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.13.0 (c (n "elrond-wasm-module-features") (v "0.13.0") (d (list (d (n "elrond-wasm") (r "^0.13.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.13.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.13.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1c6ramr1v9h4z84b5sj28psp1l824izc42kyq6gms5b3f23k7a5c") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.14.0 (c (n "elrond-wasm-module-features") (v "0.14.0") (d (list (d (n "elrond-wasm") (r "^0.14.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "08k45fkjshblazs8q29ds7sgy9hmrsk214chjdafzbivp59v42zn") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.14.1 (c (n "elrond-wasm-module-features") (v "0.14.1") (d (list (d (n "elrond-wasm") (r "^0.14.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0q322xxsjr0ymq954pmqpfs84p8jiy3ilczpz66v9lybwk15hg5r") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.14.2 (c (n "elrond-wasm-module-features") (v "0.14.2") (d (list (d (n "elrond-wasm") (r "^0.14.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.14.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.14.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.14.2") (o #t) (d #t) (k 0)))) (h "00dxjgsnz8vdzwbvwsh0a2vg5abc150swm7xa5s5jm4h7pvf9311") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.15.0 (c (n "elrond-wasm-module-features") (v "0.15.0") (d (list (d (n "elrond-wasm") (r "^0.15.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.15.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.15.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "1l2mhira39z8gqqhbz52cyd2ijanln4287rydw8rm5pcm4x3xmha") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.15.1 (c (n "elrond-wasm-module-features") (v "0.15.1") (d (list (d (n "elrond-wasm") (r "^0.15.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.15.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.15.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "0b9whx44xrksnvg83dfh20zfddlycy63kbk3kvnq6x7yhwsb7g17") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.16.0 (c (n "elrond-wasm-module-features") (v "0.16.0") (d (list (d (n "elrond-wasm") (r "^0.16.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1ql8k51nkprfgb2xb9c4ahvxx2q185x04zkya7bdl1sh79k316m3") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.16.1 (c (n "elrond-wasm-module-features") (v "0.16.1") (d (list (d (n "elrond-wasm") (r "^0.16.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "13z54w1lipb22p7is0lifmjbq7fmb7sqy0c23dhvbxxhyii3q360") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.16.2 (c (n "elrond-wasm-module-features") (v "0.16.2") (d (list (d (n "elrond-wasm") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.16.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.16.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.16.2") (o #t) (d #t) (k 0)))) (h "1d5rbbg69200vzy739n9jr62l92nnd1rckqsvhimh0lwkcm2ajl6") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.17.0 (c (n "elrond-wasm-module-features") (v "0.17.0") (d (list (d (n "elrond-wasm") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.0") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.0") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "1cl5038g3987pzvsh2bmzf8khr2wjjk0s48h47s5748xgy307wwj") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.17.1 (c (n "elrond-wasm-module-features") (v "0.17.1") (d (list (d (n "elrond-wasm") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.1") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.1") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.1") (o #t) (d #t) (k 0)))) (h "10i0ll1bj54g9cxwkw80drs5n5xw9ad8m8y5fbfp8988crdgq06k") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.17.2 (c (n "elrond-wasm-module-features") (v "0.17.2") (d (list (d (n "elrond-wasm") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.2") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.2") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.2") (o #t) (d #t) (k 0)))) (h "0smrhb4j1g1vhqzb052fr900hgjjb7dkzwlysj1wk2p3vl1l5gff") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.17.3 (c (n "elrond-wasm-module-features") (v "0.17.3") (d (list (d (n "elrond-wasm") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.3") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.3") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.3") (o #t) (d #t) (k 0)))) (h "17ndm1xiq6nca6cxsw5489af1dngccl3gdisshgp6lccfrfs2vcp") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.17.4 (c (n "elrond-wasm-module-features") (v "0.17.4") (d (list (d (n "elrond-wasm") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.17.4") (d #t) (k 2)) (d (n "elrond-wasm-derive") (r "^0.17.4") (d #t) (k 0)) (d (n "elrond-wasm-node") (r "^0.17.4") (o #t) (d #t) (k 0)))) (h "03j22hx9hiljpk3kvlmpzjav51asbicvqbi8fx8ml9qy6kg2a94h") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.18.0 (c (n "elrond-wasm-module-features") (v "0.18.0") (d (list (d (n "elrond-wasm") (r "^0.18.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0957aa1diicll9ig7wdhw4hi6nzkhm23ii7b1mzfjqg9321gbiyi") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.18.1 (c (n "elrond-wasm-module-features") (v "0.18.1") (d (list (d (n "elrond-wasm") (r "^0.18.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "1ps2k750zr74g8j7hzv9swvr2mnckivhwpsiv6q3qv7kh53laggp") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.18.2 (c (n "elrond-wasm-module-features") (v "0.18.2") (d (list (d (n "elrond-wasm") (r "^0.18.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.18.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.18.2") (o #t) (d #t) (k 0)))) (h "1gaz08r5kavbqh9ni3b2p0rymf3y7m6ixj0yl5cn8crqn3ydkv35") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.19.0 (c (n "elrond-wasm-module-features") (v "0.19.0") (d (list (d (n "elrond-wasm") (r "^0.19.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1y9qcd8rm560ync3g5apgyykbmqasw1l9rs93pc6zxs25x3rg36j") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.19.1 (c (n "elrond-wasm-module-features") (v "0.19.1") (d (list (d (n "elrond-wasm") (r "^0.19.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.19.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1b0f9d67vq30wq11b7rqcvf3ii3010zak43wl392fzab4n43sljx") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.20.0 (c (n "elrond-wasm-module-features") (v "0.20.0") (d (list (d (n "elrond-wasm") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1r4h53ccaa90b22v5r67qwbvc7dncnvz6k6nvfbz4h20bdxrx06s") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.20.1 (c (n "elrond-wasm-module-features") (v "0.20.1") (d (list (d (n "elrond-wasm") (r "^0.20.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.20.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "1nnar9f4ji4z77ka6ajdlfjvxs2xc6n3iiy7zxwlixfmrcw4znyn") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.21.0 (c (n "elrond-wasm-module-features") (v "0.21.0") (d (list (d (n "elrond-wasm") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.0") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "15yb15czjiv8vjgs88h2cjlxz81rrj0szjy6xa86v1f22kfz26xc") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.21.1 (c (n "elrond-wasm-module-features") (v "0.21.1") (d (list (d (n "elrond-wasm") (r "^0.21.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.1") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "1r9bpnvgmg9gckzj8mds5pvnz0fwqilhn9h337ggjjhbp2f8jg27") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.21.2 (c (n "elrond-wasm-module-features") (v "0.21.2") (d (list (d (n "elrond-wasm") (r "^0.21.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.21.2") (d #t) (k 2)) (d (n "elrond-wasm-node") (r "^0.21.2") (o #t) (d #t) (k 0)))) (h "09ji2xyh6f9i7m29jd58qfdy7sdqvclppmkyv26azivnk6ijk8l4") (f (quote (("wasm-output-mode" "elrond-wasm-node"))))))

(define-public crate-elrond-wasm-module-features-0.22.0 (c (n "elrond-wasm-module-features") (v "0.22.0") (d (list (d (n "elrond-wasm") (r "^0.22.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.0") (d #t) (k 2)))) (h "1p59xl8dhcv8fv58nkfiivlg4hvqlgpgzsnq8yhq2zb0gpfwk1l4")))

(define-public crate-elrond-wasm-module-features-0.22.1 (c (n "elrond-wasm-module-features") (v "0.22.1") (d (list (d (n "elrond-wasm") (r "^0.22.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.1") (d #t) (k 2)))) (h "1nszzsnw2vcn83kdjcjswy5j0zvj97pc67bsx79hqjxg0qdvd310")))

(define-public crate-elrond-wasm-module-features-0.22.2 (c (n "elrond-wasm-module-features") (v "0.22.2") (d (list (d (n "elrond-wasm") (r "^0.22.2") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.2") (d #t) (k 2)))) (h "1hz0y6i5laapd494cr12j96hsfxbncjac11n243lq0h4s37p3b1z")))

(define-public crate-elrond-wasm-module-features-0.22.3 (c (n "elrond-wasm-module-features") (v "0.22.3") (d (list (d (n "elrond-wasm") (r "^0.22.3") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.3") (d #t) (k 2)))) (h "037k6kxb9hbp76wsgr7ja3sa20rd4dr5jzvzvcarkgn61q8xj76l")))

(define-public crate-elrond-wasm-module-features-0.22.4 (c (n "elrond-wasm-module-features") (v "0.22.4") (d (list (d (n "elrond-wasm") (r "^0.22.4") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.4") (d #t) (k 2)))) (h "1djlkcl6wn6if92c0mxnds0j8ii733z1818fsnv6hs6wmzgvz0ya")))

(define-public crate-elrond-wasm-module-features-0.22.5 (c (n "elrond-wasm-module-features") (v "0.22.5") (d (list (d (n "elrond-wasm") (r "^0.22.5") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.5") (d #t) (k 2)))) (h "0b5f7zxlxzlb2ac5phvhgxii3hl8yqyar0yy2zxs4amm10pk7p4i")))

(define-public crate-elrond-wasm-module-features-0.22.6 (c (n "elrond-wasm-module-features") (v "0.22.6") (d (list (d (n "elrond-wasm") (r "^0.22.6") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.6") (d #t) (k 2)))) (h "0gaydqknjpiislsf01hxb0drv5q90wppp5li3i3v81r3x0gh1x5c")))

(define-public crate-elrond-wasm-module-features-0.22.7 (c (n "elrond-wasm-module-features") (v "0.22.7") (d (list (d (n "elrond-wasm") (r "^0.22.7") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.7") (d #t) (k 2)))) (h "1jspgi4s4flmc3gdbfc4magcrc84zz9f3yp5pfvh5gcimad7b4pq")))

(define-public crate-elrond-wasm-module-features-0.22.8 (c (n "elrond-wasm-module-features") (v "0.22.8") (d (list (d (n "elrond-wasm") (r "^0.22.8") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.8") (d #t) (k 2)))) (h "1njsmxb8mz30279rsk0zcj9vnbzxdpprn4iqviammjppa91mqw8h")))

(define-public crate-elrond-wasm-module-features-0.22.9 (c (n "elrond-wasm-module-features") (v "0.22.9") (d (list (d (n "elrond-wasm") (r "^0.22.9") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.9") (d #t) (k 2)))) (h "19gknjhh8aq77p2likb51x52s5x9rd8s6mv1wjg0ynx9hxxj017p")))

(define-public crate-elrond-wasm-module-features-0.22.10 (c (n "elrond-wasm-module-features") (v "0.22.10") (d (list (d (n "elrond-wasm") (r "^0.22.10") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.10") (d #t) (k 2)))) (h "1zpczmhfscvvmxc9jg9h8bmds4k5f7jf6x7ympb3vgkwybirybgz")))

(define-public crate-elrond-wasm-module-features-0.22.11 (c (n "elrond-wasm-module-features") (v "0.22.11") (d (list (d (n "elrond-wasm") (r "^0.22.11") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.22.11") (d #t) (k 2)))) (h "1n64vdcw3ccl8rjqg48gkyyvbikfc3mfsh3mpyfnnqqpg31fzy8b")))

(define-public crate-elrond-wasm-module-features-0.23.0 (c (n "elrond-wasm-module-features") (v "0.23.0") (d (list (d (n "elrond-wasm") (r "^0.23.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.0") (d #t) (k 2)))) (h "1infi4hvyyk788cyjhpdfn5h5lrn18zdan8mnc4hsg12vvvykq8p")))

(define-public crate-elrond-wasm-module-features-0.23.1 (c (n "elrond-wasm-module-features") (v "0.23.1") (d (list (d (n "elrond-wasm") (r "^0.23.1") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.23.1") (d #t) (k 2)))) (h "0w2acviqg4xi1g3lcq9w0lq76wrcz2hqgc54fimsq739rnj9f879")))

(define-public crate-elrond-wasm-module-features-0.24.0 (c (n "elrond-wasm-module-features") (v "0.24.0") (d (list (d (n "elrond-wasm") (r "^0.24.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.24.0") (d #t) (k 2)))) (h "173kivqjkwbgal13czdn17yz2a8a3v83gz2jnnmmr2nzmfbbnpji")))

(define-public crate-elrond-wasm-module-features-0.25.0 (c (n "elrond-wasm-module-features") (v "0.25.0") (d (list (d (n "elrond-wasm") (r "^0.25.0") (d #t) (k 0)) (d (n "elrond-wasm-debug") (r "^0.25.0") (d #t) (k 2)) (d (n "elrond-wasm-modules") (r "^0.25.0") (d #t) (k 0)))) (h "1b5v8vdjagzvqfsvqvb873qkq2s82rinfmyfpfm210ar2nxcxbj6")))

