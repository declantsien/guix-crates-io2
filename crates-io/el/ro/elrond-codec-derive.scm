(define-module (crates-io el ro elrond-codec-derive) #:use-module (crates-io))

(define-public crate-elrond-codec-derive-0.4.0 (c (n "elrond-codec-derive") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w34azy7g6kc8s77zxdjhh7z5i6qnz1knqvqxjhcpa6r0m8vnvna") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.4.1 (c (n "elrond-codec-derive") (v "0.4.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cx9cc6hf571srzdyly3rqjf2468y9ba6mc1hk60gjdw7fj6d6k2") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.4.2 (c (n "elrond-codec-derive") (v "0.4.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vhr7j4ny4slj1w0wdgiaimv4s3pbrhhrm8fx2d8xhm4j4isc0c2") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.5.0 (c (n "elrond-codec-derive") (v "0.5.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g09dcz0lcnknhf610ihnij2nlrj4kgiqmf1x2nlz7jgr73xdjsb") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.5.1 (c (n "elrond-codec-derive") (v "0.5.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04lla3as0gc2f143s66mw6py0n2kcnygm5sjckyrrc12jwy385qk") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.5.2 (c (n "elrond-codec-derive") (v "0.5.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12r728h6sm9w6jx9pv6p8dlwwaq7f6fb339017d0bm6mfv7pdjbq") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.5.3 (c (n "elrond-codec-derive") (v "0.5.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "073mc1345sm1617wcf6b8d1np978ly9bby5fm9gy2x6fqvbsnxa3") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.6.0 (c (n "elrond-codec-derive") (v "0.6.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lz5s7l2wkdk6rxq3z5gkmg58n005915gdmjsnn37k02hqy5380h") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.7.0 (c (n "elrond-codec-derive") (v "0.7.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03k0mb7zng4b9caav9s8vxia93fiddh9hhsg6nc2m12vblyqcnaq") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.0 (c (n "elrond-codec-derive") (v "0.8.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wjjipsy19ynqrz38cfcvg4z0hsakc05x1wyx623k9z1ihvlzllv") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.1 (c (n "elrond-codec-derive") (v "0.8.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mfdfl2pdj8373gk7ml79yja0mcrbzldnj4n795qadfwjk1bz2q6") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.2 (c (n "elrond-codec-derive") (v "0.8.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fl81wcvhwgmnsnm0zych2rmiqhxr4l0wqnqwr9z5s9d024f26r0") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.3 (c (n "elrond-codec-derive") (v "0.8.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f07rdgy966a168sv7wsn83l77rkm628qgwcxsn1y7p5m0g4d34c") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.4 (c (n "elrond-codec-derive") (v "0.8.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "120ind97d0rq47365ay4ggqs7ar3bypqwbqp28hn1ivy6s632wph") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.8.5 (c (n "elrond-codec-derive") (v "0.8.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n1hklh5cqxlf2r7axix0ak0jinwfvvsi4h0a08c242sy0hq7d8m") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.9.0 (c (n "elrond-codec-derive") (v "0.9.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lba8msdf1ii3b96spv2aprixwkdi9bcc4hdp7vdlqkmnmib115f") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.10.0 (c (n "elrond-codec-derive") (v "0.10.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h5rlbdrfl0qpldavjqs9w44nil10bmc7q13nxixss4cfqz6hj0c") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.11.0 (c (n "elrond-codec-derive") (v "0.11.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zvg85k4ipv8kz2243szfwvvjs9d494ia1ismk226d0dq6al53wj") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.12.0 (c (n "elrond-codec-derive") (v "0.12.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y941q7j04ncij89nyaq4wz8192780hq0xj2j8nqi987rvpi6vbk") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.13.0 (c (n "elrond-codec-derive") (v "0.13.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13jpaml6byvcxqbrjqybdh3ljf89a7z2mlklbar55kiwhjhw152a") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.14.0 (c (n "elrond-codec-derive") (v "0.14.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kvmbibbr1hzi12a924bbhsh1xmr2gxjib6p58smpbh6ynkqg29a") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.15.0 (c (n "elrond-codec-derive") (v "0.15.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fxsgax1996gq938sjdxlisq2fjfafns00n544zv48nfi0vcfa64") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-elrond-codec-derive-0.16.0 (c (n "elrond-codec-derive") (v "0.16.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gj06r5vpchwi93f0bsv5ny6nl8dqipksz1gl0bwaiv3g9dwwzih") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

