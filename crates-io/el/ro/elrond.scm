(define-module (crates-io el ro elrond) #:use-module (crates-io))

(define-public crate-elrond-0.0.1 (c (n "elrond") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "2.0.*") (d #t) (k 0)))) (h "1dv2pv5zr9ybxg9xa3g0bppdyg37ckwb0gb5n020n6xxi2hmrn09")))

(define-public crate-elrond-0.0.2 (c (n "elrond") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "2.0.*") (d #t) (k 0)))) (h "1n6qmwn3pp10jac48av2g8ybrn5nj9w1qagnbiph6vhdcwz1475q")))

(define-public crate-elrond-1.0.0 (c (n "elrond") (v "1.0.0") (d (list (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors" "std"))) (d #t) (k 0)))) (h "1iy231vamf9d8m24hrc0snidx593gnq9gyn8d0ia51yjlqqxx175")))

