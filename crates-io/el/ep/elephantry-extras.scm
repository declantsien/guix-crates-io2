(define-module (crates-io el ep elephantry-extras) #:use-module (crates-io))

(define-public crate-elephantry-extras-0.1.0 (c (n "elephantry-extras") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1s5q6fqpw8s486355ab9yxsa8ihm96wpg14yl2fpp0fcjfjsd12r") (f (quote (("yem-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.1.1 (c (n "elephantry-extras") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "15gz5b34gh7ik8217yivgx9zsyidi2ry2mxdhyq6mkphpmm4c78l") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.1.2 (c (n "elephantry-extras") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1k8p84sl2zgmk76kvrx4s737yzbvv3anqk21i5wg938r8l3imbrb") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.1.3 (c (n "elephantry-extras") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "13566wfvmj5qgcvyq2v94rd8ik5jsw9i52m74jkbr53pbq4xxws7") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.1.4 (c (n "elephantry-extras") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1lc3n29cpdf4rs0ynfbsv79z8viq3f21s1b0md9mpqhbgmwiiv1h") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.2.0 (c (n "elephantry-extras") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0dwm7rz08ccrgb80wk67kjlhvpnj5jhmn6c6jz27w9faabqi82lz") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.2.1 (c (n "elephantry-extras") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0bwf3jvl99ghpca4cibyix44l2gaddawd0vbwvbf6qxkgdyi87yv") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.3.0 (c (n "elephantry-extras") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1qr8y8dcaikf2jgh0ak9789bs8vypwlfry7p0xmbx36p7zv5b68q") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.3.1 (c (n "elephantry-extras") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1bwqfj1lz1pb8qf96hm1vvrpr6c3qpqf3hr1rbj3smxaha32ip18") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.4.0 (c (n "elephantry-extras") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.20") (o #t) (d #t) (k 0)))) (h "0rz6jr4i1si9wkiiqh2mn2j5hsadk2mrllk1k30nd685sdaqij1f") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

(define-public crate-elephantry-extras-0.5.0 (c (n "elephantry-extras") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1mp32bw714prpfwcdrm22lz49pcv04hrcy8s96l0vb2675c7mpad") (f (quote (("yew-pager" "yew") ("tera-pager" "tera" "serde") ("default"))))))

