(define-module (crates-io el ep elephantry-derive) #:use-module (crates-io))

(define-public crate-elephantry-derive-0.1.0 (c (n "elephantry-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sx15g7mq9asbrj2al4i0pnqmkfrgc7m8hqga5rs7mhk5kmwf78v")))

(define-public crate-elephantry-derive-0.2.0 (c (n "elephantry-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r9gps8skvzrj4hcb7nva1j5490p6rgmm1xcdn6bjvh6n68szqs5")))

(define-public crate-elephantry-derive-0.3.0 (c (n "elephantry-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wmmyy119nslavh5qk89bz9cgymr6bmdxx1b69p3maiz3ifxs6ac")))

(define-public crate-elephantry-derive-0.4.0 (c (n "elephantry-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rdq6yf1ialxndza99picd8sdfh1kx9jd3hb314lhmhrqm9zfw0x")))

(define-public crate-elephantry-derive-0.5.0 (c (n "elephantry-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m5zizbb4cxj73q1r2wfwi96lb28wy99k03w7rz2746z3kclb6c8")))

(define-public crate-elephantry-derive-0.6.0 (c (n "elephantry-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "087zfzy4wsy5k8y2c1v0xic0ylird5r2wpv97b48pr998yfdcxyd")))

(define-public crate-elephantry-derive-0.6.1 (c (n "elephantry-derive") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kw9nqqdjgkxbx0brm71qny6qihj5c12dmhjdpkkivai62y0sqyk")))

(define-public crate-elephantry-derive-0.7.0 (c (n "elephantry-derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08pdcs21knf3f1s0nvhz55laixk3id0wihxpp1rch4zjx0jj6h2i")))

(define-public crate-elephantry-derive-0.8.0 (c (n "elephantry-derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "080sc4h68i9cjvwgnz1l22dvs4qkcz9bwqjsb3nm1kvdvy34qjf3")))

(define-public crate-elephantry-derive-1.0.0 (c (n "elephantry-derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dc18jnddw56yy5mxz47d3fqdhzhh4w4g3a9szqj3d84rzyhx7aw")))

(define-public crate-elephantry-derive-1.1.1 (c (n "elephantry-derive") (v "1.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pbbr8q1nyh4f8r3890siw52i3nxqhpnhqcws9cf5bm6n94s8ln8")))

(define-public crate-elephantry-derive-1.2.0 (c (n "elephantry-derive") (v "1.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hnf3ybxgarz69fk50izkw22kbk73d3myjf6n5bxwp2rw7j1caig")))

(define-public crate-elephantry-derive-1.3.0 (c (n "elephantry-derive") (v "1.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "183fwhqdklcfbiq9ck6av5g3jj4v0m8n8i0xav96kj3i0pnn033q")))

(define-public crate-elephantry-derive-1.3.1 (c (n "elephantry-derive") (v "1.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zfwprck9xq3mmz4zg8zpi6diwn9inmqjplbvb79f8fhyr4i1579")))

(define-public crate-elephantry-derive-1.3.2 (c (n "elephantry-derive") (v "1.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gfa9cxf0gi1jbvg4706g6p6hg6qyaflfygk3ariaaf7lv2x6xq5")))

(define-public crate-elephantry-derive-1.4.0 (c (n "elephantry-derive") (v "1.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0znwc7yv9jj9kkgihkmqnf0mzbqnf21g3xz9yvkfspcwql2d7l2i")))

(define-public crate-elephantry-derive-1.5.0 (c (n "elephantry-derive") (v "1.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dawjnzmw4767i4n4kiw6ap1kap4jsqn3qv34acvmpd4g7q7w9z9")))

(define-public crate-elephantry-derive-1.6.0 (c (n "elephantry-derive") (v "1.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15b4rkgl8bkvwwp0l1cmg5xha8znvyh78v4q02aghs7s2l3g6p5w") (f (quote (("xml") ("uuid") ("numeric") ("net") ("money") ("json") ("geo") ("default" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-1.7.0 (c (n "elephantry-derive") (v "1.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qr0q41bh1r85xgxh2f2j3ln17kyf07lj0vpkm08pcvfrnf7mas0") (f (quote (("xml") ("uuid") ("numeric") ("net") ("money") ("json") ("geo") ("default" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-2.0.0 (c (n "elephantry-derive") (v "2.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "020kzrvmrqbpwmphi3q2zwxqk11y629qb9grn33qk20wh1dqhsqy") (f (quote (("xml") ("uuid") ("numeric") ("net") ("money") ("json") ("geo") ("default" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-2.1.0 (c (n "elephantry-derive") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09zksd9j4mvynv1wf2qhg2zf121vkal629yhp5a7qra9blp0ir6x") (f (quote (("xml") ("uuid") ("numeric") ("net") ("money") ("json") ("geo") ("default" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0-beta.1 (c (n "elephantry-derive") (v "3.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0frdmspakzdd86j1i14fkdb00xl8kpayf9xcqclixzl1yij6a572") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default" "time" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0-beta.2 (c (n "elephantry-derive") (v "3.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l2l5zb9gjx865d6aximi4k0bz4x0kblglzlm6wcacdisl9h7yh6") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default" "time" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0-beta.3 (c (n "elephantry-derive") (v "3.0.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h3wminl2762gv9qbm9mcil7ggf536g9xmpiyr4ny2phiqmxhzwr") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default" "time" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0-beta.4 (c (n "elephantry-derive") (v "3.0.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vwfckcvv1v9danfiqkpvxaylgqm3anihq9fjhc9r0l3njwxfnk0") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default" "time" "uuid") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0-rc.1 (c (n "elephantry-derive") (v "3.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qqxp7z3ql41djsw7zl40lk52mlgh7iwajkggiidldxbz8liakvq") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.0.0 (c (n "elephantry-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v2r7wfag5xgkpcj46ji46snlzs2910kppb8l5nsrvfr7dmm2x6p") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.1.0 (c (n "elephantry-derive") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12yimi7mljy96bz319xqg57zas1m9m20by3smzh01yjc639qc2c8") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.1.1 (c (n "elephantry-derive") (v "3.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "122adjc39d4qlxyr5z514s54xvsgfcbg336v48d7cf8mvhp6hhsk") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.2.0 (c (n "elephantry-derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1inqm9jhzm3jaw62xmjwbrv1rgx2a0w4kzyglwhbbhap0m300yhn") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-3.3.0 (c (n "elephantry-derive") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hvcak75qjgr75lgwlv7hyinzzn95lq1qpdn6vhalhsfjha3781z") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.0-rc.1 (c (n "elephantry-derive") (v "4.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0byr4xwl0xv3vgwpvbmpm4dr9grkjh8sssfv7wffb752cjshplz2") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.0-rc.2 (c (n "elephantry-derive") (v "4.0.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18r9d12xpwdj09gqggx4yih3ys3jd80f3laqcdryprmfqgxbp9p0") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.0-rc.3 (c (n "elephantry-derive") (v "4.0.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16p21xx8dyfmny41n0px7b1jlwv4rny4wc23wyzs5dzxzqjxrypz") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.0-rc.4 (c (n "elephantry-derive") (v "4.0.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bfq0hi5afqp9qr039igyrqbp8d4kbykxc2kphzq02wl340s9191") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.0 (c (n "elephantry-derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1drfz694q3yzqsphr8s1mrf2j4a2qd22yzmqxr2msw9annw80dk6") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

(define-public crate-elephantry-derive-4.0.1 (c (n "elephantry-derive") (v "4.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ycdgzzd2g5glbvyhiwj6q21b736m9i12jczd8vljayw0lh4hl3z") (f (quote (("xml") ("uuid") ("time") ("numeric") ("net") ("money") ("ltree") ("json") ("geo") ("default") ("date") ("bit"))))))

