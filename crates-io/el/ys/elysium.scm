(define-module (crates-io el ys elysium) #:use-module (crates-io))

(define-public crate-elysium-0.1.0 (c (n "elysium") (v "0.1.0") (h "1rl0xiqbw5j0iaw0778aif45varqzwr7lp9lnnpgy03sw4lkqn2k")))

(define-public crate-elysium-0.1.1 (c (n "elysium") (v "0.1.1") (h "0skkxzbk8d1j65fqkyjgjrqr8hln60pcycrn82n3lisyjb539v7i")))

