(define-module (crates-io el di eldiron-cli) #:use-module (crates-io))

(define-public crate-eldiron-cli-0.0.1 (c (n "eldiron-cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0ng46iip0vx0b5xmj8g2ds7f0lbxbwdx8k20x95gilsfxipkqb4b")))

(define-public crate-eldiron-cli-0.0.2 (c (n "eldiron-cli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0xfa6lc8zlkjvwmxljfyq5b00dplzzj003p6a152c9vy1j7wg6r2")))

(define-public crate-eldiron-cli-0.0.3 (c (n "eldiron-cli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1r2s235mz3ipfkgipafxvcbpmqkzy00r5hvjhn1h0w09m2acz30b")))

(define-public crate-eldiron-cli-0.0.4 (c (n "eldiron-cli") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0s50z4yvmy0gf30jlnhd0byzlkaalhqy6lmhm06y10031xjqhh9f")))

(define-public crate-eldiron-cli-0.0.5 (c (n "eldiron-cli") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0i9qrh2ppcjfj0sfn3gxglpkikk0y3h6jp6n0j62jqg0ghnyfbbb")))

(define-public crate-eldiron-cli-0.0.6 (c (n "eldiron-cli") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0zzrrjk5fjzrq2gkqcspih341qgxf9n555dv5q2za5hxyja3wvc1")))

(define-public crate-eldiron-cli-0.1.0 (c (n "eldiron-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0skkjbn7lfl6cklkaf1x9n97xxa6fsyg5cnilf9n56m0bv8z50y3")))

