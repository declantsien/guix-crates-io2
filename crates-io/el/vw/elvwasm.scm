(define-module (crates-io el vw elvwasm) #:use-module (crates-io))

(define-public crate-elvwasm-1.0.0 (c (n "elvwasm") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "json_dotpath") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wapc") (r "^1.0.0") (d #t) (k 0)) (d (n "wapc-guest") (r "^1.0") (d #t) (k 0)))) (h "1vqswrpw4r5043rv49ii5g69fca5a1cw59i79yzhc3jlj5dh9wn4") (r "1.56.0")))

