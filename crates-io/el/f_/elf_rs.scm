(define-module (crates-io el f_ elf_rs) #:use-module (crates-io))

(define-public crate-elf_rs-0.1.0 (c (n "elf_rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0hqlqyy7x2b4xq4jx736qvmhq2v744rwcyrbfa5ng1r5vdr91ak4")))

(define-public crate-elf_rs-0.1.1 (c (n "elf_rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)))) (h "15nci3wxx35dgcdk9isis7hkyc0p2jsdgx1mslbwws2dqka8s316")))

(define-public crate-elf_rs-0.1.2 (c (n "elf_rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1ikfxkbjfakryf0d119zncvfmy2j4z2n4axjr0bqgdd3l5ggwql6")))

(define-public crate-elf_rs-0.1.3 (c (n "elf_rs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0f1yg1d1hxcjc7zvymgws1zv54m98nxw9c14jnjc41gvcai4r811")))

(define-public crate-elf_rs-0.2.0 (c (n "elf_rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "00l6767b6hqlnxya15p3ganig6d4blv1bb1kg2yvql1m36szn8hq")))

(define-public crate-elf_rs-0.3.0 (c (n "elf_rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1s1kb9g59gmj4vwyxj77v2xsvh8ay8y987ahnb0760zv1vvj7xww")))

(define-public crate-elf_rs-0.3.1 (c (n "elf_rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1pxgmss8m4p1lg71l1vzviqkyfps5zn2g4lzwrff5nh7dc5p2kc9")))

