(define-module (crates-io el f_ elf_parser) #:use-module (crates-io))

(define-public crate-elf_parser-0.1.0 (c (n "elf_parser") (v "0.1.0") (h "1xsi6s22npdhx6160kdbc1j2mwady3bhhmxmawrfp226xj94lxzv")))

(define-public crate-elf_parser-0.1.1 (c (n "elf_parser") (v "0.1.1") (h "1p3f1mjc0v73hczyvhniiyb3lksdwi8m0a3qfbqzq8xiz70sp9bl")))

