(define-module (crates-io el fc elfcat) #:use-module (crates-io))

(define-public crate-elfcat-0.1.0 (c (n "elfcat") (v "0.1.0") (h "12skha5qkn93fna5gndr40xd90xhi6ssr5s3p3jwqfwdp9lrycr5") (y #t)))

(define-public crate-elfcat-0.1.1 (c (n "elfcat") (v "0.1.1") (h "07csyl7p9jzhrpjpwa4qvnfy8qwa970v24m0342p3lppa0860hg5") (y #t)))

(define-public crate-elfcat-0.1.2 (c (n "elfcat") (v "0.1.2") (h "0ssl4xbm742bqsjsbzxl2d0jf22qasb8aq57w1mvjla554c89970")))

(define-public crate-elfcat-0.1.3 (c (n "elfcat") (v "0.1.3") (h "0kazh1j0b8fjmxi2bh47sf5kzsyg51na7y65pk4sg2jc93sw3jjq")))

(define-public crate-elfcat-0.1.4 (c (n "elfcat") (v "0.1.4") (h "0hs4fajsc2vdkn3kcfprpm86k72zncyz1q2ba1qziin8rxk28pnc")))

(define-public crate-elfcat-0.1.5 (c (n "elfcat") (v "0.1.5") (h "1yq4b9d2mq6q0qzbsiaqln09a3w544p18skmvk2l950clvn2bgim")))

(define-public crate-elfcat-0.1.6 (c (n "elfcat") (v "0.1.6") (h "1yfm6zyijkjjki13m30b2dvnmqm1n21k2i06rj5pwxhds0fdkmna")))

(define-public crate-elfcat-0.1.7 (c (n "elfcat") (v "0.1.7") (h "1nqkbrmki18idipls40x7qczv9h9dfl3lh9rhjyvwhn5js4i3432")))

(define-public crate-elfcat-0.1.8 (c (n "elfcat") (v "0.1.8") (h "0qqp1f12f4rddgph7ymhyw4h1bvc2ll79dqqdxf8vv6dr5qa223w")))

