(define-module (crates-io el fl elflib_macros) #:use-module (crates-io))

(define-public crate-elflib_macros-0.1.0 (c (n "elflib_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1ax57r0p5hsh194av2rq978xgg0fsmzab1id96100zmp4k7sx8m1")))

(define-public crate-elflib_macros-0.1.1 (c (n "elflib_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "196yd0pq3bq5rdaa1k1n1fkcknjqwypcr3n333r5rnvg10v00dbp")))

(define-public crate-elflib_macros-0.1.2 (c (n "elflib_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "19161wqsf0kp157vi5d44nwc1j51g0p3mhzbcvs99b3pi2g37ncf")))

(define-public crate-elflib_macros-0.1.3 (c (n "elflib_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0ajdm7f95schhn943i251zjvd5gj77fn7kzb4nl3mn9v83ywszf3")))

