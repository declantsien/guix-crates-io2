(define-module (crates-io el fl elfloader) #:use-module (crates-io))

(define-public crate-elfloader-0.0.1 (c (n "elfloader") (v "0.0.1") (h "10grrywsplibq0ch7csyiqy143kzkvghg22bd7mpg7p0y7y0n97h")))

(define-public crate-elfloader-0.0.2 (c (n "elfloader") (v "0.0.2") (h "0p01i7iwqhiyrdqlbjkn5r1iiv69qa9rfzgbf8h248w5x2kinvx4")))

(define-public crate-elfloader-0.0.3 (c (n "elfloader") (v "0.0.3") (h "1z6qhf4y3lq3v5sk2basrwsimgwqb8ypkwx96sri66g1vihksv24")))

(define-public crate-elfloader-0.0.4 (c (n "elfloader") (v "0.0.4") (h "0l0c9pjrd5fjy8p1rzr98qnrj9hlm67i8cp6hjzl738gvzaq87y8")))

(define-public crate-elfloader-0.0.5 (c (n "elfloader") (v "0.0.5") (h "14cs36qs3ws2b989xx84h0j6dmivlgks3ljzbsmydmpzaym9m1ry")))

(define-public crate-elfloader-0.9.0 (c (n "elfloader") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7") (d #t) (k 0)))) (h "0disacc5baz9zp5yvx19325rkhfr885lvbknc7dss7aa0j3m4fyd")))

(define-public crate-elfloader-0.10.0 (c (n "elfloader") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7") (d #t) (k 0)))) (h "0n6zgfg0q59q5kyrrk52a3rhjb6mxgalgl7k1jnj8h36v115gasx")))

(define-public crate-elfloader-0.11.0 (c (n "elfloader") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7") (d #t) (k 0)))) (h "14m1nzcgjjwqzrd29hfcivnzz59g6d1rjlzbkqj4n6dla7jil55i")))

(define-public crate-elfloader-0.12.0 (c (n "elfloader") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7") (d #t) (k 0)))) (h "1ixibzmkx5laxzw4lq2dd0d57fz8843pn3ji20wpv87c5iafys73")))

(define-public crate-elfloader-0.13.0 (c (n "elfloader") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.7") (d #t) (k 0)))) (h "16pqbm2qqkaf0khqj1qrc82fxh9yjzqjsysvlnp8bxyj4spimqki")))

(define-public crate-elfloader-0.14.0 (c (n "elfloader") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.8") (d #t) (k 0)))) (h "017z8r0q8kh8idn9kdbpdjfdijl997l7bzbvrfizafqgsnlxcjd2")))

(define-public crate-elfloader-0.15.0 (c (n "elfloader") (v "0.15.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.8") (d #t) (k 0)))) (h "10cw47j2haqi5masrrh780ax4khy2v48wfc5hb01pqbbvkaipq13")))

(define-public crate-elfloader-0.16.0 (c (n "elfloader") (v "0.16.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.8") (d #t) (k 0)))) (h "0fwgwww6xl2lqz0ajhhbw1jffihv3z7jkv63b6n3pv7qbg9ihyva")))

