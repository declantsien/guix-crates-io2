(define-module (crates-io el fl elflib) #:use-module (crates-io))

(define-public crate-elflib-0.1.0 (c (n "elflib") (v "0.1.0") (d (list (d (n "binary_serde") (r "^1.0.22") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "elflib_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "116m8n7p2k464lzyd8rzf6hrknam8af6rkdpa0y1vbncfhmcps35") (f (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1.1 (c (n "elflib") (v "0.1.1") (d (list (d (n "binary_serde") (r "^1.0.22") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "elflib_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0n5sj2nsvw9f0drci1q4plr0by7zfzl9dwk5psp95c8k5m7k73gr") (f (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1.2 (c (n "elflib") (v "0.1.2") (d (list (d (n "binary_serde") (r "^1.0.22") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "elflib_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0vdzwv2yhir4r65rcqrw8av7sxb9nfynhb6hwzwx2h91jbljvrx5") (f (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1.3 (c (n "elflib") (v "0.1.3") (d (list (d (n "binary_serde") (r "^1.0.22") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "elflib_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1873gdiwqz69k6ghwnmxilnk4lbbcxkf87l1q0b5dafcskgrd6wb") (f (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

