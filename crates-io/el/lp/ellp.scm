(define-module (crates-io el lp ellp) #:use-module (crates-io))

(define-public crate-ellp-0.1.0 (c (n "ellp") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "fern") (r "^0") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cy1w73gqaqadzmri3ysv7bp0qh3qnjccm13q3zndlw0s47bnich")))

(define-public crate-ellp-0.1.1 (c (n "ellp") (v "0.1.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "fern") (r "^0") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xi7zvlma61ps72dhsgflbyvwgami2acf0yx2kv6av8w5a55d60n")))

(define-public crate-ellp-0.1.2 (c (n "ellp") (v "0.1.2") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "fern") (r "^0") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18xj7cffnabmfd6wkpdsd5kpr5yy0kfh6ka9f06l9kw5mbgjzg8g") (f (quote (("mps") ("benchmarks" "mps"))))))

(define-public crate-ellp-0.2.0 (c (n "ellp") (v "0.2.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "fern") (r "^0") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q6cwq1mvfm0978xmay34jplg7w7i4av7kcz7z5qqcxvynyah2x8") (f (quote (("mps") ("benchmarks" "mps"))))))

