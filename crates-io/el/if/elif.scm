(define-module (crates-io el if elif) #:use-module (crates-io))

(define-public crate-elif-0.0.1 (c (n "elif") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1f02ks1irvr4b5jdbv7yfzv16mq0ah9s7hb47y855jdwakb7kqga")))

