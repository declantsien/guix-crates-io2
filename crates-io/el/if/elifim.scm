(define-module (crates-io el if elifim) #:use-module (crates-io))

(define-public crate-elifim-0.1.0 (c (n "elifim") (v "0.1.0") (h "040bpdiymc8vp0fixjj70jkql85mp44vv75fwghl36j2gzgialss")))

(define-public crate-elifim-0.2.0 (c (n "elifim") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "1hikiq3yjrw0r28nmh58kxkq0iihv1m610s4bx4nz1bs7m1c0spn")))

