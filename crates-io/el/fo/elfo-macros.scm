(define-module (crates-io el fo elfo-macros) #:use-module (crates-io))

(define-public crate-elfo-macros-0.1.0 (c (n "elfo-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "037kci2g0hqbc0xwlv90vdxq8yfqbhy5hvkxjr510nhwvx23zdzj")))

(define-public crate-elfo-macros-0.1.1 (c (n "elfo-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rjsvrxh7x4pbgcs7zszlvv1y5y6g7yh63mgzrq9kcd59k9szikk")))

(define-public crate-elfo-macros-0.1.2 (c (n "elfo-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qz57jk41qrv6x3l1l8amp1sszl8jvymm6l9jabnrkpmy3dprbkm")))

(define-public crate-elfo-macros-0.1.3 (c (n "elfo-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ipv8hhvn8qwsj1b4d3w6m2xrqh3hhwcghjmq89rmp8s4aa7sbms")))

(define-public crate-elfo-macros-0.1.4 (c (n "elfo-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06p6p4fffhy8hnr83fbdkvwq0zp6s2iv07nax4i6j4rjfz3nv7pi")))

(define-public crate-elfo-macros-0.1.5 (c (n "elfo-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06xkbcx8y3242z8jsfz68a2v40zdaf7zvn4rgnmp6hlm3x1icjdn")))

(define-public crate-elfo-macros-0.1.6 (c (n "elfo-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gjyrczv3mzzawck0500f1ylvf04swbgl4ndqndrnkjb4ikz8dxj")))

(define-public crate-elfo-macros-0.1.7 (c (n "elfo-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01pqvp5bp39fs0wqxjk5q71rbx6a5j33jipsg5n968ifv0k3sg0l")))

(define-public crate-elfo-macros-0.1.8 (c (n "elfo-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g7d7h81fddhfqsx17k1g97annhvk9fvzwai8i3zknj71vhnn49m")))

(define-public crate-elfo-macros-0.1.9 (c (n "elfo-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08an44y4nr498zhlg1j7l978a4p1kzj0qimwxk8xc1jj9csclizc")))

(define-public crate-elfo-macros-0.1.10 (c (n "elfo-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01sjdnn4j92l59vn3k8libhswzpvm042crar50yq2di9p0xgi2mi")))

(define-public crate-elfo-macros-0.1.11 (c (n "elfo-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f8wfb0lv7a1jg6srx9qycz2k5vv0lvj25bvq7sc8y32kx1n6c96")))

(define-public crate-elfo-macros-0.1.12 (c (n "elfo-macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xd5q5364w61x2vf3p2vj6x1lbhgrk9q3n53yd3zl89sswnjyxmk")))

(define-public crate-elfo-macros-0.1.13 (c (n "elfo-macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cfvbb8n468f070l30nfh41wql6sk2afwbl7w5y05ymlk1sw2r8l")))

(define-public crate-elfo-macros-0.1.14 (c (n "elfo-macros") (v "0.1.14") (d (list (d (n "elfo-macros-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1qrdapxrabnd63nf0149zlzdx4qvgl33zq6gs8arsfzsicwy35f6")))

(define-public crate-elfo-macros-0.1.15 (c (n "elfo-macros") (v "0.1.15") (d (list (d (n "elfo-macros-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0dkw93d1v2h0hd4y5hbldvaqvnydjiy6q43mgbdq0vfwyf9dmmkr")))

(define-public crate-elfo-macros-0.2.0-alpha.0 (c (n "elfo-macros") (v "0.2.0-alpha.0") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "04h7mvgj5savipxd0ndypiy4fzl7psb6myhim8x9gz87hniarv0g")))

(define-public crate-elfo-macros-0.2.0-alpha.1 (c (n "elfo-macros") (v "0.2.0-alpha.1") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0950xlmh6q876qy34185l339z3l3zjxb7di518qhifxjx0gydi9q")))

(define-public crate-elfo-macros-0.2.0-alpha.2 (c (n "elfo-macros") (v "0.2.0-alpha.2") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0j07wddp99gl1phvxcw1acvkpd9ky7fjq8l58lk5gymbqji01m4j")))

(define-public crate-elfo-macros-0.2.0-alpha.3 (c (n "elfo-macros") (v "0.2.0-alpha.3") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1pygp72djl56ij0iwbcd443lklpmy1kqbgyrg2p622nxspq22lh8")))

(define-public crate-elfo-macros-0.2.0-alpha.4 (c (n "elfo-macros") (v "0.2.0-alpha.4") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "03hsnk9yib2i450103g271gf4dl04i360a070nk7h1s03ll3rfkj")))

(define-public crate-elfo-macros-0.2.0-alpha.5 (c (n "elfo-macros") (v "0.2.0-alpha.5") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1cgkfn2kf8b10h24xzv92ghcafz0b58n1v9cvsy8bs1ncrv1z80n") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.6 (c (n "elfo-macros") (v "0.2.0-alpha.6") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0da01wbpwpa6amiixi05k3xca1gp3sdznf9cmv2z1i8r209ap90f") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.7 (c (n "elfo-macros") (v "0.2.0-alpha.7") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1z8hl1kvgpcizq8cc2prjxbfbj9hld9a4hanamw2ashkdajkyjrw") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.8 (c (n "elfo-macros") (v "0.2.0-alpha.8") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0yq5m6xx06vl16li45dxvl5b6qlza5ykjj8x03lx3nshlgvp3ixg") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.9 (c (n "elfo-macros") (v "0.2.0-alpha.9") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "19qpv3xz9gihkzp3r5vjc657fad2jgi8z30paa39fs1gmx0abg1m") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.10 (c (n "elfo-macros") (v "0.2.0-alpha.10") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0cz4xks1j5r8rm544y8l0v84yrc0z5nr94qqr364414kjicnin3r") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.11 (c (n "elfo-macros") (v "0.2.0-alpha.11") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0dm71k5rs77v6sc15l2c8knqc6j3mqgfdwdnm1qvxv6nw96zlc7i") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.12 (c (n "elfo-macros") (v "0.2.0-alpha.12") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0w05hxxh47g912p5n9kadygml57dn87z2dqy6wn8nspi2s11wlyv") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.13 (c (n "elfo-macros") (v "0.2.0-alpha.13") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0chjgki65vcjy0r7gizyyp98lhqdb9821snwwdcd0dmmrcz7dzhb") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.14 (c (n "elfo-macros") (v "0.2.0-alpha.14") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1zajz5mx4c2568q121asaa3cin254dv1ddi0iw8c1zadpg814k2n") (f (quote (("network" "elfo-macros-impl/network"))))))

(define-public crate-elfo-macros-0.2.0-alpha.15 (c (n "elfo-macros") (v "0.2.0-alpha.15") (d (list (d (n "elfo-macros-impl") (r "^0.2.0-alpha.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "0vzqxz4623bv058pv8isggir0nx33b6c5mpxi0mg64qk2xib0cqk") (f (quote (("network" "elfo-macros-impl/network"))))))

