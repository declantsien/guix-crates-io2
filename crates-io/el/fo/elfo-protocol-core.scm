(define-module (crates-io el fo elfo-protocol-core) #:use-module (crates-io))

(define-public crate-elfo-protocol-core-0.1.0 (c (n "elfo-protocol-core") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.22.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22.0") (d #t) (k 0)))) (h "04wgmj0j94agslp61679ik4jclky2c5f83rf0qdn2ysr3rzahbjc") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

