(define-module (crates-io el fo elfo-macros-impl) #:use-module (crates-io))

(define-public crate-elfo-macros-impl-0.1.0 (c (n "elfo-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19336xhfm2fpxrh2qpr825wcbjkdfqvjly5cnxgc6j7nrg3257sb")))

(define-public crate-elfo-macros-impl-0.1.1 (c (n "elfo-macros-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05914b81k98sihhgnj2s5sk9hyb2rhhwrx5gaf06n5q102i4l07q")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.0 (c (n "elfo-macros-impl") (v "0.2.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ayr4nqn37risd8wlsd6cc7fa891dmrc1hz3hvhyk1nbg9l3za44")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.1 (c (n "elfo-macros-impl") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s9b32cbixzs24j6h3mfizd26gcqhvhcjv1fxi9f7wi8sm78z2ga")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.2 (c (n "elfo-macros-impl") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rwmx6yykc6q18ww9hsjl24yl9li3kd6jw3zsb7aiy1yn6pa1vpr")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.3 (c (n "elfo-macros-impl") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cjx1fq5zzhkg4wirdc4p5142r7f4d4dxbqcf1lnn1kd1i8w2z58")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.4 (c (n "elfo-macros-impl") (v "0.2.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jmj4xjz22azd557q4d2qkr3d3cgbd89vwds1g4pi3p22ipm1wxr")))

(define-public crate-elfo-macros-impl-0.2.0-alpha.5 (c (n "elfo-macros-impl") (v "0.2.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09yi5kr04l7pnz15ivhrys4rk94kdqv28g932g7gwljn8n8ycyrj") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.6 (c (n "elfo-macros-impl") (v "0.2.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fvlarw4fkcmkampidl7rsd9igcylpcjhg3ylfysxd738gc00hcz") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.7 (c (n "elfo-macros-impl") (v "0.2.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12d1yq6zpw2ak0pq25pgl9hzibqvrdmrqb4b7rniiv4cpf5fibrc") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.8 (c (n "elfo-macros-impl") (v "0.2.0-alpha.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0im3pcyk814dvi4r442fn785djsrlivbp7nxfl3p42kfi9rhz2bc") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.9 (c (n "elfo-macros-impl") (v "0.2.0-alpha.9") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fhypp4hfw4hrck87418clixw5ygcang8lgfnikka1wphqwin28f") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.10 (c (n "elfo-macros-impl") (v "0.2.0-alpha.10") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v01h1hz0sn4rhizhw6wk98q2179cqih0v46l4qghjcc0fzgd8gz") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.11 (c (n "elfo-macros-impl") (v "0.2.0-alpha.11") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04w1mhc04yigcsaw7kcqi32wpmr1lvrp6laq5b5q8hzimnas36cp") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.12 (c (n "elfo-macros-impl") (v "0.2.0-alpha.12") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14i0cwiap0c7lm96x85vcqpbmaqbr91q01a2mhh7g9gyh7w3rq02") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.13 (c (n "elfo-macros-impl") (v "0.2.0-alpha.13") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c23xg1m4p5718py0qnxnrkq1572cbagsi2yxa7ba2l04kp3jckv") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.14 (c (n "elfo-macros-impl") (v "0.2.0-alpha.14") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sc053bhj41f8w8jk6qjcal5xm5zhjmz4vg8jljmf8aaw674s9wm") (f (quote (("network"))))))

(define-public crate-elfo-macros-impl-0.2.0-alpha.15 (c (n "elfo-macros-impl") (v "0.2.0-alpha.15") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wkszf2v3hzi9pn1qpv31pmsn826n3zcb7dpsz1jbb33c8655gvx") (f (quote (("network"))))))

