(define-module (crates-io el fo elfo-utils) #:use-module (crates-io))

(define-public crate-elfo-utils-0.1.0 (c (n "elfo-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "0y9yqfydabcy41dm8q93xdzk2gyvl0512fr00pa2lly249zf28kx")))

(define-public crate-elfo-utils-0.1.1 (c (n "elfo-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.9.2") (d #t) (k 0)))) (h "109jn8x6xsqg0xhgpahzz0lq9q80kp3i3nvwflwp3j720a7vsypa")))

(define-public crate-elfo-utils-0.2.0 (c (n "elfo-utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.9.2") (d #t) (k 0)))) (h "046yrjqg3n42v6ji8s0a5d17psbcc5m36yzwg6y9kn2xwgmpc8zf")))

(define-public crate-elfo-utils-0.2.1 (c (n "elfo-utils") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.10") (d #t) (k 0)))) (h "04iyxj7sbfs6p4knkx8nbfxrb5d7khp989zkcn3kf7frlzf45hm5")))

(define-public crate-elfo-utils-0.2.2 (c (n "elfo-utils") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.11") (d #t) (k 0)))) (h "0x4hygkn5qn6ky7a6wr6ii1hglndg273ngm7hsl8p45qvfhhrhf6")))

(define-public crate-elfo-utils-0.2.3 (c (n "elfo-utils") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.11") (d #t) (k 0)))) (h "0j8x32xxi4rs4nr7d69fy9bjn3f6d1irkmb8xgsryhzqvbdvqi6q")))

(define-public crate-elfo-utils-0.2.4 (c (n "elfo-utils") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.12") (d #t) (k 0)))) (h "1qm7z91da3277ncgxfh0argrmb7n4v2ywnp57782b62v8w2hq5w1") (f (quote (("test-util"))))))

(define-public crate-elfo-utils-0.2.5 (c (n "elfo-utils") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "quanta") (r "^0.12") (d #t) (k 0)))) (h "1b6jwnjgv6pcj5nd96z7vfqgw3kjy3pmdj5p2fpca459ih8d2w2c") (f (quote (("test-util"))))))

