(define-module (crates-io el ga elgamal-curve25519) #:use-module (crates-io))

(define-public crate-elgamal-curve25519-0.1.0 (c (n "elgamal-curve25519") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "~1.1") (d #t) (k 0)) (d (n "digest") (r "~0.8") (d #t) (k 0)) (d (n "rand_core") (r "~0.3") (d #t) (k 0)) (d (n "rand_os") (r "~0.1") (d #t) (k 0)) (d (n "sha2") (r "~0.8") (d #t) (k 0)) (d (n "subtle") (r "~2.1") (d #t) (k 0)) (d (n "typenum") (r "~1.10") (d #t) (k 0)))) (h "0y6swc24yl89vdlyjbdsgcy296h00gy47c8i0yb55rn7h8icrqaz")))

(define-public crate-elgamal-curve25519-0.2.0 (c (n "elgamal-curve25519") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "~1.1") (d #t) (k 0)) (d (n "digest") (r "~0.8") (d #t) (k 0)) (d (n "rand_core") (r "~0.3") (d #t) (k 0)) (d (n "rand_os") (r "~0.1") (d #t) (k 0)) (d (n "sha2") (r "~0.8") (d #t) (k 0)) (d (n "subtle") (r "~2.1") (d #t) (k 0)) (d (n "typenum") (r "~1.10") (d #t) (k 0)))) (h "1l0nb5byhm9pvp31nsdl37wccmd0r29w7nkbjhm1d96yi2yqqswm")))

