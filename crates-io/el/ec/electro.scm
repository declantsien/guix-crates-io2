(define-module (crates-io el ec electro) #:use-module (crates-io))

(define-public crate-electro-0.1.0 (c (n "electro") (v "0.1.0") (d (list (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)))) (h "1xdxc90bm8s7cgibjgaz09mk1fg2y3a8ws8kj2nzin8dfsq5m31w")))

(define-public crate-electro-0.2.0 (c (n "electro") (v "0.2.0") (d (list (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "10cf5fqjr4m6z4zqj3i48rf9h2xfszdqfv8rcrnjw3rjii4y95mi")))

(define-public crate-electro-0.3.0 (c (n "electro") (v "0.3.0") (d (list (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "09g947hg3ncq6l7mjscvyji5kca3bxaxgmaxj8dg2z57rr8ymf1s")))

(define-public crate-electro-0.4.0 (c (n "electro") (v "0.4.0") (d (list (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "0xdlbmbc3ggxxckx1ixw1as0z1plzbwh5zl30jnm1bpi6qbq4xdw")))

(define-public crate-electro-0.4.1 (c (n "electro") (v "0.4.1") (d (list (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "0dp9lzq2hybrpf759x7fmgjsdr6j0kxy1clg7x43i7sr14w9s73q")))

(define-public crate-electro-0.5.0 (c (n "electro") (v "0.5.0") (d (list (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "10rbbs1pz8k6d3kq456dzg87azx5v7ywy3ryvhbzwiq71d2frpl2")))

(define-public crate-electro-0.6.0 (c (n "electro") (v "0.6.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "1ha5yad6gsvga8hzm8dafjwgb9q6gghflr6d4795ijwlqda18kqr")))

(define-public crate-electro-0.6.1 (c (n "electro") (v "0.6.1") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "0inx0k49c24km2qgnmjm0vqmc6dwsjmr6ndxnpj571w1bd7njzan")))

(define-public crate-electro-0.6.2 (c (n "electro") (v "0.6.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "1fg2mf4xrmpi7sgxv3c1n27p550zrrs8g82kpinbcldll74m2f44")))

(define-public crate-electro-0.7.0 (c (n "electro") (v "0.7.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "0j8f1sfinlfa152qdq0j4r5rsqgbvjypxnp7av270hkmnqgj0b7i")))

(define-public crate-electro-0.8.0 (c (n "electro") (v "0.8.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "02ikmq92kzxgxsjyvbra65hbw31g7n0977b55xwwssgcigq365z4")))

(define-public crate-electro-0.9.2 (c (n "electro") (v "0.9.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.6") (d #t) (k 0)))) (h "0p6cwf1d0ibjawm7r8n25id6yxxf48qgp5gqx878wxkp6q46xmsj")))

(define-public crate-electro-0.9.3 (c (n "electro") (v "0.9.3") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)))) (h "0jsmcw2glj52l6zy7pcxnqidrcv1vkzglqar8y9cj303a06jn6rc")))

