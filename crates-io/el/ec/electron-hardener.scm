(define-module (crates-io el ec electron-hardener) #:use-module (crates-io))

(define-public crate-electron-hardener-0.1.0 (c (n "electron-hardener") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)))) (h "14235z0h7ga9f5wxyysm3874z36nmaf4iz3rsi9i2153fwkm5bbj")))

(define-public crate-electron-hardener-0.2.0 (c (n "electron-hardener") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)))) (h "1zsaz36xjf64cf9q3saym9dmiaf6ccxiqpjc5yljii9g2qwfcrgq")))

(define-public crate-electron-hardener-0.2.1 (c (n "electron-hardener") (v "0.2.1") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)))) (h "1afas2wmihbssn0db1d5z2qwxbl1580mhki7ap9lbpq2gw1ngm91")))

(define-public crate-electron-hardener-0.2.2 (c (n "electron-hardener") (v "0.2.2") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (k 0)))) (h "14j9ar3bip9xaja5hldw6375fm5rqpz5kxa8hhf7vk534ha3w4m5")))

