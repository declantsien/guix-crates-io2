(define-module (crates-io el ec electrum2descriptors) #:use-module (crates-io))

(define-public crate-electrum2descriptors-0.1.0 (c (n "electrum2descriptors") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.25.2") (d #t) (k 0)))) (h "1gvjpr6wykg9xza6iwifc4lis0cip194jbyh0f0c5gp07ldmy7ay")))

(define-public crate-electrum2descriptors-0.2.0 (c (n "electrum2descriptors") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.25.2") (d #t) (k 0)) (d (n "miniscript") (r "^3.0.0") (d #t) (k 2)))) (h "0r8hqjx9agdjdbx9gi553qwbf3jf3ym122dg9icjwiz8mwxs8x1m")))

(define-public crate-electrum2descriptors-0.3.0 (c (n "electrum2descriptors") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.25.2") (d #t) (k 0)) (d (n "miniscript") (r "^3.0.0") (d #t) (k 2)))) (h "062bkabdiznfmwmknik3nr8chjlv1833h5k25l212c0lkw70m100")))

(define-public crate-electrum2descriptors-0.4.0 (c (n "electrum2descriptors") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.26") (d #t) (k 0)) (d (n "miniscript") (r "^5") (d #t) (k 2)))) (h "115v0vmcn9pq987wn6xzxb9qzk6d2nnfxmwqkijrwb3pv9na149b")))

(define-public crate-electrum2descriptors-0.4.1 (c (n "electrum2descriptors") (v "0.4.1") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)) (d (n "miniscript") (r "^6") (d #t) (k 2)))) (h "0l7k0h3i83sl7qy03sipk2hq290v3xcsqg88my69h304dpk9zfxi")))

(define-public crate-electrum2descriptors-0.4.2 (c (n "electrum2descriptors") (v "0.4.2") (d (list (d (n "bdk") (r "^0.15") (d #t) (k 2)) (d (n "bitcoin") (r "^0.27") (d #t) (k 0)) (d (n "miniscript") (r "^6") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1cwcv5syaxmljrvw6dgjwkkrr1mglj3s20yr5a0n0k21ih9x4rsk") (f (quote (("wallet_file" "serde" "serde_json" "regex") ("default" "wallet_file"))))))

(define-public crate-electrum2descriptors-0.4.3 (c (n "electrum2descriptors") (v "0.4.3") (d (list (d (n "bdk") (r "^0.19") (d #t) (k 2)) (d (n "bitcoin") (r "^0.28") (d #t) (k 0)) (d (n "miniscript") (r "^7") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "16bsgdw2vrn4a6hs35kklnrccv7sdl3r5k8v5p4b46v02wpvnj41") (f (quote (("wallet_file" "serde" "serde_json" "regex") ("default" "wallet_file"))))))

(define-public crate-electrum2descriptors-0.4.4 (c (n "electrum2descriptors") (v "0.4.4") (d (list (d (n "bdk") (r "^0.24") (d #t) (k 2)) (d (n "bitcoin") (r "^0.29") (d #t) (k 0)) (d (n "miniscript") (r "^8") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0n1mjmd72qwfjp3l1b6rpq3353isf31d6s62n9kl0gjpchz4pghx") (f (quote (("wallet_file" "serde" "serde_json" "regex") ("default" "wallet_file"))))))

