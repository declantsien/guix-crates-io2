(define-module (crates-io el ec electrify) #:use-module (crates-io))

(define-public crate-electrify-0.0.1 (c (n "electrify") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "jacklog") (r "^0.0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "12li9hqxjdlssvqs7mz5pwrmbj3r1j543zcfb5591m5dgjic1ndi")))

