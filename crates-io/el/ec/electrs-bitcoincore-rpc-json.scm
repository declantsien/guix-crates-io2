(define-module (crates-io el ec electrs-bitcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-electrs-bitcoincore-rpc-json-0.17.0-e1 (c (n "electrs-bitcoincore-rpc-json") (v "0.17.0-e1") (d (list (d (n "bitcoin") (r "^0.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rnb9mbxj350gi3wlkyjm3349bxrv4n0zdqszky37r1q3hlkpnrr")))

(define-public crate-electrs-bitcoincore-rpc-json-0.17.0-e2 (c (n "electrs-bitcoincore-rpc-json") (v "0.17.0-e2") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01phyk99ralfcwv05gs000kxmlihk23hv4q3203i5pajvq0zmg08")))

(define-public crate-electrs-bitcoincore-rpc-json-0.17.0-e3 (c (n "electrs-bitcoincore-rpc-json") (v "0.17.0-e3") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "119hp0p01r8xvclngxbhg4gfcmwmnfyj377zvjswjx572klsm929")))

