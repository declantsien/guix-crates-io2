(define-module (crates-io el ec electrs-request) #:use-module (crates-io))

(define-public crate-electrs-request-0.1.0 (c (n "electrs-request") (v "0.1.0") (d (list (d (n "bitcoin-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "electrum-client") (r "^0.12.0") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "=1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0") (d #t) (k 0)))) (h "0i1nv8hmr9vxqc5s18s5c5jgybm9rwwccv71ygs7jfk9ld1b3n01")))

(define-public crate-electrs-request-0.1.1 (c (n "electrs-request") (v "0.1.1") (d (list (d (n "bitcoin-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "electrum-client") (r "^0.12.0") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "=1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0") (d #t) (k 0)))) (h "1wfcvkfx8i696vfs75z31adz3wn0lpx2hdv3p9md93d5sqanywqi")))

(define-public crate-electrs-request-0.1.2 (c (n "electrs-request") (v "0.1.2") (d (list (d (n "bitcoin-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "electrum-client") (r "^0.12.0") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "=1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0") (d #t) (k 0)))) (h "0p8yxchd06f4f9mzk6509y73v69rid55swi1ih1dcdlbgsvilqhj")))

(define-public crate-electrs-request-0.1.3 (c (n "electrs-request") (v "0.1.3") (d (list (d (n "bitcoin-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "electrum-client") (r "^0.12.0") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "=1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0") (d #t) (k 0)))) (h "0qq3x9ancfz9xfs86ghhj2qwd93fns99jfb83bpy0imz7dnrj859")))

