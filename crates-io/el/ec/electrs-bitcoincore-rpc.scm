(define-module (crates-io el ec electrs-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-electrs-bitcoincore-rpc-0.17.0-e1 (c (n "electrs-bitcoincore-rpc") (v "0.17.0-e1") (d (list (d (n "electrs-bitcoincore-rpc-json") (r "^0.17.0-e1") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ynl7icspkl5db6zxlwn5knl25afyiwq321jmn9xqampakbkaqya")))

(define-public crate-electrs-bitcoincore-rpc-0.17.0-e2 (c (n "electrs-bitcoincore-rpc") (v "0.17.0-e2") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "electrs-bitcoincore-rpc-json") (r "^0.17.0-e2") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kc9c58bhjqqg2x462h3ndphz8si3s7mihx0ca54gqlwcyfw6lkv")))

(define-public crate-electrs-bitcoincore-rpc-0.17.0-e3 (c (n "electrs-bitcoincore-rpc") (v "0.17.0-e3") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "electrs-bitcoincore-rpc-json") (r "^0.17.0-e3") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1n7xc554na68s0y8m3l26csg6mir71qlz4yh7hqy9hqigl2v5xza")))

