(define-module (crates-io el ec electrical_mesurements) #:use-module (crates-io))

(define-public crate-electrical_mesurements-0.1.0 (c (n "electrical_mesurements") (v "0.1.0") (h "08h0d3rncmmrfqv4qf4f9v6xq17sqyqg4975139d0jczzw7136l1")))

(define-public crate-electrical_mesurements-0.1.1 (c (n "electrical_mesurements") (v "0.1.1") (h "1jl4kikzx392jscq7xci4276bgivnavp4jx3l3inypvfmf9daqnp")))

(define-public crate-electrical_mesurements-0.2.0 (c (n "electrical_mesurements") (v "0.2.0") (h "1w7i6v6nyrcw8cmzbx9ndf9kv2n19w9997mn14066r7a89p3nidr")))

(define-public crate-electrical_mesurements-0.3.0 (c (n "electrical_mesurements") (v "0.3.0") (h "0nd7yqfbiwsbk5wsi1w4ypwfzhp17qpg3lkvvm80yay12m5jzxlh")))

(define-public crate-electrical_mesurements-0.4.0 (c (n "electrical_mesurements") (v "0.4.0") (h "1q99a73ll9l1898wk44ac6r8bgsrgds4fnw7cxn5vxg055v7gfs3")))

(define-public crate-electrical_mesurements-0.5.0 (c (n "electrical_mesurements") (v "0.5.0") (h "121sdpdb3knsw1nk3lxkgs2jkidza3gca0513kfwn1bbllm37f6n")))

(define-public crate-electrical_mesurements-0.6.0 (c (n "electrical_mesurements") (v "0.6.0") (h "1f02pq2r3x633i7wwi4ki9wvi4hbw1c92110yxif0hwn1hphbjs5") (f (quote (("default_math") ("default" "default_math"))))))

