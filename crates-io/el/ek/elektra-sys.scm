(define-module (crates-io el ek elektra-sys) #:use-module (crates-io))

(define-public crate-elektra-sys-0.9.0 (c (n "elektra-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "076mzpr52l4r6x57yykgraj6mbfd0gvbhwhvmvkl860v46c4r7kk") (f (quote (("default"))))))

(define-public crate-elektra-sys-0.9.1 (c (n "elektra-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "07sfg7flha4ln6fwrl2ybqrwxc866qigaaci2pgklxd5cmbird89") (f (quote (("default"))))))

(define-public crate-elektra-sys-0.9.10 (c (n "elektra-sys") (v "0.9.10") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "1mhqrp07jjbqk2vsb1hijivn0l3i7ch8kv6a0gvhpxjl5dsvf11c") (f (quote (("default"))))))

(define-public crate-elektra-sys-0.10.0 (c (n "elektra-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "06xb03l9080alh2pn7a2jlbr9blvkc01bg6z763sakjqya0wm61d") (f (quote (("default"))))))

(define-public crate-elektra-sys-0.11.0 (c (n "elektra-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "08p6davv7lffw5hn7my8ccy2ka3z4gx3l86r24xb3xv2nr48j4xv") (f (quote (("default"))))))

(define-public crate-elektra-sys-0.11.1 (c (n "elektra-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)))) (h "03l74irvvjndqz1l6598md9c046b4cqzxprwwf77jd3j633a1x4n") (f (quote (("default"))))))

