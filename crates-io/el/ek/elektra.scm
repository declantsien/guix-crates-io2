(define-module (crates-io el ek elektra) #:use-module (crates-io))

(define-public crate-elektra-0.9.0 (c (n "elektra") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "elektra-sys") (r "^0.9.0") (d #t) (k 0)))) (h "04fb4kzca709nrrjffxg15vhi0szj113mgaqwbbll0q2v1kp0z0b") (f (quote (("pkg-config" "elektra-sys/pkg-config") ("default"))))))

(define-public crate-elektra-0.9.1 (c (n "elektra") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "elektra-sys") (r "^0.9.1") (d #t) (k 0)))) (h "1k5cl4gc0czcimbyxrdpc2fsvfklvpnrj09sj1rad3jxf52523vn") (f (quote (("pkg-config" "elektra-sys/pkg-config") ("default"))))))

(define-public crate-elektra-0.9.10 (c (n "elektra") (v "0.9.10") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "elektra-sys") (r "^0.9.1") (d #t) (k 0)))) (h "0rr47q7l0bg6nk0k2dwl24xx4z6sy1dbakmrypv6vdp944ysk7yp") (f (quote (("pkg-config" "elektra-sys/pkg-config") ("default"))))))

(define-public crate-elektra-0.10.0 (c (n "elektra") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "elektra-sys") (r "^0.9.1") (d #t) (k 0)))) (h "0cn8d929kh9qx5d945l80xlmxkf5vxb5izv3c269fi7g5c5wgyx8") (f (quote (("pkg-config" "elektra-sys/pkg-config") ("default"))))))

(define-public crate-elektra-0.11.1 (c (n "elektra") (v "0.11.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "elektra-sys") (r "^0.11.1") (d #t) (k 0)))) (h "1q8jmv9v89a85pgz5pzfz1h04mx2rd8y7w322p09cq9mnn1gqzva") (f (quote (("pkg-config" "elektra-sys/pkg-config") ("default"))))))

