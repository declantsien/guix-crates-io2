(define-module (crates-io el ek elektron_ngspice) #:use-module (crates-io))

(define-public crate-elektron_ngspice-0.1.0 (c (n "elektron_ngspice") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "127mbc7v0ajyam9jcw3wmdj5i7gasfvx0c3svfgnhrcpaph3mxmb") (y #t)))

(define-public crate-elektron_ngspice-0.1.1 (c (n "elektron_ngspice") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "02lak58jvlyyz3if8h56n38vqjzjnm1dqg1aamjvb18px77i2xcl") (y #t)))

(define-public crate-elektron_ngspice-0.1.2 (c (n "elektron_ngspice") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hmf1rba5p2w62byd8l4nqdp3dxsqam8inrv6gsrdcrmc8aghpmf") (y #t)))

(define-public crate-elektron_ngspice-0.1.3 (c (n "elektron_ngspice") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zw8svfsd1axmjfcb8gg0lvnvb74sv2ihiy0mphi64dgd1kgsla9")))

(define-public crate-elektron_ngspice-0.1.4 (c (n "elektron_ngspice") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hi56f0d2jm103abxjvfrjqlgsj4rxi2bg7ammd3ldki9zp2088k")))

