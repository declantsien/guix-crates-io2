(define-module (crates-io el ek elektron_reports) #:use-module (crates-io))

(define-public crate-elektron_reports-0.1.2 (c (n "elektron_reports") (v "0.1.2") (d (list (d (n "elektron_sexp") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1qm10hq63n7kx6yxnvf1aqcw4ljz60h3nfq5a9bdb5bqmzs7jxmm") (y #t)))

(define-public crate-elektron_reports-0.1.3 (c (n "elektron_reports") (v "0.1.3") (d (list (d (n "elektron_sexp") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "027924sc735fc0krqnzyrakv0xyhrw7an77ml0h1yai7cwxzcxc7") (y #t)))

(define-public crate-elektron_reports-0.1.4 (c (n "elektron_reports") (v "0.1.4") (d (list (d (n "elektron_sexp") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1y37c4w2bdjgwd9sv7pz6m4wq5hj5n056zwmg0ckns73jbrha3sr") (y #t)))

(define-public crate-elektron_reports-0.1.5 (c (n "elektron_reports") (v "0.1.5") (d (list (d (n "elektron_sexp") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02hxk89n6v2rsir86grv145dlvq87nlddrbh7svpyxqc91bz7gvw") (y #t)))

(define-public crate-elektron_reports-0.1.6 (c (n "elektron_reports") (v "0.1.6") (d (list (d (n "elektron_sexp") (r "^0.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xwnzk2g9g18mh9rgmhzk59hlbysw2fwxx2gb3kb2yn0lnqxh81z") (y #t)))

(define-public crate-elektron_reports-0.1.12 (c (n "elektron_reports") (v "0.1.12") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0va3yzdjvrybw0zc1r4hc20jcb3xk5mx4i4dlcvzhiwvah7k3azm") (y #t)))

(define-public crate-elektron_reports-0.1.13 (c (n "elektron_reports") (v "0.1.13") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ig6r5k4n8gpv2wvlkxjnrxqh76mhpl4m0f6sr6pvq7ifgfzlh33") (y #t)))

(define-public crate-elektron_reports-0.1.14 (c (n "elektron_reports") (v "0.1.14") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0rghxzj0crlmqrkgilkjmxs8lhz1x1438wjsn1s4gsr0b5hknm9b") (y #t)))

(define-public crate-elektron_reports-0.1.15 (c (n "elektron_reports") (v "0.1.15") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "021q47vhjh7f6vdi3bxnw7hz5ibp3qm6cfrsva0y9ma6idqq7jdr") (y #t)))

(define-public crate-elektron_reports-0.1.16 (c (n "elektron_reports") (v "0.1.16") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0r9dg4kp2yfr7ldyarxc06fmqbb49isalbkbwfz1j8srrvdwz7l3") (y #t)))

(define-public crate-elektron_reports-0.1.17 (c (n "elektron_reports") (v "0.1.17") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "00jpvlvxjsgi3hl0gbz06zyhi38ylng08pdvs5d1c92nm59p9p95") (y #t)))

(define-public crate-elektron_reports-0.1.20 (c (n "elektron_reports") (v "0.1.20") (d (list (d (n "elektron_sexp") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02vwwh53acywaphq8si98bkdqnv1lnvxqrp3l4v756ffw6k8r9gx") (y #t)))

