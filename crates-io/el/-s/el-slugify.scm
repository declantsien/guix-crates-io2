(define-module (crates-io el -s el-slugify) #:use-module (crates-io))

(define-public crate-el-slugify-0.1.0 (c (n "el-slugify") (v "0.1.0") (d (list (d (n "deunicode") (r "^1.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "18pm4h11fmphq7dh95vdgmgw2qm9ch64j27v38df2gdcsbnl7b80")))

(define-public crate-el-slugify-0.1.1 (c (n "el-slugify") (v "0.1.1") (d (list (d (n "deunicode") (r "^1.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "14d646n2i3fpbjk2nqy9azy8z7qqg43cj96fp31f5fh2npc5pn67")))

