(define-module (crates-io el ap elapsed-time) #:use-module (crates-io))

(define-public crate-elapsed-time-0.1.0 (c (n "elapsed-time") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10rqphz0m7jnis5zhcgv5ycwjv57grhai3i1fsgabpgrmcjzzjf3")))

(define-public crate-elapsed-time-0.1.1 (c (n "elapsed-time") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06vx5mi4g688jfz6scydx2zkg3gng5hsxzcrdp7y6pcaf94q8nin")))

