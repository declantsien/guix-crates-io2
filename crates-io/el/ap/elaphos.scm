(define-module (crates-io el ap elaphos) #:use-module (crates-io))

(define-public crate-elaphos-0.1.0 (c (n "elaphos") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("dynamic_linking"))) (d #t) (k 0)))) (h "1s605vnbn84j83visz0mc4ryhsa1cw0nf307h5sqn9kxi2h3jlya")))

(define-public crate-elaphos-0.1.1 (c (n "elaphos") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("dynamic_linking"))) (d #t) (k 0)))) (h "0adqzap1jh41x0y2fjldqa9nyh371h10916xa6sbsvavrvhn6y1w")))

