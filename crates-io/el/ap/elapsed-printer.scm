(define-module (crates-io el ap elapsed-printer) #:use-module (crates-io))

(define-public crate-elapsed-printer-0.1.0 (c (n "elapsed-printer") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0lvq1hpadx3ni2arqnva1f32rixb39d2p2wz7acbh3gwiv6qwgn7")))

