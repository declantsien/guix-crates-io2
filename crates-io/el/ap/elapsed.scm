(define-module (crates-io el ap elapsed) #:use-module (crates-io))

(define-public crate-elapsed-0.1.0 (c (n "elapsed") (v "0.1.0") (h "0dbzq64q97qj5ynybhbz7mc70w219qh9cz5wqy82bv2a1g8a8vv9")))

(define-public crate-elapsed-0.1.1 (c (n "elapsed") (v "0.1.1") (h "1p0r73cmcm54nka42pqvcv5x65kkkhdwh5jv6a9na84jpr9wx0jy")))

(define-public crate-elapsed-0.1.2 (c (n "elapsed") (v "0.1.2") (h "0acwnk47zaf88pfz1r8205ah51mjd1zx8qmdq90hgzfs4vqmlkkg")))

