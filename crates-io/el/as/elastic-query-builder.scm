(define-module (crates-io el as elastic-query-builder) #:use-module (crates-io))

(define-public crate-elastic-query-builder-0.1.0 (c (n "elastic-query-builder") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rrlk4ppc1k6szk5ya8r3dchky3rfm7x1g0ndxvg5j3k8873h2ik")))

(define-public crate-elastic-query-builder-0.1.1 (c (n "elastic-query-builder") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xra48vbvzb754g3vnxkpxv6brzs0x9rmpjimyxm3ki4ij66hwli")))

(define-public crate-elastic-query-builder-0.1.2 (c (n "elastic-query-builder") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "151rrkm8jsk11zhm12dy2lz8k7kzcykb1yhig45yyqfk395ms3il")))

(define-public crate-elastic-query-builder-0.1.3 (c (n "elastic-query-builder") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04jrb18n67r9r3kqqpy3rwpwg4sq5m82prigm34rc3wgf828a7zl")))

(define-public crate-elastic-query-builder-0.1.4 (c (n "elastic-query-builder") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086pl0apgjms1kl72cxsisbsjdaw2y8bg9v3i4c726c8ifs3rny8")))

(define-public crate-elastic-query-builder-0.1.5 (c (n "elastic-query-builder") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmsrs70m0hq7fflv8n3acyi5k6crshgrp9l86j1rmiv1hvd01s5")))

(define-public crate-elastic-query-builder-0.1.6 (c (n "elastic-query-builder") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ynjwirb6034j49lns7l30l5kddyqq53rbqhiqh2n7y702gdqqr7")))

(define-public crate-elastic-query-builder-0.1.7 (c (n "elastic-query-builder") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02w55hfwvlmhyv7jdzak52s25881kabry5cmh1qrnxh25393hg48")))

(define-public crate-elastic-query-builder-0.1.8 (c (n "elastic-query-builder") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bwrpzf7mbiz5wz3mbwdbvnnsl6lfspbjpzdi58jn7rms895d0w4")))

(define-public crate-elastic-query-builder-0.1.9 (c (n "elastic-query-builder") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aw6cbb8mw2510gmhhidfapa331hy0n6n1372ggis3ffqvxqc91b")))

(define-public crate-elastic-query-builder-0.1.10 (c (n "elastic-query-builder") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17wmpw0s2h192bbf5hflpn7qhxhcj727jngs0grd68l48gpigs8f")))

(define-public crate-elastic-query-builder-0.1.11 (c (n "elastic-query-builder") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qz4brbbzkfvxd0cnss91scbp40b7p4s7j3ml9lmx5vxhl0pw8vz")))

(define-public crate-elastic-query-builder-0.1.12 (c (n "elastic-query-builder") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "055j0dc9ipl5imx4n4w2mbgzzvv7qz52iyz3d0zgic2gna618823")))

(define-public crate-elastic-query-builder-0.1.13 (c (n "elastic-query-builder") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r6s6h6fhy4hd0dgrnsq1l4ghvcdqybxfl0mzh58d2x199pfiwyg")))

(define-public crate-elastic-query-builder-0.1.14 (c (n "elastic-query-builder") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zc8l76xx0gcipq6i75ba84qfngyq7iahhpqjcl0iw4c2y8lgcjj")))

(define-public crate-elastic-query-builder-0.1.15 (c (n "elastic-query-builder") (v "0.1.15") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ic0z5ii3wf8vrciw66aq7y4s03rnvjxdi04qxza3nnm1p28f8l3")))

(define-public crate-elastic-query-builder-0.1.16 (c (n "elastic-query-builder") (v "0.1.16") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ziwbg95kbz5kfgb2dc7m08zyy6jhfmfzmxlvligqbwbfxayfzq5")))

(define-public crate-elastic-query-builder-0.1.17 (c (n "elastic-query-builder") (v "0.1.17") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13invcs1f51lzgbg2arhg5jbwh4gcn37vm95fni58nq8gv5m57c9")))

(define-public crate-elastic-query-builder-0.1.18 (c (n "elastic-query-builder") (v "0.1.18") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iqm4ywrjgd5k69785i084hdd8xpvj4d8f46k7i5ms1n6jglnawf")))

(define-public crate-elastic-query-builder-0.1.19 (c (n "elastic-query-builder") (v "0.1.19") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vlv92x83i43zm8mnjirz3xs5rs1alw0xwx32f4xjds1n11ny3xv")))

(define-public crate-elastic-query-builder-0.1.20 (c (n "elastic-query-builder") (v "0.1.20") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09qvz1mql48yzgn2lliimq6c19055753j75qm3fs72vh4mgq0xqx")))

(define-public crate-elastic-query-builder-0.1.21 (c (n "elastic-query-builder") (v "0.1.21") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mbw82pl5nxy8nsvki5ri0q1ysn3zfz9bff8qmyp0ami7f0n2bd1")))

(define-public crate-elastic-query-builder-0.1.22 (c (n "elastic-query-builder") (v "0.1.22") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07m46df1aw9fc2z4gd67k4gnrz0rqb1gr96pffgsrg1nm3s2nqnk")))

(define-public crate-elastic-query-builder-0.1.23 (c (n "elastic-query-builder") (v "0.1.23") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07q4sgpvmjl3ybdh1fahyc1vx0hl99lk37rzdzp7323n2i2kczbc")))

(define-public crate-elastic-query-builder-0.1.24 (c (n "elastic-query-builder") (v "0.1.24") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cxablypjplqvds3irmgl8prcg0f8x02yq16k63csj39480kdxwx")))

(define-public crate-elastic-query-builder-0.1.25 (c (n "elastic-query-builder") (v "0.1.25") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qrdphcrx9dv8zzxj15qxlw29nyf2lbq09zfwpagh89iy97yb3n6")))

(define-public crate-elastic-query-builder-0.1.26 (c (n "elastic-query-builder") (v "0.1.26") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n47qfvh5by070rk2drv5xjg0q8sjnlxq155pd3hm2d6abjmbcck")))

(define-public crate-elastic-query-builder-0.1.27 (c (n "elastic-query-builder") (v "0.1.27") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "14yfny9bjwznz75xxcz1ziqb02w0261isld7fd1iqykif9cl1krl")))

(define-public crate-elastic-query-builder-0.1.28 (c (n "elastic-query-builder") (v "0.1.28") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "11569c8wcim6ldyk08iva95acc3j4lp2xkmpfzmxy6a2i6pndj3k")))

(define-public crate-elastic-query-builder-0.1.29 (c (n "elastic-query-builder") (v "0.1.29") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0qbz7y8c722ir723hqxm7v0zall09aqayrw4m5gvp3awb2wk1ghv")))

(define-public crate-elastic-query-builder-0.1.30 (c (n "elastic-query-builder") (v "0.1.30") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "182k7f8z9jlipk79ql3p06sp6bqr1gri5q3km5k8hhdhhpgg8lcx")))

(define-public crate-elastic-query-builder-0.1.31 (c (n "elastic-query-builder") (v "0.1.31") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "01wr33yb6acpjgb1qm741yq50jy8xk1dxqij99fap94lygbymh6k")))

(define-public crate-elastic-query-builder-0.1.32 (c (n "elastic-query-builder") (v "0.1.32") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0mq86pjc280zazp0cqm3jkrprvsqg1kz3cn9lwzlwmjwbgmhgl04")))

(define-public crate-elastic-query-builder-0.1.33 (c (n "elastic-query-builder") (v "0.1.33") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "195kcv6mlhlaizx31bbhn9q6f4f0bz4g9y7ilvpi0rzcpfmx71qp")))

(define-public crate-elastic-query-builder-0.1.34 (c (n "elastic-query-builder") (v "0.1.34") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0p8mzmm0s43nx4nd6vq3ram3vgajqqmn9f6wcmnj45qz8b8bwjaa")))

(define-public crate-elastic-query-builder-0.1.35 (c (n "elastic-query-builder") (v "0.1.35") (d (list (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "162yz57brn6la0ll7qn9y77wmzjbp6kymwa29prnf8xlj1vg7brw")))

(define-public crate-elastic-query-builder-0.1.36 (c (n "elastic-query-builder") (v "0.1.36") (d (list (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1v1yngrjr6y3vybigkg1rsgm2af17ilnw61c3asqbg4b4mk3r7w3")))

