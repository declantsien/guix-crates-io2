(define-module (crates-io el as elastic_requests) #:use-module (crates-io))

(define-public crate-elastic_requests-0.1.0 (c (n "elastic_requests") (v "0.1.0") (h "1lbbd3frgf3plhsf7wrj5r6bxbkxk0lbd1hk9643cfscqfnk2rc0")))

(define-public crate-elastic_requests-0.1.1 (c (n "elastic_requests") (v "0.1.1") (h "08c5gzn4hx43xgjymfjw7s4dzj30qg3dh22icnlicf69nj7z58py")))

(define-public crate-elastic_requests-0.1.2 (c (n "elastic_requests") (v "0.1.2") (h "00kqi0b9bl619n2mkan99jwk6i7v2wisjc5r1rr293vn9g5w0y1v")))

(define-public crate-elastic_requests-0.1.3 (c (n "elastic_requests") (v "0.1.3") (h "1g0zw5az1cq8dbq6ccy2qpvxwkg09g2sjm2nxhjgr09kqs119f0a")))

(define-public crate-elastic_requests-0.1.4 (c (n "elastic_requests") (v "0.1.4") (h "1m3c25s0x1906lwnp11y3p3pp6mb3sm5v4n9cghh838kzfpa0h3a")))

(define-public crate-elastic_requests-0.1.5 (c (n "elastic_requests") (v "0.1.5") (h "1rwwwjz05gw7jbbg8gmjnzswy3qi922hh42z5nd8qpgdqww50jy8")))

(define-public crate-elastic_requests-0.1.7 (c (n "elastic_requests") (v "0.1.7") (h "199vw5n9h2ixnzjcdzh3wma10a02dvr25p3cz757qxifdzjysjk3")))

(define-public crate-elastic_requests-0.2.0 (c (n "elastic_requests") (v "0.2.0") (h "1kv2ria5ki2rdhfpp5c19rr9fbxz0g19fv4gsjhb1s40yc1gd89g")))

(define-public crate-elastic_requests-0.2.1 (c (n "elastic_requests") (v "0.2.1") (h "19l5fc8ami9lnlq5s9iqp1pirkhq1vd38326jycg8mbq0ay7ajlj")))

(define-public crate-elastic_requests-0.3.0 (c (n "elastic_requests") (v "0.3.0") (h "0ia0ndx8m1kdkvil00gq7iv18hhaws2lpbkrjbx5vigkkh0wqsrk")))

(define-public crate-elastic_requests-0.4.0 (c (n "elastic_requests") (v "0.4.0") (h "1wn5033jlls9d2amnl9vskwnq1psnrf7bl51vid10qlx74c7f379")))

(define-public crate-elastic_requests-0.4.1 (c (n "elastic_requests") (v "0.4.1") (h "0i1i6rwsvd55259mrmy95v2bmcj1647fyxk2rxpnj11yk2lgz9nn")))

(define-public crate-elastic_requests-0.4.2 (c (n "elastic_requests") (v "0.4.2") (h "1khrxv4d62qq6rmccpiz0iad5hxw7lxvnapr1vdsrylx63szyd7l")))

(define-public crate-elastic_requests-0.20.0 (c (n "elastic_requests") (v "0.20.0") (h "0sxifq61kvd35wia55167isgddjgn1yyxsm1yi5mb08r1x3wyyp7")))

(define-public crate-elastic_requests-0.20.1 (c (n "elastic_requests") (v "0.20.1") (h "13n3ixp9icd8hlhi17l0mmw4p5l8nl3nvh8aschpr4xp3h7hm5gf")))

(define-public crate-elastic_requests-0.20.2 (c (n "elastic_requests") (v "0.20.2") (h "0k6ylq08f7myz20454pi67ypds8rv3ms9kmklq95hwxy28cb8b2b")))

(define-public crate-elastic_requests-0.20.3 (c (n "elastic_requests") (v "0.20.3") (h "0za0ld93cpasj6agkx4vry4qm0pdjsvxdda9y9hzb7yz2incl68p")))

(define-public crate-elastic_requests-0.20.4 (c (n "elastic_requests") (v "0.20.4") (h "1s3jljm0l8d4jlgv518r3gj9s88brsqfjafgfifrmxm0nzqv1pwn")))

(define-public crate-elastic_requests-0.20.5 (c (n "elastic_requests") (v "0.20.5") (h "08bzq3m7s4rs9ngikqhwb5aqp4df7qvl9vpabv0djva6mgh59xkk")))

(define-public crate-elastic_requests-0.20.6 (c (n "elastic_requests") (v "0.20.6") (h "12w657yfs0nklxvwcq08m764y3452y0grmbd2qfw7n2ifmbh82ay")))

(define-public crate-elastic_requests-0.20.7 (c (n "elastic_requests") (v "0.20.7") (h "01wv09ddw1l6ib858k084mxbnqyjhdrnbb6bxy7awzpys5l96fcv")))

(define-public crate-elastic_requests-0.20.8 (c (n "elastic_requests") (v "0.20.8") (h "1kc6cyp33vpiq7x790xslxnrl9zf68svz71dggaybpdpv6m49hrp")))

(define-public crate-elastic_requests-0.20.9 (c (n "elastic_requests") (v "0.20.9") (h "14bh5gxmh508bhhxj88bvhrqm5ma85naj2v7mv7yc36swb2jkj8w")))

(define-public crate-elastic_requests-0.20.10 (c (n "elastic_requests") (v "0.20.10") (h "1x5qjp7qbh25brjyyzcsjv59vwjl4mb2dfz5klbpdm214ap3vki4")))

(define-public crate-elastic_requests-0.21.0-pre.1 (c (n "elastic_requests") (v "0.21.0-pre.1") (d (list (d (n "http") (r "~0.1") (d #t) (k 0)))) (h "09s3348z8h0ipy8v04bx6kfgdqq17sjbqn810ms0q9g7qkqwi1r3")))

(define-public crate-elastic_requests-0.21.0-pre.2 (c (n "elastic_requests") (v "0.21.0-pre.2") (d (list (d (n "http") (r "~0.1") (d #t) (k 0)))) (h "1cjiq1sxp7hd4ksh17nb5kd9mczcizdziwnqwr7q4d2c3xi76ysp")))

(define-public crate-elastic_requests-0.21.0-pre.3 (c (n "elastic_requests") (v "0.21.0-pre.3") (d (list (d (n "http") (r "~0.1") (d #t) (k 0)))) (h "13md7i8jvqbzw6q8a1kk90bjj91gh7pga3gfag2azcx5664pdn43")))

(define-public crate-elastic_requests-0.21.0-pre.4 (c (n "elastic_requests") (v "0.21.0-pre.4") (d (list (d (n "http") (r "~0.1") (d #t) (k 0)))) (h "07pvixmydn921l52g9zan600z8wspmziqp2gxpvjlwwmlcgvfpiw")))

