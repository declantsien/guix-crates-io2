(define-module (crates-io el as elasticsearch-dsl) #:use-module (crates-io))

(define-public crate-elasticsearch-dsl-0.1.0 (c (n "elasticsearch-dsl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vjyccxah9wcc55klv1pssqqpqp5kz1q9bvnr3w6rwcmm0issqpp")))

(define-public crate-elasticsearch-dsl-0.1.1 (c (n "elasticsearch-dsl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1338d3jn2hfjmydvqhb5wllifjvq4syzaqhll1ck6acnv9vhxchz")))

(define-public crate-elasticsearch-dsl-0.1.2 (c (n "elasticsearch-dsl") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g216jbk5c58b5bmh0ihy7jy33r75x7v375fy5228s9g7520g4ca")))

(define-public crate-elasticsearch-dsl-0.1.3 (c (n "elasticsearch-dsl") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1258lfhwc7d8z5df31jb5hm2hxw3a0v4qmyn3fb9ldi5n2kc743r")))

(define-public crate-elasticsearch-dsl-0.1.4 (c (n "elasticsearch-dsl") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kbv7jw0d2m97a5dknszn7l7xyvdwhx5pchj9va2ras7axm25is8")))

(define-public crate-elasticsearch-dsl-0.1.5 (c (n "elasticsearch-dsl") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16s5l9d265bmh7i63fsiyd4829xfjpf7rcfdiz377605f1dvgf2x")))

(define-public crate-elasticsearch-dsl-0.1.6 (c (n "elasticsearch-dsl") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zhqwqixz3i4id6bzy27bky3pknx3w490wxh5wlrlwq3f5r4yhfd")))

(define-public crate-elasticsearch-dsl-0.1.7 (c (n "elasticsearch-dsl") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jz05vbdajg733lqqqqr0g0khd2av0c6ym6lgsylxdkdl90zb9x0")))

(define-public crate-elasticsearch-dsl-0.1.8 (c (n "elasticsearch-dsl") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18jh1jv8prkanfgmdwnq5j9yrjklrg5d930x2dh2jkg668pw6810")))

(define-public crate-elasticsearch-dsl-0.1.9 (c (n "elasticsearch-dsl") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12pf55q35n31crjp486xw2cy1xqnyswq6hgj8rlm70hsq21x8246")))

(define-public crate-elasticsearch-dsl-0.1.10 (c (n "elasticsearch-dsl") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "029vrw095l1y00zj8lzx8vf1abgkw467kdl36pr8sv5s6k0dssdn")))

(define-public crate-elasticsearch-dsl-0.1.11 (c (n "elasticsearch-dsl") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ppmpjp8nw42q5blry01r4alk0jn3bmkrdi7c90ia0srnrhc7flf")))

(define-public crate-elasticsearch-dsl-0.1.12 (c (n "elasticsearch-dsl") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w6q3dgrfqj4h6sr2arqkia6pc3lx1kaymxiz77s863z4jddn6c1")))

(define-public crate-elasticsearch-dsl-0.1.13 (c (n "elasticsearch-dsl") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hhbb3ypwcj6vn9d7ri5gi7vfk9ch4b919ihnzg3dfbkshkmqg3r")))

(define-public crate-elasticsearch-dsl-0.2.0 (c (n "elasticsearch-dsl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17f0764ighdhp386s89sgcldmrv86gxq6cj4792c54gs24c0hyz9")))

(define-public crate-elasticsearch-dsl-0.2.1 (c (n "elasticsearch-dsl") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0d9cfn2z4fgb3iz775wg4ain5vqsa43zw2w43k4fslrrpcsvvvqy")))

(define-public crate-elasticsearch-dsl-0.2.2 (c (n "elasticsearch-dsl") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14hyqd3zisdcam30p7hdnaj468ig0a157xaw8h6z2hrd0c20n7w1")))

(define-public crate-elasticsearch-dsl-0.2.3 (c (n "elasticsearch-dsl") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gdclzni0pl88428dn2lcx1406scb6a369q6r6fbwkgqfqw9y00f")))

(define-public crate-elasticsearch-dsl-0.3.0 (c (n "elasticsearch-dsl") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kpyk87f637hqj9xrlp5kg7h5w554111ry729n9ipamj3c1s7qrx")))

(define-public crate-elasticsearch-dsl-0.3.1 (c (n "elasticsearch-dsl") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0h3ahf5sad3lqlysmqvgx96ffa95r0rgg85y3hnh4jsn90sb91hz")))

(define-public crate-elasticsearch-dsl-0.3.2 (c (n "elasticsearch-dsl") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y7zvp89rv5s97wpm85lcj0ab2nhfzqk3v0rs2cc8yyz1whl26nk")))

(define-public crate-elasticsearch-dsl-0.3.3 (c (n "elasticsearch-dsl") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0x3llkv0wwdiiqi5skifm71bppw1yp1fwl53xszb74gcaa612dvn")))

(define-public crate-elasticsearch-dsl-0.3.4 (c (n "elasticsearch-dsl") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nr8bgswwbrqihxyy2mykaivwgg1xzwkp0fh95w359kys5bijy2x")))

(define-public crate-elasticsearch-dsl-0.3.5 (c (n "elasticsearch-dsl") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yzpw7jz4a0072ks5lqz66shz5cyi6xsdi2j7j4blq1gdqvdp6kb")))

(define-public crate-elasticsearch-dsl-0.3.6 (c (n "elasticsearch-dsl") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l3jgz2sad7gnsli9s5zin1f50dgbadd288a32fqnlanbcf3hz4s")))

(define-public crate-elasticsearch-dsl-0.3.7 (c (n "elasticsearch-dsl") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mwyff3imkqq0qciz8l6djy8am89p72r5hyqznq9ras44fbp24iw")))

(define-public crate-elasticsearch-dsl-0.3.8 (c (n "elasticsearch-dsl") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hhqz92s31bn55wzdji3m7kkmw7qhym68v7cyzcxw1j7dyqys2vg")))

(define-public crate-elasticsearch-dsl-0.3.9 (c (n "elasticsearch-dsl") (v "0.3.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fjy8bl2ymr4r077rk1cd84nhmfdbvz2l81dpwbx300yfwa5kzjz")))

(define-public crate-elasticsearch-dsl-0.4.0 (c (n "elasticsearch-dsl") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0892fbvx88vhv7gkzijxzhgfdaj8ldci5sxi8xdgny5hlla0cynd")))

(define-public crate-elasticsearch-dsl-0.4.1 (c (n "elasticsearch-dsl") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "04f74kqqx1ixkqmbs34mm35cgnj9gbxy18cgi95h1pav6qv10l5a")))

(define-public crate-elasticsearch-dsl-0.4.2 (c (n "elasticsearch-dsl") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "034g419cbavh420r00yha076dq5svas2lln524indijixykvm8yp")))

(define-public crate-elasticsearch-dsl-0.4.3 (c (n "elasticsearch-dsl") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1hz2i7zgak96ghdbqm9f9zylr5bryb0pam6a7yzn08y9n5bwns8i")))

(define-public crate-elasticsearch-dsl-0.4.4 (c (n "elasticsearch-dsl") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1lqcmvsfljsazc4b8ilr63b2jmg090wcjf4bfd641gcd7d0pwjd5")))

(define-public crate-elasticsearch-dsl-0.4.5 (c (n "elasticsearch-dsl") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0dg5ikdgn5wsz3nl3wmqj5lh0xff04y8v035xcqv21f29nys8z1n") (y #t)))

(define-public crate-elasticsearch-dsl-0.4.6 (c (n "elasticsearch-dsl") (v "0.4.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "18c43a32zm06rsf7xry3298qhk30y82yfy7qfji5abf1lh8aq9fl") (y #t)))

(define-public crate-elasticsearch-dsl-0.4.7 (c (n "elasticsearch-dsl") (v "0.4.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1gdbv51x1lysq6qz36layinfsb9wk4lm5bpvbbxfnhzgvhbhmhc6")))

(define-public crate-elasticsearch-dsl-0.4.8 (c (n "elasticsearch-dsl") (v "0.4.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0kyphnnpfk05zwqqkqa09vacx6bsn7arh5c10lp2dd0f6knxghbv")))

(define-public crate-elasticsearch-dsl-0.4.9 (c (n "elasticsearch-dsl") (v "0.4.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0q7nb1pci43ww6rxdzxh5gk3mn1shd23hdd4ccgx5y14arhs8a3n")))

(define-public crate-elasticsearch-dsl-0.4.10 (c (n "elasticsearch-dsl") (v "0.4.10") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1ndkgpkxjp7gcs8rdxsvy1gjdp49zx506ndk78q5s1qrnqkri264")))

(define-public crate-elasticsearch-dsl-0.4.11 (c (n "elasticsearch-dsl") (v "0.4.11") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "16yvg6qvk64szsllgsrl13k7sjqwchjhcwnkgd1gjazm36whv0hw")))

(define-public crate-elasticsearch-dsl-0.4.12 (c (n "elasticsearch-dsl") (v "0.4.12") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0zrxp5rm2v8b4zflnyy6vssysy35k3is3vkk8bpv3g5r9zp4la6n")))

(define-public crate-elasticsearch-dsl-0.4.13 (c (n "elasticsearch-dsl") (v "0.4.13") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "11baaxh1vzkzqb1bkzgqijfrm1qxiic7xwq3s7jbzab40p7zyvfi")))

(define-public crate-elasticsearch-dsl-0.4.14 (c (n "elasticsearch-dsl") (v "0.4.14") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1qc3w5lvjwmrsdk8j5ss5ysmyss173dj7v80n629gfcgq0si7av8")))

(define-public crate-elasticsearch-dsl-0.4.15 (c (n "elasticsearch-dsl") (v "0.4.15") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1p10nf7xw3j2wzhf0gdhydldrsvin0pwv5ci3mk53bbq9fcqj0if")))

(define-public crate-elasticsearch-dsl-0.4.17 (c (n "elasticsearch-dsl") (v "0.4.17") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1yz7ya8npmi01z9cpb73vxx5n8qmfbnin9lzl16yclzfs9mpmqbq")))

(define-public crate-elasticsearch-dsl-0.4.18 (c (n "elasticsearch-dsl") (v "0.4.18") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1a2bxlf31hyjng59bc17zm8h9jy9vkr432ssd307838y1kaxns97")))

(define-public crate-elasticsearch-dsl-0.4.19 (c (n "elasticsearch-dsl") (v "0.4.19") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1ssga2yaajp3hv6p1w7s6nnkp41zm2cixgfdqz3d3q6c0mzharkh")))

(define-public crate-elasticsearch-dsl-0.4.20 (c (n "elasticsearch-dsl") (v "0.4.20") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "050zndnagry3xxab6zaqlb4iyn1mli7v6dh69bwrk1x390bdd51y")))

(define-public crate-elasticsearch-dsl-0.4.21 (c (n "elasticsearch-dsl") (v "0.4.21") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0bkqsyxxxsr1qf9f23kfn4d0gviwin4z3gzq3q0b08hyhfj5kk43")))

