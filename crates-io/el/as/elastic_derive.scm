(define-module (crates-io el as elastic_derive) #:use-module (crates-io))

(define-public crate-elastic_derive-0.1.0 (c (n "elastic_derive") (v "0.1.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.1.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1zvrbfb8xrhl814dqis3iwpb6jszxb3pypdlap8jscg7z7wsp507")))

(define-public crate-elastic_derive-0.12.0 (c (n "elastic_derive") (v "0.12.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0xcvkvpgbj5fwa0jp9zrrs4xa85amypdknv82kr1x53zqlcrjxsj")))

(define-public crate-elastic_derive-0.12.1 (c (n "elastic_derive") (v "0.12.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1ihqvhxgzm0ifw29fr54bwwqxcfv8dlch3pl9zzfac01s66m2xbb")))

(define-public crate-elastic_derive-0.12.2 (c (n "elastic_derive") (v "0.12.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "100ms3cyy4fkwhigv19h1qjgnsxxljlhznf64gx0v2m3dvck9cli")))

(define-public crate-elastic_derive-0.12.3 (c (n "elastic_derive") (v "0.12.3") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1grsmkpbwagp8jq90pcmk0qw939gn4rqkjrcvhb87fwj6rv6dk00")))

(define-public crate-elastic_derive-0.13.0 (c (n "elastic_derive") (v "0.13.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.18.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1f2xygfm5ydv9fyixzdhsk0hy32syx191pa8yyfcn81plzwzmirj")))

(define-public crate-elastic_derive-0.13.1 (c (n "elastic_derive") (v "0.13.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.18.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1a5j8xd6cpfqcnwbv3gzqla5x7xj2f1lvi10wsfj1l99blgp4lmg")))

(define-public crate-elastic_derive-0.13.2 (c (n "elastic_derive") (v "0.13.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.18.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1lakb41jcn7nx6rcscy0bqxx7kmnmyrhwikj8r2hkklg8g8j4kin")))

(define-public crate-elastic_derive-0.13.3 (c (n "elastic_derive") (v "0.13.3") (d (list (d (n "elastic_types_derive_internals") (r "~0.18.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0nviiq9j4g9wpbdklvx7nfdx05ma0ran6sq0mnap2aklbbzmrd76")))

(define-public crate-elastic_derive-0.20.0 (c (n "elastic_derive") (v "0.20.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "118iwwmg08rwj92mljijdlv8b8iw6z2v59ik1qxjahs64x5rbnnf")))

(define-public crate-elastic_derive-0.20.1 (c (n "elastic_derive") (v "0.20.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0wzshsscim06ms24hncv2s6a82n7xv5zp7mjyb9dv0kigp5n2axd")))

(define-public crate-elastic_derive-0.20.2 (c (n "elastic_derive") (v "0.20.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1ax855saq39lnr2wnw919mzdm6wxm2wdps35fnwm0i3npwfw196s")))

(define-public crate-elastic_derive-0.20.4 (c (n "elastic_derive") (v "0.20.4") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1sv5b9d7b0lnla51rvbh1n4wp0x630vkfgfalnab4rsgr0q35vj6")))

(define-public crate-elastic_derive-0.20.5 (c (n "elastic_derive") (v "0.20.5") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "01hzisvsa3dnqafdndp6jyfv6maqwq8n7rv4nranxjza0cqid70h")))

(define-public crate-elastic_derive-0.20.6 (c (n "elastic_derive") (v "0.20.6") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1c3xqji2idp86ggfwn1f8y7dh89z6kwimjz1hlx6r446c6b9q1xv")))

(define-public crate-elastic_derive-0.20.7 (c (n "elastic_derive") (v "0.20.7") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0kfrkxsq4dnhzscalfyl09q8pj893dgvvm5y51vxm16v0p4y0mfq")))

(define-public crate-elastic_derive-0.20.8 (c (n "elastic_derive") (v "0.20.8") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1mk0rrvpihgxp9z8q74ggzlgpqqflnb9fj94dvirbsnlhl1mhgnn")))

(define-public crate-elastic_derive-0.20.9 (c (n "elastic_derive") (v "0.20.9") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0w0a62ia2j862cfvad52zmh44viirpjv8rfbkinq8rw2r7qviyfw")))

(define-public crate-elastic_derive-0.20.10 (c (n "elastic_derive") (v "0.20.10") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0nmjkhfyl9nw5x800j794plmp0w131zp7hqqm5chfv4rj7gnmq3p")))

(define-public crate-elastic_derive-0.21.0-pre.1 (c (n "elastic_derive") (v "0.21.0-pre.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0gsa16d5ac15h94wsc1bbdk1wbbfyb8g3v0wd22iqr5m5p194adj")))

(define-public crate-elastic_derive-0.21.0-pre.2 (c (n "elastic_derive") (v "0.21.0-pre.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.2") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0vd0wgx3zqprwsdjywlwnfqs0ysq5c8cl9ibfr4l7xhmld9z21wl")))

(define-public crate-elastic_derive-0.21.0-pre.3 (c (n "elastic_derive") (v "0.21.0-pre.3") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.3") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0h4l033b9zx55i8xbx0g34jqipk4syxak1gd9xz9ydhc0y01hjzn")))

(define-public crate-elastic_derive-0.21.0-pre.4 (c (n "elastic_derive") (v "0.21.0-pre.4") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.4") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1xx0my6yhv9i04a5rw56kv0bzvq47lxxllvr6h91w4v4yanid2q7")))

(define-public crate-elastic_derive-0.21.0-pre.5 (c (n "elastic_derive") (v "0.21.0-pre.5") (d (list (d (n "chrono") (r "~0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "~2") (d #t) (k 0)) (d (n "quick-error") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive_internals") (r "~0.15.0") (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "01bhqmz1wk3mi7fyndbijm3glavac2vn73p337aqf6kl8za8bcql")))

