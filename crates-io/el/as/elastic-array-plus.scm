(define-module (crates-io el as elastic-array-plus) #:use-module (crates-io))

(define-public crate-elastic-array-plus-0.9.0 (c (n "elastic-array-plus") (v "0.9.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0bl293sp1y5qramlc1wgzanviqfjzyydsvyf72rxq7yiq3gm6fzk") (f (quote (("heapsizeof" "heapsize"))))))

(define-public crate-elastic-array-plus-0.9.1 (c (n "elastic-array-plus") (v "0.9.1") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "096cmgq1yis5va7z9m5aaqm9iaxg6z1ra41fw64bgcfqbc68fck4") (f (quote (("std") ("heapsizeof" "heapsize") ("default" "std"))))))

(define-public crate-elastic-array-plus-0.10.0 (c (n "elastic-array-plus") (v "0.10.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "064b8s5d8azf71cgi71jlbmrpsxsqkbsnm5i1z0j1sq1998chb2n") (f (quote (("std") ("heapsizeof" "heapsize") ("default" "std"))))))

