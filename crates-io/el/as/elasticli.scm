(define-module (crates-io el as elasticli) #:use-module (crates-io))

(define-public crate-elasticli-0.1.0 (c (n "elasticli") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hydroconf") (r "^0.2.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0avj5r23jm2kpyrsz8q2x4kdz3jd79rsdgcb8wsazh0bv892qxcd")))

