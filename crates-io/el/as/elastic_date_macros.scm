(define-module (crates-io el as elastic_date_macros) #:use-module (crates-io))

(define-public crate-elastic_date_macros-0.2.0 (c (n "elastic_date_macros") (v "0.2.0") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "11fx89gyb3cv4xvqq5gv7591hrharvsrpibyq5mvhbhgms9sbcfy") (f (quote (("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_date_macros-0.2.1 (c (n "elastic_date_macros") (v "0.2.1") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0q5kvvdjb0f5m0bdhv7x70gqqf0ylcqg90i0mb99c9001khcj6a5") (f (quote (("nightly-testing" "clippy") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.2.2 (c (n "elastic_date_macros") (v "0.2.2") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "04jmklk7wr5ganrpbw0x35vla4n8sdcnrbif1x3q3bajwfjxyn4s") (f (quote (("nightly-testing" "clippy") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.3.0 (c (n "elastic_date_macros") (v "0.3.0") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "174vabz8h3a6kgkanf36xz5kpsi208ldcdmyrfq8v6w5qbjhafqk") (f (quote (("nightly-testing" "clippy") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.3.1 (c (n "elastic_date_macros") (v "0.3.1") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0wbdy2pn7vwqi266yr8n37zk15h5x161aj313s98b5fmq6sb57nm") (f (quote (("nightly-testing" "clippy") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.3.2 (c (n "elastic_date_macros") (v "0.3.2") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0nzmanz6v863kdqya3j7m9skzalar5vxr44c6v36895vaazscysv") (f (quote (("nightly-testing" "clippy") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.4.0 (c (n "elastic_date_macros") (v "0.4.0") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)))) (h "1d7lza7nw1mhdvsmifbx43p77hksqaas3iz5kpkj9pr80zikgw4z") (f (quote (("nightly-testing") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.5.0 (c (n "elastic_date_macros") (v "0.5.0") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)))) (h "0i8i4incrwycv9xlxzglfy42mzpz9nk64na1fldci0ba4dzfck9v") (f (quote (("nightly-testing") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.5.1 (c (n "elastic_date_macros") (v "0.5.1") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)))) (h "16qd1yj26mkxbr6abwn4kxv2pwibnlnz05ziw4nq4bdqbvv7dlih") (f (quote (("nightly-testing") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.5.2 (c (n "elastic_date_macros") (v "0.5.2") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)))) (h "0436bi7lj25xvz7zwbblfhd8jfhhygzaj7g3mkklb2wldfjls3b1") (f (quote (("nightly-testing") ("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.5.3 (c (n "elastic_date_macros") (v "0.5.3") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)))) (h "1g95y72mfxqv5bf9d6f8kljyklm0rxj9y7kdkwf1q1p9wy08aya4") (f (quote (("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.6.0 (c (n "elastic_date_macros") (v "0.6.0") (d (list (d (n "chrono") (r "~0.3.0") (d #t) (k 0)))) (h "07qnysmv9dary4ziwrg946phs94sg5m5qx4b7d7gy3cm12qncslb") (f (quote (("nightly")))) (y #t)))

(define-public crate-elastic_date_macros-0.7.0 (c (n "elastic_date_macros") (v "0.7.0") (h "157ys0lbihpfysjandn4zxsbzj5n20f91y109wq4ql041n5r1jl4") (y #t)))

(define-public crate-elastic_date_macros-0.7.1 (c (n "elastic_date_macros") (v "0.7.1") (h "07f21wg51n8nq60m773h70p2dyr06zpjjs58ywy89vvjkmm57byr") (y #t)))

(define-public crate-elastic_date_macros-0.7.2 (c (n "elastic_date_macros") (v "0.7.2") (h "1vzrhhh6prhm4aqv8mw6n5kdxrb6z2ljnc90n9ilf98y2x0qp8ch") (y #t)))

