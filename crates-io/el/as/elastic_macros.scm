(define-module (crates-io el as elastic_macros) #:use-module (crates-io))

(define-public crate-elastic_macros-0.1.0 (c (n "elastic_macros") (v "0.1.0") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1qh46azg4kmx7l69cq0d1ywlqvyya9s48wrfrldsbi04hzd6r6j3") (y #t)))

(define-public crate-elastic_macros-0.1.1 (c (n "elastic_macros") (v "0.1.1") (d (list (d (n "chrono") (r "~0.2.20") (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1f8cd727cs6nx3dy2wn84vc49d49fpcqbnqm0gkikka0cg4ppiw6") (y #t)))

(define-public crate-elastic_macros-0.1.2 (c (n "elastic_macros") (v "0.1.2") (d (list (d (n "chrono") (r "~0.2.20") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "11xmkz71g0z94vp4qzghllwqd966vrpsc3l8abbxznns052zvcf8") (f (quote (("types" "chrono") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_macros-0.1.3 (c (n "elastic_macros") (v "0.1.3") (d (list (d (n "chrono") (r "~0.2.20") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1y9zx0m9iglkxknf4ii607hc4jcpip4b6cifndpf28hybr4daaq3") (f (quote (("types" "chrono") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_macros-0.1.4 (c (n "elastic_macros") (v "0.1.4") (d (list (d (n "chrono") (r "~0.2.20") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (f (quote ("with-serde"))) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "04nr9hc8x99697ql00byxsjn10nkpk9hz9a76a6zwcwgz6krxna5") (f (quote (("types" "chrono") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_macros-0.2.0 (c (n "elastic_macros") (v "0.2.0") (h "1vv4pzlgba5490l4cyjsndf51d1gwmnghgbh8v2kl4hfdpzcsvxr") (y #t)))

(define-public crate-elastic_macros-0.2.1 (c (n "elastic_macros") (v "0.2.1") (h "0b0agsmjzhnrw8gjksmiln3h69qlfxqfdbhqfbfq6cf7fxc94c2q") (y #t)))

