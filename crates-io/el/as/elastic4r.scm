(define-module (crates-io el as elastic4r) #:use-module (crates-io))

(define-public crate-elastic4r-0.1.0 (c (n "elastic4r") (v "0.1.0") (d (list (d (n "confy") (r "^0.4.0") (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0i4nryzcx6pyvp76wd91qxpvc5cdfm9gbnrb10c40szqjs35500g")))

