(define-module (crates-io el as elastic-parser) #:use-module (crates-io))

(define-public crate-elastic-parser-0.1.0 (c (n "elastic-parser") (v "0.1.0") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "08kkhhdz1v1sz8n87x8ca4k487vpwa8vsn840mnrgbpng8dvxhdd")))

(define-public crate-elastic-parser-0.1.1 (c (n "elastic-parser") (v "0.1.1") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0chadzipaa7kj8gn6ch2yjcxjrvjgi716d1nilv9x2p8gcq43079")))

(define-public crate-elastic-parser-0.1.2 (c (n "elastic-parser") (v "0.1.2") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "13x8xbvz3l2cpj35zp5m0jn08srs8psw6vh67xy220k1g3wmliva")))

(define-public crate-elastic-parser-0.1.3 (c (n "elastic-parser") (v "0.1.3") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1bzznwss4wdqc10cmivbyczs4cvk9p05nrldgbgpica35f789brr")))

(define-public crate-elastic-parser-0.1.4 (c (n "elastic-parser") (v "0.1.4") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1dw6bmnl84i6q57rk2w75n5jdzq4wzady3jg0cm8kbaij73yf0kz")))

(define-public crate-elastic-parser-0.1.5 (c (n "elastic-parser") (v "0.1.5") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "15kq0akra14xqzgm9j46waxvjsi4hgc0rcqmbw556a8ibbvcskfn")))

(define-public crate-elastic-parser-0.1.6 (c (n "elastic-parser") (v "0.1.6") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "13dq3bj3wig6976hzsbd0cfxns3bs71qv6nl5grjkdvabvifhnmj")))

(define-public crate-elastic-parser-0.1.8 (c (n "elastic-parser") (v "0.1.8") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "15lhpw4q3x41xldx7jiak1hkr4infqr4c31v88q5n0wghjixamk1")))

(define-public crate-elastic-parser-0.1.9 (c (n "elastic-parser") (v "0.1.9") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1s54gdh6c1d0v81vpqnk8sj9bz5ly48ihnp5504n4dvhj91wz6dx")))

(define-public crate-elastic-parser-0.1.10 (c (n "elastic-parser") (v "0.1.10") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1bj7dm52x4vxs9x8m19nc9ny179af6xa7hv4svy2l2s02akqmb68")))

(define-public crate-elastic-parser-0.1.11 (c (n "elastic-parser") (v "0.1.11") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0rc3frs5mfh2k9ncm76s1nf2cw0jx2walmaz8sr1irmb5mx9mr12")))

(define-public crate-elastic-parser-0.1.12 (c (n "elastic-parser") (v "0.1.12") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0npvnkgnw8b464qk0a4z53wz6z77pilcn3j0vazl2lrsba3l4aa1")))

(define-public crate-elastic-parser-0.1.13 (c (n "elastic-parser") (v "0.1.13") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0ckva2vrcm9v9lc2c9pv7afpnd47g8648jkkkjw6r4rn1ncz6v3l")))

(define-public crate-elastic-parser-0.1.14 (c (n "elastic-parser") (v "0.1.14") (d (list (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1lflrgvvqqg3fz6qbb4pjixybb98mfvk7y80pb7cimlygr9qgglk")))

(define-public crate-elastic-parser-0.1.15 (c (n "elastic-parser") (v "0.1.15") (d (list (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "14aplk0bnzr95a21ppd1s17ccpx74gd7ylv6f9d9wdw2sd2vmci4")))

