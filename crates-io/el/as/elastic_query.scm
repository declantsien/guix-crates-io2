(define-module (crates-io el as elastic_query) #:use-module (crates-io))

(define-public crate-elastic_query-0.1.0 (c (n "elastic_query") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "00c9kq6dyk6qjm0d4qcv3wnwk936zjjrb4s3jq5lzkkikvx5sm7g")))

(define-public crate-elastic_query-0.2.0 (c (n "elastic_query") (v "0.2.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0qkkac9vhvk590ka9zd2garg2kmdamdx0hh8p4nvjfpsr2fm5fhs")))

(define-public crate-elastic_query-0.3.0 (c (n "elastic_query") (v "0.3.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "10jj50ks5d70bylidlj7nqah2brhkmasp98d6gxkd0r1nl4dbkpm")))

(define-public crate-elastic_query-0.4.0 (c (n "elastic_query") (v "0.4.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0zkpd70snlxwsj6xywx4c0j6h7fph5jiwwqji0rdalqnd0jbg8b3")))

(define-public crate-elastic_query-0.4.1 (c (n "elastic_query") (v "0.4.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "1pd0l9i9hq4p8mjvx1rpw9kpby8h05mpxg5bmkm1j67hg7b79b1i")))

(define-public crate-elastic_query-0.4.2 (c (n "elastic_query") (v "0.4.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0lglci34403mpcmhrkkpzqliig3x21phrg45xyzs2yjdiaf4yhad")))

(define-public crate-elastic_query-0.4.3 (c (n "elastic_query") (v "0.4.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "1xr0iixj9mvx7cz8zsndvixlvyx3kbws6arisq6cwz5sf9misg1d")))

(define-public crate-elastic_query-0.4.4 (c (n "elastic_query") (v "0.4.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "1gflrbaz0dn9s75awv7q6q9w1fvcrj79ri4b07mkb4gmmj8ac2m4")))

