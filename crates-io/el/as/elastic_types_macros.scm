(define-module (crates-io el as elastic_types_macros) #:use-module (crates-io))

(define-public crate-elastic_types_macros-0.1.0 (c (n "elastic_types_macros") (v "0.1.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "06nbhm1vw5a2bivja1krgwlxld681ppf7mn61i9b3m2nd5dlmj0r") (f (quote (("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.2.0 (c (n "elastic_types_macros") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1k6mv0dqd0cffpd44h24310c151icqff5xb0yswrcwa947acn2v0") (f (quote (("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.2.1 (c (n "elastic_types_macros") (v "0.2.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1pzzmz7jwqbqhc8x8zwr5kxlkbrhjirprf08b4lq429wd23h9n9m") (f (quote (("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.3.0 (c (n "elastic_types_macros") (v "0.3.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "11p7sdaz8w8i8af57ak7q09v2855bfcx842c7r0lfq59a2aynxnl") (f (quote (("serde-attrs") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.3.1 (c (n "elastic_types_macros") (v "0.3.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_item") (r "~0.1.0") (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "1skzkcsm9kqnccrdd7hwrnh3crqq69z2r2k0xw439db7ldavdga1") (f (quote (("serde-attrs") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.3.2 (c (n "elastic_types_macros") (v "0.3.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.2.0") (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "18zyj5r1997ckr7b42zppq057n5g8vyqa6mzfwyi26y7zp9qlx6z") (f (quote (("serde-attrs") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.3.3 (c (n "elastic_types_macros") (v "0.3.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.3.0") (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "0b35f6xpm92jab6idifyh8pll11lk4j0xsr6cwn5nmgy4s5iw9h4") (f (quote (("serde-attrs") ("nightly-testing" "clippy")))) (y #t)))

(define-public crate-elastic_types_macros-0.4.0 (c (n "elastic_types_macros") (v "0.4.0") (d (list (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.4.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.8.0") (d #t) (k 0)))) (h "0hp4sfgz1bhhvp6crbqcmy508l8clw3q73s1c13nwffdm52wl409") (f (quote (("nightly-testing")))) (y #t)))

(define-public crate-elastic_types_macros-0.5.0 (c (n "elastic_types_macros") (v "0.5.0") (d (list (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.8.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.8.0") (d #t) (k 0)))) (h "0ipjhl5kf3i486c54513dy8v1l9pbnj39higvxfgx9kj6mirykv7") (f (quote (("nightly-testing")))) (y #t)))

(define-public crate-elastic_types_macros-0.5.1 (c (n "elastic_types_macros") (v "0.5.1") (d (list (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.8.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.8.0") (d #t) (k 0)))) (h "00n6x6rvggh02cq0i1v9papscq4nxh24696axva5z3alwr33745q") (f (quote (("nightly-testing")))) (y #t)))

(define-public crate-elastic_types_macros-0.6.0 (c (n "elastic_types_macros") (v "0.6.0") (h "125lah58kgcpd4pdh757458c8ramjw7nb2mnpwxqpfxgrlpmazch") (y #t)))

(define-public crate-elastic_types_macros-0.6.1 (c (n "elastic_types_macros") (v "0.6.1") (h "0szc8j4yhgymdz56ldsmak8aj3bnvccznk37lxkpr7gifvajlbph") (y #t)))

(define-public crate-elastic_types_macros-0.6.2 (c (n "elastic_types_macros") (v "0.6.2") (h "0yw6f6kqh2hb05g82s4pv4b74n25mf9m2470and7kmmfslnzbyv3") (y #t)))

