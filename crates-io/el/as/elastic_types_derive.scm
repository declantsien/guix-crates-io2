(define-module (crates-io el as elastic_types_derive) #:use-module (crates-io))

(define-public crate-elastic_types_derive-0.6.0 (c (n "elastic_types_derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1v7rqhhq8azvvlfvvkciw1yilf575mfnvdkqfqx6yvjlj6fy86i1") (f (quote (("nightly-testing"))))))

(define-public crate-elastic_types_derive-0.6.1 (c (n "elastic_types_derive") (v "0.6.1") (d (list (d (n "post-expansion") (r "~0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1c3wc0fd1b4p2blpd5v3ip0nqvjb3i9hh0s3a3hr3jwcivv6hmp2") (f (quote (("nightly-testing"))))))

(define-public crate-elastic_types_derive-0.7.0 (c (n "elastic_types_derive") (v "0.7.0") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "16jaysaz9yzsqj4hpqhqymg95428bdg4lqd41ww352m46cly6a0y") (f (quote (("nightly-testing"))))))

(define-public crate-elastic_types_derive-0.8.0 (c (n "elastic_types_derive") (v "0.8.0") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0xfc7n8dpk2pvs8a9nsyjaqg6s76p3khw53c077y9i0smz7j8crm") (f (quote (("nightly-testing"))))))

(define-public crate-elastic_types_derive-0.9.0 (c (n "elastic_types_derive") (v "0.9.0") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1sdiz0l5pb520dysr1bpk20c8xh4mip5d58n5jmhwx2chxr50zpa") (f (quote (("nightly-testing") ("elastic"))))))

(define-public crate-elastic_types_derive-0.10.0 (c (n "elastic_types_derive") (v "0.10.0") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1bz8mxss8i2j4qqxvp6hmfzz52xs8jcp3n14wljlz6rycyi5djp4") (f (quote (("nightly-testing") ("elastic"))))))

(define-public crate-elastic_types_derive-0.10.1 (c (n "elastic_types_derive") (v "0.10.1") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0r50gnaww1wqzgavjazr8rfrgqzvpwyjak88y64iiqkbf4dxlxpn") (f (quote (("nightly-testing") ("elastic"))))))

(define-public crate-elastic_types_derive-0.10.2 (c (n "elastic_types_derive") (v "0.10.2") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.8.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.10.0") (k 0)) (d (n "serde_json") (r "~0.8.0") (d #t) (k 0)) (d (n "syn") (r "~0.9.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1dp3sxf3wgfzpwrwsibimccqy7w2ly6f4ci7pygqr4vlnfn4dy86") (f (quote (("nightly") ("elastic"))))))

(define-public crate-elastic_types_derive-0.12.0 (c (n "elastic_types_derive") (v "0.12.0") (d (list (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "~0.9.0") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "~0.12.0") (k 0)) (d (n "serde_json") (r "~0.9.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "091ml3ck8zpbd6iqram3kvdld5p6hj3ksbdf2xc07gp9axzxz2r1") (f (quote (("nightly") ("elastic"))))))

(define-public crate-elastic_types_derive-0.13.0 (c (n "elastic_types_derive") (v "0.13.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.1.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0cpa2h0a7xrykpivc0mhmhrd11km75xziv1y1lxwkb7py9hqw40b")))

(define-public crate-elastic_types_derive-0.14.0 (c (n "elastic_types_derive") (v "0.14.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.2.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0myz0lamq4k6ndqmg6pblxb9658p7z45cgh57578pc6nk5vbvgjv")))

(define-public crate-elastic_types_derive-0.15.0 (c (n "elastic_types_derive") (v "0.15.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.3.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "07zahx4dfd3vhx58292w3p94hm57qslhnks3061prdpg1lzzppya")))

(define-public crate-elastic_types_derive-0.16.0 (c (n "elastic_types_derive") (v "0.16.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.16.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0f2c0hyfl93rnnqmlh2dw3hfkq3l9z6xm3g2k0x6884nkrfr6041")))

(define-public crate-elastic_types_derive-0.17.0 (c (n "elastic_types_derive") (v "0.17.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "198lsdxlnljikq5lfigi92y4da8l3wd4m3wszvkqpgzx34ayr5di")))

(define-public crate-elastic_types_derive-0.17.1 (c (n "elastic_types_derive") (v "0.17.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0d2pabpw84c2hc5nnd6yrh1xh0lsbjskip81a51rprxxiwc1ak04")))

(define-public crate-elastic_types_derive-0.17.2 (c (n "elastic_types_derive") (v "0.17.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.17.2") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0zb2glm2hb87cg5p2s68i89mq3l1f3zsq88r58adz563pi4cqwaq")))

(define-public crate-elastic_types_derive-0.18.0 (c (n "elastic_types_derive") (v "0.18.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.18.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "1vc3rq271sf09j22xs4cbyz3ch0f0ys1mfr8g946vi31s7fx54s3")))

(define-public crate-elastic_types_derive-0.19.0 (c (n "elastic_types_derive") (v "0.19.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.19.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "13m4imcc5dp7hbcv23ybis71nn6kw95ax829py3pvmvl6vs4mkfv")))

(define-public crate-elastic_types_derive-0.19.1 (c (n "elastic_types_derive") (v "0.19.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.19.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("aster" "visit" "parsing" "full"))) (d #t) (k 0)))) (h "0ygm1c747vkkbq8gr8iw5npvj3jkrsqnm1x30ibyr6n17ya3dpfx")))

(define-public crate-elastic_types_derive-0.20.0 (c (n "elastic_types_derive") (v "0.20.0") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09669day5kj7zj78w773wvahnlwvdaaq79rlffz0k97r1diijhcz")))

(define-public crate-elastic_types_derive-0.20.1 (c (n "elastic_types_derive") (v "0.20.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "160md5phh3vvxbqpwwzbnb8kdqf00kc1j28ng4q0r364idnby6qd")))

(define-public crate-elastic_types_derive-0.20.2 (c (n "elastic_types_derive") (v "0.20.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "168k38lg0y74cky1b0aaj6vvfcfwhrxinnvpffhw8dn86d19m1zr")))

(define-public crate-elastic_types_derive-0.20.3 (c (n "elastic_types_derive") (v "0.20.3") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgaac4akq3qqlq5zqlyzlnr2vl7qc80dcd12q1xqjfdxyxlhf0m")))

(define-public crate-elastic_types_derive-0.20.4 (c (n "elastic_types_derive") (v "0.20.4") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0my3bccpmbgnkfbii3x08r21w18hwan8f2zxaj2hvxb6488a7f3m")))

(define-public crate-elastic_types_derive-0.20.5 (c (n "elastic_types_derive") (v "0.20.5") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dlhwy3hsd28fdm5297zy8accyqgpzy3in0xydcbc8lyg250n2z9")))

(define-public crate-elastic_types_derive-0.20.6 (c (n "elastic_types_derive") (v "0.20.6") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13s1g0ba6wa6v1x2zrm1ww8i4hv7impq0q9xcvmrc9bbxi1r4yvs")))

(define-public crate-elastic_types_derive-0.20.7 (c (n "elastic_types_derive") (v "0.20.7") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0raav1vh6wari6j27fin50p2cdz9c1lj658rdjrb043b9yj1k9ag")))

(define-public crate-elastic_types_derive-0.20.8 (c (n "elastic_types_derive") (v "0.20.8") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10favy73wqq894kab911ldvg5wa0w3v2gp7kn793qinkig381hl9")))

(define-public crate-elastic_types_derive-0.20.9 (c (n "elastic_types_derive") (v "0.20.9") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02kcbl6sqf3f6r88g91a6lli03k07pr6ckibib9gipfg7bbl027j")))

(define-public crate-elastic_types_derive-0.20.10 (c (n "elastic_types_derive") (v "0.20.10") (d (list (d (n "elastic_types_derive_internals") (r "~0.20.0") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yhwmbj1mf3jm6gm822sincwm5cgggiklycaaq3axzl98n3g2lfw")))

(define-public crate-elastic_types_derive-0.21.0-pre.1 (c (n "elastic_types_derive") (v "0.21.0-pre.1") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.1") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19ff7b7x64fkz2f5jvd1cw3ppjc8y0zg0f5hw4j2q4bbxz5hsbnh")))

(define-public crate-elastic_types_derive-0.21.0-pre.2 (c (n "elastic_types_derive") (v "0.21.0-pre.2") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.2") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mzlr3cq7zb9g4gjj4w0d1anr2jhyd23019wdzg1gk9srf2v218r")))

(define-public crate-elastic_types_derive-0.21.0-pre.3 (c (n "elastic_types_derive") (v "0.21.0-pre.3") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.3") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vc2wzhw6lajyp8hk40jlgfvx5j62m1v3b83r81iyypbd7b0da5f")))

(define-public crate-elastic_types_derive-0.21.0-pre.4 (c (n "elastic_types_derive") (v "0.21.0-pre.4") (d (list (d (n "elastic_types_derive_internals") (r "~0.21.0-pre.4") (d #t) (k 0)) (d (n "quote") (r "~0.3.0") (d #t) (k 0)) (d (n "syn") (r "~0.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rqvksq88mdmdvrp0w58965kbh0gpzs0l674m5dg8l6ial7cnfh3")))

