(define-module (crates-io el as elastic-queries) #:use-module (crates-io))

(define-public crate-elastic-queries-0.1.0 (c (n "elastic-queries") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.164") (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0wdpswsc0dmljxif463xz576d2cq208jwzw0md416khs12kiyi00") (f (quote (("default"))))))

