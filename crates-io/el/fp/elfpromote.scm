(define-module (crates-io el fp elfpromote) #:use-module (crates-io))

(define-public crate-elfpromote-0.1.0 (c (n "elfpromote") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atomicwrites") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "goblin") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.5") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0bj5798vi24mcm1x9pcgnmh8zlwcadfw12823v2vlcskxv0rll59")))

