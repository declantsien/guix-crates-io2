(define-module (crates-io el fu elfutils) #:use-module (crates-io))

(define-public crate-elfutils-0.0.1 (c (n "elfutils") (v "0.0.1") (d (list (d (n "libdw") (r "^0.0.1") (d #t) (k 0)) (d (n "libdwelf") (r "^0.0.1") (d #t) (k 0)) (d (n "libdwfl") (r "^0.0.1") (d #t) (k 0)) (d (n "libelf") (r "^0.0.1") (d #t) (k 0)))) (h "1lx8fwssa2h2bn0hcizcg2a0s6vj19p9d4qb8sa1gdb611c4z414")))

(define-public crate-elfutils-0.0.2 (c (n "elfutils") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "libdw") (r "^0.0.2") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libdwelf") (r "^0.0.2") (d #t) (k 0)) (d (n "libdwfl") (r "^0.0.2") (d #t) (k 0)) (d (n "libelf") (r "^0.0.2") (d #t) (k 0)))) (h "0ndhv1c4dg8w9srmbyfrin281rwvfpvfji9vjy7hs77cj128nkgl")))

(define-public crate-elfutils-0.0.4 (c (n "elfutils") (v "0.0.4") (d (list (d (n "cpp_demangle") (r "^0.2.5") (d #t) (k 2)) (d (n "libdw") (r "^0.0.4") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libdwelf") (r "^0.0.4") (d #t) (k 0)) (d (n "libdwfl") (r "^0.0.4") (d #t) (k 0)) (d (n "libelf") (r "^0.0.4") (d #t) (k 0)))) (h "0x7pigf62bpi8gpi713n7lfmgl79m1hbzydf56q8dj86wp0igw8n")))

(define-public crate-elfutils-0.0.5 (c (n "elfutils") (v "0.0.5") (d (list (d (n "cpp_demangle") (r "^0.2.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libdw") (r "^0.0.5") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libdwelf") (r "^0.0.5") (d #t) (k 0)) (d (n "libdwfl") (r "^0.0.5") (d #t) (k 0)) (d (n "libelf") (r "^0.0.5") (d #t) (k 0)))) (h "0fzinivlxwg5fpd7h4svj5lsdpc929iflc2zxd1yy6a6pg4idgvj")))

(define-public crate-elfutils-0.0.6 (c (n "elfutils") (v "0.0.6") (d (list (d (n "cpp_demangle") (r "^0.2.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libdw") (r "^0.0.6") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libdwelf") (r "^0.0.6") (d #t) (k 0)) (d (n "libdwfl") (r "^0.0.6") (d #t) (k 0)) (d (n "libelf") (r "^0.0.6") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.15") (d #t) (k 2)))) (h "0sdffwz2frb24h0j10ap3hflf62dm6ym26yl6smsmg85mywdl7gg")))

(define-public crate-elfutils-0.1.0 (c (n "elfutils") (v "0.1.0") (d (list (d (n "cpp_demangle") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libdw") (r "^0.1.0") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libdwelf") (r "^0.1.0") (d #t) (k 0)) (d (n "libdwfl") (r "^0.1.0") (d #t) (k 0)) (d (n "libelf") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.15") (d #t) (k 2)))) (h "12hvnqm1p5cs75jh4wlh3sv8y3s8sv5kgnq10vzpqbaa88qw46yx")))

