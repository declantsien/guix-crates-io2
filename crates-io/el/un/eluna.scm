(define-module (crates-io el un eluna) #:use-module (crates-io))

(define-public crate-eluna-0.1.0 (c (n "eluna") (v "0.1.0") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 0)))) (h "106zd8npvn490ffpxx4p2shgixhrdi5jpc8aapbgkar3nhw1wp3j")))

(define-public crate-eluna-0.1.1 (c (n "eluna") (v "0.1.1") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 0)))) (h "0m4ha7qa1kra5vyddxzilgpwpgyp54r63rkpai3aw604fv9zdn00")))

(define-public crate-eluna-0.1.2 (c (n "eluna") (v "0.1.2") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 0)))) (h "158ba0dc6i6y5wfixg9bkjnx9ksikc114ia6kdk4kb5m9b3kwqv3")))

