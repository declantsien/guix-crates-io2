(define-module (crates-io el b- elb-rebar) #:use-module (crates-io))

(define-public crate-elb-rebar-0.0.1 (c (n "elb-rebar") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "06qv19zlx8jc5ahsrjrkq1cvzys43ibnhm0ic9hqc39yp9lzxkm4")))

(define-public crate-elb-rebar-0.0.2 (c (n "elb-rebar") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0yc5iq7kj1hayb1xzqm84h2d9hd1lcpfapya3s3k30czsa4p8cqn")))

