(define-module (crates-io el li ellie_fmt) #:use-module (crates-io))

(define-public crate-ellie_fmt-0.4.0 (c (n "ellie_fmt") (v "0.4.0") (d (list (d (n "ellie_core") (r "^0.6.0") (d #t) (k 0)) (d (n "ellie_tokenizer") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "09dxglphf131nv2xxl8k3bg4krskfwf3qa0y0xckx1k21d4li8x6") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_fmt-0.4.1 (c (n "ellie_fmt") (v "0.4.1") (d (list (d (n "ellie_core") (r "^0.6.1") (d #t) (k 0)) (d (n "ellie_tokenizer") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "14dbr3r41d7sf2c8vsqkmwb3r1nn77865alxpzff7yac9v97739g") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_fmt-0.5.0 (c (n "ellie_fmt") (v "0.5.0") (d (list (d (n "ellie_core") (r "^0.7.0") (d #t) (k 0)) (d (n "ellie_tokenizer") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1gcx9x6p1dbb2xr6jylww2acnplxx64g9sc81k3q7fj00amp6l7f") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_fmt-0.5.2 (c (n "ellie_fmt") (v "0.5.2") (d (list (d (n "ellie_core") (r "^0.7.1") (d #t) (k 0)) (d (n "ellie_tokenizer") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "01f3s08xyi7c303mhbq1z1gdnazjcfcbvr13vixsj54j1sf8mvnr") (f (quote (("std") ("default" "std"))))))

