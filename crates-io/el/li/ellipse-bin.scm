(define-module (crates-io el li ellipse-bin) #:use-module (crates-io))

(define-public crate-ellipse-bin-1.0.0 (c (n "ellipse-bin") (v "1.0.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ellipse") (r "^0.2") (d #t) (k 0)))) (h "1n0pxll25fbnbz7dbbxm8vmma41s1ycwwj7min6ys1g5x4l0lnj3")))

