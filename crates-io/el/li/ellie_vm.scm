(define-module (crates-io el li ellie_vm) #:use-module (crates-io))

(define-public crate-ellie_vm-0.4.0 (c (n "ellie_vm") (v "0.4.0") (d (list (d (n "ellie_core") (r "^0.6.0") (k 0)) (d (n "mimalloc") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "1v8n5hsz7vghz3cals68mf44bwww6wcf3gk80p941rmp8j3jdlr9") (f (quote (("std") ("default") ("alternate_alloc" "mimalloc"))))))

(define-public crate-ellie_vm-0.4.1 (c (n "ellie_vm") (v "0.4.1") (d (list (d (n "ellie_core") (r "^0.6.1") (k 0)) (d (n "mimalloc") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "1pvr21w7sl0wbmjlrp7hjiqhzjaqvggx1k6yhlphyp3r3h5b6bqh") (f (quote (("std") ("default") ("alternate_alloc" "mimalloc"))))))

(define-public crate-ellie_vm-0.5.0 (c (n "ellie_vm") (v "0.5.0") (d (list (d (n "ellie_core") (r "^0.7.0") (k 0)) (d (n "mimalloc") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "1hm9j3wpm7kvivbxnm5lnr5qfamh32wxwzwpnldfyzmiy6r1w48m") (f (quote (("std") ("default") ("alternate_alloc" "mimalloc"))))))

(define-public crate-ellie_vm-0.5.4 (c (n "ellie_vm") (v "0.5.4") (d (list (d (n "ellie_core") (r "^0.7.2") (k 0)) (d (n "mimalloc") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "1xndh1nha65lzwbffw911anvkrlvarxl6bz5bfsaqjbmsagd2c9z") (f (quote (("std") ("default") ("alternate_alloc" "mimalloc"))))))

