(define-module (crates-io el li ellipsis-transaction-utils) #:use-module (crates-io))

(define-public crate-ellipsis-transaction-utils-0.1.0 (c (n "ellipsis-transaction-utils") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.14.9") (d #t) (k 0)))) (h "1g8z7d2k3xjlxbh94kwml7wpy28ks50bm3pvdqs8dapkm8qlhx7x")))

(define-public crate-ellipsis-transaction-utils-0.1.1 (c (n "ellipsis-transaction-utils") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.9") (d #t) (k 0)) (d (n "solana-transaction-status") (r "^1.14.9") (d #t) (k 0)))) (h "0pkzj3fx0py69vqg06b36v6amwzzq9w2qr51xgcayfgbzvxbl23v")))

(define-public crate-ellipsis-transaction-utils-1.0.0 (c (n "ellipsis-transaction-utils") (v "1.0.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "solana-sdk") (r ">=1.14.12, <1.19.0") (d #t) (k 0)) (d (n "solana-transaction-status") (r ">=1.14.12, <1.19.0") (d #t) (k 0)))) (h "1dssr1vkz3dcga5wd7q4zv7inkymprg4i6757qwrsb9ax035vis7")))

