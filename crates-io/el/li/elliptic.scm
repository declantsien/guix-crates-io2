(define-module (crates-io el li elliptic) #:use-module (crates-io))

(define-public crate-elliptic-0.1.0 (c (n "elliptic") (v "0.1.0") (d (list (d (n "elliptic-sys") (r "^0.1.0") (d #t) (k 0)))) (h "05jrsbjgsn7m539l1a52pc12ww1wbqlxv4lfcbpbiklsx3sywyha")))

(define-public crate-elliptic-0.2.0 (c (n "elliptic") (v "0.2.0") (d (list (d (n "elliptic-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1zg9i5a94ckmm4fy0zlh0q7dqkjalmkgcd8z7k5bzhypn3qj16bh")))

(define-public crate-elliptic-0.3.0 (c (n "elliptic") (v "0.3.0") (d (list (d (n "elliptic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0fbxfddfjnvq2dfv6bsmsga14xkn16ilv2dmdw8qhq0i78d74hqz")))

(define-public crate-elliptic-0.4.0 (c (n "elliptic") (v "0.4.0") (d (list (d (n "elliptic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0a4g6p4wfzznw4rvg2a9rkbqfcbqxfmjjd1mgbgfy5ij5h6agc2s") (y #t)))

(define-public crate-elliptic-0.5.0 (c (n "elliptic") (v "0.5.0") (d (list (d (n "elliptic-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0m5y4rw34gpqk1h5pf5zkhs7hrk5m9zjmkhm0qsw159k3w2vyz0y")))

