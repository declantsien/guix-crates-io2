(define-module (crates-io el li ellipticoin-api) #:use-module (crates-io))

(define-public crate-ellipticoin-api-0.0.2 (c (n "ellipticoin-api") (v "0.0.2") (d (list (d (n "wasm-rpc") (r "^0.2.13") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.13") (d #t) (k 0)))) (h "0c4rp2s453gjs6lsc984j8rqfgf17b4pvg000lac7dvdr5fac3gl")))

