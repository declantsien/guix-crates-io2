(define-module (crates-io el li ellipticoin-test-framework) #:use-module (crates-io))

(define-public crate-ellipticoin-test-framework-0.1.0 (c (n "ellipticoin-test-framework") (v "0.1.0") (d (list (d (n "cask") (r "0.*") (d #t) (k 0)) (d (n "ellipticoin") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.1.0") (d #t) (k 0)))) (h "0l5wxazdk3d1fa1rgv81qz4zxpwigd5qmszy8whbdqxqck57m0j3")))

(define-public crate-ellipticoin-test-framework-0.1.2 (c (n "ellipticoin-test-framework") (v "0.1.2") (d (list (d (n "ellipticoin") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.11") (d #t) (k 0)))) (h "0bcvsz0akpaqg0f6jkcv4js2rnnlzafi5myj9ijcbf0f3fps73x5")))

(define-public crate-ellipticoin-test-framework-0.1.3 (c (n "ellipticoin-test-framework") (v "0.1.3") (d (list (d (n "ellipticoin") (r "^0.1.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.11") (d #t) (k 0)))) (h "0njaz1xnxsmzr8n60r4722ihyikddf1dwpcv8ggb8bhd51mmk5f6")))

(define-public crate-ellipticoin-test-framework-0.1.4 (c (n "ellipticoin-test-framework") (v "0.1.4") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "ellipticoin") (r "^0.0.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.16") (d #t) (k 0)))) (h "0i95n6qadi03k7ri0dcmg72r8w4cbiv8ngdp8kpl1hcynrbpnjn8")))

