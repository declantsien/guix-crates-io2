(define-module (crates-io el li ellidri-unicase) #:use-module (crates-io))

(define-public crate-ellidri-unicase-1.0.0 (c (n "ellidri-unicase") (v "1.0.0") (h "03bz4y83i1w00v9lvb5zn6whnkpd4xsf6m1ms7j3bfaxs1q1xpcq")))

(define-public crate-ellidri-unicase-2.0.0 (c (n "ellidri-unicase") (v "2.0.0") (h "075j6ddw6icw58pyh33d1clalrvn1bp4xxj3ihy2p4jx4anig5gi")))

(define-public crate-ellidri-unicase-2.1.0 (c (n "ellidri-unicase") (v "2.1.0") (h "14ak17iv4dim4xnbg1qk7zmwibqsvrzjd70bykycdjicblyp2v9p")))

