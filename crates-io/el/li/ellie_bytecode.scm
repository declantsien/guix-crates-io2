(define-module (crates-io el li ellie_bytecode) #:use-module (crates-io))

(define-public crate-ellie_bytecode-0.3.0 (c (n "ellie_bytecode") (v "0.3.0") (d (list (d (n "ellie_core") (r "^0.6.0") (d #t) (k 0)) (d (n "ellie_parser") (r "^0.6.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fi7ql994q6gkbrk80alf8imikrlgcicpgphn7pfwlkfzlj8z9if") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_bytecode-0.3.1 (c (n "ellie_bytecode") (v "0.3.1") (d (list (d (n "ellie_core") (r "^0.6.1") (d #t) (k 0)) (d (n "ellie_parser") (r "^0.6.1") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qn2w2h56g8ms2m24ip8ycixfsbn66d9xi9wkfaq90svqhmrcgrm") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_bytecode-0.4.0 (c (n "ellie_bytecode") (v "0.4.0") (d (list (d (n "ellie_core") (r "^0.7.0") (d #t) (k 0)) (d (n "ellie_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0352hig5vwr6np9av2xq111yfpdkybsxsrs71q7gwbr9885xpvp1") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_bytecode-0.4.2 (c (n "ellie_bytecode") (v "0.4.2") (d (list (d (n "ellie_core") (r "^0.7.1") (d #t) (k 0)) (d (n "ellie_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vnz3rs78rzh83qwgidfvy0nls276f2j2izmqinlsqrcxbsw3w3q") (f (quote (("std") ("default" "std"))))))

(define-public crate-ellie_bytecode-0.4.3 (c (n "ellie_bytecode") (v "0.4.3") (d (list (d (n "ellie_core") (r "^0.7.2") (d #t) (k 0)) (d (n "ellie_parser") (r "^0.7.2") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g2glnns70xpswpl4bz8vp2g1v481iwahfsk0p6l8y1404q6y0wm") (f (quote (("std") ("default" "std"))))))

