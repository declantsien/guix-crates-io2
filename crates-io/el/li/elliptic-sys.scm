(define-module (crates-io el li elliptic-sys) #:use-module (crates-io))

(define-public crate-elliptic-sys-0.1.0 (c (n "elliptic-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "03wyrplwn6pdzjlfcydx84ismq48j4mgqb9bvvv70rwwcn51zbqh")))

(define-public crate-elliptic-sys-0.2.0 (c (n "elliptic-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0xhmkn5vq9izpmiqzniy82h31dr8y6j3jm156p1pbhpydhf9d90k")))

(define-public crate-elliptic-sys-0.3.0 (c (n "elliptic-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "1ifrn9yj45my64gg2msvhjpdckvd2m7jcjivplpx2aimx58gqyv6")))

