(define-module (crates-io el li elliptic_curve_cryptography_tools) #:use-module (crates-io))

(define-public crate-elliptic_curve_cryptography_tools-0.1.0 (c (n "elliptic_curve_cryptography_tools") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)))) (h "1qmhkf99ddf7c668j9adii9s9m2ah7i4y1cn25d5c4m9y4d8hnzg")))

