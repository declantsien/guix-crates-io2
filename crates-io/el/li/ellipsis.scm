(define-module (crates-io el li ellipsis) #:use-module (crates-io))

(define-public crate-ellipsis-0.1.0 (c (n "ellipsis") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "git2") (r "^0.7.3") (d #t) (k 0)) (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.19") (d #t) (k 0)))) (h "0fd89r2b88frlshdylnc106pwfwcjdzyzl21nwwnxndxmnrqbqs1")))

