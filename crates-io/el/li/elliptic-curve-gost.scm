(define-module (crates-io el li elliptic-curve-gost) #:use-module (crates-io))

(define-public crate-elliptic-curve-gost-0.1.0 (c (n "elliptic-curve-gost") (v "0.1.0") (d (list (d (n "crypto-bigint") (r "^0.5.1") (d #t) (k 0)))) (h "0yxadv526jyzn4cdydjc92k6019ksx1l7h5fgf49cq5s89srljha")))

