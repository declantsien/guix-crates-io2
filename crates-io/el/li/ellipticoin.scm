(define-module (crates-io el li ellipticoin) #:use-module (crates-io))

(define-public crate-ellipticoin-0.1.0 (c (n "ellipticoin") (v "0.1.0") (d (list (d (n "wasm-rpc") (r "^0.1.0") (d #t) (k 0)))) (h "0q009z96cg3m602d2n7hxhcj6yvhk65s9lacgjj0n1jjrzj0xyf1")))

(define-public crate-ellipticoin-0.1.1 (c (n "ellipticoin") (v "0.1.1") (d (list (d (n "wasm-rpc") (r "^0.1.3") (d #t) (k 0)))) (h "09q1zc94lqym7kyakpxp0v91fwamg6dgcw5mxm1y748kx8x6mbqs")))

(define-public crate-ellipticoin-0.1.2 (c (n "ellipticoin") (v "0.1.2") (d (list (d (n "wasm-rpc") (r "^0.1.3") (d #t) (k 0)))) (h "1fm879sfb2f6k42criw24mz58v5ypl0kpgcrrjbdnxnpkvj2013k")))

(define-public crate-ellipticoin-0.1.3 (c (n "ellipticoin") (v "0.1.3") (d (list (d (n "wasm-rpc") (r "^0.2.7") (d #t) (k 0)))) (h "0nq5qfj0a6f812np3adj2wb0gd8606z1vq7mcgwi41vj3zx05aph")))

(define-public crate-ellipticoin-0.1.4 (c (n "ellipticoin") (v "0.1.4") (d (list (d (n "wasm-rpc") (r "^0.2.11") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.11") (d #t) (k 0)))) (h "05b2bc482dngf33qbcvk8ywb2kc2cxwvi1qqypms4n9n3frhwzky")))

(define-public crate-ellipticoin-0.1.5 (c (n "ellipticoin") (v "0.1.5") (d (list (d (n "wasm-rpc") (r "^0.2.12") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.12") (d #t) (k 0)))) (h "18n1g8f6ny9xrgnfqdw9gbadh0k5m0i64sgyb8vxd12h35a2i5n1")))

(define-public crate-ellipticoin-0.1.6 (c (n "ellipticoin") (v "0.1.6") (d (list (d (n "ellipticoin-api") (r "^0.0.2") (d #t) (k 0)) (d (n "ellipticoin-mock-api") (r "^0.1.3") (d #t) (k 0)))) (h "18ky9kb831sqvc5fw3k0xvmbwqncmwzn595xlh04smkcgyzmqypz")))

(define-public crate-ellipticoin-0.0.3 (c (n "ellipticoin") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.15") (d #t) (k 0)))) (h "0hgw43mci2adxlifhsl29y5i603d63065dgylf0k29dx79jrr7dq")))

(define-public crate-ellipticoin-0.0.4 (c (n "ellipticoin") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.15") (d #t) (k 0)))) (h "0kll22wsmvdy3b9spxdibdsvzqhnbhamrjgyl6fxlb11nh473ryw")))

(define-public crate-ellipticoin-0.0.5 (c (n "ellipticoin") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-rpc") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.15") (d #t) (k 0)))) (h "1dqf7hkg7qymi10x1rcwlf6429rf33446lj7l0w15ahxibb3sbhq")))

