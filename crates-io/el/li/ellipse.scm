(define-module (crates-io el li ellipse) #:use-module (crates-io))

(define-public crate-ellipse-0.1.0 (c (n "ellipse") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "072nxb8r6dsf1x57vy087d9lnanyvq51kpkc79ljvihvlb7gmzj2")))

(define-public crate-ellipse-0.2.0 (c (n "ellipse") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0b85zbk54bcza4dckxwxzbvp3bv5d9w9kkz774v3kjg510mahd8q")))

