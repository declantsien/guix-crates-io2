(define-module (crates-io el li ellipticoin-mock-api) #:use-module (crates-io))

(define-public crate-ellipticoin-mock-api-0.1.3 (c (n "ellipticoin-mock-api") (v "0.1.3") (d (list (d (n "wasm-rpc") (r "^0.2.13") (d #t) (k 0)) (d (n "wasm-rpc-macros") (r "^0.2.13") (d #t) (k 0)))) (h "1rq3r0m8y7vlngw0pcckk76l55pfi1b2y6sxrplj48dzk5w2m92k")))

