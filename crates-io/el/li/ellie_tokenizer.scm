(define-module (crates-io el li ellie_tokenizer) #:use-module (crates-io))

(define-public crate-ellie_tokenizer-0.5.0 (c (n "ellie_tokenizer") (v "0.5.0") (d (list (d (n "ellie_core") (r "^0.6.0") (f (quote ("compiler_utils"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0b6ajf6a93b46l40pbczg0bhkx8f45a5358kds5wvlwmq5qx8vlh") (f (quote (("std") ("default"))))))

(define-public crate-ellie_tokenizer-0.5.1 (c (n "ellie_tokenizer") (v "0.5.1") (d (list (d (n "ellie_core") (r "^0.6.1") (f (quote ("compiler_utils"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1j4f78gq0w1ksxvf6q2wh2cz12qhxm0fi58gizywns6vs1a51zmd") (f (quote (("std") ("default"))))))

(define-public crate-ellie_tokenizer-0.6.0 (c (n "ellie_tokenizer") (v "0.6.0") (d (list (d (n "ellie_core") (r "^0.7.0") (f (quote ("compiler_utils"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0sayh4hasykgw2h6zqakljnslbxiyldb98vzc53ab1wq654kmi2a") (f (quote (("std") ("default"))))))

(define-public crate-ellie_tokenizer-0.6.1 (c (n "ellie_tokenizer") (v "0.6.1") (d (list (d (n "ellie_core") (r "^0.7.1") (f (quote ("compiler_utils"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0argk6lh2qkh7dh6b6dcd4yfr1bm0szvc5y9dnkrc5fx1aqm294v") (f (quote (("std") ("default"))))))

(define-public crate-ellie_tokenizer-0.6.2 (c (n "ellie_tokenizer") (v "0.6.2") (d (list (d (n "ellie_core") (r "^0.7.2") (f (quote ("compiler_utils"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1m0gvrnz3ih4pndbyp3nlywgxvkxqrl36qcp7rln74plzj1m5cmx") (f (quote (("std") ("default"))))))

