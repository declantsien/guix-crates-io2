(define-module (crates-io el li ellie_native_bridge) #:use-module (crates-io))

(define-public crate-ellie_native_bridge-0.1.0 (c (n "ellie_native_bridge") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)))) (h "0590wdrac7rza2xbcbfsn0b16ms3jhvjar06yma99wam88svbkia")))

