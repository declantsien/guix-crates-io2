(define-module (crates-io el li ellidri-reader) #:use-module (crates-io))

(define-public crate-ellidri-reader-0.1.0 (c (n "ellidri-reader") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "memchr") (r "^2") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (k 0)))) (h "0fqqjxkzj9bvbg0xbqs6ci789qchxwzbbwgsajmbl3pzwqg716gd")))

