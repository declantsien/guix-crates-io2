(define-module (crates-io el oq eloquent_core) #:use-module (crates-io))

(define-public crate-eloquent_core-0.1.1 (c (n "eloquent_core") (v "0.1.1") (h "046p6hyz69ywswnhigxdf8kzf7i3swh9bv9ik4c742q8kzbp59jv")))

(define-public crate-eloquent_core-0.1.2 (c (n "eloquent_core") (v "0.1.2") (h "1wj4ddkj8kanf3sb0qc3dn9sl00fkndiwplk42yzjhz8jc3mnscb")))

(define-public crate-eloquent_core-0.1.3 (c (n "eloquent_core") (v "0.1.3") (h "14s1i1mbjsixlrbsjw96xy0nd8kjdakk8kfv9fr10i6silhq1q1j")))

(define-public crate-eloquent_core-0.1.4 (c (n "eloquent_core") (v "0.1.4") (h "1rz7ldn3nr6afhxbhxj1z6djrxvgkrg3q7minnyc42ip98i9wgns")))

(define-public crate-eloquent_core-0.2.0 (c (n "eloquent_core") (v "0.2.0") (h "1vj5r0g3r7d3g19ddiz6vbjab3w6r6hpkm1j9bfjz6116yz0y4id")))

(define-public crate-eloquent_core-1.0.0 (c (n "eloquent_core") (v "1.0.0") (h "1xdcxz9rs4wrjinff5vkk9i67aasjdpyb32vyqfm0ac8sgzd8ym9")))

