(define-module (crates-io el oq eloquent) #:use-module (crates-io))

(define-public crate-eloquent-0.1.0 (c (n "eloquent") (v "0.1.0") (h "1a4hrsvxz4cyqxhgnxhwwvd68697wkw51gg19cfi8d3i9mw1ss1n")))

(define-public crate-eloquent-0.1.1 (c (n "eloquent") (v "0.1.1") (d (list (d (n "eloquent_core") (r "^0.1.1") (d #t) (k 0)))) (h "11vxdkn8gkfxnysknw54p2ysfijv1b19byrpw734xzflpjqldx72")))

(define-public crate-eloquent-0.1.2 (c (n "eloquent") (v "0.1.2") (d (list (d (n "eloquent_core") (r "^0.1.2") (d #t) (k 0)))) (h "165zigldf1lw41as8df55dssylsyn59zpcx8g7gl6a1xv1q32nab")))

(define-public crate-eloquent-0.1.3 (c (n "eloquent") (v "0.1.3") (d (list (d (n "eloquent_core") (r "^0.1.3") (d #t) (k 0)))) (h "1nmf2zz07g0whsr48z4aa1ms0wwvx5jxrym82q52x94h4sgi4nrs")))

(define-public crate-eloquent-0.1.4 (c (n "eloquent") (v "0.1.4") (d (list (d (n "eloquent_core") (r "^0.1.4") (d #t) (k 0)))) (h "1zmbphfwiazi0kyvv404bj8rwgkxww5mcgysjgny0ni2c6s26kmm")))

(define-public crate-eloquent-0.2.0 (c (n "eloquent") (v "0.2.0") (d (list (d (n "eloquent_core") (r "^0.2") (d #t) (k 0)))) (h "07wwi7caxmv9vq4q3f5c12iw7y65yiyd4fzdqhvcr08hvvb7fg5f")))

(define-public crate-eloquent-0.2.1 (c (n "eloquent") (v "0.2.1") (d (list (d (n "eloquent_core") (r "^0.2") (d #t) (k 0)))) (h "0q009bsv7v9lh74hyvfw4hf5cmynhwfliz1rvwf10nfvkw03d9hn")))

(define-public crate-eloquent-1.0.0 (c (n "eloquent") (v "1.0.0") (d (list (d (n "eloquent_core") (r "^1.0") (d #t) (k 0)))) (h "05q0ws49d8laqc8f3790ir92i7mh1kvvi0abr8bdwdkjgysdfah3")))

