(define-module (crates-io el fb elfbin) #:use-module (crates-io))

(define-public crate-elfbin-0.1.0 (c (n "elfbin") (v "0.1.0") (d (list (d (n "binbin") (r "^0.2.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0wwynlha6pihsg4z7nm85qxbi15jz064h5b8f7zc7m9pahl1d8pk")))

(define-public crate-elfbin-0.2.0 (c (n "elfbin") (v "0.2.0") (d (list (d (n "binbin") (r "^0.2.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0ss4b3mv9ia97cj2m1486pw8zc0mwd3lzr2jn6ajv0ixjxss95fa")))

(define-public crate-elfbin-0.3.0 (c (n "elfbin") (v "0.3.0") (d (list (d (n "binbin") (r "^0.2.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1l5vhafl5g1wxfcq734306k9cb2jxgr35j1a1yp96jv0pfhh5j1w")))

(define-public crate-elfbin-0.4.0 (c (n "elfbin") (v "0.4.0") (d (list (d (n "binbin") (r "^0.2.0") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0h7nw8610132k96jnadqkddbsbkgp8rpf0sc8kmidyb18cz0gi2d")))

