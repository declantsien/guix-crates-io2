(define-module (crates-io el tl eltl) #:use-module (crates-io))

(define-public crate-eltl-0.1.0 (c (n "eltl") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08dd9nvwv9cwi4dfkppk983clc0h6qzm8bnp3y6xsryazg13kjw6")))

(define-public crate-eltl-0.2.0 (c (n "eltl") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "17rj0lrmhl19nfcrg2602j9s6dxch7c0g0yr86xa4405prrsarqp")))

(define-public crate-eltl-0.2.1 (c (n "eltl") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1yxp2si4hs63zmvy0appygif5k0xhml9xna260dqv2pfyji5bx9f")))

(define-public crate-eltl-0.3.0 (c (n "eltl") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0phldbvkfpwfrgyyg6wg4417ylcvz99kww4s0qvahjfiy9wgf0n9")))

(define-public crate-eltl-0.4.0 (c (n "eltl") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "07j7cm6j6l9ix43s9fdz6wcgs5d71qdja6n9iqbyjqlbjlqc8pa6")))

(define-public crate-eltl-0.4.1 (c (n "eltl") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "09h362g0k03y5analzfl50fadzb4n9500yl9y5rx5f351pi542qw")))

(define-public crate-eltl-0.5.0 (c (n "eltl") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0bm79jmlvw4h7m09ynf416kyvq9955gim1dw9147hd4shfvb9vlc")))

(define-public crate-eltl-0.6.0 (c (n "eltl") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1vsqirkdw6j6awfdb68mzajj1za5ad0jqzxh7zvlyk5m59mzahzx")))

(define-public crate-eltl-0.7.0 (c (n "eltl") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "18v67kdwnzcvyvq1q2h6r2a02bhzf0iig8h69nrfylwj4fzdcr9j")))

