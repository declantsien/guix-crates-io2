(define-module (crates-io el md elmdoc) #:use-module (crates-io))

(define-public crate-elmdoc-0.1.1 (c (n "elmdoc") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "13w3syn9l70ql0ij32pxj44l5wfhjm5rx1pcz3yayagy3ir0f614")))

(define-public crate-elmdoc-0.1.2 (c (n "elmdoc") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "025kh4wvnnami8yzsr8mj79ncc4ismahl69rxfs3dfxmp3qn3cf4")))

(define-public crate-elmdoc-0.1.3 (c (n "elmdoc") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "1klbndy7zvpfch5pvc19446xp90fzyah2s7rx3h51yhwyb6xzan2")))

