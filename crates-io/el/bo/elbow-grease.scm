(define-module (crates-io el bo elbow-grease) #:use-module (crates-io))

(define-public crate-elbow-grease-0.0.1 (c (n "elbow-grease") (v "0.0.1") (h "0sqwypbdjjal4qv48wsrlwa2sb3y9p9dmxc3i5cqjlwvm7n3v0ll") (y #t)))

(define-public crate-elbow-grease-0.0.2 (c (n "elbow-grease") (v "0.0.2") (h "1qrwbg9yssk6n33r7yd0k3ygfldmm8xl4w9gmdc791sdydcbiyi8") (y #t)))

