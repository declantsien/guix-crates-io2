(define-module (crates-io el em elements-frequency) #:use-module (crates-io))

(define-public crate-elements-frequency-0.1.0 (c (n "elements-frequency") (v "0.1.0") (h "1wydpr8niszd8066wijk3bagxjf31v8zdvi6i88857i2dsc2j41n") (y #t)))

(define-public crate-elements-frequency-0.1.1 (c (n "elements-frequency") (v "0.1.1") (h "0s3qm4nna6m8107f7g2vpz51rff0znmq52cx02s5hsv9s831wh0w") (y #t)))

(define-public crate-elements-frequency-0.1.2 (c (n "elements-frequency") (v "0.1.2") (h "11z0hkhpaq667wwwrw4m4df7lvqngfc55c3q5iwhmhl7qmc582sk") (y #t)))

(define-public crate-elements-frequency-0.1.3 (c (n "elements-frequency") (v "0.1.3") (h "1p2ic2h73lif9n224rzr6kcppysan90x3j8k1vxkf2shmsl8ycxs") (y #t)))

(define-public crate-elements-frequency-0.1.4 (c (n "elements-frequency") (v "0.1.4") (h "16yjxxnpkn02wzz239v2kxrpi6gd06wpbly532lanx4bqv9rdvr7") (y #t)))

(define-public crate-elements-frequency-0.1.5 (c (n "elements-frequency") (v "0.1.5") (h "03ja70x4mx5z63h8kcd9yk23kkn82vjf7ms1g76n0sjbn649969s") (y #t)))

(define-public crate-elements-frequency-0.2.0 (c (n "elements-frequency") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0lyh6lb13bwxbm9gnvg1bmsvj1flpk64kvzgpp42pk9cvi6jalcf") (y #t)))

(define-public crate-elements-frequency-0.2.1 (c (n "elements-frequency") (v "0.2.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1qx84rafmqyj50ppd73pay8wsg2l71jpwi1p4hz4rxcnc6rfkfqx") (y #t)))

(define-public crate-elements-frequency-0.3.0 (c (n "elements-frequency") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0fppkln5r3a4n7v3q2b4l1ay43xmg96mc9qzvj63l8qdb5rp3cfs") (y #t)))

(define-public crate-elements-frequency-0.4.0 (c (n "elements-frequency") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon-core") (r "^1.9.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "1zxz4p26w0ckwlj216v6hrqq9qid5af31n4q2k9w179rw36s0vkr")))

(define-public crate-elements-frequency-0.5.0 (c (n "elements-frequency") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon-core") (r "^1.9.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "1cgvzh1q68ckzvq0lgjfyaqlh53qrm5x20iw86s2yslz176jipas")))

(define-public crate-elements-frequency-0.5.1 (c (n "elements-frequency") (v "0.5.1") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon-core") (r "^1.9.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "1hrg3v5j0pn76k0j08flmlxzxaj6ijg7jla4ix6xlq062f6pp0m4")))

