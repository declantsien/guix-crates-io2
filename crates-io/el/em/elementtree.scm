(define-module (crates-io el em elementtree) #:use-module (crates-io))

(define-public crate-elementtree-0.1.0 (c (n "elementtree") (v "0.1.0") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "0m0c5v383swy2fd6zmqdc7gakrj8imb5yb17c63l0zvrg45fag3n")))

(define-public crate-elementtree-0.1.1 (c (n "elementtree") (v "0.1.1") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "0hq6v82iq0qkcl7k1sik2cqr5cx7rglxc8i4qplk3g9sz11xpn78")))

(define-public crate-elementtree-0.2.0 (c (n "elementtree") (v "0.2.0") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "1q0k7mvk13y653kfvg74gfsj425d107czjll6wgmrmnb1shr7i5y")))

(define-public crate-elementtree-0.3.0 (c (n "elementtree") (v "0.3.0") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "075ksvykgmxqv4w4yhk99lk6a0wd2qvv8qpwip76kdvkg7axdfvx")))

(define-public crate-elementtree-0.4.0 (c (n "elementtree") (v "0.4.0") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "1aqkyxa2zxxbjyr9iqn4sma2y47m2i5f8jza6hz6qja67swsjga7")))

(define-public crate-elementtree-0.5.0 (c (n "elementtree") (v "0.5.0") (d (list (d (n "string_cache") (r "^0") (d #t) (k 0)) (d (n "xml-rs") (r "^0") (d #t) (k 0)))) (h "09cs2hd3g0py63d0fysb0j8mj40wj3plf825sz938dxq18nx7i8r")))

(define-public crate-elementtree-0.6.0 (c (n "elementtree") (v "0.6.0") (d (list (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0v5c4ry7mp5kljw9hvqk1k02jik9k41avnzzsnhk18ln2yrczd2v")))

(define-public crate-elementtree-0.7.0 (c (n "elementtree") (v "0.7.0") (d (list (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1qvzmbp8wijk898lxfq3gzr181ngpiw3jly8c1ffkw9w8g4ijqsz")))

(define-public crate-elementtree-1.1.0 (c (n "elementtree") (v "1.1.0") (d (list (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1mx80xx4vrplidg2fa83diix9xi4sp6lm26lw8hl7wxdarpqbkiy") (r "1.49.0")))

(define-public crate-elementtree-1.2.0 (c (n "elementtree") (v "1.2.0") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)))) (h "0pr396xz0vf6z1q6y9gp27j555xgl90yzhp3vyjdknvcd731ksdz") (r "1.49.0")))

(define-public crate-elementtree-1.2.1 (c (n "elementtree") (v "1.2.1") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)))) (h "1hbhzhxp9a4w0v4wp8383j26xl26bdgsxy44v6g2v59ph8vhg243") (r "1.49.0")))

(define-public crate-elementtree-1.2.2 (c (n "elementtree") (v "1.2.2") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)))) (h "0cg0spvbgq2s87pbl6a6b8c0fwrmvajfspv0h9zzrr14460izsc3") (r "1.49.0")))

(define-public crate-elementtree-1.2.3 (c (n "elementtree") (v "1.2.3") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 2)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)))) (h "1csw13s33grnmkqvnlp4hfvxcd6pnkqavq2ncj572n7lmi14gz9y") (r "1.56.0")))

