(define-module (crates-io el em elementwise) #:use-module (crates-io))

(define-public crate-elementwise-0.1.0 (c (n "elementwise") (v "0.1.0") (h "03cbyva1ixnbv3ivf9va3jy4f39ikjaxiqyc2a1ydcaggbkvq2x8")))

(define-public crate-elementwise-0.2.0 (c (n "elementwise") (v "0.2.0") (h "15z06vgynswm1jvmdfb46hpwzzawi3yqz1xvw0s314g84n00rkb6") (f (quote (("logical") ("default" "arithmetic") ("arithmetic"))))))

(define-public crate-elementwise-0.3.0 (c (n "elementwise") (v "0.3.0") (h "0xdhdd0f91hg0m0p9siagxvhmphdj4h0wma66acx1lxv2nnd9z0d") (f (quote (("logical") ("default" "arithmetic") ("arithmetic"))))))

(define-public crate-elementwise-0.3.1 (c (n "elementwise") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0ixxf16zwi1mfpf814c78lhk0viglf56l5xrfzl607p0bfz5ijk9") (f (quote (("logical") ("default" "arithmetic" "ndarray") ("arithmetic"))))))

(define-public crate-elementwise-0.3.2 (c (n "elementwise") (v "0.3.2") (d (list (d (n "ndarray") (r "^0.12") (o #t) (d #t) (k 0)))) (h "00pxx5wghmq5nik29022vq9r6kmxb4bzzlzvxmirzpxs2n8hnbbw") (f (quote (("logical") ("default" "arithmetic") ("arithmetic"))))))

