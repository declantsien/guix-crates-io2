(define-module (crates-io el em elements-rpc) #:use-module (crates-io))

(define-public crate-elements-rpc-0.1.0 (c (n "elements-rpc") (v "0.1.0") (d (list (d (n "elements") (r "^0.21.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonrpc_client") (r "^0.7.1") (f (quote ("reqwest"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "testcontainers") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "09mkkpcnyriqnwrxmdzz8c2408vbhjmg5vvzrrisjzmlk08hra75")))

