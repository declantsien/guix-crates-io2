(define-module (crates-io el em element-ptr-macro) #:use-module (crates-io))

(define-public crate-element-ptr-macro-0.0.1 (c (n "element-ptr-macro") (v "0.0.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1jhp0bx36584504k2ra3a6czpbq1aqyvffdb8rrlqa9wdlm4hcwh") (f (quote (("quote_into_hack"))))))

(define-public crate-element-ptr-macro-0.0.2 (c (n "element-ptr-macro") (v "0.0.2") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16vy1h0v8k6ybd6slc499c6gsl4zyicz02yy7npkgbdwng8airar") (f (quote (("quote_into_hack"))))))

