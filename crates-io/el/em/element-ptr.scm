(define-module (crates-io el em element-ptr) #:use-module (crates-io))

(define-public crate-element-ptr-0.0.1 (c (n "element-ptr") (v "0.0.1") (d (list (d (n "element-ptr-macro") (r "^0.0.1") (d #t) (k 0)))) (h "0rvr71440zx8ynpy5p5hc24y8plkdrci2z5k2j6plk7fam3xpbhs")))

(define-public crate-element-ptr-0.0.2 (c (n "element-ptr") (v "0.0.2") (d (list (d (n "element-ptr-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0cbchh8pcmvqmmmc1wjcqc5w44853769z9wk95pb231flywnwnci")))

