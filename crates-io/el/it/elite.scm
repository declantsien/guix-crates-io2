(define-module (crates-io el it elite) #:use-module (crates-io))

(define-public crate-elite-0.1.0 (c (n "elite") (v "0.1.0") (h "0zb93brjbv559yb0bhrcyfhpbzjimkibf2bjzj6r3wnlxflq3klm")))

(define-public crate-elite-0.1.1 (c (n "elite") (v "0.1.1") (h "045xfpxji6j3knq73hznxdqc952whaq6qdsl7nyfh3s45hl8x9zq")))

(define-public crate-elite-0.1.2 (c (n "elite") (v "0.1.2") (h "13h6p8gkkcq7hv6jii28zfim18pz0z48g8viymfpdgs61mqnia42")))

(define-public crate-elite-0.1.3 (c (n "elite") (v "0.1.3") (h "1vf2dv3v4bfv5mz7hrxm858w6xjk4287y07q7qcxzzhq5gxiwgsf")))

(define-public crate-elite-0.1.4 (c (n "elite") (v "0.1.4") (h "016v61sm535fi6lnwnq88bgbraipiazzyhaxzr5i06jvkwyqp0mz")))

