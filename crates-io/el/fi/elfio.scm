(define-module (crates-io el fi elfio) #:use-module (crates-io))

(define-public crate-elfio-0.3.1 (c (n "elfio") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1a585hgw962ppfjsxj9as9byqqnc0wwivf5r92q86d6kn5j935z6")))

(define-public crate-elfio-0.3.2 (c (n "elfio") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0wyqm9z6xiigmy63cxbywwss76xdpki39pxdj2bh13kachqq4vl8")))

(define-public crate-elfio-0.3.3 (c (n "elfio") (v "0.3.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1qq405xr6wf98kf6q4i01b547s7w6m10qnxmn8zy7vgp7w7r547j")))

