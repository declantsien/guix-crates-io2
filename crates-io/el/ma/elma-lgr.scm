(define-module (crates-io el ma elma-lgr) #:use-module (crates-io))

(define-public crate-elma-lgr-0.1.0 (c (n "elma-lgr") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1sbifq9di85n18nb5xkrpf0hsv76k26mxzsi99jhkaxvf47y4svj")))

(define-public crate-elma-lgr-0.2.0 (c (n "elma-lgr") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "pcx") (r "^0.1.2") (d #t) (k 0)))) (h "0n3j250dsb6lp876yzhld4wmxkjkj746fhvhaylgy22r4pvv3y8c")))

