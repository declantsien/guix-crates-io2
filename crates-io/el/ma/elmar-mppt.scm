(define-module (crates-io el ma elmar-mppt) #:use-module (crates-io))

(define-public crate-elmar-mppt-0.1.0 (c (n "elmar-mppt") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "116bzq35sbg1izqm5phwfm7zkvbdgmmznzzprif210da7818h5sk")))

(define-public crate-elmar-mppt-0.1.1 (c (n "elmar-mppt") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bxcan") (r "^0.6.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (d #t) (k 0)))) (h "1m4riwm2gqnr9jhs7x1pwl0bckf12n8jkzwmj12whpymrkmwy40p")))

