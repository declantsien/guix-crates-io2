(define-module (crates-io el ma elma) #:use-module (crates-io))

(define-public crate-elma-0.1.0 (c (n "elma") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0nwp8r2sf0wzffw2hw7kkxr2lzwmb00ff6nnpjp8kv2z01pycvqz")))

(define-public crate-elma-0.1.1 (c (n "elma") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0izd492aas5jd4g8fdd3wp7fbdw7p54g45cc3l5465sdrqqmh4nf")))

(define-public crate-elma-0.1.2 (c (n "elma") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0y94dhn654x2wnl7rwd4kg8rqs43ygaqp6977w6nk01mnbm58dfw")))

(define-public crate-elma-0.1.3 (c (n "elma") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "077gbdskn0bz1j9xdnqy88wbpydj4r2pmkd4wqhz8fys5gxx6qgs")))

(define-public crate-elma-0.1.4 (c (n "elma") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "134a2kxgakjwh87yjk5iskjkw8isx12l33kgv8aq3kchsfvg4pxx")))

(define-public crate-elma-0.1.5 (c (n "elma") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0l57nbcfyiaj1h292iri8fd7f357w2wz3gk1vnl0g23jq4q2gmdg")))

(define-public crate-elma-0.1.6 (c (n "elma") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0pvp1s6g6by1q12jn0d3rw1vqviwpmrl3xj0myyxqm8p8jzb17jk")))

(define-public crate-elma-0.1.7 (c (n "elma") (v "0.1.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "19ib5zbsikbqrxan4wxx28z0sj768iw9j0hcflcyfhmwi6qrq9v2")))

(define-public crate-elma-0.1.8 (c (n "elma") (v "0.1.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1w2bsxbdn4a0i6jrqjf3rlqymkmph7hy7b9g4f95m9096ik952k3")))

(define-public crate-elma-0.1.9 (c (n "elma") (v "0.1.9") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0iz2gd23n6ykwrpgkjmwv2hkvx17wzhppvhg1brjqjipjkxamknk")))

(define-public crate-elma-0.1.10 (c (n "elma") (v "0.1.10") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0csr3wh5xyn1mmxzrs19z9z12ianvnzk7a5siwv4918amv30wmzl")))

(define-public crate-elma-0.1.11 (c (n "elma") (v "0.1.11") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0fkyb4prgklywc7ib3fwh2c3lrpzc03wr8rf403azijrxrlp0yyv")))

(define-public crate-elma-0.1.12 (c (n "elma") (v "0.1.12") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0cciqfqx6hvxi547d0xg49nhnb56sb5j4xrzrk9br7fkc2mip3bf")))

(define-public crate-elma-0.1.13 (c (n "elma") (v "0.1.13") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0lwn8zqbsnhjfks1qwn9v2d2n2qisx8c6lhfhd8y3y729qqlv2j2")))

(define-public crate-elma-0.1.14 (c (n "elma") (v "0.1.14") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1d1akkw87dshj7p7yr5q7xy28l5farv4c6pkliygv268h6l058ji")))

