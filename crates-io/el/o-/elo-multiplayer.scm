(define-module (crates-io el o- elo-multiplayer) #:use-module (crates-io))

(define-public crate-elo-multiplayer-0.1.1 (c (n "elo-multiplayer") (v "0.1.1") (h "15ckr3fyhz0kdawq13ddq3z6gjqkj81gvwr23c6qy3r8vf4mcfq1")))

(define-public crate-elo-multiplayer-0.1.3 (c (n "elo-multiplayer") (v "0.1.3") (h "1kdaqcd6ni0vc4xmz08mljxdn56zc62lq3hd73m436g35j6c1gyn")))

(define-public crate-elo-multiplayer-0.1.4 (c (n "elo-multiplayer") (v "0.1.4") (h "11m0ivcywjrd5vnbkfkgcg3xlbahd0k47hrsn0pvhhaznxlps0ax")))

(define-public crate-elo-multiplayer-0.1.5 (c (n "elo-multiplayer") (v "0.1.5") (h "123w5z9djag1rnmgg187rc8asq19smfv27na9wa4bwya3xlh95qb")))

(define-public crate-elo-multiplayer-0.1.6 (c (n "elo-multiplayer") (v "0.1.6") (h "1z60hr1pqpprbkm4w89aj6pcr4rcmvykff76v7cnm2wz4nykkw1y")))

(define-public crate-elo-multiplayer-0.2.0 (c (n "elo-multiplayer") (v "0.2.0") (h "006mdkj953k6xhqpnad0hgd5vcn3z25k2yzhy4jrjg4sr6ppgdap")))

(define-public crate-elo-multiplayer-0.2.1 (c (n "elo-multiplayer") (v "0.2.1") (h "01kpvw3c5f62h6ikk7lznc6j19viwvp2a7hgnjrrjd3gb3bmb3x0")))

(define-public crate-elo-multiplayer-0.2.2 (c (n "elo-multiplayer") (v "0.2.2") (h "0mla8zhf63v3bkj4avfk79nsmij45zjr5hdq3n90k6y9hx2qqxh3")))

(define-public crate-elo-multiplayer-0.2.3 (c (n "elo-multiplayer") (v "0.2.3") (h "0gfmfihkvpz5cwhbj381fgq116h9ykmnhq8nmz60xbzvgdmzhl1w")))

(define-public crate-elo-multiplayer-0.2.4 (c (n "elo-multiplayer") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0mk4y24cwbgsp8hw0k5ygf2v9099ck3lk3j57y3c99k0ckmr25kz")))

