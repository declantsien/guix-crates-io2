(define-module (crates-io el ft elftools) #:use-module (crates-io))

(define-public crate-elftools-0.1.0 (c (n "elftools") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "1jlgwz962h9wph4j1zshnag7vgh19hmpibfz7zyfihbyrc4ya19s")))

