(define-module (crates-io el at elatec-twn4-simple) #:use-module (crates-io))

(define-public crate-elatec-twn4-simple-0.1.0 (c (n "elatec-twn4-simple") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0xcaqsyfass3ykqqprf8i1a2qxkzwc9sp2dc4cr006vlda05phl8")))

