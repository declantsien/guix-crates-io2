(define-module (crates-io el la ella-derive) #:use-module (crates-io))

(define-public crate-ella-derive-0.1.0 (c (n "ella-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1x3smjd9v430k5zvs1qhhkk52k3ljywvg1djw4kjwligs6j2mlav")))

(define-public crate-ella-derive-0.1.1 (c (n "ella-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1zyvhn8fa0rsnch4sskagaz4qpfnawzn62jkafk4s8jaw76b54xy")))

(define-public crate-ella-derive-0.1.2 (c (n "ella-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3gih6b2cl685hci0ahs2j0z0h48qhhszx15zag651bc9a7nx2h")))

(define-public crate-ella-derive-0.1.3 (c (n "ella-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0qkbwyrax6gdb7b8x6yjram2akqbqk2z487zigk7xmk4bjhca9vg")))

(define-public crate-ella-derive-0.1.4 (c (n "ella-derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "15ixd6cqshr8shdl1sf87j4xqjfca8svjj0mxcf204wx8dlm0q7k")))

(define-public crate-ella-derive-0.1.5 (c (n "ella-derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "12gx4i6wlbnqrl2bpbsplcv0vnj32k83fjamdbjhq6sq6iwcn27q")))

