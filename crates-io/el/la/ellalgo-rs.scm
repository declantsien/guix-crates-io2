(define-module (crates-io el la ellalgo-rs) #:use-module (crates-io))

(define-public crate-ellalgo-rs-0.1.0 (c (n "ellalgo-rs") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "05yk5939qnxwa5dmvh0165ii895m73r5dq3m47aahbd3kccjrch6")))

(define-public crate-ellalgo-rs-0.1.1 (c (n "ellalgo-rs") (v "0.1.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "svgbobdoc") (r "^0.3") (f (quote ("enable"))) (d #t) (k 0)))) (h "1syx8i9xa9zv4ycfdlvzxih8dn88wb8r061rl02np1d4casbp68f")))

