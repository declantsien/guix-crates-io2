(define-module (crates-io el vi elvis) #:use-module (crates-io))

(define-public crate-elvis-0.1.0 (c (n "elvis") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.35") (f (quote ("CssStyleDeclaration" "Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "1x4i1b2xha4z89f67c9a016b8badnqdq28b6rrd5s2r9d71sywa0")))

(define-public crate-elvis-0.2.0 (c (n "elvis") (v "0.2.0") (h "088js8bdixk09nsf44ibr25mdwdg68xdnf5qf0cyx4vvs3dbgnyy") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-0.2.1 (c (n "elvis") (v "0.2.1") (h "1106yv8dlkzdnrlzs5cadz4xfl9mkwq0vaf3974pypsf5jxk88gm") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-0.2.2 (c (n "elvis") (v "0.2.2") (h "1l0z4b39d9cbk3xn9l40j2jcsql4w56hlpqj9p9arbaa3z1vls4v") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-0.2.3 (c (n "elvis") (v "0.2.3") (d (list (d (n "elvis-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "elvis-shared") (r "^0.1.0") (d #t) (k 0)))) (h "06103jq6zabs27k9lj47k4xdx97bvc20fmvqlgcjb5bm4672gmkp") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-0.2.4 (c (n "elvis") (v "0.2.4") (d (list (d (n "elvis-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "elvis-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "elvis-web") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "14kbiipiiywcn8kk7vwmi5pzivgjldav5ir0mfd9n7gh7lxwwwgx") (f (quote (("web" "elvis-web" "elvis-shared/web") ("default" "web"))))))

(define-public crate-elvis-0.2.5 (c (n "elvis") (v "0.2.5") (d (list (d (n "elvis-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "elvis-shared") (r "^0.1.1") (d #t) (k 0)) (d (n "elvis-web") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "053sxary15sji3m3xq172dff6i3dd2iawgbav5jj25gr5342a4jg") (f (quote (("web" "elvis-web" "elvis-shared/web") ("default" "web"))))))

(define-public crate-elvis-0.2.6 (c (n "elvis") (v "0.2.6") (d (list (d (n "elvis-core") (r "^0.1.0") (d #t) (k 0)) (d (n "elvis-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "elvis-web") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "1x5asq6iqghihzr5a2mj61jx650zb1sh7i7xv2q2hzm14msjd1mq") (f (quote (("web" "elvis-web" "elvis-core/web") ("default" "web"))))))

(define-public crate-elvis-0.3.0 (c (n "elvis") (v "0.3.0") (d (list (d (n "elvis-core") (r "^0.1.1") (d #t) (k 0)) (d (n "elvis-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "elvis-web") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1mg4xbk2cyxynqbk5b2m10qvrh8r4q9hwbczhxjrsm2hsymlgii7") (f (quote (("web" "elvis-web" "elvis-core/web") ("default" "web"))))))

(define-public crate-elvis-0.3.1 (c (n "elvis") (v "0.3.1") (d (list (d (n "elvis-core") (r "^0.1") (d #t) (k 0)) (d (n "elvis-derive") (r "^0.1") (d #t) (k 0)) (d (n "elvis-web") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fd7mp3b3nq0bv8z6n1hb8i3jpfrb4zyf23saxnq2h4wqms9x96g") (f (quote (("web" "elvis-web" "elvis-core/web") ("default" "web"))))))

