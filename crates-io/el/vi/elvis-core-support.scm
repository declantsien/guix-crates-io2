(define-module (crates-io el vi elvis-core-support) #:use-module (crates-io))

(define-public crate-elvis-core-support-0.1.0 (c (n "elvis-core-support") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "13i7yyxi6mlajlsbcadywflrs8nrvc3cgnra74jsn8hsfn5sigsd")))

