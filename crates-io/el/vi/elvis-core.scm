(define-module (crates-io el vi elvis-core) #:use-module (crates-io))

(define-public crate-elvis-core-0.1.0 (c (n "elvis-core") (v "0.1.0") (h "12zrvrdnz0m5phlcqcmybsc3g8bj33lmliah35j26y5i810p7dn4") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-core-0.1.1 (c (n "elvis-core") (v "0.1.1") (d (list (d (n "elvis-core-support") (r "^0.1.0") (d #t) (k 0)))) (h "07mr7sjp68mwr0m4cb0a3v02hi3y5y7fzzgcs3ysrig066379qxc") (f (quote (("web") ("default" "web"))))))

