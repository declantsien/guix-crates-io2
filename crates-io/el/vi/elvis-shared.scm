(define-module (crates-io el vi elvis-shared) #:use-module (crates-io))

(define-public crate-elvis-shared-0.1.0 (c (n "elvis-shared") (v "0.1.0") (h "16w0njm23n5sn10nh9igci4119gb9l7mdqsfj966sdm9y9dzp36p")))

(define-public crate-elvis-shared-0.1.1 (c (n "elvis-shared") (v "0.1.1") (h "0lmn55n5d6xz51g6414bjh3ql82r5y4yn871l6zd6ha7qjv3dahq") (f (quote (("web") ("default" "web"))))))

