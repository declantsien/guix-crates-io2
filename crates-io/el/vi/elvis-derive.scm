(define-module (crates-io el vi elvis-derive) #:use-module (crates-io))

(define-public crate-elvis-derive-0.1.0 (c (n "elvis-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0av34x6fsqsqfakpz2jwpjnc428sf6xw31vlc3b9xnyhqzvhfzsc") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-derive-0.1.1 (c (n "elvis-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "08cars16bspwd88pi67n78rx7fj7f4gsa7fc56bs07pnpp2w513r") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-derive-0.1.2 (c (n "elvis-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "15kgxc40p1hf1xflbxlr7bm1126nn4r2x5syglyj0a09gc27b443") (f (quote (("web") ("default" "web"))))))

(define-public crate-elvis-derive-0.1.3 (c (n "elvis-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "03rgvc4vrk5nwgn1z0jsrxypvsqx6ljxi9v11jfm0nml4pjma27j") (f (quote (("web") ("default" "web"))))))

