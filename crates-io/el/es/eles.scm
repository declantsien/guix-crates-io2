(define-module (crates-io el es eles) #:use-module (crates-io))

(define-public crate-eles-1.0.0 (c (n "eles") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 2)))) (h "1q5n3gizcawgphyr1qxxy4qgq8c49sfshcwhqiv2bc7wdm2mc67n")))

