(define-module (crates-io el ve elves) #:use-module (crates-io))

(define-public crate-elves-0.0.1 (c (n "elves") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "10l17wbvxp06l58kn3qxqi3r1igyl1xqnmdh6cgv453pg1p9b2gm")))

(define-public crate-elves-0.0.2 (c (n "elves") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1p3phizp6bls9x40fs0zb83ylsqnknhs56d88ixbfhlj9x1mv1fw")))

(define-public crate-elves-0.1.0 (c (n "elves") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1jnj00kmlh3gd2dhg1r05va0i5i7hj812cvrhkx07ldw5sjzh4l7")))

(define-public crate-elves-0.1.1 (c (n "elves") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1j3gjv1626agv1p8kiy34v19fpdh8630crnnrqb1pcnr4h8459hm")))

(define-public crate-elves-0.2.0 (c (n "elves") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0zh08zlbymz7yd9nsn0g5637dklah7lfzdrzbc105j337ig0cjj4")))

(define-public crate-elves-0.2.1 (c (n "elves") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0cig7sm74wb1kjksvx7z0f6pr8mybysnp5iyaa9ynivn5gpnhmjs")))

