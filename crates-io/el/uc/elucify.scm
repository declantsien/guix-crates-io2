(define-module (crates-io el uc elucify) #:use-module (crates-io))

(define-public crate-elucify-0.1.0 (c (n "elucify") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bqfxs050anzlr16m18dhxk8gh2lb2qh7pf1d4i2vhbib1c5vp1r")))

