(define-module (crates-io el f2 elf2iso) #:use-module (crates-io))

(define-public crate-elf2iso-0.1.0 (c (n "elf2iso") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "10nhmm7xap1m5zl38p6h24pqb64431p9pvpyhmdkvq9jazgpmzgw")))

(define-public crate-elf2iso-0.1.1 (c (n "elf2iso") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "07l4s0kzf1gv7zymqf3f4qick8xdsifh2fai8dlyrv5xcdag6fzp")))

