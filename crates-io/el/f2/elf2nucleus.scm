(define-module (crates-io el f2 elf2nucleus) #:use-module (crates-io))

(define-public crate-elf2nucleus-0.1.0 (c (n "elf2nucleus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.6") (d #t) (k 0)) (d (n "elf_rs") (r "^0.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1q3iq2vn55vvys028abgvfndmm7m3prybpq860vsn0hag0acswq1")))

