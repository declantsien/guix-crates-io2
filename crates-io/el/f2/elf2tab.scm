(define-module (crates-io el f2 elf2tab) #:use-module (crates-io))

(define-public crate-elf2tab-0.3.0 (c (n "elf2tab") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "1cg194xdn55flk6fjyrsn34lfkq1r5s9y21bpf14714kv938zr3f")))

(define-public crate-elf2tab-0.4.0 (c (n "elf2tab") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "1yd9w132jqawny3i5pb8sqkmy33gqlavp59awc7rb72s948j0a3h")))

(define-public crate-elf2tab-0.5.0 (c (n "elf2tab") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "0b66wpqdj2qv0zgkh3bpbg317f0jgyfx8ag7d07168lwlswla8x0")))

(define-public crate-elf2tab-0.6.0 (c (n "elf2tab") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "07c6wxad4m1kkwv74hs1qn86rf6zgjk39q1vbypgsnjq4pqmlkxa")))

(define-public crate-elf2tab-0.7.0 (c (n "elf2tab") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "0zs74y6l53g6rz0krra4cgljai7ym0jm97bqkz0y1x2z16dfgi60")))

(define-public crate-elf2tab-0.8.0 (c (n "elf2tab") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "04226kif4y9v1l3ws2b18i7lvxmmv73v4b4l6rygmpl6aqm0jjc6")))

(define-public crate-elf2tab-0.9.0 (c (n "elf2tab") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.15") (d #t) (k 0)))) (h "11i7yfwmcxigb0sa1hgxhm1bxkr8n11wfg71a5daxgjh21ad5wbk")))

(define-public crate-elf2tab-0.10.0 (c (n "elf2tab") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.36") (d #t) (k 0)))) (h "01g8y6d8y7k517vfl0868pbanisq3p36vhdfdj5jif7lw6f7x4sy")))

(define-public crate-elf2tab-0.10.1 (c (n "elf2tab") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.36") (d #t) (k 0)))) (h "0klg81jxzmmllnsc13cm498rmp66y9kq1g1n4piprazngbn2sfi2")))

(define-public crate-elf2tab-0.10.2 (c (n "elf2tab") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.36") (d #t) (k 0)))) (h "14wa9m4jij9jss07lzhms1h8z8wf7j9g62993x1y4g54cia6fsbv")))

(define-public crate-elf2tab-0.11.0 (c (n "elf2tab") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("clock" "std"))) (k 0)) (d (n "clap") (r "^4.0.7") (f (quote ("derive" "color" "wrap_help"))) (d #t) (k 0)) (d (n "elf") (r "^0.7.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "rsa-der") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.36") (d #t) (k 0)))) (h "05y7c0701j0hicygs1cjjb9h03q6zph11sbk0zqgd2gpkaz8f6hk")))

(define-public crate-elf2tab-0.12.0 (c (n "elf2tab") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock" "std"))) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive" "color" "wrap_help"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0.2") (d #t) (k 0)) (d (n "elf") (r "^0.7.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.39") (d #t) (k 0)))) (h "1zzl6p8fiprpkn1da0bf8ir96qfwcp0jia2x94rxrwaq74v905p6")))

