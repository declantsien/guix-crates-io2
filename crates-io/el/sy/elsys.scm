(define-module (crates-io el sy elsys) #:use-module (crates-io))

(define-public crate-elsys-0.1.0 (c (n "elsys") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)))) (h "08dk52b4388kn127jg7g552dv04pms01lk58kcc4rdmb3xb1s775")))

(define-public crate-elsys-0.1.1 (c (n "elsys") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)))) (h "1g23kjlvk5chmq6axv5npvp8a42fl41q4gpnabfhkk8f6r0vdv4k")))

