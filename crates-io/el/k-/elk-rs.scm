(define-module (crates-io el k- elk-rs) #:use-module (crates-io))

(define-public crate-elk-rs-0.1.0 (c (n "elk-rs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.32.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgxfl1ikgr568qi77gsrfbmjyhi7kjyl7cwmc9jq7h7h8njsfam")))

