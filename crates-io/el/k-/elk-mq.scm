(define-module (crates-io el k- elk-mq) #:use-module (crates-io))

(define-public crate-elk-mq-0.1.0 (c (n "elk-mq") (v "0.1.0") (d (list (d (n "cpython") (r "^0.7") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "redis") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nmkz6safgb1pm6xlqmcllf1j3pnrrd0n8iamvl7294xyr1cvlys") (f (quote (("python_bindings" "cpython")))) (r "1.65")))

(define-public crate-elk-mq-0.1.1 (c (n "elk-mq") (v "0.1.1") (d (list (d (n "cpython") (r "^0.7") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "redis") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1larc45d8cnfml8bqiyxy4icn2swwpcdawj09g0gcbwbc6yyiiii") (f (quote (("python_bindings" "cpython")))) (r "1.65")))

