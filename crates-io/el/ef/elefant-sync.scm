(define-module (crates-io el ef elefant-sync) #:use-module (crates-io))

(define-public crate-elefant-sync-0.0.1 (c (n "elefant-sync") (v "0.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "elefant-tools") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "fs"))) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1d9ds2dlnqrj9qj5kyaxprmjg65d62qycrlkqdmwiyshrmraxll1")))

