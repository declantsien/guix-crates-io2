(define-module (crates-io el ef elefont) #:use-module (crates-io))

(define-public crate-elefont-0.1.0 (c (n "elefont") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.7.1") (f (quote ("ahash"))) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "07dsjkdc36pn3hc4y5525qxrcwgfinpkyv0ra64ckw7zz1zy05kr") (f (quote (("std"))))))

(define-public crate-elefont-0.1.1 (c (n "elefont") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.7.1") (f (quote ("ahash"))) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "07jvy3r91x7iib4jdcy77njz9s666qw0rs0cykv6b1dbbjx5whzs") (f (quote (("std"))))))

(define-public crate-elefont-0.1.2 (c (n "elefont") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.7.1") (f (quote ("ahash"))) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "0kbdifmlfjbf24wn1f5r5k3mmc3nnrs2g0dr2d76jrjx3i6bnagy") (f (quote (("std"))))))

(define-public crate-elefont-0.1.3 (c (n "elefont") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.7.1") (f (quote ("ahash"))) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "17mf8jxqsplbfnp1xirzlb81k9g7vq0kx8jhb92hhxxcv96qd0d7") (f (quote (("std"))))))

