(define-module (crates-io el ia elias-fano) #:use-module (crates-io))

(define-public crate-elias-fano-0.1.0 (c (n "elias-fano") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "0dslsv2xkc8s2c77sxirkapnipgvhshvpk66zz0c61c2s9yvrr6m")))

(define-public crate-elias-fano-0.2.0 (c (n "elias-fano") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "03frja8vly8pps42s9pqk63iclbmwxx3jdshfxk28bcq5j3d14yr")))

(define-public crate-elias-fano-0.2.1 (c (n "elias-fano") (v "0.2.1") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "1p4fj7i35kxllyz5innf7qg9mnxhssf8f6177hnxrwmcgykv96rf")))

(define-public crate-elias-fano-0.2.2 (c (n "elias-fano") (v "0.2.2") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "1yh3mcmlg7pm7yryr81kl7wc3pir8x92kb03fbaw7j93s88dd86d")))

(define-public crate-elias-fano-0.2.3 (c (n "elias-fano") (v "0.2.3") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "08f4wq01kykcam3qn5yzz5dzavf2083dbch09g3h3j2yf6hfw01w")))

(define-public crate-elias-fano-0.2.4 (c (n "elias-fano") (v "0.2.4") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "0i282mp0mjx3xhrn92cbg1h2lpfz94b8zdvmq0jckk8gq25yy056")))

(define-public crate-elias-fano-0.2.5 (c (n "elias-fano") (v "0.2.5") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "1fgxpy2wmd85lpc2721iq8nxyhvcllriw9km69hdpnpxi0dap1sx")))

(define-public crate-elias-fano-0.2.6 (c (n "elias-fano") (v "0.2.6") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "09vflzmzh4lbnrfzz2840fdwywgrzqvzv3sy186img3fnwqzqj0z")))

(define-public crate-elias-fano-1.0.0 (c (n "elias-fano") (v "1.0.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "06r40id0afkixb4ipgvdf0lzkgx1ds2iwf6yra23ff5bhdbvy0zz")))

(define-public crate-elias-fano-1.0.1 (c (n "elias-fano") (v "1.0.1") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "0ikpw51b1kz6ijs8g1qyqssvb9bk46d82f8fk2ka2xwdkn19dald")))

(define-public crate-elias-fano-1.1.0 (c (n "elias-fano") (v "1.1.0") (d (list (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)))) (h "0ia0igd0zdrlq3z6wn7l9h8ifdnxnx5dd93qvc6l0r657qs0ykgx")))

