(define-module (crates-io el og eloggr) #:use-module (crates-io))

(define-public crate-eloggr-0.1.0 (c (n "eloggr") (v "0.1.0") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.4") (d #t) (k 0)) (d (n "unix-tty") (r "^0.2") (d #t) (k 0)))) (h "14fmflbv01j58w2sp6z2ysxvp9z3y34g381vg0f9blpi90nhkc8q")))

(define-public crate-eloggr-0.1.1 (c (n "eloggr") (v "0.1.1") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.6") (d #t) (k 0)) (d (n "unix-tty") (r "^0.3") (d #t) (k 0)))) (h "0swdaz9i35g5y779jma2zik1kwnk7rn3vhmcrh4hd7l6nwhl0b46")))

(define-public crate-eloggr-0.1.2 (c (n "eloggr") (v "0.1.2") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.6") (d #t) (k 0)) (d (n "unix-tty") (r "^0.3") (d #t) (k 0)))) (h "0j1kx7k8vp3b19wkf6dq57mzs9dijd33vayc64mhsxnbyzlb2n3d")))

