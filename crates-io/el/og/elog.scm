(define-module (crates-io el og elog) #:use-module (crates-io))

(define-public crate-elog-0.1.0 (c (n "elog") (v "0.1.0") (d (list (d (n "colored") (r "^1.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ifj4a0c365x07izhwdn4a786d92hkv3sm6xg98qp52p428phcw4")))

(define-public crate-elog-0.2.0 (c (n "elog") (v "0.2.0") (d (list (d (n "colored") (r "^1.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y43zccvpvqvjd9dybr6wimd0ca3mchygvhavc60nx4xbsl7dvyd")))

(define-public crate-elog-0.3.0 (c (n "elog") (v "0.3.0") (d (list (d (n "colored") (r "^1.3") (d #t) (k 0)) (d (n "thread-id") (r "^3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rj1dj0ydbplmi5ykn3jz5w0m3sk5jcczw0986qdjayn1jz7nva9")))

