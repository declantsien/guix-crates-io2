(define-module (crates-io el ka elkai-rs) #:use-module (crates-io))

(define-public crate-elkai-rs-0.1.0 (c (n "elkai-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 2)))) (h "0k9qk6ynf0k9k4abw3rfvfzsq1i6kc4xki2xdrpyrbbl5fljj5vi")))

(define-public crate-elkai-rs-0.1.1 (c (n "elkai-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 2)))) (h "11wjajfl8zgsyl3zz0yp2kbcwf3sa0g10vgayzwlddxpriq7sp7r")))

(define-public crate-elkai-rs-0.1.2 (c (n "elkai-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 2)))) (h "11j4dsz9fnlnkhdx3i73g0v5xv5rhi849rqbxv5km6qsx86afjw1")))

(define-public crate-elkai-rs-0.1.3 (c (n "elkai-rs") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 2)))) (h "1gs3d8xfhqvpljbdn7lq8l3ii6sxhcll4nq4033ybmxbk24x1rk3")))

