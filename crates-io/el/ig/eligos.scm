(define-module (crates-io el ig eligos) #:use-module (crates-io))

(define-public crate-eligos-0.1.0 (c (n "eligos") (v "0.1.0") (d (list (d (n "bytes") (r "^0.2.11") (d #t) (k 0)) (d (n "mio") (r "^0.4.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0wm434gfsj3j66j55xbqc6h4ilahnv6ja1c2r6hsa7pdz68dj1z5")))

