(define-module (crates-io el ar elara-math) #:use-module (crates-io))

(define-public crate-elara-math-0.1.0 (c (n "elara-math") (v "0.1.0") (d (list (d (n "elara_log") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0cnr205wa2n6wka1m74wjb74zddq96j3fjmccvg5p8xmpyzbacyc")))

(define-public crate-elara-math-0.1.1 (c (n "elara-math") (v "0.1.1") (d (list (d (n "elara_log") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jjdv99vkjp696dww4qp9ngxagmsm5hccql18aqmrbbv4h91ll1r")))

