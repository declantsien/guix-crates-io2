(define-module (crates-io el ev elevate-code) #:use-module (crates-io))

(define-public crate-elevate-code-0.1.0 (c (n "elevate-code") (v "0.1.0") (d (list (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "elevate-code-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_UI_Shell" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0yd49pix4v3nlhwm9jcpri9xd4vdchyshsl7r4xi4ni3ybyim1fl")))

