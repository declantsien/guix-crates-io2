(define-module (crates-io el ev elevator) #:use-module (crates-io))

(define-public crate-elevator-0.1.0 (c (n "elevator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "winuser" "minwindef"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0jkkrqch4hrnqrxn4122cfkjvcsj64mjpn0zsnkg00gvpx2q1xsc") (y #t)))

(define-public crate-elevator-0.1.1 (c (n "elevator") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "winuser" "minwindef"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0vhpsi8l1ijavv9iypzq6gqyl63bf776kg4il5mcah003j072rv4") (y #t)))

(define-public crate-elevator-1.0.1 (c (n "elevator") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shellapi" "winuser" "minwindef" "errhandlingapi" "winbase"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "02gpm5h4cvvw7l5rxkf3nbqjvzrns5psqximzm42a7b6qkpm3c77") (y #t)))

(define-public crate-elevator-2.0.0 (c (n "elevator") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.154") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_UI" "Win32_UI_Shell" "Win32_UI_WindowsAndMessaging" "Win32_System_Registry" "Win32_System_Com"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "1ibp1m6jdrzmr6hwny4saiwijzf1ggz3l4biz71drsd075cnn8hi")))

