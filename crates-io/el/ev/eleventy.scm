(define-module (crates-io el ev eleventy) #:use-module (crates-io))

(define-public crate-eleventy-0.1.0 (c (n "eleventy") (v "0.1.0") (h "1s2jz6f7jhzvyy1idgrkndin0ylnk3kp1sx9974mb9183vchl3gl")))

(define-public crate-eleventy-0.2.0 (c (n "eleventy") (v "0.2.0") (h "05dgcv4344b2sqwvcnw95kghrb7l2z7gvc95n5q9hdjw0sl755jf")))

