(define-module (crates-io el ev elevate) #:use-module (crates-io))

(define-public crate-elevate-0.6.1 (c (n "elevate") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "simple_logger") (r "^1.8") (d #t) (k 2)))) (h "1i27yp7ip16jzr9i1hdnirvrmq6hfirdq23l7ay3lg5l3v2y5fg8")))

