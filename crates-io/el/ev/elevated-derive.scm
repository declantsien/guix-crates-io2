(define-module (crates-io el ev elevated-derive) #:use-module (crates-io))

(define-public crate-elevated-derive-0.1.0 (c (n "elevated-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1m7ajkacvdyaz54qy574hj0a4fz2pxfxa8rycqlqlbx7lfdm0kxv")))

(define-public crate-elevated-derive-0.1.1 (c (n "elevated-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq7mhacw2j3fvr2jgpx08dw7kx63bgq81w8xgwwm4mig3crpv6k")))

(define-public crate-elevated-derive-0.1.2 (c (n "elevated-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8j46wk9fwmkmqmqfhjyancj0f95y7mfwiv1riksdjzrah2aqqb")))

(define-public crate-elevated-derive-0.1.3 (c (n "elevated-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1cz8k8y9c69c4n2896plsb7kfwrf84wcx7jyph056586j4751r4w")))

