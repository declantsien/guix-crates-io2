(define-module (crates-io el ev elevation) #:use-module (crates-io))

(define-public crate-elevation-0.1.0 (c (n "elevation") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15rmjm84ffaqnqvv0jj20l3bdbx20d322fif4sjcd7rcslqzkvpd") (y #t)))

