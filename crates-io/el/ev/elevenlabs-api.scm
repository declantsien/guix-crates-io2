(define-module (crates-io el ev elevenlabs-api) #:use-module (crates-io))

(define-public crate-elevenlabs-api-0.1.0 (c (n "elevenlabs-api") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)))) (h "10jrb884zqkw1sf5gp5sya6sm51ni4143r88vfgp9dvmd29l2axd")))

(define-public crate-elevenlabs-api-0.1.1 (c (n "elevenlabs-api") (v "0.1.1") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)))) (h "08sbpcsav70cpw87805f0yzn450ph3mxcyq92rs6dj4akikzk9xg")))

