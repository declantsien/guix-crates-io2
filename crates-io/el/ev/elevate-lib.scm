(define-module (crates-io el ev elevate-lib) #:use-module (crates-io))

(define-public crate-elevate-lib-0.1.0 (c (n "elevate-lib") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "03s8fq7ffb7hhfb1yzzfzcw78jnv3vwzdlbk47w4rkkrfdpr5df6") (y #t)))

(define-public crate-elevate-lib-0.1.1 (c (n "elevate-lib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0wk6rnal5r7cgjxwpgdh5p4fdicg6l7sk9skwxb0nzxh8fbmdkv0") (y #t)))

(define-public crate-elevate-lib-0.1.2 (c (n "elevate-lib") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0dzb74cgsp0qwa4sfc1ngxb3vv62mgvq3dr2fl408gxjpapr9mb1") (y #t)))

(define-public crate-elevate-lib-0.1.3 (c (n "elevate-lib") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0hz0r7f7siibwn0m410fk82ppchmpah01yc86jshb9kl215a3khx") (y #t)))

(define-public crate-elevate-lib-0.1.4 (c (n "elevate-lib") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "16d6b6cgahfzzxm2i0pfxra7izrpk6kr0g7vphhsrspmpbf37qa2") (y #t)))

(define-public crate-elevate-lib-0.1.5 (c (n "elevate-lib") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0f1xqlvc54nf7ws02shh5af5m1iz0bpqgndvzprbdf6pzhi7yr4b") (y #t)))

(define-public crate-elevate-lib-0.1.6 (c (n "elevate-lib") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "00kbkgx5disxx1kqr1722ym0agypnfmhamvfscnwyr0jfng9lz2y") (y #t)))

(define-public crate-elevate-lib-0.1.7 (c (n "elevate-lib") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "14pqkzcaksphmj0c8rgihlap4nij9h6gw20sy9mdh95fkpj05sb3") (y #t)))

(define-public crate-elevate-lib-0.1.8 (c (n "elevate-lib") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "102pkfa3d4vhksb8vwnhj2yx12k6wznnw9d6g706wl8bcm1hg7rj") (y #t)))

(define-public crate-elevate-lib-0.1.9 (c (n "elevate-lib") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "1fapn73a8anqyx7xf739awlp91fvgr1z8764flrmy1nai96cjd0y") (y #t)))

(define-public crate-elevate-lib-0.1.0-202403171309+1.0.0-alpha.1 (c (n "elevate-lib") (v "0.1.0-202403171309+1.0.0-alpha.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "12xk8zhw8kkjkvsk3wx6yyjvdvnq0ca5l0hrw44hzfhxc2in36qq")))

(define-public crate-elevate-lib-0.1.0-202403171437+1.0.0-alpha.1 (c (n "elevate-lib") (v "0.1.0-202403171437+1.0.0-alpha.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0d5sbr4lg5imb4079bnkjzyy3zdaifh2nbkljj8mmgy58277384g")))

(define-public crate-elevate-lib-0.1.0-202403171450+1.0.0-alpha.1 (c (n "elevate-lib") (v "0.1.0-202403171450+1.0.0-alpha.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0ihxi7097gwyk04b0yf3z0hsdlfncvhiykk5bpkbshq8kp0v34g0")))

(define-public crate-elevate-lib-0.1.0-202403171545+1.0.0-alpha.1 (c (n "elevate-lib") (v "0.1.0-202403171545+1.0.0-alpha.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "017gxnbmxfcgpckk18hlr58ckxwvdrw1cwz0rsr81bzfyvrpm8zp")))

