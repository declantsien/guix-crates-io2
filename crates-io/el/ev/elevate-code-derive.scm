(define-module (crates-io el ev elevate-code-derive) #:use-module (crates-io))

(define-public crate-elevate-code-derive-0.1.0 (c (n "elevate-code-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgbhfk7q6m94qj8myn1pdqaqb2r81675439a9vrphvc5avqng02")))

