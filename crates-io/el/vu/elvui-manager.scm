(define-module (crates-io el vu elvui-manager) #:use-module (crates-io))

(define-public crate-elvui-manager-0.1.0 (c (n "elvui-manager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "039prv58drp23nbwpccz1y02v35g622icv4xmvfhfajrcamki69g")))

