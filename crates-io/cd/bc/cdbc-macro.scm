(define-module (crates-io cd bc cdbc-macro) #:use-module (crates-io))

(define-public crate-cdbc-macro-0.1.0 (c (n "cdbc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15v3a48lki99zb24lm5dj87gnfhsli26kmafnbp2kmy6rgz0pq3g") (f (quote (("default") ("debug_mode"))))))

(define-public crate-cdbc-macro-0.1.1 (c (n "cdbc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vbj3khybkpjndzhyrvsvk30qpysmvkfgv94xly5chrcl7bxyy5v") (f (quote (("default") ("debug_mode"))))))

(define-public crate-cdbc-macro-0.1.2 (c (n "cdbc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "123a5ix60791lgcyw2skh8m2w7km2bp45xk83sb95b0sidvawzsm") (f (quote (("default") ("debug_mode"))))))

(define-public crate-cdbc-macro-0.1.3 (c (n "cdbc-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00pkdsfmbq1mpnj9pq4dlv6agcjbc9ivh2q5siakcqpsyg1fwi2y") (f (quote (("default") ("debug_mode"))))))

(define-public crate-cdbc-macro-0.1.22 (c (n "cdbc-macro") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z51rrdnd1k34aiyzyh7j7ws89kk0jvdkgipch5jpf64d07pyv6n") (f (quote (("default") ("debug_mode"))))))

(define-public crate-cdbc-macro-0.1.23 (c (n "cdbc-macro") (v "0.1.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "033bv9ch2nk7v3cr95fws0113sh4hiccp5hmm4qsj40fyx8lnqh6") (f (quote (("default") ("debug_mode"))))))

