(define-module (crates-io cd ef cdefmt-parser) #:use-module (crates-io))

(define-public crate-cdefmt-parser-0.1.0 (c (n "cdefmt-parser") (v "0.1.0") (d (list (d (n "gimli") (r "^0.28") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1") (d #t) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r0aqcv3h9q9xkxq5zcry5w4j6b627ynv1pws7kc6bp2087v0khk")))

