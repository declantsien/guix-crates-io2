(define-module (crates-io cd ef cdefines) #:use-module (crates-io))

(define-public crate-cdefines-0.1.0 (c (n "cdefines") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0lyw366mxhmkv4nbq4bd4xsr2q3f84msgl3iv24d4kxg3jf6c03i")))

(define-public crate-cdefines-0.1.1 (c (n "cdefines") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0l05c7shyq3lrv2n87sr4sa8s6kyymf9w3fzm4glaqzc6k4jvhgn")))

(define-public crate-cdefines-0.1.2 (c (n "cdefines") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "132rfj0zpj6kwxlrv464d7zzm34yk2j30z0921vns9wnd2sbxgfh")))

(define-public crate-cdefines-0.1.3 (c (n "cdefines") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "1yhzl74iq6jlpk1zpzv6g3d79lwrnpjijil8mqqfgxhfzfjyab6p")))

(define-public crate-cdefines-0.1.4 (c (n "cdefines") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0pd2a9qbl0ln9728yrnajnjvk1f5m0ck45d04b27lvv45w09479l")))

