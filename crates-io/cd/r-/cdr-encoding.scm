(define-module (crates-io cd r- cdr-encoding) #:use-module (crates-io))

(define-public crate-cdr-encoding-0.10.0 (c (n "cdr-encoding") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.3") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1bmcwnrsn9lrjvhpmzll4p020q1vjlb23an8naqxmhws4akcp1nk")))

(define-public crate-cdr-encoding-0.10.1 (c (n "cdr-encoding") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.3") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0n9b4rfnw9b3qvgn2pzn10l2jmaqwxax4mdaf9fc9rfd3pz32c4h")))

