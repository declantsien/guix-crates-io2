(define-module (crates-io cd r- cdr-encoding-size-derive) #:use-module (crates-io))

(define-public crate-cdr-encoding-size-derive-0.5.0 (c (n "cdr-encoding-size-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12dzfc6dcmjzfbins6231x8qmx7wrjnxsf50zp2qp767b04pyg2y")))

(define-public crate-cdr-encoding-size-derive-0.5.1 (c (n "cdr-encoding-size-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dlz3rfqgb6f2xgfv907j77w6c7zq0ylnjyf0a54i9j4diwm6sf4")))

