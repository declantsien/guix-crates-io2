(define-module (crates-io cd r- cdr-encoding-size) #:use-module (crates-io))

(define-public crate-cdr-encoding-size-0.5.0 (c (n "cdr-encoding-size") (v "0.5.0") (d (list (d (n "cdr-encoding-size-derive") (r "~0.5") (d #t) (k 0)))) (h "1sgrf35i7a30baw86v7y9250dgb2rip51vzkbbx226ah6ry45132")))

(define-public crate-cdr-encoding-size-0.5.1 (c (n "cdr-encoding-size") (v "0.5.1") (d (list (d (n "cdr-encoding-size-derive") (r "^0.5") (d #t) (k 0)))) (h "1yzjnw2cly2wmf45n3xsggrzh7bcd27z8ws7czc192ap4x0s76xc")))

