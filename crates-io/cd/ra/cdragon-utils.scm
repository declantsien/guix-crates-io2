(define-module (crates-io cd ra cdragon-utils) #:use-module (crates-io))

(define-public crate-cdragon-utils-0.1.0 (c (n "cdragon-utils") (v "0.1.0") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1qwycb95pfndybg25k24w12y1i2qqzf4h2yq1hj4n54s1xf5jrjp") (f (quote (("guarded_file") ("default")))) (s 2) (e (quote (("parsing" "dep:nom" "dep:thiserror"))))))

(define-public crate-cdragon-utils-0.2.0 (c (n "cdragon-utils") (v "0.2.0") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "05z0mw89rs1k23k2k2shl7xw886iks40hljhpjbj82kpjaribgvx") (f (quote (("guarded_file") ("default")))) (s 2) (e (quote (("parsing" "dep:nom" "dep:thiserror"))))))

