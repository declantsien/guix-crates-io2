(define-module (crates-io cd ra cdragon-rman) #:use-module (crates-io))

(define-public crate-cdragon-rman-0.1.0 (c (n "cdragon-rman") (v "0.1.0") (d (list (d (n "cdragon-utils") (r "^0.1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 0)))) (h "1hl7q9s0y4b0nmwcszmlmsfx9wx7msms3vx3qf1qirw70j7c2nzg")))

(define-public crate-cdragon-rman-0.2.0 (c (n "cdragon-rman") (v "0.2.0") (d (list (d (n "cdragon-utils") (r "^0.2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.13") (d #t) (k 0)))) (h "1v7xgbsy4134sm3fpnw8bh6k2nasv52bhbc4wblw2rpmpzybfi33")))

