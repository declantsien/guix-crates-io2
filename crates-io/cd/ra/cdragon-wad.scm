(define-module (crates-io cd ra cdragon-wad) #:use-module (crates-io))

(define-public crate-cdragon-wad-0.1.0 (c (n "cdragon-wad") (v "0.1.0") (d (list (d (n "cdragon-hashes") (r "^0.1") (f (quote ("wad"))) (d #t) (k 0)) (d (n "cdragon-utils") (r "^0.1") (f (quote ("parsing" "guarded_file"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (k 0)))) (h "0j3n02xav0ba4jiamwf55zm7rpz4srqa7pcq0p9xk1kr6ql97r4p")))

(define-public crate-cdragon-wad-0.2.0 (c (n "cdragon-wad") (v "0.2.0") (d (list (d (n "cdragon-hashes") (r "^0.2") (f (quote ("wad"))) (d #t) (k 0)) (d (n "cdragon-utils") (r "^0.2") (f (quote ("parsing" "guarded_file"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.13") (k 0)))) (h "02bhvl2ddr7wpx49kgi7nmc5zk3dhapr2dknb6ihiaqw6w2kzbny")))

