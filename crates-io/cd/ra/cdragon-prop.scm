(define-module (crates-io cd ra cdragon-prop) #:use-module (crates-io))

(define-public crate-cdragon-prop-0.1.0 (c (n "cdragon-prop") (v "0.1.0") (d (list (d (n "cdragon-hashes") (r "^0.1") (f (quote ("bin" "wad"))) (d #t) (k 0)) (d (n "cdragon-utils") (r "^0.1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14075bb8pqm12f3h8r23lcbx11536vgmma1rh8xadkrb8h54bjjn")))

(define-public crate-cdragon-prop-0.2.0 (c (n "cdragon-prop") (v "0.2.0") (d (list (d (n "cdragon-hashes") (r "^0.2") (f (quote ("bin" "wad"))) (d #t) (k 0)) (d (n "cdragon-utils") (r "^0.2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w6a8mhlvyzacz88ss5g77kyphmnvjkf08hw03a22pcxr642mj6a")))

