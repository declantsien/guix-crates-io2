(define-module (crates-io cd ra cdragon-rst) #:use-module (crates-io))

(define-public crate-cdragon-rst-0.2.0 (c (n "cdragon-rst") (v "0.2.0") (d (list (d (n "cdragon-hashes") (r "^0.2") (f (quote ("wad"))) (d #t) (k 0)) (d (n "cdragon-utils") (r "^0.2") (f (quote ("parsing" "guarded_file"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "178kvlqin3q1hdv6ld8v199w9p5zcnwbi4nyi2b8hqbzhl0ah2jg")))

