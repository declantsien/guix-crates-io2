(define-module (crates-io cd ra cdragon-hashes) #:use-module (crates-io))

(define-public crate-cdragon-hashes-0.1.0 (c (n "cdragon-hashes") (v "0.1.0") (d (list (d (n "cdragon-utils") (r "^0.1") (f (quote ("guarded_file"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0igf7z5j986m0f8mp0c6i9y8ks9dhvixf8i3hr5k11hpprqgja88") (f (quote (("default" "bin" "wad") ("bin")))) (s 2) (e (quote (("wad" "dep:twox-hash"))))))

(define-public crate-cdragon-hashes-0.2.0 (c (n "cdragon-hashes") (v "0.2.0") (d (list (d (n "cdragon-utils") (r "^0.2") (f (quote ("guarded_file"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (o #t) (d #t) (k 0)))) (h "09g9zq570pkk555f6gd8a13ca20x8zngfvjhws6rg8g6bmncavag") (f (quote (("default" "bin" "rst" "wad") ("bin")))) (s 2) (e (quote (("wad" "dep:twox-hash") ("rst" "dep:twox-hash"))))))

