(define-module (crates-io cd pa cdparanoia) #:use-module (crates-io))

(define-public crate-cdparanoia-0.1.0 (c (n "cdparanoia") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "cdparanoia-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0p6f757h8702qly4yqlzaikwv7hn5v6c3wpqbcxxg22rs54mwzin")))

