(define-module (crates-io cd g_ cdg_renderer) #:use-module (crates-io))

(define-public crate-cdg_renderer-0.1.0 (c (n "cdg_renderer") (v "0.1.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.10") (d #t) (k 0)))) (h "1z7jvpvd8pdyykjcb3ysz73hhs6m9h86pb27ybjyd3vn8zihhndl")))

(define-public crate-cdg_renderer-0.1.1 (c (n "cdg_renderer") (v "0.1.1") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.10") (d #t) (k 0)))) (h "185ghmip33kwjgzvwm3j9n83fzv269h8lk0b375mhsi0sf7272k5")))

(define-public crate-cdg_renderer-0.2.0 (c (n "cdg_renderer") (v "0.2.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)))) (h "1b0sl7jzqh17fdpqivx473wwhn7g4bn0rb3jjg089cryz30sgiv9")))

(define-public crate-cdg_renderer-0.3.0 (c (n "cdg_renderer") (v "0.3.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)))) (h "03rmmgcdwgsbzjb069czpiyw86dncaqbrddigd5xjfiq46lr1989")))

(define-public crate-cdg_renderer-0.4.0 (c (n "cdg_renderer") (v "0.4.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)))) (h "0jvyn4d42rc4sm462xgl4987588n91bxwhy5b1wvwaib6m967f5i")))

(define-public crate-cdg_renderer-0.5.0 (c (n "cdg_renderer") (v "0.5.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1k2rrp007jzb6v148l3zzxaw8nf6jf6nvd6z55n7gzlwbmcwrha3")))

(define-public crate-cdg_renderer-0.6.0 (c (n "cdg_renderer") (v "0.6.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (k 0)))) (h "0c2pmfhzlbzwsfv5glsrhdiyfx2hxaa6ss8fd8phcn5i0wabxdcf")))

(define-public crate-cdg_renderer-0.7.0 (c (n "cdg_renderer") (v "0.7.0") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)))) (h "0qgy435xfkd0zwk3d72h91wdynh66jkcgx3p6a0hjpkh05g03knf")))

(define-public crate-cdg_renderer-0.7.1 (c (n "cdg_renderer") (v "0.7.1") (d (list (d (n "cdg") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)))) (h "15mgg4fp9gckgvcvaw7rf9aamms4b6mmjnimychj0rzq5r610lrp")))

