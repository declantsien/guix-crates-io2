(define-module (crates-io cd rs cdrs_helpers_derive) #:use-module (crates-io))

(define-public crate-cdrs_helpers_derive-0.1.0 (c (n "cdrs_helpers_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "10kzyvay8m8aqvdrg2idwf0llhdwj8vxhlc10011a9b3k47v6zdc")))

(define-public crate-cdrs_helpers_derive-0.2.0 (c (n "cdrs_helpers_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0v8vnglwf6gdg7saafw6d722m2wrf8c0736qkn81waqxdiz2zryy")))

(define-public crate-cdrs_helpers_derive-0.3.0 (c (n "cdrs_helpers_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0hnszy7vswxz72xhr17f3yqsky0b7whxlr0nns5y02l9hm4snkj5")))

(define-public crate-cdrs_helpers_derive-0.3.1 (c (n "cdrs_helpers_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0jywvba4mmd66930jm9zh4hwkbjyyn8qg8mkk0yq77wkqn1rw4h5")))

(define-public crate-cdrs_helpers_derive-0.3.3 (c (n "cdrs_helpers_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0vxrd5p9wqfz14krb7kicrkh0smz1489a4hsswv7fall59k81ahc")))

(define-public crate-cdrs_helpers_derive-0.4.0 (c (n "cdrs_helpers_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "15b9d33f73ri0j1dryj91405ph390qwfn2bq78w2dv5jlcirv28c")))

