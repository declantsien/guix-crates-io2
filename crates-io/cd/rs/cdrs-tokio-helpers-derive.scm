(define-module (crates-io cd rs cdrs-tokio-helpers-derive) #:use-module (crates-io))

(define-public crate-cdrs-tokio-helpers-derive-1.0.0 (c (n "cdrs-tokio-helpers-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1pizxdkc4lxm1bdqh22886md9vqfpvrb6xbkg05mcgdmph4jrfqr")))

(define-public crate-cdrs-tokio-helpers-derive-1.0.1 (c (n "cdrs-tokio-helpers-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0fa34ggqfin7b37bgffj5szbr23lff5zqjqrjip1iyx3gf8vdxsk")))

(define-public crate-cdrs-tokio-helpers-derive-1.1.0 (c (n "cdrs-tokio-helpers-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0a51nnsi6hn92b0avxpjizgvzc03vz8xg817vnsfcvz5dg0c52as")))

(define-public crate-cdrs-tokio-helpers-derive-1.1.1 (c (n "cdrs-tokio-helpers-derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0s9hv7cf9qhabba5v499bcimgbwqk1q740938b5k2ariy9ka0qpp")))

(define-public crate-cdrs-tokio-helpers-derive-2.0.0 (c (n "cdrs-tokio-helpers-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1w2hggskwxbzibgy4zp6c7h1l182d7m4zf0d87p87jl2yf50v304")))

(define-public crate-cdrs-tokio-helpers-derive-3.0.0 (c (n "cdrs-tokio-helpers-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1d47xl4lxzcp6qi7r0rdp92xi1bmr8i7x31jrsf4r3q9j7k7cmvk")))

(define-public crate-cdrs-tokio-helpers-derive-3.1.0 (c (n "cdrs-tokio-helpers-derive") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bw94c8sgnxmpkwh1bilx2aqsdhcqic0yj6akfswqa3sd1fiq07p")))

(define-public crate-cdrs-tokio-helpers-derive-3.2.0-beta.1 (c (n "cdrs-tokio-helpers-derive") (v "3.2.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r5jmfsw57vhys53mxcr51r13rf2bm0di89wa6d8y4fmyl4rkj1v")))

(define-public crate-cdrs-tokio-helpers-derive-3.2.0-beta.2 (c (n "cdrs-tokio-helpers-derive") (v "3.2.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nwn7af63abkl8cixbwdkiwi39aibbfzzr4mhk74xn8s6cxw464r")))

(define-public crate-cdrs-tokio-helpers-derive-3.2.0 (c (n "cdrs-tokio-helpers-derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "051l45n8rm9rvz07lpzwd4ha9iqppnl9f875hykxn3bvlcryx35s")))

(define-public crate-cdrs-tokio-helpers-derive-4.0.0 (c (n "cdrs-tokio-helpers-derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d1g113x4lh7ijr6qhr1z4gqcamcw3xsfhwwl00yz72ak8g6w2r2")))

(define-public crate-cdrs-tokio-helpers-derive-5.0.0-beta.1 (c (n "cdrs-tokio-helpers-derive") (v "5.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01mnzcxvbn89dsrbpd3y6anx0yyxi4q68dnpzinjrqk4hvq2nd5g")))

(define-public crate-cdrs-tokio-helpers-derive-5.0.0 (c (n "cdrs-tokio-helpers-derive") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hyy3p28n08lgaf0m5svis7hmskb07x92aiyv0zmyhwkb78lwvv9")))

(define-public crate-cdrs-tokio-helpers-derive-5.0.1 (c (n "cdrs-tokio-helpers-derive") (v "5.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k9j61w5b7236ylnhl4jhsansbl84295cl6x13yjg7hcp830cw1c")))

(define-public crate-cdrs-tokio-helpers-derive-5.0.2 (c (n "cdrs-tokio-helpers-derive") (v "5.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "0gfnnkh71qh2vmqxwhrk1la97cyn9imzdbfqbw4r2s1m0cvh53bi")))

(define-public crate-cdrs-tokio-helpers-derive-5.0.3 (c (n "cdrs-tokio-helpers-derive") (v "5.0.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1ksri3wmdvg5qkg90l9h57s2kq1rbbdqn39bwiyvsncacjvzpkk1")))

