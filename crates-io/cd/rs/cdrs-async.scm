(define-module (crates-io cd rs cdrs-async) #:use-module (crates-io))

(define-public crate-cdrs-async-0.1.0-alpha.0 (c (n "cdrs-async") (v "0.1.0-alpha.0") (d (list (d (n "async-std") (r "^1.4.0") (d #t) (k 0)) (d (n "async-tls") (r "^0.6") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "cassandra-proto") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lz4-compress") (r "= 0.1.0") (d #t) (k 0)) (d (n "snap") (r "^0.2.3") (d #t) (k 0)))) (h "0z8g6xsvri6i4329bf202nigdd3rbqwzlr8iirmwpj6xqbyx97sv")))

