(define-module (crates-io cd rs cdrs_helpers_derive_temp) #:use-module (crates-io))

(define-public crate-cdrs_helpers_derive_temp-0.1.0 (c (n "cdrs_helpers_derive_temp") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "12b0zkik516a107p467lxqjcqfhsr0qn8ilfdm3n2m5m1xicd9nv")))

