(define-module (crates-io cd sh cdshealpix) #:use-module (crates-io))

(define-public crate-cdshealpix-0.1.1 (c (n "cdshealpix") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1zcblznsd4bn3gzb69p7cd0n7c1jm6izxh8nmir8idz4g45mcds6") (f (quote (("nightly"))))))

(define-public crate-cdshealpix-0.1.2 (c (n "cdshealpix") (v "0.1.2") (h "1kcycl6i4laabzrm6v64xyj47dm37pal7n5rppcrpkjxjj8lzf6r")))

(define-public crate-cdshealpix-0.1.3 (c (n "cdshealpix") (v "0.1.3") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "1sz9r9jwy0fbiiw9ra8019svw771gnxdk2pyqnc2jqdna3bj15l5")))

(define-public crate-cdshealpix-0.1.4 (c (n "cdshealpix") (v "0.1.4") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "1hn7c0knhb5795fhrjv1w2ic7w7jx6553xmx4skxkbcq333rgwkg")))

(define-public crate-cdshealpix-0.1.5 (c (n "cdshealpix") (v "0.1.5") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "18r2644x4irbdkgr9bqrvrckpm05sj6w6v0q9v54fpazihbypm70")))

(define-public crate-cdshealpix-0.1.6 (c (n "cdshealpix") (v "0.1.6") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "0rs1wry6ifk0ib0mxnlqb4rwwfi9n3v5d62l0f1lkk5bnzkh5bz4")))

(define-public crate-cdshealpix-0.2.0 (c (n "cdshealpix") (v "0.2.0") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "0iwdakf1w8bpa9grc85hvcc94aj2l048hwakdk3xng4q16409m1j")))

(define-public crate-cdshealpix-0.3.0 (c (n "cdshealpix") (v "0.3.0") (d (list (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)))) (h "0dq72ijkc5rwna9qd7qsv9k6x30gqhw5qdn9p3ij7fmpa1anj6lv")))

(define-public crate-cdshealpix-0.4.0 (c (n "cdshealpix") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1f7x1r4k2wa10nvz50ry85gpp3p96j3y9jyi3npbrcbwhws5ij0j")))

(define-public crate-cdshealpix-0.5.2 (c (n "cdshealpix") (v "0.5.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0k486gk26rn5yf8fnd1jpn5m7a45wm2cshskvxdkh1xfrnw9qxr3")))

(define-public crate-cdshealpix-0.5.4 (c (n "cdshealpix") (v "0.5.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1yry08aj91j5fl1lz9g9c9f06jdkjms85l7plgdfv7cwf8snr4r3")))

(define-public crate-cdshealpix-0.5.5 (c (n "cdshealpix") (v "0.5.5") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1ndg6gnm6m08pdrgdw69hg5q19hng0s2yl5rzbp6g1fsnyygjlw8")))

(define-public crate-cdshealpix-0.6.0 (c (n "cdshealpix") (v "0.6.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1c5g1fr587kih6bjhlyad3s780wsq5hx5z85wckqwvv2amajc6fy")))

(define-public crate-cdshealpix-0.6.1 (c (n "cdshealpix") (v "0.6.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1lzh1s3wd573mz1v9i5h2afivzinqs1iiy770kl2m0s3f4phpwbx")))

(define-public crate-cdshealpix-0.6.2 (c (n "cdshealpix") (v "0.6.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "13h8d4rvgzl3fqkc0hs1b5zsr546gvav2gv1q0z8dc9pk4m7m3gq")))

(define-public crate-cdshealpix-0.6.3 (c (n "cdshealpix") (v "0.6.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1r9gbmvp49lr0l9j3wvbk2pvp0n4k00qp51wja65zb658baj9m04")))

(define-public crate-cdshealpix-0.6.4 (c (n "cdshealpix") (v "0.6.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ijxagcj5zw1zfj9d09pap8yw06i2fkc81c7qzfrdgmdidqbqi7d")))

(define-public crate-cdshealpix-0.6.5 (c (n "cdshealpix") (v "0.6.5") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "172ysmiwklyrpv57cvdvxlrciwlrqfh14dmygyq8a2afn0v2c08v")))

(define-public crate-cdshealpix-0.6.6 (c (n "cdshealpix") (v "0.6.6") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0cb6xdy1vgsnrn7dg9x8klf83wqw770qprpx21x38c0263m7k05r")))

(define-public crate-cdshealpix-0.6.7 (c (n "cdshealpix") (v "0.6.7") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1261kamhg0japg8dwpgc8s3hpnabvldxa8abn7sfajlbj786m0dx")))

(define-public crate-cdshealpix-0.6.8 (c (n "cdshealpix") (v "0.6.8") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q59i3d775b87671vhdm3p3k27vp2v5v03jwya14pvyr80bslv05")))

(define-public crate-cdshealpix-0.6.9 (c (n "cdshealpix") (v "0.6.9") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0l56286xvb1s1vxkj05785mzwn2cm4xns9m0n3ll37sghsl2ys31")))

