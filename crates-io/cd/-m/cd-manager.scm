(define-module (crates-io cd -m cd-manager) #:use-module (crates-io))

(define-public crate-cd-manager-0.1.0 (c (n "cd-manager") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0fps3kanxz1g3z0mxm0vzly1yv177v65vmr61zsyqrc8z4iy0x31")))

(define-public crate-cd-manager-0.1.1 (c (n "cd-manager") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1jiflfjkpq1zbhxvqzsncf8mi3hyw1psimml3rplp0f45jcaw4wv")))

