(define-module (crates-io cd ch cdchunking) #:use-module (crates-io))

(define-public crate-cdchunking-0.0.0 (c (n "cdchunking") (v "0.0.0") (h "1dqm0ng0m6liy5hil6g5s9rab9xg4jkfcv8ibwd10kp6p8pj4d2s")))

(define-public crate-cdchunking-0.1.0 (c (n "cdchunking") (v "0.1.0") (h "1a8znv0cch1fyawhrx6x1d7hvfrfx8c2bk11n2659c4ihha93mdd")))

(define-public crate-cdchunking-0.1.1 (c (n "cdchunking") (v "0.1.1") (h "15510n4jha9g1qr2c0dfm2nf6hcyijykcjv6a0srl0m7a5pfl9zz")))

(define-public crate-cdchunking-0.2.0 (c (n "cdchunking") (v "0.2.0") (h "0bf38nyznpzc6mnhaa4a3ls145axj09kxfclfwvq8ymxa1q2pcbc")))

(define-public crate-cdchunking-0.2.1 (c (n "cdchunking") (v "0.2.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1rf26kv4d2qxzzvgfsab4ph1lbknw32707nzj85cxj03k528g8r2")))

(define-public crate-cdchunking-1.0.0 (c (n "cdchunking") (v "1.0.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "021mxm50819ifzicpjhhxiwiap6cwgl8bjzmrv2bs6yrmshlpglh")))

(define-public crate-cdchunking-1.0.1 (c (n "cdchunking") (v "1.0.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0vq8dlcjmsxdh2697b2ddbybqrmp4vgmgdv70hja3l2v8z3a0rnb")))

