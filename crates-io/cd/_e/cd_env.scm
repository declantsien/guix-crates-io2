(define-module (crates-io cd _e cd_env) #:use-module (crates-io))

(define-public crate-cd_env-0.1.0 (c (n "cd_env") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "1igsrfz773lbjplmdvpc1lky2936kw999za8l7d7plpvjazw7wxf")))

(define-public crate-cd_env-0.1.1 (c (n "cd_env") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kkrg25a9hlxy0x40mijki3a08waf2qmdhqps7aivi0mh30ira88")))

(define-public crate-cd_env-0.1.2 (c (n "cd_env") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sfypcsz110v98svdvhkz16dzarqcid7cs9b4vha1jj7gy6ia8vc")))

(define-public crate-cd_env-0.1.3 (c (n "cd_env") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "18cmavyzx9p9brambbd8g7ps469fmnm29i4z2ym7yn9v833njgs6")))

(define-public crate-cd_env-0.2.0 (c (n "cd_env") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bhjdy02hgg5km1ijifzqnsd26c9g57ipp6w1k4wijjivby2r73w")))

