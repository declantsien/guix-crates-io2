(define-module (crates-io cd l- cdl-list-rs) #:use-module (crates-io))

(define-public crate-cdl-list-rs-0.1.0 (c (n "cdl-list-rs") (v "0.1.0") (h "0yhng19wmggrwh93rvskzznhsflymfxwsch95zgfpcmj1c5n366d")))

(define-public crate-cdl-list-rs-0.2.0 (c (n "cdl-list-rs") (v "0.2.0") (h "0zzbvpjpmqvivdasjg3s3bv5f69n61waw0g05f7f92x8h618skgj")))

