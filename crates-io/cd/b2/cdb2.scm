(define-module (crates-io cd b2 cdb2) #:use-module (crates-io))

(define-public crate-cdb2-0.7.0-alpha.1 (c (n "cdb2") (v "0.7.0-alpha.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9.1") (d #t) (k 0)))) (h "0kx4by891342i5bxk4xl4jfdz0arla36vkk50vgcpn0khz7czb19")))

(define-public crate-cdb2-0.7.0 (c (n "cdb2") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "10jl5x2pmvmg0xkaspfjafybrml7n771zr95ygwf0q8x4cmlq5sx")))

(define-public crate-cdb2-0.7.1 (c (n "cdb2") (v "0.7.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "0x578rqgf0b4fzpb8kyz2v94gy40qpzyw4331qkac46gmpfyjnxr") (r "1.70")))

