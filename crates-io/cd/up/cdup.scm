(define-module (crates-io cd up cdup) #:use-module (crates-io))

(define-public crate-cdup-0.1.0 (c (n "cdup") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "09qnlz6nr3dn9gy2jvcncmqz4qk75f5vy74bfpsl55q1qjy79hfq")))

(define-public crate-cdup-0.2.0 (c (n "cdup") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pw8jnfpzjd9mncakjm650vlasgrmrzljzjsd3qjwsk3916crrlr")))

(define-public crate-cdup-0.3.0 (c (n "cdup") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)))) (h "1vi1bgq3pbizac4snbdmdnjpx942fj9r1mkp8ys9fhmf516wzhh7")))

(define-public crate-cdup-0.4.0 (c (n "cdup") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)))) (h "0zv65nri6z5wzmc5ic9w1206gk3ihbgxpmk28m5n13vqwh3b4gvd")))

(define-public crate-cdup-0.5.0 (c (n "cdup") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)))) (h "09hfdzfsziv0gy6wab8prhifd9rbxh90qg973074rfdchfrlfdym")))

