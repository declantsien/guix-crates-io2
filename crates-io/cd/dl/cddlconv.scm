(define-module (crates-io cd dl cddlconv) #:use-module (crates-io))

(define-public crate-cddlconv-0.1.1 (c (n "cddlconv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cddl") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "08wvy5hghxjcxxjyjzgcd5h07kmd5mrwiyrhjy3invhdr8kn7qjr") (f (quote (("vector_groups"))))))

(define-public crate-cddlconv-0.1.2 (c (n "cddlconv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cddl") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "1i6p6c6mzqyyncdvz3q58w6v76lamzdcapiybrrhahwswsh75lid") (f (quote (("vector_groups"))))))

(define-public crate-cddlconv-0.1.3 (c (n "cddlconv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cddl") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "0mv05ch4v5m74c3xiv2gh5l0dw9085hfh03m0gfjcwlgrqpj3m9m") (f (quote (("vector_groups"))))))

(define-public crate-cddlconv-0.1.4 (c (n "cddlconv") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cddl") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "1wg03f1hwwxa9br28dwmpqmdn7hzzdmaln3d59wqywvcszgsxsfw") (f (quote (("vector_groups"))))))

(define-public crate-cddlconv-0.1.5 (c (n "cddlconv") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cddl") (r "^0.9.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)))) (h "0iv18y9cnrzzs97k580kimvg6dfs60i58hx6fidskpm84qppkwsm") (f (quote (("vector_groups"))))))

