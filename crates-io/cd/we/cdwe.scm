(define-module (crates-io cd we cdwe) #:use-module (crates-io))

(define-public crate-cdwe-0.1.0 (c (n "cdwe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0w7h5kc9ns9dhxqkzd6mnmsr5ll1647qbhr7i3xkhwdsh34iwwp0")))

(define-public crate-cdwe-0.1.1 (c (n "cdwe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0zadc9ygrgh6lyz0g7d98ng7wk1i40k2ad63d55rqbncm2pwvkn3")))

(define-public crate-cdwe-0.1.2 (c (n "cdwe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "06a2231hrq0rh9xlazl5mj6ij509ab6h36vvgrnrb62sgiq08fjk")))

(define-public crate-cdwe-0.1.3 (c (n "cdwe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1yhfwn5lqh82cwzs2n838x72pb9j9wiyjx585xfrpfcdrrl5mc2h")))

(define-public crate-cdwe-0.1.4 (c (n "cdwe") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "153ips6n6pipld6vzm8dakp012lcpv4dg7v0xd48vmrc821g33mq")))

(define-public crate-cdwe-0.2.0 (c (n "cdwe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "10vx72q0fvm02n1ak12mc43n52ppwh1y6aq790mi9i0isf3n5zrh")))

(define-public crate-cdwe-0.2.1 (c (n "cdwe") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1zsm1lkd56ym045cjwchhp1qy0126fz2df39c4yiphm42njkxs6v")))

(define-public crate-cdwe-0.2.2 (c (n "cdwe") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0wgbbj9sfpn8wkg72zmkk2abk61inyvxqpi24b4m5bvg6wb7gw71")))

(define-public crate-cdwe-0.2.3 (c (n "cdwe") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1d5lx6frdfqkwhwrdi7021h57v1ic1vs4zis0sw9sqlah18cd1r3")))

(define-public crate-cdwe-0.2.4 (c (n "cdwe") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0cs92zszl2z59ffmxy7289sd0az46bz5ijzdh4igbvw0nbz0w83y")))

(define-public crate-cdwe-0.3.0 (c (n "cdwe") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0bl3ppd89qpmpnnb8vcry6s5injx6v0n50synwcyrvpnhzak2g9p")))

