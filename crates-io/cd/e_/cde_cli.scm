(define-module (crates-io cd e_ cde_cli) #:use-module (crates-io))

(define-public crate-cde_cli-0.1.1 (c (n "cde_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cde") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0f1w2p8i6j04zxzrjd9ysa6wik2j0qhhd50r4gfxpiz9wargqjr3")))

(define-public crate-cde_cli-0.3.1 (c (n "cde_cli") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cde") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1iv2vzigg195g2zcglzvlkym3sxw2nfqyid5qwgk4cbnrdwacaci")))

