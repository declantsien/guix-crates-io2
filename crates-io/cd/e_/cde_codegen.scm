(define-module (crates-io cd e_ cde_codegen) #:use-module (crates-io))

(define-public crate-cde_codegen-0.1.0 (c (n "cde_codegen") (v "0.1.0") (d (list (d (n "cde") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1yhqn8q3n627fdnqa7j43blacqppv4y33iigk0mr84anc5nvkh4y")))

