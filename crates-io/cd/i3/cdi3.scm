(define-module (crates-io cd i3 cdi3) #:use-module (crates-io))

(define-public crate-cdi3-0.1.0 (c (n "cdi3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08x5wipijbaw1b2v7fz60ibxg13ay63kl48xgn15hfqm86hwvysl")))

(define-public crate-cdi3-0.2.0 (c (n "cdi3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0df13rlsd6f389pn5c3182nhaxgn7g8jxil6cy3w5z6w35rzlkc6")))

