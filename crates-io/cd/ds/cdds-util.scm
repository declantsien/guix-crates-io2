(define-module (crates-io cd ds cdds-util) #:use-module (crates-io))

(define-public crate-cdds-util-0.1.0 (c (n "cdds-util") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0w7rsppg1w19nfprpw3mqfkwdgq6zs695kmf1d7m6qh58ix0xh0k")))

(define-public crate-cdds-util-0.1.1 (c (n "cdds-util") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "1la4cl9vvi6ahyw2r0bm7fcyhiknvhmpxpmr7n31zjf24pbxrsj1")))

