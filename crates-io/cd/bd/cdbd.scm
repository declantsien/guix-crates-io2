(define-module (crates-io cd bd cdbd) #:use-module (crates-io))

(define-public crate-cdbd-0.1.0 (c (n "cdbd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mtbl") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5.0") (d #t) (k 0)) (d (n "objpool") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)) (d (n "tinycdb") (r "^0.0.7") (d #t) (k 0)))) (h "1b0axp391k2f8x3l17ssxyvxviqi0j5l5wq02gjlbzr0ligfc2h3")))

(define-public crate-cdbd-0.1.1 (c (n "cdbd") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mtbl") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5.0") (d #t) (k 0)) (d (n "objpool") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)) (d (n "tinycdb") (r "^0.0.7") (d #t) (k 0)))) (h "1lxcghws3sv43grgbxg9ynfgw23gjpq5h6xrkjmkhdzip2z8f9nh")))

