(define-module (crates-io cd iv cdivsufsort) #:use-module (crates-io))

(define-public crate-cdivsufsort-1.0.0 (c (n "cdivsufsort") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "sacabase") (r "^1.0.0") (d #t) (k 0)))) (h "1yvrkpfsgx6imjyv10qcry668lrycc82i5jh4z5y2r2z31nvfr6z") (f (quote (("crosscheck"))))))

(define-public crate-cdivsufsort-2.0.0 (c (n "cdivsufsort") (v "2.0.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "sacabase") (r "^2.0.0") (d #t) (k 0)))) (h "134vyyznqaf24bksg29d9cid5fq0n1dsfbbn2sj9sq4pj40wxvzd") (f (quote (("crosscheck"))))))

