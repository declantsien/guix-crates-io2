(define-module (crates-io cd #{74}# cd74hc4067) #:use-module (crates-io))

(define-public crate-cd74hc4067-0.1.0 (c (n "cd74hc4067") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "18qir02v7cmcqg388qz8zclrblyfqcwgn1c0cfwaanc21ba2j3r0")))

(define-public crate-cd74hc4067-0.2.0 (c (n "cd74hc4067") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "0vc62dj0j0203as2b1j9300l3576b97yf4k3a0yxl3k0bkaddcdz")))

