(define-module (crates-io cd ow cdown) #:use-module (crates-io))

(define-public crate-cdown-0.1.0 (c (n "cdown") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "1j575krh7b6f4z1g3m9ndj349dgm816v3ygv7swnkh5q9dyrrkhc")))

