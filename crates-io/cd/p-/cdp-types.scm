(define-module (crates-io cd p- cdp-types) #:use-module (crates-io))

(define-public crate-cdp-types-0.1.0 (c (n "cdp-types") (v "0.1.0") (d (list (d (n "cea708-types") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1w3r2f6hsrh52r625cswf3q2y516v22dvwi959hp21m4h4h486yc")))

