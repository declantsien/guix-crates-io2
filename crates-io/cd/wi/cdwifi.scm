(define-module (crates-io cd wi cdwifi) #:use-module (crates-io))

(define-public crate-cdwifi-0.1.0 (c (n "cdwifi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)))) (h "0h1bmbdzs5gfqlbwa1px6c45liwqlb0qzabd8d8angwrn8563irg")))

(define-public crate-cdwifi-0.1.1 (c (n "cdwifi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)))) (h "1av8f7ap3drh241fwrx97dsyawhczwz22rmx4v33cdznkk8rwr10")))

(define-public crate-cdwifi-0.1.2 (c (n "cdwifi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)))) (h "0q3acqkx7rydlsw9l76k7snd0v7an8aidwkmy2w2zlr6hp4pi30p")))

