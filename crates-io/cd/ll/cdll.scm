(define-module (crates-io cd ll cdll) #:use-module (crates-io))

(define-public crate-cdll-0.1.0 (c (n "cdll") (v "0.1.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)))) (h "0mq3bsxwmyddy9zg66c5h5hc13nf83fgpls96i8q39ipgswv1shn") (y #t)))

(define-public crate-cdll-0.1.1 (c (n "cdll") (v "0.1.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)))) (h "1jr6l8ir737qwsci9z9v5mbrnxnv8ikvd3gn9lr2irwicj8yfqrv")))

(define-public crate-cdll-0.2.0 (c (n "cdll") (v "0.2.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)))) (h "13fz83fv3crhxs7ci9f7plvm9lm1kga1cabn98v7wh2jy36gyjxb")))

(define-public crate-cdll-0.3.0 (c (n "cdll") (v "0.3.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)))) (h "16pmiyy8bx1lw2azdggdwj3x1x1dxf9qm5yj2rj3zbiim7bh1gcy")))

(define-public crate-cdll-0.4.0 (c (n "cdll") (v "0.4.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)))) (h "119188dyyx4nfr8axzw2xpvniwvfjk0m4p58bwhymg7hifa8j6si")))

(define-public crate-cdll-0.4.1 (c (n "cdll") (v "0.4.1") (h "16ia7klyg04dbvr8bg6f0srn73mz1ym6lx7r4zc62985sykvc5pd")))

(define-public crate-cdll-0.4.2 (c (n "cdll") (v "0.4.2") (h "1m7vsgha9kaj2h0i880hxs8i1j4mcvqnf3aipj4ljb3p8ar5468b")))

