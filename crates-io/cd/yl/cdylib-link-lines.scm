(define-module (crates-io cd yl cdylib-link-lines) #:use-module (crates-io))

(define-public crate-cdylib-link-lines-0.1.0 (c (n "cdylib-link-lines") (v "0.1.0") (h "141y4n9wqkqgmarwkfjg78jlxc93f28y4l67ivp79vkrcq7sbr2a")))

(define-public crate-cdylib-link-lines-0.1.1 (c (n "cdylib-link-lines") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0d4zjzzlz22wfcr5z29fra8gryacy309f8r4vmg4r1qxccn30xk3")))

(define-public crate-cdylib-link-lines-0.1.2 (c (n "cdylib-link-lines") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ay4di3prrvci37i9cbgy5chzznywx5p9bkf2w6b4x1yaqdwm0p4")))

(define-public crate-cdylib-link-lines-0.1.3 (c (n "cdylib-link-lines") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "16agh8nlphy7xcyf43ra13v525xqcaxjp56s6ywlkqvvqffxnf8x")))

(define-public crate-cdylib-link-lines-0.1.4 (c (n "cdylib-link-lines") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1wkhs6326pp545iamp4q205mryhn4xv35xnpa4g76mdllmzdn5x3")))

(define-public crate-cdylib-link-lines-0.1.5 (c (n "cdylib-link-lines") (v "0.1.5") (h "1g10ididkzsxv3p9bw3y8iq7qprkmjqsn34w1gymmpxv13psp3nr")))

