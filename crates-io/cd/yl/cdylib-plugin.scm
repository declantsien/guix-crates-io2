(define-module (crates-io cd yl cdylib-plugin) #:use-module (crates-io))

(define-public crate-cdylib-plugin-0.1.0 (c (n "cdylib-plugin") (v "0.1.0") (h "1dl6fycxjcglpb2r82l4ih6s550l1j4fxn7sv6jksx1y35y27il4")))

(define-public crate-cdylib-plugin-0.1.1 (c (n "cdylib-plugin") (v "0.1.1") (h "1j05kp1bh2cdhinqlhdl4kfzllbmzhy44s7f0q6i0zhgqlkmcrx9")))

(define-public crate-cdylib-plugin-0.1.2 (c (n "cdylib-plugin") (v "0.1.2") (h "1jz3064hkvqjj38kggd97nyygkblqzvdyi749v3v4qm0sz4ca0gm")))

(define-public crate-cdylib-plugin-0.1.3 (c (n "cdylib-plugin") (v "0.1.3") (h "1wfy0ygg5parj6bszz5n35crwb2m2mqn2dxvqflagjqi0hf849pa")))

(define-public crate-cdylib-plugin-0.1.4 (c (n "cdylib-plugin") (v "0.1.4") (h "0pzg7k3p32byh5lxafrwkrgdg000nl82kdh0g51b66nals27gghp")))

(define-public crate-cdylib-plugin-0.1.5 (c (n "cdylib-plugin") (v "0.1.5") (h "04lpjhx447piqhjqz8asz8z5bhz5gg0mgwsq0cis9qf9sbjqg29d")))

(define-public crate-cdylib-plugin-0.1.6 (c (n "cdylib-plugin") (v "0.1.6") (h "1ywfy638cnylncqsb61sl73r8kvplwh6yz67w9zw5g8wziq70h2p")))

