(define-module (crates-io kz en kzen-paillier) #:use-module (crates-io))

(define-public crate-kzen-paillier-0.4.0 (c (n "kzen-paillier") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv") (r "^0.7") (k 0) (p "curv-kzen")) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hadjcg7pmzdpzyl5garx2v23bhv8azxyh28k55g1lm9j8ha698r") (f (quote (("default" "curv/rust-gmp-kzen"))))))

(define-public crate-kzen-paillier-0.4.1 (c (n "kzen-paillier") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rnqkpzn7h6gf2r98dsn0i0bgynj5p0h9v8yjr2piah5z2kf7k1c") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

(define-public crate-kzen-paillier-0.4.2 (c (n "kzen-paillier") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.9") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dc71xbvk4b8917kb68894hflc5bjqy0h74kvmynvh532m53jc93") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

(define-public crate-kzen-paillier-0.4.3 (c (n "kzen-paillier") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.10.0") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vq4bnyhpa33hy3bwqqyg8p1cqxsd3q4c7gdn8w3h84g6ily9dwh") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

