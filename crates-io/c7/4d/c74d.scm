(define-module (crates-io c7 #{4d}# c74d) #:use-module (crates-io))

(define-public crate-c74d-0.1.0 (c (n "c74d") (v "0.1.0") (h "1lj1by0h14ra0yda1w42sc7vhszm3cvv7038n4jr9ss4j80vhrjp")))

(define-public crate-c74d-0.1.1 (c (n "c74d") (v "0.1.1") (h "0w6p3ry0fym84y5jdv5rpcbvg5vqaa8gfr6vi3jl44lz8l081w7y") (r "1.56.0")))

