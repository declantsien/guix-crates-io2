(define-module (crates-io qv op qvopenapi-sys) #:use-module (crates-io))

(define-public crate-qvopenapi-sys-0.1.0 (c (n "qvopenapi-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "1wjdqzczp04rbnp3gzin655xm15k2r4gy0arh7dlpsamp1wi2w1v") (f (quote (("disable-unwind"))))))

