(define-module (crates-io qv op qvopenapi-async) #:use-module (crates-io))

(define-public crate-qvopenapi-async-0.1.0 (c (n "qvopenapi-async") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qvopenapi") (r "^0.1.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1pl2ikvbd22r68jr1aaw7s68fk50d1b0wxbl9hhzy5gwkl83m9ny") (f (quote (("disable-unwind" "qvopenapi/disable-unwind"))))))

