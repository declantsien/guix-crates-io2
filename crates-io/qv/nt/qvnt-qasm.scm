(define-module (crates-io qv nt qvnt-qasm) #:use-module (crates-io))

(define-public crate-qvnt-qasm-0.2.0 (c (n "qvnt-qasm") (v "0.2.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0832w3q79y0a3xjnnbd45j40j5gbzr35rjrmp33cwiwpns2jl0i3") (f (quote (("no-check-ver") ("default"))))))

