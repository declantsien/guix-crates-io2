(define-module (crates-io qv nt qvnt-i) #:use-module (crates-io))

(define-public crate-qvnt-i-0.4.2 (c (n "qvnt-i") (v "0.4.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "qvnt") (r "^0.4.2") (f (quote ("cpu" "interpreter"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0gp4543n6b0z2kfix777iy5aqdd9kb96v86wcabvdjbri2143w7f")))

(define-public crate-qvnt-i-0.4.3 (c (n "qvnt-i") (v "0.4.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "qvnt") (r "^0.4.3") (f (quote ("multi-thread" "interpreter"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0bfjqf6wn7l6508y99nk2ih9d1inhydjkf3chdf0av1r31il7npl")))

(define-public crate-qvnt-i-0.4.3-fix1 (c (n "qvnt-i") (v "0.4.3-fix1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "qvnt") (r "^0.4.3") (f (quote ("multi-thread" "interpreter"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1ygf5riv6650j9fcpbgdhxamqdqkj3lvphay9y0sjz0i5xbfhyhw")))

(define-public crate-qvnt-i-0.4.4 (c (n "qvnt-i") (v "0.4.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "qvnt") (r "^0.4.4") (f (quote ("multi-thread" "interpreter"))) (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "termtree") (r "^0.4.0") (d #t) (k 0)))) (h "0bi8qr88dvkxm23q4hzh854cl53nba15mknid401dcmn3fnjcfxv") (f (quote (("tracing" "env_logger") ("default"))))))

