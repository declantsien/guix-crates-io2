(define-module (crates-io c3 -l c3-lang-linearization) #:use-module (crates-io))

(define-public crate-c3-lang-linearization-0.0.1 (c (n "c3-lang-linearization") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)))) (h "04fg2hjrqj91gdqn5iq415iy3iwkwfsr558asq8y1rb5r4cn2kkq")))

(define-public crate-c3-lang-linearization-0.0.2 (c (n "c3-lang-linearization") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1a9vfddjnwjr3npgzbi45krik7zdmc3p1zby3ycnnw2j5xdfiy5a")))

(define-public crate-c3-lang-linearization-0.0.3 (c (n "c3-lang-linearization") (v "0.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0k5dd909z1km54qvm7i3hfq3wbly53jlga3ksdvnw5lrxpxz605w")))

(define-public crate-c3-lang-linearization-0.0.4 (c (n "c3-lang-linearization") (v "0.0.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0lvhgbjyr72aa1ngd7i8sfdzpivk5djs5587jcw720ra2fvxvn7d")))

