(define-module (crates-io c3 -l c3-lang-macro) #:use-module (crates-io))

(define-public crate-c3-lang-macro-0.0.1 (c (n "c3-lang-macro") (v "0.0.1") (d (list (d (n "c3-lang-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hr4srxkvvcyxyh7lfwyqsxn1kd7mdwikmvv211b7sz220k8h4l9")))

(define-public crate-c3-lang-macro-0.0.2 (c (n "c3-lang-macro") (v "0.0.2") (d (list (d (n "c3-lang-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bwdkdf1xwy2k599x652646w7bbxnapifgzqr3qixrpxpqxhky3w")))

(define-public crate-c3-lang-macro-0.0.3 (c (n "c3-lang-macro") (v "0.0.3") (d (list (d (n "c3-lang-parser") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mhn8qgd1lb6plmxpyyilwwzvc315c1rp2ni8gy6v391z26aiv5i")))

(define-public crate-c3-lang-macro-0.0.4 (c (n "c3-lang-macro") (v "0.0.4") (d (list (d (n "c3-lang-parser") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fg5b31np704rdm1bmhg158zn1xrdrq99w0f8lld6dxg49i8g0b7")))

