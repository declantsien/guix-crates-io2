(define-module (crates-io c3 -l c3-lang-parser) #:use-module (crates-io))

(define-public crate-c3-lang-parser-0.0.1 (c (n "c3-lang-parser") (v "0.0.1") (d (list (d (n "c3-lang-linearization") (r "^0.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18y1wmn8dy6mz6qc5q0lzr3dak4fc2abmv0wq4ffj0f32nv0wz2m")))

(define-public crate-c3-lang-parser-0.0.2 (c (n "c3-lang-parser") (v "0.0.2") (d (list (d (n "c3-lang-linearization") (r "^0.0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "081kbaczp4cazbw4qa0chz4w5c957ykzviazgq00x46bx0z02ls4")))

(define-public crate-c3-lang-parser-0.0.3 (c (n "c3-lang-parser") (v "0.0.3") (d (list (d (n "c3-lang-linearization") (r "^0.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0whbvhz24q9qcc1nhlvcm9cj4xwvympmrgx5klclqwi900bvhlgx")))

(define-public crate-c3-lang-parser-0.0.4 (c (n "c3-lang-parser") (v "0.0.4") (d (list (d (n "c3-lang-linearization") (r "^0.0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s094s1r5jvqyksx94mwl4mzrsgn8dp4ivjysfx3marbnj3a20ki")))

