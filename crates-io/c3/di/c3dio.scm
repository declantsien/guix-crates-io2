(define-module (crates-io c3 di c3dio) #:use-module (crates-io))

(define-public crate-c3dio-0.1.0 (c (n "c3dio") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "0farfr7blmljhr5jhcjp03q09p3xvrg3v8mbp1852i90kn5xkrpp") (y #t)))

(define-public crate-c3dio-0.2.0 (c (n "c3dio") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "0ya08mndlwig8ly9g8bsj3za4rdsfnlg2c3wc6g9hs4701ll5hgl")))

(define-public crate-c3dio-0.3.0 (c (n "c3dio") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "1in5hj1v6nz39lcwpr7jqjdkvwzn8wnhhbczzsblbp320jr580zw")))

(define-public crate-c3dio-0.4.0 (c (n "c3dio") (v "0.4.0") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)))) (h "1q0rp4fb03m7h8c19n91qx65fdvcjivmv03yyrpj9hdv4n28pxbz")))

(define-public crate-c3dio-0.5.0 (c (n "c3dio") (v "0.5.0") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)))) (h "04xs3q54gdvc61j016s9742hcagkllvhf97jxjrw0r8jqmp4vrnq")))

(define-public crate-c3dio-0.6.0 (c (n "c3dio") (v "0.6.0") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)) (d (n "test-files") (r "^0.1.2") (d #t) (k 2)))) (h "0bh4kl7n94k4fcjfd1r2kx3gwg0385iy0ci2f697wpvsvfavc81f")))

(define-public crate-c3dio-0.6.1 (c (n "c3dio") (v "0.6.1") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)) (d (n "test-files") (r "^0.1.2") (d #t) (k 2)))) (h "1ikyl86g92a3rvr1kvpc19jhh9805g5fli0xi2v9chm8c8bvy4q0")))

(define-public crate-c3dio-0.7.0 (c (n "c3dio") (v "0.7.0") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)) (d (n "test-files") (r "^0.1.2") (d #t) (k 2)))) (h "09chadryni8vsmvs84y7dj96j194r8in6cg2g2bbkrbzp32mhaqy")))

(define-public crate-c3dio-0.8.0 (c (n "c3dio") (v "0.8.0") (d (list (d (n "grid") (r "^0.10") (d #t) (k 0)) (d (n "test-files") (r "^0.1.2") (d #t) (k 2)))) (h "1alch2qmmpzpci30jvsqkwx93nvb8jvwvwbs60ilnh9rd7vyvnw3")))

