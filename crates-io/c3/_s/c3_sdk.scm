(define-module (crates-io c3 _s c3_sdk) #:use-module (crates-io))

(define-public crate-c3_sdk-0.1.0 (c (n "c3_sdk") (v "0.1.0") (d (list (d (n "buffer") (r "^0.1.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0lanzkcmya3bj0xvn73423q32svvn076rjdn8jp6gc206vbbswz7")))

