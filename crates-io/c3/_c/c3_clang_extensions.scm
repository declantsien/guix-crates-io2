(define-module (crates-io c3 _c c3_clang_extensions) #:use-module (crates-io))

(define-public crate-c3_clang_extensions-0.3.1 (c (n "c3_clang_extensions") (v "0.3.1") (d (list (d (n "clang-sys") (r "^0.18") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "0hmv5y6azgsazz3bf3whp12x0pssiin11mvxlgq1afskbl225k95") (y #t)))

(define-public crate-c3_clang_extensions-0.3.2 (c (n "c3_clang_extensions") (v "0.3.2") (d (list (d (n "clang-sys") (r "^0.18") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "141fv9sabsbwwpjlbnfsln3kamdrc6i748g8x36cy51fqv8wgbwf") (y #t)))

(define-public crate-c3_clang_extensions-0.3.3 (c (n "c3_clang_extensions") (v "0.3.3") (d (list (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "1yzy2k92zl0zvkc7gl9llk39y6fm1q638qdv5g5q47wjpnk6xfy8") (y #t)))

(define-public crate-c3_clang_extensions-0.3.4 (c (n "c3_clang_extensions") (v "0.3.4") (d (list (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "0ikcr2sq370p9rz35d2mjwp6g3v0v5hhif4y8msrshp4l7fci7m9") (y #t)))

(define-public crate-c3_clang_extensions-0.3.5 (c (n "c3_clang_extensions") (v "0.3.5") (d (list (d (n "clang-sys") (r "^0.19") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "0m2qd7ng8xw3c6d4pwi617h1wi0ggb2smm71w3szpvvqshp27fa0") (y #t)))

(define-public crate-c3_clang_extensions-0.3.6 (c (n "c3_clang_extensions") (v "0.3.6") (d (list (d (n "clang-sys") (r "^0.20") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)))) (h "15llb9r4mmrm297zivhnv0i1xqmb3zkw32d32wwmvqpsbisj8hn8") (y #t)))

(define-public crate-c3_clang_extensions-0.3.7 (c (n "c3_clang_extensions") (v "0.3.7") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "clang-sys") (r "^0.20") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "dunce") (r "^0.1.0") (d #t) (k 1)))) (h "0nn5vl7a0hs6inqq0q6yi1pm6wx16190rzqq2fq9h9kl8nqycs6m")))

(define-public crate-c3_clang_extensions-0.3.9 (c (n "c3_clang_extensions") (v "0.3.9") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "clang-sys") (r "^0.29") (f (quote ("static" "clang_4_0"))) (d #t) (k 0)) (d (n "dunce") (r "^0.1.0") (d #t) (k 1)))) (h "1p6r07kwdi8jmnw5rcrg234pcd4p76hb3ycg1h94rlxqicjw5gnw") (l "c3_clang_extensions")))

