(define-module (crates-io c3 d- c3d-rs) #:use-module (crates-io))

(define-public crate-c3d-rs-0.1.0 (c (n "c3d-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0p9jfvba2hp37ki9wav16vzhdwgms5j89raa91873q7wzshy5ysn")))

(define-public crate-c3d-rs-0.1.1 (c (n "c3d-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0fjk7klqrnnlzcbpvg7fvpq6pvcwjw3hcn69kkp23h7dplxddx4d")))

(define-public crate-c3d-rs-0.1.2 (c (n "c3d-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "04xhdw5m5w4vqlbfpg0f2vczpkkc2qwccv4hbfi78x3h5mi9rgzi")))

(define-public crate-c3d-rs-0.1.3 (c (n "c3d-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "13d3kh2hhyf6s6z80jdfpcm57xsppwq8g1ybfsrh5y2f6167bmsa")))

(define-public crate-c3d-rs-0.1.4 (c (n "c3d-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0j3yml703mh4rh8kq21xyvs1lqvqrcnswd222xh9xxwyxbi82jm5")))

(define-public crate-c3d-rs-0.1.5 (c (n "c3d-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1r0kfy1rh6c56yf029ngwvwcl7x8fxcc6f0wzz1v8rdx0zaqn33l")))

(define-public crate-c3d-rs-0.1.6 (c (n "c3d-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0y8bp0sf22n2nn76yp7xqvmq35kq3lkf934hzfncakxss006zbl2")))

(define-public crate-c3d-rs-0.1.7 (c (n "c3d-rs") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1a3z6s8y0ym973jys449jcywmkjrh5wpci3jn6pjjcz6zdd6mliy")))

