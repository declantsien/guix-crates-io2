(define-module (crates-io c3 #{14}# c314-utils) #:use-module (crates-io))

(define-public crate-c314-utils-0.1.0 (c (n "c314-utils") (v "0.1.0") (h "1fr94j6dgaqx3fcp4n3m46i18spa0ki5x7q0djnc84ni9z702flm") (y #t)))

(define-public crate-c314-utils-0.1.1 (c (n "c314-utils") (v "0.1.1") (h "02smlwpzg05rbs56z2lbbmzvdw1z4wn0j0gd3qqlbir3j8cdkhks") (y #t)))

(define-public crate-c314-utils-0.1.2 (c (n "c314-utils") (v "0.1.2") (h "02lv3k17svdm5bj6xagkdalqb2lk9j0sn5jz6ssrcxpnblyxd7p2") (y #t)))

(define-public crate-c314-utils-0.1.3 (c (n "c314-utils") (v "0.1.3") (h "1z9fb2w3s2flgcf467yvdah7kkwa1m4187rpj27mvmd9q87fshgj") (y #t)))

(define-public crate-c314-utils-0.1.4 (c (n "c314-utils") (v "0.1.4") (h "17hai7h5hns6f32ad2xyxh6yafpmcn6qrwg2597fb1vxkq3a4cwq") (y #t)))

(define-public crate-c314-utils-0.2.0 (c (n "c314-utils") (v "0.2.0") (h "1j7g3d0r1j561iqqbm733dzqrx87hj7apqc13qw0cl3zkls04wp5") (y #t)))

