(define-module (crates-io c3 p0 c3p0_common_async) #:use-module (crates-io))

(define-public crate-c3p0_common_async-0.44.0 (c (n "c3p0_common_async") (v "0.44.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "c3p0_common") (r "^0.44.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1pqiqbx5knh9ns56ylny76k975bn2qj1p9k3a6waz4zy45xvclzq") (f (quote (("migrate" "c3p0_common/migrate") ("default"))))))

