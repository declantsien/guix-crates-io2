(define-module (crates-io c3 p0 c3p0_pool_pg) #:use-module (crates-io))

(define-public crate-c3p0_pool_pg-0.8.0 (c (n "c3p0_pool_pg") (v "0.8.0") (d (list (d (n "c3p0_common") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)))) (h "0risr25ldcqdc44ad3swxg6r2c17lx3gd6w221vx2p79pz4g60p0")))

(define-public crate-c3p0_pool_pg-0.8.2 (c (n "c3p0_pool_pg") (v "0.8.2") (d (list (d (n "c3p0_common") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)))) (h "0mi2as8v5plj9lz230pn6bdr1h99r948xkdcarzf4xb9dvp8wzdc")))

(define-public crate-c3p0_pool_pg-0.9.0 (c (n "c3p0_pool_pg") (v "0.9.0") (d (list (d (n "c3p0_common") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1r629bd8b61qixf4r6svg2bp2gm0j7dlkxx64vaqcyghh21g9api")))

(define-public crate-c3p0_pool_pg-0.9.1 (c (n "c3p0_pool_pg") (v "0.9.1") (d (list (d (n "c3p0_common") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lq616y5wv38ni2a45hh1s6mgb8gz75nl4j1l884apf6yrp9nbjp")))

(define-public crate-c3p0_pool_pg-0.10.0 (c (n "c3p0_pool_pg") (v "0.10.0") (d (list (d (n "c3p0_common") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hv8mw2dfk9mn71w66mliklarc3lmr1yvp2j77pkw16dlbvv0i3b")))

(define-public crate-c3p0_pool_pg-0.10.1 (c (n "c3p0_pool_pg") (v "0.10.1") (d (list (d (n "c3p0_common") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02ff9if8ccj6i2gasm4nhw7ky14fcrc1lr78gq96jyz40xsxkfgr")))

(define-public crate-c3p0_pool_pg-0.11.0 (c (n "c3p0_pool_pg") (v "0.11.0") (d (list (d (n "c3p0_common") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0dcmn6b96w11lx9a5xh8s4p0mbs5vdk4z2dkv1qafgab6ynkgly1")))

