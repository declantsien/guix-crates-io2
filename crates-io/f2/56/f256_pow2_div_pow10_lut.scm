(define-module (crates-io f2 #{56}# f256_pow2_div_pow10_lut) #:use-module (crates-io))

(define-public crate-f256_pow2_div_pow10_lut-0.1.1 (c (n "f256_pow2_div_pow10_lut") (v "0.1.1") (h "1sf818iphsrdm4kq4z3h8h3x01lprjgcvvcdj3638xlgqvs7hp46")))

(define-public crate-f256_pow2_div_pow10_lut-0.2.0 (c (n "f256_pow2_div_pow10_lut") (v "0.2.0") (h "1d02l87pdbd1xp6cf7lrmmmp2v8b4cxrspd4kqzp5lqp9mhf3035")))

