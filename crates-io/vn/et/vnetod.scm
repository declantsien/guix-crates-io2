(define-module (crates-io vn et vnetod) #:use-module (crates-io))

(define-public crate-vnetod-0.1.0 (c (n "vnetod") (v "0.1.0") (h "019zk33yblx9a0qvjcm728x0mxzyfr6ifgs225zjdf6gbnrzrmdv")))

(define-public crate-vnetod-0.2.0 (c (n "vnetod") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cg39cs3xs2cnda0vr8miz8v19bw48jqahx1jk1vz0kfil66l506")))

(define-public crate-vnetod-0.2.1 (c (n "vnetod") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1icdd34aajabhw53zj9g065ryjiwcg6hj2ycr0mc8bh07d2hfxaf")))

(define-public crate-vnetod-0.2.2 (c (n "vnetod") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ryrjly5hksc7vyk4c67ikyjcg8qbrks39w4db9f08ghc4h2gxbh")))

(define-public crate-vnetod-0.3.0 (c (n "vnetod") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p246frjbly6ff0dz8kzgwwiqa7c6jknjr3rf53f34lah04rzhk3")))

(define-public crate-vnetod-0.3.1 (c (n "vnetod") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "14mdwrsb02l439v148k0si4mspyy25qx2yql7wxdryig6r3g6skm")))

(define-public crate-vnetod-0.3.2 (c (n "vnetod") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("std" "derive"))) (k 0)))) (h "0pbbif1yzgllwwvq5dhsyk9d8rfixr2d18s7p0npm8xd6yji766g") (f (quote (("color" "clap/color"))))))

(define-public crate-vnetod-0.3.3 (c (n "vnetod") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("std" "derive"))) (k 0)))) (h "0nib77xs033144r0cm93zc22dyw8m66yywqrpdz9zliy1jpvkcry") (f (quote (("color" "clap/color"))))))

(define-public crate-vnetod-0.4.0 (c (n "vnetod") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("std" "env" "derive"))) (k 0)) (d (n "termcolor") (r "^1.1.3") (o #t) (d #t) (k 0)))) (h "04dz9kpypi523jncmv38pnss3bg240d3x3rjw9j2r03y1v984hss") (s 2) (e (quote (("color" "clap/color" "dep:atty" "dep:termcolor"))))))

