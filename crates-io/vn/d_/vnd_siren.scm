(define-module (crates-io vn d_ vnd_siren) #:use-module (crates-io))

(define-public crate-vnd_siren-0.1.0 (c (n "vnd_siren") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08250k2xikvc62w6s5kfrmc2wmzbqjnzkj870v2yay4da4wdwn92")))

(define-public crate-vnd_siren-0.2.0 (c (n "vnd_siren") (v "0.2.0") (d (list (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1233xqclrg3rh3rj1nh4lrjmf1nri8d5fmm7nxv7n9ay22jjawfa") (f (quote (("http-compat" "http") ("default" "http-compat"))))))

(define-public crate-vnd_siren-0.2.1 (c (n "vnd_siren") (v "0.2.1") (d (list (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07gm15iyb9i1k6xvv3zn153rhwzjhig5vnij8asjxy6vi5qzbprw") (f (quote (("http-compat" "http") ("default" "http-compat"))))))

