(define-module (crates-io vn st vnstat_parse) #:use-module (crates-io))

(define-public crate-vnstat_parse-0.1.0 (c (n "vnstat_parse") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1.16") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zzy695wz4zajvw8ig5h0jmc0kjs6y0300nsp2slb2wil9vl6cvm") (f (quote (("serde_support" "serde") ("miniserde_support" "miniserde") ("default"))))))

