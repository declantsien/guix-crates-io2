(define-module (crates-io vn db vndb_tags_get) #:use-module (crates-io))

(define-public crate-vndb_tags_get-0.1.0 (c (n "vndb_tags_get") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "136bfmichqssjvfvyky9m5xhgajcrs3jj32j2pxfq4npg5wz9kcw")))

(define-public crate-vndb_tags_get-1.0.0 (c (n "vndb_tags_get") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s0x6nsn79b55dz7260d56xvy10xhiii7crg0bl86cbqdjslzm24")))

(define-public crate-vndb_tags_get-1.1.0 (c (n "vndb_tags_get") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0smqw8sj78ywzy0rx8x6f4qazw7j6h15r0k1fy2gxcgw6npqrfgb")))

(define-public crate-vndb_tags_get-1.1.1 (c (n "vndb_tags_get") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x5h1kikqbg65rzlk081a38yhzg08vb3c474ikl4lk7lw5v4dc64")))

(define-public crate-vndb_tags_get-1.1.2 (c (n "vndb_tags_get") (v "1.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kk46qmaj33xgb1rv3nlclxpqmjknmg253cjgnd7f1ncxbk2l6vv")))

(define-public crate-vndb_tags_get-1.1.3 (c (n "vndb_tags_get") (v "1.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05qwxxq27hkbc8qn6sspag0375if7c3s51rsw2hp9gf7ln8xkhqm")))

(define-public crate-vndb_tags_get-1.1.4 (c (n "vndb_tags_get") (v "1.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02z1cw1pi4fs6ib22hvjmpn3873g9696q263py2ldm1fp3nszlyf")))

(define-public crate-vndb_tags_get-1.1.5 (c (n "vndb_tags_get") (v "1.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f890v96m28iprnfbdqcjyl3ccs47p788sj6qfmp7vp46p7h5vx5")))

(define-public crate-vndb_tags_get-1.1.6 (c (n "vndb_tags_get") (v "1.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dnksagihpyjcsyl3x6jbkhhid6v5x8qqhkrba04mbd4zi7h1c23")))

(define-public crate-vndb_tags_get-1.2.0 (c (n "vndb_tags_get") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m0wxlk5nhvr1j1nv2m570aq8k3hsvk8qqzpmpn3fb7mr2svxmm6")))

(define-public crate-vndb_tags_get-1.2.1 (c (n "vndb_tags_get") (v "1.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fr9r1ffm09ig3yhvfmrx8hy52mcbmnbmpbl4k2f276al9daa417")))

