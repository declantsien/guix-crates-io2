(define-module (crates-io vn db vndb_rs) #:use-module (crates-io))

(define-public crate-vndb_rs-0.1.0 (c (n "vndb_rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1hm2h688hccj0lm7q6wmsz4jyqhf5h17v6ddycs26pkl1hylakdw")))

(define-public crate-vndb_rs-0.1.1 (c (n "vndb_rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "0ncw72hjwl2iysvd0arzkg8y92dfz1rimdlwk0j1ys3v80wsg4xm")))

