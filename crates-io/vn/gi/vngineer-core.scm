(define-module (crates-io vn gi vngineer-core) #:use-module (crates-io))

(define-public crate-vngineer-core-1.0.0 (c (n "vngineer-core") (v "1.0.0") (d (list (d (n "intuicio-essentials") (r "^0.9") (f (quote ("vm"))) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "1sz0zv4827kzc045n2f7frhjhw4x6lbbhzqpmpp578a79v4ypypk") (f (quote (("plugins" "intuicio-essentials/plugins"))))))

(define-public crate-vngineer-core-1.0.1 (c (n "vngineer-core") (v "1.0.1") (d (list (d (n "intuicio-essentials") (r "^0.10") (f (quote ("vm"))) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "1pfzkvin5pd8a1wx613r80s5wi31bcwy61zhrhmsvmkqdnidigm5") (f (quote (("plugins" "intuicio-essentials/plugins"))))))

(define-public crate-vngineer-core-1.0.2 (c (n "vngineer-core") (v "1.0.2") (d (list (d (n "intuicio-essentials") (r "^0.12") (f (quote ("vm"))) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "1vml2jzbprzvsv5miharlkrdnjjkqxx7gfalmcapjsdlnwdfcqd9") (f (quote (("plugins" "intuicio-essentials/plugins"))))))

(define-public crate-vngineer-core-1.0.3 (c (n "vngineer-core") (v "1.0.3") (d (list (d (n "intuicio-essentials") (r "^0.13") (f (quote ("vm"))) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "08skqxgkg3sd426m1d0k4nf866dxkm68d9ic711xl4jw6bbq43wv") (f (quote (("plugins" "intuicio-essentials/plugins"))))))

