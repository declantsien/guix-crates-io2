(define-module (crates-io vn gi vngineer-simpleton) #:use-module (crates-io))

(define-public crate-vngineer-simpleton-1.0.0 (c (n "vngineer-simpleton") (v "1.0.0") (d (list (d (n "intuicio-essentials") (r "^0.9") (d #t) (k 0)) (d (n "intuicio-frontend-simpleton") (r "^0.9") (k 0)) (d (n "vngineer-core") (r "^1.0") (d #t) (k 0)))) (h "14zdi20khp5pwmijcyh15xhcxw32vrjprxwlr3bxqdyj18ljvf8r") (f (quote (("plugins" "intuicio-frontend-simpleton/plugins") ("fs" "intuicio-frontend-simpleton/fs") ("console" "intuicio-frontend-simpleton/console"))))))

(define-public crate-vngineer-simpleton-1.0.1 (c (n "vngineer-simpleton") (v "1.0.1") (d (list (d (n "intuicio-essentials") (r "^0.10") (d #t) (k 0)) (d (n "intuicio-frontend-simpleton") (r "^0.10") (k 0)) (d (n "vngineer-core") (r "^1.0") (d #t) (k 0)))) (h "02m2hgiq53assj4mi0jlzinjh9jdfi09xfy0d6q30xb266wcqsff") (f (quote (("plugins" "intuicio-frontend-simpleton/plugins") ("fs" "intuicio-frontend-simpleton/fs") ("console" "intuicio-frontend-simpleton/console"))))))

(define-public crate-vngineer-simpleton-1.0.2 (c (n "vngineer-simpleton") (v "1.0.2") (d (list (d (n "intuicio-essentials") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-frontend-simpleton") (r "^0.12") (k 0)) (d (n "vngineer-core") (r "^1.0") (d #t) (k 0)))) (h "1qb73v357wm0hciwma14js2isgl1q11af5ss64vysgj7hbnb6i8g") (f (quote (("plugins" "intuicio-frontend-simpleton/plugins") ("fs" "intuicio-frontend-simpleton/fs") ("console" "intuicio-frontend-simpleton/console"))))))

(define-public crate-vngineer-simpleton-1.0.3 (c (n "vngineer-simpleton") (v "1.0.3") (d (list (d (n "intuicio-essentials") (r "^0.13") (d #t) (k 0)) (d (n "intuicio-frontend-simpleton") (r "^0.13") (k 0)) (d (n "vngineer-core") (r "^1.0") (d #t) (k 0)))) (h "11qcrg6cs9xxly23qgxkggc5bhsay3aparzi7q0ffwa4f4n5i9ds") (f (quote (("plugins" "intuicio-frontend-simpleton/plugins") ("fs" "intuicio-frontend-simpleton/fs") ("console" "intuicio-frontend-simpleton/console"))))))

