(define-module (crates-io vn c- vnc-client) #:use-module (crates-io))

(define-public crate-vnc-client-1.0.0 (c (n "vnc-client") (v "1.0.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.13") (d #t) (k 0)) (d (n "vnc") (r "^0.4") (d #t) (k 0)) (d (n "x11") (r "^2.3") (d #t) (k 0)))) (h "10fk93ry0xc419v0f4h65g9b8v5sd18vg29m59zng18rj6g64nk7")))

