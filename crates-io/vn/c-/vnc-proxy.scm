(define-module (crates-io vn c- vnc-proxy) #:use-module (crates-io))

(define-public crate-vnc-proxy-1.0.0 (c (n "vnc-proxy") (v "1.0.0") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "vnc") (r "^0.4") (d #t) (k 0)))) (h "0dih848kyhybzc9sryypvb57k1c3k59rfvz4l65h2qb8vp6xcivm")))

