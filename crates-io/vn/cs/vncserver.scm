(define-module (crates-io vn cs vncserver) #:use-module (crates-io))

(define-public crate-vncserver-0.1.0 (c (n "vncserver") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gmw47klxdk77pn48vy9qlfnsi9q49348pryhrrc97nl1hlg8cg8")))

(define-public crate-vncserver-0.2.0 (c (n "vncserver") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1763phdpv4wsh4721pd8x82mnbdxy065layf5r1c401hxs3xz6kr")))

(define-public crate-vncserver-0.2.1 (c (n "vncserver") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ds5psdp3rb6rbnlb78pjy5sf2gq81vkvw0dy7bijdc9krzwsnbr")))

(define-public crate-vncserver-0.2.2 (c (n "vncserver") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12pgpz06qghqx1lwgw3k38gznycczrpyrk7imkpj9w9b4kfjiraa")))

