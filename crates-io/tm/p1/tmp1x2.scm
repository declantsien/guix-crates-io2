(define-module (crates-io tm p1 tmp1x2) #:use-module (crates-io))

(define-public crate-tmp1x2-0.1.0 (c (n "tmp1x2") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1d0rf9ybvbkh3ha8d9w8vis57s334f5qki8n5vxxpjrxcdmnd7hn")))

(define-public crate-tmp1x2-0.2.0 (c (n "tmp1x2") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1i70j52bq5g1xz78vjcyiq3m2x94cnq8fqnw0ywjp5bywa1b5aaa")))

(define-public crate-tmp1x2-0.2.1 (c (n "tmp1x2") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0lynykcvqbxfkrfbgnw1sfgydv81is2qnv0a8j9w574x85lhqgdl")))

(define-public crate-tmp1x2-1.0.0 (c (n "tmp1x2") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1x53xi4l08pv9b8xq7180m6xxp7rsrx7688rmz0ih72qfkhcgs5f")))

