(define-module (crates-io tm p1 tmp117) #:use-module (crates-io))

(define-public crate-tmp117-1.0.0 (c (n "tmp117") (v "1.0.0") (d (list (d (n "bilge") (r "^0.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "device-register") (r "^0.4") (d #t) (k 0)) (d (n "device-register-async") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "1nk43d9yh0z9np4li9sxlrh063albd6xf1v85czhykgr5511l95l") (r "1.75")))

