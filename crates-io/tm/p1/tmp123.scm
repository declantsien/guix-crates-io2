(define-module (crates-io tm p1 tmp123) #:use-module (crates-io))

(define-public crate-tmp123-0.1.1 (c (n "tmp123") (v "0.1.1") (h "19kbk0whm4hf0p2ylqi6v1ww56sng46fwp8z5890251si0595s24")))

(define-public crate-tmp123-0.2.0 (c (n "tmp123") (v "0.2.0") (h "0q10qgw2lina2l5sax2mhcvfd8v7q467j7amfwszxy3gxsh5zsq4")))

(define-public crate-tmp123-0.1.2 (c (n "tmp123") (v "0.1.2") (h "0sfvg47knlgbkdhkvcz1qkibvhdh0fhcsyg59npymda2j2hpa28n")))

