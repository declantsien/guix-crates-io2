(define-module (crates-io tm rs tmrs) #:use-module (crates-io))

(define-public crate-tmrs-0.1.0 (c (n "tmrs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "statistics") (r "^0.4.1") (d #t) (k 0)))) (h "00j7l2a2jsyl16p9qvpwjpfmvspvs95c27va9wfwcpj4w99kykbj") (y #t)))

(define-public crate-tmrs-0.1.1 (c (n "tmrs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "statistics") (r "^0.4.1") (d #t) (k 0)))) (h "0lk4fh0chgjhr476q3khmk8n1gylqa2g0zxk56cpk2hynsx6nk0r") (y #t)))

(define-public crate-tmrs-0.1.2 (c (n "tmrs") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "statistics") (r "^0.4.1") (d #t) (k 0)))) (h "1bj828qj85dk8a1362zzgqx229s2hs6dfrmsnlx634yk0v0vk489") (y #t)))

(define-public crate-tmrs-0.2.0 (c (n "tmrs") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "statistics") (r "^0.4.1") (d #t) (k 0)))) (h "1h1apdhg7vaqdxiz6yxz0j75n64lj1zq9pnhbxm8hdka1dqjmpra") (y #t)))

(define-public crate-tmrs-0.3.0 (c (n "tmrs") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "statistics") (r "^0.4.1") (d #t) (k 0)))) (h "1jsrfwdp5z2fbhsc91wplkihxnj4r7bcs5hla13v47mfffgd65kn")))

