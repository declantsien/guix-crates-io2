(define-module (crates-io tm c5 tmc5160) #:use-module (crates-io))

(define-public crate-tmc5160-0.0.1 (c (n "tmc5160") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1cmy8v8cjwb98xing8hy6nlmyhrw1wgav67x686brd07znyi59sz")))

