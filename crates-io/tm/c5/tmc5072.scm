(define-module (crates-io tm c5 tmc5072) #:use-module (crates-io))

(define-public crate-tmc5072-0.1.0 (c (n "tmc5072") (v "0.1.0") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i8x81ys9sva49cxxkrjmbiz1rsz6zysl1pacy6dkmq5pvs6v2ix")))

(define-public crate-tmc5072-0.1.1 (c (n "tmc5072") (v "0.1.1") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "089wfxabhv3y35d839yw9kjv32df7k5k0ya56y7g9nka0x77lh2l")))

