(define-module (crates-io tm x_ tmx_utils) #:use-module (crates-io))

(define-public crate-tmx_utils-0.1.0 (c (n "tmx_utils") (v "0.1.0") (d (list (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 0)))) (h "15hk904gd0824q8kdlpklxffsb0wcmil160pia296693ysryxfzw")))

(define-public crate-tmx_utils-0.1.1 (c (n "tmx_utils") (v "0.1.1") (d (list (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 0)))) (h "1kwmd548ck9nd5zvg46kz0hzh4m37j2bipfqsnvak2f02g0i02vw")))

(define-public crate-tmx_utils-0.1.2 (c (n "tmx_utils") (v "0.1.2") (d (list (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1wijqbw2sbc6j9b0581a0fcqbh3aykf6krq2i7k2ajxmn797b5rj")))

(define-public crate-tmx_utils-0.1.4 (c (n "tmx_utils") (v "0.1.4") (d (list (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "11fz3rmzxcdvwqfi4i4vhcwzy4dn8aryfvhbzxxbzp90ng3dyq5q")))

