(define-module (crates-io tm pl tmpl) #:use-module (crates-io))

(define-public crate-tmpl-0.1.0 (c (n "tmpl") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "clap") (r "^2.30") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tera") (r "^0.11.5") (d #t) (k 0)))) (h "1z0ak1wy7lh6vx9sj2svl2ry0kiir190njkjlddxyi2qbrdzgkbw")))

(define-public crate-tmpl-0.1.2 (c (n "tmpl") (v "0.1.2") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "18w32bplyhhahbra2nxl5ahba4wq7svv5k39bp92y1q0pa54jcxz")))

(define-public crate-tmpl-0.1.3 (c (n "tmpl") (v "0.1.3") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tera") (r "^1.0") (d #t) (k 0)))) (h "1r1464flbnralhaddz7lc9bs9xmb2b4ss6yqcg40q64f4c439mh9")))

(define-public crate-tmpl-0.2.0 (c (n "tmpl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1yzq0p1l6i5gd4ahyfdn6845x603f7r31fb4wmgfxkf1424abqsi")))

(define-public crate-tmpl-0.2.1 (c (n "tmpl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0cc6a2qdv3rvvaj0v05qjg46xlva85lsrlphz3vdlzcrkcwxd9ni")))

(define-public crate-tmpl-0.3.0 (c (n "tmpl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1xnz2ljnfvqp516mrq2yhqvy6ci79vfm4jf3nx48y155zp939lkx")))

