(define-module (crates-io tm le tmledkey-hal-drv) #:use-module (crates-io))

(define-public crate-tmledkey-hal-drv-0.0.1 (c (n "tmledkey-hal-drv") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "16v5islzfklq38psjaf4n5zk3drm38n09vbmjgynx4hq6jswp2hi") (y #t)))

(define-public crate-tmledkey-hal-drv-0.0.2 (c (n "tmledkey-hal-drv") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0p6c95hhwh58ihsm9divjrab2yg3yps06w43gg0xilscn6b99ciz")))

(define-public crate-tmledkey-hal-drv-0.1.0 (c (n "tmledkey-hal-drv") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1xkzbp4pzayclkjfbaf2ccik59bcdgxymnph2s28fvk4wlr6i9a2") (f (quote (("keys") ("galloc") ("fx" "galloc") ("demo" "clkdio" "clkdiostb" "keys" "fx") ("default" "clkdio" "clkdiostb") ("clkdiostb") ("clkdio"))))))

(define-public crate-tmledkey-hal-drv-0.1.1 (c (n "tmledkey-hal-drv") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0d6529zml6xkp1fplxm0n5a798dqsz9k7ygvnab53p3i7hlgpcgg") (f (quote (("keys") ("galloc") ("fx" "galloc") ("demo" "clkdio" "clkdiostb" "keys" "fx") ("default" "clkdio" "clkdiostb") ("clkdiostb") ("clkdio"))))))

