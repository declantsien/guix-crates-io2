(define-module (crates-io tm q- tmq-zmq-sys) #:use-module (crates-io))

(define-public crate-tmq-zmq-sys-0.9.0 (c (n "tmq-zmq-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1m0xhia6zj89ifinbambccq5yjs8xcnlgxwif9qlnnmz4bzyv50z") (l "zmq")))

