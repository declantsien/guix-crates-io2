(define-module (crates-io tm ac tmac) #:use-module (crates-io))

(define-public crate-tmac-0.1.0 (c (n "tmac") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1a13jvgif2jxr3fcfch99lf3anp459v1ii2hfcbxxf5mv6c3w5zc")))

(define-public crate-tmac-0.1.1 (c (n "tmac") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1l5l7pdi4m3iqqgx2sn7wscr1dyn41nny3rm3ylbdr3r93jq9hn9")))

(define-public crate-tmac-0.1.2 (c (n "tmac") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0cjxn4xwpi4zkhq8z0frhc4zn0ay8x8spir88cy1rh9qibd547aa")))

(define-public crate-tmac-0.1.3 (c (n "tmac") (v "0.1.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0mlvldhykazqipn6p5r9ys9q71xnlqqxmzh6wvvrhsj4p0wrvw59")))

