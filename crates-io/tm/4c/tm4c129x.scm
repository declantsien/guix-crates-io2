(define-module (crates-io tm #{4c}# tm4c129x) #:use-module (crates-io))

(define-public crate-tm4c129x-0.1.0 (c (n "tm4c129x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "10x5m6p9vh062f6jm0m9mw004nk3gigy880986ik1vf3ldj6j5y7")))

(define-public crate-tm4c129x-0.2.0 (c (n "tm4c129x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0sxkcmidq29qccqdxhjc9rzpw18nrxs1mckmqi17yz3fg1ab4fr3")))

(define-public crate-tm4c129x-0.3.0 (c (n "tm4c129x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "05k39kzzqqzi4f0w0dfj6vvqhywd16f8m58dsv57q11ssl61bc3a")))

(define-public crate-tm4c129x-0.4.0 (c (n "tm4c129x") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "19qqb3llhnx39hgkmzs9z2gjf8c1nr8d3kjpdj82s3z143rp6rhk")))

(define-public crate-tm4c129x-0.5.0 (c (n "tm4c129x") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1cbvz1g24842qy8d3rbap270nj524p3gd77b973fa6h3yvx2xyn8") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-tm4c129x-0.6.0 (c (n "tm4c129x") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "152y84afhzzmkc03d38iwp7xnrp8lay6aajvqk9vasg12m62nxmv") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-tm4c129x-0.6.1 (c (n "tm4c129x") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "075cjxbnkjvylqxi8b4w9095vpci3j0pnra6wfi229i58w4i35f8") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-tm4c129x-0.7.0 (c (n "tm4c129x") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0qiy2fw609kk6ji571gj840s4x5n9rvrkm3w0qbh8s7pxxakp6lh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c129x-0.8.0 (c (n "tm4c129x") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "097nmdkjyhz0y65n0bwg464kgdg18drs65s57pzrzp86xpa0whvx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c129x-0.9.0 (c (n "tm4c129x") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0793hs2ilcclkv3c6crkr1x9xgq6d51clpxyziki39cz6cxkpvs1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c129x-0.9.1 (c (n "tm4c129x") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "057zzj82ciqlv7sgpvfvbq53p25bzbqjyrdh7f0bryv0zl3n8bmq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c129x-0.9.2 (c (n "tm4c129x") (v "0.9.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "10n1s98z3sncwrdbq29q572b4rk9kpi3wjwzirjgm56acdkigcas") (f (quote (("rt" "cortex-m-rt/device"))))))

