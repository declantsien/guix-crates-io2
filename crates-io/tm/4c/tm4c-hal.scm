(define-module (crates-io tm #{4c}# tm4c-hal) #:use-module (crates-io))

(define-public crate-tm4c-hal-0.1.0 (c (n "tm4c-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0cr1cwd6zqvcqlgr6pzpqygr9hcrswx1gcncw6hpssx4srw42h71")))

(define-public crate-tm4c-hal-0.2.0 (c (n "tm4c-hal") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1qh6g527mksajnpp25pxw7vdkmzhilm59kfvglfb1cdxgln50fch")))

(define-public crate-tm4c-hal-0.2.1 (c (n "tm4c-hal") (v "0.2.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0jcypkn7pk9xn1v0r584anrizzlqfhiffha9b3na104as7l1cpmg")))

(define-public crate-tm4c-hal-0.2.2 (c (n "tm4c-hal") (v "0.2.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1xiq41wh1gil3mhck625pqp6k7kvf7am2kn7bwfjfai2i0imzvg5")))

(define-public crate-tm4c-hal-0.2.3 (c (n "tm4c-hal") (v "0.2.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1kvcxm4qvs9s2jcdf28xkbaxx62xlq24nxdfnvkxklmvifgi0a1v")))

(define-public crate-tm4c-hal-0.3.0 (c (n "tm4c-hal") (v "0.3.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0k3kk2nmln6rpik4csrcv3qvpsn1kpj6zrzpfajhw24nhwv6bgam")))

(define-public crate-tm4c-hal-0.4.0 (c (n "tm4c-hal") (v "0.4.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)))) (h "0ir3mpxn9afn1489k78jbppzwnzxdb8k88x17hf44598zl1zrw2a")))

(define-public crate-tm4c-hal-0.4.1 (c (n "tm4c-hal") (v "0.4.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "16wfx968ikl05fminls381mkzji1cr7fdhl01nk0rzy1593mz04g")))

(define-public crate-tm4c-hal-0.4.2 (c (n "tm4c-hal") (v "0.4.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "13qgqwxkr1j6xh9v2v9kag9rp4is2rp9f2byq776mk6gijwhqrmm")))

