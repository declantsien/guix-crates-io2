(define-module (crates-io tm #{4c}# tm4c123x) #:use-module (crates-io))

(define-public crate-tm4c123x-0.2.0 (c (n "tm4c123x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "164h8g7xkpfi63fxf0h2hqg7hc9m1s0h1ax11v1vdpl1shq1il2g")))

(define-public crate-tm4c123x-0.3.0 (c (n "tm4c123x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "11h4hv2y85j3gra7lyx0w97l1dzwbybfk9x68p5ki8hdslf1nm6h")))

(define-public crate-tm4c123x-0.4.0 (c (n "tm4c123x") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1qpwcq3qgdmm0bl4508778hmp6nhlpnn8janp4cgh3i2mja1f459")))

(define-public crate-tm4c123x-0.6.0 (c (n "tm4c123x") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "10fg3419kff4k0rihvnnwdnv3qa1ky7gplsb73g0fqzpsc086sxm") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-tm4c123x-0.6.1 (c (n "tm4c123x") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1amhwds795xx7yl6z8gzv6v7a3zhik5c9z97gssh1imgm0zmwkf3") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-tm4c123x-0.7.0 (c (n "tm4c123x") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gy9sm33cx17h7vxms5m0596grl65y691yqgmnnw1gkkajk18qaz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c123x-0.8.0 (c (n "tm4c123x") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0izpy4z2x20dr09rh3azxwkmdl2g2cm7vpqky8dkj41sccz0mb6z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c123x-0.9.0 (c (n "tm4c123x") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0grybjhwrbh7n62n97jiy7apy19n5dl9ql49s6dmvwk9rih7yzh9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c123x-0.9.1 (c (n "tm4c123x") (v "0.9.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1xp5yigykzzhyd0dmbwprmih2970yangv2sshfkxdy4glai7a4sb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tm4c123x-0.9.2 (c (n "tm4c123x") (v "0.9.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1b9am0bj1c3j8yycj54sw55bzcchdxh6nnx8lymv96hwkn8a7p7v") (f (quote (("rt" "cortex-m-rt/device"))))))

