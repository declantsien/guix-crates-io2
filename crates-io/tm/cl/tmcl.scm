(define-module (crates-io tm cl tmcl) #:use-module (crates-io))

(define-public crate-tmcl-0.1.0-alpha0 (c (n "tmcl") (v "0.1.0-alpha0") (d (list (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "03skbvi342cqg9qkw6v4lrdrqfpk9hwzccanwl31a3cxvj2j3sdv") (f (quote (("std"))))))

(define-public crate-tmcl-0.1.0-alpha1 (c (n "tmcl") (v "0.1.0-alpha1") (d (list (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "09aab5fvsfxk8gac74f3852r6f2nyvdnj7m1rgnbna2wy0dll0cd") (f (quote (("std"))))))

(define-public crate-tmcl-0.1.0-alpha2 (c (n "tmcl") (v "0.1.0-alpha2") (d (list (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "03rb7vzdqn5d17nnxmhdwvxzk04jzdbwrkd4yccwic3isnw0687w") (f (quote (("std"))))))

(define-public crate-tmcl-0.1.0-alpha3 (c (n "tmcl") (v "0.1.0-alpha3") (d (list (d (n "interior_mut") (r "^0.1.0-beta0") (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0yxvhb7czhimbxrh9w11ijrwkvlbci35gv6aqhyrdkdyqyrp3rxh") (f (quote (("std" "interior_mut/std"))))))

(define-public crate-tmcl-0.1.0-alpha4 (c (n "tmcl") (v "0.1.0-alpha4") (d (list (d (n "interior_mut") (r "^0.1.0-beta0") (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0391wwg3bxyqb9w3849fm9vnjcbvqwfz7fzcy3fpc5vbngbqa84d") (f (quote (("std" "interior_mut/std"))))))

(define-public crate-tmcl-0.1.0-beta0 (c (n "tmcl") (v "0.1.0-beta0") (d (list (d (n "interior_mut") (r "^0.1") (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0a4hp1z5i74bixdayd9db7crmhxmja16wq1f5jv6qas34d1b40wj") (f (quote (("std" "interior_mut/std"))))))

