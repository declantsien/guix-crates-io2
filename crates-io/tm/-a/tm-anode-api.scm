(define-module (crates-io tm -a tm-anode-api) #:use-module (crates-io))

(define-public crate-tm-anode-api-0.1.0 (c (n "tm-anode-api") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.9.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter") (r "=0.19.3") (d #t) (k 0)))) (h "09wqf4q2b8z7qs39gf5zr2r0n7dahjj3fiapa0vp5g71x1hgghki")))

(define-public crate-tm-anode-api-0.2.0 (c (n "tm-anode-api") (v "0.2.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.9.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.2.0") (d #t) (k 0)) (d (n "tree-sitter") (r "=0.19.3") (d #t) (k 0)))) (h "086ybakpfv3m3fh2qirx9282065shisipj0vzj8wm4ai12dj328p")))

(define-public crate-tm-anode-api-0.3.0 (c (n "tm-anode-api") (v "0.3.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.9.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.2.0") (d #t) (k 0)))) (h "0as4ggc44ijkipyjqxh77w5jqfgcii4qwv3sjdsk80is5piz8szg")))

(define-public crate-tm-anode-api-0.4.0 (c (n "tm-anode-api") (v "0.4.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.10.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)))) (h "1hnkkb9w2hyik5j0rpw0gj83a1n86avqnjddk9p846gcypc8nqpl")))

(define-public crate-tm-anode-api-0.5.0 (c (n "tm-anode-api") (v "0.5.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.10.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter") (r "=0.19.3") (d #t) (k 0)))) (h "1sb2w7v2ib91yb0f5dxaykr9wigs7ln788lg8h2mrz8r0y83ls5b")))

(define-public crate-tm-anode-api-0.5.1 (c (n "tm-anode-api") (v "0.5.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.10.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.3.0") (d #t) (k 0)) (d (n "tree-sitter") (r "=0.19.3") (d #t) (k 0)))) (h "0kryz51wip20ggkzqpzcd8njdyhn70zil8irzwnkxs6kc5i3ajg0")))

(define-public crate-tm-anode-api-0.6.0 (c (n "tm-anode-api") (v "0.6.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "machinery") (r "^0.12.0") (d #t) (k 0)) (d (n "machinery-api") (r "^0.5.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.0") (d #t) (k 0)))) (h "191f3bdzmjgmmd6chjwv9qg2ay5kf2yfcssg7gijf9kjh4y43isp")))

