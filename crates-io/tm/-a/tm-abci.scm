(define-module (crates-io tm -a tm-abci) #:use-module (crates-io))

(define-public crate-tm-abci-0.1.0 (c (n "tm-abci") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "tm-protos") (r "^0.1.0") (d #t) (k 0)))) (h "0pvym31flv908kmsp2ap5rs6k1gk7dz66rraizl1nr82qyr5hq81")))

(define-public crate-tm-abci-0.1.1 (c (n "tm-abci") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "tm-protos") (r "^0.1.0") (d #t) (k 0)))) (h "16w0c3rk16dgyxjzhk3nba4z6g69mc0kx4lknbcdqizv0lcqi8cw")))

(define-public crate-tm-abci-0.1.2 (c (n "tm-abci") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "tm-protos") (r "^0.1") (d #t) (k 0)))) (h "0np1x08xk7cr9laz2mmjjrlh9qfn30ml0s8q8phagi4g155x1f7b")))

