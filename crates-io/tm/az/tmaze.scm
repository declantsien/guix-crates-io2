(define-module (crates-io tm az tmaze) #:use-module (crates-io))

(define-public crate-tmaze-1.11.0 (c (n "tmaze") (v "1.11.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1sgin5yi25fm6ivaa4ryk6w4lpip8wzfbjzl2zq4kmvcx9kkwa5c") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.12.0 (c (n "tmaze") (v "1.12.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0sxsj1ir9j33jjhdifqjyyp8n50rhs238bc4viy6h5flrpdlscfj") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.12.1 (c (n "tmaze") (v "1.12.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0cldymbj6qragyfx1g6rl7kxpv15nlfiqfcxzn4g70lhrar7rhh8") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.12.2 (c (n "tmaze") (v "1.12.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0z2n3yziwv1cfl0lgmb36lajw3nwi67lm0ya3dhjs3mbn0kj21cn") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.12.3 (c (n "tmaze") (v "1.12.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0851sslnj6hkmzbyxwr4cgsn3zyq2vjf04p91myis73fzsi8fv8i") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.12.4 (c (n "tmaze") (v "1.12.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "16903882cazidmrs3x63dn6k8zx7yk3jbr6jzf7dlacllfy6jwif") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown"))))))

(define-public crate-tmaze-1.13.0 (c (n "tmaze") (v "1.13.0") (d (list (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1xa96qxnbwdfk59k75pwg3rs2azpag95p3i7n8ny3zsv2m7pdys8") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio"))))))

(define-public crate-tmaze-1.14.2 (c (n "tmaze") (v "1.14.2") (d (list (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "flacenc") (r "^0.3.1") (d #t) (k 1)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "mp3lame-encoder") (r "^0.1.4") (d #t) (k 1)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("wav" "mp3"))) (o #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)) (d (n "wav") (r "^1.0.0") (d #t) (k 1)))) (h "0d8m56vlw7iq6zkli99abgshfx2ra4kk46imzl57bbifpicw53fk") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown" "updates" "sound")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio") ("sound" "dep:rodio"))))))

(define-public crate-tmaze-1.14.3 (c (n "tmaze") (v "1.14.3") (d (list (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "flacenc") (r "^0.3.1") (d #t) (k 1)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "mp3lame-encoder") (r "^0.1.4") (d #t) (k 1)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("wav" "mp3"))) (o #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)) (d (n "wav") (r "^1.0.0") (d #t) (k 1)))) (h "1xlm1mlm5nkbd36sj0ygw1sg032w4c0bqqym1ci3x583fphykj38") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown" "updates" "sound")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio") ("sound" "dep:rodio"))))))

(define-public crate-tmaze-1.14.4 (c (n "tmaze") (v "1.14.4") (d (list (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.6.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.1") (f (quote ("rustls"))) (o #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "flacenc") (r "^0.3.1") (d #t) (k 1)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "mp3lame-encoder") (r "^0.1.4") (d #t) (k 1)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("wav" "mp3"))) (o #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)) (d (n "wav") (r "^1.0.0") (d #t) (k 1)))) (h "1sn0840hmb9qapv51s80nlvc49mkcqi8xc1aqmjnjhkqmd9ydrcz") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown" "updates" "sound")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio") ("sound" "dep:rodio"))))))

(define-public crate-tmaze-1.15.0 (c (n "tmaze") (v "1.15.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.7.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.11.0") (f (quote ("rustls"))) (o #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "flacenc") (r "^0.3.1") (d #t) (k 1)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mp3lame-encoder") (r "^0.1.4") (d #t) (k 1)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.18.1") (f (quote ("wav" "mp3"))) (o #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)) (d (n "wav") (r "^1.0.0") (d #t) (k 1)))) (h "1fkb9rwrdvck69b49xrvyp383q6axwvw67f3d0lfnp2lw4qw4jxa") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown" "updates" "sound")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio") ("sound" "dep:rodio"))))))

(define-public crate-tmaze-1.15.1 (c (n "tmaze") (v "1.15.1") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmaze") (r "^0.7.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.11.0") (f (quote ("rustls"))) (o #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "flacenc") (r "^0.3.1") (d #t) (k 1)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mp3lame-encoder") (r "^0.1.4") (d #t) (k 1)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "rodio") (r "^0.18.1") (f (quote ("wav" "mp3"))) (o #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)) (d (n "wav") (r "^1.0.0") (d #t) (k 1)))) (h "0nmjzkqbzjaj3kva814svp7sdqd7sxkvzhsl2dmrqpq17nrjdbxn") (f (quote (("hashbrown" "cmaze/hashbrown") ("default" "hashbrown" "updates" "sound")))) (s 2) (e (quote (("updates" "dep:crates_io_api" "dep:semver" "dep:tokio") ("sound" "dep:rodio"))))))

