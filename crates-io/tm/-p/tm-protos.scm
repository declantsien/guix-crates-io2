(define-module (crates-io tm -p tm-protos) #:use-module (crates-io))

(define-public crate-tm-protos-0.1.0 (c (n "tm-protos") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "prost") (r "^0.8") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-types") (r "^0.8") (k 0)))) (h "0z0iqd50mw8ij3yvqqhd47qigfsqvakgma9aizr6hsgpn1173ivq")))

(define-public crate-tm-protos-0.1.1 (c (n "tm-protos") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "prost") (r "^0.10") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-build") (r "^0.10.4") (d #t) (k 1)) (d (n "prost-types") (r "^0.10") (k 0)))) (h "0b6lk0yx73pn3l58383k78j65q78dhr26q81krc4iyx4jrh607gh")))

(define-public crate-tm-protos-0.1.2 (c (n "tm-protos") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (k 0)))) (h "00my72w0lzccm1jb5mwzfpkkfm574spi2fcl5c3d369zg20lsmm7")))

(define-public crate-tm-protos-0.1.3 (c (n "tm-protos") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "prost") (r "^0.11") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-types") (r "^0.11") (k 0)))) (h "1fvnf5i1ai5jqsx6amblfzay8x88zhp6qjn1vrhlczbi7wlbmijq")))

