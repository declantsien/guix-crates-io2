(define-module (crates-io tm db tmdb) #:use-module (crates-io))

(define-public crate-tmdb-0.1.0 (c (n "tmdb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1fkm9nsp8hyv6dwn9cwvhq9js4c5byw1klph815psg7qmbnkm6w8")))

(define-public crate-tmdb-0.1.1 (c (n "tmdb") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1cfifsllq8zx95hnr9p47lfjcnhhk7qdzivvrv9kyxgq0g4v44dq")))

(define-public crate-tmdb-1.0.0 (c (n "tmdb") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1bc7c3dnbiihcil6cg7k2v896iwrayjgy76dsb7f9ind05h3awfa")))

(define-public crate-tmdb-1.0.1 (c (n "tmdb") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "1b89z67y3xr647jrm8gsha3lj7yl5kwg2brn3agnwx0kvw4v7scm")))

(define-public crate-tmdb-1.1.0 (c (n "tmdb") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "1bg1mncx4nnrlj7awq144m3qg91wa4j74q59hykgk76wg1gq34ig")))

(define-public crate-tmdb-2.0.0 (c (n "tmdb") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "132h2pydqwznsmyxgmim0hy62325y2y2wxdjv7gww8b698rhbkl5")))

(define-public crate-tmdb-3.0.0 (c (n "tmdb") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)))) (h "1xwlhlp99myfslkwnqsnvk05a3h5kyzpjvbr2qdnk4d3q18k40ca")))

