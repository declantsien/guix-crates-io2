(define-module (crates-io tm db tmdb-cli) #:use-module (crates-io))

(define-public crate-tmdb-cli-0.1.0 (c (n "tmdb-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syncwrap") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0scm9dh8c03xs2r9sy77j1y80q3irfgg8fwwdnqh2iiv4b50v6rr") (f (quote (("sync"))))))

