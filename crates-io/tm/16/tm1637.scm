(define-module (crates-io tm #{16}# tm1637) #:use-module (crates-io))

(define-public crate-tm1637-0.0.1 (c (n "tm1637") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1g8z3lzv43zw3byjxag2xnpr0rwv59n3js4izwqkdpadpb8ln34y")))

(define-public crate-tm1637-0.1.0 (c (n "tm1637") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "049lkk11iyqjmy2ibkvxc5y3828xfpki175lisfdf7c2d4m32n1d")))

