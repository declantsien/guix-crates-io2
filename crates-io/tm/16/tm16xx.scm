(define-module (crates-io tm #{16}# tm16xx) #:use-module (crates-io))

(define-public crate-tm16xx-0.1.0 (c (n "tm16xx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "08wrlds0ff3z9vl4c6vcmwn1ys0f8jyk49zdiiq7lzm21fyq49zr") (f (quote (("tm1628") ("default"))))))

(define-public crate-tm16xx-0.1.1 (c (n "tm16xx") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1vim43h618v4xqgs1v7cc9c06rai10r1c795nn85da366szmx78d") (f (quote (("tm1628") ("default"))))))

(define-public crate-tm16xx-0.1.2 (c (n "tm16xx") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "05694rv80x4awpb9zcd91xs5bsx45bgqf6xwafki60bk23j7nan4") (f (quote (("tm1628") ("default"))))))

(define-public crate-tm16xx-0.1.3 (c (n "tm16xx") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "180d6kq905684a7hmc4ndbjchigf7jwil9hggrc0svd8jrxrvk7c") (f (quote (("tm1628") ("default"))))))

