(define-module (crates-io tm #{16}# tm1637-gpio-driver) #:use-module (crates-io))

(define-public crate-tm1637-gpio-driver-1.0.0 (c (n "tm1637-gpio-driver") (v "1.0.0") (h "17yasanwm8swzxhby3r9jcr51h29p41mpdl92nr6xg0grb1grd1h")))

(define-public crate-tm1637-gpio-driver-1.1.0 (c (n "tm1637-gpio-driver") (v "1.1.0") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0qiqpfsar83azn7ha41v5d6hqy775ygh78nqii3zbl3pncq27rfd") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("default" "fourdigit7segdis"))))))

(define-public crate-tm1637-gpio-driver-1.1.1 (c (n "tm1637-gpio-driver") (v "1.1.1") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1bzry070g3i9pv7x34iy8wf1i1y2wdmlarq024jsagcglbk7bbpi") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("default" "fourdigit7segdis"))))))

(define-public crate-tm1637-gpio-driver-1.1.2 (c (n "tm1637-gpio-driver") (v "1.1.2") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "12dkdrah1mxp4n6ka2r5v37dd7rv437458lliqiv2j0mz3cg7xp4") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("default" "fourdigit7segdis"))))))

(define-public crate-tm1637-gpio-driver-1.1.3 (c (n "tm1637-gpio-driver") (v "1.1.3") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0s4ynwm40i4sh3yhx5700s3m70kap4jxi5gzxkp5vdjn4zaf669i") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("default" "fourdigit7segdis"))))))

(define-public crate-tm1637-gpio-driver-1.1.4 (c (n "tm1637-gpio-driver") (v "1.1.4") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "187mrw45kwr2j5mjdifkvggnz8yrl3dq3cciyq255ahv9f4rgg98") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("default" "fourdigit7segdis"))))))

(define-public crate-tm1637-gpio-driver-1.1.5 (c (n "tm1637-gpio-driver") (v "1.1.5") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "01s9lnrmbg8f62mjzi00hrj2295nid0l0y5ncryfc70hbrkwvixk") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-1.1.6 (c (n "tm1637-gpio-driver") (v "1.1.6") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0kwsd2lp7ng9139lbm34kil2rg9g1vhbzdbfx8ksz1b7ly6yjgns") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-1.2.0 (c (n "tm1637-gpio-driver") (v "1.2.0") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0iva9l5f1db83gz52ycrsbrr6ar31v0hd48nr7i9333qaygjy4r5") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-1.2.1 (c (n "tm1637-gpio-driver") (v "1.2.1") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "19facxhm6qgp1zs5pbjzxssdv2xcf1wzp856jqfbqnwz6v5bgi61") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-1.2.2 (c (n "tm1637-gpio-driver") (v "1.2.2") (d (list (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1x3fa1wnbgb7bx1pk0n8044qx4zwkfnfmq5n89394p1m79rzpbci") (f (quote (("gpio-api-wiringpi" "wiringpi") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-2.0.0 (c (n "tm1637-gpio-driver") (v "2.0.0") (d (list (d (n "gpio") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0ml7ckgml0s3wkw4kpj8kpiv274gqaz01mlbq22n3qzqxr5s23kk") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-2.0.1 (c (n "tm1637-gpio-driver") (v "2.0.1") (d (list (d (n "gpio") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "14q852zg7h3xfln945hwrjkzfmbv6vvbzsqzwvypmmjsfmydmgh6") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy"))))))

(define-public crate-tm1637-gpio-driver-2.0.2 (c (n "tm1637-gpio-driver") (v "2.0.2") (d (list (d (n "gpio") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1gd212n2ywlwlwxi0341s3km0hbj618fr470iv7hm67jnv42hdj9") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy" "gpio-api-gpio_cdev"))))))

(define-public crate-tm1637-gpio-driver-2.0.3 (c (n "tm1637-gpio-driver") (v "2.0.3") (d (list (d (n "gpio") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0jqp9zafk52nlfifrjpmd7vswhziz52k6hx12a7sp61h35xpk81c") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy" "gpio-api-gpio_cdev")))) (y #t)))

(define-public crate-tm1637-gpio-driver-2.0.4 (c (n "tm1637-gpio-driver") (v "2.0.4") (d (list (d (n "gpio") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "13yydgwzpcpnmqbjaxl14xf2rnjnfx4n50z9a0i5iakyvqvhnici") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy" "gpio-api-gpio_cdev"))))))

(define-public crate-tm1637-gpio-driver-2.0.5 (c (n "tm1637-gpio-driver") (v "2.0.5") (d (list (d (n "gpio") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "05nn62wl651n6xg03yp0pzzh22rjn6z5mfb6m3xhn2sw272bga0a") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy" "gpio-api-gpio_cdev"))))))

(define-public crate-tm1637-gpio-driver-2.0.6 (c (n "tm1637-gpio-driver") (v "2.0.6") (d (list (d (n "gpio") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "08m1adp0v0z4jzs1yj0bmgdqv6nf6bx6xvpicvfkznab15m9ygbw") (f (quote (("gpio-api-wiringpi" "wiringpi") ("gpio-api-sysfs_gpio" "sysfs_gpio") ("gpio-api-gpio_cdev" "gpio-cdev") ("gpio-api-gpio" "gpio") ("fourdigit7segdis") ("dummy") ("default" "fourdigit7segdis" "dummy" "gpio-api-gpio_cdev"))))))

