(define-module (crates-io tm #{16}# tm1637-embedded-hal) #:use-module (crates-io))

(define-public crate-tm1637-embedded-hal-0.1.0 (c (n "tm1637-embedded-hal") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "duplicate") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "00x4xq2hi357121ydbjy5ahp8h1nd4dl5k6hzrpj620z0b8lmzd8") (f (quote (("mappings") ("impl-debug") ("disable-checks") ("demo" "mappings") ("default" "async" "impl-debug")))) (s 2) (e (quote (("impl-defmt-format" "dep:defmt") ("blocking" "dep:embedded-hal") ("async" "dep:embedded-hal" "dep:embedded-hal-async"))))))

