(define-module (crates-io tm an tman) #:use-module (crates-io))

(define-public crate-tman-1.0.0 (c (n "tman") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1hxcwykajfvz6hcvfpll0i04p0qllck61a6c59922vhc3mkq8ap8")))

