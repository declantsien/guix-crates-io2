(define-module (crates-io tm fl tmflib-derive) #:use-module (crates-io))

(define-public crate-tmflib-derive-0.1.13 (c (n "tmflib-derive") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0zidhlhfljshd775y5hzdx7wwfx9gnydqmsch2v8q81l58cvwb0n") (y #t)))

(define-public crate-tmflib-derive-0.1.14 (c (n "tmflib-derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "173j8rm7cn792n1kkb2243s6vc1ggwgmkn0zqifrsbgxnb3zmc4q")))

(define-public crate-tmflib-derive-0.1.15 (c (n "tmflib-derive") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "01wi6g5sfq3l3lkp9ss84pljmm6ms9f9yh2b8n91293rkd6c2k9h")))

(define-public crate-tmflib-derive-0.1.16 (c (n "tmflib-derive") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "133j75lk507gsib5171lidy3adw15yg7xgy29w1bsbkfaq78hhp6")))

