(define-module (crates-io tm oe tmoe) #:use-module (crates-io))

(define-public crate-tmoe-0.0.1 (c (n "tmoe") (v "0.0.1") (d (list (d (n "cursive") (r "^0.16.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g1zrap1aag0s7lpb2lbpgb2d6pmghddd8rs2ws61bqqwdzfq59b") (y #t)))

