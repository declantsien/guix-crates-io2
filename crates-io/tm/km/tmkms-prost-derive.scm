(define-module (crates-io tm km tmkms-prost-derive) #:use-module (crates-io))

(define-public crate-tmkms-prost-derive-0.0.1 (c (n "tmkms-prost-derive") (v "0.0.1") (d (list (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1l9aj14llnzjr565r2dx7p81njrdhfys73n5sz6xfm8y1ffv28i9") (y #t)))

