(define-module (crates-io tm -d tm-derive) #:use-module (crates-io))

(define-public crate-tm-derive-2020.11.0 (c (n "tm-derive") (v "2020.11.0") (h "05hbmpbl5xxykx6vjbampr9bjcwlizbbns25jrjjmcjmwlcxl41n")))

(define-public crate-tm-derive-2020.11.1 (c (n "tm-derive") (v "2020.11.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vwwpsgskfy097gxi6mab9ll17kk54qn3xj5w8hm32dvdvfpqg03")))

