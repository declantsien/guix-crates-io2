(define-module (crates-io tm pt tmptoml) #:use-module (crates-io))

(define-public crate-tmptoml-0.1.0 (c (n "tmptoml") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jlgd7h68mxc47fkfl9ildw6r81g1w052mcyijj3ksrqxhw77imf")))

(define-public crate-tmptoml-0.1.1 (c (n "tmptoml") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1d2hqsgp2qhz3j97ddbgpmisbh9bjv7qd99jgkxi79ry7j490haw")))

(define-public crate-tmptoml-0.1.2 (c (n "tmptoml") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0a1fwq3qz6c2m1zaz9yk9pkq3c1ykjygr3zbz1r9m4132sqj489b")))

(define-public crate-tmptoml-0.1.3 (c (n "tmptoml") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hf2y9zfjs49mhgqx96l5ww0zygbsf5q2rfbpzja7z65w02pdrng")))

