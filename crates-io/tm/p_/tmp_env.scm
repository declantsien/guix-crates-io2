(define-module (crates-io tm p_ tmp_env) #:use-module (crates-io))

(define-public crate-tmp_env-0.1.0 (c (n "tmp_env") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "08ikliqw6fifx9kak3686i2cjzxp37x3mcjbpmw354am7f01p73x")))

(define-public crate-tmp_env-0.1.1 (c (n "tmp_env") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0hv8fsv4aygmg3772i9npzj56v1dmnl71sm8d854yg4clbjvjvm5")))

