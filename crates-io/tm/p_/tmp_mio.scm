(define-module (crates-io tm p_ tmp_mio) #:use-module (crates-io))

(define-public crate-tmp_mio-0.5.2 (c (n "tmp_mio") (v "0.5.2") (d (list (d (n "bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "miow") (r "^0.1.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.19") (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)) (d (n "slab") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "^0.1.33") (d #t) (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (k 0)))) (h "06bc7wp4g3nnglq1mdja4sjxlabx8fpb83cq4hzg25ab0av7dkli")))

