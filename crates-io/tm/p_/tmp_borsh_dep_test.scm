(define-module (crates-io tm p_ tmp_borsh_dep_test) #:use-module (crates-io))

(define-public crate-tmp_borsh_dep_test-0.1.0 (c (n "tmp_borsh_dep_test") (v "0.1.0") (d (list (d (n "borsh") (r ">=1.0.0, <1.1.0") (f (quote ("unstable__schema" "derive"))) (d #t) (k 0)))) (h "1m4psdy4da6r7jdiaas1ci9af047881cqqk0qbqgx8xkdpk8sgzb") (y #t)))

