(define-module (crates-io tm ag tmag5170) #:use-module (crates-io))

(define-public crate-tmag5170-0.1.0 (c (n "tmag5170") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "crc_all") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "panic-rtt-target") (r "^0.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 2)))) (h "17lw90f53sa22fmaq1bh6n0knr3gzmfvhqyz8jksp92c2lyn57kd")))

(define-public crate-tmag5170-0.1.1 (c (n "tmag5170") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "crc_all") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "panic-rtt-target") (r "^0.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 2)))) (h "01xbnss3ny34dmk0kd4wjr3a2bj0xqjf2k7s08pdajcyl82j79z0")))

