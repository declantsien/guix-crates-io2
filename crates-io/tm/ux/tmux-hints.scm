(define-module (crates-io tm ux tmux-hints) #:use-module (crates-io))

(define-public crate-tmux-hints-0.1.0 (c (n "tmux-hints") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0ad0bfd8la2l981pz2r3p6r34m2wmpz60j9929ji9rnakrlgnl09")))

(define-public crate-tmux-hints-0.1.1 (c (n "tmux-hints") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1lwf7lz4lyak3mjdc1sxkabg6lf4b5n7p8vmpj969638gv99dsn3")))

