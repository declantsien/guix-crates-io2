(define-module (crates-io tm ux tmux-time-tracker) #:use-module (crates-io))

(define-public crate-tmux-time-tracker-0.1.0 (c (n "tmux-time-tracker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("sqlite" "runtime-tokio-rustls" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06iknyjkxbmzwadsfp2m3x42sn3k491nwmxdh918xrskqk5b1zyi")))

(define-public crate-tmux-time-tracker-0.1.1 (c (n "tmux-time-tracker") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("sqlite" "runtime-tokio-rustls" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2acmad0ifh7y8d8qazgrvf05vfyyw7003hv4l3qbpnignyhsza")))

(define-public crate-tmux-time-tracker-0.1.2 (c (n "tmux-time-tracker") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("sqlite" "runtime-tokio-rustls" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cwmmimkgdr89pywscrzzr9sw8kvbr5ax8vq9lxpy1fscqdpxl7l")))

(define-public crate-tmux-time-tracker-0.1.3 (c (n "tmux-time-tracker") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "macros" "sqlite" "migrate" "chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fs9f08lz1lc5nm5wdqcf8mcyakd8sg3r7b3swskm9rw0wbxfvrj")))

