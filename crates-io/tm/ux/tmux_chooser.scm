(define-module (crates-io tm ux tmux_chooser) #:use-module (crates-io))

(define-public crate-tmux_chooser-0.1.0 (c (n "tmux_chooser") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.0.6") (d #t) (k 0)))) (h "0bf47pmsrh0q69xr6bw0h0qv2v9l9nkc0kn15ld3bqc9laqw3s97")))

(define-public crate-tmux_chooser-0.2.0 (c (n "tmux_chooser") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.0.6") (d #t) (k 0)))) (h "1r66nryymwh4s21qy6fliwjb658n1s6bsngyh2bkjg8x44wmrvvr")))

(define-public crate-tmux_chooser-0.3.0 (c (n "tmux_chooser") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.0.6") (d #t) (k 0)))) (h "1q1d5dcfqybil6nyysmj146i85ghfcxl1n2wjgi8zf9mlmnyjr8y")))

(define-public crate-tmux_chooser-0.4.0 (c (n "tmux_chooser") (v "0.4.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.0.6") (d #t) (k 0)))) (h "1c1yx6anvh6kxzd2vdwyy0lhl25jj0zi2yps3a99ivgiiffdzkxj")))

(define-public crate-tmux_chooser-0.5.0 (c (n "tmux_chooser") (v "0.5.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.0.6") (d #t) (k 0)))) (h "1np2dlhn7h0kmpf1qp55bnphl1pjq89vdnm98r5k7ix0ki7y9680")))

