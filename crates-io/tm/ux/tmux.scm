(define-module (crates-io tm ux tmux) #:use-module (crates-io))

(define-public crate-tmux-0.1.0 (c (n "tmux") (v "0.1.0") (h "0jkbv7c9yxmcdmwaqrc95cfjw35ami550rfp06m2d4vhij6g0c8g")))

(define-public crate-tmux-0.25.0 (c (n "tmux") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "insta") (r "^1.6.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "names") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "ssh2") (r "^0.9.1") (d #t) (k 2)) (d (n "suggestion") (r "^0.1.0") (d #t) (k 0)) (d (n "zellij-client") (r "^0.25.0") (d #t) (k 0)) (d (n "zellij-server") (r "^0.25.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.25.0") (d #t) (k 0)))) (h "04hdhmkqy8mlc226f57y81b82mn87g1knjcv0aijz8sk15l4d4hi") (f (quote (("disable_automatic_asset_installation")))) (r "1.56")))

