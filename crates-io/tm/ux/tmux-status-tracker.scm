(define-module (crates-io tm ux tmux-status-tracker) #:use-module (crates-io))

(define-public crate-tmux-status-tracker-1.0.0 (c (n "tmux-status-tracker") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1p1r3c69w0zyz9c1cc9brg5996djxn84sw864bcgi3x7x55c6f19")))

(define-public crate-tmux-status-tracker-2.0.0 (c (n "tmux-status-tracker") (v "2.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "11x3s8vldq383ni8kb0mb2fb961hn523v8qzr2ps2srj5fghms9h")))

(define-public crate-tmux-status-tracker-2.1.0 (c (n "tmux-status-tracker") (v "2.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0cwmsac1fgxgc0abwga4vn4s042v7andwqfr63z1s45hb56h3jzi")))

(define-public crate-tmux-status-tracker-2.2.0 (c (n "tmux-status-tracker") (v "2.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1fv5h8z9z0rlyly9x6jvxk0w0xrdw2sz6s86cy645kh4z1v7rym8")))

(define-public crate-tmux-status-tracker-2.2.1 (c (n "tmux-status-tracker") (v "2.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1ks2l39dcraq0hvgsrw1jnr3dcyxnxhc0blswyda5xhk6jcbg2wr")))

