(define-module (crates-io tm ux tmux_min_attacher) #:use-module (crates-io))

(define-public crate-tmux_min_attacher-1.0.0 (c (n "tmux_min_attacher") (v "1.0.0") (d (list (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1x74r26yxyikip0fayajvxwpkp58awnpl5ac70mxkhc39zbjc92g")))

