(define-module (crates-io tm ux tmux-remux) #:use-module (crates-io))

(define-public crate-tmux-remux-0.2.0 (c (n "tmux-remux") (v "0.2.0") (d (list (d (n "pico-args") (r "^0.5.0") (f (quote ("combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.2.1") (d #t) (k 0)))) (h "110lkz0yn51bph4mqgr877b6dgijqjkn8wwrz6abh2ma25nlnyfv")))

