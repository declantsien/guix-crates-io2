(define-module (crates-io tm ux tmux-lib) #:use-module (crates-io))

(define-public crate-tmux-lib-0.2.1 (c (n "tmux-lib") (v "0.2.1") (d (list (d (n "async-std") (r "^1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i7i8j342c4kgwzv06an0qg4x3r0726zv0rqa1wdkrgf83n9fq3l") (r "1.59.0")))

(define-public crate-tmux-lib-0.2.2 (c (n "tmux-lib") (v "0.2.2") (d (list (d (n "async-std") (r "^1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jsxz9mgm5qpss34gsg5grnpl994mrp2318pljvkw5g0fc01qmqn") (r "1.59.0")))

(define-public crate-tmux-lib-0.3.0 (c (n "tmux-lib") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01xdby9lxzxdhnzw088wqr1f7vq2y20qjvb14j2lf8zxp69vxdqd") (r "1.59.0")))

