(define-module (crates-io tm ux tmux-thumbs) #:use-module (crates-io))

(define-public crate-tmux-thumbs-0.1.0 (c (n "tmux-thumbs") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "0xh2fj81jy6bgmnrihh3kiip6wzpbyablx3g407l9k88x7zlvc6q") (y #t)))

(define-public crate-tmux-thumbs-0.1.1 (c (n "tmux-thumbs") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "102c4m89i9rs6fw3yqn6ynihm0msg564rldab27nh2nwcwmql88s") (y #t)))

(define-public crate-tmux-thumbs-0.2.0 (c (n "tmux-thumbs") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "09x4ci65r461fh2xddcnk9dw3js1f2dg5z8mlklklp0bcai68r2q") (y #t)))

(define-public crate-tmux-thumbs-0.2.1 (c (n "tmux-thumbs") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "1p2xwsx8gfal6181lf62kkk93f31ybp5ixlf4b6p5hdns3waaiy8") (y #t)))

(define-public crate-tmux-thumbs-0.2.2 (c (n "tmux-thumbs") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "02ai4z9y9r7a4qj8lx6di6k3ammy607rsakgdp4r1i5knp7vlrga") (y #t)))

(define-public crate-tmux-thumbs-0.2.3 (c (n "tmux-thumbs") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "0flv88pljkmibvx2mi6qbir1qh064x5vj3ylbrc59q499mixzrwi") (y #t)))

(define-public crate-tmux-thumbs-0.2.4 (c (n "tmux-thumbs") (v "0.2.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "1iis07dvxxczfx6r3wbzn7902l35xbjbfq3bwx021i8fvdfwhhl2") (y #t)))

