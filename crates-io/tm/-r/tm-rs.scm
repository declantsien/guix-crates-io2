(define-module (crates-io tm -r tm-rs) #:use-module (crates-io))

(define-public crate-tm-rs-2020.11.0 (c (n "tm-rs") (v "2020.11.0") (d (list (d (n "tm-sys") (r ">=2020.11.0, <2021.0.0") (d #t) (k 0)))) (h "1db4i2njbhapgishibc84yar01781w5jprx6ia2j8l2lwzn9fsd4")))

(define-public crate-tm-rs-2020.11.1 (c (n "tm-rs") (v "2020.11.1") (d (list (d (n "tm-sys") (r ">=2020.11.0, <2021.0.0") (d #t) (k 0)))) (h "0nw11y9krcr55ggdnag020ykgk5xa10vhza61q95sdgbb360hqjg")))

(define-public crate-tm-rs-2020.11.3 (c (n "tm-rs") (v "2020.11.3") (d (list (d (n "anymap") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "tm-sys") (r ">=2020.11.0, <2021.0.0") (d #t) (k 0)))) (h "1g7pfmpy6ivbp9mk3l8jx1ny9m41lvpaf0p7qj0jk8plqgsm8qjg")))

(define-public crate-tm-rs-2020.11.4 (c (n "tm-rs") (v "2020.11.4") (d (list (d (n "anymap") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "tm-sys") (r ">=2020.11.0, <2021.0.0") (d #t) (k 0)))) (h "0ig4fs4b3jxd8ds4bqin92x6d4f1wvzclhf07pxyxfx1h438jpyw")))

(define-public crate-tm-rs-2020.11.5 (c (n "tm-rs") (v "2020.11.5") (d (list (d (n "anymap") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "tm-sys") (r ">=2020.11.0, <2021.0.0") (d #t) (k 0)))) (h "016gbbjay0r7wxnqjldrwhg7r9hn9fnxx89mkzr5lzqrkxv2wajg")))

(define-public crate-tm-rs-2020.11.6 (c (n "tm-rs") (v "2020.11.6") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tm-sys") (r "^2020.11") (d #t) (k 0)))) (h "1dxd7m1zspqr909k0qcbdkh0naphvkb9i105jym8p97y4pv19qfw")))

(define-public crate-tm-rs-2020.11.7 (c (n "tm-rs") (v "2020.11.7") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tm-sys") (r "^2020.11") (d #t) (k 0)))) (h "1ah4qkk568m8x52qsycal8m51drksxsm2p1jkaw7kzxdlsbs06ai")))

(define-public crate-tm-rs-2020.11.8 (c (n "tm-rs") (v "2020.11.8") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tm-sys") (r "^2020.11") (d #t) (k 0)))) (h "11giafkxmn7lw3s01bbb70krj5kc23wkpj3sgsbgpigj9a8kxn7a")))

(define-public crate-tm-rs-2020.11.10 (c (n "tm-rs") (v "2020.11.10") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tm-sys") (r "^2020.11.6") (d #t) (k 0)))) (h "1wvy3khn0c6wfn5bin23za41xpckd8b3rn4ggxcnwjazy6698hp1")))

(define-public crate-tm-rs-2020.11.11 (c (n "tm-rs") (v "2020.11.11") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tm-sys") (r "^2020.11.6") (d #t) (k 0)))) (h "1fic03q2by7xnk390yym0zda18ihx5sbd4mdz7pqjk5bnnjkzz28")))

