(define-module (crates-io tm la tmlatestbackup) #:use-module (crates-io))

(define-public crate-tmlatestbackup-0.1.0 (c (n "tmlatestbackup") (v "0.1.0") (d (list (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "18llwqnzvwgifmf2qk462vnvhnmmrbicsf87m3nqwpwgfpala37n")))

(define-public crate-tmlatestbackup-0.1.1 (c (n "tmlatestbackup") (v "0.1.1") (d (list (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "14vi699fdqsh6db3abwnqm4xrw3hnpli4pz3pr494wjg4n9is95z")))

(define-public crate-tmlatestbackup-0.1.2 (c (n "tmlatestbackup") (v "0.1.2") (d (list (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "07j3znszjx37dqj9736ca0146kfx8w33d08qcr3lq81xhpl99nyg")))

