(define-module (crates-io tm l_ tml_parser) #:use-module (crates-io))

(define-public crate-tml_parser-1.0.0 (c (n "tml_parser") (v "1.0.0") (h "1isvl8p0mx93vr8swvgkk1hl2p430m6206pch6r8n3ha777g4mjw")))

(define-public crate-tml_parser-1.0.2 (c (n "tml_parser") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)))) (h "011avcqvw45vxrny01xilzr025lpr0sr498x5ddkhk7wsvckqdh0")))

(define-public crate-tml_parser-1.0.3 (c (n "tml_parser") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)))) (h "054mi0yw8aiy6f51q0m461yvkry629mkxv1zrvpqkm65jld7j3ck")))

(define-public crate-tml_parser-1.0.4 (c (n "tml_parser") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)))) (h "0320c61g4rs9vzah29h765wgs4c55h988apmnql8jrkdglss9p1a")))

(define-public crate-tml_parser-1.0.5 (c (n "tml_parser") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w4xqs785nni8qdwlana74lmizbcxvaqlin5byrv3wykcqvfhw18")))

(define-public crate-tml_parser-1.0.6 (c (n "tml_parser") (v "1.0.6") (d (list (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)))) (h "01m78wb8g248p1x4yyvsnqv1wiqn811zpff2nvzpjqjlawidlxnv")))

