(define-module (crates-io tm i- tmi-parser) #:use-module (crates-io))

(define-public crate-tmi-parser-0.1.0 (c (n "tmi-parser") (v "0.1.0") (h "0kxnbjlazhxckl9d27nx9rw2c9gwxldm62600f4f11f8dqq7dnh3")))

(define-public crate-tmi-parser-0.1.1-alpha (c (n "tmi-parser") (v "0.1.1-alpha") (h "0awvmch10vhj1vgq1w35743q39r9cxqrzrrpz2dmfhg6an6z5a1b")))

(define-public crate-tmi-parser-0.1.1-beta (c (n "tmi-parser") (v "0.1.1-beta") (h "0z879rvmk1ak244786nw4dladrk0hxahxjd84my5x6fznizf4dmz")))

(define-public crate-tmi-parser-0.1.1 (c (n "tmi-parser") (v "0.1.1") (h "0jjwdknp7yv4ar44gqhh7v6v8mnqdx6g6p3x4faz25m2cv16lfv9")))

(define-public crate-tmi-parser-0.1.2-alpha (c (n "tmi-parser") (v "0.1.2-alpha") (h "1g0vsbv905wj5yaxgvraqznxgxfh33n20xhnlzv7a0jyqm26vk0l")))

