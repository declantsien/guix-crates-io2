(define-module (crates-io tm p0 tmp006) #:use-module (crates-io))

(define-public crate-tmp006-0.1.0 (c (n "tmp006") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "18275vaia45bpdiffwnprw0vbnxm6911n5znapz83x74y0726hl9")))

(define-public crate-tmp006-0.2.0 (c (n "tmp006") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0q1kknrhpizjjn7ncxzk4yakb53v3kziw9b9v9gp7szwx81fkm6c")))

(define-public crate-tmp006-1.0.0 (c (n "tmp006") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0dbvng3whh7hv1i0mxxf0wcahbiv62mw6i8859nl799ppd6c0scv")))

