(define-module (crates-io tm pd tmpdir) #:use-module (crates-io))

(define-public crate-tmpdir-1.0.0 (c (n "tmpdir") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.12") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("fs" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0594mg6sbz5kdj52ima1gc8yxykdyl9wc36acpk42cnnpd11kmdl")))

