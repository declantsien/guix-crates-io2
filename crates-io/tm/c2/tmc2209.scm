(define-module (crates-io tm c2 tmc2209) #:use-module (crates-io))

(define-public crate-tmc2209-0.1.0 (c (n "tmc2209") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0q0kfwnlaa4dq0ll0836w7gnlz6v17pv7hyasyr151vd0r08kpca")))

(define-public crate-tmc2209-0.2.0 (c (n "tmc2209") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1zhyfgs7aby3p9pa19d1wr0a05vji2c70wrv1kjfsbfvz0vk4w2r") (f (quote (("hash" "hash32" "hash32-derive"))))))

(define-public crate-tmc2209-0.2.1 (c (n "tmc2209") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0qlj8203r5c0n36mfxad9dgzv6v5nl48zwxpgrkm4nk2aibrh1wj") (f (quote (("hash" "hash32" "hash32-derive"))))))

(define-public crate-tmc2209-0.2.2 (c (n "tmc2209") (v "0.2.2") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "10r7i91sq0ryrynpw9zy8li9ab1d8cqhg4qz736sndqyj3sxnli5") (f (quote (("hash" "hash32" "hash32-derive"))))))

