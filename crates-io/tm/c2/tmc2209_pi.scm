(define-module (crates-io tm c2 tmc2209_pi) #:use-module (crates-io))

(define-public crate-tmc2209_pi-0.1.0 (c (n "tmc2209_pi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "packed_struct") (r "^0.10") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cbwdp6z7lqg9snvs8x1kz5akxlqdcgv43sj2p5jk0nin8msmxp2")))

