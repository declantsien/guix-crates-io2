(define-module (crates-io tm -l tm-language) #:use-module (crates-io))

(define-public crate-tm-language-0.0.0 (c (n "tm-language") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "06yn5a4cdp3chhis4nymkkqmqha4z9g4cnrs1k4nvrap30p9hnvk") (f (quote (("default"))))))

