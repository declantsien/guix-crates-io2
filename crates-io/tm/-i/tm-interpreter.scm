(define-module (crates-io tm #{-i}# tm-interpreter) #:use-module (crates-io))

(define-public crate-tm-interpreter-0.1.0 (c (n "tm-interpreter") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_scan") (r "^0.4.1") (d #t) (k 0)))) (h "01n8bxmqvr410ll9h65m4nlhx3ac8x29rxl8491186w9payr9v6d")))

