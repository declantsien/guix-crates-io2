(define-module (crates-io tm -w tm-wheel) #:use-module (crates-io))

(define-public crate-tm-wheel-0.1.0 (c (n "tm-wheel") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "08qrnaq97b8bsydg1pqwklbr3r3za4ghnq010xn5qs18n6rh1ra7") (f (quote (("default" "std")))) (y #t) (s 2) (e (quote (("std" "dep:slab"))))))

(define-public crate-tm-wheel-0.1.1 (c (n "tm-wheel") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "080mkhh9s2rpyj8wwm8pjy9zw2jgrcwmmbi4zx18j9s1lf056mbp") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:slab"))))))

