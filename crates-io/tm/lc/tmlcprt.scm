(define-module (crates-io tm lc tmlcprt) #:use-module (crates-io))

(define-public crate-tmlcprt-0.1.0-pre.1 (c (n "tmlcprt") (v "0.1.0-pre.1") (d (list (d (n "tendermint-light-client-verifier") (r "^0.28.0-pre.1") (d #t) (k 0)))) (h "1w1xvmrph0yb2ddzywrizd14d629gsigh8azfl1yk54hpza711fv")))

(define-public crate-tmlcprt-0.1.0 (c (n "tmlcprt") (v "0.1.0") (d (list (d (n "tendermint-light-client-verifier") (r "^0.28.0-pre.1") (d #t) (k 0)))) (h "1w6d4s1c1a4zrgyfsxnf9d8l1c663xxaw485qyn78ydffynisvqk")))

