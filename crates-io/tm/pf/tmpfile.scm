(define-module (crates-io tm pf tmpfile) #:use-module (crates-io))

(define-public crate-tmpfile-0.0.1 (c (n "tmpfile") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "swctx") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1gkfx90748h9zff4zwgv4mbgp1c8icr5111wa52dqgd99gszy025") (s 2) (e (quote (("defer-persist" "dep:swctx")))) (r "1.56")))

(define-public crate-tmpfile-0.0.2 (c (n "tmpfile") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "swctx") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1zbs3lgwhg5f1p3zhw5ji05aklafnv567vgamsafrz0211k8f1g1") (s 2) (e (quote (("defer-persist" "dep:swctx")))) (r "1.56")))

