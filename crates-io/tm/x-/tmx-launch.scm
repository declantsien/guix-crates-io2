(define-module (crates-io tm x- tmx-launch) #:use-module (crates-io))

(define-public crate-tmx-launch-0.1.0 (c (n "tmx-launch") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0s92wy2jyni3zjnj6v8ma1156dq4l9hj6rqk3slxrv6pwdrzflrk")))

