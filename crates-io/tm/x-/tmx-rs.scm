(define-module (crates-io tm x- tmx-rs) #:use-module (crates-io))

(define-public crate-tmx-rs-0.1.0 (c (n "tmx-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "17q9bidb7h5mq5v2p73gis4rmw2xl2q4b0qy2v4azf5y3w977l7s")))

