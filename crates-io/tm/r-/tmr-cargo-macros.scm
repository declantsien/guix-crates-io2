(define-module (crates-io tm r- tmr-cargo-macros) #:use-module (crates-io))

(define-public crate-tmr-cargo-macros-0.1.0 (c (n "tmr-cargo-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1x1hjlxasaawldsfxm1ldrafcj3lm9aprq3nbimil77c2sn5vlja") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.2.0 (c (n "tmr-cargo-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "13lck0fq0i0qlxssk672fsm3caj5s2c2kkw9s316x8na5d9wiih5") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.3.0 (c (n "tmr-cargo-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1rc4nrg200g9fvdv1r2n7gcmcbp9v3swkbkhhynypv72gng2ahc2") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.4.0 (c (n "tmr-cargo-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1iqk7dmwrhlhcib6gbiaxlqljywi9fkaf0nxdv37ahh6czw9wvhq") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.5.0 (c (n "tmr-cargo-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1jkzhvj5h6fjw1mwgcjlnhv5kgyr91y8ghsw4vpd66g8ncjhsy48") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.6.0 (c (n "tmr-cargo-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1nxbdw4l2kwazwcf9s40mccrrqnjkxf1k8qhrwpqa1pk8rhsq6nj") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.7.0 (c (n "tmr-cargo-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1b3hxwkfmifalbd75pssz4r9bq4yskd0inwnyyrzj58jk36y55m3") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.8.0 (c (n "tmr-cargo-macros") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1vpblcvdpr3vp8ghbc23a13628v0qhfp5qlkds3i3npri84mk303") (r "1.72")))

(define-public crate-tmr-cargo-macros-0.9.0 (c (n "tmr-cargo-macros") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "112z3yhgb6in51yjw951p13bllyba8lk8s1z5gcawp7w581h4v2k") (r "1.72")))

