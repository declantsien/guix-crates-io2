(define-module (crates-io tm r- tmr-cargo) #:use-module (crates-io))

(define-public crate-tmr-cargo-0.1.0 (c (n "tmr-cargo") (v "0.1.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "17xmpd1sn29v1mmcpj49aryd0h1k6xk976g7mklrj5brmy0p50pj") (r "1.72")))

(define-public crate-tmr-cargo-0.2.0 (c (n "tmr-cargo") (v "0.2.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "1q5dkisibpxr8gpiws382mynrlklx3yy3nnfdjif1zpl70jqh0mi") (r "1.72")))

(define-public crate-tmr-cargo-0.3.0 (c (n "tmr-cargo") (v "0.3.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "0nq90784fdlq4rhc5jrali29rabw4yj7lzrl9zqa754mjny0xriz") (r "1.72")))

(define-public crate-tmr-cargo-0.4.0 (c (n "tmr-cargo") (v "0.4.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "1sp0d4h2bmndah7m9jmr8x4dr315r0lcxj8hqkkwsvmnr7qfr3s4") (r "1.72")))

(define-public crate-tmr-cargo-0.5.0 (c (n "tmr-cargo") (v "0.5.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "15r9zr3ckzbkg0bxy7zcc9rbfv70sbhadsx1x2v4x4cxxwx0i85k") (r "1.72")))

(define-public crate-tmr-cargo-0.6.0 (c (n "tmr-cargo") (v "0.6.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "12ii8h1nmg2xfvycshmzjnq2zqpvjqhbn8nqpdcfdn82cnwfzqxb") (r "1.72")))

(define-public crate-tmr-cargo-0.7.0 (c (n "tmr-cargo") (v "0.7.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "1r1ggfjap6pj7gmb632v8d8qc14qf6zcmcyajac2b5ykq9z8mk86") (r "1.72")))

(define-public crate-tmr-cargo-0.8.0 (c (n "tmr-cargo") (v "0.8.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "0i5v18m8dh54980lipwi520z4khh52gx9mdp8xjjg5bhzkz4gbz1") (r "1.72")))

(define-public crate-tmr-cargo-0.9.0 (c (n "tmr-cargo") (v "0.9.0") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "0i52rajrfldpijg273aa8x98k8s3rqzrjzq25gzmanaadndh627r") (r "1.72")))

(define-public crate-tmr-cargo-0.9.1 (c (n "tmr-cargo") (v "0.9.1") (d (list (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tmr-cargo-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.8") (d #t) (k 0)))) (h "1kahz7hvk1gidcgmqqwlsm74rd06y3bj5w0v5l6i2hr8jrx4ykc5") (r "1.72")))

