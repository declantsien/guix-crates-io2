(define-module (crates-io uz er uzers) #:use-module (crates-io))

(define-public crate-uzers-0.11.1 (c (n "uzers") (v "0.11.1") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0rm4mfph8sj7vi70f71gd1fx6cclcjrkvcb36gqznvfxxw47zxvn") (f (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.11.2 (c (n "uzers") (v "0.11.2") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1k5wsfvxsvs8l65wspcwhbfrkw0k85kbk5db3bawl7grydi6am29") (f (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.11.3 (c (n "uzers") (v "0.11.3") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0qrzbhncbv4s52lgyzs2pxn1b6gmx9k7h1rdwdwix44cgvf87lkn") (f (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.12.0 (c (n "uzers") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)))) (h "1r4d8i3ji1m1s2nr9vrvn0ilk9jihbzq7kpg94akp6ym2rg8g1bx") (f (quote (("test-integration") ("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

