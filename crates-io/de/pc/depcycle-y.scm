(define-module (crates-io de pc depcycle-y) #:use-module (crates-io))

(define-public crate-depcycle-y-1.0.0 (c (n "depcycle-y") (v "1.0.0") (d (list (d (n "depcycle-x") (r "^1") (o #t) (d #t) (k 0)))) (h "1r3g8mj9n2g3pisj00k0wyqxbmq1l03mchiqnxkrjj3fpamnylz8") (f (quote (("x" "depcycle-x"))))))

