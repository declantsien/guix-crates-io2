(define-module (crates-io de pc depcheck-rs-node) #:use-module (crates-io))

(define-public crate-depcheck-rs-node-0.0.1 (c (n "depcheck-rs-node") (v "0.0.1") (d (list (d (n "depckeck-rs-core") (r "^0.0.1") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.0") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.0") (d #t) (k 0)))) (h "13rr7h108x1fsxp0f9k7nlw1w9w5iacbx76yax5gnlca9c9r1zc5")))

(define-public crate-depcheck-rs-node-0.0.22 (c (n "depcheck-rs-node") (v "0.0.22") (d (list (d (n "depckeck-rs-core") (r "^0.0.22") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.0") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.0") (d #t) (k 0)))) (h "1zf654wp6qjpl7s8aa3mjr81fr4iw0xm6dwc8ikrrwyy2gbh9mfz")))

(define-public crate-depcheck-rs-node-0.0.23 (c (n "depcheck-rs-node") (v "0.0.23") (d (list (d (n "depckeck-rs-core") (r "^0.0.23") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.1") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.1") (d #t) (k 0)))) (h "1419vwvmbf01pnq1flcc5h8xg15m7jw2fd92fccrnkv5sz8j00zm")))

(define-public crate-depcheck-rs-node-0.0.24 (c (n "depcheck-rs-node") (v "0.0.24") (d (list (d (n "depckeck-rs-core") (r "^0.0.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.1") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.1") (d #t) (k 0)))) (h "0mb8z1r4aikygkna6g7r4vppgslfbg4n5bph3j3adwjdq8z2z1ij")))

(define-public crate-depcheck-rs-node-0.0.25 (c (n "depcheck-rs-node") (v "0.0.25") (d (list (d (n "depckeck-rs-core") (r "^0.0.25") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.1") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.1") (d #t) (k 0)))) (h "1gsfp88wwa1lxwn0h3z56r4aa13ykmlh1a68w1p1ic23w5nzk76r")))

(define-public crate-depcheck-rs-node-0.0.26 (c (n "depcheck-rs-node") (v "0.0.26") (d (list (d (n "depckeck-rs-core") (r "^0.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "napi") (r "^2.3.0-canary.1") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.0-canary.1") (d #t) (k 0)))) (h "1dwsj83cyvcld60yah0rsarkmiyzgswwim1bqp0a8hsyw3x87kwp")))

(define-public crate-depcheck-rs-node-0.0.27 (c (n "depcheck-rs-node") (v "0.0.27") (d (list (d (n "depckeck-rs-core") (r "^0.0.27") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "napi") (r "^2.3.3") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^1.2.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.3.3") (d #t) (k 0)))) (h "114bwdrzi5wrpwgs4bv8yd1kx6sdv8ncmfqlkn1wrr0bbm71l1zj")))

(define-public crate-depcheck-rs-node-0.0.28 (c (n "depcheck-rs-node") (v "0.0.28") (d (list (d (n "depckeck-rs-core") (r "^0.0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "napi") (r "^2.5.0") (f (quote ("napi4"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.5.0") (d #t) (k 0)))) (h "1sf8dpfdig9mpyvmbh43sy42p89krwzq2zrdj4glhm51hlj8spq0")))

