(define-module (crates-io de pc depcycle-x) #:use-module (crates-io))

(define-public crate-depcycle-x-0.1.0 (c (n "depcycle-x") (v "0.1.0") (h "00pgr29b1rydckh7ric0zbar2y1sw56nbsg68waacv3aplvvp4zs")))

(define-public crate-depcycle-x-1.0.0 (c (n "depcycle-x") (v "1.0.0") (h "0idshk1djlc5i08p3was1mmfk85n2b5w8g2c79p8mx1p7rjbncy5")))

(define-public crate-depcycle-x-1.0.1 (c (n "depcycle-x") (v "1.0.1") (d (list (d (n "depcycle-y") (r "^1") (o #t) (d #t) (k 0)))) (h "19fc1a626v8wcwl74w949q8xmibq3882zg0nh39lm7s8qn0f1ric") (f (quote (("y" "depcycle-y"))))))

