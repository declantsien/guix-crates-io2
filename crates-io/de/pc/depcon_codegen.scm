(define-module (crates-io de pc depcon_codegen) #:use-module (crates-io))

(define-public crate-depcon_codegen-0.0.0 (c (n "depcon_codegen") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "16w77i43f474wglqwnyjcnm2d3xbxcfks2ndqjy9yldj6ghnqalh")))

(define-public crate-depcon_codegen-0.1.0 (c (n "depcon_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0hj85a430v9v2kx67r09qvjddlbvhi8zv8ks2flfg244wbiqhhy4")))

(define-public crate-depcon_codegen-0.2.0 (c (n "depcon_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "089hmn904nnvxxiw9bxpabsmbqc6c6w9rybpmh6r0s3la6x458w8")))

(define-public crate-depcon_codegen-0.3.0 (c (n "depcon_codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "066iqp1y4vf66sx0yr5dp6b8yqmx9m19avi7798sm2qr6d8nhwrc")))

