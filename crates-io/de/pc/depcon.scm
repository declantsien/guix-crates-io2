(define-module (crates-io de pc depcon) #:use-module (crates-io))

(define-public crate-depcon-0.0.0 (c (n "depcon") (v "0.0.0") (d (list (d (n "depcon_codegen") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "inventory") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0rs67kw7lpswc5xs4yw9axdnnyylhkgypnx5x55hidpklaj9jydi") (f (quote (("default" "codegen") ("codegen" "depcon_codegen"))))))

(define-public crate-depcon-0.1.0 (c (n "depcon") (v "0.1.0") (d (list (d (n "depcon_codegen") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "inventory") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1hgffim2rdr7xkbsnx1jr5brb1vb897qjzy2n574chyyh8g9rkdy") (f (quote (("default" "codegen") ("codegen" "depcon_codegen"))))))

(define-public crate-depcon-0.2.0 (c (n "depcon") (v "0.2.0") (d (list (d (n "depcon_codegen") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "inventory") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "17cl6nnra49sp14pgzb1z01krly79d1v57k1rdn48kqv2iiasd6g") (f (quote (("default" "codegen") ("codegen" "depcon_codegen"))))))

(define-public crate-depcon-0.3.0 (c (n "depcon") (v "0.3.0") (d (list (d (n "depcon_codegen") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "inventory") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1f3hd0vbgq2argx896j9pgkb0r9ng3vl5xc3zha6ahjz0r4jn47a") (f (quote (("default" "codegen") ("codegen" "depcon_codegen"))))))

