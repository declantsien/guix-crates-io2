(define-module (crates-io de b- deb-rust) #:use-module (crates-io))

(define-public crate-deb-rust-0.1.0 (c (n "deb-rust") (v "0.1.0") (d (list (d (n "ar") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1cdmn9mqhsyhpgr4kir88fcyl42ankwl0p2nd7fzcg0zfsff5ssx")))

(define-public crate-deb-rust-0.1.1 (c (n "deb-rust") (v "0.1.1") (d (list (d (n "ar") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1n0w8gzvaryfq66663l8fh88chvk9hi98x5xxcwl7vxjmspyrz04")))

(define-public crate-deb-rust-0.1.2 (c (n "deb-rust") (v "0.1.2") (d (list (d (n "ar") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1l0ajsf2h4cs5w7fyjn48vlkkijc3p0v080xnvvgvj64fpjgxj1f")))

