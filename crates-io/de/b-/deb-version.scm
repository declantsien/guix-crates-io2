(define-module (crates-io de b- deb-version) #:use-module (crates-io))

(define-public crate-deb-version-0.1.0 (c (n "deb-version") (v "0.1.0") (d (list (d (n "apt-pkg-native") (r "^0.2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)))) (h "0yz7w8wziczyrci8cdli23zqlzdrf89x38q5ygri3j358ww34c6h")))

(define-public crate-deb-version-0.1.1 (c (n "deb-version") (v "0.1.1") (d (list (d (n "apt-pkg-native") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "1bfnc5vw47avr5h94mqykp6xba3609xv79rrxwsjg1mnl862b45k")))

