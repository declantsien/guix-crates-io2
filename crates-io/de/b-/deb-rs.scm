(define-module (crates-io de b- deb-rs) #:use-module (crates-io))

(define-public crate-deb-rs-0.1.0 (c (n "deb-rs") (v "0.1.0") (d (list (d (n "cmd_lib") (r "^0.8.5") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1kx2i9hxgk9dycr74y04zwzw6wd3sq69pql9hlpib6zfiwc9bs7p")))

(define-public crate-deb-rs-0.1.1 (c (n "deb-rs") (v "0.1.1") (d (list (d (n "cmd_lib") (r "^0.8.5") (d #t) (k 0)) (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v8sssnq3q417k014ccbm583933sc46pwpwmc4h9smbbcz4y8svd")))

(define-public crate-deb-rs-0.1.2 (c (n "deb-rs") (v "0.1.2") (d (list (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "run_script") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f35rjlvcr4gx9ljn9rvnf4nw6d03zkihg7ys2js3bgvlyzjps7v")))

