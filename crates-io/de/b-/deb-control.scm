(define-module (crates-io de b- deb-control) #:use-module (crates-io))

(define-public crate-deb-control-0.2.0 (c (n "deb-control") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "sailfish") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fz1lriv5yvii13cvmv0gh0a3asqja6iwpkdhsi89vynihsfw8rk") (y #t)))

(define-public crate-deb-control-0.3.0 (c (n "deb-control") (v "0.3.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pkgspec") (r "^0.2") (d #t) (k 0)))) (h "1lj5dj92a82iza278j2jljgwnirzbl8hy14blnn6v8sshrx251id") (y #t)))

(define-public crate-deb-control-0.3.1 (c (n "deb-control") (v "0.3.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pkgspec") (r "^0.2") (d #t) (k 0)))) (h "1v09fy52bckw6mdxdzz6qisq2f3ws448f0n0zczp0pc0mr107bv0") (y #t)))

