(define-module (crates-io de b- deb-libsolv-resolver) #:use-module (crates-io))

(define-public crate-deb-libsolv-resolver-0.1.0 (c (n "deb-libsolv-resolver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "defaultmap") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsolv-sys") (r "^0.1") (d #t) (k 0)))) (h "0h7y2afyq932q1dkbnzxhvg54v1ppjf708i1hs1bkjk4x9y7ckaa")))

