(define-module (crates-io de l- del-gl) #:use-module (crates-io))

(define-public crate-del-gl-0.1.0 (c (n "del-gl") (v "0.1.0") (d (list (d (n "del-misc") (r "^0.1.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)))) (h "0bd8aj6amb4b19q5gk455d163c19x5dpazngwswydxv7ik5cn1zr") (y #t)))

(define-public crate-del-gl-0.1.1 (c (n "del-gl") (v "0.1.1") (d (list (d (n "del-misc") (r "^0.1.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)))) (h "0y5ggg3hrd2h4kfq1llxq45y53r2wgwmw55xhd7a9pzw5xr2a4l9")))

(define-public crate-del-gl-0.1.2 (c (n "del-gl") (v "0.1.2") (d (list (d (n "del-misc") (r "^0.1.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "glutin") (r "^0.29.1") (d #t) (k 0)))) (h "18fcjqdmnyapj7mqsw4sfgkrhm5lb2lj6ppjj4n9hf546mpr9gjs")))

