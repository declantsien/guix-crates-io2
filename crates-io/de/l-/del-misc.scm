(define-module (crates-io de l- del-misc) #:use-module (crates-io))

(define-public crate-del-misc-0.1.0 (c (n "del-misc") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1f2xnaq66rhsvr14bn9q9h6lv0hx7qbg88isl2wswf8vigkfvrcl")))

(define-public crate-del-misc-0.1.1 (c (n "del-misc") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "del-geo") (r "^0.1.1") (d #t) (k 0)) (d (n "del-ls") (r "^0.1.2") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "198dp39w60qppy7n08c87f2x0vk8q7zmzv6cmj80za2na7nw1c3x")))

(define-public crate-del-misc-0.1.2 (c (n "del-misc") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "del-geo") (r "^0.1.1") (d #t) (k 0)) (d (n "del-ls") (r "^0.1.2") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0ixqdjiip866afljjhcm2j7ss468pbkvh6cgvl352jakvpkm27sw")))

(define-public crate-del-misc-0.1.3 (c (n "del-misc") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "del-geo") (r "^0.1.1") (d #t) (k 0)) (d (n "del-ls") (r "^0.1.2") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "13wxw0hqxcwfnlys3c0v38v5h9xgkrm2gzwdkkpv7li980nwrp4p")))

(define-public crate-del-misc-0.1.4 (c (n "del-misc") (v "0.1.4") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "del-geo") (r "^0.1.4") (d #t) (k 0)) (d (n "del-ls") (r "^0.1.2") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.11") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1sifi566k11fljqf0pq6pks1927sqzkx5ygfcqcc3h77i0ynr2p2")))

