(define-module (crates-io de l- del-rs) #:use-module (crates-io))

(define-public crate-del-rs-0.1.0 (c (n "del-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zwc0m4j2z4aqw1r25b610mdipnws1q2dx1gclsc9lq71q632gjc") (y #t)))

(define-public crate-del-rs-0.2.0 (c (n "del-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1v674qb5v8gwlbm5fqh8nhfhr9qfawzh9ajfa6r1mqvar7w739l0")))

