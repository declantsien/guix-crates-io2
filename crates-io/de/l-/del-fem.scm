(define-module (crates-io de l- del-fem) #:use-module (crates-io))

(define-public crate-del-fem-0.1.0 (c (n "del-fem") (v "0.1.0") (d (list (d (n "del-geo") (r "^0.1.18") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0klchjgfcwr7nvs5g7jgqlrilja5jwvn42q85cfxjnm562r781ka")))

(define-public crate-del-fem-0.1.1 (c (n "del-fem") (v "0.1.1") (d (list (d (n "del-geo") (r "^0.1.18") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.25") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1d7bzlq1v8sd0lrwmcz6g767z4a8453zvl2iw76csg5pc93ci22v")))

(define-public crate-del-fem-0.1.2 (c (n "del-fem") (v "0.1.2") (d (list (d (n "del-geo") (r "^0.1.19") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.25") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1jjljz2nwhq8b8pj2jcc0z731j14l0acc48hy49qlarfj1bsp3x5")))

(define-public crate-del-fem-0.1.3 (c (n "del-fem") (v "0.1.3") (d (list (d (n "del-geo") (r "^0.1.20") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.25") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "08rxh183mkx0vyyvhizzb4sb2bckkyrh526rm7cm5rpir05krw2c")))

