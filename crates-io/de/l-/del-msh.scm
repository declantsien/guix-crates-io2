(define-module (crates-io de l- del-msh) #:use-module (crates-io))

(define-public crate-del-msh-0.1.0 (c (n "del-msh") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0bk67a05yx9farxn74hka2a9sadhsd8n24ngshdlqsfjywh9kjrx")))

(define-public crate-del-msh-0.1.1 (c (n "del-msh") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0xszzwnaddkap8ycyr07k28anqgpxi80m29cm73d6qjri673ih1c")))

(define-public crate-del-msh-0.1.2 (c (n "del-msh") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1x9lzlgy342bcdsxs1x1c2rv1chxsg66m5dwh8bbc4j8dklapn6d")))

(define-public crate-del-msh-0.1.3 (c (n "del-msh") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1wjqiz3b2mf45av1qlxwxcpi1vhfrr3hpy89rz0bi9qibc8b6ybw")))

(define-public crate-del-msh-0.1.4 (c (n "del-msh") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "03cf7cn37czcd3mi57h4w0q9cb093wsz9gkg1azi85n2xbk2lzb4")))

(define-public crate-del-msh-0.1.5 (c (n "del-msh") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1q1wgk1jsmby4dl1m9y2vbvbmrfrn8hba3bgc20z3igvgg087xf9")))

(define-public crate-del-msh-0.1.6 (c (n "del-msh") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1vxc2sl0jswkkfxbih4afq5hgw5y0rfb8gx4lb5dawgv7hqyr2g9")))

(define-public crate-del-msh-0.1.7 (c (n "del-msh") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0vz9avakkdxy14mkbhq1kppp267m2ifvbhhl82q3q00c1xbn2p5y")))

(define-public crate-del-msh-0.1.8 (c (n "del-msh") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "15flxxmg3g1c5glmac3gp45c1ds0pc20ik1djydkh9dcw2q1n742")))

(define-public crate-del-msh-0.1.9 (c (n "del-msh") (v "0.1.9") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0ahj9zcbm1j141b7f5nsr0yiskkzv9q539a5qcadjsxjmgdbq06n")))

(define-public crate-del-msh-0.1.10 (c (n "del-msh") (v "0.1.10") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0fm8nv1pskn88r2l3q64nrwdk00ajcph4h9615g84gd00bhdcrhv")))

(define-public crate-del-msh-0.1.11 (c (n "del-msh") (v "0.1.11") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0cc2hpz9q7fsb384jaf5m2z33w6l129qz16skspfspwlksz35wk5")))

(define-public crate-del-msh-0.1.12 (c (n "del-msh") (v "0.1.12") (d (list (d (n "del-geo") (r "^0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "01x6s7in7acgvmpv4qyccigqhn9h3c99jyqipy7w8mhb2jcvyi66")))

(define-public crate-del-msh-0.1.13 (c (n "del-msh") (v "0.1.13") (d (list (d (n "del-geo") (r "^0.1.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1s1b24zmwb6vfvkpgb3qx6xk5paizc9qcq48mpsbqcsp29zhjyw3")))

(define-public crate-del-msh-0.1.14 (c (n "del-msh") (v "0.1.14") (d (list (d (n "del-geo") (r "^0.1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dpg73gvisq6j32dz3filj8jikb8lmfvi7jf12w9hpfzmd3bn1fy")))

(define-public crate-del-msh-0.1.15 (c (n "del-msh") (v "0.1.15") (d (list (d (n "del-geo") (r "^0.1.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nsn8rbrgdildj0snkafnkrxfw8z2cw683g09qm7xnd5ksx856n2")))

(define-public crate-del-msh-0.1.16 (c (n "del-msh") (v "0.1.16") (d (list (d (n "del-geo") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18c89g6m0fn05a4v3j3xaym3qaywh3crkzmpnzx03gqr0rg7zyvl")))

(define-public crate-del-msh-0.1.17 (c (n "del-msh") (v "0.1.17") (d (list (d (n "del-geo") (r "^0.1.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1laswxb169ksf587z7xwp68nflx530593lxys345482akklf6z7s")))

(define-public crate-del-msh-0.1.18 (c (n "del-msh") (v "0.1.18") (d (list (d (n "del-geo") (r "^0.1.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14gck59ppq4ripkddaaz2r8ih44vbvkn03i7yr143c0kfyha6csb")))

(define-public crate-del-msh-0.1.19 (c (n "del-msh") (v "0.1.19") (d (list (d (n "del-geo") (r "^0.1.11") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y4n0dbnmpra4ydj2z5i4q6x6lhj28n2gxw7rv6jj0439b4s87d5")))

(define-public crate-del-msh-0.1.20 (c (n "del-msh") (v "0.1.20") (d (list (d (n "del-geo") (r "^0.1.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b31rr553mj98slhw96w04g8f57i2aah7a3lrn699giv19rc28x9")))

(define-public crate-del-msh-0.1.21 (c (n "del-msh") (v "0.1.21") (d (list (d (n "del-geo") (r "^0.1.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x8sbg8xm7xrxlkcwpqfifphkiqsw769ir99l09f4yc2k5plggb3")))

(define-public crate-del-msh-0.1.22 (c (n "del-msh") (v "0.1.22") (d (list (d (n "del-geo") (r "^0.1.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bpda9fsyhhhidc3yibsp295rnb5188agm89497arbvx6f80sf52")))

(define-public crate-del-msh-0.1.23 (c (n "del-msh") (v "0.1.23") (d (list (d (n "del-geo") (r "^0.1.15") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y3b182n2yfi7bas5ahjncyxh6vf6r8mpkv44q6z3xir1bi7zsip")))

(define-public crate-del-msh-0.1.24 (c (n "del-msh") (v "0.1.24") (d (list (d (n "del-geo") (r "^0.1.16") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nkb6ymf5n4c5ip09glm1xs59j8rgpgabz72c45wg7wcizn79p64")))

(define-public crate-del-msh-0.1.25 (c (n "del-msh") (v "0.1.25") (d (list (d (n "del-geo") (r "^0.1.18") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sqcxq0nnpayf8zjxag014a3lk0xlcqscw0fjfisa8vh223ircwl")))

(define-public crate-del-msh-0.1.26 (c (n "del-msh") (v "0.1.26") (d (list (d (n "del-geo") (r "^0.1.20") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y97fifq0im9zbwicw07g7n2n7yhgi5qg1255kn7p5mh42a4nj1c")))

(define-public crate-del-msh-0.1.27 (c (n "del-msh") (v "0.1.27") (d (list (d (n "del-geo") (r "^0.1.21") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02lxfh8i51p514vy8dcd5h4ckv1qnh4amkxp0f5ac59plfcfgh8v")))

(define-public crate-del-msh-0.1.28 (c (n "del-msh") (v "0.1.28") (d (list (d (n "del-geo") (r "^0.1.22") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18z84hvi5296l9khjw5b0qfhkd49zq7a07pv44c9j93yvbm699sx")))

(define-public crate-del-msh-0.1.29 (c (n "del-msh") (v "0.1.29") (d (list (d (n "del-geo") (r "^0.1.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1r1ky62pvc6hqjdg46wn9syxkmnvzsidpggjblm3yimwkg0787s6")))

(define-public crate-del-msh-0.1.30 (c (n "del-msh") (v "0.1.30") (d (list (d (n "del-geo") (r "^0.1.24") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15hn8s3dlgsa9dmv9jsfj1b5c4z3smh570ig6jf8j3nv062z8mbk")))

