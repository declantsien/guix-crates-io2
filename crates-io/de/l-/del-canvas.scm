(define-module (crates-io de l- del-canvas) #:use-module (crates-io))

(define-public crate-del-canvas-0.1.0 (c (n "del-canvas") (v "0.1.0") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "0h2plfk64qzawv29wjfymnwhpa5vf056ljr1macdz3hda7w9plxw")))

(define-public crate-del-canvas-0.1.1 (c (n "del-canvas") (v "0.1.1") (d (list (d (n "del-geo") (r "^0.1.22") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.28") (d #t) (k 0)) (d (n "gif") (r "^0.13.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "0iwingpdf59pb43i1a02qva0pydwl9d2bjapr00gdzjxymgi8f8x")))

