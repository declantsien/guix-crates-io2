(define-module (crates-io de l- del-cad) #:use-module (crates-io))

(define-public crate-del-cad-0.1.0 (c (n "del-cad") (v "0.1.0") (d (list (d (n "del-dtri") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "1x89k73ggj6g8bzkjxd5x9fsh56ka6fc3rrglqkx21r1h2wdhfaf")))

(define-public crate-del-cad-0.1.1 (c (n "del-cad") (v "0.1.1") (d (list (d (n "del-dtri") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "0si3qw8lingvl6r7kr4gaw4dcsnqsm5nlcj3mrwmhjlb6dqnlnpd")))

(define-public crate-del-cad-0.1.2 (c (n "del-cad") (v "0.1.2") (d (list (d (n "del-dtri") (r "^0.1.2") (d #t) (k 0)) (d (n "del-geo") (r "^0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ordered-float") (r "^3.9.2") (d #t) (k 0)))) (h "0aysywabad2bf09nc8m4hripc8azwhk5yvk2ix5ilcb20sab7p3a")))

