(define-module (crates-io de l- del-dtri) #:use-module (crates-io))

(define-public crate-del-dtri-0.1.0 (c (n "del-dtri") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "15501sp0cwx8g99k96syiwj2cj2kcqi1zj29n92v0n2wd930jccs") (y #t)))

(define-public crate-del-dtri-0.1.1 (c (n "del-dtri") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "15rwdrw9272mymy10i1sihrls18j53gcgcjnymx16mmn39whsas5") (y #t)))

(define-public crate-del-dtri-0.1.2 (c (n "del-dtri") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1mg05n6bl69zmdkqdrdkshxh7zgci6bfmj0n72wl3rffccm42931") (y #t)))

(define-public crate-del-dtri-0.1.3 (c (n "del-dtri") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0cd2gg4f21iflyg4xjapz395s7k32d58gp0bipik593dv47xf1r2") (y #t)))

(define-public crate-del-dtri-0.1.4 (c (n "del-dtri") (v "0.1.4") (d (list (d (n "del-geo") (r "^0.1.20") (d #t) (k 0)) (d (n "del-msh") (r "^0.1.25") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0r62y38x2cfpjylqwd6wk7a48gqyakrpvqnj8dgdan3x1fkrzll7") (y #t)))

