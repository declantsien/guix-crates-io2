(define-module (crates-io de l- del-ls) #:use-module (crates-io))

(define-public crate-del-ls-0.1.0 (c (n "del-ls") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1jb22mbwsyrj0hjh807dy4qwbzy6qpaccs71dp73rx14xpanh8il")))

(define-public crate-del-ls-0.1.1 (c (n "del-ls") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "08h2x46r49lr42j17hra6r3alyszas18wfv7ymqpmhqqiiikvjk8")))

(define-public crate-del-ls-0.1.2 (c (n "del-ls") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1llvivg0gzfq0vjbhkwd7w7gx924bc0vp7mi5q1n27ph55d2y41a")))

(define-public crate-del-ls-0.1.3 (c (n "del-ls") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0z0pncf23lkzwrphrzm3zpbhl04czycfjy0bzmdb3pzhv6pxskq9")))

(define-public crate-del-ls-0.1.4 (c (n "del-ls") (v "0.1.4") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0ncf2s9z4ipzjrcn0h0pzhhb62z78wgvapmbdpirxx9dd9xpxzpd")))

