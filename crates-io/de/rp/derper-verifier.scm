(define-module (crates-io de rp derper-verifier) #:use-module (crates-io))

(define-public crate-derper-verifier-0.1.0 (c (n "derper-verifier") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "03l4azk424fr2zpbdgi3905f31i1cqbbb9kya7acvb63nx2g8fcj")))

