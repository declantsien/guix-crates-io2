(define-module (crates-io de rp derp) #:use-module (crates-io))

(define-public crate-derp-0.0.1 (c (n "derp") (v "0.0.1") (d (list (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "03rvy7y508jrgpglr668iz418vgb2jjcnami6wsm2gbdgcjdycw2")))

(define-public crate-derp-0.0.2 (c (n "derp") (v "0.0.2") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "1b9kszabxd2ps907427ph8dcixn36lnnaw5pxdrf0v7qxy1l6whg") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.3 (c (n "derp") (v "0.0.3") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0jlzvc6bc4wfji93gxa4g1flks8hhhn13c2w4qijb1c8xrja49lh") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.4 (c (n "derp") (v "0.0.4") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "07chay9r8y9fdkb1iha3rwn48hivj27m8c0rkj1i8llr3wjhfw9z") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.5 (c (n "derp") (v "0.0.5") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0pc2lfh1zl2jlrj3k4v4m817glmlw17dzmgvdcwjvq8d9z0glzcd") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.6 (c (n "derp") (v "0.0.6") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "00bcwr67p9d5hr0j5k6a8acma6bhzgdwlarl3qkr87dr0x1lx3gn") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.7 (c (n "derp") (v "0.0.7") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0lyw265cn4bixxqzg7xak8xw6imgnf6hr5cd475whf656y5hsyn2") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.8 (c (n "derp") (v "0.0.8") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0x80x8aqxawdkpd541r2f56avlr7rmikbp847p63pfsxzf29fg57") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.9 (c (n "derp") (v "0.0.9") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "19qha929cpxisas6bl37x8gyx3ig6y6srcx661kjghanmbarisq3") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.10 (c (n "derp") (v "0.0.10") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0npn41c74kbxjfjs6yqjh6yx7p3y0z69q1fx5jbcrg7pq0qaw346") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.11 (c (n "derp") (v "0.0.11") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.6") (d #t) (k 0)))) (h "1cabcdks0m3dya4lgaaizy6i56q470djp4b9c4gidrxa67q6qs4f") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.13 (c (n "derp") (v "0.0.13") (d (list (d (n "clap") (r "^2.23") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "pem") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.7") (d #t) (k 0)))) (h "1bqicbhcywqgc88akwmy4iy8kngcg98rg13q0021cmq1ix2fxw4l") (f (quote (("cli" "clap" "data-encoding" "pem"))))))

(define-public crate-derp-0.0.14 (c (n "derp") (v "0.0.14") (d (list (d (n "untrusted") (r "^0.7") (d #t) (k 0)))) (h "0sdi1n14070qi8127mjlpgwkpbp3x4jmcpi1k3j3g93gkgylrf69")))

