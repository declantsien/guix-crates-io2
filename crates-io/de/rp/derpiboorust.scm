(define-module (crates-io de rp derpiboorust) #:use-module (crates-io))

(define-public crate-derpiboorust-0.1.0 (c (n "derpiboorust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0r099ia5rj0hcg8gv0x46izz5ys1gr7i7nr9ghf4cn2zyk5p7611")))

(define-public crate-derpiboorust-0.1.1 (c (n "derpiboorust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1i4f4h72f9zippxmidm5fy7w5n1kpr9v3dz737d8j772z84p4mrh")))

