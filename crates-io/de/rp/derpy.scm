(define-module (crates-io de rp derpy) #:use-module (crates-io))

(define-public crate-derpy-0.1.0 (c (n "derpy") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.12") (d #t) (k 0)))) (h "1g28mdy151k4hr77dkdsk4i11nhl99a4qpj5r1cl8mdx3qcp2q60")))

