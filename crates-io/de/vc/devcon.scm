(define-module (crates-io de vc devcon) #:use-module (crates-io))

(define-public crate-devcon-0.1.0 (c (n "devcon") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1m95c3srhzf21pb8g3s1678bl7kw3c48a5rrhdnplz06pja60c2p")))

(define-public crate-devcon-0.2.0 (c (n "devcon") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "008v3kwm08qad39qmwc59mjg6n1a6np87wpyzdy5rzxqi5z3fscn")))

