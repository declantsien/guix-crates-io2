(define-module (crates-io de vc devc) #:use-module (crates-io))

(define-public crate-devc-0.0.0-alpha.1 (c (n "devc") (v "0.0.0-alpha.1") (h "0p4fr4y92pdscad80hzl3qr15npy32m3g962ysq02pjb0mm61qah")))

(define-public crate-devc-0.0.0-alpha.2 (c (n "devc") (v "0.0.0-alpha.2") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "devc-core") (r "^0.0.0-alpha.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qd8fq1as51rdml83mhc8f2kg6x0r3h2mlpivd50dg5byga8r9ck")))

