(define-module (crates-io de vc devcade_onboard_types) #:use-module (crates-io))

(define-public crate-devcade_onboard_types-0.1.0 (c (n "devcade_onboard_types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "13a8952ipbvlxjkl1dzdajlpxgws2xx98f1qxbvppv6dp3818dfw")))

