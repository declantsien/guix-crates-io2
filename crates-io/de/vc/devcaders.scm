(define-module (crates-io de vc devcaders) #:use-module (crates-io))

(define-public crate-devcaders-0.1.0 (c (n "devcaders") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)))) (h "0jwyz506i0y41n1ppajyvgn6mlr6c706iqb9hcwkk8x9lwg1y9xk")))

(define-public crate-devcaders-0.2.0 (c (n "devcaders") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)))) (h "1pi19zj1k2achid4bijpfi4s27qibci3jfwp0pxza21vqz7c5cdz")))

(define-public crate-devcaders-0.2.1 (c (n "devcaders") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)))) (h "0zrh665x0xjqzi8qflf4a8w61dl0ixkxmqql8b004ig1yrx43j4z")))

(define-public crate-devcaders-0.2.2 (c (n "devcaders") (v "0.2.2") (d (list (d (n "bevy") (r "^0.10.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)))) (h "1mg62k3y7fdplddm0qh1da5z478iq2japh90svlr0i3fid744qyg")))

(define-public crate-devcaders-0.3.0 (c (n "devcaders") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)))) (h "0ssz4q5pvl0r2sakvcsb9ygaxscxq5mv9m0109086fr3273jhlqf")))

(define-public crate-devcaders-0.3.1 (c (n "devcaders") (v "0.3.1") (d (list (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)))) (h "1ysmrvvvxqk4cbn6r51567skbil6765n2jzysvy71crsfbdj0sr3")))

(define-public crate-devcaders-0.4.0 (c (n "devcaders") (v "0.4.0") (d (list (d (n "async-compat") (r "^0.2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "devcade_onboard_types") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync" "net" "io-util" "rt"))) (d #t) (k 0)))) (h "0n2kz7scgixr89ikxxm8bkijxhr93bqchns1pdqnwlq252va6zd0")))

(define-public crate-devcaders-0.5.0 (c (n "devcaders") (v "0.5.0") (d (list (d (n "async-compat") (r "^0.2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.10.0") (k 0)) (d (n "devcade_onboard_types") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync" "net" "io-util" "rt"))) (d #t) (k 0)))) (h "1wpd293advkj8z20hswr0g3xm5qb73s3rbh9civxyrb9ab19y4kp")))

(define-public crate-devcaders-0.6.0 (c (n "devcaders") (v "0.6.0") (d (list (d (n "async-compat") (r "^0.2.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12.0") (f (quote ("multi-threaded"))) (k 0)) (d (n "devcade_onboard_types") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("sync" "net" "io-util" "rt"))) (d #t) (k 0)))) (h "07amsr4wggmwpbkdwcivpm4w18wirzmnll102xgr9vfmxdsvibbv")))

