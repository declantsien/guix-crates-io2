(define-module (crates-io de vt devtime) #:use-module (crates-io))

(define-public crate-devtime-0.1.0 (c (n "devtime") (v "0.1.0") (h "0mcl6ma1jj9ka0nf8r3jiwhwagywsfapv9f8944p31y6fciqny41")))

(define-public crate-devtime-0.2.0 (c (n "devtime") (v "0.2.0") (h "0dqzbqz0gksp257ki9lrn6nq9hjbskqzzgz8d3py0f5sb2d7gkp0")))

