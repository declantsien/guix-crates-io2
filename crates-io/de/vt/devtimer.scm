(define-module (crates-io de vt devtimer) #:use-module (crates-io))

(define-public crate-devtimer-1.0.0 (c (n "devtimer") (v "1.0.0") (h "1xh1jzafvrjq41wza0d1wgsv8lm5c5cwc9czaxccxknrwrbwy0xv")))

(define-public crate-devtimer-1.0.1 (c (n "devtimer") (v "1.0.1") (h "162y6w5f8g6874ghg238vchi29z66nwcfif8g1jfkafwf5wxs01c")))

(define-public crate-devtimer-1.1.0 (c (n "devtimer") (v "1.1.0") (h "0mmrv58qvjadshjabfj2sz0ypjs1x96cjblrj1frlc2f3zbf5v0d")))

(define-public crate-devtimer-1.1.1 (c (n "devtimer") (v "1.1.1") (h "0ppd28144aqfdj3n2h5lyh9k3hsi833pfqbh2sdb4zddbi3y2d9n")))

(define-public crate-devtimer-1.1.2 (c (n "devtimer") (v "1.1.2") (h "1wihpr0pxc6hprdc0fsg5qrf5b3pm32fvm1zpjlp6qmj8jqy3w4y")))

(define-public crate-devtimer-2.0.0 (c (n "devtimer") (v "2.0.0") (h "0wrcwz4srrswhzjwyjdzi96h8rf3jj19l481yn8yd1drl4vv1ni3")))

(define-public crate-devtimer-3.0.0 (c (n "devtimer") (v "3.0.0") (h "18n93vsxxkp6vhnd7av1q1zc217ms5rcf3bz46flj0il0gw17w2f")))

(define-public crate-devtimer-3.0.1 (c (n "devtimer") (v "3.0.1") (h "16ii33w15l1yqry9lqk1c8asaaj24kf364h4hd2x7gd2mjjw67nw")))

(define-public crate-devtimer-3.0.2 (c (n "devtimer") (v "3.0.2") (h "1cfg8k55fkqdnk6dpqyhlgy0lmiqbkdshgc2im91x31j2rk7ar81")))

(define-public crate-devtimer-3.0.3 (c (n "devtimer") (v "3.0.3") (h "03jyc1vwmwg189y1ghaz5dizfiyi9n7946m50c40gnr3kdhxwxim")))

(define-public crate-devtimer-4.0.0 (c (n "devtimer") (v "4.0.0") (h "0i5jx15g0iawr0w4glyc5kwvw124qphvb07gsxy67yab4jwvfdb0")))

(define-public crate-devtimer-4.0.1 (c (n "devtimer") (v "4.0.1") (h "1hdp8w6x5d8yaq0fy747ng5sxjb7lp0001ap8s4bkxljkaakjwwh")))

