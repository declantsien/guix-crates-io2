(define-module (crates-io de lf delfi) #:use-module (crates-io))

(define-public crate-delfi-0.0.1 (c (n "delfi") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17wdqsci7ph8afm1np0k7lmp5bzdmd6p7bq2azhzmi5iqhfx7hl1")))

(define-public crate-delfi-0.0.2 (c (n "delfi") (v "0.0.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09wibnf46p64jvikgpn430gdgiwcp5h0k493jgk1jfrn3xa5v22b")))

(define-public crate-delfi-0.0.3 (c (n "delfi") (v "0.0.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i0l5fq1a8zpv9c4nb3d0fxzn19l6wiis9a21gqi4m6s1f3yxvif")))

(define-public crate-delfi-0.0.4 (c (n "delfi") (v "0.0.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00da26zwb1dglh6sm6xl9f8nnp3yxy3vlmqyb268b5ldbbrcyjc8")))

(define-public crate-delfi-0.0.5 (c (n "delfi") (v "0.0.5") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1c706bqy3xlcbz4f7chgwg65li2vk0dhms2q6m3igd0ibmlrfkzp")))

(define-public crate-delfi-0.1.0 (c (n "delfi") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "delfi-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1xxzj397bi5r0pix1vznv2cr1l6wdyzyn7q33czys8qi3jr804vz") (f (quote (("macros" "delfi-macros"))))))

