(define-module (crates-io de lf delfast) #:use-module (crates-io))

(define-public crate-delfast-0.1.5 (c (n "delfast") (v "0.1.5") (h "1b8aqvi1c69nw4ky42kg04c4jg9lrd20jha7c5ba3l4arkffk0d3")))

(define-public crate-delfast-0.1.6 (c (n "delfast") (v "0.1.6") (h "0npvq59jzjd6wfbl6vcp4zl9dfxpajvdwhc77bd1s4p426ppf5k2")))

(define-public crate-delfast-0.1.7 (c (n "delfast") (v "0.1.7") (h "0fywwiqi3cry6b80xi2dddg972qdvba6y1khdixd8bab4z4bi3qq")))

(define-public crate-delfast-0.1.8 (c (n "delfast") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "19hb68bvyzp71kc162s980mgacdyri6fz2d95wpy4rhdfxnyaz99")))

(define-public crate-delfast-0.1.9 (c (n "delfast") (v "0.1.9") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fyp6gf9kviql3jpbwnn5xzv64a1qsip7m5frd0fisrgchybaq2")))

