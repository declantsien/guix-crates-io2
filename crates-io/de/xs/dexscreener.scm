(define-module (crates-io de xs dexscreener) #:use-module (crates-io))

(define-public crate-dexscreener-0.1.0 (c (n "dexscreener") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "alloc"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1922833m6697wq28r5kl8cblh79mmq3631f3gj870i9sbr4nsvm5")))

(define-public crate-dexscreener-0.2.0 (c (n "dexscreener") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "std" "alloc"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0xfwz3739c64q8zrzl6ch4s98nv06dyban5b8yyaxm8ps47r4ssc")))

