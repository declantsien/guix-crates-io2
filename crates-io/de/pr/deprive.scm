(define-module (crates-io de pr deprive) #:use-module (crates-io))

(define-public crate-deprive-0.1.0 (c (n "deprive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ab1vyqwhi3q92jcrkxffdkjq3xplbmaf79my3hyx0f3l4jnalhn")))

(define-public crate-deprive-0.2.0 (c (n "deprive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a3z8lybssjyihppcn5c653jzvp7gpssi83c8p15a7pp825kkshp")))

(define-public crate-deprive-0.2.1 (c (n "deprive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05k6syda2af51wq139cks5zjnm88ids7bwz26xjf18n5nkpd36kp")))

