(define-module (crates-io de pr deprecate-until) #:use-module (crates-io))

(define-public crate-deprecate-until-0.1.0 (c (n "deprecate-until") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (k 0)) (d (n "quote") (r "^1.0.29") (k 0)) (d (n "semver") (r "^1.0.17") (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "12zmmwb9bb7lcki1a7vnlbs0pwhhy3n0j0z57c1gv6n7s2if5hky") (r "1.60.0")))

(define-public crate-deprecate-until-0.1.1 (c (n "deprecate-until") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "semver") (r "^1.0.20") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "0177x1ifh5296phrgrib40s1gc23ifsj0f89msjybfzg4vw6fdvs") (r "1.60.0")))

