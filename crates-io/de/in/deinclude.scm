(define-module (crates-io de in deinclude) #:use-module (crates-io))

(define-public crate-deinclude-0.1.0 (c (n "deinclude") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "0xvvy048wm69b9ppcnqn1lyx567xjxh7jr1s8iiadg2nhr0bb5v2")))

(define-public crate-deinclude-0.1.1 (c (n "deinclude") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "0qfakii00nr3l7vp2xaxaa2pj2jw9ggkqnlc5hva463h5y76phrg")))

