(define-module (crates-io de in deindentor) #:use-module (crates-io))

(define-public crate-deindentor-1.0.0 (c (n "deindentor") (v "1.0.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z02h5nqh7r8xdihqh6c7zjnz316h2pjm2xmpwn9ycws592qbdzr")))

(define-public crate-deindentor-1.0.1 (c (n "deindentor") (v "1.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mgylrfnzjdd7a9swjf5gvrcpcmdqj7nrjphffk9cw77y8d03gjn")))

(define-public crate-deindentor-1.0.2 (c (n "deindentor") (v "1.0.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8zvx4d2x60h05bhcdfwdzjhf4q4rsvpyfna50zixkz8mddsr5p")))

(define-public crate-deindentor-1.1.0 (c (n "deindentor") (v "1.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04rwc4xdz09hv1ba6pqxmrn16c60f0b7q3gish9y8m5p7cgcm5y2")))

(define-public crate-deindentor-1.1.1 (c (n "deindentor") (v "1.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zzw4qr0p5v3b8v3qspiwmkwsmyh7z75rssx5w6894c5r051q9ij")))

