(define-module (crates-io de in deinflect) #:use-module (crates-io))

(define-public crate-deinflect-0.1.0 (c (n "deinflect") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "07w02nflq3kfpzwaxh5q8kj2rmq9lqvjvw4brd71vqdj4qnhgjj1")))

(define-public crate-deinflect-0.1.1 (c (n "deinflect") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0pdkc706qia4w8rblsbn8qr9x867brx3r0swbld12w1fvrh7gplq")))

(define-public crate-deinflect-0.1.2 (c (n "deinflect") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0cbf94a7y5bf4wdcsadq0p6byjlf4jang9r4ji9b3pd4nlgbx1v9")))

(define-public crate-deinflect-0.1.3 (c (n "deinflect") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0s4hcw6q32gdyfig242pwn5q5idbm5q9s4p5j0mfbnx656byrh5k")))

(define-public crate-deinflect-0.1.4 (c (n "deinflect") (v "0.1.4") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01rbhi42bfl9pzndl62k301agnpvji1kpqywkzl8kq2l4njn5gh5") (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde"))))))

