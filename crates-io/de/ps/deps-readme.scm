(define-module (crates-io de ps deps-readme) #:use-module (crates-io))

(define-public crate-deps-readme-0.1.0 (c (n "deps-readme") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c42j8n8l40r9zh3ls2wfnd0zxg8jghz0km6nj3y4h9z4fxmb324")))

