(define-module (crates-io de ps deps-gen) #:use-module (crates-io))

(define-public crate-deps-gen-0.0.1 (c (n "deps-gen") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1myijnafasz9v56w84rjcf5by6z6qslv5x1mx64dr2n4arp99rq1")))

(define-public crate-deps-gen-0.0.2 (c (n "deps-gen") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ri6mfcb5zd5xi2sgrnxlddwv4f0viql60pv5m8lwhskb2ar9xvx")))

(define-public crate-deps-gen-0.1.0 (c (n "deps-gen") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "19fwjg6109ar2gr7flj8xbjdfn5g0jfik7k7jc0xzf8i5jd3ica9")))

(define-public crate-deps-gen-0.1.1 (c (n "deps-gen") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "05plc8f7kqlf8npwak9ga8gbyvyin2xlri12wb8vvgbycnjsf1ha")))

(define-public crate-deps-gen-0.2.0 (c (n "deps-gen") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1igkrw7jm21nvy27rl6r63jx5a4drqq77msxfgg54wrr4ksi997y")))

