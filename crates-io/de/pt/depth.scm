(define-module (crates-io de pt depth) #:use-module (crates-io))

(define-public crate-depth-0.0.0 (c (n "depth") (v "0.0.0") (h "1n2kdsznfcz58n6qxfxczkhpi9gsn9495qq4xw568phkhlwl9nnw")))

(define-public crate-depth-0.0.1 (c (n "depth") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0prphq2pkmnz965j3ag6z2yhci0kq6ziakslkcxvp5v2qixg1y9c")))

(define-public crate-depth-0.0.2 (c (n "depth") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "01fzqy5gcrib1rkn5p056sv2w4d101j0kcyvx6cvirdnhf5s092k")))

(define-public crate-depth-0.0.3 (c (n "depth") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0f2cc4vdwgcr62jskmhz10imbjs5bfgydi8cvqf89dk0d07wfr98")))

(define-public crate-depth-0.0.4 (c (n "depth") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crates_io_api") (r "^0.8.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0cbxsfj9gifkv3cbw80dqwrd2md1gp8vairdvifsnr6ak8b947ly")))

