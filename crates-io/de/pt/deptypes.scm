(define-module (crates-io de pt deptypes) #:use-module (crates-io))

(define-public crate-deptypes-0.1.0 (c (n "deptypes") (v "0.1.0") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "generics2") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1w1n96ybvkfq9y8fd07whl9wh1adcv7fy4ga14v1qar1wj1zhnba") (f (quote (("std") ("nightly" "gat" "never") ("never") ("gat") ("default" "std")))) (y #t)))

(define-public crate-deptypes-0.1.1 (c (n "deptypes") (v "0.1.1") (d (list (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "generics2") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "18vysl7d5gsxacacyby53c3bwb7mi334xvw09ws7i7xjc44wjnfl") (f (quote (("std") ("nightly" "gat" "never") ("never") ("gat") ("default" "std"))))))

(define-public crate-deptypes-0.2.0 (c (n "deptypes") (v "0.2.0") (d (list (d (n "derive-where") (r "^1.2.7") (d #t) (k 0)) (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "generics2") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0r7v4cjngyw6kl31f74al7n0z0rlsbws3m8dbbnanwvbhxfl65pl") (f (quote (("trusted_len") ("std") ("nightly" "never" "trusted_len") ("never") ("default" "std")))) (r "1.66")))

(define-public crate-deptypes-0.2.1 (c (n "deptypes") (v "0.2.1") (d (list (d (n "derive-where") (r "^1.2.7") (d #t) (k 0)) (d (n "generativity") (r "^1.0.0") (d #t) (k 0)) (d (n "generics2") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0ax0nq4kgwdm523znmp5c3kqpjiw2v1g2zlmjfx3vvpza1fi78bg") (f (quote (("trusted_len") ("std") ("nightly" "never" "trusted_len") ("never") ("default" "std")))) (r "1.66")))

