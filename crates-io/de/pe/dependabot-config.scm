(define-module (crates-io de pe dependabot-config) #:use-module (crates-io))

(define-public crate-dependabot-config-0.1.0 (c (n "dependabot-config") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.5.2") (f (quote ("std" "serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.16") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0kg1ah43ypgnsyv8k4z0pnr1s7r5i96928bmv64njf1yz0q1l110")))

(define-public crate-dependabot-config-0.2.0 (c (n "dependabot-config") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.5.2") (f (quote ("std" "serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.16") (d #t) (k 0)))) (h "0b4zmw60cq593hl6dr3f30622pmhy13lxpmnyyz415gipvxh2igh")))

(define-public crate-dependabot-config-0.2.1 (c (n "dependabot-config") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.5.2") (f (quote ("std" "serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.16") (d #t) (k 0)))) (h "0zvvsf6d0wjd9003qhqi5135g9d0sd5f3sicflcdyv0gxrfqmc8d") (r "1.51")))

(define-public crate-dependabot-config-0.2.2 (c (n "dependabot-config") (v "0.2.2") (d (list (d (n "indexmap") (r "^1.5.2") (f (quote ("std" "serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0gww8awl872n3v62571hwgfrpnvanfl0n8flv8ygzfza417j28kj") (r "1.58")))

(define-public crate-dependabot-config-0.3.0 (c (n "dependabot-config") (v "0.3.0") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0q8y8p6g0x3c5lanjcfyjrvrsgnsvhdsgj7yippkf437ra1lvzfa") (r "1.64")))

(define-public crate-dependabot-config-0.3.1 (c (n "dependabot-config") (v "0.3.1") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1zjdzx0flgv619fdfwvpn4689r0mpjmjcj6wfi34jaagr32blg41") (r "1.64")))

(define-public crate-dependabot-config-0.3.2 (c (n "dependabot-config") (v "0.3.2") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1ilhx84bwd089y5d2cvjgjsjdwn9c85snq9i941vc0d690f0jjq9") (r "1.64")))

(define-public crate-dependabot-config-0.3.3 (c (n "dependabot-config") (v "0.3.3") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "07h1q7sakmjn1plf7r0nx8icbv4ghw1s6kvix4zhxqr3q40r3knl") (r "1.64")))

(define-public crate-dependabot-config-0.3.4 (c (n "dependabot-config") (v "0.3.4") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.165") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0y8w548pn51fbgv9d58a1b4gbql66p1fba6pvsh9nrvl6hjs5hc1") (r "1.64")))

