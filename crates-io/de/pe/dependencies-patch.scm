(define-module (crates-io de pe dependencies-patch) #:use-module (crates-io))

(define-public crate-dependencies-patch-0.1.0 (c (n "dependencies-patch") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "11aja4b50c7mb23zx7ws4jn6360h4j0yvyranibrnabbd6rxv9wz")))

(define-public crate-dependencies-patch-0.2.0 (c (n "dependencies-patch") (v "0.2.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1w78x8k8niiafs65qpyjk2dn5422kk704vbi32bsf7z0lv94swzh")))

