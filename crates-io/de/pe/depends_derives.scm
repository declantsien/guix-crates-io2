(define-module (crates-io de pe depends_derives) #:use-module (crates-io))

(define-public crate-depends_derives-0.1.0 (c (n "depends_derives") (v "0.1.0") (d (list (d (n "depends_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ry94nvpr11kqrajc0qj6ygc4qx8gdc22xvzgld66ia3gl06vy35")))

(define-public crate-depends_derives-0.2.0 (c (n "depends_derives") (v "0.2.0") (d (list (d (n "depends_core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0h7qz1gzc10pl4j6cnhqbn1qivjc30x0226ryfbz9sz2phjhwij7")))

(define-public crate-depends_derives-0.3.0 (c (n "depends_derives") (v "0.3.0") (d (list (d (n "depends_core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "12dnmlhpn654b41p4a0jaxs2ihqq9adm37cnz2w8777ixc6h05r8")))

(define-public crate-depends_derives-0.4.0 (c (n "depends_derives") (v "0.4.0") (d (list (d (n "depends_core") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00k10pb4sp02gkqx8615h1z84jj3afa42xhh673sx8irlxq2nyxg")))

(define-public crate-depends_derives-0.4.1 (c (n "depends_derives") (v "0.4.1") (d (list (d (n "depends_core") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1885d3lqjdx48h6qrki44jrdld6br8jja7m2xyl7mghxvbfwhh8q")))

(define-public crate-depends_derives-0.5.0 (c (n "depends_derives") (v "0.5.0") (d (list (d (n "depends_core") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0r7qra6a672900kyk172059s7za65wijva5y49phykc57hx3ds5y")))

(define-public crate-depends_derives-0.6.0 (c (n "depends_derives") (v "0.6.0") (d (list (d (n "depends_core") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0x0hg9fma1mgwd6am8rhd3nmvxp0wkimy8sjx9qnp838v7w03dva")))

(define-public crate-depends_derives-0.7.0 (c (n "depends_derives") (v "0.7.0") (d (list (d (n "depends_core") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1cvh05cvjsq400vmrncj279zz563l18jhq39nx2dzi8kz564ckdq")))

(define-public crate-depends_derives-0.7.1 (c (n "depends_derives") (v "0.7.1") (d (list (d (n "depends_core") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0qwrg0kj19yf4gdqapf9zw4bqz6q6dq6cphq5f43yqr48pcvwny4")))

(define-public crate-depends_derives-0.8.0 (c (n "depends_derives") (v "0.8.0") (d (list (d (n "depends_core") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "17slsmjb4mvyb6xmcca9n4ykiibjfk7srvnzx726n3aa93pm6ya3") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.9.0 (c (n "depends_derives") (v "0.9.0") (d (list (d (n "depends_core") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1nrxhajxq6d9gpmywn2nsgdkmwbx7lfgw1fq59bzxfjkz9qsmzm2") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.9.1 (c (n "depends_derives") (v "0.9.1") (d (list (d (n "depends_core") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "03yi35g5pmpqhd6mh4l9l45zdg656i2mgjpbkzlpxnp1b6x4hq9y") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.9.2 (c (n "depends_derives") (v "0.9.2") (d (list (d (n "depends_core") (r "^0.9.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "06qbk190q8zpkb4i6gn60w72rc93143jj3rjns9kwh30qp38xisk") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.10.0 (c (n "depends_derives") (v "0.10.0") (d (list (d (n "depends_core") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0cjxhvjpyk1p0l5r9pz3zmhwfb9wiz1pwsp1qvyh9kgk5yvdpwiy") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.10.1 (c (n "depends_derives") (v "0.10.1") (d (list (d (n "depends_core") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1cg56liaypwx846524x5rimcwlppp6q0vvx82szkh2yp2a1v86gc") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

(define-public crate-depends_derives-0.10.2 (c (n "depends_derives") (v "0.10.2") (d (list (d (n "depends_core") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1n3mh4n3kfsf4vwbydgxhp7wapcks4zn86rkcm0xqffh503nalhn") (f (quote (("graphviz" "depends_core/graphviz") ("default"))))))

