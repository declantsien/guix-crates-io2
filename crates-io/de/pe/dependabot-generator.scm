(define-module (crates-io de pe dependabot-generator) #:use-module (crates-io))

(define-public crate-dependabot-generator-0.1.0 (c (n "dependabot-generator") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0m9fw231zjqhp8h5c1fmiw6ag1hn4s30rl961nair8qdw2w8n1j5")))

(define-public crate-dependabot-generator-0.1.1 (c (n "dependabot-generator") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dependabot-config") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0gzh2ay2c1wpb9hxzjp2qs53qzqkzncmab4rzyw4p39mdfvdr8x7")))

(define-public crate-dependabot-generator-0.1.2 (c (n "dependabot-generator") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dependabot-config") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1rchraka4ig3ymqgy8m9j4m3nwkz85fil1hhs4hxr3l7yly5h2sz")))

(define-public crate-dependabot-generator-0.1.3 (c (n "dependabot-generator") (v "0.1.3") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dependabot-config") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0ffsf31axachbz2iic4brfn742z1pcfkfvfb1sz7cp42810fvwk8")))

