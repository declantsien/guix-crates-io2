(define-module (crates-io de pe depends) #:use-module (crates-io))

(define-public crate-depends-0.1.0 (c (n "depends") (v "0.1.0") (d (list (d (n "depends_core") (r "^0.1.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.1.0") (d #t) (k 0)))) (h "1hrb4l98pnf3865akrlqfjcmfy4j72vk731f94kfdnrwdjy6ghgp") (f (quote (("graphviz") ("default"))))))

(define-public crate-depends-0.2.0 (c (n "depends") (v "0.2.0") (d (list (d (n "depends_core") (r "^0.2.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.2.0") (d #t) (k 0)))) (h "0p2h6lzj8zgkdikxhmz79jl2l8da2b185ah94ifv4lks7j8gk4s4") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.3.0 (c (n "depends") (v "0.3.0") (d (list (d (n "depends_core") (r "^0.3.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.3.0") (d #t) (k 0)))) (h "0yarv7y2ai8pi5avq17g7fw4vy5cn94pmwivn1n3vclncygixsqc") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.4.0 (c (n "depends") (v "0.4.0") (d (list (d (n "depends_core") (r "^0.4.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.4.0") (d #t) (k 0)))) (h "0lsn51yzw6ap0di9pdpknrmjln4n2z300a0039afmgvd7zwhx58g") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.4.1 (c (n "depends") (v "0.4.1") (d (list (d (n "depends_core") (r "^0.4.1") (d #t) (k 0)) (d (n "depends_derives") (r "^0.4.1") (d #t) (k 0)))) (h "13kazdydk3brbxhibwajzd0y0lskb2wx8z2dqpbj5q5q4sw8mz11") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.5.0 (c (n "depends") (v "0.5.0") (d (list (d (n "depends_core") (r "^0.5.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.5.0") (d #t) (k 0)))) (h "0zjmbjx7wsry3giyad2r80qzdwwcngkljs12zl991c93n4paij5k") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.6.0 (c (n "depends") (v "0.6.0") (d (list (d (n "depends_core") (r "^0.6.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.6.0") (d #t) (k 0)))) (h "093j5mn08pk218whsc9ylda36d0agfas81xf66xsiqq3a91wb22p") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.7.0 (c (n "depends") (v "0.7.0") (d (list (d (n "depends_core") (r "^0.7.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.7.0") (d #t) (k 0)))) (h "0c2631a3kh9hgfj6jzda1cxz9x7jzdc7rssrcqi438254rys082g") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.7.1 (c (n "depends") (v "0.7.1") (d (list (d (n "depends_core") (r "^0.7.1") (d #t) (k 0)) (d (n "depends_derives") (r "^0.7.1") (d #t) (k 0)))) (h "0fpma07flx3lbf56szcd23f6k98c74xpxmpi992r9rdrq9cgc0xw") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz") ("default"))))))

(define-public crate-depends-0.8.0 (c (n "depends") (v "0.8.0") (d (list (d (n "depends_core") (r "^0.8.0") (d #t) (k 0)) (d (n "depends_derives") (r "^0.8.0") (d #t) (k 0)))) (h "1w519qpdcbxy8karlh5ylx8i6x657gacn49vw9h9y1lk7kqn8srz") (f (quote (("hashbrown" "depends_core/hashbrown") ("graphviz" "depends_core/graphviz" "depends_derives/graphviz") ("default"))))))

(define-public crate-depends-0.9.0 (c (n "depends") (v "0.9.0") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0g2r1ib4ph58sf2p4l62v3ds8bzmprk9c3ywa4cbklkws22fmmhl") (f (quote (("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

(define-public crate-depends-0.9.1 (c (n "depends") (v "0.9.1") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.9.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "02z42g9ak33dhhhijbblf8sl43hi2drlh8rf2a7c8zan22y3qavl") (f (quote (("test-utils") ("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

(define-public crate-depends-0.9.2 (c (n "depends") (v "0.9.2") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.9.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0caki658vd5n0csbqlnwirsaga0bkd1a98hb2704m4rgvb35pn1d") (f (quote (("test-utils") ("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

(define-public crate-depends-0.10.0 (c (n "depends") (v "0.10.0") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.10.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0pncxbrx0xzr54dngai0ddg0rlqynwh8nnvks874qkc9nkmi5n7l") (f (quote (("test-utils") ("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

(define-public crate-depends-0.10.1 (c (n "depends") (v "0.10.1") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.10.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0a2m66gdz278b9yaha448hg6arlrk98xrc0iznzk4xpqba5jcmni") (f (quote (("test-utils") ("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

(define-public crate-depends-0.10.2 (c (n "depends") (v "0.10.2") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "depends_derives") (r "^0.10.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("ahash"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0vfadldrgcb1bzsqvc7ghazlnaqa95q7bpm0xd5rbapfv93xg6cj") (f (quote (("test-utils") ("graphviz" "depends_derives/graphviz") ("default")))) (s 2) (e (quote (("hashbrown" "dep:ahash" "dep:hashbrown"))))))

