(define-module (crates-io de pe dependency-refresh) #:use-module (crates-io))

(define-public crate-dependency-refresh-0.1.3 (c (n "dependency-refresh") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "0n1pp874mvhqgb7jq4md6x65c2dzp7k8j3whf23bmcfxgzp8r9hb")))

(define-public crate-dependency-refresh-0.1.4 (c (n "dependency-refresh") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1whzq3lcjnyhia86w79kf5d0v2naz2lj0zib94v7k1xmvhg9r33z")))

(define-public crate-dependency-refresh-0.1.5 (c (n "dependency-refresh") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "1v0mmk3x953f91f9dr7qkvqy348xc4h4miwlqjkrgn15m3ym32qg")))

(define-public crate-dependency-refresh-0.1.6 (c (n "dependency-refresh") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "1hf0fb42vjqr6cmzj4xyh0wxd8j05pzacsip27b7p3qpf37dmn78")))

(define-public crate-dependency-refresh-0.1.7 (c (n "dependency-refresh") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "1b04239g3g0aqai74idnbmdwbgan0lqrb7cfq92kyrygb7d2g0bq")))

(define-public crate-dependency-refresh-0.1.8 (c (n "dependency-refresh") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "03ddmmq2955df4n9f8yhpcx473hyk9lbr3qdssvdjz903mlhfzwn")))

(define-public crate-dependency-refresh-0.1.9 (c (n "dependency-refresh") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "05185yb4nyscgcy9zxvh7fcz51w9j3cc76sazcnxvdv1800a867x")))

(define-public crate-dependency-refresh-0.1.10 (c (n "dependency-refresh") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "1k7v4vd64zcwk4la6h8ax3vbxpa7l0y4nrkxvfkkqanph9737ink")))

(define-public crate-dependency-refresh-0.1.11 (c (n "dependency-refresh") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1s6g0my4cq54jif4lksgb1pm3vgqgfljca9ypdj3cpk6v1pm3mjp")))

(define-public crate-dependency-refresh-0.1.12 (c (n "dependency-refresh") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0xsajd34jdf8b3zb42szhspy0ih3m81hvqrp7kzjlsf5djwfrws0")))

(define-public crate-dependency-refresh-0.2.0 (c (n "dependency-refresh") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "09w2a3k8b3mpc0r781w6gkjnr2xbjh874vkaw0k50njcghaqagwg")))

(define-public crate-dependency-refresh-0.3.0 (c (n "dependency-refresh") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)))) (h "1r9qx5b5gdcpw7d8q51n4am4bs27xn06y0km75khk860k6vksv3x")))

