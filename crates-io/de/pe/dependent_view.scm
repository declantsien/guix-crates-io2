(define-module (crates-io de pe dependent_view) #:use-module (crates-io))

(define-public crate-dependent_view-1.0.0 (c (n "dependent_view") (v "1.0.0") (h "09qn0k39hi57b12218wgsk0zrricn17wx55d9253s2v79sfxwl3s")))

(define-public crate-dependent_view-1.0.1 (c (n "dependent_view") (v "1.0.1") (h "11l74zl39339lqcvp76cdbvd4vxgs12p7zr0ncdvky3digb511zw")))

(define-public crate-dependent_view-1.0.2 (c (n "dependent_view") (v "1.0.2") (h "19lkzzvb0piafr0km3g84mirmvv6ly19l6diamikngyj5lj0yj2g")))

