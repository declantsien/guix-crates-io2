(define-module (crates-io de pe dependy) #:use-module (crates-io))

(define-public crate-dependy-0.2.0 (c (n "dependy") (v "0.2.0") (d (list (d (n "daggy") (r "^0.4.1") (d #t) (k 0)))) (h "0wb4c6k00z5y54scigdjzz7180j7d0m4n8zs4p2839krw0bq3bq1")))

(define-public crate-dependy-0.2.1 (c (n "dependy") (v "0.2.1") (d (list (d (n "daggy") (r "^0.4.1") (d #t) (k 0)))) (h "1g63x5k9rhys1gc15q0y7kanadim0rhpz1vwqp8pk9mhr4r948s3")))

(define-public crate-dependy-0.3.0 (c (n "dependy") (v "0.3.0") (d (list (d (n "daggy") (r "^0.4.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.9") (d #t) (k 0)))) (h "1w7i0bkmi7bccmyqxhljflgv4ifkia8dz228mb8lmg7s9vjif24y")))

(define-public crate-dependy-0.4.0 (c (n "dependy") (v "0.4.0") (d (list (d (n "daggy") (r "^0.5.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)))) (h "138s5zdniki5fjzfmllw06anzk2sn4i27gc95gg5d7csxbmdlfc3")))

(define-public crate-dependy-0.4.1 (c (n "dependy") (v "0.4.1") (d (list (d (n "daggy") (r "^0.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1xc9c5m1ckq8x8c8wi78xy3y26shs1jwvh24zr4n7a6rhnrfj9az")))

