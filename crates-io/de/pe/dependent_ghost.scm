(define-module (crates-io de pe dependent_ghost) #:use-module (crates-io))

(define-public crate-dependent_ghost-0.1.0 (c (n "dependent_ghost") (v "0.1.0") (h "0ardi8php5l8a15vy5bvpj2ggylnyah78480limhvrml96bh1w1h")))

(define-public crate-dependent_ghost-0.1.1 (c (n "dependent_ghost") (v "0.1.1") (h "1zl88jdyyhlqj3553ds7im8hjk141pb4hb0b30xir9chz03jngsc")))

