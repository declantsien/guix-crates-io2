(define-module (crates-io de pe dependent_sort) #:use-module (crates-io))

(define-public crate-dependent_sort-0.0.0 (c (n "dependent_sort") (v "0.0.0") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)))) (h "03lj8rfg8w2q2q0bkfq9alx7hzp14g4rv5piq0wm9ckj8gfjxgn2") (f (quote (("default"))))))

(define-public crate-dependent_sort-0.1.0 (c (n "dependent_sort") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "0fbiawf32agrw35bgz28drjqfacj3npizpspsqqxic11mhm2w20i") (f (quote (("default"))))))

(define-public crate-dependent_sort-0.1.1 (c (n "dependent_sort") (v "0.1.1") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "0x48jm4w6a57bxwplfvnxrn8xiwilrf15qr3sm5011dckb4bjmdi") (f (quote (("default"))))))

(define-public crate-dependent_sort-0.2.0 (c (n "dependent_sort") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "13hshf5kv7qhcgar6c4nkiibza82c01apcgkank1y44vi6mv48b7") (f (quote (("default"))))))

