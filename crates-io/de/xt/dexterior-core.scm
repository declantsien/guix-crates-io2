(define-module (crates-io de xt dexterior-core) #:use-module (crates-io))

(define-public crate-dexterior-core-0.1.0 (c (n "dexterior-core") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "mshio") (r "^0.4.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0s9hlzs20lyg6wcpjwsr7zfmyp5p7v8xrvbfx38s86vaw5kfrcxq")))

