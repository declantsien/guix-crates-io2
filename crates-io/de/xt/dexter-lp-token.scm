(define-module (crates-io de xt dexter-lp-token) #:use-module (crates-io))

(define-public crate-dexter-lp-token-1.0.0 (c (n "dexter-lp-token") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20-base") (r "^1.0.1") (f (quote ("library"))) (d #t) (k 0)) (d (n "dexter") (r "^1.3.0") (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1ws79d0clfnxm8azmjb1lb2jkzmdb6sdjydkc84bkgvlv4zmag8l") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

