(define-module (crates-io de xt dexterous_developer_builder) #:use-module (crates-io))

(define-public crate-dexterous_developer_builder-0.1.0 (c (n "dexterous_developer_builder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "debounce") (r "^0.2") (d #t) (k 0)) (d (n "dexterous_developer_types") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "which") (r "^6") (d #t) (k 0)))) (h "112nf331wnizmyqpsdrc1sb0kxiyjx8w06fr5y4vj71gq70bxjns")))

(define-public crate-dexterous_developer_builder-0.2.0 (c (n "dexterous_developer_builder") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "debounce") (r "^0.2") (d #t) (k 0)) (d (n "dexterous_developer_types") (r "^0.2.0") (d #t) (k 0)) (d (n "notify") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "which") (r "^6") (d #t) (k 0)))) (h "0wdx9gg86f1k4x30c2h58mdfjlq7sgbwdazc6gszspwhcbx68g9l")))

