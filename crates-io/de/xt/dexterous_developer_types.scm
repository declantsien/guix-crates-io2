(define-module (crates-io de xt dexterous_developer_types) #:use-module (crates-io))

(define-public crate-dexterous_developer_types-0.1.0 (c (n "dexterous_developer_types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "17ny8csixha4ygamnfm3jx2jhfj28lf13z2d19szki69hxqmaddd")))

(define-public crate-dexterous_developer_types-0.2.0 (c (n "dexterous_developer_types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gpl87b5kaq5d26z2j7jcisy3y9acchzls6wm9fa1jxxczdvmlvq")))

