(define-module (crates-io de xt dexterous_developer_dynamic) #:use-module (crates-io))

(define-public crate-dexterous_developer_dynamic-0.0.9 (c (n "dexterous_developer_dynamic") (v "0.0.9") (d (list (d (n "dexterous_developer_internal") (r "^0.0.9") (k 0)))) (h "1pq75rwskpg3f0ivivbhbvlqapy5a02z503z8ql4rp7mslzvjz4p")))

(define-public crate-dexterous_developer_dynamic-0.0.10 (c (n "dexterous_developer_dynamic") (v "0.0.10") (d (list (d (n "dexterous_developer_internal") (r "^0.0.10") (k 0)))) (h "0jfkdg281q1w4rdnw7y6jlckcr4dbbyzps2gqb7p8jarv79qi7mb")))

(define-public crate-dexterous_developer_dynamic-0.0.11-pre.1 (c (n "dexterous_developer_dynamic") (v "0.0.11-pre.1") (d (list (d (n "dexterous_developer_internal") (r "^0.0.11-pre.1") (k 0)))) (h "1rks3kzyf8lbgq5sv0p4lq1cib8k2n55nv5j1f1k9fk3mhb3d65z")))

(define-public crate-dexterous_developer_dynamic-0.0.11 (c (n "dexterous_developer_dynamic") (v "0.0.11") (d (list (d (n "dexterous_developer_internal") (r "^0.0.11") (k 0)))) (h "0qkk5n18rs8lbdd98qxgavqcn7r1wagchpi1jssvvmp16a6bw8il")))

(define-public crate-dexterous_developer_dynamic-0.0.12 (c (n "dexterous_developer_dynamic") (v "0.0.12") (d (list (d (n "dexterous_developer_internal") (r "^0.0.12") (k 0)))) (h "1vsbcxg68v7fzyjzn31cnhxp4978rsq1vha72iqrixqqjcfvfnjs")))

(define-public crate-dexterous_developer_dynamic-0.1.0 (c (n "dexterous_developer_dynamic") (v "0.1.0") (d (list (d (n "dexterous_developer_internal") (r "^0.1.0") (k 0)))) (h "11qw3gl6h68xv3b8sl6m82chb70743sw7hjs290wi0gjnnqsd1yr")))

(define-public crate-dexterous_developer_dynamic-0.2.0 (c (n "dexterous_developer_dynamic") (v "0.2.0") (d (list (d (n "bevy_dexterous_developer") (r "^0.2.0") (o #t) (k 0)) (d (n "dexterous_developer_internal") (r "^0.2.0") (k 0)))) (h "1rwi1zphs7nnyrkw609722msdkkx7chlr2d778bs4k9if2h3j695") (s 2) (e (quote (("bevy" "dep:bevy_dexterous_developer"))))))

