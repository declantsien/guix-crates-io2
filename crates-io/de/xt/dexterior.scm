(define-module (crates-io de xt dexterior) #:use-module (crates-io))

(define-public crate-dexterior-0.1.0 (c (n "dexterior") (v "0.1.0") (d (list (d (n "dexterior-core") (r "^0.1.0") (d #t) (k 0)) (d (n "dexterior-visuals") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "1ps72ckwad4nr2b5rsnzdq97zym328mj56pwpchk2mbgqzib8xym") (f (quote (("visuals" "dexterior-visuals") ("default"))))))

(define-public crate-dexterior-0.1.1 (c (n "dexterior") (v "0.1.1") (d (list (d (n "dexterior-core") (r "^0.1.0") (d #t) (k 0)) (d (n "dexterior-visuals") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "1pzdh4w05hgb5q114kyb3p5y0lvhm1pqhi631q6gff1bqp6bfb0r") (f (quote (("visuals" "dexterior-visuals") ("default"))))))

