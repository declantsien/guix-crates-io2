(define-module (crates-io de vb devbox-test-args) #:use-module (crates-io))

(define-public crate-devbox-test-args-0.1.0 (c (n "devbox-test-args") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iw1fdfmr91z47jp2gds7lpfa94q9sjl1j833y6kijsby46a8hhk")))

(define-public crate-devbox-test-args-0.1.1 (c (n "devbox-test-args") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17h0q5nvlypgi5x30b59qzr1q5khydv99lfncv4aibcl3kp4wigp")))

(define-public crate-devbox-test-args-0.1.2 (c (n "devbox-test-args") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pf8ngfcqv3jcxgaqx8yahk133g88w4qrvmmbvdlkp5kxhfqd4nq")))

