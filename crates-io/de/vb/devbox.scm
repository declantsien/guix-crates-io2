(define-module (crates-io de vb devbox) #:use-module (crates-io))

(define-public crate-devbox-0.1.0 (c (n "devbox") (v "0.1.0") (d (list (d (n "devbox-build") (r "^0.1.0") (d #t) (k 0)) (d (n "devbox-test-args") (r "^0.1.2") (d #t) (k 0)))) (h "186qf2zkb2xmdcghxq3mncg9f6vksxa2zq6bbvcrq6l7zhsws1i8")))

(define-public crate-devbox-0.1.1 (c (n "devbox") (v "0.1.1") (d (list (d (n "devbox-build") (r "^0.1.1") (d #t) (k 0)) (d (n "devbox-test-args") (r "^0.1.2") (d #t) (k 0)))) (h "15r74yg1yvr2305kdcanq2a68vr1i37x00q7k7khn74rj5vbv90c")))

