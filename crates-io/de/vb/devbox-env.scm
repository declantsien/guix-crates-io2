(define-module (crates-io de vb devbox-env) #:use-module (crates-io))

(define-public crate-devbox-env-0.1.0 (c (n "devbox-env") (v "0.1.0") (h "1j638yadpw155gjivnqq8frblj6h9bmr0r1l5vnnl5p2j8v2i701")))

(define-public crate-devbox-env-1.1.0 (c (n "devbox-env") (v "1.1.0") (h "1k78a10wzhxhh3za5dpa0s3ssaky5ks9kbjg8p10jmh4pbsy5h4m")))

(define-public crate-devbox-env-1.1.1 (c (n "devbox-env") (v "1.1.1") (h "0bbdxcxx95kzvkzsm9zjnczkh3kwff71in2ngnx6lviiqzgck5ql")))

