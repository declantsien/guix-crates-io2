(define-module (crates-io de vb devbox-build) #:use-module (crates-io))

(define-public crate-devbox-build-0.1.0 (c (n "devbox-build") (v "0.1.0") (d (list (d (n "devbox-test-args") (r "^0.1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "globset") (r "^0.4.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17h4riicp3p85hd00pnvhnzy94w6ar5zvccnv10afydz6zb2ssyz")))

(define-public crate-devbox-build-0.1.1 (c (n "devbox-build") (v "0.1.1") (d (list (d (n "devbox-test-args") (r "^0.1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "globset") (r "^0.4.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0fxfbv49qrz41d5scrx9dyl16621gw96zlz0izcy94i0gaa62dbg")))

