(define-module (crates-io de vb devbit_99bugs_led_display_driver) #:use-module (crates-io))

(define-public crate-devbit_99bugs_led_display_driver-0.2.1 (c (n "devbit_99bugs_led_display_driver") (v "0.2.1") (d (list (d (n "spidev") (r "^0.5.0") (d #t) (k 0)))) (h "1pjhvgf0r2zpkccyhm3qvr077s2q2b61zljw24yvmvpld4qib1ay")))

