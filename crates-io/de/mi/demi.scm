(define-module (crates-io de mi demi) #:use-module (crates-io))

(define-public crate-demi-0.1.0 (c (n "demi") (v "0.1.0") (h "1ljylyn3jjv1a73lbj9kkwzhf9fn0682rdrnzdba8kd8kp68qnfm")))

(define-public crate-demi-0.1.2 (c (n "demi") (v "0.1.2") (h "0as8ly74qy88i3plnrpfhsnanipabshrvw78js8nncpjfnd9b2zy")))

(define-public crate-demi-0.1.3 (c (n "demi") (v "0.1.3") (h "03qi967386y1vfflp6nvafwd46g6blqrczpkqwbxyln5z287srnj") (y #t)))

