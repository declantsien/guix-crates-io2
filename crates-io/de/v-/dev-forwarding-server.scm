(define-module (crates-io de v- dev-forwarding-server) #:use-module (crates-io))

(define-public crate-dev-forwarding-server-0.1.0 (c (n "dev-forwarding-server") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-reverse-proxy") (r "^0.4") (d #t) (k 0)))) (h "0pkk6k6zm5qv79kxflapk1v3h589inl0m82mmkp2h4w6z87rm1a5")))

