(define-module (crates-io de v- dev-workspaces) #:use-module (crates-io))

(define-public crate-dev-workspaces-0.1.0 (c (n "dev-workspaces") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "1rkiizwqkd5qmiajgqdkwlbx8wnzfj11n2sr1lasnw18nqji0qv1")))

(define-public crate-dev-workspaces-0.2.0 (c (n "dev-workspaces") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "047i6bxmjv9xx3mm4j41v5wrvzxjaxr1ycar72mifgm8jqwdpgk9")))

