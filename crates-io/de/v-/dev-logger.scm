(define-module (crates-io de v- dev-logger) #:use-module (crates-io))

(define-public crate-dev-logger-0.1.0 (c (n "dev-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1jmlzl3vx745wdk3wrgfdqrz1dpi87pvj5w7yhm7ymwp2h49g185")))

