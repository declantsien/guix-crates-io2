(define-module (crates-io de si designal) #:use-module (crates-io))

(define-public crate-designal-0.1.0 (c (n "designal") (v "0.1.0") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1axh4bk1d17kv4rpbmv9rplampxd08mxv0921raiff73w50zvv89")))

(define-public crate-designal-0.2.0 (c (n "designal") (v "0.2.0") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "04krj375sq23rdigahqji1pnil4db5w8dywh02z0xgszz4xhh5qi")))

(define-public crate-designal-0.2.1 (c (n "designal") (v "0.2.1") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rn155ys2443p62dp9jvx3gpkmicvhsj3iqig9j5018ndk86zzz0")))

(define-public crate-designal-0.2.2 (c (n "designal") (v "0.2.2") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0wadci1ixnwav15smzqxcr7rw54k1cy31f6z0mi80d1bbh007qr7")))

(define-public crate-designal-0.2.3 (c (n "designal") (v "0.2.3") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fngjwzc07applinw1s2kjkyfilsl1vzi46qnfkyzmm77gzqq724")))

(define-public crate-designal-0.2.4 (c (n "designal") (v "0.2.4") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1im3gj2ydcmhvfd6mv1lsqwzjnssi0vfr60s740bzp19nh4lpzl2")))

(define-public crate-designal-0.3.0 (c (n "designal") (v "0.3.0") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nyxc6xm4g5ldp3m93fc816g5ca26vgkz0piily66iwa0xk3jd58")))

(define-public crate-designal-0.3.1 (c (n "designal") (v "0.3.1") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1slkdwa9yky5yqbgj1zbxfcld5lc36v9zhf9c7m55cpl5bdkvfcq")))

(define-public crate-designal-0.3.2 (c (n "designal") (v "0.3.2") (d (list (d (n "futures-signals") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1iv2mmrh4z726wz5dd1sjsf1812gfj5ywpgv1bja2wlfmwqxsbqw")))

