(define-module (crates-io de si desimplex) #:use-module (crates-io))

(define-public crate-desimplex-0.1.0 (c (n "desimplex") (v "0.1.0") (d (list (d (n "noise") (r "^0.5.1") (d #t) (k 0)) (d (n "schedule_recv") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)))) (h "1291p9v89pg8cj89s7sw7r7ywp6llm3q37lw6azv7k3fza3mbnx7")))

