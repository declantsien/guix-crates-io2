(define-module (crates-io de si designerhelper-rs) #:use-module (crates-io))

(define-public crate-DesignerHelper-rs-0.1.0 (c (n "DesignerHelper-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0g3w8dcvnw69yqhnzw4d4rsw9gyb5rc1bsk2z7xdy4fnzbh611jr")))

(define-public crate-DesignerHelper-rs-0.1.1 (c (n "DesignerHelper-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "05cl0h1pgbpyqiwadd2qcpnfafrg2g35cj5nixh2mw878i8n0rqj")))

(define-public crate-DesignerHelper-rs-0.1.2 (c (n "DesignerHelper-rs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)))) (h "04x5lvc12ql94db4bff97pzg23g8piklkqiihphwcf472cc35sc6")))

