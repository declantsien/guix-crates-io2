(define-module (crates-io de si designo) #:use-module (crates-io))

(define-public crate-designo-0.1.0 (c (n "designo") (v "0.1.0") (h "0ss32r11qp7lhbz2x26riyikly777qm3hkqad9b6sz6m2z08awyr")))

(define-public crate-designo-0.2.0 (c (n "designo") (v "0.2.0") (h "0fgdf99cszgyg2hdgswm492gnjrgcr7311fr4mci0p6ww3a7f56k") (f (quote (("paypal"))))))

