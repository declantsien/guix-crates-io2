(define-module (crates-io de si designspace) #:use-module (crates-io))

(define-public crate-designspace-0.1.0 (c (n "designspace") (v "0.1.0") (d (list (d (n "fonttools") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "norad") (r "^0.4") (f (quote ("rayon" "kurbo"))) (o #t) (d #t) (k 0)) (d (n "otspec") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0vq5xrcsaskic7vac79iz1fas6q8f4nmkhf4i6xzqpp8kdd2npwl")))

