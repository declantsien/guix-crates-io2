(define-module (crates-io de si desim) #:use-module (crates-io))

(define-public crate-desim-0.1.0 (c (n "desim") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1k98y5lbcin80y5wbdgn2jpvpvf4dvvvn56wllphrc58077cj4ip")))

(define-public crate-desim-0.2.0 (c (n "desim") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0dz474r7wk8r4w30ym9h2wlk08dr2ia2vxnnphq8m652pfkkj2cd")))

(define-public crate-desim-0.3.0 (c (n "desim") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)))) (h "1mrqbamff9g2lzc9k4nnd0jhkgf6ksx08nvs4663ykvm2y6w5xk8")))

(define-public crate-desim-0.4.0 (c (n "desim") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)))) (h "1hqc3jc9vlx23chrxhmhcmw7a3prs795kxn5alk91awdavf7y9xr")))

