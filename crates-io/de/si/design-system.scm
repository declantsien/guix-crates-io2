(define-module (crates-io de si design-system) #:use-module (crates-io))

(define-public crate-design-system-0.0.1 (c (n "design-system") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "css-in-rust") (r "^0.4.1") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("KeyboardEvent" "console" "Document" "Element"))) (d #t) (k 0)) (d (n "web_logger") (r "^0.2") (d #t) (k 0)) (d (n "yew") (r "^0.17") (f (quote ("web_sys"))) (d #t) (k 0)) (d (n "yew_styles") (r "^0.7") (f (quote ("button"))) (d #t) (k 0)))) (h "1xcry3j053lh0i8l1fjr2bw4wr45bvgl44szas8v6c3w203bnw98")))

