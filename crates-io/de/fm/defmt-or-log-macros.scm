(define-module (crates-io de fm defmt-or-log-macros) #:use-module (crates-io))

(define-public crate-defmt-or-log-macros-0.1.0 (c (n "defmt-or-log-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "19x9pzdpkqmmza595nkn3nqrq70s41dg82md37a1cjap745araj2")))

(define-public crate-defmt-or-log-macros-0.1.1 (c (n "defmt-or-log-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1bnmvlk6ma00115gjjfvy066ywz4pj122lxhndlasx46fgkrf1rx")))

(define-public crate-defmt-or-log-macros-0.1.2 (c (n "defmt-or-log-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0qqpykffkmgsqc8w44pq9bzah8lg1l5557sf3phcidyvkqlxsxfn")))

