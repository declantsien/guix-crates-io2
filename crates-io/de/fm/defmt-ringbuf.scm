(define-module (crates-io de fm defmt-ringbuf) #:use-module (crates-io))

(define-public crate-defmt-ringbuf-0.1.0 (c (n "defmt-ringbuf") (v "0.1.0") (d (list (d (n "critical-section") (r "^1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0xywkp0z2b1k5biwzk3jlxd6hc5v6kym4g3gzh5kni4jiy1y050f")))

(define-public crate-defmt-ringbuf-0.2.0 (c (n "defmt-ringbuf") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "critical-section") (r "^1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0q1zf36l3qvz9izng32avp9rwbyz0zq9y6vdcdba09m864c75xkx")))

