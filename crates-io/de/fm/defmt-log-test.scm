(define-module (crates-io de fm defmt-log-test) #:use-module (crates-io))

(define-public crate-defmt-log-test-0.1.0 (c (n "defmt-log-test") (v "0.1.0") (d (list (d (n "defmt") (r "^0.2.1") (d #t) (k 0)))) (h "09b235kgvwn49j6vv63km663110as3w566jiz8lc3346whm7lgcc") (f (quote (("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug"))))))

