(define-module (crates-io de fm defmt-elf2table) #:use-module (crates-io))

(define-public crate-defmt-elf2table-0.1.0 (c (n "defmt-elf2table") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "gimli") (r "^0.22.0") (d #t) (k 0)) (d (n "object") (r "^0.21.0") (f (quote ("read_core" "elf" "std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "087mra67r663y1a519b46g99wxyh7d99rkkdkdcbbivyc0ynw6c3") (f (quote (("unstable"))))))

