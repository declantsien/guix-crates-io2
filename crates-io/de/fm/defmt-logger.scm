(define-module (crates-io de fm defmt-logger) #:use-module (crates-io))

(define-public crate-defmt-logger-0.1.0 (c (n "defmt-logger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.1.4") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (f (quote ("std"))) (d #t) (k 0)))) (h "0z7nb9d66s407ycjvgvphx7ikg6ybr5qy3lgncixn1lkmkiyz8ac") (f (quote (("unstable"))))))

