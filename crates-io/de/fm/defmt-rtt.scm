(define-module (crates-io de fm defmt-rtt) #:use-module (crates-io))

(define-public crate-defmt-rtt-0.1.0 (c (n "defmt-rtt") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "defmt") (r "^0.1.0") (d #t) (k 0)))) (h "0m73g6ri2hczfq67daqd88i1dxj32nwlpswgfc8djwrxv2rqhfra")))

(define-public crate-defmt-rtt-0.2.0 (c (n "defmt-rtt") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)))) (h "0frw4irggif5zhnl2zydfab4w1713h85g5yvykcx4cvbrm1wmpx5")))

(define-public crate-defmt-rtt-0.3.0 (c (n "defmt-rtt") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "16w4l3y8qhvkr5r1igm5wcdmm87vja44j4sif9g20l7r7hr1cd0w")))

(define-public crate-defmt-rtt-0.3.1 (c (n "defmt-rtt") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0r1vv649yvz0xwc82199d7iyk9w8k1959258pvfjs5j86zyi0wbm")))

(define-public crate-defmt-rtt-0.3.2 (c (n "defmt-rtt") (v "0.3.2") (d (list (d (n "critical-section") (r "^0.2.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "1c8qkw10ci312jbj27pp6a2x58ihjz6k56v2jy6m0zc4b2yvnb0x")))

(define-public crate-defmt-rtt-0.4.0 (c (n "defmt-rtt") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "17y48jwcn901jvqf0s26x1giqqx4rlc9f83xkknrkpb429v276b0")))

(define-public crate-defmt-rtt-0.4.1 (c (n "defmt-rtt") (v "0.4.1") (d (list (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0l8cxzmp1cfp5rx0bg4rgas8093zdrpsl8dqr2vm05xwvfrrgdms")))

