(define-module (crates-io de fm defmt-bbq) #:use-module (crates-io))

(define-public crate-defmt-bbq-0.0.1 (c (n "defmt-bbq") (v "0.0.1") (d (list (d (n "bbqueue") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "14p16hgfgf8m9sd2q53iwq4i20iqkaq49h2niqsbyh9wgmahgrms") (f (quote (("default" "defmt/encoding-rzcobs"))))))

(define-public crate-defmt-bbq-0.0.2 (c (n "defmt-bbq") (v "0.0.2") (d (list (d (n "bbqueue") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "1ryrn12zg25f6w4rabph7m90riyzs89mik2dj3c888kdp6s4r27z") (f (quote (("default" "defmt/encoding-rzcobs"))))))

(define-public crate-defmt-bbq-0.1.0 (c (n "defmt-bbq") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.5.1") (f (quote ("defmt_0_3"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "09x3qxspdjrcms6c2an19n45r9yyz4ikylbv97mhqp1qmb9xydy9") (f (quote (("default" "defmt/encoding-rzcobs"))))))

