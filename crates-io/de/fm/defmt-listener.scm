(define-module (crates-io de fm defmt-listener) #:use-module (crates-io))

(define-public crate-defmt-listener-0.1.0 (c (n "defmt-listener") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.3.3") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13vqcs9jl5vrvbq7nyn9ksmbl38zkwghcwlxs6jqwf7avim3mqjv") (y #t)))

(define-public crate-defmt-listener-0.1.1 (c (n "defmt-listener") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.3.3") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14fzipyp6jkzm142hqifivhm75mx7xncjvf8wc2lvrm9khzf847l")))

