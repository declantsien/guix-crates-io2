(define-module (crates-io de fm defmt-or-log) #:use-module (crates-io))

(define-public crate-defmt-or-log-0.1.0 (c (n "defmt-or-log") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "1f5mbrcnw8wji107i8q8j9n4m99cz05vichr0y66i1v32kv5r9ya") (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-defmt-or-log-0.1.1 (c (n "defmt-or-log") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "0xjynzi9swxdwj0fgp07njg5fw7zpbbdd7fk14fccd5hs3ipddna") (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-defmt-or-log-0.2.0 (c (n "defmt-or-log") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "10csm418injxnv4696fb2rhnh4sggv73ql3ff3nrq3w92qdb8jda") (f (quote (("default" "at_least_one") ("at_least_one")))) (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-defmt-or-log-0.2.1 (c (n "defmt-or-log") (v "0.2.1") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "defmt-or-log-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)))) (h "0fn5ljfhirnzpd4qdm7x4j6yhzsw3ivi6y08v53sp1gf9l5n6w43") (f (quote (("default" "at_least_one") ("at_least_one")))) (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

