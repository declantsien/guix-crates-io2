(define-module (crates-io de fm defmt-itm) #:use-module (crates-io))

(define-public crate-defmt-itm-0.2.0 (c (n "defmt-itm") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.4") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)))) (h "0r1swfa3kirfsg5lvnfzyz7816wgaz1kg11ahpjbpl9vfpglagb5")))

(define-public crate-defmt-itm-0.3.0 (c (n "defmt-itm") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0p1waqqx2wmwcpx4zac65ywqxlqb92p0dczv175q7gnihk9s91qx")))

