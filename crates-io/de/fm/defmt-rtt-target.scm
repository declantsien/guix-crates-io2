(define-module (crates-io de fm defmt-rtt-target) #:use-module (crates-io))

(define-public crate-defmt-rtt-target-0.3.0 (c (n "defmt-rtt-target") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (d #t) (k 0)))) (h "05zpx1qd9wl8bj5jyhfccqf4cssrwankl6c2i163lpx91q6kidz9")))

