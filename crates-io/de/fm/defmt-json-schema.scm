(define-module (crates-io de fm defmt-json-schema) #:use-module (crates-io))

(define-public crate-defmt-json-schema-0.1.0 (c (n "defmt-json-schema") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0smd5mxzk3b6j82ykb0fzk34f6xl1fcchsyqhprir9jpiqi4vc4n")))

