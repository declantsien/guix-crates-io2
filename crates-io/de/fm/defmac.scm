(define-module (crates-io de fm defmac) #:use-module (crates-io))

(define-public crate-defmac-0.1.0 (c (n "defmac") (v "0.1.0") (h "1w1dy0fxc6s2kkj2fjwh6qxf0xzjgxypcffva4dvnzzc16j0vm64")))

(define-public crate-defmac-0.1.1 (c (n "defmac") (v "0.1.1") (h "0615g10yn1z3fjikh77ammm30yxskivahbhh2g81wy19lrz092kv")))

(define-public crate-defmac-0.1.2 (c (n "defmac") (v "0.1.2") (h "1q6282vghszhrfcv1nn6b83i9c1680v8m3dsmjb5q7y5r45286l4")))

(define-public crate-defmac-0.1.3 (c (n "defmac") (v "0.1.3") (h "17giv0n0n1r64z0dahfvkjy3ys517jxyhs8sd9lmgvcljpjyryxa")))

(define-public crate-defmac-0.2.0 (c (n "defmac") (v "0.2.0") (h "01ff3jdmcc5waffkwllndnx5hsn414r7x1rq4ib73n7awsyzxkxv")))

(define-public crate-defmac-0.2.1 (c (n "defmac") (v "0.2.1") (h "14cqfvc0f1pkd6gdhjxa2wv3iibqprc0n203ims8lvg96752ynfm")))

