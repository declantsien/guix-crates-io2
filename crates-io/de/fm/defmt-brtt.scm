(define-module (crates-io de fm defmt-brtt) #:use-module (crates-io))

(define-public crate-defmt-brtt-0.0.0 (c (n "defmt-brtt") (v "0.0.0") (h "1bz3p6klays70smqpy5s87cdn16v5qc0b8xknb9zl0iwb624zwsl")))

(define-public crate-defmt-brtt-0.1.0 (c (n "defmt-brtt") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.5.1") (f (quote ("defmt_0_3"))) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "12gbd3ll5sc4h3s9c882bnd1apqi625dnxybgmvi05yz1gc21bpm") (f (quote (("rtt") ("default" "rtt" "bbq" "defmt/encoding-rzcobs") ("bbq") ("async-await"))))))

(define-public crate-defmt-brtt-0.1.1 (c (n "defmt-brtt") (v "0.1.1") (d (list (d (n "bbqueue") (r "^0.5.1") (f (quote ("defmt_0_3"))) (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0y4alnspwikl9c99210w181mypv2lx2cn7qhp099vj6h6lvarw62") (f (quote (("rtt") ("default" "rtt" "bbq" "defmt/encoding-rzcobs") ("async-await")))) (s 2) (e (quote (("bbq" "dep:bbqueue"))))))

