(define-module (crates-io de fm defmt-test) #:use-module (crates-io))

(define-public crate-defmt-test-0.1.0 (c (n "defmt-test") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "defmt") (r "^0.1.0") (d #t) (k 0)) (d (n "defmt-test-macros") (r "^0.1.0") (d #t) (k 0)))) (h "135g08aqirnswakcbahq2678a5nfdsjzpzlawm4f1ibdhmimf3vk")))

(define-public crate-defmt-test-0.1.1 (c (n "defmt-test") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "defmt") (r "^0.1.0") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.1.1") (d #t) (k 0)))) (h "0jxz3dhqv4f8g8k2c9i18i2xa09q3aar54hiykh61zbfabwc6h9a")))

(define-public crate-defmt-test-0.2.0 (c (n "defmt-test") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.1.1") (d #t) (k 0)))) (h "0va46r7ygfhbszczmxkix215d663qqn1n8wy5vfyhhvvsx04lyyr") (f (quote (("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "defmt-trace"))))))

(define-public crate-defmt-test-0.2.1 (c (n "defmt-test") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.2.0") (d #t) (k 0)))) (h "0r6vrvfwln2063lh738hk8fpcpabdawgc7jl87svqxan8c5xr49s") (f (quote (("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "defmt-trace"))))))

(define-public crate-defmt-test-0.2.2 (c (n "defmt-test") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "defmt") (r "^0.2.1") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.2.0") (d #t) (k 0)))) (h "1a7fkf6va5sy5a922c9h98fa1j5wm52i6nb1ryay8pfxf4nnf3x2") (f (quote (("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "defmt-trace"))))))

(define-public crate-defmt-test-0.2.3 (c (n "defmt-test") (v "0.2.3") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.14") (d #t) (k 0)) (d (n "defmt") (r "^0.2.2") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.2.1") (d #t) (k 0)))) (h "02gchp2kpa84lj54vbp6yxmalway95jv0pv0rrxniyw8258xzvgb") (f (quote (("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "defmt-trace"))))))

(define-public crate-defmt-test-0.3.0 (c (n "defmt-test") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.3.0") (d #t) (k 0)))) (h "126axnbxpbldnj712qnnwnard8lzm8hqlz84wayc3804l4f4zwjd")))

(define-public crate-defmt-test-0.3.1 (c (n "defmt-test") (v "0.3.1") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.3.0") (d #t) (k 0)))) (h "0lc8w0jmmvqinvn93ryv4hjpcrs3m4m5q2g6cddzlmwh8yfcfaf6")))

(define-public crate-defmt-test-0.3.2 (c (n "defmt-test") (v "0.3.2") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.5") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "defmt-test-macros") (r "=0.3.1") (d #t) (k 0)))) (h "1waxgkkni26m8ab0x41lp7md1032hzg44wl7hhcb354gqgl6c299")))

