(define-module (crates-io de fm defmt-parser) #:use-module (crates-io))

(define-public crate-defmt-parser-0.1.0 (c (n "defmt-parser") (v "0.1.0") (h "1phng9zm2h4xwa4crbcpyl9nk3hb0nn5dqi3db40f14k2kzk0aw1") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.2.0 (c (n "defmt-parser") (v "0.2.0") (h "188f4lnkq464flk253nrpj0jgj6wx5gpj1m6j1czjir1xiwgf5n8") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.2.1 (c (n "defmt-parser") (v "0.2.1") (h "0134vbxfjsfqs3sbksajbc31gw94400qhckm81j6dd3xx09y6qzw") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.2.2 (c (n "defmt-parser") (v "0.2.2") (h "1zp9rsqqppjqabibw7m1igixmhdhrkr8mhr183ikamjz9wmiqqmw") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.3.0 (c (n "defmt-parser") (v "0.3.0") (h "1is90zrcifdky4rpra450779c3jf3bc2xwcqbj9fy6m5w48f074d") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.3.1 (c (n "defmt-parser") (v "0.3.1") (h "0vm04l2jqksc7zmld8q5dm55c42c20zsxqifvsivm69djwlkvchd") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.3.2 (c (n "defmt-parser") (v "0.3.2") (d (list (d (n "rstest") (r "^0.17") (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "190v5kagqmyxz87f0hbssfamd9pnbxsdrj9d9b7k50z3fm2rjpp1") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.3.3 (c (n "defmt-parser") (v "0.3.3") (d (list (d (n "rstest") (r "^0.17") (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l6c9rjjiir27pk8yh5kcsbwzzzng71sbgzc9jy98zzx5b029696") (f (quote (("unstable"))))))

(define-public crate-defmt-parser-0.3.4 (c (n "defmt-parser") (v "0.3.4") (d (list (d (n "rstest") (r "^0.17") (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03zpg0i6vlalw7m976z66n70s041rvwii8qn3grxgs1hwgpmyjpz") (f (quote (("unstable"))))))

