(define-module (crates-io de fm defmt-print) #:use-module (crates-io))

(define-public crate-defmt-print-0.1.0 (c (n "defmt-print") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.1.4") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "defmt-elf2table") (r "^0.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "defmt-logger") (r "^0.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "123nm7h22h1d4aqkj9lwxrp8xm2k5rxgbwhzqbw8vb1z03zvl5xj")))

(define-public crate-defmt-print-0.2.0 (c (n "defmt-print") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.2.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "02crd4r4mv7lcfzq0cc6y5giydzp70qxm9h0hz1dai3hfrj9n8f6")))

(define-public crate-defmt-print-0.2.1 (c (n "defmt-print") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.2.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0zsgq2q251yqmln6b6aip4x0w2iq4i1sk6s3iw14q974kg1qnhc3")))

(define-public crate-defmt-print-0.2.2 (c (n "defmt-print") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.2.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1lx32ncs47al51mnsi3hgxr88nvpla6f5wrfxks473dvg3484rg4")))

(define-public crate-defmt-print-0.3.0 (c (n "defmt-print") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02ccjn3nn0h4yvm7a8nqhh7csaf5a4mzzbzz541jwgqn27lp1nq6")))

(define-public crate-defmt-print-0.3.2 (c (n "defmt-print") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1wh7wnvr32ii6n575727f463mwcan4k37lc977dy55lbsr6nl68x")))

(define-public crate-defmt-print-0.3.3 (c (n "defmt-print") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.3") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14wbhi457ln0cqrcqzvd1rz823v94l6z0r7flrcyzyxh576cf5ln")))

(define-public crate-defmt-print-0.3.4 (c (n "defmt-print") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.4") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "124mdv9blya2gd07ag83jgl65i7kvp7flaav450864608c4xlnw1")))

(define-public crate-defmt-print-0.3.5 (c (n "defmt-print") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0prvk48sd9z17zrb2m6jd0z2caghggm5ii33q4mgsarvzlfn06id") (y #t)))

(define-public crate-defmt-print-0.3.6 (c (n "defmt-print") (v "0.3.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.6") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "030gy4zmhgq5vh1d8581vdlcy9g0c3q3xj549sg6bx6n7rmpix9s")))

(define-public crate-defmt-print-0.3.7 (c (n "defmt-print") (v "0.3.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fn563shyshm441hibr1a2mf9wi5lk0zz5b0mzb050z0lgpfvagy")))

(define-public crate-defmt-print-0.3.8 (c (n "defmt-print") (v "0.3.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.7") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hmmw6sm06h0rbq3r0fi8h2ij3vyxw7l6vj0zanv5d67g9ybk9jl")))

(define-public crate-defmt-print-0.3.9 (c (n "defmt-print") (v "0.3.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.8") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1f0ik78li292q1v8cbakchii53cc9bsip1b99afg7pgc5ma2zgax")))

(define-public crate-defmt-print-0.3.10 (c (n "defmt-print") (v "0.3.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.9") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l2fski1k0q79hx20dffzi27b1q37p94iw0df7f3wf1vbpb0cm6z")))

(define-public crate-defmt-print-0.3.11 (c (n "defmt-print") (v "0.3.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.10") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fm73lvm76gs5d3jj3xchjzf2zmzfpqs47bspqh2100ivmbinab5")))

(define-public crate-defmt-print-0.3.12 (c (n "defmt-print") (v "0.3.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.11") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1gyjr3sd7jpybyqf83nri75gnzb3cvqs6ln81xv8wljy6h00v0kn")))

