(define-module (crates-io de fm defmt) #:use-module (crates-io))

(define-public crate-defmt-0.0.0 (c (n "defmt") (v "0.0.0") (h "1a90q3f46yarm4z0xlxzif8p992gndsala6b27vmyp8rbm9hqrmf")))

(define-public crate-defmt-0.1.0 (c (n "defmt") (v "0.1.0") (d (list (d (n "defmt-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)))) (h "01lqbrj5mqd70bx3gaaylcl8a0vnnvzm5gmkaa5yyq376mjpq2yf") (f (quote (("alloc")))) (l "defmt")))

(define-public crate-defmt-0.1.1 (c (n "defmt") (v "0.1.1") (d (list (d (n "defmt-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)))) (h "0dlsqd31m8bqvg1j1r7rvrg7xlhwrwvj3ql5i7zvm2wsnqwj39sv") (f (quote (("alloc")))) (l "defmt")))

(define-public crate-defmt-0.1.2 (c (n "defmt") (v "0.1.2") (d (list (d (n "defmt-macros") (r ">=0.1.1, <0.2.0") (d #t) (k 0)) (d (n "heapless") (r ">=0.5.6, <0.6.0") (d #t) (k 0)) (d (n "semver") (r ">=0.11.0, <0.12.0") (d #t) (k 1)))) (h "0312yn5ybafliq3426a57cqvfjkn111bf5i4gwc9mhj4iqawj50i") (f (quote (("alloc")))) (l "defmt")))

(define-public crate-defmt-0.1.3 (c (n "defmt") (v "0.1.3") (d (list (d (n "defmt-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)))) (h "067nq1byjyf1p9cc2036d5p1aa0zz5n1b8nwg5mlivv5h6bsnq21") (f (quote (("alloc")))) (l "defmt")))

(define-public crate-defmt-0.2.0 (c (n "defmt") (v "0.2.0") (d (list (d (n "defmt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0k5mq5glxg2hhs7azb47j0pkyykx1srw0ygb4jm2dxb4dv9brimf") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.2.1 (c (n "defmt") (v "0.2.1") (d (list (d (n "defmt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "00knra9fb6h5idb67kbjhkhsdyjjg82wbaacvac5814b4xcgspvk") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.2.2 (c (n "defmt") (v "0.2.2") (d (list (d (n "defmt-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.3") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0nywr1vncflbv1inpxs61kkgpp6553pf5lmnnnw6jiwb4b723fks") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.2.3 (c (n "defmt") (v "0.2.3") (d (list (d (n "defmt-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "semver") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "03zzxnlnvb7qzdv513mrn2xpqsg7cbqghgsqf3x4l5h8sbsrdzhm") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.0 (c (n "defmt") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "181l5wv6sihrjwjvk97wznmg2iaz3w1ljsx2dfzjssmhsbs5vyv2") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.1 (c (n "defmt") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fd7nhscsdrs090blmrknf5k7bfsm2gi4rf8qpq5s2jc7pjanllj") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (y #t) (l "defmt")))

(define-public crate-defmt-0.3.2 (c (n "defmt") (v "0.3.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1cvbl44cdrp7mdw0qbkmx5qnld94qks7355qsw9z1gyrjisax86k") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.3 (c (n "defmt") (v "0.3.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1x3z6ipwp0jrj1kprjpa2rbgyyhvrmhwsp00lpw4ywcpwp6r6cfh") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (y #t) (l "defmt")))

(define-public crate-defmt-0.3.4 (c (n "defmt") (v "0.3.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "10lq06n9sln304y4vigfyrj2pa69kbcfilc8z4mm2ixk7jyp6rlm") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.5 (c (n "defmt") (v "0.3.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "161zix28wi2i3926vqakyi6wp8p9zi1krf2rssvrzqpyn88x18m8") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.6-beta.0 (c (n "defmt") (v "0.3.6-beta.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "15zbrgfgbfax8lb7f578ma8k3wdbzpirv8c6yv4nw6rwcyrnbnqa") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.6-beta.1 (c (n "defmt") (v "0.3.6-beta.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1anmjczrxkg9fhcvhx4yv1hf8bd03fl2s81yzi3qqw3cm04aldcw") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.6 (c (n "defmt") (v "0.3.6") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "15a53435jpy9jj3g49mxp94g961zslggbin2nd9f2va20wlmaf9r") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("alloc")))) (l "defmt")))

(define-public crate-defmt-0.3.7 (c (n "defmt") (v "0.3.7") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0f84133pl224wndwdzbrzyjww21wdk5i4gl5dcnm4x0q7ibja9x2") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("avoid-default-panic") ("alloc")))) (y #t) (l "defmt")))

(define-public crate-defmt-0.3.8 (c (n "defmt") (v "0.3.8") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defmt-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1q79ryg6q1i2nfhs5wcrc018y8sblvsjlryl45qqi2v6c8id57d9") (f (quote (("unstable-test" "defmt-macros/unstable-test") ("ip_in_core") ("encoding-rzcobs") ("encoding-raw") ("avoid-default-panic") ("alloc")))) (l "defmt")))

