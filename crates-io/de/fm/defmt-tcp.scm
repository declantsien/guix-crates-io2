(define-module (crates-io de fm defmt-tcp) #:use-module (crates-io))

(define-public crate-defmt-tcp-0.1.0 (c (n "defmt-tcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qjbw6wrn8ysk2xinmc4gaxg1ahz8mdv3v17jdca7b5smgrr1nj6") (y #t)))

(define-public crate-defmt-tcp-0.1.1 (c (n "defmt-tcp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "defmt-decoder") (r "=0.3.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1012f6435vinn5hkqlz43ln8kyf9lpfii8lbdyz0pcxf3j832p51")))

(define-public crate-defmt-tcp-0.1.2 (c (n "defmt-tcp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "defmt-decoder") (r "^0.3.9") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1g7fgppp2rzxv2594kjbwjjfb78q9d3ca2ynhldf5cbnr3cjwfls") (r "1.76.0")))

