(define-module (crates-io de fm defmt-test-macros) #:use-module (crates-io))

(define-public crate-defmt-test-macros-0.1.0 (c (n "defmt-test-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "159jkab71naixvp1l11q5d2r47rqib0lvmn1ffdp3y1b7nph11jy")))

(define-public crate-defmt-test-macros-0.1.1 (c (n "defmt-test-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "040vswyncm0h8lm4ql2lragkrzwci77sfwbmh2r5ca1ik5fwjz52")))

(define-public crate-defmt-test-macros-0.2.0 (c (n "defmt-test-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1s335ybylxngzbc5lhb146a1c9yc6szppfrjp00g4fc9mkvgynir")))

(define-public crate-defmt-test-macros-0.2.1 (c (n "defmt-test-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0046p84bn20bdzwi7nr2rqnca5inv8mxpa9id0hfc8ylzplkpnb8")))

(define-public crate-defmt-test-macros-0.3.0 (c (n "defmt-test-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1h9dl8w4m0mmf0vcya4khqh79xk78sydz504l8djxmv383mdz84l")))

(define-public crate-defmt-test-macros-0.3.1 (c (n "defmt-test-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09d1fqfra8bhg549p3bbrmgzx84c931aq9i8mhk9ff26lbnccjwq")))

