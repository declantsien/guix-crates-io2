(define-module (crates-io de bt debt64) #:use-module (crates-io))

(define-public crate-debt64-0.0.2 (c (n "debt64") (v "0.0.2") (h "0ricvwkwb1ssimpv31kn5rzd0pzwjhxi492010yidryq04zbkq4j")))

(define-public crate-debt64-0.1.0 (c (n "debt64") (v "0.1.0") (h "1iv085fisr60f0rnnp90rvl4rpqkpyn9kdnybx92b7az0y97qpsi")))

(define-public crate-debt64-1.0.0 (c (n "debt64") (v "1.0.0") (h "025kwqklyr3bz3nig9cdk2cwvsxg8bx66cdyszz6jc8y5ibpgw14")))

(define-public crate-debt64-1.1.0 (c (n "debt64") (v "1.1.0") (h "1cywlmkpwc0rxknjxbwxxwjzq4sr35zv65sm20z4mmfv182p653n")))

(define-public crate-debt64-1.1.1 (c (n "debt64") (v "1.1.1") (h "1iim4d2sa7qmakm444sqdrp2p77ja1ingh4zhsdx4qdz4sqm3n0v")))

(define-public crate-debt64-1.2.0 (c (n "debt64") (v "1.2.0") (h "0msk41f1x03kj066wk3ixfnk7gf89an1mjsf06r6b4k4mpaki8r4")))

(define-public crate-debt64-2.0.0 (c (n "debt64") (v "2.0.0") (h "1b3829b1k2j8d83p49sgm36c7jlvvq7g0icgc2lgwxcfhllpgfxw")))

(define-public crate-debt64-3.0.1 (c (n "debt64") (v "3.0.1") (h "0jfq82q54vfhwl7k83ng2a8ghzrygdjcla9ywwv7cz713xv0j61c") (f (quote (("std"))))))

(define-public crate-debt64-3.1.0 (c (n "debt64") (v "3.1.0") (h "0h5nqy39ywpvq8g2a2sv95xl6ni5x888j606xwlz7f1xcr94hj2w") (f (quote (("std"))))))

(define-public crate-debt64-3.1.1 (c (n "debt64") (v "3.1.1") (h "09fvm2g7p1jsxl7vmf0zgi4s7pqs7xfhhz9b6b054gvv3zrbq3hh") (f (quote (("std"))))))

(define-public crate-debt64-3.1.2 (c (n "debt64") (v "3.1.2") (h "0mk5sw28hl2qj55h5xwcjwz9aqvxwckp7p73sz0zdff8p5a0yvdw") (f (quote (("std"))))))

(define-public crate-debt64-4.0.0 (c (n "debt64") (v "4.0.0") (h "1wsijd39pr4xvh7lc55b7x054qq66yx2jxm22q0235a45s3461vh") (f (quote (("std"))))))

(define-public crate-debt64-5.0.0 (c (n "debt64") (v "5.0.0") (h "01qjvgpbqsqsxgybm80fzr94xyyvj23134x7hgkp59bxsak4lgbn") (f (quote (("std"))))))

(define-public crate-debt64-6.0.0 (c (n "debt64") (v "6.0.0") (h "18p10fy06rgsa3b1q3scdv0xx19zhx011ycwsp87aw3qcnr2fh7m") (f (quote (("std"))))))

(define-public crate-debt64-7.0.0 (c (n "debt64") (v "7.0.0") (h "0gh2sckfrpxzbwdanjgw6m6n90i16rsgsjc95nk2py30qv34b0rn") (f (quote (("std"))))))

(define-public crate-debt64-8.0.1 (c (n "debt64") (v "8.0.1") (h "1x8yghjvc7l3k8vrlhmwxwis8ynsik1smrg69a6ziiinvx1ks5sj") (f (quote (("std"))))))

(define-public crate-debt64-8.0.2 (c (n "debt64") (v "8.0.2") (h "0cwhrbgiikn0x0z72nfsrzrr5car4m11krkmqmxjnjp2bfzawfdm") (f (quote (("std"))))))

(define-public crate-debt64-9.0.0 (c (n "debt64") (v "9.0.0") (h "0fpqhmdj5mnd5351ngafayqq4mmqbd377bmiqal86bx5yzsyxdfh") (f (quote (("std"))))))

