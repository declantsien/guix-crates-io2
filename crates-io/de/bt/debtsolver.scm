(define-module (crates-io de bt debtsolver) #:use-module (crates-io))

(define-public crate-debtsolver-0.1.0 (c (n "debtsolver") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.1") (d #t) (k 0)))) (h "153wvpfzbn13m8n5ln9hz2wpkd5zspzds3bchm3kfhkx0vw0l495")))

(define-public crate-debtsolver-0.2.0 (c (n "debtsolver") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "rusty-money") (r "^0.1.0") (d #t) (k 0)))) (h "0wb898c6vl9rc7gqab3d22azsla1v15822nkbzi8fzy3gsl06la4")))

