(define-module (crates-io de qm deqmap) #:use-module (crates-io))

(define-public crate-deqmap-0.0.1 (c (n "deqmap") (v "0.0.1") (h "0cd1hply6i8ygsj6xn8q3pnbfjplz49dpg9mkivlyaairsp2md05") (r "1.64.0")))

(define-public crate-deqmap-0.0.2 (c (n "deqmap") (v "0.0.2") (h "1b1hkq42habwvgaf8755bm6jxmfq3qp0z771hfj964qwy7pcl591") (r "1.64.0")))

(define-public crate-deqmap-0.0.3 (c (n "deqmap") (v "0.0.3") (h "1wm0fz52z0i4i9ygz1zmdndi561dk93sncsj8718gp0dbln5dfyx") (r "1.64.0")))

(define-public crate-deqmap-0.0.4 (c (n "deqmap") (v "0.0.4") (h "046x4mjnfxiwf64xlx0g7y2ma8daa1drvhxgf8wkv16j51dm4kp6") (r "1.64.0")))

