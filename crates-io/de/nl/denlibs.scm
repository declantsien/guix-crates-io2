(define-module (crates-io de nl denlibs) #:use-module (crates-io))

(define-public crate-denlibs-0.1.0 (c (n "denlibs") (v "0.1.0") (h "0dnr77ssyba1kdmj2jirzmhr5lkbzx07smxx66my3z9ns1qx83rf") (y #t)))

(define-public crate-denlibs-0.1.1 (c (n "denlibs") (v "0.1.1") (h "1kn27jjz5zjs95a4bcpbi6n0fpaqgkqkhs1nbz15i9gpybbbxas8") (y #t)))

(define-public crate-denlibs-0.1.2 (c (n "denlibs") (v "0.1.2") (h "0xsi3dx87mmrvyr2h61blbrg90k2zb889v1rv9jr18vnz87qzfck") (y #t)))

(define-public crate-denlibs-0.1.3 (c (n "denlibs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "1vbhk52jm5qb3zq2ihbwxs3gpn8azqw9rkf1pxyprhln0ymfv5p7") (y #t)))

(define-public crate-denlibs-0.1.4 (c (n "denlibs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0sjy16ypcprrph7z4zjqsq5qq361kpv7891lycjrsmqdprpwazaz")))

(define-public crate-denlibs-0.1.5 (c (n "denlibs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "1v59gfn9a6qjhk0d53x3s4qkl4xwsy6r6nkdaiy7jz3dk5ab290x")))

(define-public crate-denlibs-0.1.6 (c (n "denlibs") (v "0.1.6") (d (list (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0azgbyyzbv6x9ldg7gmiaxpn5230367w5jx52fx5c7jq0vrr10k0")))

(define-public crate-denlibs-0.1.7 (c (n "denlibs") (v "0.1.7") (d (list (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0zvjza807bddccqhhbaa7rknp5in19m77raq1s76i4hxrdp471gk")))

(define-public crate-denlibs-0.1.8 (c (n "denlibs") (v "0.1.8") (d (list (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0n93zp8yk5gizy6g451qypd02bmbiy71l0sp5n20mgxfhmvv6srs")))

(define-public crate-denlibs-0.1.9 (c (n "denlibs") (v "0.1.9") (d (list (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0mryk0kkbr8xv6xffhfaqpfd4klbz1biq195a8p0sixl39gy84xy")))

