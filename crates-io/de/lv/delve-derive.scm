(define-module (crates-io de lv delve-derive) #:use-module (crates-io))

(define-public crate-delve-derive-0.1.0 (c (n "delve-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0cbc075xcw9jqfl0fp8a6j1b56g5mnac1vvy81dbpydm8ngl2qc9")))

(define-public crate-delve-derive-0.2.0 (c (n "delve-derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "01py9bcq3khjg5n93ddgxgmfkcax9q006dy5wjsn4khqjbszlpar")))

(define-public crate-delve-derive-0.3.0 (c (n "delve-derive") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1jiz976f8dc4rz34nkd0cisbzhsn2xxd8sz0q87kqrm0f5wza8gr")))

