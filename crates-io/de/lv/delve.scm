(define-module (crates-io de lv delve) #:use-module (crates-io))

(define-public crate-delve-0.1.0 (c (n "delve") (v "0.1.0") (d (list (d (n "delve-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "delve-derive") (r "^0.1.0") (d #t) (k 2)))) (h "1wa0hww91syp61xm9cz6h86zf5w9lq5aznw01sjkfbv99dm9f2wp") (f (quote (("std") ("derive" "delve-derive") ("default" "std"))))))

(define-public crate-delve-0.2.0 (c (n "delve") (v "0.2.0") (d (list (d (n "delve-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "delve-derive") (r "^0.2.0") (d #t) (k 2)))) (h "10xc8l9k8vqbjjrayhlx2pk3xdavjhl9wrw062r3hy3p8zjq3wcn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:delve-derive"))))))

(define-public crate-delve-0.3.0 (c (n "delve") (v "0.3.0") (d (list (d (n "delve-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "delve-derive") (r "^0.3.0") (d #t) (k 2)))) (h "06fd1njzvy4zvhrdzya86d5l0zfyzzdls41yz99r7vvskpy2f942") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("derive" "dep:delve-derive"))))))

