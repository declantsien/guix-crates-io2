(define-module (crates-io de ml deml) #:use-module (crates-io))

(define-public crate-deml-0.1.0 (c (n "deml") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dagrs") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "0zc5kzw88wrc25n69caqpzgk4gbczb8hjmhr68ispibybdpnsbvz")))

