(define-module (crates-io de ut deutsche-bahn-delay-reasons) #:use-module (crates-io))

(define-public crate-deutsche-bahn-delay-reasons-0.1.0 (c (n "deutsche-bahn-delay-reasons") (v "0.1.0") (d (list (d (n "enum_index") (r "^0.2.0") (d #t) (k 0)) (d (n "enum_index_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)))) (h "0id6bqlk3qqmfadv5rzrx9ags8z8k5zlbn5yw0511lr3lh51a654")))

(define-public crate-deutsche-bahn-delay-reasons-0.1.1 (c (n "deutsche-bahn-delay-reasons") (v "0.1.1") (d (list (d (n "enum_index") (r "^0.2.0") (d #t) (k 0)) (d (n "enum_index_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)))) (h "0ks8ry3ap81x44dspfakmsbjq5l515pkrkapfp80r3jf43drsfxh")))

(define-public crate-deutsche-bahn-delay-reasons-0.2.0 (c (n "deutsche-bahn-delay-reasons") (v "0.2.0") (d (list (d (n "enum_index") (r "^0.2.0") (d #t) (k 0)) (d (n "enum_index_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)))) (h "16gxvc58vdc6g6xmgkzxf61jndxayy2cmmfjmwljgvfj30pv0lg8")))

