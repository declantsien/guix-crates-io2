(define-module (crates-io de ut deuterium_plugin) #:use-module (crates-io))

(define-public crate-deuterium_plugin-0.3.2 (c (n "deuterium_plugin") (v "0.3.2") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "06c1y27viq7r3vhjx7ncjp1hkk528zfp8d7b40a5nzlqz7nk41sm")))

(define-public crate-deuterium_plugin-0.3.3 (c (n "deuterium_plugin") (v "0.3.3") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ab2bmiwf0saci1sz7n333n0c8dw2ryvfjk61g0m3ghpsnbahqbc")))

(define-public crate-deuterium_plugin-0.3.4 (c (n "deuterium_plugin") (v "0.3.4") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "00y1nccqc4fni4ldqm06nbzfdzzfkg8w16xifjv2m04yvwnahd1h")))

(define-public crate-deuterium_plugin-0.5.0 (c (n "deuterium_plugin") (v "0.5.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0xsskcikivz2xnhcmgwpfqv1ck5z9f35l5arhjw3gm5jp7pn9fks")))

(define-public crate-deuterium_plugin-0.5.1 (c (n "deuterium_plugin") (v "0.5.1") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0xipra4p20wfpncvp69xn9b98wwmg2a3r7lg1mrsn2r7x9agisvr")))

(define-public crate-deuterium_plugin-0.5.2 (c (n "deuterium_plugin") (v "0.5.2") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0nzmia7ys2jqdflwg8hgl9jgka6k2ck7ycy2na9azpvrs9cva04n")))

(define-public crate-deuterium_plugin-0.5.3 (c (n "deuterium_plugin") (v "0.5.3") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "111algi0alni9zdl28s5dall6bymac08y0q5q5iyq523zkscxq3s")))

