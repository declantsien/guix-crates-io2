(define-module (crates-io de ut deuterium) #:use-module (crates-io))

(define-public crate-deuterium-0.0.1 (c (n "deuterium") (v "0.0.1") (d (list (d (n "postgres") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1xja5078avwkd47v47lqyj317zj1c5rizqzg4fxd4a8fpzw4a9fa") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.1.1 (c (n "deuterium") (v "0.1.1") (d (list (d (n "postgres") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "19rg7zkf25jmhmzpklb0nyvvkjzvvvmq2irl48h2ihpw6kpxlm6b") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.2.0 (c (n "deuterium") (v "0.2.0") (d (list (d (n "postgres") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1ayaxrrss9cnjr0ibz6gs0nn8id382znqc028vcvpkk6kixn6byg") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.2.1 (c (n "deuterium") (v "0.2.1") (d (list (d (n "postgres") (r "*") (o #t) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "10dak4z966zvmy1iqdpx24vqgix2czgkiiqazmxkqdfajjj2csbd") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.3.0 (c (n "deuterium") (v "0.3.0") (d (list (d (n "postgres") (r "*") (f (quote ("uuid"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1y60anszxnkcxkpvm09d66pvdzbpnaxidzr7y4lnc79g4q6c7gi5") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.3.1 (c (n "deuterium") (v "0.3.1") (d (list (d (n "postgres") (r "*") (f (quote ("uuid"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0rn6wilc558bh8c3xkb0xd0inb7f0r6pm6zbc8bvqdra85l6z18p") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.3.2 (c (n "deuterium") (v "0.3.2") (d (list (d (n "postgres") (r "*") (f (quote ("uuid"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1nh65k2z3z1dxrhynd02c2bbb61aj4gz7qn6z0aqd53zvg9y55x1") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.3.3 (c (n "deuterium") (v "0.3.3") (d (list (d (n "postgres") (r "*") (f (quote ("uuid" "rustc-serialize" "time"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0dfqgc9vhczbhnagrf67kbnrm0hgrb682lkaz00fpm6qywkqnkfp") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.4.0 (c (n "deuterium") (v "0.4.0") (d (list (d (n "postgres") (r "*") (f (quote ("uuid" "rustc-serialize" "time"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0g6cgdwz7j2kagn6jg7a62ri76xjf4kc77nd31jpv29dhlvc9ldv") (f (quote (("default" "postgres"))))))

(define-public crate-deuterium-0.5.0 (c (n "deuterium") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "~0.15.2") (f (quote ("with-uuid" "with-serde_json" "with-chrono"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "189yf815v1806ql7nj7mlagia0xl3sxb9w4jvci46c93nk64wqcv") (f (quote (("default" "postgres"))))))

