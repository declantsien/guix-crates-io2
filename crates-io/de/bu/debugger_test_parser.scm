(define-module (crates-io de bu debugger_test_parser) #:use-module (crates-io))

(define-public crate-debugger_test_parser-0.1.0 (c (n "debugger_test_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1z5hxyafq8hh3sc9vikzlb42p3ygnxr8hr8lm4c0vg4kiaryj723") (y #t)))

(define-public crate-debugger_test_parser-0.1.1 (c (n "debugger_test_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1crdckrp7lgk1rg6vnf69ssvnxw96k0k2z39pqfdh6gq546qqmrv") (y #t)))

(define-public crate-debugger_test_parser-0.1.2 (c (n "debugger_test_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1arab1fznh5lphnqyyz9kddwh861rqrx29gfwpqxsab65prh3bvr") (y #t)))

(define-public crate-debugger_test_parser-0.1.3 (c (n "debugger_test_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0xcyxw0si7j4h701aksdd08j8jmrzc58833g66wm4xvp592kdrgb")))

