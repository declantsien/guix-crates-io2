(define-module (crates-io de bu debug_utils) #:use-module (crates-io))

(define-public crate-debug_utils-0.1.0 (c (n "debug_utils") (v "0.1.0") (h "0kdsfdmjybfl20p8nz5rlqmak8yrzzzlqivmr17m6fjzjc0c07rq")))

(define-public crate-debug_utils-0.1.1 (c (n "debug_utils") (v "0.1.1") (h "1xbvrc4gk69c9vakxbz5i7918a55v60jdvawshlj24vkahhfx5k9")))

(define-public crate-debug_utils-0.1.2 (c (n "debug_utils") (v "0.1.2") (h "0ja0j9rb8bv8zzdk6dlsanvbvdmvhnn99b4nsi59jrjhryf0z1rk")))

