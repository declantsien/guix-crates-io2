(define-module (crates-io de bu debug_stub_derive) #:use-module (crates-io))

(define-public crate-debug_stub_derive-0.1.0 (c (n "debug_stub_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0w37fjngs7h32vz2yj5sg3rjjgd63y51268g9m0yha35xh7iakp1")))

(define-public crate-debug_stub_derive-0.2.0 (c (n "debug_stub_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0qixdng1ps347sgjjlbnjsak30as691a3nc8fdaa98gsy2nxshxz")))

(define-public crate-debug_stub_derive-0.3.0 (c (n "debug_stub_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0szzlg89wnc40s7xmk7xfhlw6hiyzzbl21iprb1i6cw55y57yss9")))

