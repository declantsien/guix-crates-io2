(define-module (crates-io de bu debug-message) #:use-module (crates-io))

(define-public crate-debug-message-1.0.0 (c (n "debug-message") (v "1.0.0") (h "1kmq4q235m6zyf1079v5ap8zv41wy0kvf8i0z5j9j15n1wcr5435")))

(define-public crate-debug-message-1.1.0 (c (n "debug-message") (v "1.1.0") (h "0qk58pz4vwcyr4mhp3mj91z6qjzwx85ms9sg6inv819pjwxh0vzi")))

(define-public crate-debug-message-1.2.0 (c (n "debug-message") (v "1.2.0") (h "1v3k7iy1dq4zxwd5gnjiwq00ndc0c6a1cscswyzj0xzdbvzpa2ah")))

