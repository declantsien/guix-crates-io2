(define-module (crates-io de bu debug3-derive) #:use-module (crates-io))

(define-public crate-debug3-derive-0.1.0 (c (n "debug3-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "0qjaiw205ad58bfk1k3vhn0xg2v58bbr91njypiclykpv41bnz15")))

(define-public crate-debug3-derive-0.2.0 (c (n "debug3-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "0cmvckfri9lg0qinq3aggs76s9pjij0f6ywip87hiyyh76346c0q")))

(define-public crate-debug3-derive-0.3.0 (c (n "debug3-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "06vdxwql9mck3207w7bbzmbhkwifxds1bg8hv1apa5xg9gknxzkv")))

(define-public crate-debug3-derive-0.4.0 (c (n "debug3-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0g04j1rbix30r2n7hm1asazlbzv9z5j6whz4axf3gwwlmjmr9qvn")))

