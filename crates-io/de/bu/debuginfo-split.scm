(define-module (crates-io de bu debuginfo-split) #:use-module (crates-io))

(define-public crate-debuginfo-split-0.1.0 (c (n "debuginfo-split") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.17") (d #t) (k 0)))) (h "174w08bv2ickx3xz6sy8pys35h1z7dkzaj423f0dg4gkg5alnnl9")))

(define-public crate-debuginfo-split-0.1.18 (c (n "debuginfo-split") (v "0.1.18") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.18") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.18") (d #t) (k 0)))) (h "1r4kbq6aycjg13k7yax3zxlnzd70514skwaszv6szzs655hmnr6q")))

(define-public crate-debuginfo-split-0.1.19 (c (n "debuginfo-split") (v "0.1.19") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.19") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.19") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.19") (d #t) (k 0)))) (h "1n08cdprzdj8r2sb43ggmpd53nvv5gkd8hcjg5bfpsh6jvy4q2n4")))

(define-public crate-debuginfo-split-0.1.20 (c (n "debuginfo-split") (v "0.1.20") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.20") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.20") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.20") (d #t) (k 0)))) (h "1na0nyhk4g6kzd1yp6jbchby8ir3rzz9b01pfpibrzr0aa9n7b6x")))

(define-public crate-debuginfo-split-0.1.21 (c (n "debuginfo-split") (v "0.1.21") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.21") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.21") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.21") (d #t) (k 0)))) (h "092i9v4wjvj819bcs16id805yqqj16p0q5lswh453manprv8f175")))

(define-public crate-debuginfo-split-0.1.22 (c (n "debuginfo-split") (v "0.1.22") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core-wasm-ast") (r "^0.1.22") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.22") (d #t) (k 0)))) (h "13iay52rbznp9wdyf7lh9av926jyn1xmi2yljc91rss80dj1pyx2")))

