(define-module (crates-io de bu debug_finder) #:use-module (crates-io))

(define-public crate-debug_finder-0.1.0 (c (n "debug_finder") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1sp3198lxyw4i3wlfagjp639q3mcif8zkdmi7d5800ljjgiiljyp")))

(define-public crate-debug_finder-0.1.1 (c (n "debug_finder") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "15cd8a7nqs913sxkd1b94cgdh7gcm7lm6kn42yk0wkdsd4ki20qm")))

(define-public crate-debug_finder-0.1.2 (c (n "debug_finder") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0q8l77qcwqn86059mnf9d9cb2bk1slaxdx7a16mgq51hq3sxis6q")))

(define-public crate-debug_finder-0.2.0 (c (n "debug_finder") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ilazhwdab45ik5i8k87czk7rjk0l256hwdyq3fl6bqar6hjkls3")))

(define-public crate-debug_finder-0.2.1 (c (n "debug_finder") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1gj626nfw4qa7j3k84nz0788wf1j6ajwr9cgw393y12vif9gycri")))

(define-public crate-debug_finder-0.2.2 (c (n "debug_finder") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0lax8424ywd9gxiqswry33492053n0wvy315690klkdms8lhggar")))

(define-public crate-debug_finder-0.2.3 (c (n "debug_finder") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0zmbzg0mszd5v3xkqjw9c5ym1canljbndvk06b8qdj0lgy0xpihq")))

(define-public crate-debug_finder-0.2.4 (c (n "debug_finder") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1ma2xrpbhl9zhgidnx5005fb11d6hrgvyzsy99xr9k44p3avcry2")))

(define-public crate-debug_finder-0.3.0 (c (n "debug_finder") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0qj59ajcqj4gdsss3ddbd3bxjaanmxwsa937v3gxgwvacxa5hf2n")))

(define-public crate-debug_finder-0.3.1 (c (n "debug_finder") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1vq47rr100xgdm70czqhc0hcjak0dqlwabwk75k4zhic66cjzz55")))

