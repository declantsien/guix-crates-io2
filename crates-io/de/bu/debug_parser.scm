(define-module (crates-io de bu debug_parser) #:use-module (crates-io))

(define-public crate-debug_parser-0.1.0 (c (n "debug_parser") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "13p5p739cbm297f5r04z16m3nc6z7qi6pxiskxf4x634ijh0k8x6")))

(define-public crate-debug_parser-0.1.1 (c (n "debug_parser") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "00fl08gjy1y4ki9dhshf9p1f3f2zrraspd7anni0z6qkhdpj65d3") (y #t)))

(define-public crate-debug_parser-0.1.2 (c (n "debug_parser") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0r9lj25qvp47j0y46hdxi31qna4asb2p3w7mbw38c6hg3v05sb10")))

(define-public crate-debug_parser-0.1.4 (c (n "debug_parser") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0gb3n3wqmr5ljvqb7pk1jycqf6p5p45pbmf9iaaqfmqvs90d1pvi")))

