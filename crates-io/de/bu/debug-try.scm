(define-module (crates-io de bu debug-try) #:use-module (crates-io))

(define-public crate-debug-try-0.1.0 (c (n "debug-try") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "00967nxa1w3r5srmc1krfjq9fz7647mkx8h6vax2f1pkvhb13pix")))

