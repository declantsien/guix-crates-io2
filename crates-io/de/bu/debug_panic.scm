(define-module (crates-io de bu debug_panic) #:use-module (crates-io))

(define-public crate-debug_panic-0.1.0 (c (n "debug_panic") (v "0.1.0") (h "1iwzsr4picfh4isf0fgx6k0r657ynknz2kqpnavcfyb10a261ha9")))

(define-public crate-debug_panic-0.1.1 (c (n "debug_panic") (v "0.1.1") (h "0kdqxbagb0849931l4xlq0vyz1c8dzjfmm5k9i583pa59h1y3cf8")))

(define-public crate-debug_panic-0.1.2 (c (n "debug_panic") (v "0.1.2") (h "0w93p8541rw64h3i99g4l2h82k0ivhrcg8g2239n2nslx9hc83ca")))

(define-public crate-debug_panic-0.2.1 (c (n "debug_panic") (v "0.2.1") (h "19m2g8a55cq2pzrgjhb7i45m25n1r0p7v3gb3m1ykqpc1h8ynxwk")))

