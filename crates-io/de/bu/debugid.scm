(define-module (crates-io de bu debugid) #:use-module (crates-io))

(define-public crate-debugid-0.1.0 (c (n "debugid") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.36") (o #t) (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1j67lay3pfy6r3ffq2d8yd6m28n7bp33xk2zjp89c7pfc5ppwrkq") (f (quote (("with_serde" "serde" "serde_plain") ("default"))))))

(define-public crate-debugid-0.1.1 (c (n "debugid") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.36") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "04bnwspjq7n7dy8hj4gyvb9b9mc4vbwfzih9ryshfhdyv32nz3ih") (f (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.2.0 (c (n "debugid") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.36") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 2)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1q2820bqylpl99iigrswzdjfi10jlvy91xjpy5y4xgzfwf39di2p") (f (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.3.0 (c (n "debugid") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 2)) (d (n "uuid") (r "^0.7.0") (d #t) (k 0)))) (h "0cycrajjq2ij07lja8665l9xfcz1xcv949icw0lzr0y77i2ilnfd") (f (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.3.1 (c (n "debugid") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1z4jz7w5j9snpdcj7w1hs7i5imni5z1gjiqmcj397xbqj6x8ic7f") (f (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.4.0 (c (n "debugid") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1sg31gi6xcb8xjh584k37l197676y5vp79yyz57lj7pcmlkrd308") (f (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.5.0 (c (n "debugid") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0apps5c9fjibzz3pyd26jfcp6jjmk4vs34pl6y6m6ganqvyijl8s")))

(define-public crate-debugid-0.5.1 (c (n "debugid") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0qa83xgx3cwhqh4sj7f7dkyxl7nanll6li2bwj9gcwpng8rigbrm")))

(define-public crate-debugid-0.6.0 (c (n "debugid") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1p931x6y9lch5xxx1fdn1l23py6wd7y0ghy1ih8n2ym7ggxdcg0l") (y #t)))

(define-public crate-debugid-0.5.2 (c (n "debugid") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1ipckq5a3zp4kbyq3xdr1djvhf3vp91180jx8prwmxzqrklmjbdm")))

(define-public crate-debugid-0.5.3 (c (n "debugid") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "1x3xx40g5jppr5d0k6sw9082hvn4gaf14ci24rvslqxi8w9ss7bm")))

(define-public crate-debugid-0.7.0 (c (n "debugid") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0jzad3k1mmhy562rkb73a8crk191rbix3k26nb382xb0v7l2r7n4")))

(define-public crate-debugid-0.7.1 (c (n "debugid") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1z1am6i55gyskqv1pbnmcpk5mdpdva929wrjg3hlfxrxcqr4ha9n")))

(define-public crate-debugid-0.7.2 (c (n "debugid") (v "0.7.2") (d (list (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1fh2nl4dzbcm3bn9knb30phprm1mhr826wb268m7w2gjqalga77r")))

(define-public crate-debugid-0.7.3 (c (n "debugid") (v "0.7.3") (d (list (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0c370jsmnb0ahssda6f11l72ns1xpqrcmswa6y2zhknq66pqgvnn")))

(define-public crate-debugid-0.8.0 (c (n "debugid") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "13f15dfvn07fa7087pmacixqqv0lmj4hv93biw4ldr48ypk55xdy")))

