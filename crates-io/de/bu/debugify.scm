(define-module (crates-io de bu debugify) #:use-module (crates-io))

(define-public crate-debugify-0.1.0 (c (n "debugify") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w0hnnd9kf4dkhlrjbwfhk2ikmzp2vlkl2dgr5y0s54hi442ls0p")))

(define-public crate-debugify-0.2.0 (c (n "debugify") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12mv125bhfnh0lh2nv5zzm2xd6w5gmlv50ndixmqf33g048q37lf")))

