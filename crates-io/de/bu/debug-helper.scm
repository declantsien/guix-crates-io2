(define-module (crates-io de bu debug-helper) #:use-module (crates-io))

(define-public crate-debug-helper-0.1.0 (c (n "debug-helper") (v "0.1.0") (h "065dgbg5qb3ghzrzc456zz2jbgsinsil8z4ymn9v9czmnf74l2rb")))

(define-public crate-debug-helper-0.1.1 (c (n "debug-helper") (v "0.1.1") (h "1335fvbfk574q72lpzr8ay2987wbqi6xmi3caq7lwxbrhic61swi")))

(define-public crate-debug-helper-0.2.0 (c (n "debug-helper") (v "0.2.0") (h "1br516w6m2xnn1ym564w7549f5xsgji8is4907bi4vgd315y134f") (y #t)))

(define-public crate-debug-helper-0.1.2 (c (n "debug-helper") (v "0.1.2") (h "1bc5nnd11pm5q9rvkahq8vjygdwsc0hx5mk9f5lv3mrcc5j2w5m5")))

(define-public crate-debug-helper-0.1.3 (c (n "debug-helper") (v "0.1.3") (h "19h109jsjssq25hy5zl1dmsw2r29jj733sr89gmbrwsnfrgnpz6h")))

(define-public crate-debug-helper-0.1.4 (c (n "debug-helper") (v "0.1.4") (h "0zabz1jmr7n2k7hyqrc0s8wncd3mx897vl6zmgl9yyl69jf7zcpy")))

(define-public crate-debug-helper-0.2.1 (c (n "debug-helper") (v "0.2.1") (h "03hd2m2amsvwb74lvdqnp31fs20248dbz4cwrakmj0m19q5di5s1")))

(define-public crate-debug-helper-0.3.0 (c (n "debug-helper") (v "0.3.0") (h "147mqwl2h88g9qjwhgiwv46pbcpj4g3ly28jaw1k5fp199h4lh8x")))

(define-public crate-debug-helper-0.3.1 (c (n "debug-helper") (v "0.3.1") (h "05a7ma3vd0k1y7p0fq49n5d8nn6523riwrw9vdzsb2w6fsn855z5")))

(define-public crate-debug-helper-0.3.2 (c (n "debug-helper") (v "0.3.2") (h "18yq21fms9j8j03zy6yh4j07mf7r7fqwfrhzpbmvpw1ldqfxzfn7")))

(define-public crate-debug-helper-0.3.3 (c (n "debug-helper") (v "0.3.3") (h "1c7p95hf6qrbbmsjwwka0v2xivn7sr02yla4r0327lbqw65f6lwk")))

(define-public crate-debug-helper-0.3.4 (c (n "debug-helper") (v "0.3.4") (h "0b4a7sr9ck3sb2srb7h7qxk8m9diz0j5qbqrlhp5jzj6ahxfmbbx")))

(define-public crate-debug-helper-0.3.5 (c (n "debug-helper") (v "0.3.5") (h "05sf7ysym0bmfppzhadsxvfx7c508kcpk1jm79qji88qhga82pg1")))

(define-public crate-debug-helper-0.3.6 (c (n "debug-helper") (v "0.3.6") (h "1lqi0a4lxsbr9mr8m5awhcbmayvzsbpg4b4p5h45lzk5i7gxd298")))

(define-public crate-debug-helper-0.3.7 (c (n "debug-helper") (v "0.3.7") (h "1704g2qrvxcfw9rcf5kk0yryaykhs6r5vd5ss4l5cb2zcd0qbz4k")))

(define-public crate-debug-helper-0.3.8 (c (n "debug-helper") (v "0.3.8") (h "00ndg40a1yllbngkbhn8cqj612646kziw2wychp6p92k6dqmy54w")))

(define-public crate-debug-helper-0.3.9 (c (n "debug-helper") (v "0.3.9") (h "19a5n267i7p7q3hnv93mj4l8plkm6gkbpd5w9zsdcng8y2wnnvhp")))

(define-public crate-debug-helper-0.3.10 (c (n "debug-helper") (v "0.3.10") (h "0pj34zars6plbp054igqq1kiiqw8ia92b6zi8z144kzjjjw5p2ls")))

(define-public crate-debug-helper-0.3.11 (c (n "debug-helper") (v "0.3.11") (h "0k29p6slv6r580b0mqav28am77x2nq1p87x5vjyp6vw4cxl5jq24")))

(define-public crate-debug-helper-0.3.12 (c (n "debug-helper") (v "0.3.12") (h "1a73xl73psmzyihd62jy30g1acfmgjmyi2k8khmh170mrq6x3yvn")))

(define-public crate-debug-helper-0.3.13 (c (n "debug-helper") (v "0.3.13") (h "0bhnpzpgmg8dkdr27g2b49slf6ca79m4idcb01z2krs0qkifhy7m")))

