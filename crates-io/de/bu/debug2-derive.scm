(define-module (crates-io de bu debug2-derive) #:use-module (crates-io))

(define-public crate-debug2-derive-0.1.0 (c (n "debug2-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "06l73rixxpi14s61lj22vhpi7ahgccxa6kjvry9jjqyx864pxcvp")))

(define-public crate-debug2-derive-0.1.1 (c (n "debug2-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1nlydacs1v5y5cs7kb9wpa1ygdrsfw7nh0qy5z5fi1b442cd3c4g")))

