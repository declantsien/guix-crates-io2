(define-module (crates-io de bu debugging_helpers) #:use-module (crates-io))

(define-public crate-debugging_helpers-0.1.0 (c (n "debugging_helpers") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1awdnv78dsykmxa9y8238519vmqk06xx8cspqqaibk1acyzqy55k")))

