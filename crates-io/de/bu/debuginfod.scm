(define-module (crates-io de bu debuginfod) #:use-module (crates-io))

(define-public crate-debuginfod-0.0.0 (c (n "debuginfod") (v "0.0.0") (h "0g068n01danz6gjyhbkl2faz7rriwkzcdpdhc8g6xxmylb03n1fp")))

(define-public crate-debuginfod-0.1.0 (c (n "debuginfod") (v "0.1.0") (d (list (d (n "_log_unused") (r "^0.4.6") (d #t) (k 2) (p "log")) (d (n "_openssl_unused") (r "^0.10.35") (d #t) (k 2) (p "openssl")) (d (n "_rustc_version_unused") (r "^0.2.2") (d #t) (k 2) (p "rustc_version")) (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "blazesym") (r "^0.2.0-alpha.11") (k 2)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (o #t) (k 0)))) (h "0wyyw4jwpb7i9sl6xk33jvfj97hfmla4yz345cx98b67b4cyinps") (f (quote (("default" "fs-cache")))) (s 2) (e (quote (("fs-cache" "dep:tempfile")))) (r "1.64")))

