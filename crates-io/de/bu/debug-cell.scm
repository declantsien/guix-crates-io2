(define-module (crates-io de bu debug-cell) #:use-module (crates-io))

(define-public crate-debug-cell-0.1.0 (c (n "debug-cell") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.1") (d #t) (k 0)))) (h "0py1ly98yakzwk9jgzjgncnk189as7plll2843b401zawpwirq9k")))

(define-public crate-debug-cell-0.1.1 (c (n "debug-cell") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1vx4nggc7awp5xgkfjc7f4jk12nmwvfvqdbinl7l18155z04zvim")))

