(define-module (crates-io de bu debug-span) #:use-module (crates-io))

(define-public crate-debug-span-0.1.0 (c (n "debug-span") (v "0.1.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1hr4n4zqqagscvrbykz7rfci856ynb1dzj8p5c7wvlgd2chzapl4")))

(define-public crate-debug-span-0.2.0 (c (n "debug-span") (v "0.2.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1prx7sx0v3i5s91bg2qs69fdx7lmaflc08lw3kr8kix1203vdjn0") (f (quote (("default" "proc-macro2"))))))

