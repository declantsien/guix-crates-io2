(define-module (crates-io de bu debug-rs) #:use-module (crates-io))

(define-public crate-debug-rs-0.1.0 (c (n "debug-rs") (v "0.1.0") (d (list (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "globset") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "14jxj9y2vkv4hgzfl7pfrvmbhw8g6mwyicpx5l90rzfvfjh5kdbc") (f (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1.1 (c (n "debug-rs") (v "0.1.1") (d (list (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "globset") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0610hw9kln4kjn0yga2l92dh7n188l0vblx4kczvjnrn1b3v53g9") (f (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1.2 (c (n "debug-rs") (v "0.1.2") (d (list (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "globset") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1y2l1biqja0rb9ki1ydjzwsf1ic8q1jmy4j7czsw219hli13jq81") (f (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1.3 (c (n "debug-rs") (v "0.1.3") (d (list (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "globset") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1afqycfa42zpgxjmfnkc3xdbd7m7scdl2xwl31lbkjf9qy6ap2km") (f (quote (("disable") ("default" "debug_build_only") ("debug_build_only"))))))

