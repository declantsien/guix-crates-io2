(define-module (crates-io de bu debug-map-sorted) #:use-module (crates-io))

(define-public crate-debug-map-sorted-0.1.0 (c (n "debug-map-sorted") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1xvfkg647qvkbsssii8nnwhhlclwg4fdf4w99h9m32d5c4yms7lb")))

(define-public crate-debug-map-sorted-0.1.1 (c (n "debug-map-sorted") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "08dp7zbh8b9kqahwb6xv4ya4wqvb2shxmah7zbwlnwqq6sldzivm")))

