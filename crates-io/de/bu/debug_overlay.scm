(define-module (crates-io de bu debug_overlay) #:use-module (crates-io))

(define-public crate-debug_overlay-0.1.0 (c (n "debug_overlay") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "1p711aqw3z3rxkq52cyvg2k1vpp9z3xb9fvy4asb9kwkay26gxmw") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2.0 (c (n "debug_overlay") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "0nsk16nha02b3pl5n3llcx0xcz4d45rwrivjsghxy91qd6139yds") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2.1 (c (n "debug_overlay") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "0k3r0ghhfl4fqb54s0q5fyzqm2w5qzmm4vgw0pv5qz5if0h5nx66") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2.2 (c (n "debug_overlay") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "0lkszhs0w6dg8qs5fcjshw7id1a9mzv65gwrlcsdw5caczhi6lnb") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.3.0 (c (n "debug_overlay") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.19.3") (o #t) (d #t) (k 0)))) (h "0isxg8jk51bsrq97y6833mrpzv31h46ida48wjzc9213cvnd18fj") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.4.0 (c (n "debug_overlay") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "wgpu-core") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1zjya8gkz5kzjb31dk8zysq0iwzcf59ysjacwiq85iagc9bas2zv") (s 2) (e (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

