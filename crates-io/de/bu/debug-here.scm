(define-module (crates-io de bu debug-here) #:use-module (crates-io))

(define-public crate-debug-here-0.1.0 (c (n "debug-here") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "07z1fmy28p1b36y62q0qxaca1zfac0p9rf9i3k5sw6ksp4wnj73j")))

(define-public crate-debug-here-0.1.1 (c (n "debug-here") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "1mnj0vrq9nv659wcg72radn8hcl44z2s8d9hrd9an0bqkcidqvgd")))

(define-public crate-debug-here-0.1.2 (c (n "debug-here") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "0rj6y43mf253s2j3qslzzk3v71w5ky9fscnzkbks384x5j5z43xa")))

(define-public crate-debug-here-0.2.0 (c (n "debug-here") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "01960n564x7dw7yjckfvvbbhkbq6rhb53sshiyijci6lsl74wmb4")))

(define-public crate-debug-here-0.2.1 (c (n "debug-here") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("debugapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13qy9il1bkpagpp4v2f088mys2bmqs37wwx3hmxsx99rz5605nhl")))

(define-public crate-debug-here-0.2.2 (c (n "debug-here") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("debugapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cggcm85md5jsw0n0cngwahmwy3dkp11rmq6cjm31wsm7zw92xqx")))

