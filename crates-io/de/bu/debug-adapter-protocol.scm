(define-module (crates-io de bu debug-adapter-protocol) #:use-module (crates-io))

(define-public crate-debug-adapter-protocol-0.1.0 (c (n "debug-adapter-protocol") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.12") (d #t) (k 0)))) (h "0ym2pxikvfadiccz3jdblw8c3k73fwfhzxqbsnb4v7k2x6g30pwn")))

