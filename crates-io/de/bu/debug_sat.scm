(define-module (crates-io de bu debug_sat) #:use-module (crates-io))

(define-public crate-debug_sat-0.1.0 (c (n "debug_sat") (v "0.1.0") (h "1nj1334a9a2l0blgqws1a4khdzah8wv29xqln513lqlrkfv161mp")))

(define-public crate-debug_sat-0.2.0 (c (n "debug_sat") (v "0.2.0") (h "00in3zs5p5xbg4bify7gff5hx0ln35v3f1bzc7vzmb8jj3b4r9if")))

(define-public crate-debug_sat-0.3.0 (c (n "debug_sat") (v "0.3.0") (h "0388ik4yr56b2qp5cm4m2y9k5vz42pcw4fqf1d3vy3yrvlwcbd7v")))

(define-public crate-debug_sat-0.4.0 (c (n "debug_sat") (v "0.4.0") (h "1a62m8bf50qz52h3k6ckiali27mbrvla16b9b0d9d4myajnjzrdb")))

