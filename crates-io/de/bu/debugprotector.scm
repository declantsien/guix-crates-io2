(define-module (crates-io de bu debugprotector) #:use-module (crates-io))

(define-public crate-debugprotector-0.1.0 (c (n "debugprotector") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (d #t) (k 0)) (d (n "winsafe") (r "^0.0.13") (f (quote ("kernel"))) (d #t) (k 0)))) (h "1jxxx963ck1mf7vgkndppw3ws51sdnqjcvqb2lbyfprwyrdlyjvp") (y #t)))

(define-public crate-debugprotector-0.1.2 (c (n "debugprotector") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (d #t) (k 0)) (d (n "winsafe") (r "^0.0.13") (f (quote ("kernel"))) (d #t) (k 0)))) (h "12sa800nfvl4dpwf5ca5z1mzyplcmlxfcmbq9r258fjhb7c1y7lz")))

(define-public crate-debugprotector-0.1.3 (c (n "debugprotector") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (d #t) (k 0)) (d (n "winsafe") (r "^0.0.13") (f (quote ("kernel"))) (d #t) (k 0)))) (h "16pk87wixf919fnpr8s3wqas0bcwb49wvpdjhgm7j948qk852gvs")))

(define-public crate-debugprotector-0.1.4 (c (n "debugprotector") (v "0.1.4") (d (list (d (n "obfstr") (r "^0.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (d #t) (k 0)) (d (n "winsafe") (r "^0.0.13") (f (quote ("kernel"))) (d #t) (k 0)))) (h "1i7rf1kha4nnxmbqw2w8cm4gwpkp5gpka26z88wxysj4145azxny")))

