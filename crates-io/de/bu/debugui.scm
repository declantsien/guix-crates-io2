(define-module (crates-io de bu debugui) #:use-module (crates-io))

(define-public crate-debugui-0.1.0 (c (n "debugui") (v "0.1.0") (d (list (d (n "egui") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "egui-winit") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "linkme") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0z07w7fwps9rr9grh2q42fr1f0bi4rr63dng9523587zwnvd6j55") (f (quote (("enabled" "egui" "egui-winit" "egui-wgpu" "linkme" "parking_lot" "pollster" "wgpu"))))))

