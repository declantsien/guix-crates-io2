(define-module (crates-io de bu debugless-unwrap) #:use-module (crates-io))

(define-public crate-debugless-unwrap-0.0.1 (c (n "debugless-unwrap") (v "0.0.1") (h "1dv2q3zhny6f1bjybkqg9zz9aybs6j72jaxygvj8lqha9ciwr7x5")))

(define-public crate-debugless-unwrap-0.0.2 (c (n "debugless-unwrap") (v "0.0.2") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)))) (h "0b4cg8cj0rg5gbrwgnyl777al3si3jwbq576hnhx1275fyzlbpqw")))

(define-public crate-debugless-unwrap-0.0.3 (c (n "debugless-unwrap") (v "0.0.3") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "199vfr65ijwnlz8ic76q5yp73km55glp9ippgi1jqhak36llgvm3")))

(define-public crate-debugless-unwrap-0.0.4 (c (n "debugless-unwrap") (v "0.0.4") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "00445wsxxb33i35a0sgb5rnlnq3gvas6q9gjjf29w1hc1isx007l")))

