(define-module (crates-io de bu debug_tree) #:use-module (crates-io))

(define-public crate-debug_tree-0.2.1 (c (n "debug_tree") (v "0.2.1") (h "0cprnzhihc46ijdq1hdwmlnkyn5jiygmapxi8dcqajkgx7j6pa5i")))

(define-public crate-debug_tree-0.2.2 (c (n "debug_tree") (v "0.2.2") (h "0sjx764y62p9r7mz7g0s8bn4f29v2mxx6ravs0ivrckzfflwwzz9")))

(define-public crate-debug_tree-0.2.3 (c (n "debug_tree") (v "0.2.3") (h "07j44gvbapf5y9cf86lnjadglxah7adv9qghx945m8rhy6cq4rpg")))

(define-public crate-debug_tree-0.2.4 (c (n "debug_tree") (v "0.2.4") (h "08466dn7gh55da71avlr32w4ax1w3x9qjkna6mx1z6v43s0znyyk")))

(define-public crate-debug_tree-0.2.5 (c (n "debug_tree") (v "0.2.5") (h "0ir0bfz80gj31xiphc7b6lks81n3h7nqnkrl114iz90qwi1gf007")))

(define-public crate-debug_tree-0.3.0 (c (n "debug_tree") (v "0.3.0") (d (list (d (n "once_cell") (r "~1.3.1") (d #t) (k 0)))) (h "0ariw2izsdgh8b5hj9vmxkrjih6cxd69s008fcj95x2dw0pqjxmy")))

(define-public crate-debug_tree-0.3.1 (c (n "debug_tree") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "once_cell") (r "~1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("macros" "fs"))) (d #t) (k 2)))) (h "1jjiz3ny2hcqy60afdcbybwrycakihdw09sdp0bq7fz595kq0b7q")))

(define-public crate-debug_tree-0.4.0 (c (n "debug_tree") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("macros" "fs"))) (d #t) (k 2)))) (h "0mwfkaq8sld6y5f5dp5drl9qbr0sl4xjbr1l7hnr0i6qya1w67id")))

