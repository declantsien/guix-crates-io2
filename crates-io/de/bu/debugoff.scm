(define-module (crates-io de bu debugoff) #:use-module (crates-io))

(define-public crate-debugoff-0.1.0 (c (n "debugoff") (v "0.1.0") (d (list (d (n "const-random") (r "^0.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "goldberg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "0z8pzan6razjpq08w02m3l8vyk3y9sd20ry6qgfjgq6avyhn3l8w") (f (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.1.1 (c (n "debugoff") (v "0.1.1") (d (list (d (n "const-random") (r "^0.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "goldberg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "0wyj4vps7ai9ad0vkrknykvrf48wafq953b919wbgnv7x5ryvf5b") (f (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2.0 (c (n "debugoff") (v "0.2.0") (d (list (d (n "const-random") (r "^0.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "goldberg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "14q3cn01hy4dqpxfyc2h56xnzm3zr754czwfx4w0hjcl4sxlrrk4") (f (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2.1 (c (n "debugoff") (v "0.2.1") (d (list (d (n "const-random") (r "^0.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "goldberg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "0rjb69rffdh8y02cdrj1pzz5lphxg3b8a18zpfgjs9mdkr4235w2") (f (quote (("syscallobf") ("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2.2 (c (n "debugoff") (v "0.2.2") (d (list (d (n "const-random") (r "^0.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "goldberg") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "1ziddv0wmf36niknwpqs4ca1k4rgk8bpsxdpbavfnfnz5vagad51") (f (quote (("syscallobf") ("obfuscate" "goldberg"))))))

