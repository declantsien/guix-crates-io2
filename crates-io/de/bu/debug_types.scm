(define-module (crates-io de bu debug_types) #:use-module (crates-io))

(define-public crate-debug_types-1.0.0 (c (n "debug_types") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1vrcs9nph9c3xxb5684yz2k4z1vyv45qqnjgx07af9xxc0vz64r6")))

