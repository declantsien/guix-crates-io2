(define-module (crates-io de bu debug_plotter) #:use-module (crates-io))

(define-public crate-debug_plotter-0.1.0 (c (n "debug_plotter") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1nk36j5ly8rnzc16x9vwgxvnm1wn94yi8d17kaczxi5gghiggdx1") (f (quote (("default" "debug") ("debug" "plotters" "once_cell" "num-traits"))))))

(define-public crate-debug_plotter-0.1.1 (c (n "debug_plotter") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ax6l1ahdk4bf77lpps8wbmra57vbrzamrw8yljrk49x8fg8bndz") (f (quote (("default" "debug") ("debug" "plotters" "once_cell" "num-traits"))))))

(define-public crate-debug_plotter-0.2.0 (c (n "debug_plotter") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1bspgyvcxmxz8qhpskjhx40xn79acbybix3dph3ihg49bhlkmssy") (f (quote (("default" "debug") ("debug" "plotters" "num-traits"))))))

(define-public crate-debug_plotter-0.2.1 (c (n "debug_plotter") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1w88m3bv5sx2p27nh75z2c2ccv9cnxxpc5c6qvcvfvfx3p2hpja4") (f (quote (("plot-release") ("default" "debug") ("debug" "plotters" "num-traits"))))))

