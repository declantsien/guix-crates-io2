(define-module (crates-io de bu debug-derive) #:use-module (crates-io))

(define-public crate-debug-derive-0.0.0 (c (n "debug-derive") (v "0.0.0") (h "1789fdans8nigadmvlc3ncvi0qp9v07iqlx6027r8q4xnw50pdnp") (y #t)))

(define-public crate-debug-derive-2.0.1 (c (n "debug-derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1p7mi2w12bnw7vllq872b97wvkf9dq0bxyh2dc56cx7p4258znly") (f (quote (("std"))))))

(define-public crate-debug-derive-2.0.2 (c (n "debug-derive") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "11gqwphbi2sbl8qw4py4s9v562w03dxl07nfzdplyhcisr0dkrln") (f (quote (("std"))))))

(define-public crate-debug-derive-2.1.2 (c (n "debug-derive") (v "2.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "0g45hjpdv8rym9bd5r9v86q772jwairl0l1sq7mahja8x0rk55mi") (f (quote (("std"))))))

