(define-module (crates-io de bu debug_code) #:use-module (crates-io))

(define-public crate-debug_code-0.1.0 (c (n "debug_code") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1csrsqwfs0h535lncvihryg4dsdf83072mq0c8cbxwyjmrzdx556")))

(define-public crate-debug_code-0.1.1 (c (n "debug_code") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1llcspj5dixgj9xgnih12s3wnzwsq0s75i73a4lgn7kjbs7j5mj9")))

