(define-module (crates-io de bu debug-impl-parser) #:use-module (crates-io))

(define-public crate-debug-impl-parser-0.1.0 (c (n "debug-impl-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "0a4sp9v9gdvwpzgiq8d9w1k5ga9w3q5szmahphr4x5nz3nsfgwx3") (y #t)))

