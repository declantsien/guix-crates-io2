(define-module (crates-io de bu debug-ignore) #:use-module (crates-io))

(define-public crate-debug-ignore-1.0.0 (c (n "debug-ignore") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15gfp4q9688gziwjink34ycq052ya603bz5bsaksis3cxr4l1c48")))

(define-public crate-debug-ignore-1.0.1 (c (n "debug-ignore") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0i8prxp09456asxs4v7r1hb26lapz4rrkpfxl3q92i2fbb6qjc12")))

(define-public crate-debug-ignore-1.0.2 (c (n "debug-ignore") (v "1.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "06vx6dryyyxrj03iav9qc08rygpzyhyrlhp66gwvm4cbbx66jlby")))

(define-public crate-debug-ignore-1.0.3 (c (n "debug-ignore") (v "1.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1kzld8z7bldjmnnn3vlpm6m2j9hgizkk7lfwkm4kqirgkssb0j2b")))

(define-public crate-debug-ignore-1.0.4 (c (n "debug-ignore") (v "1.0.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l0dgsf4mbpvkqz62shg6fchqwdpjxxn0j2sd5xjh9s86cdqq31y")))

(define-public crate-debug-ignore-1.0.5 (c (n "debug-ignore") (v "1.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08gwdny6124ggy4hyli92hdyiqc5j2z9lqhbw81k0mgljcfyvrzz")))

