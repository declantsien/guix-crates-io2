(define-module (crates-io de bu debug_unreachable) #:use-module (crates-io))

(define-public crate-debug_unreachable-0.0.1 (c (n "debug_unreachable") (v "0.0.1") (h "17a1pjv0kyg9n5sma8vf3h7jr003sjsv77kh3ljgw5fl56dwxy7z")))

(define-public crate-debug_unreachable-0.0.2 (c (n "debug_unreachable") (v "0.0.2") (h "0c0bj5xzni7izdzgx7vmir59rvzvvqxm7sjg3gm7chirc8pyj89a")))

(define-public crate-debug_unreachable-0.0.3 (c (n "debug_unreachable") (v "0.0.3") (h "07f9i0mhha2c88mzmwsgk86zgkxpmrlppnlmswv0apy8h7z1fcax")))

(define-public crate-debug_unreachable-0.0.4 (c (n "debug_unreachable") (v "0.0.4") (h "14dawd721krspzk2k9502b1msz9d7amlgcf3n3pq8vn7gbb8n96a")))

(define-public crate-debug_unreachable-0.0.5 (c (n "debug_unreachable") (v "0.0.5") (d (list (d (n "unreachable") (r "^0") (d #t) (k 0)))) (h "1lldjhb98brkbiq7rcgcnz06f2kz95pkx198nhscrm309rqmrhsl")))

(define-public crate-debug_unreachable-0.0.6 (c (n "debug_unreachable") (v "0.0.6") (d (list (d (n "unreachable") (r "^0") (d #t) (k 0)))) (h "0y5zis8pdzjicnvb9l09fd9738p45b2mm25n1srhpjbz6l8pgw46")))

(define-public crate-debug_unreachable-0.1.0 (c (n "debug_unreachable") (v "0.1.0") (d (list (d (n "unreachable") (r "^0") (d #t) (k 0)))) (h "0nx61j5ayzrbdzha1nxdyp49nmbywhw0vmhfv1k1in8bwwbqxs4h")))

(define-public crate-debug_unreachable-0.1.1 (c (n "debug_unreachable") (v "0.1.1") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1cx4vh1d0bxdpb8l9lyiv03az4527lykxy39s4a958swf2n2w0ws")))

