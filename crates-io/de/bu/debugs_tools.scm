(define-module (crates-io de bu debugs_tools) #:use-module (crates-io))

(define-public crate-debugs_tools-0.1.0 (c (n "debugs_tools") (v "0.1.0") (h "15qj2zj4cnwcx67n86gzdw1dhc6yjg88wwjp65r7y6cic527fc6p")))

(define-public crate-debugs_tools-0.2.0 (c (n "debugs_tools") (v "0.2.0") (h "1a2f774hr4czxm0ys0jgskm8nqv5rc01q5b0ibw6k10rk4zwlbrf")))

(define-public crate-debugs_tools-0.2.1 (c (n "debugs_tools") (v "0.2.1") (h "1n27yqhw61q44mvyy7fdr05q2g6f9wdfc8yiwmm4s756q464hvah")))

