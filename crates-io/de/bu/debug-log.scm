(define-module (crates-io de bu debug-log) #:use-module (crates-io))

(define-public crate-debug-log-0.1.0 (c (n "debug-log") (v "0.1.0") (h "1lrxf7mpw8zvnwq4zcnwyy7d24ix1yb2yx32aaczqivcf33r1cby")))

(define-public crate-debug-log-0.1.1 (c (n "debug-log") (v "0.1.1") (h "02y6xwzlz369ivd316pd559nrl49zpp63impc5318nx2cn7cwb6d")))

(define-public crate-debug-log-0.1.2 (c (n "debug-log") (v "0.1.2") (h "0dxga8f0j2pc1vi3zqryhn9jkw3kgf8l2mlql2nplipf2b8z4wgs")))

(define-public crate-debug-log-0.1.3 (c (n "debug-log") (v "0.1.3") (h "0f6w6nfd5xcvdrcvld3jpf39lqj77a0ga7xmwf220dj4d1k5r8r2")))

(define-public crate-debug-log-0.1.4 (c (n "debug-log") (v "0.1.4") (h "0m32fr5m4h7hj2xarz8v8mwis7fwqz4zvkbd0ady7ziia1ygakrn")))

(define-public crate-debug-log-0.2.0 (c (n "debug-log") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (o #t) (d #t) (k 0)))) (h "1y82ih1qpjsfsf3360z7mxfkaamsw2ahnp6g3aqrbgvfrikariz4") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.2.1 (c (n "debug-log") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (o #t) (d #t) (k 0)))) (h "0iy9fabw15brwxb9d3092l84zc55vyf2md526arycw1zs1ykxl93") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.2.2 (c (n "debug-log") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0wvcn909a1im89485j4ij8xyyspcj8ikckl7baillk0l1jf9s3xr") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.3.0 (c (n "debug-log") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0qwxc06dpjm1hqilm079mkkljqwsc0lq61h9ssnzjd9gf3jggqcy") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.3.1 (c (n "debug-log") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0fjch669sr9hrrsvbrzlxj9d2m4ar5yb9j6c5ibgq8zc56v63y6a") (f (quote (("wasm" "wasm-bindgen"))))))

