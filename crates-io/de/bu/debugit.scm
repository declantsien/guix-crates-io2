(define-module (crates-io de bu debugit) #:use-module (crates-io))

(define-public crate-debugit-0.1.0 (c (n "debugit") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1p40n97nqyyhsx6qm2qnrrlzznkxazn29c6qxwb3s3qgh69ab1y7")))

(define-public crate-debugit-0.1.1 (c (n "debugit") (v "0.1.1") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0awa9x9qizb6f1s9l381dkx5rf6jxjnnjrlj2qk12zxzx5ql8i2p")))

(define-public crate-debugit-0.1.2 (c (n "debugit") (v "0.1.2") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1qr5d2j0zk2gpc2qyf5pwq0k7qyz38yf49q3fngv1wjd0gizghk3")))

(define-public crate-debugit-0.2.0 (c (n "debugit") (v "0.2.0") (h "09pi91knrdkjcnrsvsssz4a3l223frpz2w3375a3bhrnsmdr0rr3")))

