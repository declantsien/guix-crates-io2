(define-module (crates-io de bu debug-here-gdb-wrapper) #:use-module (crates-io))

(define-public crate-debug-here-gdb-wrapper-0.1.0 (c (n "debug-here-gdb-wrapper") (v "0.1.0") (h "00r7a0vj7jlc3mp0id60k7xvqzc24vlh6yswp1x0l6af86qjj582")))

(define-public crate-debug-here-gdb-wrapper-0.2.0 (c (n "debug-here-gdb-wrapper") (v "0.2.0") (h "0ffvfnjfp0ql17fw2gvnw8jw1r7d22yr57daa2pppv2nbmypl42m")))

