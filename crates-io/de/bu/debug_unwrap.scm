(define-module (crates-io de bu debug_unwrap) #:use-module (crates-io))

(define-public crate-debug_unwrap-0.0.1 (c (n "debug_unwrap") (v "0.0.1") (h "1ydqk9k1zfnxk0gq4mnac9y2r6719bg8rgcha5qw9qpsh1v36y3i") (y #t)))

(define-public crate-debug_unwrap-1.0.0 (c (n "debug_unwrap") (v "1.0.0") (h "10na7xs7vqvb3ncmy6z82cqfmih3p6vmnm7jv16ap8q4jwydrj11") (f (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1.0.1 (c (n "debug_unwrap") (v "1.0.1") (h "1a5l36k3zd1ji9y0imfj6g6ydfl35m9wz4fzn1ysmk81kr627k4n") (f (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1.0.2 (c (n "debug_unwrap") (v "1.0.2") (h "1xnppv9kav5p7cgjy9ck6p6gkp1gd9pnidrsfrgrwanzw4rk6vbv") (f (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1.0.3 (c (n "debug_unwrap") (v "1.0.3") (h "027hdifsx0mbv1r4h9j6yjhvx68sn2k8fmmij9wjd79if1yn98g7") (f (quote (("peel") ("out") ("o") ("deprecate") ("default" "out"))))))

