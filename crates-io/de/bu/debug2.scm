(define-module (crates-io de bu debug2) #:use-module (crates-io))

(define-public crate-debug2-0.1.0 (c (n "debug2") (v "0.1.0") (d (list (d (n "debug2-derive") (r "^0.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)))) (h "0k642jy892jy1dhyfirx5590g4282x5ky9s6j891hqyzlcp0cw2y")))

(define-public crate-debug2-0.1.1 (c (n "debug2") (v "0.1.1") (d (list (d (n "debug2-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)))) (h "1c00fnd0r4925yhrfjdiig68sd23138ir5m1jws2s665f6gk7ams")))

