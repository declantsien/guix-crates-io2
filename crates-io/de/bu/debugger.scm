(define-module (crates-io de bu debugger) #:use-module (crates-io))

(define-public crate-debugger-0.0.1 (c (n "debugger") (v "0.0.1") (d (list (d (n "capstone") (r "^0.11.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "fireball") (r "^0.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1n2rk6xs8qhsyz317ww1y6swiwlp0ixv8m2n49f44z9qg3b74aby")))

