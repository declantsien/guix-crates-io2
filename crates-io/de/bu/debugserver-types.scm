(define-module (crates-io de bu debugserver-types) #:use-module (crates-io))

(define-public crate-debugserver-types-0.2.0 (c (n "debugserver-types") (v "0.2.0") (d (list (d (n "schemafy") (r "^0.2.0") (d #t) (k 1)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1k0wy3hf0lkd76bxz13p9wdzdvw9xf1fpp7pja6w29fl6wh50c5p")))

(define-public crate-debugserver-types-0.1.0 (c (n "debugserver-types") (v "0.1.0") (d (list (d (n "schemafy") (r "^0.1.1") (d #t) (k 1)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "00kakwiznlyq5licacacl7kbwvfqwdkjydl96l00xgj03n7ivmyc")))

(define-public crate-debugserver-types-0.3.0 (c (n "debugserver-types") (v "0.3.0") (d (list (d (n "schemafy") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15y8mg4g0yfd6zlx45mgv2y80d4cakx6kfqz81xhdjfad9rqs57n")))

(define-public crate-debugserver-types-0.4.0 (c (n "debugserver-types") (v "0.4.0") (d (list (d (n "schemafy") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xv0a3kryn3ph7gl2cl8kcs24jirsmdsdkdq0rbznsns9r3rr83m")))

(define-public crate-debugserver-types-0.5.0 (c (n "debugserver-types") (v "0.5.0") (d (list (d (n "schemafy") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jmgv2f77y1k20gldkvf3w7ibshb34kxz0hqwkjfh57df1587xib")))

