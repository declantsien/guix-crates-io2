(define-module (crates-io de de dede) #:use-module (crates-io))

(define-public crate-dede-0.1.0 (c (n "dede") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fasfp62bw9vkpch7wkq3rza591l9wm618j2n9airlfllnnf42gb")))

(define-public crate-dede-0.1.1 (c (n "dede") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18dd7nsqjqy9hw1j5crlasdf5vf07lkp8ld0bwwakhazc89an5i6")))

