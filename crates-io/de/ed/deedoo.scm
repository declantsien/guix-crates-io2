(define-module (crates-io de ed deedoo) #:use-module (crates-io))

(define-public crate-deedoo-0.1.0 (c (n "deedoo") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0pjr87dgkw1wnhg6i7dw1sdfp3dg0983pfv1gl7rg197fpzznzmb")))

(define-public crate-deedoo-0.1.2 (c (n "deedoo") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1679p9qgnyg68dy7wdcnxpd62f36vz7mjy3prkhmhqiag6zfgywf")))

(define-public crate-deedoo-0.1.3 (c (n "deedoo") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0p80q4mv8d8pbsqrwhk1r3cy4wk4qdk17cl1ya2rgjysl631fy3h")))

