(define-module (crates-io de ct dectree-rs) #:use-module (crates-io))

(define-public crate-dectree-rs-0.1.0 (c (n "dectree-rs") (v "0.1.0") (h "0k6lh15ygh8ypxr5yhrv6w8pg4h94clms5rlpqs5in65pnm1skvi")))

(define-public crate-dectree-rs-0.1.1 (c (n "dectree-rs") (v "0.1.1") (h "1s8hyq6qs6yqf8lrb916q142j888kji4fw4862sappfdrf1kga4r")))

