(define-module (crates-io de sd desdemona) #:use-module (crates-io))

(define-public crate-desdemona-0.1.0 (c (n "desdemona") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "text_io") (r "^0") (d #t) (k 0)))) (h "1n591n11cr4na9nqfmd7lpfy064kcxnm10gghbi5zfch0vhlljy1")))

(define-public crate-desdemona-0.1.1 (c (n "desdemona") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "text_io") (r "^0") (d #t) (k 0)))) (h "1g30zc6rmfj9f23pjwh3kbazjm2xlzs6fc7kj0rr9jmrvmz90ixp")))

(define-public crate-desdemona-0.2.0 (c (n "desdemona") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "text_io") (r "^0") (d #t) (k 0)))) (h "1ksvn6fk9wdxl44fdjyzi2lqkngz0vh5kdlvgzsmms44rj9qli2p")))

(define-public crate-desdemona-0.2.1 (c (n "desdemona") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "text_io") (r "^0") (d #t) (k 0)))) (h "0frlz0l5fddzrmn0m7d658i2vwgyq61f587qykz8zqkgaf447znj")))

(define-public crate-desdemona-0.3.0 (c (n "desdemona") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "text_io") (r "^0") (d #t) (k 0)))) (h "0fn2awjr57ql29q7knxzz9wvqmhzp96385fgvvk48g8nlxy1ghk2")))

