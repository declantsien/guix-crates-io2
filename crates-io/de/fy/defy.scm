(define-module (crates-io de fy defy) #:use-module (crates-io))

(define-public crate-defy-0.1.0 (c (n "defy") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "0czbcqr4b4k4b45f9mqf18yxcy3dsqabw4jcqckakvjhqqlv7q6c")))

(define-public crate-defy-0.1.1 (c (n "defy") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "1y8d14mwnjlsskjh4synjsr4a8ra8ndivfjs90dw457cqc4a3yp9")))

(define-public crate-defy-0.1.2 (c (n "defy") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "0rzl9n8is65bv6k74y37qyrnlhy4dzcz3qsba6xzsvvw4kzlkg36")))

(define-public crate-defy-0.1.3 (c (n "defy") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "0n9mmw7ddwc10dx46n606gxlwidg00zlv8qyjsdv0whjpird0a4q")))

(define-public crate-defy-0.1.4 (c (n "defy") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "0cwlcm99canqr2ydxfz22rmjdawzjbwk8y26ycjq4j7nar180n19")))

(define-public crate-defy-0.1.5 (c (n "defy") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "yew") (r "^0.20.0") (f (quote ("ssr"))) (d #t) (k 2)))) (h "09mv7lfq7sc97crh7yd3rnnakr5d9nfqlyg6ilv9wi7a5n9z5d37")))

