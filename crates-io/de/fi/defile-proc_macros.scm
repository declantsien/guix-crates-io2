(define-module (crates-io de fi defile-proc_macros) #:use-module (crates-io))

(define-public crate-defile-proc_macros-0.1.0 (c (n "defile-proc_macros") (v "0.1.0") (h "03fv3n5gvdbdmmb5iha9i4wcmi6ymimcxq3qf2hvdpyi7p2r0h2y")))

(define-public crate-defile-proc_macros-0.1.1 (c (n "defile-proc_macros") (v "0.1.1") (h "10ayrrbkgs5n0n3jwr7f1pga61brsz7n68ykcr3vycf36s9a2l0h")))

(define-public crate-defile-proc_macros-0.1.2 (c (n "defile-proc_macros") (v "0.1.2") (h "0ilv9bczvssvgjd3da4ama0j2daa4fkl0jiih9xcrz2fmj1l8vzq")))

(define-public crate-defile-proc_macros-0.2.0-rc1 (c (n "defile-proc_macros") (v "0.2.0-rc1") (h "1gcv7lhhhmljqf369vqh2gp4icvc19hlf82i75yqn95xm263k0yk")))

(define-public crate-defile-proc_macros-0.2.0 (c (n "defile-proc_macros") (v "0.2.0") (h "0h4inx0cv80a0wh8bjfj16h7kkvgjx4ff28qw4qxspr0afmpz8jm")))

(define-public crate-defile-proc_macros-0.2.1 (c (n "defile-proc_macros") (v "0.2.1") (h "0pi4h1whsslfmcxr31fwkh2hypifafpbdslzi6g723zv1d3c3ic7")))

