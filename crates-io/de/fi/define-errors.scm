(define-module (crates-io de fi define-errors) #:use-module (crates-io))

(define-public crate-define-errors-0.1.0 (c (n "define-errors") (v "0.1.0") (h "0njmxhdaqlg4jd3y6qplkrwwg9y1v3zhj4plpxjmalv4q2pj94w7") (y #t)))

(define-public crate-define-errors-0.1.1 (c (n "define-errors") (v "0.1.1") (h "0nydjbzgfy0ds28jahfi49lfizykl84k5x0q96ihv6h8nrgsim8r") (y #t)))

(define-public crate-define-errors-0.1.2 (c (n "define-errors") (v "0.1.2") (h "16xxivph4qi7qx1hnrs9zk6a5m0rsyzjrdxpr7cyrjmqbfxiawbr") (y #t)))

(define-public crate-define-errors-0.1.3 (c (n "define-errors") (v "0.1.3") (h "0xwcwdnbkwlp2znm1x3vs58zrjvyd3cs5s3z951m9m3pc39j30gl") (y #t)))

(define-public crate-define-errors-0.1.4 (c (n "define-errors") (v "0.1.4") (h "06p5wig3abjjl3b8vkf00cy8qfnk0hpigljxypxqnmfh076s9sf8") (y #t)))

(define-public crate-define-errors-0.1.5 (c (n "define-errors") (v "0.1.5") (h "10i293qfhmj919cxs75fq2m4yaszyw75f0aij4apgvkhiphdgsrp") (y #t)))

(define-public crate-define-errors-0.1.6 (c (n "define-errors") (v "0.1.6") (h "14qkch3iq72ixcifv03d6ssb05cl0wd6jl99x0wf634jnzp1dm9j") (y #t)))

(define-public crate-define-errors-0.1.7 (c (n "define-errors") (v "0.1.7") (h "11412d59pl0l956n2d6s5anys8dlcfx3j2akc07w7vv7k8n23kd2") (y #t)))

(define-public crate-define-errors-0.1.8 (c (n "define-errors") (v "0.1.8") (h "17j5xczrk1m7yklkgvpr109n7l3ahizdq9nx38d06b9ry28iqvy0")))

