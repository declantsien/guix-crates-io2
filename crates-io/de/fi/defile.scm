(define-module (crates-io de fi defile) #:use-module (crates-io))

(define-public crate-defile-0.1.0 (c (n "defile") (v "0.1.0") (d (list (d (n "proc_macros") (r "^0.1.0") (d #t) (k 0) (p "defile-proc_macros")))) (h "1qy2rljqyfj7adgw2baibk85s29b4555yg16dh3sxxcf4lad8w1d") (f (quote (("nightly"))))))

(define-public crate-defile-0.1.1 (c (n "defile") (v "0.1.1") (d (list (d (n "proc_macros") (r "^0.1.1") (d #t) (k 0) (p "defile-proc_macros")))) (h "0m8a7ssg8f2k4zn27hd1lcjj3znclz10laa2kprlkyv6nmlq9s7r") (f (quote (("nightly"))))))

(define-public crate-defile-0.1.2 (c (n "defile") (v "0.1.2") (d (list (d (n "proc_macros") (r "^0.1.2") (d #t) (k 0) (p "defile-proc_macros")))) (h "04r5b4r9dw0h3q74c4rk81kcpq2x3wyfkrfd0nj7i8f9yygp8qj3") (f (quote (("nightly"))))))

(define-public crate-defile-0.2.0-rc1 (c (n "defile") (v "0.2.0-rc1") (d (list (d (n "defile-proc_macros") (r "=0.2.0-rc1") (d #t) (k 0)))) (h "0w307qy4wzwnbzm4pljm7rmq09b57z5qz09211rp9abihd5qf6mk") (f (quote (("better-docs"))))))

(define-public crate-defile-0.2.0 (c (n "defile") (v "0.2.0") (d (list (d (n "defile-proc_macros") (r "=0.2.0") (d #t) (k 0)))) (h "07z2anrc9ppa3z8xpw41lp821w514nhjj2v1iha91hiw1bvff4g1") (f (quote (("better-docs"))))))

(define-public crate-defile-0.2.1 (c (n "defile") (v "0.2.1") (d (list (d (n "defile-proc_macros") (r "=0.2.1") (d #t) (k 0)))) (h "0bclrn061p74fmvl36aqfrbr8jm46lw6vhf10g48c0l5anj28czs") (f (quote (("better-docs"))))))

