(define-module (crates-io de fi define_error) #:use-module (crates-io))

(define-public crate-define_error-1.0.0 (c (n "define_error") (v "1.0.0") (h "1cypcx4yn9ps78kgh5nlqj9d95n1lmay5wxn67p3qxrajyg1ifms")))

(define-public crate-define_error-1.1.0 (c (n "define_error") (v "1.1.0") (h "147dkql20f0934nsl99c6b6ircw966nzd6zcvwbf9lx032r26vd9")))

(define-public crate-define_error-1.2.0 (c (n "define_error") (v "1.2.0") (h "0wb3218aih52b47ny7r1wyzxdwj8mfsi8sxk58n0dy0jibnjil2j")))

(define-public crate-define_error-1.3.0 (c (n "define_error") (v "1.3.0") (h "1pl05fp358iyq2h32xzv9mz1cvncl813ayb0a0djabyvdxs4wgp9")))

(define-public crate-define_error-2.3.0 (c (n "define_error") (v "2.3.0") (h "0lm17wpbyadzlhplq3m9p3wxzf8nwpccdn13pk9mfr113l81qs61")))

(define-public crate-define_error-3.0.0 (c (n "define_error") (v "3.0.0") (h "0yca988nzax8dkg1bgjn1d1a7zhgazh81j6j3gy1a4ag7rr7mndr")))

(define-public crate-define_error-4.0.0 (c (n "define_error") (v "4.0.0") (h "1m92b2y5yqzdf57gx7js91fhrg2r0nqjp2wy6d7jciyfx2v04hqa")))

(define-public crate-define_error-4.0.1 (c (n "define_error") (v "4.0.1") (h "128jf6ilkn4h67r51qh6zvdb4mkv5zqkx7386ny6irdbznagffb9")))

