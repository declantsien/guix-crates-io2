(define-module (crates-io de fi defios) #:use-module (crates-io))

(define-public crate-defios-0.1.0 (c (n "defios") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "solana-program") (r "~1.16.21") (d #t) (k 0)))) (h "17pzn4fnxshkgpsxnrr8gsa5clawiphd4fdr1bqsvs2k59xbx9h2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

