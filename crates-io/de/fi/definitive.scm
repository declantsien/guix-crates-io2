(define-module (crates-io de fi definitive) #:use-module (crates-io))

(define-public crate-definitive-0.0.0-alpha.0 (c (n "definitive") (v "0.0.0-alpha.0") (h "0nc9jx355ppjp6ny47shsrw21mn1jv0ny7h08l6d54420cyljfxw") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0-alpha.1 (c (n "definitive") (v "0.0.0-alpha.1") (h "0azz19lqziby4n90j02snmnbhglqyx9h733f5ydml4vw4k6ilq7a") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0-alpha.2 (c (n "definitive") (v "0.0.0-alpha.2") (h "1z78v114gsg16nfnlf9mfz9w8g4r0fsv8h366rbrlapnq88v4ixy") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0-alpha.3 (c (n "definitive") (v "0.0.0-alpha.3") (h "1b0a134j2ij9gcdwzhii24q7sfr3hc2sgk5j0bwg0cwgl6czk2j3") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-definitive-0.1.0 (c (n "definitive") (v "0.1.0") (h "1ggwblp9vqgxbl5gql0sw8yxq98sbasm0m2ard1mgn3rhg5ldqxj") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

