(define-module (crates-io de fi define_into_enum) #:use-module (crates-io))

(define-public crate-define_into_enum-0.1.0 (c (n "define_into_enum") (v "0.1.0") (h "13lhl3icms1ff34vscfzrcm06z716bvhh17fzw6b33vj9y95lpq7") (y #t)))

(define-public crate-define_into_enum-0.1.1 (c (n "define_into_enum") (v "0.1.1") (h "1xdw1cjz41j76bmm5y6chwlf3ndc5mlb938r1s06vcm8sz28jxap")))

