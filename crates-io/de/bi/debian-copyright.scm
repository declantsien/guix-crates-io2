(define-module (crates-io de bi debian-copyright) #:use-module (crates-io))

(define-public crate-debian-copyright-0.1.2 (c (n "debian-copyright") (v "0.1.2") (d (list (d (n "deb822-lossless") (r "^0.1.2") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "07ydm6ir7bhpn0nlkxmqz4862zwalwv4pq91s6qm7zlq3fqdwdvx")))

(define-public crate-debian-copyright-0.1.7 (c (n "debian-copyright") (v "0.1.7") (d (list (d (n "deb822-lossless") (r "^0.1.7") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "02bz8m6vcz6pwxwdlsj67wc50ynfk311awqr8w68r5d00qlpd0xx")))

(define-public crate-debian-copyright-0.1.8 (c (n "debian-copyright") (v "0.1.8") (d (list (d (n "deb822-lossless") (r "^0.1.7") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "06drlsbm7a2qzfdw9fwibjfsbl821bsm8fk526f0li2b8vgkd0kd")))

