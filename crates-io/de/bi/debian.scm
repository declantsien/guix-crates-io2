(define-module (crates-io de bi debian) #:use-module (crates-io))

(define-public crate-debian-0.1.0 (c (n "debian") (v "0.1.0") (d (list (d (n "chrono") (r ">= 0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "14rbcwy9v74zp1p9rq979asxv58wsnlk2qlv0q0r61b88sr7avlc")))

(define-public crate-debian-0.1.1 (c (n "debian") (v "0.1.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1paaffgj7kkfn8rhl752jwfzvafc525d1mhkrddsxrz9gkh4kiv5")))

