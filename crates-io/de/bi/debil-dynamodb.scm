(define-module (crates-io de bi debil-dynamodb) #:use-module (crates-io))

(define-public crate-debil-dynamodb-0.1.0 (c (n "debil-dynamodb") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "debil") (r "^0.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.43.0") (d #t) (k 0)))) (h "1w43cmzdpy5s24crxw3r6yb7y8z3kvd9bpvx622k19m0ra2pyp20")))

(define-public crate-debil-dynamodb-0.1.1 (c (n "debil-dynamodb") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "debil") (r "^0.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.43.0") (d #t) (k 0)))) (h "129956zhmimkisf9qdh0lqfgbpwiv653411w6xix71q588k35svh")))

(define-public crate-debil-dynamodb-0.1.2 (c (n "debil-dynamodb") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "debil") (r "^0.3.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.43.0") (d #t) (k 0)))) (h "1cnapa7g5ilkdmf4f4qds6q6zl7lmp62lhk071l7al4pr0fvjmgx")))

