(define-module (crates-io de bi debil) #:use-module (crates-io))

(define-public crate-debil-0.1.0 (c (n "debil") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0s5p8kq1chml911hbpawirdp7h1zmj7d7cf5mxv578rdy3igvg64")))

(define-public crate-debil-0.1.1 (c (n "debil") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1rk0sk02q12ridrxqsbp7915inbga1pl0axrrmmfw8iqqql9x4ch")))

(define-public crate-debil-0.1.2 (c (n "debil") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1x6nwkpqg1g78k3jxv7szwi0pkn9zi3f6hmqrhlcf41afj7q1xps")))

(define-public crate-debil-0.2.0 (c (n "debil") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1vvqk8vg8x0zz9q8lddwv18qd3vm70fp0ajc4dz3zii6b0icksrc")))

(define-public crate-debil-0.2.1 (c (n "debil") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1n7rwpv8srr6qghhsq8rnqdj441slvkq7rchgylz8mpdsjm5v296")))

(define-public crate-debil-0.3.0 (c (n "debil") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.0") (d #t) (k 0)))) (h "07g650r6f2dfpbmhzkxgbbwpcxbmir64alz3xz5kcip9ab2l491x")))

(define-public crate-debil-0.3.1 (c (n "debil") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.0") (d #t) (k 0)))) (h "06ad1lf0zrpdsrp4xax29ppm8j82cw0pwzx5j520bpxcjlv93bv3")))

(define-public crate-debil-0.3.2 (c (n "debil") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1wcgq07x1c59331vl1kxg1jpghsn18k17dy8lp2jns3a66q3i5rw")))

(define-public crate-debil-0.3.3 (c (n "debil") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.1") (d #t) (k 0)))) (h "1qr0hblzl3n13fz82mqpjdcqaqrynhrl0i3sysnjnzbb07mfkigr")))

(define-public crate-debil-0.4.0 (c (n "debil") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.1") (d #t) (k 0)))) (h "11xzfy4qsx691plwmnrnmhsbr5j8m2fivc7havvg4kfzhhfzsipm")))

(define-public crate-debil-0.4.1 (c (n "debil") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.1") (d #t) (k 0)))) (h "0m6wlbyz7w9alb4xhraxvbvfcmx57hc8hjxjvf766bs61pqnlw5k")))

(define-public crate-debil-0.4.2 (c (n "debil") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.1") (d #t) (k 0)))) (h "0rwwqqm22982rkl4lwpapyq5gb9qkhzhffddsgk86ing4w35zzdw")))

(define-public crate-debil-0.5.0 (c (n "debil") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.49") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "debil-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "mysql_async") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1i7xmdbhkjf1liwndpiy0jh3jk24cjbr84pj81cczfyns8f5rmgy") (f (quote (("sqlite" "rusqlite") ("mysql" "mysql_async" "futures"))))))

