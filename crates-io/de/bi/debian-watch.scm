(define-module (crates-io de bi debian-watch) #:use-module (crates-io))

(define-public crate-debian-watch-0.1.0 (c (n "debian-watch") (v "0.1.0") (h "0wyl68g53q5zsqcpnm03pw1kcxsi7d2dmbp7997rzldi3fkqm44w")))

(define-public crate-debian-watch-0.2.0 (c (n "debian-watch") (v "0.2.0") (d (list (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "1nrsd73h698sr5g5rh6wn97y3jx82hcdimy7a4nwcc5riaj720jw")))

(define-public crate-debian-watch-0.2.1 (c (n "debian-watch") (v "0.2.1") (d (list (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "0s8yhr04gd6ykhnk68j5k23rayh25198ahc3dkkhsq81vx419x91")))

(define-public crate-debian-watch-0.2.2 (c (n "debian-watch") (v "0.2.2") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0wwagpn23rm8mbv1579m0phyhzvcqa2pbr7aaw5q0sxf3sbppibp")))

(define-public crate-debian-watch-0.2.3 (c (n "debian-watch") (v "0.2.3") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1qp9xs4z0m48aw1ycadcnqi3sqd2khs459lgxlpww993ljvlb4m0")))

(define-public crate-debian-watch-0.2.4 (c (n "debian-watch") (v "0.2.4") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "021d4zbbhf61m98n6pyfyzcgfsm9w975adq1smfdfj5kavg66z2j")))

(define-public crate-debian-watch-0.2.5 (c (n "debian-watch") (v "0.2.5") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0a28lgk1wll8icdyni618z6yq0g9ffn2jcpjdf5wcwzx66vil7kv")))

(define-public crate-debian-watch-0.2.6 (c (n "debian-watch") (v "0.2.6") (d (list (d (n "debversion") (r ">=0.3") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "16kghwmynmwlck3zr9lv4d8gdf8l25yid4z42a91vb6cyd496py2")))

(define-public crate-debian-watch-0.2.7 (c (n "debian-watch") (v "0.2.7") (d (list (d (n "debversion") (r ">=0.3") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "17v3x6vahcbgmb71kw33flkgn0dafh8h1sxavgavdrrgi1ldhssh")))

