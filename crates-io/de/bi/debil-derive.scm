(define-module (crates-io de bi debil-derive) #:use-module (crates-io))

(define-public crate-debil-derive-0.1.0 (c (n "debil-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x7ad04wmk1i38sx261vfcaj6vj0sji07dbq7lw2c9xhg77pvn4q")))

(define-public crate-debil-derive-0.2.0 (c (n "debil-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nibdis4ywaikklwy4jiisp9iwwv7vapizaayp4vgqkp7fyanly1")))

(define-public crate-debil-derive-0.2.1 (c (n "debil-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bpbcy1spnw5km6gcvz1sv4dx78qzimakhd8slxx3c1jizr9bhmg")))

