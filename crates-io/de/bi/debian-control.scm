(define-module (crates-io de bi debian-control) #:use-module (crates-io))

(define-public crate-debian-control-0.1.0 (c (n "debian-control") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cvh7jpyy2xcjn4iia4n4kz1ac3z69ay00j5w82rw6pjh3xm8p6x")))

(define-public crate-debian-control-0.1.1 (c (n "debian-control") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10rr5ghpfm1xxgc2gy85l8bspyvy4y83mc798rdfa8wnj7gyh959")))

(define-public crate-debian-control-0.1.2 (c (n "debian-control") (v "0.1.2") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "168g1pf7w4wjip8sn2g2ghzqmjmf3z0nlqmmkgsjbxzis8v97v4y") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-debian-control-0.1.3 (c (n "debian-control") (v "0.1.3") (d (list (d (n "deb822-lossless") (r "^0.1.2") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "0i2rmlvwj29sfppkdrn2bzrs6aqsg8l960lnpvmxcr0cfl271xcq")))

(define-public crate-debian-control-0.1.4 (c (n "debian-control") (v "0.1.4") (d (list (d (n "deb822-lossless") (r "^0.1.4") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)))) (h "0k53cfwl9wpkmzrnyk8kvg4jnm49jfjlyl724kz2b4vk749w26rr")))

(define-public crate-debian-control-0.1.6 (c (n "debian-control") (v "0.1.6") (d (list (d (n "deb822-lossless") (r "^0.1.6") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0mfyd7r87vyx0gqfzpqr46l3fisdi8d66r9y9wjnypklddwh1sdw")))

(define-public crate-debian-control-0.1.7 (c (n "debian-control") (v "0.1.7") (d (list (d (n "deb822-lossless") (r "^0.1.7") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "08bk2alla2qyszvkp5dnj3nvfnc9x4sjs63z2aml15hqlrdqpv3c")))

(define-public crate-debian-control-0.1.8 (c (n "debian-control") (v "0.1.8") (d (list (d (n "deb822-lossless") (r "^0.1.7") (d #t) (k 0)) (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "02wq733ldh9pc0f8pm2q9c775mwqz4fmwair0wygx8vkp3fcqa7x")))

