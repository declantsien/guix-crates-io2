(define-module (crates-io de fl deflate64) #:use-module (crates-io))

(define-public crate-deflate64-0.1.0 (c (n "deflate64") (v "0.1.0") (h "1r39x945qs53yk57v765p3nwkq9b4kmhvziijv16vbk41g590fm8")))

(define-public crate-deflate64-0.1.1 (c (n "deflate64") (v "0.1.1") (h "1ajxrlmhanyfbn30f0w56j8j8c6pdy9k3jcx0vdgnd9k4hjjm2cx")))

(define-public crate-deflate64-0.1.2 (c (n "deflate64") (v "0.1.2") (h "13xb6pr4kvz1gignd7z84mabacf68l1px9bdbbd3skah57axc9sw")))

(define-public crate-deflate64-0.1.3 (c (n "deflate64") (v "0.1.3") (h "16wr0rda65ilhb95gzdnnybljmla2skn1bjwl6wv5gzhqm9h9a4w")))

(define-public crate-deflate64-0.1.4 (c (n "deflate64") (v "0.1.4") (h "0in6g0svkp9jd0f6g9ncchnv48l8z7f276sbkjxfi310q4v24wcl")))

(define-public crate-deflate64-0.1.5 (c (n "deflate64") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "0q2g25kr1anrkgvm2qazm0z8jcy1m8h064sppdrdx0m54ns5pp1h")))

(define-public crate-deflate64-0.1.6 (c (n "deflate64") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "1aagh5mmyr8p08if33hizqwiq2as90v9smla89nydq6pivsfy766")))

(define-public crate-deflate64-0.1.7 (c (n "deflate64") (v "0.1.7") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "14437jawq2yb9jcdrzkvn6yjnl9nx31hda5nxzsvczkl37gc2xlm")))

(define-public crate-deflate64-0.1.8 (c (n "deflate64") (v "0.1.8") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "0391p9mx076hm77x2wvmb0qlpnc15s443yzkvin0pgkncg4fdb43")))

