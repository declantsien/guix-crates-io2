(define-module (crates-io de fl deflect) #:use-module (crates-io))

(define-public crate-deflect-0.0.0 (c (n "deflect") (v "0.0.0") (h "1ic6acqnkpn7v7v3vr2lbg6m8ph5aj69vhk14qnh1xb1vpqmfp4x")))

(define-public crate-deflect-0.1.0 (c (n "deflect") (v "0.1.0") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "procmaps") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0m8v3wqzw6b1q21db73v3jinbvk5cq552dia7kfxsa9g6gqizzs6") (r "1.66.0")))

