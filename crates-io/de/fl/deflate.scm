(define-module (crates-io de fl deflate) #:use-module (crates-io))

(define-public crate-deflate-0.1.0 (c (n "deflate") (v "0.1.0") (d (list (d (n "adler32") (r "^0.2.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "0xy24107fq9hf8vrk25g61pk6yp5ym94kg5xy3bzs3qr5ng60iji") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.2.0 (c (n "deflate") (v "0.2.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "0rcigcgj89qrswyhsr274knkchfrchkw81wnfgxxfyd022cz9qrz") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.3.0 (c (n "deflate") (v "0.3.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "19f8752jlvjl1kz8yvg90i9gl32fwz0rlawchfd8avh0ika4n439") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.4.0 (c (n "deflate") (v "0.4.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "1g6q4j5zsxhmqzrwbqbq5sj5d33d5h75p5a674zmysp5s41gcf3j") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.5.0 (c (n "deflate") (v "0.5.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "1nsbgaas0sa5aadqkj8yr9pgl5jd6lzqrkxrwmi3ymkhydm4shi9") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.5.1 (c (n "deflate") (v "0.5.1") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "17fncfwmg00s1mqmnybyi1m6nzvnwr5jvnrisbgbkg29g7wshjr8") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-deflate-0.6.0 (c (n "deflate") (v "0.6.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "1npmi5wgmnr2ldzc6by0zzmzcv0hx4aj5pakc2p9kf9gwym66jx5") (y #t)))

(define-public crate-deflate-0.7.0 (c (n "deflate") (v "0.7.0") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "07z52rm7607hfvs7a47irnqcz3x08s3jd21b295wlqif9i0zm6pq") (y #t)))

(define-public crate-deflate-0.7.1 (c (n "deflate") (v "0.7.1") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "1jgaspx2cxq1hshysij2qf0yv0rbw9i4hy1crdcw5i001kq3py4z")))

(define-public crate-deflate-0.7.2 (c (n "deflate") (v "0.7.2") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "0kfsqzkaj2myky0cxazqkbh8yyqm3vld3n1d84438yq13vd3pcpa")))

(define-public crate-deflate-0.7.3 (c (n "deflate") (v "0.7.3") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "0h3llsqzgz63rbdz23crnabn1hinc38b718f4hsxg1inrr501nd7")))

(define-public crate-deflate-0.7.4 (c (n "deflate") (v "0.7.4") (d (list (d (n "adler32") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "107skf5hharcm766p620xl5yir874ijlnrb9l6wkl64f7bgg7i94")))

(define-public crate-deflate-0.7.5 (c (n "deflate") (v "0.7.5") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 2)))) (h "19vpxc1c5a5yclghlmrq9sbqd01k198h512bd2bgqnkp9fpjmc7b")))

(define-public crate-deflate-0.7.6 (c (n "deflate") (v "0.7.6") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.18") (d #t) (k 2)))) (h "0bwf4n0bgizv1125jyns1vwmzwg6gm7vx25jvg2brwrndd36vd2l")))

(define-public crate-deflate-0.7.7 (c (n "deflate") (v "0.7.7") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.18") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0aj79iipbwny5p8cc62xri33wjqhcvk9r4mjnry84dwwl87b14wz") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.8 (c (n "deflate") (v "0.7.8") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.18") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17nr8dvz8g7k76593bcmm84as2b26ly43dwv57530ksy6yfgr87p") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.9 (c (n "deflate") (v "0.7.9") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ywiy4xracn4fjfljkc77yf1f78jxi98i55ajj0hc09ziybjahj6") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.10 (c (n "deflate") (v "0.7.10") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10q2k3wpjkl8dc6aqg6hisr72myhmcl7hk30jznbg4ml4d54ffcg") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.11 (c (n "deflate") (v "0.7.11") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ya1lapi98wfspwqsn49ssxqhk6i033wkdasiaapdc6ry836vwk4") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.12 (c (n "deflate") (v "0.7.12") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1gf5z4zjxvc5zfwp4wmx9a28jb1y3gpvlr033qwpkvyxyl1xgzn2") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.13 (c (n "deflate") (v "0.7.13") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1p90b0a65ysik7bmz134lsf50gq2rvm63w9c8js63dkdvfdm331b") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.14 (c (n "deflate") (v "0.7.14") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ad376hk5am8af15ihnz5sw6naxnpj5l4zz896fh6hcffbql8hz3") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.15 (c (n "deflate") (v "0.7.15") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "09q4lqpv3mwlqm67ykavx0s37qrda2jbi71gf2r9c9fpgjw56n6x") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.16 (c (n "deflate") (v "0.7.16") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0avra908abxw9a4gb2jy27q90cxamm1vair1sfpyi8az6visgcn4") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.17 (c (n "deflate") (v "0.7.17") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cfzaafiw0k5ax1x8ia41ihmps8llzcyzlqimdkrfwdbm9cxmpad") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.18 (c (n "deflate") (v "0.7.18") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "gzip-header") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0cbv8agmzzfndxdyfcfaqfl0y3k3ig7rf71s1absj08rk06i5j1j") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.19 (c (n "deflate") (v "0.7.19") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (d #t) (k 2)) (d (n "gzip-header") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11pcam84siqfhika4g87d8wc4mr8z2ljlrkqbjdl33bfw4kbnsla") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.7.20 (c (n "deflate") (v "0.7.20") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1d7d9fpmgjnznrksmd3vlv3dyw01wsrm11ifil6ag22871xnlyvh") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.0 (c (n "deflate") (v "0.8.0") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.2") (d #t) (k 2)))) (h "13wh22symwllysjf8xrlyxh9r3z9h0hhay93f0kf1c7qv0i6ws4m") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.1 (c (n "deflate") (v "0.8.1") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.2") (d #t) (k 2)))) (h "0yk1vadra1r5ghd9nn5c820i5mmj983cddwx0x5qp6f4lnsx7lnl") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.2 (c (n "deflate") (v "0.8.2") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.2") (d #t) (k 2)))) (h "1lkqy0yi415i6fiydqf43224bzlwah8j6vxm3c53ds3n6i5f3wzm") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.3 (c (n "deflate") (v "0.8.3") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "1w48ld2284lc6iaw7vnksj8qdmfk81mvg5vl1arh6fd38bggc3h5") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.4 (c (n "deflate") (v "0.8.4") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "1mqvzg1xc9dxrmg4xq53swjp01f4jaq5bvj7z6km5v9z4yid5rg7") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.8.5 (c (n "deflate") (v "0.8.5") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "1s47787qplg37cy30m0y6s9av5yxw70hg2kjvnvpilcn38gdqdjn") (f (quote (("gzip" "gzip-header") ("benchmarks")))) (y #t)))

(define-public crate-deflate-0.8.6 (c (n "deflate") (v "0.8.6") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "0x6iqlayg129w63999kz97m279m0jj4x4sm6gkqlvmp73y70yxvk") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.9.0 (c (n "deflate") (v "0.9.0") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "0rfrd3ackcf6iygyrl2i4xbjas138h4jc7pfykywp6jxcy38b6xg") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-0.9.1 (c (n "deflate") (v "0.9.1") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "gzip-header") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3.6") (d #t) (k 2)))) (h "0w0ww0hrq4bjnihxgbnrri4lj5c8yzg31fyzx36fd9pvvw2vz5az") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

(define-public crate-deflate-1.0.0 (c (n "deflate") (v "1.0.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "gzip-header") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.0") (d #t) (k 2)))) (h "0bs319wa9wl7pn9j6jrrxg1gaqbak581rkx210cbix0qyljpwvy8") (f (quote (("gzip" "gzip-header") ("benchmarks"))))))

