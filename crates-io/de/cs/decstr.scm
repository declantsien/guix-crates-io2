(define-module (crates-io de cs decstr) #:use-module (crates-io))

(define-public crate-decstr-0.1.0 (c (n "decstr") (v "0.1.0") (d (list (d (n "dec") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "069an8h1w6hsf9p3l3mw0rsn8pj0gvnr0sil62lchmg545302h5d") (f (quote (("std")))) (s 2) (e (quote (("arbitrary-precision" "std" "dep:num-bigint" "dep:num-traits"))))))

(define-public crate-decstr-0.1.1 (c (n "decstr") (v "0.1.1") (d (list (d (n "dec") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "059a2xxavyqhafvw8hpcs0virz8cxz8g6wrf4sgw45hmy13z28bh") (f (quote (("std")))) (s 2) (e (quote (("arbitrary-precision" "std" "dep:num-bigint" "dep:num-traits"))))))

(define-public crate-decstr-0.1.2 (c (n "decstr") (v "0.1.2") (d (list (d (n "dec") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0siazd57qds3h89vci7h0fbmmdhg2ik8zhypzvx848dc3r9ymcvh") (f (quote (("std")))) (s 2) (e (quote (("arbitrary-precision" "std" "dep:num-bigint" "dep:num-traits"))))))

(define-public crate-decstr-0.1.3 (c (n "decstr") (v "0.1.3") (d (list (d (n "dec") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "1zl0618pw9lqsla1vj3zwxjzgmqwx882f977r8c6yf63az5ric92") (f (quote (("std")))) (s 2) (e (quote (("arbitrary-precision" "std" "dep:num-bigint" "dep:num-traits"))))))

(define-public crate-decstr-0.2.0 (c (n "decstr") (v "0.2.0") (d (list (d (n "dec") (r "^0.4") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "19j12ls0vair7wkajc7by4qc3kn08f94dgy3am5bilnayk0rpcsv") (f (quote (("std")))) (s 2) (e (quote (("arbitrary-precision" "std" "dep:num-bigint" "dep:num-traits"))))))

