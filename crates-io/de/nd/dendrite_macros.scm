(define-module (crates-io de nd dendrite_macros) #:use-module (crates-io))

(define-public crate-dendrite_macros-0.1.0 (c (n "dendrite_macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bqini7l2d9p1k3pqka527w8m3f037yh2vkqjqjk12a4h0hlxs1s")))

(define-public crate-dendrite_macros-0.2.0 (c (n "dendrite_macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1izrj56cmyinxw6wxd5wh4sp664d364pdfg3zvzqig4mjvdvwwr4")))

(define-public crate-dendrite_macros-0.3.0 (c (n "dendrite_macros") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0avsdahz11gcvf23l855879978xj141h143k1z8jnk5s6amdwikd")))

(define-public crate-dendrite_macros-0.4.0 (c (n "dendrite_macros") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19s3n9lhb6njvfkkxsxxn5lv5wj0pbxv9yl21141gb0d9v6128qz")))

(define-public crate-dendrite_macros-0.5.0 (c (n "dendrite_macros") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0946nvcmwjvhkrdb30yvak005p95l5ppza7hddfrvlfjjxrfcc6d")))

