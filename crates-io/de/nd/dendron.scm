(define-module (crates-io de nd dendron) #:use-module (crates-io))

(define-public crate-dendron-0.1.0 (c (n "dendron") (v "0.1.0") (h "1xr68v1jrvk42vsdw2sk2k2pgs2xqxf42ld6pf27ah786fgsrz4p") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-dendron-0.1.1 (c (n "dendron") (v "0.1.1") (h "15279s04mq5rx4h7cgra239s62r5l1iz6cvkd1816ynzs2h7sza7") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-dendron-0.1.2 (c (n "dendron") (v "0.1.2") (h "1fpb5k51g46kh170510gw7qkqg7wyybj2hvzqz2wmwnxgnrlfxf9") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-dendron-0.1.3 (c (n "dendron") (v "0.1.3") (h "0z0q1v4fl32d04n4g1bgd1skvjnmzd4ryw3pdihrb9y4d2z2521l") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-dendron-0.1.4 (c (n "dendron") (v "0.1.4") (h "1mc342y3fya0gj37izwc46m0wabd8c1y8cy4mf2g5ncyyf7ydkdf") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-dendron-0.1.5 (c (n "dendron") (v "0.1.5") (h "092k802jzkp5i3lvl0n3zcvs1x9ippx0qp2pnl83xv1bbdp64h99") (f (quote (("std") ("default" "std")))) (r "1.59")))

