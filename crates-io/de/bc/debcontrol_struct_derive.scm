(define-module (crates-io de bc debcontrol_struct_derive) #:use-module (crates-io))

(define-public crate-debcontrol_struct_derive-0.1.0 (c (n "debcontrol_struct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bgvami351nh2ncckwi00jmfw2la8ip2g7l66gb50blzk64nl9pd")))

(define-public crate-debcontrol_struct_derive-0.2.0 (c (n "debcontrol_struct_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sdyq4cakps9xh2k1d90yhzj1pa00c679k4vrn14whjgqdp9mspl")))

(define-public crate-debcontrol_struct_derive-0.3.0 (c (n "debcontrol_struct_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j1wa10gsn8swdm2fjdzbrpyzvh6ybz141vynz15qri9x2is0zhx")))

(define-public crate-debcontrol_struct_derive-0.3.1 (c (n "debcontrol_struct_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1163kciy3qhw6yspmq2y605ny84wmm9c3b41h4r7w5zw56f09w7p")))

