(define-module (crates-io de bc debcontrol) #:use-module (crates-io))

(define-public crate-debcontrol-0.1.0 (c (n "debcontrol") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "json") (r "^0.12.1") (d #t) (k 2)) (d (n "nom") (r "^5.1.0") (f (quote ("alloc"))) (k 0)))) (h "12rc7p620xk0pv9c8nbk5pl2qq66cc828cg1ns2f5nmzqn470pdq") (f (quote (("verbose-errors") ("std" "nom/std") ("default" "std" "verbose-errors"))))))

(define-public crate-debcontrol-0.1.1 (c (n "debcontrol") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "json") (r "^0.12.1") (d #t) (k 2)) (d (n "nom") (r "^5.1.0") (f (quote ("alloc"))) (k 0)))) (h "07587xrlhnldiccajyx2r95q2icn553lm3kcp69whvb2vxhf70n9") (f (quote (("verbose-errors") ("std" "nom/std") ("default" "std" "verbose-errors"))))))

