(define-module (crates-io de bc debcontrol_struct) #:use-module (crates-io))

(define-public crate-debcontrol_struct-0.1.0 (c (n "debcontrol_struct") (v "0.1.0") (d (list (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.1.0") (d #t) (k 2)))) (h "1rbjjxl5ykp5y58p2s39qmmjw8hh6qnvw3mp3brvqpjnka60408f") (f (quote (("derive" "debcontrol_struct_derive") ("default" "derive"))))))

(define-public crate-debcontrol_struct-0.2.0 (c (n "debcontrol_struct") (v "0.2.0") (d (list (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.2.0") (d #t) (k 2)))) (h "1rs93kkjfllmdq3ssasify070gvxa713kcjrxnnn81b25bwhjs9a") (f (quote (("derive" "debcontrol_struct_derive") ("default" "derive"))))))

(define-public crate-debcontrol_struct-0.3.0 (c (n "debcontrol_struct") (v "0.3.0") (d (list (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.3.0") (d #t) (k 2)))) (h "0ifh3vay6ia6ja05y8w3wi3vndxzw3q6c6mlw9mqhvff6r0v3f6j") (f (quote (("derive" "debcontrol_struct_derive") ("default" "derive"))))))

(define-public crate-debcontrol_struct-0.3.1 (c (n "debcontrol_struct") (v "0.3.1") (d (list (d (n "debcontrol") (r "^0.1.1") (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.3.1") (d #t) (k 2)))) (h "04s76270bhfqx5claawik5yrmzn1g486p6sziy9i0dswg9gpczmf") (f (quote (("derive" "debcontrol_struct_derive") ("default" "derive"))))))

