(define-module (crates-io de bc debcontrol-struct-with-oma-decontrol) #:use-module (crates-io))

(define-public crate-debcontrol-struct-with-oma-decontrol-0.3.1 (c (n "debcontrol-struct-with-oma-decontrol") (v "0.3.1") (d (list (d (n "debcontrol") (r "^0.3") (d #t) (k 0) (p "oma-debcontrol")) (d (n "debcontrol_struct_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "debcontrol_struct_derive") (r "^0.3.1") (d #t) (k 2)))) (h "0k2ngd22c4n78razxjxn67ab40i0qjj7dhfdgph78v37rmfac91w") (f (quote (("derive" "debcontrol_struct_derive") ("default" "derive"))))))

