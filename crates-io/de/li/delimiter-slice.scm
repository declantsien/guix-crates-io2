(define-module (crates-io de li delimiter-slice) #:use-module (crates-io))

(define-public crate-delimiter-slice-0.1.0 (c (n "delimiter-slice") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("io"))) (d #t) (k 2)))) (h "0g0lpmvbnrj0hfa95ml98ljy8siq3l043b3200fs15ywjnb0fjdw")))

(define-public crate-delimiter-slice-0.2.0 (c (n "delimiter-slice") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("io"))) (d #t) (k 2)))) (h "10lag42g4in1h11x6ac4m9vacac4nihfx9ywpbkp1cag88wiqgga")))

(define-public crate-delimiter-slice-0.2.1 (c (n "delimiter-slice") (v "0.2.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("io"))) (d #t) (k 2)))) (h "1yj01n9pjj9441wj4bap9yf2a8ck4kajayk5v24n4fazl73fx8pw")))

