(define-module (crates-io de li deli) #:use-module (crates-io))

(define-public crate-deli-0.1.0 (c (n "deli") (v "0.1.0") (d (list (d (n "deli-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "idb") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-wasm-bindgen") (r "^0.4.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (k 2)))) (h "1afmcjsivnxcfb0w7sc9ag214z0x29hrdqb9sd9k5idw2yq011qy")))

