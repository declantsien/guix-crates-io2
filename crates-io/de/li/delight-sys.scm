(define-module (crates-io de li delight-sys) #:use-module (crates-io))

(define-public crate-delight-sys-0.1.0 (c (n "delight-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "delight-build") (r "^0.1") (d #t) (k 1)) (d (n "dlopen2") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1sv58vrra6hzi6ihgi36hq8581508m8cvyfip9il947kc2maj6xd") (f (quote (("link_lib3delight" "delight-build/link_lib3delight") ("download_lib3delight" "delight-build/download_lib3delight") ("default" "dlopen2"))))))

(define-public crate-delight-sys-0.1.1 (c (n "delight-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "delight-build") (r "^0.1") (d #t) (k 1)) (d (n "dlopen2") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "09d5wp6n36mdaw6l71givpwki40in4l6cbgx92l8pf9v125p4m0n") (f (quote (("link_lib3delight" "delight-build/link_lib3delight") ("download_lib3delight" "delight-build/download_lib3delight") ("default" "dlopen2"))))))

(define-public crate-delight-sys-0.1.2 (c (n "delight-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "delight-build") (r "^0.1") (d #t) (k 1)) (d (n "dlopen2") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0jy1m4xp4cwkx7hdzdxj00xns096j3s44s22ymni5nffrgbqvxri") (f (quote (("link_lib3delight" "delight-build/link_lib3delight") ("download_lib3delight" "delight-build/download_lib3delight") ("default" "dlopen2"))))))

