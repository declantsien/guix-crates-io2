(define-module (crates-io de li delimited-protobuf) #:use-module (crates-io))

(define-public crate-delimited-protobuf-0.1.0 (c (n "delimited-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.1.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "1zzar9sz48l7ixnkgrvwv1pny0yx340qria2rx7vn6yqzlrcp382")))

(define-public crate-delimited-protobuf-0.1.1 (c (n "delimited-protobuf") (v "0.1.1") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "04vwbvpj14l9vqsjisgvzsclnf98mhbmxxf1c9a4j0rwwf9ii3z7")))

