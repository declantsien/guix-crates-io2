(define-module (crates-io de li delight) #:use-module (crates-io))

(define-public crate-delight-0.1.0 (c (n "delight") (v "0.1.0") (d (list (d (n "delight-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x2hnxvn0bmhng8xm3cvgvhfcrmk24a4xrdbrviia8kjkigw4z41")))

(define-public crate-delight-0.1.1 (c (n "delight") (v "0.1.1") (d (list (d (n "delight-sys") (r "^0.1") (d #t) (k 0)))) (h "1jfg7p5229hqws0965b5a4xqnn5fhranika1x4fbahjn110r27xz") (f (quote (("link_lib3delight" "delight-sys/link_lib3delight") ("download_lib3delight" "delight-sys/download_lib3delight"))))))

