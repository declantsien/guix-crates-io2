(define-module (crates-io de li delight-build) #:use-module (crates-io))

(define-public crate-delight-build-0.1.0 (c (n "delight-build") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "1vr78mzvw21yl0p6z9vx807vyxckcf64gw8ym168i6rgvq2p794l") (f (quote (("link_lib3delight") ("download_lib3delight"))))))

(define-public crate-delight-build-0.1.1 (c (n "delight-build") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "1fr8hd61habj2x04q6mx8ajja2fqch7idvrw8gp333gnsb2ydl78") (f (quote (("link_lib3delight") ("download_lib3delight"))))))

