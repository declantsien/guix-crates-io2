(define-module (crates-io de li delim) #:use-module (crates-io))

(define-public crate-delim-0.1.0 (c (n "delim") (v "0.1.0") (h "0gq1q5gd2gkwv1zapb85kcx79r2604cjdrwp4cy7w210qf8x04mk")))

(define-public crate-delim-0.1.1 (c (n "delim") (v "0.1.1") (h "16152vf1dk020bks4b86jcpw3f49s9kzx03q8vi3z8r80dg1qsd2")))

