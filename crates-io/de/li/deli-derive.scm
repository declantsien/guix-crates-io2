(define-module (crates-io de li deli-derive) #:use-module (crates-io))

(define-public crate-deli-derive-0.1.0 (c (n "deli-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1afjf3jv46334akb403pq75ax98rjsl786qbcmygd4gsl084jzph")))

