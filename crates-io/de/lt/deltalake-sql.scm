(define-module (crates-io de lt deltalake-sql) #:use-module (crates-io))

(define-public crate-deltalake-sql-0.1.0 (c (n "deltalake-sql") (v "0.1.0") (d (list (d (n "arrow-schema") (r "^50") (d #t) (k 2)) (d (n "datafusion-common") (r "^35") (d #t) (k 0)) (d (n "datafusion-expr") (r "^35") (d #t) (k 0)) (d (n "datafusion-sql") (r "^35") (d #t) (k 0)))) (h "1zgkn23nf8jcp0i3lm3zz5vna20ikn4rcxr181g0hf164hmqd6zl") (r "1.72")))

