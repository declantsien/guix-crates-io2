(define-module (crates-io de lt delta_rs) #:use-module (crates-io))

(define-public crate-delta_rs-0.1.0 (c (n "delta_rs") (v "0.1.0") (h "0vn58k0hm5bpjvsmawy4cqc6cj4lkm32yc24157z4wl79dxwnclx") (y #t)))

(define-public crate-delta_rs-0.1.1 (c (n "delta_rs") (v "0.1.1") (h "12ip0jqfh31d0gjhi2h1x9hqgf222kp71z1df48ghl0dbygkgbv9") (y #t)))

