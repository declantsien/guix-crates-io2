(define-module (crates-io de lt delta_e) #:use-module (crates-io))

(define-public crate-delta_e-0.1.0 (c (n "delta_e") (v "0.1.0") (d (list (d (n "lab") (r "^0.4.2") (d #t) (k 0)))) (h "0yzip5zywwbwfndwvxdbaq380b1and29s8j23wgp0392smwb386b")))

(define-public crate-delta_e-0.2.0 (c (n "delta_e") (v "0.2.0") (d (list (d (n "lab") (r "^0.4.2") (d #t) (k 0)))) (h "01mi24v3fs7mk8nh2pbn62gjm118racmhgxn3fskbcs773hc5qr7")))

(define-public crate-delta_e-0.2.1 (c (n "delta_e") (v "0.2.1") (d (list (d (n "lab") (r "^0.7") (d #t) (k 0)))) (h "18rxibmi27ark8vj367qm2iqmv5x293l8fm9ang4y2sv3l251sf5")))

