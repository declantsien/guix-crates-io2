(define-module (crates-io de lt deltalake-catalog-glue) #:use-module (crates-io))

(define-public crate-deltalake-catalog-glue-0.1.0 (c (n "deltalake-catalog-glue") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "aws-config") (r "^1") (d #t) (k 0)) (d (n "aws-sdk-glue") (r "^1") (d #t) (k 0)) (d (n "deltalake-core") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wgfc6y3bfyl60q1h3cvp99v2ryr63vpd55vdwsml4jdvvvaqs4d") (r "1.72")))

