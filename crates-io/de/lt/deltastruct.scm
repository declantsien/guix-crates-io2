(define-module (crates-io de lt deltastruct) #:use-module (crates-io))

(define-public crate-deltastruct-0.1.0 (c (n "deltastruct") (v "0.1.0") (d (list (d (n "deltastruct_proc") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "163kpvmimjzgqfpw39vlghdkhgj5in74n9pmfsxvnf2bw753gvcp")))

(define-public crate-deltastruct-0.1.6 (c (n "deltastruct") (v "0.1.6") (d (list (d (n "deltastruct_proc") (r "^0.1.6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zrs4p9gb2ywla50j72wy91935nr5nhb157g9id9lxrm2iisgpqc")))

(define-public crate-deltastruct-0.1.7 (c (n "deltastruct") (v "0.1.7") (d (list (d (n "deltastruct_proc") (r "^0.1.6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dw3jmx3ph1nja52f9vcdifzvk872lprii813lx51qh3kghjymc2")))

(define-public crate-deltastruct-0.1.8 (c (n "deltastruct") (v "0.1.8") (d (list (d (n "deltastruct_proc") (r "^0.1.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k8xgv7qz7f8rw7qz61jnb7fjjyl3gr68iz1g57rxfqw588q55yi")))

