(define-module (crates-io de lt deltachat-provider-database) #:use-module (crates-io))

(define-public crate-deltachat-provider-database-0.1.0 (c (n "deltachat-provider-database") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "15h7jrh83m556jz4lafb7m1fnrjsvysp5s4zmlw2c0xpn282zs16") (y #t)))

(define-public crate-deltachat-provider-database-0.2.0 (c (n "deltachat-provider-database") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "1xrd927aqrinbx7vwcryzmb555mv3r45imcsddncy9ywlfncbpkn") (y #t)))

(define-public crate-deltachat-provider-database-0.2.1 (c (n "deltachat-provider-database") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "0fxr6k3ccas18a7cfcciy2hwgld9ml84ip6ckyc7mp4z1l3blkc1") (y #t)))

(define-public crate-deltachat-provider-database-0.3.0 (c (n "deltachat-provider-database") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "0lic4hhbsa04gz7nx80fvbq7h55nlhbvvmq1r1jhbjbvia4zn67w") (y #t)))

