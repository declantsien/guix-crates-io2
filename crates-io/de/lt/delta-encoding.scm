(define-module (crates-io de lt delta-encoding) #:use-module (crates-io))

(define-public crate-delta-encoding-0.1.0 (c (n "delta-encoding") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12zvypx538abkwv6327z8nr84mz3fk6f3lrqqbw83nzryj5cv8ay")))

(define-public crate-delta-encoding-0.2.0 (c (n "delta-encoding") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13lwd44asznzkidcd3p7frpsac9iz9c1f7ba9inbkfd7d4c322ny")))

(define-public crate-delta-encoding-0.3.0 (c (n "delta-encoding") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g7xxs9qij8y4vnmkz0h8c517pdzqpriv7xjaiiifc4v7xgiij9i")))

(define-public crate-delta-encoding-0.4.0 (c (n "delta-encoding") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q5wixvipvnhnksmla7xcxbxxnbgmg29sh33jlabkmxkxsji71ag")))

