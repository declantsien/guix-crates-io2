(define-module (crates-io de lt deltachat_message_parser) #:use-module (crates-io))

(define-public crate-deltachat_message_parser-0.3.0 (c (n "deltachat_message_parser") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "14hz1j2fh3v05yn38ir6wi28fw8385by80zq79jv19yyqvrakf4l")))

(define-public crate-deltachat_message_parser-0.4.0 (c (n "deltachat_message_parser") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "1cfzyvxjcw5nv2gwby1k9gjz6vd64vslahczxb44vyw3i5nkvr23")))

(define-public crate-deltachat_message_parser-0.5.0 (c (n "deltachat_message_parser") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "007r3g3f3akrz10nh160yaydh088w27d9hasq7qm655nhl6afh2k")))

(define-public crate-deltachat_message_parser-0.6.0 (c (n "deltachat_message_parser") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "09bg627ynwjhnlw09ncpkws2vgihxag2h9i6k9anc4bq78f753g5")))

(define-public crate-deltachat_message_parser-0.7.0 (c (n "deltachat_message_parser") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "1d19cqrdnyljnv3mgsvjywy9iwpwab3cdg2qsg5kr3p7f6cp4scl")))

(define-public crate-deltachat_message_parser-0.8.0 (c (n "deltachat_message_parser") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "1k8bnxkjnrnz7gyyhdc2ldrm5x613n12s6rmfdvlal9gngs80vw2")))

(define-public crate-deltachat_message_parser-0.9.0 (c (n "deltachat_message_parser") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "0iwppm1kpz553cws4iz85vqyw25lh3kbj8yvfhl9ydvmkyxa58ah")))

(define-public crate-deltachat_message_parser-0.10.0 (c (n "deltachat_message_parser") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "unic-idna-punycode") (r "^0.9.0") (d #t) (k 0)))) (h "1l4km1d5qwcykyg8dqwx333fjv7d8zri0f1zdy1css5p1k1sampq")))

