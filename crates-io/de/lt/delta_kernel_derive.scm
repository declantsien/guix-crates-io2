(define-module (crates-io de lt delta_kernel_derive) #:use-module (crates-io))

(define-public crate-delta_kernel_derive-0.0.1 (c (n "delta_kernel_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zg0vmhm2b4g4h0jlfqp9myi7bd5xqzng651ismqv3wg03c7yr29")))

