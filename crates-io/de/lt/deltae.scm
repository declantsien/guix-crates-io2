(define-module (crates-io de lt deltae) #:use-module (crates-io))

(define-public crate-deltae-0.1.2 (c (n "deltae") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0cp0w2ks03p45w9wdjq914jafhf4byzzl9l40k13q0hw7zxs0s76")))

(define-public crate-deltae-0.1.3 (c (n "deltae") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0nbv8wl6d4qfkxcm7y13348y2c6wb4554pkpxd2fwmgv89c9mmz1")))

(define-public crate-deltae-0.1.4 (c (n "deltae") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0g44nml6pnip5jkrmacp00ap5s6svqb3lafmzpc05s4as8pakaih")))

(define-public crate-deltae-0.1.5 (c (n "deltae") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0fpns25sfy2b136if20i6n6yqbhn3kkaicvnbjckkqywxpi4yf1f")))

(define-public crate-deltae-0.2.0 (c (n "deltae") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0wl0c9w8apm657wnmy68nnhjiwgmj8am5jwm8yfpvgwvdq7q3h2j")))

(define-public crate-deltae-0.2.1 (c (n "deltae") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "1najmzsqr5agdvh1lzdm2a0253mpr1xwrhvvkfx4h7dkzjlnnhp1")))

(define-public crate-deltae-0.3.0 (c (n "deltae") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0nmd9v0cfxz3rj9n7wv91pk3s7p2mc5w8l7a773zqqpclj8ws4p4")))

(define-public crate-deltae-0.3.1 (c (n "deltae") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)))) (h "0lws5fg4gjvm8g60lk8hwy6d34ivsdlaqff8asxbk253q3i13wry")))

(define-public crate-deltae-0.3.2 (c (n "deltae") (v "0.3.2") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 2)))) (h "1d3hw9hpvicl9x0x34jr2ybjk5g5ym1lhbyz6zj31110gq8zaaap")))

