(define-module (crates-io de lt deltav_calc) #:use-module (crates-io))

(define-public crate-deltav_calc-0.1.0 (c (n "deltav_calc") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0865llqh6j1y67bbp9kkdcy2x7qk1660wj3x6qggh3p0xbfy0gjw")))

