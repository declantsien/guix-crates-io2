(define-module (crates-io de lt delta_patch) #:use-module (crates-io))

(define-public crate-delta_patch-0.1.0 (c (n "delta_patch") (v "0.1.0") (d (list (d (n "blake2") (r "^0.7.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "07s9xvkqlvgm9ib14h75fhyprkbdqs73l8ixc46d0f38rka8fi63")))

