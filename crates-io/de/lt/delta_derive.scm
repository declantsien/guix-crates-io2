(define-module (crates-io de lt delta_derive) #:use-module (crates-io))

(define-public crate-delta_derive-0.1.0 (c (n "delta_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10hvyf0z48dps1dqwb7is2vrkdw5rq0frqvn6nqia6fqi51nl9gd") (y #t)))

