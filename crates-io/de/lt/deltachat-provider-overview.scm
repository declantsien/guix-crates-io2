(define-module (crates-io de lt deltachat-provider-overview) #:use-module (crates-io))

(define-public crate-deltachat-provider-overview-0.1.0 (c (n "deltachat-provider-overview") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "1qs07wpcbpiqxcbkdk1vrkdplcgg9r7davmqin3mky5h4qv7pvsv") (y #t)))

(define-public crate-deltachat-provider-overview-0.1.1 (c (n "deltachat-provider-overview") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 1)))) (h "0d8yc7jhvq86dpyw22cbd5k3mh1grgr0jsvzrv6wqnvpqxy7cind") (y #t)))

