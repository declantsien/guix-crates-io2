(define-module (crates-io de lt delta-struct-macros) #:use-module (crates-io))

(define-public crate-delta-struct-macros-0.1.0 (c (n "delta-struct-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmv34kdwpfangmq1xakfpq834j19kb7fbx1brbxyvsiaxyj7n2q")))

