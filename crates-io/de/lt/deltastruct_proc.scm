(define-module (crates-io de lt deltastruct_proc) #:use-module (crates-io))

(define-public crate-deltastruct_proc-0.1.0 (c (n "deltastruct_proc") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "07sc8z31jl6xlls87z19qpcm67q9xh4vhwj455zvcjxadg2pa952")))

(define-public crate-deltastruct_proc-0.1.1 (c (n "deltastruct_proc") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1c4zszffsimgmgg586zyb5hzwgn0chmhf8iif61bgq93li9knlk0")))

(define-public crate-deltastruct_proc-0.1.2 (c (n "deltastruct_proc") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18r7ki94vx0xh4px58n7fs5crvriw4v588001gwxgda2dr231h7r")))

(define-public crate-deltastruct_proc-0.1.3 (c (n "deltastruct_proc") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0ar1gazhgpy9h6y1nfpqsjb4wzxyysxhyzvfdg6yz2bgp5x27m3b")))

(define-public crate-deltastruct_proc-0.1.4 (c (n "deltastruct_proc") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "13fiwsbwkildyj2wikcvspkznd0ccjimi6rdc4xglp20g2h4355i")))

(define-public crate-deltastruct_proc-0.1.5 (c (n "deltastruct_proc") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1rpc875kfjb2xcy4ycjf9hjfwmbjhyz01hszprlxhciilghr36yr")))

(define-public crate-deltastruct_proc-0.1.6 (c (n "deltastruct_proc") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0qs6d930bg9pxcvl6r0x18m9jznb3c6dkcqan64w6nkpsmkaf9h8")))

(define-public crate-deltastruct_proc-0.1.8 (c (n "deltastruct_proc") (v "0.1.8") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "025p1vz4l5kga9x83smp5rdp3097ryikymfsgx4kbis1z2s3skn0")))

