(define-module (crates-io de lt delta) #:use-module (crates-io))

(define-public crate-delta-0.1.0 (c (n "delta") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ssk6xnh2zh9wg3hr4yl2iswpdm1wrp02f2lbxh7i8wqm4dcgxl5")))

(define-public crate-delta-0.1.1 (c (n "delta") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "134y34z8648q1k6zvqjxn7n276k9zlzarhk37niap96i6hm962y5")))

(define-public crate-delta-0.2.0 (c (n "delta") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "19kk7257mghv45zd2h4lgpwjh65gj9kw18w94w02hfs9swijgmdw")))

(define-public crate-delta-0.2.1 (c (n "delta") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06qp0v1nmyzfam37yrn6pvad5g78isyxl4y4qpdb3gj85z9xn18d")))

