(define-module (crates-io de ja dejavu-engine) #:use-module (crates-io))

(define-public crate-dejavu-engine-0.1.0 (c (n "dejavu-engine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02nbs0r6si1n96bcamgi7mqn6a7x8m897sq5bb2i2drqq0zdinfb") (f (quote (("json" "serde_json" "serde_json/preserve_order") ("default"))))))

