(define-module (crates-io de ja dejavu-parser) #:use-module (crates-io))

(define-public crate-dejavu-parser-0.0.0 (c (n "dejavu-parser") (v "0.0.0") (d (list (d (n "peginator") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.4.0") (d #t) (k 1)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.5") (d #t) (k 0)))) (h "0nqycd8vjvbjm09bm83qwv19z3jyqgjnv30fjd2s3159yw8fy5d6") (f (quote (("default"))))))

(define-public crate-dejavu-parser-0.1.0 (c (n "dejavu-parser") (v "0.1.0") (d (list (d (n "peginator") (r "^0.6.0") (d #t) (k 0)) (d (n "peginator_codegen") (r "^0.6.0") (d #t) (k 1)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.5") (d #t) (k 0)))) (h "1z27gkcg0bann082s504k4a61hdc0rynx5cvz8rsi5nzhasc5vb9") (f (quote (("default"))))))

(define-public crate-dejavu-parser-0.1.1 (c (n "dejavu-parser") (v "0.1.1") (d (list (d (n "yggdrasil-rt") (r "^0.0.11") (d #t) (k 0)) (d (n "yggdrasil-shared") (r "0.2.*") (d #t) (k 2)))) (h "0y3062jf9ls6glm1izqb7h2ghkd23wklm4a41jzmbnfi76hgdp93") (f (quote (("default"))))))

