(define-module (crates-io de ja dejavu-runtime) #:use-module (crates-io))

(define-public crate-dejavu-runtime-0.1.0 (c (n "dejavu-runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "1xjwi78zb9vcrj5c8yiw3pyz7v81ygkd4qv9lzamyrid4glrr675") (f (quote (("json" "serde_json" "serde_json/preserve_order") ("default"))))))

(define-public crate-dejavu-runtime-0.1.1 (c (n "dejavu-runtime") (v "0.1.1") (h "00nv4b1rqa6lpsmh7r9qxkm6sdwfl6dhs73xffgq9612vw6h5qww") (f (quote (("default"))))))

