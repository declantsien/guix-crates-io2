(define-module (crates-io de po depot) #:use-module (crates-io))

(define-public crate-depot-0.1.0 (c (n "depot") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "11735nizvfyrgka0nm74yaszcshw2568gdq87iwcyxwa12ig2gfv")))

(define-public crate-depot-0.2.0 (c (n "depot") (v "0.2.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01m116yz4w9gawmcwqzywri97n821spx5yvd59a4wlzj5bzcyg4x")))

