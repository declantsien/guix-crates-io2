(define-module (crates-io de po depon) #:use-module (crates-io))

(define-public crate-depon-0.1.0 (c (n "depon") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)))) (h "1wmgqc9a5gxaxgrpcl24rh9mfi7gch945l5f3451xj33ap5xq8hr")))

(define-public crate-depon-0.1.1 (c (n "depon") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)))) (h "1y27n8assc0920kc31dxyqgc2fnd4ailrhygydg4nf85iqdyn70w")))

