(define-module (crates-io de c- dec-number) #:use-module (crates-io))

(define-public crate-dec-number-0.0.1 (c (n "dec-number") (v "0.0.1") (h "0ymq4yr0hck1zvmmhfmhjdvikcs577hqja9n5f73zv19k4nv6g70")))

(define-public crate-dec-number-0.0.2 (c (n "dec-number") (v "0.0.2") (h "0f2j5293xpn96kgzpi7fz1ipliacmvpvxg8jh6ybj8laiipl3rw0")))

(define-public crate-dec-number-0.0.4 (c (n "dec-number") (v "0.0.4") (h "1cjabzg4xn0y3nghj7krdzd17flfnc9kscfxsbz7smlg1bknz8ws")))

