(define-module (crates-io de qu deque_deck) #:use-module (crates-io))

(define-public crate-deque_deck-0.1.0 (c (n "deque_deck") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0l3z1kpciwmrbicv7a1nnmhf1xq4084j3z76p9dyh27kskp0vn43")))

(define-public crate-deque_deck-0.1.1 (c (n "deque_deck") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0zpj9wmw2n9vygl8k3hdzw75iv6c27iwm94a86zly98lv4ashbdq")))

(define-public crate-deque_deck-0.2.1 (c (n "deque_deck") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "0gv2icv60y85fg8jqnpr6q77azs242dk1jl0z4l03qpp88r06h18")))

