(define-module (crates-io de qu dequemap) #:use-module (crates-io))

(define-public crate-dequemap-0.1.0 (c (n "dequemap") (v "0.1.0") (h "04jfcaf19zda8ll5afimms67nx61g4xlh0r7pj29300gpw5cj3h8") (f (quote (("std") ("default" "std")))) (r "1.47")))

(define-public crate-dequemap-0.1.1 (c (n "dequemap") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "17by40nf0mah928v8n52czc0rnr4lgqg599kh9nmx8xgswcy6wb4") (f (quote (("std") ("default" "std")))) (r "1.47")))

(define-public crate-dequemap-0.1.2 (c (n "dequemap") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0bzjd1phf90vzc7gdjs2q87lc25i68mmdldqgksl8y16hqf0zli2") (f (quote (("std") ("hashmap" "hashbrown") ("default" "std" "btreemap") ("btreemap")))) (r "1.47")))

(define-public crate-dequemap-0.1.3 (c (n "dequemap") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "07mhxx67yi9x07rxha46b6ds26vl9bapjzp8qnx4sxhgsjv62ajp") (f (quote (("std") ("hashmap" "hashbrown") ("default" "std" "btreemap") ("btreemap")))) (r "1.47")))

(define-public crate-dequemap-0.2.0 (c (n "dequemap") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0vy1qdz39z64fgqjxfr54izi4vnkhg01jxi0ayg929839piqjdad") (f (quote (("std") ("hashmap" "hashbrown") ("default" "std" "btreemap") ("btreemap")))) (r "1.47")))

