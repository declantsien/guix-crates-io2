(define-module (crates-io de qu deque-loader-derive) #:use-module (crates-io))

(define-public crate-deque-loader-derive-0.1.0-beta.3 (c (n "deque-loader-derive") (v "0.1.0-beta.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1xghm30sapx2ramvj3c4jvbng7zpwivz7j8d978i55zhk0rv5q3c") (y #t)))

