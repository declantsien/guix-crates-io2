(define-module (crates-io de qu deque) #:use-module (crates-io))

(define-public crate-deque-0.1.0 (c (n "deque") (v "0.1.0") (h "13pr2s2ppwhkdhmnrc3y3jbskca67crxqw794y4f2ip1m38hr4z7")))

(define-public crate-deque-0.1.1 (c (n "deque") (v "0.1.1") (h "0h90bl36d53v6pn4aqrn62wh5mi7bsys2rrzmqdihv8wi33i2rhs")))

(define-public crate-deque-0.1.2 (c (n "deque") (v "0.1.2") (h "0dhpr5naa3xy3d0cikhhs1jaj12kyrxc46iic6crbc09qh412nav")))

(define-public crate-deque-0.1.3 (c (n "deque") (v "0.1.3") (h "11fajkw5zwz452jr61ksfyhazz503h43rd4dhhap890ww5ynh1c9")))

(define-public crate-deque-0.1.4 (c (n "deque") (v "0.1.4") (h "01a8sgpd8201yp0mr3ka4av7c4m9sf7dhvksfcgw3jvqknp3i5h5")))

(define-public crate-deque-0.1.5 (c (n "deque") (v "0.1.5") (h "10vq3nkplyzamaxb6szyjx3av244nic5csv4s30z0wnc4xw5304c")))

(define-public crate-deque-0.1.6 (c (n "deque") (v "0.1.6") (h "18ic9jh4m90k8cgfcg7s6kbvm2syhm84qbwra0jnwmj4s89bda36")))

(define-public crate-deque-0.1.7 (c (n "deque") (v "0.1.7") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "1sj5g9xf1gw89m48f9z4wpx97xl03y5x9p1kxpg4z3dwrm5drr31")))

(define-public crate-deque-0.1.8 (c (n "deque") (v "0.1.8") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "0gfhpcb0if1211w1yvwv2fydxjhwphxfh3v2rg3mca0kgqb4a1dk")))

(define-public crate-deque-0.1.9 (c (n "deque") (v "0.1.9") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "0jzjy7vrfvp08d26lvp13kc17r50sn0njdb2arawjrzfcd53ibyh")))

(define-public crate-deque-0.2.0 (c (n "deque") (v "0.2.0") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0hjyqklyhf2m2l2ymr5ar7rdzd7ifjcjiaiqycnlmp3xckwgfpz0")))

(define-public crate-deque-0.2.1 (c (n "deque") (v "0.2.1") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "12jkwl0idx5dyrhhldlx53fz00ain54cjjmanrhnrs7i3xs4xq2y")))

(define-public crate-deque-0.2.3 (c (n "deque") (v "0.2.3") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0r947118amr1hvfzcq2cgcib02kcpkyi8yarm5jhpn609pddkxc8")))

(define-public crate-deque-0.3.0 (c (n "deque") (v "0.3.0") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "072rpkkxgjbryl3321qagjrnv2dcp5z52v9va426cxywn4ysf364")))

(define-public crate-deque-0.3.1 (c (n "deque") (v "0.3.1") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1gz75w8l3l0rjxxa9j1ykxra2kb5828j297av1g7h4g78286a50n")))

(define-public crate-deque-0.3.2 (c (n "deque") (v "0.3.2") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "09zspmbp7mpfyax74qa7b5p8pbm5z115a4n7abbri5sqg3jdm556")))

