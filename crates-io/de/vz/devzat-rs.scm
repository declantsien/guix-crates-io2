(define-module (crates-io de vz devzat-rs) #:use-module (crates-io))

(define-public crate-devzat-rs-0.1.0 (c (n "devzat-rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "07g690pg4ga98rrx46bqgxm63az4d81b7qz54d9r9zbjsiykkly8") (y #t)))

(define-public crate-devzat-rs-0.1.1 (c (n "devzat-rs") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1gbvv74pypsj937w2mq08rff7nlk777bqfr01kmyc55q0swrjph2")))

(define-public crate-devzat-rs-0.1.2 (c (n "devzat-rs") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.4") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "0qlbb8fp6yfr6805dhqbay1zmp46fgsjd9y5399bf2i3f5bn7l0n")))

(define-public crate-devzat-rs-0.1.3 (c (n "devzat-rs") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.4") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1j03zivkv40l7dfikzikp7dm86inkjzrpmra5jpyzavn2fpn7ls2")))

(define-public crate-devzat-rs-0.1.4 (c (n "devzat-rs") (v "0.1.4") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "07p53j09gb2cfpgh5v0251w4qq9ryrmjyn4jkj08jnmfsw3609ml")))

