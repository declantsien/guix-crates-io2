(define-module (crates-io de ce decent-cli) #:use-module (crates-io))

(define-public crate-decent-cli-0.0.1 (c (n "decent-cli") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "decent") (r "^0.0.1") (d #t) (k 0)))) (h "1xliz2qbrc7jryxszqf92xfbi9bw91qyl8308wj8rjd5r2zpdk43")))

