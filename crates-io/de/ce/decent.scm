(define-module (crates-io de ce decent) #:use-module (crates-io))

(define-public crate-decent-0.0.1 (c (n "decent") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qxzl6bfgdk5snyhmzdwxj8fvs6rb1lffhyzrfj03n8zalahfdh6")))

