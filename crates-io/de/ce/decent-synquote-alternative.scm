(define-module (crates-io de ce decent-synquote-alternative) #:use-module (crates-io))

(define-public crate-decent-synquote-alternative-0.1.0 (c (n "decent-synquote-alternative") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "10mw3b01n2lc3ph342azdblm7mrdkd8apqfrfr20kd1zi4mqqaf5")))

(define-public crate-decent-synquote-alternative-0.2.0 (c (n "decent-synquote-alternative") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1snpygh3vjlhbk8aniy25dlp8rzhckdwjk8p2ias6xjpkg3h6s8d")))

(define-public crate-decent-synquote-alternative-0.3.0 (c (n "decent-synquote-alternative") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0fj47mzk8hqgf8zna45azr5dfcfxm15bqf3vazirvgynx6rhpcxz")))

(define-public crate-decent-synquote-alternative-0.4.0 (c (n "decent-synquote-alternative") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1n9gdww7slc0gs9gmz0i4rj02fdgz05gdqd5sjvjd9swr8rwkc9c")))

