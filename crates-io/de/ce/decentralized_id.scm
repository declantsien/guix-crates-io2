(define-module (crates-io de ce decentralized_id) #:use-module (crates-io))

(define-public crate-decentralized_id-0.0.1 (c (n "decentralized_id") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pgp") (r "^0.4.1") (d #t) (k 0)))) (h "0dvhhcgmj9plrxbhdc4cqwpfwiwmpvnxzmkbq9gbcs9k39a53kfl")))

(define-public crate-decentralized_id-0.0.2 (c (n "decentralized_id") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pgp") (r "^0.4.1") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "0jn5n1n2gx3yfk939kr4p3v1lf1xc3gp7zdfygm31w47zxp21id2")))

