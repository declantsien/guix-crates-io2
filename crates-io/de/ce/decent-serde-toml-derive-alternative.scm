(define-module (crates-io de ce decent-serde-toml-derive-alternative) #:use-module (crates-io))

(define-public crate-decent-serde-toml-derive-alternative-0.1.0 (c (n "decent-serde-toml-derive-alternative") (v "0.1.0") (d (list (d (n "decent-synquote-alternative") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1lkfml13ipc4x2ma15ic3rxllragg26mxz3g25rgd7iiwps4h6vc")))

(define-public crate-decent-serde-toml-derive-alternative-0.2.0 (c (n "decent-serde-toml-derive-alternative") (v "0.2.0") (d (list (d (n "decent-synquote-alternative") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "00gnkfss1yiva3vjwcg22y11gbh34r75zfhrc0w1hbgr7kxj3h6r")))

(define-public crate-decent-serde-toml-derive-alternative-0.3.0 (c (n "decent-serde-toml-derive-alternative") (v "0.3.0") (d (list (d (n "decent-synquote-alternative") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0zk7sgj0q8kpmpqycg5al4p2sp719qkcrngzqw4gsb0dv92qfj2k")))

