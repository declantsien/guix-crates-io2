(define-module (crates-io de ce decent-serde-json-alternative) #:use-module (crates-io))

(define-public crate-decent-serde-json-alternative-0.1.0 (c (n "decent-serde-json-alternative") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1zqj3gk6kljwvm7g9gj7hwa203f1k68fnjz71dfkw2g88pv6k9hx")))

(define-public crate-decent-serde-json-alternative-0.2.0 (c (n "decent-serde-json-alternative") (v "0.2.0") (d (list (d (n "decent-serde-json-derive-alternative") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0jwcv26jr1dm9pw4iqjyz3s5z94pk3hlb3franc79862bibcn59r") (f (quote (("derive" "decent-serde-json-derive-alternative") ("default"))))))

(define-public crate-decent-serde-json-alternative-0.2.1 (c (n "decent-serde-json-alternative") (v "0.2.1") (d (list (d (n "decent-serde-json-derive-alternative") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1kz55yzvg3kifvz3yqzzbq0i6k17akqwcdkg21y80l9wdc1hkqbc") (f (quote (("derive" "decent-serde-json-derive-alternative") ("default"))))))

(define-public crate-decent-serde-json-alternative-0.3.0 (c (n "decent-serde-json-alternative") (v "0.3.0") (d (list (d (n "decent-serde-json-derive-alternative") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0ws66rx318i98vkf0wdy1a7777mdxnszf7j1xxkj5mxry0ckrvvv")))

(define-public crate-decent-serde-json-alternative-0.4.0 (c (n "decent-serde-json-alternative") (v "0.4.0") (d (list (d (n "decent-serde-json-derive-alternative") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0wr5jml3ffi0d6nvhhn8491aypqn3raij8iwnpjq2mjk1zwnmdzx")))

(define-public crate-decent-serde-json-alternative-0.5.0 (c (n "decent-serde-json-alternative") (v "0.5.0") (d (list (d (n "decent-serde-json-derive-alternative") (r "^0.5") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "03m4kyf983l35v1vda6zhcibz35bxdl8ncq9x0307njblj4iifqg")))

