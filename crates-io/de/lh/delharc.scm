(define-module (crates-io de lh delharc) #:use-module (crates-io))

(define-public crate-delharc-0.1.0 (c (n "delharc") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jlzf18x9znbrs2jy5fxwjq5dvx3dx3aj6ii05i1ljckxy9k5s64") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.2.0 (c (n "delharc") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1hiasnhkq3ff0cxch49wi4ccp970jxaz7p43k5s1mq4psnh5laf4") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.2.1 (c (n "delharc") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ayv9mism53d582wllndrg08xc1sa7fpz93gym2pxkjfll4yg710") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.2.2 (c (n "delharc") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0mw05s1fvrvyzwsvpz033vs4zk30mmy5gv56mm6xh7vqf9s4p309") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.3.0 (c (n "delharc") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3.5") (d #t) (k 2)) (d (n "memchr") (r "^2") (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ad1y7m0g746mn0ng5nh1vvhzf76blb32iph9k2l237f985hdgsw") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.4.0 (c (n "delharc") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.4") (d #t) (k 2)) (d (n "memchr") (r "^2") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17smmvhywpj4z3xans7pclj7cb9n70r0jisw440nn56msxpigrxk") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.5.0 (c (n "delharc") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.4") (d #t) (k 2)) (d (n "memchr") (r "^2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ab0m7z266pqiqvfz729vj3azciwv4hy5bmd2n3pb510w7nv287l") (f (quote (("lz") ("lhx") ("lh1") ("default" "lh1" "lz"))))))

(define-public crate-delharc-0.6.0 (c (n "delharc") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "crc-any") (r "^2.4") (k 2)) (d (n "memchr") (r "^2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fr61fx4jmvr3mkn68igv24lqny7qlypzffis9262214s79q3zdc") (f (quote (("std" "chrono/std" "chrono/clock") ("lz") ("lhx") ("lh1") ("default" "std" "lh1" "lz"))))))

(define-public crate-delharc-0.6.1 (c (n "delharc") (v "0.6.1") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "crc-any") (r "^2.5") (k 2)) (d (n "memchr") (r "^2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18g5haj6bj92azif4jifhdy9vrv6blg3wyvpmxslh2gm2wkbm4qw") (f (quote (("std" "chrono/std" "chrono/clock") ("lz") ("lhx") ("lh1") ("default" "std" "lh1" "lz"))))))

