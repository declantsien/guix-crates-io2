(define-module (crates-io de pm depmap) #:use-module (crates-io))

(define-public crate-depmap-0.0.1 (c (n "depmap") (v "0.0.1") (h "14583dg9ad5a05j4mq5v97bdkdf4hhpgp3c8rnf5vfx668dxza3s") (f (quote (("default"))))))

(define-public crate-depmap-0.0.2 (c (n "depmap") (v "0.0.2") (h "0gv925q3apds2iq9f7n8v33pbvmfvd99qqgcx8136i1f2z5iv6a3") (f (quote (("default"))))))

(define-public crate-depmap-0.0.3 (c (n "depmap") (v "0.0.3") (h "0g6ibgw7vmp8kkd9zc2kwn5nxyf6njpy357lxxv424h0a5zgmwrh") (f (quote (("default"))))))

