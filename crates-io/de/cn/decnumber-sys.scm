(define-module (crates-io de cn decnumber-sys) #:use-module (crates-io))

(define-public crate-decnumber-sys-0.1.0 (c (n "decnumber-sys") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0pfkin0xzibijcnmxjl7rkgi96v1l7gdc1qj2b7n9d6nlbpfrwpb") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.1 (c (n "decnumber-sys") (v "0.1.1") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0p4i1xkpvqf29r934b6ya0y2q0a4nnczfhl2sfjlx687dj7d7kjm") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.2 (c (n "decnumber-sys") (v "0.1.2") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1i99myw06c7nxs89kbjvhyf1xwc9hzndyfg4q4p9fz2l6sdf6p96") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.3 (c (n "decnumber-sys") (v "0.1.3") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vsc7r9sn7002iir54p7r2as2ikvbr4xaf5ayk20hq6yjwymzwp9") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.4 (c (n "decnumber-sys") (v "0.1.4") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0xa881g5flxv3f03gqdrrcjd5xyi8q8ilpbsqfqcgnd3s6gqb4ka") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.5 (c (n "decnumber-sys") (v "0.1.5") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0yx4ixj6bsa6hmvsxkcmiq4yvry26p8qc810lb04nwhriyarpabn") (l "decnumber")))

(define-public crate-decnumber-sys-0.1.6 (c (n "decnumber-sys") (v "0.1.6") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 2)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0nqsdlryhi9f3150rbh18wna5ccsl9qr8gyidpabrmabh4rvrd13") (l "decnumber")))

