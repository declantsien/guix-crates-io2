(define-module (crates-io de uc deuces-rs) #:use-module (crates-io))

(define-public crate-deuces-rs-0.1.0 (c (n "deuces-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lpr4d6wbwjii6ykgqj4ggd103sykbqd6c1m3kgw3fk4lwdqgv4q")))

(define-public crate-deuces-rs-0.2.0 (c (n "deuces-rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02cv9c11gpcdmh3wvzqkn5s5p7g6mqif222ga2icymry769p8law")))

(define-public crate-deuces-rs-0.3.0 (c (n "deuces-rs") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pb6xgm78asczn2zq8z7r2xwilbzzv9lr411la2s592if5kya7hm")))

