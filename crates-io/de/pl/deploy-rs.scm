(define-module (crates-io de pl deploy-rs) #:use-module (crates-io))

(define-public crate-deploy-rs-0.0.0 (c (n "deploy-rs") (v "0.0.0") (h "0v38dv3f0c1fdakw5kiynayi0wy5nwarjh0wb6zydxlqpkmsz4cc")))

(define-public crate-deploy-rs-0.1.0 (c (n "deploy-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "18yfbppxb4qaly84qa8sp1dfhkdda8fk806pflqix5r721j4v5my")))

(define-public crate-deploy-rs-0.1.1 (c (n "deploy-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "14qjnnq4kanbhsw7s52q404z9igkc72kyacvp874qb43ph5nk5az")))

(define-public crate-deploy-rs-0.1.2 (c (n "deploy-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1mm467bkw89m7si99226ny23c5h8gnyrq45lzq3v4q6bq4g854j5")))

