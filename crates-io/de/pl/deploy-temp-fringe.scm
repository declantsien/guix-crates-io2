(define-module (crates-io de pl deploy-temp-fringe) #:use-module (crates-io))

(define-public crate-deploy-temp-fringe-1.2.2 (c (n "deploy-temp-fringe") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1f1gskpn2kvrzc40k5vi8qs8fvb9ds6pfkibkn78cpj9lm2n7mhi") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-deploy-temp-fringe-1.2.3 (c (n "deploy-temp-fringe") (v "1.2.3") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0viddc65wm08zb3ad3sx7v4xrbpars4fz5znmmjccvvfhh62ibax") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-deploy-temp-fringe-1.2.4 (c (n "deploy-temp-fringe") (v "1.2.4") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1acnfhndyii72dffv8k1pnyrpxidcdpp203860ppyzcljy7l3haq") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

