(define-module (crates-io de pl deployer) #:use-module (crates-io))

(define-public crate-deployer-0.0.1 (c (n "deployer") (v "0.0.1") (d (list (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0qad6dz0d2nwd7j7zhd0vm1905xkr0gwfpgvk8vwrg64vs65i73d")))

(define-public crate-deployer-0.0.2 (c (n "deployer") (v "0.0.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)))) (h "0h8yv6q05cxfi3g5qdx1i3nd35dybq9506jqi1q3jisipzgyrnpf")))

