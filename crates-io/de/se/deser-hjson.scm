(define-module (crates-io de se deser-hjson) #:use-module (crates-io))

(define-public crate-deser-hjson-0.1.0 (c (n "deser-hjson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1dzs68zjijg4vpn8zl68b10sxzqbnzbwq6pqvq3glmj4xbhmh3d5")))

(define-public crate-deser-hjson-0.1.1 (c (n "deser-hjson") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0w87r3qsmk7p425873xd22gpvw6lxifxhcw7qkhakjwaby1sr9m6")))

(define-public crate-deser-hjson-0.1.2 (c (n "deser-hjson") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "05m3f6rvib83b01gcs78s6qazq1mnardwgygxy1ip376cmx1a7kd")))

(define-public crate-deser-hjson-0.1.3 (c (n "deser-hjson") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1nimphdplpz1k03m2pjrl4a4k3k9v2pgjyvb3gpblcsdhp0c69g1")))

(define-public crate-deser-hjson-0.1.4 (c (n "deser-hjson") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "17hhd0x4f38b3js1nfc0zgz1b6scpkr0zddn61k659yvj94kbqg2")))

(define-public crate-deser-hjson-0.1.5 (c (n "deser-hjson") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d4806rw7vpfxf6xzgrjkw09zmwl0jnmml667ryiqf177zpaj4lv")))

(define-public crate-deser-hjson-0.1.6 (c (n "deser-hjson") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "153shi4rvw4zdryn0dgw19wgc2w3ffsi0r687xzhzh2m6qxz6cka")))

(define-public crate-deser-hjson-0.1.7 (c (n "deser-hjson") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a2ac17k0h311k9v5zmps7b2k7062j4ks3m7q7j4xmhpskbx62zy")))

(define-public crate-deser-hjson-0.1.8 (c (n "deser-hjson") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1azpspv7wbyga67wlgim7canhiywx160vbc1i6gc7kp4v6yyk5vl")))

(define-public crate-deser-hjson-0.1.9 (c (n "deser-hjson") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hapad6l2k9pxh87s3f247dfic3gg1gxw3df8qgfm6jwshb6ygb9")))

(define-public crate-deser-hjson-0.1.10 (c (n "deser-hjson") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c4qkfi08s94fg4fzd1929qdmj3rgd84i2klp1dppckhr1bfgcqf")))

(define-public crate-deser-hjson-0.1.11 (c (n "deser-hjson") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xhxv9ff6wcjpcydq68700qily1ivd7vzzw2s7f21z398wja79mc")))

(define-public crate-deser-hjson-0.1.12 (c (n "deser-hjson") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pns760ki3wn6fkasx5xr0d8lhd7f3dza2wgm40szihvz07vd5fr")))

(define-public crate-deser-hjson-0.1.13 (c (n "deser-hjson") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wp05allbw2nw9swnl6xf1ddg678sslz3gxjns35zr5j64q8yjvl")))

(define-public crate-deser-hjson-1.0.0 (c (n "deser-hjson") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xphadj0ljkykj9fzyaslhv2ybz0bkcqx981ch51vx4knwwf8a0v")))

(define-public crate-deser-hjson-1.0.1 (c (n "deser-hjson") (v "1.0.1") (d (list (d (n "glassbench") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15p8y312kkll1jl17hyfjp8ww8cp1vilr0anrh8bn4azzgj76adz")))

(define-public crate-deser-hjson-1.0.2 (c (n "deser-hjson") (v "1.0.2") (d (list (d (n "glassbench") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x93mash0z43li7k5mam4pqbjvlb6m5mldrn8wvgkk9y3zsnyj0z")))

(define-public crate-deser-hjson-1.1.0 (c (n "deser-hjson") (v "1.1.0") (d (list (d (n "glassbench") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gq4njp8vp794zwflgwgxa6rjmxnv9h6xhp2d99ig6b10wim56vr")))

(define-public crate-deser-hjson-1.1.1 (c (n "deser-hjson") (v "1.1.1") (d (list (d (n "glassbench") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d2f0pblkzvl14r14hwzm80985icf3r1w9axghzyyj5j48sshha1")))

(define-public crate-deser-hjson-1.2.0 (c (n "deser-hjson") (v "1.2.0") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bacgp2zhyxj6m7hh00sl65b8bripw5nb80jwcniplglzscspq9h")))

(define-public crate-deser-hjson-2.0.0 (c (n "deser-hjson") (v "2.0.0") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l8b80ad8nx64326wwlphgmxg5v318856icc7yq8jxy852kmssdh")))

(define-public crate-deser-hjson-2.1.0 (c (n "deser-hjson") (v "2.1.0") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lrqy7klsmmqw30q1rb2b7p4k1y7jxh10948wk9cg7x33wzg9i7d")))

(define-public crate-deser-hjson-2.2.0 (c (n "deser-hjson") (v "2.2.0") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "165d2dwi7x20iqjcvbsa5ci506rwxrvi1g3cjjc7ql0rgkby6kwv")))

(define-public crate-deser-hjson-2.2.1 (c (n "deser-hjson") (v "2.2.1") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bnrgdr6bwr2618gybspra6rxq51ag4paxay77bz94yhvrqz9w1")))

(define-public crate-deser-hjson-2.2.2 (c (n "deser-hjson") (v "2.2.2") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16nqqi61g4rv6x7gj6n1n7njvd5s89y1ivmzqybpkl5bw2fsv7bd")))

(define-public crate-deser-hjson-2.2.3 (c (n "deser-hjson") (v "2.2.3") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "065qs38vz1yrrib7wj5vxsxdkk4axs2kq5qivcw1bp3x81dnppnz")))

(define-public crate-deser-hjson-2.2.4 (c (n "deser-hjson") (v "2.2.4") (d (list (d (n "glassbench") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qc82vh683wb3359fs5r9fwi37wjnb17zfmrwkbxw22w172am53x")))

