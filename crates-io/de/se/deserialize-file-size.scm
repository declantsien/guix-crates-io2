(define-module (crates-io de se deserialize-file-size) #:use-module (crates-io))

(define-public crate-deserialize-file-size-1.0.0 (c (n "deserialize-file-size") (v "1.0.0") (d (list (d (n "byte-unit") (r "^4.0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0xjz4hc9dq96hj4vphhndv9p21ja8mryazapfd35jpc1pknqcyfx")))

