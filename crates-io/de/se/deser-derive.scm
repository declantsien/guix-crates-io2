(define-module (crates-io de se deser-derive) #:use-module (crates-io))

(define-public crate-deser-derive-0.3.0 (c (n "deser-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0c044cwl96m6fk892mgdkswqhdm5p5g41p41avb7gvmy1fa7rbln")))

(define-public crate-deser-derive-0.3.1 (c (n "deser-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1pg8w4axarzk1kjyfwxvwkwcwslh3dpgn63z42l44prpprg44zn1")))

(define-public crate-deser-derive-0.4.0 (c (n "deser-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "1lf86f25v316scffy4n3xqxbbrmda33769ci3marwdmfsl8crszl")))

(define-public crate-deser-derive-0.5.0 (c (n "deser-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0rpqkwj33a4a546dhh8shvgzjwq4nxr07jwxhv3zjd9hb2r9h3fm")))

(define-public crate-deser-derive-0.6.0 (c (n "deser-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0xhdld9lym9r5dsdi7pcccaxd56knz444pwbwmyqnggq14gqllmm")))

(define-public crate-deser-derive-0.7.0 (c (n "deser-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0m6z7zsldywghg3vjslghbikgvxd9dy30ab8s0s70i0pg0kf1s8i")))

(define-public crate-deser-derive-0.8.0 (c (n "deser-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "09rj6r1xl8dicijf7zg835xmm70pq01ccs9v7cipidvy8km7mk6r")))

