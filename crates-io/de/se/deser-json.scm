(define-module (crates-io de se deser-json) #:use-module (crates-io))

(define-public crate-deser-json-0.7.0 (c (n "deser-json") (v "0.7.0") (d (list (d (n "deser") (r "^0.7.0") (d #t) (k 0)) (d (n "deser") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xzdfinh53j57xi7hchxik2iwfgvapsmywjfr3cyavkpmz2xgpx3")))

(define-public crate-deser-json-0.8.0 (c (n "deser-json") (v "0.8.0") (d (list (d (n "deser") (r "^0.8.0") (d #t) (k 0)) (d (n "deser") (r "^0.8.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "itoa") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "0gc4lkhk7x3b623gf7fkc7zcgqlx79kh2rw4awy8x4jpw3qjb4b4") (f (quote (("speedups" "itoa" "ryu"))))))

