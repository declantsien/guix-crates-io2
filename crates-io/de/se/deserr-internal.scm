(define-module (crates-io de se deserr-internal) #:use-module (crates-io))

(define-public crate-deserr-internal-0.1.0 (c (n "deserr-internal") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1qj5ccl4rnbwdnfcj6y4zwqy806kq4jwhivwvxz2s6dcn3mbm260")))

(define-public crate-deserr-internal-0.1.2 (c (n "deserr-internal") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1ykv7akqnx6537l9dhswlliv5r8shcy1zxz6j7z0lzdz5zsz347j")))

(define-public crate-deserr-internal-0.1.4 (c (n "deserr-internal") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "16n4yxxw497jxim2zglnn1nbg4dyf3swjri1l9vc66sq4wfdwcbi")))

(define-public crate-deserr-internal-0.1.5 (c (n "deserr-internal") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0arjipx87zasggl4kdnv1fxy26cvrlcdlsfxrggcv0mpsg5iar0r")))

(define-public crate-deserr-internal-0.1.6 (c (n "deserr-internal") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0lh71nsx61nsypsbvfscdpk8w77i8vpawhwwc0ah8ssqk5h3azgz")))

(define-public crate-deserr-internal-0.2.0 (c (n "deserr-internal") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "18f3nd4fgc6xaq5pzkl28z64w7rgipnsk5pnjs82k1q6d24i4214")))

(define-public crate-deserr-internal-0.3.0 (c (n "deserr-internal") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0l8lgknrsf8a1j797kgfla5dxhyp44gmipbh7lx253gpib6jh2c6")))

(define-public crate-deserr-internal-0.4.0 (c (n "deserr-internal") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1r1saw4hsmjhibacbl3199qiyz9wig90369i14mm8jbidlc14m2d")))

(define-public crate-deserr-internal-0.4.1 (c (n "deserr-internal") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1fd41ls0s09i65irxgfp3qjjkda3vijbc56nf3fskswyrbw4cwn2")))

(define-public crate-deserr-internal-0.5.0 (c (n "deserr-internal") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "06llii1wzq2i2a4wq1n2l76izxllvrqzdkynwpjcja0m34dwbqfa")))

(define-public crate-deserr-internal-0.6.0 (c (n "deserr-internal") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1vzkcsp2rsjmi0qqdpw1rhd03cxz8602lmx3qy0di1yqwn35qkcc")))

(define-public crate-deserr-internal-0.7.0 (c (n "deserr-internal") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0zm3jlrd7k37c2h2clxsmkikbcxv9y0c07n8svbcnplr9iidkbcw")))

