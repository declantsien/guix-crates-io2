(define-module (crates-io de se deserialize_xml) #:use-module (crates-io))

(define-public crate-deserialize_xml-0.1.0 (c (n "deserialize_xml") (v "0.1.0") (d (list (d (n "deserialize_xml_derive") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "04aahzjxr142fld8clk69fqsa31rz5qkkzwqfy29z7byzcf5ksai")))

(define-public crate-deserialize_xml-0.2.0 (c (n "deserialize_xml") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "deserialize_xml_derive") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0w75pj31iyic69ysg9bw4zg60srzh6qsb0pxqhda7vh2gpgn9kaa") (y #t)))

(define-public crate-deserialize_xml-0.2.1 (c (n "deserialize_xml") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "deserialize_xml_derive") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0cihf5jpj3lfrv7q4pnfn0qfa36jaki8a14na6rrfsrnyxrmi4mb")))

