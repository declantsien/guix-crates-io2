(define-module (crates-io de se desed) #:use-module (crates-io))

(define-public crate-desed-1.0.0 (c (n "desed") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "17kmbgf8xlv66x3qw48yy7l7hdlqldhdwsfsp9xwkf8w7c06h1a7")))

(define-public crate-desed-1.1.0 (c (n "desed") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "15pgm78y77l7xb77z732bn9k72qmca6i3qiyp6yiqw9bm2m62i5y")))

(define-public crate-desed-1.1.1 (c (n "desed") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "193nvdf87x3c3h80dm2akm31l23h0rc67cn96byjrjvg9v44bws0")))

(define-public crate-desed-1.1.2 (c (n "desed") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "1kj2ypza4kfnp6fxivza6y8xln5szcx4z7m6p16iysydsb2q4n2k")))

(define-public crate-desed-1.1.3 (c (n "desed") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "1l13700cyhy0vlz12w5000chh08qs9dpkxf7shc7ja6gq2fhllhc")))

(define-public crate-desed-1.1.4 (c (n "desed") (v "1.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "075mlr9yg9vpr6hpzynils9c4x0yca2s3xfws5fxnfk5v0xb9w6s")))

(define-public crate-desed-1.2.0 (c (n "desed") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "inotify") (r "^0.8.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "kqueue") (r "^1.0.1") (d #t) (t "cfg(any(target_os = \"darwin\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)) (d (n "tui") (r "^0.9.1") (f (quote ("crossterm"))) (k 0)))) (h "050svsdlk1gnv9m9vxlwq5cjbawxai5y4jdhs2c9233pdxms5vf5")))

(define-public crate-desed-1.2.1 (c (n "desed") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "inotify") (r "^0.10.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "kqueue") (r "^1.0.6") (d #t) (t "cfg(any(target_os = \"darwin\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm"))) (k 0)))) (h "0513a7dw7dq8s22iaxfrhzfl99f2kfw1cbkwhkggxm9rc98qmyr5")))

