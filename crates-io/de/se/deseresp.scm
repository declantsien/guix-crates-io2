(define-module (crates-io de se deseresp) #:use-module (crates-io))

(define-public crate-deseresp-0.1.0 (c (n "deseresp") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lf7zv4k6kd46f87mccynj8na2ldkw88vjk05vq2cbxvgzc7js3h")))

(define-public crate-deseresp-0.1.1 (c (n "deseresp") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "031pb495ciibhdz1fan6qw23bygc0i58n3h8pp326xg0zcw2jn2z")))

(define-public crate-deseresp-0.1.2 (c (n "deseresp") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01ahl50r9clc0dqjdhadayakd1v36a92wyv70wk9alfdajhrxmcl")))

(define-public crate-deseresp-0.1.3 (c (n "deseresp") (v "0.1.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sbmn7lqq4dlj8gzi6m3rbww2n9dis6cv53ksv877k9fvwwakxqn")))

(define-public crate-deseresp-0.1.4 (c (n "deseresp") (v "0.1.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07m4sq2bbll54lzl9ryk68y6lqhnscvx34vgl39qdhswzdd2gd49")))

(define-public crate-deseresp-0.1.5 (c (n "deseresp") (v "0.1.5") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gv53pg7l5vp0lnrd8fsgwrbj45nvnm197np3vm4849pfdpdf4b4")))

(define-public crate-deseresp-0.1.6 (c (n "deseresp") (v "0.1.6") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09sdqdslhsm6wm2xmh9mmvwnlnhhqzp4xkmd6q8m53qh5is4zl0p")))

(define-public crate-deseresp-0.1.7 (c (n "deseresp") (v "0.1.7") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ksc5z5722kdclcip9h5k5vsmbid3z2mqbw69vcqy3d7a3mimjc6")))

(define-public crate-deseresp-0.1.8 (c (n "deseresp") (v "0.1.8") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "033m6q4xg8zfw1ac1iw9aqmrazndmx5mhzk81n1bix37b48h7q2r")))

(define-public crate-deseresp-0.1.9 (c (n "deseresp") (v "0.1.9") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dbzna03fgsz099ycgicv0bwa2qmrarjpswx2kvbjr2fdiixha23")))

