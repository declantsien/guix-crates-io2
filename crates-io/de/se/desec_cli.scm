(define-module (crates-io de se desec_cli) #:use-module (crates-io))

(define-public crate-desec_cli-0.1.0 (c (n "desec_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "desec_api") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "16izdc6k2rpal4i835nwlcdc0vg8s7k5cidvkxvix7iq8k14kn8a") (s 2) (e (quote (("logging" "dep:env_logger")))) (r "1.77.2")))

(define-public crate-desec_cli-0.1.1 (c (n "desec_cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "desec_api") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "03gm4vv7mpm5387x2b30zap4n5p99ylxiv0xnv3l2al34d4jh46l") (s 2) (e (quote (("logging" "dep:env_logger")))) (r "1.77.2")))

(define-public crate-desec_cli-0.1.2 (c (n "desec_cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "desec_api") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0a8pjkx9033xsynlnv9j3g45z8i1cm4594bmwr0sia92pk746kvx") (s 2) (e (quote (("logging" "dep:env_logger")))) (r "1.74.1")))

