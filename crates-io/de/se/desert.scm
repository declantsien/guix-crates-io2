(define-module (crates-io de se desert) #:use-module (crates-io))

(define-public crate-desert-1.0.0 (c (n "desert") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "0zfgdbnq1di17qhvpnq1mrkkhkajvjic41w5wzbxw3d2g5095dvd")))

(define-public crate-desert-1.0.1 (c (n "desert") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1y3jh19vr4xj268lyjfcs7gff66l597wbz1ns13zzg0mmmzia8vw")))

(define-public crate-desert-1.0.2 (c (n "desert") (v "1.0.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "09sz9k0ip84lm30xlh12ak2021g6qbqawz98l3zjm38mm279c8fq")))

(define-public crate-desert-1.0.3 (c (n "desert") (v "1.0.3") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1ns5nn4rr7d23g85b90bn78b2ip3rp6zzan9crdy9q5aw7ym3396")))

(define-public crate-desert-2.0.0 (c (n "desert") (v "2.0.0") (h "06jddl5zdyyfzdcn8ssciqjhislrzbakcaslwn7zmnmg7yn2qsc9")))

(define-public crate-desert-2.0.1 (c (n "desert") (v "2.0.1") (h "05gi7j6x16hj0jmg73wb1c0mnhp45id1i1psarq7mvqs09a59sm5")))

