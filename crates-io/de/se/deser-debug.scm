(define-module (crates-io de se deser-debug) #:use-module (crates-io))

(define-public crate-deser-debug-0.2.0 (c (n "deser-debug") (v "0.2.0") (d (list (d (n "deser") (r "^0.2.0") (d #t) (k 0)))) (h "1gf0hgvl6i3qm1as3qf189ddk0jz87cigiwv95nm7rkjdgpfas1r")))

(define-public crate-deser-debug-0.3.0 (c (n "deser-debug") (v "0.3.0") (d (list (d (n "deser") (r "^0.3.0") (d #t) (k 0)))) (h "1c5c7y4h1z71fwk4g169gpggzyyy6lzm8kwcrrjaga3jmss2jpij")))

(define-public crate-deser-debug-0.3.1 (c (n "deser-debug") (v "0.3.1") (d (list (d (n "deser") (r "^0.3.1") (d #t) (k 0)))) (h "0p1gmdk5441qpclg7wcdwn9qyxr4581zs0d8s0jazfhlgr9w4s9x")))

(define-public crate-deser-debug-0.4.0 (c (n "deser-debug") (v "0.4.0") (d (list (d (n "deser") (r "^0.4.0") (d #t) (k 0)))) (h "1b900kaxp1ly4j3dbkcm75ka7g020y6zx20rqmyhavq1fc5inl7a")))

(define-public crate-deser-debug-0.5.0 (c (n "deser-debug") (v "0.5.0") (d (list (d (n "deser") (r "^0.5.0") (d #t) (k 0)))) (h "0g9f4zywsnrfsb3504dkgd3w2a43wml6qzl82xbmssf92gd4hyl4")))

(define-public crate-deser-debug-0.6.0 (c (n "deser-debug") (v "0.6.0") (d (list (d (n "deser") (r "^0.6.0") (d #t) (k 0)))) (h "17srwzwmyg9ss4ak0libc13hjnbq2a1bag19m84p0nyn10d2qw52")))

(define-public crate-deser-debug-0.7.0 (c (n "deser-debug") (v "0.7.0") (d (list (d (n "deser") (r "^0.7.0") (d #t) (k 0)))) (h "18f72bfy5sk3984dr780adii3clm6lxpih8dg9fndlzpqcyqpiaj")))

(define-public crate-deser-debug-0.8.0 (c (n "deser-debug") (v "0.8.0") (d (list (d (n "deser") (r "^0.8.0") (d #t) (k 0)))) (h "0acb62yf8482nygxzz87hzhn6zn8a75pw0sd2wh2l17ff4m4045w")))

