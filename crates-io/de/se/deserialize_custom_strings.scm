(define-module (crates-io de se deserialize_custom_strings) #:use-module (crates-io))

(define-public crate-deserialize_custom_strings-0.1.0 (c (n "deserialize_custom_strings") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0dcv1yzj5r3539pxdl6w7rq6yr0q55bhjg1ksyjqbw4y6izj40bw")))

(define-public crate-deserialize_custom_strings-0.1.1 (c (n "deserialize_custom_strings") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ispbzanbklkmwp630765cxvzbpc4zpvlp0gd5xx40zh9ij8v1cf")))

(define-public crate-deserialize_custom_strings-0.1.2 (c (n "deserialize_custom_strings") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0g2b2aryaprl6drrqy6nkqf5bsm3c9bmbhrp29fylcw4jbjlz1k8")))

(define-public crate-deserialize_custom_strings-0.1.3 (c (n "deserialize_custom_strings") (v "0.1.3") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1i7bvwylh4vb8n8gx07fk0dqwvwskw1i5pdxx1npk7pac53w31x9")))

