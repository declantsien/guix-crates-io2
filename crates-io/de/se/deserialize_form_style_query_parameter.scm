(define-module (crates-io de se deserialize_form_style_query_parameter) #:use-module (crates-io))

(define-public crate-deserialize_form_style_query_parameter-0.1.0 (c (n "deserialize_form_style_query_parameter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1jxpc42zmr6wqrpz865zs8s1g7y10flh0dpss1x3clvz3kv70d3p")))

(define-public crate-deserialize_form_style_query_parameter-0.1.1 (c (n "deserialize_form_style_query_parameter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "19f33zm0dixn29hm3qkag56g57dy9z0xgikvdzfw46ip09yvdvk2")))

(define-public crate-deserialize_form_style_query_parameter-0.1.2 (c (n "deserialize_form_style_query_parameter") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1733pfjsz54bf8c49iwspsk8n4srnj8lmjp5ccdid94cv67akaz1")))

(define-public crate-deserialize_form_style_query_parameter-0.1.3 (c (n "deserialize_form_style_query_parameter") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "04dmal9rwrgw752akxazsbxvdm7p85fdqphsyda9l9n9r67zc73n")))

(define-public crate-deserialize_form_style_query_parameter-0.2.0 (c (n "deserialize_form_style_query_parameter") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1c6wkngfgfwfmd0wpylknvpk23wacdzfxpdxv5ppamn5y5gzcqd6")))

(define-public crate-deserialize_form_style_query_parameter-0.2.1 (c (n "deserialize_form_style_query_parameter") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1ib4w09zn3m6dgvicj4lnxrh9f83k7n21rhz0fzgxmgyhln5831n")))

(define-public crate-deserialize_form_style_query_parameter-0.2.2 (c (n "deserialize_form_style_query_parameter") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "0qkfddp5gy8kms7qgzivnbkgap714prhhllvbx4gz3m5rh5rix9j")))

