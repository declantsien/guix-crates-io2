(define-module (crates-io de se deserter) #:use-module (crates-io))

(define-public crate-deserter-0.1.0 (c (n "deserter") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5kkl5sk4cp7yrm1q1nscdjlcyn2mrvmh5iclzc7vhp7galwf7r")))

(define-public crate-deserter-0.1.1 (c (n "deserter") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "1q1izppgnasgvd2wbnq2ymd1qyip2s5s14qm5phwn0g8sgdjcsph")))

(define-public crate-deserter-0.1.2 (c (n "deserter") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "0530xrr0ax07wd2kb97hm3vi61zr2wi1ahlnvmazijxv3z9nzsyz")))

(define-public crate-deserter-0.1.3 (c (n "deserter") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "1jqwr6h61mpg9n0sbmyfm7w2fhz6qm7sf7pkzcv3zynzgfxbgr85")))

(define-public crate-deserter-0.1.4 (c (n "deserter") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full"))) (d #t) (k 0)))) (h "12h32bfxm3ngn780ic7l8l7077ip2n2hf6z401w7dhl879kcizwi")))

