(define-module (crates-io de se deser) #:use-module (crates-io))

(define-public crate-deser-0.0.1 (c (n "deser") (v "0.0.1") (h "1bmc0by3i0maa2viv9qrvlw8h56sqlqny1bvyq7vjdf3n361rgvy")))

(define-public crate-deser-0.1.0 (c (n "deser") (v "0.1.0") (h "04l7mxm1z6gm5q8yfy4zbc95cb4gpp68kl8rrhyz2s7vkd5xbbmd")))

(define-public crate-deser-0.2.0 (c (n "deser") (v "0.2.0") (h "1vkjanvqfickyz9qmam988sc19mjgih3xah1w7vyndrqjg0aj3hx")))

(define-public crate-deser-0.3.0 (c (n "deser") (v "0.3.0") (d (list (d (n "deser-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0q7bk983apg872l34p3q1vdrfb8mbmk28n6hjis75dbmppq9y9lz") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.3.1 (c (n "deser") (v "0.3.1") (d (list (d (n "deser-derive") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "12hgrdrbp6x5k9z6y3lz97xmixgljaqpx3q5gjcqn25ia9z773jm") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.4.0 (c (n "deser") (v "0.4.0") (d (list (d (n "deser-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1ag976n0xhb0imbizbaxmx3kli9lqcqnr783qdl9lkh62zl82mrc") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.5.0 (c (n "deser") (v "0.5.0") (d (list (d (n "deser-derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1cjv42vrgh9v9wz8lpl5lifyq8sfxdwid9034amw0by7z2lra9w3") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.6.0 (c (n "deser") (v "0.6.0") (d (list (d (n "deser-derive") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "06iwr98mjvq3rfajvpbpwhgxad6vkr8gs1v60hwi47xs2x0nv3ip") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.7.0 (c (n "deser") (v "0.7.0") (d (list (d (n "deser-derive") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "057rjnfnyi56816i3zsvj6x5378kap4mcmfgysakinbhr3czk3xl") (f (quote (("derive" "deser-derive"))))))

(define-public crate-deser-0.8.0 (c (n "deser") (v "0.8.0") (d (list (d (n "deser-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0nznzs8kqcdjsxg5i33x0mks56295182dqmd8rfafs5qwg5h9h1k") (f (quote (("derive" "deser-derive"))))))

