(define-module (crates-io de se desenv_macros) #:use-module (crates-io))

(define-public crate-desenv_macros-0.1.0 (c (n "desenv_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "038zmkdi5vjgybkjkgvsjsl1wjh8ffgv703awcjzl11k0dxb4f2c") (r "1.58.0")))

