(define-module (crates-io de vr devrc-core) #:use-module (crates-io))

(define-public crate-devrc-core-0.5.0 (c (n "devrc-core") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07kwhyac2rdd6cwarqz6l0yb7p5f6blfp6m6nq67d6ybq9pkbssg")))

(define-public crate-devrc-core-0.5.1 (c (n "devrc-core") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "19rjrvawkiff10z4n0c1d7b6shzw4583adzlh5nmwz4gglhszm6x")))

(define-public crate-devrc-core-0.5.2 (c (n "devrc-core") (v "0.5.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12d2dajhy76y0d0w8h0idb5dh0msxxyf057bpkhf4xzr3af6qqcq")))

(define-public crate-devrc-core-0.5.3 (c (n "devrc-core") (v "0.5.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0z0i6lw675m6180g2l7qklgiz89zsvx072jp26ifi0byvrwwcq61")))

(define-public crate-devrc-core-0.5.4 (c (n "devrc-core") (v "0.5.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "02avlc0f54aw0mpg0d8zsxrwdarrc1my5a0hrwc8jl73l8fkj448")))

(define-public crate-devrc-core-0.6.0 (c (n "devrc-core") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "19wjvw692g8x1i92wq5lak6d02nr2isx2cshlcjz040x8p38j49a")))

