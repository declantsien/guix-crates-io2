(define-module (crates-io de ni denim) #:use-module (crates-io))

(define-public crate-denim-0.1.0 (c (n "denim") (v "0.1.0") (d (list (d (n "cotton") (r "^0.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1vrwjprhvsc48nqaaic40jwwb0nmzabfh82v0y33mbk713mi1jak")))

(define-public crate-denim-0.1.1 (c (n "denim") (v "0.1.1") (d (list (d (n "cotton") (r "^0.0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "06qf3pxjsaz8b647idyd15zwq6a80g11zi7ck6rxdgpbg6pl0xrx")))

(define-public crate-denim-0.1.3 (c (n "denim") (v "0.1.3") (d (list (d (n "cotton") (r "^0.0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0lz0z011s8c69kbmvflahfdd3vjgmgq1gjix2bm4323xbdd2n6jd")))

(define-public crate-denim-0.1.4 (c (n "denim") (v "0.1.4") (d (list (d (n "cotton") (r "^0.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0h8jf827b3bxqabyr2ihkkiv9frjdbliih4xspy8330ccki0rj1k")))

(define-public crate-denim-0.1.5 (c (n "denim") (v "0.1.5") (d (list (d (n "cotton") (r "^0.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0g40d120wich78y4kizdq2pmhjgaif4wa6in2c6jckvwjwz5r6kh")))

(define-public crate-denim-0.2.0 (c (n "denim") (v "0.2.0") (d (list (d (n "cotton") (r "^0.1.0") (f (quote ("errors" "args" "logging" "app" "hashing" "process"))) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1vq83nm8bzg8qrij20nqrz0i7wc3yjn65m00g1jk1ljhwxrmz4lf")))

