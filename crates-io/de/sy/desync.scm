(define-module (crates-io de sy desync) #:use-module (crates-io))

(define-public crate-desync-0.1.0 (c (n "desync") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0z4sgh2hqnpc1y5c2q4a02d9yr6735mm24bndj89ch7zavilir33")))

(define-public crate-desync-0.1.1 (c (n "desync") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "11jhp3bh3wsr7xvsswzam7smc1yn1pmlbakknqq1kq614fy46gmw")))

(define-public crate-desync-0.1.2 (c (n "desync") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0scsdmyn4fd2q6yz5hsvy89zia5qj0fnss8yxhiyr6k2ykl7qmlz")))

(define-public crate-desync-0.2.0 (c (n "desync") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1bg1bv5cih9hzz3ch6w0hiyjrgii7vz3k9ggpvnqiz8pdj1f77wq")))

(define-public crate-desync-0.2.1 (c (n "desync") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "03vm6ap8i70rr4rrf5md35bckfzyrhxfvm91cycypgxnzpqamg1c")))

(define-public crate-desync-0.2.2 (c (n "desync") (v "0.2.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0j4ghiwqgzbkgz64nlmjkg2fn5d6k7wmr0rm4aa0ww24ms9hwygn")))

(define-public crate-desync-0.2.3 (c (n "desync") (v "0.2.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0h0dvfqkhb48lqi5h3hqi5cb4f76pl8llc4p8d13na6329ywmapy")))

(define-public crate-desync-0.2.4 (c (n "desync") (v "0.2.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0shcljrxjr4j2jl4v0dy32vcz87x2yl6bfw00xfg3vky1q28gf1p")))

(define-public crate-desync-0.3.0 (c (n "desync") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "042lm4p91s1dx3iy9fb4zsknhd58cspmh5k5fgwrgpc83z90rhvs")))

(define-public crate-desync-0.4.0 (c (n "desync") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ycrw4xbkbm18ahb5w675f30bdr73igl30mz084ffhzy8pxnzjqx")))

(define-public crate-desync-0.4.1 (c (n "desync") (v "0.4.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0if5lyi4rbwkmf7zvmwcs8g67839cxr739sc5m7r6aq83axacpy5")))

(define-public crate-desync-0.5.0 (c (n "desync") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1sgq55k1plz9j394k485c550160q7km8qq7q0c2pn2f0a218rbmh")))

(define-public crate-desync-0.6.0 (c (n "desync") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0jmsmxmk2x62jsaw21la1wllmmhsriavcrfl7y8p20v8mmwbw94l")))

(define-public crate-desync-0.6.1 (c (n "desync") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0xrp3ij3s10slrbfm1pnihy68klw7g82ri2b2m6f8afspcykqxqm")))

(define-public crate-desync-0.6.2 (c (n "desync") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "01xs42gh59s6qj883hx8n61nwx38bhrpabn9l67gy5342k2wiknv")))

(define-public crate-desync-0.7.0 (c (n "desync") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0ylj5p7xqk5yzm7p6r7slm2h8a4zwn4s1jk2zvba88x3bb7pvwnm")))

(define-public crate-desync-0.7.1 (c (n "desync") (v "0.7.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02mgl9dvbrh8zvb4hvk7pxjicbbwiazmqgrha93vhjhrdm2y31x2")))

(define-public crate-desync-0.8.0 (c (n "desync") (v "0.8.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ns3klrj7pyyr3zwrln201zjvmdn5zkjsrwfhcp861rsm4378pai")))

(define-public crate-desync-0.8.1 (c (n "desync") (v "0.8.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16k77sgs165hfb7byqic07fb4axj2541qsvyfz0b2nhbdfim6qay")))

(define-public crate-desync-0.8.2 (c (n "desync") (v "0.8.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "14l14ra7gmr27abw4kxprkp0w2l6pjlairkjki2q0wgk2hriqs9w")))

(define-public crate-desync-0.7.2 (c (n "desync") (v "0.7.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1di81s53rfgjlgivm4z0g1zqqxgwn2wcyfgffj18k4q8f7b1mjm3")))

