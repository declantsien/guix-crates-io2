(define-module (crates-io de fs defs) #:use-module (crates-io))

(define-public crate-defs-0.1.0 (c (n "defs") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.32") (d #t) (k 0)))) (h "0wq7gwlbzg03x0ql34m00ln7vvc59993hjmmg4a5spfhls5knzi5")))

