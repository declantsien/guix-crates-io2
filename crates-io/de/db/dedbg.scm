(define-module (crates-io de db dedbg) #:use-module (crates-io))

(define-public crate-dedbg-0.1.0 (c (n "dedbg") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wwx6szf4d7yyjh7qi29md844w5afk9myy24j8xn13fv3qvd85p4")))

(define-public crate-dedbg-0.1.1 (c (n "dedbg") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1fsljshbsqy64a4sl7fwn92rr9w5rgy76v4ndma3i9f75iqhqpf3")))

