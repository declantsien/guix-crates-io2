(define-module (crates-io de en deen-proc) #:use-module (crates-io))

(define-public crate-deen-proc-0.1.0 (c (n "deen-proc") (v "0.1.0") (d (list (d (n "deen") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0kbrd1i73z3bmbjjaqxv77vfb11aklridr5rmx6ck1bpsrfv6fa4")))

