(define-module (crates-io de ls delsum-poly) #:use-module (crates-io))

(define-public crate-delsum-poly-0.1.0 (c (n "delsum-poly") (v "0.1.0") (d (list (d (n "cxx") (r "^0.5") (d #t) (k 0)) (d (n "cxx-build") (r "^0.5") (d #t) (k 1)))) (h "03zckk56ap39sv3wa431h6ca9ccc9wdkw1bb8kvx746mrhx3r2l8") (l "NTL")))

(define-public crate-delsum-poly-0.1.1 (c (n "delsum-poly") (v "0.1.1") (d (list (d (n "cxx") (r ">=1.0.2, <2.0.0") (d #t) (k 0)) (d (n "cxx-build") (r ">=1.0.2, <2.0.0") (d #t) (k 1)))) (h "113rqagy4zwhzbf4sgrvg8zkcnpay7k2kmg8sg1hhlkw66zwsdv9") (y #t) (l "NTL")))

(define-public crate-delsum-poly-0.1.2 (c (n "delsum-poly") (v "0.1.2") (d (list (d (n "cxx") (r ">=1.0.2, <2.0.0") (d #t) (k 0)) (d (n "cxx-build") (r ">=1.0.2, <2.0.0") (d #t) (k 1)))) (h "0v38dhishahmmw89a681gprwnmzpfdmxndmpma4pxnaax237ph2n") (l "NTL")))

(define-public crate-delsum-poly-0.2.0 (c (n "delsum-poly") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0.32") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.32") (d #t) (k 1)))) (h "1f2gl7srwr35s6vanwjfnbmknaxn3ldczgzp9xqrhxf0nvs9gnz5") (l "NTL")))

(define-public crate-delsum-poly-0.2.1 (c (n "delsum-poly") (v "0.2.1") (d (list (d (n "cxx") (r "^1.0.115") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.115") (d #t) (k 1)))) (h "1qw5lssr05skzgswlgqbz77z4nc2d3cnjhpvb7065q9axk5m9bsh") (l "NTL")))

