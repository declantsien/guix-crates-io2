(define-module (crates-io de ls delsum) #:use-module (crates-io))

(define-public crate-delsum-0.1.0 (c (n "delsum") (v "0.1.0") (d (list (d (n "delsum-lib") (r "^0.1") (k 0)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vq283m860zdmj03ynwrqxl8gpdi8hq11zp9rajzx6k2fppldpdq") (f (quote (("parallel" "delsum-lib/default" "rayon") ("default" "parallel"))))))

(define-public crate-delsum-0.1.1 (c (n "delsum") (v "0.1.1") (d (list (d (n "delsum-lib") (r ">=0.1.2, <0.2.0") (k 0)) (d (n "rayon") (r ">=1.1.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "0m2f0rs9w8m8q2030s225vq6aggw31my8i7vy20a24x05pppgm3i") (f (quote (("parallel" "delsum-lib/default" "rayon") ("default" "parallel")))) (y #t)))

(define-public crate-delsum-0.1.2 (c (n "delsum") (v "0.1.2") (d (list (d (n "delsum-lib") (r ">=0.1.2, <0.2.0") (k 0)) (d (n "rayon") (r ">=1.1.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "0m12hz9pwfkp6x4xyzq02xyql9a9x36zdaq4saganwvfnbldyk1d") (f (quote (("parallel" "delsum-lib/default" "rayon") ("default" "parallel"))))))

(define-public crate-delsum-0.2.0 (c (n "delsum") (v "0.2.0") (d (list (d (n "delsum-lib") (r "^0.2.0") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nwbjh39a2d7fqpm7ip5dw2wda37slzqv3k5alijp011gh73nryp") (f (quote (("parallel" "delsum-lib/default" "rayon") ("default" "parallel"))))))

(define-public crate-delsum-0.2.1 (c (n "delsum") (v "0.2.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "delsum-lib") (r "^0.2.1") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "1cq9j1si0f64vfx1gyvjg30p3srpxq4n5kxlj315kjck6qs2dcb7") (f (quote (("parallel" "delsum-lib/default" "rayon") ("default" "parallel"))))))

