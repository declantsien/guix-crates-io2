(define-module (crates-io de ka deka) #:use-module (crates-io))

(define-public crate-deka-0.1.0 (c (n "deka") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "156l1aadrkpxkdjs9dvvjrcnbsggscvfiqylq2mxwkl8397ndpys")))

(define-public crate-deka-0.2.0 (c (n "deka") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0rmp158vjl1qjrzr5kgwfmwrlc0cifdkzpppqn4jl7bqdx82fvqx")))

(define-public crate-deka-0.2.1 (c (n "deka") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1mipzsh1n6aqw3a3pvq7qjxp8x8pp71l0iv52ddi8lh0xy98wvw1")))

(define-public crate-deka-0.3.2 (c (n "deka") (v "0.3.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "terminal-clipboard") (r "^0.3.1") (d #t) (k 0)))) (h "0gx9f5hggd3dzj06n6x7yzr8maavnzk17xn6flwkp8y647igjyy1")))

(define-public crate-deka-0.3.3 (c (n "deka") (v "0.3.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "terminal-clipboard") (r "^0.3.1") (d #t) (k 0)))) (h "11lk3cn3glwm9djl1cm396bfzbq0jbwfd461z815880p0dgc0sli")))

