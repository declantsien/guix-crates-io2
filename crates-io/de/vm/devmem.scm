(define-module (crates-io de vm devmem) #:use-module (crates-io))

(define-public crate-devmem-0.1.0 (c (n "devmem") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fvgq1f2z19q1cz4ki46vw3w311nnp3scja4c3x7q2894n9yxwww")))

(define-public crate-devmem-0.1.1 (c (n "devmem") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "179799ypmxrca92nh9s8k1dihh3vdkgprrj7v17vx9bani88dh0d")))

