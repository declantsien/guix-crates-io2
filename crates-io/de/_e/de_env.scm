(define-module (crates-io de _e de_env) #:use-module (crates-io))

(define-public crate-de_env-0.1.0 (c (n "de_env") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "097ixbazykkcq1pmm6b2dnwipp0jj112iyi32jkkvi1wfy1csqgc") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-beta (c (n "de_env") (v "1.0.0-beta") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "1vplxrfjc3j63n75nnsq764mv3arz50jg6kszkc9r051ix0mzh0d") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-beta.1 (c (n "de_env") (v "1.0.0-beta.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "02giici26v72158jmayys8wkg3v09i74xxwnaylrrpi7d093p2v8") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-beta.2 (c (n "de_env") (v "1.0.0-beta.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "1hplp6119kd0d27dlbh5qb8b5zh3q7cgnlrl26yf6lcllxjg8dg9") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-beta.3 (c (n "de_env") (v "1.0.0-beta.3") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "00vglfnn550dgzfn42fx4m2fr9pq00jldf09rgg8nndjav9d85ba") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-rc (c (n "de_env") (v "1.0.0-rc") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "0jl958s2l1lcvj7njmm2n94msymzy8hwbd9zsjvazyhm55l5hrkz") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-rc.1 (c (n "de_env") (v "1.0.0-rc.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "0avxphdcr0vn7gwbcnj597iz0msznvkxmy67im970lii2g0zx7bq") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0-rc.2 (c (n "de_env") (v "1.0.0-rc.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "0m4sgba8cp56hkv1j021bcp9md576ad8klcnyc3p7aaqynhfyif0") (f (quote (("truthy-falsy") ("default" "truthy-falsy")))) (y #t)))

(define-public crate-de_env-1.0.0 (c (n "de_env") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)))) (h "0pq22wj4g78vjjnawqqjpb3gx1ciy71fqrz3llahm82b0sb6axmp") (f (quote (("truthy-falsy") ("default" "truthy-falsy"))))))

