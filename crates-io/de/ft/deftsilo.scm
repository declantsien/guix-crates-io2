(define-module (crates-io de ft deftsilo) #:use-module (crates-io))

(define-public crate-deftsilo-0.1.0 (c (n "deftsilo") (v "0.1.0") (d (list (d (n "arrrg") (r "^0.3") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0ch1kq1lzl192nmm93bp9y30qpp1l46s9a8a0znrsjg79k725l29")))

