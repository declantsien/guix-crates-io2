(define-module (crates-io de ph dephy_proto) #:use-module (crates-io))

(define-public crate-dephy_proto-0.1.0 (c (n "dephy_proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)))) (h "02pc1v40kc8hjfrnjsywkv0jp6qmsxxcqy5cypd43kqvb7wj5af0") (y #t)))

(define-public crate-dephy_proto-0.1.1 (c (n "dephy_proto") (v "0.1.1") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)))) (h "1yq0vm98h0pamb44m9d2k9c384w5l8l2jaznhqxrda26svasqsf4")))

(define-public crate-dephy_proto-0.1.2 (c (n "dephy_proto") (v "0.1.2") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)))) (h "0y66hrnxd0iymqxid9rfxmhx8d6gxy609701qv0ya1i26iyhic6j")))

(define-public crate-dephy_proto-0.1.3 (c (n "dephy_proto") (v "0.1.3") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)))) (h "0yba8ralkqngs95d69krhjhs37lvk5b7bvrjqvnpjdwk0bnh4k96")))

