(define-module (crates-io de gr degrees) #:use-module (crates-io))

(define-public crate-degrees-0.1.0 (c (n "degrees") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pa3d41byqisb1h2bcdv54f3l3g1qgbkzxl35cwljxzmnlc619ik")))

(define-public crate-degrees-0.2.0 (c (n "degrees") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sp3513iqgvi8sngnh5maq0zdbysd381z12cn55chyx3363jw37k")))

(define-public crate-degrees-0.3.0 (c (n "degrees") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1skzhvdmappcw8mjx4faxnv0f5fm3pzrpyqn9d0yw60knpjanrvi") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.4.0 (c (n "degrees") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0zkknl86d53d4rlik635qd29cadrb1mv1k92ffk2gy9yrvcbix1c") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.5.0 (c (n "degrees") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "1rxfy1qyy3137040rz8gq26wbclazgfdkn3cy21ym7f798vsdl9m") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.6.0 (c (n "degrees") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "19v0192cg2skmig6k65l03jpgjc9xqf7fww8zkl1zkfva9v7360n") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

