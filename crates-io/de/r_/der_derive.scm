(define-module (crates-io de r_ der_derive) #:use-module (crates-io))

(define-public crate-der_derive-0.0.0 (c (n "der_derive") (v "0.0.0") (h "0b7ml6gfpap4hlhl40ikjgmf40ws24ggag8p2dsn5vangyygxsci") (y #t)))

(define-public crate-der_derive-0.1.0 (c (n "der_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0nv6nyq3mdhk86ic5hc7fmcq4fk14fxcg1l0ldm5w5alljib3hr8")))

(define-public crate-der_derive-0.2.0 (c (n "der_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1rnxg7bgd5m7gpmvjha6i0l39inz1apfqvxn62kpa6jhn6bqg3hy") (y #t)))

(define-public crate-der_derive-0.2.1 (c (n "der_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1xqvsj0lii2grrbqkf9kamgssz0arswbqkx7injzamycd9xzsfgr") (y #t)))

(define-public crate-der_derive-0.2.2 (c (n "der_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "08xv2a26k7madvx7lfq8zzzbjkii1v7kv97zih3akcqdks7bryxl")))

(define-public crate-der_derive-0.3.0 (c (n "der_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0d1cn7svvjc653kn28cxjkmm11zvzxcjrsn5pkalirz9apvcv07d")))

(define-public crate-der_derive-0.4.0 (c (n "der_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0nl6zaqgdcznv6g1xr9g1r4ppxl37i9lx2ciab5jfiqym2fxnq2k")))

(define-public crate-der_derive-0.4.1 (c (n "der_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0snv85yfy9iln05qsgbhwr1159gd0jfrgzj5dkrnricdc0y3pvca")))

(define-public crate-der_derive-0.5.0-pre.1 (c (n "der_derive") (v "0.5.0-pre.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0c4kc2gf33pazrdb7fncpfy09zip7by1060i081fb28ikk8pd7dm")))

(define-public crate-der_derive-0.5.0 (c (n "der_derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zw4p6yqklv4i76ms2a0gcmna648337r379d5ljgpbir5cyqylrs") (r "1.56")))

(define-public crate-der_derive-0.6.0-pre.0 (c (n "der_derive") (v "0.6.0-pre.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13pg1ymw2kn3gh1a935zaprq2i39zr57v4hq4cjp7m0rdpcn585q") (r "1.56")))

(define-public crate-der_derive-0.6.0-pre.1 (c (n "der_derive") (v "0.6.0-pre.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0773pd6apgcwgwy8qn2lnq4b4aq05dv2rxbckbmiw1bfnaqy70s5") (r "1.56")))

(define-public crate-der_derive-0.6.0-pre.2 (c (n "der_derive") (v "0.6.0-pre.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h4q3xailsdqzij8dlsrspx20qdyv4dzlrn8n45wzsvl126jp77k") (r "1.56")))

(define-public crate-der_derive-0.6.0-pre.3 (c (n "der_derive") (v "0.6.0-pre.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0iibv1rxqxzzyl8gmmskm1kwxrgqq6qd19ys0w4pgh5p8qrigka5") (r "1.56")))

(define-public crate-der_derive-0.6.0-pre.4 (c (n "der_derive") (v "0.6.0-pre.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ncbfid810axs01a749ri5chlsshnncjjraah3z1xkgcrw3qg0h5") (r "1.56")))

(define-public crate-der_derive-0.6.0 (c (n "der_derive") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "046r88psv21cq5b4kgqx0520ilvw5j2k5ni6vv12rpafh8jhj3hq") (r "1.56")))

(define-public crate-der_derive-0.6.1 (c (n "der_derive") (v "0.6.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fg3dv4cjjwd4a6dh62ch2gb477s1pvwh5s8wbg567rsbgdivxwf") (r "1.56")))

(define-public crate-der_derive-0.7.0 (c (n "der_derive") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r4hd25xznsl9xqb5a9dzgpqf45y12bvf3wr652m0fj5sm3q92b3") (r "1.65")))

(define-public crate-der_derive-0.7.1 (c (n "der_derive") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cmyza28s52wfb67ymydjmvsc4m3sfp98dv9vprx6ibmdfx94iqi") (r "1.65")))

(define-public crate-der_derive-0.7.2 (c (n "der_derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jg0y3k46bpygwc5cqha07axz5sdnsx5116g3nxf0rwrabj7rs2z") (r "1.65")))

(define-public crate-der_derive-0.8.0-pre.0 (c (n "der_derive") (v "0.8.0-pre.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rlwz2gxd4jkfkrq53gcd4dhmr7s44l6sm4gqxnqfy63i9vyj7nx") (r "1.65")))

