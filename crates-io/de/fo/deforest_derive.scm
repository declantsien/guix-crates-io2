(define-module (crates-io de fo deforest_derive) #:use-module (crates-io))

(define-public crate-deforest_derive-0.1.0 (c (n "deforest_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "13pibzh376201nh8bh69vw2dfp2p9mbk7jcgmb35rbln5l3dkfgm") (r "1.70")))

(define-public crate-deforest_derive-0.3.0 (c (n "deforest_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "010kspjk1cnamcfb998ngq564216d1lbxrfjwlkddl5qc6rd458y") (r "1.70")))

