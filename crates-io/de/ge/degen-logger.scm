(define-module (crates-io de ge degen-logger) #:use-module (crates-io))

(define-public crate-degen-logger-0.1.0 (c (n "degen-logger") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "04rpg6drwy02glifcgkfh99skg2d1jrr4lcy9m0af1j8csw07fww")))

(define-public crate-degen-logger-0.2.0 (c (n "degen-logger") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1cbi4b5mzxriqwckr86vnpxn3kzbc9vj149k787s3mxgq0ncp4b8")))

