(define-module (crates-io de vd devd-rs) #:use-module (crates-io))

(define-public crate-devd-rs-0.1.0 (c (n "devd-rs") (v "0.1.0") (d (list (d (n "nix") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0x0xsd4dx3dvdxi7iqkff7c50r1rg4dnrnyb464xpl0badsxbyvk")))

(define-public crate-devd-rs-0.2.0 (c (n "devd-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "156j5w7phkdijm6dcagr0w7a22hwsmzv6slyg3jkxi7wml9d5wx5")))

(define-public crate-devd-rs-0.2.1 (c (n "devd-rs") (v "0.2.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0k23q8a0vm6r96i7jqqv3m4smpshd2hf8cmpsc0g9fiq3i4arjg7")))

(define-public crate-devd-rs-0.3.0 (c (n "devd-rs") (v "0.3.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "19sjzr7kdsqxs0r3s5asz2vilhd6ncq1cxfw16crz7hddhb9y00d")))

(define-public crate-devd-rs-0.3.1 (c (n "devd-rs") (v "0.3.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1qfc6bcdnsjlbwr9hma2ny10a7pv83bylrkp6jwxzazdravwqi8r")))

(define-public crate-devd-rs-0.3.2 (c (n "devd-rs") (v "0.3.2") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "0v8ys4453xy78m84rmiz8dmxx341w2y7mzsr6sm2s2375izky727")))

(define-public crate-devd-rs-0.3.3 (c (n "devd-rs") (v "0.3.3") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)))) (h "1rq5yca930kh98xab2rrvxpz3wp4rdg1liy7ypx4j3pnyjcmmrad")))

(define-public crate-devd-rs-0.3.4 (c (n "devd-rs") (v "0.3.4") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)))) (h "116zqhmdsh3lb1m329wdbkblwxfmcb2wvbdvgcpx6j2ibw8s8qcq")))

(define-public crate-devd-rs-0.3.5 (c (n "devd-rs") (v "0.3.5") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)))) (h "031n8amnir06iybk7473d8vr3x5xmf5c4c5hj4qflspjws7mnc8c")))

(define-public crate-devd-rs-0.3.6 (c (n "devd-rs") (v "0.3.6") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)))) (h "15fkr028silhhk2d6m0lhw9mql3nzhja7h01zi30nlchnl2g24wk")))

