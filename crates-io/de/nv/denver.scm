(define-module (crates-io de nv denver) #:use-module (crates-io))

(define-public crate-denver-0.1.0 (c (n "denver") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "00s9bsy5si14rhky5pkf25c203i08hgsd3g8894a9zqq6mjw35jq")))

(define-public crate-denver-0.2.0 (c (n "denver") (v "0.2.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "05r06kg8nlc5bbrhj2x2b387j1gdy0pszjhchkglgn5ag3bamyjg")))

(define-public crate-denver-0.2.1 (c (n "denver") (v "0.2.1") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1p8b4p936gr9jm7cixgjsbivy10b30snfxdf64xd3fm34yzyhzrv")))

(define-public crate-denver-0.3.0 (c (n "denver") (v "0.3.0") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.22.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "11ln7ka2gl24k55l6g6d56whvwd4qns9nl6z0c3m57cwfxq4cdcg")))

(define-public crate-denver-0.3.1 (c (n "denver") (v "0.3.1") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.22.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0cmcfcffnfrcqsshc636mjhzmjq6yfyv9ny1qs42ggkdap8m4gxr")))

