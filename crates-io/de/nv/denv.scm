(define-module (crates-io de nv denv) #:use-module (crates-io))

(define-public crate-denv-0.2.0 (c (n "denv") (v "0.2.0") (d (list (d (n "exec") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "17fm65p7674wvch6nicifl9a7bidypbs4rr9qg3li15pfmq746bb")))

(define-public crate-denv-0.3.0 (c (n "denv") (v "0.3.0") (d (list (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vc7p0mi38iq3v4mww3ywkda8xml5g5pv7mx4sblh08hvrjg60w0")))

