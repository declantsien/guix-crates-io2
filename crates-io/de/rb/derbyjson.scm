(define-module (crates-io de rb derbyjson) #:use-module (crates-io))

(define-public crate-derbyjson-0.0.1 (c (n "derbyjson") (v "0.0.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.3") (d #t) (k 0)))) (h "153hmq7iknvvx1sm0b4lzcgs15cqxqdji7cwhn1sqch9v307hyrh")))

(define-public crate-derbyjson-0.0.2 (c (n "derbyjson") (v "0.0.2") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.3") (d #t) (k 0)))) (h "06qd468n95n0vchqgrp2nkj9bkn2806sjqqzv9kbdih8vz40wlk0")))

(define-public crate-derbyjson-0.0.3 (c (n "derbyjson") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hq48jzl7ghl4nqrvpnk3bsp2wnrad5hw7jpib61a9jvavr1m37r")))

