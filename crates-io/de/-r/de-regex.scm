(define-module (crates-io de -r de-regex) #:use-module (crates-io))

(define-public crate-de-regex-0.1.0 (c (n "de-regex") (v "0.1.0") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fsf054m4vdg28m7dc2c88vwcwb854n3a8clm89i2yh4n9vmr5v7")))

