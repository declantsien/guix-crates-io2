(define-module (crates-io de ve develop_debug) #:use-module (crates-io))

(define-public crate-develop_debug-0.0.0 (c (n "develop_debug") (v "0.0.0") (h "138kc15z4zkfrvzidn8wclkkp7c5wgzyd65i1kc7yj8dwp5kn5ms")))

(define-public crate-develop_debug-0.1.0 (c (n "develop_debug") (v "0.1.0") (h "0gdayvgri3q3lm16p33kv2rbf8xlzlf5fpddlvp8c9h715s5gdxx") (f (quote (("default") ("debug"))))))

(define-public crate-develop_debug-0.1.1 (c (n "develop_debug") (v "0.1.1") (h "06532wx3qfbsyzi1zrmwk9qxf0g04d0x8mxzgk3axmcch4dibkxi") (f (quote (("default") ("debug"))))))

(define-public crate-develop_debug-0.1.2 (c (n "develop_debug") (v "0.1.2") (h "09998r6i8mgvappgyd7nvmj3k1gn6plpc7xw1yz3gv060vr5z1yf") (f (quote (("default") ("debug"))))))

(define-public crate-develop_debug-0.2.0 (c (n "develop_debug") (v "0.2.0") (h "1xw395g7cn6017czlwzsr6hig1gsd07pd1zbyihlw98cc8l96wna") (y #t)))

(define-public crate-develop_debug-0.2.2 (c (n "develop_debug") (v "0.2.2") (h "0xpzhs8g2jwyv77icbdr51gaphdq3ghdg370zi4knr6fsnlv2y7b") (y #t)))

(define-public crate-develop_debug-0.2.3 (c (n "develop_debug") (v "0.2.3") (h "1b792895y3d3z3xy1s8axyl4fqb3pyryf19hhi63985fpkd9rizs") (y #t)))

(define-public crate-develop_debug-0.2.4 (c (n "develop_debug") (v "0.2.4") (h "1q81gp5va8532phiy5a4h3khykacrplwbqmn458dsdva1s0ba68b")))

(define-public crate-develop_debug-0.2.5 (c (n "develop_debug") (v "0.2.5") (h "1m5i1gzi3rli13dd4ljssxn8m10964kbrz5bnhg6np49skksvqkx")))

(define-public crate-develop_debug-0.2.6 (c (n "develop_debug") (v "0.2.6") (h "1f7cjzcamfhmqxpavddf7g9183gj2whw6v35fi3ag5fvvq7liq6g")))

(define-public crate-develop_debug-0.3.1 (c (n "develop_debug") (v "0.3.1") (h "0rgjs3kaqd0slx36cj4z56039cqyz5al3ysfnzgi4smshwxz492j")))

(define-public crate-develop_debug-0.4.1 (c (n "develop_debug") (v "0.4.1") (h "1air0nhzcslzjw2h6vsz7qk5p3z6hzid2d26x9vvl76hdjmfiqw5")))

(define-public crate-develop_debug-0.5.0 (c (n "develop_debug") (v "0.5.0") (h "0h4jn6yqhjk3lc9vx71dcm2icydy29r40fa9mh8qqbpi8qfs7gji")))

(define-public crate-develop_debug-0.6.0 (c (n "develop_debug") (v "0.6.0") (h "1kqg736x41jvqqvl8vmlgrp3hmy3h9x5q1hdqz9la76ibsp94x0v") (y #t)))

(define-public crate-develop_debug-0.6.1 (c (n "develop_debug") (v "0.6.1") (h "1zrmk3dywhsqlzqb6gpfzzg545pv8y5gvvsds1ss57fxsnxxhagy")))

