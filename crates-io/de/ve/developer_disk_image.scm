(define-module (crates-io de ve developer_disk_image) #:use-module (crates-io))

(define-public crate-developer_disk_image-0.1.0 (c (n "developer_disk_image") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ngl9qwgbwbcfb961vyvvcmncjlfq7nr25z8ij3d4mh8wxzcaaw8")))

