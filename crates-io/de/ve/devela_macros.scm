(define-module (crates-io de ve devela_macros) #:use-module (crates-io))

(define-public crate-devela_macros-0.1.0 (c (n "devela_macros") (v "0.1.0") (h "11ybi4kwbs5h0391jx4034xmckxghllgwvmqbhlcgsj9209wh9l6") (r "1.56.1")))

(define-public crate-devela_macros-0.2.0 (c (n "devela_macros") (v "0.2.0") (h "10izr2ka09in07j97yzdd8vcmnxgld3ikr54mpr9sbxjv8h0qkqj") (r "1.56.1")))

(define-public crate-devela_macros-0.3.0 (c (n "devela_macros") (v "0.3.0") (h "1hb0fx6h4a9x3hd6xn8q7r4kv38psca831xbvbkkdwxrcma5wb94") (r "1.56.1")))

(define-public crate-devela_macros-0.3.1 (c (n "devela_macros") (v "0.3.1") (h "1hvnxkmp2dfl53z7cfd10mxb08i4z3s8rpq7dy76qzplk0j2ffvl") (r "1.56.1")))

(define-public crate-devela_macros-0.4.0 (c (n "devela_macros") (v "0.4.0") (h "1g12dsi192pgsckkk0jnn2cqf47wnbd3c6l5266lsgbp096gvb56") (r "1.60.0")))

(define-public crate-devela_macros-0.5.0 (c (n "devela_macros") (v "0.5.0") (h "0mvwibkazwszqi5v7r2zf4nn70igaqwi2s4ss8zi5nfwws4hgfiw") (f (quote (("nightly_docs")))) (r "1.60.0")))

(define-public crate-devela_macros-0.6.0 (c (n "devela_macros") (v "0.6.0") (h "0zc9qk7d4j9x7bj7mlwq20iha07mq1iq461m022l74i5llfk6pvp") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("nightly_docs" "nightly" "alloc") ("nightly") ("default" "alloc" "safe") ("alloc")))) (r "1.60.0")))

(define-public crate-devela_macros-0.6.1 (c (n "devela_macros") (v "0.6.1") (h "0xylpqqw1pmbz03qfai06qn5nwk8dj2psbiry88v616kmq72kzpl") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("nightly_docs" "nightly" "alloc") ("nightly") ("default" "alloc" "safe") ("alloc")))) (r "1.60.0")))

(define-public crate-devela_macros-0.7.0 (c (n "devela_macros") (v "0.7.0") (h "1zvfk2srx6qvywg70ngzqrfg40kyh8k655yv8k9i9s9i0md66hnq") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("nightly_docs" "nightly" "std" "unsafe") ("nightly") ("default" "alloc" "safe") ("alloc")))) (r "1.72.1")))

(define-public crate-devela_macros-0.7.1 (c (n "devela_macros") (v "0.7.1") (h "0qlb7mpp54qmmx4ijz1pz7y4rwxhp0vvzx0k4ww97cazz2r0ir1s") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("nightly_docs" "nightly" "std" "unsafe") ("nightly") ("default" "alloc" "safe") ("alloc")))) (r "1.72.1")))

(define-public crate-devela_macros-0.8.0 (c (n "devela_macros") (v "0.8.0") (h "007lpyjfnykiv6wnbw2yr3cb2ynzy13in5vaqxhq37z3lp7nlarr") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("nightly_docs" "nightly" "std" "unsafe") ("nightly") ("default" "alloc" "safe") ("alloc")))) (r "1.72.1")))

(define-public crate-devela_macros-0.9.0 (c (n "devela_macros") (v "0.9.0") (h "0lr3yj8pn5kf2b2bpamzz3d0dfcb644dzviiyap3pwcihkpy1z9j") (f (quote (("unsafe") ("std" "alloc") ("safe") ("nightly") ("docsrs" "nightly" "std" "unsafe") ("default" "std" "safe") ("alloc")))) (r "1.72.1")))

(define-public crate-devela_macros-0.10.0 (c (n "devela_macros") (v "0.10.0") (h "0gr31wfqjjnh7h16bdwjqj9gb56ykkds82s1i342853ign3088fr") (f (quote (("unsafe") ("std" "alloc") ("safe") ("nightly") ("docsrs" "nightly" "std" "unsafe") ("default" "std" "safe") ("alloc")))) (r "1.72.1")))

