(define-module (crates-io de ve devenv) #:use-module (crates-io))

(define-public crate-devenv-0.1.0 (c (n "devenv") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cached") (r "^0.34.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "paris") (r "^1.5.13") (f (quote ("macros" "no_logger"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "1r7r5kpc658s8g6hz2v51r2jm492apidwyyzp5qvixksjzlnw6rr")))

