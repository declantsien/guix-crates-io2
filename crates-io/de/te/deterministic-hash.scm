(define-module (crates-io de te deterministic-hash) #:use-module (crates-io))

(define-public crate-deterministic-hash-1.0.0 (c (n "deterministic-hash") (v "1.0.0") (d (list (d (n "crc") (r "^1.8") (k 2)))) (h "0qrbapsw979x95wx572v0rhk8vzdymdcklzda8c8xwslvrrf631y")))

(define-public crate-deterministic-hash-1.0.1 (c (n "deterministic-hash") (v "1.0.1") (d (list (d (n "crc") (r "^1.8") (k 2)))) (h "1w4kispqahsycqizsffk36lfaz6cdh0w6vvh17vl7pwycd7g7aai")))

