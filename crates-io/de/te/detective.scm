(define-module (crates-io de te detective) #:use-module (crates-io))

(define-public crate-detective-0.1.0 (c (n "detective") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d191gg4s9y5hhw7mjw3jqxmq9602nard17bh2ifb3z6p9nmpc7j")))

(define-public crate-detective-0.2.0 (c (n "detective") (v "0.2.0") (d (list (d (n "pty-process") (r "^0.4.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "06dgx35qdhfifd5kq0gw053scnmwq7f0kfvcc0q0swva5xgz72k8")))

(define-public crate-detective-0.3.0 (c (n "detective") (v "0.3.0") (d (list (d (n "pty-process") (r "^0.4.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "0bfyiayqj1sraxz4v8nm65am65yl271bnqnf6mkbsmsznp3cw8f8")))

