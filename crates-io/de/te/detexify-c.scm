(define-module (crates-io de te detexify-c) #:use-module (crates-io))

(define-public crate-detexify-c-0.4.0 (c (n "detexify-c") (v "0.4.0") (d (list (d (n "cbindgen") (r "^0.18.0") (d #t) (k 1)) (d (n "detexify") (r "^0.4.0") (d #t) (k 0)))) (h "0l61aaqj0l1rc93csywcxr95nwxghcmba3khcc3wryhzvgmfdlhd")))

