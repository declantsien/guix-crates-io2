(define-module (crates-io de te detect-lang) #:use-module (crates-io))

(define-public crate-detect-lang-0.1.0 (c (n "detect-lang") (v "0.1.0") (h "1mwh7sz2xchgl0v07jp1dchrpqzlxgfg7b1j2ibssl5nwkdz4r29")))

(define-public crate-detect-lang-0.1.1 (c (n "detect-lang") (v "0.1.1") (h "1mp6pzxcq9gi2ibbw10g132im3wdi5xcv1s8q4pmry417w3yfyrz")))

(define-public crate-detect-lang-0.1.2 (c (n "detect-lang") (v "0.1.2") (h "1lc2sjrp2h5nljpcv7vrxls32vmz2gngr53cs4x1pci73wniv0l4")))

(define-public crate-detect-lang-0.1.3 (c (n "detect-lang") (v "0.1.3") (h "19r08nl3jscygqlr6zlfdkr5ql195adz5r3jhgv2kmnfv5yfxr5g") (y #t)))

(define-public crate-detect-lang-0.1.4 (c (n "detect-lang") (v "0.1.4") (h "100gmg5wqgr1lwc2xzai8qpnm3zq6byzb2kw4az047wxdnawg73q")))

(define-public crate-detect-lang-0.1.5 (c (n "detect-lang") (v "0.1.5") (h "1b60z6xyy2s44nd6pxdxbfnnmap0iagpshf9520yy572p9m381q5")))

