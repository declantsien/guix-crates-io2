(define-module (crates-io de te deterministic-keygen) #:use-module (crates-io))

(define-public crate-deterministic-keygen-0.0.3 (c (n "deterministic-keygen") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.10.0-pre.1") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "08qjvj8r5nbll5k60hpx9sax0rvfppij4plk65cmwdalvmddlb7k") (y #t)))

(define-public crate-deterministic-keygen-0.0.5 (c (n "deterministic-keygen") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.10.0-pre.1") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "05lix3fxqh3wra29dc4llsf3vmxpfkhm87mhj7ibajb132xgfanc") (y #t)))

(define-public crate-deterministic-keygen-0.0.6 (c (n "deterministic-keygen") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsa") (r "^0.10.0-pre.1") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)))) (h "1v3ipwqgkaw3jzavfd9njkwm1fx0algmz25vqkpnk5dylf4nihr0")))

