(define-module (crates-io de te deterministic-finite-automaton) #:use-module (crates-io))

(define-public crate-deterministic-finite-automaton-0.1.0 (c (n "deterministic-finite-automaton") (v "0.1.0") (h "1cad02ha8fd568rn5l6achc9p5apb009is37ff6w8skqk7ybhpn5")))

(define-public crate-deterministic-finite-automaton-0.1.1 (c (n "deterministic-finite-automaton") (v "0.1.1") (h "0gcz6idmgv6asxfav5q4w3312cp007nvljrysckb170wnqzw8hfg")))

