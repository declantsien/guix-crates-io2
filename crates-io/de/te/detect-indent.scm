(define-module (crates-io de te detect-indent) #:use-module (crates-io))

(define-public crate-detect-indent-0.1.0 (c (n "detect-indent") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06rdr0ihdad037cgwcxz3dbwc0b5ps5bwr5gig6bli2ynxkiiqcs")))

