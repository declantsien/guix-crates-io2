(define-module (crates-io de te detect_flake) #:use-module (crates-io))

(define-public crate-detect_flake-0.1.0 (c (n "detect_flake") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1arhp50rz4pp8h61l1ylqsz06w1rkqzpllmpl4y4rhpzapyglfyr")))

(define-public crate-detect_flake-0.1.1 (c (n "detect_flake") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0d7xdd3729lp42vkmy6nc4vjm191k5mnkyana9bi9zsysrlhqh5z")))

(define-public crate-detect_flake-0.2.0 (c (n "detect_flake") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0a6ksc11cbmgi0l0vpz2dikh5f3mp6w098irdynbn4agsmld4yq1")))

(define-public crate-detect_flake-0.2.1 (c (n "detect_flake") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1qahvravsnfldvj249aigsjw8i01xmr5kfann79ks521x4p9rkmg")))

(define-public crate-detect_flake-0.3.0 (c (n "detect_flake") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0dzcyajgcz4ky09ch70v1f6pqqgwhv3q4vnsfvsb8ix0v02369k6")))

(define-public crate-detect_flake-0.3.1 (c (n "detect_flake") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "18k08pvfpj6jwc5445fyzpk1ikg1zqjhlq5h9psfk6yv98hlx3yk")))

(define-public crate-detect_flake-0.3.2 (c (n "detect_flake") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1lvva8y9il7vqmnjj4qhp9ckw9qwmd3pi0w0sh6skkq1jcfgqqmd")))

(define-public crate-detect_flake-0.4.0 (c (n "detect_flake") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "101035g2hww01mi99i3ns0w1clz55a1aqj4awjik8zmrfillmb5m")))

(define-public crate-detect_flake-0.4.1 (c (n "detect_flake") (v "0.4.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "0grjyifbarspjhb3skpskk2liih2zdcd8a0m26q913q094hn36l2")))

(define-public crate-detect_flake-0.5.0 (c (n "detect_flake") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "060jjby1sh2g89i0v3d89vvvr8gy64v7ixgia53ywpsnsj6vf93k")))

(define-public crate-detect_flake-0.5.1 (c (n "detect_flake") (v "0.5.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0asgjxm48raqmx33l2jf2lcx4ixhyxhlrv2lqglfkpcwwpqb5wcn")))

(define-public crate-detect_flake-0.5.2 (c (n "detect_flake") (v "0.5.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "040wbgxdrk4hsd6l2ysqswxsnhgr42fmmjiqy60nmq9h5rkfvmqs")))

(define-public crate-detect_flake-0.5.3 (c (n "detect_flake") (v "0.5.3") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)))) (h "1fh22xc57vad7q101a6q09khwzkzfvryd2qq6ralsjsv46wxwfsm")))

(define-public crate-detect_flake-0.5.4 (c (n "detect_flake") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1lp1cw4y07v9mqk79bhx5804acwvd7sq7y1nlrl249mx32mq7yj0")))

(define-public crate-detect_flake-0.5.5 (c (n "detect_flake") (v "0.5.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1m5a5kkfnkxzrnmrr3mlm3xplapghcrlkd51nj2i0crkkbf2hjrv")))

(define-public crate-detect_flake-0.5.6 (c (n "detect_flake") (v "0.5.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1iqz9qdn9dwi1xgnhcyypsn9l5slvsgxz2cb333b3v406c9ais1z")))

(define-public crate-detect_flake-0.5.7 (c (n "detect_flake") (v "0.5.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "13n9pc8a4rh4z8d80khz1n9g7hi39zs6fipd4ghjsj5gc4xw0j0z")))

(define-public crate-detect_flake-0.6.0 (c (n "detect_flake") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "1yqp4357ww0fpv5i4zsga34wfmqpdbkn2kakpwf5lpg31582bns8")))

