(define-module (crates-io de te detexify-server) #:use-module (crates-io))

(define-public crate-detexify-server-0.1.0 (c (n "detexify-server") (v "0.1.0") (d (list (d (n "detexify") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m1cijz4yjd1l136lz59rpsx6gm1dh63nc5p1z2k3i7739vxg8r1")))

(define-public crate-detexify-server-0.2.0 (c (n "detexify-server") (v "0.2.0") (d (list (d (n "detexify") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iq5vl9z8dqj38ipharv757xh6x3cdysra3djn39vxzkkfnl3swn")))

(define-public crate-detexify-server-0.4.0 (c (n "detexify-server") (v "0.4.0") (d (list (d (n "detexify") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ma8wzdmbfmc1mqdrynfl8gk96199sizj62923m486rxf6x5m0ll")))

