(define-module (crates-io de te deterministic) #:use-module (crates-io))

(define-public crate-deterministic-0.0.1 (c (n "deterministic") (v "0.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0j267zac8nwqlmh2hvri19inid03xm888prbswi4m136zw8w5g1b")))

(define-public crate-deterministic-0.0.2 (c (n "deterministic") (v "0.0.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1bpjhva6bcalnjam300p4m54kmk5am6qd1qch6j46wxf8b7mlxvq")))

(define-public crate-deterministic-0.0.3 (c (n "deterministic") (v "0.0.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "17vbd6868apklc4pbyld0liiw3gjxcmhjr24hxq6v94lfagdmci2")))

(define-public crate-deterministic-0.0.4 (c (n "deterministic") (v "0.0.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kvhcz7vkgf9lzhx6g0hxq0yf110xk65710famn6ci6m73bdfh28")))

(define-public crate-deterministic-0.0.5 (c (n "deterministic") (v "0.0.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01nbmsa6s9zwpj2m6iq4snc97fkc9xcsjn3z4xs8wff9610f3g1w")))

(define-public crate-deterministic-0.0.6 (c (n "deterministic") (v "0.0.6") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g1zm1pxd6jk4f5xiagh6ggg4pq9l6j7zl8gz68mkb6lkmi1ssal")))

(define-public crate-deterministic-0.0.7 (c (n "deterministic") (v "0.0.7") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xvz0dyccj41y79r55wkpbxxqdk0cp7nwg7cvim9ss3a0rnhi1k8")))

(define-public crate-deterministic-0.0.8 (c (n "deterministic") (v "0.0.8") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1k81ya909zv0rbrjqciainv00wn0b6skhgn9axsjcimj5cq2d8fq")))

(define-public crate-deterministic-0.0.9 (c (n "deterministic") (v "0.0.9") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0by71cbwfx6mrq0nhvlc3gilvf5bvqcg0zd2qixcvdxhm779svpw")))

(define-public crate-deterministic-0.1.0 (c (n "deterministic") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18rgpymm4dgxmhzbgajwlnl8kk9c5an1nfcfwfvg0hy1nn0qchcf")))

(define-public crate-deterministic-0.1.2 (c (n "deterministic") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1140ai6g8sr4y93jncnh0kqld8wchi88pydbdivhvlpgmkqz6b09")))

