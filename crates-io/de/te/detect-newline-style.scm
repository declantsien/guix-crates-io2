(define-module (crates-io de te detect-newline-style) #:use-module (crates-io))

(define-public crate-detect-newline-style-0.1.0 (c (n "detect-newline-style") (v "0.1.0") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1m7szvkbnfszy8a7anjdj22c4362swacma9km90794g5ndrcf1qx")))

(define-public crate-detect-newline-style-0.1.1 (c (n "detect-newline-style") (v "0.1.1") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0dbrkmamblrzpnbkbfyffyhjilh7hxjp1c2j55b1xxa796pz1ldm")))

(define-public crate-detect-newline-style-0.1.2 (c (n "detect-newline-style") (v "0.1.2") (d (list (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0j9pcjk2ab21f36fqybz69whd1c4xy60hy7qd5v59aqm6rfg490i")))

