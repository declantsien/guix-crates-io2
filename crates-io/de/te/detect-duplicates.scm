(define-module (crates-io de te detect-duplicates) #:use-module (crates-io))

(define-public crate-detect-duplicates-0.1.0 (c (n "detect-duplicates") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ip04q2hl6bpnplja117k7afhf5001ny4gx709mwxy25sx11faj")))

(define-public crate-detect-duplicates-0.1.1 (c (n "detect-duplicates") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l799969s88s38q2sy67y0332vimkviz1xxhsg8jdap3xdrvilm3")))

(define-public crate-detect-duplicates-0.1.2 (c (n "detect-duplicates") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ciyx8bmzj214q6imqfj49k99ccc62a18w99slimrll72hz0jan")))

(define-public crate-detect-duplicates-0.1.3 (c (n "detect-duplicates") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0di1m326shxb4kjf9v4jwvgrliczqvyi7jbf52pqslsjy6sfkid0")))

(define-public crate-detect-duplicates-0.1.4 (c (n "detect-duplicates") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bknm91y45kz0y56ak9x7k4z1s5ffc78ppmx0r1ki0d1dcabym83")))

(define-public crate-detect-duplicates-0.2.0 (c (n "detect-duplicates") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "05s98v1p4cqhjmm3gjlyim21gp0mkmh7j03m0mclxksm8g8yy25c")))

(define-public crate-detect-duplicates-0.2.1 (c (n "detect-duplicates") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r5nl32f202ry7c050ma8jy0hsl2fkb8l47r0r292kb59zrq4mdn")))

