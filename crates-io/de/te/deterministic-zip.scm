(define-module (crates-io de te deterministic-zip) #:use-module (crates-io))

(define-public crate-deterministic-zip-0.0.1 (c (n "deterministic-zip") (v "0.0.1") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (f (quote ("deflate" "bzip2"))) (d #t) (k 0)))) (h "1mbnb2q1f5q7l2f09qapbbp09myf8mbifaa8l2n4fx7xs0lsw50k")))

(define-public crate-deterministic-zip-0.0.2 (c (n "deterministic-zip") (v "0.0.2") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)) (d (n "zip") (r "^0.5.3") (f (quote ("deflate" "bzip2"))) (d #t) (k 0)))) (h "18ffgbq8nlqjip19kj091k9c0hpf5pmqd8mz3ycziw8dl7f2kvg7")))

(define-public crate-deterministic-zip-0.0.3 (c (n "deterministic-zip") (v "0.0.3") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (f (quote ("deflate" "bzip2"))) (d #t) (k 0)))) (h "18q0kcx9ip29zw1fnmnr3pyja7rxi0bs048xf2ns1c7hhfqbmig4")))

(define-public crate-deterministic-zip-0.0.4 (c (n "deterministic-zip") (v "0.0.4") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (f (quote ("deflate" "bzip2"))) (d #t) (k 0)))) (h "0bcv17xlzlfilcwn74pkslv80pqvrz9jzdafp7mi3ysjyzdr9lzb")))

