(define-module (crates-io de te detect-compression) #:use-module (crates-io))

(define-public crate-detect-compression-0.1.0 (c (n "detect-compression") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 0)))) (h "0jirfkw7z1wy88pizghbavjgxjm6k9ngh8rkszx5iinc075aq5s6")))

(define-public crate-detect-compression-0.1.1 (c (n "detect-compression") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 0)))) (h "1rnrkj2llvh1ki8my07cz7i28akvs363hf7c18l87vkix52iarzn")))

(define-public crate-detect-compression-0.1.2 (c (n "detect-compression") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 0)))) (h "1w393r3hd7v7p9dx4hvv3nyybh6zn7qsjzxzixnm4x2llin1m7ba")))

