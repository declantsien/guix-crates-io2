(define-module (crates-io de te detecteff) #:use-module (crates-io))

(define-public crate-detecteff-0.1.0 (c (n "detecteff") (v "0.1.0") (d (list (d (n "argrust") (r "^0.1.0") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "07c7qps79c9vi2rxgqpln4yfq7hvy3hj1m29ngrfn5lja1fhdrrd")))

(define-public crate-detecteff-0.2.0 (c (n "detecteff") (v "0.2.0") (d (list (d (n "argrust") (r "^0.1.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "1fskb60y6mka2d5ram9fa7bg0lgbnni0z9dx9szkbrd9sfyix80y")))

(define-public crate-detecteff-0.3.0 (c (n "detecteff") (v "0.3.0") (d (list (d (n "argrust") (r "^0.1.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "1zim5ydnvhlwvhq76gcflqq8i1frz3frimi7891pgqzlh3bnvk4w")))

(define-public crate-detecteff-0.3.1 (c (n "detecteff") (v "0.3.1") (d (list (d (n "argrust") (r "^0.1.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "1rz9na6ar5nqc958nvyx7q4gb2xfa88xx6kn6lsd5n9wsrn26ila")))

