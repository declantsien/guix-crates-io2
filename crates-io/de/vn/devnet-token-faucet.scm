(define-module (crates-io de vn devnet-token-faucet) #:use-module (crates-io))

(define-public crate-devnet-token-faucet-0.1.0 (c (n "devnet-token-faucet") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.5") (d #t) (k 0)))) (h "0d5k6rmi23yp4lyx0rig8s6xk01x8kldd84z08gwy5n1mk7vqx42") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

