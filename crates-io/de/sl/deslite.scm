(define-module (crates-io de sl deslite) #:use-module (crates-io))

(define-public crate-deslite-0.1.0 (c (n "deslite") (v "0.1.0") (d (list (d (n "libsqlite3-sys") (r "^0.9.3") (d #t) (k 0)))) (h "1yn3pys2dvsrag54z16q5lq650jd227xn247v9acl3ci05l58vn5")))

(define-public crate-deslite-0.1.1 (c (n "deslite") (v "0.1.1") (d (list (d (n "libsqlite3-sys") (r "^0.13.0") (d #t) (k 0)))) (h "1662snfdv59srl9186c0ip5ybiw7x9vb1pir9hxh91bh9m4s38nd")))

