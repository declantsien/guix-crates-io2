(define-module (crates-io de wp dewpoint) #:use-module (crates-io))

(define-public crate-dewpoint-0.1.0 (c (n "dewpoint") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1zlsbv0ymn8a029640s5rw121mfs0srzpr25iv2dkk0vkjk3hhjj") (y #t)))

