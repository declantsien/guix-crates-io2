(define-module (crates-io de f- def-mod) #:use-module (crates-io))

(define-public crate-def-mod-0.1.0 (c (n "def-mod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1j68jh1f6b09x128p60dv5040jsdjsbfw8xqr427rsnn329l9slx")))

(define-public crate-def-mod-0.2.0 (c (n "def-mod") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1zl0ss0f04gc96l651i7xglsxpj6fpx1p417z562gkmn874pp4ig")))

(define-public crate-def-mod-0.3.0 (c (n "def-mod") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1hvd4z1wgh4vjbs2p8wv02gcsa97xl4q2zswywips0ipm2i0a6hd")))

(define-public crate-def-mod-0.4.0 (c (n "def-mod") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1wdg9kv86znksk56n58ybmhigf92yk4vh8s0vqx4q6nyyf12rshp")))

(define-public crate-def-mod-0.5.0 (c (n "def-mod") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1v44s6ljr7r35rp087fkv7dg56qydc1j3cphxgq998l6fd7jxi0y") (f (quote (("derive-debug" "syn/extra-traits") ("default"))))))

