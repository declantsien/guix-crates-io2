(define-module (crates-io de q_ deq_core) #:use-module (crates-io))

(define-public crate-deq_core-0.1.0 (c (n "deq_core") (v "0.1.0") (h "0iq87hqznc1ml3a7pilhykz6mv8bi18qvg1dsmqmnx29iiwnl5iv")))

(define-public crate-deq_core-0.1.1 (c (n "deq_core") (v "0.1.1") (h "1xqvjjhbn5fyigwj8gp1cnyrpfby5w0k9mfjcc12prm2dlmnl43l")))

(define-public crate-deq_core-0.1.2 (c (n "deq_core") (v "0.1.2") (h "1bgy9s2il25m524790ij3nwkgpkz327krv7hk6cm5fmzmb0hq417")))

(define-public crate-deq_core-0.1.5 (c (n "deq_core") (v "0.1.5") (h "0wnaglpgmq2s5azkm71xg7rlk1zf3kx18jkkhdbf4x7prcm30yb1")))

