(define-module (crates-io de pu depub) #:use-module (crates-io))

(define-public crate-depub-0.1.0 (c (n "depub") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "15zkykswg42f1wwl493gcwl6ik8cy0v2jiqrmfkb5q2ffmlh2wf6")))

(define-public crate-depub-0.1.1 (c (n "depub") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0y9rf1yxnq0jkbvyda3h6xw9dyap2j1d15y6dq8k3jclc5q6mlp2")))

