(define-module (crates-io de p- dep-expand) #:use-module (crates-io))

(define-public crate-dep-expand-0.1.0 (c (n "dep-expand") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "syn-select") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "009za7cykjh0ydnf70qm32gg8isf5sm5wc59yprj8pxykfg7qmvg")))

