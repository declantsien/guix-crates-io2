(define-module (crates-io de p- dep-inj-target) #:use-module (crates-io))

(define-public crate-dep-inj-target-0.1.0 (c (n "dep-inj-target") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1abfjlz5xl3pfdlfgqzyx01k4i0fdyaizpkl5q36ip4783nnnnz6")))

