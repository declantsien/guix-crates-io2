(define-module (crates-io de p- dep-check) #:use-module (crates-io))

(define-public crate-dep-check-0.1.0 (c (n "dep-check") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crates-index") (r "^0.12.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "105rv3h9g0m3n2wj11j9b8p0m21mhfikad5ba3vfzha3xhkqnd7l")))

