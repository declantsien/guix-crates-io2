(define-module (crates-io de p- dep-inj) #:use-module (crates-io))

(define-public crate-dep-inj-0.1.0 (c (n "dep-inj") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gp3x67bnq55wxk50nvx54yixayb5nw8ill892k9pxacii13mjdj")))

