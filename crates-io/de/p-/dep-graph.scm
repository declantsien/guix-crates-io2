(define-module (crates-io de p- dep-graph) #:use-module (crates-io))

(define-public crate-dep-graph-0.1.0 (c (n "dep-graph") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1r96lvgdzvgk5g6n6ghi6fc1523hakbkpmj5glb53h2z2wx8gnvw")))

(define-public crate-dep-graph-0.1.1 (c (n "dep-graph") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "190m0pqg1xvj6a9qji5iajd4x63niyg30z7bzq803jxikxz6mg5p")))

(define-public crate-dep-graph-0.1.2 (c (n "dep-graph") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1igizz44dlc183vj0qqzncq40pjqihqx87dvaj20740jr7rzf6xm")))

(define-public crate-dep-graph-0.1.3 (c (n "dep-graph") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "1mg8zbhqqh62q58bmj2knzj6kfpiribgzzsg59dyghad9036hdrq")))

(define-public crate-dep-graph-0.1.4 (c (n "dep-graph") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1i4ix9nac5xww1gddvlv135bk0lwpxgg897h9p329wxzhz127801") (f (quote (("default" "rayon"))))))

(define-public crate-dep-graph-0.1.5 (c (n "dep-graph") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0lric4ryj855qvsp5l67kr1wgxcwx7j282mzn53rcww1hg19mhmp") (f (quote (("default" "rayon"))))))

(define-public crate-dep-graph-0.1.6 (c (n "dep-graph") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1fkq0g75k3bd7s9bgkp8sxp8ngx92bf3v6i6ni30vic7wgxmg32h") (f (quote (("default" "rayon"))))))

(define-public crate-dep-graph-0.2.0 (c (n "dep-graph") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0xl3q9g221qf03is7gpmw9yqq0i2a6flqh56n8qsap2vp0kl5fzn") (f (quote (("parallel" "rayon" "crossbeam-channel") ("default" "parallel"))))))

