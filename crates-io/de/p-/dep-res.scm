(define-module (crates-io de p- dep-res) #:use-module (crates-io))

(define-public crate-dep-res-0.1.0 (c (n "dep-res") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tuples") (r "^1.12") (d #t) (k 0)))) (h "016qdmhqq9lw1scvrx4hsp43smxmnixlrlb2w6d7zpn3179g4vk2")))

