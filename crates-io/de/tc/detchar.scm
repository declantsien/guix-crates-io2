(define-module (crates-io de tc detchar) #:use-module (crates-io))

(define-public crate-detchar-0.1.0 (c (n "detchar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-release") (r "^0.20") (d #t) (k 2)) (d (n "chardetng") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jrp4fcx6p1klcbsn6n5qpv03j9rgz8vssdabvcmxqr7zx8890y0") (f (quote (("multithreading" "chardetng/multithreading"))))))

