(define-module (crates-io de vl devlog) #:use-module (crates-io))

(define-public crate-devlog-0.1.0 (c (n "devlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1mba596bl58yi4l9k94g1a5837ldi33x3mm35dmzzsssl9bfk122")))

(define-public crate-devlog-0.1.1 (c (n "devlog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0q2jammw8sq7b94bj3zpg0s693j583mzaxkcvzk2as27ns64y3xg")))

(define-public crate-devlog-0.2.0 (c (n "devlog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wwvjwxfy91kmjp7n054h7za3ipgq7cgc1cbbx0qi2623cxl8z56")))

(define-public crate-devlog-0.3.0 (c (n "devlog") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0r2waqlvwvizc54rddghgcrh3slc09gra9wxa5dm4k19c9cx2d3j")))

(define-public crate-devlog-1.0.0 (c (n "devlog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1isy161n48b69dsw6v0v01x43nncj5mj53ik2mm2753h4wmvyawk")))

(define-public crate-devlog-1.0.1 (c (n "devlog") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0f5i5ihjcifm123ayb382v09h02b0zrcq74a813a9icpwqxadwx9")))

(define-public crate-devlog-1.0.2 (c (n "devlog") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0ah0wmik65d0li5lnalqn5692a954ipfna3j81f2bfcygya06gqh")))

(define-public crate-devlog-1.0.3 (c (n "devlog") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0n1xpi8xn3mas7dpbbflxpiiliffz73iijjdd6q46gfrrxa7w5dx")))

(define-public crate-devlog-1.0.4 (c (n "devlog") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "16dhplcsbryvxv0smhljlmwkfcg7s5npkx19bf8br44pq5dm1735")))

(define-public crate-devlog-1.0.5 (c (n "devlog") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "04igll82g9xjk3b05dkys9fddir35nxb5iir1arqr8ldd6nn1431")))

(define-public crate-devlog-1.1.0 (c (n "devlog") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1bdg7ynp45871b181qa241w4xh77yr1cvb7plmx8jjdw8fdpifkf")))

(define-public crate-devlog-1.2.0 (c (n "devlog") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1z14h65rhn25a29j2v006qasgdx2901h6r58q1zb2p0gkmv18vc6")))

(define-public crate-devlog-1.2.1 (c (n "devlog") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1mnmkfc7wd14phaznshnwjcr18cfwci4s5wzbhgqbfm187bwckza")))

