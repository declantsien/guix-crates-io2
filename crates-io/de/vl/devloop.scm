(define-module (crates-io de vl devloop) #:use-module (crates-io))

(define-public crate-devloop-0.1.0 (c (n "devloop") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "1ghwnj3gmkahg2zpixxa1v5xhbf4pvah4yippfx46zzqf6kybwvf")))

(define-public crate-devloop-0.1.1 (c (n "devloop") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "1p1ijw6ynfsb73s6by97k27h18bpydfc7nyy02365pvv9fl7ip6v")))

(define-public crate-devloop-0.1.2 (c (n "devloop") (v "0.1.2") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "14bskkwggck7azasgzh36gv9mrakp2bi95rl4mz7riav2marlbiq")))

(define-public crate-devloop-0.1.3 (c (n "devloop") (v "0.1.3") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "0dbgash1mp5lw2ki3plcyc76wvpm5b9g4m4bzxbk2m7mgxa03m0z")))

(define-public crate-devloop-0.1.4 (c (n "devloop") (v "0.1.4") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "crossterm_terminal") (r "0.1.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "1yy6rckyps520wgcxb7qn87k7z10hviwmgwacb1cpqg4qivcgazg")))

(define-public crate-devloop-0.1.5 (c (n "devloop") (v "0.1.5") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "colored") (r "1.5.*") (d #t) (k 0)) (d (n "crossterm_terminal") (r "0.1.*") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "yaml-rust") (r "0.3.*") (d #t) (k 0)))) (h "0ysxdnr4b5c4kqaxyws5s57x2zcayw277hbknybnz6plcwx2h4kx")))

(define-public crate-devloop-0.2.0 (c (n "devloop") (v "0.2.0") (d (list (d (n "crossterm") (r "0.18.*") (d #t) (k 0)) (d (n "ctrlc") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0k3cph92hz9ydrkagl0i3h8kr9fr7p3sam5g7ag6dbmpd7zcvmhk")))

(define-public crate-devloop-0.2.1 (c (n "devloop") (v "0.2.1") (d (list (d (n "crossterm") (r "0.18.*") (d #t) (k 0)) (d (n "ctrlc") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1aridvnfhgpzravwm2m8hwbsphdaak37035lcq144i0c3s8b3cpg")))

(define-public crate-devloop-0.2.2 (c (n "devloop") (v "0.2.2") (d (list (d (n "crossterm") (r "0.18.*") (d #t) (k 0)) (d (n "ctrlc") (r "3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1lx9f7qr2vg5gnhcvbzhp1v3a7wn1g4d9zim0nk310vdjmq4dfbl")))

