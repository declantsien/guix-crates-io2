(define-module (crates-io de v_ dev_util) #:use-module (crates-io))

(define-public crate-dev_util-0.1.0 (c (n "dev_util") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "14s6bf3dbjdsn520j84jwi3jij2p0158i2pv8p32wkdh2iipga2y")))

(define-public crate-dev_util-0.1.1 (c (n "dev_util") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "03nxallnbnck76lj91z8z3ilipay7g2h0iajxqmmy94s8z39cac6")))

(define-public crate-dev_util-0.1.2 (c (n "dev_util") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1pni9f722xnjdk03khxc4zh069y4jm4b74jjgzk76pdi8sza1rq7")))

