(define-module (crates-io de v_ dev_bestia_url_utf8) #:use-module (crates-io))

(define-public crate-dev_bestia_url_utf8-0.1.26 (c (n "dev_bestia_url_utf8") (v "0.1.26") (d (list (d (n "dev_bestia_string_utils") (r "^0.1.19") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "15ckfcyf63qc2x1swqcabjmdw7528cnb9sl4m3dmkwcz81chqjnr")))

(define-public crate-dev_bestia_url_utf8-0.1.27 (c (n "dev_bestia_url_utf8") (v "0.1.27") (d (list (d (n "dev_bestia_string_utils") (r "^0.1.19") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1pk0y7br74iqv15hak6gx2cvaw4wyyibmlax7pcg5gv40bbh4rhy")))

(define-public crate-dev_bestia_url_utf8-0.1.28 (c (n "dev_bestia_url_utf8") (v "0.1.28") (d (list (d (n "dev_bestia_string_utils") (r "^0.1.19") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "05zzhywslsb1cg5y21ajm8jx4vjk7gm16l2mh9w4i3rpi2h4dq4w")))

