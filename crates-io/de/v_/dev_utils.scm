(define-module (crates-io de v_ dev_utils) #:use-module (crates-io))

(define-public crate-dev_utils-0.1.0 (c (n "dev_utils") (v "0.1.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0szzypm3bzjfrhlr0fjfrr5z90py2xfzmvvvaymj6fx91h87fc4s") (y #t)))

(define-public crate-dev_utils-0.0.2 (c (n "dev_utils") (v "0.0.2") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "1ck5ks0p76ylcnfrhydvzfbgbl30d56sbxpl428qi07nj2lmv8pr")))

(define-public crate-dev_utils-0.0.3 (c (n "dev_utils") (v "0.0.3") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0q992i197d89iq10q3nglmwdx53qnk5p3gnv194xyng03pari9pm")))

(define-public crate-dev_utils-0.0.4 (c (n "dev_utils") (v "0.0.4") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "1mad4szy2gc3zz0glr3ml2c70c6kcz7in8mag3x534yfx6nx5xz5")))

(define-public crate-dev_utils-0.0.5 (c (n "dev_utils") (v "0.0.5") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "11maw8ms6a5p8yd51nrfim3b7s4d61xpdc7fnlvblr4wj2vgj5sy")))

(define-public crate-dev_utils-0.0.6 (c (n "dev_utils") (v "0.0.6") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "1n1xf4brm14ir4170hkr5wvr5j50570dvajvycl2hl7hfsy5zlrf")))

(define-public crate-dev_utils-0.0.7 (c (n "dev_utils") (v "0.0.7") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "1jx847pp0kb29m6s6b7pl1mvvdjqr8ndgba74i7fc4c99y65hgyg")))

(define-public crate-dev_utils-0.0.8 (c (n "dev_utils") (v "0.0.8") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "1a1ag889wvbw4ml5lmgj5fb15wilm5an2zlwlh4nyyhvsdz1a919")))

(define-public crate-dev_utils-0.0.9 (c (n "dev_utils") (v "0.0.9") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "121jdrsxjp2hlf6xdg25sp2d75q13j4wd8qksir21sz7pgrac6y8")))

(define-public crate-dev_utils-0.0.10 (c (n "dev_utils") (v "0.0.10") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "158gykzqcwi2klpvsa0cvvk7q8ziz0yy6y1mvys6c3cbxl0180k6")))

(define-public crate-dev_utils-0.0.11 (c (n "dev_utils") (v "0.0.11") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0iglidzdyqqm54kyjg81wkbdj7243pbrl8gmal0dqiivsccmcx6i")))

(define-public crate-dev_utils-0.0.12 (c (n "dev_utils") (v "0.0.12") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0cjw8p183w7qk5hw6i5a3ymcniyrl705m76ny0hp0g4fbq08zf12") (y #t)))

(define-public crate-dev_utils-0.0.13 (c (n "dev_utils") (v "0.0.13") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "15473vhsmf7wg5rjsfxhv1i8hq07w684c0awsd5prnjykzl4nwcy")))

