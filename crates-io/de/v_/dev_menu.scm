(define-module (crates-io de v_ dev_menu) #:use-module (crates-io))

(define-public crate-dev_menu-0.1.0 (c (n "dev_menu") (v "0.1.0") (d (list (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "0.3.*") (d #t) (k 0)) (d (n "piston") (r "0.1.*") (d #t) (k 0)))) (h "1n220yl24n6giy14hhkjkcvgrcz0w1y19j194d70rbdj54h7apxl")))

(define-public crate-dev_menu-0.2.0 (c (n "dev_menu") (v "0.2.0") (d (list (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.1") (d #t) (k 0)))) (h "03hczq1593gq5a4r831hz0s4wdsp9afbz90wbysnp1j6jcxz3jab")))

(define-public crate-dev_menu-0.3.0 (c (n "dev_menu") (v "0.3.0") (d (list (d (n "gfx") (r "^0.7.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)))) (h "0pjr5qv089vkasvwvd6rrdkrhqxm0v208fj1swvgkq1a2jzhj665")))

(define-public crate-dev_menu-0.4.0 (c (n "dev_menu") (v "0.4.0") (d (list (d (n "gfx") (r "^0.7.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)))) (h "1pq0vw4857bq3zpnd8c9kp877icyl3h5ygbxz10wrmzzpz6g11l5")))

(define-public crate-dev_menu-0.5.0 (c (n "dev_menu") (v "0.5.0") (d (list (d (n "gfx") (r "^0.7.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)))) (h "05f3z1ks0m2bnr4kavw0krdqaa171d0pw0vk3h0gngzdnzf9g60b")))

(define-public crate-dev_menu-0.6.0 (c (n "dev_menu") (v "0.6.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.7.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)))) (h "1pgvvicwb90hg6j0bcclbhxwmbvb4xziph0nbwc0g211n7j0a61g")))

(define-public crate-dev_menu-0.7.0 (c (n "dev_menu") (v "0.7.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)))) (h "1x42dxnc48i45jj67k30rwg4j39v3kf5rilrnsxg5i5j5g14j2bz")))

(define-public crate-dev_menu-0.8.0 (c (n "dev_menu") (v "0.8.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.9.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)))) (h "12f4vnlwzmr172n6bvca9kdrhxvj2dqikzjarqmdlbdc9x69wdwb")))

(define-public crate-dev_menu-0.9.0 (c (n "dev_menu") (v "0.9.0") (d (list (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.11.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)))) (h "18bwv3br8xpzpvd64xn1kphshvw7rwy9gd61453vvlk30b2xg9i0")))

(define-public crate-dev_menu-0.9.1 (c (n "dev_menu") (v "0.9.1") (d (list (d (n "gfx") (r "^0.12.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.11.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)))) (h "0vb7954r4ylck6g4cmsq45x1y556pqzkfj88k6mkl73mb7hh3ams")))

(define-public crate-dev_menu-0.10.0 (c (n "dev_menu") (v "0.10.0") (d (list (d (n "gfx") (r "^0.12.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.11.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)))) (h "1snwxf6q54z1bxfsqd50nrlg0j95hsqk2ck2h84v6arw1vb0j0mr")))

(define-public crate-dev_menu-0.11.0 (c (n "dev_menu") (v "0.11.0") (d (list (d (n "gfx") (r "^0.13.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)))) (h "1jiqligxmb22gk76xva2g3f6r8bi8gy57rcmwxhcvx6mwwsn48fc")))

(define-public crate-dev_menu-0.12.0 (c (n "dev_menu") (v "0.12.0") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)))) (h "133j317wpwyvyd4nrkvcv6i1aw6mgvdyzajafpnq3z5rlfn7rvxk")))

(define-public crate-dev_menu-0.13.0 (c (n "dev_menu") (v "0.13.0") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)))) (h "0lzf4ng2haz2i4kd3a871zk1jx1r5jhjp0zslzldww3im6dcc8bl")))

(define-public crate-dev_menu-0.14.0 (c (n "dev_menu") (v "0.14.0") (d (list (d (n "gfx") (r "^0.15.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)))) (h "0n7i21d1w1z9b0scvxla16jymyjfgpqjrhqpi6rghji51h7ayyxf")))

(define-public crate-dev_menu-0.15.0 (c (n "dev_menu") (v "0.15.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.15.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)))) (h "04f57mmla3z9659kjrb6zr6gh7yc8rfikz3df9rknqi6fyy99zph")))

(define-public crate-dev_menu-0.16.0 (c (n "dev_menu") (v "0.16.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)))) (h "0sk0alkwszx5i0d53nv2m3ddazh940mqvb75wphz1q2laxbm8222")))

(define-public crate-dev_menu-0.17.0 (c (n "dev_menu") (v "0.17.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)))) (h "02kh2kdwwc3kwf1z72mb0vk5lpvm0vq1rw5svv729vlnjz9gg8fq")))

(define-public crate-dev_menu-0.18.0 (c (n "dev_menu") (v "0.18.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.18.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)))) (h "1d8nixfv2zp5z83v5a0sia8nwlyfr0ksmd8hgsxz9ik5cjrylarf")))

(define-public crate-dev_menu-0.19.0 (c (n "dev_menu") (v "0.19.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)))) (h "0zjqz5fh0y54wssmjb7jlrcksc9m464nnp0yq9wj89x06a089wqp")))

(define-public crate-dev_menu-0.20.0 (c (n "dev_menu") (v "0.20.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)))) (h "1mz5jcyy5ggzi3nca4ni6l1a62bvvix8cw9q52yb0la2kf513w1g")))

(define-public crate-dev_menu-0.21.0 (c (n "dev_menu") (v "0.21.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)))) (h "1xbrkpsp59h33ch56lhxjkkb737md2xay9ldj292rk295xs76z9p")))

(define-public crate-dev_menu-0.22.0 (c (n "dev_menu") (v "0.22.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)))) (h "16lls9kx6bg309sj1f4zav9zj5g0j7w12klvkv13fl3n5wsfhp21")))

(define-public crate-dev_menu-0.23.0 (c (n "dev_menu") (v "0.23.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)))) (h "08ljnna3px4g4jgmyx9qi8kjzkz02bwb3kdrb8d99k9n492d2j32")))

(define-public crate-dev_menu-0.24.0 (c (n "dev_menu") (v "0.24.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)))) (h "1g0svc16x28m1myffh68nxyihad4jcsxmk476f6513b48bd86r8y")))

(define-public crate-dev_menu-0.25.0 (c (n "dev_menu") (v "0.25.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)))) (h "1xp4wcysq74i183g1z2zqzph6vrn4d07rqm938q07fizk7cg0k26")))

(define-public crate-dev_menu-0.26.0 (c (n "dev_menu") (v "0.26.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)))) (h "1dym0pgq4sv57qcvhxfqmskfz5dj7glhgifrmwhc47gd2l7s8d4y")))

(define-public crate-dev_menu-0.27.0 (c (n "dev_menu") (v "0.27.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)))) (h "1bglvxshiirnj3124ki671v50972b9p4jwihqslh4y9fpric4vk5")))

(define-public crate-dev_menu-0.28.0 (c (n "dev_menu") (v "0.28.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)))) (h "1whbfq7bwqkv23k78cm8ilwzs6jgnsbhbfcmdpx7xq5vnlvdk5wv")))

(define-public crate-dev_menu-0.29.0 (c (n "dev_menu") (v "0.29.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)))) (h "1xxylsrr24wzx3ckw8sz5kadqfldhawcnhvigc4ab7zgld9kdv10")))

(define-public crate-dev_menu-0.30.0 (c (n "dev_menu") (v "0.30.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)))) (h "0l5y42vqr95b4jkds0qjp8568g5qfb2ggjkryh090zxg7kvhzwgf")))

(define-public crate-dev_menu-0.31.0 (c (n "dev_menu") (v "0.31.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "0v20xn5m32q4w73wpiadggzspfx7qz7991hfc808dx8z9limlmg5")))

(define-public crate-dev_menu-0.32.0 (c (n "dev_menu") (v "0.32.0") (d (list (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "13dfikshwgvy82gvsksw6qs4ajvbahf3qrhmzy9m4ny9p0hzivcp")))

(define-public crate-dev_menu-0.33.0 (c (n "dev_menu") (v "0.33.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.26.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "1alcbf25vm4vwyfqxxlms9zbjvw64yjwl45vfqz66pwdscx7n6gv")))

(define-public crate-dev_menu-0.34.0 (c (n "dev_menu") (v "0.34.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "1qnz6wzl3955xd06anlibw54gyj0m9ycjyfhrfds3512l8kgq8jl")))

(define-public crate-dev_menu-0.35.0 (c (n "dev_menu") (v "0.35.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "010bbi9ar71wihrzir78bskzq197kpmvvbbz0bbn4pjwnh97c77k")))

(define-public crate-dev_menu-0.36.0 (c (n "dev_menu") (v "0.36.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "15vgfg55aqnhk4zvsx46nl8zgbvbyk22rzg04cmhqr4973hc3hxn")))

(define-public crate-dev_menu-0.37.0 (c (n "dev_menu") (v "0.37.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.29.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "15cnbhmfv2yp9nmhycwmyqy6wbv2ww2ypsf9nj1vm512lyqhycgl")))

(define-public crate-dev_menu-0.38.0 (c (n "dev_menu") (v "0.38.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.30.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "06prz99wbwcg8wl9dahidd2bl1a94qwm6r12zcq17w8jif8mqirn")))

(define-public crate-dev_menu-0.39.0 (c (n "dev_menu") (v "0.39.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.31.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "1bfxdgpw8l1hgjxxfrqib80gymx5s1rbpgfbiamvgzn3gd3q8yvq")))

(define-public crate-dev_menu-0.40.0 (c (n "dev_menu") (v "0.40.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.32.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "1xk13xnis4f94asmkwj4pi390gpbk28xypcp96s2dnpzzil69xkn")))

(define-public crate-dev_menu-0.41.0 (c (n "dev_menu") (v "0.41.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.33.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "1zww4wgq3bvfs86vhzkf3db07xg1hhqq9kcxxsc6sbfkhb3hz328")))

