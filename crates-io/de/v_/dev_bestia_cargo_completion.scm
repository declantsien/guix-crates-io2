(define-module (crates-io de v_ dev_bestia_cargo_completion) #:use-module (crates-io))

(define-public crate-dev_bestia_cargo_completion-2021.817.1148 (c (n "dev_bestia_cargo_completion") (v "2021.817.1148") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1hdq0mcjgfkipywd2b8kr1linyjzp5h9lp3wv0c08sprppvhxxrp") (y #t)))

(define-public crate-dev_bestia_cargo_completion-2021.817.1711 (c (n "dev_bestia_cargo_completion") (v "2021.817.1711") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0p4106wziz6dhc5703fg50gd42zk6amzma9dqqwzdl9maxrr30fk") (y #t)))

(define-public crate-dev_bestia_cargo_completion-2021.817.1756 (c (n "dev_bestia_cargo_completion") (v "2021.817.1756") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1afz8ngmpqpl247hiwdpr2g1qqfdp8skznndi1rvz7wm5ssql59b") (y #t)))

(define-public crate-dev_bestia_cargo_completion-2021.819.909 (c (n "dev_bestia_cargo_completion") (v "2021.819.909") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1qm455zv84zlvjrnc0cgvyvsdf1dnk3s2wyy1x18a37c094apxlr") (y #t)))

(define-public crate-dev_bestia_cargo_completion-2021.1230.1047 (c (n "dev_bestia_cargo_completion") (v "2021.1230.1047") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1wnij3rkwzzihg9fba754ixyqfw4y14rvngsa2nx9dqif1bpnmzd") (y #t)))

(define-public crate-dev_bestia_cargo_completion-2023.531.1119 (c (n "dev_bestia_cargo_completion") (v "2023.531.1119") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1mkqij08p44bsyryvb3wd0q3c266n6xvqy0gbviv0jdgp9llb13f")))

(define-public crate-dev_bestia_cargo_completion-2024.306.2242 (c (n "dev_bestia_cargo_completion") (v "2024.306.2242") (h "1icm5qpzgj0828vkgdqfp4rchqj9c5vaa7ss8g5qa7zsrwxf1ln8")))

(define-public crate-dev_bestia_cargo_completion-2024.306.2322 (c (n "dev_bestia_cargo_completion") (v "2024.306.2322") (h "1lm3n39lzpdrs9c4cy9my1win2vn5ql9rjbrlwxk7955420x1kvg")))

(define-public crate-dev_bestia_cargo_completion-2024.307.1911 (c (n "dev_bestia_cargo_completion") (v "2024.307.1911") (h "185miwhwzgrmq025k11h2sd6rzfyyprvs8ncnjdv3w6mchsdzw28")))

(define-public crate-dev_bestia_cargo_completion-2024.421.1905 (c (n "dev_bestia_cargo_completion") (v "2024.421.1905") (h "1kksr80x64axxz4kw9jdkvgqgz8ncb0d7qz42x8haascqjkw1hlc")))

