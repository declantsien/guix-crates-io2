(define-module (crates-io de mu demurgos_headers) #:use-module (crates-io))

(define-public crate-demurgos_headers-0.3.8 (c (n "demurgos_headers") (v "0.3.8") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "headers-core") (r "^0.2") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "httpdate") (r "^1") (d #t) (k 0)) (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "0j68zc7s647sjs6hd9cbbaxhlxh536szviy962qvwps4w6s1ppiz") (f (quote (("nightly"))))))

