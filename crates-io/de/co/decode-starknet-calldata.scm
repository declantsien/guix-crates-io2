(define-module (crates-io de co decode-starknet-calldata) #:use-module (crates-io))

(define-public crate-decode-starknet-calldata-0.1.0 (c (n "decode-starknet-calldata") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.4.2") (d #t) (k 0)) (d (n "starknet") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "15pbbsdh1yd92n138mi4iflgvjc51qfpy0lqi0l74x0y2xkpiapi")))

(define-public crate-decode-starknet-calldata-0.1.1 (c (n "decode-starknet-calldata") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.4.2") (d #t) (k 0)) (d (n "starknet") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0y050if70l5cc3pzx0j5f9cbfvkp3nkh4aia51m2wlgq82q6x49l")))

(define-public crate-decode-starknet-calldata-0.1.2 (c (n "decode-starknet-calldata") (v "0.1.2") (d (list (d (n "bigdecimal") (r "^0.4.2") (d #t) (k 0)) (d (n "starknet") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0ypzwd1i6p6fjj6wzivsygx5ck2rbckrjijwndrgpp7b4jcypcsf")))

