(define-module (crates-io de co deco) #:use-module (crates-io))

(define-public crate-deco-0.1.0 (c (n "deco") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "04bik9m2lhmdanrr9agzrdkgfhdpd8z19lck1d5xdfhrgw83k7md")))

(define-public crate-deco-0.2.0 (c (n "deco") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0n94ap646ax9s0dl7jcg3gf2nzcj7llj03yjbbszys55s987bfxi")))

