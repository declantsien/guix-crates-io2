(define-module (crates-io de co decommerce) #:use-module (crates-io))

(define-public crate-decommerce-0.0.1 (c (n "decommerce") (v "0.0.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (f (quote ("serve" "json"))) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "09gyad6xhci4kcamhrkldq2qby0fjik6iq32mbx5lkv0sfwyp877")))

(define-public crate-decommerce-0.0.2 (c (n "decommerce") (v "0.0.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (f (quote ("serve" "json"))) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1lrsfqn1xw1v6mibbflvh0ywmfbbg1n3qbdlrzzhz7xrlildk64f")))

