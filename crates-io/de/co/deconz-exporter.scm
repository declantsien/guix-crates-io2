(define-module (crates-io de co deconz-exporter) #:use-module (crates-io))

(define-public crate-deconz-exporter-0.1.0 (c (n "deconz-exporter") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1kfnz97df983h3harkxky6hajbk1xxqyqfwdh3xv93s4griz5pg4")))

