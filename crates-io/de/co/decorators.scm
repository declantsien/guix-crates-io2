(define-module (crates-io de co decorators) #:use-module (crates-io))

(define-public crate-decorators-0.1.0 (c (n "decorators") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f7y5z0w8id084nzrabddgkxmg2q5037yvf27ly92zyv165bn0b7") (y #t)))

(define-public crate-decorators-0.1.1 (c (n "decorators") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lfcy9jrz3p1knfhkfbrxr3w582nyyfm1zrvr6y6y31vpzpwqznp") (y #t)))

(define-public crate-decorators-0.1.2 (c (n "decorators") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r36zmr8rnf6m3b1bzif1cn4gqgw83rs7pg31cn7svkv74ql54nj")))

(define-public crate-decorators-0.1.3 (c (n "decorators") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fgxg2x5r7ivbrflnhbrp9ifh0bgn23qwkm7bwzvqfmm6rxqvdcb") (y #t)))

(define-public crate-decorators-0.1.4 (c (n "decorators") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18v3nv9y6bbs994mfl546ymgsbn66izfdfksq4frvs9mk6k7w3l9")))

