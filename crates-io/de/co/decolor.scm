(define-module (crates-io de co decolor) #:use-module (crates-io))

(define-public crate-decolor-0.1.0 (c (n "decolor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 0)))) (h "07vly6ba6dh3zl7nkvqa86jhqpn780jrc4iq2n1y6vnjpcrf9gdb") (r "1.72")))

(define-public crate-decolor-0.1.1 (c (n "decolor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 0)))) (h "084clnfp0i951c734yvg7r08768np61hxcpvgzwnfg23kk7lfq3z") (r "1.72")))

(define-public crate-decolor-0.1.2 (c (n "decolor") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1jl87hs858d1xhdkgby5662jfnz7qyhgfa9k1rfbvad695g3d74f") (r "1.72")))

