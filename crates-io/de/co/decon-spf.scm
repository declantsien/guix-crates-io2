(define-module (crates-io de co decon-spf) #:use-module (crates-io))

(define-public crate-decon-spf-0.1.0 (c (n "decon-spf") (v "0.1.0") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "17svp3gr5w06a3ss6b7wcs9bvgm5pi8xb6dghsspn84lg5adcanx") (y #t)))

(define-public crate-decon-spf-0.2.0 (c (n "decon-spf") (v "0.2.0") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "0kbra99rabip51w2gh7d87hqd0r064c208w4k57gcpim5b7s1gy8")))

(define-public crate-decon-spf-0.2.1 (c (n "decon-spf") (v "0.2.1") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "0jiz4kqgv77s8sf6lqam46l2ahakzpcgj9a3l3x58j8hlxzkcgys")))

(define-public crate-decon-spf-0.2.2 (c (n "decon-spf") (v "0.2.2") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "1620g3jdq0knby4d8ai7jf940lpl4zdhaylsxk6gdcswp1zaypz8") (f (quote (("warn-dns" "addr")))) (y #t)))

(define-public crate-decon-spf-0.2.3 (c (n "decon-spf") (v "0.2.3") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "0zmc942cwvy48aiacq3hxv9gd7qr717z63491a0khzx81vwb4ac4") (f (quote (("warn-dns" "addr"))))))

(define-public crate-decon-spf-0.2.4 (c (n "decon-spf") (v "0.2.4") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "0bk9zhgmkk9i533mngfy6f5am66xj805gh27jsrlmn6ihqvzgqp0") (f (quote (("warn-dns" "addr") ("strict-dns" "addr"))))))

(define-public crate-decon-spf-0.2.5 (c (n "decon-spf") (v "0.2.5") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20.1") (d #t) (k 2)))) (h "0q7hcafhl0kgnzd66c04vjn6jlb2a3qc2m1jmmc05dhwv2vg9hrr") (f (quote (("warn-dns" "addr") ("strict-dns" "addr"))))))

(define-public crate-decon-spf-0.2.6 (c (n "decon-spf") (v "0.2.6") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.23.2") (d #t) (k 2)))) (h "0a9y8ka7zqcnnbvxsf5kgjs8j8cr61z2s8ysjz639f9wnn0518dx") (f (quote (("warn-dns" "addr") ("strict-dns" "addr")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-decon-spf-0.2.7 (c (n "decon-spf") (v "0.2.7") (d (list (d (n "addr") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.23.2") (d #t) (k 2)))) (h "1p427nd9fdw6q2m38b4p44j2n1wyf2djhjw7xsl9hxdzw634dx97") (f (quote (("warn-dns" "addr") ("strict-dns" "addr")))) (s 2) (e (quote (("serde" "dep:serde"))))))

