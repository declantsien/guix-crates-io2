(define-module (crates-io de co decorum) #:use-module (crates-io))

(define-public crate-decorum-0.0.0 (c (n "decorum") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0amjp7dmlj7mmgs1f8xf2qpglzvdql0y2jrja7vdzaslcxs1x0d8") (f (quote (("serialize-serde" "serde") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.0.1 (c (n "decorum") (v "0.0.1") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ry871dl227xrxysflfha5pij7z77ig6qr1lcsahjkg27wl98cdv") (f (quote (("serialize-serde" "serde") ("default" "always-enforce-policy" "serialize-serde") ("always-enforce-policy"))))))

(define-public crate-decorum-0.0.2 (c (n "decorum") (v "0.0.2") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qx81b234w33113ajnrxcyp32w97si8ki770w61g1ql0l1z3xj84") (f (quote (("serialize-serde" "serde") ("default" "always-enforce-policy" "serialize-serde") ("always-enforce-policy"))))))

(define-public crate-decorum-0.0.3 (c (n "decorum") (v "0.0.3") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bip2r2brzrq7a91snl8d7b3xsfwdhpw6l6z3a5chc6vf3zjycip") (f (quote (("serialize-serde" "serde") ("enforce-constraints") ("default" "enforce-constraints" "serialize-serde"))))))

(define-public crate-decorum-0.0.4 (c (n "decorum") (v "0.0.4") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "153wki4x3r8q66cvp9ls9yfdkpk6kip91xlkmgb0ka153h7836gn") (f (quote (("serialize-serde" "serde") ("enforce-constraints") ("default" "enforce-constraints" "serialize-serde"))))))

(define-public crate-decorum-0.0.5 (c (n "decorum") (v "0.0.5") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hp2ixl4fl7s72ypk5k5afa251v9fhvvq91a194gsb867bxg3hax") (f (quote (("serialize-serde" "serde") ("enforce-constraints") ("default" "enforce-constraints" "serialize-serde"))))))

(define-public crate-decorum-0.0.6 (c (n "decorum") (v "0.0.6") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0n7dsl1xqpnmfjz8ihrsdq9dsagv0jwgbl83kpx8y139gvdmxv44") (f (quote (("serialize-serde" "serde") ("enforce-constraints") ("default" "enforce-constraints" "serialize-serde"))))))

(define-public crate-decorum-0.0.7 (c (n "decorum") (v "0.0.7") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yy6lz9qyvylrznpawxbz3vwxnq6qgphgkw448miljqkj1wc7v5v") (f (quote (("serialize-serde" "serde") ("enforce-constraints") ("default" "enforce-constraints" "serialize-serde"))))))

(define-public crate-decorum-0.0.8 (c (n "decorum") (v "0.0.8") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qvxsb5jp4hi342924h1xz8shvsb6yni1b3hx3qg76kxvlb979n2") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.0.9 (c (n "decorum") (v "0.0.9") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lr5ib3ra4qq9575cik3sibny3ab52rrmpa8li2qcfslpvm5d9zp") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.0.10 (c (n "decorum") (v "0.0.10") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f66swhvdhbamwcpschvdfzsqnfwlp7vbafjfwqmjrrrrryfq7z5") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.0.11 (c (n "decorum") (v "0.0.11") (d (list (d (n "derivative") (r "1.0.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0q5c52fdsbi4a3bj6mrkz9ag0hp7nxc208iycxd84cimfaivc44q") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.1.0 (c (n "decorum") (v "0.1.0") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m68v1r69fim0yvm3pl57z1d3g7c3mk5c419rs1khxs4zxdjv844") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.1.1 (c (n "decorum") (v "0.1.1") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1msr1wsx28p9hiijgavwsa8zz88qhigmhj3xl0w31gyyn2cj9yz8") (f (quote (("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde"))))))

(define-public crate-decorum-0.1.2 (c (n "decorum") (v "0.1.2") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1z2xzamb8b0f1lsji1lnx4qcf330gxsri0n63ai4k608g43nycq8") (f (quote (("std") ("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde" "std"))))))

(define-public crate-decorum-0.1.3 (c (n "decorum") (v "0.1.3") (d (list (d (n "num") (r "0.2.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zl2imsl4fdvqy3fn3hqn3777yzv13v8sm2dk3fi56dyym6106j9") (f (quote (("std") ("serialize-serde" "serde" "serde_derive") ("default" "serialize-serde" "std"))))))

(define-public crate-decorum-0.2.0 (c (n "decorum") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.0") (o #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0zyjiws3ddpsl52pg5kzwz3amr4yhfnydp4z7glkcr1909l5wapg") (f (quote (("std" "approx/std" "num-traits/std") ("serialize-serde" "serde" "serde_derive") ("default" "approx" "serialize-serde" "std"))))))

(define-public crate-decorum-0.3.0 (c (n "decorum") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.0") (o #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0sv7bdiaxxqnywf4fdd7brv9avjjihl6gi40f28faxa6l233x4q2") (f (quote (("std" "approx/std" "num-traits/std") ("serialize-serde" "serde" "serde_derive") ("default" "approx" "serialize-serde" "std"))))))

(define-public crate-decorum-0.3.1 (c (n "decorum") (v "0.3.1") (d (list (d (n "approx") (r "^0.3.0") (o #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "1kz5zlh2j345vgr9fbs223wxgz0hd3jkndj91hzmqkx1r39mj5r8") (f (quote (("std" "approx/std" "num-traits/std") ("serialize-serde" "serde" "serde_derive") ("default" "approx" "serialize-serde" "std"))))))

