(define-module (crates-io de co decompound) #:use-module (crates-io))

(define-public crate-decompound-0.1.0 (c (n "decompound") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "unicode_titlecase") (r "^2.2.0") (d #t) (k 0)))) (h "1cx859d25pavi1j8mlr265hn4gnc8i44rkwvlzza2x989l8p37gi") (r "1.64.0")))

(define-public crate-decompound-0.2.0 (c (n "decompound") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "unicode_titlecase") (r "^2.2.0") (d #t) (k 0)))) (h "1j7l6wjrkyk6i8bpk6ycdff67sz5wfx354nn6igdpwb90d2m6bns") (r "1.64.0")))

(define-public crate-decompound-0.3.0 (c (n "decompound") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "unicode_titlecase") (r "^2.2.0") (d #t) (k 0)))) (h "0pc30y3191sfy1dg9hvdg92g3lbp8ym058rn7by7xfxiv55sy8jr") (r "1.64.0")))

