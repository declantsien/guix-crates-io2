(define-module (crates-io de ci decision) #:use-module (crates-io))

(define-public crate-decision-0.1.0 (c (n "decision") (v "0.1.0") (h "02r3by6qnrvv6iw0jkl3qwvqg9zm3sjqzsziws5a8clgrs13c9rl")))

(define-public crate-decision-0.1.1 (c (n "decision") (v "0.1.1") (h "1aaygppcjal2wjj2bd1sl4aai8khvvff2jzkjw0pgnbbp3syna0x")))

(define-public crate-decision-0.1.2 (c (n "decision") (v "0.1.2") (h "0v2hz23lgh7grlr0cw2z3wcifqiqkjly9yv8z8vnb2jypcx6svgs")))

(define-public crate-decision-0.1.3 (c (n "decision") (v "0.1.3") (h "1adf5fsm2n63jgkci7r70ipf9kl516inafpyrwi73bx7g5imnv0y")))

