(define-module (crates-io de ci decimate) #:use-module (crates-io))

(define-public crate-decimate-0.1.0 (c (n "decimate") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (o #t) (d #t) (k 0)))) (h "1wl0kxkq08y9v4d23kdj500xp4m12c09kvrsz7ifd11hmzxds417") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-decimate-0.2.0 (c (n "decimate") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j80kkd4k0lpjyn0b5ks6d422221wif2hfmiaaxj63wl7qjdr7fy") (f (quote (("default" "serde"))))))

(define-public crate-decimate-0.2.1 (c (n "decimate") (v "0.2.1") (d (list (d (n "bincode") (r "~0.8") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0mr7l4fyb4vsqdbm606r7rrdhq1z04n9l5hp4wy59jnqvdpynh5d") (f (quote (("default" "serde"))))))

(define-public crate-decimate-0.2.2 (c (n "decimate") (v "0.2.2") (d (list (d (n "bincode") (r "~0.8") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "1q7b069asx0ihxsc5v95fhhrcxzq3bvaqmrcxjjpkfcr9vv1zb0k") (f (quote (("default" "serde"))))))

