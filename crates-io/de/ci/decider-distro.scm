(define-module (crates-io de ci decider-distro) #:use-module (crates-io))

(define-public crate-decider-distro-0.4.15 (c (n "decider-distro") (v "0.4.15") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1yszhw1pxx7vqmqvcqrslps0ipkd4b782v71dkvd6gjdhhigqcj0")))

(define-public crate-decider-distro-0.4.16 (c (n "decider-distro") (v "0.4.16") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0bwlnid5in2s6cqffp70s6qwssbx4l402p4l8nzsywz82ix9j7rm")))

(define-public crate-decider-distro-0.4.17 (c (n "decider-distro") (v "0.4.17") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1djf63qnj1jc95c8b5wy8fbdr8dqfr7adhdc86ia2smq84zsw0zh")))

(define-public crate-decider-distro-0.5.0 (c (n "decider-distro") (v "0.5.0") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0b45b0zfpdjb5jsjymqqz27rhihgjfyz92sqhdfwqgz7md872rqd")))

(define-public crate-decider-distro-0.5.1 (c (n "decider-distro") (v "0.5.1") (d (list (d (n "built") (r "^0.6.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1bvdh84gjqmj4jlqn080325dbzasajhflpwx71npyh8bcgfnkicj")))

(define-public crate-decider-distro-0.5.3 (c (n "decider-distro") (v "0.5.3") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1vn8n4wb5xwmhkr14nxdni75jal8bjiyv384437swgz3mcxi9zs9")))

(define-public crate-decider-distro-0.5.4 (c (n "decider-distro") (v "0.5.4") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "05d9wxkprswz463myswrydc179l8hyhivv6m8qfx5f399fdbwkf6")))

(define-public crate-decider-distro-0.5.5 (c (n "decider-distro") (v "0.5.5") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "03z4gkr828yd43sb20jkpfsf9cz2d688pid8jnx696wv8v0kvp9p")))

(define-public crate-decider-distro-0.5.6 (c (n "decider-distro") (v "0.5.6") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.31") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1vw0pmv2nvxpmkqd3gk3pjpib3qii879pwv47h1zp0m2j4xfj8i2")))

(define-public crate-decider-distro-0.5.7 (c (n "decider-distro") (v "0.5.7") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.31") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "06vwxi6xskbac39r4cyka6mff3vxhwm7apivcm9ycv2f1m8wyr32")))

(define-public crate-decider-distro-0.5.8 (c (n "decider-distro") (v "0.5.8") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1071dayhq67gc019hnir9yz2sfm9kr8fim2gc4dyd65m9v6am35y")))

(define-public crate-decider-distro-0.5.9 (c (n "decider-distro") (v "0.5.9") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0sw3yqkkj97clq2kf3szk1a1fb14fh66qqlm01yc8k1w7h5m6iyp")))

(define-public crate-decider-distro-0.5.10 (c (n "decider-distro") (v "0.5.10") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "18pvn4rqw0jnpnbqyr9ilqdv6n5k67asgw8ip17mfx57as1df3wh")))

(define-public crate-decider-distro-0.5.11 (c (n "decider-distro") (v "0.5.11") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0yi5w39xc1fw7h9w4pd7mbgnfd4xid5bqvq12x3k36f89jpdljac")))

(define-public crate-decider-distro-0.5.12 (c (n "decider-distro") (v "0.5.12") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "05pnsi269z67bsb2j4dxn3cqij374mny4xiaagdnba7pqg7azvij")))

(define-public crate-decider-distro-0.5.13 (c (n "decider-distro") (v "0.5.13") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1l5wryc6kzid2wbrir0fr5gncwwjiz86mf9mrxdf9wc7hdhca2f8")))

(define-public crate-decider-distro-0.5.14 (c (n "decider-distro") (v "0.5.14") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "00n4ynmqgaikv576kihjzwli543kslqlqgs2hy4g9rvlwnq4r8xq")))

(define-public crate-decider-distro-0.5.15 (c (n "decider-distro") (v "0.5.15") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1wlf1rs71fkiw0yy12j16n1vmc90wry23z3n4pn4bgbjr889nmb9")))

(define-public crate-decider-distro-0.5.16 (c (n "decider-distro") (v "0.5.16") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.6.10") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "039i1hhkswmynpvksqcxvnai2wbamhn504h0lxhral0rnbpsfvzj")))

(define-public crate-decider-distro-0.5.17 (c (n "decider-distro") (v "0.5.17") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "08jy3iaq4c9kp2zbqixr1a8h2k9mynf7038v0yy49km2qp52qmv6")))

(define-public crate-decider-distro-0.6.0 (c (n "decider-distro") (v "0.6.0") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0pf0di0m51lwddfmdl4180dar5wlwg0gqzcqc7pvvm670h7nzld5")))

(define-public crate-decider-distro-0.6.1 (c (n "decider-distro") (v "0.6.1") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "14n9i31nwk1cm4mlpzmkw2cdknfdbvl0q0a8ql9wcaa860s4rirc")))

(define-public crate-decider-distro-0.6.2 (c (n "decider-distro") (v "0.6.2") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1vrwn1j03cli374r4i2rnhwkpg85pl57z7jkfr5gdnx0gz51397v")))

(define-public crate-decider-distro-0.6.3 (c (n "decider-distro") (v "0.6.3") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0yp1pjk7pqdzfg311lz80c9vw2zzvvhxanvf94jiky2g64bqgxd9")))

(define-public crate-decider-distro-0.6.4 (c (n "decider-distro") (v "0.6.4") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0c0an4wm66c698pkgc6z88v1snb214762q22w124acb4gic7pc97")))

(define-public crate-decider-distro-0.6.5 (c (n "decider-distro") (v "0.6.5") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1zv5xk8dr34wr23l2n1z7xzpj40fksqcpd0vr78h5vp3dgjmqlnd")))

(define-public crate-decider-distro-0.6.6 (c (n "decider-distro") (v "0.6.6") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0zi8k5i5zr2b8ri5pdb3dv8cziwqsiyn4hdxr59pkjgdia97gjbg")))

(define-public crate-decider-distro-0.6.7 (c (n "decider-distro") (v "0.6.7") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1ask7wxmixfdsaz3vpdj2jl4zf3cdvm82vx9k4qnqnx4yivdxa03")))

(define-public crate-decider-distro-0.6.8 (c (n "decider-distro") (v "0.6.8") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1mbqnkrykykp5ad8ik44v332mryk8qkbr861nylwkc7dx776dvv7")))

(define-public crate-decider-distro-0.6.9 (c (n "decider-distro") (v "0.6.9") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "09dlr88s0h04fsfdb2d6g5f6xgf807fi0jwcnf2viafgh3wchjh8")))

(define-public crate-decider-distro-0.6.10 (c (n "decider-distro") (v "0.6.10") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "18fydn4dkhw6xhvxpk0wzvpgckbwzjj9n8mn63wbyvp1nahvp1aq")))

(define-public crate-decider-distro-0.6.11 (c (n "decider-distro") (v "0.6.11") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0s5zizfdfbyp0vj9fbxd4n2zl1hyb58i4300cydil48in9raiw8g")))

(define-public crate-decider-distro-0.6.12 (c (n "decider-distro") (v "0.6.12") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.7.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "05p8i90drp5sqki4mq3nlkb582mx1hcsddp8i9nszrg0d9qalrpy")))

