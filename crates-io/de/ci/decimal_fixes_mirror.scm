(define-module (crates-io de ci decimal_fixes_mirror) #:use-module (crates-io))

(define-public crate-decimal_fixes_mirror-2.0.4-fix1.0.0 (c (n "decimal_fixes_mirror") (v "2.0.4-fix1.0.0") (d (list (d (n "bitflags") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "ord_subset") (r ">=3.0.0, <4.0.0") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">=0.3.0, <0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1yhzsnxwkwpna1xkx1lidjw8ricy6jzs55rbjr8l7qsa5yjmg2c7") (f (quote (("default" "ord_subset" "rustc-serialize" "serde"))))))

