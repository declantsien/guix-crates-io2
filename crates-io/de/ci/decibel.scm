(define-module (crates-io de ci decibel) #:use-module (crates-io))

(define-public crate-decibel-0.1.0 (c (n "decibel") (v "0.1.0") (h "137apfigxf26pc8vlf33lf7fwm09qpwb2k7xm5ph5n1yqrcvm3mp")))

(define-public crate-decibel-0.1.1 (c (n "decibel") (v "0.1.1") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0iq4hv9fvj6gagrmmmxwsarpnh9xivpf900dlqpgbdp7fpff5yhq")))

(define-public crate-decibel-0.1.2 (c (n "decibel") (v "0.1.2") (h "1bfmd8lrhainnz6yqxx68xpaa9hk6cbr6p9yfr7x3v74qdc2bna1")))

