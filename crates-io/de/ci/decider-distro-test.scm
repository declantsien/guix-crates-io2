(define-module (crates-io de ci decider-distro-test) #:use-module (crates-io))

(define-public crate-decider-distro-test-0.4.8 (c (n "decider-distro-test") (v "0.4.8") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "07k4zw027abfa8y6v2i56f4lmqz3zn3d8hwdisih7j4lfb5hn9n3")))

(define-public crate-decider-distro-test-0.4.8-1 (c (n "decider-distro-test") (v "0.4.8-1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "058k6gwj2rqnn5nsx4vy3mkwykzir5jp9ibdyasy3q8ng37fbky7")))

(define-public crate-decider-distro-test-0.4.8-2 (c (n "decider-distro-test") (v "0.4.8-2") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0bhp7p8daga1p07kmqwjfmr4c94z97501hzvimln5z216cngssk4")))

(define-public crate-decider-distro-test-0.4.8-3 (c (n "decider-distro-test") (v "0.4.8-3") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "fluence-spell-dtos") (r "^0.5.11") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "189qahp0piibk3ca6nny6m4cmf46mfrd8swyccd8brmsjdx6b38k")))

