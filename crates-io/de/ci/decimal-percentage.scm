(define-module (crates-io de ci decimal-percentage) #:use-module (crates-io))

(define-public crate-decimal-percentage-0.1.0 (c (n "decimal-percentage") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1j3j1prl1376zqw2dljsdnibjsra7p5mvk86zi13jy6fy2nljxil") (y #t)))

(define-public crate-decimal-percentage-0.1.1 (c (n "decimal-percentage") (v "0.1.1") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "1y37b5chfldcihm5a13g6mwrzw8q3l396484n2mf2p6d0472hb58")))

(define-public crate-decimal-percentage-0.1.2 (c (n "decimal-percentage") (v "0.1.2") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hbpj1r31lj02hfk4m3s49awwyjb641zvshcxcpwq6jvwlnh3cyz")))

(define-public crate-decimal-percentage-0.1.3 (c (n "decimal-percentage") (v "0.1.3") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "078x4fq6v3ad27vhc3as4fag1xzz84x48d8zpmvd1403dzkkh95p")))

(define-public crate-decimal-percentage-0.1.4 (c (n "decimal-percentage") (v "0.1.4") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "19iw5vm7z74fc7l602f221sj269450djvlims76g8c6b84srdr84")))

