(define-module (crates-io de ci decimal-wad) #:use-module (crates-io))

(define-public crate-decimal-wad-0.1.0 (c (n "decimal-wad") (v "0.1.0") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "1c46hip5xlsva1cjrkxl1f4hvc9k9gszjmi8rm5ymylzgbx0i1gh") (y #t)))

(define-public crate-decimal-wad-0.1.1 (c (n "decimal-wad") (v "0.1.1") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "1fafqf78hkg6jcdxrv8hp3pqxyga6aamkjwi7bxnq5yfyxy6dyza")))

(define-public crate-decimal-wad-0.1.2 (c (n "decimal-wad") (v "0.1.2") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "02zam3x6pjhh8x1m2ixms4xym92fzrmcr07badahklx6mn3m6cx3")))

(define-public crate-decimal-wad-0.1.3 (c (n "decimal-wad") (v "0.1.3") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "1rr3jpnb3as68bpsp0lpwk1c7as565wabawzsv74a0bmp54fj9xd")))

(define-public crate-decimal-wad-0.1.4 (c (n "decimal-wad") (v "0.1.4") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "08vi93yq0mnyczd9w9drvrkj5xxxa0vpawz36wmsiqv03vb9qcxp")))

(define-public crate-decimal-wad-0.1.5 (c (n "decimal-wad") (v "0.1.5") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "0wqdc51q9dfgmxy64lj2nbjdb46hqcy5klqjs5b14is30qqyhdd9")))

(define-public crate-decimal-wad-0.1.6 (c (n "decimal-wad") (v "0.1.6") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "1yav5rxrgbswgw0glvjr8li7380zjm3ighlp80qapq65zz8zy47g")))

(define-public crate-decimal-wad-0.1.7 (c (n "decimal-wad") (v "0.1.7") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "1x504m5jn5vm35dgjyj764rlrs83cq9ljay9cs6zi3dkwkjj9dn3")))

(define-public crate-decimal-wad-0.1.8 (c (n "decimal-wad") (v "0.1.8") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "0myzaj51i59rjsdjhgk9vpy1270l8whqq8kdlz59rs86hi6rvz3v")))

(define-public crate-decimal-wad-0.1.9 (c (n "decimal-wad") (v "0.1.9") (d (list (d (n "uint") (r "^0.9.1") (k 0)))) (h "03zxfz1z8sksdlsbncxlawghxwrif0d75774hpj2pn7pwhv2gk7k") (f (quote (("ops-traits") ("default" "ops-traits"))))))

