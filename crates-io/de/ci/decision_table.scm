(define-module (crates-io de ci decision_table) #:use-module (crates-io))

(define-public crate-decision_table-0.0.1 (c (n "decision_table") (v "0.0.1") (h "082ss5v7makm3048ib8x2x04wdlqkdrss7085jcnrpbkx50y4243")))

(define-public crate-decision_table-0.0.2 (c (n "decision_table") (v "0.0.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0xgsjk9y3wdp71s6p0b3d6gkc9q7ziqjmbghmfv4daz2rhp6bw6v")))

