(define-module (crates-io de ci decide) #:use-module (crates-io))

(define-public crate-decide-0.1.0 (c (n "decide") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "cmd_lib") (r "^0.7.8") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "01xas3fj2izslgk6gl2c608c4cf2243swnh3a2v5ra11lzkga835")))

(define-public crate-decide-0.1.1 (c (n "decide") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "cmd_lib") (r "^0.7.8") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1r8asnhlanbwziyclp6sg9gdh9ikd9ilrlrq30qcbc29klpj33rg")))

