(define-module (crates-io de lo delorean) #:use-module (crates-io))

(define-public crate-delorean-0.1.0 (c (n "delorean") (v "0.1.0") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-util" "rt-core" "macros"))) (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "0vmr9m697nfkimbvbjhr34m3paqzpb1qxf70q4jb42lwnc1n0a9a")))

(define-public crate-delorean-0.2.0 (c (n "delorean") (v "0.2.0") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "0zxq6gsax7lxdmk518vi8wddvj9wrxai3wrmjmjbjd5q67hdzr87")))

(define-public crate-delorean-0.2.1 (c (n "delorean") (v "0.2.1") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded" "macros" "time"))) (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "1r4rci01pi656kvzr0i31nc738pzry087asqc6rd7w20xxyb4af4")))

(define-public crate-delorean-0.3.0 (c (n "delorean") (v "0.3.0") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "~0.15.8") (f (quote ("config"))) (k 1)))) (h "03w0750zx9ns93c05bz6inlndwb4pkann8z6v91hidz3rj40fsfl")))

