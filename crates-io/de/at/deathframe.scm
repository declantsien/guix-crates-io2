(define-module (crates-io de at deathframe) #:use-module (crates-io))

(define-public crate-deathframe-0.0.0 (c (n "deathframe") (v "0.0.0") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1dqackc1wkkr4cmng0j5975mgmg13mjw5y2g973s3znzpr2ssgrk")))

(define-public crate-deathframe-0.0.1 (c (n "deathframe") (v "0.0.1") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1ghdmy005srm1nx3xihxmdrzq97l54c3i6xscanxjly6r2kbjjmg")))

(define-public crate-deathframe-0.1.0 (c (n "deathframe") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0f8h94wabpm11mp0yhw5n2b4kzslfa3gszxlgx271vwna67ak6lb")))

(define-public crate-deathframe-0.1.1 (c (n "deathframe") (v "0.1.1") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0amwfihckdrj6czw8ipxixx1xd6mqiyh0i0ffqpr96hc5znhk5sc")))

(define-public crate-deathframe-0.1.2 (c (n "deathframe") (v "0.1.2") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "16v1684xp4fpd7i0l8284s2snkfq38jdm6qajafss1b18v5x7j4r")))

(define-public crate-deathframe-0.1.3 (c (n "deathframe") (v "0.1.3") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0hhcwd5v42i6frzr78vd6ygdkbh2880ighypbci6x7j599s79z1j")))

(define-public crate-deathframe-0.1.4 (c (n "deathframe") (v "0.1.4") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "14y9gpb4924jmsmcryv4pwq216z52m9lrh1v3xkph41svpx70ixm")))

(define-public crate-deathframe-0.2.0 (c (n "deathframe") (v "0.2.0") (d (list (d (n "amethyst") (r "^0.12.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "18x5ff364lm1551rxgjs446wp428cg2crwck47bl7ibkll96jvyg")))

(define-public crate-deathframe-0.2.1 (c (n "deathframe") (v "0.2.1") (d (list (d (n "amethyst") (r "^0.12.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0xmglri986c658kg27aa90mii57mxlmlp42wr7zyhrf9mf95h1vd")))

(define-public crate-deathframe-0.3.0 (c (n "deathframe") (v "0.3.0") (d (list (d (n "amethyst") (r "^0.13.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "06ir07wdscshi8fym29yvxd47vj49d35xfjgcjabq0y1f4ws5hqz") (y #t)))

(define-public crate-deathframe-0.3.1 (c (n "deathframe") (v "0.3.1") (d (list (d (n "amethyst") (r "^0.13.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0zvvx1p84bqdvyfi3jybxwl4pq3w6i2db6rwmpncyd2vk19jmfk3")))

(define-public crate-deathframe-0.4.0 (c (n "deathframe") (v "0.4.0") (d (list (d (n "amethyst") (r "^0.13.1") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0qf5dvkgyk85zbwzz1bmcn9az1ws9xscqh34lg1xl4bzkdg9hads")))

(define-public crate-deathframe-0.4.1 (c (n "deathframe") (v "0.4.1") (d (list (d (n "amethyst") (r "^0.13.1") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0bjif7635dxg41m4v8wq9jdmd682c8ylfqhhhsh7xm9hpn579jga")))

(define-public crate-deathframe-0.4.2 (c (n "deathframe") (v "0.4.2") (d (list (d (n "amethyst") (r "^0.13.1") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0syy5iparvl0gz3xajl0xa1gcx886i11k41cz6m7556pam27rgwq")))

(define-public crate-deathframe-0.4.3 (c (n "deathframe") (v "0.4.3") (d (list (d (n "amethyst") (r "^0.13.1") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1xwf6xym9m3as00cv4j8prly1xvhn07xk39cvyhx10id30vac9km")))

(define-public crate-deathframe-0.5.0 (c (n "deathframe") (v "0.5.0") (d (list (d (n "amethyst") (r "^0.13.2") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1ys5mmvy3kwaw07zznah87zh4y7chm044723ljk45g58m4xf126y")))

(define-public crate-deathframe-0.5.1 (c (n "deathframe") (v "0.5.1") (d (list (d (n "amethyst") (r "^0.13.2") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "bitflags") (r "= 1.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1alwmgq06k0fg24aamkax01jixwhkk650ig5ag8skcjydc8ch9kw")))

