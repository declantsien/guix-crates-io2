(define-module (crates-io de at death_god_logging_tool) #:use-module (crates-io))

(define-public crate-death_god_logging_tool-1.0.0 (c (n "death_god_logging_tool") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0wmbw6i2y57m8rz8pfcp9sswg83wzdylfd130ix73k6hmf5ladz8")))

(define-public crate-death_god_logging_tool-1.0.1 (c (n "death_god_logging_tool") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0qrgdkcb501biaifnwp636yw2zwwvsdvniqhsygdc8skk97ags4v")))

(define-public crate-death_god_logging_tool-1.0.2 (c (n "death_god_logging_tool") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1gwmvl5q34jcmp1pwaxz9kavd0238vrx897mhms2jdq078hxnqz4")))

(define-public crate-death_god_logging_tool-1.0.3 (c (n "death_god_logging_tool") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0civ2mb6ikz9fij2f45gf4qwl0bxr2fb7q7rlf4am3lkcvhnipxk")))

(define-public crate-death_god_logging_tool-1.0.4 (c (n "death_god_logging_tool") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "19b47pzp355cznl6981fmjr06mgqqichxmhz18wnpn1w5z21yzl5")))

(define-public crate-death_god_logging_tool-1.0.5 (c (n "death_god_logging_tool") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "01rr497kry4ahs7sprkxss9214srajf7xxgb79cgrwy024dvaaqv")))

(define-public crate-death_god_logging_tool-1.0.6 (c (n "death_god_logging_tool") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1i560vgvwxrbmhd9vipr3rkh2b42g3y0z6f4dvmax79m3xhpr97l")))

(define-public crate-death_god_logging_tool-1.0.7 (c (n "death_god_logging_tool") (v "1.0.7") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "17py9jsww9qqh7yc8qzp1qavb4j630vz5g76abj4p4bb6y0cc4pd")))

(define-public crate-death_god_logging_tool-1.0.8 (c (n "death_god_logging_tool") (v "1.0.8") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1f5iljd0izmzb0fcnkih28f0g1im2yfbc5ms26d5i7csqa7g1qkv")))

(define-public crate-death_god_logging_tool-1.0.9 (c (n "death_god_logging_tool") (v "1.0.9") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "15zhn53ggjisdldzcccwrf7siq8jyr0mrrrmz4wm4jf7nvpvvqkc")))

