(define-module (crates-io de la delaunator) #:use-module (crates-io))

(define-public crate-delaunator-0.1.0 (c (n "delaunator") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0xm1aa5pxmf1hspd79vb8fggmf577763gngk9vpwnr2fhf973m1y")))

(define-public crate-delaunator-0.1.1 (c (n "delaunator") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0cx5s2fwi6b44wflxv5q0lf9qwqhp54sw876iaicdk13g8fhn6gi")))

(define-public crate-delaunator-0.2.0 (c (n "delaunator") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 2)))) (h "1p1dfrg84hdc3dnab6dp3q6kbifabbzhdirmqgxsvbvdnqi93pg3")))

(define-public crate-delaunator-0.2.1 (c (n "delaunator") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1g7gkj99hg6x00aqghck8mpxi9c4dyd9azzwnpp4crfyw87qcky5")))

(define-public crate-delaunator-1.0.0 (c (n "delaunator") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "robust") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "17hr9jrynxdj6a4388h4ljdxc63zsha7p6n2vssg61qd94rygrj7") (f (quote (("std") ("default" "std"))))))

(define-public crate-delaunator-1.0.1 (c (n "delaunator") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "robust") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1hmndhq1rw8ay2vcxw1k1yb6l6fvyyrgs1s335fik6phg4jh8clf") (f (quote (("std") ("default" "std"))))))

(define-public crate-delaunator-1.0.2 (c (n "delaunator") (v "1.0.2") (d (list (d (n "robust") (r "^0.2.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "1yil74wapj9ffqa7pi36xnny0bmwhkjg7c4k1l530f3sdhw6xd0a") (f (quote (("std") ("default" "std"))))))

