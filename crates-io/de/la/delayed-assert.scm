(define-module (crates-io de la delayed-assert) #:use-module (crates-io))

(define-public crate-delayed-assert-0.1.0 (c (n "delayed-assert") (v "0.1.0") (d (list (d (n "truthy") (r "^1.1") (d #t) (k 0)))) (h "0ri3jgv0704n71a6fr5645vbcc6sqgvj6lffjx3xxdn0vx88f5md")))

(define-public crate-delayed-assert-0.1.1 (c (n "delayed-assert") (v "0.1.1") (d (list (d (n "truthy") (r "^1.1") (d #t) (k 0)))) (h "1ysnxilh2aaxlsqh7nr20k3nhky3c9jj0gspfizak1a8mhczr0aj")))

