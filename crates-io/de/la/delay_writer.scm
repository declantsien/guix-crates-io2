(define-module (crates-io de la delay_writer) #:use-module (crates-io))

(define-public crate-delay_writer-0.1.0 (c (n "delay_writer") (v "0.1.0") (h "03dvx86ss92iv49w1d9g4b3hji0dsicx64rr50l7ylln0w7hq218")))

(define-public crate-delay_writer-0.2.0 (c (n "delay_writer") (v "0.2.0") (h "08f7v8yx30grwjdfcmf0naz51rqk2qahwfc8xjhb24i0qck15gf8")))

(define-public crate-delay_writer-0.2.1 (c (n "delay_writer") (v "0.2.1") (h "049m6c0c1m8w1zvv3d7kf8ragjq84lmya37zh77r6gczwfx966zw")))

