(define-module (crates-io de la delay-handler) #:use-module (crates-io))

(define-public crate-delay-handler-0.1.0 (c (n "delay-handler") (v "0.1.0") (d (list (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.2") (f (quote ("time"))) (d #t) (k 0)))) (h "1m5y6gh65r9scw0d62jj7sznrism37p5lbqag8y6ipybi954jdcw")))

(define-public crate-delay-handler-0.1.1 (c (n "delay-handler") (v "0.1.1") (d (list (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0617vdknyj9xzq8f7vybpraar7v7jg6xzv990ifval3g81gk41l3")))

