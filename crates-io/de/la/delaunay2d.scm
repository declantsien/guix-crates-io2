(define-module (crates-io de la delaunay2d) #:use-module (crates-io))

(define-public crate-delaunay2d-0.0.1 (c (n "delaunay2d") (v "0.0.1") (h "0jdgvvqp68v5l8n2h5x43m3vh79zs45mpfgfkp78sxn7pqpg4ibm")))

(define-public crate-delaunay2d-0.0.2 (c (n "delaunay2d") (v "0.0.2") (h "1xs4cxi2p9ikisw2hm1c5l6dvmr36slvkii03jic4slpi236hvki")))

