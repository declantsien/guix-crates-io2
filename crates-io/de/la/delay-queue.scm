(define-module (crates-io de la delay-queue) #:use-module (crates-io))

(define-public crate-delay-queue-0.1.0 (c (n "delay-queue") (v "0.1.0") (d (list (d (n "timebomb") (r "^0.1.2") (d #t) (k 2)))) (h "1423aj415c70nlbnsqngydk7bgiils1vvy73wycdikb54vnbcj9y")))

(define-public crate-delay-queue-0.2.0 (c (n "delay-queue") (v "0.2.0") (d (list (d (n "timebomb") (r "^0.1.2") (d #t) (k 2)))) (h "1ad828z27pl1ks53g8y6aj5sr2b8ij4xrikfmcv14x5wzi7srzpl")))

