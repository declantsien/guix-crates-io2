(define-module (crates-io de la delaunay_creator) #:use-module (crates-io))

(define-public crate-delaunay_creator-0.2.0 (c (n "delaunay_creator") (v "0.2.0") (h "181s2snxj44va5cf387z3vpwssc03slfbzw0am98zbjfai1qj14j")))

(define-public crate-delaunay_creator-0.2.1 (c (n "delaunay_creator") (v "0.2.1") (h "1fwcirrry8knbjf9846r46xb5vajjs92hh9jkdnz9wwizkc5wwwj")))

(define-public crate-delaunay_creator-0.2.2 (c (n "delaunay_creator") (v "0.2.2") (h "1n9aifd9n48ffdn8yaphghwy2naby084k69pjnaislmzxww2jzdf")))

(define-public crate-delaunay_creator-0.2.3 (c (n "delaunay_creator") (v "0.2.3") (h "0sjmwihkh51q7w9p51yka7rwnydsdn74wb0lgavjsnq5kb79x6ax")))

(define-public crate-delaunay_creator-0.2.4 (c (n "delaunay_creator") (v "0.2.4") (h "1sagqiypi5s5jspg3730qjsrjm9azf4d4xgpkdvlxl384i07ncjx")))

(define-public crate-delaunay_creator-0.2.5 (c (n "delaunay_creator") (v "0.2.5") (d (list (d (n "plotters") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11blvy07b6l3fs6nzl2030n1mmi09a3ws9vcqjms5g837hz4hmby")))

(define-public crate-delaunay_creator-0.2.6 (c (n "delaunay_creator") (v "0.2.6") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1gllgwf7yicakbksf3ilrq3nvgd1w750ij8jzzj67z43cdd6zzz1")))

