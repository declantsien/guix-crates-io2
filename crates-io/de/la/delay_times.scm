(define-module (crates-io de la delay_times) #:use-module (crates-io))

(define-public crate-delay_times-0.1.0 (c (n "delay_times") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)))) (h "0zivx3ryil2x70mlds427xbbgazqkgslp3gm6cl083l3n3iiyks1")))

(define-public crate-delay_times-0.1.1 (c (n "delay_times") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)))) (h "0wkb0y8qhc4nfal68w3znrqvyv3flcy7yhmh29nyns0k2xsyz0zl")))

(define-public crate-delay_times-0.1.2 (c (n "delay_times") (v "0.1.2") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)))) (h "18r3qc7al2m3cn858ish29019kjc39wgw0vyks8zva2qlzdrs4f9")))

(define-public crate-delay_times-0.2.0 (c (n "delay_times") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)))) (h "1s4w359q00dhyypkj35kazgnx5iyiik91xmdc2b46syjjj3rr96j")))

