(define-module (crates-io de la delay_map) #:use-module (crates-io))

(define-public crate-delay_map-0.1.0 (c (n "delay_map") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("time"))) (d #t) (k 0)))) (h "040rgkxqi3j8wigmj6xc7w29nb28f1mwp2nwr1v8h1ly9i2wkdbg")))

(define-public crate-delay_map-0.1.1 (c (n "delay_map") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("time"))) (d #t) (k 0)))) (h "0dld69aplq3ry0xn78kjahxxaylbpkl67zz1kabji5my56bww5k7")))

(define-public crate-delay_map-0.1.2 (c (n "delay_map") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("time"))) (d #t) (k 0)))) (h "1xl3bmsv30q1makirpblkxk23qald691pjwvpzf30j7ymg9pakcw")))

(define-public crate-delay_map-0.2.0 (c (n "delay_map") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.9") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0rizir1kmb487fpqvr518ny84cwy658grrlm3z6ml3zphrj89ssq")))

(define-public crate-delay_map-0.3.0 (c (n "delay_map") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1s7qn8ln8sa9x9dlb14y2kd6x5f6ppv0dsd0njvdr7prrcjmqdg4")))

