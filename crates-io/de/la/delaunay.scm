(define-module (crates-io de la delaunay) #:use-module (crates-io))

(define-public crate-delaunay-0.0.0 (c (n "delaunay") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1lcxz8aia8xyn5v2ix9lwdz9v8f26bzgpz2p6k53bh3mmy74n77y") (f (quote (("default"))))))

(define-public crate-delaunay-0.0.1 (c (n "delaunay") (v "0.0.1") (d (list (d (n "shape-core") (r "^0.1.3") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "135875b27rz8fxhscscfnp8k34y4az8c9wyq724h038ph8ayg153") (f (quote (("default"))))))

(define-public crate-delaunay-0.1.0 (c (n "delaunay") (v "0.1.0") (d (list (d (n "shape-core") (r "^0.1.3") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "054axpvmyq0hh2x8ynz2jvxlqpvaqjsc82i9kvdab3jijbblzpsr") (f (quote (("default"))))))

