(define-module (crates-io de la delayqueue) #:use-module (crates-io))

(define-public crate-delayqueue-0.0.1 (c (n "delayqueue") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rz03wghrj663hhxqp7b18vxyypq1k31idqxg13pc3sp6ql6rw12")))

(define-public crate-delayqueue-0.0.2 (c (n "delayqueue") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jf0iv7aqccn6bnzr0153102ww60bm1xb3v9l6p2jykz38yi4i6r")))

