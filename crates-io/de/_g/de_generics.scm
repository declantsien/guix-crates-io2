(define-module (crates-io de _g de_generics) #:use-module (crates-io))

(define-public crate-de_generics-0.1.0 (c (n "de_generics") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "02nvf7lxa9dpis2ac03qg0x0mk37rvqmrmyhwdgd4zi83m2vn53q")))

