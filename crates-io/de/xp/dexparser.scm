(define-module (crates-io de xp dexparser) #:use-module (crates-io))

(define-public crate-dexparser-0.6.0 (c (n "dexparser") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "mutf8") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "012i4i815kbsl0xslv28bmm10hhs9wcnfxj74m4f7hrd2rf3rhi5")))

(define-public crate-dexparser-0.6.1 (c (n "dexparser") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "mutf8") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "03r4pdxs5axbb0l3shm8d66d6rlbhzmwcq0sfxanmkabq5033wln")))

(define-public crate-dexparser-0.6.2 (c (n "dexparser") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "mutf8") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1x8zjrgxdzjnkxxryi00ck85vjcz0kz9jili65qn49yiagaq6qgr")))

