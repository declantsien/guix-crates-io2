(define-module (crates-io de to detours-sys) #:use-module (crates-io))

(define-public crate-detours-sys-0.1.0 (c (n "detours-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "winapi") (r "^0.3.7") (f (quote ("synchapi" "processthreadsapi" "sysinfoapi"))) (d #t) (k 2)))) (h "13acqw39801025s84c6k47vm8jii5ygdw08avq41rpl3xc9yza11") (f (quote (("default") ("buildtime_bindgen" "bindgen")))) (y #t) (l "detours")))

(define-public crate-detours-sys-0.1.1 (c (n "detours-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "winapi") (r "^0.3.7") (f (quote ("synchapi" "processthreadsapi" "sysinfoapi"))) (d #t) (k 2)))) (h "1f0b523n4vmj7qaps2hl6z070590qxlaka6cijjfb02mx4y4mk95") (f (quote (("default") ("buildtime_bindgen" "bindgen")))) (y #t) (l "detours")))

(define-public crate-detours-sys-0.1.2 (c (n "detours-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "winapi") (r "^0.3.7") (f (quote ("synchapi" "processthreadsapi" "sysinfoapi"))) (d #t) (k 2)))) (h "11qmrmsvsmyisby4fksh40lkhz4wyhii67v6macgmq5jm1j9s20s") (f (quote (("default") ("buildtime_bindgen" "bindgen")))) (l "detours")))

