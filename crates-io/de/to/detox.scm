(define-module (crates-io de to detox) #:use-module (crates-io))

(define-public crate-detox-0.1.0 (c (n "detox") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "10932r0v85hk01wy8xxiwa5rxclp0drfdf7x595vphz0qi4aafkq")))

(define-public crate-detox-0.1.1 (c (n "detox") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1syx9bn0k6rwj5id9fqxz9ixn8qpw8pcpfm6nb2ikj5ar7cyvmpr")))

(define-public crate-detox-0.1.2 (c (n "detox") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "085sayl1fy449b07q5hqasvl9gfx43ksrgjx2ckwgx532b3drjwz")))

