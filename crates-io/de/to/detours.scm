(define-module (crates-io de to detours) #:use-module (crates-io))

(define-public crate-detours-0.1.0 (c (n "detours") (v "0.1.0") (d (list (d (n "detours-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0xlwlqmx23503nf4q1h2zc8x15g5d33w46p6z2xrqyk04rmwg7ca") (f (quote (("default") ("buildtime_bindgen" "detours-sys/buildtime_bindgen")))) (y #t)))

