(define-module (crates-io de to detour-sys) #:use-module (crates-io))

(define-public crate-detour-sys-0.1.0 (c (n "detour-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "0x2fqviwqi21z3xn15r7alw0pa3783km9wpjzd9f07nrq5xfdxmp")))

