(define-module (crates-io de to detours-rs) #:use-module (crates-io))

(define-public crate-detours-rs-0.1.0 (c (n "detours-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "windows") (r "^0.48") (f (quote ("Win32_System_Threading"))) (d #t) (k 0)))) (h "1fgiqhbc09w9ibahffjfj7pqm7gyq0s4ka8aj7x6a9k00l6drm8p")))

(define-public crate-detours-rs-0.2.0 (c (n "detours-rs") (v "0.2.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "07s03zq4xvmi7zpf7aahg7zicp67midnnjmiajclgwihjnixjlaz")))

