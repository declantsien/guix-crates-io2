(define-module (crates-io de tr detrojt) #:use-module (crates-io))

(define-public crate-detrojt-0.1.0 (c (n "detrojt") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0jnkqiwiny8az70pl3cmzgzfxllasl8mmxmrffj91rx919zx3dbh")))

(define-public crate-detrojt-0.1.1 (c (n "detrojt") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1m9h6zv0nwy6x0psyxhq6k3s6frfr8yzfa1mdr1asj3gi9ywd00c")))

