(define-module (crates-io de tr detrim) #:use-module (crates-io))

(define-public crate-detrim-0.0.0 (c (n "detrim") (v "0.0.0") (h "01cpk7j0fndnpff05j5vdmrrrhdij0jnvyw7fnhiad8j70cv23bq") (y #t)))

(define-public crate-detrim-0.1.0 (c (n "detrim") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "070lxs4sfcqy9dja21s855m1cq5k2zlk446c74cp2rrni60ndhp0") (y #t) (r "1.56.1")))

(define-public crate-detrim-0.1.1 (c (n "detrim") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0p4wg124i03nfr3rvi2q6v5wifcywncd2vd9hdg6ijrz2bgi293d") (y #t) (r "1.56.1")))

(define-public crate-detrim-0.1.2 (c (n "detrim") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nhq8kn9jlm9ad03jikh6cj9lknisss0l3a4763ag2jwpcxq9653") (f (quote (("std") ("default" "std")))) (r "1.56.1")))

(define-public crate-detrim-0.1.3 (c (n "detrim") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1lj31rk0pidgsp641d7r9gxva1h150s5sw8lvdyjfwr94v27vfzd") (f (quote (("std") ("default" "std")))) (r "1.56.1")))

