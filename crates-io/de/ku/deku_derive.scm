(define-module (crates-io de ku deku_derive) #:use-module (crates-io))

(define-public crate-deku_derive-0.1.1 (c (n "deku_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rprw3sjqnyak7idw8s7gaw81z3hh8cfxz9blqbc69iqic9l94g8") (y #t)))

(define-public crate-deku_derive-0.1.2 (c (n "deku_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vwmd31nf973jmxdrs3mbnqmj7d2kmpc9ri4fp4k4972hqlj2ag2") (y #t)))

(define-public crate-deku_derive-0.1.4 (c (n "deku_derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01dbsd75xbwr6sxz6mly60s0icl19pv1512hr4b9kfa0d6xpw46g") (y #t)))

(define-public crate-deku_derive-0.2.0 (c (n "deku_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v05yrvivwwg6sxkl43wch6zi8n3sgr05f05pljlq899v9rnbbb2") (y #t)))

(define-public crate-deku_derive-0.3.0 (c (n "deku_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xvqyc0b0c271xaqp30glk19pijhcs0rs9gsvffin4pprak94rl5") (y #t)))

(define-public crate-deku_derive-0.3.1 (c (n "deku_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v88g85xnal0f92pxx4j1ckpqs0fp6q83g6px67fb5j633kj7igr") (y #t)))

(define-public crate-deku_derive-0.4.0 (c (n "deku_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05l34dpgdh2yms3aymw8igkky4sjqnyrjgml659vivvkhg33v82w") (y #t)))

(define-public crate-deku_derive-0.4.1 (c (n "deku_derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08dzsa720kjriwiw74pav49llzcx18mav46fqrjpgzcbsr6532f2") (y #t)))

(define-public crate-deku_derive-0.5.0 (c (n "deku_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "139n11a5xxs8y6xhdla9h4fpca565qhl92m88r32akng619q5gaw")))

(define-public crate-deku_derive-0.5.1 (c (n "deku_derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vsm2sqd3m23kmfnwrhz8xljpsdq1518paz8sxdi3c4c2c7arj4r")))

(define-public crate-deku_derive-0.5.2 (c (n "deku_derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "105ykbzqfjy268f5165xqc1fzb088mwfsms16c6y6018z9x99w90")))

(define-public crate-deku_derive-0.6.0 (c (n "deku_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19d2wz4pfi21vbbfi34f4ncrnhrn0xs12hiy4plxz8c2bw6sxvln")))

(define-public crate-deku_derive-0.6.1 (c (n "deku_derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ykr97gz8jligvk85rawizxhx0fgfnfs31cf7lq696s2vjqfb8ka")))

(define-public crate-deku_derive-0.7.0 (c (n "deku_derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nmgjb3yy5y5pni14pb7aadhvrzr9621b76jrc7ljnklbb6y4kxr")))

(define-public crate-deku_derive-0.7.1 (c (n "deku_derive") (v "0.7.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vw18li6ch2imnkr5h231h7xlpfs1qfyz5nb3zfgmkjcpsda25vh")))

(define-public crate-deku_derive-0.7.2 (c (n "deku_derive") (v "0.7.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i2dg8zalzw2dj35rnjcpfl5g65rymp9ibxxdifx8d7j5j1dalz6")))

(define-public crate-deku_derive-0.8.0 (c (n "deku_derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kykr2mmrykq1qqlbgxzims78lq98hsygksva9kja7l7w8ayqgbj")))

(define-public crate-deku_derive-0.9.0 (c (n "deku_derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1spkz6shhwlk8cn1w4r4r0zzmjdkypsv6i0cw52r8wl3zn6q610w")))

(define-public crate-deku_derive-0.9.1 (c (n "deku_derive") (v "0.9.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rhkwm9cwp1namlkz4brrrr9wmdhcyskkin201s9y1h65ifgrm71")))

(define-public crate-deku_derive-0.9.2 (c (n "deku_derive") (v "0.9.2") (d (list (d (n "darling") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "059yn8rapldccckbrxi1a2jihsg10jsrwlxp3732ay086nxvgxgp")))

(define-public crate-deku_derive-0.9.3 (c (n "deku_derive") (v "0.9.3") (d (list (d (n "darling") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zwnkaz153ny1zmn8y6n4c11zwcpmd8gf1yxcyz8pkw0whqmil4f")))

(define-public crate-deku_derive-0.10.0 (c (n "deku_derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ilqmyv3zny3whk9bp54z9w2glgswn67a5wpm3p3vq4cd4gx4avw")))

(define-public crate-deku_derive-0.10.1 (c (n "deku_derive") (v "0.10.1") (d (list (d (n "darling") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d9w014hbyi1nviw58scipl8zz18bryqxwy3jiwh2qs6gbkwh96c")))

(define-public crate-deku_derive-0.11.0 (c (n "deku_derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0za1dk3sbwhhy70j5b1m2zyh4w58v006cmbly9lm499k2ij2x3wh") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.0 (c (n "deku_derive") (v "0.12.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s6wh2j6mikcq6g979v5xj5clm601djyq1198m1s5dcncr2pcvw4") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.1 (c (n "deku_derive") (v "0.12.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n1zp5q06xzskncc4lcd1a2r82h77iy7fzyx2h5afhy0f2a21y43") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.2 (c (n "deku_derive") (v "0.12.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1969sr4n0lyqcvqjf1y1706lyc6a88zwjcg527mpflqfphmbcxgf") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.3 (c (n "deku_derive") (v "0.12.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x51xf4n9vsm8iywavh13p9qv0nvgfxnxcaxvlxb01zrdqbagvv6") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.4 (c (n "deku_derive") (v "0.12.4") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15rp0jkd50p4cp3rjhpc63z1w9vdndfph73bxzffirkz0vwdrja8") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.5 (c (n "deku_derive") (v "0.12.5") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1871v80d6vbvzv954yyzp6xs09jcw7n0yapaklccrffnqpsz1xwv") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.13.0 (c (n "deku_derive") (v "0.13.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05kvgr2z1wghsl2spkqwqdi39y08qr3aa9q1vy73cnzzpr0bnjrw") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.13.1 (c (n "deku_derive") (v "0.13.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0whghxrzp451zy47rz9ji8pcpbihnss9icwqircpmj6iris45ysd") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.12.6 (c (n "deku_derive") (v "0.12.6") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mqqqgibyz4nl52lqqgd5zqg32l62l716isvgx8s9m5acjqs07k9") (f (quote (("std" "proc-macro-crate"))))))

(define-public crate-deku_derive-0.14.0 (c (n "deku_derive") (v "0.14.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k34j7xpkqhgw47bwyr9qmxcyzraapnl27nlhfgkm6rgrryd6hx0") (f (quote (("std" "proc-macro-crate") ("logging"))))))

(define-public crate-deku_derive-0.14.1 (c (n "deku_derive") (v "0.14.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r8735zx9nfzvivkfi4zwnlmwj9xxj2rwksq4cx8adzhwdp0whxa") (f (quote (("std" "proc-macro-crate") ("logging"))))))

(define-public crate-deku_derive-0.15.0 (c (n "deku_derive") (v "0.15.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vz8xp4y5ba6v7abnlrzlm4lkmm650pdclvc8abq9xx97zkn2w6p") (f (quote (("std" "proc-macro-crate") ("logging"))))))

(define-public crate-deku_derive-0.15.1 (c (n "deku_derive") (v "0.15.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03c48wz78mal0x64ppf6hhn62199n7wwgj79kk23skbkm6fhj2ch") (f (quote (("std" "proc-macro-crate") ("logging"))))))

(define-public crate-deku_derive-0.16.0 (c (n "deku_derive") (v "0.16.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17h0wn3xipgdrg312dd2z0aaks6pfv3xfjm7aaiib4i3f8js2b2f") (f (quote (("std" "proc-macro-crate") ("logging"))))))

(define-public crate-deku_derive-0.17.0 (c (n "deku_derive") (v "0.17.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04v9336hqn5ivvrqka1zlygmjc6j6bfqs7740crfi79zz5rljlyp") (f (quote (("std" "proc-macro-crate") ("no-assert-string") ("logging")))) (r "1.71")))

