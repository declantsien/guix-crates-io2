(define-module (crates-io de br debris) #:use-module (crates-io))

(define-public crate-debris-0.1.0 (c (n "debris") (v "0.1.0") (h "1s96bhb8hkqrmzfqhnax33i79bnfis4mkif1bv07kzb7sqlh4hp5")))

(define-public crate-debris-0.1.1 (c (n "debris") (v "0.1.1") (h "0kqvnxbsmnb17vang6ziv62amg0gga76ja4pp2dli5i80h8lhp02")))

