(define-module (crates-io de br debra-common) #:use-module (crates-io))

(define-public crate-debra-common-0.1.0 (c (n "debra-common") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "reclaim") (r "^0.2.0") (k 0)))) (h "0yy8na8hf58j7g9brrmq82wfiqkg7h2mwy42dh7bzp64h5sgrfvd") (f (quote (("std" "arrayvec/std" "reclaim/std") ("default" "std"))))))

(define-public crate-debra-common-0.1.1 (c (n "debra-common") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "reclaim") (r "^0.2.1") (k 0)))) (h "15a6182na29xg0gjh9q7bg0hf455awrx0qs0d4jbzrqgfj0vmsjx") (f (quote (("std" "arrayvec/std" "reclaim/std") ("default" "std"))))))

