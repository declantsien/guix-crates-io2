(define-module (crates-io de gi degit) #:use-module (crates-io))

(define-public crate-degit-0.1.0 (c (n "degit") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0wcddvykj04bn95jfwfcimwgjgp7im6jpl8dbj6faj523s1ffx0s")))

(define-public crate-degit-0.1.1 (c (n "degit") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "1hgwbavrbxclm9d04p7z4gdc7a6kwsly62j3ak1kxbcn8gw1xmax")))

(define-public crate-degit-0.1.2 (c (n "degit") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "07fh1rmjasy4ic6ifj2cz5ddxb6fy0mgydzfcjikc1ysnp45nqsc")))

(define-public crate-degit-0.1.3 (c (n "degit") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "146hi3nfn5cz100ndarqk37xcvz74yqswmykj2izqmkrk7w88dnb")))

