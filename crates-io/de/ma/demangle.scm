(define-module (crates-io de ma demangle) #:use-module (crates-io))

(define-public crate-demangle-0.1.0 (c (n "demangle") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symbolic") (r "^12.8.0") (f (quote ("demangle"))) (k 0)))) (h "1x5y0kz19jaridhh4s4j3f0vr5lhfzxm0izr1wy3s5jfmgbqizgh")))

(define-public crate-demangle-0.1.1 (c (n "demangle") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symbolic") (r "^12.8.0") (f (quote ("demangle"))) (k 0)))) (h "1xk5vk6lfg44snsvd29ray7b53cv76dmzxbvd0ff8ny0ac1grrha")))

(define-public crate-demangle-0.1.2 (c (n "demangle") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symbolic") (r "^12.8.0") (f (quote ("demangle"))) (k 0)))) (h "1fxcq9z4n8az00wpfks3gwc2hpw9y00av33m2nn0b8vgna1y5yn9")))

