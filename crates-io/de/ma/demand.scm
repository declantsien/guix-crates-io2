(define-module (crates-io de ma demand) #:use-module (crates-io))

(define-public crate-demand-0.1.0 (c (n "demand") (v "0.1.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "09d893ms30dnq5l3174js4mlvpydv2cmzmngcsxrmp1hm6la4qhd")))

(define-public crate-demand-0.1.1 (c (n "demand") (v "0.1.1") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0glpa9g0dwh2crvn4vk3nq5708ngb4x6km5nli410c73xn23jsy7")))

(define-public crate-demand-0.2.0 (c (n "demand") (v "0.2.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0ca9ciybdp8gxxf4ss1jigqgc3h086cypvl2nd7rdddj45m7pix5")))

(define-public crate-demand-0.3.0 (c (n "demand") (v "0.3.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "03bzvn6s3gnsvc2ahaq33sxcj78dz05hbw3752jzp7py0y8ijj6q")))

(define-public crate-demand-0.4.0 (c (n "demand") (v "0.4.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0kc8ihkf2k5ah014a4cr69v5jh7339sxzz6xhc0rhkmi2zpsd3s2")))

(define-public crate-demand-1.0.0 (c (n "demand") (v "1.0.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "02pjbdb7z96jwrhxnhbwsp5w6vxv6ai3raqb0pzdavkbl83ha3c3")))

(define-public crate-demand-1.0.1 (c (n "demand") (v "1.0.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0m0a9xvy8x7bh7jp1g1dv93mg5fn03g2wvf8fxypanaz6fix3c8i")))

(define-public crate-demand-1.0.2 (c (n "demand") (v "1.0.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0bi2pvxnl6gkp9vl1n6ix8ch2xak2nmn89hgrvkm6kwrxy0dznh5")))

(define-public crate-demand-1.1.0 (c (n "demand") (v "1.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "130i3q1r2zpjv3xc9j1dk0n6rbiv915zjzgi0vz0kf94madp6ch4")))

(define-public crate-demand-1.1.1 (c (n "demand") (v "1.1.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1jy3hn5xls9j7fis9rwc99acg99722dxjnawcad38x99c4sgw0xb")))

(define-public crate-demand-1.1.2 (c (n "demand") (v "1.1.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0jw4aflaj98v12qshnjikbmgmhh43wrwp95l5b1zjk7zg0ys38fd")))

(define-public crate-demand-1.2.0 (c (n "demand") (v "1.2.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1wfx24w59lhg7j51a3l7x2phbvz1j90slq7nkis5s42cbqdca456")))

(define-public crate-demand-1.2.1 (c (n "demand") (v "1.2.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "00m81si61nr5sqzzncahcg1hqj1i78hbhhvyrhcaykfll8ajcw42")))

(define-public crate-demand-1.2.2 (c (n "demand") (v "1.2.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "09wysc4k1r7xgjk6bbvnmpg3fmnjfw2p4vimdjzw3riwb80ha3mc")))

(define-public crate-demand-1.2.3 (c (n "demand") (v "1.2.3") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "02qdylzb94yzg3fm7pvwj1asbmjs3ql97ddblavdifx68iwasryi")))

