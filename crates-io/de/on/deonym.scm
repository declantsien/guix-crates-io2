(define-module (crates-io de on deonym) #:use-module (crates-io))

(define-public crate-deonym-0.0.0 (c (n "deonym") (v "0.0.0") (h "0ynn59435kz62v7pvd2grlhv7vm9pzq0wwc8wdzh0mxay1dbanas") (y #t)))

(define-public crate-deonym-0.1.0 (c (n "deonym") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)))) (h "1wjhppx61rzj0sqyn6g0iivpkx0scj5xfhjgjj0wjzj3yky5q9c1") (y #t)))

(define-public crate-deonym-0.1.1 (c (n "deonym") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)))) (h "1g2aljddxp2dgqnv0lb210dlxynb2fb22iq8hn1zf1ynp8jm4hbz") (y #t)))

(define-public crate-deonym-0.1.2 (c (n "deonym") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)))) (h "0rbckv3xyy54i6fzy92x5bgcl8jq0kzssnbkmn1bb3x9zjsb5sch") (y #t)))

