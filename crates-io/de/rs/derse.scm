(define-module (crates-io de rs derse) #:use-module (crates-io))

(define-public crate-derse-0.1.0 (c (n "derse") (v "0.1.0") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "1hqn8zqy7m226v0sl1pdqqwdv9vs5ifbsrlrvn3rrh568bnqimak")))

(define-public crate-derse-0.1.1 (c (n "derse") (v "0.1.1") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "03gbxg47xq5aajgigra1c6rsvvh047s8kq21vh3y1q07bbl8yf6b")))

(define-public crate-derse-0.1.2 (c (n "derse") (v "0.1.2") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "0l6msf773l9agmmq6ckk2l2zq5y88gs2fjz3lkdcg0vxar1xdcdv")))

(define-public crate-derse-0.1.3 (c (n "derse") (v "0.1.3") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "1frc38kah7fc5d2nnw4mxp40fzlxi8awxaz2dxz69naagb0sg8aj")))

(define-public crate-derse-0.1.4 (c (n "derse") (v "0.1.4") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "1xyaas7mjp1fnamnfkw87ipkwkj3c1spz4mwi2rri3yplxg0di5g")))

(define-public crate-derse-0.1.5 (c (n "derse") (v "0.1.5") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "0nyvrh1qwcdhama8px8crp0bjp6fd3wjhp1xmv18v0bblql5b8sp")))

(define-public crate-derse-0.1.6 (c (n "derse") (v "0.1.6") (d (list (d (n "derse-derive") (r "^0") (d #t) (k 0)))) (h "1vrxhvphxym1razq48hp4r8hrjsnixqg0wjm0wkhjad1n0ng3cx5")))

(define-public crate-derse-0.1.7 (c (n "derse") (v "0.1.7") (d (list (d (n "derse-derive") (r ">=0.1.5") (d #t) (k 0)))) (h "15224mw3snnq872r3wwg85fvw8cdh93g2p6l1jvgz3c037j71vpr")))

