(define-module (crates-io de rs derse-derive) #:use-module (crates-io))

(define-public crate-derse-derive-0.1.0 (c (n "derse-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ld9wxp417wyb0w8lz3ngpxj01p885ggrbq1ks47s6xcj1j8cnl4")))

(define-public crate-derse-derive-0.1.1 (c (n "derse-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1jm6ds21fykpg5inlyh7p82bas08drsqq4hbqq7id4hd60is9d5l")))

(define-public crate-derse-derive-0.1.2 (c (n "derse-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1yy1ybmvgh9hf3p9pishv307xyi49npax6h97w86vfs1w13gs0r4")))

(define-public crate-derse-derive-0.1.3 (c (n "derse-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1lxw6077is6n9a6brwld3pjx768w00g7yxiqzq5pbvl60bgb81r8")))

(define-public crate-derse-derive-0.1.4 (c (n "derse-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1g0rylg12hv9ws1hsifvxnb5rs35cb8saly6dq6apj8fsipwhg7r")))

(define-public crate-derse-derive-0.1.5 (c (n "derse-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1s50rm9gz9qinzmh4c0y55iqw9dxdgvcvx9ndfikn8wkpqr3icv0")))

