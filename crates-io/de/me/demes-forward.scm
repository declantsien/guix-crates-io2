(define-module (crates-io de me demes-forward) #:use-module (crates-io))

(define-public crate-demes-forward-0.1.0 (c (n "demes-forward") (v "0.1.0") (d (list (d (n "demes") (r ">0.2.1, <0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0baqy602hsjdpydnk22jic5ndn6srcq4x5dhrkcw5pk38swxld6m")))

(define-public crate-demes-forward-0.2.0 (c (n "demes-forward") (v "0.2.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "~0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "00a579f6zaz4nrigwyh3wkf17wwnsr3nvr8i7qaw4rjrxj0mfb25")))

(define-public crate-demes-forward-0.2.1 (c (n "demes-forward") (v "0.2.1") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "~0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0q5mdq72nb9yn1xig72inq2w5zp7vw4sn6alx17cc67vwa4g7ykk") (r "1.58.1")))

(define-public crate-demes-forward-0.3.0-alpha.0 (c (n "demes-forward") (v "0.3.0-alpha.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "1k3f8474wxii7hlcmkzqxyy6s3vf5g3afgm1ca9j2xsfald0qly5") (r "1.58.1")))

(define-public crate-demes-forward-0.3.0-alpha.1 (c (n "demes-forward") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "19ksrb85ibam5bpzf1nkmq71jiwpmm7q3wfrqng3ryqcppiflsk2") (r "1.58.1")))

(define-public crate-demes-forward-0.3.0 (c (n "demes-forward") (v "0.3.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "0css9qkmf7w9xncgqrk8yq0ja7wdhk3gnhlydqj1dfh8fgp0ljd4") (r "1.58.1")))

(define-public crate-demes-forward-0.4.0 (c (n "demes-forward") (v "0.4.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "1awhy8vgmj24241vl304f0m17jdqhrw78wm5gn0q3w8y1432mclm") (r "1.60.0")))

(define-public crate-demes-forward-0.5.0 (c (n "demes-forward") (v "0.5.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 2)) (d (n "demes") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "~1") (d #t) (k 0)))) (h "1z29y3mr7psylvqyrsa9nb70m60i5a95zgw56r1f1d9m3ry0dfwp") (r "1.60.0")))

