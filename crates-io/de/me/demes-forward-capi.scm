(define-module (crates-io de me demes-forward-capi) #:use-module (crates-io))

(define-public crate-demes-forward-capi-0.1.0 (c (n "demes-forward-capi") (v "0.1.0") (d (list (d (n "demes-forward") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "09dc8cjv98msjndhc49pm9jb1w8mpbsqmbjpsh42xyc602qhdp32")))

(define-public crate-demes-forward-capi-0.2.0 (c (n "demes-forward-capi") (v "0.2.0") (d (list (d (n "demes-forward") (r "~0.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "14rgfhsf3xcphpm9d90y9fkywfcnd6s13qyqgbxwlcr9s6zqir4r")))

(define-public crate-demes-forward-capi-0.3.0 (c (n "demes-forward-capi") (v "0.3.0") (d (list (d (n "demes-forward") (r "~0.2") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1x7xri9l111qgphcr2fmlrpxqw2vaprkc0wdk8rwpjr43gpjyz8d")))

(define-public crate-demes-forward-capi-0.4.0-alpha.0 (c (n "demes-forward-capi") (v "0.4.0-alpha.0") (d (list (d (n "demes-forward") (r "^0.3.0-alpha.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1hawpp9h88h2y3gnyjkfzgz6kgm113snfqyydsg4z1k4pyja8jha")))

(define-public crate-demes-forward-capi-0.4.0-alpha.1 (c (n "demes-forward-capi") (v "0.4.0-alpha.1") (d (list (d (n "demes-forward") (r "^0.3.0-alpha.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1cnk0wgni7hi71gvdb1pyfzdld9vqq705k0jxzxr74s1ibg87r6h")))

(define-public crate-demes-forward-capi-0.4.0 (c (n "demes-forward-capi") (v "0.4.0") (d (list (d (n "demes-forward") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "13gn2h6qikgz269h55jsg5z2jbi87xd10bmay3cxm22mw2lcc4f0")))

(define-public crate-demes-forward-capi-0.4.1 (c (n "demes-forward-capi") (v "0.4.1") (d (list (d (n "demes-forward") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1b3aq2wjw6sr9nqlr8v71hi9kjj4wq48k39irgvlwyipnj74ws7s")))

(define-public crate-demes-forward-capi-0.5.0 (c (n "demes-forward-capi") (v "0.5.0") (d (list (d (n "demes-forward") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "02bfkwxrm55qdhjnjz8i2ir0d29y1r2gfszzqkc417f9aca0ccyb")))

