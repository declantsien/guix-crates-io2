(define-module (crates-io de me dementia) #:use-module (crates-io))

(define-public crate-dementia-0.0.1 (c (n "dementia") (v "0.0.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "12lpxwl3g8ac1ck4afzlknqnbsjaa4fxn6prg6icjaapd9r1ii9c")))

(define-public crate-dementia-0.0.2 (c (n "dementia") (v "0.0.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "0q7d9q2fd14iiyr1c2h7vvimla2qkz4whb47czjs9va8fa94cb9s")))

(define-public crate-dementia-0.0.3 (c (n "dementia") (v "0.0.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "0n0avs1nm6ndf5hawnnajiw1g833scjiyb9bkmnr35lm9chfv98c")))

(define-public crate-dementia-0.0.4 (c (n "dementia") (v "0.0.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0hv3x89ljfcmc02c58jw9id15aa001cjcqvl6pd1wsh8a14r71hx")))

(define-public crate-dementia-0.0.5 (c (n "dementia") (v "0.0.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "143isr6iicddggp0581i336ly53a874q91b126mjsgvw77mh584k")))

