(define-module (crates-io de ng dengine) #:use-module (crates-io))

(define-public crate-dengine-0.1.0 (c (n "dengine") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "dengine_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "deslite") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql") (r "^14.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "17942k14scrz99s9lbavig4h9y4a6vvkvnb70gbwrhlymhj9innc")))

(define-public crate-dengine-0.1.1 (c (n "dengine") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "dengine_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "deslite") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql") (r "^14.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "0n6xxaaysbh3gdkdjzd4vlpildf80ahy8mzdj6n71wql9phizqzm")))

(define-public crate-dengine-0.1.2 (c (n "dengine") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "dengine_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "deslite") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql") (r "^14.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "0aqhkspghx141aphzjy1bc5i72bk84zcnmrrj8npwi57jh6hbgbw")))

(define-public crate-dengine-0.1.3 (c (n "dengine") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "dengine_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "deslite") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql") (r "^14.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)))) (h "1qanbqg45mi3blbi9swqpfrik136fdb5l0j56bx4h1vr9sjv9229")))

