(define-module (crates-io de ng dengine_derive) #:use-module (crates-io))

(define-public crate-dengine_derive-0.1.0 (c (n "dengine_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "048l9cjwqp939xn0cj126i9kla2l6vjil4cyps0w9n13c2p1zcis")))

(define-public crate-dengine_derive-0.1.1 (c (n "dengine_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (d #t) (k 0)))) (h "1a3vncaw5b28rh5zrzz5wwy1g5xbpnb014gy468v52qkin47m1vj")))

(define-public crate-dengine_derive-0.1.2 (c (n "dengine_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)))) (h "01044980x5lbavr6qgdw5jm93y62dyrqam41fdrddn483fya7gpv")))

