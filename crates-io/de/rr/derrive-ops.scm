(define-module (crates-io de rr derrive-ops) #:use-module (crates-io))

(define-public crate-derrive-ops-0.1.0 (c (n "derrive-ops") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1gimrlsjdpgd5kcwd3qhrh02ghfsc85bkv71g0w0gyyddm9872mc") (y #t)))

