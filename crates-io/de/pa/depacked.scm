(define-module (crates-io de pa depacked) #:use-module (crates-io))

(define-public crate-depacked-0.1.0 (c (n "depacked") (v "0.1.0") (d (list (d (n "skiplist") (r "^0.4.0") (d #t) (k 0)))) (h "04ggqmxldyhp7cc4pw75lh0l8x5x3zdwylpq7axzs248sq3d33i3")))

(define-public crate-depacked-0.2.0 (c (n "depacked") (v "0.2.0") (d (list (d (n "skiplist") (r "^0.4.0") (d #t) (k 0)))) (h "00xmggsj7hcp33kiddpshxg8dxlaqk5zgagcr32r44l8s65a8w5y")))

(define-public crate-depacked-0.2.1 (c (n "depacked") (v "0.2.1") (d (list (d (n "skiplist") (r "^0.4.0") (d #t) (k 0)))) (h "1lh6qarl6wcbs1m9algn0yb59pjzd82fggxkz6svlsn3b9hgl991")))

(define-public crate-depacked-0.2.2 (c (n "depacked") (v "0.2.2") (d (list (d (n "skiplist") (r "^0.4.0") (d #t) (k 0)))) (h "1sz1ln8ybz9bphvvmjs8zcw62bbkscg1ycirm24nm2vbjaxlmlc4")))

(define-public crate-depacked-0.2.3 (c (n "depacked") (v "0.2.3") (d (list (d (n "skiplist") (r "^0.4.0") (d #t) (k 0)))) (h "01ay9rc98l07sx2r2cfl5nj2n6cwfbj01wfsjx6ns31zjpfhlkzw")))

