(define-module (crates-io de pa department) #:use-module (crates-io))

(define-public crate-department-0.1.0 (c (n "department") (v "0.1.0") (d (list (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0ac3wn6hkvhcybcslrgczrw6azqjzrmavjzsrqnk9jgy3yisdg9k") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.1.1 (c (n "department") (v "0.1.1") (d (list (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0wzkj2z3cyaww245hx8zi8gnx0vywr7jii7gkzhw74q7sg1lgxp3") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.1.2 (c (n "department") (v "0.1.2") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "13shbghzc0bqr1rwvrbm4k0dsp1phygqzhnhz8r6i77p3v3pd2s2") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.2.0 (c (n "department") (v "0.2.0") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "1x4hbcb5q91zbvdnki9564fls9wgwrflams31wnng34d4asqnxr0") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2.1 (c (n "department") (v "0.2.1") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "1p2lpy33132k6s4yhb3m09fqp456f80zhl74vbkqwvqkg8k44csf") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2.2 (c (n "department") (v "0.2.2") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "1rxyaibzkzsafmcfk32klakxig85jhyqj7sxjxs5chfjma27xif1") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2.3 (c (n "department") (v "0.2.3") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "0r0xkysjyzg0czbn0n8azr5fp0qm68biby2wymw58p7v3vvsbrcg") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2.4 (c (n "department") (v "0.2.4") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "0kp809c2bhc10h15r99m40vaial5npz7f18ln303hqnkp4ywwl1k") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2.5 (c (n "department") (v "0.2.5") (d (list (d (n "spin") (r "^0.9") (f (quote ("spin_mutex" "mutex"))) (k 0)))) (h "12qmxsk232zgq04iaaxf32zpwkif9x37hgavynqcbbi00dwi3hqm") (f (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

