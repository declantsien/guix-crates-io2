(define-module (crates-io de p3 dep3) #:use-module (crates-io))

(define-public crate-dep3-0.1.0 (c (n "dep3") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deb822-lossless") (r "^0.1.6") (d #t) (k 0)))) (h "184qnhay1h0vakdg4cfnnib5y91di5fc4arba5rcyx54la3mkp5z")))

(define-public crate-dep3-0.1.1 (c (n "dep3") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deb822-lossless") (r "^0.1.6") (d #t) (k 0)))) (h "0dd362wrcfcnj3iz9w9a9vv4a591fbbzrf1mvd32n9rv8ml4fc7q")))

(define-public crate-dep3-0.1.2 (c (n "dep3") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deb822-lossless") (r "^0.1.6") (d #t) (k 0)))) (h "1is3whmyayfca7l8vdwlmdvnx5ag0n7nnig1jcdhg5kzq4xlpin0")))

