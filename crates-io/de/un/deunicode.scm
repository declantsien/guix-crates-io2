(define-module (crates-io de un deunicode) #:use-module (crates-io))

(define-public crate-deunicode-0.4.0 (c (n "deunicode") (v "0.4.0") (h "1ih3lksc7pw95nq7mcs8lwxh2qmifdwvcw436x3vn4n0j2v73aa8") (y #t)))

(define-public crate-deunicode-0.4.1 (c (n "deunicode") (v "0.4.1") (h "1pjwpih0dam844sh0yyjcfj7h1c5z7h03vjpv34azlh7g3c362wm") (y #t)))

(define-public crate-deunicode-0.4.2 (c (n "deunicode") (v "0.4.2") (h "0kxq7jmiwz4fs9z2jf65dyrc4n0ba82ican3v8q82brx0crb8a2w") (y #t)))

(define-public crate-deunicode-0.4.3 (c (n "deunicode") (v "0.4.3") (h "146nc3wlwz2j9awwf7zf76qvjcahnls0mlv9jm6clcvr9dlph245") (y #t)))

(define-public crate-deunicode-1.0.0 (c (n "deunicode") (v "1.0.0") (h "0q3mhnz4mzhi088h60n5n7i6ibw2wacbj687bmh61ppdpmdhz2na") (y #t)))

(define-public crate-deunicode-1.1.0 (c (n "deunicode") (v "1.1.0") (h "1z534mxyndah3xphdy30c7p6r9c4f8vlfaq4qidlcfbra4ddwz9h") (y #t)))

(define-public crate-deunicode-1.1.1 (c (n "deunicode") (v "1.1.1") (h "1ccd1d7bjr9z47njpsd4khnya9mywhwhli62h7hr2i70zlnml4c0") (y #t)))

(define-public crate-deunicode-1.2.0 (c (n "deunicode") (v "1.2.0") (h "1diyq2bhk9z21abygvmf28p2h6rincs3nsd33032aadpdrnpbdy0")))

(define-public crate-deunicode-1.2.1 (c (n "deunicode") (v "1.2.1") (h "0p4lwpb2371qw45k2hpqggqxmvyb6lr92717d3gyx0mnp0rxsi10")))

(define-public crate-deunicode-1.3.0 (c (n "deunicode") (v "1.3.0") (h "1wpqgdfhssaqd25hvm898vgmc3x7wv5y1vcy70balr4zcdfpfdvz")))

(define-public crate-deunicode-1.3.1 (c (n "deunicode") (v "1.3.1") (h "1pmfcndh6vx8bfpm5826h9rmqq8iclmfkvpnlww1dpz72mp77jgj")))

(define-public crate-deunicode-1.3.2 (c (n "deunicode") (v "1.3.2") (h "1mhw4rv748fz42mfibw39p99y9r36zfbbs64kgjja9nlh126mzq8") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-1.3.3 (c (n "deunicode") (v "1.3.3") (h "0spkk1f8ncz0z6pmdfiamqcr237igg550gv55g8m6jks497vl6wc") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-0.4.4 (c (n "deunicode") (v "0.4.4") (h "0hrfp55qxvx5n8niw4d60pr7zkm0g7wc01r5a1d221h9lnk06lnr") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-1.4.0 (c (n "deunicode") (v "1.4.0") (h "058bckh3q26d8k3xh374vw6y4srlm4lnvvr7fzxf8wf33fg7f3c9") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-0.4.5 (c (n "deunicode") (v "0.4.5") (d (list (d (n "deunicode1") (r "^1") (o #t) (k 0) (p "deunicode")))) (h "133m63d7x3y3kpf8hrdkjpqma9l74bv59rpmpb8rqgn2i6zz3nvi") (f (quote (("default" "alloc")))) (s 2) (e (quote (("force-upgrade" "dep:deunicode1") ("alloc" "deunicode1?/alloc"))))))

(define-public crate-deunicode-1.4.1 (c (n "deunicode") (v "1.4.1") (h "1rmxjv7460j1bwbfgv84wgy53cbcc1ajpzb4kzjmnib1v3sbl6ka") (f (quote (("default" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-deunicode-1.4.2 (c (n "deunicode") (v "1.4.2") (h "0nj8pycs1idacgraqpgc2lwihawb945pirlsnx047iy5fd9s7qis") (f (quote (("default" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-deunicode-1.4.3 (c (n "deunicode") (v "1.4.3") (h "156zkxmlfm1qg84x8jaqbj3mpc86lzwqiv0xr2w9di2ncw959s5n") (f (quote (("default" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-deunicode-1.4.4 (c (n "deunicode") (v "1.4.4") (h "0khx3356c984ycplqailbdm2fybza6aj5szhwql3drj48w4z0bij") (f (quote (("default" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-deunicode-1.6.0 (c (n "deunicode") (v "1.6.0") (h "006gnml4jy3m03yqma8qvx7kl9i2bw667za9f7yc6k9ckv64959k") (f (quote (("default" "alloc") ("alloc")))) (r "1.64")))

