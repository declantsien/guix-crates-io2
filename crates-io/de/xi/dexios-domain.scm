(define-module (crates-io de xi dexios-domain) #:use-module (crates-io))

(define-public crate-dexios-domain-0.0.0 (c (n "dexios-domain") (v "0.0.0") (h "0ccagndac4nklyyi9a402rrcp1wrsk2qkvylwbkv3kjknlw59q32")))

(define-public crate-dexios-domain-1.0.0 (c (n "dexios-domain") (v "1.0.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "dcore") (r "^1.2.0") (d #t) (k 0) (p "dexios-core")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("zstd"))) (k 0)))) (h "1drxf5jdirgd6p8icdbn6c1jlfaswxqcav0k9hja4ib7jcvmd2fm")))

(define-public crate-dexios-domain-1.0.1 (c (n "dexios-domain") (v "1.0.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "core") (r "^1.2.0") (d #t) (k 0) (p "dexios-core")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("zstd"))) (k 0)))) (h "1lscixbzl77ka17m1xbzf1lhhjxxgqnvg7py3k073j90nz9xpzvp")))

