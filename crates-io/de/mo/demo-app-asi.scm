(define-module (crates-io de mo demo-app-asi) #:use-module (crates-io))

(define-public crate-demo-app-asi-1.0.0-alpha.1 (c (n "demo-app-asi") (v "1.0.0-alpha.1") (h "1mi7w7cl48rfa42wfwaa8mafg2n7v35gpsn12fcw95dlvp5ajp78") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.2 (c (n "demo-app-asi") (v "1.0.0-alpha.2") (h "1rbqbzr0znp90z2xlvdd8wr14ps3wb62s2r821yykz4kd83azvdj") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.3 (c (n "demo-app-asi") (v "1.0.0-alpha.3") (h "05gwnj8ax48a8ss5b25x7d8li2a48ip76jc8wf6h4w8dpynwwczs") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.4 (c (n "demo-app-asi") (v "1.0.0-alpha.4") (h "1qc27dbfyfcjg2swpca711n2ipzxnfynfi9d4lxbldfwa5gsp52x") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.5 (c (n "demo-app-asi") (v "1.0.0-alpha.5") (h "0gmgp1y7pqspb1002w3r064rv1a6ckl3r2sb2i5clgxkl281i6gx") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.6 (c (n "demo-app-asi") (v "1.0.0-alpha.6") (h "01pjf471aj42dalg2jqijxvkllpf15gyyay6ybdd7rr82wz3czxi") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.7 (c (n "demo-app-asi") (v "1.0.0-alpha.7") (h "0f3rqwdjjn8bshz8vhgcfijgwzpsmr1r89sc4dqxi8gabblf0i5i") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-beta.1 (c (n "demo-app-asi") (v "1.0.0-beta.1") (h "0yjijy0l5famlimzl7k03za1nw8nbw5bah2v3ixkswa2yxz78h3k") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0 (c (n "demo-app-asi") (v "1.0.0") (h "06dzvfimhg41m846biypyq8rvpn94qv8dgfcbw34ajqb3vgl1mj2") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.8 (c (n "demo-app-asi") (v "1.0.0-alpha.8") (h "16liiiwlq0cbsaxfb1b9il9nswvpfrylscbvxmq8rxiw3436g17l") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-beta.2 (c (n "demo-app-asi") (v "1.0.0-beta.2") (h "1iy78n3fxjaw2d6ng3j7qvlf47qd69djdws5n0i814885nq43rm5") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.9 (c (n "demo-app-asi") (v "1.0.0-alpha.9") (h "1934nfl6faydvgw24w002m5fq2qkj9c0lgrks1074aww93da2jpr") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.10 (c (n "demo-app-asi") (v "1.0.0-alpha.10") (h "1l2cg6f7wndbzfmdqd41br4km2gz2gp0djgnj8hc8rd8vw2qp3sz") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-beta.3 (c (n "demo-app-asi") (v "1.0.0-beta.3") (h "15jl61byghp6x864nzq64dngx31diijls8g3q0aj9kbnl7qhnphi") (r "1.64.0")))

(define-public crate-demo-app-asi-1.1.0 (c (n "demo-app-asi") (v "1.1.0") (h "1mdyxrknj3gdyk6q4jcwsn4a87iacnvj93qq268vxdlsf18863x7") (r "1.64.0")))

(define-public crate-demo-app-asi-1.0.0-alpha.11 (c (n "demo-app-asi") (v "1.0.0-alpha.11") (h "0qs44935pbm2w8a4yyysqijjjz37v77n6jk0a4cwpl8xry3ch4nb") (r "1.64.0")))

(define-public crate-demo-app-asi-1.2.0-beta.1 (c (n "demo-app-asi") (v "1.2.0-beta.1") (h "0cjj8idih3qdr41k5ip7mv82mi0l34yhhcbj191yywqs00940yqr") (r "1.64.0")))

(define-public crate-demo-app-asi-1.2.0-alpha.1 (c (n "demo-app-asi") (v "1.2.0-alpha.1") (h "10qbg51f8a7dds1wy5j4pjn2h6db327g9rymc7p9ri48sfjr00d7") (r "1.64.0")))

(define-public crate-demo-app-asi-1.2.0 (c (n "demo-app-asi") (v "1.2.0") (h "169cwj4kjaym3hdvlcsvbwzmkvnpw59z7pn881z0z606hg3pb5s5") (r "1.64.0")))

(define-public crate-demo-app-asi-1.3.0 (c (n "demo-app-asi") (v "1.3.0") (h "0wfpx7ixklw3msh1hl7jadw5hfwvq0fvca2h34kcrdvgbdjz1ggz") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.0-alpha.1 (c (n "demo-app-asi") (v "1.4.0-alpha.1") (h "0di2krlxh8k5rfhgi6y1hq6dxsav7k6s6qpd9c5nzbr14wdvnynb") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.0-beta.1 (c (n "demo-app-asi") (v "1.4.0-beta.1") (h "1wn08vg0j71zbffmg0y4g7lj0agqpdz6a48nd700gppvkdw3yc7s") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.0 (c (n "demo-app-asi") (v "1.4.0") (h "18fzs0jighag6fl6hralvz88s9xbw4vzh7y061qfb4r4irr342r7") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.1-alpha.1 (c (n "demo-app-asi") (v "1.4.1-alpha.1") (h "00h4a2klkvzdvki557zs6m33iqwkw0kzpqjyqq93gw698lxwblv8") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.1-beta.1 (c (n "demo-app-asi") (v "1.4.1-beta.1") (h "1z3dq4wp4s6ynf46lc2a9qa92gs21kck3i7akb5gpf8bzm3fl21q") (r "1.64.0")))

(define-public crate-demo-app-asi-1.4.1 (c (n "demo-app-asi") (v "1.4.1") (h "13r5jkl0b2lmnnsmp5zldjf1rxgpnj2bvljkb1wjga6yjjbv66f3") (r "1.64.0")))

(define-public crate-demo-app-asi-1.5.0-alpha.1 (c (n "demo-app-asi") (v "1.5.0-alpha.1") (h "0k5z48hfkmvxsk07pvz5j64svmglw92p5dshwv6nvgqvqy8ixxyn") (r "1.64.0")))

(define-public crate-demo-app-asi-1.5.0-alpha.2 (c (n "demo-app-asi") (v "1.5.0-alpha.2") (h "0lf6v6q4n0k39f553qz3nkr975j72f7khawyc9b83z3dh821j1r4") (r "1.64.0")))

