(define-module (crates-io de mo demo-crate-123) #:use-module (crates-io))

(define-public crate-demo-crate-123-0.1.0 (c (n "demo-crate-123") (v "0.1.0") (h "15f8vlanmds3fjnza6wn0cli7wjfjh2yr5n1lis641jhfq2sfa19") (y #t)))

(define-public crate-demo-crate-123-0.1.1 (c (n "demo-crate-123") (v "0.1.1") (h "01q29xamfb8x3zy6362pbwfym4g3xdkqlpmy5hpv9mxxqk9y388g") (y #t)))

(define-public crate-demo-crate-123-0.1.2 (c (n "demo-crate-123") (v "0.1.2") (h "0aj76g0iiky09sa5xm8pwkbhb364d95xzpgmhly5p3kdrz5j7g6f") (y #t)))

