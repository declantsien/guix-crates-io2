(define-module (crates-io de mo demonstrate) #:use-module (crates-io))

(define-public crate-demonstrate-0.1.0 (c (n "demonstrate") (v "0.1.0") (h "13hk26b73ln3ghiv4x63gwzkj3yv0pvk9q99jvrn166nipr07636")))

(define-public crate-demonstrate-0.2.0 (c (n "demonstrate") (v "0.2.0") (d (list (d (n "async-attributes") (r "^1.1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "1sinly3zhircm1mm8ainjh0z40w1cjz6y7x4vyk38k6lxjfgznr3")))

(define-public crate-demonstrate-0.2.1 (c (n "demonstrate") (v "0.2.1") (d (list (d (n "async-attributes") (r "^1.1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "14lclg7c50mm79ksv5yih0qypk5mfhc6pnrqvpq89fq35pn9wixa")))

(define-public crate-demonstrate-0.2.2 (c (n "demonstrate") (v "0.2.2") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1067dv6mzrj55dv139cpfql4wcm3jy1ds5hg2dibig1b7d49dy2y")))

(define-public crate-demonstrate-0.2.3 (c (n "demonstrate") (v "0.2.3") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xz71wlhqhp1s6135py9dcdb99vicn50sjvqbvzpxbngp2q5zz14")))

(define-public crate-demonstrate-0.2.4 (c (n "demonstrate") (v "0.2.4") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bg2b6k25d2wf1xx1dr1aww91yzz0mmfmnj5hlpmk555ks6gps72")))

(define-public crate-demonstrate-0.2.5 (c (n "demonstrate") (v "0.2.5") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11gxgzqwis3nn2j99irhs80vg3gfn27fslfpq4kapbapihm1r3vx")))

(define-public crate-demonstrate-0.2.6 (c (n "demonstrate") (v "0.2.6") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xd0wgd1p2w7xcd9ahzhgk7riz8ch1jgdlcqr6ppxknx4qzngx14")))

(define-public crate-demonstrate-0.2.7 (c (n "demonstrate") (v "0.2.7") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1id70nvx5xbhyhg6f3lbl7zhkzncrkiahva20q0cjsxmmz7js2s9")))

(define-public crate-demonstrate-0.2.8 (c (n "demonstrate") (v "0.2.8") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8449asnhk4k9v43ibrcw1jbzrvwh7662c4hyhjw4vzx9cdzmvn")))

(define-public crate-demonstrate-0.3.0 (c (n "demonstrate") (v "0.3.0") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zw0rzxspfn82c8yb14vn7l6vd5fjyksl6clpz4w9gsg8a8lg8q3")))

(define-public crate-demonstrate-0.3.1 (c (n "demonstrate") (v "0.3.1") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xq000qys9r7jvkx0pixwin8gd9lbxwwia4sq03n9y46jm7mymz3")))

(define-public crate-demonstrate-0.3.2 (c (n "demonstrate") (v "0.3.2") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jddcbjq4zqpmi597x4cfn1qhn1c8xdqgggqgdlcg2jhahy6sidl")))

(define-public crate-demonstrate-0.4.0 (c (n "demonstrate") (v "0.4.0") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vl7x7rjva7q3qjqjgfzmqbhln1f5mmk1fjrv30ja4449yr9mmd7")))

(define-public crate-demonstrate-0.4.1 (c (n "demonstrate") (v "0.4.1") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fzayz2d9krdz29mmyfcqc1ysk064n1m77p65fjchj5gmh3y6baf")))

(define-public crate-demonstrate-0.4.2 (c (n "demonstrate") (v "0.4.2") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.12") (d #t) (k 0)))) (h "149gn8xa3jq1n28xnikpzs7ns8xbgxnwmbc2y6pkvq6g15ws8wll")))

(define-public crate-demonstrate-0.4.3 (c (n "demonstrate") (v "0.4.3") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.12") (d #t) (k 0)))) (h "022zi8ji5sywavda8sbzvadh6qlvl5hj0l8fslmryl39jczs8d4y")))

(define-public crate-demonstrate-0.4.4 (c (n "demonstrate") (v "0.4.4") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.12") (d #t) (k 0)))) (h "1ngm8iyb85p01i3v5wpmsxr9bycfwbjk0y4kvqlc6mhpdfa23k6q")))

(define-public crate-demonstrate-0.4.5 (c (n "demonstrate") (v "0.4.5") (d (list (d (n "async-attributes") (r "^1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.12") (d #t) (k 0)))) (h "11q94f9xnmp8i1r4q4sa7kx67hbvsdsa82cidmbz6gz3bzwn6ij8")))

