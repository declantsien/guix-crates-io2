(define-module (crates-io de mo demo_xyz) #:use-module (crates-io))

(define-public crate-demo_xyz-0.1.0 (c (n "demo_xyz") (v "0.1.0") (h "178ijinp3cgwc7m6a36zh7dazwq5rqxvlqqnpwk73hyfvy0p011g")))

(define-public crate-demo_xyz-0.1.1 (c (n "demo_xyz") (v "0.1.1") (h "0hd4pj6zdpgs4qvjh0289s7p2wlpnz1ipg02f6z1mgh931y1hh2v")))

