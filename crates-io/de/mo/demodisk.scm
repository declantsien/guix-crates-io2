(define-module (crates-io de mo demodisk) #:use-module (crates-io))

(define-public crate-demoDisk-0.0.1 (c (n "demoDisk") (v "0.0.1") (h "0ri19lansb085j8xv5gj15pp4wlm2a4dqnk2vysycimv3299nzpw") (y #t)))

(define-public crate-demoDisk-0.0.5 (c (n "demoDisk") (v "0.0.5") (h "0m8d0ihalxbpfspin858l00fd38yy6nzadds73886p4bw2dyvv7q") (y #t)))

(define-public crate-demoDisk-0.0.7 (c (n "demoDisk") (v "0.0.7") (h "04z753f2r6wvrmxfwpn4dpzklscivnchacgcngv2ldd7i5bjdcws") (y #t)))

(define-public crate-demoDisk-0.1.1 (c (n "demoDisk") (v "0.1.1") (h "0zqz9bqa8vvb6qac8pc45dv1zrn4qk7asndajcyii68y130xqsmc") (y #t)))

(define-public crate-demoDisk-0.1.3 (c (n "demoDisk") (v "0.1.3") (h "1mcnr1v4nzd2863d83bbczhx5hgbk24rpiwll27rykm131l6iwff") (y #t)))

(define-public crate-demoDisk-0.1.4 (c (n "demoDisk") (v "0.1.4") (h "09ncdic3gpl8ckig7vjyifrbi5za34f8n1rxdp4x681xb6gylqjl") (y #t)))

(define-public crate-demoDisk-0.1.77 (c (n "demoDisk") (v "0.1.77") (h "0c5hlmnrjyjrb1fjp7iwir2x80641cybff229biw55vza7qsp345") (y #t)))

(define-public crate-demoDisk-0.77.0 (c (n "demoDisk") (v "0.77.0") (h "0p34nq24z3crrhf4m3sym45j2pscvw87bsppshcck7493br8pfwm") (y #t)))

(define-public crate-demoDisk-7.7.18 (c (n "demoDisk") (v "7.7.18") (h "1hks967nsagw40aw664wnpn7fa377zmlkfxhfrfjqdxyy7s2w54s") (y #t)))

(define-public crate-demoDisk-2018.7.7 (c (n "demoDisk") (v "2018.7.7") (h "1kzpzsl8nnp48pj1acfzy447s8q5pc12gl8fqqbj7drn305fxxk7") (y #t)))

(define-public crate-demoDisk-2019.12.13 (c (n "demoDisk") (v "2019.12.13") (h "1pnyzsy04kdqp2x6lj6l4bwvl6nmmfwc3y2jdjbs055s1by35ipa") (y #t)))

(define-public crate-demoDisk-9.9.9 (c (n "demoDisk") (v "9.9.9") (h "16vv2c3asyhikw5r8qz7l80p5c1ryqp831fvh0apjiirnqm9jfya") (y #t)))

(define-public crate-demoDisk-9999.999.99 (c (n "demoDisk") (v "9999.999.99") (h "11lj5a89dynw0ngp4lgqdw3p4cpm7p8gqk7v4pl2ydmdh4m9c4qa") (y #t)))

(define-public crate-demoDisk-99999.99999.99999 (c (n "demoDisk") (v "99999.99999.99999") (h "084idxma33wldbxr2jmbxm17gziq3rvcz9basn7lljl8bh51327w") (y #t)))

(define-public crate-demoDisk-9999999.9999999.9999999 (c (n "demoDisk") (v "9999999.9999999.9999999") (h "18f04894883b8srn7qzd71qzd6hb74j8dk9pvacvavicc70b24c5") (y #t)))

(define-public crate-demoDisk-999999999.999999999.999999999 (c (n "demoDisk") (v "999999999.999999999.999999999") (h "0nayindwd7zf7ahi15hs1hwfh45265vm4pc1jq4ampkn4rzqmawf")))

