(define-module (crates-io de mo demoji-rs) #:use-module (crates-io))

(define-public crate-demoji-rs-0.1.0 (c (n "demoji-rs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0kw086v2m92arj2xw428011ap1ppsy2x6jyxq0br8rh45ia1bdi8")))

