(define-module (crates-io de mo demoji) #:use-module (crates-io))

(define-public crate-demoji-0.0.1 (c (n "demoji") (v "0.0.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0fv4lzgnwxg1ls7k8r89ikrla30ayrbd4q57acrd38z85h940243")))

(define-public crate-demoji-0.0.2 (c (n "demoji") (v "0.0.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07qhak2vwh29n1f6fbifxvkry93klspscsdsd2546prsvm1whsgf")))

(define-public crate-demoji-0.0.3 (c (n "demoji") (v "0.0.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0xvh7gifbl79m2cykbijf2g5mv3iymdapy9n5bi84bn98pyn053m")))

