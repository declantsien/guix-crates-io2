(define-module (crates-io de mo demo-hack-impl) #:use-module (crates-io))

(define-public crate-demo-hack-impl-0.0.1 (c (n "demo-hack-impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack-impl") (r "^0.2") (d #t) (k 0)))) (h "1g397mjrprrmk7cshrmgzwpz8xaadd33qpls2h4qsf0nc4dxvjyd")))

(define-public crate-demo-hack-impl-0.0.3 (c (n "demo-hack-impl") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "03qa6i3gmxxwc2qnvy155kdv1xh23wdvkidddkxmxh2pv3jyrigy")))

(define-public crate-demo-hack-impl-0.0.4 (c (n "demo-hack-impl") (v "0.0.4") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1wb267k9g40z8p458q1g8nrq3x8cq13s2wnvx5fy9rd011f8qy7h")))

(define-public crate-demo-hack-impl-0.0.5 (c (n "demo-hack-impl") (v "0.0.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1f1fdl60xjas9wlmcl9v6f56vgm3mzwr019kcifav5464rx3w3ld")))

