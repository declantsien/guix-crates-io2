(define-module (crates-io de mo demo-hack) #:use-module (crates-io))

(define-public crate-demo-hack-0.0.1 (c (n "demo-hack") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack-impl") (r "^0.2") (d #t) (k 0)))) (h "0jr10p3bh6sn46rbjk9xgcxn4n0phpp16jw9kwfmljyi6d6n8ilb")))

(define-public crate-demo-hack-0.0.3 (c (n "demo-hack") (v "0.0.3") (d (list (d (n "demo-hack-impl") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0vvwmdk8qrxmmqdw1hxyhp7if5d0qsl19xxkipd04ajsd3g6jraj")))

(define-public crate-demo-hack-0.0.4 (c (n "demo-hack") (v "0.0.4") (d (list (d (n "demo-hack-impl") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1mrcvy5ilnqv68qxgzp2g0zmwix7s8ic3lxj69yd18x3izcr7kb3")))

(define-public crate-demo-hack-0.0.5 (c (n "demo-hack") (v "0.0.5") (d (list (d (n "demo-hack-impl") (r "^0.0.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0m0114p1g0zzrdph5bg03i8m8p70vrwn3whs191jrbjcrmh5lmnp")))

