(define-module (crates-io de mo demo) #:use-module (crates-io))

(define-public crate-demo-0.1.0 (c (n "demo") (v "0.1.0") (h "0kyvnsgck0k2w1y8dpf5lzbzad8qw7771cdrpbvi3gbwq67nwa5r") (y #t)))

(define-public crate-demo-0.1.1 (c (n "demo") (v "0.1.1") (h "0bl7b69f3w594dnm0db9pl8f497p86ind60dgrqlwx7xhq7mgsaj") (y #t)))

