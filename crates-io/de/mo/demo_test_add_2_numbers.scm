(define-module (crates-io de mo demo_test_add_2_numbers) #:use-module (crates-io))

(define-public crate-demo_test_add_2_numbers-0.1.0 (c (n "demo_test_add_2_numbers") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1wnkpakx3w2zi762jg0bj77983vj3jqpn29c7ksa69h388w7f52s")))

(define-public crate-demo_test_add_2_numbers-0.1.1 (c (n "demo_test_add_2_numbers") (v "0.1.1") (h "13ki3gisjxyw7xkam5b1q34nq0hnn81hngkfqm2jwc92h2ql01ms")))

(define-public crate-demo_test_add_2_numbers-0.1.2 (c (n "demo_test_add_2_numbers") (v "0.1.2") (h "13pw5r7cm927hk7g9yysi40w0fy8wc67x9izj82xdwy3sx551h0c")))

