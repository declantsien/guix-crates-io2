(define-module (crates-io de mo demo-crate-6154) #:use-module (crates-io))

(define-public crate-demo-crate-6154-0.1.0 (c (n "demo-crate-6154") (v "0.1.0") (h "0gfa0d5b1zbgksy169fidhfjaf424w1mmwxxlyzcgcrfad8j57im") (r "1.50")))

(define-public crate-demo-crate-6154-0.2.0 (c (n "demo-crate-6154") (v "0.2.0") (h "03yvdq2qv50qpvxw1j2w7pmlr0p5v5ia1vz2i3y5w43mp4jx1yhl") (r "1.59")))

