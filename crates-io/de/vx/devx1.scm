(define-module (crates-io de vx devx1) #:use-module (crates-io))

(define-public crate-devx1-0.1.0 (c (n "devx1") (v "0.1.0") (d (list (d (n "cosmwasm") (r "^0.7.0") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^0.7.0") (k 2)) (d (n "cw-storage") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "= 0.5") (d #t) (k 0)) (d (n "serde") (r "= 1.0.103") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "snafu") (r "= 0.5.0") (f (quote ("rust_1_30"))) (k 0)))) (h "1h3ws7vzc3hmn1c6k2prcpfzjdr3xbrvqhhjhg0g0ypnnrhm9din") (f (quote (("singlepass" "cosmwasm-vm/default-singlepass") ("default" "cranelift") ("cranelift" "cosmwasm-vm/default-cranelift") ("backtraces" "cosmwasm/backtraces" "cosmwasm-vm/backtraces"))))))

