(define-module (crates-io de vx devx-pre-commit) #:use-module (crates-io))

(define-public crate-devx-pre-commit-0.1.0 (c (n "devx-pre-commit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.1.0") (d #t) (k 0)))) (h "0ng240v5x5yjafm5nb3khcjnjkivys7x461v7pllxh21bdpbkrng")))

(define-public crate-devx-pre-commit-0.2.0 (c (n "devx-pre-commit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.2.0") (d #t) (k 0)))) (h "0q047l6i3jxj1dkisfl8w52vs4xl9a0kidfyf2y8adg1nv4fwmy1")))

(define-public crate-devx-pre-commit-0.3.0 (c (n "devx-pre-commit") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.3.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.3") (d #t) (k 0)))) (h "0i42x64ygiyq91lvmb93cyx6y2iynb2x3adjrw8yjnrj6dv08a61")))

(define-public crate-devx-pre-commit-0.3.1 (c (n "devx-pre-commit") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.3.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.4") (d #t) (k 0)))) (h "1ndqwsnxl66jx4bl6mm6fvw921s31jk30jl5zkfs8r349v8f8c7j")))

(define-public crate-devx-pre-commit-0.4.0 (c (n "devx-pre-commit") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.4.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.4") (d #t) (k 0)))) (h "0cp31k2a4kidz8l3qfw2ygzpjfvz0g2s4dqxcqdcx1d7wi15pvjc")))

(define-public crate-devx-pre-commit-0.5.0 (c (n "devx-pre-commit") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "devx-cmd") (r "^0.5.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.4") (d #t) (k 0)))) (h "0b6qbbzizq7llgpz5vbbiyf52s09rr1qvh79dlb3jw2x0h5kncw2")))

