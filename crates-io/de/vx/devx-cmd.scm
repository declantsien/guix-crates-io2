(define-module (crates-io de vx devx-cmd) #:use-module (crates-io))

(define-public crate-devx-cmd-0.1.0 (c (n "devx-cmd") (v "0.1.0") (h "1rwbpxw8b68rzvvymn0aiylb4d3vnj4dfqdajifkav3bpmx9ig4s")))

(define-public crate-devx-cmd-0.2.0 (c (n "devx-cmd") (v "0.2.0") (h "0jaq4nw1saw9x4ys35v0mj64w79qjij6x9hr722pkm2bkf5kxfcd")))

(define-public crate-devx-cmd-0.3.0 (c (n "devx-cmd") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (k 2)))) (h "0g5xlmb4qkc3z3q4g5fi085hzx9816raj49fdlxzpdvrm4ybp8lb")))

(define-public crate-devx-cmd-0.3.1 (c (n "devx-cmd") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (k 2)))) (h "1qm208m0x8gpykk44mkyqml7hllysfr9ypipjpjhsx3z7zh8c7bp")))

(define-public crate-devx-cmd-0.4.0 (c (n "devx-cmd") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (k 2)))) (h "0fvv4kpg3axfg0klbfm84l498r0mzqk9lnznvwa7q6cb5v6c440m")))

(define-public crate-devx-cmd-0.5.0 (c (n "devx-cmd") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (k 2)))) (h "134c8spmz5ds6fsd53v5nwgprzzy3vb47q842p50pdc2syb8vw95")))

