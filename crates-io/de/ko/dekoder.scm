(define-module (crates-io de ko dekoder) #:use-module (crates-io))

(define-public crate-dekoder-0.1.0 (c (n "dekoder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "1zcl4pg21q8121qr0ri03c0nwqn4bcvf97n49l92isp0msgbbw0y") (r "1.69.0")))

