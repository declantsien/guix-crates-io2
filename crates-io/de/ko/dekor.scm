(define-module (crates-io de ko dekor) #:use-module (crates-io))

(define-public crate-dekor-0.1.0 (c (n "dekor") (v "0.1.0") (h "1xw2w5261vbvgvbimy3jjmpf0xajkzbin2a6kmj43yqq3p82iqq2")))

(define-public crate-dekor-0.2.0 (c (n "dekor") (v "0.2.0") (h "0gy0pnkrll6zm54rn8w0zny7f24hs1ycfnkvs0mik7scz8by14m6")))

(define-public crate-dekor-0.2.1 (c (n "dekor") (v "0.2.1") (h "0xz076ar1nzpd9qhfdw4viz3h22jbc9m4mxmvaqhpbqvif0jikzc")))

(define-public crate-dekor-0.2.2 (c (n "dekor") (v "0.2.2") (h "0bynz16r032dlgmgfccpyqdxhhmllhbqf8zgxnhffib66s247vxk")))

