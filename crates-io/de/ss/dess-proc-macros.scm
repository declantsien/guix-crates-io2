(define-module (crates-io de ss dess-proc-macros) #:use-module (crates-io))

(define-public crate-dess-proc-macros-0.1.0 (c (n "dess-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "02jj4s4gmhppr4xd0ijfazqy30m13kpswkvc2x4lz7m8p2crwn2m")))

(define-public crate-dess-proc-macros-0.1.1 (c (n "dess-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0j130afshcyza4g4cxq0ffk7jhg2r5s4j1g5xfxj1q927a9pk54v")))

