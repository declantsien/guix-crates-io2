(define-module (crates-io de ss dessin-macros) #:use-module (crates-io))

(define-public crate-dessin-macros-0.8.3-pre (c (n "dessin-macros") (v "0.8.3-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "053rx8mfgkdj2alhk7gbiyg884ja04wl0g5n7knrfl9j4ncw5qjx")))

(define-public crate-dessin-macros-0.8.4-pre (c (n "dessin-macros") (v "0.8.4-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xiid6llpx5amcx71j5y9z9fvnfk3ffjmhfyjl3902inksn8j3cy")))

(define-public crate-dessin-macros-0.8.5-pre (c (n "dessin-macros") (v "0.8.5-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f62p15ns5kl1wfn2a9n97q0p149rfqn1mc2hq6hc23l6jm9lqla")))

(define-public crate-dessin-macros-0.8.6-pre (c (n "dessin-macros") (v "0.8.6-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sy74s1rxscp302w7k4js3c53dc8p2nivvp2vzfz2kbfb7rm41wb")))

(define-public crate-dessin-macros-0.8.7-pre (c (n "dessin-macros") (v "0.8.7-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vwzzgxvl1d4myiqb1rk271lndw3wz00ik6xzi6vqphp073488cx")))

(define-public crate-dessin-macros-0.8.8-pre (c (n "dessin-macros") (v "0.8.8-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08z933pq5j9acb2hxwdhz6xfbgw1rkp1piq5wrw41030i3qjl5jy")))

(define-public crate-dessin-macros-0.8.9-pre (c (n "dessin-macros") (v "0.8.9-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h30ysmjqmvzxyg616wgbc4ba7dxmdlg3cbdh01li6pmzf647sfk")))

(define-public crate-dessin-macros-0.8.10-pre (c (n "dessin-macros") (v "0.8.10-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n45x42aa9mp8whsj6dpj1c0qr50x1zz0f6kkggd8rqyk2lh7zwx")))

(define-public crate-dessin-macros-0.8.11-pre (c (n "dessin-macros") (v "0.8.11-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ag2h439vwjwj4325awrp6f3maik7cqzcdygqy5h17c3kqyw83ra")))

(define-public crate-dessin-macros-0.8.12-pre (c (n "dessin-macros") (v "0.8.12-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "186hhgq327d6gzalx9ckchq4ik6p1di478kz1lhgx1m5k24r8wcd")))

(define-public crate-dessin-macros-0.8.13-pre (c (n "dessin-macros") (v "0.8.13-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rl4v09xckd8nsxmdxqnrcyxb1ary2114y17hbslalikyxdra0x9")))

(define-public crate-dessin-macros-0.8.14-pre (c (n "dessin-macros") (v "0.8.14-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jsnppzpr53mnir8y33f0d2xjx58ka3bpic7nlv7hj2naxvfph0q")))

(define-public crate-dessin-macros-0.8.15-pre (c (n "dessin-macros") (v "0.8.15-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yvzymda6mnl3aw4vryqw09pjngz7v0fvij1izq5dg9cp87spsdp")))

(define-public crate-dessin-macros-0.8.16-pre (c (n "dessin-macros") (v "0.8.16-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1463xs1bnw6i9b5lbpr76ph71sjfm1b8g36n62hcgcdw6wrf7d1j")))

(define-public crate-dessin-macros-0.8.17-pre (c (n "dessin-macros") (v "0.8.17-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gbnz8cx1iajlhnvw7iq0lkmp17ldb9xzbx2p812yds8m6kqq66f")))

(define-public crate-dessin-macros-0.8.18-pre (c (n "dessin-macros") (v "0.8.18-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17dp0mwllf0db7gsjs26ibwns8y6q2plhqdcghbx7cdf6d4djspy")))

(define-public crate-dessin-macros-0.8.19-pre (c (n "dessin-macros") (v "0.8.19-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00h9vhmi7z82l43kwmbkzcnknwpv9qrww7w5an520isz37ppgmpp")))

(define-public crate-dessin-macros-0.8.20-pre (c (n "dessin-macros") (v "0.8.20-pre") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j24ss2mbdffg1xwmcwbbkxf2y2qwyg8j5w43dilm5yxvnyddc52")))

