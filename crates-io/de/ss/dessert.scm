(define-module (crates-io de ss dessert) #:use-module (crates-io))

(define-public crate-dessert-0.1.0 (c (n "dessert") (v "0.1.0") (d (list (d (n "dessert-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 0)))) (h "1rbqndn61wr58v9837ps65vci16b5acgm4v7mffagjshzka1jmib")))

(define-public crate-dessert-0.1.1 (c (n "dessert") (v "0.1.1") (d (list (d (n "dessert-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)))) (h "1wlsiwizicsgwr2vhvfv49qzzx42kq3a0hkz7cllq9bdnsy9b427")))

(define-public crate-dessert-0.1.2 (c (n "dessert") (v "0.1.2") (d (list (d (n "dessert-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)))) (h "04wym5nfcm27pvcn5dxh3mzvb5jybx52wyjb98yrc940l1vb0vbp")))

