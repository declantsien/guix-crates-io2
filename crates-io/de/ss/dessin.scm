(define-module (crates-io de ss dessin) #:use-module (crates-io))

(define-public crate-dessin-0.2.0 (c (n "dessin") (v "0.2.0") (d (list (d (n "algebr") (r "^0.1.0") (d #t) (k 0)))) (h "16l6wx5mb4fhpqbbmzlbxnqy17vj0nzymisa69sqf5mw4zvkkgqc")))

(define-public crate-dessin-0.2.1 (c (n "dessin") (v "0.2.1") (d (list (d (n "algebr") (r "^0.1.0") (d #t) (k 0)))) (h "09m8jwqwbhcgs1ggf8p4fkh5nadnfd69d37kjr0b8lsyn8pwqqcf")))

(define-public crate-dessin-0.2.2 (c (n "dessin") (v "0.2.2") (d (list (d (n "algebr") (r "^0.1.0") (d #t) (k 0)))) (h "0grjvb86nk1s8gcfx9ggd461b5hyp8k4vlq9ln4xf4pb5v6ivbmi")))

(define-public crate-dessin-0.2.3 (c (n "dessin") (v "0.2.3") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1jiw0k2pqbwpbxn9jmf805sdi27wqwagj35y1v01bimb76fi3745") (y #t)))

(define-public crate-dessin-0.2.4 (c (n "dessin") (v "0.2.4") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "006l6px0xnmzhd0rl2vngfl83i2x93nrvicw2wjf8glr1phr83kc")))

(define-public crate-dessin-0.2.5 (c (n "dessin") (v "0.2.5") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "11xilq9qra6awk248k0dkq1jsikffbxmszv6dlyllnv8z7ln2myw")))

(define-public crate-dessin-0.2.6 (c (n "dessin") (v "0.2.6") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0y44d7l3lgy2h7x9a92wzrx7ykx2ayx3cfckfmix5slrnnr7n25f")))

(define-public crate-dessin-0.3.0 (c (n "dessin") (v "0.3.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1gawmx0z2d7rc40gnhf1myna74a8n50yw3mh256bz7x17p39ipd0")))

(define-public crate-dessin-0.3.1 (c (n "dessin") (v "0.3.1") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "03wfa0jaq4rl3w8z89ws2ff4bdmzl52p6sf0jlargrps6g49akis")))

(define-public crate-dessin-0.4.0 (c (n "dessin") (v "0.4.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1yj2pypg3ff0fqbdn3n47czrig7069jv4ckajiv7gk3v0z4cw68g") (y #t)))

(define-public crate-dessin-0.4.1 (c (n "dessin") (v "0.4.1") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "03xx5nf3137lq16jkwdnwihpnwv39mi1vm759zdxcafgqi59q82j")))

(define-public crate-dessin-0.4.2 (c (n "dessin") (v "0.4.2") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "07jx4n36lxl7129fg0g35r6zk1d52ka5j5bvi8rb9icpb9b340rj")))

(define-public crate-dessin-0.4.3 (c (n "dessin") (v "0.4.3") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0ha76whgsmya6qgrwvqpbm4sj0734kn1nji262zhd1iqdrbin04v")))

(define-public crate-dessin-0.4.4 (c (n "dessin") (v "0.4.4") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1qq79f6d6rwm75zrjz6ij0rd1rvwpnv99bjqmpjgziwmacircsz6") (y #t)))

(define-public crate-dessin-0.4.5 (c (n "dessin") (v "0.4.5") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0s9q7dh42pbcm9cw7nhrb5k6rcvvj8hrmw5wd1gar9hig6lmhzsl")))

(define-public crate-dessin-0.4.6 (c (n "dessin") (v "0.4.6") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0d8cyjjf2ada56cllqj716frb75bznlvx1h69b2saf9m0nqig5x9")))

(define-public crate-dessin-0.4.7 (c (n "dessin") (v "0.4.7") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0p2afdwfg6n1i4jlx03avj7qqpdh3185f8qvwcnx7007n6rxm0z2") (y #t)))

(define-public crate-dessin-0.4.8 (c (n "dessin") (v "0.4.8") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0vzqawziqqygmxxp8w09kldd5928knldgbxb8ckbr4a2zpzdpf87")))

(define-public crate-dessin-0.4.9 (c (n "dessin") (v "0.4.9") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0qql65dx5afrzq1srvx4lm8878rdpbg1sq4wacyhbxc8d417v66j") (y #t)))

(define-public crate-dessin-0.4.10 (c (n "dessin") (v "0.4.10") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "149gmqqbjav9zljl9wbqlzvsm4vg0zxgirrpjhmx0hjr0lfjkpd5")))

(define-public crate-dessin-0.4.11 (c (n "dessin") (v "0.4.11") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "009jaizmd2pfsaq9kv7iriv8wiib8q7l1fd5yqfag1l9vjiabm12")))

(define-public crate-dessin-0.4.12 (c (n "dessin") (v "0.4.12") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "15lm4xxgcbpwz60h38mg96abb2kcycq549nigf9s0s5879agskly") (y #t)))

(define-public crate-dessin-0.5.0 (c (n "dessin") (v "0.5.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0gxf86dn7jnprpnif37sgimmfgnnpfdhly4bxmwb6hvi84lyiqx6")))

(define-public crate-dessin-0.6.0 (c (n "dessin") (v "0.6.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0g3x514ydsdjnpzk2p8j33lczrf6yk8lqdylxd2y06wf7a4xzzn7")))

(define-public crate-dessin-0.6.1 (c (n "dessin") (v "0.6.1") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0xqnb0swh4sq464h2gqmj6hcxvbwgxlz5k2phrlss7mrksax4bvw")))

(define-public crate-dessin-0.6.2 (c (n "dessin") (v "0.6.2") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "05irkyxgqalxcykh27b18nk0pk9a1iny9dx0yfr8afswjnld0big")))

(define-public crate-dessin-0.6.3 (c (n "dessin") (v "0.6.3") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1l8s33haza5nkks7nk9w8qyjgk66l78cf3rp8q0ny3cq9jj3bb86")))

(define-public crate-dessin-0.6.4 (c (n "dessin") (v "0.6.4") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0zdrrmb8ybvxfhm5w24d89a1wjqhwb5p3y348l5nsi9l6k3iyp32")))

(define-public crate-dessin-0.6.5 (c (n "dessin") (v "0.6.5") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0y017fk12h10wh867svnk3wa25453c9msny8lax3agp1kfi0rf28")))

(define-public crate-dessin-0.6.6 (c (n "dessin") (v "0.6.6") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1mjh21wwsiwki34i2bs4vi4h7pwch0kzc0yrj6nsv8a9nrkkpz5w")))

(define-public crate-dessin-0.6.7 (c (n "dessin") (v "0.6.7") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "12xmdjnrcxwamyafdj8k4vbyw2w5s4h23k7vzi7wdgwj1p86j2pb")))

(define-public crate-dessin-0.6.8 (c (n "dessin") (v "0.6.8") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0v9a2r3g25425z4vn2h33szcrdzn1hsswcg16hrv8bghsi7szry9")))

(define-public crate-dessin-0.6.9 (c (n "dessin") (v "0.6.9") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1hgxj377iccdi4hsrc5mi91vci9i3mrlq8wx5l3536k33fkfv21y")))

(define-public crate-dessin-0.6.10 (c (n "dessin") (v "0.6.10") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0780hz86v9j38b5d2xn6dn0njcwsawc13q8h83hyd6hx8sm5g7nf")))

(define-public crate-dessin-0.6.11 (c (n "dessin") (v "0.6.11") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1zx1nz08a7mzv6bz5n4js3cvv7g209vpffqas80zypv3ma425rrr") (y #t)))

(define-public crate-dessin-0.6.12 (c (n "dessin") (v "0.6.12") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "1gdyyrd2ijn6d315njdq514lpsg4i5gnrq95517rzqyndj1g3aj9")))

(define-public crate-dessin-0.6.13 (c (n "dessin") (v "0.6.13") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "11l7c8kwd9l5xbwda6df2lwrpp9nr6d2ipzcsyvp5n36zq2k0ds1")))

(define-public crate-dessin-0.6.14 (c (n "dessin") (v "0.6.14") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)))) (h "0l92vlkg1n5pb7hxix11i92wssn1bcnflll18jka01gymmfj8dnz") (y #t)))

(define-public crate-dessin-0.6.15 (c (n "dessin") (v "0.6.15") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "04rn0p6gnjc4m1yy7xqzbmxq3bp0hysiacnwb0ryww77jdg8p14c")))

(define-public crate-dessin-0.6.16 (c (n "dessin") (v "0.6.16") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1c48d2k226sn32j1l9gj4a4cigknryjwkbd8p03mg02jr77lgr24")))

(define-public crate-dessin-0.7.0 (c (n "dessin") (v "0.7.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1az97lzpww8l9pbx8dfgxrx8v6k8xnrk4py02n4r6gzyzcpab8b1")))

(define-public crate-dessin-0.7.1 (c (n "dessin") (v "0.7.1") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0zss3njswx1m2kyc4gw1xq5n76dsba5l16syqpd8dab0c28d90x5")))

(define-public crate-dessin-0.7.2 (c (n "dessin") (v "0.7.2") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "08jpnrljyskah6y7zny71bs8c65rxyrdx2fy3yj8dwj3rciq6iqa")))

(define-public crate-dessin-0.7.3 (c (n "dessin") (v "0.7.3") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1yv2krl4qxacnykqsw92bdsf3nzj2qrfz5916d31psdvj68za2p7")))

(define-public crate-dessin-0.8.0-pre (c (n "dessin") (v "0.8.0-pre") (d (list (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "0lpil11ws0kgs86avcwaw789b72vvan0y28mxrp1nvpb78sy74c0")))

(define-public crate-dessin-0.8.1-pre (c (n "dessin") (v "0.8.1-pre") (d (list (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "183sbajldm7vwpq9qkqdkzhb3kav4vsd6csaw5sqz6l9pwf9brn9")))

(define-public crate-dessin-0.8.2-pre (c (n "dessin") (v "0.8.2-pre") (d (list (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "0dy6a5fnz0l5qg79x15gd7rb9pp4rwkddmw6d78fbyz9wyhjyy8m")))

(define-public crate-dessin-0.8.3-pre (c (n "dessin") (v "0.8.3-pre") (d (list (d (n "dessin-macros") (r "^0.8.3-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "051j44x7g1d5l7xn2zcyf5nmwafbih9mzw4jpsxn6srsn0xyb1rq")))

(define-public crate-dessin-0.8.4-pre (c (n "dessin") (v "0.8.4-pre") (d (list (d (n "dessin-macros") (r "^0.8.4-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "0d4sjzi618492ifnnr5d9vk7j8l3xnkalfzrdr0pgzv2fyc23z2y")))

(define-public crate-dessin-0.8.5-pre (c (n "dessin") (v "0.8.5-pre") (d (list (d (n "dessin-macros") (r "^0.8.5-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "19mfdi9ca7mvnk65mrfvqcynhdqvfcw8f0g8xal30n96xhqabs9n")))

(define-public crate-dessin-0.8.6-pre (c (n "dessin") (v "0.8.6-pre") (d (list (d (n "dessin-macros") (r "^0.8.6-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "1583wlfdkn9d7idwr6c24j7gs22m3k8bljzabfag3dmy90j6zsl7") (f (quote (("reactive") ("default"))))))

(define-public crate-dessin-0.8.7-pre (c (n "dessin") (v "0.8.7-pre") (d (list (d (n "dessin-macros") (r "^0.8.7-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "0bgbcyhj8vviqp5clc8lbqsq51nn8qa2rlcldhgxk8nb576wbpvz") (f (quote (("reactive") ("default"))))))

(define-public crate-dessin-0.8.8-pre (c (n "dessin") (v "0.8.8-pre") (d (list (d (n "dessin-macros") (r "^0.8.8-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "palette") (r "^0.7.1") (d #t) (k 0)))) (h "0xzalak1802rhycw5mwkfasbvkkw4shm77x3fwi02pf8hyilk0ik") (f (quote (("reactive") ("default"))))))

(define-public crate-dessin-0.8.9-pre (c (n "dessin") (v "0.8.9-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.9-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1a67f953xp71rkgbrl25blva24w6qylzywhspsnb98ld2mgpw0vn") (f (quote (("reactive") ("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.10-pre (c (n "dessin") (v "0.8.10-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.10-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0dklm1nmymnci07wqi4na7k4z9xxa5i65gab30nmzw4kacak4pcj") (f (quote (("reactive") ("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.11-pre (c (n "dessin") (v "0.8.11-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.11-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1w4pvd6k4y90d95hyg7nd5ryxmjb59zrnamhgzwv7c4h4pfxxzfy") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.12-pre (c (n "dessin") (v "0.8.12-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.12-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0za02x1xmcmlpvzs5w9w58psvkfbrar315awi6q7gjx4wkl6hqyx") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.13-pre (c (n "dessin") (v "0.8.13-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.13-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0pk4nc598kvnfiwhqqrwjsrmf4917f0jyp2p5y4pnd1y16c93mdj") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.14-pre (c (n "dessin") (v "0.8.14-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.14-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1pf5329z9aiw3fvqdvvabc9xgssvwvr311h8gn32mp9f2gzifrk6") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.15-pre (c (n "dessin") (v "0.8.15-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.15-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "15y8gxc5s9sdflg8mrycadmhfcws9b0sb0h9djw4hp17c9p2qqk3") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.16-pre (c (n "dessin") (v "0.8.16-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.16-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0nhhx10x4kjs1q43pz517pm05pqxf0jbjf0h6nv0dgb0mgm03fmm") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.17-pre (c (n "dessin") (v "0.8.17-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.17-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1kvpcg98nilwhpyabibygzgq1gh3f61xcxkz57gqkhhddpzfd6n1") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.18-pre (c (n "dessin") (v "0.8.18-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.18-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0q1iv5za5fxamzps2zxyq5yz2lifqwivch8i610k1kkjgvhl1r8q") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.19-pre (c (n "dessin") (v "0.8.19-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.19-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1mf2xw5khxd6h7yx9c8zi5l3jvbxjc8brx778wf35dx072m4lkff") (f (quote (("default-font") ("default" "default-font"))))))

(define-public crate-dessin-0.8.20-pre (c (n "dessin") (v "0.8.20-pre") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "dessin-macros") (r "^0.8.20-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "11ldx0wcsp50ln4fjjiiwziw8p2pnbnv49lwvbxjp7hn6j1amx23") (f (quote (("default-font") ("default" "default-font"))))))

