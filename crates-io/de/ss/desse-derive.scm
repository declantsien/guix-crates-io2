(define-module (crates-io de ss desse-derive) #:use-module (crates-io))

(define-public crate-desse-derive-0.1.0 (c (n "desse-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ds708vy1qjc521lxfwbbzhs9970jz81addycvn8j9yvpzfcl66y")))

(define-public crate-desse-derive-0.1.1 (c (n "desse-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ilvw6vapibjzf4l1vqb5pvsvp0g0qwp3z6b5hlhcvdflw1x1zww")))

(define-public crate-desse-derive-0.2.0 (c (n "desse-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1zcv776lhhv6b4chbsi1m7fhzhj1sigqvppkn9np5381n0acdqwx")))

(define-public crate-desse-derive-0.2.1 (c (n "desse-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1a376vanpka5h17kyi8gsydpjs1a1w2rkbxisszf3s0m01sfs89k")))

