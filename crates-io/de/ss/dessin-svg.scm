(define-module (crates-io de ss dessin-svg) #:use-module (crates-io))

(define-public crate-dessin-svg-0.1.0 (c (n "dessin-svg") (v "0.1.0") (d (list (d (n "algebr") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2") (d #t) (k 0)))) (h "17l3dwj8bm1xwayk38j0j0dpxr1vdrx15q3zckxjii0lmaf0nwv7")))

(define-public crate-dessin-svg-0.2.0 (c (n "dessin-svg") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2.4") (d #t) (k 0)))) (h "11nlj69v4yacjnazshma4h104apn2f96x3scdg45qwii1sd2ql1r")))

(define-public crate-dessin-svg-0.2.1 (c (n "dessin-svg") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2.4") (d #t) (k 0)))) (h "08ikzzq1q6v31r5f215b1bvgmvcbmb7vx5pml1d630aydhcagda5")))

(define-public crate-dessin-svg-0.2.2 (c (n "dessin-svg") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2.5") (d #t) (k 0)))) (h "1by8mw36mn4jw97jb38wca0s0mqayi0bskdar8pbl9b3py0f609l")))

(define-public crate-dessin-svg-0.2.3 (c (n "dessin-svg") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2.6") (d #t) (k 0)))) (h "1fcfg74f013hw2rabwnn0934sdd6d0jm9pya9f2836b197b000jp")))

(define-public crate-dessin-svg-0.2.4 (c (n "dessin-svg") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.2.6") (d #t) (k 0)))) (h "05agff8fxh8290nj27szyhm9v8nq2yljknagw2j85phg5z5zirx2")))

(define-public crate-dessin-svg-0.3.0 (c (n "dessin-svg") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.3.0") (d #t) (k 0)))) (h "0hmajj3pkjkw98lhld39s3yg51lk9drc3h85hmhbbanpf0f16bp7")))

(define-public crate-dessin-svg-0.3.1 (c (n "dessin-svg") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.3.1") (d #t) (k 0)))) (h "0gra97jag9dbbq1g7b5h812ivgyxdlhb8bljfvg83d12apcfpk14")))

(define-public crate-dessin-svg-0.3.2 (c (n "dessin-svg") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.3.1") (d #t) (k 0)))) (h "0rx2x4rqd4fz6ch22cb2crswfxkm95sk37ahpl7i84m69lxmmm1k")))

(define-public crate-dessin-svg-0.4.0 (c (n "dessin-svg") (v "0.4.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.2") (d #t) (k 0)))) (h "0lrrig1sr74imbq5x1wl531gq3flkar4agja485h4wgiagarq4bj")))

(define-public crate-dessin-svg-0.4.1 (c (n "dessin-svg") (v "0.4.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.3") (d #t) (k 0)))) (h "0kafxwzia5ny5zpyff50qd6k83jjhz1a7p4z1wj2yxyxv98wigjy")))

(define-public crate-dessin-svg-0.4.2 (c (n "dessin-svg") (v "0.4.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.3") (d #t) (k 0)))) (h "06jlzal5cwz21d1ppsk8dfl8fqqibwh6yfq9xw12xxk8l9c5kjx0")))

(define-public crate-dessin-svg-0.4.3 (c (n "dessin-svg") (v "0.4.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.5") (d #t) (k 0)))) (h "13zlwbhvmixzx8bard25pgyyqphrcm45q8wyk2bjnapfkpgq97a7")))

(define-public crate-dessin-svg-0.4.4 (c (n "dessin-svg") (v "0.4.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.6") (d #t) (k 0)))) (h "15d5vg688l9wp93zr6a9b8i9y43x23kvxmvy3r4wl4pkn2hwhqxb")))

(define-public crate-dessin-svg-0.4.8 (c (n "dessin-svg") (v "0.4.8") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.8") (d #t) (k 0)))) (h "1my5vs5jqcyy3nq06vk6302ipv2l59m4rdf8g67rdkcdw91i253s")))

(define-public crate-dessin-svg-0.4.9 (c (n "dessin-svg") (v "0.4.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.9") (d #t) (k 0)))) (h "1dmpn7bgnhkr2q4fxcdapb0m5pmqq1hc33sr8ik4vr8pwq6w6l6l") (y #t)))

(define-public crate-dessin-svg-0.4.10 (c (n "dessin-svg") (v "0.4.10") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.10") (d #t) (k 0)))) (h "1f0f1sgpkaikw1ygf3zby1s46i4yp0gcx0gkj2f984kfmpgfvk79")))

(define-public crate-dessin-svg-0.4.11 (c (n "dessin-svg") (v "0.4.11") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.4.11") (d #t) (k 0)))) (h "0qbvxdvxip0802zq023ba2llbq8qwwz3xm6v8705sxf9bzgqhkqp")))

(define-public crate-dessin-svg-0.6.0 (c (n "dessin-svg") (v "0.6.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.0") (d #t) (k 0)))) (h "09g1vmag2flig6amhabhihvgjzzcshsjdihbfsv83pi045ycf8k9")))

(define-public crate-dessin-svg-0.6.1 (c (n "dessin-svg") (v "0.6.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.0") (d #t) (k 0)))) (h "1aykn356sdj1ak5zixrj8fsnsg4wzsy98xi9mzyxvji0nds7h5xs")))

(define-public crate-dessin-svg-0.6.2 (c (n "dessin-svg") (v "0.6.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.2") (d #t) (k 0)))) (h "02ff11svd7fff04zb5dl72y0cxb9krv26mcma9686dnz2syhi66d")))

(define-public crate-dessin-svg-0.6.3 (c (n "dessin-svg") (v "0.6.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.3") (d #t) (k 0)))) (h "09yszy2l6f6xvyicpfmndxvz8dydwxwrv2n3z6m9s9m1crybslp0")))

(define-public crate-dessin-svg-0.6.4 (c (n "dessin-svg") (v "0.6.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.4") (d #t) (k 0)))) (h "1v4q4nz8rbbkjayjfn682rxh6q1pq9k8kxqhv0s0h9h9ddbkqccx")))

(define-public crate-dessin-svg-0.6.5 (c (n "dessin-svg") (v "0.6.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.5") (d #t) (k 0)))) (h "1527mr8ygh57giss7nd8xd6ipvxp64c9b48hnxpx0a3ma5iags6j")))

(define-public crate-dessin-svg-0.6.6 (c (n "dessin-svg") (v "0.6.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.6") (d #t) (k 0)))) (h "15zxsq0h29b0hglx89j8653j2kjd8j83r2wa8b8yxxxfwq9wqlh6")))

(define-public crate-dessin-svg-0.6.7 (c (n "dessin-svg") (v "0.6.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.7") (d #t) (k 0)))) (h "0y8cvpsyjj85xgcc4ja07wzdx4vf3hjfmkpnfsdlhgihs555rbfn")))

(define-public crate-dessin-svg-0.6.8 (c (n "dessin-svg") (v "0.6.8") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.8") (d #t) (k 0)))) (h "11g9xmqky8hicrgaq0k505zfffj0snm4az9kapf4gn309lkdqw1s")))

(define-public crate-dessin-svg-0.6.9 (c (n "dessin-svg") (v "0.6.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.9") (d #t) (k 0)))) (h "1icmgnhkvlq8vgsn4dby3ksxnrfp0awiwnkhx71q68qn947fmic6")))

(define-public crate-dessin-svg-0.6.10 (c (n "dessin-svg") (v "0.6.10") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.10") (d #t) (k 0)))) (h "0s38sn33jx2bqr22xdg412kv64kvrn1mh4w46n4wiy7h3lpdjm71")))

(define-public crate-dessin-svg-0.6.11 (c (n "dessin-svg") (v "0.6.11") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.11") (d #t) (k 0)))) (h "1zvmbcxq3q1w50da94nrg9jg7mhbpjnl3lxvnb477fz5bx0rfz58") (y #t)))

(define-public crate-dessin-svg-0.6.12 (c (n "dessin-svg") (v "0.6.12") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.12") (d #t) (k 0)))) (h "070akvfbnkd9x4zn8vbqizwljyfz7vxb1ccq5bb7f07wnz7xlm4g")))

(define-public crate-dessin-svg-0.6.13 (c (n "dessin-svg") (v "0.6.13") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.13") (d #t) (k 0)))) (h "0fja77hdl8w42b69vn6jlv5lz9b3hsvm026bhl87fvv42ra8c8j0")))

(define-public crate-dessin-svg-0.6.14 (c (n "dessin-svg") (v "0.6.14") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.14") (d #t) (k 0)))) (h "0b5n72qmwnavch8srjv5lz82a6z7c3qk012rpg2gq0ljdrahbqrf") (y #t)))

(define-public crate-dessin-svg-0.6.15 (c (n "dessin-svg") (v "0.6.15") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.15") (d #t) (k 0)))) (h "12rsa8vnx8za5ckfc3wdb2wi95hwzyw9c8pdf0z5yyq78s83a872")))

(define-public crate-dessin-svg-0.6.16 (c (n "dessin-svg") (v "0.6.16") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.6.16") (d #t) (k 0)))) (h "1224f3zhv0dnlg4ciyqmhrasrd6vmn3vag3d3icn9lk1yvgw2a4s")))

(define-public crate-dessin-svg-0.7.0 (c (n "dessin-svg") (v "0.7.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.7.0") (d #t) (k 0)))) (h "07715g7pr3li4p8rwp31vkhqiaj8z3y4mrfgcn3ng9197qw6j95f")))

(define-public crate-dessin-svg-0.7.1 (c (n "dessin-svg") (v "0.7.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.7.1") (d #t) (k 0)))) (h "1wxy1vg402wifiw8ibfdfd72s1n3jsp7mssy4i7wq2jfr5b06rzl")))

(define-public crate-dessin-svg-0.7.2 (c (n "dessin-svg") (v "0.7.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dessin") (r "^0.7.2") (d #t) (k 0)))) (h "0kqkw3d09bcw3jb6yk3ff9rq5xjir7yndc8kbfda3br5ssgm2wgh")))

(define-public crate-dessin-svg-0.7.3 (c (n "dessin-svg") (v "0.7.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "dessin") (r "^0.7.3") (d #t) (k 0)))) (h "0wz8000iskgkl0qd8skvyvpifb5g891w5qwyqwvfwgybdy4kfrd6")))

(define-public crate-dessin-svg-0.8.1-pre (c (n "dessin-svg") (v "0.8.1-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.1-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11hbh2shc9bxh220r2vpf1nqxrxlkz6n0qi2q5r8qv2fih25dpg9")))

(define-public crate-dessin-svg-0.8.2-pre (c (n "dessin-svg") (v "0.8.2-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.2-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w1bqrsyhhx7hswfv241ry3agx6impg04nq996j87h7ps1dpphjw")))

(define-public crate-dessin-svg-0.8.3-pre (c (n "dessin-svg") (v "0.8.3-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.3-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10ypj45dsv7qyhh6n4ngrnjqwmf3sswdzp8qp5zgc304j761d0qp")))

(define-public crate-dessin-svg-0.8.4-pre (c (n "dessin-svg") (v "0.8.4-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.4-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z5x0jiy8k2vhhzv3d1kn6b7zy7dvfkcxxqpi2jkhqvihl4rwxbp")))

(define-public crate-dessin-svg-0.8.5-pre (c (n "dessin-svg") (v "0.8.5-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.5-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "044si6faxwjljar0qi1nmy295b7pgsq9i2bzzxzvs2pqilxcrz17")))

(define-public crate-dessin-svg-0.8.6-pre (c (n "dessin-svg") (v "0.8.6-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.6-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13crdjp4dz5pwdy9p93f7lk0vhnrfk3yyfcszkl7li5m1v3qkz5w")))

(define-public crate-dessin-svg-0.8.7-pre (c (n "dessin-svg") (v "0.8.7-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.7-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ak22k9y7snfi5kwlsdm1d8fgni6kdjlz0w344fyya3rwk9j21vg")))

(define-public crate-dessin-svg-0.8.8-pre (c (n "dessin-svg") (v "0.8.8-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.8-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hkw0sbmmn4w521vi682xhjyq1209k3rvl9m1pkj4wnlgg963xag")))

(define-public crate-dessin-svg-0.8.9-pre (c (n "dessin-svg") (v "0.8.9-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.9-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v0gg86xc9iw5glcqvp86fq2hrdqgv9bdv8zjbwpqza51kdsxrpb")))

(define-public crate-dessin-svg-0.8.10-pre (c (n "dessin-svg") (v "0.8.10-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.10-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g4k9zy92aggvyia5qrl952z9amiq74x0536rbdgd41dkf0qnfh2")))

(define-public crate-dessin-svg-0.8.11-pre (c (n "dessin-svg") (v "0.8.11-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.11-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vzs7mx713w5x4bayr9bcpbc20vybxk7r5v9ibld63518ikzizjr")))

(define-public crate-dessin-svg-0.8.12-pre (c (n "dessin-svg") (v "0.8.12-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.12-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1569j674bfwi59bjjfzq2nkl8xvnvkc8kxh7hn35gpdv86ghkbxj")))

(define-public crate-dessin-svg-0.8.13-pre (c (n "dessin-svg") (v "0.8.13-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.13-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f56nzcjgnacjcdv5yzhyzkrpl3lndajjkyjdq4734dlvay9x1q5")))

(define-public crate-dessin-svg-0.8.14-pre (c (n "dessin-svg") (v "0.8.14-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.14-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "027hh745yi66zvlqbzybgridhmw9blrq09hpr7yhipk5brxsi810")))

(define-public crate-dessin-svg-0.8.15-pre (c (n "dessin-svg") (v "0.8.15-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.15-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "179c795pghvq4mdjy3v7kpk07ggy7iadsl47lbwl8jrsc267vs1v")))

(define-public crate-dessin-svg-0.8.16-pre (c (n "dessin-svg") (v "0.8.16-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.16-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m3xqzqrn4w1d61m8pjpklddd9xl8ipva0qidl14xgmqw64cjvsr")))

(define-public crate-dessin-svg-0.8.17-pre (c (n "dessin-svg") (v "0.8.17-pre") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 0)) (d (n "dessin") (r "^0.8.17-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "116xqiypn43w9n2x7rgkc9fd02hdysgzpvlqn0mpr2249px1npc8")))

(define-public crate-dessin-svg-0.8.18-pre (c (n "dessin-svg") (v "0.8.18-pre") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "dessin") (r "^0.8.18-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "023l5wps5nh9a1kbvy249nmm6hx60gzzwjlc1315c2abl5sys0sh")))

(define-public crate-dessin-svg-0.8.19-pre (c (n "dessin-svg") (v "0.8.19-pre") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "dessin") (r "^0.8.19-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m1pq7p7d10hsh2gc6kfxwxhjhjhkfmqlws56s0pq560xxkika1c")))

(define-public crate-dessin-svg-0.8.20-pre (c (n "dessin-svg") (v "0.8.20-pre") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "dessin") (r "^0.8.20-pre") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02zrh3i9mvwwiacqx5x0j69ggnlq6v9crga4ybx74rc4qf81dky5")))

