(define-module (crates-io de ss dessin-contrib) #:use-module (crates-io))

(define-public crate-dessin-contrib-0.4.0 (c (n "dessin-contrib") (v "0.4.0") (d (list (d (n "dessin") (r "^0.4.2") (d #t) (k 0)))) (h "1zjwx7dlsqiy6zs0mkdfjbcyqv59a9i52ivvs6zgfcbvshx70il6") (y #t)))

(define-public crate-dessin-contrib-0.4.1 (c (n "dessin-contrib") (v "0.4.1") (d (list (d (n "dessin") (r "^0.4.3") (d #t) (k 0)))) (h "0pdllpqif4wa21b2j6r4acdql5p9zdi527nqlsz1wdzdpggb37ia") (y #t)))

