(define-module (crates-io de ss dessert-derive) #:use-module (crates-io))

(define-public crate-dessert-derive-0.1.0 (c (n "dessert-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1w7xwxx2swyp0x7rvn07irrx4ab41kf2hbnclq7n9p3d03wc85r6")))

(define-public crate-dessert-derive-0.1.2 (c (n "dessert-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1xhndgqq8yfih333h0iql5i9xh1pz60cdkzq28l8c17xbcsz9fpj")))

