(define-module (crates-io de ss dessin-pdf) #:use-module (crates-io))

(define-public crate-dessin-pdf-0.4.0 (c (n "dessin-pdf") (v "0.4.0") (d (list (d (n "dessin") (r "^0.4.6") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1wmpvxgzlg68jgjg2xfm7dz42d26qicdm8d3ddijnwrsly3nsgyv")))

(define-public crate-dessin-pdf-0.4.8 (c (n "dessin-pdf") (v "0.4.8") (d (list (d (n "dessin") (r "^0.4.8") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "02aj383rwmhgnax26i5n9mvdc7c0zgbvblbg3dyk089jdyrqfdx8")))

(define-public crate-dessin-pdf-0.4.10 (c (n "dessin-pdf") (v "0.4.10") (d (list (d (n "dessin") (r "^0.4.10") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "10kvkr9w8vgr4ch3h9ajx0yb9ln21w7rxn5n7307hhdhk3rrk0m7")))

(define-public crate-dessin-pdf-0.6.0 (c (n "dessin-pdf") (v "0.6.0") (d (list (d (n "dessin") (r "^0.6.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "120873ffrl3hymdwwz340zscz58q5bmvjln4996p1m76krzy1i6w")))

(define-public crate-dessin-pdf-0.6.1 (c (n "dessin-pdf") (v "0.6.1") (d (list (d (n "dessin") (r "^0.6.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1kzc9913i370y4b0yasfnnjba0skgqw04phapsam2za6p5k9vf3r")))

(define-public crate-dessin-pdf-0.6.2 (c (n "dessin-pdf") (v "0.6.2") (d (list (d (n "dessin") (r "^0.6.2") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "043zpzj8agm2dyi346iawx8jaws6vgdibyci8f355dfjshh0wpqs")))

(define-public crate-dessin-pdf-0.6.3 (c (n "dessin-pdf") (v "0.6.3") (d (list (d (n "dessin") (r "^0.6.3") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1na2acqc9v7nnsihh2lpdi1h5fwp88j8acdy2zq26fli2p5kgaiv")))

(define-public crate-dessin-pdf-0.6.4 (c (n "dessin-pdf") (v "0.6.4") (d (list (d (n "dessin") (r "^0.6.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0cbdwcj80mkfrgy6bwwp0pql3vmqa4x3zkj0g5v7wha81mpyxq02")))

(define-public crate-dessin-pdf-0.6.5 (c (n "dessin-pdf") (v "0.6.5") (d (list (d (n "dessin") (r "^0.6.5") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0ybskhf71niz65wfcbmdcmaadyjikhgz052c16p0cmqj5g23np77")))

(define-public crate-dessin-pdf-0.6.6 (c (n "dessin-pdf") (v "0.6.6") (d (list (d (n "dessin") (r "^0.6.6") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "08hkf0l4f38awsan6rdhldvhldjn4k8k1fj23zfv4a0hm6xwjbca")))

(define-public crate-dessin-pdf-0.6.7 (c (n "dessin-pdf") (v "0.6.7") (d (list (d (n "dessin") (r "^0.6.7") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1xy01b15hpin9r81ckdn435b0mjl58pywwd9vrlms3z53b3r3v2r")))

(define-public crate-dessin-pdf-0.6.8 (c (n "dessin-pdf") (v "0.6.8") (d (list (d (n "dessin") (r "^0.6.8") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0rzjw1mg6gp2ydw21ay3ngy1x2b275rdqw68dvy9ryky3ayjglly")))

(define-public crate-dessin-pdf-0.6.9 (c (n "dessin-pdf") (v "0.6.9") (d (list (d (n "dessin") (r "^0.6.9") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0853sj5afk6knza0qmc6j6jicm5mk2sbchkjdr9jg0a9qzx0l24p")))

(define-public crate-dessin-pdf-0.6.10 (c (n "dessin-pdf") (v "0.6.10") (d (list (d (n "dessin") (r "^0.6.10") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0l50p4k7c4jjnc9v4hfg3gb3a5zlmaa54qjyn4jrrwj218wd92qg")))

(define-public crate-dessin-pdf-0.6.11 (c (n "dessin-pdf") (v "0.6.11") (d (list (d (n "dessin") (r "^0.6.11") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0mc57wdim9wm4rili03alg9x3bnxb1hnbl2qkw7jbqs780af9h2y") (y #t)))

(define-public crate-dessin-pdf-0.6.12 (c (n "dessin-pdf") (v "0.6.12") (d (list (d (n "dessin") (r "^0.6.12") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1991grqw6ydj1dfhmf3gzvjhy0jl8kbxswdlcqqkaq04yva79f9h")))

(define-public crate-dessin-pdf-0.6.13 (c (n "dessin-pdf") (v "0.6.13") (d (list (d (n "dessin") (r "^0.6.13") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1vfy7anhzli8jpg1lxlpg5hb7imrjln8797ac65khk0vns17cxgi")))

(define-public crate-dessin-pdf-0.6.14 (c (n "dessin-pdf") (v "0.6.14") (d (list (d (n "dessin") (r "^0.6.14") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "12gdisi90sjj3brmf846d0wf942pf9cr89bqdy87hxpvazky2zcw") (y #t)))

(define-public crate-dessin-pdf-0.6.15 (c (n "dessin-pdf") (v "0.6.15") (d (list (d (n "dessin") (r "^0.6.15") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "086rzwzbjbx37qs1jbxxmizw09yzcfz2zlzk7dj81b7n6fg1dv9f")))

(define-public crate-dessin-pdf-0.6.16 (c (n "dessin-pdf") (v "0.6.16") (d (list (d (n "dessin") (r "^0.6.16") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.0") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1ibrplwq89w2p98s23jlq3nr3zxxaahnwi668kzj9wmnv0359jz2")))

(define-public crate-dessin-pdf-0.7.0 (c (n "dessin-pdf") (v "0.7.0") (d (list (d (n "dessin") (r "^0.7.0") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.1") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1fppm7r7wbfqwinianrffr710m5804zhxmcmkfp5v9m4h22r4r00")))

(define-public crate-dessin-pdf-0.7.1 (c (n "dessin-pdf") (v "0.7.1") (d (list (d (n "dessin") (r "^0.7.1") (d #t) (k 0)) (d (n "printpdf") (r "^0.4.1") (f (quote ("webp"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1g2xwa5lmda03l2w7a30vv8z9zj42jjrxcpcf4vcz5fsa7y9cjyc")))

(define-public crate-dessin-pdf-0.7.2 (c (n "dessin-pdf") (v "0.7.2") (d (list (d (n "dessin") (r "^0.7.2") (d #t) (k 0)) (d (n "printpdf") (r "^0.5.3") (f (quote ("webp" "svg"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1ms9325x8v4j4lcpdw915vyflzyh2srbdm1skfifikz6rd65q6jm")))

(define-public crate-dessin-pdf-0.7.3 (c (n "dessin-pdf") (v "0.7.3") (d (list (d (n "dessin") (r "^0.7.3") (d #t) (k 0)) (d (n "printpdf") (r "^0.5.3") (f (quote ("webp" "svg"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0058cmd0g0njsi6g4rmr6kb7ki7da6fxyy7jkg5qbx7v5irax704")))

(define-public crate-dessin-pdf-0.8.18-pre (c (n "dessin-pdf") (v "0.8.18-pre") (d (list (d (n "dessin") (r "^0.8.18-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "printpdf") (r "^0.6") (f (quote ("webp" "svg"))) (d #t) (k 0)))) (h "1c8xmsin9fq0w0g5nfnzs25nkfq7kwqif7ivfm7p279rcn3vi726")))

(define-public crate-dessin-pdf-0.8.19-pre (c (n "dessin-pdf") (v "0.8.19-pre") (d (list (d (n "dessin") (r "^0.8.19-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "printpdf") (r "^0.6") (f (quote ("webp" "svg"))) (d #t) (k 0)))) (h "091b2sff6b3v2nzwzqajwr0dp6nsn48g0q69ynlyq64a3mkh382n")))

(define-public crate-dessin-pdf-0.8.20-pre (c (n "dessin-pdf") (v "0.8.20-pre") (d (list (d (n "dessin") (r "^0.8.20-pre") (d #t) (k 0)) (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "printpdf") (r "^0.6") (f (quote ("webp" "svg"))) (d #t) (k 0)))) (h "1v83wpr2xk5n3pwjq8j3vs1b4za59cgx02v388qi38x6ilynci1q")))

