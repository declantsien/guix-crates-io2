(define-module (crates-io de sp despell) #:use-module (crates-io))

(define-public crate-despell-1.0.0 (c (n "despell") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "15lq6i2qmgdw5jbn1f2rjv78irnyxdnivgcwbnamrswcdgl8jy1k")))

(define-public crate-despell-1.0.1 (c (n "despell") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1cws9pay6ryijvanqv7dn6z1nr0yvrrli8n49dsqkmfgw2xb8fwy")))

