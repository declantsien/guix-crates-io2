(define-module (crates-io de sp despatma) #:use-module (crates-io))

(define-public crate-despatma-0.1.0 (c (n "despatma") (v "0.1.0") (d (list (d (n "despatma-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "tokenstream2-tmpl") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11806vnw8niknsa4j4crxh2kp48mq2icdgz86m5d9xpf3daxf8is")))

