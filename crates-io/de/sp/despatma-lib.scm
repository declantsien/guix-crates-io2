(define-module (crates-io de sp despatma-lib) #:use-module (crates-io))

(define-public crate-despatma-lib-0.1.0 (c (n "despatma-lib") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "tokenstream2-tmpl") (r "^0.1") (d #t) (k 0)))) (h "1idq8xvg0ciwllmyjbgl2r7cgpdm2k09k7y10b3907qva5iyg9r1") (f (quote (("extra-traits"))))))

