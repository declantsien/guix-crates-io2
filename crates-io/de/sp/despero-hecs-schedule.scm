(define-module (crates-io de sp despero-hecs-schedule) #:use-module (crates-io))

(define-public crate-despero-hecs-schedule-0.6.2 (c (n "despero-hecs-schedule") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "atomic_refcell") (r "^0.1.8") (d #t) (k 0)) (d (n "hecs") (r "^0.9.1-f") (f (quote ("macros"))) (d #t) (k 0) (p "despero-hecs")) (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z3wcr2gyg48h7zj4i2qpmpi5spqf7src1y0acc48fifagp0dj6w") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

