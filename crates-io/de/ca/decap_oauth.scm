(define-module (crates-io de ca decap_oauth) #:use-module (crates-io))

(define-public crate-decap_oauth-0.0.1 (c (n "decap_oauth") (v "0.0.1") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09wq6gbc5gcqy9fsimggdf7v94bhh7qp2rwdqm2lr6xc449dvm2i")))

(define-public crate-decap_oauth-0.0.2 (c (n "decap_oauth") (v "0.0.2") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jlmygf6n8bz1lq5bmvsvxxzj67jdr0qsf6ncp7bn96ln6mj39xj")))

