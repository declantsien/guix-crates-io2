(define-module (crates-io de ca decasm) #:use-module (crates-io))

(define-public crate-decasm-0.1.0 (c (n "decasm") (v "0.1.0") (h "05kgm84l2ffkjhrsylz5icd7iv2csyjidvk69b37yklpnnj4cdhc")))

(define-public crate-decasm-0.1.1 (c (n "decasm") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1mz4m6q8iqv5lvdmnrnk2f5c6vj0267yqj2afdl43fxf1v2vgqhn")))

(define-public crate-decasm-0.1.2 (c (n "decasm") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "11glngz37sqlsi31zh5db79cy29r69cw1bl7hg4ri840f43ys16w")))

(define-public crate-decasm-0.1.3 (c (n "decasm") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "00h98b9y7vxw5gj9hnbalbid0yvq32lv62s6zjzr6k65wym7f4pa")))

