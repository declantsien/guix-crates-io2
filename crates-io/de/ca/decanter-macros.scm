(define-module (crates-io de ca decanter-macros) #:use-module (crates-io))

(define-public crate-decanter-macros-0.1.6 (c (n "decanter-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1x1vmhncsjb6k7q5v0mnbzz7wqgjijyl129l9bm8h68m5j8252ll")))

