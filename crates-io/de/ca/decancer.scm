(define-module (crates-io de ca decancer) #:use-module (crates-io))

(define-public crate-decancer-1.3.3 (c (n "decancer") (v "1.3.3") (h "11g7rvlnlxyfl531ivrnr2nj1cql75ips192i1a0z352js6mzild")))

(define-public crate-decancer-1.4.0 (c (n "decancer") (v "1.4.0") (h "16cz4j1bzsf7s6qwip4vgbzki48lrcj55pl3zv8kjgs1m9i3pr0j")))

(define-public crate-decancer-1.4.1 (c (n "decancer") (v "1.4.1") (h "1z42m3pbwf62b83fn7qr0wi83dkh8whmp6xw9hy838a72ms6y4sb")))

(define-public crate-decancer-1.5.1 (c (n "decancer") (v "1.5.1") (h "1n8sqlrficd7nw3sb0x92yhny5wj7zh2dilc1q5g7d4yayidm7a3")))

(define-public crate-decancer-1.5.2 (c (n "decancer") (v "1.5.2") (h "1l5hsldg8j10bby3m15428ypji2s51ihszmkxjzjxqz95x0ahxnq")))

(define-public crate-decancer-1.5.3 (c (n "decancer") (v "1.5.3") (h "0s1j9ip3rp5g48rg6zdvczf7d7yfpjlxjaw712nl07qvpmhd6j2g")))

(define-public crate-decancer-1.5.4 (c (n "decancer") (v "1.5.4") (h "0lrrsa22ly1ljyamayma2s7yh4fahv01igfm0cyf3pr435z4wrww")))

(define-public crate-decancer-1.5.5 (c (n "decancer") (v "1.5.5") (h "0g2v8mcvnw4iq7mlra2svkz3m2xbfk1pd5w5abc5zmkm0gqzhm6q")))

(define-public crate-decancer-1.6.0 (c (n "decancer") (v "1.6.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "025fabav7b4sj04gxpzrg7vyvip5zwjk864lvj10qwzhvxzd2kr8") (f (quote (("std") ("default" "std"))))))

(define-public crate-decancer-1.6.1 (c (n "decancer") (v "1.6.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0gdb42hihas701hkws07pblz0ns7k0lbv54mjy9i8d7m5klbjr0p") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-1.6.2 (c (n "decancer") (v "1.6.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ff8rd3r3mvq85yvva3im4an0ynpiv2aphdz6zn7j831vskjg0c0") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-1.6.3 (c (n "decancer") (v "1.6.3") (d (list (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "148mbck1s809jiis7fs0mxrkfsi9h55ydl00fgsqrsyry2k145xh") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-1.6.4 (c (n "decancer") (v "1.6.4") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0awyrdq4bmnl91lrjxw4bq941im00lmi1r6kqawk1zcwf8wvw4d6") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-1.6.5 (c (n "decancer") (v "1.6.5") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03cx6m0c9nj7y7h26jww0yfw03cvwm94xia7ihyw49ddmpv0j2q8") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-2.0.0 (c (n "decancer") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vy56xxd67yb9pwjqkfb3nlfyh7yqzjx7w5yv5q1lrw5c2amrf79") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-2.0.1 (c (n "decancer") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1xl6y8sal1nc22c3lkrrrw09sfqcd2zp0s722awgdzm5ll0jabxl") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-2.0.2 (c (n "decancer") (v "2.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03qc64n9s0gcnawl7p5zisk8839cnbaw0yj4iy8z52nc81qc9x0l") (f (quote (("std") ("default" "std")))) (r "1.64.0")))

(define-public crate-decancer-3.0.0 (c (n "decancer") (v "3.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0nxriaykfv3kn7qgx0f7v8g06j1nr8b8xkzpw4zbkjyxc33lpjg3") (r "1.64.0")))

(define-public crate-decancer-3.0.1 (c (n "decancer") (v "3.0.1") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0a69ffzrw93f2nm638qrdfqyqlnr27grlcx3r2jv2zild6yqb5xl") (r "1.64.0")))

(define-public crate-decancer-3.0.2 (c (n "decancer") (v "3.0.2") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0yjm2dmi6f96h5bg4ljpmfpdd4jgg9wpgqdf6l5v1sg9s4hy40q1") (r "1.64.0")))

(define-public crate-decancer-3.1.0 (c (n "decancer") (v "3.1.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qcfihhfra263gwm0hnjcbcl1c0gzx7c05h3xlb7c0yz3kib6zlj") (f (quote (("options") ("leetspeak" "regex" "lazy_static") ("default" "options" "leetspeak")))) (r "1.64.0")))

(define-public crate-decancer-3.1.1 (c (n "decancer") (v "3.1.1") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0c3xj6mfhsx7mzg9gs1ca4xb5ciq8crn1pbbijbz1i1zdmw86q0v") (f (quote (("options") ("leetspeak" "regex" "lazy_static") ("default" "options" "leetspeak")))) (r "1.64.0")))

(define-public crate-decancer-3.1.2 (c (n "decancer") (v "3.1.2") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "10w3gki7jhwzs8ljidb70km4pbqa0andka8qpwi08wqvvzrbnjjl") (f (quote (("options") ("leetspeak" "regex" "lazy_static") ("default" "options" "leetspeak")))) (r "1.64.0")))

(define-public crate-decancer-3.2.0 (c (n "decancer") (v "3.2.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "07iv93843p9h2g0lbbkzm1jc2vs634ckn4w53b768yrm0bmx2lx0") (f (quote (("options") ("leetspeak" "regex" "lazy_static") ("default" "options" "leetspeak")))) (r "1.65.0")))

