(define-module (crates-io de ca deca) #:use-module (crates-io))

(define-public crate-deca-0.0.1 (c (n "deca") (v "0.0.1") (h "06059iqr3bqwbwvpkyz2k87vl53vn4rv1a90agdj706l9ardc3lg")))

(define-public crate-deca-0.0.2 (c (n "deca") (v "0.0.2") (h "1zxxqy5n47nrxawcwfvfchvngj8csk3gmwnn111mcpy93vd9f770")))

(define-public crate-deca-0.0.3 (c (n "deca") (v "0.0.3") (h "0719jnq166mm3x1q9qmynbjpw6576l6zij4r1nh6af68q82pj9rg")))

(define-public crate-deca-0.0.4 (c (n "deca") (v "0.0.4") (h "0kl0ds2kncj5vqn8rcgxgcjq15f2zrmdfvkm06cc2qkdhgwsvvs5")))

(define-public crate-deca-0.0.5 (c (n "deca") (v "0.0.5") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0b44vnci3a0dgmnm8qgjs2gbbq7cjyyi5f771g92lnla8hziyw7w")))

(define-public crate-deca-0.0.6 (c (n "deca") (v "0.0.6") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1hga4q1fh03qr309sz6qa4hrynj4spx9p1mmbiarljpdbxrkyag1")))

(define-public crate-deca-0.0.7 (c (n "deca") (v "0.0.7") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "octopt") (r "^0.1.3") (d #t) (k 0)))) (h "12398875pq4l5g5kaymqygvnq3yii0lmqszmn8zcyz1v760jy3cp")))

(define-public crate-deca-0.0.8 (c (n "deca") (v "0.0.8") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "octopt") (r "^0.1") (d #t) (k 0)))) (h "0yxwq1nvsb6z0776z5j4i6b5fmyn8pfk1628aj6a99cacks3qdjr")))

(define-public crate-deca-0.0.9 (c (n "deca") (v "0.0.9") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "octopt") (r "^0.1") (d #t) (k 0)))) (h "1l6074s57lx39k68nm32w1rfl1cadmvrw0msz38ig7bnz3h7ry20")))

(define-public crate-deca-0.0.10 (c (n "deca") (v "0.0.10") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "octopt") (r "^1.0") (d #t) (k 0)))) (h "0i73w620z2a3npa366yrfnjxvb41g0bwiwfyrqksl5iw5pzway8s")))

