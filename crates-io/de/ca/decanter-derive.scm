(define-module (crates-io de ca decanter-derive) #:use-module (crates-io))

(define-public crate-decanter-derive-0.1.1 (c (n "decanter-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nv4lc0zs7igavrrqscfk7laz2hdv99n6lrr6aggaijj7fqy173q")))

(define-public crate-decanter-derive-0.1.2 (c (n "decanter-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwpcf7w318h3ga37nf39pjyv1njk9ngd6x6yiw9d8y43vf0prqk")))

(define-public crate-decanter-derive-0.1.3 (c (n "decanter-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cgpgljn6ka9y2vs2y2a070zv3zrsgwp0a9jcf3241h76647946g")))

(define-public crate-decanter-derive-0.1.4 (c (n "decanter-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0s9mk6v1bh0srprrc1cd4gq7plpm0a07f2pib69p89m02vrr8wfi")))

(define-public crate-decanter-derive-0.1.5 (c (n "decanter-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rbk8wb3xmv4vp53766sxkbqihv5rgi0rjhn9lxim81jjn3aid52")))

(define-public crate-decanter-derive-0.1.6 (c (n "decanter-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "187ibknxjgnqrmz0z907j84fjdhz3xq2zbv76pchgk59zqxsxkzv")))

