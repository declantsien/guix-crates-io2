(define-module (crates-io de ca decay) #:use-module (crates-io))

(define-public crate-decay-0.1.0 (c (n "decay") (v "0.1.0") (h "0x7pf85z24vsxygm44lcs8k5y4chy3xbvgcrw7scf4h7fh29lg7n")))

(define-public crate-decay-0.1.1 (c (n "decay") (v "0.1.1") (h "10ail88h4hzq6vdpbkp9pc5m6yny53341v2mzlaxnrpi5kicpcfz")))

