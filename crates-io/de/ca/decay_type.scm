(define-module (crates-io de ca decay_type) #:use-module (crates-io))

(define-public crate-decay_type-0.1.0 (c (n "decay_type") (v "0.1.0") (h "0wz5r3yg5d0mvdcn2r8mglnrivlsbqr3r40nip5d20r2g6wa4007")))

(define-public crate-decay_type-0.1.1 (c (n "decay_type") (v "0.1.1") (h "0p3swkv3wmqvl2zb9i57q2v72xzj241dd0py5h7fih6686s5c0n5")))

