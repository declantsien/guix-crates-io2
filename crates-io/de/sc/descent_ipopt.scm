(define-module (crates-io de sc descent_ipopt) #:use-module (crates-io))

(define-public crate-descent_ipopt-0.3.0 (c (n "descent_ipopt") (v "0.3.0") (d (list (d (n "descent") (r "^0.3") (d #t) (k 0)) (d (n "descent_macro") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zdyfz2aji2lf9a6gfg0infg8p01hy09hkvjf70dw4knzl8pys52")))

(define-public crate-descent_ipopt-0.4.0 (c (n "descent_ipopt") (v "0.4.0") (d (list (d (n "descent") (r "^0.4") (d #t) (k 0)) (d (n "descent_macro") (r "^0.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1l4piiaip8nayhq7a50f5h8hawx4mkh8f0br7qk4080h6l5hkzl9")))

(define-public crate-descent_ipopt-0.5.0 (c (n "descent_ipopt") (v "0.5.0") (d (list (d (n "descent") (r "^0.5") (d #t) (k 0)) (d (n "descent_macro") (r "^0.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0sqgm40jjdlwicq163ryv2w492ywmkzsl23dj6bb84bjyvlx3mjj")))

