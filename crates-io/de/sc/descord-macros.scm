(define-module (crates-io de sc descord-macros) #:use-module (crates-io))

(define-public crate-descord-macros-0.1.0 (c (n "descord-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "039437md24ha46di6yrgwfw778h2jbzka8iyq39dazy0r125xkbf")))

(define-public crate-descord-macros-0.1.1 (c (n "descord-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "0vycqvrx4as6ch35552wvw1rm2v0czxhm6bm3ys9d07y5y855nyr")))

(define-public crate-descord-macros-0.1.2 (c (n "descord-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0c6qhm76fcyfrh9ccqh4rkaivjnwdxkczrim8hci0kppfwwrgb0p")))

(define-public crate-descord-macros-0.1.3 (c (n "descord-macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "030m64721wzxsz5bh4f2phsbh6nan5aq89zcxh7zn9y1y8g1wgz7")))

(define-public crate-descord-macros-0.1.4 (c (n "descord-macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0xmcdfzfqvxjpc0lr1xm9xz7981pmipbnrkq0lvk6qxrb4psjf2q")))

