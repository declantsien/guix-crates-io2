(define-module (crates-io de sc descent) #:use-module (crates-io))

(define-public crate-descent-0.1.0 (c (n "descent") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w0s56abiaqllnwz5mcx2lirqq4z40yrcllhq1a97s0mkp4fkghd")))

(define-public crate-descent-0.2.0 (c (n "descent") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cnmhzxdz3sbic3c946yjwvxp4vyrb51hbpxbxldb8bip9094clj")))

(define-public crate-descent-0.3.0 (c (n "descent") (v "0.3.0") (h "02v650w5q350avz1dv04w5k94v8w6llii4jkjyp8dh5800z41d3v")))

(define-public crate-descent-0.4.0 (c (n "descent") (v "0.4.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1ir3smxjkbqf5npfpgz47yrrq86j4swknzmcv506svwwz5ly3g1g")))

(define-public crate-descent-0.5.0 (c (n "descent") (v "0.5.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1i1xsc4cjs3r6svq19zhba51zg2wycaspc3i6p21z6nrrl8dl01i")))

