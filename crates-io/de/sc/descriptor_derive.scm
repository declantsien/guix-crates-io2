(define-module (crates-io de sc descriptor_derive) #:use-module (crates-io))

(define-public crate-descriptor_derive-0.0.1 (c (n "descriptor_derive") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lv0fqm32j4mdv1zz2wiifhiq545p3zxhbyyjkndr40limk1zzq3")))

(define-public crate-descriptor_derive-0.0.3 (c (n "descriptor_derive") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w94wrrgaw993c42pr98yhfpkrw91xv0ahzqbzyqar673m078i4b")))

(define-public crate-descriptor_derive-0.0.4 (c (n "descriptor_derive") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jld3v359m1750yhlx450c4qs4q5nnlpg3nbz6zzpnk5729ivi1n")))

