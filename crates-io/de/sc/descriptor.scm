(define-module (crates-io de sc descriptor) #:use-module (crates-io))

(define-public crate-descriptor-0.0.1 (c (n "descriptor") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "descriptor_derive") (r "=0.0.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)))) (h "1m6gd552qdm1scxdwcywypp1mvyi0qn30v0w7lf79i13wmps1srh")))

(define-public crate-descriptor-0.0.1-patch-readme (c (n "descriptor") (v "0.0.1-patch-readme") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "descriptor_derive") (r "=0.0.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)))) (h "1h0nch9gzl98xxcvjqa8cswq0wjmsl2v9n9km3nqvykazj0mpdvb") (y #t)))

(define-public crate-descriptor-0.0.2 (c (n "descriptor") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "descriptor_derive") (r "=0.0.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)))) (h "1h4xxvsl3gz10si3pykpkqbhnlla5r35x59i9nv0n5ds9ffwc2gg")))

(define-public crate-descriptor-0.0.3 (c (n "descriptor") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "descriptor_derive") (r "=0.0.3") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)))) (h "16vbv25s28rhb7pc0bx3xl76fzpw33lkmrjra6jl02hckl1f4nlr")))

(define-public crate-descriptor-0.0.4 (c (n "descriptor") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "descriptor_derive") (r "=0.0.4") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)))) (h "1am3ayx0g9rmw9x41fih9w5vnicvzkbwpab0sb859k2zbr05cs24")))

