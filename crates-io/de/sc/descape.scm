(define-module (crates-io de sc descape) #:use-module (crates-io))

(define-public crate-descape-1.0.0 (c (n "descape") (v "1.0.0") (h "1zns55dalmhkjn8rqsk27i89n56pr0a71c4hdwpr6sagb9z0b6pj") (y #t)))

(define-public crate-descape-1.0.1 (c (n "descape") (v "1.0.1") (h "14nzzqynh36x5zssqy83nx2c2q4nhwsjr0wbn89sqy45hdd11g8a") (y #t)))

(define-public crate-descape-1.1.0 (c (n "descape") (v "1.1.0") (h "1aq59car7lyz31qqp23j0bkv33q43va191q147y2yll9adz961ih") (y #t)))

(define-public crate-descape-1.1.1 (c (n "descape") (v "1.1.1") (h "0ij6nwr21clgs0c0y059xx5jgwjc6qs738wm16xzkhj0qwymlmns")))

(define-public crate-descape-1.1.2 (c (n "descape") (v "1.1.2") (h "135nq4k1pr37556i12wvjiig32jb5hbdfl825gvbay7g5cqhlsir")))

