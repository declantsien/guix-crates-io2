(define-module (crates-io de sc descriptive_toml_derive_macro) #:use-module (crates-io))

(define-public crate-descriptive_toml_derive_macro-0.1.0 (c (n "descriptive_toml_derive_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1qcpq0nn1gbsix72fsan2wyr8rbx0pi1c053qy63imsh2zjglf1q")))

