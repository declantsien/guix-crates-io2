(define-module (crates-io de sc desc) #:use-module (crates-io))

(define-public crate-desc-0.1.0 (c (n "desc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjbf5djdkfl3dikkzps36yiw0lyf02w8nch9cl1bxb07yi9qpnv")))

