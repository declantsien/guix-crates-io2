(define-module (crates-io de sc descargador) #:use-module (crates-io))

(define-public crate-descargador-0.1.0 (c (n "descargador") (v "0.1.0") (h "1xnfxbnsb3xk6flrwd82bfvzgwzv5f90c64zxh38hvwh6zx61fnn") (y #t)))

(define-public crate-descargador-0.0.1 (c (n "descargador") (v "0.0.1") (d (list (d (n "cargo-audit") (r "^0.17.6") (d #t) (k 2)))) (h "10znwp2p701yjrylx8444jlfmzrpqs3r4s5149xbb9wfqikz4b2f")))

