(define-module (crates-io de sc descent_macro) #:use-module (crates-io))

(define-public crate-descent_macro-0.3.0 (c (n "descent_macro") (v "0.3.0") (d (list (d (n "descent") (r "^0.3") (d #t) (k 2)))) (h "11sjxgpn6lwpwlrnic3ll32zn1i81ni0gk7nb7slxkpf93ai82li")))

(define-public crate-descent_macro-0.3.1 (c (n "descent_macro") (v "0.3.1") (d (list (d (n "descent") (r "^0.3") (d #t) (k 2)))) (h "1zad76igw49m8kyg9lidkfqcgl2dk54b317q8bn941xzpifbdvyn")))

(define-public crate-descent_macro-0.4.0 (c (n "descent_macro") (v "0.4.0") (d (list (d (n "descent") (r "^0.4") (d #t) (k 2)))) (h "0p2p1wy4wjjhvslmcn61lfa94wag098qiw0n5vj4xk8zz1agrzji")))

(define-public crate-descent_macro-0.5.0 (c (n "descent_macro") (v "0.5.0") (d (list (d (n "descent") (r "^0.5") (d #t) (k 2)))) (h "0wl2hifph4slcnq5r9wyww92w4j3vdzpgap2kf9g0fp36525wd00")))

