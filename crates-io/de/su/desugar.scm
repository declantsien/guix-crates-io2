(define-module (crates-io de su desugar) #:use-module (crates-io))

(define-public crate-desugar-0.1.0 (c (n "desugar") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "10qr5vz9yvd91l8jykpl9sf34vvdb0dwh35v40lg4v3b6xqcayyr")))

(define-public crate-desugar-0.1.1 (c (n "desugar") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0ij37x5yqyc8jcpjyw2j2cqvw56f2a8c2a4v6zm3qzs1mb07xsfa")))

