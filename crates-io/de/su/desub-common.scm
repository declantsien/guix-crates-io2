(define-module (crates-io de su desub-common) #:use-module (crates-io))

(define-public crate-desub-common-0.1.0 (c (n "desub-common") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^4.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^4.0.0") (d #t) (k 0)))) (h "0km79a8dq7kablf81n9qqs7rbqhy90mlirxcin45gh6jd35v3h1g")))

