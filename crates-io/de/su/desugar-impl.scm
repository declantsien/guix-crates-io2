(define-module (crates-io de su desugar-impl) #:use-module (crates-io))

(define-public crate-desugar-impl-0.1.0 (c (n "desugar-impl") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19fdfx76yiavgxw1aqpynz66jhca26b1cb56cczjz9k19pj82dpj") (y #t)))

(define-public crate-desugar-impl-0.1.1 (c (n "desugar-impl") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lch7x943l4xyl7k1w6q2644gl0rj4g81hghx8y6rxxrxch5hjkd") (y #t)))

