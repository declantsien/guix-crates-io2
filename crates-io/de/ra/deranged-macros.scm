(define-module (crates-io de ra deranged-macros) #:use-module (crates-io))

(define-public crate-deranged-macros-0.0.0-alpha.0 (c (n "deranged-macros") (v "0.0.0-alpha.0") (h "0gd6bzzcflr683vlga43mrjxw40z0dziny29dafwv5pn17xxhz4r") (y #t)))

(define-public crate-deranged-macros-0.1.0 (c (n "deranged-macros") (v "0.1.0") (h "1isg16g5jkwsgh8rdhplg30x424jybsqix0p75xnndfwph51mjwr")))

