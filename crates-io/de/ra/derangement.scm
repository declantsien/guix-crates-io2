(define-module (crates-io de ra derangement) #:use-module (crates-io))

(define-public crate-derangement-0.1.1 (c (n "derangement") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "0ii5y4dik9i5p109cg7khk256q9qk0k2vmz32nzv1z4d4hh8pv0g")))

(define-public crate-derangement-0.1.3 (c (n "derangement") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "00gjqqhymnngy72y6jfnlqcj8sv0nbwjdhwg6nnnxmp4jmn7hmw3")))

