(define-module (crates-io de x_ dex_checksum_tools) #:use-module (crates-io))

(define-public crate-dex_checksum_tools-0.1.0 (c (n "dex_checksum_tools") (v "0.1.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0bsvhl344iv6zqi94hbniwnv3cdz0idk2zn1vaykwyp3p90plhvl")))

