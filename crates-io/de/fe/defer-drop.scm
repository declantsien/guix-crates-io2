(define-module (crates-io de fe defer-drop) #:use-module (crates-io))

(define-public crate-defer-drop-1.0.0 (c (n "defer-drop") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0y7rdw3jfkcnr95i5mvb4xk9akv9kmq3gsqqr9qb9x4d74anpdg8")))

(define-public crate-defer-drop-1.0.1 (c (n "defer-drop") (v "1.0.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1d3pmmn5k2ir3yv8z8fnv4jprs5aijkz5pbdyl8x8kp18m90bbhq")))

(define-public crate-defer-drop-1.1.0 (c (n "defer-drop") (v "1.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0wlyyd40mhibrlpdsicfnmgad9042cfydbcclmf8bmwzi4d89r57")))

(define-public crate-defer-drop-1.2.0 (c (n "defer-drop") (v "1.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (k 0)))) (h "1w505xzkzlrkby5cwja84l38nnr5qv5hk4ii18rb0ha3bq7cm2l2")))

(define-public crate-defer-drop-1.3.0 (c (n "defer-drop") (v "1.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0mswjjksrr6fvsgxvp64mzwkjkzjmpwjfaw4n76jhsvalsgyq4zn")))

