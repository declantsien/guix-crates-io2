(define-module (crates-io de fe deferred_vector) #:use-module (crates-io))

(define-public crate-deferred_vector-0.1.0 (c (n "deferred_vector") (v "0.1.0") (h "1fqiw71alnyp9yj6nmq2mpj7blhf9pbp5yjcya5yqmaghgwpjzs2")))

(define-public crate-deferred_vector-0.1.1 (c (n "deferred_vector") (v "0.1.1") (h "1ivp6abric897jfb9p960w92irlgxdf67nv0zpgg6vbk19dlj0cq")))

(define-public crate-deferred_vector-0.1.2 (c (n "deferred_vector") (v "0.1.2") (h "15bc8zlhckg82p9q8k423zhn5q3q8wf3xh445i433qlbb4mpk275")))

