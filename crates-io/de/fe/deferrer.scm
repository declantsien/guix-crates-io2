(define-module (crates-io de fe deferrer) #:use-module (crates-io))

(define-public crate-deferrer-0.1.1 (c (n "deferrer") (v "0.1.1") (h "1hyqy8yznx15j31x7slxnir8dag3fk1z2vdc7nvnvvpmsmzlabnw")))

(define-public crate-deferrer-0.1.2 (c (n "deferrer") (v "0.1.2") (h "01rd3llgq8n6pwc5kspmih4wirxxl46f5vyh9ixvmrvpxj4nmfxb")))

(define-public crate-deferrer-0.1.3 (c (n "deferrer") (v "0.1.3") (h "190k74paa9sbnccii0qk51pg9yhvi2kwj37w5709gjniip906sgy")))

(define-public crate-deferrer-0.1.4 (c (n "deferrer") (v "0.1.4") (h "0llwlkc6cdzr1928g086vgp7xnaczv6phhg1h2s95j1jb68pc0zx")))

