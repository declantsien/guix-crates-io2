(define-module (crates-io de fe deferred-reference) #:use-module (crates-io))

(define-public crate-deferred-reference-0.1.0 (c (n "deferred-reference") (v "0.1.0") (h "0n3apbsfd9grf5sl7jjd84grbjgvar5p3ngv3d1y6xdmkgmahaim") (f (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized")))) (y #t)))

(define-public crate-deferred-reference-0.1.1 (c (n "deferred-reference") (v "0.1.1") (h "1fznh6lw2fv1w2wxfr6ix8gs9s1asi3gb44yw429zw946sw20p33") (f (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized"))))))

(define-public crate-deferred-reference-0.1.2 (c (n "deferred-reference") (v "0.1.2") (h "0d4hmdvnnlga6gzrbiby5s7jl90ik5xdgr90gwmpaaavqmxv6n90") (f (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized"))))))

