(define-module (crates-io de fe defer) #:use-module (crates-io))

(define-public crate-defer-0.1.0 (c (n "defer") (v "0.1.0") (h "1pmcz4av2wvw8zrccmac86dsyy34qlwacdhajp1qjpjx6jk0axk4")))

(define-public crate-defer-0.2.0 (c (n "defer") (v "0.2.0") (h "14k1q2c4djdw0bayzknzhf8f8mn350nrq0dfxblf2w16rbhf07b9")))

(define-public crate-defer-0.2.1 (c (n "defer") (v "0.2.1") (h "1s7qig25n27rajvsn013sb8k6bgdv67936yz5dwb37yzr1qp234k")))

