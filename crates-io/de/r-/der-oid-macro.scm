(define-module (crates-io de r- der-oid-macro) #:use-module (crates-io))

(define-public crate-der-oid-macro-0.1.0 (c (n "der-oid-macro") (v "0.1.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1kabnrfp7jrb2ws3id74n5lp9kmiba99x86f4kvww616s7kqsw3l")))

(define-public crate-der-oid-macro-0.2.0 (c (n "der-oid-macro") (v "0.2.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1bbp68qzqbpy5l4llpp9rspgbj78ggh7nssn83h3nb3pkmi5hrg6")))

(define-public crate-der-oid-macro-0.3.0 (c (n "der-oid-macro") (v "0.3.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1z1jjw3jlsas7pj23bzn33y6kirs911f5ggxd51p0pl7rwzd25xx")))

(define-public crate-der-oid-macro-0.4.0 (c (n "der-oid-macro") (v "0.4.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mjgpd0c9glranngv2gjbfjkzr6hmsagi0d52nhzrh4qpdhczk54")))

(define-public crate-der-oid-macro-0.5.0 (c (n "der-oid-macro") (v "0.5.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dply8g2p72hfhyymkrkr7fjqy844drj19xbrfkqrp55nq4z4fn7")))

