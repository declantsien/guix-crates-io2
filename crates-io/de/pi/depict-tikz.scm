(define-module (crates-io de pi depict-tikz) #:use-module (crates-io))

(define-public crate-depict-tikz-0.1.0 (c (n "depict-tikz") (v "0.1.0") (d (list (d (n "depict") (r "^0.1") (f (quote ("cvxpy" "minion"))) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1k516cx3arhgl4gb31mahhidms4300fibglvgpasas07jry0a104")))

