(define-module (crates-io de ep deepl-api-client) #:use-module (crates-io))

(define-public crate-deepl-api-client-0.1.0 (c (n "deepl-api-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1l6dlb98y5f89rar8nsqdsgzcrjqak88q0vjz0bf0k6x8xxdbg3y")))

(define-public crate-deepl-api-client-0.2.0 (c (n "deepl-api-client") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0hqic494n1f2xaz39whapb77wald0vf5pngbndjvfw8djnxq0f8n")))

(define-public crate-deepl-api-client-0.2.1 (c (n "deepl-api-client") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4") (f (quote ("json"))) (d #t) (k 0)))) (h "1jdxp3gmdnhpf504va4n5fxyzciwan1farhw5lyw9pkgysni4lpb")))

(define-public crate-deepl-api-client-0.2.2 (c (n "deepl-api-client") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4") (f (quote ("json"))) (d #t) (k 0)))) (h "1lwk29p1h99rxxnrbpyjnns994p394lwg51znpy4ldsg0mdc0jfk")))

