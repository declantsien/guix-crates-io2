(define-module (crates-io de ep deep_causality_macros) #:use-module (crates-io))

(define-public crate-deep_causality_macros-0.4.0 (c (n "deep_causality_macros") (v "0.4.0") (h "0m13ksc7a4xg04q3xpgp9fbj75wl1cxdx2bpgrwks6hb6d1j0pw6") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.1 (c (n "deep_causality_macros") (v "0.4.1") (h "1293yk80rgm77csizyzlmq7bdh1jsgjdljans31n14ah5gfjzf0b") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.2 (c (n "deep_causality_macros") (v "0.4.2") (h "0086k4shfgnrzbd8fna6p9c6g3p8qcs39ik2xhvxg1ik0jsgww8c") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.3 (c (n "deep_causality_macros") (v "0.4.3") (h "1wqjp1zkk8nci3fvc84kg2v6rgmkxq7zavx6fkyybpxssrlb7dk6") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.4 (c (n "deep_causality_macros") (v "0.4.4") (h "01nzjb5svb0piah4zhs41s8r1cg70ddzbh3yg33fny0xvlyhr7pp") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.5 (c (n "deep_causality_macros") (v "0.4.5") (h "17aaflq0j92z72hp7d7qfx0kqscgm88drqh172hfv2bv05b2nm64") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.6 (c (n "deep_causality_macros") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1x9j330dhwmkwp4iiipsgrblnxg93m9g0g68n4lv21dpxib92jds") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.7 (c (n "deep_causality_macros") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17lmmhm6729wc0a6lhcdx41dgwzxirzagcvxbk6nm1wyvj6hmsd2") (r "1.65")))

(define-public crate-deep_causality_macros-0.4.8 (c (n "deep_causality_macros") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0p7xvwmzigzfiw6a5sm62dxgcvwkfqyxc0asyyxb3fn8n2jc10lj") (r "1.65")))

