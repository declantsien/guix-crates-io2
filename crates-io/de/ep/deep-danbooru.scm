(define-module (crates-io de ep deep-danbooru) #:use-module (crates-io))

(define-public crate-deep-danbooru-0.0.0 (c (n "deep-danbooru") (v "0.0.0") (d (list (d (n "image") (r "^0.24.5") (f (quote ("png" "jpeg" "jpeg_rayon"))) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ort") (r "^1.14.1") (k 0)))) (h "0g0mkz5ibbhpfmyk78z33qsaq73r6km8gvzgcrxxmv31gl588apw") (f (quote (("default"))))))

