(define-module (crates-io de ep deepmesa) #:use-module (crates-io))

(define-public crate-deepmesa-0.1.0 (c (n "deepmesa") (v "0.1.0") (d (list (d (n "deepmesa-lists") (r "^0.1.0-dev") (d #t) (k 0)))) (h "19qbxg666y1qgldq4gmnn48dalm6lpbg5x56qkxahrjm0kip2r4h") (y #t)))

(define-public crate-deepmesa-0.2.0 (c (n "deepmesa") (v "0.2.0") (d (list (d (n "deepmesa-lists") (r "^0.2.0") (d #t) (k 0)))) (h "1x0aq0c9ycxzk8hnmv879k99dszwp7bv4k3bklv33xfggdxmhyc6") (y #t)))

(define-public crate-deepmesa-0.2.1 (c (n "deepmesa") (v "0.2.1") (d (list (d (n "deepmesa-lists") (r "^0.2.1-dev") (d #t) (k 0)))) (h "04avhhj2q87vdhdnp1nir2g570kzkf8jxlfvsdqxqr7zgs203bi4") (y #t)))

(define-public crate-deepmesa-0.3.0 (c (n "deepmesa") (v "0.3.0") (d (list (d (n "deepmesa-lists") (r "^0.3.0") (d #t) (k 0)))) (h "1js6i7mwn4v1slrv2l60n5qir13bf49x6s6nkfpdfq6km6868imb") (y #t)))

(define-public crate-deepmesa-0.4.0 (c (n "deepmesa") (v "0.4.0") (d (list (d (n "deepmesa-lists") (r "^0.4.0") (d #t) (k 0)))) (h "0m569g84mvx73jifw91yfsj82v6nac919pf4p80487z6r4633c1b") (y #t)))

(define-public crate-deepmesa-0.4.1 (c (n "deepmesa") (v "0.4.1") (d (list (d (n "deepmesa-lists") (r "^0.4.1-dev") (d #t) (k 0)))) (h "10ppk04axyllpsmsfx0sfm48zhswpv4klxzkkgcrv26fbn5h60hm") (y #t)))

(define-public crate-deepmesa-0.5.0 (c (n "deepmesa") (v "0.5.0") (d (list (d (n "deepmesa-lists") (r "^0.5.0") (d #t) (k 0)))) (h "0wzva89mzjchnya9zb31naapvkhymp64xrvz6x20w60ja27y5wr0") (y #t)))

(define-public crate-deepmesa-0.6.0 (c (n "deepmesa") (v "0.6.0") (d (list (d (n "deepmesa-collections") (r "^0.6.0") (d #t) (k 0)) (d (n "deepmesa-lists") (r "^0.6.0") (d #t) (k 0)))) (h "1029vz97x5wslq2bk8hq7pyjnpcq71cb1jfk9a75gpmh468qp9r5") (y #t)))

(define-public crate-deepmesa-0.6.1 (c (n "deepmesa") (v "0.6.1") (d (list (d (n "deepmesa-collections") (r "^0.6.1-dev") (d #t) (k 0)) (d (n "deepmesa-lists") (r "^0.6.1-dev") (d #t) (k 0)))) (h "05xc10iczvhqdhh8mycss5qxmkjzrv5wii3yf6y2f6cmzf2afdgw") (y #t)))

(define-public crate-deepmesa-0.7.0 (c (n "deepmesa") (v "0.7.0") (d (list (d (n "deepmesa-collections") (r "^0.7.0") (d #t) (k 0)) (d (n "deepmesa-lists") (r "^0.7.0") (d #t) (k 0)))) (h "0v7j461sbxl6yc7jh8i1daa9zwld3yva88crisq6zxcrinmqjg5l") (y #t)))

(define-public crate-deepmesa-0.9.0 (c (n "deepmesa") (v "0.9.0") (d (list (d (n "deepmesa-collections") (r "^0.9.0") (d #t) (k 0)) (d (n "deepmesa-encoding") (r "^0.9.0") (d #t) (k 0)) (d (n "deepmesa-lists") (r "^0.9.0") (d #t) (k 0)))) (h "0f483g4ygasw5dz8k5shn586c9pxhh81vmm07pia021dkxgj0s2g") (y #t)))

(define-public crate-deepmesa-0.9.1 (c (n "deepmesa") (v "0.9.1") (d (list (d (n "deepmesa-collections") (r "^0.9.1") (d #t) (k 0)) (d (n "deepmesa-encoding") (r "^0.9.1") (d #t) (k 0)) (d (n "deepmesa-lists") (r "^0.9.1") (d #t) (k 0)))) (h "0gzc4jbbyz79lx8757l7k30n0nbkhxjmqrhi3ckq3i5i1h6ym86p")))

