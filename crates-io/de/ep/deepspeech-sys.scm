(define-module (crates-io de ep deepspeech-sys) #:use-module (crates-io))

(define-public crate-deepspeech-sys-0.0.1 (c (n "deepspeech-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "034l04kw7ix0qj02wlmqjb5f43bnqhd1x18mlxyk75sirgr5hi9w")))

(define-public crate-deepspeech-sys-0.0.2 (c (n "deepspeech-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "16i51ds7w1633mzp0wwh9sm34gbf89n40mzgm5y4fbm3pam9ag1d")))

(define-public crate-deepspeech-sys-0.0.3 (c (n "deepspeech-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "0ymfmx90x4h0gj6p4qh4iwcn13szz52nwf2v459nin59cw619qvq")))

(define-public crate-deepspeech-sys-0.0.4 (c (n "deepspeech-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)))) (h "0npqbqd9f24v4bppkml7nx8kbwmm8avglsrvfirnnakwnlak847n")))

(define-public crate-deepspeech-sys-0.1.0 (c (n "deepspeech-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)))) (h "136ghzpb0q28zyv1acp1lkzablh8x24qqs0z5dawid0ajv2xz2rm")))

(define-public crate-deepspeech-sys-0.2.0 (c (n "deepspeech-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)))) (h "10ba3w1xzjw4gha482fzl1xqhmrnzl3333hwngsb6qk64hbj904r")))

(define-public crate-deepspeech-sys-0.5.0 (c (n "deepspeech-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1p88raz946fpvhsw48rmqbdal152jyll7ri8krp6gqbg25bya6ln")))

(define-public crate-deepspeech-sys-0.6.0 (c (n "deepspeech-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("runtime"))) (k 1)) (d (n "proc-macro2") (r "^1.0") (k 1)))) (h "08ip2qjlprhilh9jlyxd92zyfwdcqyyj5da3ydnbwc3yz0g20xwl")))

(define-public crate-deepspeech-sys-0.6.1 (c (n "deepspeech-sys") (v "0.6.1") (h "0jmy21jq24hij57qckn68h0d223sv7ap0kax27l1a6crjqfnkv35")))

(define-public crate-deepspeech-sys-0.7.0 (c (n "deepspeech-sys") (v "0.7.0") (h "1ssdj5a3jsj5qka69h9465m3kkwbqqjpizrxxz3m6w0fp4z6lx4x")))

(define-public crate-deepspeech-sys-0.8.0 (c (n "deepspeech-sys") (v "0.8.0") (h "1ymi38agykvq35zsjj2slq5g4mc33cpi7fh9gq8xgkr78jdcilh4")))

(define-public crate-deepspeech-sys-0.9.0 (c (n "deepspeech-sys") (v "0.9.0") (h "1cbvw6yi3djvwirf3nwk8811rxf34cql3hg6v5x367wp724s61yd")))

(define-public crate-deepspeech-sys-0.9.1 (c (n "deepspeech-sys") (v "0.9.1") (h "1065a9y6cgxskhqr3cnlksmgz1xgjymzncybg5mnxkz40f0wwmcb")))

