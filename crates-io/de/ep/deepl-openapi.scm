(define-module (crates-io de ep deepl-openapi) #:use-module (crates-io))

(define-public crate-deepl-openapi-2.7.0 (c (n "deepl-openapi") (v "2.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1khqqcfzf1m399pfp45whcqxzm80yx3hdnqmwmcgp9pjkivg9ssc") (y #t)))

(define-public crate-deepl-openapi-1.0.0 (c (n "deepl-openapi") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jc5ff0kaial4qq1jn0q83l1m4ravq9ks3q5sxsvlyinyyy51y17") (y #t)))

(define-public crate-deepl-openapi-2.7.1 (c (n "deepl-openapi") (v "2.7.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "14wf4b8lcr8r6bsl59zd0flgkb84hflh2f0fny1fj4hj93sn7w15")))

