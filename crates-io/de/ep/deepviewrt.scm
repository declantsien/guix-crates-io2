(define-module (crates-io de ep deepviewrt) #:use-module (crates-io))

(define-public crate-deepviewrt-0.1.0 (c (n "deepviewrt") (v "0.1.0") (d (list (d (n "deepviewrt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1z36b5a43q6v8m6hk67wb9r4g4bvm84kw82asrpy3n3irid571al")))

(define-public crate-deepviewrt-0.0.0 (c (n "deepviewrt") (v "0.0.0") (d (list (d (n "deepviewrt-sys") (r "^0.0.0") (d #t) (k 0)))) (h "0msjli6zmdy8ki3cvwbbwcfvkh046767aw1jrmkc2anbhx9g9z8n")))

(define-public crate-deepviewrt-0.2.5 (c (n "deepviewrt") (v "0.2.5") (d (list (d (n "deepviewrt-sys") (r "^0.2.5") (d #t) (k 0)))) (h "06hnvmqdrhrqa2h0gxlac2rxkvzw799wih3s81869yifvbrc5qbn")))

(define-public crate-deepviewrt-0.2.6 (c (n "deepviewrt") (v "0.2.6") (d (list (d (n "deepviewrt-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1lmm7mdr5rv8brifs48b54f2lpyr260vbs91qf4k8rgyaj39ayy6")))

(define-public crate-deepviewrt-0.3.0 (c (n "deepviewrt") (v "0.3.0") (d (list (d (n "deepviewrt-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0j95ffh43zr2ynvg6ldw4r3zdqr07q3ls8dx1czicpsi2z9m9225")))

(define-public crate-deepviewrt-0.4.0 (c (n "deepviewrt") (v "0.4.0") (d (list (d (n "deepviewrt-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1fi0m6v98y0fijbyspg2wpypkqvyk4n3j9iwvsba4lmgzkyn929f")))

(define-public crate-deepviewrt-0.5.0 (c (n "deepviewrt") (v "0.5.0") (d (list (d (n "deepviewrt-sys") (r "^0.5.0") (d #t) (k 0)))) (h "04jsig0qpvw072i6y0rjnqpqzj8pyahymq9cb1giahz89hm8brp4")))

