(define-module (crates-io de ep deep_core) #:use-module (crates-io))

(define-public crate-deep_core-0.1.0 (c (n "deep_core") (v "0.1.0") (h "113gzpryr1yfjfwlqrj7c4nv87x262ardhpydnch7cpwq91vnb5h") (y #t)))

(define-public crate-deep_core-0.1.1 (c (n "deep_core") (v "0.1.1") (h "09q667mr59za2kammk11w9z1qyjrvgyh746h731kjvwblvgmsvly")))

