(define-module (crates-io de ep deepfrog) #:use-module (crates-io))

(define-public crate-deepfrog-0.2.0 (c (n "deepfrog") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "folia") (r "^0.0.5") (d #t) (k 0)) (d (n "rust-bert") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0bildym02k0a200al9xms8xsyncq61qjdsxhpbaadwkgbphaanag")))

(define-public crate-deepfrog-0.2.1 (c (n "deepfrog") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "folia") (r "^0.0.5") (d #t) (k 0)) (d (n "rust-bert") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1mff9lyvvzg9s8akaiihmz9rrkmiaa66pnh9xc0w4b4p11sp951r")))

