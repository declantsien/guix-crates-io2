(define-module (crates-io de ep deep_though) #:use-module (crates-io))

(define-public crate-deep_though-0.1.0 (c (n "deep_though") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx" "serde"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nwz5cd1x7p2q7gafd7kcaybixpvmjzhsa1w8y5d7mgky6kay8f8") (y #t)))

