(define-module (crates-io de ep deepmesa-encoding) #:use-module (crates-io))

(define-public crate-deepmesa-encoding-0.9.0 (c (n "deepmesa-encoding") (v "0.9.0") (d (list (d (n "deepmesa-collections") (r "^0.9.0") (d #t) (k 0)))) (h "12a3f4drvsvh23iizyb4zazs0fbq43x45lq12rpcg6giym76rjhd")))

(define-public crate-deepmesa-encoding-0.9.1 (c (n "deepmesa-encoding") (v "0.9.1") (d (list (d (n "deepmesa-collections") (r "^0.9.1") (d #t) (k 0)))) (h "0z9k5gq9zzs3g7py838q2mgk5sb5n0k2cx89nx12pbinfxv51d8w")))

