(define-module (crates-io de ep deep_causality) #:use-module (crates-io))

(define-public crate-deep_causality-0.2.4 (c (n "deep_causality") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1rfk2nsawsjdbvqylwqb1i960x0cpx0af15hym7z9xaaprrb231g") (r "1.65")))

(define-public crate-deep_causality-0.2.7 (c (n "deep_causality") (v "0.2.7") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0zkkhzi2igrszyhj0a72w9if8ay8550y0b421k35qcyx2f8hwrzk") (r "1.65")))

(define-public crate-deep_causality-0.2.8 (c (n "deep_causality") (v "0.2.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "14y2zmixfxdws9wylwvvysc7x68lqiphih4chiy0nbnch41ik4iw") (r "1.65")))

(define-public crate-deep_causality-0.3.0 (c (n "deep_causality") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "0cdlmjlwb78zpb8q1gcbljqbx03cmalx3k66v3gxryvmsqlipvz9") (r "1.65")))

(define-public crate-deep_causality-0.3.1 (c (n "deep_causality") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "000c7smg7fxhyh3lyy2x1aqv1llkvnb4kk27ybqf95d59bb89ph1") (r "1.65")))

(define-public crate-deep_causality-0.4.0 (c (n "deep_causality") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.4") (d #t) (k 0)))) (h "08pshqpadxb8w7acnpxgwxvppmqafbqydaiz1kgwhw05cg0vjnvg") (r "1.65")))

(define-public crate-deep_causality-0.5.0 (c (n "deep_causality") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "18fwnn0hjj475rs6hm1zzsplwxp81h1sv1inzp8w0dizcqnk1bhn") (r "1.65")))

(define-public crate-deep_causality-0.6.0 (c (n "deep_causality") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "0cxr21yh2r7dm6p9cwy2simik8xmzjsgvpkrwh6k4iv32vh11699") (r "1.65")))

(define-public crate-deep_causality-0.6.1 (c (n "deep_causality") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "13sir5gh2scxyhxdllhyca3gxysy3w22sjd67z5gvhfwkfn5w4j8") (r "1.65")))

(define-public crate-deep_causality-0.6.2 (c (n "deep_causality") (v "0.6.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "0fapdlk612505yxbs8xkg7pkliqr510fhqkry9zj60i5xl8dn37b") (r "1.65")))

(define-public crate-deep_causality-0.6.3 (c (n "deep_causality") (v "0.6.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "12wc4a52fp509pqnhyhjmfsnc15ax1anh3s8ni3lmghnkvf4l4rl") (r "1.65")))

(define-public crate-deep_causality-0.6.4 (c (n "deep_causality") (v "0.6.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "0bbp2nlff98v6wq0rg3434xib47357kx02djy97lmbkm4rz9b001") (r "1.65")))

(define-public crate-deep_causality-0.6.5 (c (n "deep_causality") (v "0.6.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "1kdxg4p2qsyh3i4zb7cmz9dk6dww7kq5fcgls44n9fz47hn16fly") (r "1.65")))

(define-public crate-deep_causality-0.7.0 (c (n "deep_causality") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dcl_data_structures") (r "^0.4") (d #t) (k 0)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "ultragraph") (r "^0.5") (d #t) (k 0)))) (h "0bb057iz2h998xbsgw7j3xr9p60xd4fqv8pfmpix7w0fwlmqnm9f") (r "1.65")))

