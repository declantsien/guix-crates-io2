(define-module (crates-io de ep deep-translator) #:use-module (crates-io))

(define-public crate-deep-translator-0.8.0 (c (n "deep-translator") (v "0.8.0") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1ky7609fnig27h15p5l0m1cbqis39lsdbj8qsb6mfw4lch240f2d")))

