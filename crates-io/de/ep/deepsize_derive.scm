(define-module (crates-io de ep deepsize_derive) #:use-module (crates-io))

(define-public crate-deepsize_derive-0.1.0 (c (n "deepsize_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0nsc3ah8bij0vgnn5acan1rgmm9w8i4n25f8v09i6bjk27vpfvqn")))

(define-public crate-deepsize_derive-0.1.1 (c (n "deepsize_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "14p92z0f5rzkvk85npw2k2r8zxd32l0gza0miw0wpg5yajvwiypf")))

(define-public crate-deepsize_derive-0.1.2 (c (n "deepsize_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15i7qybxhdp2y2h3xksyiqrwqki8xrvl60j1asjc3j1v3za020cr")))

