(define-module (crates-io de ep deepmesa-lists) #:use-module (crates-io))

(define-public crate-deepmesa-lists-0.1.0 (c (n "deepmesa-lists") (v "0.1.0") (h "0r89h664z5aa6yrn4ngj2q6gp1k7p955y1pd17vs63jhapdhxnkl") (y #t)))

(define-public crate-deepmesa-lists-0.2.0 (c (n "deepmesa-lists") (v "0.2.0") (h "1jb9g1bl1m2izp00grxyvy5f91q71ynca9vfhi8d3jvw5m6zns14") (y #t)))

(define-public crate-deepmesa-lists-0.2.1 (c (n "deepmesa-lists") (v "0.2.1") (h "0z2yrprh7lch0yhh8263ihx7mlhci307zsfysdzlsz8xaf7f7him") (y #t)))

(define-public crate-deepmesa-lists-0.3.0 (c (n "deepmesa-lists") (v "0.3.0") (h "0a0jz662ny4y0my1kgji9cdprhmfzihss1mx6xs0hh9lq353agwq") (y #t)))

(define-public crate-deepmesa-lists-0.4.0 (c (n "deepmesa-lists") (v "0.4.0") (h "13rl6zqdgikv1rpd4dblbzcmgvcpdvav50d84mgd840l88rvbw74") (y #t)))

(define-public crate-deepmesa-lists-0.4.1 (c (n "deepmesa-lists") (v "0.4.1") (h "1fgkgrm45drj1rp1lniki07bcirshgy2xigqxqadz9im4iy5y9dc") (y #t)))

(define-public crate-deepmesa-lists-0.5.0 (c (n "deepmesa-lists") (v "0.5.0") (h "13z4dznl8cp645h5y4fcd1dsxmpvpax5khg750xqz0qw1hpjs5ic")))

(define-public crate-deepmesa-lists-0.6.0 (c (n "deepmesa-lists") (v "0.6.0") (h "14m0hwmh9pvmh7ivbhagj87kgj969bn73nqwba7v37p035rak22x")))

(define-public crate-deepmesa-lists-0.6.1 (c (n "deepmesa-lists") (v "0.6.1") (h "020r97dihvm4ibadvbj06yc8b0vbnxpdpvmqxkjfg6am9bwlxxln")))

(define-public crate-deepmesa-lists-0.7.0 (c (n "deepmesa-lists") (v "0.7.0") (h "0dvwb7c4syv6gc3zk45r1xrasxhcpgc29wwx4bdckchkmgipjmr8")))

(define-public crate-deepmesa-lists-0.9.0 (c (n "deepmesa-lists") (v "0.9.0") (h "1myzdccbd3d9b6hzzklgmzsjwpd2zb0dkkhsrsxshxmgqc4k283h")))

(define-public crate-deepmesa-lists-0.9.1 (c (n "deepmesa-lists") (v "0.9.1") (h "1m9s215l8a2zq31wcr1maqydw5q728kjx6ynhmm1d63rs0b22qcw")))

