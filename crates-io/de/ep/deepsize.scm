(define-module (crates-io de ep deepsize) #:use-module (crates-io))

(define-public crate-deepsize-0.1.0 (c (n "deepsize") (v "0.1.0") (d (list (d (n "deepsize_derive") (r "^0.1") (d #t) (k 0)))) (h "0pcbqcn77h3nhsrnqy86hkmwchmw2q1i43y1k5qna72ql37ny65r")))

(define-public crate-deepsize-0.1.1 (c (n "deepsize") (v "0.1.1") (d (list (d (n "deepsize_derive") (r "^0.1") (d #t) (k 0)))) (h "1h4nnqigka6ra90qd1hkfmn3kmzcmwhiiv2fj5jcx73ng5374li6")))

(define-public crate-deepsize-0.1.2 (c (n "deepsize") (v "0.1.2") (d (list (d (n "deepsize_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "deepsize_derive") (r "^0.1.1") (d #t) (k 2)) (d (n "slotmap") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0iqqqhrh572ddivj35y8n4nhhsdxv25vbn9xbrfzpdhzbfw5djrg") (f (quote (("std") ("slotmap_support" "slotmap") ("derive" "deepsize_derive") ("default" "std" "derive"))))))

(define-public crate-deepsize-0.2.0 (c (n "deepsize") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "deepsize_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "deepsize_derive") (r "^0.1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0v5rn98i6j4jfpcm93mg8by3ddwhanvjiyd3pszzfsvgqdz9inqw") (f (quote (("std") ("derive" "deepsize_derive") ("default" "std" "derive"))))))

