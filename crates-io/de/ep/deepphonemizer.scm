(define-module (crates-io de ep deepphonemizer) #:use-module (crates-io))

(define-public crate-deepphonemizer-1.0.0 (c (n "deepphonemizer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "tch") (r "^0.14.0") (f (quote ("download-libtorch"))) (d #t) (k 0)))) (h "14py11sv362302k9v3zfik9pdfbdkpacdimi8ma5k7z31dzjc73l")))

