(define-module (crates-io de ep deepspeech) #:use-module (crates-io))

(define-public crate-deepspeech-0.0.1 (c (n "deepspeech") (v "0.0.1") (d (list (d (n "deepspeech-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "088c5b9qbni15zm9kmh2vsz9w9vyanf07nl68fnxag34yv5g1pgi")))

(define-public crate-deepspeech-0.0.2 (c (n "deepspeech") (v "0.0.2") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1np4chx0h5bmhm1m34ml6qixcvg4yn1bvzvlkrml7lf3y6z9c1hn")))

(define-public crate-deepspeech-0.0.3 (c (n "deepspeech") (v "0.0.3") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d8n71fijpyihf6lk7h1d30hlbn9f2kl6g8pwimnk7sf16xq75cz")))

(define-public crate-deepspeech-0.0.4 (c (n "deepspeech") (v "0.0.4") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jqb14n9dhglick85r0n2s8ifh8j3mssw2h0r94ks856vjdg8h9y")))

(define-public crate-deepspeech-0.1.0 (c (n "deepspeech") (v "0.1.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g3ak09f257zihhw00q0y8qdsmpkd3vgnhmv7nzgjppwrgb2mykr")))

(define-public crate-deepspeech-0.2.0 (c (n "deepspeech") (v "0.2.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cfqn55h0ga9bwn0751ya75psfp2cirhy9cy875pf2ybm5xxpqpx")))

(define-public crate-deepspeech-0.4.0 (c (n "deepspeech") (v "0.4.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a1llcrj34rlib32bq06dz18cxg5gsr4a04b6g28xcszdvnrprnh")))

(define-public crate-deepspeech-0.6.0 (c (n "deepspeech") (v "0.6.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zb8vbakyfxvrs77nh5qha4153pvzk9dqiziksr36gws64crhn7p")))

(define-public crate-deepspeech-0.6.1 (c (n "deepspeech") (v "0.6.1") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mhfw68bs9qsnwy0d6ajl0qcvpi9rd4f21gv0mzj73cjh61gx3fs")))

(define-public crate-deepspeech-0.7.0 (c (n "deepspeech") (v "0.7.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "171sid8id3qhsgczv2n81iihvjxpdfi0idkx5nzc7gg1lzqpxq36")))

(define-public crate-deepspeech-0.8.0 (c (n "deepspeech") (v "0.8.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0ziy8q2s3299s9vn5xalc3avldyycs393r1icab6q233c5455lf7") (f (quote (("static_bindings" "deepspeech-sys") ("dynamic" "libloading") ("default" "static_bindings"))))))

(define-public crate-deepspeech-0.9.0 (c (n "deepspeech") (v "0.9.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (o #t) (d #t) (k 0)))) (h "06n9j93a0m09w5v6i0ymyxi7dwqivll1i5z0cna889a8yza5ggl7") (f (quote (("static_bindings" "deepspeech-sys") ("dynamic" "libloading") ("default" "static_bindings"))))))

(define-public crate-deepspeech-0.9.1 (c (n "deepspeech") (v "0.9.1") (d (list (d (n "audrey") (r "^0.3") (d #t) (k 2)) (d (n "dasp_interpolate") (r "^0.11") (f (quote ("linear"))) (d #t) (k 2)) (d (n "dasp_signal") (r "^0.11") (d #t) (k 2)) (d (n "deepspeech-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0i0ym5qwy5f41jfipig1a9vajkpzcyflac4n3fwyc1dld6inc2yz") (f (quote (("static_bindings" "deepspeech-sys") ("dynamic" "libloading") ("default" "static_bindings"))))))

