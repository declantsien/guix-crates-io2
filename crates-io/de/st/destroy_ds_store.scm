(define-module (crates-io de st destroy_ds_store) #:use-module (crates-io))

(define-public crate-destroy_ds_store-1.0.0 (c (n "destroy_ds_store") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "10iwspm0fn7b3wn90gynh7nk7kilz89ai559ij30gnznx31b1z3b")))

(define-public crate-destroy_ds_store-1.0.1 (c (n "destroy_ds_store") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1kszxn9fddvlgjf8fzx3glx1hiqmz801l2x6bcpf5xrp17y2y9j5")))

