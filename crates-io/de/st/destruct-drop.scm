(define-module (crates-io de st destruct-drop) #:use-module (crates-io))

(define-public crate-destruct-drop-0.1.0 (c (n "destruct-drop") (v "0.1.0") (d (list (d (n "destruct-drop-derive") (r "^0.1") (d #t) (k 0)))) (h "1jfjbiq7sk3m85l6bdc3m7hp6d4bzh94f6iw59fmfpqiljhxv0my")))

(define-public crate-destruct-drop-0.2.0 (c (n "destruct-drop") (v "0.2.0") (d (list (d (n "destruct-drop-derive") (r "^0.2") (d #t) (k 0)))) (h "1q7006mh23p6rw3in3g0pd7h4blqa1ln79yvqzk3fp61jqx81vqf")))

