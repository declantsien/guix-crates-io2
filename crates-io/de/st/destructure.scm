(define-module (crates-io de st destructure) #:use-module (crates-io))

(define-public crate-destructure-0.1.0 (c (n "destructure") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0m3s407qri77ml7hgm41yjipi54f1ybf48v1aqir1s0v7wz3kls8")))

(define-public crate-destructure-0.1.1 (c (n "destructure") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w567j0bab06arjac9dwkc1cwsk8qig31n5vc69g1vdabm2x1010")))

(define-public crate-destructure-0.1.2 (c (n "destructure") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "07v8k1lg8p80x674cvf9d23ixxph97ik3l1qx7ffnab69yygag5a")))

(define-public crate-destructure-0.2.0 (c (n "destructure") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g6wxpyg1ia4qqkdyc4s0zkak3fqfzq9kg8rmfqf01c9wygy3fhr")))

(define-public crate-destructure-0.3.0 (c (n "destructure") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "117dzsn3a8b83r7wgxkvjfkvf176vccl0awpl1p5nsirnmjvas1p")))

(define-public crate-destructure-0.4.3 (c (n "destructure") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y0bb1yqyqk581qpy11vwbprc44iqvrb73ccvx6pd7js8hs2drif")))

(define-public crate-destructure-0.4.4 (c (n "destructure") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1znsqla4qssszvw3mn50p40vsqx9cvy3paxhbccqavba7gfljdmh")))

(define-public crate-destructure-0.4.5 (c (n "destructure") (v "0.4.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "04rxx9am0b55q0j46wy83ijbgghisqy2md1a6wd388gnnnpd8p5h")))

(define-public crate-destructure-0.5.5 (c (n "destructure") (v "0.5.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "177va5r141gikm8fzh8pay7y8r80wbbcbav6hn192r9mc3qjwilw")))

(define-public crate-destructure-0.5.6 (c (n "destructure") (v "0.5.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "109qh17byd2v85iqc256c6566v8h9dk9q0zpwh67q25aq2jjzmyg")))

