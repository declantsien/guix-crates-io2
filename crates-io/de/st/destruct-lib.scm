(define-module (crates-io de st destruct-lib) #:use-module (crates-io))

(define-public crate-destruct-lib-0.1.0 (c (n "destruct-lib") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 2)))) (h "1qf8rhlp1wn50v3zhvzan3biir8z8sm6b2b633i5xc3gpwvcdf14")))

(define-public crate-destruct-lib-0.1.1 (c (n "destruct-lib") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 2)))) (h "0wvdpldypvk9f4p93cqf68dmjy34qnb57sqi6m73a7higkpsy25j")))

(define-public crate-destruct-lib-0.1.2 (c (n "destruct-lib") (v "0.1.2") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 2)))) (h "143cc2vqmfzc0ksms949hvvmff70slfzz2ajzs30454hvz5hnv95")))

