(define-module (crates-io de st destructure_traitobject) #:use-module (crates-io))

(define-public crate-destructure_traitobject-0.2.0 (c (n "destructure_traitobject") (v "0.2.0") (h "1ir5x9f5zksr1fs788jy5g2hyyc2hnnx7kwi87wd451wd5apb1rw")))

(define-public crate-destructure_traitobject-0.3.0 (c (n "destructure_traitobject") (v "0.3.0") (h "0kbdnzbbi5wcvyccmznzisi7hjyl68pqp9jnc6hpqqfkw5rd66a7")))

