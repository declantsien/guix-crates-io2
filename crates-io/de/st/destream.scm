(define-module (crates-io de st destream) #:use-module (crates-io))

(define-public crate-destream-0.1.0 (c (n "destream") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1s5zdkn9hc4fhqhy5gd1v8i3s9s9982q8nirnq2anwfzanm65s45")))

(define-public crate-destream-0.1.1 (c (n "destream") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ml3j8wkxgygs1r15ql7nsfhdl913lij585p4h3psb6dqbrxsrdv")))

(define-public crate-destream-0.1.2 (c (n "destream") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ypjq0mj60yfy783hwlkaxgqr9v94f2ldyy0w7x01kp08qwmb6k7")))

(define-public crate-destream-0.1.3 (c (n "destream") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1fanqqws3fm0qw7mbpkj4ws7qkwk4hl2n9m9sbc3gf027yxxa8z3")))

(define-public crate-destream-0.1.4 (c (n "destream") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "05253b1hh4hdz1bpb4vphqq8jpiq9ri0268syynaz0x1dlis7c3l")))

(define-public crate-destream-0.2.0 (c (n "destream") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "14fnh06k40hwi4p33mnwhhsa4jvm8xbvangn41z9n6d8higz63s6")))

(define-public crate-destream-0.3.0 (c (n "destream") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "14vh5bis6mx00qd8ccby2i7y4a960x1fhpifgyd92h28a7wq3fcc")))

(define-public crate-destream-0.3.1 (c (n "destream") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0vb5m5fmwcc33aj43ixmx05zs22kazrj90racw1nai06i4ksgcnl")))

(define-public crate-destream-0.3.2 (c (n "destream") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0vc3w0lll1jvhqgi5s3c1b0sk35rbww1bn45jxqga44aa4a91d0n")))

(define-public crate-destream-0.4.0 (c (n "destream") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "03wibxddmxp6bxj4sm92iwsqjnjxpzz94wryfh3s5hrfqbyzhi5s")))

(define-public crate-destream-0.4.1 (c (n "destream") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0qqhbsrv5w4mybvcl8yg4gh7llpygm31ckyv38xq4h03zxpld0p7")))

(define-public crate-destream-0.5.0 (c (n "destream") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0rr0bpzl0yfsiyzvhmdpn401v5g6viy62sanhsy4g6d25llblqkk")))

(define-public crate-destream-0.5.1 (c (n "destream") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1726x95h61iyzidyyz79awdk944fpcd9c9wyrnv831ghk4n33ijy")))

(define-public crate-destream-0.5.2 (c (n "destream") (v "0.5.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nfaadwxv1avbjxmy75bb13ysxj40qnrs35m0pbb83i88h5lhmy1")))

(define-public crate-destream-0.6.0 (c (n "destream") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0rzg3cxpw1g9gplv092ksm06pgyag4hpvkqljjagvaniycdbwrq0") (y #t)))

(define-public crate-destream-0.6.1 (c (n "destream") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "15cjvxw8ny331c5fb754806y9p4nzajg1l97df6g9gbnm0ijn1rr") (y #t)))

(define-public crate-destream-0.6.2 (c (n "destream") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "1v1k3k7c5zmccn4h8vjrpw97mda85fwayajg53y4drvcjm83w22l")))

(define-public crate-destream-0.6.3 (c (n "destream") (v "0.6.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "1gd1sx3rf7fkb80dnpf7yrmhprjbm6fv7mhqbiis0hbr8yi1fagp")))

(define-public crate-destream-0.7.0 (c (n "destream") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "1f36zba9q1l4j47mwng4fwrpmf2jb40nq2d42lmw7ga4pq247ckp")))

(define-public crate-destream-0.7.1 (c (n "destream") (v "0.7.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "0la5r934nnlaj8blhzxw2dwgx5nfpg89var4nc035s20aivai5l4")))

(define-public crate-destream-0.7.2 (c (n "destream") (v "0.7.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "1q2z9dbnydbr83m8k7yr7bap8n23yl39428r7l2r4l6cwp2k6n3v")))

(define-public crate-destream-0.7.3 (c (n "destream") (v "0.7.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (d #t) (k 0)))) (h "13gs86kbj0cg3avbq239ridw20b75bb1shgiwbnpkblqbysh14cw")))

