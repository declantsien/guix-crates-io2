(define-module (crates-io de st destruct-drop-derive) #:use-module (crates-io))

(define-public crate-destruct-drop-derive-0.1.0 (c (n "destruct-drop-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gxrf15b4p0jxzg0fzw15qykw2cvbq7jcjylcfs2cq2wkq7y6hqf")))

(define-public crate-destruct-drop-derive-0.1.1 (c (n "destruct-drop-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18pryxv771a7vbx4ycxb64i34id86d8yf0iqqci3hd1i9n43sbc0")))

(define-public crate-destruct-drop-derive-0.2.0 (c (n "destruct-drop-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "105i4v9f2z0k5afqh633xsxzb26nq9g36ir8zdkniv7yryjpyfhk")))

