(define-module (crates-io de st destruct) #:use-module (crates-io))

(define-public crate-destruct-0.1.0 (c (n "destruct") (v "0.1.0") (d (list (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "destruct-lib") (r "^0.1") (d #t) (k 0)))) (h "1j9s2f3ndm51py0wn1wb6w9rcl08y60p7i7iiby4nwaligj49r7f")))

(define-public crate-destruct-0.1.1 (c (n "destruct") (v "0.1.1") (d (list (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "destruct-lib") (r "^0.1") (d #t) (k 0)))) (h "1jjc8dc0l5rjbb0314lq7fqfxqwkl5jwa084mfj45clw4xmhcb3j")))

(define-public crate-destruct-0.1.2 (c (n "destruct") (v "0.1.2") (d (list (d (n "destruct-derive") (r "^0.1") (d #t) (k 0)) (d (n "destruct-lib") (r "^0.1") (d #t) (k 0)))) (h "1cka7j0hd5jl827chbvrxx7adybs9fmscnxqa5rfi29dk14jckl3")))

