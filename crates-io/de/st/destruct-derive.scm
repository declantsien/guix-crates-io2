(define-module (crates-io de st destruct-derive) #:use-module (crates-io))

(define-public crate-destruct-derive-0.1.0 (c (n "destruct-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1csvnwralqr4gy8fjq63gkkds36xxzcqdvq70355pklgd7ila2vj")))

(define-public crate-destruct-derive-0.1.1 (c (n "destruct-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w5np78lx14ra1781ncf1h3a7gvmgqmsfr90jp2rsx65f7ys0yd9")))

(define-public crate-destruct-derive-0.1.2 (c (n "destruct-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06sywr2x4b6gbihnjricvkvwhz3890awcs2r6r9qnz5f9snzkwn1")))

