(define-module (crates-io de sk desk-terminal) #:use-module (crates-io))

(define-public crate-desk-terminal-0.0.0 (c (n "desk-terminal") (v "0.0.0") (d (list (d (n "bevy") (r "^0.8") (d #t) (k 0)) (d (n "command") (r "^0.0.0") (d #t) (k 0) (p "desk-command")) (d (n "desk-window") (r "^0.0.0") (d #t) (k 0) (p "desk-window")) (d (n "deskc-ids") (r "^0.0.0") (d #t) (k 0) (p "deskc-ids")) (d (n "deskc-types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")) (d (n "dworkspace") (r "^0.0.0") (d #t) (k 0) (p "dworkspace")) (d (n "dworkspace-codebase") (r "^0.0.0") (d #t) (k 0) (p "dworkspace-codebase")) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "system-ordering") (r "^0.0.0") (d #t) (k 0) (p "desk-system-ordering")))) (h "05191bj7a1q4993zqmncqsg9x38aapfw2hsf6s25055mzr4w80c1")))

