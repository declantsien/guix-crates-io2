(define-module (crates-io de sk deskc-amirgen) #:use-module (crates-io))

(define-public crate-deskc-amirgen-0.0.0 (c (n "deskc-amirgen") (v "0.0.0") (d (list (d (n "amir") (r "^0.0.0") (d #t) (k 0) (p "deskc-amir")) (d (n "thir") (r "^0.0.0") (d #t) (k 0) (p "deskc-thir")) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")))) (h "1hrw7p60cqxdxkqdi7ad9ccsdw2vi6bv9lq15z1kgiv66xndi6fs")))

