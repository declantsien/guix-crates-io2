(define-module (crates-io de sk desk-x-theme) #:use-module (crates-io))

(define-public crate-desk-x-theme-0.0.0 (c (n "desk-x-theme") (v "0.0.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qs15qfnzcl6cyl8as0q3b24ab8amb898ydcha5y4r45gm6grq64")))

