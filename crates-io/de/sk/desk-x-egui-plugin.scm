(define-module (crates-io de sk desk-x-egui-plugin) #:use-module (crates-io))

(define-public crate-desk-x-egui-plugin-0.0.0 (c (n "desk-x-egui-plugin") (v "0.0.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "theme") (r "^0.0.0") (d #t) (k 0) (p "desk-x-theme")))) (h "1c4l52byi3x5cszahy7qnadjhwhqgl6ip2x1swy5k3gb4id6z9ah")))

