(define-module (crates-io de sk desk-x) #:use-module (crates-io))

(define-public crate-desk-x-0.0.0 (c (n "desk-x") (v "0.0.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "egui-plugin") (r "^0.0.0") (d #t) (k 0) (p "desk-x-egui-plugin")) (d (n "web-sys") (r "^0.3.57") (d #t) (k 0)))) (h "0vdyvllj05vjxwzzx645rzz624mlpkn1fwx64k03mh547zcvaz2m") (f (quote (("inspector" "bevy-inspector-egui"))))))

