(define-module (crates-io de sk desktopentries) #:use-module (crates-io))

(define-public crate-desktopentries-0.1.0 (c (n "desktopentries") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0kwvya0s6v3v3r9w2m0nlcansx3qdqd7j2ylb5j87klay3pwsr4p")))

(define-public crate-desktopentries-0.1.1 (c (n "desktopentries") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0f58ban8rr5470yz1cfz3n60i80cpwfnclayh0c5nqizlxr9k8n3")))

(define-public crate-desktopentries-0.1.2 (c (n "desktopentries") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "1p9i33wih35n34i51jj17s6pj5s3igfcnkgryfi724ikrhs95pj5")))

