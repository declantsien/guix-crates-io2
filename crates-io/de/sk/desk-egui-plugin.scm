(define-module (crates-io de sk desk-egui-plugin) #:use-module (crates-io))

(define-public crate-desk-egui-plugin-0.0.0 (c (n "desk-egui-plugin") (v "0.0.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "theme") (r "^0.0.0") (d #t) (k 0) (p "desk-theme")))) (h "05k4z6h3zhrcbhb9dzsqrpp8svhcyy67hzzg7nnvnid57ghg8v9m")))

