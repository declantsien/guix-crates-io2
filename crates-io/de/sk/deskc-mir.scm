(define-module (crates-io de sk deskc-mir) #:use-module (crates-io))

(define-public crate-deskc-mir-0.0.0 (c (n "deskc-mir") (v "0.0.0") (d (list (d (n "amir") (r "^0.0.0") (d #t) (k 0) (p "deskc-amir")) (d (n "link") (r "^0.0.0") (d #t) (k 0) (p "deskc-link")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")))) (h "03f5s5znhkxybiqf13p3gb1svgyixx78jswqxd2zgpqy196hx4xj") (f (quote (("withserde" "serde" "amir/withserde"))))))

