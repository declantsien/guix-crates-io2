(define-module (crates-io de sk deskc-amir) #:use-module (crates-io))

(define-public crate-deskc-amir-0.0.0 (c (n "deskc-amir") (v "0.0.0") (d (list (d (n "link") (r "^0.0.0") (d #t) (k 0) (p "deskc-link")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")))) (h "0bpmhvnl4riipm1cqm0cp0ylrq4kik4hrhlgmfynx5glw5d51d44") (f (quote (("withserde" "serde" "link/withserde"))))))

