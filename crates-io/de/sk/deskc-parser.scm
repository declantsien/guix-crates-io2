(define-module (crates-io de sk deskc-parser) #:use-module (crates-io))

(define-public crate-deskc-parser-0.0.0 (c (n "deskc-parser") (v "0.0.0") (d (list (d (n "ast") (r "^0.0.0") (d #t) (k 0) (p "deskc-ast")) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "lexer") (r "^0.0.0") (d #t) (k 2) (p "deskc-lexer")) (d (n "textual-diagnostics") (r "^0.0.0") (d #t) (k 0) (p "deskc-textual-diagnostics")) (d (n "tokens") (r "^0.0.0") (d #t) (k 0) (p "deskc-tokens")) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "10zzqwbkgsrbxzhbsig96nkcf89v6269gw9pj3yrs5va1ccld02v")))

