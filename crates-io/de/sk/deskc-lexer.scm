(define-module (crates-io de sk deskc-lexer) #:use-module (crates-io))

(define-public crate-deskc-lexer-0.0.0 (c (n "deskc-lexer") (v "0.0.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "textual-diagnostics") (r "^0.0.0") (d #t) (k 0) (p "deskc-textual-diagnostics")) (d (n "tokens") (r "^0.0.0") (d #t) (k 0) (p "deskc-tokens")) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1msjmajnnd39ndv7a0g8jbwirqbsk5p7bh49ck69x362f6aiv65d")))

