(define-module (crates-io de sk desk-locker) #:use-module (crates-io))

(define-public crate-desk-locker-1.0.0 (c (n "desk-locker") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "desk-logind") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("screensaver"))) (d #t) (k 0)))) (h "1p2bf265ddnvmzxa4vcjl87hp71yz2dsm5d8q677zv0lx3ajdrs0")))

(define-public crate-desk-locker-1.0.1 (c (n "desk-locker") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "desk-logind") (r "^1.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("screensaver"))) (d #t) (k 0)))) (h "129aq2lcl2xn22ffmbimswyzajscn12lf6cxh0mm2sw7jy0fgxx8")))

