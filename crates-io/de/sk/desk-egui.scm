(define-module (crates-io de sk desk-egui) #:use-module (crates-io))

(define-public crate-desk-egui-0.0.0 (c (n "desk-egui") (v "0.0.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "theme") (r "^0.0.0") (d #t) (k 0) (p "desk-theme")))) (h "0av0rr7s48wlf99afc54kxqyxzy692j3dsy76g425q1a917y05xb")))

