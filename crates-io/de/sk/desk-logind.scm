(define-module (crates-io de sk desk-logind) #:use-module (crates-io))

(define-public crate-desk-logind-1.0.0 (c (n "desk-logind") (v "1.0.0") (d (list (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05a6hydhbq56dm1d0vmij0x4x9krv6zc481x4dq5svvs2d54b14m")))

(define-public crate-desk-logind-1.1.0 (c (n "desk-logind") (v "1.1.0") (d (list (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rzji9igy8kdc2iswidimibm1x1mlq0886q2p71mnvr8wxbc2aqy")))

(define-public crate-desk-logind-1.1.1 (c (n "desk-logind") (v "1.1.1") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1894zkdg7jjl4h49a07d0gwgdp5286xgg2mxhzbzspkq12hpvwhg")))

