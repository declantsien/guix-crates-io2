(define-module (crates-io de sk desktop) #:use-module (crates-io))

(define-public crate-desktop-0.1.0 (c (n "desktop") (v "0.1.0") (h "0af0ij5nh06fnyqrx95ml156hs3h1hw44ikdjcvhr2a93l1q8nb6")))

(define-public crate-desktop-0.1.1 (c (n "desktop") (v "0.1.1") (h "1pl7rbmwxcqapp0m5k14hihkp9fm6pgxc3r602bv3kk9is0m2kas")))

(define-public crate-desktop-0.1.2 (c (n "desktop") (v "0.1.2") (h "1r8mly1nsk1w3053kl7h638vz1pwqmq76pwdrx2gvviixjvxqik3")))

(define-public crate-desktop-1.0.0 (c (n "desktop") (v "1.0.0") (h "1zz023pldkppgkq5x4y5s8g8j6rdpxzw9bkmb0bvfczc41f00sc1")))

(define-public crate-desktop-1.0.1 (c (n "desktop") (v "1.0.1") (h "1s3w381cjxrgfsl94zvmd41s4pcr2ilak0skv5ih4zfj2h4095qr")))

(define-public crate-desktop-1.0.2 (c (n "desktop") (v "1.0.2") (h "1lvcc04vx8dbzhb4n9vsbf3c267rb5qcl9ihc6majjm2vq5jvmp3")))

(define-public crate-desktop-1.0.3 (c (n "desktop") (v "1.0.3") (h "1d870xjmczzhi6yq7qj58bs6idrba4vvzr68l0gvwlf7m5kri4d0")))

(define-public crate-desktop-1.0.4 (c (n "desktop") (v "1.0.4") (h "09fnbldx35diwzwcdxqb7l8yikr0a776y7vh9zvj5ia65aypgmb0")))

(define-public crate-desktop-1.0.5 (c (n "desktop") (v "1.0.5") (h "17km23qlcawbbbihyky8254a35gimjs0wnswzbi1x8z1ascym5wj")))

