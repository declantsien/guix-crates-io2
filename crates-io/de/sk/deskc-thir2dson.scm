(define-module (crates-io de sk deskc-thir2dson) #:use-module (crates-io))

(define-public crate-deskc-thir2dson-0.0.0 (c (n "deskc-thir2dson") (v "0.0.0") (d (list (d (n "dson") (r "^0.0.0") (d #t) (k 0) (p "deskc-dson")) (d (n "thir") (r "^0.0.0") (d #t) (k 0) (p "deskc-thir")) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")))) (h "144b1hwwd4pldf60yqzyy2w1g9dgxzms5wghgp3n6m1lnaakxs9d")))

