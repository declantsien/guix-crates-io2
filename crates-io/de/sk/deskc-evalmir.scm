(define-module (crates-io de sk deskc-evalmir) #:use-module (crates-io))

(define-public crate-deskc-evalmir-0.0.0 (c (n "deskc-evalmir") (v "0.0.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 2)) (d (n "hirgen") (r "^0.0.0") (d #t) (k 2) (p "deskc-hirgen")) (d (n "lexer") (r "^0.0.0") (d #t) (k 2) (p "deskc-lexer")) (d (n "mir") (r "^0.0.0") (d #t) (k 0) (p "deskc-mir")) (d (n "parser") (r "^0.0.0") (d #t) (k 2) (p "deskc-parser")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typeinfer") (r "^0.0.0") (d #t) (k 2) (p "deskc-typeinfer")) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")))) (h "0iv1ga98dncdshxhifv35lfzqzz0w9w5qm5s78ln925kpw1070im") (f (quote (("withserde" "serde" "mir/withserde" "types/withserde"))))))

