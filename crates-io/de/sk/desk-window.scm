(define-module (crates-io de sk desk-window) #:use-module (crates-io))

(define-public crate-desk-window-0.0.0 (c (n "desk-window") (v "0.0.0") (d (list (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "deskc-ids") (r "^0.0.0") (d #t) (k 0) (p "deskc-ids")) (d (n "dworkspace") (r "^0.0.0") (d #t) (k 0) (p "dworkspace")) (d (n "dworkspace-components") (r "^0.0.0") (d #t) (k 0) (p "dworkspace-components")) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "17dkizk4j223dp4gcjb9yphmsninkz3ipxhvkq0r9wakcqksphqx")))

