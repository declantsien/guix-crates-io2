(define-module (crates-io de sk deskc-hirgen) #:use-module (crates-io))

(define-public crate-deskc-hirgen-0.0.0 (c (n "deskc-hirgen") (v "0.0.0") (d (list (d (n "ast") (r "^0.0.0") (d #t) (k 0) (p "deskc-ast")) (d (n "chumsky") (r "^0.8.0") (d #t) (k 2)) (d (n "file") (r "^0.0.0") (d #t) (k 0) (p "deskc-file")) (d (n "hir") (r "^0.0.0") (d #t) (k 0) (p "deskc-hir")) (d (n "lexer") (r "^0.0.0") (d #t) (k 2) (p "deskc-lexer")) (d (n "parser") (r "^0.0.0") (d #t) (k 2) (p "deskc-parser")) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1yl4z9d83hy9930nn0lxilynrajivkd3cdl6xgd7lp4zjyfqgsrx")))

