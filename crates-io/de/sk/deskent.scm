(define-module (crates-io de sk deskent) #:use-module (crates-io))

(define-public crate-deskent-0.1.0 (c (n "deskent") (v "0.1.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "rust-ini") (r "^0.12.0") (d #t) (k 0)))) (h "16714f3isx957afrdcy4sq8xlyfv5ll8bqanj7v7kwss5hl3di3v")))

(define-public crate-deskent-0.2.0 (c (n "deskent") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)))) (h "0cqdg7b0hjc01lawzqx2r0ri5w441gcbqlwa3b48l0i7g1j219gd")))

(define-public crate-deskent-0.3.0 (c (n "deskent") (v "0.3.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)))) (h "0r2kqxvy5wgdgzzzk6c9r6dmy35mycqkf6iz86js10im7si3risf")))

