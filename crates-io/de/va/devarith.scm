(define-module (crates-io de va devarith) #:use-module (crates-io))

(define-public crate-devarith-0.1.0 (c (n "devarith") (v "0.1.0") (h "1sc1af756cn70ijkggbwzdvvfp82l4km7mm2klim0fpsha6xgj87") (y #t)))

(define-public crate-devarith-0.1.1 (c (n "devarith") (v "0.1.1") (h "0ihn6chcaask81249vj8iyblxqzn4japf4sms21cq47qlkg5df1z") (y #t)))

(define-public crate-devarith-0.1.2 (c (n "devarith") (v "0.1.2") (h "1b2c9rc5v9mpsr113x4qi45lsnacsljdyn09yk46jmfkv0mcpc7k") (y #t)))

