(define-module (crates-io de le delegate-display) #:use-module (crates-io))

(define-public crate-delegate-display-1.0.0 (c (n "delegate-display") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "0m4y9f9g02cg9pb7bvwnnkhnvblnghm9xik72pr8lnb1p2sv3v7i") (y #t) (r "1.56.0")))

(define-public crate-delegate-display-1.0.1 (c (n "delegate-display") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "0gh5rc4i0c81h69kzis26zffjylyb1inqczjpw95in2f0qcx68d2") (r "1.56.0")))

(define-public crate-delegate-display-1.0.2 (c (n "delegate-display") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "0n07amy6n49yamq7mw1wpg5zzrnkxz69545hax7c4ix6li2ac2g1") (r "1.56.0")))

(define-public crate-delegate-display-1.0.3 (c (n "delegate-display") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "0q1n8pakrhz92dia9fz3lfpz8138vikg5cywgndvqjg770chrk34") (r "1.56.0")))

(define-public crate-delegate-display-1.0.4 (c (n "delegate-display") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "15lgyfj1gn6za41pxgwjafa6w0679jcfjdm5h3mzz35ic1kka3bd") (r "1.56.0")))

(define-public crate-delegate-display-1.0.5 (c (n "delegate-display") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "1v32fpi59ddp0fx6xs3wi31m4bf17vmhkzvayd4q4p3ny81abf02") (r "1.56.0")))

(define-public crate-delegate-display-1.0.6 (c (n "delegate-display") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0d9nk88k5vpc2vi7626dycg2fzl939b07niaws9rnn50vjn22qqw") (r "1.56.0")))

(define-public crate-delegate-display-1.0.7 (c (n "delegate-display") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0fir3k9d8ilafak6h9bj49ywnsg0iz42pswb8v34nhipakvvisz2") (r "1.56.0")))

(define-public crate-delegate-display-1.0.8 (c (n "delegate-display") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1333rlzvvf7j7p088b2kcnqxnwh53n44n549l8ha20f11m0ms13n") (r "1.56.0")))

(define-public crate-delegate-display-2.0.0 (c (n "delegate-display") (v "2.0.0") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "09xkxm0w4679mqhpglz9b84a262sma16m8kvx6c06428fyka8323") (r "1.60.0")))

(define-public crate-delegate-display-2.1.0 (c (n "delegate-display") (v "2.1.0") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1l88wqfzrwc4b29jmfnicr76ix8608lh7g0i8halk68y3xddncif") (r "1.60.0")))

(define-public crate-delegate-display-2.1.1 (c (n "delegate-display") (v "2.1.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0rndsj11q9kl8q42blvvlhly3dfh6rp25xmv3742l51ky80m5a4q") (r "1.60.0")))

