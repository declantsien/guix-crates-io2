(define-module (crates-io de le delegation-program-sdk) #:use-module (crates-io))

(define-public crate-delegation-program-sdk-0.0.1 (c (n "delegation-program-sdk") (v "0.0.1") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "0vqnbszh62ap0ivab6rxpd27fgjcdrfq06jv4gc5k5mr1fkp10pz")))

(define-public crate-delegation-program-sdk-0.0.2 (c (n "delegation-program-sdk") (v "0.0.2") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "0z8bdfvhph2b74mqwm4f3hfxllgdrndwck7iz20mbjkj9rmcdm68")))

