(define-module (crates-io de le delegate-attr) #:use-module (crates-io))

(define-public crate-delegate-attr-0.1.0 (c (n "delegate-attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1l3m3d05nj685yncv0fjcanr5scw1c0z2b0pb7lk9vzgnjm8hymb")))

(define-public crate-delegate-attr-0.1.1 (c (n "delegate-attr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1wabc8v1ngf8llcv61jf8si5y68i9xb1xaysw2x0dfaiy6j1dh9c")))

(define-public crate-delegate-attr-0.2.0 (c (n "delegate-attr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "17b6r1npj52nr6qghvadl2n2k8xcx036j1p4cbqwdflzf4mivjh8")))

(define-public crate-delegate-attr-0.2.1 (c (n "delegate-attr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1dy61xnkr0svzjz9x5p40wmr4knis69b19i1nwvm7h7v0zba0ccf")))

(define-public crate-delegate-attr-0.2.2 (c (n "delegate-attr") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0cqm3b88kvd7q6q266qc7zc1a2kv44my72y14w5q5nixnncn6bby")))

(define-public crate-delegate-attr-0.2.3 (c (n "delegate-attr") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "07xw05lg1s1k4xvjqnmvsi6y7csa2rzf46gfzw2sx6h6lcf25r99")))

(define-public crate-delegate-attr-0.2.4 (c (n "delegate-attr") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0p20g5sgd2ipmp5c8dicgjmaswaw76hbwbnwywq8xf62ckz6apzw")))

(define-public crate-delegate-attr-0.2.5 (c (n "delegate-attr") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "1qwgw1cfcyzkq0wn2v2si7nqa7i1a7gy7qp7hscakvhvs5pwr25p")))

(define-public crate-delegate-attr-0.2.6 (c (n "delegate-attr") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0ifn3a7qkqhwbjrhd42d9b177y6v4clbmhg6gwxklwfj09pc9ig4")))

(define-public crate-delegate-attr-0.2.7 (c (n "delegate-attr") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0q27z51q29di81cj7hwx5dg3fa3iym3jk8h7v4vbygpzr33f965a")))

(define-public crate-delegate-attr-0.2.8 (c (n "delegate-attr") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0m9zk14znkjzxymi3ld10n91q3cwihx8173gj73h3j16d10hg324")))

(define-public crate-delegate-attr-0.2.9 (c (n "delegate-attr") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "1ax3l4lh9rpar36g48kpfdscsrx4l7fki3kfh6fl41x4vfh7wzpf")))

(define-public crate-delegate-attr-0.3.0 (c (n "delegate-attr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0ns7ik866ah5q05s41dz5pg2y6s4x0xa6bj19cb7arrfkg4w9aji")))

