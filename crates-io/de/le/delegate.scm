(define-module (crates-io de le delegate) #:use-module (crates-io))

(define-public crate-delegate-0.1.0 (c (n "delegate") (v "0.1.0") (h "032b6hl68l7zqqgv062np4bbp81wzjbgp6v1670176jb6i5z4fdq")))

(define-public crate-delegate-0.1.1 (c (n "delegate") (v "0.1.1") (h "14wa664ryyw5k1qs8lf5d91p2cbrd2fvvmfyskg3dbk8q6i0cr3q")))

(define-public crate-delegate-0.1.2 (c (n "delegate") (v "0.1.2") (h "0pg4r6z7n0n3cl4ggz5pdqpgxcz2457jgi34dncsamx33chjv9hy")))

(define-public crate-delegate-0.1.3 (c (n "delegate") (v "0.1.3") (h "1zv49x3kfrclyzm3kawvvkxgzfc94h6zzdpa2m6f02pha1dabsc8")))

(define-public crate-delegate-0.2.0 (c (n "delegate") (v "0.2.0") (h "0j7rc5n45lsffbzjk49dl2x9v7fzrkrng68q2mihk71mkbpri75n")))

(define-public crate-delegate-0.3.0 (c (n "delegate") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "128jgf0zxy7mpdfi9igj0lmw9jwyjpki358sbvrjlqdyxiksyp5z")))

(define-public crate-delegate-0.4.0 (c (n "delegate") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j90vilbx15z7aarvfjgwbdjwn34lyk8sanjxnv5b47mlm87l099")))

(define-public crate-delegate-0.4.1 (c (n "delegate") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l048dmx3f8wza28ksq06cd414zipyjmfcb8p1ig8r931gjv1vrx")))

(define-public crate-delegate-0.4.2 (c (n "delegate") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03v7f0n1v1vycsyii7dwb4i60inv3l14m46h2jmaripza2qcv4pz")))

(define-public crate-delegate-0.4.3 (c (n "delegate") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ckkfax0g697829dblhwva7zd8qn5xnr5zxs93m8n2k0icl33r8j")))

(define-public crate-delegate-0.5.0 (c (n "delegate") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "131w246z739cd6bqni2j43xj3z2in79mpv0y9hhxdsb36lxd0zm5") (y #t)))

(define-public crate-delegate-0.5.1 (c (n "delegate") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jq1yq7h6cyd0fxb8m1chcib79lnyg81ppb9bd8y0xxiq9hillnr")))

(define-public crate-delegate-0.5.2 (c (n "delegate") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0c688qxscm8lhmqa5s7662vq0k13j5f178nyghycmd8bpysk7mqg")))

(define-public crate-delegate-0.6.0 (c (n "delegate") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ngdcbdf7v1c2f2zs6ly6bpvs7a1knrc6k63n23nzm3hw7mgjxkz")))

(define-public crate-delegate-0.6.1 (c (n "delegate") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "19s9lpv6380i65rwz7h6vbczvijq5dcwyar8dxvmrrr62qxv9ikb")))

(define-public crate-delegate-0.6.2 (c (n "delegate") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "1bxi3shkqnhdskrm8q533ckgxyl5hdhb7k3c3xjgm74dfhqpmi1m")))

(define-public crate-delegate-0.7.0 (c (n "delegate") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "05ypmi5xm4y8zzscsccn6gr9yjwkkhycyfi22nj5asa6jm4js2np")))

(define-public crate-delegate-0.8.0 (c (n "delegate") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "1pgwcwjz7rj5b956jvc0a8dzq5kicdbj2q1wfifxqcvmjslj8ah8")))

(define-public crate-delegate-0.9.0 (c (n "delegate") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "0g7yh4wpb8343vf81ihgcqhn324nf12fygck7dhf39arbknf0n6k")))

(define-public crate-delegate-0.10.0 (c (n "delegate") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "18f80hpy90g49nf028a1snqjyscphw4zvqn8mhxbv58bqxsxzr8f")))

(define-public crate-delegate-0.11.0 (c (n "delegate") (v "0.11.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "17wxc2ga2cqnwag9yh3jwl0gmalk54sgq6mzrcjnwnxhb9siv05l")))

(define-public crate-delegate-0.12.0 (c (n "delegate") (v "0.12.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "16zpkc6v2ss5qivwx7p7vb1bjnb6s0p7kkifaqkgyl7bpv68y0af")))

