(define-module (crates-io de le delete) #:use-module (crates-io))

(define-public crate-delete-0.1.0 (c (n "delete") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0hb4q6yqwxy5zbj52lr25s5j1v6vhphys6k1sw1qpmry56rk61m6") (y #t)))

(define-public crate-delete-1.0.0 (c (n "delete") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "1jhxnnpjasrgyrzi44r9cw4d0cs2b2f3y6z194zvjj54w0i0c1f8")))

(define-public crate-delete-1.0.1 (c (n "delete") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "197xfpfzd04jw8b845lnnqkr99ry28bg0kcrjgpnmaafswqjvqxi")))

