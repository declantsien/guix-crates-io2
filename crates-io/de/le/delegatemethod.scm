(define-module (crates-io de le delegatemethod) #:use-module (crates-io))

(define-public crate-delegatemethod-0.1.0 (c (n "delegatemethod") (v "0.1.0") (h "0lm9cjsr6qg034pl545pbh6p788qgkwgxf90sc17m6qslwrjacl4")))

(define-public crate-delegatemethod-0.1.1 (c (n "delegatemethod") (v "0.1.1") (h "045hvl6k6c0gkp2ldml3vx4103wimk6hr3r5n0p0hlk97q5c3x0m")))

(define-public crate-delegatemethod-0.2.0 (c (n "delegatemethod") (v "0.2.0") (h "1y26llnhxa7mshmqlaspx64jc1knsqwaqkcdg5kzhdblbb3dklv7")))

(define-public crate-delegatemethod-0.2.1-pre (c (n "delegatemethod") (v "0.2.1-pre") (h "0nd2g1753f8w1pwh5b4sjsy2mj1888xqcxrk6ax784band1462pc") (y #t)))

(define-public crate-delegatemethod-0.2.1 (c (n "delegatemethod") (v "0.2.1") (h "0zymk713myr02yps8ip2bi5jhky7w542j4gl2rh53mh58a4gx7bm")))

(define-public crate-delegatemethod-0.2.2 (c (n "delegatemethod") (v "0.2.2") (h "1sipkafqrjg3pxx42c1lik4qnijw88bv5nqz8662jx13rsijd3dl")))

