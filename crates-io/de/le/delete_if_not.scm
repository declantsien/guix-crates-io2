(define-module (crates-io de le delete_if_not) #:use-module (crates-io))

(define-public crate-delete_if_not-0.1.0 (c (n "delete_if_not") (v "0.1.0") (h "13zbbfh2l3cb2mdjkk3553y95dy5g5crlfg9z36adw8z3i0s6gg5") (r "1.77")))

(define-public crate-delete_if_not-0.1.1 (c (n "delete_if_not") (v "0.1.1") (h "0j1f63kk0ndck2ibwdq01g4n96vc3y6yjxddhaly4w0qlfd72c5n") (r "1.77")))

(define-public crate-delete_if_not-0.1.2 (c (n "delete_if_not") (v "0.1.2") (h "0fnvah0jrb4qmjjk09xcylspq4pa2y7m6ablh7lx010336s024m5") (r "1.77")))

