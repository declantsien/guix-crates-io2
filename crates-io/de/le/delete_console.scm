(define-module (crates-io de le delete_console) #:use-module (crates-io))

(define-public crate-delete_console-0.1.0 (c (n "delete_console") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "swc_core") (r "0.44.*") (f (quote ("plugin_transform"))) (d #t) (k 0)) (d (n "swc_plugin_loadable_components") (r "^0.3.0") (d #t) (k 0)) (d (n "testing") (r "^0.31.31") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "05xq30s0r63xky133fnc26bq51fklvxn22ddsb4kzjp1sk8hy0ra")))

