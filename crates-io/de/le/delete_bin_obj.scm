(define-module (crates-io de le delete_bin_obj) #:use-module (crates-io))

(define-public crate-delete_bin_obj-0.1.0 (c (n "delete_bin_obj") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "155pq8kmfiw9mm1igan74abivfnp3n633icp96jwh2qa33gh6drg")))

(define-public crate-delete_bin_obj-0.2.0 (c (n "delete_bin_obj") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01i209526c7kd3qhlwq8qm9icr2ba62yn7n4bfd2bhs1bzwcgiwy")))

(define-public crate-delete_bin_obj-0.2.1 (c (n "delete_bin_obj") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "12q0c08yh58hbbk242ksk9c7pinxa702yqa0mv98vvrhvhnm22p9")))

(define-public crate-delete_bin_obj-1.0.0 (c (n "delete_bin_obj") (v "1.0.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01xdrfclvspfmqymyhv05z23679a0abwngwal5gja721bwb4j1mc")))

(define-public crate-delete_bin_obj-1.0.1 (c (n "delete_bin_obj") (v "1.0.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0nh0bfqzwymmms17fpghhspaqdyh8010pbazjkqk7rkjg8kfgm1r")))

