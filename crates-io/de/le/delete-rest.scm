(define-module (crates-io de le delete-rest) #:use-module (crates-io))

(define-public crate-delete-rest-0.3.0 (c (n "delete-rest") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0hmhwawm4pwhhjyv27rg7zvl33nzf9hsf66566lnszvk76k4dqx7") (y #t)))

