(define-module (crates-io de bo debounced) #:use-module (crates-io))

(define-public crate-debounced-0.1.0 (c (n "debounced") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3.25") (f (quote ("sink"))) (d #t) (k 2)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (f (quote ("sink"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full" "test-util"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1dgssvwnrgsv4wpyjmi9kj777y32ipjcqjzslc0sm84zdcsb1n73")))

