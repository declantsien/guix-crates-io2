(define-module (crates-io de bo debot-position-manager) #:use-module (crates-io))

(define-public crate-debot-position-manager-0.1.0 (c (n "debot-position-manager") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yk4mzcnkcgp6bk7j5gpds5fzqlw3sgh6zkkb03ddhapfc7h9idj")))

(define-public crate-debot-position-manager-0.1.1 (c (n "debot-position-manager") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qf8dr1n5rlgalj1i6i55z673gqxilcl3iw45p791b4apc9hi3bl")))

(define-public crate-debot-position-manager-0.1.2 (c (n "debot-position-manager") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rlxabk2brn0km746c1jqpmz6p89gz03y4qzvmw0q7sfa9rjkcvv")))

(define-public crate-debot-position-manager-0.1.3 (c (n "debot-position-manager") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hrxixahkbz1qmrlf40rxdbc4pn90y9q58lcybmmnrnis7szdjpq")))

(define-public crate-debot-position-manager-0.1.4 (c (n "debot-position-manager") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0id7vkpp12163ajgbasqbhsvl8jq6jcsm7zsa1k5qllnmi2hszpv")))

(define-public crate-debot-position-manager-0.1.5 (c (n "debot-position-manager") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qigrcbp88zav5rghhpfd44k7lhl8k0iz7zh6ljl2n84qi28py3h")))

(define-public crate-debot-position-manager-0.1.6 (c (n "debot-position-manager") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xx3gwa341d1dxhyhig6nnyxhwqrd1ccsvmbhm81zz9l8308wx2p")))

(define-public crate-debot-position-manager-0.1.7 (c (n "debot-position-manager") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ixyj6jv8dfr4gl7hwj15mv9z67pjm4k6lc62wx0n87njgi0hfyi")))

(define-public crate-debot-position-manager-0.1.8 (c (n "debot-position-manager") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "153b1z283453xqmpc33anh1hfh714y1njn7335n1yrfpwi3hiz8r")))

(define-public crate-debot-position-manager-0.1.9 (c (n "debot-position-manager") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f2iknb82wrclpb0y4f05vkbpls1nz6payg1p7acrrn4a42kzpir")))

(define-public crate-debot-position-manager-0.2.0 (c (n "debot-position-manager") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09zsd9wmqj30jdp0fq789c2grhhd4j9xz0bb76ik8dbwldsw890d")))

(define-public crate-debot-position-manager-0.2.1 (c (n "debot-position-manager") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bdv0mrzg8p2914awfrh2nlma9sigkg8l2wzfy0cj3qwmj20nlx1")))

(define-public crate-debot-position-manager-0.2.2 (c (n "debot-position-manager") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01mpa8dp89pm4pnpq71azc3xl8pjbg12q1rl4an9b79p48xc7xly")))

(define-public crate-debot-position-manager-0.2.3 (c (n "debot-position-manager") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "029ldh2imnhjc4rc5r52jdvc6dmlhf4zh93jvijxv1l2bbcxc362")))

(define-public crate-debot-position-manager-0.2.4 (c (n "debot-position-manager") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q91hipskvylsyir3s15bpczfzsvxad7ichmpa4wz0k06knhn9bs")))

(define-public crate-debot-position-manager-0.2.5 (c (n "debot-position-manager") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10spaq2vnymix0h5ihxqpa9crhk8sbx234b1lsc242q9c59mja7l")))

(define-public crate-debot-position-manager-0.2.6 (c (n "debot-position-manager") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bwi19zysrzvjffkaggxkfplmabn2zq7ljvq8dbna6zwfiaj19zj")))

(define-public crate-debot-position-manager-0.2.7 (c (n "debot-position-manager") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xh6bib02890raac7w66zdm2sp6cay08cpkvh78phbchamk449l8")))

(define-public crate-debot-position-manager-0.2.8 (c (n "debot-position-manager") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nscyjd6rk3rvngvr9aa2051gszzi3mby0spj9c0fknmajd757r8")))

(define-public crate-debot-position-manager-0.2.9 (c (n "debot-position-manager") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16wcg2mc4zksph03f6w7k4lxbf0gz81pn3fb43b42gf0vzw9963q")))

(define-public crate-debot-position-manager-0.3.0 (c (n "debot-position-manager") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rzqid9p5ypg1ci507r0pgas2albgk2cnppyv2a5s1zay20kv1ki")))

(define-public crate-debot-position-manager-0.3.1 (c (n "debot-position-manager") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yajbdkxyfx1md3s3f5l3c5bmr6cjg9wqiwsvy1ffafks0gcksr4")))

(define-public crate-debot-position-manager-0.3.2 (c (n "debot-position-manager") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gl9kdlh45vvvddqnn6cqfzgxs8lr8j31i6fv5jgzd76zh5gp7w7")))

(define-public crate-debot-position-manager-0.3.3 (c (n "debot-position-manager") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yl089mybv5vl5nnqs93m3cl67rzjjkdgn4lk90rvp8r8q47n61h")))

(define-public crate-debot-position-manager-0.3.4 (c (n "debot-position-manager") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bm6asypfyirqd7xhpc5pnadpg7ik39jpmnn61yhrrcfj3m2gxsc")))

(define-public crate-debot-position-manager-0.4.0 (c (n "debot-position-manager") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yysps1iijpwmxk508pqm6s6rrf5z31makb8jl4qh5hcaskpdymc")))

(define-public crate-debot-position-manager-0.4.1 (c (n "debot-position-manager") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03zds4npkxn38a04s2rvf47axgf88kiqhl71ajiv5am0jmkcvxkb")))

(define-public crate-debot-position-manager-1.0.0 (c (n "debot-position-manager") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k1ps85g0563gnyg4y6rc9v5q0jr9xni1g9hkq6n8qny496radbq")))

(define-public crate-debot-position-manager-1.1.0 (c (n "debot-position-manager") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ygbcq6cp8hm0k31jw02lrp6z8pxn3lg1dd2la7d1hqarv3sxhh")))

(define-public crate-debot-position-manager-1.1.1 (c (n "debot-position-manager") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11f6wgijkycfg542131g81bhkh9sk4k9kma2yhfz4ilqbk09229k")))

(define-public crate-debot-position-manager-1.1.2 (c (n "debot-position-manager") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ksb4wvvqzk0sz5d3qs491b4q9sa9sz2yhnkv6760gjd08z4flbw")))

(define-public crate-debot-position-manager-1.1.3 (c (n "debot-position-manager") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "017qqrchfdgm8w6isj390b2x2sv8rwdqmi4m1aly24n2m2s9z8xp")))

(define-public crate-debot-position-manager-1.1.4 (c (n "debot-position-manager") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s7h0z7w6kzzy11r3lmlfw6wbd8cs0wy2rry82bsp93h2pirpfhs")))

(define-public crate-debot-position-manager-1.1.5 (c (n "debot-position-manager") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00s0qvabh6bz6ynj217lzhzsj1r9zmk70iy10dy3h03jc29gp54v")))

(define-public crate-debot-position-manager-1.1.6 (c (n "debot-position-manager") (v "1.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l0mpwrv1bjafazzxwpfa9n3dkc6ik27hd4bqv7xck9q10vj1jl5")))

(define-public crate-debot-position-manager-1.1.7 (c (n "debot-position-manager") (v "1.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sl9syb3n7xvyqhjl7j2j0ibcxnrdx8la48cxyisz2ah4czgp084")))

(define-public crate-debot-position-manager-1.1.8 (c (n "debot-position-manager") (v "1.1.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "050rjc0bgqgz1izqx1h4473z6276if6y79fl43rz6x5yvk3jxkz2")))

(define-public crate-debot-position-manager-1.1.9 (c (n "debot-position-manager") (v "1.1.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "079nvs69k78ah3l4i8kdikjrf88rb2pv2769rjcppisy14nr0xpq")))

(define-public crate-debot-position-manager-1.1.10 (c (n "debot-position-manager") (v "1.1.10") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wyh2rysj0z0qzf23dfvg1hqwfawiszv63dhn4mxqqwcwsdc71l4")))

(define-public crate-debot-position-manager-1.1.11 (c (n "debot-position-manager") (v "1.1.11") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0injznr6r1yl9x7blyiapr2sl189m9gr3pirdrymp8z60dc0155w")))

(define-public crate-debot-position-manager-1.2.0 (c (n "debot-position-manager") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07v1c703ndvqwncc6j57d4bqnx6xwvg96f6mbf7d3rf352pghgl7")))

(define-public crate-debot-position-manager-1.2.2 (c (n "debot-position-manager") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdg53c3w5b7lha0mbg1apk6hn8wsx00gac3qrgn3brv3iwzhxvh")))

(define-public crate-debot-position-manager-1.2.4 (c (n "debot-position-manager") (v "1.2.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0inir2fa6x8xpaqm93a4wd6sxh0dr93dhf4zdz0kfr211v3rh0yg")))

(define-public crate-debot-position-manager-1.2.5 (c (n "debot-position-manager") (v "1.2.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05sh8svd3cm1mh5vzw1vqimdj3ymi6ma6vqfpfvi7cgld4rgnzca")))

(define-public crate-debot-position-manager-1.2.6 (c (n "debot-position-manager") (v "1.2.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h5vllzj1aai2ha1fg0s8hb3rd0q559dac06v3sh2wqjn5d08hb")))

(define-public crate-debot-position-manager-1.2.7 (c (n "debot-position-manager") (v "1.2.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07rfffybh4jmz4z8v9j3hw7flha676slhnyakps34pd6xmg7jxh6")))

(define-public crate-debot-position-manager-1.2.8 (c (n "debot-position-manager") (v "1.2.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rldv4wzii3fk5m24xmb4mxsjf27d48il3zzaj3fgbk23i60d0aj")))

(define-public crate-debot-position-manager-1.2.9 (c (n "debot-position-manager") (v "1.2.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01nr4p170fzyqiadzsd8a4s8h7g9sqc0dggkgxvfqhsab7dn33wv")))

(define-public crate-debot-position-manager-1.2.10 (c (n "debot-position-manager") (v "1.2.10") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11jkmkpqf2ir64ysphgm2w063ndnb7l4bab0wlck1d9p272v8nm3")))

(define-public crate-debot-position-manager-1.2.11 (c (n "debot-position-manager") (v "1.2.11") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f9zfbv1a7vs1l5grc7v00r22qzvpsvgij7rvwah36njxka8br47")))

(define-public crate-debot-position-manager-1.2.12 (c (n "debot-position-manager") (v "1.2.12") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fsb84qg7wp4cpq5zf78mvs0m8x3lhg3zmswj211g0v53pbllnm2")))

(define-public crate-debot-position-manager-1.2.13 (c (n "debot-position-manager") (v "1.2.13") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j1y37yd1scg71zs38g5g9qbx6cp7fmcclsi8jw5ppq0n5i0fqsl")))

(define-public crate-debot-position-manager-1.2.14 (c (n "debot-position-manager") (v "1.2.14") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m30rs6b0lvsgvc0rqkvsfdpli3dv1i5lhkc716csbhs66hpgaxd")))

(define-public crate-debot-position-manager-1.2.15 (c (n "debot-position-manager") (v "1.2.15") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p5mvlysngg0nl9mpg5jwv3hr8cpkgbqnqi3a03awc0s3m369jiq")))

(define-public crate-debot-position-manager-1.2.16 (c (n "debot-position-manager") (v "1.2.16") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z8fgws6qj5dlavyc87pqs2kpjkfi32haca0kipyc6xc6shb59fv")))

(define-public crate-debot-position-manager-1.2.17 (c (n "debot-position-manager") (v "1.2.17") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "137l0gb33wsxga9lj0vnf4qbmb40m4bkkwx3aqdvbkc7zjfiz7ym")))

(define-public crate-debot-position-manager-1.2.18 (c (n "debot-position-manager") (v "1.2.18") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05m0l0d1i1vgzjf3ml2mir17vcniqlanmxsp4qxgwfziccizaxki")))

(define-public crate-debot-position-manager-1.2.19 (c (n "debot-position-manager") (v "1.2.19") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i1viz1ihy8x7zfc76jw9abikgwrd8i4gc7z708wqam2fzhw7y10")))

(define-public crate-debot-position-manager-1.2.20 (c (n "debot-position-manager") (v "1.2.20") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12pbx0f2bja6152xwpgz7xhlcb7098g1hiy9g3c46w9rr0mc8vdy")))

(define-public crate-debot-position-manager-1.2.21 (c (n "debot-position-manager") (v "1.2.21") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gzn0mf9icqf8xcq1zkgzdh5rbnrnmcda0c2plak2pj4kmgp7yn0")))

(define-public crate-debot-position-manager-1.2.22 (c (n "debot-position-manager") (v "1.2.22") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pciy57p8n4mwkagyx55v1q38f6ffshli4ayvg08h42wzr3imw6i")))

(define-public crate-debot-position-manager-1.2.23 (c (n "debot-position-manager") (v "1.2.23") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "060q3pqz2ywq55qi48z8cyx6n1rajkbnzl7bw90rwi07f0id06ls")))

(define-public crate-debot-position-manager-1.2.24 (c (n "debot-position-manager") (v "1.2.24") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "141xkpyxkpv3vhm3l8qdfkvf7swxxrvsmwj6mlb84kyzww3jr9db")))

(define-public crate-debot-position-manager-1.2.25 (c (n "debot-position-manager") (v "1.2.25") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14b32pdx2s44rpi08vxgj011x6h164kxhpdhm7fnhijf9b6rqb3r")))

(define-public crate-debot-position-manager-1.2.26 (c (n "debot-position-manager") (v "1.2.26") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01jsz98x27h04241i8x6sl5yyj1v83vib4yr0ym6a36zl8bzp4zm")))

(define-public crate-debot-position-manager-1.2.27 (c (n "debot-position-manager") (v "1.2.27") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "141frljyvh0hsxwqmsp6jg2xx1xjmw8jp7b869vn21wypx2jchlf")))

(define-public crate-debot-position-manager-1.2.28 (c (n "debot-position-manager") (v "1.2.28") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hsmwn6sb1nqp4ql7dm10dkfkxqcavp5hdk6dpmim6n3216dpjzr")))

(define-public crate-debot-position-manager-1.2.30 (c (n "debot-position-manager") (v "1.2.30") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18x1lxgl3cb5jl3ki8asg8nkyp36xxm0akdj1pigygj8ik2kvgc1")))

(define-public crate-debot-position-manager-1.2.31 (c (n "debot-position-manager") (v "1.2.31") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bq40z4z8hjsc6j00jjzlbv5468df24w7m5wb87cmddrmjspa40i")))

(define-public crate-debot-position-manager-1.2.32 (c (n "debot-position-manager") (v "1.2.32") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01b08dk2lm5lva1f0x8llr6p2mv0jyy9gfhv6nsx67lhaxqzrjrm")))

(define-public crate-debot-position-manager-1.2.33 (c (n "debot-position-manager") (v "1.2.33") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bdkydfa6sb04bvj278xdr7x3zi7i50bfg5b8wwi6554ic6zqiqw")))

(define-public crate-debot-position-manager-1.2.34 (c (n "debot-position-manager") (v "1.2.34") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w3c7ri9rd7pnphz4bnq0zkxm3fclpn6lrn3vdzqf1yl0zwaq42r")))

(define-public crate-debot-position-manager-1.2.35 (c (n "debot-position-manager") (v "1.2.35") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wpf1ahlac92wgwi2y8nrsqhl11bfvc2hfy7xhasj8zi48y7qqwh")))

(define-public crate-debot-position-manager-1.2.36 (c (n "debot-position-manager") (v "1.2.36") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fn5vwwn0nlfwj702pf04lmacsqvdvxlbbmfv2rfnqdq4qcwzgva")))

(define-public crate-debot-position-manager-1.2.38 (c (n "debot-position-manager") (v "1.2.38") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pyhnwpjkydwrj9qbvcr596076vhh475ch9ws4k926cfs0pqf8l8")))

(define-public crate-debot-position-manager-1.2.39 (c (n "debot-position-manager") (v "1.2.39") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yiwjjy303q9hz5im40vqzj5h9xaf5gbizqygvjzfky7m5087j0a")))

(define-public crate-debot-position-manager-1.2.40 (c (n "debot-position-manager") (v "1.2.40") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l6dkzqdqxjnnlhalmd9ajk8h1bnkvls1431z30mhnaidgljirw0")))

(define-public crate-debot-position-manager-1.2.41 (c (n "debot-position-manager") (v "1.2.41") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15vkiv1a3f5y1z8w37xrk101x79imz2x3nx5hs82b9pz0nbw701k")))

(define-public crate-debot-position-manager-1.2.42 (c (n "debot-position-manager") (v "1.2.42") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11k7qyn01lgn80rrg9iwlvkxh6cgd12nmmc86z808z0qn135d0mc")))

(define-public crate-debot-position-manager-1.2.43 (c (n "debot-position-manager") (v "1.2.43") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b9hpa610vqpfk5vilxplyc9hgbv7v12j9dxkwrvm0mhi9m8swmr")))

(define-public crate-debot-position-manager-1.2.44 (c (n "debot-position-manager") (v "1.2.44") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ai9x51clbf47cyz5iwhb0wf9w1b3zm2584mfndbnidi390cdaan")))

(define-public crate-debot-position-manager-1.2.45 (c (n "debot-position-manager") (v "1.2.45") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lv6vvizz8xmc88jp7nkp9jswf0830x6p6wydjcfrfkamdb3q7zd")))

(define-public crate-debot-position-manager-1.2.46 (c (n "debot-position-manager") (v "1.2.46") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l93x3yzp9mwi0w70k9nk8hjnv9h8l2p4b6k8wgd8fyfi9f8lg07")))

(define-public crate-debot-position-manager-1.2.47 (c (n "debot-position-manager") (v "1.2.47") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00prnl7r9qq1kj68dcv1n17fw9pdag3f84nnps7n87hjyj4b37y0")))

(define-public crate-debot-position-manager-1.2.48 (c (n "debot-position-manager") (v "1.2.48") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jn6hf3i0ixzgssbnyaicwcxbkzf7vsn7i4blivffg1w0kzl2yys")))

(define-public crate-debot-position-manager-1.2.50 (c (n "debot-position-manager") (v "1.2.50") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dvh1i04yb9xxx5h2gpjf90nz3x3icg98l7nch248dmxaqkb21xw")))

(define-public crate-debot-position-manager-1.2.51 (c (n "debot-position-manager") (v "1.2.51") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07hpcrdlk1a18bk35pa8s8pakmg1x3khcqi4hi7zc7gywq952iyz")))

(define-public crate-debot-position-manager-1.2.52 (c (n "debot-position-manager") (v "1.2.52") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rcnv8hyjss5v456r3hy6mhnx5db7az9hxbaajac3msn9bm36jwv")))

(define-public crate-debot-position-manager-1.2.53 (c (n "debot-position-manager") (v "1.2.53") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y8gmdkbwgdkhqkv560i48smbpgmzy2xcmy754j3glv9lgzyrdw7")))

(define-public crate-debot-position-manager-1.2.54 (c (n "debot-position-manager") (v "1.2.54") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fz4rrwggnm2whvfcxif41r41cpl8l2hhv5xcvd5k9b7r22dh5lm")))

(define-public crate-debot-position-manager-1.2.55 (c (n "debot-position-manager") (v "1.2.55") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r5w5kgslbnhgnp3x2ib23ickq36cpx2j7gs3ifq575fk0168fii")))

(define-public crate-debot-position-manager-1.2.56 (c (n "debot-position-manager") (v "1.2.56") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yiqpzhmpnmirkdbm5l9ajb5v3as2mpk1b606mkf6c3d45hsiwcx")))

(define-public crate-debot-position-manager-1.2.57 (c (n "debot-position-manager") (v "1.2.57") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k1rxd37y3c4gs9ka22x77xymsc3nxmdidbzqgvcds4zi5arsi4j")))

(define-public crate-debot-position-manager-1.2.58 (c (n "debot-position-manager") (v "1.2.58") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a6kjd6zp98n7826vzar2wyc5zjwsvw4f6n3nmp5aiqxjfz5nzdw")))

(define-public crate-debot-position-manager-1.2.59 (c (n "debot-position-manager") (v "1.2.59") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1apgshv7az4m64qn2018ckas8906yn36hs7rwqjirb8cn7fxxi2c")))

(define-public crate-debot-position-manager-1.2.60 (c (n "debot-position-manager") (v "1.2.60") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pmxlxcz3vj78gcic1yvfzj4b1s5jgsspfxm7f66s41h6n9ksc4g")))

(define-public crate-debot-position-manager-1.2.61 (c (n "debot-position-manager") (v "1.2.61") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zxqqpmca86amw9hzpn41gd2jmbbjcxl992ffsi7xw4m84bciv1h")))

(define-public crate-debot-position-manager-1.2.62 (c (n "debot-position-manager") (v "1.2.62") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r6vpnjbkqsqh61qx3bf6zih1v0n8pdsk7wrhrpf8yd5iksnjjmw")))

(define-public crate-debot-position-manager-1.2.63 (c (n "debot-position-manager") (v "1.2.63") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ik75hdz8fs65zjyqqbph6cq4shbv11xsxbvim19v9v8dhjy777i")))

(define-public crate-debot-position-manager-1.2.65 (c (n "debot-position-manager") (v "1.2.65") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m6pm7rxab46xp041j4wdb429h2h27yk8p1nky0x3fclwv6204wq")))

(define-public crate-debot-position-manager-1.2.66 (c (n "debot-position-manager") (v "1.2.66") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17gm3i40mqj4489f8w80ldf5szmpdqkwf7kyzb7mpj43994qj0zw")))

(define-public crate-debot-position-manager-1.2.67 (c (n "debot-position-manager") (v "1.2.67") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w12fq5gfm7xgmsihwsg1fwqjn2qw6dmx2irpvs3ha88ak6dr3cc")))

(define-public crate-debot-position-manager-1.2.68 (c (n "debot-position-manager") (v "1.2.68") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ybqz7qg9z59wn1bfxwqi58fi53c75xli8ddl4kn1j5aqpa0kkdj")))

(define-public crate-debot-position-manager-1.2.69 (c (n "debot-position-manager") (v "1.2.69") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n4lv26hv6n14lb7qs52val5vnrmrh554jzavg5j6hp8018adwnc")))

(define-public crate-debot-position-manager-1.2.70 (c (n "debot-position-manager") (v "1.2.70") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rzvzyxafi5n6w3q85jglahnlv0k8m7kxv5648b8iq41k2gmlq9")))

(define-public crate-debot-position-manager-1.2.71 (c (n "debot-position-manager") (v "1.2.71") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15yd2gadc5092nms47q6migv22vp72ivnh2y3lv95k7g2p8jqlh2")))

(define-public crate-debot-position-manager-1.2.72 (c (n "debot-position-manager") (v "1.2.72") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yiphl9mdpgmg7y80v9wdvpg6z8gads6w278vj8vwmq54n9vy1vx")))

(define-public crate-debot-position-manager-1.2.73 (c (n "debot-position-manager") (v "1.2.73") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04dzdy4z0xk7wawyl4q8kjxx3vj7qj8pl2mcg3rs00w8qg35spf5")))

(define-public crate-debot-position-manager-1.2.74 (c (n "debot-position-manager") (v "1.2.74") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sfkvy7y985yms8sy1i606cl40cldf79h0fha5hfkwjxq4hbrfsc")))

(define-public crate-debot-position-manager-1.2.75 (c (n "debot-position-manager") (v "1.2.75") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lz415jqgdj9yymywdy358qx32wycndmvdn8m63dd8anii5pj25j")))

(define-public crate-debot-position-manager-1.2.76 (c (n "debot-position-manager") (v "1.2.76") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06f4g58xlncslsx66212ws4cc0rc6n6wkjvvz5ckqgi7176nz1xd")))

(define-public crate-debot-position-manager-1.2.77 (c (n "debot-position-manager") (v "1.2.77") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1arhmgfswycsrry1yjjvd0c6anj5hds46br9havplk0hxdraisym")))

(define-public crate-debot-position-manager-1.2.78 (c (n "debot-position-manager") (v "1.2.78") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1160vzh01pdb6icnc07mxlh4f8dpjh8c10lnqf9fawp0cz31rvi0")))

(define-public crate-debot-position-manager-1.2.79 (c (n "debot-position-manager") (v "1.2.79") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x9kwb1b1ic556pfbl8nz1600jfklbky06zzghgbis7f2lhsav2d")))

(define-public crate-debot-position-manager-1.2.80 (c (n "debot-position-manager") (v "1.2.80") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vg3arq9vr3z0x6gv8ipy2mlxl4mr3q6xqmvy7lbxk31irsz8kic")))

(define-public crate-debot-position-manager-1.2.81 (c (n "debot-position-manager") (v "1.2.81") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05y696xza11s7psjjjk93nh6kh67g9adj5i3ifdy0pbd7kczf9z8")))

(define-public crate-debot-position-manager-1.2.82 (c (n "debot-position-manager") (v "1.2.82") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fwk1r3xjhy4nb89wv4abjvczd8pwwy0s41kg855d0i2czsg2hrr")))

(define-public crate-debot-position-manager-1.2.83 (c (n "debot-position-manager") (v "1.2.83") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yg617gaj52167m1ggaa3c4bc029v3kbjq9z32k5x3kfdysywp5x")))

(define-public crate-debot-position-manager-1.2.84 (c (n "debot-position-manager") (v "1.2.84") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1radd1shw1x6y6ll0a3yf7x826kjcfalpjv9m2jkjssw2qrxfxv3")))

(define-public crate-debot-position-manager-1.2.85 (c (n "debot-position-manager") (v "1.2.85") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fh7k0921kyb7dmfn200smlvhi620j0wp68c9b69v1kqxz2mdaik")))

(define-public crate-debot-position-manager-1.2.86 (c (n "debot-position-manager") (v "1.2.86") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ldgvj82b79sapx83qcxql8y6ivjk4ibr5zh3j26cwfijd02wll3")))

(define-public crate-debot-position-manager-1.2.87 (c (n "debot-position-manager") (v "1.2.87") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "137vsaxkypb1wakhyaljaa8xra7n90sjhymdh0arppabbkby1vg4")))

(define-public crate-debot-position-manager-1.2.88 (c (n "debot-position-manager") (v "1.2.88") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fbqsbl0jxl0p5s0s2gf4pq3zkfw0kgrgmq148g50a5h5nadg0rm")))

(define-public crate-debot-position-manager-1.2.89 (c (n "debot-position-manager") (v "1.2.89") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19dprdvri9r38mjxg8saxhij3zgfyl7yjdmc2qs7finmbfa2x62x")))

(define-public crate-debot-position-manager-1.2.90 (c (n "debot-position-manager") (v "1.2.90") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vj5g3rvglwaw9dd9fy2m132vq0xms8yc070akknp9hh2sxn66hk")))

(define-public crate-debot-position-manager-1.2.91 (c (n "debot-position-manager") (v "1.2.91") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bfgl4j0qdk68yhlv9xq61xbzl5p5kklw3pdxygqq4g7qb79nxw7")))

(define-public crate-debot-position-manager-1.2.92 (c (n "debot-position-manager") (v "1.2.92") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10r6wl8as661qnv11ryhl3sk4ml8x9vmany124rmyj47jv9496pj")))

(define-public crate-debot-position-manager-1.2.93 (c (n "debot-position-manager") (v "1.2.93") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x437wqw9rl7f8n3v3gyihdqzllgsblb46gaj7pxg70nk0lg7d7c")))

(define-public crate-debot-position-manager-1.2.94 (c (n "debot-position-manager") (v "1.2.94") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11wy3j4xnb9qb9m825wpppzl8mk4ixx60xlc5f2dwl7yv62ny0kk")))

(define-public crate-debot-position-manager-1.2.95 (c (n "debot-position-manager") (v "1.2.95") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x703zcacf5l9vgvfi3glhyp7mr5h6zi3plarwd3v4198wn77ylk")))

(define-public crate-debot-position-manager-1.2.96 (c (n "debot-position-manager") (v "1.2.96") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jy94wvpfcyxixig9x5z567d8adlld9gcy0ga6bw93wzfw0qw14h")))

(define-public crate-debot-position-manager-1.2.97 (c (n "debot-position-manager") (v "1.2.97") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wbppdh4gd17s5d8rr24hylvc9sl6bwwz1dld2358fqyx7vcdby4")))

(define-public crate-debot-position-manager-1.2.98 (c (n "debot-position-manager") (v "1.2.98") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14xk4b7rqxy69xkv5jrp9ji4ib8wiypb0mbp77qahkw2bgir45ic")))

(define-public crate-debot-position-manager-1.2.99 (c (n "debot-position-manager") (v "1.2.99") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c51dx4248m1vrfrk3d8figvwsps4lyjncv6lkpl14kycbv4ifsm")))

(define-public crate-debot-position-manager-1.2.100 (c (n "debot-position-manager") (v "1.2.100") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "065mafr93hi882m68gdvdwc0dy3r3wbsbbgzp08h11mkiln2wv7g")))

(define-public crate-debot-position-manager-1.2.101 (c (n "debot-position-manager") (v "1.2.101") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "120g2mxkpxsx089mjwcfmrd4q47aain8fz8hr3ck2nia0q2gxbaj")))

(define-public crate-debot-position-manager-1.2.102 (c (n "debot-position-manager") (v "1.2.102") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x5vj2pj88dpl8iy4gwzmkarczw3crwbkb4nj4z266s3knp2vipl")))

(define-public crate-debot-position-manager-1.2.103 (c (n "debot-position-manager") (v "1.2.103") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mw4c0qqrm06nw0axmj4dvqi43df8s7y5y2yhs6ypqzz54c0cgaj")))

(define-public crate-debot-position-manager-1.2.104 (c (n "debot-position-manager") (v "1.2.104") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i8irdqqpgzm1rj7shsq45l7x60d78vsqy48l2f6fkibjgv7nhql")))

(define-public crate-debot-position-manager-1.2.105 (c (n "debot-position-manager") (v "1.2.105") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cd3cxxfmwna27r1wcr49qp73mqc0m73pxfvjzklgzs0qa9smc93")))

(define-public crate-debot-position-manager-1.2.106 (c (n "debot-position-manager") (v "1.2.106") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "debot-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hyyqry3zbml1f5qdq74kz9lgifq4d7s4cpi7w0y85si0zxlgzvm")))

