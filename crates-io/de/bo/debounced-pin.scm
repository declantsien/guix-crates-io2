(define-module (crates-io de bo debounced-pin) #:use-module (crates-io))

(define-public crate-debounced-pin-0.1.0 (c (n "debounced-pin") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1q547nkf2mr4khhqdhvxadl932b774khbi2p0m9i0x5x670vy3pk")))

(define-public crate-debounced-pin-0.1.1 (c (n "debounced-pin") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1f25ahhcji3dpsw6mkj0sl8vybzvxywkhd9bgk81cxbhihz22hjx")))

(define-public crate-debounced-pin-0.2.0 (c (n "debounced-pin") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 2)))) (h "10l60839zqzy0afj4xwckla0b8s0z3d1a01ijzch11xdq8by9l70")))

(define-public crate-debounced-pin-0.3.0 (c (n "debounced-pin") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (f (quote ("derive"))) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "stm32f3xx-hal") (r "^0.3.0") (f (quote ("rt" "unproven" "stm32f303"))) (d #t) (k 2)))) (h "0vpv7gng0mqzv4094j1hfinbg5gzk7mpzcs8pmp7nhcva9p1bkkr")))

