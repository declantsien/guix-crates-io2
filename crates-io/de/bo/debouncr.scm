(define-module (crates-io de bo debouncr) #:use-module (crates-io))

(define-public crate-debouncr-0.1.0 (c (n "debouncr") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "1yrj72p0vj4dmjq6rnh58qx2fmcl7ff13w8nih213a72hh38iw8k")))

(define-public crate-debouncr-0.1.1 (c (n "debouncr") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "0g86s6048lxzhp6wjqcnilfjqmlzgrgl0rnfz13hqvl6cld9mycp")))

(define-public crate-debouncr-0.1.2 (c (n "debouncr") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "0hslswvjj9w2vs5pqwgiwxi4dm95v83zwvdl2i8b0xfmna99mb3s")))

(define-public crate-debouncr-0.1.3 (c (n "debouncr") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "0sn1cnyxvhpxlajppd38jw1kkrrhramqkm8xskcpxbm9fj69ffaa")))

(define-public crate-debouncr-0.2.0 (c (n "debouncr") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "19i32k83ciw1x90s3h5xql0rv10is1lz38xjyzldv250kra2n2zp")))

(define-public crate-debouncr-0.2.1 (c (n "debouncr") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "07jzmlspivk7i6apiiy1p0522biysgmq6y8s44nrz6ai6x7rr2gd")))

(define-public crate-debouncr-0.2.2 (c (n "debouncr") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)))) (h "0swhfgqlabknpwdqkb8p8ixhil30di1gh7lqs7ijbapl3fp43lbq")))

