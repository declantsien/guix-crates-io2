(define-module (crates-io de bo debot-ether-ether-utils) #:use-module (crates-io))

(define-public crate-debot-ether-ether-utils-0.1.0 (c (n "debot-ether-ether-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "ethers-middleware") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16q1xg71flaycbcajak60zd6hnzm9pnh4y39l0nznlhqr5gv8k9x")))

