(define-module (crates-io de bo deboog-derive) #:use-module (crates-io))

(define-public crate-deboog-derive-0.1.1 (c (n "deboog-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dprgpq2pjn4ydidvs0skzr5j3jhwhn721f3m4pcp2p053c60pq0")))

