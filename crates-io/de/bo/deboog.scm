(define-module (crates-io de bo deboog) #:use-module (crates-io))

(define-public crate-deboog-0.1.1 (c (n "deboog") (v "0.1.1") (d (list (d (n "deboog-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1k99kp87dnl9cw8aj1vaczhjqbh9abiml3527i8a09b7cm1nvsjr")))

(define-public crate-deboog-0.2.0 (c (n "deboog") (v "0.2.0") (d (list (d (n "deboog-derive") (r "^0.1.1") (d #t) (k 0)))) (h "05x4lfr77d9jkan72wwi9s315b2kzsjf7d5g6rwxbz3zyb0rva2c")))

