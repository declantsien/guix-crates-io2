(define-module (crates-io de bo debounce) #:use-module (crates-io))

(define-public crate-debounce-0.1.0 (c (n "debounce") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0m1a9wh3nz6zjy915w3drkdr7c8zgyxy3nl3b4mckhm0ra3x73sd")))

(define-public crate-debounce-0.1.1 (c (n "debounce") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0bjx385g91a40bkb22wj62wmr3j5ndql0psjhhzmy76hqnfrk7hq")))

(define-public crate-debounce-0.2.0 (c (n "debounce") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0qdfakgijn09xdf27csigsd58jg7qqvlpdm3rrzi12b29bwrscfn")))

(define-public crate-debounce-0.2.1 (c (n "debounce") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0z1papc8z5vj0w10ppx3ffd6x8s5flmcsx0v7z4crslxl6bhnhvc")))

(define-public crate-debounce-0.2.2 (c (n "debounce") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0d5ddqh1gm40lvrcxn7lk4p4xdgavkv5yk1z6fdqxgc2bv4mnbkz")))

