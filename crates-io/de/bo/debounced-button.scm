(define-module (crates-io de bo debounced-button) #:use-module (crates-io))

(define-public crate-debounced-button-0.1.0 (c (n "debounced-button") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0v1kh5vij8h14gy0bq9kxal3qcrcvzwqvpshbhnmwijnhb06jfq2")))

(define-public crate-debounced-button-0.2.0 (c (n "debounced-button") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "09na1f9842j0fghcvsr91wv663285vz5m7h8iqd125v0ai4mp398")))

(define-public crate-debounced-button-0.3.0 (c (n "debounced-button") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0vs5rzignfq8xgmgjg0zj354swbh3cbhzs1129g9f3j0bg4ajil5")))

(define-public crate-debounced-button-0.4.0 (c (n "debounced-button") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1537s0m8ifh7ks5whjriqyhxzzihjz2vm9xbyi92iszv67aiqs65")))

(define-public crate-debounced-button-0.4.1 (c (n "debounced-button") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1r33sjahsj5677rm88vb5kc4lffb40smaxb7ina2y0w4d10yazwk")))

