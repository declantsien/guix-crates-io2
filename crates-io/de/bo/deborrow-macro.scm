(define-module (crates-io de bo deborrow-macro) #:use-module (crates-io))

(define-public crate-deborrow-macro-0.1.0 (c (n "deborrow-macro") (v "0.1.0") (h "0qbqg295hf5ja1jk424vmgck5d2iwz1yz42z571h21b8zwv6p8i6")))

(define-public crate-deborrow-macro-0.1.1 (c (n "deborrow-macro") (v "0.1.1") (h "0f3wg2zg5cxxsf8cclgfnkagnjx4z3i3mysv6301ly8ml8n5d01h")))

(define-public crate-deborrow-macro-0.1.2 (c (n "deborrow-macro") (v "0.1.2") (h "187zxpma6017gzs65n4a74hgsqh8xc7jjmnhhw28z0c04ngz0qjq")))

(define-public crate-deborrow-macro-0.1.3 (c (n "deborrow-macro") (v "0.1.3") (h "1j5hdqsvm0a1vcckjlmrxdpyvml6flasr2b6fi0wc3zliz0slf31")))

(define-public crate-deborrow-macro-0.2.0 (c (n "deborrow-macro") (v "0.2.0") (h "0j5rwrdxxn6hycpia34mi7rfbf69skgpyh6j615rda0vhz0166jm")))

