(define-module (crates-io de bo debox-open-sdk) #:use-module (crates-io))

(define-public crate-debox-open-sdk-0.1.0 (c (n "debox-open-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ksql0y7didpvp087r4nbm2mdmydp7c61wi779cp509cyr7a8pkz")))

(define-public crate-debox-open-sdk-0.2.0 (c (n "debox-open-sdk") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s75s49r1g968ax8xkacpgcyq3r9pfgachbwz0n51k55ah2177rp")))

(define-public crate-debox-open-sdk-0.3.0 (c (n "debox-open-sdk") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bqbcd7wyrv65kif8rwbrcgm67vl79kffl84vi27x74gvkcyi0l4")))

(define-public crate-debox-open-sdk-0.4.0 (c (n "debox-open-sdk") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nymk9d821947xc1brkmssi8q6l9yzqkj6xsaisngm1xqshia830")))

(define-public crate-debox-open-sdk-0.5.0 (c (n "debox-open-sdk") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v356jii6rn9py4gzmhl6ldx6sc8izyjvb71r5jk45vrlpyzzgbm")))

