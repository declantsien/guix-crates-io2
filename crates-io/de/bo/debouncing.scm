(define-module (crates-io de bo debouncing) #:use-module (crates-io))

(define-public crate-debouncing-0.1.0 (c (n "debouncing") (v "0.1.0") (d (list (d (n "no-std-compat") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1cvmk22zb1j1wdnz3i3ns2ahlay65p7dja3ca4pkqfai5ncpkw27")))

(define-public crate-debouncing-0.2.0 (c (n "debouncing") (v "0.2.0") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0g4nincnvahj8v4l5pibqhqlqngrmwvwadv081myphxp5kmr0ksd")))

