(define-module (crates-io de fa default-option-arr) #:use-module (crates-io))

(define-public crate-default-option-arr-0.1.0 (c (n "default-option-arr") (v "0.1.0") (h "1dv0c9slcykq0liq0s136qvhbiz13a1azgqqw38s3jcpmaxdpgah")))

(define-public crate-default-option-arr-0.1.1 (c (n "default-option-arr") (v "0.1.1") (h "1pqf2swq10dqfvvli7ii1jccxccvjcnm3jd7wqi6ldq6ggrszkld")))

