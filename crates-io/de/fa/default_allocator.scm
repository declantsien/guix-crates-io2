(define-module (crates-io de fa default_allocator) #:use-module (crates-io))

(define-public crate-default_allocator-0.1.0 (c (n "default_allocator") (v "0.1.0") (d (list (d (n "loca") (r "^0.4") (d #t) (k 0)))) (h "12zxxz953kbmiagq0bw71r1jh1xl0x3m51plbhbp4zzl8y2y2366")))

(define-public crate-default_allocator-0.1.1 (c (n "default_allocator") (v "0.1.1") (d (list (d (n "loca") (r ">= 0.4, < 0.6") (d #t) (k 0)))) (h "1rlxcqj8vw7gah6c4lqcv2w8fja4x4ibdljzsviz0ffnbs8vania")))

(define-public crate-default_allocator-0.1.2 (c (n "default_allocator") (v "0.1.2") (d (list (d (n "loca") (r "^0.5.4") (d #t) (k 0)))) (h "003mpzh8ni3lg8vpkmfpsbbcqcgmsh1dq5g905bx7c2i3amdhmrd") (f (quote (("stable-rust" "loca/stable-rust"))))))

(define-public crate-default_allocator-0.2.0 (c (n "default_allocator") (v "0.2.0") (d (list (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "10sr22ffs5hsn21iqi52p5sb61zq5v97ac8p4vvxnr83hgf4al8a")))

(define-public crate-default_allocator-0.2.1 (c (n "default_allocator") (v "0.2.1") (d (list (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1n2iy42hdi9vi3v9svmfj60plsirxrdszkcxzs6981051ydd6j75")))

(define-public crate-default_allocator-0.2.2 (c (n "default_allocator") (v "0.2.2") (d (list (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1v3r3mdj9zc5y4sik6ppkh34553g6s8613p070n0prm6x20p3wwa")))

(define-public crate-default_allocator-0.2.3 (c (n "default_allocator") (v "0.2.3") (d (list (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1xwv6idw6yhhr0w2v9469k60w5sf09gbg7ici0800253xbh8wqnx")))

(define-public crate-default_allocator-0.3.0 (c (n "default_allocator") (v "0.3.0") (d (list (d (n "loca") (r "^0.7") (d #t) (k 0)))) (h "08pkq47aq9h2y1b005cl5zl9mss8yb3c7r1siarvw9f5nm6ra20h")))

