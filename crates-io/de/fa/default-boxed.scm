(define-module (crates-io de fa default-boxed) #:use-module (crates-io))

(define-public crate-default-boxed-0.1.0 (c (n "default-boxed") (v "0.1.0") (d (list (d (n "default-boxed-derive") (r "^0.1.0") (d #t) (k 0)))) (h "01qjnzncfs603a77iy2qfdwjgkx551z5v3d28a43fpsnvd9c1nly")))

(define-public crate-default-boxed-0.1.1 (c (n "default-boxed") (v "0.1.1") (d (list (d (n "default-boxed-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0h0lfwvwxhwdcsbilns9cpiig60v7cilhk5hmh8npz9a3n5wcfiy")))

(define-public crate-default-boxed-0.1.2 (c (n "default-boxed") (v "0.1.2") (d (list (d (n "default-boxed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0wsf32z17lmfzims3m80xs0hfjybwxz78drp19zs7fzxxdj6wgk3")))

(define-public crate-default-boxed-0.1.3 (c (n "default-boxed") (v "0.1.3") (d (list (d (n "default-boxed-derive") (r "^0.1.3") (d #t) (k 0)))) (h "10czzb8hgp1f7j8mm534gp7fmf6c05jp963yf0idr3fxb77hafds")))

(define-public crate-default-boxed-0.1.4 (c (n "default-boxed") (v "0.1.4") (d (list (d (n "default-boxed-derive") (r "^0.1.3") (d #t) (k 0)))) (h "0fj1miscp54smnlw4a76sf6phyb1kpwh06c0k7lw07j9g7r0ifxa")))

(define-public crate-default-boxed-0.1.5 (c (n "default-boxed") (v "0.1.5") (d (list (d (n "default-boxed-derive") (r "^0.1.4") (d #t) (k 0)))) (h "00zcf9hb1nzf6vqyrvqlr0awni6sznq24h0a2kksx4fyz9r3rgpn")))

(define-public crate-default-boxed-0.1.6 (c (n "default-boxed") (v "0.1.6") (d (list (d (n "default-boxed-derive") (r "^0.1.4") (d #t) (k 0)))) (h "1iq0ckin49zqrnlqmj2r9ww93q284vxc86hl827vqqwzfgdvigzp")))

(define-public crate-default-boxed-0.1.7 (c (n "default-boxed") (v "0.1.7") (d (list (d (n "default-boxed-derive") (r "^0.1.4") (d #t) (k 0)))) (h "1kl5h2ya4yz479fbkdwyqy63gsns1fkqfbi34zzw3j5rr4vbvdmb")))

(define-public crate-default-boxed-0.1.8 (c (n "default-boxed") (v "0.1.8") (d (list (d (n "default-boxed-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0r1rv464rxvlxc07054qrk5rqdyyv3j413cy6bv4xahzf3qckwgq")))

(define-public crate-default-boxed-0.1.9 (c (n "default-boxed") (v "0.1.9") (d (list (d (n "default-boxed-derive") (r "^0.1.5") (d #t) (k 0)))) (h "07nwqp703ii17awxldg86q3c6kkw11d1f44b5lqmq58i1y9aj9c0")))

(define-public crate-default-boxed-0.1.10 (c (n "default-boxed") (v "0.1.10") (d (list (d (n "default-boxed-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.28") (d #t) (k 2)))) (h "1qqlfwvdfi8crlqdyvhiawp54zvmpvgysrrmk30rj4b6x6fzy8md")))

(define-public crate-default-boxed-0.1.11 (c (n "default-boxed") (v "0.1.11") (d (list (d (n "default-boxed-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.28") (d #t) (t "cfg(not(miri))") (k 2)))) (h "1w2npp4wqn4mc8g8yx1vw783klidl08qhrx1w4pqm4jpziaiyilc")))

(define-public crate-default-boxed-0.2.0 (c (n "default-boxed") (v "0.2.0") (d (list (d (n "default-boxed-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.28") (d #t) (t "cfg(not(miri))") (k 2)))) (h "0c8wgbjs3m6iwf3fnzd6axg7qnabixgy89pbwp78mk9v1g5wg35l")))

