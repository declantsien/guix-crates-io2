(define-module (crates-io de fa default-struct-builder) #:use-module (crates-io))

(define-public crate-default-struct-builder-0.1.0 (c (n "default-struct-builder") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03s6dw502p9x22bhgzvz3p1ic4khl6b4cd8bm115wjz0m6pzksjn")))

(define-public crate-default-struct-builder-0.2.0 (c (n "default-struct-builder") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1vahh6c514b304dr63mzzbm5sjikd9mjx72byvbgbnqpllfbmhm2")))

(define-public crate-default-struct-builder-0.2.1 (c (n "default-struct-builder") (v "0.2.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1p09si6skqqnvbr1h2h2s23lwn5naggyy627ama4svlrfjqb88aw")))

(define-public crate-default-struct-builder-0.3.0 (c (n "default-struct-builder") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11j9w98camx0s86ypjrf5lr6x2zjlgvcwf6k7isg11nvjmz06945")))

(define-public crate-default-struct-builder-0.4.0 (c (n "default-struct-builder") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ikz7gmccxhl6ib21jgzp9z027asmaihkvd1agh38jpm3yx0yi45")))

(define-public crate-default-struct-builder-0.4.1 (c (n "default-struct-builder") (v "0.4.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1i9jfia5s75c812iw7bmmgy62q2a1s4m8byhycgy3dkspsddq47f")))

(define-public crate-default-struct-builder-0.4.2 (c (n "default-struct-builder") (v "0.4.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jlvdsbqdww031mwncjr1szammqlwqywazhjhwjcyqdwkgj5v4sd")))

(define-public crate-default-struct-builder-0.5.0 (c (n "default-struct-builder") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "055ccqvf9811r0ry635anzij2fgp66kzglalawglkzdqjvd91ypq")))

