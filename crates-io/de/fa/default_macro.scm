(define-module (crates-io de fa default_macro) #:use-module (crates-io))

(define-public crate-default_macro-0.1.0 (c (n "default_macro") (v "0.1.0") (h "0val1g74s1ixiryry4a5bcc4jlixsdk700f4n5f8z5c301f2js70")))

(define-public crate-default_macro-0.2.0 (c (n "default_macro") (v "0.2.0") (h "0f3i2v6bi0g67xwfzrnkgbln1kg8x4273xzrjmchwm8ixjmb6q96")))

(define-public crate-default_macro-0.2.1 (c (n "default_macro") (v "0.2.1") (h "0pj2zy70pdn1yafs3vs6jsggnnxg0khn23i6jxj5zi591mv2mxvm")))

