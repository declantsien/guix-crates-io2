(define-module (crates-io de fa default-test) #:use-module (crates-io))

(define-public crate-default-test-0.1.0 (c (n "default-test") (v "0.1.0") (h "08zwqh5j81x9s8h5z2smcnqh7pkvj89hgy5lc0dyz88hj6lqw21y")))

(define-public crate-default-test-0.1.1 (c (n "default-test") (v "0.1.1") (h "1w6mknv9ykfsr1sg9g30y01rqhskg822az35wcmri9rlx8fmlzs6")))

