(define-module (crates-io de fa default-env) #:use-module (crates-io))

(define-public crate-default-env-0.1.0 (c (n "default-env") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1gg4362lgp16hjizliv99yf67fra31xgbz88zikyi8vqbl91byy5")))

(define-public crate-default-env-0.1.1 (c (n "default-env") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lxzfyd26h2779bsh18fwlg8bm6grap88pk2zjgffxwjsa1fnlzp")))

