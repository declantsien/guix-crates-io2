(define-module (crates-io de fa default2) #:use-module (crates-io))

(define-public crate-default2-0.1.0 (c (n "default2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bq5f6pyscyaz0pznmjj47aqyxq28n1lmi3gd6rhcwkw4ch30nr5")))

(define-public crate-default2-0.1.1 (c (n "default2") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05i8sb5bbc5d27hjd9d6l2nw868mwfxl0z152x72waxwvb9ghsv9")))

(define-public crate-default2-0.2.0 (c (n "default2") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qay6d79lhkayw2q9yaj5d77ds071z4a21mvn9jyflsqbzxm6zz9")))

(define-public crate-default2-0.2.1 (c (n "default2") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lqcgmavv4dzdl69iwjdqabv3vn32dsx2bginnpa3n681zam1w43")))

(define-public crate-default2-0.3.0 (c (n "default2") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "10ww7961jmrnmms96r7b234519l1m3hl579713pf95f3z2p64n6l")))

(define-public crate-default2-0.3.1 (c (n "default2") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nabdwilmsa8klz2s4br8nxm7vsbszjha5xv7xdl0vzq1dl547z9")))

