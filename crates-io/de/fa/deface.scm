(define-module (crates-io de fa deface) #:use-module (crates-io))

(define-public crate-deface-0.1.0 (c (n "deface") (v "0.1.0") (h "0zlim6qjh3wn4rbw587b68wx4ln82ng66m9x6arg8kdxgkicj5rn")))

(define-public crate-deface-0.1.1 (c (n "deface") (v "0.1.1") (h "1q2ckw6jhy5yckywcyvlyin9g67qpa28ix0n3hbjsm76rbycg06n")))

(define-public crate-deface-0.1.2 (c (n "deface") (v "0.1.2") (d (list (d (n "myriad") (r "^0.1.1") (d #t) (k 0)))) (h "0a0wj7yj9lhrc46zh7ks9jrvqdydxxsav1mwb9vcyzvmpwh73n5x")))

