(define-module (crates-io de fa defaultdict) #:use-module (crates-io))

(define-public crate-defaultdict-0.1.0 (c (n "defaultdict") (v "0.1.0") (h "0vwzdydy2rjp05cyjivv8d749zqyxlwbhlwba9x7m3wqygs9cp15")))

(define-public crate-defaultdict-0.2.0 (c (n "defaultdict") (v "0.2.0") (h "1z7vazapzaxkf9pvq17bj8nh8hc6qsjk8d92sxm05kvspidk2mys")))

(define-public crate-defaultdict-0.4.0 (c (n "defaultdict") (v "0.4.0") (h "1s5h579agy66g8pl9znklraqmyw8m33snzzlzpkhm80g33bxjazr")))

(define-public crate-defaultdict-0.5.0 (c (n "defaultdict") (v "0.5.0") (h "0ydwv5cx3miari4w2bmf2zvncgs5wgzblq9q817gm9xcpi8z9wsm")))

(define-public crate-defaultdict-0.6.0 (c (n "defaultdict") (v "0.6.0") (h "0wn1m6ydhnwbh1k3glnqzp8r6p33j7y7kclwfnsi8g9kv0zvx57p")))

(define-public crate-defaultdict-0.7.0 (c (n "defaultdict") (v "0.7.0") (h "1amb5sqpdix3cxxkh1npwr98720bf2i3fbjbi06ji8dirknnrv22")))

(define-public crate-defaultdict-0.7.1 (c (n "defaultdict") (v "0.7.1") (h "1mi9saajdll5bril67dc474wkd2l2yx62l37iyf3ixyfq61jm27x")))

(define-public crate-defaultdict-0.8.0 (c (n "defaultdict") (v "0.8.0") (h "0600h1sps6ir43akf0z82dljn4f4knzwgz8xvc7zi1l1gd459m8i")))

(define-public crate-defaultdict-0.9.0 (c (n "defaultdict") (v "0.9.0") (h "1lxrh8rc0yv9wqzm78lm1cb9py34p6d0i1zzxsikhzl8g42h0n52")))

(define-public crate-defaultdict-0.10.0 (c (n "defaultdict") (v "0.10.0") (h "03cqpzcv87z6abkv8mr3jwnrxgvzaipn64q92q23m3l9z5b4q2g5")))

(define-public crate-defaultdict-0.11.0 (c (n "defaultdict") (v "0.11.0") (h "04dl4h12mdbrz36h17fcs5c4vwfxhj1sbq79h6hslxkf8ac9zmnl")))

(define-public crate-defaultdict-0.12.0 (c (n "defaultdict") (v "0.12.0") (h "1w2js06rqg2ir6smk6vnx622dchwny8ahz29nvnaymk63ypachvd")))

(define-public crate-defaultdict-0.12.1 (c (n "defaultdict") (v "0.12.1") (h "10cfs3awv7kc9l9h2sglgxvb52lnz7h5ss5lxli8hxw5ckiir3vg")))

(define-public crate-defaultdict-0.13.0 (c (n "defaultdict") (v "0.13.0") (h "15kgvvqy9lpgr82x3gaq8abb1zddc9sk5q50csgkz4irim9wvi1k")))

(define-public crate-defaultdict-0.14.0 (c (n "defaultdict") (v "0.14.0") (h "0x20kdzxgav8x1rnd5l2zv1g1vj7b1bjzkz7g7dakdfc7axlnbl8")))

(define-public crate-defaultdict-0.15.0 (c (n "defaultdict") (v "0.15.0") (h "00wig7hzq2xkh45dc8g4ivwx6h1yjfsix27hqr3s7ic4nl2x9sx2")))

(define-public crate-defaultdict-0.16.0 (c (n "defaultdict") (v "0.16.0") (h "1chc2r5mn937lnwprhgkqrdiz5m25pv2njhr9gv9ak1i4ln53kij")))

(define-public crate-defaultdict-0.16.1 (c (n "defaultdict") (v "0.16.1") (h "05qpfnyl86vczsg2yv3hs6jjqvrh0gd8nbjpjbdx7yrm8x0qr50s")))

(define-public crate-defaultdict-0.17.0 (c (n "defaultdict") (v "0.17.0") (h "1pz053fhr10y03yph3wq50fpgb8xmgqrzfi52nq7mdmaj0kpzr6x")))

(define-public crate-defaultdict-0.18.0 (c (n "defaultdict") (v "0.18.0") (h "1z8nqirpfc78qpcfgpvvznmnxd370v11hbwdws3rrc96ajsxxhz2")))

