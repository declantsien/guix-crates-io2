(define-module (crates-io de fa defaultmgr) #:use-module (crates-io))

(define-public crate-defaultmgr-0.5.0 (c (n "defaultmgr") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "13msh2qwlw5bl3z05037jjh019whiv07xl1bj02jbrq488jzvn70") (y #t)))

