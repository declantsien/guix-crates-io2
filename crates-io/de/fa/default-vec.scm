(define-module (crates-io de fa default-vec) #:use-module (crates-io))

(define-public crate-default-vec-0.1.0 (c (n "default-vec") (v "0.1.0") (h "1fs23n3mpfqmx5hlvp3m84814qr90qp060qrnhszwjzkyvq1zzvl")))

(define-public crate-default-vec-0.1.1 (c (n "default-vec") (v "0.1.1") (h "1v7wyaikp2h52j75wzi89zdj75j7sbhr5h1h585w575qk2pgin3h")))

(define-public crate-default-vec-0.2.0 (c (n "default-vec") (v "0.2.0") (h "0dsdhaqbmcc0hzd7d5mzzmvkfq68rmhxyzws0nwi7d59778wgwsd")))

