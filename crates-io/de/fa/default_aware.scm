(define-module (crates-io de fa default_aware) #:use-module (crates-io))

(define-public crate-default_aware-0.1.0 (c (n "default_aware") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l1vhvsnmzvaf999ziljkf1j0w3zzbsljss4y1kybild7v64n978") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-default_aware-0.2.0 (c (n "default_aware") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vxz3a284bysr2l67ia5m81c1yg4z34498jkc26v7b47q809g906") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

