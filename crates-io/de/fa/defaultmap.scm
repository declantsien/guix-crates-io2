(define-module (crates-io de fa defaultmap) #:use-module (crates-io))

(define-public crate-defaultmap-0.1.0 (c (n "defaultmap") (v "0.1.0") (d (list (d (n "delegatemethod") (r "^0.2.2") (d #t) (k 0)))) (h "11zfjxiip3vigrydagfbvs0w4ras0cg21g9qb5wgi3jwq5ldnyxc")))

(define-public crate-defaultmap-0.1.1 (c (n "defaultmap") (v "0.1.1") (h "1b0wpq59i6zbgbhkijfjhr5vsdjnmpx2ajnvianzngmk56201bb5")))

(define-public crate-defaultmap-0.1.2 (c (n "defaultmap") (v "0.1.2") (h "1djrlqpz5msk1adwsw3ygdpbn04l9169a0wymw0kn3bmdiby7mf6")))

(define-public crate-defaultmap-0.1.3 (c (n "defaultmap") (v "0.1.3") (h "0f8x5x3rvk0q1jpipa5adck2my94qwxqwhyx200qn1pg3lrp7w4j")))

(define-public crate-defaultmap-0.2.0 (c (n "defaultmap") (v "0.2.0") (h "0d8d6v4rkq7isdkga4kyzxknhspdzy9r782fyxi34nxh9qifarmw")))

(define-public crate-defaultmap-0.2.1 (c (n "defaultmap") (v "0.2.1") (h "0c1f95d70z5vfbrmjzmv3hr4sg16sz5c5gikypxaz21wmafycylk")))

(define-public crate-defaultmap-0.2.2 (c (n "defaultmap") (v "0.2.2") (h "0nzrm6kalka3fbh17xlmipayc08sdhwcjh9yx1xydzalhawvacpd") (y #t)))

(define-public crate-defaultmap-0.3.0 (c (n "defaultmap") (v "0.3.0") (h "0mssibgy02qacz6cixzrchk9g1y1x7qlwxjy5saqzd3938b3126k")))

(define-public crate-defaultmap-0.3.1 (c (n "defaultmap") (v "0.3.1") (h "1cja4x6bklhl2axj51yvpkh72ffq0d8ksbvdgwrb5704lfjlrjj2")))

(define-public crate-defaultmap-0.4.0 (c (n "defaultmap") (v "0.4.0") (h "016qig0paa5swmfrkqz7anjc95pniay2czwafp32a8h89fhs25wg")))

(define-public crate-defaultmap-0.5.0 (c (n "defaultmap") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d78s14v64w0ly20j6zb2nkharc24jq9gcl6zync6qb5svxx9i2i") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-defaultmap-0.6.0 (c (n "defaultmap") (v "0.6.0") (d (list (d (n "derive_more") (r "=1.0.0-beta.3") (f (quote ("debug"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "1hzp5cdn170gnmaxvq3rfx25jnqy5hcf72w9alpq99kg7rjggy8m") (f (quote (("default")))) (s 2) (e (quote (("with-serde" "dep:serde")))) (r "1.71.0")))

