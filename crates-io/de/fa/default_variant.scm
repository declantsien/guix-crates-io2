(define-module (crates-io de fa default_variant) #:use-module (crates-io))

(define-public crate-default_variant-0.1.0 (c (n "default_variant") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v5884x5zygjsphl6xv1mqh5dimgxky58z5kh2sf81cki4v0ks22")))

