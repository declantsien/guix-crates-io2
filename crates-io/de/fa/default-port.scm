(define-module (crates-io de fa default-port) #:use-module (crates-io))

(define-public crate-default-port-1.0.0 (c (n "default-port") (v "1.0.0") (h "07s1xnyimrx0b7di70ji8s946xyiv7l16934vw0jkjgqy3j553av")))

(define-public crate-default-port-2.0.0 (c (n "default-port") (v "2.0.0") (h "0673aqcp2j4jfk3j9lhk6i0ikllf47j8ycd4pihn8wrjbwb5ikv1")))

(define-public crate-default-port-2.1.0 (c (n "default-port") (v "2.1.0") (h "08ap8hgismhm03vcp25wqxkai3in8zicwn4fbjd6cqd6dbzi840h")))

