(define-module (crates-io de fa default) #:use-module (crates-io))

(define-public crate-default-0.1.0 (c (n "default") (v "0.1.0") (h "1gi6ysz3yjzdlarsngc10d56zbxm56yy4lnvaj0dlk4fy6qzsdg8")))

(define-public crate-default-0.1.1 (c (n "default") (v "0.1.1") (h "0fa648w6n5i9naqpy21nimcdbxggxzg0kr09bwwglmxybh5qkbq4")))

(define-public crate-default-0.1.2 (c (n "default") (v "0.1.2") (h "19xs7nxy23knkjpfyypcbagjbknxfjz42mi14pw2gnwl7kg56f7k")))

