(define-module (crates-io de fa defaults) #:use-module (crates-io))

(define-public crate-defaults-0.1.0 (c (n "defaults") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11sd26bpqvkpmzlqlp50zvw9574fw2qmy83hgvsbffqfz919rccr")))

(define-public crate-defaults-0.2.0 (c (n "defaults") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rx2kddbkaf0p0is36ggail0zklb20cqlm9sn8na46prchymbsa0")))

