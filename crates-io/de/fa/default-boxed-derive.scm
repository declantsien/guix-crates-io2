(define-module (crates-io de fa default-boxed-derive) #:use-module (crates-io))

(define-public crate-default-boxed-derive-0.1.0 (c (n "default-boxed-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1rqj0f0xzdny4vpkf6ai0i6iwf7sdv2h9a1j5ybii6kyinwdil19")))

(define-public crate-default-boxed-derive-0.1.1 (c (n "default-boxed-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0hfi1by8fmxcsr7lgv97287q3pr2z1dpillywi9gbc5k2g24h72w")))

(define-public crate-default-boxed-derive-0.1.2 (c (n "default-boxed-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1bsawlnv1j48n53aq72k839mlz0svb63kczzr0jqj8k0xnygswsy")))

(define-public crate-default-boxed-derive-0.1.3 (c (n "default-boxed-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0az6rbpgd0pcqy0k61whr5ik43mhwzxba76h1binrl659rh79b8r")))

(define-public crate-default-boxed-derive-0.1.4 (c (n "default-boxed-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0x55gmn8vfix0hv69cx6i4s2kh5z1gz5qncrjv7snsm46ifpq0f0")))

(define-public crate-default-boxed-derive-0.1.5 (c (n "default-boxed-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0nrhqs4c38h2d62x8kjfcf3w0fafljac3swd3ni35j4if28mnldx")))

(define-public crate-default-boxed-derive-0.1.6 (c (n "default-boxed-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "parsing" "printing"))) (d #t) (k 0)))) (h "0axpynj94z9vzcn0d6sfyyy0ix0lkgbzwwwc7nsypbvhxwh6pcii")))

(define-public crate-default-boxed-derive-0.1.7 (c (n "default-boxed-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "parsing" "printing"))) (d #t) (k 0)))) (h "1myilxjblz0av9gkhpmrkv06hr10i98qn4jndk7k8ydw362spar3")))

(define-public crate-default-boxed-derive-0.2.0 (c (n "default-boxed-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "parsing" "printing"))) (d #t) (k 0)))) (h "0phg7aiqzrfi5jpj4bxf3s0fwzapnmz3flz0y0x7wvwrg2hddfwl")))

