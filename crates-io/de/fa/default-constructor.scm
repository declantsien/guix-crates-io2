(define-module (crates-io de fa default-constructor) #:use-module (crates-io))

(define-public crate-default-constructor-0.1.0 (c (n "default-constructor") (v "0.1.0") (h "0r83g1mkzn7gbazqpjprghfr953lkyjzixcwkmil4xs5sdy7j6mh")))

(define-public crate-default-constructor-0.1.1 (c (n "default-constructor") (v "0.1.1") (h "1f7cdxd5ffj2kc7n1px4i9sq8w4kcixapyn903fp9vhac8rkwm42")))

(define-public crate-default-constructor-0.1.2 (c (n "default-constructor") (v "0.1.2") (h "1y578gql5z06347asj89m97gmzjbval3ym3zaap6xwcfkrsl4plq")))

(define-public crate-default-constructor-0.1.3 (c (n "default-constructor") (v "0.1.3") (h "1mszz00dnhvh5wh77dyacpjiyn8cfrg4x2ccqi9fdfwfav0f4qjl")))

(define-public crate-default-constructor-0.2.0 (c (n "default-constructor") (v "0.2.0") (h "01vlc5dnwvgll6c2y21nn0ngwhilkq7i16y3wyizwrdnmhcm5w7m") (f (quote (("std") ("default" "std"))))))

(define-public crate-default-constructor-0.2.1 (c (n "default-constructor") (v "0.2.1") (h "1x8x5xwdw1dghwjmdfvgblnd6r7048i5hnxpq924z0p9wz8qizis") (f (quote (("std") ("default" "std"))))))

