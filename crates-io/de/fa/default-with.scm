(define-module (crates-io de fa default-with) #:use-module (crates-io))

(define-public crate-default-with-0.1.0 (c (n "default-with") (v "0.1.0") (h "1kvlrxdygsjklvx5d218p5z44kqv2qi2w4qz3awvj0s16x6qas8a")))

(define-public crate-default-with-0.1.1 (c (n "default-with") (v "0.1.1") (h "1vnrbjln7ahnd6frhjpamhgn95ydcpq0gwm10g8wbfa4wbnqn865")))

