(define-module (crates-io de fa default-from-serde) #:use-module (crates-io))

(define-public crate-default-from-serde-0.1.0 (c (n "default-from-serde") (v "0.1.0") (h "05b85qcsg2p1rs8icahrmsaqnp94cnnbnzx68s6328cwfyyrphi1")))

(define-public crate-default-from-serde-0.1.1 (c (n "default-from-serde") (v "0.1.1") (d (list (d (n "derive-default-from-serde") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 2)))) (h "0fx4ir7qy81m62c7850ri5hmald4k28jmg1s42np21daj0mg08hh") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-default-from-serde-0.1.2 (c (n "default-from-serde") (v "0.1.2") (d (list (d (n "derive-default-from-serde") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 2)))) (h "1mjndz2fhnxsdm3r14ai3ijliil9pg68gvc1x40gqx3gdhpxy951") (f (quote (("std" "serde/std") ("default" "std"))))))

