(define-module (crates-io de fa default-args) #:use-module (crates-io))

(define-public crate-default-args-1.0.0-beta (c (n "default-args") (v "1.0.0-beta") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15gp2dnp5n3lm6pamv3hnn0x9pzi8hdh6jph15jjhppqjcjjnsq8")))

(define-public crate-default-args-1.0.0 (c (n "default-args") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1sf55164sa87s9f0jkj4g7qka8gv2s4l1hy4rlkw8iw8m3spichk")))

