(define-module (crates-io de fa default_kwargs) #:use-module (crates-io))

(define-public crate-default_kwargs-0.1.0 (c (n "default_kwargs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.58") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.58") (f (quote ("diff"))) (d #t) (k 2)))) (h "06ri89n98z6g3g8qxbjmbm889ym1i9cq725bgyarbk6fjkzcy175")))

