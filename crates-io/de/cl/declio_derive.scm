(define-module (crates-io de cl declio_derive) #:use-module (crates-io))

(define-public crate-declio_derive-0.1.0 (c (n "declio_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nwwidap0vgnp7c073n8hwh65z4rc6hv3s64q8yp8y89xh87sqzc")))

(define-public crate-declio_derive-0.2.0 (c (n "declio_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lq03jvaxki1pwqm01gzfafpi09lhal1qx3csn72klchi4089cjy")))

