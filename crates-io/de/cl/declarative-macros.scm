(define-module (crates-io de cl declarative-macros) #:use-module (crates-io))

(define-public crate-declarative-macros-0.1.0 (c (n "declarative-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1bf9rk9ssll58lfx9jziaq81i3kyngdhyrn96qxwv11a20r38l4i") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.2.0 (c (n "declarative-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1fdkngkdqpjry1m6g13zyxs3d42ar7yg8dp8acp05dsky2a9s47x") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.3.0 (c (n "declarative-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07019m2vjciv4gcqlwqi8a0y3bp1w2na821il6c5y7xbgzx3l9cs") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.3.1 (c (n "declarative-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1s3srnpc6rgcwgimvhncnzxk35y8wv6ss4mv8fq47v86cyki8rqv") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.3.2 (c (n "declarative-macros") (v "0.3.2") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ky382xg7p6bak51ps7ajjhb5bssfb1vb10hfgif2cs28p2n8413") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.4.0 (c (n "declarative-macros") (v "0.4.0") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1vfabyq401zzx8g77dfw8p47l63fym3lj7gjm2js0623r99k25sz") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.4.1 (c (n "declarative-macros") (v "0.4.1") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0z8sqxq2df88xzadqf81hfls986rgyh3bkiib7fsmmkk81cnsga3") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.5.0 (c (n "declarative-macros") (v "0.5.0") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1lb2cyhxa7067qwqmvsh369iv8bxiq6ygf5nnd8g3h0k6p1dq611") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.5.1 (c (n "declarative-macros") (v "0.5.1") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "068d635hm75x2j9p8iibm5y6m3s9112mgs25s2s700fwnv33i1xp") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.5.2 (c (n "declarative-macros") (v "0.5.2") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12mjmmvvb46s7j7zj36mblqzcgmmwfqjxb18x6zbw88vy05mvjbi") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.5.3 (c (n "declarative-macros") (v "0.5.3") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1cda542hd7fbicykhy91r5ib471mggscrggk5japcq2ig2iv3lq5") (f (quote (("builder-mode"))))))

(define-public crate-declarative-macros-0.6.0 (c (n "declarative-macros") (v "0.6.0") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10gq9wblxank71whccscy0rbh9qq5bsy0xvybxna4qhrvlkiblnk")))

(define-public crate-declarative-macros-0.7.0 (c (n "declarative-macros") (v "0.7.0") (d (list (d (n "compact_str") (r "^0.7") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1b425i1bl4piwvxp8l837zd0zhw9a2h0kjykkaq5phsznbzngpbg")))

