(define-module (crates-io de cl declaration_site) #:use-module (crates-io))

(define-public crate-declaration_site-0.1.0 (c (n "declaration_site") (v "0.1.0") (d (list (d (n "findshlibs") (r "^0.10.2") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.7.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.7.0") (f (quote ("rust"))) (k 0)))) (h "10hznyvp41i2ggr0gcyzj09hhznd4s0flckmm11gvn5fk3v6588b")))

(define-public crate-declaration_site-0.2.0 (c (n "declaration_site") (v "0.2.0") (d (list (d (n "findshlibs") (r "^0.10.2") (d #t) (k 0)) (d (n "goblin") (r "^0.5.1") (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.7.0") (d #t) (k 0)) (d (n "symbolic-debuginfo") (r "^8.7.0") (f (quote ("elf" "macho" "ms" "sourcebundle" "wasm"))) (k 0)) (d (n "symbolic-demangle") (r "^8.7.0") (f (quote ("rust"))) (k 0)))) (h "03n2x3986pzl6gmrfpwz5y83ihz84bxp372i47m280vc5swjvfl8")))

