(define-module (crates-io de cl declio) #:use-module (crates-io))

(define-public crate-declio-0.1.0 (c (n "declio") (v "0.1.0") (d (list (d (n "declio_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0a735l9dqc8g1kb21jxjlyjqgnp7vn95avlra1iwi9iagzdlrm7g") (f (quote (("derive" "declio_derive") ("default" "derive"))))))

(define-public crate-declio-0.2.0 (c (n "declio") (v "0.2.0") (d (list (d (n "declio_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0r2jr2a1b390by0mpq9m4fnlvxbs9717hvmny5840c85kaq0lbdf") (f (quote (("derive" "declio_derive") ("default" "derive"))))))

