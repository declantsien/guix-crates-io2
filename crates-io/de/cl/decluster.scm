(define-module (crates-io de cl decluster) #:use-module (crates-io))

(define-public crate-decluster-1.0.0 (c (n "decluster") (v "1.0.0") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b71warvcbyf51kd6ad85llg0crpslzshnjza1j6k93wdiwc9c78")))

(define-public crate-decluster-1.0.1 (c (n "decluster") (v "1.0.1") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y8h9jvwpqcda52629mwlk57s9zsn0kz56kibw36q3nppw2s2yyl")))

(define-public crate-decluster-1.0.2 (c (n "decluster") (v "1.0.2") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n1gihfrlhsim5rb62b64r5pmk692rmyj478rsy9zzh5i1rwqbv9")))

(define-public crate-decluster-1.0.3 (c (n "decluster") (v "1.0.3") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vlm4yj03y9fhcw09nlw939v7d1ji9i25fdaji78n64z8bfd4qgx")))

(define-public crate-decluster-1.0.4 (c (n "decluster") (v "1.0.4") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1281kjv3hfz7bv12bb5axfwhq142x3y3wks6fk83il90w907jz52")))

(define-public crate-decluster-1.0.5 (c (n "decluster") (v "1.0.5") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ivrak6iy7szq3h2328jl9d3mrs29r3nrdzwxjk0wkm483g00vmx")))

(define-public crate-decluster-1.0.6 (c (n "decluster") (v "1.0.6") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dxwlbxmhka5158hs4qzr2fcagvja50k28x842w0yrm3x3h2nj32")))

(define-public crate-decluster-1.0.7 (c (n "decluster") (v "1.0.7") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xfw8772zlk901zxdbwpv3ynm9xcaxcj23q3lnpvqkv7b4g3jygr")))

(define-public crate-decluster-1.0.8 (c (n "decluster") (v "1.0.8") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nr5zkpqil11wsvp0ly34iav7kyqvn4lvydgmy3dv3c76z16pyci")))

(define-public crate-decluster-1.0.9 (c (n "decluster") (v "1.0.9") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wd76iwlvxkvmz0c92h5c30q33bn8aiw983x0k41glkqzigvns4a")))

(define-public crate-decluster-1.1.0 (c (n "decluster") (v "1.1.0") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jyaasx63lh28lqvxgd3zyx8mmm87aky3gjjxrdqdb0li7fq42j3") (y #t)))

(define-public crate-decluster-1.0.12 (c (n "decluster") (v "1.0.12") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rc9lf9ayfix7fnwbff5ppw16k4mhgn8n7625dnq8sanadzcr4cx")))

(define-public crate-decluster-1.0.13 (c (n "decluster") (v "1.0.13") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15v1kdliw7ixz5zsf8qwgaqkj0qjzyjx5aymklqb27d39dvj7hmk")))

(define-public crate-decluster-1.0.14 (c (n "decluster") (v "1.0.14") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zhan8crnbw1s6h0dh0g7k4lqd8v0nr6j8pwlyxq2xw1mrg3gr1y")))

(define-public crate-decluster-1.0.15 (c (n "decluster") (v "1.0.15") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mgq0gqqw6qj71ppiv4d9nsyz8y4nlmnl2cfpx1rbp0jklnm7mnd")))

(define-public crate-decluster-1.0.16 (c (n "decluster") (v "1.0.16") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m3rx4w8yj8kms3k88ca7wcndg3j97bg05iyxy1fradjx1rd20qk")))

(define-public crate-decluster-1.0.17 (c (n "decluster") (v "1.0.17") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0njlyqaqigxab05ij70pfms0y8n1av0b3g3b8g9z5ga89qi6ls2w")))

(define-public crate-decluster-1.0.18 (c (n "decluster") (v "1.0.18") (d (list (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "120sd2dqxp1djrnjc19njcm5097bj9isgahhbikjxgqpz6j8jib2")))

