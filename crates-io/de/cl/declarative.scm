(define-module (crates-io de cl declarative) #:use-module (crates-io))

(define-public crate-declarative-0.1.0 (c (n "declarative") (v "0.1.0") (d (list (d (n "declarative") (r "^0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1m1rx204cc3b3dqhfvf27z0qjb5cqfd0szwhkv5pskqy3vpclsn0") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.2.0 (c (n "declarative") (v "0.2.0") (d (list (d (n "declarative") (r "^0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1rl7pcq58ib4q5nli6kl8la55103dlf9b2g6xzgmgq7bklh1ixp4") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.3.1 (c (n "declarative") (v "0.3.1") (d (list (d (n "declarative") (r "^0.3") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1slg55agfnkq8ali74g48fks9r7sk2b7gq0icgmsib5s5fjwqnwk") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.3.2 (c (n "declarative") (v "0.3.2") (d (list (d (n "declarative") (r "^0.3") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0kc14rg26ckpdgrvx6fh26458g50c5synb67lkgqnxn4p5mw7x1d") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.4.0 (c (n "declarative") (v "0.4.0") (d (list (d (n "declarative") (r "^0.4, ^0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0r78dgcw6jgk56yqaw75xsgdzfrimfmryxqgparjcamz31gmpsbv") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.4.1 (c (n "declarative") (v "0.4.1") (d (list (d (n "declarative") (r "^0.4.1") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0") (f (quote ("v4_6"))) (k 2) (p "gtk4")) (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0jcv5lq7iksckfnf350ci88nlbwscr945dy3k90azpn5cwgs93p7") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.5.0 (c (n "declarative") (v "0.5.0") (d (list (d (n "declarative") (r "^0.5.0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.6") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "1h889gxxgda1k9zcm5vzzja84rdzk551q5pnrs1x5syf642a24ca") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.5.1 (c (n "declarative") (v "0.5.1") (d (list (d (n "declarative") (r "^0.5.1") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.6") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "0z2i6xw8la2yc1xgl8kxv3s2jgkpin61p85vv64h0ybbdd0h1c6d") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.5.2 (c (n "declarative") (v "0.5.2") (d (list (d (n "declarative") (r "^0.5.2") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.6") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "1ldgywpgrs61gal4hc07is916ldbamls4qcnr2w1v3vfqnqzkjyl") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.5.3 (c (n "declarative") (v "0.5.3") (d (list (d (n "declarative") (r "^0.5.3") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.6") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "1kp6rg4zpx7jna9x7kcikw739j64754l0ln9f9pmwid8z44f2hza") (f (quote (("gtk-rs" "builder-mode") ("builder-mode" "declarative/builder-mode"))))))

(define-public crate-declarative-0.6.0 (c (n "declarative") (v "0.6.0") (d (list (d (n "declarative") (r "^0.6.0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.7") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "01b4zy6wxhf3n4avs5n44v5fgg7phhi00pq7n76d5wvr5dm48483")))

(define-public crate-declarative-0.7.0 (c (n "declarative") (v "0.7.0") (d (list (d (n "async-channel") (r "^2") (d #t) (k 2)) (d (n "declarative") (r "^0.7.0") (d #t) (k 0) (p "declarative-macros")) (d (n "gtk") (r "^0.8") (f (quote ("v4_6"))) (k 2) (p "gtk4")))) (h "1flkpsm66js0avvzdcy6ww0dwvamxly0l7rg98sz8zj4jf5ywh07")))

