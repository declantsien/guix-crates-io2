(define-module (crates-io de cl decline-word) #:use-module (crates-io))

(define-public crate-decline-word-0.1.0 (c (n "decline-word") (v "0.1.0") (h "0sbg9g6ffvsvfl8adgi0xdjs9j799q712zapfn1wngqpmlcqamfp")))

(define-public crate-decline-word-0.1.1 (c (n "decline-word") (v "0.1.1") (h "1x7mbdyi5nz02k5x96jvcwm8g7qgsfp49c6kqjm67arv6b76kmrj")))

(define-public crate-decline-word-0.1.2 (c (n "decline-word") (v "0.1.2") (h "0j43l05m8xj2mxmhi2g9qfnjqfxpshlbdxcwc7gp90xlmqvgg6ky")))

