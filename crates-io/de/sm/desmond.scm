(define-module (crates-io de sm desmond) #:use-module (crates-io))

(define-public crate-desmond-0.1.0 (c (n "desmond") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ics") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yz7zzsl51ww30wwkdjg24d24vyn50pvplj6cql1drs7bmjc0fvi")))

