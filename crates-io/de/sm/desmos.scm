(define-module (crates-io de sm desmos) #:use-module (crates-io))

(define-public crate-desmos-0.1.0 (c (n "desmos") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.13.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1yany863sfcfrk0vn63ixdkwpkn86932jbxsd86932yx8bxy6jnq") (y #t)))

(define-public crate-desmos-0.1.1 (c (n "desmos") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^0.13.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1j6134vk28bx0khdi550vgqmylkmqf71liknq9m7zymq85h8c67h") (y #t)))

(define-public crate-desmos-0.1.2 (c (n "desmos") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "=0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0gamybs46ia0c9h9gc63kp02fx6h24aywi2az8rv93pv29d9i9na") (y #t)))

(define-public crate-desmos-0.1.3 (c (n "desmos") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xisc2fvs3ljx3iq5givgzy97cchfpaxc3zz4c593syh5q9s25rx") (y #t)))

