(define-module (crates-io de sm desmos-cw) #:use-module (crates-io))

(define-public crate-desmos-cw-0.1.4 (c (n "desmos-cw") (v "0.1.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "05i8lik4bybrjgh7j82pna1wi415qwzs5jygs1xx9vb7a6sg9w7q")))

(define-public crate-desmos-cw-0.1.5 (c (n "desmos-cw") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ljd43jk778fxjdhg5nbp8ss7ma0snyyvc7vrzsv4m8l0hz9q1nx")))

