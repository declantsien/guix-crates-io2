(define-module (crates-io de sm desmos-std) #:use-module (crates-io))

(define-public crate-desmos-std-0.1.6 (c (n "desmos-std") (v "0.1.6") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "196pyvlllynfqmiwq7mh062p76scl840q2nq7p7h35lhpd8n9v2h")))

