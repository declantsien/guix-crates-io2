(define-module (crates-io de sm desmume-rs) #:use-module (crates-io))

(define-public crate-desmume-rs-0.1.0 (c (n "desmume-rs") (v "0.1.0") (d (list (d (n "desmume-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "064yaqv1zvph6hafnhq85vc1hshycrx9y12nqzalxrnzbhglqrhr")))

(define-public crate-desmume-rs-0.1.1 (c (n "desmume-rs") (v "0.1.1") (d (list (d (n "desmume-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01fdrhykqzlcqvys0111m2qjjs88bgfkbz9dhmb52lg6kdknl5lm")))

