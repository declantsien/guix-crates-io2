(define-module (crates-io de sm desmos-mock) #:use-module (crates-io))

(define-public crate-desmos-mock-0.1.0 (c (n "desmos-mock") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (f (quote ("stargate"))) (d #t) (k 0)))) (h "1ip123v226hsrawmq4lg0bmvr7m9dzyv8c5qzvd8fik1ps6949gy")))

(define-public crate-desmos-mock-2.0.1 (c (n "desmos-mock") (v "2.0.1") (d (list (d (n "cosmwasm-std") (r "^1.2.6") (f (quote ("stargate"))) (d #t) (k 0)))) (h "0ad7sm8bxq98dl5hg1yh9ifmqr4ls988fiha7f54iqz094ljfv9a")))

(define-public crate-desmos-mock-3.0.0 (c (n "desmos-mock") (v "3.0.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("stargate"))) (d #t) (k 0)))) (h "1w73bi02f1wlf7vyvkkdzza8rwdshwkbplai7992rw0gqkz51r61")))

