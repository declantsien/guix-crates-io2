(define-module (crates-io de sm desmume-sys) #:use-module (crates-io))

(define-public crate-desmume-sys-0.0.1 (c (n "desmume-sys") (v "0.0.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 1)))) (h "1k3ac16q23k7xk7yn8p8sg3s56sm9x40hrgbbf5fx07zmnim95wl") (l "desmume")))

(define-public crate-desmume-sys-0.0.2 (c (n "desmume-sys") (v "0.0.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 1)))) (h "1qvql9dlxz2d9986kri0sh2lq71a9v9ng6h6aqcmycakpk7xa544") (l "desmume")))

