(define-module (crates-io de fr defrag-dirs) #:use-module (crates-io))

(define-public crate-defrag-dirs-0.1.0 (c (n "defrag-dirs") (v "0.1.0") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0qfmwwsiys3bhgcdswqgiq8ki4iz4pk9f91y873hmiknb7yxprqs")))

(define-public crate-defrag-dirs-0.1.1 (c (n "defrag-dirs") (v "0.1.1") (d (list (d (n "btrfs2") (r "^1.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "133vzhfhg150c6xrdvsrzkhdx6vh2w897lxx88dbj1pms3x9da0l")))

