(define-module (crates-io de fr defrag) #:use-module (crates-io))

(define-public crate-defrag-0.1.0 (c (n "defrag") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "1nhp27x044lfjhvckmfxcd53kg9pcqyv5mq7q6xffc6a2s8hy19d")))

(define-public crate-defrag-0.1.1 (c (n "defrag") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "0h7siw5didqczjg0m1fyiz6b4v3j2ppkbffi98laqijq85s71fpq")))

(define-public crate-defrag-0.1.2 (c (n "defrag") (v "0.1.2") (d (list (d (n "cbuf") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "0l17lvzbsis3w9jql4j663ir8k1nlipqcbx160sr0wscv28mc4iw") (f (quote (("no_std") ("default"))))))

(define-public crate-defrag-0.1.3 (c (n "defrag") (v "0.1.3") (d (list (d (n "cbuf") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "1839kghr54vjqcafyy45qakh41prnr1mhd7ilfxaa5674ixb5gia") (f (quote (("no_std") ("default"))))))

(define-public crate-defrag-0.1.4 (c (n "defrag") (v "0.1.4") (d (list (d (n "cbuf") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "15ypxndigqg05h0ld9r7sqml7gsrh0208aifw5z7bp4671a89rcc") (f (quote (("no_std") ("default"))))))

