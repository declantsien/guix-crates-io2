(define-module (crates-io de fr defr) #:use-module (crates-io))

(define-public crate-defr-0.1.0 (c (n "defr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10dmbidl59r2f2a2a9a3afjfakq0yxsphzhx9884dwf04mkgvd9q")))

