(define-module (crates-io de ld deldup) #:use-module (crates-io))

(define-public crate-deldup-1.0.0 (c (n "deldup") (v "1.0.0") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1sl1j4dq3yhgn0ihzjafs64ydaj13wjgf6x0km4xf9z07ad19dng")))

(define-public crate-deldup-1.0.1 (c (n "deldup") (v "1.0.1") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1vf483mzxxcsl4p024jz69zz6kvval1nkf3nkmz1ibzmpy5cys9d")))

