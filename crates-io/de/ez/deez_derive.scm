(define-module (crates-io de ez deez_derive) #:use-module (crates-io))

(define-public crate-deez_derive-0.1.0 (c (n "deez_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfny80ygmnmy44kxyyyjf5n85h3lbc12jh79isps6p5dm78qngj")))

(define-public crate-deez_derive-0.1.1 (c (n "deez_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1l319yvglhx0p2is5i124d2jggdf542rnzv8385k7i7s89idzniy")))

(define-public crate-deez_derive-0.2.0 (c (n "deez_derive") (v "0.2.0") (d (list (d (n "attribute-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1l8bh4nnchrk8lx4pa6zmwc8n0j66h2hs3rw6c5zxwyv5zkan58f")))

(define-public crate-deez_derive-0.2.1 (c (n "deez_derive") (v "0.2.1") (d (list (d (n "attribute-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "12sfnfzf7n22kc6syz3pgbi5lzfl3dy0j1iz658f6vgb190kb65p")))

(define-public crate-deez_derive-0.2.2 (c (n "deez_derive") (v "0.2.2") (d (list (d (n "attribute-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0iyp4dqj8jgik4ssphsmv25kzywa963c6clzfyf7h0ag7d7al281")))

(define-public crate-deez_derive-0.3.0 (c (n "deez_derive") (v "0.3.0") (d (list (d (n "attribute-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1w49rc0yczzj2h3zif2da4m0b217xq7z2ykjs5hmrd6yhiwk0dm3")))

