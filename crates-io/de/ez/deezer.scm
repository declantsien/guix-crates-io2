(define-module (crates-io de ez deezer) #:use-module (crates-io))

(define-public crate-deezer-0.1.0 (c (n "deezer") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 2)))) (h "13vghcw8m2ihwmisia5d95mq73llxaxcs813mglhqb96fa1ffyda")))

