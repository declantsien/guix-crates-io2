(define-module (crates-io de ba debarchive) #:use-module (crates-io))

(define-public crate-debarchive-0.1.0 (c (n "debarchive") (v "0.1.0") (d (list (d (n "ar") (r "^0.6.0") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "tar") (r "^0.4.17") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1lrsz9xxq8dpalwifrqq376w6hzsjg19k18g3fzw9w8vsbgli3wv")))

(define-public crate-debarchive-0.2.0 (c (n "debarchive") (v "0.2.0") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0nd7pjwqzg9pgp6n8ra694qd6k6w7rv7xhk64j0xcr34nrv83p86") (y #t)))

(define-public crate-debarchive-0.2.1 (c (n "debarchive") (v "0.2.1") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1zh6744rbhy071n99l2s2x46mba7gl0fahhqgbzaiw401grs3bjw")))

