(define-module (crates-io de af deaf) #:use-module (crates-io))

(define-public crate-deaf-0.1.2 (c (n "deaf") (v "0.1.2") (d (list (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0qb9idac4b0j5jsnn6r1zppjw3q87527l8b6j1fb4ihmvav36b5l")))

