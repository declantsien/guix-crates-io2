(define-module (crates-io de b8 deb822) #:use-module (crates-io))

(define-public crate-deb822-0.1.0 (c (n "deb822") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "05my83hysndwqzgh10yvz24h84kbfai039lzv1jprwkcmwn3vbhg")))

(define-public crate-deb822-0.1.1 (c (n "deb822") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0540fxia87ig05hwcl0d3gjv1mr13zirb1aizllnkvjfcvrxawqx")))

