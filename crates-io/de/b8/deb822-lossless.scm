(define-module (crates-io de b8 deb822-lossless) #:use-module (crates-io))

(define-public crate-deb822-lossless-0.1.2 (c (n "deb822-lossless") (v "0.1.2") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p87ciavbj3lvabaa2m2p9rwnfkxba9d63sva0fjsspvpbyfp0ij") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-deb822-lossless-0.1.4 (c (n "deb822-lossless") (v "0.1.4") (d (list (d (n "debversion") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "136j54c61fy3qk2riv962syw13r5pibz8rz7q1lpxmv2xqxpd4a0") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-deb822-lossless-0.1.6 (c (n "deb822-lossless") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03k0697akc2p44p3i5nxqid20s7zkpnsk9n6sr6sn9ia09fa9x0g") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-deb822-lossless-0.1.7 (c (n "deb822-lossless") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sih0i8kdaims9js3bgvj4bq411ihdhzxc5jnj5was3y8hzc0694") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-deb822-lossless-0.1.8 (c (n "deb822-lossless") (v "0.1.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rowan") (r "^0.15.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08cik71kha6glmpxzhd9krx5zh8djc2kpwaac6gbp2fvgqii9bz3") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

