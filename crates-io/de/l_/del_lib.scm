(define-module (crates-io de l_ del_lib) #:use-module (crates-io))

(define-public crate-del_lib-0.1.0 (c (n "del_lib") (v "0.1.0") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "0b6f47391w7x3p7plbshg7dl1dl8nq2xwvxa74c5ff40hfn82h8x")))

