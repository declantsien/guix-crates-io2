(define-module (crates-io de l_ del_line) #:use-module (crates-io))

(define-public crate-del_line-0.1.0 (c (n "del_line") (v "0.1.0") (h "0zy6n8ymspm9a28gdziry3lnv7z3ax8844856d3id3gsnc5aykvh")))

(define-public crate-del_line-0.1.1 (c (n "del_line") (v "0.1.1") (h "1l6vzshjm7srszld406g19za252qjp40d3n072ak9v6f2m96k5ma")))

