(define-module (crates-io de pg depgraph) #:use-module (crates-io))

(define-public crate-depgraph-0.1.0 (c (n "depgraph") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1cwry7avwk031nzb40fwr42gz8wlrjkzhniqldl2q6p2d28ywghw") (f (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2.0 (c (n "depgraph") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1xkvy478z1j5bfh6rl13crwdsbq818ngw7k1i3ngmdjcdm8smxsy") (f (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2.1 (c (n "depgraph") (v "0.2.1") (d (list (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0q9v2nrdxd0m4mrzicygvdsm9inljrc1531xk5pflm8ycp7mplyr") (f (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2.2 (c (n "depgraph") (v "0.2.2") (d (list (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "06f4nyj6rh57a878aib5a75866vkwdfhffxfa1g3g6ng929zsc7r") (f (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.3.0 (c (n "depgraph") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0vnaw0vi23bgq32si0n28ayz1f7bwpf2x2ysi79mhli5f8xl4rkd") (f (quote (("petgraph_visible") ("default"))))))

