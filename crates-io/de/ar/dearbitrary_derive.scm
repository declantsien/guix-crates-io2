(define-module (crates-io de ar dearbitrary_derive) #:use-module (crates-io))

(define-public crate-dearbitrary_derive-1.0.0 (c (n "dearbitrary_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y14c3py43p6r9wz14v673c7k4p30gzw547gwlxgnl6afjfgyd0z") (r "1.70.0")))

(define-public crate-dearbitrary_derive-1.0.1 (c (n "dearbitrary_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ib8v1dirxzqdkj10zwyavdg00jhxlvi9sxw30gvrgibqsk1bzm1") (r "1.70.0")))

