(define-module (crates-io de ar dearbitrary) #:use-module (crates-io))

(define-public crate-dearbitrary-1.0.0 (c (n "dearbitrary") (v "1.0.0") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dearbitrary_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0w8cgaibh75zh751v82cxa62hw2gdri1sf1nvjcg3745ygfz7lw7") (f (quote (("derive" "dearbitrary_derive")))) (r "1.70.0")))

(define-public crate-dearbitrary-1.0.1 (c (n "dearbitrary") (v "1.0.1") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dearbitrary_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "14kpmbwfflyc4ffhr704w2yvjsqh69fj1x6a57lrnrx7cpcjljd6") (f (quote (("derive" "dearbitrary_derive")))) (r "1.70.0")))

(define-public crate-dearbitrary-1.0.2 (c (n "dearbitrary") (v "1.0.2") (d (list (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dearbitrary_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1946g6vyz85w2k6alhqlicpamp8bivz2lv3askj5c864fygjdiw3") (f (quote (("derive" "dearbitrary_derive")))) (r "1.70.0")))

(define-public crate-dearbitrary-1.0.4 (c (n "dearbitrary") (v "1.0.4") (d (list (d (n "arbitrary") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "derive_dearbitrary") (r "^1.0.4") (o #t) (d #t) (k 0)))) (h "1m0lnc89m41iwlqgndyrbz2qri6w0drp0ylzqs2ynrb93l4yd3kh") (f (quote (("derive" "derive_dearbitrary")))) (r "1.70.0")))

