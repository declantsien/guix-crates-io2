(define-module (crates-io de ar dear) #:use-module (crates-io))

(define-public crate-dear-0.1.0 (c (n "dear") (v "0.1.0") (h "1hasnhc0xz0v17dgdpd6wb0afibhwz78zys9mg171x8x0i1a2xdi") (y #t)))

(define-public crate-dear-0.1.1 (c (n "dear") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "17j8f6p3k9v156jmmw0sqpp6bnqhdn9vgczhglsfd5z6lfx6v7f4")))

