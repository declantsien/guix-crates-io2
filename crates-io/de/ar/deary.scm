(define-module (crates-io de ar deary) #:use-module (crates-io))

(define-public crate-deary-0.0.1 (c (n "deary") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0506jq297cpii3qddkh7ydh6sb8vij3hy1yspwahkz0z8qxwp629")))

