(define-module (crates-io de ad deadbolt-parser) #:use-module (crates-io))

(define-public crate-deadbolt-parser-0.1.0 (c (n "deadbolt-parser") (v "0.1.0") (d (list (d (n "deadbolt-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0n0cgwhw925ymw7rik88kpj3kbp9df898dvvhxs1120pnla0kv60")))

(define-public crate-deadbolt-parser-0.1.1 (c (n "deadbolt-parser") (v "0.1.1") (d (list (d (n "deadbolt-crypto") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0k8ayb9j3d1prg5ivzyxnwf0a62v6rqjrv17icw2v8lnkv4nskia")))

