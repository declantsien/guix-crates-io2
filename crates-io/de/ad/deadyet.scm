(define-module (crates-io de ad deadyet) #:use-module (crates-io))

(define-public crate-deadyet-0.1.0 (c (n "deadyet") (v "0.1.0") (h "1fimfh55zccmgasnqw5nang41fg4q5nrx167hgj88vs3kiv9cl4x")))

(define-public crate-deadyet-0.2.1 (c (n "deadyet") (v "0.2.1") (d (list (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (f (quote ("handlebars_templates" "tera_templates"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i9apsnrjqzqa4002q02znd5zc9dn2ff1hf5yhmbpgc035zd6qpw")))

(define-public crate-deadyet-0.2.2 (c (n "deadyet") (v "0.2.2") (d (list (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (f (quote ("handlebars_templates" "tera_templates"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16lja70rqq6hbpwaf5gqxqa3l22nznx0xbfq9x4rbxw6scjpwpmv")))

(define-public crate-deadyet-0.3.0 (c (n "deadyet") (v "0.3.0") (d (list (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.5") (f (quote ("handlebars_templates" "tera_templates"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qlmvyrfmpz5jxlsfg0ib8nk7vp74p1q3908aapz8iikz48sn69h")))

