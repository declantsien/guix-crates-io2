(define-module (crates-io de ad deadlocker) #:use-module (crates-io))

(define-public crate-deadlocker-0.1.0 (c (n "deadlocker") (v "0.1.0") (d (list (d (n "deadlocker_derive") (r "^0") (o #t) (d #t) (k 0)) (d (n "deadlocker_derive") (r "^0") (d #t) (k 2)))) (h "0wsqwflyxgihk1qsp64yp7a9758nz8vyh8sxg5v0n0hqfv1c4jw6") (f (quote (("derive" "deadlocker_derive") ("default" "derive")))) (r "1.65")))

