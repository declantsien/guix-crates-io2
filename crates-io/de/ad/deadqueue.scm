(define-module (crates-io de ad deadqueue) #:use-module (crates-io))

(define-public crate-deadqueue-0.1.0 (c (n "deadqueue") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("sync" "macros" "rt-core"))) (d #t) (k 2)))) (h "06wv2xkgvrs4r3fc27i3wmhxg27xx0jzjqcqxmazx520ybfynhcz") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

(define-public crate-deadqueue-0.2.0 (c (n "deadqueue") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10ak4mr44fmv63pw3wxcsh9bnldfp0w547y58pm3p0809i686qrs") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

(define-public crate-deadqueue-0.2.1 (c (n "deadqueue") (v "0.2.1") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1ky8qcwcq44h6sjh413vihap8fzl07h5lqkva8a3bl0yn3244f7i") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

(define-public crate-deadqueue-0.2.2 (c (n "deadqueue") (v "0.2.2") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "073x2wjfiqlwvd2k0c43p804gavwi1ch0l5y5m1ydfkjphd50pa5") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

(define-public crate-deadqueue-0.2.3 (c (n "deadqueue") (v "0.2.3") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "13gfh393iz7hrdi69m6m80kp3j2xl8256828wr7zvs3qw4ji4p5k") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

(define-public crate-deadqueue-0.2.4 (c (n "deadqueue") (v "0.2.4") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1wspr6wcz8sp18kmhcwkw57dp6ccxgf8jnck2liidpqkscgmd8hn") (f (quote (("unlimited") ("resizable" "unlimited") ("limited") ("default" "unlimited" "resizable" "limited"))))))

