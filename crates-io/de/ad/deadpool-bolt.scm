(define-module (crates-io de ad deadpool-bolt) #:use-module (crates-io))

(define-public crate-deadpool-bolt-0.0.0 (c (n "deadpool-bolt") (v "0.0.0") (h "0nbrga2c1amk40gmdq8yyxnhq7cvipld7jck0ajag9v9wdrrn97z")))

(define-public crate-deadpool-bolt-0.1.0 (c (n "deadpool-bolt") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "bolt-client") (r "^0.10.0") (f (quote ("tokio-stream"))) (d #t) (k 0)) (d (n "deadpool") (r "^0.9.0") (f (quote ("managed" "rt_tokio_1"))) (k 0)) (d (n "futures-util") (r "^0.3.0") (f (quote ("alloc"))) (k 2)) (d (n "tokio") (r "^1.14.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6.0") (f (quote ("compat"))) (d #t) (k 0)))) (h "0q8ak9sipxmyf5d0c545s358c59al273jabs9b7vnvmcihmgfb7q")))

(define-public crate-deadpool-bolt-0.2.0 (c (n "deadpool-bolt") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "bolt-client") (r "^0.11.0") (f (quote ("tokio-stream"))) (d #t) (k 0)) (d (n "deadpool") (r "^0.9.0") (f (quote ("managed" "rt_tokio_1"))) (k 0)) (d (n "futures-util") (r "^0.3.0") (f (quote ("alloc"))) (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("compat"))) (d #t) (k 0)))) (h "1ly199c49c7x4x9sskyczgc5q4yr5b2cr0ls36crayj6l1hyhhdy")))

