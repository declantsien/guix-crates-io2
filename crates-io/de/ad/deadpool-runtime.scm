(define-module (crates-io de ad deadpool-runtime) #:use-module (crates-io))

(define-public crate-deadpool-runtime-0.1.0 (c (n "deadpool-runtime") (v "0.1.0") (d (list (d (n "async-std_1") (r "^1.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio_1") (r "^1.0") (f (quote ("sync"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "0z6xsd1f2j09n45kjwca0y36i735zpz2rig3rszxbccg976wx6hh") (y #t)))

(define-public crate-deadpool-runtime-0.1.1 (c (n "deadpool-runtime") (v "0.1.1") (d (list (d (n "async-std_1") (r "^1.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio_1") (r "^1.0") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "07kw052fkxs1mdw94hrcddzygk4ypq8a727jmv1bfgrf47fp8fpm")))

(define-public crate-deadpool-runtime-0.1.2 (c (n "deadpool-runtime") (v "0.1.2") (d (list (d (n "async-std_1") (r "^1.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio_1") (r "^1.0") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "1q91y663xxi003ybxxiprk7z93hbxxrvzg8g1733qv0gri3718za")))

(define-public crate-deadpool-runtime-0.1.3 (c (n "deadpool-runtime") (v "0.1.3") (d (list (d (n "async-std_1") (r "^1.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio_1") (r "^1.0") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "0j9zmm37z6m4fnkf40z6qwidi4z1gwk0piqzx7yz6riazrjakpv3") (r "1.63")))

(define-public crate-deadpool-runtime-0.1.4 (c (n "deadpool-runtime") (v "0.1.4") (d (list (d (n "async-std_1") (r "^1.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0) (p "async-std")) (d (n "tokio_1") (r "^1.0") (f (quote ("time" "rt"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "0arbchl5j887hcfvjy4gq38d32055s5cf7pkpmwn0lfw3ss6ca89") (r "1.75")))

