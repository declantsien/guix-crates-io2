(define-module (crates-io de ad deadline) #:use-module (crates-io))

(define-public crate-deadline-0.1.0 (c (n "deadline") (v "0.1.0") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s2gvah9yyvjh8yslr0nfl4pvn2qpmixaycjr0qpkpzrj6f9l0z2")))

(define-public crate-deadline-0.1.1 (c (n "deadline") (v "0.1.1") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kxd6wv2qywamy86fv15hgpfkpi7fj7xm46plr4f603sxqa4k6mf")))

(define-public crate-deadline-0.2.0 (c (n "deadline") (v "0.2.0") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)))) (h "13yk2vmpk80dgh7gfw8rxm1m6h32zj9bib7b19a17vx34glqmnx7")))

