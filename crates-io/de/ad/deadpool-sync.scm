(define-module (crates-io de ad deadpool-sync) #:use-module (crates-io))

(define-public crate-deadpool-sync-0.1.0 (c (n "deadpool-sync") (v "0.1.0") (d (list (d (n "deadpool") (r "^0.9.0") (d #t) (k 0)))) (h "0756321g58761za9sw9nar1lvqzdn5wh23vfvrzm6cjbnr2a7gmi")))

(define-public crate-deadpool-sync-0.1.1 (c (n "deadpool-sync") (v "0.1.1") (d (list (d (n "deadpool-runtime") (r "^0.1.2") (d #t) (k 0)))) (h "1sxly93k2xmdd76ih9b88qzw4621kh6yyqglfgljwrhbn5hlssbh") (y #t)))

(define-public crate-deadpool-sync-0.1.2 (c (n "deadpool-sync") (v "0.1.2") (d (list (d (n "deadpool-runtime") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02habnbf9gna5yg5parpfbgz1342mzyxmd5lcz7f9jhk9i4p1nzq") (r "1.63")))

(define-public crate-deadpool-sync-0.1.3 (c (n "deadpool-sync") (v "0.1.3") (d (list (d (n "deadpool-runtime") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1s50gz56m8rhb7p8vw8jpgikwjx0v7x407hw3sjfvqyv52n17hic") (r "1.75")))

