(define-module (crates-io de ad deadnews-template-rust) #:use-module (crates-io))

(define-public crate-deadnews-template-rust-0.1.0 (c (n "deadnews-template-rust") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "05s1m22q66011rmlvywn21idf27xip2bhr37p5s9lj61wnnm092f")))

(define-public crate-deadnews-template-rust-0.1.1 (c (n "deadnews-template-rust") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hv592krn2ghvxhmnh17gcckjhs5jgm5jfaafgikxrri0bldmhr9")))

(define-public crate-deadnews-template-rust-0.1.2 (c (n "deadnews-template-rust") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0lrh18dyy6npk1qvmxh7slcj4w4szyax30zi54x840ahc6kbdsax")))

(define-public crate-deadnews-template-rust-0.1.4 (c (n "deadnews-template-rust") (v "0.1.4") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "10z98qslp67fscays5b5bkrbmgmaznz40ynkkx40i7m3grgv09pl")))

