(define-module (crates-io de ad deadlocker_derive) #:use-module (crates-io))

(define-public crate-deadlocker_derive-0.1.0 (c (n "deadlocker_derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10y4jikr96sd9c2lzr42ypmalbyrdaiq3kmshkp5iib6x7x005jn") (r "1.65")))

