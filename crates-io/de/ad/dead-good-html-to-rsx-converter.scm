(define-module (crates-io de ad dead-good-html-to-rsx-converter) #:use-module (crates-io))

(define-public crate-dead-good-html-to-rsx-converter-0.1.0 (c (n "dead-good-html-to-rsx-converter") (v "0.1.0") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1q4778sdl6gzj38282lpclaadhy54zd8v6vs9yfdkp8kzmqfa7m2")))

(define-public crate-dead-good-html-to-rsx-converter-0.1.1 (c (n "dead-good-html-to-rsx-converter") (v "0.1.1") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "03fpc7g9nw9p8bs0x0js271fa5h7m66l0d4lrasl3yxfsxm8zl88")))

(define-public crate-dead-good-html-to-rsx-converter-0.2.0 (c (n "dead-good-html-to-rsx-converter") (v "0.2.0") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1ys58vn45rbjgyq56nw1s19rxn3825s7n6zl0f6zfsyxfqdd94x2")))

(define-public crate-dead-good-html-to-rsx-converter-0.3.0 (c (n "dead-good-html-to-rsx-converter") (v "0.3.0") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1mg0rq852pd6a2c07m5cfg5hk6x45rf8yj0j448y3m2wmpajqqrg")))

