(define-module (crates-io de ad deadman) #:use-module (crates-io))

(define-public crate-deadman-0.1.0 (c (n "deadman") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "axum-test") (r "^13.1.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio" "sqlite"))) (d #t) (k 0)) (d (n "teloxide") (r "^0.12.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v7"))) (d #t) (k 0)))) (h "0jwy3rljrccw2gnr1sd46dh4l2sydjac0yz81bfid3051qd138mr")))

