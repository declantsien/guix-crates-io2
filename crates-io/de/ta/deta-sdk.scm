(define-module (crates-io de ta deta-sdk) #:use-module (crates-io))

(define-public crate-deta-sdk-0.1.0 (c (n "deta-sdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "105rb1pr719q9ns5xhgdhfx8cj0dpmygdp2bzpkhi846h6dfy9lj") (y #t)))

(define-public crate-deta-sdk-0.1.1 (c (n "deta-sdk") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "1daab9zzkaa22fm5l249879k2c6wspi7fk21xmw74c6lxgl2in45") (y #t)))

(define-public crate-deta-sdk-0.1.2 (c (n "deta-sdk") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "1jvrg75f0gy3kny86qjnh453zfw036c79qz31q69hs594bpvwbhv") (y #t)))

