(define-module (crates-io de ta detalib) #:use-module (crates-io))

(define-public crate-detalib-0.1.0 (c (n "detalib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "196kx7s0k62rpyv6m0hfczbz42g4qqi0cc3hgqhd16r4mdxvxwlr")))

(define-public crate-detalib-0.1.1 (c (n "detalib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "07zwx1dr50fwxkxdyid2h1yapmsjh6c2571nbasclv4mkfmkqlzp")))

(define-public crate-detalib-0.1.2 (c (n "detalib") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "1wrpnncdxk2rg0d2ygabqcxb075x96ppz939bhw1d4ncg1zqx6px")))

(define-public crate-detalib-0.1.3 (c (n "detalib") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "056iql7zs3j6f0jvkdnx15m6g9hsrq1a5diiaivnwm5iyhq9bs4w") (y #t)))

(define-public crate-detalib-0.1.4 (c (n "detalib") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "0pwa7dvd946djfdl2fvh36qjl7xdf7nv07vlmr5aksidc2lwcrsg") (y #t)))

(define-public crate-detalib-0.1.5 (c (n "detalib") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("rustls" "json"))) (d #t) (k 0)))) (h "17y00qjkppyz0qqffpgwwalg05hivfx9ry52216mw99wbpdi35d7")))

