(define-module (crates-io de ta deta) #:use-module (crates-io))

(define-public crate-deta-0.1.0 (c (n "deta") (v "0.1.0") (h "11g2h0mh934a1cvy6x64559bj6fxslvx3xbyrc5c01p1apb8w28g")))

(define-public crate-deta-0.2.0 (c (n "deta") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "03sr73g05c9j86gavpci31f5yxd0f5lk5xj5fs980ndvhi84b6mj")))

(define-public crate-deta-0.3.0 (c (n "deta") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "01lfh60ybwymyy2h3q6b2d9ndm1hh0z7nppd8h4na43lvg8hhv6p")))

