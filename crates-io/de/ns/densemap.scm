(define-module (crates-io de ns densemap) #:use-module (crates-io))

(define-public crate-densemap-0.1.1 (c (n "densemap") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)))) (h "1gg6lv1nbjgivvkclsbbxcw8va5cyb7x29hfz5s3fjywj1cpz7qc")))

(define-public crate-densemap-0.1.2 (c (n "densemap") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)))) (h "1kn14x21qzbqkw4l1xy22ygiym1bqi4p9wkjcaqs69lfrvdc7a10") (f (quote (("std") ("default" "std"))))))

(define-public crate-densemap-0.1.3 (c (n "densemap") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)))) (h "143p7pscivsd99r1js9bm11jnzhklyjr0663x1qgcbxfxrcbyscp") (f (quote (("std") ("default" "std"))))))

