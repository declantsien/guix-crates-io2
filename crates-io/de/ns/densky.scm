(define-module (crates-io de ns densky) #:use-module (crates-io))

(define-public crate-densky-0.1.0 (c (n "densky") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("cargo" "unstable-styles"))) (d #t) (k 0)) (d (n "densky-core") (r "^0.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "recv-dir") (r "^0.2.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1zxxjdf3vbfk3h1qrn5f6nvw5p1fy1yvm2q64q4wjb8c9w978mpb")))

