(define-module (crates-io de ns dens) #:use-module (crates-io))

(define-public crate-dens-0.1.0 (c (n "dens") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "density-sys") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "19ycif64vz2x6kyq0hnkn5n04i41iiq3iya8a7pqa48j63bxjpad")))

