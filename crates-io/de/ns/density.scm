(define-module (crates-io de ns density) #:use-module (crates-io))

(define-public crate-density-0.1.0 (c (n "density") (v "0.1.0") (h "0iayqp6nsdmmhhl9qm30ff3s2g5nqg5m7a2rmwbcwgyfw9ragakr")))

(define-public crate-density-0.2.0 (c (n "density") (v "0.2.0") (h "0gp4bmyymhvw66kicyb89idwdb2sq0jw4mn0hyr9nvaz2mbhhwyv")))

(define-public crate-density-0.3.0 (c (n "density") (v "0.3.0") (h "0s0wgb8lzfg4q75l1zpyygc6xkpkyhx2kbdn87pmcgxbx7vcfalq")))

(define-public crate-density-0.3.1 (c (n "density") (v "0.3.1") (h "0dkiql9d41sav7n96ca9cgbjifcv67k3z65kv3fgq0fyxgligjv2")))

(define-public crate-density-0.4.0 (c (n "density") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1lzyig7dfnyaffirj6rc2vpzxzljdnlz81swvmpvm2h76pn36ymb")))

(define-public crate-density-0.5.0 (c (n "density") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1c8kj17hjx7qad8pcsznzwc0p4bnjhpwpih6mbbaprw3i6fcgvrb")))

(define-public crate-density-0.5.1 (c (n "density") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1rl6pf4qhqcd0j72qyj53p45cc2mkhi9vx9jzgp96gv7f42sk0d9")))

