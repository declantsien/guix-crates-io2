(define-module (crates-io de ns densearray) #:use-module (crates-io))

(define-public crate-densearray-0.1.0 (c (n "densearray") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "densearray_kernels") (r "^0.0.1") (d #t) (k 0)) (d (n "openblas_ffi") (r "^0.1") (d #t) (k 0)))) (h "00sn4024j7pahqrlmmybwkvlgbgsx0li542qh4wsc7j1zi28r3gz")))

(define-public crate-densearray-0.1.1 (c (n "densearray") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "cblas_ffi") (r "^0.1") (d #t) (k 0)) (d (n "densearray_kernels") (r "^0.0.1") (d #t) (k 0)) (d (n "mkl_link") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "openblas_ffi") (r "^0.1") (d #t) (k 0)))) (h "14yz0xmx3lb0s6kj0mncpqjdnrfi3lrna54s4mmv4djcb37mgs2m") (f (quote (("mkl_parallel" "mkl_link/openmp") ("default"))))))

