(define-module (crates-io de ns densky-core) #:use-module (crates-io))

(define-public crate-densky-core-0.1.0 (c (n "densky-core") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dynamic-html") (r "^1.0.0") (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "prettify-js") (r "^0.1.0") (d #t) (k 0)) (d (n "recur-fn") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "054wc92rq8vn0gfkv0smiydx75lic1nbw96wdv0gzkhgmq8qw31r")))

