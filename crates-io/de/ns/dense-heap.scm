(define-module (crates-io de ns dense-heap) #:use-module (crates-io))

(define-public crate-dense-heap-0.1.0 (c (n "dense-heap") (v "0.1.0") (h "0262qa8hyharr9q6ga9ai1779amz9lhrypinigfdn095gsh27m6g") (y #t)))

(define-public crate-dense-heap-0.1.1 (c (n "dense-heap") (v "0.1.1") (h "0rgdps8ngzk1f48qm08bira5vlkrp9rp7sm0vlw89kgc5x308mkn") (y #t)))

(define-public crate-dense-heap-0.1.2 (c (n "dense-heap") (v "0.1.2") (h "17zr77gbayqq3v9c6kz4551m8r2kdk1c62lmn3rf97lsgy9cxvi2")))

