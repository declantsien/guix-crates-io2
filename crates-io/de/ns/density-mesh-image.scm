(define-module (crates-io de ns density-mesh-image) #:use-module (crates-io))

(define-public crate-density-mesh-image-1.0.0 (c (n "density-mesh-image") (v "1.0.0") (d (list (d (n "density-mesh-core") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "004nng34hgfm50ndviz8h51b0zph9nky0i7xk2w0b7ivlv81ypah")))

(define-public crate-density-mesh-image-1.1.0 (c (n "density-mesh-image") (v "1.1.0") (d (list (d (n "density-mesh-core") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "094acfnhpaki7ffbalvi0xg7wkd3p53k2s98v5bxhgv6bl11bpk2") (f (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1.2.0 (c (n "density-mesh-image") (v "1.2.0") (d (list (d (n "density-mesh-core") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7c2r9vii87h0c5bwhbahvi8cqia1yfnawnf00903ah021m37rj") (f (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1.3.0 (c (n "density-mesh-image") (v "1.3.0") (d (list (d (n "density-mesh-core") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16v4ansbrq778va4av7wzd506ywnil3m859hfqh4r2zgdb0jixg8") (f (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1.4.0 (c (n "density-mesh-image") (v "1.4.0") (d (list (d (n "density-mesh-core") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzsrvr6fc0p1i3g64h96syqn9090gyzbqnzhxph3027pjyvbdg2") (f (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1.5.0 (c (n "density-mesh-image") (v "1.5.0") (d (list (d (n "density-mesh-core") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gz9zk7rkji7iwd1s6jlypnghchypqgzvqlkww5pk258map0lp3h") (f (quote (("parallel" "density-mesh-core/parallel"))))))

