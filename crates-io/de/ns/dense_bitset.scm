(define-module (crates-io de ns dense_bitset) #:use-module (crates-io))

(define-public crate-dense_bitset-0.1.0 (c (n "dense_bitset") (v "0.1.0") (h "1ln3al0l5z87jq84rrpqnm3m7li5kqacwfhikq0klgy58lzh9is0")))

(define-public crate-dense_bitset-0.1.1 (c (n "dense_bitset") (v "0.1.1") (h "163400rsg1rz1sx143g4gxqny6vncsyz9chs9fkl6f5m598g258x")))

