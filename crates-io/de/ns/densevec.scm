(define-module (crates-io de ns densevec) #:use-module (crates-io))

(define-public crate-densevec-0.1.0 (c (n "densevec") (v "0.1.0") (h "1640ncs32xfcfj6i9schff3wvlm9jl2lxzkws3w8p301zf1fmxcv")))

(define-public crate-densevec-0.2.0 (c (n "densevec") (v "0.2.0") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zwbjmp2fqcjflvdyixwc29v73zlx08k6jyr4v3zamnfp3qwih4s") (f (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.0 (c (n "densevec") (v "0.3.0") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1814lqi655kinysjvmy98f54ngwnbajiaj917w86lsbfz6yl09aj") (f (quote (("unstable") ("parallel" "rayon")))) (y #t)))

(define-public crate-densevec-0.3.1 (c (n "densevec") (v "0.3.1") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n7r3ahnm6a416nd072msl1kyyc4wyjkfqkw1g14zmxljas2zjks") (f (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.2 (c (n "densevec") (v "0.3.2") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qb1qmjgwk86qmpk6da29an4i57djs8qvxvlhgvdjx5g3qb6d6a8") (f (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.3 (c (n "densevec") (v "0.3.3") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00c0x7w448xc6rnylwiz9pq4xylcxqdcvby00jai6pii67sadmcn") (f (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.4 (c (n "densevec") (v "0.3.4") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lgd3xlxccw7p6rlxvasapm78c6kmby51vafr7qpgbs1v43gm6i5") (f (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.5 (c (n "densevec") (v "0.3.5") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1jzshb9f7hsdmcmk8y8208vv8xm8bcih8a1kglr46hvz2arzkr54") (f (quote (("unstable") ("serialize" "serde" "serde_derive") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3.6 (c (n "densevec") (v "0.3.6") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "17q864b9c7hmyiz1kpvpvgphi7ydjyvpjv5vf9xwswin058i6cxh") (f (quote (("unstable") ("serialize" "serde" "serde_derive") ("parallel" "rayon"))))))

(define-public crate-densevec-0.4.0 (c (n "densevec") (v "0.4.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0jvyhrxzh2790wybahj1f78bnl3zw5y0ayr90k2gf20scl6909ib") (f (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

(define-public crate-densevec-0.5.0 (c (n "densevec") (v "0.5.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1r5r7iinkc4sab0yc2cg4wc4np2whwlmw31a6s00dkiy6b37kpb9") (f (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

(define-public crate-densevec-0.5.1 (c (n "densevec") (v "0.5.1") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1lydnb58bpycaapykl7fi9d0h7sfvyr841i18z7vqxhxl7fnm8jb") (f (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

