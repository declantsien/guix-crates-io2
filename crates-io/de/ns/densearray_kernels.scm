(define-module (crates-io de ns densearray_kernels) #:use-module (crates-io))

(define-public crate-densearray_kernels-0.0.1 (c (n "densearray_kernels") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yc0kj7addx2iwqlsw6rygk9i6in0ldbq0bi3w7s9xzzlqx07s0a")))

