(define-module (crates-io de ns density-sys) #:use-module (crates-io))

(define-public crate-density-sys-0.1.0 (c (n "density-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1p1n5gw680jqb5i3lc1dlk5m489vysh222gwhkv0bj22w8ax6jwc")))

