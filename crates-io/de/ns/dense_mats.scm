(define-module (crates-io de ns dense_mats) #:use-module (crates-io))

(define-public crate-dense_mats-0.1.0 (c (n "dense_mats") (v "0.1.0") (h "06nbcan6yy2n3f6cskvhy8lsgbwqyfr5nc95p6v9129p4xq76a30")))

(define-public crate-dense_mats-0.1.1 (c (n "dense_mats") (v "0.1.1") (h "1vj4kiiwrxgdhs5y60g198y238sgxyp125bqihihakbp27arkjhl")))

(define-public crate-dense_mats-0.2.0 (c (n "dense_mats") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "18dhvn8l5ax9hmv7r8r0gr2gk2389rxz1vjv7183cj43zz2hhz6v")))

(define-public crate-dense_mats-0.3.0 (c (n "dense_mats") (v "0.3.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0nmpr1bv107d5726qj4ww40q0qf11sdpq1djp1lhr1j5fxlc0m99")))

(define-public crate-dense_mats-0.3.1 (c (n "dense_mats") (v "0.3.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "11ki3kgyg9k0lwn3y9f1rw74cb2l2zsbfh75v3j5iy1dwv2lsb2q")))

(define-public crate-dense_mats-0.3.2 (c (n "dense_mats") (v "0.3.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0fw8akdwcc5qp53ahsak6rdnkjik25sr2l363cwlg5l2ln8mgxjr")))

(define-public crate-dense_mats-0.3.3 (c (n "dense_mats") (v "0.3.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0ba7sb5xk0xvnkr5d1q08s7jriknwlv362b2yp64hi1amgy2syyy")))

(define-public crate-dense_mats-0.3.4 (c (n "dense_mats") (v "0.3.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "16jnd0v1qv6lfnlxggdvlmirnxmafgdg6mrip46axsh6h5mkcra5")))

(define-public crate-dense_mats-0.3.5 (c (n "dense_mats") (v "0.3.5") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1ph0gkysa2xpnmnhr8nfgi8k04a10ykljnbxj1l8sc8bsliangj9")))

(define-public crate-dense_mats-0.4.0 (c (n "dense_mats") (v "0.4.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1k8069bsirk29fblvyx5sk15c70f0xcsqzp5x7nnlqpjb7pzlr7q")))

