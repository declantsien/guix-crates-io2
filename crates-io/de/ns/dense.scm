(define-module (crates-io de ns dense) #:use-module (crates-io))

(define-public crate-dense-1.0.0 (c (n "dense") (v "1.0.0") (d (list (d (n "mnist_read") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "ndarray") (r ">=0.13.1, <0.14.0") (d #t) (k 0)))) (h "0i2j4lvl247jb5npgn5vrwp67gzc62fxy7px64p344pkadhxsvi3")))

(define-public crate-dense-1.0.1 (c (n "dense") (v "1.0.1") (d (list (d (n "mnist_read") (r ">=1.0.1, <2.0.0") (d #t) (k 0)) (d (n "ndarray") (r ">=0.13.1, <0.14.0") (d #t) (k 0)))) (h "0lfmi6l9p13l70dp6883m06nsfy1ifbzm3r23lxiphql2l9zjzlr")))

(define-public crate-dense-1.0.2 (c (n "dense") (v "1.0.2") (d (list (d (n "mnist_read") (r ">=1.0.1, <2.0.0") (d #t) (k 0)) (d (n "ndarray") (r ">=0.13.1, <0.14.0") (d #t) (k 0)))) (h "1c05idh25f9xjladm6yqp05s37mjzqf8y201wpgdiamq2lvvck1q")))

(define-public crate-dense-1.1.0 (c (n "dense") (v "1.1.0") (d (list (d (n "mnist_read") (r "^1.0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)))) (h "1s4f1l079cdxz2iqpz3vfba9c9krwwhc7ansiv50mks4cnqzxpd7")))

(define-public crate-dense-2.0.0 (c (n "dense") (v "2.0.0") (d (list (d (n "mnist_read") (r "^1.0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "19qkscw6a8mh4marwm6iapigmay09vrrzw2wryjd8ziccn1jrd1k")))

