(define-module (crates-io de ns density-mesh-core) #:use-module (crates-io))

(define-public crate-density-mesh-core-1.0.0 (c (n "density-mesh-core") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "0v7fz10war9fpvjnj16b9zawb1rlgx4m7shg9l6m8j6v2p3s9ml1")))

(define-public crate-density-mesh-core-1.1.0 (c (n "density-mesh-core") (v "1.1.0") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "0cpiiss9vpixqa3dbjqrgxj1arpyv1izxcz5s4ymfb252kdvk8dl") (f (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1.2.0 (c (n "density-mesh-core") (v "1.2.0") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "0aljr0in5bh73m1awr9im4vvi8v81phsxvkr086mph5qkww9763k") (f (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1.3.0 (c (n "density-mesh-core") (v "1.3.0") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "09m3vg8k61vjfrx4rghq8lil1ynwd2n2x61n310ix0wl9x1jzv7w") (f (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1.4.0 (c (n "density-mesh-core") (v "1.4.0") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "0vynbzhbm49viq7xv76iwy26nhamicsbnxnf5pska873xa7abpbr") (f (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1.5.0 (c (n "density-mesh-core") (v "1.5.0") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "triangulation") (r "^0.1") (d #t) (k 0)))) (h "0fc8yi1ryy0ajkcxl1646gmmjcw2l3cnnvxr6zdxgmc0hw86ngjw") (f (quote (("parallel" "rayon"))))))

