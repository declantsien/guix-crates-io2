(define-module (crates-io de ms demsf-rs) #:use-module (crates-io))

(define-public crate-demsf-rs-1.0.0 (c (n "demsf-rs") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1yl13a5407qgcrbpivkzdfxw8z17m7dgkr9ylrnjvylnlm112ifp") (y #t) (r "1.71.1")))

(define-public crate-demsf-rs-1.0.1 (c (n "demsf-rs") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0kvybwibrnfyshvw581f7znf7f24h6dxwp3av7apa125jnaxgcvm") (y #t) (r "1.71.1")))

(define-public crate-demsf-rs-1.0.2 (c (n "demsf-rs") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1w2wdk2zm5r52nr5xx98jw0m3yanhngw2hpmzbln3shypc53jkmd") (r "1.71.1")))

