(define-module (crates-io de ms demsos) #:use-module (crates-io))

(define-public crate-demsos-0.0.0 (c (n "demsos") (v "0.0.0") (h "0hd12rjnddj46mcrhwy016p6j91zahd0cw75x4bzjkqd2i6miig7") (y #t)))

(define-public crate-demsos-0.1.0 (c (n "demsos") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pahaygccmkwln5skxs5kr6f4mdi469w9vapzrfppcw5nqpin60q")))

(define-public crate-demsos-0.2.0 (c (n "demsos") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbmkzb5jvv3fp9nhha1dgk431ys72r5gwg261fhj4bvvn40hrgb")))

