(define-module (crates-io de no deno_plugin_num_cpus) #:use-module (crates-io))

(define-public crate-deno_plugin_num_cpus-0.1.1 (c (n "deno_plugin_num_cpus") (v "0.1.1") (d (list (d (n "deno_core") (r "^0.54.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "17prw3r971h14967fg0af1lrzfsjni75khxy8ys2krwq3d9h7zv7")))

(define-public crate-deno_plugin_num_cpus-0.1.2 (c (n "deno_plugin_num_cpus") (v "0.1.2") (d (list (d (n "deno_core") (r "^0.54.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1ydhbjlyyxd2fixxkrp6bmxvw4ypr56mv0m5qvclkfc3pxnq72mc")))

