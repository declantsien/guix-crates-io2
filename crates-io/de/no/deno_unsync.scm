(define-module (crates-io de no deno_unsync) #:use-module (crates-io))

(define-public crate-deno_unsync-0.1.0 (c (n "deno_unsync") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1pxsr31zsshfzmf9bcwihz14g9kn4jf6jaz8b0h0wmv71wacww68")))

(define-public crate-deno_unsync-0.1.1 (c (n "deno_unsync") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1cy7vwk5s6dbb5rp0r7q5z0m6n95f1pdf0z63gfivrr5bwh882dc")))

(define-public crate-deno_unsync-0.2.0 (c (n "deno_unsync") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1vw24sx6b8ms6yz8861a7f27hrsilmpfzxhaw6gmjrsqk6dwahds")))

(define-public crate-deno_unsync-0.2.1 (c (n "deno_unsync") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "05av9s97j2g58lr8whip17nvzn2yi7yc7bib56jp8qsx8mifa807")))

(define-public crate-deno_unsync-0.3.0 (c (n "deno_unsync") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (d #t) (k 2)))) (h "14w9xdm7gcnz65s00igbsm6vqim0lg43qy7wxikfal7x59rg7a7q")))

(define-public crate-deno_unsync-0.3.1 (c (n "deno_unsync") (v "0.3.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (d #t) (k 2)))) (h "1rnnw6311xnsqcvk4bsn6l3kbrk8jlvp4lxnk5x44wnkns0jz41f")))

(define-public crate-deno_unsync-0.3.2 (c (n "deno_unsync") (v "0.3.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0fg740gkcpgwr7vhrbdj1540ah37hzqs15mfilcaxnw46phggprh")))

(define-public crate-deno_unsync-0.3.3 (c (n "deno_unsync") (v "0.3.3") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0dlpa65fy8fziqjqiillyszy55hq9pzjyyzzbk3ml2hyz1x9rmz3")))

(define-public crate-deno_unsync-0.3.4 (c (n "deno_unsync") (v "0.3.4") (d (list (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt" "time"))) (d #t) (k 2)))) (h "195r6vvfpdybqxrmkssfv85pfi23x9i71hvd0p45r6lb4zlsamvm")))

