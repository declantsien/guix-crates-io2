(define-module (crates-io de no deno_json_op) #:use-module (crates-io))

(define-public crate-deno_json_op-0.1.0 (c (n "deno_json_op") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lsi9v2b4vpa9g2pk82vn7p8yd80s9x3dbwlwvg3mh36sk507xxg")))

(define-public crate-deno_json_op-0.1.1 (c (n "deno_json_op") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvmk6q14mwhn70l474037m303zdwmnwd2isaxnv93vsp9dis7ig")))

