(define-module (crates-io de no deno_file) #:use-module (crates-io))

(define-public crate-deno_file-0.2.0 (c (n "deno_file") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.84.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "07chd3dfpz199mdhp5v33ih0y4a02xlb2nk5lpgb9c0sggvnkb0f")))

(define-public crate-deno_file-0.3.0 (c (n "deno_file") (v "0.3.0") (d (list (d (n "deno_core") (r "^0.85.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "11jra8sbzgm2m0ybm2q9vms3v3jnh0wcbkd2gky13gxc3zbw8hw2")))

(define-public crate-deno_file-0.4.0 (c (n "deno_file") (v "0.4.0") (d (list (d (n "deno_core") (r "^0.86.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1a2zn1l3crlzn6gh61zl27lq557j7s18296mk5cw3qdlayz19ma7")))

(define-public crate-deno_file-0.5.0 (c (n "deno_file") (v "0.5.0") (d (list (d (n "deno_core") (r "^0.87.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0k0srqphd3nb0wccnz9rhgpxcvlijhxvwy64pjm220yvhfxb8ymy")))

(define-public crate-deno_file-0.6.0 (c (n "deno_file") (v "0.6.0") (d (list (d (n "deno_core") (r "^0.88.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1r7jrxbd1mcslmkh5a4137hiwhbd2scq2gc0cvvpylpjw15dadvv")))

(define-public crate-deno_file-0.6.1 (c (n "deno_file") (v "0.6.1") (d (list (d (n "deno_core") (r "^0.88.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yjbq67mhaz8ryj1i5p7hvbwpcqk96mfsjyg1k8yszb7v425zmp4")))

(define-public crate-deno_file-0.7.0 (c (n "deno_file") (v "0.7.0") (d (list (d (n "deno_core") (r "^0.89.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1c3z32iyfpvx1hj7g5rncvb2lyqan9jzh7wnhl2q583b6hhiaqsb")))

