(define-module (crates-io de no deno-x86_64-apple-darwin) #:use-module (crates-io))

(define-public crate-deno-x86_64-apple-darwin-0.3.5-temp1 (c (n "deno-x86_64-apple-darwin") (v "0.3.5-temp1") (h "08vy1rjj8wgmjvld4axrarvh6fbgkv4qrbw8yji3dq2imgxkfbiv") (y #t)))

(define-public crate-deno-x86_64-apple-darwin-0.3.5-temp3 (c (n "deno-x86_64-apple-darwin") (v "0.3.5-temp3") (h "1adk5w54k8nb5hkw1xd2l7ympcybxkl9gaq455nn8p1radnfnv31") (y #t)))

(define-public crate-deno-x86_64-apple-darwin-0.3.5-temp4 (c (n "deno-x86_64-apple-darwin") (v "0.3.5-temp4") (h "0i7hrvfywn3n9nyfqyfj23zpfjpk884zpcxhd5h6nwgdady84pyp") (y #t)))

(define-public crate-deno-x86_64-apple-darwin-0.3.5-temp5 (c (n "deno-x86_64-apple-darwin") (v "0.3.5-temp5") (h "1fwvhx674lnqxl41z346qcibfmxn23mszhm54m90iamg60s4bksy") (y #t)))

(define-public crate-deno-x86_64-apple-darwin-0.3.5 (c (n "deno-x86_64-apple-darwin") (v "0.3.5") (h "15ci6b70v5qm5qkznqmpkz4yqgm57wa6j8jxw3j8g6dwyxp5f70y")))

(define-public crate-deno-x86_64-apple-darwin-0.3.6 (c (n "deno-x86_64-apple-darwin") (v "0.3.6") (h "015x0snf83n13rhgflax8mhmc5lxcikmplw1k9xyrv515nksv34x")))

(define-public crate-deno-x86_64-apple-darwin-0.3.7 (c (n "deno-x86_64-apple-darwin") (v "0.3.7") (h "1w1c6r5q0bwrclvs9gf08hz90bznan4jr0vanvsa50bv6yszqx0c")))

(define-public crate-deno-x86_64-apple-darwin-0.3.8 (c (n "deno-x86_64-apple-darwin") (v "0.3.8") (h "0mb672ifk382ik64wx98z59n4wyy6d8qqw3x4jfs4szyn2nxb6wy")))

(define-public crate-deno-x86_64-apple-darwin-0.3.9 (c (n "deno-x86_64-apple-darwin") (v "0.3.9") (h "1frayk9sp9b8x138svs49w6azhzwiqq4d2dinpnddn8ps5mc1bz0")))

(define-public crate-deno-x86_64-apple-darwin-0.3.10 (c (n "deno-x86_64-apple-darwin") (v "0.3.10") (h "0a2kkmii15ab6ilca6ykll5jn5f6iz6i8c40hg1f8xwhrjskkw4l")))

(define-public crate-deno-x86_64-apple-darwin-0.3.11 (c (n "deno-x86_64-apple-darwin") (v "0.3.11") (h "1ahmav5qg2h5ncg0xv2kbxv5hvjz2qf8xk82cgibr4z9xnp05gqq")))

(define-public crate-deno-x86_64-apple-darwin-0.4.0 (c (n "deno-x86_64-apple-darwin") (v "0.4.0") (h "1bx19jg3mpsnvpvhyrqx2d3c3mkxfzzprfq9nh7czmrr1mv0zppq")))

(define-public crate-deno-x86_64-apple-darwin-0.5.0 (c (n "deno-x86_64-apple-darwin") (v "0.5.0") (h "1fa5j60zyx315qxr2wnrn3s78naih73cgza28715v6vdvpgi4rrs")))

(define-public crate-deno-x86_64-apple-darwin-0.6.0 (c (n "deno-x86_64-apple-darwin") (v "0.6.0") (h "0cqf0c158aj2z12wymv2pdjwxq144r701inbjscz15lsrvs1v22c")))

(define-public crate-deno-x86_64-apple-darwin-0.7.0 (c (n "deno-x86_64-apple-darwin") (v "0.7.0") (h "0ld2a8lhnjhi0c57givsgvlr377cgwg8b00bjqcz87srpwjy4cxk")))

(define-public crate-deno-x86_64-apple-darwin-0.8.0 (c (n "deno-x86_64-apple-darwin") (v "0.8.0") (h "08508b9rl5igsx6240fyls4mgk64b1cvk7c1s3fxq1fiwjdyv3r1")))

(define-public crate-deno-x86_64-apple-darwin-0.9.0 (c (n "deno-x86_64-apple-darwin") (v "0.9.0") (h "0wqdg3sx6ckknzrrqrlp5gxqff96ar3jlxy0fc5vz0vpyazrjzs9")))

(define-public crate-deno-x86_64-apple-darwin-0.10.0 (c (n "deno-x86_64-apple-darwin") (v "0.10.0") (h "0p0ndg0pi1vfplajbrx97kjsvlw6sqqlqc8hcjcj4qlgxfx4dcq1")))

(define-public crate-deno-x86_64-apple-darwin-0.11.0 (c (n "deno-x86_64-apple-darwin") (v "0.11.0") (h "1i5zfbscvvjhs1h5hx9b1hq86zhzjm60ix8sfb5jfch75d7ydrpf")))

(define-public crate-deno-x86_64-apple-darwin-0.12.0 (c (n "deno-x86_64-apple-darwin") (v "0.12.0") (h "0ciazbq1a20ngayscc7d0vq20mxlvy1ry08ckslb7rv7im8v32a9")))

(define-public crate-deno-x86_64-apple-darwin-0.13.0 (c (n "deno-x86_64-apple-darwin") (v "0.13.0") (h "16yifq6h9wzkq9zg6bm9k7pk1dnq4lhmns12fiaxqq9x6km13478")))

(define-public crate-deno-x86_64-apple-darwin-0.14.0 (c (n "deno-x86_64-apple-darwin") (v "0.14.0") (h "1ca58x7vaxnj7nzrkh014l7jc9jd832pc9cfngismxs5p1i6samr")))

(define-public crate-deno-x86_64-apple-darwin-0.15.0 (c (n "deno-x86_64-apple-darwin") (v "0.15.0") (h "0pakbqg5kfsgdfx6zchawrhig45y7hjm8h44bimnk88cr5g4vpnc")))

(define-public crate-deno-x86_64-apple-darwin-0.16.0 (c (n "deno-x86_64-apple-darwin") (v "0.16.0") (h "0p83qjq3lk9hhz12pn1im6bm04gk1knm77lmdgl8k0d65wx640li")))

(define-public crate-deno-x86_64-apple-darwin-0.17.0 (c (n "deno-x86_64-apple-darwin") (v "0.17.0") (h "0ld84mscl2gcr5ivj04sp63i6majmbni0jd5gkc8k0fx1r8r8y1b")))

(define-public crate-deno-x86_64-apple-darwin-0.18.0 (c (n "deno-x86_64-apple-darwin") (v "0.18.0") (h "18p0crjn7l2j7z768ca9fmk526637c3vi0ia4qlbhq1vqkm8lksv")))

(define-public crate-deno-x86_64-apple-darwin-0.19.0 (c (n "deno-x86_64-apple-darwin") (v "0.19.0") (h "0z0mizjw3g9kak4js3q3z26d28jgbgbrks50byns7r3iaqqvb99r")))

(define-public crate-deno-x86_64-apple-darwin-0.20.0 (c (n "deno-x86_64-apple-darwin") (v "0.20.0") (h "17qs2md5dprphkxh2ibz2g08g7cq1h1m25rs57bypjng62g9dsdq")))

(define-public crate-deno-x86_64-apple-darwin-0.21.0 (c (n "deno-x86_64-apple-darwin") (v "0.21.0") (h "152z6kkdwrpljaw4a144jv32pl8chk3p5wa3d83laqxwlfq86y75")))

(define-public crate-deno-x86_64-apple-darwin-0.22.0 (c (n "deno-x86_64-apple-darwin") (v "0.22.0") (h "1aqc3r9jk147mhs1w6k4acc49qfddhr32xfdm7x64zinm1vw4niz")))

(define-public crate-deno-x86_64-apple-darwin-0.23.0 (c (n "deno-x86_64-apple-darwin") (v "0.23.0") (h "0r1l22qhzix76l046fbzfwwf5i4kxhvdcyydc99idsj9ca2xvlj0")))

(define-public crate-deno-x86_64-apple-darwin-0.24.0 (c (n "deno-x86_64-apple-darwin") (v "0.24.0") (h "0j80kjhdmcpm9vkimp142q37qkdf6wgj43c4bmcdpw8bvfb9i4l7")))

