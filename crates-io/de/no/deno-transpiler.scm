(define-module (crates-io de no deno-transpiler) #:use-module (crates-io))

(define-public crate-deno-transpiler-0.1.0 (c (n "deno-transpiler") (v "0.1.0") (d (list (d (n "deno_ast") (r "^0.14") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.129") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1p27xalhdadz365zl8r7d8kn4j7gjxbgl9fzrhg0cbyx33763bp0")))

(define-public crate-deno-transpiler-0.2.0 (c (n "deno-transpiler") (v "0.2.0") (d (list (d (n "deno_ast") (r "^0.14") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.131") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1bcxazk4z476yv1vy2sqbs0aqy9lw9n20a7w8q0i9hzmkhb48z48")))

(define-public crate-deno-transpiler-0.2.1 (c (n "deno-transpiler") (v "0.2.1") (d (list (d (n "deno_ast") (r "^0.14") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.132.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "16w01lljx1hc9nnm9cl54j6i47p5fiw9mmz5s2kwy58m5ck9s5ws")))

(define-public crate-deno-transpiler-0.2.2 (c (n "deno-transpiler") (v "0.2.2") (d (list (d (n "deno_ast") (r "^0.14.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.132.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 2)))) (h "09v4rii6w48iiiapnwwjxxhmq6hvfwnfg8ax7rl7575x7rqac7f8")))

(define-public crate-deno-transpiler-0.2.3 (c (n "deno-transpiler") (v "0.2.3") (d (list (d (n "deno_ast") (r "^0.14.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.133.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 2)))) (h "011ddznrs7xjc6k2hpj2alpy8cwdc3w21rw8ihimdv9sywc4lqa3")))

(define-public crate-deno-transpiler-0.2.4 (c (n "deno-transpiler") (v "0.2.4") (d (list (d (n "deno_ast") (r "^0.14.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.134.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 2)))) (h "1vhzv8vhmxkw1hh3p28r3hyx14k5591vlbgqmf7b28q7z4g5qmpk")))

(define-public crate-deno-transpiler-0.2.5 (c (n "deno-transpiler") (v "0.2.5") (d (list (d (n "deno_ast") (r "^0.14.1") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.134.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 2)))) (h "1v15x4hcx58z0q3llwjg7lvycp614m2bd5kny7b36zmh8a799za6")))

(define-public crate-deno-transpiler-0.3.0 (c (n "deno-transpiler") (v "0.3.0") (d (list (d (n "deno_ast") (r "^0.15.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.135.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)))) (h "0vbg2grlpaif19rgh7zi22fhviphhsslgpihziwqjy8gbmp0bgvz")))

(define-public crate-deno-transpiler-0.3.1 (c (n "deno-transpiler") (v "0.3.1") (d (list (d (n "deno_ast") (r "^0.15.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.136.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)))) (h "05ydf09a7yv7zlk67yj4aqz3wmsqw9jgrpnc38y5w47f69qdf0bx")))

(define-public crate-deno-transpiler-0.3.2 (c (n "deno-transpiler") (v "0.3.2") (d (list (d (n "deno_ast") (r "^0.15.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.137.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)))) (h "15bqgvcix8f219v5gp7bb2g33gk2h7xzqz3z3c01kqvf2ih9hwpf")))

(define-public crate-deno-transpiler-0.3.3 (c (n "deno-transpiler") (v "0.3.3") (d (list (d (n "deno_ast") (r "^0.15.0") (f (quote ("minifier" "module_specifier" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.138.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)))) (h "1z6f8b26ybg5hx7fiwrqp0971hijzadn8iipbjbkc1yzqkyy93ac")))

(define-public crate-deno-transpiler-0.3.4 (c (n "deno-transpiler") (v "0.3.4") (d (list (d (n "deno_ast") (r "^0.16.0") (f (quote ("bundler" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.142.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "swc_ecma_minifier") (r "^0.120.2") (d #t) (k 0)))) (h "138nkj5xrcmip3r04dzdlxgzxnd5nf0js7lkcsndbprgv3iyl2kf")))

(define-public crate-deno-transpiler-0.4.0 (c (n "deno-transpiler") (v "0.4.0") (d (list (d (n "deno_ast") (r "^0.17.0") (f (quote ("bundler" "transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.147.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "swc_ecma_minifier") (r "^0.136.1") (d #t) (k 0)))) (h "02w0w3cy1kkynbr7j7dnzkcgww834fxmhvfr8b9k8ikyafjdlviw")))

