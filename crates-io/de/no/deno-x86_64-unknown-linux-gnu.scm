(define-module (crates-io de no deno-x86_64-unknown-linux-gnu) #:use-module (crates-io))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5-temp1 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5-temp1") (h "0dq6q2zb7md7i8h4gysb4pksrfhp2bw41zyy2bm3vy5mnkgcjbgy") (y #t)))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5-temp2 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5-temp2") (h "16jq43kgxyjm3ma6ln75rl52rbhhvmmsschzxwiyi28xyd4qv1mb") (y #t)))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5-temp3 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5-temp3") (h "1b9p8c6k5hrhybi2cy3zfb72ba1rgq6x22qh9zjrbpq3hk4qyjhm") (y #t)))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5-temp4 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5-temp4") (h "17npkkxs3nyfsz7rfz0abxfjf5kslffsd6wp4p9vfwc1z95b3vyh") (y #t)))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5-temp5 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5-temp5") (h "0yqy2kmd1877acbar7ab4mcbf990d6lixcl0jvxia8rc3jz0w00h") (y #t)))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.5 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.5") (h "0l2zcg9hp6rbb1wa8hyls4nqx9n181kaynhy9z0y840z1j6zzn1r")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.6 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.6") (h "09zcaq11wfj2kp0dv9fbllps3iadwrvsbjbb3vqlg010wslafn92")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.7 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.7") (h "0sd6dfqx86kqn8ffnqb0ry14fvq94qhixnjni2gqnjww3082jix7")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.8 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.8") (h "1dglh9zawg5qpxaaj55inib3pkq7z1wg3dxybwvxlv610lmhz3s8")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.9 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.9") (h "1z6jcbzxnd8ad7zwqlp4imi5xclha39vf75w1v5a4pm06cykqj18")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.10 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.10") (h "0pp4hd2aiyqks724njzl967x952mk3nlskn51xkcaifs4gbxmbbl")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.3.11 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.3.11") (h "08484wvi4j64d5y9lxbph4vnzfl328xa7z7l34cf4hiwp8phdx6s")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.4.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.4.0") (h "15z6g4cdsbgcmvkhsq3v4dg9489bkip2h5h6z7yqv7v1ddsyaxj9")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.5.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.5.0") (h "0mjkq01w1qw2ii59y03mv1wgzg7630waaccnxi64jv286lbg3743")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.6.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.6.0") (h "16diyyp5qdb2ylyhd3vx3nkayfjlmpvs8vdsc2wc1vg1dg24cm7x")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.7.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.7.0") (h "1aqn1k7lm8zckzvlm5s0x0cf0bhp2g26shj1xvqbhanzwj93bira")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.8.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.8.0") (h "0n0i9d8b1jfz767bnrcv8rpka3r7jp47dys72mf6hkjw1hv3dvxl")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.9.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.9.0") (h "135clvqs99581k3i5z0gjcsxbvpa3jyhvvkh605q8wkmwb5wwidj")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.10.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.10.0") (h "1l58nblww5jdnp1icxpxp217lcviz0j5zpygcz4in4kqj6dpxhpm")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.11.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.11.0") (h "0ldg4546wv680kj3ja6a203png97hg8yay6q6s7d04l90z1ypw0r")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.12.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.12.0") (h "1nbym4c1rhf9917q3176x0k2im6pw834x0pj2in8dz8vkpz4fmz5")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.13.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.13.0") (h "0skb1lzvpwwcqxlvr0y6n2cr4fvnjwr7gaxwjpqp7kn2cwr4fcq8")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.14.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.14.0") (h "1sx3d4cbgnqbjk7bc9pqwyvb9jxhny9hhk85vxxaax9qbkc9asjz")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.15.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.15.0") (h "17g85h3b3j5bj2337c89aciqyw8ryh00f7bg3ghmigmqc5w4rvlr")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.16.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.16.0") (h "02d7q86sxyw3li2h914kxga6islncqyyjmpaxypx121da28p1f61")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.17.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.17.0") (h "0dvas3d6h5h52lipwsshhr3n481smnx5dk5g48065zr419v2idc9")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.18.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.18.0") (h "1gjx7hpl1sfi1jll47w2flv2zc19j6i78i16iyjqv6swnyx5h3kn")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.19.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.19.0") (h "048zk9vf78hcnilzj575g0lfn8zzr3kpi3q2n0dxfgprydijnbb7")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.20.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.20.0") (h "1iq34zddzsfbvh11ijndxfmcnx9q5czy42wzi3g1hqq7l7r4cn3l")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.21.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.21.0") (h "08qlvciilrl6m388xdkscf4flfyd68ibqqg5ikz0d58f9nh7k42g")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.22.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.22.0") (h "1xxhi7jyj40b1agcrrm4xjl0ng16raqj8lzd9h1479pbpm2657vs")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.23.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.23.0") (h "0r76cs8hwyi66mq7k7s94vvdfhz2c9lh3rjyind8lsw9an698nng")))

(define-public crate-deno-x86_64-unknown-linux-gnu-0.24.0 (c (n "deno-x86_64-unknown-linux-gnu") (v "0.24.0") (h "1348j49d6bk53xyd0wxf5zifsapkmhqm8mgwlvgbins0hdvp469w")))

