(define-module (crates-io de no deno_terminal) #:use-module (crates-io))

(define-public crate-deno_terminal-0.0.1 (c (n "deno_terminal") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (o #t) (d #t) (k 0)))) (h "0w4nngcjhi19z9gw6xc9myd4sy54jwlgkmzhxb5pv7nny5drg2x5") (f (quote (("default" "colors") ("colors" "termcolor"))))))

(define-public crate-deno_terminal-0.1.0 (c (n "deno_terminal") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (o #t) (d #t) (k 0)))) (h "14fq8rsqx0h2dzjn93d35g7mx2iwbbv928mdgx0y2rbmylp70ixq") (f (quote (("default" "colors") ("colors" "termcolor"))))))

(define-public crate-deno_terminal-0.1.1 (c (n "deno_terminal") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (o #t) (d #t) (k 0)))) (h "1bwf0pxbxr4sf9ak7rj32f5bv96gxvxpd6j0hswzhxgkwza3fqvy") (f (quote (("default" "colors") ("colors" "termcolor"))))))

