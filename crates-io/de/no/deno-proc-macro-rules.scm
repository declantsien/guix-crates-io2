(define-module (crates-io de no deno-proc-macro-rules) #:use-module (crates-io))

(define-public crate-deno-proc-macro-rules-0.3.0 (c (n "deno-proc-macro-rules") (v "0.3.0") (d (list (d (n "deno-proc-macro-rules-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f4mg5n322xsvsz5cr2y04kj713waxh07yd5c4ms6czcckx4q396")))

(define-public crate-deno-proc-macro-rules-0.3.1 (c (n "deno-proc-macro-rules") (v "0.3.1") (d (list (d (n "deno-proc-macro-rules-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l7f5a610bgvs1q9ybny5y4myh6ryw8a61l6nxfd9zbck2214jlq")))

(define-public crate-deno-proc-macro-rules-0.3.2 (c (n "deno-proc-macro-rules") (v "0.3.2") (d (list (d (n "deno-proc-macro-rules-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vx2bv8fy1w9d1k394fk49abbisi93f7x5h0a9b685gwvbzw4r9w")))

