(define-module (crates-io de no deno_plugin_starter) #:use-module (crates-io))

(define-public crate-deno_plugin_starter-0.1.4 (c (n "deno_plugin_starter") (v "0.1.4") (d (list (d (n "deno_core") (r "^0.81.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)))) (h "0ixjnlyz26rh00yklpmz06njmsmngaf48wjc6krgh5drv3miyrjh")))

