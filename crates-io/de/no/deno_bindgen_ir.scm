(define-module (crates-io de no deno_bindgen_ir) #:use-module (crates-io))

(define-public crate-deno_bindgen_ir-0.1.0 (c (n "deno_bindgen_ir") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rlwv0qk3ywckf9f3vgsz30mxdg8b88dnv3fgyy38200673ipgj0")))

