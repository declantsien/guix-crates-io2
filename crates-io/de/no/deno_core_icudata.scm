(define-module (crates-io de no deno_core_icudata) #:use-module (crates-io))

(define-public crate-deno_core_icudata-0.0.73 (c (n "deno_core_icudata") (v "0.0.73") (h "1c969grjf40nvn0j77yy28ark7ac7ccnkmk2y5rc7960k3m52fd1")))

(define-public crate-deno_core_icudata-0.73.0 (c (n "deno_core_icudata") (v "0.73.0") (h "1lwsiaij04fvxxpy1gkq99i6pjpjkx4m5aib89nx4ldn00dn0qkg")))

