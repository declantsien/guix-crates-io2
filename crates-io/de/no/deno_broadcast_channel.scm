(define-module (crates-io de no deno_broadcast_channel) #:use-module (crates-io))

(define-public crate-deno_broadcast_channel-0.1.0 (c (n "deno_broadcast_channel") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.88.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mc32crv74vpigl0x6am90qv6adnq1zrj43rrx9ybrhfw61a4py6")))

(define-public crate-deno_broadcast_channel-0.2.0 (c (n "deno_broadcast_channel") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.89.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "12sw7q277ca3ysz2ljmkh9835jy65brhc64qx7ii53ljg6mgikb9")))

(define-public crate-deno_broadcast_channel-0.3.0 (c (n "deno_broadcast_channel") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.90.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0frn82kzb8ggvcabjxnbanky479f26hic42admhwrx5pm8qk6a74")))

(define-public crate-deno_broadcast_channel-0.4.0 (c (n "deno_broadcast_channel") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.91.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bm66vnwb7a6wk8i4ddx13bb41c219m9dy5c3xn1zxbnyfz9ci7m")))

(define-public crate-deno_broadcast_channel-0.4.1 (c (n "deno_broadcast_channel") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.91.1") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dpnssfbfb0fiycv5c4rs806bgq4b38mfkcnij6sl5w7yzr7gkcs")))

(define-public crate-deno_broadcast_channel-0.5.0 (c (n "deno_broadcast_channel") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.93.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qjgv56971v6ihk23miafbpcypwgd6z4yk2kwrh4rws0lbdznn5p")))

(define-public crate-deno_broadcast_channel-0.6.0 (c (n "deno_broadcast_channel") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.94.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l289n233vnj62hfvl4kql4gfzhg1090lpjbs764bzwk706vgaf4")))

(define-public crate-deno_broadcast_channel-0.7.0 (c (n "deno_broadcast_channel") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.95.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nz7q9pzaiilvhbga4nhyzmsn7ipb7cnvylas2ybfpn28g69xfnc")))

(define-public crate-deno_broadcast_channel-0.8.0 (c (n "deno_broadcast_channel") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.96.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "052a8l3114a5f8fff2f78apvy67c3dg32ph3y1gb6wmzip0rnzmg")))

(define-public crate-deno_broadcast_channel-0.9.0 (c (n "deno_broadcast_channel") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.97.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nsg3823kmjyy5wzx7jkx99xxlm9w89vw3p5vpd0dab4q1f503ax")))

(define-public crate-deno_broadcast_channel-0.10.0 (c (n "deno_broadcast_channel") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.98.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dhsk5kiiq5lrb0c144dnwjywwj3zsyrd3drs2nsjyaaxpvvkbrh")))

(define-public crate-deno_broadcast_channel-0.11.0 (c (n "deno_broadcast_channel") (v "0.11.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.99.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gy7p5jhpry2mpmsi6wmdfm50sa0zmhn57v3vqig5k2njf69bn7k")))

(define-public crate-deno_broadcast_channel-0.12.0 (c (n "deno_broadcast_channel") (v "0.12.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.100.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q3xjk0q8xd7rlrbfxvbcpdac2cy6dk3cq0n2s6h19pfr7mc07bp")))

(define-public crate-deno_broadcast_channel-0.13.0 (c (n "deno_broadcast_channel") (v "0.13.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.101.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vmc8nz7llcc133rgzsj4534l802rq94x184grl8db03k0iqgn16")))

(define-public crate-deno_broadcast_channel-0.14.0 (c (n "deno_broadcast_channel") (v "0.14.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.102.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zbznd47zch0kh0x40zng9xjfzz8ikvg04dd176ykfng7kb2mkk9")))

(define-public crate-deno_broadcast_channel-0.15.0 (c (n "deno_broadcast_channel") (v "0.15.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.103.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qk0cm4j4yw98p10s52bhzmvrgsxa2x59r7nrldla73c76wx6jj0")))

(define-public crate-deno_broadcast_channel-0.16.0 (c (n "deno_broadcast_channel") (v "0.16.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.104.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0j0g1012354x6k4aijfj49cad4ws1p5j7778pw48ks138c97q8pj")))

(define-public crate-deno_broadcast_channel-0.17.0 (c (n "deno_broadcast_channel") (v "0.17.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.105.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zgkm0nip726snz99ybxfdmm2ah3p5dqlvcv4d3mg2dcbd1nci7j")))

(define-public crate-deno_broadcast_channel-0.18.0 (c (n "deno_broadcast_channel") (v "0.18.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.106.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "03ww2jlnlwgz3276mcrx57d7arxzr7c37ln28r33nbhc9yhy19j5")))

(define-public crate-deno_broadcast_channel-0.19.0 (c (n "deno_broadcast_channel") (v "0.19.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.107.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "17b468006kyrw4r6fgwxi7mg6vgxs983dns3mwn9fi53z3c8x9ih")))

(define-public crate-deno_broadcast_channel-0.20.0 (c (n "deno_broadcast_channel") (v "0.20.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.108.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jv54i6si1wwnz490m7hd8nrqxc94x39wf62pg2kj4iy2yr5fac8")))

(define-public crate-deno_broadcast_channel-0.21.0 (c (n "deno_broadcast_channel") (v "0.21.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.109.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x5xv96hb7cx1dnnv62npcnrzyzylvaxf5wfql9amvgbimms654r")))

(define-public crate-deno_broadcast_channel-0.22.0 (c (n "deno_broadcast_channel") (v "0.22.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.110.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bxcz8alvj4i93zyrzznk50jps1hrkpnhyab7hgpgglimlhzc1c9")))

(define-public crate-deno_broadcast_channel-0.23.0 (c (n "deno_broadcast_channel") (v "0.23.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.111.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cyg2hs55wfhjcdhfcid36vi0yilqq0saif11mdf3l5fjxs61xh1")))

(define-public crate-deno_broadcast_channel-0.24.0 (c (n "deno_broadcast_channel") (v "0.24.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dvik0sl54gbid0q0nb9asklx14axrh9gpyaznan2k2vxxs63gnh")))

(define-public crate-deno_broadcast_channel-0.25.0 (c (n "deno_broadcast_channel") (v "0.25.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.113.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "099rcwdidda29jyxy46w5bsmgvrvs9kdm3n2g9yah4wlzsbffci2")))

(define-public crate-deno_broadcast_channel-0.26.0 (c (n "deno_broadcast_channel") (v "0.26.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.114.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a3nvj7a2v157bxh2242pcq3vcc46fsy95ird90z96z9s95ddkcx")))

(define-public crate-deno_broadcast_channel-0.28.0 (c (n "deno_broadcast_channel") (v "0.28.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.116.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "11ngp51lyb6p4fm3mdifjmid9m9z9nqiklmw879999n87qspix9z")))

(define-public crate-deno_broadcast_channel-0.29.0 (c (n "deno_broadcast_channel") (v "0.29.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.117.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "19r9crgcv8vjp9z0w14v377a3m3p99liysidxizj75gxxymzk9ya")))

(define-public crate-deno_broadcast_channel-0.30.0 (c (n "deno_broadcast_channel") (v "0.30.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.118.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1126i5pbqmnrs1vs904na7fv95wivm4704c84hch7m1ynls8sk91")))

(define-public crate-deno_broadcast_channel-0.31.0 (c (n "deno_broadcast_channel") (v "0.31.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.119.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0y4i606hqcamj7vsmkrvb4krym7qndfyrp90sz9c7srhxdb32sr3")))

(define-public crate-deno_broadcast_channel-0.32.0 (c (n "deno_broadcast_channel") (v "0.32.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.120.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "185fwk4i3622a3p29rbixl2xqxa0l23d0yi9b24c9wdsxi6wl60q")))

(define-public crate-deno_broadcast_channel-0.33.0 (c (n "deno_broadcast_channel") (v "0.33.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.121.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v182masds1gjqybkd1zgg30cri05j6s8829yr4ygqrii02mwlv0")))

(define-public crate-deno_broadcast_channel-0.34.0 (c (n "deno_broadcast_channel") (v "0.34.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.122.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "10d7hrp09vzgcjsdfzyp2clgb7vid59nmim645g9cig01hbjslxq")))

(define-public crate-deno_broadcast_channel-0.35.0 (c (n "deno_broadcast_channel") (v "0.35.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.123.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "03sna36yvjidsknlazrpp0m7whvrx51hlszlvl880ss41whp7g7y")))

(define-public crate-deno_broadcast_channel-0.36.0 (c (n "deno_broadcast_channel") (v "0.36.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.124.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "117fsccx6jsykr8hjsar5jn73h72ra3ql5waxgz9pq1bcadl6vm9")))

(define-public crate-deno_broadcast_channel-0.37.0 (c (n "deno_broadcast_channel") (v "0.37.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.125.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ym9h16c55zqzxni5xhwgla7lc1kbd0m8ildmr01qwjp5gkx49fb")))

(define-public crate-deno_broadcast_channel-0.38.0 (c (n "deno_broadcast_channel") (v "0.38.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.126.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xifabfw2cmxzwm5b10lj166a5z39ssyc2yl4g064yxncw1q3bj8")))

(define-public crate-deno_broadcast_channel-0.39.0 (c (n "deno_broadcast_channel") (v "0.39.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.127.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m73r8j4554vlj05zh1vm81yzgp7cy82hgccqrgww4zl45j77lr7")))

(define-public crate-deno_broadcast_channel-0.40.0 (c (n "deno_broadcast_channel") (v "0.40.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.128.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f2sa8abix6zg0qx39hrppkppb41h0rlc7yfx99spz62c2d4g6qi")))

(define-public crate-deno_broadcast_channel-0.41.0 (c (n "deno_broadcast_channel") (v "0.41.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.129.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1y9waqk02lxchzdyfw3l0sih3kadrh21ili8rvv9bab4kl6g65zw")))

(define-public crate-deno_broadcast_channel-0.42.0 (c (n "deno_broadcast_channel") (v "0.42.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.130.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "09hnah15whaini0rc4fx63mp0dg9qqv1bgmaq6vcskazv5i3syfj")))

(define-public crate-deno_broadcast_channel-0.43.0 (c (n "deno_broadcast_channel") (v "0.43.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.131.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lqgzds9lhnapiyj4dlr05j84560pxw9kbsxc7vy4ry5zgxk50n7")))

(define-public crate-deno_broadcast_channel-0.44.0 (c (n "deno_broadcast_channel") (v "0.44.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.132.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qbqq4i6468b8akvfv1srbxgm0x123q6bpdzyww1jnnqxzg7432f")))

(define-public crate-deno_broadcast_channel-0.45.0 (c (n "deno_broadcast_channel") (v "0.45.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.133.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lwk1141ca1dll3jxgm2kr6s1602xm6ywsp7ay8dx2ygz1fpc0f9")))

(define-public crate-deno_broadcast_channel-0.46.0 (c (n "deno_broadcast_channel") (v "0.46.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.134.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lx488nswmfp6v00l4j8z3zchr8vcz2qmw10kn59a5libjlfwi6v")))

(define-public crate-deno_broadcast_channel-0.47.0 (c (n "deno_broadcast_channel") (v "0.47.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.135.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lpzf9janvff61jmvdkdf5adqfvil31pxgvdphhq6g2ikhb97v89")))

(define-public crate-deno_broadcast_channel-0.48.0 (c (n "deno_broadcast_channel") (v "0.48.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.136.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n6f7vw9mfjjdgl3jw2k1qk9f33n0wzsyv2wdj2d8rlq02s8s1mw")))

(define-public crate-deno_broadcast_channel-0.49.0 (c (n "deno_broadcast_channel") (v "0.49.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.137.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hx1ww4z2ddnrl6gv6rzzfy6hz3dsywamphl0qh2jcxh30qyb24f")))

(define-public crate-deno_broadcast_channel-0.50.0 (c (n "deno_broadcast_channel") (v "0.50.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.138.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qbv7g4hwfnaic5g10j3z7xx3iw48p18i51cr658kslff4dklbln")))

(define-public crate-deno_broadcast_channel-0.51.0 (c (n "deno_broadcast_channel") (v "0.51.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.139.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "03wcpf9c28w298nwbz2pckrlsv8fdig00hm6wjpxzsy59xc303lk")))

(define-public crate-deno_broadcast_channel-0.52.0 (c (n "deno_broadcast_channel") (v "0.52.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.140.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "15fx2zyg9hci02dhhyw2rzf9rrsdrrhzzza73l24kmhz85chsl0x")))

(define-public crate-deno_broadcast_channel-0.53.0 (c (n "deno_broadcast_channel") (v "0.53.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.141.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yzdq1xixqxmyipajyz6l92gzdmzn7pkdqhmxcib2wmkmm4wrzwx")))

(define-public crate-deno_broadcast_channel-0.54.0 (c (n "deno_broadcast_channel") (v "0.54.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.142.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "08j7nqgr1dq03bxx86xmaqx5ahwlq0rdf1n7mzbnx6swn4bczzl4")))

(define-public crate-deno_broadcast_channel-0.55.0 (c (n "deno_broadcast_channel") (v "0.55.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.143.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rdlqzcz522jfbkkzy9436b86m467xmwxlisf6d48frrp1xff0b7")))

(define-public crate-deno_broadcast_channel-0.56.0 (c (n "deno_broadcast_channel") (v "0.56.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.144.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1w68j8fqm2pl4bciavfc30zdr2lw3sga6kvvn59nvzksb3ihmrl1")))

(define-public crate-deno_broadcast_channel-0.57.0 (c (n "deno_broadcast_channel") (v "0.57.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.145.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1knfxvq5c7svl30micbldr81vgyan0h70bm6qcr1c88mzjrq7bln")))

(define-public crate-deno_broadcast_channel-0.58.0 (c (n "deno_broadcast_channel") (v "0.58.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.146.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "07wv6hz1zg27wvibl0kg3nshj62sr3f4x86jwywm6yvqkp0gzzki")))

(define-public crate-deno_broadcast_channel-0.59.0 (c (n "deno_broadcast_channel") (v "0.59.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.147.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bxqz5gd97yxjp8yvdzhka0ipm8qyxyfma3vq7is93m01jpwjpp8")))

(define-public crate-deno_broadcast_channel-0.60.0 (c (n "deno_broadcast_channel") (v "0.60.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.148.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "07ad4rd4zmgnpaph0nrcvp304nyl9q4y30ywhk8vm5r35vpycigc")))

(define-public crate-deno_broadcast_channel-0.61.0 (c (n "deno_broadcast_channel") (v "0.61.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.149.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0izwbl94l74yvlvmg22wsg4yzqx6lwwsm2i9ps891lc49l32iz0h")))

(define-public crate-deno_broadcast_channel-0.62.0 (c (n "deno_broadcast_channel") (v "0.62.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.150.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jxkr82nbkms3wpkmzynq57icmlfh646swx8n0dycypmwwjsfq5h")))

(define-public crate-deno_broadcast_channel-0.63.0 (c (n "deno_broadcast_channel") (v "0.63.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.151.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nqq7qq3jw730jdn34qfrrlbw3q3nq2803gnwbmwidsvpg968gx7")))

(define-public crate-deno_broadcast_channel-0.64.0 (c (n "deno_broadcast_channel") (v "0.64.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.152.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rz5dc4birq73lrkndrgf2vvz0q3mb5crfl0w6zisy94s5zsrdbd")))

(define-public crate-deno_broadcast_channel-0.65.0 (c (n "deno_broadcast_channel") (v "0.65.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.153.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gkdsbvmyfjmbc6wjpp1j0vi2lz5gdw42jg3mysnrwkxqgi91whn")))

(define-public crate-deno_broadcast_channel-0.66.0 (c (n "deno_broadcast_channel") (v "0.66.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.154.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ddr1m44r8m86krsixb6dllzxlryslhk6lrza3ppyq9235yq12bx")))

(define-public crate-deno_broadcast_channel-0.67.0 (c (n "deno_broadcast_channel") (v "0.67.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.155.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "13xcz1h8c3c7x1kjyjpl603j48zsfg8x13gz58fgxb24qyf9jlmg")))

(define-public crate-deno_broadcast_channel-0.68.0 (c (n "deno_broadcast_channel") (v "0.68.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.156.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0chiy1xamzlyrxrdfgha73iy5b5b1mds633rvlpjcxmghp79r962")))

(define-public crate-deno_broadcast_channel-0.69.0 (c (n "deno_broadcast_channel") (v "0.69.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.157.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qpw26l34d3mzsqpl038vwn3pswa75qvr61v7ndpy7liph71bn58")))

(define-public crate-deno_broadcast_channel-0.70.0 (c (n "deno_broadcast_channel") (v "0.70.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.158.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "11izbspsga5p3hpvks6dpi1a842qx4bmqr9b4i6bm758b1qg2zq8")))

(define-public crate-deno_broadcast_channel-0.71.0 (c (n "deno_broadcast_channel") (v "0.71.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.159.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "10sprg5zpnszpvq269s3zigrs2p96skv4h6s9m0cykrxd6m8ly07")))

(define-public crate-deno_broadcast_channel-0.72.0 (c (n "deno_broadcast_channel") (v "0.72.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.160.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jpw2jsf934vyz45gn5mv0wqwpssdzg1x3frxlyi1brjk8b0m2ii")))

(define-public crate-deno_broadcast_channel-0.73.0 (c (n "deno_broadcast_channel") (v "0.73.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.161.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nsrs1y76j838f05vfl1776n4w2k11cll1j4c1syhnf4a4fkn666")))

(define-public crate-deno_broadcast_channel-0.74.0 (c (n "deno_broadcast_channel") (v "0.74.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.162.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rnx0plzhmjf2wyvqy5s9rbn3kb8i5zh92iwr7b1qfhxdrlzyi1j")))

(define-public crate-deno_broadcast_channel-0.75.0 (c (n "deno_broadcast_channel") (v "0.75.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.163.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0622a2qf0immkx0r7z712h1ynqwwaw8xr1xmhddpm637dfsp4js9")))

(define-public crate-deno_broadcast_channel-0.76.0 (c (n "deno_broadcast_channel") (v "0.76.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.164.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rfpkwdmw4qky4n15l3jn99vbg3k8b8q9avypfyj7rkq95pnnaap")))

(define-public crate-deno_broadcast_channel-0.77.0 (c (n "deno_broadcast_channel") (v "0.77.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.165.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "14p824cbz5877px9alrpp13qwl93j96divgkmvfjilhxw7sf3cfx")))

(define-public crate-deno_broadcast_channel-0.78.0 (c (n "deno_broadcast_channel") (v "0.78.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.166.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kaqrbiyi31s9yck8l7nr1wm9dhlkg3q8wfcpl4i539zmhsjk9lf")))

(define-public crate-deno_broadcast_channel-0.79.0 (c (n "deno_broadcast_channel") (v "0.79.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.167.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0333md0r0j2wlnp1xljvzjlrmswg1cagdphaw887id49wpjpdszx")))

(define-public crate-deno_broadcast_channel-0.80.0 (c (n "deno_broadcast_channel") (v "0.80.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.168.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jx0bf7vb1q25szv2z452pi436av1jgc1gg7ddwqk72cf2v4slna")))

(define-public crate-deno_broadcast_channel-0.81.0 (c (n "deno_broadcast_channel") (v "0.81.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.169.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q4cyzhaqrg32vwz93vp2hqgbinsic2s03a8037why6rycbfl51j")))

(define-public crate-deno_broadcast_channel-0.82.0 (c (n "deno_broadcast_channel") (v "0.82.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.170.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "12vy4cbq3xq6lfjis6s7hpgv24x5p8p22ndnzn0gl9gfm56skfaz")))

(define-public crate-deno_broadcast_channel-0.83.0 (c (n "deno_broadcast_channel") (v "0.83.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.171.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "121grnsk86slxrr2x5y61hv6vyprspxh90rdjszflqswd8qxzb8y")))

(define-public crate-deno_broadcast_channel-0.84.0 (c (n "deno_broadcast_channel") (v "0.84.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.172.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m98q6hcqpnypi5malmw84q8wqr7v56p4plmkqaqi91w8h9fdw7n")))

(define-public crate-deno_broadcast_channel-0.85.0 (c (n "deno_broadcast_channel") (v "0.85.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.173.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "=1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d733kjfzl93gzkk2v2n4ailkv9fnglmln08hb9cyvlbp5v6lr36")))

(define-public crate-deno_broadcast_channel-0.86.0 (c (n "deno_broadcast_channel") (v "0.86.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.174.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "084x0rqm20jggrknzld8xbwp11a6l26q99dxn7043ajmgrib4k06")))

(define-public crate-deno_broadcast_channel-0.87.0 (c (n "deno_broadcast_channel") (v "0.87.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.175.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wv8hzrnjc1vspw775sxjhdv6f11p1pn9zjr0kgrbpqmdggx1snh")))

(define-public crate-deno_broadcast_channel-0.88.0 (c (n "deno_broadcast_channel") (v "0.88.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.176.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p0d2b7ayc4g0mx1sh8m3856fcyaprgifzzq72ay8im6z62d1na6")))

(define-public crate-deno_broadcast_channel-0.89.0 (c (n "deno_broadcast_channel") (v "0.89.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.177.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "05zxgzk55xynrvzmj0scwilj8328kiis863v08jvfiiz85j41z2a")))

(define-public crate-deno_broadcast_channel-0.90.0 (c (n "deno_broadcast_channel") (v "0.90.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.178.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mr8g26kg7lalnpj813x34qfmd04fl7c2r40dbq9090dmnklkyr2")))

(define-public crate-deno_broadcast_channel-0.91.0 (c (n "deno_broadcast_channel") (v "0.91.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.179.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cnfz9awymshcn1ksipf6acihn4m1jadnd3d7nm2adfj95sz0bz0")))

(define-public crate-deno_broadcast_channel-0.92.0 (c (n "deno_broadcast_channel") (v "0.92.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.180.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hhi2m5gskskrdan8d229yps2bczy9qj2hw24xn8hw57v7fp81cm")))

(define-public crate-deno_broadcast_channel-0.93.0 (c (n "deno_broadcast_channel") (v "0.93.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.181.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zmlvqnpqdsjh7xxrlak2bgydncgwkl565vyxz9x3ds2hfj8raiy")))

(define-public crate-deno_broadcast_channel-0.94.0 (c (n "deno_broadcast_channel") (v "0.94.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.182.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p5313c77r7ci0scqy4hdckkm5cfh8acq6vcafbmjdqqnz61nddm")))

(define-public crate-deno_broadcast_channel-0.95.0 (c (n "deno_broadcast_channel") (v "0.95.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.183.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "06jlmd69h326jzx5jh1pvcdn5dmvkll6yvirmx38mf9jnw8rvylr")))

(define-public crate-deno_broadcast_channel-0.96.0 (c (n "deno_broadcast_channel") (v "0.96.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.184.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "14d0lkvnhgyiy9d8ha1mf8bflgkpysm1nmd78zk714sbv9wban4h")))

(define-public crate-deno_broadcast_channel-0.97.0 (c (n "deno_broadcast_channel") (v "0.97.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.185.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ni35r22rgk6brdkhwc6akrjv0sm6hzgr65vmyzzchfcbpvvav82")))

(define-public crate-deno_broadcast_channel-0.98.0 (c (n "deno_broadcast_channel") (v "0.98.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.186.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "029ah8q72na929g3amyzkl51650pxby9fwv0rbl9f9l3m9yg2g9p")))

(define-public crate-deno_broadcast_channel-0.99.0 (c (n "deno_broadcast_channel") (v "0.99.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.187.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s1j7a8vac19xqk05nw5gk1x9jbdw10w43x8n0qpl1y19nxz02l8")))

(define-public crate-deno_broadcast_channel-0.100.0 (c (n "deno_broadcast_channel") (v "0.100.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.188.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qrszb9l1qf1w9lagdld27mw15pr9w8mlrjmdaym8pmcy00sr3mb")))

(define-public crate-deno_broadcast_channel-0.101.0 (c (n "deno_broadcast_channel") (v "0.101.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.189.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "03va89vhn7k7aj4sdp3r3b6lgvc8fd0bg2mxxn96p0r9viqr13gn")))

(define-public crate-deno_broadcast_channel-0.102.0 (c (n "deno_broadcast_channel") (v "0.102.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.190.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q28sxajaqz9yfl828xdrnflai8ghqjkifx5qzgwy3shl4995g8h")))

(define-public crate-deno_broadcast_channel-0.103.0 (c (n "deno_broadcast_channel") (v "0.103.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.191.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p5hqvdxsh7j5f3bqk0wavixssvkkl8d81q5ai1s7g37pna3sbpc")))

(define-public crate-deno_broadcast_channel-0.104.0 (c (n "deno_broadcast_channel") (v "0.104.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.194.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pv75lhhaaqym2jfhcw35qjwy24ji836xf23krfrm93l0pbblch1")))

(define-public crate-deno_broadcast_channel-0.105.0 (c (n "deno_broadcast_channel") (v "0.105.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "00l919v6m44zfyfv7xcrhvl3npjgal3wlnbwvns0rlq0y0xs1wmm")))

(define-public crate-deno_broadcast_channel-0.106.0 (c (n "deno_broadcast_channel") (v "0.106.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "17b5gnmzxh8y3s3i3qgx4j0xmfi38ah6l3f8f237cibxjk1i535s")))

(define-public crate-deno_broadcast_channel-0.107.0 (c (n "deno_broadcast_channel") (v "0.107.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.197.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z98vbgyc3gvjxn74nz42kl8r76lhxsv5a4nfysr8pd73907k5sv")))

(define-public crate-deno_broadcast_channel-0.108.0 (c (n "deno_broadcast_channel") (v "0.108.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.199.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gvy05akng2cvs511qwnlda6ja20b36ks8jwrhcck0m2j6k8qgmg")))

(define-public crate-deno_broadcast_channel-0.109.0 (c (n "deno_broadcast_channel") (v "0.109.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.200.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ld7l83d9rwhdhb54mxnz23fsl8fzvgwv6f1s89cd0jqf7n9axm6")))

(define-public crate-deno_broadcast_channel-0.110.0 (c (n "deno_broadcast_channel") (v "0.110.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.202.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jf67d3fb9saiixl9c1kkyb9f38j3vm904h7v2s07rpqrx8ks354")))

(define-public crate-deno_broadcast_channel-0.111.0 (c (n "deno_broadcast_channel") (v "0.111.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0djr7g1lvl9fsklcyxqzypz8fwvqjway6prbcipzbwh24vxy66lx")))

(define-public crate-deno_broadcast_channel-0.112.0 (c (n "deno_broadcast_channel") (v "0.112.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jb759npvr3y8gcwbpnmm7plfifwbsc837nz3q9fxinbwvgcwck6")))

(define-public crate-deno_broadcast_channel-0.113.0 (c (n "deno_broadcast_channel") (v "0.113.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.214.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l39vwmipg98dp891hia6v8f9svz2i2nxgvxjql06160mfkxmf2x")))

(define-public crate-deno_broadcast_channel-0.114.0 (c (n "deno_broadcast_channel") (v "0.114.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.218.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "14ngd653xxcczzfc82filmkn9fvvda8p6pxdswgy6dpm1yab5lqz")))

(define-public crate-deno_broadcast_channel-0.115.0 (c (n "deno_broadcast_channel") (v "0.115.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.222.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "19mv13sxy3n1a38f05axgwzqlhh0cvm8s67w0ybwdfj4a2fs8989")))

(define-public crate-deno_broadcast_channel-0.116.0 (c (n "deno_broadcast_channel") (v "0.116.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.224.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pimhyknc1kx3adh3yj5d08fwrc4816ql68gq73nxcf2abdmcmhi")))

(define-public crate-deno_broadcast_channel-0.117.0 (c (n "deno_broadcast_channel") (v "0.117.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.229.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wk2sqh6zwp1w85pq4z9qh2cdd3qwgjk9acc6116j15637s113rm")))

(define-public crate-deno_broadcast_channel-0.118.0 (c (n "deno_broadcast_channel") (v "0.118.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.230.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vx805g0kwpz6iigl5nwm0i3j0qjcv98qfdmci5yfdqqkfs8i83g")))

(define-public crate-deno_broadcast_channel-0.119.0 (c (n "deno_broadcast_channel") (v "0.119.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "013g7cxz3irhy3r82ii9q6nlb7m1sq6hjsrqh9lhvsngngfhlglp")))

(define-public crate-deno_broadcast_channel-0.120.0 (c (n "deno_broadcast_channel") (v "0.120.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g8nii4cpkcxw9irgndf3g1336jqz9nmppcallrqlj2bv1g7ygdj")))

(define-public crate-deno_broadcast_channel-0.121.0 (c (n "deno_broadcast_channel") (v "0.121.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "02dj562mkk2icxiqbf0dwkam0lxx6rs2h9pqv9541cbkkjr14wsy")))

(define-public crate-deno_broadcast_channel-0.122.0 (c (n "deno_broadcast_channel") (v "0.122.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yi64h3awkbl1gknik5di2grqjjsy2cyxmlykyks2dsv66gf5jwi")))

(define-public crate-deno_broadcast_channel-0.123.0 (c (n "deno_broadcast_channel") (v "0.123.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q7qaxsyyhvq13za8633j3hnqavs9j15i03chs6h92vbzr9pw10m")))

(define-public crate-deno_broadcast_channel-0.124.0 (c (n "deno_broadcast_channel") (v "0.124.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.243.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g7aiggnz7fkqxzm89m5ya6gk7prfhmnwwgvbrlmy4i7ccs3lx38")))

(define-public crate-deno_broadcast_channel-0.125.0 (c (n "deno_broadcast_channel") (v "0.125.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q37zrh2vflwvd5r0p3lhxfv34ni8s3kkw3znyg312yai9rfkwr7")))

(define-public crate-deno_broadcast_channel-0.126.0 (c (n "deno_broadcast_channel") (v "0.126.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "18cj81x0n4xg86yhyy758rwhgg5qpb5pk53pmwyv79mshybhj57r")))

(define-public crate-deno_broadcast_channel-0.127.0 (c (n "deno_broadcast_channel") (v "0.127.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z46dkhavyq2wfnai5p0igr77bv7gj72xy7iwsa4id1rj77yzybk")))

(define-public crate-deno_broadcast_channel-0.128.0 (c (n "deno_broadcast_channel") (v "0.128.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "10kkw7a5dawzkdcj7dk2fm32blvkga4pa8y2h8w4w37g0f3cql6x")))

(define-public crate-deno_broadcast_channel-0.129.0 (c (n "deno_broadcast_channel") (v "0.129.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.254.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "07ylzq5ag3glj7ri1p77xwn9ii8gdxsdpgnyjqs05073rmlf290b")))

(define-public crate-deno_broadcast_channel-0.130.0 (c (n "deno_broadcast_channel") (v "0.130.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.256.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "00lw0migswl7b2q8pmwich5y8lsa28493irp4fh9mfikv2ya5l1p")))

(define-public crate-deno_broadcast_channel-0.131.0 (c (n "deno_broadcast_channel") (v "0.131.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.260.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jdphm4319n4qgs39ipg8ivjkv0rmd6wd7428sny3yc7yc1vrwvl")))

(define-public crate-deno_broadcast_channel-0.132.0 (c (n "deno_broadcast_channel") (v "0.132.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.262.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z5xynpj6hnicy6gsy36qx3yxl2wkv1b0mshypnkkmrbxy4k90xs")))

(define-public crate-deno_broadcast_channel-0.133.0 (c (n "deno_broadcast_channel") (v "0.133.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.264.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "11q44mibkqfi2p7nc69sam7384hgs89miajzqgb8l0ckgjc0mj7q")))

(define-public crate-deno_broadcast_channel-0.134.0 (c (n "deno_broadcast_channel") (v "0.134.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.265.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zqk14bq32p4a4a5h6jvaqm37w6zs1v5wlk8wfj6b0kxwjk1j13p")))

(define-public crate-deno_broadcast_channel-0.135.0 (c (n "deno_broadcast_channel") (v "0.135.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.269.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "13jmwgw5kmby4jdvvvmvlsx9rv1m5yy1p8p1bbrzj9jwl29qgkpw")))

(define-public crate-deno_broadcast_channel-0.136.0 (c (n "deno_broadcast_channel") (v "0.136.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.270.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m2gkfnrijrrxvlgkk80rk1jr5680vycjmrj8r9rnc07gwwzvgza")))

(define-public crate-deno_broadcast_channel-0.137.0 (c (n "deno_broadcast_channel") (v "0.137.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1whg6jc75kvjsfjdl166vwqcn43vp8vkk1hs4ppb7xpp8g54dfqh")))

(define-public crate-deno_broadcast_channel-0.138.0 (c (n "deno_broadcast_channel") (v "0.138.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ivc2w9n1028z636sa8gxii4ghjcz095vgsm646irhs60qn3i2rp")))

(define-public crate-deno_broadcast_channel-0.139.0 (c (n "deno_broadcast_channel") (v "0.139.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1477dqnb1gvyvs9n5xlp5qph0qbcrvpxvw9wqrzy9fjwb6f5hwpf")))

(define-public crate-deno_broadcast_channel-0.140.0 (c (n "deno_broadcast_channel") (v "0.140.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0r38ngb3brj1qa9flnr87h0dzbnlclx20r0pa5w0fyw2whkh6d3q")))

(define-public crate-deno_broadcast_channel-0.141.0 (c (n "deno_broadcast_channel") (v "0.141.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.275.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jabg9a0cvynpwb73ih0vhrh4vkgq7id574zgnl4xlgs21lnx8kz")))

(define-public crate-deno_broadcast_channel-0.142.0 (c (n "deno_broadcast_channel") (v "0.142.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "13bzznl3f9fd1cbfvgp0rxba8ywjm83rywicx1r6nkrf69163760") (y #t)))

(define-public crate-deno_broadcast_channel-0.143.0 (c (n "deno_broadcast_channel") (v "0.143.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0w3bdsc6infswmd6dar9fnyr5x7pnqvry2d2wmmwscycnw80iw3h")))

(define-public crate-deno_broadcast_channel-0.144.0 (c (n "deno_broadcast_channel") (v "0.144.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.279.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1kl5sq3ic1sv8x4v3ll8rhz0bdn1hcw7nj0l308g49849fns7m6a")))

(define-public crate-deno_broadcast_channel-0.145.0 (c (n "deno_broadcast_channel") (v "0.145.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xvjc6pg4x1xj1i5b8p45gfw7iwa3jvcz9qw24ps163gy4vwq3hh")))

(define-public crate-deno_broadcast_channel-0.146.0 (c (n "deno_broadcast_channel") (v "0.146.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vb34r83ipz93cqxhyrq6dlmyx06wqkpmg7y795ag1nikgxnifh3")))

(define-public crate-deno_broadcast_channel-0.147.0 (c (n "deno_broadcast_channel") (v "0.147.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0iwhywzq4mw30qmqbfk8q2i1xl49r1xirn0my10f7yqx5qw0wsfs")))

