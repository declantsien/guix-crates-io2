(define-module (crates-io de no deno-libffi-sys) #:use-module (crates-io))

(define-public crate-deno-libffi-sys-0.0.1 (c (n "deno-libffi-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1cq9m0j3ym72rh1vv4f6zxizaf1jkadlwr486isfnxs0j04asxnj") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.2 (c (n "deno-libffi-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "0lfky32r1jv5dwlbacxjg34xbh0ypiri744l6rjhran8fc4qa4jg") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.3 (c (n "deno-libffi-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "0mgmpnr4l3b6n9l38ws1dci5rq8f3a2jw6gsrr23jkyc0m92p88m") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.4 (c (n "deno-libffi-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1z7hc1agqgnrwbrk86xlmdp2yvsh31xbxfi0hmx4xp6zw9r45dv9") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.5 (c (n "deno-libffi-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "13mbpbk564y9d3wpcrzrb9w2yb22rsxh6cs4qn98znz868a86rrl") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.6 (c (n "deno-libffi-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1yb36mpq7ldbyn2g9zzsgfl4vlma09vryfhrv03jq38ic81im4m5") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-deno-libffi-sys-0.0.7 (c (n "deno-libffi-sys") (v "0.0.7") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1f41c5kjsh8k5ap93kvk40dw1q949yp1yii9f03lnhn2jy2kwyyi") (f (quote (("system") ("complex")))) (l "ffi")))

