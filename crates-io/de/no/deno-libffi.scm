(define-module (crates-io de no deno-libffi) #:use-module (crates-io))

(define-public crate-deno-libffi-0.0.1 (c (n "deno-libffi") (v "0.0.1") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0dayxxlvjn3zswcy2187bq9m6amrdz5h4534piddq0c9ip7fxrr6") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.2 (c (n "deno-libffi") (v "0.0.2") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "04kk1av9c0ydcl4nan9gl252i30nlp8vqh541r8wpj2jk9wz4yfh") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.3 (c (n "deno-libffi") (v "0.0.3") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0qbpsjjx4p7awp0lmawblxp7iaxdq6vhpzidqq2k08z6m7p4r11s") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.4 (c (n "deno-libffi") (v "0.0.4") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1l5860ng2mlipmhff6wcs1p9zw063kj9ymyfvv8jba3inihlql68") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.5 (c (n "deno-libffi") (v "0.0.5") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "=0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "10axkad80jr8sb1ylzsnc5fhjxj33skhig9flacnj4ls7avd95z2") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.6 (c (n "deno-libffi") (v "0.0.6") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "=0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "00cx9mcixiw1aknjpdcc6vwcqgs5xy1rjsi0ycncvkxms8j5rya1") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

(define-public crate-deno-libffi-0.0.7 (c (n "deno-libffi") (v "0.0.7") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "deno-libffi-sys") (r "=0.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "11lwi5f9rrzdmq4y906maqjnr7x847i4iqiy8c5i53fl1gj7did8") (f (quote (("system" "deno-libffi-sys/system") ("complex"))))))

