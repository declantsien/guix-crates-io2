(define-module (crates-io de no deno_lockfile) #:use-module (crates-io))

(define-public crate-deno_lockfile-0.1.0 (c (n "deno_lockfile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "=1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.85") (d #t) (k 0)))) (h "1f0d78v84gpypvic0fdanm1a9np1irr1gbyxr5g7w0zk04kr6gsj")))

(define-public crate-deno_lockfile-0.2.0 (c (n "deno_lockfile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "=1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.85") (d #t) (k 0)))) (h "0ylaa9s3kv9jr0ygadx89np6dga68wmg8s3s89232w53fiky5dci")))

(define-public crate-deno_lockfile-0.3.0 (c (n "deno_lockfile") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0yd1n3lc6vs2i5szv21spzh4fc86c3izi7bqyvk9m033i4frijbi")))

(define-public crate-deno_lockfile-0.4.0 (c (n "deno_lockfile") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1q5ii6l0lwxrmdww90lg9hf2gjkigc48ic1fzv3b4h6df3d5jal3")))

(define-public crate-deno_lockfile-0.5.0 (c (n "deno_lockfile") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0dhs541mf67l7k342hpv7r94szininsxbsd2lc653lhgwqfilq29")))

(define-public crate-deno_lockfile-0.6.0 (c (n "deno_lockfile") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "192pa1gay6ks6m4sfxr5mavc9916ghszi0l7wwyfd7p8a9rznqhn")))

(define-public crate-deno_lockfile-0.7.0 (c (n "deno_lockfile") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "000mmhgk2l78am5knm9xfz79nprxj81xnzwwjl077mhyb1qim73w")))

(define-public crate-deno_lockfile-0.8.0 (c (n "deno_lockfile") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0h91k96v6s3fx820i39fi1p6257z7dinjkdd7nyi0g5iglpyqq18")))

(define-public crate-deno_lockfile-0.9.0 (c (n "deno_lockfile") (v "0.9.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)))) (h "17zmhhrbl2v7qzzbxa30rpm1ldlxjbpzra0j6mzzkkwf53r9w4vz")))

(define-public crate-deno_lockfile-0.10.0 (c (n "deno_lockfile") (v "0.10.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)))) (h "1lrh59w7cp1s8bmz654ckc56xv91s556aspqz9hjdmqff7cn5ach")))

(define-public crate-deno_lockfile-0.11.0 (c (n "deno_lockfile") (v "0.11.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)))) (h "1gyvwdfkgk9cl3bdqjr59mqlxsfpjbgxqc8b1n5zh5prvs8cl1fk")))

(define-public crate-deno_lockfile-0.12.0 (c (n "deno_lockfile") (v "0.12.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)))) (h "0c6gn5lavai0y3zrhar5nnq82s4dzsq0bnznjfcbmmkhybsckkc4")))

(define-public crate-deno_lockfile-0.13.0 (c (n "deno_lockfile") (v "0.13.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)))) (h "1j48jh8wj3g3383gy0bdkg0h96lrvhvdx7vw54l0qyxw37f8k2z8")))

(define-public crate-deno_lockfile-0.14.0 (c (n "deno_lockfile") (v "0.14.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hlrd5g4bzva56pdsysjbx8pgzv6clmnm0llysviplzcfylczkjl")))

(define-public crate-deno_lockfile-0.14.1 (c (n "deno_lockfile") (v "0.14.1") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1b14ig58wzq0h1zvyd915dwsgl8d7mmq96ag4ii1cl0mlivb2dda")))

(define-public crate-deno_lockfile-0.15.0 (c (n "deno_lockfile") (v "0.15.0") (d (list (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1v7skn0z069jf3g8kdawcvd6xrq96w9xfra9azhc662fza8wq7ry")))

(define-public crate-deno_lockfile-0.16.0 (c (n "deno_lockfile") (v "0.16.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "17cfaf48gqfbglzzvpim92499cixdq516ynngq8qilm95ddn5mic")))

(define-public crate-deno_lockfile-0.16.1 (c (n "deno_lockfile") (v "0.16.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zcnykx6s6aayacsb0mn66fm9ahivsykyp5rnyijisy4s24d5043")))

(define-public crate-deno_lockfile-0.16.2 (c (n "deno_lockfile") (v "0.16.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1vzm24xj26fvq30rgzfpmrmxzxffd6l5hy0gz9z14afd80vz6f0h")))

(define-public crate-deno_lockfile-0.17.0 (c (n "deno_lockfile") (v "0.17.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "142fap4v995wwfm9dq58h2ad8rnp0g1cbc5chkpjnjcyi9v8kxai")))

(define-public crate-deno_lockfile-0.17.1 (c (n "deno_lockfile") (v "0.17.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "=0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "05rjmzryv8zhaqwynjjjalbip9wxd6q9cnh72m0vs8vjhik3sry7")))

(define-public crate-deno_lockfile-0.17.2 (c (n "deno_lockfile") (v "0.17.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0xr00jll5i2bjx992cc1z2njlmpwqblz77apbxc0xinywri9zllc")))

(define-public crate-deno_lockfile-0.18.0 (c (n "deno_lockfile") (v "0.18.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1q3dv9c82ih8w3zs5nixapyq5i18syhwnysnv9lmpl4ya7d6xq6z")))

(define-public crate-deno_lockfile-0.18.1 (c (n "deno_lockfile") (v "0.18.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0wjld8il4hd3yvizxvj601xwscvky0np1bvdl0ryxfi9pr56kmh0")))

(define-public crate-deno_lockfile-0.18.2 (c (n "deno_lockfile") (v "0.18.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qjmvi1qdvmsabc0a9zc4s06r3xfn5bf4dhl06lv49a4rhrqcd4z")))

(define-public crate-deno_lockfile-0.19.0 (c (n "deno_lockfile") (v "0.19.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1szgn67pp8khnqr49m0r6n4jvclk04jgd2sh1yr5mwi4x6542dc8")))

(define-public crate-deno_lockfile-0.20.0 (c (n "deno_lockfile") (v "0.20.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0y8k89hqxk8j9105q7169svja42mi7h160c2b3cfwbgcba80y4i3")))

