(define-module (crates-io de no deno_napi) #:use-module (crates-io))

(define-public crate-deno_napi-0.1.0 (c (n "deno_napi") (v "0.1.0") (h "1ssysia39b126c1zcd656f1m1zv7wn7rrq08pjq7hhm1mp7a6kzq")))

(define-public crate-deno_napi-0.2.0 (c (n "deno_napi") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.154.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1bq6ac0ma99ks2vc7j6hah611zhb0ia3ids5bny7zg0hbnxbwm0z")))

(define-public crate-deno_napi-0.3.0 (c (n "deno_napi") (v "0.3.0") (d (list (d (n "deno_core") (r "^0.155.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1c25swr2kbd24mgvvidsndf5h5cwwlw6m91x2y0x33v3i71nq2ww")))

(define-public crate-deno_napi-0.4.0 (c (n "deno_napi") (v "0.4.0") (d (list (d (n "deno_core") (r "^0.156.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "01l2s0rbxapxgcykpy5n103rq4y54f0vn79zl9bgcg8jrs22m7id")))

(define-public crate-deno_napi-0.5.0 (c (n "deno_napi") (v "0.5.0") (d (list (d (n "deno_core") (r "^0.157.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rw2f11aabs8nz5w4f7ani4wfr4sl8indifbiqrjdqkb0yv70pwn")))

(define-public crate-deno_napi-0.6.0 (c (n "deno_napi") (v "0.6.0") (d (list (d (n "deno_core") (r "^0.158.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0cclrl3jglyrzr5jyiajf0irgddm7d1d64fmxlgfn1mgbr6c2bap")))

(define-public crate-deno_napi-0.7.0 (c (n "deno_napi") (v "0.7.0") (d (list (d (n "deno_core") (r "^0.159.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1y5cn2ymlg109h6brjhwrhqkha46zrriin4nbnchnkscsn30z0lq")))

(define-public crate-deno_napi-0.8.0 (c (n "deno_napi") (v "0.8.0") (d (list (d (n "deno_core") (r "^0.160.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rd29g9jf0mca6k35g9rzyxjgibl3065dlrkn9zy2mnxbg35vbxd")))

(define-public crate-deno_napi-0.9.0 (c (n "deno_napi") (v "0.9.0") (d (list (d (n "deno_core") (r "^0.161.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0q7lw6xdnj74w9rq2kz80kjd6whmil1j33v494227ax2vklkj1aj")))

(define-public crate-deno_napi-0.10.0 (c (n "deno_napi") (v "0.10.0") (d (list (d (n "deno_core") (r "^0.162.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1hqhqf2w3j4yh7l0p0yrdz4ajzf5mipfc8chj0plivqxdiw1md5p")))

(define-public crate-deno_napi-0.11.0 (c (n "deno_napi") (v "0.11.0") (d (list (d (n "deno_core") (r "^0.163.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "01fmzjfm6bilgy0m5nrg9nxc6vvz0ihxrvikaagjyvhigrcpkh0z")))

(define-public crate-deno_napi-0.12.0 (c (n "deno_napi") (v "0.12.0") (d (list (d (n "deno_core") (r "^0.164.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1akfyplr28prwacwg8br1ppigi6hb0906ydh31rhnkw8gq3lj551")))

(define-public crate-deno_napi-0.13.0 (c (n "deno_napi") (v "0.13.0") (d (list (d (n "deno_core") (r "^0.165.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1kjcjhd3pc4nrg4gbrb81jk86fq8xpbkcc4dcwhpn42gz8llw024")))

(define-public crate-deno_napi-0.14.0 (c (n "deno_napi") (v "0.14.0") (d (list (d (n "deno_core") (r "^0.166.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0sp366js34iaszwy2bqrsvlvkpph3p8xzb2vkgcsl6adxkgasq1h")))

(define-public crate-deno_napi-0.15.0 (c (n "deno_napi") (v "0.15.0") (d (list (d (n "deno_core") (r "^0.167.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1g1z2ig5m8cv0w3vz47qibj6rhi1qnmxhq3vwv2vbjf9awph5vky")))

(define-public crate-deno_napi-0.16.0 (c (n "deno_napi") (v "0.16.0") (d (list (d (n "deno_core") (r "^0.168.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0i71xy5pmv2dc814zin02mlaxcdg17j202c5syjsrliny1r8qmsh")))

(define-public crate-deno_napi-0.17.0 (c (n "deno_napi") (v "0.17.0") (d (list (d (n "deno_core") (r "^0.169.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1gs6kipdqd19mks6v88pylaf8mwxgq42dipbfl7xwcd0h0lrwkcs")))

(define-public crate-deno_napi-0.18.0 (c (n "deno_napi") (v "0.18.0") (d (list (d (n "deno_core") (r "^0.170.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1gsxhdgc8qm7sxn19gy2mcdyjpj0n0i1cc928k6cp6jq5l5fbbq5")))

(define-public crate-deno_napi-0.19.0 (c (n "deno_napi") (v "0.19.0") (d (list (d (n "deno_core") (r "^0.171.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0aalm7dsdmkjmjv3548m85w83c8f1vxspb3asy3bg9cm9y7wcan4")))

(define-public crate-deno_napi-0.20.0 (c (n "deno_napi") (v "0.20.0") (d (list (d (n "deno_core") (r "^0.172.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0qw06jssmq7x2zmn90p289jzj7wi7cbrvhjivyp92v61gb2k8qi5")))

(define-public crate-deno_napi-0.21.0 (c (n "deno_napi") (v "0.21.0") (d (list (d (n "deno_core") (r "^0.173.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1nshxk28lp47999hrn502am33y73vqn0i65rdw0vvs0zq6q7y1cp")))

(define-public crate-deno_napi-0.22.0 (c (n "deno_napi") (v "0.22.0") (d (list (d (n "deno_core") (r "^0.174.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1n5fin1j8dirnd08b9c2ycfvabhw63p7jc9gkf10iqc8crqh3n7h")))

(define-public crate-deno_napi-0.23.0 (c (n "deno_napi") (v "0.23.0") (d (list (d (n "deno_core") (r "^0.175.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1vf5nqikn5wv2nfsjymlrc5mrs8shcnmdj2pwga25yjwwvl5zxka")))

(define-public crate-deno_napi-0.24.0 (c (n "deno_napi") (v "0.24.0") (d (list (d (n "deno_core") (r "^0.176.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1ymr3ki0y6lf622h1qhm4xsf2d4il0yb469xh2n7kcjvi7rcdj0c")))

(define-public crate-deno_napi-0.25.0 (c (n "deno_napi") (v "0.25.0") (d (list (d (n "deno_core") (r "^0.177.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0x47bdhq34d04k1wd82x1pdrh64sazsdxqkd4hjlap5a7i0nyd1d")))

(define-public crate-deno_napi-0.26.0 (c (n "deno_napi") (v "0.26.0") (d (list (d (n "deno_core") (r "^0.178.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "066z41k113z5s2d7chfz97finihgnbqwn0xrim6z5x72pwwxkz6f")))

(define-public crate-deno_napi-0.27.0 (c (n "deno_napi") (v "0.27.0") (d (list (d (n "deno_core") (r "^0.179.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1r8fyz0rci52hnjavi9lzwizvp8184y58d3hc0w7znjlgrr5fvk0")))

(define-public crate-deno_napi-0.28.0 (c (n "deno_napi") (v "0.28.0") (d (list (d (n "deno_core") (r "^0.180.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "027fcvfzlsbals2zk2ax0jiabfli4d07bvr50j2z5651gbyp84zv")))

(define-public crate-deno_napi-0.29.0 (c (n "deno_napi") (v "0.29.0") (d (list (d (n "deno_core") (r "^0.181.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "09j4h3gnxn2s2bn7gm3zj9k1z9qqzpg2ip1533la9akl5f6mc17y")))

(define-public crate-deno_napi-0.30.0 (c (n "deno_napi") (v "0.30.0") (d (list (d (n "deno_core") (r "^0.182.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "18c1hqrcqdy153p875khl1kf46ls6fksdwrlx6zzmfjk7i2qpqw9")))

(define-public crate-deno_napi-0.31.0 (c (n "deno_napi") (v "0.31.0") (d (list (d (n "deno_core") (r "^0.183.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "06k837kvap4gn0gq3q4v6pzs9c0nm4wy9jsy6yxgvggpj5kbcvbp")))

(define-public crate-deno_napi-0.32.0 (c (n "deno_napi") (v "0.32.0") (d (list (d (n "deno_core") (r "^0.184.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0k0h1cpscqzizqvjahy2c7nq0irz8a45p5lsjrhym1dq7g8sq6x8")))

(define-public crate-deno_napi-0.33.0 (c (n "deno_napi") (v "0.33.0") (d (list (d (n "deno_core") (r "^0.185.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rznq6nwknqpp9zird3vgkqbr3g3f11g7ci4b6by5jhdzz860b81")))

(define-public crate-deno_napi-0.34.0 (c (n "deno_napi") (v "0.34.0") (d (list (d (n "deno_core") (r "^0.186.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0p6qb4qi556lw9zrihq7j8rj3syzrnlqvdw4l0768fc2hrvjv5yp")))

(define-public crate-deno_napi-0.35.0 (c (n "deno_napi") (v "0.35.0") (d (list (d (n "deno_core") (r "^0.187.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "11cpzxwa5p8k4jk0lv0jys2ispahykkffv6w98rn6wkp1x78dl2x")))

(define-public crate-deno_napi-0.36.0 (c (n "deno_napi") (v "0.36.0") (d (list (d (n "deno_core") (r "^0.188.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "10a5namhs6yhgsh8m6vwxqiyn46bwinisv0w5094gmfh1969kvm5")))

(define-public crate-deno_napi-0.37.0 (c (n "deno_napi") (v "0.37.0") (d (list (d (n "deno_core") (r "^0.189.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1gh3j0qbjaml83ri6n1c4q0gskxj0gvv47h95kz0simqlhxyrwcx")))

(define-public crate-deno_napi-0.38.0 (c (n "deno_napi") (v "0.38.0") (d (list (d (n "deno_core") (r "^0.190.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1y0bjmz72f82n7ki0bgkxywi4h7zlikgczyjfmcfhw8i5y7slc1b")))

(define-public crate-deno_napi-0.39.0 (c (n "deno_napi") (v "0.39.0") (d (list (d (n "deno_core") (r "^0.191.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "02905hh4d32c3fylv2xqp6rqx7bfhkm0bikvhqn4wzn90nqnny88")))

(define-public crate-deno_napi-0.40.0 (c (n "deno_napi") (v "0.40.0") (d (list (d (n "deno_core") (r "^0.194.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "179d4rdadjcq1cb3wbixbmk61pbjh6g0jzc4ql7ybnfsrvkvg9pa")))

(define-public crate-deno_napi-0.41.0 (c (n "deno_napi") (v "0.41.0") (d (list (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "01axa37176bjy9b3dzpg2qdsyljq7f3in2lbk3hr74y2a5v0wcnf")))

(define-public crate-deno_napi-0.42.0 (c (n "deno_napi") (v "0.42.0") (d (list (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0n99ih5lx8m6kyjqcdcnm49kyaz4yfip43jjjjahc9qfa3glpd7w")))

(define-public crate-deno_napi-0.43.0 (c (n "deno_napi") (v "0.43.0") (d (list (d (n "deno_core") (r "^0.197.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1m36rr7x9cqf8i1xy4r4qqxkvp8ms8sq12aiagffzgs0y95skayw")))

(define-public crate-deno_napi-0.44.0 (c (n "deno_napi") (v "0.44.0") (d (list (d (n "deno_core") (r "^0.199.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0klsr64krb0d23q7anrhz996jhjs9mg06wpf526wpxfw8d5m7wdf")))

(define-public crate-deno_napi-0.45.0 (c (n "deno_napi") (v "0.45.0") (d (list (d (n "deno_core") (r "^0.200.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "05wlmb3h58igvm8grkpvrd57cy9d8yfya78w69n48djld65xqnaz")))

(define-public crate-deno_napi-0.46.0 (c (n "deno_napi") (v "0.46.0") (d (list (d (n "deno_core") (r "^0.202.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1s5mw8ka3w5mznps4j8y0iph9jlwn0syzn8mc2qh85dn6kkr3ysq")))

(define-public crate-deno_napi-0.47.0 (c (n "deno_napi") (v "0.47.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0782hh7zaqzniyp6zgm4pfnl8pnygb6k6s8nnz5wxq2vbq420r9w")))

(define-public crate-deno_napi-0.48.0 (c (n "deno_napi") (v "0.48.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1pd4870yw991kv52r9xwgsxnjpy7kyc5s1zs3czhbdx3i938mcfs")))

(define-public crate-deno_napi-0.49.0 (c (n "deno_napi") (v "0.49.0") (d (list (d (n "deno_core") (r "^0.214.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0cqdrj6bmnghz5cr89f8inmb0a1v10bj3kw0qgdqjd1in3jjc1lk")))

(define-public crate-deno_napi-0.50.0 (c (n "deno_napi") (v "0.50.0") (d (list (d (n "deno_core") (r "^0.218.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0mg5gdgf6jdcf45a05mljar757220dl0hj7b1h81m3vib0xy5219")))

(define-public crate-deno_napi-0.51.0 (c (n "deno_napi") (v "0.51.0") (d (list (d (n "deno_core") (r "^0.222.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "105z8s2ri4fz6gc98wzqc1kshgg16zs1pyspbcr34rb6cz0cxq69")))

(define-public crate-deno_napi-0.52.0 (c (n "deno_napi") (v "0.52.0") (d (list (d (n "deno_core") (r "^0.224.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1a0b4d3ybbls2jfgbm9a9h2jhb3sn33mbdr3vhbd829vayn91qyi")))

(define-public crate-deno_napi-0.53.0 (c (n "deno_napi") (v "0.53.0") (d (list (d (n "deno_core") (r "^0.229.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1szz0ddlf9c662zh7r2c4dmn3wj3rxa9mlhmbnwcqm5skfg4yskg")))

(define-public crate-deno_napi-0.54.0 (c (n "deno_napi") (v "0.54.0") (d (list (d (n "deno_core") (r "^0.230.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "03m4pv1f3ii3jccx9546nqgzq5x373cri967wqn1wldzpnmalw3f")))

(define-public crate-deno_napi-0.55.0 (c (n "deno_napi") (v "0.55.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1dqa50qbnyinlhryzs28zv0c7dd8ixkyc2j704qnv9f9lh514msr")))

(define-public crate-deno_napi-0.56.0 (c (n "deno_napi") (v "0.56.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1z7n7ilmg8ziq63dra5ipxapp26g1z86vzazqalgz443qf5k5pcg")))

(define-public crate-deno_napi-0.57.0 (c (n "deno_napi") (v "0.57.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1wfn5988sr6lgnzd3yspm232yb8zfx0cnaiavjxg7nmz8449iqm2")))

(define-public crate-deno_napi-0.58.0 (c (n "deno_napi") (v "0.58.0") (d (list (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "04g8ajlws9vnknglqlw57q0hm67hmp3nm7s9v46alasdc457j35m")))

(define-public crate-deno_napi-0.59.0 (c (n "deno_napi") (v "0.59.0") (d (list (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "06g40m4iz8as5lmn318902g1w9jp7mp9n4wpas3ym2830bcjxr4z")))

(define-public crate-deno_napi-0.60.0 (c (n "deno_napi") (v "0.60.0") (d (list (d (n "deno_core") (r "^0.243.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "192mkslx0j2n570sk1fl8fbiirgca2jhv94dalrbw9g09dbpsl0b")))

(define-public crate-deno_napi-0.61.0 (c (n "deno_napi") (v "0.61.0") (d (list (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0jl8a9f8vfyg1qvzjbm2gx2gyfg6lxj28hjm57x13qdyzrjyw52c")))

(define-public crate-deno_napi-0.62.0 (c (n "deno_napi") (v "0.62.0") (d (list (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1c0lpziv9zl03934dbmlg521sm771w15fbmgdncwdffjm1q9bqcs")))

(define-public crate-deno_napi-0.63.0 (c (n "deno_napi") (v "0.63.0") (d (list (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "017jhcf4vlr7gzsfz1hvhd2qsp8z4084pkah9d3rj0cqcsnijzpz")))

(define-public crate-deno_napi-0.64.0 (c (n "deno_napi") (v "0.64.0") (d (list (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0iq6y46zm2hy657j6f861jh6j77q0xi4v0njxb4zaqgz93mydh65")))

(define-public crate-deno_napi-0.65.0 (c (n "deno_napi") (v "0.65.0") (d (list (d (n "deno_core") (r "^0.254.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rm3a6v69zxzv1nri6c18ghghrppqzhpj0kh66w41z2dqgm3sal0")))

(define-public crate-deno_napi-0.66.0 (c (n "deno_napi") (v "0.66.0") (d (list (d (n "deno_core") (r "^0.256.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "04c8zagmsabca7b5bzs170k3yqb3y4c0l07hngx2zq8j063s95yv")))

(define-public crate-deno_napi-0.67.0 (c (n "deno_napi") (v "0.67.0") (d (list (d (n "deno_core") (r "^0.260.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1cklag2hkc7jpqjqz0d8lcrrp0wccgvk2m7sl045h8zrl6qax1r8")))

(define-public crate-deno_napi-0.68.0 (c (n "deno_napi") (v "0.68.0") (d (list (d (n "deno_core") (r "^0.262.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "152wnc59wjy0zaznvjjd3n80rfg1h5z2lsxhyf28jy7srv3x5fgf")))

(define-public crate-deno_napi-0.69.0 (c (n "deno_napi") (v "0.69.0") (d (list (d (n "deno_core") (r "^0.264.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0q6h6cin9c6w4wi8jld10swi1x9ljhms6vxbbk9sl5lcsqfn0jdy")))

(define-public crate-deno_napi-0.70.0 (c (n "deno_napi") (v "0.70.0") (d (list (d (n "deno_core") (r "^0.265.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0pjqa1jcsg36lsdhbasmn3mq5xr6qdpq6wy87hads6gwka9wmw6c")))

(define-public crate-deno_napi-0.71.0 (c (n "deno_napi") (v "0.71.0") (d (list (d (n "deno_core") (r "^0.269.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "15vnarc3q211b75r87797d9ksm9k1i54v8jfa3lks7nhl61rbzwx")))

(define-public crate-deno_napi-0.72.0 (c (n "deno_napi") (v "0.72.0") (d (list (d (n "deno_core") (r "^0.270.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1ybjjf1r54qv3ja6jarwf2x5inwwkvpd7aw9r6b2naf3dkxklm8k")))

(define-public crate-deno_napi-0.73.0 (c (n "deno_napi") (v "0.73.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1180fm4z62178p6ad4z20rdixm7ckg1wc949ym6992wwg0sj5vah")))

(define-public crate-deno_napi-0.74.0 (c (n "deno_napi") (v "0.74.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0qnyy2g949mb8niiymgyh9w2kicn59zhnjhfk075w4spxm1rv41k")))

(define-public crate-deno_napi-0.75.0 (c (n "deno_napi") (v "0.75.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0qf48cmykaf1ir054vp2cmysp9pkmr4hw5765bvlgjgj4qahig9s")))

(define-public crate-deno_napi-0.76.0 (c (n "deno_napi") (v "0.76.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1bfd8v7xlra4f34lns0r5bqipy95z6df4ywxa9f6j160lryc10sz")))

(define-public crate-deno_napi-0.77.0 (c (n "deno_napi") (v "0.77.0") (d (list (d (n "deno_core") (r "^0.275.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1xa5dss6jaf98093lpk7xql5is8vvwmz9vn7q2s03k61pjjrvsmf")))

(define-public crate-deno_napi-0.78.0 (c (n "deno_napi") (v "0.78.0") (d (list (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1x2h6kd9xqxpb2sghpsib6vn7nzva85wh9qvksvf51gjbp04506c") (y #t)))

(define-public crate-deno_napi-0.79.0 (c (n "deno_napi") (v "0.79.0") (d (list (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "04h0kny4vwg28vad553hy31l90r6nlfzcpn0v8cmppyknzra8ip3")))

(define-public crate-deno_napi-0.80.0 (c (n "deno_napi") (v "0.80.0") (d (list (d (n "deno_core") (r "^0.279.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0vmx2rwrhp09ggryn080pwhy0fjmrsy50hx30yxnllrnxvciyrlx")))

(define-public crate-deno_napi-0.81.0 (c (n "deno_napi") (v "0.81.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0aqda6v0wq90bhii4l0sf259i2a3ms1chkkmgq9vmxd878q44li5")))

(define-public crate-deno_napi-0.82.0 (c (n "deno_napi") (v "0.82.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1dh37b82yg15zj5bk94lagsbsb547c2l74jzbhs5lx0iz9a2sxal")))

(define-public crate-deno_napi-0.83.0 (c (n "deno_napi") (v "0.83.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0hsnnihi890c7c0j427ln1zsdmcyb7bf83c8v7y60z2b93i3bm7p")))

