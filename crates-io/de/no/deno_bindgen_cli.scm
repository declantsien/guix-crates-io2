(define-module (crates-io de no deno_bindgen_cli) #:use-module (crates-io))

(define-public crate-deno_bindgen_cli-0.1.0 (c (n "deno_bindgen_cli") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "deno_bindgen_ir") (r "^0.1.0") (d #t) (k 0)) (d (n "dlopen2") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "04ickwqw9alvlcmg2zmqhj70nksdgyvm0ygmka9dylg3sqqbgv8l")))

