(define-module (crates-io de no deno-x86_64-pc-windows-msvc) #:use-module (crates-io))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.5-test1 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.5-test1") (h "1ddy93mw51svpgv7ws965l4ik4ddvlycdyp90l8iwxs2p432cwgc") (y #t)))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.5-temp3 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.5-temp3") (h "1ljxar9bnv2smx8yf0z28s6a54ys2yqwh3ypnk5rg3bp7zc7b9gi") (y #t)))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.5-temp4 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.5-temp4") (h "1vp346bzsl78hqxy2xfx9axr0f8rgqwc6wf31y6ra0b4cnfcb8vb") (y #t)))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.5-temp5 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.5-temp5") (h "1mr45zrfcrblkiq06q96s8cw3d3sjzxajbaic3bn427x8l8lxfaw") (y #t)))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.5 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.5") (h "18rc7r6mgjnl37xz7iarl334pxv6jypdbvgk7rkpjb8yqm1bfkyv")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.6 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.6") (h "1kijqr8nfsdqdl07qp37aa62bdsaqw6kfa1305xbjj1w9wqfgv9y")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.7 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.7") (h "1mniid6saz1pyq6qmay0a095waa0n47k93wggy10qg4594j5q4cz")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.8 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.8") (h "1jpz7f275nwmrz4g4sl0jxcn852xj141wngf845py2bqk99hs093")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.9 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.9") (h "1rz5bnblb6b81rv78df6s9l0xv1i6dkdgzvzfx924qbcw7q0msgv")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.10 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.10") (h "1byxfjnjizfk2vxji29f4db9zhh35jy9imlymhfvq4xaiwj6asls")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.3.11 (c (n "deno-x86_64-pc-windows-msvc") (v "0.3.11") (h "19cm04qc4id8rqxjqf1k3hl5aqqk0rh9jj4kwbss0l2lzncvs3yr")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.4.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.4.0") (h "0yy0m7grqfqk2qyb9wj9ghz9vj4qxj11vslhss74hach0y5awvmd")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.5.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.5.0") (h "1vkwm7b0d9wvxcrqkc0yccbfyacsh1zvq7pjkcym5ib0xa879glf")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.6.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.6.0") (h "0hdrwvmmf95qp4kyc4c39m4dzpj7b4ww6k2f12b3ykbqj6qdwyac")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.7.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.7.0") (h "16wndpfz46wn0790cywhzwpih3kj885xmpksih5708npdj7i4jgw")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.8.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.8.0") (h "1cxa62bkgw2dmp8zjiphkbwkcxn61q8r91sim1plj8kvg05b62xc")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.9.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.9.0") (h "0n173d2q3vw93iw562fr6zvqmd121h21s8bm4yf7fcxqcpcn6a31")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.10.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.10.0") (h "03dh1v35hiiylbxs36s663sgvrwagaka5vwccvb29z9i2cl3pnkc")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.11.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.11.0") (h "18067i0n3rzj8m7rk0a55gh89fa3wl5pjmkiypnfknlbajz7rn5z")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.12.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.12.0") (h "0sfl72kj4ikvbrawi2wv11pm1qcsw6831qal9sdavi4vrj3r0xwh")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.13.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.13.0") (h "11xh0g0ziy6gl4wxpcw64vz034p5gz9j7k6vvavhvzk57gk7v91n")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.14.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.14.0") (h "0csb37w23has0r8vw4x469ap5147arl4ijwqhfzcpb89jwzhpxqj")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.15.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.15.0") (h "1ndpp2cjq05m1y8kfl7aq4ar8agfqid9cy23dlhral2drdv72853")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.16.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.16.0") (h "1q1k017nh43sdcgw3zhdg5jxf3y74faf1k7yfg14phf3djcz8y27")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.17.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.17.0") (h "0vj1bl8ilp2q3dh898qns4c9p7yw6j148sxl5qgvd3190bs6h3md")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.18.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.18.0") (h "1rdcx23akfpzlmhy577j21s9f7nfwj0783zaa55r3amhqmhkqg67")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.19.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.19.0") (h "1vlqw1r98w3lkhvxwpshnnfinsqpx1w701ymgxqb7798fq1882a8")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.20.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.20.0") (h "002bkk0nbgs31j9bl7ndby4blkj4dc21p17384nszw7gz3ilnbm1")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.21.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.21.0") (h "0ddb50n1gfkibxw6nv2xs815jmvv5935qah8ff1sgfqy5zw58smy")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.22.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.22.0") (h "1y8yvxgmgxam1yghx6kii4qz34nsay4isy4mhqzrnlkg28vfl73f")))

(define-public crate-deno-x86_64-pc-windows-msvc-0.23.0 (c (n "deno-x86_64-pc-windows-msvc") (v "0.23.0") (h "0qp57dslh3i7hpmip3r59hjlj0rq04s02b892n39wyafgkm007x8")))

