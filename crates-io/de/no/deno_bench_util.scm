(define-module (crates-io de no deno_bench_util) #:use-module (crates-io))

(define-public crate-deno_bench_util-0.1.0 (c (n "deno_bench_util") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.88.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10ar8cibidscgj3a1zlk8kd95r0aw2fh4ghbwc91rbr10l9zv0bs")))

(define-public crate-deno_bench_util-0.2.0 (c (n "deno_bench_util") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.89.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00gxsmf578aq6n7ipcssbh0jclhd33vhvnv7bavnvcf905zpbbzr")))

(define-public crate-deno_bench_util-0.3.0 (c (n "deno_bench_util") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.90.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wlxlskr3hm26dcjddvgn4jq4kk72haid1z04pz8fhbnrsknniih")))

(define-public crate-deno_bench_util-0.4.0 (c (n "deno_bench_util") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.91.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0zarglhdnm7lxvxvv8wxhvqhgvnd1ikwgzfnapfsp0mml1kmj4")))

(define-public crate-deno_bench_util-0.5.0 (c (n "deno_bench_util") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.93.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9gg12nlmwrx72a01rr6spmx8gphriq3rfn9iz3s7nrb3dhrcmp")))

(define-public crate-deno_bench_util-0.6.0 (c (n "deno_bench_util") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.94.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hq0kf3zw9q31nawdz24yczz4g6a39h0in2qpsz0770r6l2s3mi1")))

(define-public crate-deno_bench_util-0.7.0 (c (n "deno_bench_util") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.95.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09vavbcg0c5jzxrrclgpv38kwl1rrz9yikxmn5dmmc8zfi6x3nc6")))

(define-public crate-deno_bench_util-0.8.0 (c (n "deno_bench_util") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.96.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvl3mcqlc4ay8la0br3p9j22i67bjy43gm10qqflkchyngrsvk7")))

(define-public crate-deno_bench_util-0.9.0 (c (n "deno_bench_util") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.97.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "194vhy2dn3p2l2vzv19g0x00w1qn6d4m3c7z9hgssjn0mynrrmw5")))

(define-public crate-deno_bench_util-0.10.0 (c (n "deno_bench_util") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.98.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03yi63x46z2i1k2lgg9lc2d24an6g5115ya6c15j9al7lhg0ax7n")))

(define-public crate-deno_bench_util-0.11.0 (c (n "deno_bench_util") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.99.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxhmsg521ln4a3dc4xhml2zi947bl5swjnhgh88jqs67r070bp1")))

(define-public crate-deno_bench_util-0.12.0 (c (n "deno_bench_util") (v "0.12.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.100.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "14701zhjapv6w1a0lq6i5p9rzz9nwk60rnz9pfwvv496lzynbk7q")))

(define-public crate-deno_bench_util-0.13.0 (c (n "deno_bench_util") (v "0.13.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.101.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13d0yijnl8kvmvnimgiw2yjkxd0apsns88n7inppn5ghny8whpvn")))

(define-public crate-deno_bench_util-0.14.0 (c (n "deno_bench_util") (v "0.14.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.102.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yw2i63ml95wnrfn7isi2rd0bcxkgqlaynz7jd3pr9qnf80rv80k")))

(define-public crate-deno_bench_util-0.15.0 (c (n "deno_bench_util") (v "0.15.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.103.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnkck4rq14nh6q0bcdkfpqn8l3milkgp2w9n290kms5lvqsch2c")))

(define-public crate-deno_bench_util-0.16.0 (c (n "deno_bench_util") (v "0.16.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.104.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02plfyv23jr28242qhwqp0qr6f597gv1rlzk2p7vb3pkdpx8vh05")))

(define-public crate-deno_bench_util-0.17.0 (c (n "deno_bench_util") (v "0.17.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.105.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10rsxinqpzm92sqpq5n94zm38g766lcgy5d684bsv3k4b59pagb6")))

(define-public crate-deno_bench_util-0.18.0 (c (n "deno_bench_util") (v "0.18.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.106.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00dhynyfzdb7a87q3wiy156f6sc8mlm58icrirzirj7qr24b08iv")))

(define-public crate-deno_bench_util-0.19.0 (c (n "deno_bench_util") (v "0.19.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.107.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bb3h6pp102zg3c0inds79iqp9xc3j8lnyldzlp9qbn8msh0yx37")))

(define-public crate-deno_bench_util-0.20.0 (c (n "deno_bench_util") (v "0.20.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.108.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0657yzarjx70z83hmk7wjxqj0qp6n5wmc3s2z7i6pfba2jdixlqx")))

(define-public crate-deno_bench_util-0.21.0 (c (n "deno_bench_util") (v "0.21.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.109.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmx7dxj8z6c0jkyb27jy4c9dirhz955k1danz45f9sabj24nmxi")))

(define-public crate-deno_bench_util-0.22.0 (c (n "deno_bench_util") (v "0.22.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.110.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gy6sdgpa53nyvknad9bc3l4a4c48ybyr39hyjksps93nl252gz6")))

(define-public crate-deno_bench_util-0.23.0 (c (n "deno_bench_util") (v "0.23.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.111.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jmy68ak52z5mcjywqa8av5ny9cs1fwm6i6m4r22hfx0pcg13v3x")))

(define-public crate-deno_bench_util-0.24.0 (c (n "deno_bench_util") (v "0.24.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1swhjplm474c1dvxq0r3za5i9y6m5zpsq38r9c8jgav48wmy506p")))

(define-public crate-deno_bench_util-0.25.0 (c (n "deno_bench_util") (v "0.25.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.113.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1115jsqal85mdjivks7d25ikq9lqkb384001zrag22n4milm69cs")))

(define-public crate-deno_bench_util-0.26.0 (c (n "deno_bench_util") (v "0.26.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.114.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rw49a4agbkm11iyzgkbr21ismnr7kqzf9md8zj0b7v1xqgy4paf")))

(define-public crate-deno_bench_util-0.28.0 (c (n "deno_bench_util") (v "0.28.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.116.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0frnpv11a49528jwriqc1jxbcxgq0sxkldhakidz2pnlqsmzbk0l")))

(define-public crate-deno_bench_util-0.29.0 (c (n "deno_bench_util") (v "0.29.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.117.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pijf464nkqnh31wdh66pqn0f3q4j7a3jvgqrgdm49m64qhrdrjb")))

(define-public crate-deno_bench_util-0.30.0 (c (n "deno_bench_util") (v "0.30.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.118.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jg734sd8bwi50r1rqkhi9yc6rg6q5qfnwnmhyx6v9x4g4x8kmpx")))

(define-public crate-deno_bench_util-0.31.0 (c (n "deno_bench_util") (v "0.31.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.119.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d7cyhmkqqdfw8qw15hy0q8d5nnny0qj5nqf4313qc6kwy3cvns1")))

(define-public crate-deno_bench_util-0.32.0 (c (n "deno_bench_util") (v "0.32.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.120.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kfgyiahbida1zdinfr8qmqjrscbbkm53pa8kh8zbsvavnd66c2s")))

(define-public crate-deno_bench_util-0.33.0 (c (n "deno_bench_util") (v "0.33.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.121.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "070b4m102ga2msky5dd0waqibdhcaayf3f076xalgqh8ba555fpd")))

(define-public crate-deno_bench_util-0.34.0 (c (n "deno_bench_util") (v "0.34.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.122.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gwwlrcqs4slv14kkk883s2iq4vn0ci115q3rx55gqxmb3jl2fg4")))

(define-public crate-deno_bench_util-0.35.0 (c (n "deno_bench_util") (v "0.35.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.123.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18kwp1xcyrx3h2nm8cjfp3qnc56bsv16643lzigid7wxg912nsvs")))

(define-public crate-deno_bench_util-0.36.0 (c (n "deno_bench_util") (v "0.36.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.124.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yclcn6b9gldjlpwmrngjcz9ysffbhljb6afvx51vf8amd5rc4vk")))

(define-public crate-deno_bench_util-0.37.0 (c (n "deno_bench_util") (v "0.37.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.125.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dny2xif7i1yf5r7csrz8zzxc6a62621kw4316b17ffr2vbk04m2")))

(define-public crate-deno_bench_util-0.38.0 (c (n "deno_bench_util") (v "0.38.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.126.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y75ykhhs6ja1m6z6fk1f2506kz1gd7mwf39rmv9i9ml9rjn7fm5")))

(define-public crate-deno_bench_util-0.39.0 (c (n "deno_bench_util") (v "0.39.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.127.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12d6dlmhmakz1i6rnc7zzj0xihfjslajww62km1fbzrhsn39fm2s")))

(define-public crate-deno_bench_util-0.40.0 (c (n "deno_bench_util") (v "0.40.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.128.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "00m4c75valq1vlw38s24r3bfq1kil3bh601a03b37lb4cbiw8llc")))

(define-public crate-deno_bench_util-0.41.0 (c (n "deno_bench_util") (v "0.41.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.129.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgcncfp8fwdxq937m2apkjwbb4qk5b24rlpsspn566dr819k05i")))

(define-public crate-deno_bench_util-0.42.0 (c (n "deno_bench_util") (v "0.42.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.130.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1x4ls7z1kd8r7i0xv5dycpfzgcx332l1975a04snlixylkwahwry")))

(define-public crate-deno_bench_util-0.43.0 (c (n "deno_bench_util") (v "0.43.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.131.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1vws7abv20h1ynpx8n606sb0wd3734rqygsn4ihkw1v6chh6arvx")))

(define-public crate-deno_bench_util-0.44.0 (c (n "deno_bench_util") (v "0.44.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.132.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncfniv1c0bjqs087zzcspdaca2h51lvgd4xcxxf6kzfma78xwpc")))

(define-public crate-deno_bench_util-0.45.0 (c (n "deno_bench_util") (v "0.45.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.133.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdigd98bck4gk7mg3mfkd8aam9caxvqb5gghh9vxgap8nrs8r76")))

(define-public crate-deno_bench_util-0.46.0 (c (n "deno_bench_util") (v "0.46.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.134.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0zhfjij34s41cwj7l3fsfh9fqc03g73w0waj5gca3afjfyn729zd")))

(define-public crate-deno_bench_util-0.47.0 (c (n "deno_bench_util") (v "0.47.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.135.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "124493f3h3blfs7gayiwslp2lpcgqwp0v4j8wx9hdyq4yhp24vki")))

(define-public crate-deno_bench_util-0.48.0 (c (n "deno_bench_util") (v "0.48.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.136.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1ha3lif936crwvx8ahq11jf23vqsl1kk35m5hpp0svmscksi53fa")))

(define-public crate-deno_bench_util-0.49.0 (c (n "deno_bench_util") (v "0.49.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.137.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "12fp8qkkab35bbf2cwl8llmlq2kjnyb72dp51p0612szaz24hx56")))

(define-public crate-deno_bench_util-0.50.0 (c (n "deno_bench_util") (v "0.50.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.138.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvi0q194sg30vjibl9f525k1skz123rxys3ixlbz450s8cayy5c")))

(define-public crate-deno_bench_util-0.51.0 (c (n "deno_bench_util") (v "0.51.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.139.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1pwsf2mq7ja4dxzl01gfm967rvzfa1dfz7r63yr54z6g2l4lq55i")))

(define-public crate-deno_bench_util-0.52.0 (c (n "deno_bench_util") (v "0.52.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.140.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1lb4m1gsh2dqw69679wgw3i59rm1alsg410z2dbdp77bywv3k2sv")))

(define-public crate-deno_bench_util-0.53.0 (c (n "deno_bench_util") (v "0.53.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.141.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnq6znclrvcl46w517gjynrd2jipjx3gz6q24xjrdap61frs65v")))

(define-public crate-deno_bench_util-0.54.0 (c (n "deno_bench_util") (v "0.54.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.142.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0lhxirk272q17jy6cl14z5kj5w9h5j1v2by58yhlqm5b19rf0fh6")))

(define-public crate-deno_bench_util-0.55.0 (c (n "deno_bench_util") (v "0.55.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.143.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "130c45zvf1rz68y185ih7qbj42rmjxi2dx1qgjmj94dsv63gn0q4")))

(define-public crate-deno_bench_util-0.56.0 (c (n "deno_bench_util") (v "0.56.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.144.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "00slx8isvs2h663yg9afm7hkjqnwgzp09jpkk1kqz7sj1q2yixbv")))

(define-public crate-deno_bench_util-0.57.0 (c (n "deno_bench_util") (v "0.57.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.145.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdj7iayx28jpcqwgw2ahjhr7spyl7my9wv8px21q9x913ymzw6z")))

(define-public crate-deno_bench_util-0.58.0 (c (n "deno_bench_util") (v "0.58.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.146.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0lj8zwxhqlhiyvhk2ywkbk6isw7way25njmsczyqhz9hcb4kb170")))

(define-public crate-deno_bench_util-0.59.0 (c (n "deno_bench_util") (v "0.59.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.147.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "09l1ihsgymlawqyaiz6kr3f0rydq3zv0mvrk99c4h8ms1zzia2pn")))

(define-public crate-deno_bench_util-0.60.0 (c (n "deno_bench_util") (v "0.60.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.148.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1hpc1gd97p9khdvr9fjj1j87ik2p31cjbv1mh8p0cmvv4f5xwsw9")))

(define-public crate-deno_bench_util-0.61.0 (c (n "deno_bench_util") (v "0.61.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.149.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "187lz7yxx4awgy29al8x984z2irs07fqkc46rfpczgk6wd6vcr7q")))

(define-public crate-deno_bench_util-0.62.0 (c (n "deno_bench_util") (v "0.62.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.150.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0br22kb8m5ih0wd4vlz2568pl7zaqpb42g8j1biwnklbcz448zyf")))

(define-public crate-deno_bench_util-0.63.0 (c (n "deno_bench_util") (v "0.63.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.151.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0nqgnvwq925bw52armxf03rmvi443n4jl7bmpj84zzgaksg19rq3")))

(define-public crate-deno_bench_util-0.64.0 (c (n "deno_bench_util") (v "0.64.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.152.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "153i82m7ri2mq4spbcivgh6810ym1k9s4swm9z2jbia1n7s4kflk")))

(define-public crate-deno_bench_util-0.65.0 (c (n "deno_bench_util") (v "0.65.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.153.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0282r7g1wd2b263hbb27n0l3ahvr4i02fx0mh98dd4c57ci4mjbq")))

(define-public crate-deno_bench_util-0.66.0 (c (n "deno_bench_util") (v "0.66.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.154.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0q35771b00p0clwrshj4vngyhq6l8cqycr3vayif69kj312n5l7s")))

(define-public crate-deno_bench_util-0.67.0 (c (n "deno_bench_util") (v "0.67.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.155.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "11c1qjmf4f7ikg15sr87ngf6jzwxwxh7hykbl9ysw64ckzh5p1mn")))

(define-public crate-deno_bench_util-0.68.0 (c (n "deno_bench_util") (v "0.68.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.156.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzq1rrlbfp3l8kgz6b9c8ls1qg10x5n2c0728bhrcqpa7wl5bml")))

(define-public crate-deno_bench_util-0.69.0 (c (n "deno_bench_util") (v "0.69.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.157.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "149mmbhlz4qn6gdmli1hjypq5fz6fr0glic8np5alxj7xj077iy4")))

(define-public crate-deno_bench_util-0.70.0 (c (n "deno_bench_util") (v "0.70.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.158.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1l5ds54diadi6vdb0mfz55bxyha69xdianwwnqw4qxln5hk1gs2q")))

(define-public crate-deno_bench_util-0.71.0 (c (n "deno_bench_util") (v "0.71.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.159.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdl9xfbgnw59zks24670fgakylp88mrx00qh6y6ivakyjg1b87v")))

(define-public crate-deno_bench_util-0.72.0 (c (n "deno_bench_util") (v "0.72.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.160.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 0)))) (h "167hciva6sl31mc8iszaqc2zckh98463nniaif5iy0awpa8npibw")))

(define-public crate-deno_bench_util-0.73.0 (c (n "deno_bench_util") (v "0.73.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.161.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.14.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gd9irrhfd8lwwyszy2dds67qd1ckra1ygbr9qj2bnd7di6ahgla")))

(define-public crate-deno_bench_util-0.74.0 (c (n "deno_bench_util") (v "0.74.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.162.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.14.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwa52xanmpx6g25dflv1cr8q366cf5vc04ycx0m0z8lnin4knfi")))

(define-public crate-deno_bench_util-0.75.0 (c (n "deno_bench_util") (v "0.75.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.163.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a25x97q01hky71766z7m6rkh7gqgi91rpg9xgqc1104dgzlk915")))

(define-public crate-deno_bench_util-0.76.0 (c (n "deno_bench_util") (v "0.76.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.164.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cini8pm8dny8y4wbwvi579b6j3hcadx3lfk5v6gxanh5ai0za9i")))

(define-public crate-deno_bench_util-0.77.0 (c (n "deno_bench_util") (v "0.77.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.165.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "036qf8czpdfx7qb590hjgc47xqsv122n1ppvj8rkcv2ys3v144g6")))

(define-public crate-deno_bench_util-0.78.0 (c (n "deno_bench_util") (v "0.78.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.166.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g4f9jbm2zpf6bxlgh9nvigvnsxcwrvgiz055cfprraakgcvj9y6")))

(define-public crate-deno_bench_util-0.79.0 (c (n "deno_bench_util") (v "0.79.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.167.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ngxf5w8rnc1qdld8fv3h3l34lw5bs7ddjb5941y8l2lmnv6ian6")))

(define-public crate-deno_bench_util-0.80.0 (c (n "deno_bench_util") (v "0.80.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.168.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ax8wh00kp6k5vwgxmpwhp38dvfm6l54mpff75nfc747r3v03pby")))

(define-public crate-deno_bench_util-0.81.0 (c (n "deno_bench_util") (v "0.81.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.169.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1n4sfxk8by0z3kabjzgkky1qs2lpcm6iyigppc8mc0cjs5520rsl")))

(define-public crate-deno_bench_util-0.82.0 (c (n "deno_bench_util") (v "0.82.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.170.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1b7hs8p25ib4q0q77a5r0lj32rr62vsz4hffvh7922zhkbkf0iwc")))

(define-public crate-deno_bench_util-0.83.0 (c (n "deno_bench_util") (v "0.83.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.171.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ab8ji69f50r7apdj4w8vbfqmpx2rsn4nirv70z06r4abdmha0sp")))

(define-public crate-deno_bench_util-0.84.0 (c (n "deno_bench_util") (v "0.84.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.172.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgdsr3v1mki5j8qqr8a2k9cajisw5m7hmbdj5chmb9sz4zp4v53")))

(define-public crate-deno_bench_util-0.85.0 (c (n "deno_bench_util") (v "0.85.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.173.0") (d #t) (k 0)) (d (n "once_cell") (r "=1.16.0") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvnq17ln5svlcgy6z8xfbzqzfyzwnlm2fd5q9rqksq6ni1cxgn2")))

(define-public crate-deno_bench_util-0.86.0 (c (n "deno_bench_util") (v "0.86.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.174.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "111mczzi6073iq9mab130i01spii5nm3l8aq2wivfv82gnis3b5g")))

(define-public crate-deno_bench_util-0.87.0 (c (n "deno_bench_util") (v "0.87.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.175.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1djjw15h2bfs8kbf4h1yaq1vbq6gccl0h8f8fy46wplqrj169p46")))

(define-public crate-deno_bench_util-0.88.0 (c (n "deno_bench_util") (v "0.88.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.176.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gws3xk9kbj1mw5s6r61x463knzfsicajawk2j5lgaxa0jk529na")))

(define-public crate-deno_bench_util-0.89.0 (c (n "deno_bench_util") (v "0.89.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.177.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qwwcjhj29wzxx0nlbjfc2lgi8r0z411wpdy4sa34m8x3dzjl2lj")))

(define-public crate-deno_bench_util-0.90.0 (c (n "deno_bench_util") (v "0.90.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.178.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0xg4j02fz276kw0gfxzarcn475jns5pagd9a2a7y1zy6agpclv")))

(define-public crate-deno_bench_util-0.91.0 (c (n "deno_bench_util") (v "0.91.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.179.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2avskwkdg1af00f7ngjspqcp22blhz5fpj8mkkndba2g1dnr4z")))

(define-public crate-deno_bench_util-0.92.0 (c (n "deno_bench_util") (v "0.92.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.180.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ygjlrh4aaqpizr8h1pic6wh8f75pyz9y287rl6xk2zg6s51cxdg")))

(define-public crate-deno_bench_util-0.93.0 (c (n "deno_bench_util") (v "0.93.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.181.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19llxzc3k1a0p9skfqalw9sx2fz2xlmhn98qpv69fy6zfawpix7p")))

(define-public crate-deno_bench_util-0.94.0 (c (n "deno_bench_util") (v "0.94.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.182.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iril3hq0br3lrlkxkd11fav6bahkbj8xkm1rs840d6jxdsi7dpn")))

(define-public crate-deno_bench_util-0.95.0 (c (n "deno_bench_util") (v "0.95.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.183.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "=1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10j32806jnnfamdzq2g01yxizn18jfpyf9qfa3knkrxnn96apcc0")))

(define-public crate-deno_bench_util-0.96.0 (c (n "deno_bench_util") (v "0.96.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.184.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cin36gk20a3qxj4fa8mzjhpd78ybhwv721cf40r7frwfdkw79wq")))

(define-public crate-deno_bench_util-0.97.0 (c (n "deno_bench_util") (v "0.97.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.185.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w35p341hjkraawdmr5k3c624glbnjvgrlarbdyhm0ckmjx29igi")))

(define-public crate-deno_bench_util-0.98.0 (c (n "deno_bench_util") (v "0.98.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.186.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnpvwcfwzza3cz20r3qdijdd5lrrpznlnb19wixyq090g3iz31j")))

(define-public crate-deno_bench_util-0.99.0 (c (n "deno_bench_util") (v "0.99.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.187.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iix0cz0f7s2jfljbdfns3wjy1r5g139lxabywkjbbpqgibbix5z")))

(define-public crate-deno_bench_util-0.100.0 (c (n "deno_bench_util") (v "0.100.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.188.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "15a8z2znkvv1663yi7x3vxsgiq7qcvn11gpqmjw4zcfnfiax7rcj")))

(define-public crate-deno_bench_util-0.101.0 (c (n "deno_bench_util") (v "0.101.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.189.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zhn97rdq0nf7jfw2jlx7sla45nva9p07mzssyh7k16c9zc5g9nj")))

(define-public crate-deno_bench_util-0.102.0 (c (n "deno_bench_util") (v "0.102.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.190.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pk86hdsmjfax1nk681y940bzc6y4ssffs78l4asbjqlv6dvjimn")))

(define-public crate-deno_bench_util-0.103.0 (c (n "deno_bench_util") (v "0.103.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.191.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kijn6vkz9jr8d25zhsm29jp5r93nh90xmz2inc05p1qypbfyxwj")))

(define-public crate-deno_bench_util-0.104.0 (c (n "deno_bench_util") (v "0.104.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.194.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wbi4hdagc99yx43cl40hwxyl5fkyphjsg6ssn93n2mkjgkgwm8m")))

(define-public crate-deno_bench_util-0.105.0 (c (n "deno_bench_util") (v "0.105.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mk5gwcakaxrpqpcfk68czq7sg47znfyljpig3mhzb343m92b9hi")))

(define-public crate-deno_bench_util-0.106.0 (c (n "deno_bench_util") (v "0.106.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8pjkkq7ijfdskd2v0b82pd5v55jr6h4z37f478pc9ih6kcxw4x")))

(define-public crate-deno_bench_util-0.107.0 (c (n "deno_bench_util") (v "0.107.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.197.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncqj2jr31fy46zgq29d7psgbgaq7kj98a85zm4gcrgq47zbb6fi")))

(define-public crate-deno_bench_util-0.108.0 (c (n "deno_bench_util") (v "0.108.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.199.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03h7sl0y78rbhlrmxby7rmnvqq9ip0dhqf2cmzmmjl113zxbq2fh")))

(define-public crate-deno_bench_util-0.109.0 (c (n "deno_bench_util") (v "0.109.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.200.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02nhsq0b85ld8jl7n0m0g76rvn3wnw7icl5vkqsdb2cs3vfx89b4")))

(define-public crate-deno_bench_util-0.110.0 (c (n "deno_bench_util") (v "0.110.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.202.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "03l9js546i6mx8r6mrrkcxz16n3md0qp0xngg35qkjvbb3yxwa7q")))

(define-public crate-deno_bench_util-0.111.0 (c (n "deno_bench_util") (v "0.111.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "127zl9ba63llh1m4bsqpyzijjlzh4wviw1ynvsd3hqdy8mvqrajv")))

(define-public crate-deno_bench_util-0.112.0 (c (n "deno_bench_util") (v "0.112.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5y0jnipyhx0187xh40yddspr8qb28ynr7k6jw8wsv3dvia1yh3")))

(define-public crate-deno_bench_util-0.113.0 (c (n "deno_bench_util") (v "0.113.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.214.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pampam02digvm13jsjm1n9na7z4bbicnq42a1jjd0fz8bnlck1w")))

(define-public crate-deno_bench_util-0.114.0 (c (n "deno_bench_util") (v "0.114.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.218.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07iqp0s5bnbwpdmipns4pn0iy1r2ngdwg5yhqx5rw1sw8ksk5q64")))

(define-public crate-deno_bench_util-0.115.0 (c (n "deno_bench_util") (v "0.115.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.222.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13j06rbas8p0sk5lp1frzp7dgh9y0zgbcm6rm3w315lbiw3xmz2h")))

(define-public crate-deno_bench_util-0.116.0 (c (n "deno_bench_util") (v "0.116.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.224.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nzhcc21bivfmjhs40ks8a5z4294cqymx0zcgmfhb0vr0v16n5p1")))

(define-public crate-deno_bench_util-0.117.0 (c (n "deno_bench_util") (v "0.117.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.229.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "16l08rr1x367c59wfkx8ksrq2mlj139mrbxkd9hag0i7zjf6dsck")))

(define-public crate-deno_bench_util-0.118.0 (c (n "deno_bench_util") (v "0.118.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.230.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "09xg9cm7spj3f2v98yl3i08wimmirr4jcs1s113caxpv9l5c7l22")))

(define-public crate-deno_bench_util-0.119.0 (c (n "deno_bench_util") (v "0.119.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "060r6rfyxg2a99a0ws1wfmcwfy2wdq41r48w40zhiafd4xp8k832")))

(define-public crate-deno_bench_util-0.120.0 (c (n "deno_bench_util") (v "0.120.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qyh6av5dmd7shqvmx7wsn8d9d20db6704vlf9mf99g2dzijlh03")))

(define-public crate-deno_bench_util-0.121.0 (c (n "deno_bench_util") (v "0.121.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aiszkwbz1zf03i7sq20y1szaav75fb8j3zjpzr5d1dcanpzgi47")))

(define-public crate-deno_bench_util-0.122.0 (c (n "deno_bench_util") (v "0.122.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqg168kba7al561c4hx96fpi72s4j4c2ljgs0q3kbxp468zpd0h")))

(define-public crate-deno_bench_util-0.123.0 (c (n "deno_bench_util") (v "0.123.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05aa9rnvab02jaqhl5jirzka7kgi34aspdwi50r8hixjma11w0f1")))

(define-public crate-deno_bench_util-0.124.0 (c (n "deno_bench_util") (v "0.124.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.243.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kfclyyfbgn9lhppdh9ncw97c67b67iz7z1zpi18mgflp4f3pib2")))

(define-public crate-deno_bench_util-0.125.0 (c (n "deno_bench_util") (v "0.125.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m6cmxdlj9c7ix7szsqx9gbrr0gy6jvqfj3vi7z1p6vksw445qm5")))

(define-public crate-deno_bench_util-0.126.0 (c (n "deno_bench_util") (v "0.126.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lbbr0jk1rx6ljcln7vg7jsiplf6b86b1jjz3sa258g8w3xb056q")))

(define-public crate-deno_bench_util-0.127.0 (c (n "deno_bench_util") (v "0.127.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "043in0h7x4wb4ah612lwmz2hrkn1fnvfbn78p6l3k0n6i7i1qx6b")))

(define-public crate-deno_bench_util-0.128.0 (c (n "deno_bench_util") (v "0.128.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m1mszgzqlxq42jy7j857rk4byxh9rbbfmybadvgxaik480f07mn")))

(define-public crate-deno_bench_util-0.129.0 (c (n "deno_bench_util") (v "0.129.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.254.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mx0w2ig4sx2r4jf7smqmc3d5jy3ffxv1gjyiiwkri2c6k0slz0d")))

(define-public crate-deno_bench_util-0.130.0 (c (n "deno_bench_util") (v "0.130.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.256.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ma4mk226fk140ddnxhfh90nfbqrhz07qyxcgpf5jqnxyrsibg6h")))

(define-public crate-deno_bench_util-0.131.0 (c (n "deno_bench_util") (v "0.131.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.260.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "14vkgng8llyr8h3v4ckr07pxb7fw3522vlfjgj9k4zn9h2cwzgzy")))

(define-public crate-deno_bench_util-0.132.0 (c (n "deno_bench_util") (v "0.132.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.262.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n0dp87qa7c9rw8c590mhcs1lzpv4gf9py0is6s9ma2jxk9y5ccd")))

(define-public crate-deno_bench_util-0.133.0 (c (n "deno_bench_util") (v "0.133.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.264.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dplc6lxgpyrs2jg3q2n1g57nyv16xjfi1jpcxajrk8fp8bjg98x")))

(define-public crate-deno_bench_util-0.134.0 (c (n "deno_bench_util") (v "0.134.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.265.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19j0n4nflianr6rq1ca4hk4jjmnai2i1y1hhkgpaa0jjkiqb50sq")))

(define-public crate-deno_bench_util-0.135.0 (c (n "deno_bench_util") (v "0.135.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.269.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "027pvk9j4v1a09gr9kbxgpr2vh8i5c4d5vwaxmgmfmxk8va4qm5v")))

(define-public crate-deno_bench_util-0.136.0 (c (n "deno_bench_util") (v "0.136.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.270.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xp6safagpcbz43zrk20scndl8chxxbc55ahhcn9jc4xhxflqy5p")))

(define-public crate-deno_bench_util-0.137.0 (c (n "deno_bench_util") (v "0.137.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zsqvfc348374jsmvln0qdga61dg5l7s7qrd714mb81fk5h1k0w9")))

(define-public crate-deno_bench_util-0.138.0 (c (n "deno_bench_util") (v "0.138.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fqj6pvf1ssdqsppw8ng77mz3s420f0bw3646ja065lm4dbhj3rz")))

(define-public crate-deno_bench_util-0.139.0 (c (n "deno_bench_util") (v "0.139.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a225xdimwl20q7jkyap50m7sblj4mkvyqd5ng7nfvfszaaz98dr")))

(define-public crate-deno_bench_util-0.140.0 (c (n "deno_bench_util") (v "0.140.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3kxfdhjsds7h2v4w8adx97fnspzk42yfnan6wy4w782sbkd4cz")))

(define-public crate-deno_bench_util-0.141.0 (c (n "deno_bench_util") (v "0.141.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.275.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j7084znfdqk9kw0wmm6yg6b60w8flirgbnl9ijraz136r00yxsy")))

(define-public crate-deno_bench_util-0.142.0 (c (n "deno_bench_util") (v "0.142.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j412yi0mbf4l4fwl20qln73jh7iswwclsq5sz7khg36sry3nd8x")))

(define-public crate-deno_bench_util-0.143.0 (c (n "deno_bench_util") (v "0.143.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vv3892xf4wm70kqq6z1mj0xyf8jrvc1hk5a0nn64l0606gaiwix")))

(define-public crate-deno_bench_util-0.144.0 (c (n "deno_bench_util") (v "0.144.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.279.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b8lsh4p8i4bg3vwlm9dqdd96pb7bgvvv95kdyfm14rhi43500fg")))

(define-public crate-deno_bench_util-0.145.0 (c (n "deno_bench_util") (v "0.145.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11j9bg395rc8kz7sxvrarzks5d7pfnyj9vdm31785bgnfhby7qwa")))

(define-public crate-deno_bench_util-0.146.0 (c (n "deno_bench_util") (v "0.146.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jp6k561vf7cxwpfhwzf1phfrsm1v17861p8c8w1kbm2ng2l69qx")))

(define-public crate-deno_bench_util-0.147.0 (c (n "deno_bench_util") (v "0.147.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nw3vsnxlbihw0skw8nfj3xs26jjqgimnh1awzvg2ygz574wd1kq")))

