(define-module (crates-io de no deno_plguin_starter) #:use-module (crates-io))

(define-public crate-deno_plguin_starter-0.1.2 (c (n "deno_plguin_starter") (v "0.1.2") (d (list (d (n "deno_core") (r "^0.54.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "1mi85z6azkfwrbjd4g8vzgq7irbwr0aa7z49h5v41mqzmibj45qp")))

