(define-module (crates-io de no deno-pal) #:use-module (crates-io))

(define-public crate-deno-pal-1.0.0-alpha (c (n "deno-pal") (v "1.0.0-alpha") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "091cayr7xf1mw0rph5psfxz9ljbi7bphplzgqq8fjakyv7r4f7rl") (y #t)))

