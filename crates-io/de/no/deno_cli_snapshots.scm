(define-module (crates-io de no deno_cli_snapshots) #:use-module (crates-io))

(define-public crate-deno_cli_snapshots-0.0.3 (c (n "deno_cli_snapshots") (v "0.0.3") (d (list (d (n "deno") (r "^0.15.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.0.3") (d #t) (k 1)))) (h "1cxs833763bryijd5y73ifvfic4hkyak7ihnf9r1jbv4b2lkzaay")))

(define-public crate-deno_cli_snapshots-0.18.0-preview (c (n "deno_cli_snapshots") (v "0.18.0-preview") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.0") (d #t) (k 1)))) (h "1af8ml5kg7wwz14gbgq2s5ixp9gcipn9bv81csg9zx2qmf1yrq0d")))

(define-public crate-deno_cli_snapshots-0.18.0-preview2 (c (n "deno_cli_snapshots") (v "0.18.0-preview2") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.0-preview2") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.18.0-preview2") (d #t) (k 1)))) (h "1xilfi44p8k8iwbzw7y35cngchw5sjcldizypbfd6cyiac7fqjxn")))

(define-public crate-deno_cli_snapshots-0.18.0-preview4 (c (n "deno_cli_snapshots") (v "0.18.0-preview4") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.1") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.18.1") (d #t) (k 1)))) (h "16h60c2mfv07rdqv77vb97s4xc8148dg46z11q130kq118hq65cw")))

(define-public crate-deno_cli_snapshots-0.18.1 (c (n "deno_cli_snapshots") (v "0.18.1") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.1") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.18.1") (d #t) (k 1)))) (h "0da5wpkr909901c5f31nki774icr6x198my3j3kiaaq7dlx1ijp8")))

(define-public crate-deno_cli_snapshots-0.18.2 (c (n "deno_cli_snapshots") (v "0.18.2") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.2") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.18.2") (d #t) (k 1)))) (h "0khlz1m8clqjabkbh0173fcjp8y2nb80w8bqradfsb9mwqb1rj5r")))

(define-public crate-deno_cli_snapshots-0.18.3 (c (n "deno_cli_snapshots") (v "0.18.3") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.18.3") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.18.3") (d #t) (k 1)))) (h "0nmvn8srnbnc7pbxq848z446jvpmaq1x70l1aa864pdgwkn5xiny")))

(define-public crate-deno_cli_snapshots-0.19.0 (c (n "deno_cli_snapshots") (v "0.19.0") (d (list (d (n "deno") (r "^0.19.0") (d #t) (k 2)) (d (n "deno_typescript") (r "^0.19.0") (d #t) (k 0)) (d (n "deno_typescript") (r "^0.19.0") (d #t) (k 1)))) (h "12x5iwxaqxhywyziwirzs3hbkc6mnlcqzj57z1i72qzd12lhfzzs")))

