(define-module (crates-io de no deno_whoami) #:use-module (crates-io))

(define-public crate-deno_whoami-0.1.0 (c (n "deno_whoami") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "whoami") (r "^1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "whoami") (r "^1.4") (d #t) (k 2)))) (h "1qwj44501b1saacabfcilh59gxg0msci67b7kkq2g2mrjam4qpp7")))

