(define-module (crates-io de no deno_bindgen) #:use-module (crates-io))

(define-public crate-deno_bindgen-0.1.0 (c (n "deno_bindgen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sx8mq3avaxxapgaip5idbr6gzv7jba6vfjw2q0xnqr5vgy9iid3")))

(define-public crate-deno_bindgen-0.1.1 (c (n "deno_bindgen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1afb6jv25g2g6b5zccqr45f4y3vgcwibmmm9ls8z1lqaggi7j2hc")))

(define-public crate-deno_bindgen-0.2.0 (c (n "deno_bindgen") (v "0.2.0") (d (list (d (n "deno_bindgen_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "173j1a38lnjch1drysxj2srqjqk8v9qdgc7p848li5qsxy128rzv")))

(define-public crate-deno_bindgen-0.3.0 (c (n "deno_bindgen") (v "0.3.0") (d (list (d (n "deno_bindgen_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y5kpn9f2rlr2sn5cnr5rc0krif9c9wzg6r375wr6a64cc50430j")))

(define-public crate-deno_bindgen-0.3.1 (c (n "deno_bindgen") (v "0.3.1") (d (list (d (n "deno_bindgen_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i74dxd2d4gh4p1dg9nl30imz8p849ms5p828fr0vq174sj1hxhq")))

(define-public crate-deno_bindgen-0.3.2 (c (n "deno_bindgen") (v "0.3.2") (d (list (d (n "deno_bindgen_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17m86551xqbwfwqcngijgb4hj20m5a7qzvapg76m45g9681a7ayz")))

(define-public crate-deno_bindgen-0.4.0 (c (n "deno_bindgen") (v "0.4.0") (d (list (d (n "deno_bindgen_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x4ff2fcqlzm2yc0q3qv49w21kfdbbwiwymnczxy7xwdd41g3fhf")))

(define-public crate-deno_bindgen-0.4.1 (c (n "deno_bindgen") (v "0.4.1") (d (list (d (n "deno_bindgen_macro") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "134rhmyl8v2vxgkw3rzyjx5imilkzrcyrnmdw6z4xnq0w513axjr")))

(define-public crate-deno_bindgen-0.5.0 (c (n "deno_bindgen") (v "0.5.0") (d (list (d (n "deno_bindgen_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0s0gf2y77y78x901rbijfam5dqvzyf8c8z812j9ypsd1mmjvqrqr")))

(define-public crate-deno_bindgen-0.5.1 (c (n "deno_bindgen") (v "0.5.1") (d (list (d (n "deno_bindgen_macro") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fwkq9gqxj78zv6zxrq4q3fnds1zid8dqd68sbfrd949h31q3kam")))

(define-public crate-deno_bindgen-0.6.0 (c (n "deno_bindgen") (v "0.6.0") (d (list (d (n "deno_bindgen_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "085hqz2pc3nppql51avd4dvckqlyr7gcc4j01mqf4gn4472n2mrg")))

(define-public crate-deno_bindgen-0.7.0 (c (n "deno_bindgen") (v "0.7.0") (d (list (d (n "deno_bindgen_macro") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01gqn7b8cih0s1rcsh5f79zvcl04g7j1y6dqw91l1xcj3n0sq9m1")))

(define-public crate-deno_bindgen-0.8.0 (c (n "deno_bindgen") (v "0.8.0") (d (list (d (n "deno_bindgen_macro") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1l1z425chsl2rfmcwzgx2mz9vbgvjsk0lcb7an3h4bxp312k935i")))

(define-public crate-deno_bindgen-0.8.1 (c (n "deno_bindgen") (v "0.8.1") (d (list (d (n "deno_bindgen_macro") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wlphady7wx2a0vxyci35zi36ym4fm9yxdq0lnr9imh0x5qd8yvv")))

(define-public crate-deno_bindgen-0.9.0-alpha (c (n "deno_bindgen") (v "0.9.0-alpha") (d (list (d (n "deno_bindgen_ir") (r "^0.1.0") (d #t) (k 0)) (d (n "deno_bindgen_macro") (r "^0.9.0-alpha") (d #t) (k 0)) (d (n "linkme") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1m740lvkhqz0mbnspjcaqmycn6vy5wkdjxm58xyyirck2ih5c841")))

