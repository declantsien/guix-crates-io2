(define-module (crates-io de no deno-proc-macro-rules-macros) #:use-module (crates-io))

(define-public crate-deno-proc-macro-rules-macros-0.3.0 (c (n "deno-proc-macro-rules-macros") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "065cbc9d50qpdxcf4z3ms6vgc528lcp530drl960xcmvbdwh5hj3")))

(define-public crate-deno-proc-macro-rules-macros-0.3.1 (c (n "deno-proc-macro-rules-macros") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ih2xl72296n6rn1gnhq9rkzcla7g9viq88zw0j4z4vsvaqnw1p4")))

(define-public crate-deno-proc-macro-rules-macros-0.3.2 (c (n "deno-proc-macro-rules-macros") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d84d5505hzbcsbj3njac7nim0hzdvfs84sphs8327j5nw9b6irh")))

