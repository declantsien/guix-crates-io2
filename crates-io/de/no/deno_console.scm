(define-module (crates-io de no deno_console) #:use-module (crates-io))

(define-public crate-deno_console-0.1.0 (c (n "deno_console") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.81.0") (d #t) (k 0)))) (h "1lfm3rwvyc7hyn9vxfi2z93mgn6gpxw00pm3n3mryfrbdd5zma5j")))

(define-public crate-deno_console-0.2.0 (c (n "deno_console") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.82.0") (d #t) (k 0)))) (h "11c0555ckd77xxjxvcmq64sg0kcrb9cr45g7k5swc528jyvmicg7")))

(define-public crate-deno_console-0.2.1 (c (n "deno_console") (v "0.2.1") (d (list (d (n "deno_core") (r "^0.83.0") (d #t) (k 0)))) (h "165vjkp93jk2l7mj2ir5llqd40plkfy39wh7wawam18iadi6kij9")))

(define-public crate-deno_console-0.3.0 (c (n "deno_console") (v "0.3.0") (d (list (d (n "deno_core") (r "^0.84.0") (d #t) (k 0)))) (h "0wdaaghj1ihc139gw7vspwipm2cp603hncdbqrmdy1rmgxjv1cbr")))

(define-public crate-deno_console-0.4.0 (c (n "deno_console") (v "0.4.0") (d (list (d (n "deno_core") (r "^0.85.0") (d #t) (k 0)))) (h "0qy2fl0lbbhy3fivgzr5l77qjrdf3p29kn9x1ais6rlq3a5j1ixp")))

(define-public crate-deno_console-0.5.0 (c (n "deno_console") (v "0.5.0") (d (list (d (n "deno_core") (r "^0.86.0") (d #t) (k 0)))) (h "0pr5a6wmi0rkn7nh2m4p435nq1cc37l40xj9yp7wcp1x0jnd8cga")))

(define-public crate-deno_console-0.6.0 (c (n "deno_console") (v "0.6.0") (d (list (d (n "deno_core") (r "^0.87.0") (d #t) (k 0)))) (h "1nvpflipilpnq8sxda762gn19iglljxs50kb8c096rai4chs0m4j")))

(define-public crate-deno_console-0.7.0 (c (n "deno_console") (v "0.7.0") (d (list (d (n "deno_core") (r "^0.88.0") (d #t) (k 0)))) (h "02ppqyxp8qyn7h5by7dm35z2nya30fd5mvbcglz7k0i2qz0p2w2h")))

(define-public crate-deno_console-0.7.1 (c (n "deno_console") (v "0.7.1") (d (list (d (n "deno_core") (r "^0.88.1") (d #t) (k 0)))) (h "1qwzv46qslnb251ldkkczswcf69haiscaik99ivzbfayvajqxdwd")))

(define-public crate-deno_console-0.8.0 (c (n "deno_console") (v "0.8.0") (d (list (d (n "deno_core") (r "^0.89.0") (d #t) (k 0)))) (h "0fa4m8jfqqgd2s9f55w8f5iibjf0jr8r6z05nx82ww97rx65yy0d")))

(define-public crate-deno_console-0.9.0 (c (n "deno_console") (v "0.9.0") (d (list (d (n "deno_core") (r "^0.90.0") (d #t) (k 0)))) (h "0yycl2mhmym4j62psdgs0g0fmrmxvqbb4zmvmcks5zpam6cd8xzk")))

(define-public crate-deno_console-0.10.0 (c (n "deno_console") (v "0.10.0") (d (list (d (n "deno_core") (r "^0.91.0") (d #t) (k 0)))) (h "1wll1437j61w11rcwld060fcx7bq419r6nzwdf34cxrnmlzrp79i")))

(define-public crate-deno_console-0.10.1 (c (n "deno_console") (v "0.10.1") (d (list (d (n "deno_core") (r "^0.91.1") (d #t) (k 0)))) (h "0gc6rxy95870h7yz4mr46ppcmglq18a6a27jgf2ivbz86xjd13rf")))

(define-public crate-deno_console-0.11.0 (c (n "deno_console") (v "0.11.0") (d (list (d (n "deno_core") (r "^0.93.0") (d #t) (k 0)))) (h "0wr1hgkscrby891riyg61149j7hbi62zf054wilxk8cyavq8dd7b")))

(define-public crate-deno_console-0.12.0 (c (n "deno_console") (v "0.12.0") (d (list (d (n "deno_core") (r "^0.94.0") (d #t) (k 0)))) (h "14001czhm1gwdwwzr0x6y3hvwcizada6fh9h2xgkqrlfbjrzz1d7")))

(define-public crate-deno_console-0.13.0 (c (n "deno_console") (v "0.13.0") (d (list (d (n "deno_core") (r "^0.95.0") (d #t) (k 0)))) (h "0cz66csvw01yq4q3vj5h9y4pj5bbc9lz9yhk913321nyz89c9wlg")))

(define-public crate-deno_console-0.14.0 (c (n "deno_console") (v "0.14.0") (d (list (d (n "deno_core") (r "^0.96.0") (d #t) (k 0)))) (h "1v63j1p7dwhg28dcimdnyzz87c4fkhgyv5p02hrsq1m16kshlklb")))

(define-public crate-deno_console-0.15.0 (c (n "deno_console") (v "0.15.0") (d (list (d (n "deno_core") (r "^0.97.0") (d #t) (k 0)))) (h "0ni6m5sw7cni55kjy4lnmp45nyhvwmsm649j8ry50hhv6bpsj0xc")))

(define-public crate-deno_console-0.16.0 (c (n "deno_console") (v "0.16.0") (d (list (d (n "deno_core") (r "^0.98.0") (d #t) (k 0)))) (h "0rbx405aqwwxdsr1f2chjj2z5fxj3p159vd6qmvaqay86929qq6s")))

(define-public crate-deno_console-0.17.0 (c (n "deno_console") (v "0.17.0") (d (list (d (n "deno_core") (r "^0.99.0") (d #t) (k 0)))) (h "0j3q2pbzjdm895lxd5ip01xpig3ig5a9c41ilyh8yr8v3bn8g24p")))

(define-public crate-deno_console-0.18.0 (c (n "deno_console") (v "0.18.0") (d (list (d (n "deno_core") (r "^0.100.0") (d #t) (k 0)))) (h "1qmjm4x5bl8m1dgrzlj4fijy2ybimlk9dznm0i3z63ldmylfjnsn")))

(define-public crate-deno_console-0.19.0 (c (n "deno_console") (v "0.19.0") (d (list (d (n "deno_core") (r "^0.101.0") (d #t) (k 0)))) (h "18s8vv6s08m3j24jb2lgiav387ifgcw3b957xx2x062l23nfmj75")))

(define-public crate-deno_console-0.20.0 (c (n "deno_console") (v "0.20.0") (d (list (d (n "deno_core") (r "^0.102.0") (d #t) (k 0)))) (h "0f4817ipgbj32v2dkyx4ahvk5gvcihyn8q7qfyjjnrp5lhsks9di")))

(define-public crate-deno_console-0.21.0 (c (n "deno_console") (v "0.21.0") (d (list (d (n "deno_core") (r "^0.103.0") (d #t) (k 0)))) (h "16x7vwp97zaj6x8y5pky6m3wxsb6lm0mm2n3iad8f9665ijm54al")))

(define-public crate-deno_console-0.22.0 (c (n "deno_console") (v "0.22.0") (d (list (d (n "deno_core") (r "^0.104.0") (d #t) (k 0)))) (h "0qqmp9y0ff43ky26cvkdrr3crykf7mpfm8s9xm194vx2n3j74aqd")))

(define-public crate-deno_console-0.23.0 (c (n "deno_console") (v "0.23.0") (d (list (d (n "deno_core") (r "^0.105.0") (d #t) (k 0)))) (h "0g6m4h20id3nwcs0jjk3rgrmk21qllv1dccd9gwsg4x81xp3dc3v")))

(define-public crate-deno_console-0.24.0 (c (n "deno_console") (v "0.24.0") (d (list (d (n "deno_core") (r "^0.106.0") (d #t) (k 0)))) (h "19l187zqnjxaqv66j5brcyx6vbqr3c8035ri1nsmys53phmlwh3n")))

(define-public crate-deno_console-0.25.0 (c (n "deno_console") (v "0.25.0") (d (list (d (n "deno_core") (r "^0.107.0") (d #t) (k 0)))) (h "1fpcpb8ccwg8jcwpbksxdqzsqllkks58y79mbh1nd8dvidxfhllr")))

(define-public crate-deno_console-0.26.0 (c (n "deno_console") (v "0.26.0") (d (list (d (n "deno_core") (r "^0.108.0") (d #t) (k 0)))) (h "1g63f42lpwhkjjfgl4x38kbpp8kx1nb52gk97fz627sbmrxpr3rh")))

(define-public crate-deno_console-0.27.0 (c (n "deno_console") (v "0.27.0") (d (list (d (n "deno_core") (r "^0.109.0") (d #t) (k 0)))) (h "1f1298vj7ka5p427y2nkykvffvx4q14qwfq4j70806q9wsv52jkl")))

(define-public crate-deno_console-0.28.0 (c (n "deno_console") (v "0.28.0") (d (list (d (n "deno_core") (r "^0.110.0") (d #t) (k 0)))) (h "14zqibsh6mgxhp72dk4mfwydp4p4i36sr3akbclj0h3wsj8hwjcw")))

(define-public crate-deno_console-0.29.0 (c (n "deno_console") (v "0.29.0") (d (list (d (n "deno_core") (r "^0.111.0") (d #t) (k 0)))) (h "02dxfdbsjw3iiwknbjbg47x11pnx51bh6839c7cw9bgpmm3bl5wk")))

(define-public crate-deno_console-0.30.0 (c (n "deno_console") (v "0.30.0") (d (list (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)))) (h "0k8sis7w8nvivkv26rnnsqik6wvgn76jk7galjl9bgsn48g3w08l")))

(define-public crate-deno_console-0.31.0 (c (n "deno_console") (v "0.31.0") (d (list (d (n "deno_core") (r "^0.113.0") (d #t) (k 0)))) (h "11fs5dp2rivwi5schswl7d65d1dysjhdw8rpwrvajaymmd3bnqdb")))

(define-public crate-deno_console-0.32.0 (c (n "deno_console") (v "0.32.0") (d (list (d (n "deno_core") (r "^0.114.0") (d #t) (k 0)))) (h "13dg4a3gzv2xx0x76w8sy9b9lmsy5vj6yiw91vssxpfgfhdkskbx")))

(define-public crate-deno_console-0.34.0 (c (n "deno_console") (v "0.34.0") (d (list (d (n "deno_core") (r "^0.116.0") (d #t) (k 0)))) (h "0z0c1vjvn5n9bggf10dl7xfd9859a4qhpg6rdnrl1zqk4ik6hxwn")))

(define-public crate-deno_console-0.35.0 (c (n "deno_console") (v "0.35.0") (d (list (d (n "deno_core") (r "^0.117.0") (d #t) (k 0)))) (h "11w99axwn6fk2lvr1zxl9p84ggy5g2x7w9f9brqn50654pkfv6pg")))

(define-public crate-deno_console-0.36.0 (c (n "deno_console") (v "0.36.0") (d (list (d (n "deno_core") (r "^0.118.0") (d #t) (k 0)))) (h "1imr05dh60488axnbral57rz0bd5pbli7n2ql1yqkj9jnyayzcvw")))

(define-public crate-deno_console-0.37.0 (c (n "deno_console") (v "0.37.0") (d (list (d (n "deno_core") (r "^0.119.0") (d #t) (k 0)))) (h "04ldqhwf5xx14a14n5mycn0cx6aalqs412sryss8lsl4wmid5kyx")))

(define-public crate-deno_console-0.38.0 (c (n "deno_console") (v "0.38.0") (d (list (d (n "deno_core") (r "^0.120.0") (d #t) (k 0)))) (h "0zppnicy3ivkzl122c2ngv4fkvh6781pf30vbxbz8kyisp7c65w7")))

(define-public crate-deno_console-0.39.0 (c (n "deno_console") (v "0.39.0") (d (list (d (n "deno_core") (r "^0.121.0") (d #t) (k 0)))) (h "0vw4afccjbfgwzvk1b9l2c7ssgxcprmm5cz6yay3h16s6dkscqgm")))

(define-public crate-deno_console-0.40.0 (c (n "deno_console") (v "0.40.0") (d (list (d (n "deno_core") (r "^0.122.0") (d #t) (k 0)))) (h "1kl7rblys5nydy1fz442k1ds894lkbm3jrl07c9358riq7bn3pdx")))

(define-public crate-deno_console-0.41.0 (c (n "deno_console") (v "0.41.0") (d (list (d (n "deno_core") (r "^0.123.0") (d #t) (k 0)))) (h "14f3vxq4ja3wljgsqxcjwc4kbh40fqcn34fa4f55cf99pb726z3j")))

(define-public crate-deno_console-0.42.0 (c (n "deno_console") (v "0.42.0") (d (list (d (n "deno_core") (r "^0.124.0") (d #t) (k 0)))) (h "10d5zwyvfrl2ppvls0xb3f8ckppx8n8m8yxrpfjxiz4bmq8igzqy")))

(define-public crate-deno_console-0.43.0 (c (n "deno_console") (v "0.43.0") (d (list (d (n "deno_core") (r "^0.125.0") (d #t) (k 0)))) (h "0ycpg59qyvwmcpiw040xca7bqb046yg6cv4nklwhyb04cc8lxlf9")))

(define-public crate-deno_console-0.44.0 (c (n "deno_console") (v "0.44.0") (d (list (d (n "deno_core") (r "^0.126.0") (d #t) (k 0)))) (h "0p9n9lz2sh2kbvwhclcaychrz3409bnifkqdvn8a7yi5sr0dsjh9")))

(define-public crate-deno_console-0.45.0 (c (n "deno_console") (v "0.45.0") (d (list (d (n "deno_core") (r "^0.127.0") (d #t) (k 0)))) (h "0997r26v5wvq6d1n74zdkx5zpca4ks7pwkn7q66n0xdjwwrmsa7g")))

(define-public crate-deno_console-0.46.0 (c (n "deno_console") (v "0.46.0") (d (list (d (n "deno_core") (r "^0.128.0") (d #t) (k 0)))) (h "0741nwnvia260h879fw5d20ld9cbv334ivm1xlry802j0qbb3vib")))

(define-public crate-deno_console-0.47.0 (c (n "deno_console") (v "0.47.0") (d (list (d (n "deno_core") (r "^0.129.0") (d #t) (k 0)))) (h "11nk2sghv9ry2hrk0kivac7qknc05m762y96rnkz2sr3nn55cxxb")))

(define-public crate-deno_console-0.48.0 (c (n "deno_console") (v "0.48.0") (d (list (d (n "deno_core") (r "^0.130.0") (d #t) (k 0)))) (h "0gnw6dz48ixv79ylqbyjc8iiw75q1b51rny8lzqi6r93d4nwkfik")))

(define-public crate-deno_console-0.49.0 (c (n "deno_console") (v "0.49.0") (d (list (d (n "deno_core") (r "^0.131.0") (d #t) (k 0)))) (h "1m4vz1mmkspp965kgcgvdf7203f60s144k49m761vyv8k2l6hqji")))

(define-public crate-deno_console-0.50.0 (c (n "deno_console") (v "0.50.0") (d (list (d (n "deno_core") (r "^0.132.0") (d #t) (k 0)))) (h "0iw94cjs127rcmcq6y569ccp0lgwkqwz1363jhw4dqi5jzw9l3da")))

(define-public crate-deno_console-0.51.0 (c (n "deno_console") (v "0.51.0") (d (list (d (n "deno_core") (r "^0.133.0") (d #t) (k 0)))) (h "0ly1qcx1dxab7va1kwdmz32mjhjxz85js352mrqv8mr9axp63s3f")))

(define-public crate-deno_console-0.52.0 (c (n "deno_console") (v "0.52.0") (d (list (d (n "deno_core") (r "^0.134.0") (d #t) (k 0)))) (h "0jvh5cawhxsr2syys67iayaiz747dcvfrhjn0l9swhb817ck1613")))

(define-public crate-deno_console-0.53.0 (c (n "deno_console") (v "0.53.0") (d (list (d (n "deno_core") (r "^0.135.0") (d #t) (k 0)))) (h "1q0z1kh33p79v1ql0swlf4p7x25d3fn8alls1ww3cl3932881cs2")))

(define-public crate-deno_console-0.54.0 (c (n "deno_console") (v "0.54.0") (d (list (d (n "deno_core") (r "^0.136.0") (d #t) (k 0)))) (h "0cq4xscg9mqn7ip8li2k2977l9ghcca1kgj5b3jr9bbhyqhywifv")))

(define-public crate-deno_console-0.55.0 (c (n "deno_console") (v "0.55.0") (d (list (d (n "deno_core") (r "^0.137.0") (d #t) (k 0)))) (h "0f1dnp2wak4alaln8b0is7zwpwq6pnsvxrnni0z3flfr5wkdkh10")))

(define-public crate-deno_console-0.56.0 (c (n "deno_console") (v "0.56.0") (d (list (d (n "deno_core") (r "^0.138.0") (d #t) (k 0)))) (h "0d4zmln925vnc582nnixkamq55d38azla3j1j050wm364y5wciyl")))

(define-public crate-deno_console-0.57.0 (c (n "deno_console") (v "0.57.0") (d (list (d (n "deno_core") (r "^0.139.0") (d #t) (k 0)))) (h "1hdp7jx500gfqy5gc0jldwxgh79jy80k798gy3b1p21rpdkrxqff")))

(define-public crate-deno_console-0.58.0 (c (n "deno_console") (v "0.58.0") (d (list (d (n "deno_core") (r "^0.140.0") (d #t) (k 0)))) (h "1yj9z3a2hdckbjg2apwxby01pcqmgzg3gs485gfb20y9bf2sajxw")))

(define-public crate-deno_console-0.59.0 (c (n "deno_console") (v "0.59.0") (d (list (d (n "deno_core") (r "^0.141.0") (d #t) (k 0)))) (h "03k8mbsqir17s43c2rq8fmsc8ql1bqv01xllr8l5cwv72jnqf8h8")))

(define-public crate-deno_console-0.60.0 (c (n "deno_console") (v "0.60.0") (d (list (d (n "deno_core") (r "^0.142.0") (d #t) (k 0)))) (h "1dpq6nh188w3bx4gab7h2xy53z5g3rw0r8fbh67602bljq3bwy40")))

(define-public crate-deno_console-0.61.0 (c (n "deno_console") (v "0.61.0") (d (list (d (n "deno_core") (r "^0.143.0") (d #t) (k 0)))) (h "154gx6874m7bpqmhi1xb1dqycnn84zgn24d7wfjlacybyg6mb2f8")))

(define-public crate-deno_console-0.62.0 (c (n "deno_console") (v "0.62.0") (d (list (d (n "deno_core") (r "^0.144.0") (d #t) (k 0)))) (h "0bsb48wibkfdvavmarbvw58avl86k0wd9ij3iyxkbjskzdgjc26x")))

(define-public crate-deno_console-0.63.0 (c (n "deno_console") (v "0.63.0") (d (list (d (n "deno_core") (r "^0.145.0") (d #t) (k 0)))) (h "0zblbfj6dvlh427mvgsrlak466zqxjmwis7k62qkzyswm2qb4ljw")))

(define-public crate-deno_console-0.64.0 (c (n "deno_console") (v "0.64.0") (d (list (d (n "deno_core") (r "^0.146.0") (d #t) (k 0)))) (h "0bcah27rawdr900qrjnwngzjynymc9d7zp9s7siwy6wacs6ms8jx")))

(define-public crate-deno_console-0.65.0 (c (n "deno_console") (v "0.65.0") (d (list (d (n "deno_core") (r "^0.147.0") (d #t) (k 0)))) (h "1983k89yrrpn6j7k7y8dkv2qzm7sgjm5pn694q329w1ayllyn139")))

(define-public crate-deno_console-0.66.0 (c (n "deno_console") (v "0.66.0") (d (list (d (n "deno_core") (r "^0.148.0") (d #t) (k 0)))) (h "1d36jbxfawjaj5i1mp9aiv3wk2msapsdsbpbq0xcnvkjzvc8phdb")))

(define-public crate-deno_console-0.67.0 (c (n "deno_console") (v "0.67.0") (d (list (d (n "deno_core") (r "^0.149.0") (d #t) (k 0)))) (h "1hi0igx8vvwhfzwpx9k4c896vbii3h2iq84bnjd8q9cjqvq6347p")))

(define-public crate-deno_console-0.68.0 (c (n "deno_console") (v "0.68.0") (d (list (d (n "deno_core") (r "^0.150.0") (d #t) (k 0)))) (h "1k9zdzyf3xcz58kvb588x7iqzxaw8ck6xjzrd2l12vkb0wn15gj2")))

(define-public crate-deno_console-0.69.0 (c (n "deno_console") (v "0.69.0") (d (list (d (n "deno_core") (r "^0.151.0") (d #t) (k 0)))) (h "105k6g52kybsc0xlanllq34ipvaxjlgaq7a1ifm02w89s15nik82")))

(define-public crate-deno_console-0.70.0 (c (n "deno_console") (v "0.70.0") (d (list (d (n "deno_core") (r "^0.152.0") (d #t) (k 0)))) (h "0ga2500imd4z0f3wkzmdis60v95d7zmcgpalvz0mp1lynd4di7nh")))

(define-public crate-deno_console-0.71.0 (c (n "deno_console") (v "0.71.0") (d (list (d (n "deno_core") (r "^0.153.0") (d #t) (k 0)))) (h "14199vxycqnz6jkdryigasx9yr0791pm5db81ywdg1ci3kiys3fj")))

(define-public crate-deno_console-0.72.0 (c (n "deno_console") (v "0.72.0") (d (list (d (n "deno_core") (r "^0.154.0") (d #t) (k 0)))) (h "1hvdknvddfk36gwqpba4r6sspf5w4dwq726w7lnpfz255xja4n1s")))

(define-public crate-deno_console-0.73.0 (c (n "deno_console") (v "0.73.0") (d (list (d (n "deno_core") (r "^0.155.0") (d #t) (k 0)))) (h "0v3cfr8yx2vmq8mywgiq85x1zq8zpcj5siad9iqk77zkqw542vib")))

(define-public crate-deno_console-0.74.0 (c (n "deno_console") (v "0.74.0") (d (list (d (n "deno_core") (r "^0.156.0") (d #t) (k 0)))) (h "1iq1f0f66sdrvv6izffik7vl778m9bpizvj3lg9yh1xkd05z1fc0")))

(define-public crate-deno_console-0.75.0 (c (n "deno_console") (v "0.75.0") (d (list (d (n "deno_core") (r "^0.157.0") (d #t) (k 0)))) (h "1w36dwdqfrpggasrv6cmyxk6m9p115ax1iacndxvhww05hk92316")))

(define-public crate-deno_console-0.76.0 (c (n "deno_console") (v "0.76.0") (d (list (d (n "deno_core") (r "^0.158.0") (d #t) (k 0)))) (h "1g92cj24v947svsf0576iwjqn3p9vlx66aq6lxash1yfqgs9q2ch")))

(define-public crate-deno_console-0.77.0 (c (n "deno_console") (v "0.77.0") (d (list (d (n "deno_core") (r "^0.159.0") (d #t) (k 0)))) (h "0gyagknb0swc619ih763rdgj5x0969v01s9b6xgqpvbz3sx7rhwf")))

(define-public crate-deno_console-0.78.0 (c (n "deno_console") (v "0.78.0") (d (list (d (n "deno_core") (r "^0.160.0") (d #t) (k 0)))) (h "0vh009jdxiz4awk4adsg6jx3xl1wjr8nysicczvgarcjmz1x3hqv")))

(define-public crate-deno_console-0.79.0 (c (n "deno_console") (v "0.79.0") (d (list (d (n "deno_core") (r "^0.161.0") (d #t) (k 0)))) (h "11nlirg3w7899bfpcis0304h306df8hhfnmjgdrv6y6ylw7jywlq")))

(define-public crate-deno_console-0.80.0 (c (n "deno_console") (v "0.80.0") (d (list (d (n "deno_core") (r "^0.162.0") (d #t) (k 0)))) (h "0jbhi1sx41y52pvfhv5xv88pcm6qppn7qvbngxdjrv7ajxs3z8b8")))

(define-public crate-deno_console-0.81.0 (c (n "deno_console") (v "0.81.0") (d (list (d (n "deno_core") (r "^0.163.0") (d #t) (k 0)))) (h "17im8k20i4qn8g855g9raskps6b6ayx2xa006g6cj0s78qvsadff")))

(define-public crate-deno_console-0.82.0 (c (n "deno_console") (v "0.82.0") (d (list (d (n "deno_core") (r "^0.164.0") (d #t) (k 0)))) (h "1j8vfry1sssnq97bl2rs6dcc5sqwhjlzls80p85qn6hchlljp0cd")))

(define-public crate-deno_console-0.83.0 (c (n "deno_console") (v "0.83.0") (d (list (d (n "deno_core") (r "^0.165.0") (d #t) (k 0)))) (h "0m393vq3br5b1q0d4jvl9n7xqw2vpqlnkmarkyb6a1d2fp8vgbp8")))

(define-public crate-deno_console-0.84.0 (c (n "deno_console") (v "0.84.0") (d (list (d (n "deno_core") (r "^0.166.0") (d #t) (k 0)))) (h "1a1lzm933i476g44knavyvamqpbinpnii3dk3byl0brq3gnfvz5i")))

(define-public crate-deno_console-0.85.0 (c (n "deno_console") (v "0.85.0") (d (list (d (n "deno_core") (r "^0.167.0") (d #t) (k 0)))) (h "096pn56963rj0vl8mh85y4qdp6qb701p0wchm45rdjzavqhhjcwb")))

(define-public crate-deno_console-0.86.0 (c (n "deno_console") (v "0.86.0") (d (list (d (n "deno_core") (r "^0.168.0") (d #t) (k 0)))) (h "0vgqmy59dh9xknfdncbb4zbb6v2ysckzhr7p9aihsa9xx4l8bk5y")))

(define-public crate-deno_console-0.87.0 (c (n "deno_console") (v "0.87.0") (d (list (d (n "deno_core") (r "^0.169.0") (d #t) (k 0)))) (h "0cic4f7bg1iyrgzbz8bz5bz6qra9d0lmcd2mrgpmqqsffdiq8xgh")))

(define-public crate-deno_console-0.88.0 (c (n "deno_console") (v "0.88.0") (d (list (d (n "deno_core") (r "^0.170.0") (d #t) (k 0)))) (h "1dh6h7vm3rda5a0dvq2j43a44vsc63xd43pxhxbk9d7qnbj1071a")))

(define-public crate-deno_console-0.89.0 (c (n "deno_console") (v "0.89.0") (d (list (d (n "deno_core") (r "^0.171.0") (d #t) (k 0)))) (h "0bn5k078za0nsimkmrp7a9laqmipya4340kl22an28jwcgs6rnkc")))

(define-public crate-deno_console-0.90.0 (c (n "deno_console") (v "0.90.0") (d (list (d (n "deno_core") (r "^0.172.0") (d #t) (k 0)))) (h "144pg59y483y920n4qicnlqqjd59yp41fapl0ldsfigq0b3d55r0")))

(define-public crate-deno_console-0.91.0 (c (n "deno_console") (v "0.91.0") (d (list (d (n "deno_core") (r "^0.173.0") (d #t) (k 0)))) (h "0994hp4gjn0n1jm06li0c6hzkxz6w3rjb8zssxyi0kbran4av42j")))

(define-public crate-deno_console-0.92.0 (c (n "deno_console") (v "0.92.0") (d (list (d (n "deno_core") (r "^0.174.0") (d #t) (k 0)))) (h "1kld8239h1rhjypqc1csk9ywcrfxp75hla282c787ad9cv5kn6am")))

(define-public crate-deno_console-0.93.0 (c (n "deno_console") (v "0.93.0") (d (list (d (n "deno_core") (r "^0.175.0") (d #t) (k 0)))) (h "1h07vih2vm5g0lx6s10bbw1k8zjz78s6l9j6xp7jhhpyrzw2qs77")))

(define-public crate-deno_console-0.94.0 (c (n "deno_console") (v "0.94.0") (d (list (d (n "deno_core") (r "^0.176.0") (d #t) (k 0)))) (h "1s3ga9brz4l8rrzwc7s73rz77axcpwahykassxz10iss7ybncdkr")))

(define-public crate-deno_console-0.94.1 (c (n "deno_console") (v "0.94.1") (d (list (d (n "deno_core") (r "^0.176.0") (d #t) (k 0)))) (h "1gvxr83d5d6x52aanc8x8mihhrqinmdjrsa6rjimdk875v4in389")))

(define-public crate-deno_console-0.95.0 (c (n "deno_console") (v "0.95.0") (d (list (d (n "deno_core") (r "^0.177.0") (d #t) (k 0)))) (h "1ywkjfy9gc3l43n3np2hyw1adizdskrdvaz3gygpmspkz7hs5amp")))

(define-public crate-deno_console-0.96.0 (c (n "deno_console") (v "0.96.0") (d (list (d (n "deno_core") (r "^0.178.0") (d #t) (k 0)))) (h "1h5yzp2fprqzidic68x8nq7fbxbw4ip8h6mgcygjiyi05530ib00")))

(define-public crate-deno_console-0.97.0 (c (n "deno_console") (v "0.97.0") (d (list (d (n "deno_core") (r "^0.179.0") (d #t) (k 0)))) (h "13qcdls4imh7b7js2fhjrkj31n49rsgf1ksn54s0bpm82rqn2vcn")))

(define-public crate-deno_console-0.98.0 (c (n "deno_console") (v "0.98.0") (d (list (d (n "deno_core") (r "^0.180.0") (d #t) (k 0)))) (h "19p4mmkv57ybchazbw8yh2nh6aicbdyfsvnzvpdg2lrisl9nvwgc")))

(define-public crate-deno_console-0.99.0 (c (n "deno_console") (v "0.99.0") (d (list (d (n "deno_core") (r "^0.181.0") (d #t) (k 0)))) (h "08q85s7ymwfk8fr94ai6rpgvywjzihadq5jryxm6svqj528mwaqn")))

(define-public crate-deno_console-0.100.0 (c (n "deno_console") (v "0.100.0") (d (list (d (n "deno_core") (r "^0.182.0") (d #t) (k 0)))) (h "0hva1wphrbzj38x2qz8hmhngqzmrx0fgb3137s9jkh9kj180jj6m")))

(define-public crate-deno_console-0.101.0 (c (n "deno_console") (v "0.101.0") (d (list (d (n "deno_core") (r "^0.183.0") (d #t) (k 0)))) (h "1arbjhg23a8g52b7qwijpg7bvw034a7q3fdx5fsvwvrllc6f305r")))

(define-public crate-deno_console-0.102.0 (c (n "deno_console") (v "0.102.0") (d (list (d (n "deno_core") (r "^0.184.0") (d #t) (k 0)))) (h "1dbf2y2hswnnpvqyqsjhb5j8cskcc6qrfdnma1xr29v4xj0f0qfp")))

(define-public crate-deno_console-0.103.0 (c (n "deno_console") (v "0.103.0") (d (list (d (n "deno_core") (r "^0.185.0") (d #t) (k 0)))) (h "07z5c5xapf7ax9znyf66l9c1b6djxy0x70f56mkps2syxf44sll0")))

(define-public crate-deno_console-0.104.0 (c (n "deno_console") (v "0.104.0") (d (list (d (n "deno_core") (r "^0.186.0") (d #t) (k 0)))) (h "1ki6n7mskc5akj7pd5blazrr51w0i7vql5wvfjz81f4p1v6vqcfi")))

(define-public crate-deno_console-0.105.0 (c (n "deno_console") (v "0.105.0") (d (list (d (n "deno_core") (r "^0.187.0") (d #t) (k 0)))) (h "0f80vb5r0xa6ncmd378r5crmnfwbs8ayjcr331b61hmark5dbrhw")))

(define-public crate-deno_console-0.106.0 (c (n "deno_console") (v "0.106.0") (d (list (d (n "deno_core") (r "^0.188.0") (d #t) (k 0)))) (h "0n4p560m5brp0vz7swj00pkhicqddjncy7azgvaswcxir1y8brw6")))

(define-public crate-deno_console-0.107.0 (c (n "deno_console") (v "0.107.0") (d (list (d (n "deno_core") (r "^0.189.0") (d #t) (k 0)))) (h "0i3kmnq7s06qvx2dyfx0h8bydxbip9rz2sq34rjxwy3pc2vn0973")))

(define-public crate-deno_console-0.108.0 (c (n "deno_console") (v "0.108.0") (d (list (d (n "deno_core") (r "^0.190.0") (d #t) (k 0)))) (h "0a0zirx9zn84m321wg08jizl3zs0z75dg7h2hm5dhq998ihvmn46")))

(define-public crate-deno_console-0.109.0 (c (n "deno_console") (v "0.109.0") (d (list (d (n "deno_core") (r "^0.191.0") (d #t) (k 0)))) (h "1kzymyjg6hlgw24gbris31d944fhws39b1bd5l9g5h9p0dm3psq4")))

(define-public crate-deno_console-0.110.0 (c (n "deno_console") (v "0.110.0") (d (list (d (n "deno_core") (r "^0.194.0") (d #t) (k 0)))) (h "1c83r2kkficm0y5m56b90q33968jdgszs7l7xcc55chx8gacb5k9")))

(define-public crate-deno_console-0.111.0 (c (n "deno_console") (v "0.111.0") (d (list (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)))) (h "00yar9r7vii6y4arkk0809hkljjgqv229frxaasdgn2j49qfva10")))

(define-public crate-deno_console-0.112.0 (c (n "deno_console") (v "0.112.0") (d (list (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)))) (h "17ikpd4d7jqc3bx3v57rvirgfhxs8z7nvfkq3qnmzhk8dnczyg6p")))

(define-public crate-deno_console-0.113.0 (c (n "deno_console") (v "0.113.0") (d (list (d (n "deno_core") (r "^0.197.0") (d #t) (k 0)))) (h "1bckhh3vvd1p2lm7f68s9c0c0vswdlbnxhqr5ghvcmfgcl7iccfh")))

(define-public crate-deno_console-0.114.0 (c (n "deno_console") (v "0.114.0") (d (list (d (n "deno_core") (r "^0.199.0") (d #t) (k 0)))) (h "1hl4vcl1qldgnfaganv7468w6l5vwqgzry2jygsc76b4y8gz8gbs")))

(define-public crate-deno_console-0.115.0 (c (n "deno_console") (v "0.115.0") (d (list (d (n "deno_core") (r "^0.200.0") (d #t) (k 0)))) (h "052jcggpr718qhq7l57jw86mfagdfdkzqagbdnb8asc2k2vhbaqr")))

(define-public crate-deno_console-0.116.0 (c (n "deno_console") (v "0.116.0") (d (list (d (n "deno_core") (r "^0.202.0") (d #t) (k 0)))) (h "0jgpwhlg0kchgxvc1wrhz8wl2brq93sh4l6vn69cdvsd2x2m1jhc")))

(define-public crate-deno_console-0.117.0 (c (n "deno_console") (v "0.117.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)))) (h "15sm7wa5hxxlfjpxbym0qlpz8spz9a18l9y07j57rf9lk2l1cs6z")))

(define-public crate-deno_console-0.118.0 (c (n "deno_console") (v "0.118.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)))) (h "1p03r6qblbwanhjm6qn7841kwiyf74kjcxi1gmyndy4z9c43472a")))

(define-public crate-deno_console-0.119.0 (c (n "deno_console") (v "0.119.0") (d (list (d (n "deno_core") (r "^0.214.0") (d #t) (k 0)))) (h "18r5hinkgi5sbgss82f9fp6dr0579c3qc927181sial7kp10aqqh")))

(define-public crate-deno_console-0.120.0 (c (n "deno_console") (v "0.120.0") (d (list (d (n "deno_core") (r "^0.218.0") (d #t) (k 0)))) (h "0hmkw5c6gdjwimbk6cf6zpj3fqiymrys7ssh3raxg7lafm3ws5wp")))

(define-public crate-deno_console-0.121.0 (c (n "deno_console") (v "0.121.0") (d (list (d (n "deno_core") (r "^0.222.0") (d #t) (k 0)))) (h "0jzfmrhgskzcddl9957y9h63d3lfwsrgppgld4kdw7ldnbibqb5p")))

(define-public crate-deno_console-0.122.0 (c (n "deno_console") (v "0.122.0") (d (list (d (n "deno_core") (r "^0.224.0") (d #t) (k 0)))) (h "09r3fcmk96k56jc4ygcqj1frab47xmgzr8z7xq4hwlsgpgw3hwk4")))

(define-public crate-deno_console-0.123.0 (c (n "deno_console") (v "0.123.0") (d (list (d (n "deno_core") (r "^0.229.0") (d #t) (k 0)))) (h "11mrhfjdw09na0b2s3jn54wa6wld2r8d4bzxlvhj8sfdiajrab4d")))

(define-public crate-deno_console-0.124.0 (c (n "deno_console") (v "0.124.0") (d (list (d (n "deno_core") (r "^0.230.0") (d #t) (k 0)))) (h "1wbckmy0pb2sa8gjjlc3ypsvwhv0q0j5qsnxlwy5dp74l76pagmy")))

(define-public crate-deno_console-0.125.0 (c (n "deno_console") (v "0.125.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)))) (h "1fmbxmnhjnblh8g9736apfls3avkgbln0jxx4c0kbwl29m7ksm4j")))

(define-public crate-deno_console-0.126.0 (c (n "deno_console") (v "0.126.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)))) (h "1wr2pdl3ks4avvral5kkk7d4b4dr6rqnwplnm1xfdqaspih809mn")))

(define-public crate-deno_console-0.127.0 (c (n "deno_console") (v "0.127.0") (d (list (d (n "deno_core") (r "^0.232.0") (d #t) (k 0)))) (h "0ifa31mpbws7q09llhjya9d0xgn0sq3qz3xbil2j0xc58jw4mvfy")))

(define-public crate-deno_console-0.128.0 (c (n "deno_console") (v "0.128.0") (d (list (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)))) (h "03pdx1a44lk4hd3i8cwx5fyx2bv6ml7jlxab4id6dj9a6l4j26qv")))

(define-public crate-deno_console-0.129.0 (c (n "deno_console") (v "0.129.0") (d (list (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)))) (h "1n96cdwc51j44v2qqp6pwv088rp82q24wixq9y6ixsvdssfk9z6s")))

(define-public crate-deno_console-0.130.0 (c (n "deno_console") (v "0.130.0") (d (list (d (n "deno_core") (r "^0.243.0") (d #t) (k 0)))) (h "0a9d80n4s2x5h3sx4mqq1kq5wsq3m6hr0ckk63h2zybg10zxrmdr")))

(define-public crate-deno_console-0.131.0 (c (n "deno_console") (v "0.131.0") (d (list (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)))) (h "1bpy42hn2ag21bw2f27c3zb5ydj4b27vzifzn7jifdf36nd8ms0z")))

(define-public crate-deno_console-0.132.0 (c (n "deno_console") (v "0.132.0") (d (list (d (n "deno_core") (r "^0.245.0") (d #t) (k 0)))) (h "0w14jv4g9xp8v39bb9zhxynlnk2lrfxhvbc54cdh2kyv2bndb0rb")))

(define-public crate-deno_console-0.133.0 (c (n "deno_console") (v "0.133.0") (d (list (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)))) (h "03xkkkmp9cn8d6x8xsibnb3f7pnnn1z6ggqyp4mck10qbv1a1kac")))

(define-public crate-deno_console-0.134.0 (c (n "deno_console") (v "0.134.0") (d (list (d (n "deno_core") (r "^0.249.0") (d #t) (k 0)))) (h "0gwffprmbbbx3ainc52wdh0bf9p23vqgjacfwi47dbyi63ghl9ir")))

(define-public crate-deno_console-0.135.0 (c (n "deno_console") (v "0.135.0") (d (list (d (n "deno_core") (r "^0.254.0") (d #t) (k 0)))) (h "1in3h9l5zckdzviks4d456fn5y4mmimgh8l8qxkv5mqvhf6k93cd")))

(define-public crate-deno_console-0.136.0 (c (n "deno_console") (v "0.136.0") (d (list (d (n "deno_core") (r "^0.256.0") (d #t) (k 0)))) (h "0c1haps4w1s1qq9gyyf95zcmxwkjsxnqy2yr64z4xgdlmv2bd2jr")))

(define-public crate-deno_console-0.137.0 (c (n "deno_console") (v "0.137.0") (d (list (d (n "deno_core") (r "^0.260.0") (d #t) (k 0)))) (h "1wr5w90w8r5sh47nn4jmb2k6w2ic2sf1h6v5ah83x4i4m01j5rz2")))

(define-public crate-deno_console-0.138.0 (c (n "deno_console") (v "0.138.0") (d (list (d (n "deno_core") (r "^0.262.0") (d #t) (k 0)))) (h "187c1wzz493jv5l9mvrg0gmf2rxl0ca6537jmw2473y8jvb3qzqj")))

(define-public crate-deno_console-0.139.0 (c (n "deno_console") (v "0.139.0") (d (list (d (n "deno_core") (r "^0.264.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)))) (h "076glnz4vwr02yhyg2mbf6yn936ncrk6zvpnipxlgfyg15sabqlb")))

(define-public crate-deno_console-0.140.0 (c (n "deno_console") (v "0.140.0") (d (list (d (n "deno_core") (r "^0.265.0") (f (quote ("snapshot_data_bincode"))) (d #t) (k 0)))) (h "1gf0dnx1d62psapvkzdrdd31g1qvfca71aa4cqf2r6749hl8pcim")))

(define-public crate-deno_console-0.141.0 (c (n "deno_console") (v "0.141.0") (d (list (d (n "deno_core") (r "^0.269.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)))) (h "16950prml1fh6xrwzcpfsh3prba3hkywwwz1i64rrxj0jcbsyxa6")))

(define-public crate-deno_console-0.142.0 (c (n "deno_console") (v "0.142.0") (d (list (d (n "deno_core") (r "^0.270.0") (f (quote ("lazy_eval_snapshot"))) (d #t) (k 0)))) (h "16rjgvgcr4xnsxy9p76vg0zwc8cmymga1nhw7wbiv1lmal4dw4hp")))

(define-public crate-deno_console-0.143.0 (c (n "deno_console") (v "0.143.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)))) (h "1ghkgl8aixn7ag5n828ky1byy4jkxzmdjhnjjvavzc0fxf6hsxqz")))

(define-public crate-deno_console-0.144.0 (c (n "deno_console") (v "0.144.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)))) (h "0clmxyi402xm6afi1hqj84a898kz6b7f0dqh8md3yb64s3800aip")))

(define-public crate-deno_console-0.145.0 (c (n "deno_console") (v "0.145.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)))) (h "01z5ry0vg1l4rsrsx3bg35aswhsy0kxi6v7j3vzr3lxc32za7s5b")))

(define-public crate-deno_console-0.146.0 (c (n "deno_console") (v "0.146.0") (d (list (d (n "deno_core") (r "^0.272.0") (d #t) (k 0)))) (h "00hf2c55ywmbyfvq1qrpg6zqksjnhdrs7797mngrq86hq0b021pa")))

(define-public crate-deno_console-0.147.0 (c (n "deno_console") (v "0.147.0") (d (list (d (n "deno_core") (r "^0.275.0") (d #t) (k 0)))) (h "006j3svffdq5kp7ip98b5qwvjlxhl42ln5p60yxzvlva5gjwjgxx")))

(define-public crate-deno_console-0.148.0 (c (n "deno_console") (v "0.148.0") (d (list (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)))) (h "1gvyxr3lgfrmar170mngbsxv4fln26p4ss19shqjn71bq5l232w7") (y #t)))

(define-public crate-deno_console-0.149.0 (c (n "deno_console") (v "0.149.0") (d (list (d (n "deno_core") (r "^0.278.0") (d #t) (k 0)))) (h "033z0nf6cn11avxiqfx2h0infwfvinm064m3aza0z4c9vsr736dq")))

(define-public crate-deno_console-0.150.0 (c (n "deno_console") (v "0.150.0") (d (list (d (n "deno_core") (r "^0.279.0") (d #t) (k 0)))) (h "14gs9s84j06gzm97nyb88pn47rhirdmxnhmqldjkc6k58hcm5mrq")))

(define-public crate-deno_console-0.151.0 (c (n "deno_console") (v "0.151.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)))) (h "0bjfdzzhjz0cmbspjq4n7wk0p70ckydsvhfrnfimyyjjdyipwrdi")))

(define-public crate-deno_console-0.152.0 (c (n "deno_console") (v "0.152.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)))) (h "0hdxc6pb9k4np8d9f91lzfkgcmf4g45x1jfgn4bqm4b5fbl0h2rb")))

(define-public crate-deno_console-0.153.0 (c (n "deno_console") (v "0.153.0") (d (list (d (n "deno_core") (r "^0.280.0") (d #t) (k 0)))) (h "10dg1y517bxhrpysl1awcqshfds1106fdd2d4bij39fskk28dv0i")))

