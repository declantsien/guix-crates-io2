(define-module (crates-io de no denow_wsi) #:use-module (crates-io))

(define-public crate-denow_wsi-0.3.2 (c (n "denow_wsi") (v "0.3.2") (d (list (d (n "deno_core") (r "^0.170.0") (d #t) (k 0)) (d (n "deno_webgpu") (r "^0.3.2") (d #t) (k 0) (p "denow_webgpu")) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "=1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "06n25ip2hpi09vhl4kh53ny1ni1axi4wvj3k6p7jcl64pvnzm8zc")))

