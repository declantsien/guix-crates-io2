(define-module (crates-io de no deno_typescript) #:use-module (crates-io))

(define-public crate-deno_typescript-0.0.1 (c (n "deno_typescript") (v "0.0.1") (d (list (d (n "deno") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18jzsw797y6m7lc1bzl7f0q12ldr2sl96cmkdpp3gcx0p14kq4pi")))

(define-public crate-deno_typescript-0.0.2 (c (n "deno_typescript") (v "0.0.2") (d (list (d (n "deno") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09g8r97138s5db95nd6llqgij0qyzfqqzqdszhciliz0r7ri2qy8")))

(define-public crate-deno_typescript-0.0.3 (c (n "deno_typescript") (v "0.0.3") (d (list (d (n "deno") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0543qz608ydb4ap7xvgzm312ic3qilscj5r4cpxdrikp8kq283bm")))

(define-public crate-deno_typescript-0.18.0 (c (n "deno_typescript") (v "0.18.0") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "07a2ff4ys20303gxnwdhwy8nmcgl38l6qx53sqahk84p6wsvxpx7")))

(define-public crate-deno_typescript-0.18.0-preview2 (c (n "deno_typescript") (v "0.18.0-preview2") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0cv1rx3az4vhg3mfg0d0brvcqbw0xmfq2yh6b7sccabirs7mrpm6")))

(define-public crate-deno_typescript-0.18.0-preview3 (c (n "deno_typescript") (v "0.18.0-preview3") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0m525l48xjlwxk0y5fd09sb6gfs8h4rqqksj2fjy5zminh75sjf1")))

(define-public crate-deno_typescript-0.18.0-preview4 (c (n "deno_typescript") (v "0.18.0-preview4") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "09p5kbf6nv4vzhljiaxxvsf4gsr6xy04dvm9cf6af817r2c6kwc0")))

(define-public crate-deno_typescript-0.18.1 (c (n "deno_typescript") (v "0.18.1") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0ba434783g9d4ciakd1wz500kh42hqcsib89fsgvwlldq7lbrkzs")))

(define-public crate-deno_typescript-0.18.2 (c (n "deno_typescript") (v "0.18.2") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "14kdafwf460wa7ammiyki7fjbx6fvn56qjvnnzph7h5l5pglva05")))

(define-public crate-deno_typescript-0.18.3 (c (n "deno_typescript") (v "0.18.3") (d (list (d (n "deno") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0577vsh2y69nmdppnh5wjn5kr119x32fbxbyl32kyzpnxflmh5py")))

(define-public crate-deno_typescript-0.19.0 (c (n "deno_typescript") (v "0.19.0") (d (list (d (n "deno") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "17a03ykcv0vp4q8waf7sna1pizvc92hzw6phhd3wdxirhw0l0k0n")))

(define-public crate-deno_typescript-0.20.0 (c (n "deno_typescript") (v "0.20.0") (d (list (d (n "deno") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "188zzh35707cfw0pdb748f1sw3wwhl0hlfqnqpiqvgyah3rqisq0")))

(define-public crate-deno_typescript-0.21.0 (c (n "deno_typescript") (v "0.21.0") (d (list (d (n "deno") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "15dpn0j3grkp7g651kgvws2gi1qcfmniwi7j3rilwi9h8rk2qkcg")))

(define-public crate-deno_typescript-0.22.0 (c (n "deno_typescript") (v "0.22.0") (d (list (d (n "deno") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0pf26czdk0pyg6mhdbnws7iakvlcmqrimx973w3p0isys2446bis")))

(define-public crate-deno_typescript-0.23.0 (c (n "deno_typescript") (v "0.23.0") (d (list (d (n "deno") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1r2wfjsg9hb25apzwrwkxnmj1q3zmcnvgsmakhdq4kh189dplpxl")))

(define-public crate-deno_typescript-0.29.0 (c (n "deno_typescript") (v "0.29.0") (d (list (d (n "deno_core") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "116277d10b7qrw37s9rs48vl7g6b2fpzb49yn47y80hcd48vb8h8")))

(define-public crate-deno_typescript-0.30.0 (c (n "deno_typescript") (v "0.30.0") (d (list (d (n "deno_core") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0579v7qah0pfgnprcp18h04dsdhwwpj6fjym82ixp45achmcnya5")))

(define-public crate-deno_typescript-0.30.1 (c (n "deno_typescript") (v "0.30.1") (d (list (d (n "deno_core") (r "^0.30.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0b15a1vircxyyrgbb5gm5wmgwpjp20mkgrjbi3sy30b9yw950kcp")))

(define-public crate-deno_typescript-0.31.0 (c (n "deno_typescript") (v "0.31.0") (d (list (d (n "deno_core") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "00ynph7c0pg3wdsf07q7g4k8rc21pslrw5ayqwaq73dmvhwrpp3k")))

(define-public crate-deno_typescript-0.32.0 (c (n "deno_typescript") (v "0.32.0") (d (list (d (n "deno_core") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1bbixkwjr28d7asxl2lffqlwpy1nqr0sb3l5xb157fql7dypapiw")))

(define-public crate-deno_typescript-0.33.0 (c (n "deno_typescript") (v "0.33.0") (d (list (d (n "deno_core") (r "^0.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "130h6750qm5nwygpiqfmxw4p29003nzbb04l9s20axinsxpai5yg")))

(define-public crate-deno_typescript-0.34.0 (c (n "deno_typescript") (v "0.34.0") (d (list (d (n "deno_core") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "15j6naip91p0f7r7gggsy02i425kf9hsn6fv25mnnark419cx68i")))

(define-public crate-deno_typescript-0.35.0 (c (n "deno_typescript") (v "0.35.0") (d (list (d (n "deno_core") (r "^0.35.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1c3qjl7zx8fdrp2k542cvbv9r6gxjq67l4lhkcvd8nrx7m1p389m")))

(define-public crate-deno_typescript-0.36.0 (c (n "deno_typescript") (v "0.36.0") (d (list (d (n "deno_core") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "150n3zqg6cg7py4rzms0myf7gkcbywva63zwqfivf9djvj8laf1q")))

(define-public crate-deno_typescript-0.37.0 (c (n "deno_typescript") (v "0.37.0") (d (list (d (n "deno_core") (r "^0.37.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0kkzs8ws97iw7w3jvq8blpvimcyx3kcggsxgjd3alnq4yxfz28v1")))

(define-public crate-deno_typescript-0.37.1 (c (n "deno_typescript") (v "0.37.1") (d (list (d (n "deno_core") (r "^0.37.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "010p3p7amjnr6l8ym1dd04n0wmxgg7fr80jx4iw86mssvywpvr7c")))

(define-public crate-deno_typescript-0.38.0 (c (n "deno_typescript") (v "0.38.0") (d (list (d (n "deno_core") (r "^0.38.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0d4cvnl3ljcz79849ri69zpgxdbqxy9dgpdl5j6z1885i0pvci95")))

(define-public crate-deno_typescript-0.39.0 (c (n "deno_typescript") (v "0.39.0") (d (list (d (n "deno_core") (r "^0.39.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0d4ldbki7da7krh7dqnm2f9adfac60b8ckmqfn298w4bkmdr91vs")))

(define-public crate-deno_typescript-0.40.0 (c (n "deno_typescript") (v "0.40.0") (d (list (d (n "deno_core") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)))) (h "17w5wj25mpcfc7gp0ax71izqwbswkqj442in72xv9bfh5dywpa7r")))

(define-public crate-deno_typescript-0.41.0 (c (n "deno_typescript") (v "0.41.0") (d (list (d (n "deno_core") (r "^0.41.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "1lj2lqd3qy7sz67dj5r10d0vxnk1yc36dw05czhqpffly1smkzvn")))

(define-public crate-deno_typescript-0.42.0 (c (n "deno_typescript") (v "0.42.0") (d (list (d (n "deno_core") (r "^0.42.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0nbhkycg19jfsfajpjzfz3273m4ihcr38nf05n47b89dmkvfinbi")))

(define-public crate-deno_typescript-0.42.1 (c (n "deno_typescript") (v "0.42.1") (d (list (d (n "deno_core") (r "^0.42.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "1bh8w458mfvzvm3g6c3pwg30rn1hwv1b7lg01yfim8xqdmsfvjzb")))

(define-public crate-deno_typescript-0.43.0 (c (n "deno_typescript") (v "0.43.0") (d (list (d (n "deno_core") (r "^0.43.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "19y8xjbyafx84jwmghvyz3i7mjyag143whay8j3bbg83jhm3gz2v")))

(define-public crate-deno_typescript-0.44.0 (c (n "deno_typescript") (v "0.44.0") (d (list (d (n "deno_core") (r "^0.44.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "00pm9a3vda6m9j66mv14zrgydmmvxwdmyy53z5g3ic5239y2ynlp")))

(define-public crate-deno_typescript-0.45.0 (c (n "deno_typescript") (v "0.45.0") (d (list (d (n "deno_core") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "1yhbj8d971h0xrdfhpi57nkvs7yymcsfm98l18ffdw0jyq8qk2sb")))

(define-public crate-deno_typescript-0.45.1 (c (n "deno_typescript") (v "0.45.1") (d (list (d (n "deno_core") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "0c072gyan17531xnrwxxf25w4p7nilci6as5yz7abij9002y91yl")))

(define-public crate-deno_typescript-0.45.2 (c (n "deno_typescript") (v "0.45.2") (d (list (d (n "deno_core") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "0pm608phq4mhl09qjj6k6jyip15292vfr4iyl162gx6q32cqs7fj")))

(define-public crate-deno_typescript-0.46.0 (c (n "deno_typescript") (v "0.46.0") (d (list (d (n "deno_core") (r "^0.46.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "0pps704jjbvfwlk128wjd464hv1c4s2h1vzbr975hy02718rbhg7")))

(define-public crate-deno_typescript-0.47.0 (c (n "deno_typescript") (v "0.47.0") (d (list (d (n "deno_core") (r "^0.47.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0y6ag46bcsp71rnmkiy5397cqigyg4ib89qfhp9knmk5y4j22sc8")))

(define-public crate-deno_typescript-0.47.1 (c (n "deno_typescript") (v "0.47.1") (d (list (d (n "deno_core") (r "^0.47.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0fq1w3cx7kvnlfpf9i6bsb44gx2rrpjb7984xm04724w5p36sncd")))

(define-public crate-deno_typescript-0.48.0 (c (n "deno_typescript") (v "0.48.0") (d (list (d (n "deno_core") (r "^0.48.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0zycz5gfn56ram77gm6dn1lrm87hvh2g6zwan9s5pwkd8iamyplf")))

(define-public crate-deno_typescript-0.48.1 (c (n "deno_typescript") (v "0.48.1") (d (list (d (n "deno_core") (r "^0.48.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "0zcds7lgp5v3b4x1v6s3967w6dcj9wpda0krvddw7k5zm6yn9x37")))

(define-public crate-deno_typescript-0.48.2 (c (n "deno_typescript") (v "0.48.2") (d (list (d (n "deno_core") (r "^0.48.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "19wl81y6dm1f2hbh7w4b7qcg0l4hcik6cqn3grq66j8f15z6in5z")))

(define-public crate-deno_typescript-0.48.3 (c (n "deno_typescript") (v "0.48.3") (d (list (d (n "deno_core") (r "^0.48.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "02gw41gy9nmvkm53z0cyiicf7cp6xzy6mcnq2m4s6l948an4298w")))

(define-public crate-deno_typescript-0.49.0 (c (n "deno_typescript") (v "0.49.0") (d (list (d (n "deno_core") (r "^0.49.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "0lxa72gc3y6i4w5vh4vzjxhyhs7d27j3zw35z4w4g1gifa9msiy9")))

