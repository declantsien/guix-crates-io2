(define-module (crates-io de no deno-ls-init) #:use-module (crates-io))

(define-public crate-deno-ls-init-0.1.0 (c (n "deno-ls-init") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "109jinrzan5lp9pm8rr1c5cd57i1jrrmnf3xjfll3rlcnbc331jc")))

(define-public crate-deno-ls-init-0.2.0 (c (n "deno-ls-init") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "0nqyjbxfzyrpl3w7sygypr5v1d8z7bkkysqklysgavdffgzkf2mw")))

