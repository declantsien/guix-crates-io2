(define-module (crates-io de no deno_timers) #:use-module (crates-io))

(define-public crate-deno_timers-0.1.0 (c (n "deno_timers") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.84.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14n0jmbjn2i3269nbksraqyzv59akj944i3ch72ddjdh453r6zmd")))

(define-public crate-deno_timers-0.2.0 (c (n "deno_timers") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.85.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dy87myhijfh2fzyl4k49cfj755knjqhcj32s3zmq7myrrrz3810")))

(define-public crate-deno_timers-0.3.0 (c (n "deno_timers") (v "0.3.0") (d (list (d (n "deno_core") (r "^0.86.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bahifkvyyfy7zp51vhk0w1l3d7yb90xdzdxyj467bjcl2nm7c2v")))

(define-public crate-deno_timers-0.4.0 (c (n "deno_timers") (v "0.4.0") (d (list (d (n "deno_core") (r "^0.87.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j34jiq4aw4q9kyhh7p4lyjibi2vxkjrwkvjhgaafdbckhnd0hm5")))

(define-public crate-deno_timers-0.5.0 (c (n "deno_timers") (v "0.5.0") (d (list (d (n "deno_core") (r "^0.88.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rg5rqkc4nhd5frlyvma8mgfz1jhbxi78c1q3qcyd5vyzchf1lwp")))

(define-public crate-deno_timers-0.5.1 (c (n "deno_timers") (v "0.5.1") (d (list (d (n "deno_core") (r "^0.88.1") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qsj7qm7zjwnri8yaf1wrlm3x4vpam64kq7zl0cw23mjwv6w1p2h")))

(define-public crate-deno_timers-0.6.0 (c (n "deno_timers") (v "0.6.0") (d (list (d (n "deno_bench_util") (r "^0.2.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.89.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qpz3l603bqr8k9sn6kh898xfy463q04ayy3sprjrdxcqvf7rwp5")))

(define-public crate-deno_timers-0.7.0 (c (n "deno_timers") (v "0.7.0") (d (list (d (n "deno_bench_util") (r "^0.3.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.90.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mjvj413ak04vvqis6xkp4ij8qj84ric5h74y2psngs93b47q457")))

(define-public crate-deno_timers-0.8.0 (c (n "deno_timers") (v "0.8.0") (d (list (d (n "deno_bench_util") (r "^0.4.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.91.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)))) (h "168jl0558hqi7l093np0vgsnhyv2v0y385rxld11si7ljnbmbrf1")))

(define-public crate-deno_timers-0.8.1 (c (n "deno_timers") (v "0.8.1") (d (list (d (n "deno_bench_util") (r "^0.4.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.91.1") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b98vrwzzb0kj8z4ifix5g7ni4a4pww9h6ajgnbsmn99yjlr8ac2")))

(define-public crate-deno_timers-0.9.0 (c (n "deno_timers") (v "0.9.0") (d (list (d (n "deno_bench_util") (r "^0.5.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.93.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.11.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.42.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.11.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zxvy0h3ls3r9rc9a4pxnixq8yzhls6qa3b9bj1vc2yq2v5x4ba9")))

(define-public crate-deno_timers-0.10.0 (c (n "deno_timers") (v "0.10.0") (d (list (d (n "deno_bench_util") (r "^0.6.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.94.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.12.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.43.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ah4g4rgq1pg6si1p155ax4q4xw7allhyf21i73mjnh18iz1rnqp")))

(define-public crate-deno_timers-0.11.0 (c (n "deno_timers") (v "0.11.0") (d (list (d (n "deno_bench_util") (r "^0.7.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.95.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.13.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.44.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.13.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f892d6yg3vm433gmwf2my0y5asmbfkm21irq57flgniaf872mkl")))

(define-public crate-deno_timers-0.12.0 (c (n "deno_timers") (v "0.12.0") (d (list (d (n "deno_bench_util") (r "^0.8.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.96.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.14.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.45.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.14.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ws9bcblmvk10s9lcbnhvysqw6lcvcbf9r29b51bsfycjsl203ma")))

(define-public crate-deno_timers-0.13.0 (c (n "deno_timers") (v "0.13.0") (d (list (d (n "deno_bench_util") (r "^0.9.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.97.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.15.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.46.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.15.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "14cc1wrgc7ip81w5db7agy7740jwa0b5n0dmhhgphqcgj6h1b7jl")))

(define-public crate-deno_timers-0.14.0 (c (n "deno_timers") (v "0.14.0") (d (list (d (n "deno_bench_util") (r "^0.10.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.98.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.16.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.47.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.8.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vh0qwhsg9x00v4i7fzxhx786a2w2md2b66mfsl1rcv7g9dicfqy")))

(define-public crate-deno_timers-0.15.0 (c (n "deno_timers") (v "0.15.0") (d (list (d (n "deno_bench_util") (r "^0.11.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.99.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.17.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.48.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.17.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0akb9mcmffzc7gas94zvbaw3dhqim1qyayh5rfsrcmbdrq6v1j49")))

(define-public crate-deno_timers-0.16.0 (c (n "deno_timers") (v "0.16.0") (d (list (d (n "deno_bench_util") (r "^0.12.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.100.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.49.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.18.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c23g8g3z0nyk4swwmnv0k2sd517yaviwiggz7cch0412rjfpxxp")))

(define-public crate-deno_timers-0.17.0 (c (n "deno_timers") (v "0.17.0") (d (list (d (n "deno_bench_util") (r "^0.13.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.101.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.19.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.50.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.19.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12mn7nbccngm1ygqdc63d5709f4nqzhd92c180wbza7im73k9nys")))

(define-public crate-deno_timers-0.18.0 (c (n "deno_timers") (v "0.18.0") (d (list (d (n "deno_bench_util") (r "^0.14.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.102.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.20.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.51.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.20.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10gvh375pzdrlsa31q5ajhnjfp6ps141zxh4jhsbnw4yvnjp6lc5")))

(define-public crate-deno_timers-0.19.0 (c (n "deno_timers") (v "0.19.0") (d (list (d (n "deno_bench_util") (r "^0.15.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.103.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.21.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.52.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.21.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxvina2j1zhzmqc6lpvda7c9w8fqgy02w11628gqsaidbzv6hyb")))

(define-public crate-deno_timers-0.20.0 (c (n "deno_timers") (v "0.20.0") (d (list (d (n "deno_bench_util") (r "^0.16.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.104.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.22.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.53.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.22.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5nsrf56qpp1fgyfzdzc4k2p24nk74y09wywlnq6yljypcy4w3g")))

(define-public crate-deno_timers-0.21.0 (c (n "deno_timers") (v "0.21.0") (d (list (d (n "deno_bench_util") (r "^0.17.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.105.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.23.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.54.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.23.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0asahsz4wa3f52rf6spgn5kk879zyb21pqwklz6g6rs590vdkx5l")))

(define-public crate-deno_timers-0.22.0 (c (n "deno_timers") (v "0.22.0") (d (list (d (n "deno_bench_util") (r "^0.18.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.106.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.24.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.55.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.24.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "19zknjq0yhcmkplfmg35s237j5c9p0lb421xrhz22fbvh2x8d65v")))

(define-public crate-deno_timers-0.23.0 (c (n "deno_timers") (v "0.23.0") (d (list (d (n "deno_bench_util") (r "^0.19.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.107.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.25.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.56.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.25.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10kb8ghha43bfr6qjk3fmvpahcjnwg7aqdwf8d1mqzrz0pcf1dvc")))

(define-public crate-deno_timers-0.24.0 (c (n "deno_timers") (v "0.24.0") (d (list (d (n "deno_bench_util") (r "^0.20.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.108.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.26.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.57.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.26.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fh31pnjsgqzrz6k3llla4ahbw9d1da6k1xy04z3qvv2npqhbpyi")))

(define-public crate-deno_timers-0.25.0 (c (n "deno_timers") (v "0.25.0") (d (list (d (n "deno_bench_util") (r "^0.21.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.109.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.27.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.58.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.27.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mb7w829zlxs61pvms23v32cnbmznkp7w90v64wsni5xj72dh266")))

(define-public crate-deno_timers-0.26.0 (c (n "deno_timers") (v "0.26.0") (d (list (d (n "deno_bench_util") (r "^0.22.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.110.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.28.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.59.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.28.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wa9f9kki9plrppydhqdwi42r2alciihay263z8qiaxg6b4c4r2b")))

(define-public crate-deno_timers-0.27.0 (c (n "deno_timers") (v "0.27.0") (d (list (d (n "deno_bench_util") (r "^0.23.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.111.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.29.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.60.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.29.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "053gcczp6axhpyza3vkmjcsz3sd4lbm180b0315z7hfbqsz0p9j3")))

(define-public crate-deno_timers-0.28.0 (c (n "deno_timers") (v "0.28.0") (d (list (d (n "deno_bench_util") (r "^0.24.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.112.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.30.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.61.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.30.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ysr2jq5nlxshc3j3qxa6kawp8lmrgpqsifzl1ggvy1v8mi69i0k")))

(define-public crate-deno_timers-0.29.0 (c (n "deno_timers") (v "0.29.0") (d (list (d (n "deno_bench_util") (r "^0.25.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.113.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.31.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.62.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.31.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "191d2f3swdqiwv6941gqv7fp2sl417r89ymxi1sc8l4fgq19zjsl")))

(define-public crate-deno_timers-0.30.0 (c (n "deno_timers") (v "0.30.0") (d (list (d (n "deno_bench_util") (r "^0.26.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.114.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.32.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.63.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.32.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08nxixrjaw5pfga43ndzh5bvpk0y62bxa7rvx9y23vqipvf917i2")))

(define-public crate-deno_timers-0.32.0 (c (n "deno_timers") (v "0.32.0") (d (list (d (n "deno_bench_util") (r "^0.28.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.116.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.34.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.65.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.34.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j4brj4hf0bkfnzipcv5wklr1pfjd8n32i7jpzafgh0slgsr1mvq")))

(define-public crate-deno_timers-0.33.0 (c (n "deno_timers") (v "0.33.0") (d (list (d (n "deno_bench_util") (r "^0.29.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.117.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.35.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.66.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.35.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lim01g2xhr031av903kh84pm609cpr70lrbc2ncgmrwcg50h5n1")))

(define-public crate-deno_timers-0.34.0 (c (n "deno_timers") (v "0.34.0") (d (list (d (n "deno_bench_util") (r "^0.30.0") (d #t) (k 2)) (d (n "deno_core") (r "^0.118.0") (d #t) (k 0)) (d (n "deno_url") (r "^0.36.0") (d #t) (k 2)) (d (n "deno_web") (r "^0.67.0") (d #t) (k 2)) (d (n "deno_webidl") (r "^0.36.0") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x15690pbh85qnynw5nbynywl4bhbc7wa721yfdi988saa7ci9c2")))

