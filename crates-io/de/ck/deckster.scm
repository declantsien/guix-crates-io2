(define-module (crates-io de ck deckster) #:use-module (crates-io))

(define-public crate-deckster-0.2.0 (c (n "deckster") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "seahorse") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tui") (r "^0.13") (d #t) (k 0)))) (h "0lmjvykslz3ajcqirv8ipzfy5860n0k3yj2nikr9d6p25hj3245s")))

(define-public crate-deckster-0.2.1 (c (n "deckster") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "seahorse") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tui") (r "^0.13") (d #t) (k 0)))) (h "08a8vm9c97rjasc6v2hnkxak76319qfzaxp4rfc5rji04fbm2jxa")))

