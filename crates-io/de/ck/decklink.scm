(define-module (crates-io de ck decklink) #:use-module (crates-io))

(define-public crate-decklink-0.1.0 (c (n "decklink") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0j6n45zx1knz3h9hfx9chb44isxyaj1icrmqckm8dgq6jpvwha8n")))

