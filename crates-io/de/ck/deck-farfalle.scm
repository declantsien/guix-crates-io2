(define-module (crates-io de ck deck-farfalle) #:use-module (crates-io))

(define-public crate-deck-farfalle-0.1.0 (c (n "deck-farfalle") (v "0.1.0") (d (list (d (n "crypto-permutation") (r "^0.1") (d #t) (k 0)) (d (n "permutation-keccak") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "permutation-xoodoo") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "xoofff") (r "^0.1") (d #t) (k 2)))) (h "0jiyzrwv29k605g6v8mbz5n70vsaabsi5c8hxfivb5bwp6p5z9c7") (f (quote (("default")))) (s 2) (e (quote (("xoofff" "dep:permutation-xoodoo") ("kravatte" "dep:permutation-keccak") ("debug" "permutation-keccak?/debug" "permutation-xoodoo?/debug")))) (r "1.65")))

