(define-module (crates-io de ck deck_codes) #:use-module (crates-io))

(define-public crate-deck_codes-0.1.0 (c (n "deck_codes") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.5") (d #t) (k 0)))) (h "13qg4vj4kq3mx49sns4b00x1i84n86s983j2788qv70ix49qm6b0")))

(define-public crate-deck_codes-0.2.0 (c (n "deck_codes") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.6.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.5") (d #t) (k 0)))) (h "0z0pnzmb3hmq7f14a441c4flhzabvpkvm9xjnakrsmwi9spwzxv7")))

(define-public crate-deck_codes-0.2.1 (c (n "deck_codes") (v "0.2.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.6.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.5") (d #t) (k 0)))) (h "16wv0k946mcpxx6m5alhkbsxm792n0ad90ira3zkkda8cz4b91sn")))

(define-public crate-deck_codes-0.2.2 (c (n "deck_codes") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kw1fcii1n7rb86p9hab16s6fb45494cms3ldc09qnwp3h3a2alw")))

