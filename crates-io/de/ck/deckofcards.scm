(define-module (crates-io de ck deckofcards) #:use-module (crates-io))

(define-public crate-deckofcards-0.1.0 (c (n "deckofcards") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0cis869k6y2sv0lngk2wpvxbxzjf2j788q2wlh3g583jh45y3r3b")))

(define-public crate-deckofcards-0.2.0 (c (n "deckofcards") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0cg8zjqdy4gm9cyyrq0ycm9bcyfkg0x39l6xc0a6gcwgid5w8fk4")))

(define-public crate-deckofcards-0.3.0 (c (n "deckofcards") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "14pf1plr6y693hv89sk37png4220mm5hmcxgvj6mv9yps2nz155g")))

(define-public crate-deckofcards-0.3.1 (c (n "deckofcards") (v "0.3.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "05hyl5cf9b8r8j0d9s7zaj7qna3zjzim99s2jrgldzwfyvimbai9")))

(define-public crate-deckofcards-0.3.3 (c (n "deckofcards") (v "0.3.3") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0j8q4b8g2n08w0dvb4a5wbrildih9gyr7h6s7wyb8mz76aifs35d")))

(define-public crate-deckofcards-0.3.4 (c (n "deckofcards") (v "0.3.4") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1lvmpa5qigqb6wprc5cjnl64rr81bzwk9w7yrpkg43s986i942dc")))

(define-public crate-deckofcards-0.3.5 (c (n "deckofcards") (v "0.3.5") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1w2hx46hf0mnd7pmjwsbm5971swdd5rhvl30xl2g9vh2cnjwiqnm")))

(define-public crate-deckofcards-0.4.0 (c (n "deckofcards") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1aajq0lrgs4xj29w3dksgdvxwsagqd1ymw9wh929a3r7hn2pnzw6")))

