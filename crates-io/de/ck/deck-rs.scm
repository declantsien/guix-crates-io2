(define-module (crates-io de ck deck-rs) #:use-module (crates-io))

(define-public crate-deck-rs-0.1.0 (c (n "deck-rs") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0l9mzgjws0i42hd73v46pv73fr371qsg25yprvms14hzv1pmi7cm")))

