(define-module (crates-io de im deimos) #:use-module (crates-io))

(define-public crate-deimos-0.1.0 (c (n "deimos") (v "0.1.0") (h "1plas89pg54qrfp724fp2g02ryf8ipag67mj1azpa9l82dlld6lj") (y #t)))

(define-public crate-deimos-0.1.1 (c (n "deimos") (v "0.1.1") (h "10x59c4iq0rv4pl6bc7m88rqhz6mrin4xhybsfcy6zx121x8xab0") (y #t)))

(define-public crate-deimos-0.2.0 (c (n "deimos") (v "0.2.0") (h "1p73m48m3sbkqj5iz757n40y9yn6b2dczc0xypibi8p1rw9fcvib")))

