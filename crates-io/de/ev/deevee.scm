(define-module (crates-io de ev deevee) #:use-module (crates-io))

(define-public crate-deevee-0.2.0 (c (n "deevee") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^3.2.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1kc6sm8kwhiamk2pfv2vfvd230k2mwg6h0hw9df5k96i0v0lcb5r")))

(define-public crate-deevee-0.3.0 (c (n "deevee") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^3.2.1") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "12wn5vnm49xa215lpzfwcgazh2053kn4zilpy9kpbiz8lacpibdj")))

