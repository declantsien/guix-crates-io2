(define-module (crates-io de re deref_owned) #:use-module (crates-io))

(define-public crate-deref_owned-0.1.0 (c (n "deref_owned") (v "0.1.0") (h "1pl1ml2lyr4hv0a6974ndxpcqavjbx357v0xdb5lah1h0fvycbib")))

(define-public crate-deref_owned-0.2.0 (c (n "deref_owned") (v "0.2.0") (h "01s8066g5p5av1ppw1l819ycn15ccrsifza88vq5qx31vxxqfbcy")))

(define-public crate-deref_owned-0.3.0 (c (n "deref_owned") (v "0.3.0") (h "0617g2d3xwk3d4iwazd49v9mjs43fxwyyhmaxbrcaby99qk6zkqw")))

(define-public crate-deref_owned-0.4.0 (c (n "deref_owned") (v "0.4.0") (h "07102y3z05yx1kdmhww7d1fipwpb7dcmphhvkk6nw553lk7bzq26")))

(define-public crate-deref_owned-0.5.0 (c (n "deref_owned") (v "0.5.0") (h "1mkplbp2v92zqdn1mayhzzc7gxlmxsxnncg76jkmchq78cmh3ylx") (y #t)))

(define-public crate-deref_owned-0.6.0 (c (n "deref_owned") (v "0.6.0") (h "1kqqhslpqg0cwvyw7321s79prka2w0k2y0kqnmc2n0jma80vk1rd") (y #t)))

(define-public crate-deref_owned-0.6.1 (c (n "deref_owned") (v "0.6.1") (h "1rp4y4dfw2a174whi683akzmlykymmkcmxyzv8vbh1b7dig9iii4")))

(define-public crate-deref_owned-0.7.0 (c (n "deref_owned") (v "0.7.0") (h "1hcv5i71c7smym6m0zd8896p5hgfhcfzgslg4s91p54qc8jbyhf1")))

(define-public crate-deref_owned-0.8.0 (c (n "deref_owned") (v "0.8.0") (h "181j6pqzlrmmcn5gzi3dzy7saawz22vxyxjw87r9j9ckic2n99ar")))

(define-public crate-deref_owned-0.8.1 (c (n "deref_owned") (v "0.8.1") (h "059c92w00a9i478hxvfmw4qbllna9myyy1sq1y2bv5xz6gbv6dkp")))

(define-public crate-deref_owned-0.9.0 (c (n "deref_owned") (v "0.9.0") (h "0n04gm3gzwng7b46nwj5hb4my6vykzxfmnfq38gjwz1h8gdr2xx6") (y #t)))

(define-public crate-deref_owned-0.8.2 (c (n "deref_owned") (v "0.8.2") (h "02ylncmalpkljcslg01r6q4ydmmrq7xi6qpnhf6839fivl7jnl59")))

(define-public crate-deref_owned-0.10.0 (c (n "deref_owned") (v "0.10.0") (h "1gf6gyha2vjj3gaja4v6f6bnr2lji99kz18cjjfw3b8iaggqqajz")))

(define-public crate-deref_owned-0.10.1 (c (n "deref_owned") (v "0.10.1") (h "12b0w1686w9j1m1dk8a68x6sqknd8bpzgid51gkg7lkc7in94485")))

(define-public crate-deref_owned-0.10.2 (c (n "deref_owned") (v "0.10.2") (h "1n731384iirfnzj78xkh0ns61k59h51my02xh394a31m4rvlhpga")))

