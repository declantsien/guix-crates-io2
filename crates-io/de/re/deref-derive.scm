(define-module (crates-io de re deref-derive) #:use-module (crates-io))

(define-public crate-deref-derive-0.1.0 (c (n "deref-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wrjw8gldn6fqag0c38bv6dv36fnigj55shapn402n9qsmyvnma2")))

