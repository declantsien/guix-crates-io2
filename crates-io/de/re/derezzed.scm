(define-module (crates-io de re derezzed) #:use-module (crates-io))

(define-public crate-derezzed-0.0.1-alpha.0 (c (n "derezzed") (v "0.0.1-alpha.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hk21f5mvx3dzmgc0sn9y9r84fkrz9i8fr68aqnnmjjai4lld0lb")))

