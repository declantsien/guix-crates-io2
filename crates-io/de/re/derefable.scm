(define-module (crates-io de re derefable) #:use-module (crates-io))

(define-public crate-derefable-0.1.0 (c (n "derefable") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ryb8k7dj4v41rx62qj9qnghg2wli679b0f9f4q7cxch53qsn6g5")))

