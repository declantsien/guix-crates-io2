(define-module (crates-io de ox deoxysii) #:use-module (crates-io))

(define-public crate-deoxysii-0.1.0 (c (n "deoxysii") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 2)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "zeroize") (r "^0.6") (d #t) (k 0)))) (h "01yj74al1qpx3ks61f62mnyc0z1dmc6f7740ajjywzwlds03a1g2")))

(define-public crate-deoxysii-0.2.0 (c (n "deoxysii") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 2)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "zeroize") (r "^0.6") (d #t) (k 0)))) (h "0ybnmy5arwf45iasg1w54cbwc1673s46kkf447bjz1019crf6f1f")))

(define-public crate-deoxysii-0.2.1 (c (n "deoxysii") (v "0.2.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^0.6") (d #t) (k 0)))) (h "1dsy7wjdr7ffmaywjxys4gspb5893fgcvvjizaldhniwjq458h4c")))

(define-public crate-deoxysii-0.2.2 (c (n "deoxysii") (v "0.2.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "01gfx5dxh79i2n4v721sr40b0y2017jhrbl62hk7yd3397mvk1gg")))

(define-public crate-deoxysii-0.2.3 (c (n "deoxysii") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "08drn6rk9s7lms5hxpfq6pqrqaz4zlzrddl7jc35vzr561bqc43a")))

(define-public crate-deoxysii-0.2.4 (c (n "deoxysii") (v "0.2.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0288sbf5j492ck7jx5c6mfpawsld2nsdwy63qmi4fypzx6kxkfgs")))

