(define-module (crates-io de ox deoxy-core) #:use-module (crates-io))

(define-public crate-deoxy-core-0.2.0 (c (n "deoxy-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.84") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (o #t) (d #t) (k 0)))) (h "0jajm21vzz0137j1h2g54asn4fvs0ak0s3c6lps2gzs381scsrz6") (f (quote (("use_serde" "serde" "serde_derive") ("default"))))))

(define-public crate-deoxy-core-0.2.1 (c (n "deoxy-core") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.84") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (o #t) (d #t) (k 0)))) (h "07a5zimsi08dqaj0s63gfmd2yvicn9lm4914nx5rmk0811vwnzbv") (f (quote (("use_serde" "serde" "serde_derive") ("default"))))))

