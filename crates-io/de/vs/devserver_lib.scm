(define-module (crates-io de vs devserver_lib) #:use-module (crates-io))

(define-public crate-devserver_lib-0.1.0 (c (n "devserver_lib") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "1cvijxmk0y11irs6cbd7byfvkhzjj66zlwjhdy670xhfl4n3ywi6") (y #t)))

(define-public crate-devserver_lib-0.1.1 (c (n "devserver_lib") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "0zlhxynfxdc7p9bj9rl1rd0bjlkr57wg8sxnn65f45lzx48ba67n") (y #t)))

(define-public crate-devserver_lib-0.1.2 (c (n "devserver_lib") (v "0.1.2") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "0qyf6ig18365sx521hz6wz0bxgwvff2w0nmf43nlw8inmh94mv6d") (y #t)))

(define-public crate-devserver_lib-0.1.3 (c (n "devserver_lib") (v "0.1.3") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0myqdg9777snaw8xrg0gy57bqz6nh8910dbh5rhasid987qyyfmn") (f (quote (("reload" "notify" "sha-1" "base64") ("default")))) (y #t)))

(define-public crate-devserver_lib-0.1.4 (c (n "devserver_lib") (v "0.1.4") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "099d8mylng1ibg80bvfcby644v2dd8jl9bbp3lc5a9vbrdgivib4") (f (quote (("reload" "notify" "sha-1" "base64") ("default")))) (y #t)))

(define-public crate-devserver_lib-0.1.5 (c (n "devserver_lib") (v "0.1.5") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "1pjlppvzip4mp6z2f6n5bjpahfacq3x12q8208626wkq1s0myafk") (f (quote (("reload" "notify" "sha-1" "base64") ("default")))) (y #t)))

(define-public crate-devserver_lib-0.1.7 (c (n "devserver_lib") (v "0.1.7") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0kk7cgvwr3dnrplra3nidwn946y6n4a9vc3mrayc3rsixap7qxf7") (f (quote (("reload" "notify" "sha-1" "base64") ("default"))))))

(define-public crate-devserver_lib-0.4.0 (c (n "devserver_lib") (v "0.4.0") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "05vmbq2jp3zylh350fi2p666ag9ppkrk04apnfjxi9kpbli7gdhx") (f (quote (("reload" "notify" "sha-1" "base64") ("default"))))))

(define-public crate-devserver_lib-0.4.1 (c (n "devserver_lib") (v "0.4.1") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "073vssjclqpwppfljml8lrdyv0hdkakr4f5964jbw18lrhdbgd4g") (f (quote (("reload" "notify" "sha-1" "base64") ("https" "native-tls") ("default" "https"))))))

(define-public crate-devserver_lib-0.4.2 (c (n "devserver_lib") (v "0.4.2") (d (list (d (n "base64") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0rmz0d4k7h4bqhdblnw5445gnfcybz9swnk4lz60j56bp3dibwpd") (f (quote (("reload" "notify" "sha-1" "base64") ("https" "native-tls") ("default" "https"))))))

