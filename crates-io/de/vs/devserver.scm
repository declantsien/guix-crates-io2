(define-module (crates-io de vs devserver) #:use-module (crates-io))

(define-public crate-devserver-0.1.0 (c (n "devserver") (v "0.1.0") (d (list (d (n "devserver_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1h9qvpw4q2v8lyxzgps0w745fz2y1xk8z3as23ja8kkz6ca7pxis") (y #t)))

(define-public crate-devserver-0.1.1 (c (n "devserver") (v "0.1.1") (d (list (d (n "devserver_lib") (r "^0.1.1") (d #t) (k 0)))) (h "12w6cgy234salj4bqm4rysfndgxh6my7wb80d14lk2qkizd10amb") (y #t)))

(define-public crate-devserver-0.1.2 (c (n "devserver") (v "0.1.2") (d (list (d (n "devserver_lib") (r "^0.1.2") (d #t) (k 0)))) (h "0w5ydfdnbb7520fvj5hf5jdbznjnv5nd20ycrl6qwcrahc7vldbq") (y #t)))

(define-public crate-devserver-0.1.3 (c (n "devserver") (v "0.1.3") (d (list (d (n "devserver_lib") (r "^0.1.3") (f (quote ("reload"))) (d #t) (k 0)))) (h "0w6rlbfch8n2dih0xf7q1svjfkwjkbx434xynnfj8yqlyiqxfpy8") (y #t)))

(define-public crate-devserver-0.1.4 (c (n "devserver") (v "0.1.4") (d (list (d (n "devserver_lib") (r "^0.1.3") (f (quote ("reload"))) (d #t) (k 0)))) (h "0gqpa264hav84j7n1vwfxfq2y220k9b6z5mi862l0s83i3frxwld") (y #t)))

(define-public crate-devserver-0.1.5 (c (n "devserver") (v "0.1.5") (d (list (d (n "devserver_lib") (r "^0.1.3") (f (quote ("reload"))) (d #t) (k 0)))) (h "0vqv4c7df0sfnr7gm54zmbzrrici5hig75fy2rdb936cx6jyfci2") (y #t)))

(define-public crate-devserver-0.1.6 (c (n "devserver") (v "0.1.6") (d (list (d (n "devserver_lib") (r "^0.1.3") (f (quote ("reload"))) (d #t) (k 0)))) (h "0slv3wsarz0azqp20zyx47522phzfig0r4vjqz7xz6fj8rjm4sp7")))

(define-public crate-devserver-0.1.7 (c (n "devserver") (v "0.1.7") (d (list (d (n "devserver_lib") (r "^0.1.7") (f (quote ("reload"))) (d #t) (k 0)))) (h "0c39ripr9v3ikgzdcybxg3igx7j9wp1snz72i1r8mmcdvpya5q11")))

(define-public crate-devserver-0.4.0 (c (n "devserver") (v "0.4.0") (d (list (d (n "devserver_lib") (r "^0.4.0") (f (quote ("reload"))) (d #t) (k 0)))) (h "07dpdmcgfbf36cvkjxh7g8mqma4bq90np8iv3v6xci210smym96p")))

(define-public crate-devserver-0.4.1 (c (n "devserver") (v "0.4.1") (d (list (d (n "devserver_lib") (r "^0.4.1") (f (quote ("reload" "https"))) (d #t) (k 0)))) (h "12whp6gnd93916kik9wdv1khxj1nywisksi836dz0j2vl6pz0cbs")))

(define-public crate-devserver-0.4.2 (c (n "devserver") (v "0.4.2") (d (list (d (n "devserver_lib") (r "^0.4.1") (f (quote ("reload" "https"))) (d #t) (k 0)))) (h "063fia20bzpmbjz9hnxhbg7827b0rvjnibvcd3g20cl1xzb1gfqw")))

