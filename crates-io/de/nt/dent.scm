(define-module (crates-io de nt dent) #:use-module (crates-io))

(define-public crate-dent-0.1.0 (c (n "dent") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)))) (h "1dwi7w9hjdk8xvk8gzdf4z5jwxkr11h4pmvbi8gjas98l10848gb")))

(define-public crate-dent-0.2.0 (c (n "dent") (v "0.2.0") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "stamp") (r "^0.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)))) (h "17l6fk7vgl2j7bs5f5izdncjldz376k0h4pv5sj323b0ps81c68n")))

(define-public crate-dent-0.3.0 (c (n "dent") (v "0.3.0") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "stamp") (r "^0.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)))) (h "08m1xzwwsjkirr497gm6vkswdfcyy8gi49i54izc0d54lbmw546p")))

(define-public crate-dent-0.4.0 (c (n "dent") (v "0.4.0") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "stamp") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)))) (h "0msywap0xfidjac90cm0vq9fxbsjxa9lgqxqglsmjprv5zh8982x")))

(define-public crate-dent-0.4.1 (c (n "dent") (v "0.4.1") (d (list (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "stamp") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)))) (h "06nvqdxf5lbaxf0racjf18g3x1pbhhj3rnl701wf6jrnspqq4wkz")))

