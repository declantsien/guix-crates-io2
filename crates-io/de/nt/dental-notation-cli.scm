(define-module (crates-io de nt dental-notation-cli) #:use-module (crates-io))

(define-public crate-dental-notation-cli-0.1.0 (c (n "dental-notation-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dental-notation") (r "^1.0.0") (d #t) (k 0)))) (h "03615qchpbcf45p8gcqf9s4p9n99fkgwd2qjg4lvjx4xg2nklmn5")))

