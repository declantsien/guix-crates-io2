(define-module (crates-io de ts detsurtnu) #:use-module (crates-io))

(define-public crate-detsurtnu-0.1.0 (c (n "detsurtnu") (v "0.1.0") (h "1pxmhx2nvqv5iygf1z1zacp56x5p1pi2avk28phfihd5x3n35qda")))

(define-public crate-detsurtnu-0.2.0 (c (n "detsurtnu") (v "0.2.0") (h "19jx74vy1i41b6qi26gid4i8ssjq1w3qay8bwixxhmnkr5lpyb3f")))

(define-public crate-detsurtnu-0.3.0 (c (n "detsurtnu") (v "0.3.0") (h "0iy368hfka8jrj53yw2w2vnvysds30rqi3f6az5knkwhz4ph8a37")))

(define-public crate-detsurtnu-0.3.1 (c (n "detsurtnu") (v "0.3.1") (h "02qxmpn9v4p68z599i536jjjnxip4l4cwalnlzshxdkg0jgiw2p1")))

(define-public crate-detsurtnu-0.3.2 (c (n "detsurtnu") (v "0.3.2") (h "0938fnm1hbd3rmxrlzyzaqcg1gk3sw9dwa2sx5qxdvms5ric5p05")))

(define-public crate-detsurtnu-0.5.0 (c (n "detsurtnu") (v "0.5.0") (h "096yv6kbifn6k9iw0wjdhhsnfjzgz2h0zr8a73qk1rpdzzy5vxyb")))

(define-public crate-detsurtnu-0.5.1 (c (n "detsurtnu") (v "0.5.1") (h "1dq5msq4vwbc2jqzmyyzhj1hms5dahhy0mgcxhz4mn1mxvq2ay4v")))

(define-public crate-detsurtnu-0.6.0 (c (n "detsurtnu") (v "0.6.0") (h "1lzg33gxbnp2hmj0p547gqhzpfca86yqs10aghw9b7wp2yabj4v9")))

(define-public crate-detsurtnu-0.6.1 (c (n "detsurtnu") (v "0.6.1") (h "179nx2rinam0if8bvmwnm99kphljx2nmkf7fm297w5smz2vhws2z")))

(define-public crate-detsurtnu-0.6.2 (c (n "detsurtnu") (v "0.6.2") (h "00z4sg24cdcrmc679hib1qx41jw7cj945pipa62ppwqa9w38b4v0")))

(define-public crate-detsurtnu-0.7.0 (c (n "detsurtnu") (v "0.7.0") (h "0l4ky20y08zn26ggkgxwr79hh19112vrrcb5zvhjnnafq650pdpc")))

(define-public crate-detsurtnu-0.7.0-alpha1 (c (n "detsurtnu") (v "0.7.0-alpha1") (h "1rr7c5xd0dgpn3shl55xi5a5wgfgj5f4hi572q85jf68lkd0mcf4")))

