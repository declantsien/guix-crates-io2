(define-module (crates-io de ri derive-diff) #:use-module (crates-io))

(define-public crate-derive-diff-0.1.0 (c (n "derive-diff") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "15swspi4r7wwcn01ii1c2dlca7gzf4kl8nk2wqwg8mkahqx0m57p")))

(define-public crate-derive-diff-0.2.0 (c (n "derive-diff") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1xj05l6scqwnpqyjnan7vmk8ay0hi41b5xisrpdakiz7ycvgm48q")))

(define-public crate-derive-diff-0.2.1 (c (n "derive-diff") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1lfn7vb7lck7flw79vhyyarjf9jwvkx1rkv9vip1w812vr382jrr")))

(define-public crate-derive-diff-0.2.2 (c (n "derive-diff") (v "0.2.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "09w257vkqqp2zpjpls1qj372f6vf6yw8a431yhddzn0d3baxm97x")))

(define-public crate-derive-diff-0.2.3 (c (n "derive-diff") (v "0.2.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1sspsb55yfk92aih16k9c1n2z08qri7wdjk8b6cvnh2sb7z2i621")))

(define-public crate-derive-diff-0.2.4 (c (n "derive-diff") (v "0.2.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0f6y1cixs6c60h3bd4rmp42wx8kg4rf7chlfqfc8vh9q949djfpk")))

(define-public crate-derive-diff-0.2.5 (c (n "derive-diff") (v "0.2.5") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "struct-diff") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gwy7rxsrh7s91mgkn89688gc4cbfsya3i0bw1q1k8ic4wpq6daf")))

