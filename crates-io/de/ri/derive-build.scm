(define-module (crates-io de ri derive-build) #:use-module (crates-io))

(define-public crate-derive-build-0.1.0 (c (n "derive-build") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1fv2n297qpm7vpv9k2d8049d421f4kax55mz7518w93jk483xixq")))

(define-public crate-derive-build-0.1.1 (c (n "derive-build") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1jlmqmvb7qrsipaby9z5pl02vbgji7msn4l1jcnmb901xhd7397m")))

