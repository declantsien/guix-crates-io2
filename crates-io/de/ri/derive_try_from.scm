(define-module (crates-io de ri derive_try_from) #:use-module (crates-io))

(define-public crate-derive_try_from-0.1.0 (c (n "derive_try_from") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m9r1j58b4f0c3ppj2m9kf2cwdb78brlnisda1ww04pdj6axd964") (y #t)))

(define-public crate-derive_try_from-0.2.0 (c (n "derive_try_from") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mg73mbhkv3f32ah0a8nkh0yxg0pi0865w2h3c0kq85dqbf8z076") (y #t)))

