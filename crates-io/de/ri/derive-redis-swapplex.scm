(define-module (crates-io de ri derive-redis-swapplex) #:use-module (crates-io))

(define-public crate-derive-redis-swapplex-0.2.0 (c (n "derive-redis-swapplex") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0xqzpkvhf0w6y6ki84lzw1mra413nq3qwshdfrwb7gwxz6zww6ky")))

(define-public crate-derive-redis-swapplex-0.4.0 (c (n "derive-redis-swapplex") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1h7ww30m2m2nycg0h5sx84gfl056g3ylwwsc2mhmyya7amhrrdxj")))

(define-public crate-derive-redis-swapplex-0.4.1 (c (n "derive-redis-swapplex") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v9nv1l85idqq10y56mxy89axi7w2w6yvjc8jjn98x2z60kzd7s5")))

(define-public crate-derive-redis-swapplex-0.5.0 (c (n "derive-redis-swapplex") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11k56pq6wvw3w5knwk69lcm0cb93j2nadcvisyz0l3sgzxm7p4av")))

(define-public crate-derive-redis-swapplex-0.6.0 (c (n "derive-redis-swapplex") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ilyn881ns3g9cn6zdbf9288f77czqybczj29clgqv65aqjk9kyy")))

(define-public crate-derive-redis-swapplex-0.8.0 (c (n "derive-redis-swapplex") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xranbvslxdwk5wls4mnjlb0m7vsdm4p6pvpbxjmrigb9nayi3v9")))

(define-public crate-derive-redis-swapplex-0.9.0 (c (n "derive-redis-swapplex") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0b9nrydhqmyiidspj2li8rxgijigz4j47fvjan6vmfswkgb6w4i7")))

(define-public crate-derive-redis-swapplex-0.10.0 (c (n "derive-redis-swapplex") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1il0bcc6nyk5662kzan3bfa9nidbh0liny5kf55iybikcq8zxni3")))

(define-public crate-derive-redis-swapplex-0.11.0 (c (n "derive-redis-swapplex") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02z4qhnpibm2x92ja1r47j12p7f64vxvkms9g3qyzhh0cr8vqal0")))

(define-public crate-derive-redis-swapplex-0.12.0 (c (n "derive-redis-swapplex") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "17hxldyvcr46agrb5p6cma3wwg37cnglrmjbkzrsbidi9pdr8qak")))

(define-public crate-derive-redis-swapplex-0.12.1 (c (n "derive-redis-swapplex") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k7i3vrlri9ws400alg8s97iz9rfggi6ra7r4faqlv1klbpdnhwh")))

