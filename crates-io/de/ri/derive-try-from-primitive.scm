(define-module (crates-io de ri derive-try-from-primitive) #:use-module (crates-io))

(define-public crate-derive-try-from-primitive-0.1.0 (c (n "derive-try-from-primitive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0pfy3q3hksb1fyhvvxjx1az6514j9zqqdb661pavcd2pn5gddnw1")))

(define-public crate-derive-try-from-primitive-1.0.0 (c (n "derive-try-from-primitive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p5d072zpznb5pwbvqgasp65mx6jzj124nkgpdri25gi9l4wyb1h")))

