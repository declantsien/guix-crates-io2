(define-module (crates-io de ri derive_utils) #:use-module (crates-io))

(define-public crate-derive_utils-0.1.0 (c (n "derive_utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vbmbrllynagxyrry8bfclnrh1gpmlclqws4gs8a3gz6nv10f1vb") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-derive_utils-0.1.1 (c (n "derive_utils") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yd7i19zl3jkk5sj9if2n8ilp4cj0vgqlby1cl66fpy221fb965c") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.2.0 (c (n "derive_utils") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04jn9gdl0fj6m2rydr1jsfjm30pp2rbi1zz71794zlqh89kamdwz") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.3.0 (c (n "derive_utils") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03jy7ldvk7vg98s8c2s2dzwflz2s7h1b9jq6al4fpqq67glwjzna") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.4.0 (c (n "derive_utils") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ysj0k5w6kayswqvhbs4a8h4hy5kyiajr6gdjmgzy33pfpz59jhd") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.5.0 (c (n "derive_utils") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rx1qd67b8pr1s7w1pm2186mkb6xiri001sdd6vjmric3d2m0qx0") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.5.1 (c (n "derive_utils") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09rd0xyn82fsdxr2wkdl3gvfkbwbqb9x67n5wqggpzp8qk7hic2q") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.5.2 (c (n "derive_utils") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7b5hs0q7i640b4nfxw1v2v6qc4llg5agpi2xnyngi6s3d1dcia") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.5.3 (c (n "derive_utils") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1zls19kq3yihj8j4jkp9hkdd3y0i0j1m4g7z916scj2y6408vwd6") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.5.4 (c (n "derive_utils") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1hji8l64yadhr19xk6vh07gfj3lk6436y0a8z3w9bx769csrr80w") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_utils-0.6.0 (c (n "derive_utils") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "09bpm6z814q0hhn79rbficzr19ypaw8y0r4532irh12kd72br5rp")))

(define-public crate-derive_utils-0.6.1 (c (n "derive_utils") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1y66f753chsc6hkfjws4f3srmp7var4av85lcib1wggaimk6z6sw")))

(define-public crate-derive_utils-0.6.2 (c (n "derive_utils") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5rvwp3g9984hcnva6y46605a5mdk7scmyc3imzawxq2v7ds2ym")))

(define-public crate-derive_utils-0.6.3 (c (n "derive_utils") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "059drxlws1hk0h3grh33pcrkc9azpv3nqfgqrvbnv90nxhdb8jkd")))

(define-public crate-derive_utils-0.7.0 (c (n "derive_utils") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1mz6igmdsq2ryr63b120jmrfdd6lxxparfsmq9i5srrv1a4d1kjq")))

(define-public crate-derive_utils-0.7.1 (c (n "derive_utils") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0jlh0mswwcqfbhq1xmqmnfjxrgxzp5jxnfwp2nicmgwrzvzgbvsx")))

(define-public crate-derive_utils-0.7.2 (c (n "derive_utils") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "173xzaixf50pavng9n7rjdv1h418awj70s2is0appy6aqa774lc0")))

(define-public crate-derive_utils-0.8.0 (c (n "derive_utils") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "003k44h90d88rss5mmwjxjpxr8iii8gf47nbzr0c0acic3dp3m3s")))

(define-public crate-derive_utils-0.9.0 (c (n "derive_utils") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10bmnsfpn6psjgpdlzfy6cna1akqgg27av4q6rr7fza678vd0a8k")))

(define-public crate-derive_utils-0.9.1 (c (n "derive_utils") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rh2dcqrby43np37shrxz21q0paj37gg71k114bad1w06n3s4jhi")))

(define-public crate-derive_utils-0.10.0 (c (n "derive_utils") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "07ab4j1yw58xvxcad6iryygkb11c8a1bdwxpsvswv1ns2824ix9x")))

(define-public crate-derive_utils-0.11.0 (c (n "derive_utils") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0i1mzsx99z98cc6r0g0x6ds5f2qgm7lg2d2i49kn34aiynwnw6b4")))

(define-public crate-derive_utils-0.11.1 (c (n "derive_utils") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1rrphpxm9xzxwy49ypdziafwxhv0z64kail5yz1ajmdkr5klhl39")))

(define-public crate-derive_utils-0.11.2 (c (n "derive_utils") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1gx7giwn8x427d5f8c92n9h0hhcqdsasvz7i8iq2rqffvhalqask")))

(define-public crate-derive_utils-0.12.0 (c (n "derive_utils") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "18jzdz4rpqxm8fhbb5i866ssjnh61j6ibaac4n632nkkd2agk43m") (r "1.31")))

(define-public crate-derive_utils-0.13.0 (c (n "derive_utils") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0vf9pxj0mxd7jzxwan3imha15b0kl995lhnl3ag72a7mjfkzdy6z") (r "1.56")))

(define-public crate-derive_utils-0.13.1 (c (n "derive_utils") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "1wxzq55dd0qi13zc5q85nvlazzj029lmgbk7swv3w3kr3cg1bki0") (r "1.56")))

(define-public crate-derive_utils-0.13.2 (c (n "derive_utils") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "08cf3jks20wbyjlk0cqq6k7dhdsyhifpknywng60j1kjx4jsvg4s") (r "1.56")))

(define-public crate-derive_utils-0.14.0 (c (n "derive_utils") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n85rzc69dkx2gmc5wdi20i10awrai54154606v3jqvr8r6gzskv") (y #t) (r "1.56")))

(define-public crate-derive_utils-0.14.1 (c (n "derive_utils") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09mrh7dyznamfma4ln7zdfh5lnkppsd50y456wmgqvff2h85mfv1") (r "1.56")))

