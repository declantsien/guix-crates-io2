(define-module (crates-io de ri derive_create) #:use-module (crates-io))

(define-public crate-derive_create-0.1.0-alpha.0 (c (n "derive_create") (v "0.1.0-alpha.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0bmh5w7kdml0z29czn18431bssnfdc6k4c975a3m40azlrixl2xn")))

(define-public crate-derive_create-0.1.0-alpha.1 (c (n "derive_create") (v "0.1.0-alpha.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "17p3zdhnjmp0hfw3al0dd3rbf902p4fviqk98jx7i2mca8j0bwlv")))

