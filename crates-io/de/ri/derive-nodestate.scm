(define-module (crates-io de ri derive-nodestate) #:use-module (crates-io))

(define-public crate-derive-nodestate-0.5.0 (c (n "derive-nodestate") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "012g728lg682vqs5mdn4p78y28xkaz0c6wbrpz0fkw18phh13108")))

