(define-module (crates-io de ri derive_dearbitrary) #:use-module (crates-io))

(define-public crate-derive_dearbitrary-1.2.0 (c (n "derive_dearbitrary") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h5nqzknhid63xgvw4bqvi37lbhmh0xfqy4vn732baf8nrprxpxg") (r "1.63.0")))

(define-public crate-derive_dearbitrary-1.0.4 (c (n "derive_dearbitrary") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bm4xb6m0zs57pz942qf4jqgajivggw3smcv4abbx2jxl1sir0pj") (r "1.70.0")))

