(define-module (crates-io de ri derive_setters) #:use-module (crates-io))

(define-public crate-derive_setters-0.1.0 (c (n "derive_setters") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11h8krw8mq1klpva6d05m1px7cfx97a7y7518a6wc87fjp3v29mk") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.1 (c (n "derive_setters") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06pb39gnrw5fmwbxgbmsm7jv9wa57s559jidxw2xsa2zx3bd5fl8") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.2 (c (n "derive_setters") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f09x031b1acza2mm77zlmvz1v9spr8pcvchiax3r5zns6af7dwq") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.3 (c (n "derive_setters") (v "0.1.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0hfg08y5fb4bfb90rg5rkz5k1kwr8kvwffjv41nyivq4d1k3jv6y") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.4 (c (n "derive_setters") (v "0.1.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0a74yndhfdr6ljls3p3vs6fhijwvy1hmnw8bcm9v6fyx34n62136") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.5 (c (n "derive_setters") (v "0.1.5") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jz84mlsnrqd061jwlplhcwvxmpm7k22rniaxyjwqdqab2s43kzi") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-derive_setters-0.1.6 (c (n "derive_setters") (v "0.1.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03890w7p6zslxl0cbwxddx1sjcsmiypsgyghp1li24sf0lrz13jf") (f (quote (("nightly" "proc-macro2/nightly"))))))

