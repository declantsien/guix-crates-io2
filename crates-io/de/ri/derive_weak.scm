(define-module (crates-io de ri derive_weak) #:use-module (crates-io))

(define-public crate-derive_weak-0.1.0 (c (n "derive_weak") (v "0.1.0") (d (list (d (n "async_object") (r "^0.1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0fb5k0cdh6plxgp4qp74q79l223jba75i8qhnh1zljcd9kaihqf5")))

