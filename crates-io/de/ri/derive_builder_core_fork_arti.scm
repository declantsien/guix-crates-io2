(define-module (crates-io de ri derive_builder_core_fork_arti) #:use-module (crates-io))

(define-public crate-derive_builder_core_fork_arti-0.11.2 (c (n "derive_builder_core_fork_arti") (v "0.11.2") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k9ifag7mxnq7pn241qgfh1savhrg8w1lplsma635rlvqwavgh94") (f (quote (("clippy"))))))

