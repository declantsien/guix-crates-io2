(define-module (crates-io de ri derive_enum_macros) #:use-module (crates-io))

(define-public crate-derive_enum_macros-0.1.0 (c (n "derive_enum_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "09911qf5lqqhxj9h8ng6x7h89fmd73q6cmda4zb0pwqvygjsszqn") (f (quote (("name") ("from_str") ("default"))))))

(define-public crate-derive_enum_macros-0.2.0 (c (n "derive_enum_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0zmpjbd4d34afm7l1slfpws659zw3ws18dv07dzxx98zm8g0n1mz") (f (quote (("name") ("iter") ("from_str") ("default"))))))

(define-public crate-derive_enum_macros-0.2.1 (c (n "derive_enum_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1fd5yyx9qd7ra99hzbl7n2zcjbwq57lkbx2hgqvky7vlmykdgjrw") (f (quote (("name") ("iter") ("from_str") ("default"))))))

(define-public crate-derive_enum_macros-0.2.2 (c (n "derive_enum_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1pmcappmlzb2mjn3zs9kp2n6xr7shmvl5fmb53zgdsxdfnfgnvws") (f (quote (("name") ("iter") ("from_str") ("default"))))))

