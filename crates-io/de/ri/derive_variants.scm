(define-module (crates-io de ri derive_variants) #:use-module (crates-io))

(define-public crate-derive_variants-0.1.0 (c (n "derive_variants") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0pqvzidglggpmwl0sz7zi0fna2bi2y9k0pcmvmkyys8rhc9ixymv")))

(define-public crate-derive_variants-0.1.1 (c (n "derive_variants") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1vsiyr63ljc6c33q1c57hafzg28bcdmkwy3yc8pmqkf7sfcmgffs")))

(define-public crate-derive_variants-0.1.2 (c (n "derive_variants") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0c8xi5m2yqk38m776c15swypnbn7dgyaay4yvkw8mm67j3ms2sdj")))

(define-public crate-derive_variants-0.1.3 (c (n "derive_variants") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0qg42cpzr2wcivzhghl7bc5r6jh29kdk06mbqfv4a7xp0qnxnh21")))

