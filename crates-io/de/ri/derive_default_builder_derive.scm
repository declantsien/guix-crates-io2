(define-module (crates-io de ri derive_default_builder_derive) #:use-module (crates-io))

(define-public crate-derive_default_builder_derive-0.1.0 (c (n "derive_default_builder_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0g9qmdhif1syk7v8mxcbg08yimmcr6l30kwdp5rscr6nchkalpy6")))

(define-public crate-derive_default_builder_derive-0.1.1 (c (n "derive_default_builder_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "184dpgsgr2m7xv8n52lmksw035f8pja8hd6d4gfd63ldndjc46v7")))

(define-public crate-derive_default_builder_derive-0.1.2 (c (n "derive_default_builder_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0cgam3dvgp7pclhrbnyflll52hjswy8w9prf38h6xjcvybd9r3av")))

(define-public crate-derive_default_builder_derive-0.1.3 (c (n "derive_default_builder_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1kr4lvgjs2wj6w6ashv1fg7q0g0n1a1xw8ma0l2bcbn8h4xsf84y")))

(define-public crate-derive_default_builder_derive-0.1.4 (c (n "derive_default_builder_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "122hgmfddq5r10iwqpfv3crcyiqzis0549lyx21gyybkhncqfczq")))

(define-public crate-derive_default_builder_derive-0.1.5 (c (n "derive_default_builder_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "02nsh2ndv9shcaz5gizdph15bpblx6d11shjsmakwwhj9lwall4h")))

(define-public crate-derive_default_builder_derive-0.1.6 (c (n "derive_default_builder_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "04rlj80750m5gmvcsjgilygl42j2qran1yivrakpsxrhyx3sb23v")))

(define-public crate-derive_default_builder_derive-0.1.7 (c (n "derive_default_builder_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0f1ji2kp11mw94s702w8rpn7cgh7grih4nx2m3lplbfi1nqmhahd")))

(define-public crate-derive_default_builder_derive-0.1.8 (c (n "derive_default_builder_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1rv0q435pr5ggk4b20jzyb86sp5fxcmaz2x9hvn7153s4ihhnlly")))

