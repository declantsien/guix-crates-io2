(define-module (crates-io de ri derive-async-local) #:use-module (crates-io))

(define-public crate-derive-async-local-0.4.0 (c (n "derive-async-local") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zgxfyw4wk6ak0x48wmggsplsz7hgp58llirbhicg2j758i5160b")))

(define-public crate-derive-async-local-0.4.1 (c (n "derive-async-local") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04nz89qj1mhsb5iskpzyhzagg962xqmidiapqka3y778870sn08j")))

(define-public crate-derive-async-local-0.5.0 (c (n "derive-async-local") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06aaf18nxdvxb8900vr2x7aq459dlmfw6r5695nd9v4s5vfi2jvi")))

(define-public crate-derive-async-local-0.5.1 (c (n "derive-async-local") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vh3yd96b2ahmjd8p4l0vjjc45wwhzhwjcsh4pf251rfllxp6rva")))

(define-public crate-derive-async-local-0.6.0 (c (n "derive-async-local") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1agkvv6xpx2kadx3jyfmxbbw3bk4az42178lxz86adak02zqfck3")))

(define-public crate-derive-async-local-0.7.0 (c (n "derive-async-local") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sck0kkbhxkv2zfvn5l8np3mfgnlvvb4s30fbd89khvs1p8q5lz5")))

(define-public crate-derive-async-local-0.8.0 (c (n "derive-async-local") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16fwhrv4wkc2cgwby51jgss0j4nv10jrc994ka9ppgl74cm0197q")))

(define-public crate-derive-async-local-0.9.0 (c (n "derive-async-local") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i8s4mllna8yhf4dqfl2kc17ps0lxxgr0y84qrykw4zn6s1xwzy6")))

(define-public crate-derive-async-local-0.10.0 (c (n "derive-async-local") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1adpcjh9s4izibsn0hmnhxfb5x2cp9vji09lx1b86mfj359w3nd3")))

(define-public crate-derive-async-local-0.11.0 (c (n "derive-async-local") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0klabr230d6n7zv53n87c91kj5k8cjx8w5qvygqhqcxal752k3yy")))

(define-public crate-derive-async-local-0.11.1 (c (n "derive-async-local") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08xs35id157a4rxlqzg9xi6kz891hqh2ppy7r8vnhqhf0h79a5rn")))

(define-public crate-derive-async-local-0.12.0 (c (n "derive-async-local") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "198j55k4f63kjdr5p9hk6m0i4ivbnch51xc9pqmmg01lm9iza90y")))

(define-public crate-derive-async-local-0.13.0 (c (n "derive-async-local") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xzfyqf2grdqwkq8apj7dn2nfazxwpxc6n08k7l3a76nnbkd1xh4")))

(define-public crate-derive-async-local-0.14.0 (c (n "derive-async-local") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kym7w7d1m3l2ars89038n69c0kc1wrpf2mjf6czzrghxkxm34w9")))

(define-public crate-derive-async-local-0.15.0 (c (n "derive-async-local") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "116s3rga6frkxmyxa2h1ls4pqz8gg2d94vff7mq4yd5n62q2fvm8")))

(define-public crate-derive-async-local-1.0.0 (c (n "derive-async-local") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gpig5z0cc26jr5s38ak1m2bklahv7g4ckidy4q501b3rn4qaq8m")))

(define-public crate-derive-async-local-1.1.0 (c (n "derive-async-local") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pam06kb913iqk1lj3mp9m0hz60fdkkwl4s0zx7scsxq77n71bna")))

(define-public crate-derive-async-local-1.1.1 (c (n "derive-async-local") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iq7gcvn6pwqg9sqcz230ylbm83hqyb7hr3jhirbzwrxdcmlch5j")))

(define-public crate-derive-async-local-1.1.2 (c (n "derive-async-local") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gaivlmgvmk1fzga3n5w855yydjznmbbh8hm9v5nqs04xaal90rs")))

(define-public crate-derive-async-local-1.2.0 (c (n "derive-async-local") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive"))) (d #t) (k 0)))) (h "105c3qrd7wggh6p5x1bfgddci0wryji77j9zfyqhhnvv4g17sall")))

(define-public crate-derive-async-local-2.0.0 (c (n "derive-async-local") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f7nrfrwpnaariikfrzql30ndhkncs21225ifrf2says1xvf357k")))

