(define-module (crates-io de ri derive-new) #:use-module (crates-io))

(define-public crate-derive-new-0.1.0 (c (n "derive-new") (v "0.1.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "0by815bajam9kwpc9vdw1gg5fl5qgaz8d4hqfs8vim670z1mbdnh")))

(define-public crate-derive-new-0.1.1 (c (n "derive-new") (v "0.1.1") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "107byzjh4rqzfyq48dzryzkvxm29i75kk745vc4ijkw5l6j0li4g")))

(define-public crate-derive-new-0.1.2 (c (n "derive-new") (v "0.1.2") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "1ij6q5v30sl3wzas8bqmyjj160yq010rbdd6ca16b4h70n0flsic")))

(define-public crate-derive-new-0.1.3 (c (n "derive-new") (v "0.1.3") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "035z7qmc3vy80yzary8xrfv42wh19k45bp6jqakv2iq3596ffdwv")))

(define-public crate-derive-new-0.2.0 (c (n "derive-new") (v "0.2.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "1qdw0z6bv52jxlxaiwjfzkcnysvfjkixvvc0pmam65j40bf6lpnn")))

(define-public crate-derive-new-0.3.0 (c (n "derive-new") (v "0.3.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "13mbqh703n05fy6y3r6b7sf4al7n91a6i293zf1h834yp6inrgj1")))

(define-public crate-derive-new-0.4.0 (c (n "derive-new") (v "0.4.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "0fr8ymf8likfxabgjm0pi144zv98c7mhlwsmhhxkjjb20vz0gqmi")))

(define-public crate-derive-new-0.5.0 (c (n "derive-new") (v "0.5.0") (d (list (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1flq9a2lbrcql1innf0cbygrhlgp1vdf3hj8nwz1q12ln1x64ps1") (f (quote (("compiletest" "compiletest_rs"))))))

(define-public crate-derive-new-0.5.1 (c (n "derive-new") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1hnknfzbn639x4jw8grdqqan2ms1b72y982913mgb9f8svhviy4j")))

(define-public crate-derive-new-0.5.2 (c (n "derive-new") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0wcxwppqhasbcmc589dwkrfakfyyixsjzv0wn3qliaa7mcxr5jvg")))

(define-public crate-derive-new-0.5.3 (c (n "derive-new") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0ykkqa4g41zrwh750xxh6lyjmxnkbvj09sqy4hxlsclv7x6iy28j")))

(define-public crate-derive-new-0.5.4 (c (n "derive-new") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0ym1jw1xc5av12ij9fxckvf6far8zbbqmv0f8kw194j4gjap7vff")))

(define-public crate-derive-new-0.5.5 (c (n "derive-new") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1akxknykn8z02r684dplm1vgwrxy1dslv6vjr7600kn14sbcg7l9")))

(define-public crate-derive-new-0.5.6 (c (n "derive-new") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0z6qwdlxfdjrdpfgb7acxvfn1kxfv99g92fpyi32a1xfjvl1993c")))

(define-public crate-derive-new-0.5.7 (c (n "derive-new") (v "0.5.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1c4bxqbpd456sv6b9zw0y4sqdb39w0la5jg7w7xirj993dbh9zf3") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive-new-0.5.8 (c (n "derive-new") (v "0.5.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ncibp4jhpkym7namg3viqyw8hljd32n6abg64af8qjwrn91iwvi") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive-new-0.5.9 (c (n "derive-new") (v "0.5.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0d9m5kcj1rdmdjqfgj7rxxhdzx0as7p4rp1mjx5j6w5dl2f3461l") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive-new-0.6.0 (c (n "derive-new") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1b8jv6jx0b8jgkz9kmz0ciqmnf74xkk0mmvkb5z1c87932kdwl6i") (f (quote (("std") ("default" "std"))))))

