(define-module (crates-io de ri derive_builder_macro_fork_arti) #:use-module (crates-io))

(define-public crate-derive_builder_macro_fork_arti-0.11.2 (c (n "derive_builder_macro_fork_arti") (v "0.11.2") (d (list (d (n "derive_builder_core") (r "=0.11.2") (d #t) (k 0) (p "derive_builder_core_fork_arti")) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wzb10608ksxfr5cjh0lan4brdrcpfqv4bkqpx3dk728l9lpg239") (f (quote (("clippy" "derive_builder_core/clippy"))))))

