(define-module (crates-io de ri derivit-core) #:use-module (crates-io))

(define-public crate-derivit-core-0.1.0 (c (n "derivit-core") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j4inbq0c53hsv6ja1v1q7zbh6cnmr6w36250kyx6qqrl68g1pic")))

(define-public crate-derivit-core-0.1.1 (c (n "derivit-core") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17g2bwssk2mksa4wzbj0pc3ybhj42jdsf333f3n5h74bhw67gjx9")))

(define-public crate-derivit-core-0.1.2 (c (n "derivit-core") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hz8ql2xr3fxy8czvp554yijkb4r49if5xn00j1jcwzmp7wplgkq")))

(define-public crate-derivit-core-0.1.5 (c (n "derivit-core") (v "0.1.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6iycbrqypjalvhm6z2i4iblxqb9m76pdysncl8r4dkvwr4v955")))

