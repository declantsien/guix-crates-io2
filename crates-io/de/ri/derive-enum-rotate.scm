(define-module (crates-io de ri derive-enum-rotate) #:use-module (crates-io))

(define-public crate-derive-enum-rotate-0.1.0 (c (n "derive-enum-rotate") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1wv0wd50r9di2cxf2g9qqdhb79mhpbnjb9p8w9jkin57vllkmh0b")))

(define-public crate-derive-enum-rotate-0.1.1 (c (n "derive-enum-rotate") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1yphbz1chhmssll2z8s2njvsmiksgcygqgacrw3w93bi2xicjsgb")))

