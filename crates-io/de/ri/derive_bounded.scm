(define-module (crates-io de ri derive_bounded) #:use-module (crates-io))

(define-public crate-derive_bounded-0.1.0 (c (n "derive_bounded") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1jjmzpxyvkba0g0blcw16kpzbzbq052yl7w8h6qm4v067rg3s2kz")))

(define-public crate-derive_bounded-0.2.0 (c (n "derive_bounded") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "085fyliw9xgxdq30hnvlas0v6n0hv7gksg3mml56c35wf85pmfmz")))

(define-public crate-derive_bounded-0.3.0 (c (n "derive_bounded") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0ydzk8pr4a63pjizrdx2pmlf1xyady6phr30na5vclqaqcr4gp25")))

(define-public crate-derive_bounded-0.4.0 (c (n "derive_bounded") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "19d8a7drhrchsx4dlh81ymir4g6z15ih4q1j2vvadblfjr3s40m8")))

(define-public crate-derive_bounded-0.5.0 (c (n "derive_bounded") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1nbj4vs1nivaynw9wq148y0c4j8lapvgsj8vnbd11azhqsyyd1s9")))

