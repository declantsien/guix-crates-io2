(define-module (crates-io de ri derivation) #:use-module (crates-io))

(define-public crate-derivation-0.1.0 (c (n "derivation") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "10bm7bqvvri0nfq63ci34l5szpr7c5wbabf6rbaw7hm9znqfl14l")))

