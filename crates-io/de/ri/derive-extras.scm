(define-module (crates-io de ri derive-extras) #:use-module (crates-io))

(define-public crate-derive-extras-0.1.0 (c (n "derive-extras") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "04zawmfy0mwl3499m0ybdn4w6msn42lwmq4dlzsqa9ydh80yx1pn")))

