(define-module (crates-io de ri derive_wrapper) #:use-module (crates-io))

(define-public crate-derive_wrapper-0.1.0 (c (n "derive_wrapper") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1wcym33z4f7akwvxz8f57hl2gm51vig4mb9ffr9hpq3cvxm816bn") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.1 (c (n "derive_wrapper") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "12vi1yv021ajpd8hq4mhv9981c81cl50j3xn38hlxiz2g0sv255k") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.2 (c (n "derive_wrapper") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "044bl7826vlq7mhdpy18rmhyq3mii71yi75z2aq78nphvn5np0di") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-derive_wrapper-0.1.3 (c (n "derive_wrapper") (v "0.1.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0sv3n0ra2a0pk2a3s944hw4c8ag5ag8aass4xd9n1iymy8h6xc42") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.4 (c (n "derive_wrapper") (v "0.1.4") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0a6fmb28fkc49n6fbp0lwnh0l3g3wplbbplpl91rzyjknf21inbc") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.5 (c (n "derive_wrapper") (v "0.1.5") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "15wx58wsfgvpl476s9y9q3ydpigdpn41f2yffx67wjjmipm9q2zf") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.6 (c (n "derive_wrapper") (v "0.1.6") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jq8vfc8j0r1ilfd64pmc1gq1mfjb23fd0dsikbl4fnxrac4izdn") (f (quote (("std") ("default" "std"))))))

(define-public crate-derive_wrapper-0.1.7 (c (n "derive_wrapper") (v "0.1.7") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18c5svlfgpr9k2b69dzm4x4682d0ab5cp5yd2s39bjj34j208mia") (f (quote (("std") ("default" "std"))))))

