(define-module (crates-io de ri derive_empty_traits) #:use-module (crates-io))

(define-public crate-derive_empty_traits-0.1.0 (c (n "derive_empty_traits") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0l38xli2yja1x4ngn3n37frhprrv31h1aihi8i7dkfh9mak0vj8j")))

