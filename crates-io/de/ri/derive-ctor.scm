(define-module (crates-io de ri derive-ctor) #:use-module (crates-io))

(define-public crate-derive-ctor-0.1.0 (c (n "derive-ctor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (d #t) (k 0)))) (h "0cxjqxvqhwx3ispdiifw48l0i06xdr4cmw19zb4yyckz64vajcx3")))

(define-public crate-derive-ctor-0.1.1 (c (n "derive-ctor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (d #t) (k 0)))) (h "12rlkw1vn2yqb5f2m5fhjqlnn4hm9bcv076pay19sk814bgvciwr") (f (quote (("no-std"))))))

(define-public crate-derive-ctor-0.2.0 (c (n "derive-ctor") (v "0.2.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (d #t) (k 0)))) (h "0vcal102x8vj4ns7r9pmskk89h982m80lbrflhp745hmjij44j4q") (f (quote (("no-std"))))))

