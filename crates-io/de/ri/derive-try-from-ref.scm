(define-module (crates-io de ri derive-try-from-ref) #:use-module (crates-io))

(define-public crate-derive-try-from-ref-0.1.0 (c (n "derive-try-from-ref") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0zi9zdbkflj8pq0czkd7kbdmhv88lndzkzapllpi6740zdy65fzv")))

