(define-module (crates-io de ri derive-com-impl) #:use-module (crates-io))

(define-public crate-derive-com-impl-0.1.0-alpha1 (c (n "derive-com-impl") (v "0.1.0-alpha1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i3fcpjcwpl9mmaidv5b7w0k8sfiy9d0vxc8xzf1wb0z110zfqnn")))

(define-public crate-derive-com-impl-0.1.0-alpha2 (c (n "derive-com-impl") (v "0.1.0-alpha2") (d (list (d (n "com-impl") (r "^0.1.0-alpha2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (k 2)))) (h "01k3l5g7dh2kj5s827sb9g7k19i7rn355yrgjhpcbhd4j6b5vpr2")))

(define-public crate-derive-com-impl-0.1.0-alpha3 (c (n "derive-com-impl") (v "0.1.0-alpha3") (d (list (d (n "com-impl") (r "^0.1.0-alpha2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (k 2)))) (h "1njb6z8idcy5vmz36qhj2jp5d80jhqbkmfpvppvbbk3mh7gk91h0")))

(define-public crate-derive-com-impl-0.1.0 (c (n "derive-com-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "00bvazb0szh9ab3hir0ciscb8aq595mq5gwc8ncy8hifxhk7gfjg")))

(define-public crate-derive-com-impl-0.1.1 (c (n "derive-com-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "10c30w8y00ndg7h7h0142dahwlgaajapkabqic4gj24g842sx6vi")))

(define-public crate-derive-com-impl-0.2.0 (c (n "derive-com-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "13yq2n5gz99kabfkww62n588hamm1ns4lrvbjqfbrp4lllzkbn1g")))

