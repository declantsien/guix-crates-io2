(define-module (crates-io de ri derive_derive) #:use-module (crates-io))

(define-public crate-derive_derive-1.0.0 (c (n "derive_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0r0n8x01js4mmqbsk3q6x4f8p0brw661gk8xgjsk2sldjafxqcad")))

(define-public crate-derive_derive-2.0.0 (c (n "derive_derive") (v "2.0.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "14m2h5brlr6bkshybm59b5ii6577lhkqy0q5zigraqvnx88fcdls")))

