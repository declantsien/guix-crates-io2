(define-module (crates-io de ri derivate) #:use-module (crates-io))

(define-public crate-derivate-0.1.0 (c (n "derivate") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "~0.5") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "0rs0vgm9rwgvb96m9yl2f37fgk605xv6g4cv3adji9xs6cvmkjzy") (f (quote (("nightly" "compiletest_rs"))))))

