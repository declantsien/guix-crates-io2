(define-module (crates-io de ri derive-discriminant) #:use-module (crates-io))

(define-public crate-derive-discriminant-0.1.0 (c (n "derive-discriminant") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4n6piyryk70w1v3m36grbxs9x65wzvhgd0yfdaqr3ck3f8xi9r")))

(define-public crate-derive-discriminant-0.1.1 (c (n "derive-discriminant") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1lw1j96zh2q4nrc12wbypfh23n04y42scw2iwrhq38dvjmwjaq9g")))

