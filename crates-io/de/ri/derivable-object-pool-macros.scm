(define-module (crates-io de ri derivable-object-pool-macros) #:use-module (crates-io))

(define-public crate-derivable-object-pool-macros-0.1.0 (c (n "derivable-object-pool-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.24") (d #t) (k 0)))) (h "0gmjb0h3k1cl2kydg249fzwan3v5kvcwfpn3jm2b0k6pz3b7fmj3")))

