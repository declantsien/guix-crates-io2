(define-module (crates-io de ri derive-ex) #:use-module (crates-io))

(define-public crate-derive-ex-0.1.0 (c (n "derive-ex") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1limlnyzjijk6w4rywxixzvzfk17cqyn2lx2vkiqiw03lc6sf0jl")))

(define-public crate-derive-ex-0.1.1 (c (n "derive-ex") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1zdq4dwj9jhzbw1q04p5cjpsjjip4gsnksnbpv887vx70vhibkcq")))

(define-public crate-derive-ex-0.1.2 (c (n "derive-ex") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1586im0nh79dzbkj75m8kq237rk7yyacabh4qxxh4h0scy32l64c")))

(define-public crate-derive-ex-0.1.3 (c (n "derive-ex") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0nhaxaqs1m0xl1p89ywg9iyh1mmflcivnmminw36yzgy9z5bn78w")))

(define-public crate-derive-ex-0.1.4 (c (n "derive-ex") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0xbrnm6wg4gp5ndjpw7b0rxvrijg17w6kifw50p61wvha43wpbvx")))

(define-public crate-derive-ex-0.1.5 (c (n "derive-ex") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "02jxq5z5n0a8samvzwf1gbxqhz7jkm2971bbd6lvjjvv0wsq729l")))

(define-public crate-derive-ex-0.1.6 (c (n "derive-ex") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "04vab1krf479dys1ppxhgh3sni52mfwb4jisdap1h0mwsszaxxd8")))

(define-public crate-derive-ex-0.1.7 (c (n "derive-ex") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0rafkn6qw6fh5hhfjqb8f4rp4ymngbaa4yyh0ry3fr75cx04kflm")))

(define-public crate-derive-ex-0.1.8 (c (n "derive-ex") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "01slhi25a1j2qcsxqcpm79qs4q4hmsifqix8d1zx973bkwlmzadv")))

