(define-module (crates-io de ri derive_lit) #:use-module (crates-io))

(define-public crate-derive_lit-0.1.0 (c (n "derive_lit") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1i7v6kvaqpfz4c29wyd201yjj0w6igby9sq63qbyb9y82h8qwzvh")))

