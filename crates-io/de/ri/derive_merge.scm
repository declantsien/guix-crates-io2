(define-module (crates-io de ri derive_merge) #:use-module (crates-io))

(define-public crate-derive_merge-0.1.0 (c (n "derive_merge") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16x401hgacfcizv0ppqwl37jr35p8infkb4g4nyjffr61b7h40vw")))

