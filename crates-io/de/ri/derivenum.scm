(define-module (crates-io de ri derivenum) #:use-module (crates-io))

(define-public crate-derivenum-0.1.0 (c (n "derivenum") (v "0.1.0") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07hxrrgxvvpgdygwwnwfl1qhj7hi5z2r6pp341ygmzrfj2s0hkdf")))

(define-public crate-derivenum-0.1.1 (c (n "derivenum") (v "0.1.1") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yfcn9ysa0vibzr3pyxjdjqk7j0v9039vh4gmq6yqq3hnxp5hzhv")))

