(define-module (crates-io de ri derive-box-dyn) #:use-module (crates-io))

(define-public crate-derive-box-dyn-0.1.0 (c (n "derive-box-dyn") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "14kkdrxmyf6rrq7hvfqdshp8ij0x30rzg48kmmlimx89d9n9c3d3")))

(define-public crate-derive-box-dyn-0.1.1 (c (n "derive-box-dyn") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0y78ixy1hm3cjns6sy65w0z7fqrv3la4h2cxf9xlylkr7bbvlnpf")))

