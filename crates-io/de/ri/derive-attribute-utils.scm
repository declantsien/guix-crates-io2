(define-module (crates-io de ri derive-attribute-utils) #:use-module (crates-io))

(define-public crate-derive-attribute-utils-0.1.0 (c (n "derive-attribute-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn_v1") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0) (p "syn")) (d (n "syn_v2") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0) (p "syn")) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jf7nq1dwrlpzv1fdzm3dn0vafynq39wly1mhnf8wvl0hzvqmrqs") (f (quote (("syn_2" "syn_v2") ("syn_1" "syn_v1"))))))

(define-public crate-derive-attribute-utils-0.1.1 (c (n "derive-attribute-utils") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn_v1") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0) (p "syn")) (d (n "syn_v2") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0) (p "syn")) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zmyna1phayy5dlxq4w6gyq4w0619k1qs3145wl81vhx1ngv9hfc") (f (quote (("syn_2" "syn_v2") ("syn_1" "syn_v1"))))))

