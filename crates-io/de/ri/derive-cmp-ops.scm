(define-module (crates-io de ri derive-cmp-ops) #:use-module (crates-io))

(define-public crate-derive-cmp-ops-0.1.0 (c (n "derive-cmp-ops") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "04plxi80s40b5axpmny0vwc3g8fkznh7yv7k350r1s0kspqfpi4p")))

