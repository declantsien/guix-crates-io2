(define-module (crates-io de ri derive-quickcheck-arbitrary) #:use-module (crates-io))

(define-public crate-derive-quickcheck-arbitrary-0.1.0 (c (n "derive-quickcheck-arbitrary") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "1i2da45knvm0dkhcagk9vc039i664ywqcsnd6n5b4af8zcrwkaas")))

(define-public crate-derive-quickcheck-arbitrary-0.1.1 (c (n "derive-quickcheck-arbitrary") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "1i9lw2ihswd8y5cl26waqkxvdbhmsr1j85lvsa13n6ck0a7vxfk5")))

(define-public crate-derive-quickcheck-arbitrary-0.1.2 (c (n "derive-quickcheck-arbitrary") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "0ij5vkbyq6ak3jfzykxkmvf12ff172gj6rpxbv1gs217sn828p7k")))

(define-public crate-derive-quickcheck-arbitrary-0.1.3 (c (n "derive-quickcheck-arbitrary") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (d #t) (k 2)))) (h "1raci37cy7h7ijyg1ykvap98zab003ad0f4x2b9xmx68ib1qazb9")))

