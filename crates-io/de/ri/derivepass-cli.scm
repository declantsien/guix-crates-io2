(define-module (crates-io de ri derivepass-cli) #:use-module (crates-io))

(define-public crate-derivepass-cli-0.1.0 (c (n "derivepass-cli") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "radix64") (r "^0.6.2") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "scrypt") (r "^0.2") (k 0)))) (h "1hmrabbvk9p0naxhd3hmrw0kpmlvrya8ah3878ni6279fas0hijy")))

