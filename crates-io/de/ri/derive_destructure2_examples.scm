(define-module (crates-io de ri derive_destructure2_examples) #:use-module (crates-io))

(define-public crate-derive_destructure2_examples-0.1.0 (c (n "derive_destructure2_examples") (v "0.1.0") (d (list (d (n "derive_destructure2") (r "^0.0.1") (d #t) (k 0)))) (h "1ha7d5z4r7vjn5ijgmayygd5dnyyxldzvprdscj3schccvqi9122")))

(define-public crate-derive_destructure2_examples-0.1.1 (c (n "derive_destructure2_examples") (v "0.1.1") (d (list (d (n "derive_destructure2") (r "^0.0.1") (d #t) (k 0)))) (h "0zpw9ihnasvfg3kwqcfw0f2k2gm5pxzzzwgf114wrp9arslv42cz")))

(define-public crate-derive_destructure2_examples-0.1.2 (c (n "derive_destructure2_examples") (v "0.1.2") (d (list (d (n "derive_destructure2") (r "^0.0.1") (d #t) (k 0)))) (h "1ryvmgpw3z3icwxmr587ydjm6yqs0m40j4khlh26k62qjwd8zxmd")))

(define-public crate-derive_destructure2_examples-0.1.3 (c (n "derive_destructure2_examples") (v "0.1.3") (d (list (d (n "derive_destructure2") (r "^0.1.0") (d #t) (k 0)))) (h "0ijllqr9rg66lgilam99p253lhrf0d1jp33phyc1qx76zgsmvwx6")))

(define-public crate-derive_destructure2_examples-0.1.4 (c (n "derive_destructure2_examples") (v "0.1.4") (d (list (d (n "derive_destructure2") (r "^0.1.1") (d #t) (k 0)))) (h "1dn51fvcfs4iycv81xr5vhcdfxzza3z0msv46dln1qqsyj0qknx6")))

