(define-module (crates-io de ri derive-macros) #:use-module (crates-io))

(define-public crate-derive-macros-1.0.0 (c (n "derive-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16d4abi9xy0xhb3n0vr2akszcp3h51c51mf7g3w0k15wbzcf2nvp") (f (quote (("toml") ("json") ("default"))))))

(define-public crate-derive-macros-2.0.0 (c (n "derive-macros") (v "2.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jdxp11b03dwrqxk7da7zd04kwc81ws6xw402n7wqnk0m4g63in3") (f (quote (("yaml") ("toml") ("json") ("default"))))))

(define-public crate-derive-macros-2.1.0 (c (n "derive-macros") (v "2.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0r2z7si56vbk70whizs69g9r012nrzmcrf2fb3k7vsiijv4ipgiz") (f (quote (("yaml") ("toml") ("json") ("dirs") ("default"))))))

(define-public crate-derive-macros-2.1.1 (c (n "derive-macros") (v "2.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11ag3aqlarpmn4lsjx6vpj01z5vaj69s44h00qk1hhbi6rjbw924") (f (quote (("yaml") ("toml") ("json") ("dirs") ("default"))))))

