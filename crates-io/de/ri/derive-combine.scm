(define-module (crates-io de ri derive-combine) #:use-module (crates-io))

(define-public crate-derive-combine-0.1.0 (c (n "derive-combine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1x1dlavazxsfrwn65hyhyjrw34i928gkrj87jzzinck4zjnqsh4k")))

