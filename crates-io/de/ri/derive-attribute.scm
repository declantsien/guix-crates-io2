(define-module (crates-io de ri derive-attribute) #:use-module (crates-io))

(define-public crate-derive-attribute-0.1.0 (c (n "derive-attribute") (v "0.1.0") (d (list (d (n "derive-attribute-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "derive-attribute-utils") (r "^0.1.0") (d #t) (k 0)))) (h "071n36w3vhzr3isgakqdia0xr0vlr8dd1rg7nnsmhq66xf1ipk2k") (f (quote (("syn_2" "derive-attribute-utils/syn_2" "derive-attribute-macros/syn_2") ("syn_1" "derive-attribute-utils/syn_1" "derive-attribute-macros/syn_1"))))))

(define-public crate-derive-attribute-0.1.1 (c (n "derive-attribute") (v "0.1.1") (d (list (d (n "derive-attribute-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "derive-attribute-utils") (r "^0.1.0") (d #t) (k 0)))) (h "15l1gjhqd45a0rj84ff69iz77ly51khbrx379q97zab0240ajypc") (f (quote (("syn_2" "derive-attribute-utils/syn_2" "derive-attribute-macros/syn_2") ("syn_1" "derive-attribute-utils/syn_1" "derive-attribute-macros/syn_1"))))))

(define-public crate-derive-attribute-0.1.2 (c (n "derive-attribute") (v "0.1.2") (d (list (d (n "derive-attribute-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "derive-attribute-utils") (r "^0.1.1") (d #t) (k 0)))) (h "0l83iv6d8z5y484kldrrspx1j051rcc560rhkcjsxlhmj1acig1a") (f (quote (("syn_2" "derive-attribute-utils/syn_2" "derive-attribute-macros/syn_2") ("syn_1" "derive-attribute-utils/syn_1" "derive-attribute-macros/syn_1"))))))

