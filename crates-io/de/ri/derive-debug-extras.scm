(define-module (crates-io de ri derive-debug-extras) #:use-module (crates-io))

(define-public crate-derive-debug-extras-0.1.0 (c (n "derive-debug-extras") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.1.0") (d #t) (k 0)))) (h "0mslv6k7hqfbd2nmm0nl2n3mmmv3pzpf2cgxzlr8hzb6dprjpf2v")))

(define-public crate-derive-debug-extras-0.2.0 (c (n "derive-debug-extras") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.2.0") (d #t) (k 0)))) (h "1swwimq4m7g3jzdqmnk1k3dn2jiq3lg7nmdvvhvdkmc1cjrw0gqy") (f (quote (("auto-debug-single-tuple-inline"))))))

(define-public crate-derive-debug-extras-0.2.1 (c (n "derive-debug-extras") (v "0.2.1") (d (list (d (n "syn-helpers") (r "^0.4.0") (d #t) (k 0)))) (h "1a64wwki9gd55bxks98j1yhnb3l8zlfm0chra2yj0wf936rccs20") (f (quote (("auto-debug-single-tuple-inline"))))))

(define-public crate-derive-debug-extras-0.2.2 (c (n "derive-debug-extras") (v "0.2.2") (d (list (d (n "syn-helpers") (r "^0.4.1") (d #t) (k 0)))) (h "0cfiv6cqhjrm0h5v15pp2766qpzi8nyly587i5mxncx9fy6adrzh") (f (quote (("auto-debug-single-tuple-inline"))))))

(define-public crate-derive-debug-extras-0.3.0 (c (n "derive-debug-extras") (v "0.3.0") (d (list (d (n "syn-helpers") (r "^0.5") (d #t) (k 0)))) (h "03da04dqd5yi9wcwps9qf03ajdvk6m8mfqk3c2m3gilsjp2h8smh") (f (quote (("auto-debug-single-tuple-inline"))))))

