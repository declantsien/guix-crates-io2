(define-module (crates-io de ri derive-system) #:use-module (crates-io))

(define-public crate-derive-system-0.1.0 (c (n "derive-system") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "lifecycle") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fl8lpwqzxns6dlki6411qhz0bpn65a8pijhfbcja0fjqww879a6")))

