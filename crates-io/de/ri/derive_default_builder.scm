(define-module (crates-io de ri derive_default_builder) #:use-module (crates-io))

(define-public crate-derive_default_builder-0.1.2 (c (n "derive_default_builder") (v "0.1.2") (d (list (d (n "derive_default_builder_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1nmxpywmqs2z17pqsp8y3rsmja8wc60jls5d1xn8g9fpwzkkp449")))

(define-public crate-derive_default_builder-0.1.4 (c (n "derive_default_builder") (v "0.1.4") (d (list (d (n "derive_default_builder_derive") (r "^0.1.3") (d #t) (k 0)))) (h "1j6xnlxx6kdmwgn13vbp4c49cvx9bf7xr611gd8cjzyb4yqcdgay")))

(define-public crate-derive_default_builder-0.1.5 (c (n "derive_default_builder") (v "0.1.5") (d (list (d (n "derive_default_builder_derive") (r "^0.1.5") (d #t) (k 0)))) (h "04i4pxbcd9rniy97dwxnkd24z07dcxk21c9ikzr2sk1jsczkjgxk")))

(define-public crate-derive_default_builder-0.1.6 (c (n "derive_default_builder") (v "0.1.6") (d (list (d (n "derive_default_builder_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0fdpmkfkms0dfv8bwpxnin0csxaah11b9afajrx4jk8b006r27wh")))

(define-public crate-derive_default_builder-0.1.7 (c (n "derive_default_builder") (v "0.1.7") (d (list (d (n "derive_default_builder_derive") (r "^0.1.6") (d #t) (k 0)))) (h "08z9wz6mkrw55s88zvchkynxwkailhnbmqizn63r9i6yb3yhzwnc")))

(define-public crate-derive_default_builder-0.1.8 (c (n "derive_default_builder") (v "0.1.8") (d (list (d (n "derive_default_builder_derive") (r "^0.1.8") (d #t) (k 0)))) (h "1ara4qhhsm4ag8mcsr0iiv3a1sl6r3bgv9hvjl3hyx8q0fdzrfx4")))

