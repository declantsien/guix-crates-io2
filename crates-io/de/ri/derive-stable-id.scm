(define-module (crates-io de ri derive-stable-id) #:use-module (crates-io))

(define-public crate-derive-stable-id-0.1.0 (c (n "derive-stable-id") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "065wbqh1r42f3j7nzk5ary3vpr98m11m4mp45nzy83vg20hyjcyc")))

(define-public crate-derive-stable-id-0.2.0 (c (n "derive-stable-id") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0nq6yxnpbgv3s05kr38c3aibnmqr5zdvqsh2bn4qaxgvl41crfkb")))

(define-public crate-derive-stable-id-0.2.1 (c (n "derive-stable-id") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0jz6p0ry2rampi7j7vjbd29gl51aaw0d9idz7zdhzvw87b03w491")))

(define-public crate-derive-stable-id-0.3.0 (c (n "derive-stable-id") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "12lbi99mr0pny8jka48vq46w9hz4gy38f21xpcr9c9r3955jbchr")))

