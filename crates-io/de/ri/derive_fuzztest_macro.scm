(define-module (crates-io de ri derive_fuzztest_macro) #:use-module (crates-io))

(define-public crate-derive_fuzztest_macro-0.1.0 (c (n "derive_fuzztest_macro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "010ylr55vkpqwmbfz88qriz3hybkwb7zgpfy9qhv7inwac61dqf8") (f (quote (("quickcheck") ("proptest"))))))

(define-public crate-derive_fuzztest_macro-0.1.1 (c (n "derive_fuzztest_macro") (v "0.1.1") (d (list (d (n "derive_fuzztest") (r "^0.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2.16") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w7k2i26dppidk4411l3mgjh33vdpzl01ym3zp384bfxajxm2rii") (f (quote (("quickcheck") ("proptest"))))))

