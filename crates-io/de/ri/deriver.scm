(define-module (crates-io de ri deriver) #:use-module (crates-io))

(define-public crate-deriver-0.0.0 (c (n "deriver") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0a7sdzmv5hfwxnhwvmc27bxiwd2bqc6ka6c318kwqbb1mm8jqxdk") (f (quote (("default"))))))

