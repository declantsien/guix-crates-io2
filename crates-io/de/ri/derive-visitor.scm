(define-module (crates-io de ri derive-visitor) #:use-module (crates-io))

(define-public crate-derive-visitor-0.1.0 (c (n "derive-visitor") (v "0.1.0") (d (list (d (n "derive-visitor-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lrlmlapw73rcm4vx4kw0h6vf3alb5wkzdf1g6wjf9jkmvwhz4sf") (y #t)))

(define-public crate-derive-visitor-0.1.1 (c (n "derive-visitor") (v "0.1.1") (d (list (d (n "derive-visitor-macros") (r "^0.1.0") (d #t) (k 0)))) (h "181ps3aanfh026f57mrz5c17ypny2b3a802qghsfglv35n4gpcba")))

(define-public crate-derive-visitor-0.1.2 (c (n "derive-visitor") (v "0.1.2") (d (list (d (n "derive-visitor-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1b775pzsbcwry5bs27psd8r5ghzzsg04nd2h8m9dbl6qx1cn1cqd") (f (quote (("std-types-drive"))))))

(define-public crate-derive-visitor-0.2.1 (c (n "derive-visitor") (v "0.2.1") (d (list (d (n "derive-visitor-macros") (r "^0.2.1") (d #t) (k 0)))) (h "0p3h9xm0hl6dlal5m60cmv5009a4bxa276fj6lp7ypbh55nqaadw") (f (quote (("std-types-drive"))))))

(define-public crate-derive-visitor-0.3.0 (c (n "derive-visitor") (v "0.3.0") (d (list (d (n "derive-visitor-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1mhwhqvnbvxq6ppg6w1ksb062ikr15k84gbgqjza2xwry4r5h111") (f (quote (("std-types-drive"))))))

(define-public crate-derive-visitor-0.4.0 (c (n "derive-visitor") (v "0.4.0") (d (list (d (n "derive-visitor-macros") (r "^0.4.0") (d #t) (k 0)))) (h "17nacybndh0n2ayf0vchcrm5n4k6bmx605ijvax7qw5rhggnawfl") (f (quote (("std-types-drive"))))))

