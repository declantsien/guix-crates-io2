(define-module (crates-io de ri derived) #:use-module (crates-io))

(define-public crate-derived-0.0.1 (c (n "derived") (v "0.0.1") (h "19hwnqz093gilpfx9kn5lskc57b6clg6jdj801ir6059cx34whx8") (y #t)))

(define-public crate-derived-0.1.0 (c (n "derived") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjs937xd30x60694lf3x6mzvnr2a22h8szli16p699mp3skashb") (y #t)))

(define-public crate-derived-0.1.1 (c (n "derived") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0al8r7fqc9zgf45v2fmygs67811j9hz4dxl8ywh7q345lnd9hx1d")))

(define-public crate-derived-0.2.0 (c (n "derived") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "03cwz0nwd4dizzvws6c8ix3pmyg5613ljiml2d7p15ph6qnx3cgf")))

(define-public crate-derived-0.3.0 (c (n "derived") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbzii5apxyi8ayinghflaaalbf33576b7z33lhjich468p7vvhf")))

(define-public crate-derived-0.3.1 (c (n "derived") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "19zs034v761nal3jryscwinv3hdfzl0md96w5g2qgyhgaycnmsp8")))

(define-public crate-derived-0.4.0 (c (n "derived") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1s152cd8xa8qw1iv6scp5bx76c0nwczm71vh1cvpbsgjjxpf8g")))

(define-public crate-derived-0.4.1 (c (n "derived") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0m3c2cqrh3ra3dj6c0bdax0akyl4bv2gk2319r7l6zpfclyb07cs")))

(define-public crate-derived-0.4.2 (c (n "derived") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0p26ddapa59s5gvpvnpqwkf9j369jsvql59h80y0mpjznvibdxgd")))

