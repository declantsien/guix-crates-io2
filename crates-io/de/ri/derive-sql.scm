(define-module (crates-io de ri derive-sql) #:use-module (crates-io))

(define-public crate-derive-sql-0.1.0 (c (n "derive-sql") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 2)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "09s7h00j0as6d8cxxhvm0cjcmb19rphjxc9pacmc7mbk42yqknij")))

(define-public crate-derive-sql-0.2.0 (c (n "derive-sql") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 2)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pii56vb9z5k5k56azxmnvwdazllwzvpqcsi4m27z5ajrxys7064")))

(define-public crate-derive-sql-0.2.1 (c (n "derive-sql") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 2)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ff1i8z68rhrcccgk5128r6kqx3xcqbdnfzbjrd09l160i161869")))

(define-public crate-derive-sql-0.3.0 (c (n "derive-sql") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("chrono"))) (d #t) (k 2)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0ahysjc61zpw0hw09sq2dx3z6ssx713kjhgv4bp8msn678njaaar")))

(define-public crate-derive-sql-0.4.0 (c (n "derive-sql") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("chrono"))) (d #t) (k 2)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "12cy59q40lnf5bqlmnl80168daxf0cimy5q6arhr9f22sixh7ky9")))

(define-public crate-derive-sql-0.5.0 (c (n "derive-sql") (v "0.5.0") (d (list (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (d #t) (k 2)) (d (n "rusqlite") (r "^0") (f (quote ("chrono"))) (d #t) (k 2)))) (h "05caw1la898is6k8vx31kc4ch41zqpxr6q1chv4b8249b1qv6cwz") (f (quote (("with-mock") ("sqlite" "derive-sql-sqlite")))) (y #t)))

(define-public crate-derive-sql-0.5.1 (c (n "derive-sql") (v "0.5.1") (d (list (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (d #t) (k 2)) (d (n "rusqlite") (r "^0") (f (quote ("chrono"))) (d #t) (k 2)))) (h "0jzq1vfhs13y09hql5wy2si63r9razn1zl3p0hp3bvgx4b59p8vp") (f (quote (("with-mock") ("sqlite" "derive-sql-sqlite")))) (y #t)))

(define-public crate-derive-sql-0.5.2 (c (n "derive-sql") (v "0.5.2") (d (list (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (d #t) (k 2)) (d (n "rusqlite") (r "^0") (f (quote ("chrono"))) (d #t) (k 2)))) (h "1lyabkpsf6xn45gylxx9m3a0qizy1wx225jlk44llrgbbpw2m34y") (f (quote (("with-mock") ("sqlite" "derive-sql-sqlite"))))))

(define-public crate-derive-sql-0.6.0 (c (n "derive-sql") (v "0.6.0") (d (list (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0") (d #t) (k 2)))) (h "1lfc7r88lyppj3vfp1g6igrzfhy3bcd1rv42p6fcf91pkk0fqnq6") (f (quote (("sqlite" "derive-sql-sqlite") ("default"))))))

(define-public crate-derive-sql-0.7.0 (c (n "derive-sql") (v "0.7.0") (d (list (d (n "derive-sql-mysql") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0") (d #t) (k 2)))) (h "111vzv0is7ld2yc1i3xxbkgg6nkrvy69mywc22xq1wjqm7qkznpg") (f (quote (("sqlite" "derive-sql-sqlite") ("mysql" "derive-sql-mysql") ("default"))))))

(define-public crate-derive-sql-0.8.0 (c (n "derive-sql") (v "0.8.0") (d (list (d (n "derive-sql-mysql") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0") (d #t) (k 2)))) (h "1c81pnqwxim2q255rgpz3x33m4alwd1ym4wz2psvl9jblvm9z9nq") (f (quote (("sqlite" "derive-sql-sqlite") ("mysql" "derive-sql-mysql") ("default"))))))

(define-public crate-derive-sql-0.8.1 (c (n "derive-sql") (v "0.8.1") (d (list (d (n "derive-sql-mysql") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0") (d #t) (k 2)))) (h "0j93k2s99a4n12r5v1qhbi2qvwzmfciz1y89dwpla2x52r5jq8s1") (f (quote (("sqlite" "derive-sql-sqlite") ("mysql" "derive-sql-mysql") ("default"))))))

(define-public crate-derive-sql-0.9.0 (c (n "derive-sql") (v "0.9.0") (d (list (d (n "derive-sql-mysql") (r "^0") (o #t) (d #t) (k 0)) (d (n "derive-sql-sqlite") (r "^0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mockall") (r "^0") (d #t) (k 2)) (d (n "mysql") (r "^24") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0") (f (quote ("vtab"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zyd7ckszqrjgmfj2ms0y94m4wkamfj31rybw6c7dyckjh49vlfw") (f (quote (("sqlite" "rusqlite" "derive-sql-sqlite") ("default")))) (s 2) (e (quote (("mysql" "dep:mysql" "derive-sql-mysql"))))))

