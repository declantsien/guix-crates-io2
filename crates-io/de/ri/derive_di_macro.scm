(define-module (crates-io de ri derive_di_macro) #:use-module (crates-io))

(define-public crate-derive_di_macro-0.2.0 (c (n "derive_di_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sm6fixcqzb5qiq8ri8glpbacyzwzlg8rv1fba6ag6ys8k650791")))

(define-public crate-derive_di_macro-0.2.1 (c (n "derive_di_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m3g1y49wda0gq6cznbqm6b5wbfhmnbkdqqklg0r600p3bizy501")))

(define-public crate-derive_di_macro-0.3.0 (c (n "derive_di_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "141s513v4857022n638p2kj28g2nvqnq4azw7gd5mgcczywwlf1g")))

