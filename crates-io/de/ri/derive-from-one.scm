(define-module (crates-io de ri derive-from-one) #:use-module (crates-io))

(define-public crate-derive-from-one-0.1.0 (c (n "derive-from-one") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zv91wrd28klf0x58cbsjcyxm570hinxhnps6za83rj57w1lgnmc") (r "1.70")))

