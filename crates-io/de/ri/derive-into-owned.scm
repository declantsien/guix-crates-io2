(define-module (crates-io de ri derive-into-owned) #:use-module (crates-io))

(define-public crate-derive-into-owned-0.1.0 (c (n "derive-into-owned") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0qnac89fds5m3rz6s2g557rxy2l3qgcshyw8ll9j0n8xsc2cwvsp")))

(define-public crate-derive-into-owned-0.2.0 (c (n "derive-into-owned") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08y9qkiyqgm4sjcnl1xnm4f3ffb3pi4g8f46daqaf69q3vc9979c")))

