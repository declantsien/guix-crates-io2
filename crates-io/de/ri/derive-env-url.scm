(define-module (crates-io de ri derive-env-url) #:use-module (crates-io))

(define-public crate-derive-env-url-1.0.0 (c (n "derive-env-url") (v "1.0.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "091cmygbn109av9g7n7prwqb7ynkv78b59q3bmrlp7d0imk597fs")))

(define-public crate-derive-env-url-1.0.1 (c (n "derive-env-url") (v "1.0.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "08sa1dslbdg5drh96l1q2gn99p4x0b5a9pdr1hxkmg6rl568p7j6")))

(define-public crate-derive-env-url-1.0.2 (c (n "derive-env-url") (v "1.0.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0z4b778bxkri6q3g7rlnzwkdk4qmfrvxb85mlb1kbzmmm8a0dqa6") (y #t)))

(define-public crate-derive-env-url-1.0.3 (c (n "derive-env-url") (v "1.0.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1kdv212aics8mwa40xx1a7flpx7mp4nh2h1gwxqgi6hi3sk720n4")))

(define-public crate-derive-env-url-1.0.4 (c (n "derive-env-url") (v "1.0.4") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1dp7szkg2v6n7gmid29a25db4r6jg9a80gyhyzb6l0b8q8spmbj0")))

(define-public crate-derive-env-url-2.0.0 (c (n "derive-env-url") (v "2.0.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "04g0w1cqwabi8mkpshx8jyb3sf4b21lbb4hzp6nkhn5pbznsha2g") (y #t)))

(define-public crate-derive-env-url-2.0.1 (c (n "derive-env-url") (v "2.0.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0cclzanjynnqr6p9i4ba9c8nq8rav0v2aspmy8svqa2cpk35f8vz") (y #t)))

(define-public crate-derive-env-url-2.0.2 (c (n "derive-env-url") (v "2.0.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0kdjrfdbrwkv0wfl9xv99mzlb18c2gcjqwdvx3m0r512kv0pnv5q") (y #t)))

(define-public crate-derive-env-url-2.0.3 (c (n "derive-env-url") (v "2.0.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "07fp3jsxayl74l5ganslh1w8z2l22z0lpmy5vpynf7hzzh0v0dvm")))

(define-public crate-derive-env-url-2.0.4 (c (n "derive-env-url") (v "2.0.4") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0vwv6rdrns0rmnkf6sf9q4fdh4lgvf37qn5h557zzh98aqgk1kad")))

(define-public crate-derive-env-url-2.1.0 (c (n "derive-env-url") (v "2.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1vw41ky1ms33lsfynb42sy3a8asdnblb3269hcyk91rnj7bmsjzb")))

