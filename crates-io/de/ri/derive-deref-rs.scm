(define-module (crates-io de ri derive-deref-rs) #:use-module (crates-io))

(define-public crate-derive-deref-rs-0.1.0 (c (n "derive-deref-rs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zhc9wzzrdwrvn3z0hqylmz5jrmwncy4v53c0zg1q57lv5sdzykp")))

(define-public crate-derive-deref-rs-0.1.1 (c (n "derive-deref-rs") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "19pqf4vc85gsmq26y6gzzz85f752ag5kxqyq0dfy9by7nijvli7v")))

