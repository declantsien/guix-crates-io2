(define-module (crates-io de ri derive_jface) #:use-module (crates-io))

(define-public crate-derive_jface-0.1.0 (c (n "derive_jface") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v1xbf5jl2xn85nzvmp1vhnnar6y3a4zxk1i7i5f6m15wzgkzd79")))

(define-public crate-derive_jface-0.1.1 (c (n "derive_jface") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fm0sr7vzcbj2j48my6m82g418xr36jf4icsx876nan10s829b58")))

