(define-module (crates-io de ri derivation-path) #:use-module (crates-io))

(define-public crate-derivation-path-0.1.0 (c (n "derivation-path") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (k 0)))) (h "0176gc0was9asvs0a42fblnp2p5vg0iwjqxccig5vqlwnbak4p02") (f (quote (("std") ("default" "std"))))))

(define-public crate-derivation-path-0.1.1 (c (n "derivation-path") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (k 0)))) (h "0cbv6k1hhir073wcxk9dxvx7zdi7dbpldl2hawm9yq5xhgbny3h4") (f (quote (("std") ("default" "std"))))))

(define-public crate-derivation-path-0.1.2 (c (n "derivation-path") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (k 0)))) (h "094wq56jvvpap73z3m5la9r30lslvww7slllvc1qmr88y4hc5bmx") (f (quote (("std") ("default" "std"))))))

(define-public crate-derivation-path-0.1.3 (c (n "derivation-path") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (k 0)))) (h "1nz2nwid35zayshmr3p9fn4inm1nw9sigxjgc05ljnn7r2l8hcqr") (f (quote (("std") ("default" "std"))))))

(define-public crate-derivation-path-0.2.0 (c (n "derivation-path") (v "0.2.0") (h "1w5dw1vkh544wkjl1dx5c1hz4x912mxyq0wchvnxic8x78ckfp3f") (f (quote (("std") ("default" "std"))))))

