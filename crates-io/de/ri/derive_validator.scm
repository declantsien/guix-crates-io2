(define-module (crates-io de ri derive_validator) #:use-module (crates-io))

(define-public crate-derive_validator-0.1.0 (c (n "derive_validator") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "validify_types") (r "^0.1.0") (d #t) (k 0)))) (h "1r35lklm3zbhj0az2sgxrzibzz8r6crkqbldvy30n7ypxmd411ps") (y #t)))

