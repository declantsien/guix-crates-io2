(define-module (crates-io de ri derive-com-wrapper) #:use-module (crates-io))

(define-public crate-derive-com-wrapper-0.1.0-alpha1 (c (n "derive-com-wrapper") (v "0.1.0-alpha1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0rpwgv6hvfwq9kp8aswd8zxz243z2fwr3qmlf8cqhlvzpiapldjr")))

(define-public crate-derive-com-wrapper-0.1.0-alpha2 (c (n "derive-com-wrapper") (v "0.1.0-alpha2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1wjn4mxl1rd15w17kkgw7pfrfxwhc91kr4arkp8wazj6kh754n8m")))

(define-public crate-derive-com-wrapper-0.1.0-alpha3 (c (n "derive-com-wrapper") (v "0.1.0-alpha3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "00ixpwxbgry4aymcm9v237gb74drncwdb4fcyf8lqlgb13if1j5y")))

(define-public crate-derive-com-wrapper-0.1.0-alpha4 (c (n "derive-com-wrapper") (v "0.1.0-alpha4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0z15fx7ip4jcg57a5vhcwy28f93mswq9jafrl5i9lnf1gfm10srp")))

(define-public crate-derive-com-wrapper-0.1.0 (c (n "derive-com-wrapper") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z9zsns52b3lzavcpd1hzqynnhgklz6yynnvh41s38xkca3vk4ks")))

(define-public crate-derive-com-wrapper-0.1.0-docstest (c (n "derive-com-wrapper") (v "0.1.0-docstest") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bjdic81csvfwszy482lm7h14gh07qkzfwp95n0d5fhfhxgdvmh7") (y #t)))

