(define-module (crates-io de ri derive_type_level) #:use-module (crates-io))

(define-public crate-derive_type_level-0.0.20 (c (n "derive_type_level") (v "0.0.20") (d (list (d (n "derive_type_level_lib") (r "^0.0.20") (d #t) (k 0)))) (h "1mgssy4h7vgpyq9y6pbga1m4sfqijhjlfg9977rykycynnvzqdbc")))

(define-public crate-derive_type_level-0.0.21 (c (n "derive_type_level") (v "0.0.21") (d (list (d (n "derive_type_level_lib") (r "^0.0.21") (d #t) (k 0)))) (h "0dkv3x2nwygs5qxplpzdlm7ln2z9mphajzvfyil8wqskpax4hsrp")))

(define-public crate-derive_type_level-0.1.0 (c (n "derive_type_level") (v "0.1.0") (d (list (d (n "derive_type_level_lib") (r "^0.1.0") (d #t) (k 0)))) (h "04qwih7nhr2rzxwmvs4rjdh9w03g0kr6vqqqyjp1rrpr5q02pyqb")))

