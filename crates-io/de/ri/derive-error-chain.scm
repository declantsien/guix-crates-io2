(define-module (crates-io de ri derive-error-chain) #:use-module (crates-io))

(define-public crate-derive-error-chain-0.1.0 (c (n "derive-error-chain") (v "0.1.0") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "0.10.*") (d #t) (k 0)))) (h "0nxkzhd58j69fzgs3i8hqyjkchf9n6gi1h63hj549f48qr54yn88")))

(define-public crate-derive-error-chain-0.1.1 (c (n "derive-error-chain") (v "0.1.1") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "0.10.*") (d #t) (k 0)))) (h "1384yk043k1y0lwv8clhb4pz4kw2rlnm3ld8vs9lirsdmvcs7yzl")))

(define-public crate-derive-error-chain-0.1.2 (c (n "derive-error-chain") (v "0.1.2") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "0.10.*") (d #t) (k 0)))) (h "1vf1clfdllflci5wf7bqdli82msmvag0imrg49w2pj32ggaycr2z")))

(define-public crate-derive-error-chain-0.7.0 (c (n "derive-error-chain") (v "0.7.0") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "0.10.*") (d #t) (k 0)))) (h "104c7f5805lblyr100hgwzqqjmlrbd4hwy22f1q8v351qcsmq1l3")))

(define-public crate-derive-error-chain-0.7.1 (c (n "derive-error-chain") (v "0.7.1") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "^0.10.4") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1vfa4y9i4rbzz24cgn7x0gmgi7yzj9w3nk4cps8clxmxnadp6x6h")))

(define-public crate-derive-error-chain-0.7.2 (c (n "derive-error-chain") (v "0.7.2") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "^0.10.4") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0kysw15fksajc4yhjbz3msp80c29ks49y3l9s0r21hzvrylbrli1")))

(define-public crate-derive-error-chain-0.8.0 (c (n "derive-error-chain") (v "0.8.0") (d (list (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "syn") (r "^0.10.4") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1fqgksk7nkahh93vscxch91wa28alvwld89v5akn5f5jac6swkiz")))

(define-public crate-derive-error-chain-0.8.1 (c (n "derive-error-chain") (v "0.8.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "0.11.*") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "08ny1n68fpjw4nkn7v5dhsp0c1s1k6v10s26d3qpsix0np2y03p1")))

(define-public crate-derive-error-chain-0.9.0 (c (n "derive-error-chain") (v "0.9.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "0.11.*") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0kzvdixw613vmg9v4xyp3xfwfd7rir2qjv2la0h63mrmh77zzb83")))

(define-public crate-derive-error-chain-0.10.0 (c (n "derive-error-chain") (v "0.10.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "0.11.*") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0fa00q2lykyzsr4gl466j2byn9vn8k761vcj825mqjiigqy7k82s")))

(define-public crate-derive-error-chain-0.10.1 (c (n "derive-error-chain") (v "0.10.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "0.11.*") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0jnybrpiq5jzx69xq74cwxply36js02z14y9sym8sf2iwsnsk71w")))

(define-public crate-derive-error-chain-0.11.0 (c (n "derive-error-chain") (v "0.11.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "0.11.*") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "160xxj1j9w1493rwjrhgqmm0dw9bf5n54rhf97m3mikjmwa3064j")))

(define-public crate-derive-error-chain-0.11.1 (c (n "derive-error-chain") (v "0.11.1") (d (list (d (n "error-chain") (r "0.11.*") (d #t) (k 2)) (d (n "proc-macro2") (r "0.3.*") (d #t) (k 0)) (d (n "quote") (r "0.5.*") (d #t) (k 0)) (d (n "syn") (r "0.13.*") (f (quote ("derive" "full" "printing"))) (d #t) (k 0)) (d (n "syntex_fmt_macros") (r "0.5.*") (d #t) (k 0)))) (h "0csdwkqp5bc8lps9q15yf4z0xdw5l2193cwrhbkn2118pspm0i6b")))

(define-public crate-derive-error-chain-0.11.2 (c (n "derive-error-chain") (v "0.11.2") (d (list (d (n "error-chain") (r "0.11.*") (d #t) (k 2)) (d (n "proc-macro2") (r "0.4.*") (d #t) (k 0)) (d (n "quote") (r "0.6.*") (d #t) (k 0)) (d (n "syn") (r "0.14.*") (f (quote ("derive" "full" "printing"))) (d #t) (k 0)) (d (n "syntex_fmt_macros") (r "0.5.*") (d #t) (k 0)))) (h "12pjx3gf3s436pzrbrjy5896763fbhl6ighsaza90nscvmmh1y6m")))

