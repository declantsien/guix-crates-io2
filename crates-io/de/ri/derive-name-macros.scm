(define-module (crates-io de ri derive-name-macros) #:use-module (crates-io))

(define-public crate-derive-name-macros-0.1.0 (c (n "derive-name-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "064nhc93izn0i56cxchn0bbz1ql0yskhrjnz5zbk3bcn8k5lyg5f")))

(define-public crate-derive-name-macros-0.1.1 (c (n "derive-name-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1crcpn1xwfsxbzfwf79cag5ad30l7lvywii7g3h6l9vfizpjvzrz")))

(define-public crate-derive-name-macros-1.0.0 (c (n "derive-name-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0x6v3hyhhl5dq27752di4x7235xq2bwkvwh4jp81nlfag6r6sf8l")))

(define-public crate-derive-name-macros-1.0.1 (c (n "derive-name-macros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1dwpdg88nxc5blv67z3ajv0mw090a6ra6c12rddzxkylcby5wrwm")))

(define-public crate-derive-name-macros-1.1.0 (c (n "derive-name-macros") (v "1.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "050xc8g4dv4vsysazha6ch9d0vgfrhavrikwv3yrffw73lznj7jj")))

(define-public crate-derive-name-macros-1.1.1 (c (n "derive-name-macros") (v "1.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zcb9jb4kgqnas9hsav1jirk7fml4bddmigfqzblf8i9p6grjq5a")))

