(define-module (crates-io de ri derive_recursive) #:use-module (crates-io))

(define-public crate-derive_recursive-1.0.0 (c (n "derive_recursive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aw3n8wqm41my03a2km1s3z1b7bm8y6hl3m3jyrp6wfk3amnwfam")))

(define-public crate-derive_recursive-1.0.1 (c (n "derive_recursive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ns4cn0ifgj5mcil8dsi1n6b1w4xgj1hp92bf30vcrpfyjh92qpv")))

(define-public crate-derive_recursive-1.0.2 (c (n "derive_recursive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0njf1g381ds89mylnlwg67vvwkwjzm8a8c8gjlfjpqc2xaqa7gln")))

(define-public crate-derive_recursive-1.0.3 (c (n "derive_recursive") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yxi66vgp1qhdj2g9mm7c3h904az2h2xabnaw2y9mq0v9l7mcnn1")))

