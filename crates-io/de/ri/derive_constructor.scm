(define-module (crates-io de ri derive_constructor) #:use-module (crates-io))

(define-public crate-derive_constructor-0.1.0 (c (n "derive_constructor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "07n4hqj5i6k0k3rdxkbj4ljy9gil9jf2z03cmqnz4by0y93fdajz")))

(define-public crate-derive_constructor-0.1.1 (c (n "derive_constructor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "01ajg17s445y2n7lqf2bb2zh4xnx2gj61jf61bmvzdlk6w3pxyk3")))

