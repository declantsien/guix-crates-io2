(define-module (crates-io de ri derivative) #:use-module (crates-io))

(define-public crate-derivative-0.0.1 (c (n "derivative") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.8") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "0900fk15cflwkzisxwcmsb1f1hjapj7sjgdv0bmzsg96h5fx92wj")))

(define-public crate-derivative-0.0.2 (c (n "derivative") (v "0.0.2") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "1psgajg3rnc0aq7n8s4140bvr3cx88gf1af39wxcmshrcwpalwpi")))

(define-public crate-derivative-0.0.3 (c (n "derivative") (v "0.0.3") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "1kysgqr584yras4nf5dws218sh8zhzxlrkcnb8zf04mpibmx48hd")))

(define-public crate-derivative-0.1.0 (c (n "derivative") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "1r0r2j322c37xy9bbiavqa0pfisbxswb9laxvspwdakx7m3vkkvm")))

(define-public crate-derivative-0.2.0 (c (n "derivative") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "0yj8z6nbbpabfrb9f81d331q2h44aspj833p0zb2fw3hi44lcriy")))

(define-public crate-derivative-0.3.0 (c (n "derivative") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "~0.5") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "1msbfygvfgcqhjkfn2clswv8rrs6rpddvv3vrb768lz8nkydv840")))

(define-public crate-derivative-0.3.1 (c (n "derivative") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "~0.5") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "02zs2i6hqjdcsqp5fyw16rlijvnszyfkdcfwp8irljjxaplnff81") (f (quote (("nightly" "compiletest_rs"))))))

(define-public crate-derivative-1.0.0 (c (n "derivative") (v "1.0.0") (d (list (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "~0.5") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "full" "visit"))) (d #t) (k 0)))) (h "1pmxh6jcdyibfqfijskh86spp1s10ls5kk33qaysalsfx38ddcv7") (f (quote (("nightly" "compiletest_rs"))))))

(define-public crate-derivative-1.0.1 (c (n "derivative") (v "1.0.1") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1rs61d2r3vkvgali2w34wsfz8ya7rlg3vxnfrj1bwy81jjfqfna6") (f (quote (("nightly" "compiletest_rs"))))))

(define-public crate-derivative-1.0.2 (c (n "derivative") (v "1.0.2") (d (list (d (n "compiletest_rs") (r "^0.3.14") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.10") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "164q0yqpgk1p2bp83ls1bipwh8zxrsvy6qzbpbmdvgdydmkyjwv0") (f (quote (("use_core") ("test-nightly" "compiletest_rs"))))))

(define-public crate-derivative-1.0.3 (c (n "derivative") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.10") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fpfcw0if70gnp8hvz6ki2wasldzi31pnwx6jmjq18zpxqqa8b4l") (f (quote (("use_core") ("test-nightly" "trybuild"))))))

(define-public crate-derivative-1.0.4 (c (n "derivative") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.10") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dbiw51q4b33gwhkicai06xym0hb6fkid9xn24h3x2k68qsqhv9w") (f (quote (("use_core") ("test-nightly" "trybuild"))))))

(define-public crate-derivative-2.0.0 (c (n "derivative") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "0arvzg7ps6sy5ljlj7b3rarjldvcjq2isrdyja1dgb35qqdbsgn7") (f (quote (("use_core"))))))

(define-public crate-derivative-2.0.1 (c (n "derivative") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "0jc6b40mqv81p49y3lkqdvj75ywzab7lv82j9sy7xmsp4rrz5ha0") (f (quote (("use_core"))))))

(define-public crate-derivative-2.0.2 (c (n "derivative") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "0lc17x3q4lnbha61c7wj1sw1gqwhp1yz7si59r5whbkkjzmx550v") (f (quote (("use_core"))))))

(define-public crate-derivative-2.1.0 (c (n "derivative") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "0mdk3kl3nb8rwblfpfz3cqdsrlgvn9w47hlc9wdxvznfnxv4vbhy") (f (quote (("use_core"))))))

(define-public crate-derivative-2.1.1 (c (n "derivative") (v "2.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "03rqx8j9q5nlrpr7w8cwwrvw916pr0ahzs3y8yln18cx6mh2nn6b") (f (quote (("use_core"))))))

(define-public crate-derivative-2.1.2 (c (n "derivative") (v "2.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18, <1.0.23") (d #t) (k 2)))) (h "1yyggzz7pm5rcalr13jhn1pmnlkv83zx1mwsy9y6fm3gb1hg0w83") (f (quote (("use_core"))))))

(define-public crate-derivative-2.1.3 (c (n "derivative") (v "2.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18, <1.0.23") (d #t) (k 2)))) (h "10x1wk3h5kawgbflzgn5j49m3lgznkfdqkb4ih4dwv7sxxs5ivga") (f (quote (("use_core"))))))

(define-public crate-derivative-2.2.0 (c (n "derivative") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.18, < 1.0.23") (d #t) (k 2)))) (h "02vpb81wisk2zh1d5f44szzxamzinqgq2k8ydrfjj2wwkrgdvhzw") (f (quote (("use_core"))))))

