(define-module (crates-io de ri derive-knet) #:use-module (crates-io))

(define-public crate-derive-knet-0.1.0 (c (n "derive-knet") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ipw33gajxgmgnck7q6rfd2kiv8aarsd1x4xjvl5kxlczyjhsx4x")))

(define-public crate-derive-knet-0.2.0 (c (n "derive-knet") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dlchdn6289gl71dazs8lg6kis1gn3sxd7mvww3m4nj04wswg6j2")))

