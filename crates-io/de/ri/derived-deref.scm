(define-module (crates-io de ri derived-deref) #:use-module (crates-io))

(define-public crate-derived-deref-0.1.0 (c (n "derived-deref") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yw5w0k6m2y39db4gj00b3ajf42x4yc8gms3zb8igc2pmwacf917")))

(define-public crate-derived-deref-1.0.0 (c (n "derived-deref") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bqf4hfbz268gxf3jp651b7vy7yaxsxws35fidpgcnnph80vhnvh")))

(define-public crate-derived-deref-2.0.0 (c (n "derived-deref") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nb79cb42j6qpjnzdkkv8jb6k8djas4i64dpm7saqvkwblm2j82a")))

(define-public crate-derived-deref-2.0.1 (c (n "derived-deref") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikg8fw0ziax3qcnzdksdg9gl7akn512bhg41d2ymrzfg5j57s3q")))

(define-public crate-derived-deref-2.1.0 (c (n "derived-deref") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gmllwc6sq7v0hqxn88rn2h7j282zh8ws7m98dbl4rfd7h1g4pl0")))

