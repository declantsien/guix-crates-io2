(define-module (crates-io de ri derive_arbitrary) #:use-module (crates-io))

(define-public crate-derive_arbitrary-0.2.0 (c (n "derive_arbitrary") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "11gl54rcfkx0wp38jn1b3l6hjnch0xwxqx2j9ij0gk0ba14vwngf")))

(define-public crate-derive_arbitrary-0.3.0 (c (n "derive_arbitrary") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "0vg3ks40x3bjnvm6q1lrbimxl53bl4038spxmw0g00ibrx9nnj4s")))

(define-public crate-derive_arbitrary-0.3.1 (c (n "derive_arbitrary") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (d #t) (k 0)))) (h "1mman4d3s353jwlp8xypjnqyx34l9ph7ja5fi5sc0sjjipwsjydr")))

(define-public crate-derive_arbitrary-0.3.3 (c (n "derive_arbitrary") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "15dd6bkdv7zjx0ziwxmjddwknk57pmzcqml9qnmpr92l89ag1cns")))

(define-public crate-derive_arbitrary-0.4.0 (c (n "derive_arbitrary") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1n0qizqg5ahh6f9xk6m6zzkix7382byzp3zcb32bs03d3fkxdvfa")))

(define-public crate-derive_arbitrary-0.4.2 (c (n "derive_arbitrary") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10cq2lahm5mawnb443p5ck6jvlk6m6x5n3jdq0li8w17wszd6dcp")))

(define-public crate-derive_arbitrary-0.4.3 (c (n "derive_arbitrary") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pvhh32kpi5pjmynrg0xhy8l967mrchmw7jg6splmrgqz4mv4pv6")))

(define-public crate-derive_arbitrary-0.4.4 (c (n "derive_arbitrary") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15klaf0szz91in8awsc3q9azbnqbqgf45wlmzg3ajy8wpn77bvjw")))

(define-public crate-derive_arbitrary-0.4.5 (c (n "derive_arbitrary") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vyjacw4drkiln1h4j5z7rsmjqq2xihsfg528k6pvkp7sf2k3d02")))

(define-public crate-derive_arbitrary-0.4.6 (c (n "derive_arbitrary") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c67sr6j3sfrm3ff1xy7k3gkv4rq37dzppkl41qbidkn2b4cdxyh")))

(define-public crate-derive_arbitrary-0.4.7 (c (n "derive_arbitrary") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rp0z4k0j5ip0bx6dssg97l4q6bakhf6lm5h1lpr3p3kwjsi585i")))

(define-public crate-derive_arbitrary-1.0.0-rc1 (c (n "derive_arbitrary") (v "1.0.0-rc1") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15p7d54h5bkw0yv1s1zpnb5dz2nfqgw58qnk6smszybbqic44p23")))

(define-public crate-derive_arbitrary-1.0.0-rc2 (c (n "derive_arbitrary") (v "1.0.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w6l806vln2c89dlhlrcf6m6xmy0vzq4b1pk1r6df5z3aix7ajbk")))

(define-public crate-derive_arbitrary-1.0.0 (c (n "derive_arbitrary") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06icz1w8ppdrppbc3yiy1dqjl5v1i9mxzmfnzp2mrsjx0w6xv2fz")))

(define-public crate-derive_arbitrary-1.0.1 (c (n "derive_arbitrary") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nig0iydva7a9h9i9qyi6an9w5qjafmn3qlzvdqqiw0x2kp824jz")))

(define-public crate-derive_arbitrary-1.0.2 (c (n "derive_arbitrary") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wzd18pw1dwjkgnskfpvlg76j2pv99fb2hzzjsrxi8l7iqh2jimj")))

(define-public crate-derive_arbitrary-1.1.0 (c (n "derive_arbitrary") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yaamhgq4zip3yyzyrjsn0n7yfppilv8zn82v1xwinimq033rqlq")))

(define-public crate-derive_arbitrary-1.1.1 (c (n "derive_arbitrary") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p6lhjzzsd3838cyz9flfjggx0plv53ckgg95vmjdh9wdh203l4i")))

(define-public crate-derive_arbitrary-1.1.2 (c (n "derive_arbitrary") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m5ll4wfywcil4ccpd1hy782ai77zlsypajmcr2sfcwhvlkxna47")))

(define-public crate-derive_arbitrary-1.1.3 (c (n "derive_arbitrary") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gazljxka3zgfn9l65ia5jy970j6plwpslbd8s0vdbbkc58pg9f9")))

(define-public crate-derive_arbitrary-1.1.4 (c (n "derive_arbitrary") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01sfdd9ajpgyqjynck8xz4rdrwjrf0c5519vlg5r4zlbhc8vcjfp")))

(define-public crate-derive_arbitrary-1.1.5 (c (n "derive_arbitrary") (v "1.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z268jpxgxl1cax4x9f80sc93p2y99ny6kbmsx4bv7c4sy1pjbyi")))

(define-public crate-derive_arbitrary-1.1.6 (c (n "derive_arbitrary") (v "1.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cgrl6a54gjbffyg88y71a8xngy3j7kakmis2ykxfrfq85jxcsi2")))

(define-public crate-derive_arbitrary-1.2.0 (c (n "derive_arbitrary") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zw12jc6k6aixqs6m2rsj56grhx2xjw2l8rhr8rj1wj897qdy0s9") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.2.1 (c (n "derive_arbitrary") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "023jjbx4yr84hmzyvbnk3vbyfmgpp8vsiz4526xlg05jmsan98gq") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.2.2 (c (n "derive_arbitrary") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2k03sw9ia4vvx22ymignz3iwsrkxr04lfscb3vywgmynzhning") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.2.3 (c (n "derive_arbitrary") (v "1.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "005x40g2k89g31lx4dzi4biw76s42b5cvpmvk209w8if3rqf9vlb") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.3.0 (c (n "derive_arbitrary") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1dy8pmv7d8diqhbh1b88v8q5g0inwkgb465877jqimbjqjgfpkgk") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.3.1 (c (n "derive_arbitrary") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1f7ykmr8jb8gzmfvar4cp4a6l91ahi5rq581q8nwbgq38jnyzq2k") (r "1.63.0")))

(define-public crate-derive_arbitrary-1.3.2 (c (n "derive_arbitrary") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "04bnd985frl81r5sgixgpvncnnj1bfpfnd7qvdx1aahnqi9pbrv7") (r "1.63.0")))

