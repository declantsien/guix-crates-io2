(define-module (crates-io de ri derive-sql-mysql) #:use-module (crates-io))

(define-public crate-derive-sql-mysql-0.7.0 (c (n "derive-sql-mysql") (v "0.7.0") (d (list (d (n "attribute-derive") (r "^0") (d #t) (k 0)) (d (n "derive-sql") (r "^0") (d #t) (k 2)) (d (n "mysql") (r "^24") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jib7y6j6g7k6hgf9ssclsq4kinnwl69jx47xb4cch8n9ws2z05m")))

(define-public crate-derive-sql-mysql-0.8.0 (c (n "derive-sql-mysql") (v "0.8.0") (d (list (d (n "attribute-derive") (r "^0") (d #t) (k 0)) (d (n "derive-sql") (r "^0") (d #t) (k 2)) (d (n "derive-sql-common") (r "^0") (d #t) (k 0)) (d (n "mysql") (r "^24") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "01z6b0p9dlqngp1wy08z87bwcx6dljnqzfv6dw9iwhhby5724frx")))

(define-public crate-derive-sql-mysql-0.8.1 (c (n "derive-sql-mysql") (v "0.8.1") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "derive-sql") (r "^0") (d #t) (k 2)) (d (n "derive-sql-common") (r "^0") (d #t) (k 0)) (d (n "mysql") (r "^24") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jf63cqj4x6sxq03xhsd8p48hsxgcs0d7spqr52y6nq0ys19im2r")))

(define-public crate-derive-sql-mysql-0.9.0 (c (n "derive-sql-mysql") (v "0.9.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "derive-sql") (r "^0") (f (quote ("mysql"))) (d #t) (k 2)) (d (n "derive-sql-common") (r "^0") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 2)) (d (n "mysql") (r "^24") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "01f3pyxxbi3chf0p0n0rp19bsaamvg9h6g688c2x6i04jiydq6sa")))

