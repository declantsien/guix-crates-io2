(define-module (crates-io de ri derive_hub) #:use-module (crates-io))

(define-public crate-derive_hub-0.1.0 (c (n "derive_hub") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1ydj3kplw5iv8yvmm0bl3rgyfrkpw55d25457jj01k4j96j0w17n")))

