(define-module (crates-io de ri derive_dumb) #:use-module (crates-io))

(define-public crate-derive_dumb-0.1.0 (c (n "derive_dumb") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xfsdpk7p21j8xy1akmcgdnika0wzm586m40kmr3dz6cv345amc5")))

(define-public crate-derive_dumb-0.1.1 (c (n "derive_dumb") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vf51fsmwwc9z5shmx4a21j875g66yrilb4jhqh3qkl9g0mn7kby")))

(define-public crate-derive_dumb-0.1.2 (c (n "derive_dumb") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1v1xzfhb6lis8fchh9cmmkl4adb9nfw099s8pycmx715arnyvh73")))

(define-public crate-derive_dumb-0.1.3 (c (n "derive_dumb") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vl0vgkd44ii6nqziassipg38hpsm0ybfgnzgp1cchni85s7pxmv")))

(define-public crate-derive_dumb-0.1.4 (c (n "derive_dumb") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w921990fhwam1abp0j9m1bn85x87mmrz0q6ycx542fsbxs54hwv")))

(define-public crate-derive_dumb-0.2.4 (c (n "derive_dumb") (v "0.2.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1y76ar1l4rwlgx2ivm0f5b4jk5qawggdrrv4ryp885cpv5fw8wyj")))

