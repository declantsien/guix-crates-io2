(define-module (crates-io de ri derive_environment) #:use-module (crates-io))

(define-public crate-derive_environment-0.1.0 (c (n "derive_environment") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0bxwz9gl3x6wsxygr3h78pmz3qwi9w9b0vzld7wfzpwwy4azraqm")))

(define-public crate-derive_environment-1.0.0 (c (n "derive_environment") (v "1.0.0") (d (list (d (n "derive_environment_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0pyr1myf2fiqml1dlj1gddc0miqv9439c0fswfi6m0rc0vi4wd56")))

(define-public crate-derive_environment-1.1.0 (c (n "derive_environment") (v "1.1.0") (d (list (d (n "derive_environment_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ya5ik5sx6abfivz4s4lwdcvvj8al4m5i95z6rm7j057wpzfw7az")))

