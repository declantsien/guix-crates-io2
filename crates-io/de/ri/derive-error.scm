(define-module (crates-io de ri derive-error) #:use-module (crates-io))

(define-public crate-derive-error-0.0.0 (c (n "derive-error") (v "0.0.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1qy1fxln7l1f0a55snx1y1mgjilp53hllp319h8m4k5p95qxcdlc")))

(define-public crate-derive-error-0.0.1 (c (n "derive-error") (v "0.0.1") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1823n4mr388w4jcr56ywiv78ydll0kf204rza6mj08i278iaribx")))

(define-public crate-derive-error-0.0.2 (c (n "derive-error") (v "0.0.2") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "15bb9pmvrhjzjqwlzkj1dd9rhijw9ghypk891my4mlpjmrqhv27r")))

(define-public crate-derive-error-0.0.3 (c (n "derive-error") (v "0.0.3") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1lvnx2bgp45wv9m908mivvvn8i7mzxv4d8l5r891jyffmfrip7v2")))

(define-public crate-derive-error-0.0.4 (c (n "derive-error") (v "0.0.4") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.4") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1j624ma4jw911yg3qqlvfybgk7614k2blhg6wgnb38wyn90882gc")))

(define-public crate-derive-error-0.0.5 (c (n "derive-error") (v "0.0.5") (d (list (d (n "case") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "01h8w3jy7qsm2wx7rky76y2b4922wzfrkmfhqr8xnhf1zgm3hqpb")))

