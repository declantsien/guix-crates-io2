(define-module (crates-io de ri derive_rich) #:use-module (crates-io))

(define-public crate-derive_rich-0.1.0 (c (n "derive_rich") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ks9ka6n2b6malsd8f0snqgvxlcwklxbh3jnnwc4lvkmc4vg3k2d")))

(define-public crate-derive_rich-0.2.0 (c (n "derive_rich") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dqhmav9d7gbz7hcwr4a999xs017hin9vyrziw7xzxw1pfr7zaab")))

(define-public crate-derive_rich-0.2.1 (c (n "derive_rich") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "181vw0ax5q7my86b71lv2al2amrysivm0h4pwa6hqdn8mg29l80s")))

(define-public crate-derive_rich-0.2.2 (c (n "derive_rich") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "163xkgkymln10ik4lryqfm0gby0w32zbz1hidv1cy9b4bdmvj342")))

(define-public crate-derive_rich-0.3.0 (c (n "derive_rich") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m08nw48hw1hxlf3mbd95axqjls5g48hwi18h0zqfyaspinh5nxh")))

(define-public crate-derive_rich-0.4.0 (c (n "derive_rich") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "176m9pcm1dggqhsyi8mvy0wkwkjyanny2ad40zsd9a543r8hd0mj")))

(define-public crate-derive_rich-0.4.1 (c (n "derive_rich") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07y6qhwghak0ygxc8j1ibgd1i8qvvccgpfxcxphaiarsj1pkqg0i")))

(define-public crate-derive_rich-0.4.3 (c (n "derive_rich") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q92pjnkp4xdc8fgvv6a6lb0s1l4amyp5ypspjw1x46l3yaay6sc")))

(define-public crate-derive_rich-0.4.4 (c (n "derive_rich") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0340afpklmd6dbg2b1sh03k7gnswbyv7h1yaj9pmad69bl7zxcky")))

