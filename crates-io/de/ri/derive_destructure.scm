(define-module (crates-io de ri derive_destructure) #:use-module (crates-io))

(define-public crate-derive_destructure-1.0.0 (c (n "derive_destructure") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)))) (h "1kdw3jd19ap0y614p2h37xns9q5ji9jaqv040ic7p4pxxvif4f5w")))

