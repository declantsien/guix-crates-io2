(define-module (crates-io de ri derive_typst_intoval) #:use-module (crates-io))

(define-public crate-derive_typst_intoval-0.1.0 (c (n "derive_typst_intoval") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)) (d (n "typst") (r "^0.11") (d #t) (k 0)) (d (n "typst-macros") (r "^0.11") (d #t) (k 0)))) (h "1mv531701zwpz1ry4krblvc0yw24c4zmgpr5xkjp8xfch1x3ibwl")))

(define-public crate-derive_typst_intoval-0.2.0 (c (n "derive_typst_intoval") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)) (d (n "typst") (r "^0.11") (d #t) (k 0)) (d (n "typst-macros") (r "^0.11") (d #t) (k 0)))) (h "0rkyk6dwb1fydqmy1l2wqqsjjmmnghk9f9xv1nfpjwjimzfbg880") (y #t)))

(define-public crate-derive_typst_intoval-0.2.1 (c (n "derive_typst_intoval") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)) (d (n "typst") (r "^0.11") (d #t) (k 0)) (d (n "typst-macros") (r "^0.11") (d #t) (k 0)))) (h "1snk4b14d94fmd9pjy7mq3g4w9jl2yy7gcmfx37ilbdk5jg8xkss")))

