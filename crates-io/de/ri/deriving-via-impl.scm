(define-module (crates-io de ri deriving-via-impl) #:use-module (crates-io))

(define-public crate-deriving-via-impl-0.1.0 (c (n "deriving-via-impl") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6wz9dcc7r1k7zdwy9bk60gc8qv1zk64yl52crm6gb68wqvzn0f")))

