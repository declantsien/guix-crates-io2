(define-module (crates-io de ri derive_deref) #:use-module (crates-io))

(define-public crate-derive_deref-1.0.0 (c (n "derive_deref") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("aster"))) (d #t) (k 0)))) (h "1f6dxk8i9cp8clqa7zhp74iwyqqyix715jsqvfqa5n717p8pz5vm")))

(define-public crate-derive_deref-1.0.1 (c (n "derive_deref") (v "1.0.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0rzv4d7a9dk5b75zh8gdiqyrik1rj63ja87fpikl1m3gq9bh2b13")))

(define-public crate-derive_deref-1.0.2 (c (n "derive_deref") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1k57b4yqf0gkd0z295jn4r5mdqn4xa7arkq19jrcivc9jf5hymry")))

(define-public crate-derive_deref-1.1.0 (c (n "derive_deref") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "13wb7dpn8q4ysx8sqfyfq4h1wqn9a67jgi708aj668x41bdlym8i")))

(define-public crate-derive_deref-1.1.1 (c (n "derive_deref") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0arv2qqff7c7jm97xldq5kj39xglcnjqf9bppbx6j4wlv7icxnyw")))

