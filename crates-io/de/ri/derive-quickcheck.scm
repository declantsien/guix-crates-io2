(define-module (crates-io de ri derive-quickcheck) #:use-module (crates-io))

(define-public crate-derive-quickcheck-0.1.0 (c (n "derive-quickcheck") (v "0.1.0") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xr2z32gs3s10aval8qflhlzs81mnfxwplcfpz92v3pdvy9j44va")))

(define-public crate-derive-quickcheck-0.1.1 (c (n "derive-quickcheck") (v "0.1.1") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5yw1hxvy1pg5y3knllcfw6i2p4fkifjka0hglad5bb03l21rmx")))

(define-public crate-derive-quickcheck-0.1.2 (c (n "derive-quickcheck") (v "0.1.2") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "0r7wvm27v4vcmaj8jnqcbbzdm5bpld8fs38nmwdc0kpqsp8jq2z8")))

(define-public crate-derive-quickcheck-0.1.3 (c (n "derive-quickcheck") (v "0.1.3") (d (list (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xc0a912yin2hcgvfq8hap5iyd7v97h0ll2kxpfcn3xgdqcr8g2f")))

(define-public crate-derive-quickcheck-0.2.0 (c (n "derive-quickcheck") (v "0.2.0") (d (list (d (n "breadth-first-zip") (r ">=0.3.0") (d #t) (k 0)) (d (n "heck") (r ">=0.4") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1") (d #t) (k 0)) (d (n "quote") (r ">=1") (d #t) (k 0)) (d (n "syn") (r ">=2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xi14s9ck5a73fyxamz340brx6bap3lwyhwjvq44qnx6prrs28xw")))

