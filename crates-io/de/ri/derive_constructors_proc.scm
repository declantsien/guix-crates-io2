(define-module (crates-io de ri derive_constructors_proc) #:use-module (crates-io))

(define-public crate-derive_constructors_proc-1.0.0 (c (n "derive_constructors_proc") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xpb9bmlfby8wimrc6dgkg6ypqrxya266i39z6xzp21cwpsvy48y")))

