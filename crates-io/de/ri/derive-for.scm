(define-module (crates-io de ri derive-for) #:use-module (crates-io))

(define-public crate-derive-for-0.1.0 (c (n "derive-for") (v "0.1.0") (h "18zyavvz0mczkqs53yjfyf912qbg8nd0pwgfxn6nl0sdypgs6mf3")))

(define-public crate-derive-for-0.1.1 (c (n "derive-for") (v "0.1.1") (h "0bxnggkn09mwiybqf3s5ffns6g0zhwx63fldcanj3haan7hb7imv")))

(define-public crate-derive-for-0.2.1 (c (n "derive-for") (v "0.2.1") (h "1lxa5vcj19fmj542zr0shqnzg7xfd3xw078a21fgm9g6msvp08hh")))

(define-public crate-derive-for-1.2.1 (c (n "derive-for") (v "1.2.1") (h "0ql01ijgvrg9y81pyjayn7ns5w5xza1jgfdspv5kncw446799lqq")))

(define-public crate-derive-for-1.2.2 (c (n "derive-for") (v "1.2.2") (h "1rkilfwrq7xcy1zdghilk1gz08n861n8pmk6b2b9fdv3liy1z2d9")))

