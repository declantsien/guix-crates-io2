(define-module (crates-io de ri derive_codec_sv2) #:use-module (crates-io))

(define-public crate-derive_codec_sv2-0.1.0 (c (n "derive_codec_sv2") (v "0.1.0") (d (list (d (n "binary_codec_sv2") (r "^0.1.0") (d #t) (k 0)))) (h "0ryy70c4d68dnr7j83lfj7a2973v16a7w9w3cnpnkfaqn7m5hlbx")))

(define-public crate-derive_codec_sv2-0.1.1 (c (n "derive_codec_sv2") (v "0.1.1") (d (list (d (n "binary_codec_sv2") (r "^0.1.1") (d #t) (k 0)))) (h "1026iix3nsyqag4q6wva9l9nfxdq4c161ds1hapnfsngz458b0ac")))

(define-public crate-derive_codec_sv2-0.1.2 (c (n "derive_codec_sv2") (v "0.1.2") (d (list (d (n "binary_codec_sv2") (r "0.1.*") (d #t) (k 0)))) (h "0sb9zqa7l4mmkkslpygmir8cisjkm6mf09rr53vsq33ykj3009a5")))

(define-public crate-derive_codec_sv2-0.1.3 (c (n "derive_codec_sv2") (v "0.1.3") (d (list (d (n "binary_codec_sv2") (r "^0.1.3") (d #t) (k 0)))) (h "0wkznhsq803ghfpci0ib3mhxhnbrd0hna4vfmz8m4zr7lw875xxy")))

(define-public crate-derive_codec_sv2-0.1.5 (c (n "derive_codec_sv2") (v "0.1.5") (d (list (d (n "binary_codec_sv2") (r "^0.1.3") (d #t) (k 0)))) (h "0mpkvw33zx5pgffd942dyr2b808ym2d3f5b3bas3fgvqxmfd7ila")))

(define-public crate-derive_codec_sv2-1.0.0 (c (n "derive_codec_sv2") (v "1.0.0") (d (list (d (n "binary_codec_sv2") (r "^1.0.0") (d #t) (k 0)))) (h "00rfgcr16q9vimcx975vfaciviaijv5abzl729pf3mqfyassplv4")))

