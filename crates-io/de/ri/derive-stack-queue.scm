(define-module (crates-io de ri derive-stack-queue) #:use-module (crates-io))

(define-public crate-derive-stack-queue-0.0.1-beta.5 (c (n "derive-stack-queue") (v "0.0.1-beta.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rp9yr7002c0bfifvgb1cn7hsx9lxx5rkjafcljjmh3wmn2qvppf") (y #t)))

(define-public crate-derive-stack-queue-0.0.1-beta.6 (c (n "derive-stack-queue") (v "0.0.1-beta.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m2dx374hljhzypd3gzbd00kdz4x1yjcfjvis6wj04wsliglw661") (y #t)))

(define-public crate-derive-stack-queue-0.0.1-beta.7 (c (n "derive-stack-queue") (v "0.0.1-beta.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ipd7p2x106c7vsnpj7c5bbfiyirqa7kn0asvpikm8qy4y5k1ym0") (y #t)))

(define-public crate-derive-stack-queue-0.0.1-beta.8 (c (n "derive-stack-queue") (v "0.0.1-beta.8") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jdv57y44y9dsrp1ls57h1gaqwv26dccbfvnk4yf5dbgg00rzv7v") (y #t)))

(define-public crate-derive-stack-queue-0.0.1-beta.9 (c (n "derive-stack-queue") (v "0.0.1-beta.9") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "086dsjvv8mdpvclkjmpcas5dv7a6i5jr2nhc5yb25351fvm32rcf")))

(define-public crate-derive-stack-queue-0.0.1-beta.10 (c (n "derive-stack-queue") (v "0.0.1-beta.10") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14whb2xqvr7az7d3089r6zrzdg17fd3blzmdi282jml73mh5xacx")))

(define-public crate-derive-stack-queue-0.0.1-beta.11 (c (n "derive-stack-queue") (v "0.0.1-beta.11") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qnl7qqr7wgzvdv3495mks6axbxdg2zk8r9achyvcp1fwrn0cf6k") (y #t)))

(define-public crate-derive-stack-queue-0.0.1-beta.12 (c (n "derive-stack-queue") (v "0.0.1-beta.12") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16c7w54vh4sj9bn41m002hm00g11l4yck52233dqwlcn63r7hcxv")))

(define-public crate-derive-stack-queue-0.0.1-beta.13 (c (n "derive-stack-queue") (v "0.0.1-beta.13") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zl5l6fs31sfvh0ga2qrj25l6s4w472x4570rv35cph4jk0pxbvz")))

(define-public crate-derive-stack-queue-0.0.1-beta.14 (c (n "derive-stack-queue") (v "0.0.1-beta.14") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1a0jripy3ipiybjdvfkpik7l758laj30dgv618n6in21bprpgjir")))

(define-public crate-derive-stack-queue-0.0.1-beta.15 (c (n "derive-stack-queue") (v "0.0.1-beta.15") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ibfjj8pk806af2f8ssz5h5br225n75iinjghvavd7r92qfj9i7j")))

(define-public crate-derive-stack-queue-0.0.1-beta.16 (c (n "derive-stack-queue") (v "0.0.1-beta.16") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14nc493zgiyvca498isxw35bmxj86f60x8nn6d8arfyy61g77csy")))

(define-public crate-derive-stack-queue-0.0.1-beta.17 (c (n "derive-stack-queue") (v "0.0.1-beta.17") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lqcacs9i3d3j6n5b5dmia8rhswz0j6kqvxrd0axnlm0abhycnbv") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-derive-stack-queue-0.1.0 (c (n "derive-stack-queue") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mwycll2i8c0v3ah4gvipcczm1ira74pdfcbaifc216ccmp04np3") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-derive-stack-queue-0.2.0 (c (n "derive-stack-queue") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1h4wq0di6bpn3hc4c8q7lql5jymcv4pgcsj7dzan4i3hl5np1x9f") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-derive-stack-queue-0.3.0 (c (n "derive-stack-queue") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "12wlfjpx8nvqj7gak2nxipgjzcrzf6094286lk601xqd9m59ydf8")))

(define-public crate-derive-stack-queue-0.3.1 (c (n "derive-stack-queue") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wf75qsmrkl25mbdwaznv0v112js7d3jz4hh6vbysn93r5i9i5v3")))

(define-public crate-derive-stack-queue-0.3.2 (c (n "derive-stack-queue") (v "0.3.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0cg7q8q4dp3sgppgxrxcx61nwrarggd0z0djslf0pbxjww03fhdl")))

(define-public crate-derive-stack-queue-0.3.3 (c (n "derive-stack-queue") (v "0.3.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00r0s0xvlx9swi0bdrgkgs0h4mg82hqrmhgmj5574ngpf02f57fk")))

(define-public crate-derive-stack-queue-0.4.0 (c (n "derive-stack-queue") (v "0.4.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fq4p89567450vh3zz6irfrv481q2w5y4rwxg744lsvrc5wp367w")))

(define-public crate-derive-stack-queue-0.5.0 (c (n "derive-stack-queue") (v "0.5.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dka43571x5662q70kbkdaizv8f9m6cpl6sv39ijnkdniqi22a64")))

(define-public crate-derive-stack-queue-0.6.0 (c (n "derive-stack-queue") (v "0.6.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d86bz06nl51anpj1py6lfqn17n8wgkp04asmn26pwcmdx8c1ba7")))

(define-public crate-derive-stack-queue-0.7.0 (c (n "derive-stack-queue") (v "0.7.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ng47rgsnpkgx0d8swcimrin0y7hpac2wjah6rarg5c3p77gz0jr")))

(define-public crate-derive-stack-queue-0.8.0 (c (n "derive-stack-queue") (v "0.8.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s5gf154r9nhbvkxrhcgpw3mndlds4izfzsnbg7xxp2jhxw8lylz")))

(define-public crate-derive-stack-queue-0.9.0 (c (n "derive-stack-queue") (v "0.9.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pdvfinzhkzgnhgg662jzn4zipcha6pz6krv0zjy9vm52v0lfnwx")))

(define-public crate-derive-stack-queue-0.10.0 (c (n "derive-stack-queue") (v "0.10.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18lmjr26a2w62rrxbgp3hbns5fci8r3p9ln5kxlawwfi0m6jj5fh")))

(define-public crate-derive-stack-queue-0.11.0 (c (n "derive-stack-queue") (v "0.11.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1j7b0a3manxn88ir487w8wfmq9822hf1y91lgqjawhp2yx5n21y8")))

(define-public crate-derive-stack-queue-0.12.0 (c (n "derive-stack-queue") (v "0.12.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bvq1nsl4zwq3xgf1mh3a5yi7gwjkrcg1y3r7rz7h82b1wzpjzgf")))

(define-public crate-derive-stack-queue-0.13.0 (c (n "derive-stack-queue") (v "0.13.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ngi5aaxdsbczhj0lnph1ynglb9x86b6h5mj9501k4l92671d34v")))

(define-public crate-derive-stack-queue-0.14.0 (c (n "derive-stack-queue") (v "0.14.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1af1spynbinxg5b97v368gkkrh8a4hzmybrmh4jf38c48lr0mway")))

