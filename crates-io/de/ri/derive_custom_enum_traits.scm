(define-module (crates-io de ri derive_custom_enum_traits) #:use-module (crates-io))

(define-public crate-derive_custom_enum_traits-0.1.0 (c (n "derive_custom_enum_traits") (v "0.1.0") (d (list (d (n "custom_enum_traits") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0afdpd47bml3gh0k7s5bfilfcn0g337v6n4522c3vy0wi10rx6w2")))

