(define-module (crates-io de ri derive-attribute-macros) #:use-module (crates-io))

(define-public crate-derive-attribute-macros-0.1.0 (c (n "derive-attribute-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "derive-attribute-utils") (r "^0.1.0") (f (quote ("syn_2"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn_v2") (r "^2") (f (quote ("full"))) (d #t) (k 0) (p "syn")))) (h "1vpk8ix0fhgv532x394dqc1xpvlvg38mbdzcqrh35ngg9s0g4dw4") (f (quote (("syn_2") ("syn_1"))))))

(define-public crate-derive-attribute-macros-0.1.1 (c (n "derive-attribute-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "derive-attribute-utils") (r "^0.1.0") (f (quote ("syn_2"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn_v2") (r "^2") (f (quote ("full"))) (d #t) (k 0) (p "syn")))) (h "1fmmzznvi91s3dazrja24a8i2syasz2dd76fa001anb6qyajivyv") (f (quote (("syn_2") ("syn_1"))))))

