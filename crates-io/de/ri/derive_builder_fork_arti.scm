(define-module (crates-io de ri derive_builder_fork_arti) #:use-module (crates-io))

(define-public crate-derive_builder_fork_arti-0.11.2 (c (n "derive_builder_fork_arti") (v "0.11.2") (d (list (d (n "derive_builder_macro") (r "=0.11.2") (d #t) (k 0) (p "derive_builder_macro_fork_arti")) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.38") (d #t) (k 2)))) (h "0a6jwynyal1jqqg4mv3m4gzdmn1b3k8sd9d9j360wkazb56y5sn3") (f (quote (("std") ("default" "std") ("clippy" "derive_builder_macro/clippy"))))))

