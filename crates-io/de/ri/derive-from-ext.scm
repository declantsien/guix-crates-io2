(define-module (crates-io de ri derive-from-ext) #:use-module (crates-io))

(define-public crate-derive-from-ext-0.1.0 (c (n "derive-from-ext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-unnamed-struct") (r "^0.1") (d #t) (k 0)))) (h "136ps032mdq7dl9qcqnijbf5vxgn9vxz4wfm2n1g7xzsq63lmikk")))

(define-public crate-derive-from-ext-0.2.0 (c (n "derive-from-ext") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-unnamed-struct") (r "^0.1") (d #t) (k 0)))) (h "0izm6ph14lxzi6c1a1dgfq21i42lhh357s489w9mrwqgb7k5zl0c")))

