(define-module (crates-io de ri derive_everything) #:use-module (crates-io))

(define-public crate-derive_everything-0.1.0 (c (n "derive_everything") (v "0.1.0") (d (list (d (n "impls") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yskv6mmrgfffm1mz6h5rqzj6rckls0sd50hx7smv6wrgh058ys5") (y #t)))

(define-public crate-derive_everything-0.1.1 (c (n "derive_everything") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i589326i2kdxvgv0w7pqy6fpc1xjjd6fj5nnlxr3j7x6ayqcpfa")))

