(define-module (crates-io de ri derive_environment_macros) #:use-module (crates-io))

(define-public crate-derive_environment_macros-1.0.0 (c (n "derive_environment_macros") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "13s2idiwgfs8m2br68mha8mnwq2k5434kib3bslsx6l55fgnfpkb")))

