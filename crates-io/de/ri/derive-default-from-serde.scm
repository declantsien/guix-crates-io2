(define-module (crates-io de ri derive-default-from-serde) #:use-module (crates-io))

(define-public crate-derive-default-from-serde-0.1.0 (c (n "derive-default-from-serde") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1kr9rwg3x492r7c1f71zh1d3sl7c52aq6c23m4jy9xxzyhr5p8na")))

(define-public crate-derive-default-from-serde-0.1.1 (c (n "derive-default-from-serde") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0hn3jw32m22xhrd3m31rrr9x86afpxc8ybids03miadamsbxpas1")))

(define-public crate-derive-default-from-serde-0.1.2 (c (n "derive-default-from-serde") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1lnc79i5wg4rkvwjqyps9hv50mrb22kfsx25xlb52saa6nc5ckrq")))

