(define-module (crates-io de ri derive-partial-eq-extras) #:use-module (crates-io))

(define-public crate-derive-partial-eq-extras-0.1.0 (c (n "derive-partial-eq-extras") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.2.0") (d #t) (k 0)))) (h "11r6xv4sy14p55bss8qkm6sajfcg2mcvwswas9dngnflfhza98zc")))

(define-public crate-derive-partial-eq-extras-0.1.1 (c (n "derive-partial-eq-extras") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.0") (d #t) (k 0)))) (h "1g8c9fskzn1awnv0wjrha4sazbvpfl7fpw8p61p6rbpqrfylx8kh")))

(define-public crate-derive-partial-eq-extras-0.1.2 (c (n "derive-partial-eq-extras") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-helpers") (r "^0.4.1") (d #t) (k 0)))) (h "06aaznsiml570fj79ys2a0dxaha4p2za9hglrysmdypii4mlyjcq")))

(define-public crate-derive-partial-eq-extras-0.2.0 (c (n "derive-partial-eq-extras") (v "0.2.0") (d (list (d (n "syn-helpers") (r "^0.5") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "1pwv0wi320kil0pgij59pqljr44g24q8n6cvb4mjl1nj7nxazs4d")))

