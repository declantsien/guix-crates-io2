(define-module (crates-io de ri derive-elves) #:use-module (crates-io))

(define-public crate-derive-elves-0.1.0 (c (n "derive-elves") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "09215gh1sdcb8crn2dv1wmdcqncsfvppqqjk5hkfzrhsydmx9xj3")))

(define-public crate-derive-elves-0.1.1 (c (n "derive-elves") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "055z4xn9i70nwfwnl436xxd1prbcz8xh5nzfvbka4lmmwph2sbkm")))

(define-public crate-derive-elves-0.1.2 (c (n "derive-elves") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1smy65bcyid17vkqdw4wm6hy906y3nvasxhv913zjbz3w1ln7pbr")))

