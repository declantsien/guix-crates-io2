(define-module (crates-io de ri derive_util) #:use-module (crates-io))

(define-public crate-derive_util-0.1.0 (c (n "derive_util") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nf5irl50ifrkvl7nh63kyfbqdwx52yn1wsdnffsp02ykvrgs0q4")))

(define-public crate-derive_util-0.1.1 (c (n "derive_util") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rbpl69wrxf8b775dagndyxvx5ilgizv28vlkqnaalkz6gdkr475")))

(define-public crate-derive_util-0.1.2 (c (n "derive_util") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01c5i7r4kyr5n2m037cpljk2yr6nx4sfvj9kbf27w0006msz196b")))

(define-public crate-derive_util-0.2.0 (c (n "derive_util") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04dq1m5cclij1hqf70173lij0vx6bxzqx96i0xv99bp0aj3xgb93")))

(define-public crate-derive_util-0.3.0 (c (n "derive_util") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m67dhz63hjw16hbg509rkxl4idll8ww7k6661hyy4g8ggdx4djq")))

