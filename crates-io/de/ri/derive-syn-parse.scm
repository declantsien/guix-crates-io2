(define-module (crates-io de ri derive-syn-parse) #:use-module (crates-io))

(define-public crate-derive-syn-parse-0.1.0 (c (n "derive-syn-parse") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1vw86mis39ln2s5qbiahjk2xmcp58yanirn3s9a2l9jqaygj30sg") (y #t)))

(define-public crate-derive-syn-parse-0.1.1 (c (n "derive-syn-parse") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1lzw9v1sv7gzlflp4bwmwl413qv3wbm969ls2yc28ckgpr8b40a9") (y #t)))

(define-public crate-derive-syn-parse-0.1.2 (c (n "derive-syn-parse") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0slgjzx5d7fhdr1pak4h74xywy9ndyr2smc7ippjmlp5yxzhmyi1")))

(define-public crate-derive-syn-parse-0.1.3 (c (n "derive-syn-parse") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0xbpqswfl336vb669dvpar2qqmnf9qycq4js1dwcli5bwpf2k0q1")))

(define-public crate-derive-syn-parse-0.1.4 (c (n "derive-syn-parse") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0rqf08j1rfc4szlbxid4gswpf59nqpkkaa03d4lka9dpgd8pqw4k")))

(define-public crate-derive-syn-parse-0.1.5 (c (n "derive-syn-parse") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1kb8qwyqmvdxzr5qd0rqgai7j3lv7gq0ad0zpwdbl7fx37qid4g7")))

(define-public crate-derive-syn-parse-0.2.0 (c (n "derive-syn-parse") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0qjplkxiia1b8yaghv3q1lm93862l5arlkbs95a0wz1b2gl7qpfn") (f (quote (("full" "syn/full"))))))

