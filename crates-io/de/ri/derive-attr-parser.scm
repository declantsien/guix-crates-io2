(define-module (crates-io de ri derive-attr-parser) #:use-module (crates-io))

(define-public crate-derive-attr-parser-0.1.0 (c (n "derive-attr-parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (f (quote ("default" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (f (quote ("default"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "01b4jzmp5qh2qlkx101r45ppqp1v9hvvszx3xjln6v7d0kxnx023")))

(define-public crate-derive-attr-parser-0.1.1 (c (n "derive-attr-parser") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (f (quote ("default" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (f (quote ("default"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0d5chlgpcq83bhi7yna95dv0n136q171al946xipjgm5fzzyniwz")))

