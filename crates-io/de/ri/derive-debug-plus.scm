(define-module (crates-io de ri derive-debug-plus) #:use-module (crates-io))

(define-public crate-derive-debug-plus-0.2.0 (c (n "derive-debug-plus") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05vyzmyvqqsl9zap24x7lw0fjd1b8pkxfmb96nbm4zim8z68j93c")))

(define-public crate-derive-debug-plus-0.3.0 (c (n "derive-debug-plus") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1a2d1gb68pq1hf55zdj3cmj8bfmlghlq5h0vcflvscv2zbrxlmzw")))

(define-public crate-derive-debug-plus-0.4.0 (c (n "derive-debug-plus") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03wqh5y306rjw0fv42gw9vq2zk5whn69fqhkzq5lybhx1ihhdb46")))

(define-public crate-derive-debug-plus-0.4.1 (c (n "derive-debug-plus") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "15lsn4a73j7mwv1110pwzwgkn31cdzgszny309vwh7irkq0ka5mx")))

(define-public crate-derive-debug-plus-0.5.0 (c (n "derive-debug-plus") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12lan58lls5m3mnpagfjz61g8wk3wh1axdfhcqss1bhqa2s11zzl")))

