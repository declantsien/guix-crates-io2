(define-module (crates-io de ri derive-hex) #:use-module (crates-io))

(define-public crate-derive-hex-0.1.0 (c (n "derive-hex") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bn218dr0sliijhqjz17ssz0nbr44f03xs9d4bqrx57dzg3652hn")))

(define-public crate-derive-hex-0.1.1 (c (n "derive-hex") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fz3mxs3isjknn4hfsm9x1lhcda6ncq9jkqii6v9anr1c8z02ijb")))

(define-public crate-derive-hex-0.1.2 (c (n "derive-hex") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v6nbvdzcxl5n3mhci7j4a21dwrmq2643fdx9wgirk9j7iaihrjb")))

