(define-module (crates-io de ri derive_convert) #:use-module (crates-io))

(define-public crate-derive_convert-0.3.0 (c (n "derive_convert") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1idcjavgcd8xz7r3f290zry39c5anc5c970ns5ca57fdx1blwi3v")))

(define-public crate-derive_convert-0.3.1 (c (n "derive_convert") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zymjdv75l0ppqpm74ahf86c4mk9lcz2nkig5jjzcfsg9jr94684")))

(define-public crate-derive_convert-0.3.2 (c (n "derive_convert") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwasw6xcppl1csfdkrhkpm4vzjakxkq03w8l8xrmpfb406x4mjg")))

(define-public crate-derive_convert-0.4.0 (c (n "derive_convert") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l803205h4f1k777jgzkg8k6rw8kghhysmjvk5p3imdb6j1x215c")))

(define-public crate-derive_convert-0.5.0 (c (n "derive_convert") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ix1nmyi789rg32myhdcc52l4di9g7dx1lgnzxgrkqvkx58fic9k")))

