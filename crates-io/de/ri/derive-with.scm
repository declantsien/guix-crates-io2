(define-module (crates-io de ri derive-with) #:use-module (crates-io))

(define-public crate-derive-with-0.1.0 (c (n "derive-with") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mjr0691bc2jxz0bawpa1xrdskxdr1mj9dzvwd8mlx011vsrxpd2")))

(define-public crate-derive-with-0.2.0 (c (n "derive-with") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0njqb9bm2gfxmwhwylrwwj9iqsak7x2dnapqsdnk8ciynb5m9cwh")))

(define-public crate-derive-with-0.3.0 (c (n "derive-with") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0f4bdy3cpyc3w3dr5jlqkwxx5zhcpaqyw7nm2jilpix1rmvxllnz")))

(define-public crate-derive-with-0.4.0 (c (n "derive-with") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "097yk1b5vgfscmbh68a7p3m8gr38rpc3b8q4dvlaj21jchr2w0fs")))

(define-public crate-derive-with-0.5.0 (c (n "derive-with") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0a7hjp4jgnpvn7hgw2n13340ybd4hj4h4bn9vpr50k2065ds47l4")))

