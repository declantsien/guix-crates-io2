(define-module (crates-io de ri derive_pod) #:use-module (crates-io))

(define-public crate-derive_pod-0.1.0 (c (n "derive_pod") (v "0.1.0") (h "0vcpw65b46l85ix360r98qrvhvr0ddbhzv71v898dhz3730brpf9")))

(define-public crate-derive_pod-0.1.1 (c (n "derive_pod") (v "0.1.1") (h "1jiy5zv37jg146w3rdg7y93mwzyl37bvw8i6nb6xaslazhklab9f")))

(define-public crate-derive_pod-0.1.2 (c (n "derive_pod") (v "0.1.2") (h "1j1g0izf53y5rsgjldb1xjm69xz7gz7vah0xbzhm9jjgsw36gsn2")))

