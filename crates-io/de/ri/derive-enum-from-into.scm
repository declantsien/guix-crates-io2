(define-module (crates-io de ri derive-enum-from-into) #:use-module (crates-io))

(define-public crate-derive-enum-from-into-0.1.0 (c (n "derive-enum-from-into") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lgb1ypk0yqaxhrvrl7dkm93hv4q1vn830cbkfs8vd9wqh4a16cp")))

(define-public crate-derive-enum-from-into-0.1.1 (c (n "derive-enum-from-into") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mfrmlgxg0rpdg1ivb5rhz0lf9x8bljs87xwx58vc7q3q2vs3hmd")))

(define-public crate-derive-enum-from-into-0.2.0 (c (n "derive-enum-from-into") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cvf6bfflgm173rhif6894h3wvgib4gy069b8p5mk11sw6ndvra2")))

