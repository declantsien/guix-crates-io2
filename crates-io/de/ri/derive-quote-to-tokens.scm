(define-module (crates-io de ri derive-quote-to-tokens) #:use-module (crates-io))

(define-public crate-derive-quote-to-tokens-0.1.0 (c (n "derive-quote-to-tokens") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.67") (k 2)) (d (n "quote") (r "^1.0.33") (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "syn-helpers") (r "^0.4.5") (d #t) (k 0)))) (h "12xcymr3si882ym8svyrfvjnkpqks6x3hj7lycw4ksz43svh9xj9")))

(define-public crate-derive-quote-to-tokens-0.1.1 (c (n "derive-quote-to-tokens") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (k 2)) (d (n "quote") (r "^1.0.35") (k 2)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "syn-helpers") (r "^0.5.0") (d #t) (k 0)))) (h "04yhgrhz072rmqia0ngfs1hygzzy952pd86ky94vy02bd70hfdkk")))

