(define-module (crates-io de ri derive-getters) #:use-module (crates-io))

(define-public crate-derive-getters-0.0.1 (c (n "derive-getters") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (d #t) (k 0)))) (h "0g51pza6nbprggr4v61lxr7c3c2w8rabhdyks6y8171gy838yla8")))

(define-public crate-derive-getters-0.0.2 (c (n "derive-getters") (v "0.0.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (d #t) (k 0)))) (h "0ixrvhkq98r4amafcxliq1lbg4sq2qzqsniccy1c5zcp1g51ncrl")))

(define-public crate-derive-getters-0.0.3 (c (n "derive-getters") (v "0.0.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1pgf63i78br06vapkpyfyyk3jhd5njayvdlyqjqwgf27jliw6kfa")))

(define-public crate-derive-getters-0.0.4 (c (n "derive-getters") (v "0.0.4") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0f2raiz125rclmg5m0nrcd907f72ac1bgfvpmfgxz3mps03n9pxb")))

(define-public crate-derive-getters-0.0.5 (c (n "derive-getters") (v "0.0.5") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0ay35bc415jg5aq0qqgnil9mvs74iiml952yxj783qmmk0889p1k") (y #t)))

(define-public crate-derive-getters-0.0.6 (c (n "derive-getters") (v "0.0.6") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0m82wvg9yn6bz1bb7aas777f0hij4q1zyrw9ir28bbqb9flgklx6") (y #t)))

(define-public crate-derive-getters-0.0.7 (c (n "derive-getters") (v "0.0.7") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0g0g89ccjkjshbdjhvbxwrdr6vl0x1njav3vrh3d1p8dkbmj5qa6")))

(define-public crate-derive-getters-0.0.8 (c (n "derive-getters") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06xna9gs07bkgip7al6xkipf67rh5k5h5fmzn7fn6b799ngsfiaa")))

(define-public crate-derive-getters-0.0.9 (c (n "derive-getters") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08vmxhabc1jnpgiwapvlmhrqfqx158b14pm345q1xjf1snfa4cmi")))

(define-public crate-derive-getters-0.1.0 (c (n "derive-getters") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cbn177ql3bvg50hdnf6349hvn5s043kd94sc2cfwq32ydbkfpng")))

(define-public crate-derive-getters-0.1.1 (c (n "derive-getters") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0s8q2xqzjlp6q01pnb82lhcyw20k7h113jqc8iks337h2wjlxp0n")))

(define-public crate-derive-getters-0.2.0 (c (n "derive-getters") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1cw6cvi5qzdbfy3wbwsknzbkqwq9wv4h2jis9waj1hyr1xkhan8c")))

(define-public crate-derive-getters-0.2.1 (c (n "derive-getters") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ir7ggq7r9r215xvwfr8xw88q4pvv44gi16s55w3d6lwpxig48h1")))

(define-public crate-derive-getters-0.3.0 (c (n "derive-getters") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1pz81w3blnkd6dzjjdlfxy542pf0pf5da7fsrnbj6r03dsmkab3s")))

(define-public crate-derive-getters-0.4.0 (c (n "derive-getters") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1fpngz5ikc8c9pfmq8nrxsd1qrkcflp3y2y6rjl2wmwpq2m36r0a") (r "1.60.0")))

