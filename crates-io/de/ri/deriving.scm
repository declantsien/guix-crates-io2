(define-module (crates-io de ri deriving) #:use-module (crates-io))

(define-public crate-deriving-0.1.0 (c (n "deriving") (v "0.1.0") (h "11cw3hm16yr7717gwiz3hrxx6dans8navvh62xjs7h6hn5bcf54k")))

(define-public crate-deriving-0.1.1 (c (n "deriving") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gyb6ch014dr5nknyi0v3dxz6myrnbvk818f3c1x13zbg5lmnrdk")))

(define-public crate-deriving-0.1.2 (c (n "deriving") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zqppjrm1ggjrd6md2f72h736k9spj72z0y0kfhqzzawd1yxq57j")))

(define-public crate-deriving-0.1.3 (c (n "deriving") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gf7l6ghrjzzlr914myk6rdli7kay38ggh5abhw01jjkakjsypkf")))

