(define-module (crates-io de ri derive-diesel-connection) #:use-module (crates-io))

(define-public crate-derive-diesel-connection-2.0.0-rc.0 (c (n "derive-diesel-connection") (v "2.0.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ra3s8zbya1czk195vpnzhapvrj2kbcmfgaa7mgylbcainxpdl6z")))

(define-public crate-derive-diesel-connection-2.0.0 (c (n "derive-diesel-connection") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ax8dlh6m1mw9id49zcdbrh8n7qp2japbyls435v0c5b7r5yi3hi")))

(define-public crate-derive-diesel-connection-4.0.0 (c (n "derive-diesel-connection") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18bpy684dyadlfa38vw0h96a30zvwahgs3plnqk79b2iz392dy4m")))

(define-public crate-derive-diesel-connection-4.1.0 (c (n "derive-diesel-connection") (v "4.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1kkcbiw2c90a8k40nchhrpcyw9rwwxxzkmpii8gxi479mpxnj1nv")))

