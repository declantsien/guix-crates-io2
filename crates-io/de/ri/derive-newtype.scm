(define-module (crates-io de ri derive-newtype) #:use-module (crates-io))

(define-public crate-derive-newtype-0.1.0 (c (n "derive-newtype") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "081j2d4mxyhdl602c67vv5snd7gjk8sci4cj8bw1lmfhaaamkavz")))

(define-public crate-derive-newtype-0.1.1 (c (n "derive-newtype") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "18gj1i4m97i6qn7hf33lx8nyi2q6x4gn4zblqkwnws10619gdq4w")))

(define-public crate-derive-newtype-0.1.2 (c (n "derive-newtype") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "1qy2zaldbagxnm53a7g7y7z1hshgqh0zxlhfvyyyabwx10w8b0hk")))

(define-public crate-derive-newtype-0.2.0 (c (n "derive-newtype") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "1wx8nb7gg5gqga2hvy3gx5xxb3xlxq4dnr35km9xgy7krj4zq2x7")))

(define-public crate-derive-newtype-0.2.1 (c (n "derive-newtype") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "1m8544rk7r21plybhd3hniifap5h4h9zwqdzdwc6192gmv0rpbhk")))

(define-public crate-derive-newtype-0.2.2 (c (n "derive-newtype") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "0zfqcb61fyb7asrmr9ixvi3m864g1ms44h7a9r2hmhr3nxy0944f")))

(define-public crate-derive-newtype-0.2.3 (c (n "derive-newtype") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (d #t) (k 0)))) (h "1ja1czny8s8cngfd84airk1hbpz8kq4vnmzv2pzk9ws3fv3733sg")))

