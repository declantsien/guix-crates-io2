(define-module (crates-io de ri derivit) #:use-module (crates-io))

(define-public crate-derivit-0.0.0 (c (n "derivit") (v "0.0.0") (h "15aid5l2ybidigqjv5zpkbrq9ddjhnb31prg8b0y8xqj441prk68")))

(define-public crate-derivit-0.0.1 (c (n "derivit") (v "0.0.1") (d (list (d (n "enumit") (r "^0.0.2") (d #t) (k 0)) (d (n "fromit") (r "^0.0.3") (d #t) (k 0)) (d (n "infoit") (r "^0.0.0") (d #t) (k 0)) (d (n "viewit") (r "^0.0.1") (d #t) (k 0)))) (h "1c1pm1gdskan6r2h6pzbxvgx40djsx2ig7hkm2lsz1hbjvgqavii")))

(define-public crate-derivit-0.1.1 (c (n "derivit") (v "0.1.1") (d (list (d (n "enumit") (r "^0.0.2") (d #t) (k 0)) (d (n "fromit") (r "^0.0.3") (d #t) (k 0)) (d (n "infoit") (r "^0.0.0") (d #t) (k 0)) (d (n "viewit") (r "^0.0.1") (d #t) (k 0)))) (h "1w3ck7dhc2xv1rkm3f4ccxw81grzkv3nlxjh6vf93lasvablssx7")))

(define-public crate-derivit-0.1.2 (c (n "derivit") (v "0.1.2") (d (list (d (n "enumit") (r "^0.1.2") (d #t) (k 0)) (d (n "fromit") (r "^0.1.2") (d #t) (k 0)) (d (n "infoit") (r "^0.1.2") (d #t) (k 0)) (d (n "viewit") (r "^0.1.2") (d #t) (k 0)))) (h "0ihxbwvfhbw16bllj87q7vygx0yfs5w4ll66cac9npfqxnxgnavh")))

