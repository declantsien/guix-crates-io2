(define-module (crates-io de ri derive-debug) #:use-module (crates-io))

(define-public crate-derive-debug-0.1.0 (c (n "derive-debug") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0a52qd2q35zvkddrn2gh6nifk4krliaqdrw3zs517x9aq5w2xal2")))

(define-public crate-derive-debug-0.1.1 (c (n "derive-debug") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1m0rdxpyinlvx9qdji32g02jm4l4dpcmcjkgrcgqv52if89gy67v")))

(define-public crate-derive-debug-0.1.2 (c (n "derive-debug") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1cbkfwxs29pzfqx0dhw6vvfnwi7c0jahm6jbwyldavvmrzhzfgp5")))

