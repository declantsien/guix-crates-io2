(define-module (crates-io de ri derive) #:use-module (crates-io))

(define-public crate-derive-0.1.0 (c (n "derive") (v "0.1.0") (h "1nmdp0lizqmg5s5zygfi5fjpcq7k1k3p8qbdii0bcadxjx8jspwc")))

(define-public crate-derive-1.0.0 (c (n "derive") (v "1.0.0") (h "02zdr83l1z5b4fawp5whc02lgkhzyxsj9z1iwbr7799hr0kaxqvj")))

