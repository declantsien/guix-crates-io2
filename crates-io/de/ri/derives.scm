(define-module (crates-io de ri derives) #:use-module (crates-io))

(define-public crate-derives-0.1.0 (c (n "derives") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "1pfqr9gi3g2fzcnf35rn1ky0j2lcfajvps85vsyq0sp6gwvq6lw6")))

