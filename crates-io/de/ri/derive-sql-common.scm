(define-module (crates-io de ri derive-sql-common) #:use-module (crates-io))

(define-public crate-derive-sql-common-0.8.0 (c (n "derive-sql-common") (v "0.8.0") (d (list (d (n "attribute-derive") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0793ih2aixrxmhj1hzzjsk66yf7jyirzfghy3rdr8rz1xhvvxmgl")))

(define-public crate-derive-sql-common-0.8.1 (c (n "derive-sql-common") (v "0.8.1") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nh4d4vsi1nvkxrzjg8z251inwqpphhzmsnmdprv564adg250vrk")))

(define-public crate-derive-sql-common-0.9.0 (c (n "derive-sql-common") (v "0.9.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11bmh8vnk23g97cssixqajb027lb1lbpzq772bgg85gd9vdw4hfw")))

