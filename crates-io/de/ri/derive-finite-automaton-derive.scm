(define-module (crates-io de ri derive-finite-automaton-derive) #:use-module (crates-io))

(define-public crate-derive-finite-automaton-derive-0.1.0 (c (n "derive-finite-automaton-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("printing" "full" "parsing"))) (d #t) (k 0)))) (h "0lffdkd01k06a3vy72cclj1a1cvgwniszij2qvl3xhfd97pz3wg1")))

(define-public crate-derive-finite-automaton-derive-0.1.1 (c (n "derive-finite-automaton-derive") (v "0.1.1") (d (list (d (n "either_n") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("printing" "full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1kzm2m97kd974w8cv2pz3lcdr36fx1l5p618lrzxrh4zlfpxh6yn")))

(define-public crate-derive-finite-automaton-derive-0.1.2 (c (n "derive-finite-automaton-derive") (v "0.1.2") (d (list (d (n "either_n") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("printing" "full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1hq2hmbhi4hvkgjknwcx3g2vi13sb0lzf970b7y2ln7x4fzig3s9")))

(define-public crate-derive-finite-automaton-derive-0.1.3 (c (n "derive-finite-automaton-derive") (v "0.1.3") (d (list (d (n "either_n") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("printing" "full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "04rnzj9n2a3mbjykgdvab9y8xzs837sfn1sjq1xmknigg4p4dfyl")))

(define-public crate-derive-finite-automaton-derive-0.2.0 (c (n "derive-finite-automaton-derive") (v "0.2.0") (d (list (d (n "either_n") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("printing" "full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "14znklhmxz9880vcbw9wqk087naj6zj648ajy9ijg6zvddjyhvn2")))

