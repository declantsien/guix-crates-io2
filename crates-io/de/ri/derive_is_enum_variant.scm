(define-module (crates-io de ri derive_is_enum_variant) #:use-module (crates-io))

(define-public crate-derive_is_enum_variant-0.1.0 (c (n "derive_is_enum_variant") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1fw1y3c39hxr8lw6rvd04rwjdbwnn7981j5wky8spg1fagyx1jqi")))

(define-public crate-derive_is_enum_variant-0.1.1 (c (n "derive_is_enum_variant") (v "0.1.1") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "15w18649m4h47pdpr04id0wv8br8bg606zvrafcrfijihicqib6h")))

