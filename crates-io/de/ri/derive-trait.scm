(define-module (crates-io de ri derive-trait) #:use-module (crates-io))

(define-public crate-derive-trait-0.0.1 (c (n "derive-trait") (v "0.0.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrmgxgz8ysn16swg77dkf4zrw1jb6yp3i47zd7ikajiqppc6syh")))

(define-public crate-derive-trait-0.0.2 (c (n "derive-trait") (v "0.0.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1hc919d8mwhpp1h9g0bw1y1y638jm1q8lbpp0r6rxhzks5b9c7q9")))

(define-public crate-derive-trait-0.0.3 (c (n "derive-trait") (v "0.0.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0qs5ahsrv10zs693v3h0ai3rbdd82bsd33w90qr3qj1skg22wbaj")))

(define-public crate-derive-trait-0.0.4 (c (n "derive-trait") (v "0.0.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12529625ggd6w4q6vvpggnykzg5nrkiaw9yjc7r7q3q8s79dfg3d")))

