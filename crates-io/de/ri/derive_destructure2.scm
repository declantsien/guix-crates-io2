(define-module (crates-io de ri derive_destructure2) #:use-module (crates-io))

(define-public crate-derive_destructure2-0.0.1 (c (n "derive_destructure2") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05riamx0l1494n4v0r2ypjzqg3x513zy2ah7jqx8sy4k3k8fkzgb")))

(define-public crate-derive_destructure2-0.0.2 (c (n "derive_destructure2") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hbrn6mvgrw3imm8hxkq47207cmspggnc5vdnj3b0ka0081n00p9")))

(define-public crate-derive_destructure2-0.1.0 (c (n "derive_destructure2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pc6ry5x2xc7z9zfsq9f5a0dlhl2cxnx12ysb0zr12fn1sqjzkjc")))

(define-public crate-derive_destructure2-0.1.1 (c (n "derive_destructure2") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xgjac64s3ycjzgxm8mvdlymrwhq05npsx2iwmrql0p1fmc7xjrm")))

(define-public crate-derive_destructure2-0.1.2 (c (n "derive_destructure2") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "15cwn425xqyqsmipbsjv1vy3g8114dxpc6gach5w09dd1ccnhjxc")))

(define-public crate-derive_destructure2-0.1.3 (c (n "derive_destructure2") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "037w2cv2429vhwrgpqshzygzncdcqxhmmviiq07nyagzj2n9gdk4")))

