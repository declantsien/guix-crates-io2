(define-module (crates-io de ri derive_display_from_debug) #:use-module (crates-io))

(define-public crate-derive_display_from_debug-0.1.0 (c (n "derive_display_from_debug") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "0kxbnxkpah6xq9g7rq1m1wib802f9v83dpm9zgvjl9n20w3c4li0")))

(define-public crate-derive_display_from_debug-0.1.1 (c (n "derive_display_from_debug") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "1ppdmy7w2mck6chb1896ga83670i0lyl3wwc7d23b0x96x6iwld5")))

(define-public crate-derive_display_from_debug-0.1.2 (c (n "derive_display_from_debug") (v "0.1.2") (d (list (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (d #t) (k 0)))) (h "15z44ycinirc513j1jc87lc7ydvyh31mjr731pcnsr9zc9fq2w4b")))

