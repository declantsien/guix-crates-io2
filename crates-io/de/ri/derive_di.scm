(define-module (crates-io de ri derive_di) #:use-module (crates-io))

(define-public crate-derive_di-0.2.0 (c (n "derive_di") (v "0.2.0") (d (list (d (n "derive_di_core") (r "^0.2") (d #t) (k 0)) (d (n "derive_di_macro") (r "^0.2") (d #t) (k 0)))) (h "00ank5lvv9lkcvanl5wynamzyb3jj5s16arbdf78s1va6v1k8v8z")))

(define-public crate-derive_di-0.2.1 (c (n "derive_di") (v "0.2.1") (d (list (d (n "derive_di_core") (r "^0.2") (d #t) (k 0)) (d (n "derive_di_macro") (r "^0.2") (d #t) (k 0)))) (h "03xa6n6dljqsiq95k5jqb3i3mmqw8nf4if7k8m2pb175z4wcjmxr")))

(define-public crate-derive_di-0.2.2 (c (n "derive_di") (v "0.2.2") (d (list (d (n "derive_di_core") (r "^0.2") (d #t) (k 0)) (d (n "derive_di_macro") (r "^0.2.1") (d #t) (k 0)))) (h "16s7lpxg6k2bl2i39113a6vxjr97f388389zjgp11zjh8nwha3a9")))

(define-public crate-derive_di-0.3.0 (c (n "derive_di") (v "0.3.0") (d (list (d (n "derive_di_core") (r "^0.2") (d #t) (k 0)) (d (n "derive_di_macro") (r "^0.3") (d #t) (k 0)))) (h "05zp5k0mxpsd5jg3wqz8w2r7csb7ffv5y46qbi8jq2m4n0wlpkk7")))

