(define-module (crates-io de ri derive-ocaml) #:use-module (crates-io))

(define-public crate-derive-ocaml-0.1.0 (c (n "derive-ocaml") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1ypirh0kq4bv0fwgmnww69b8400gxrpfkgvbvwf3hcsgh4ar6qdw") (f (quote (("stubs" "syn/full") ("derive" "synstructure") ("default" "derive"))))))

(define-public crate-derive-ocaml-0.1.1 (c (n "derive-ocaml") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (o #t) (d #t) (k 0)))) (h "18k9rwz063svris6cxwv74aqr24a5nxkmsgbqghywnlgsgms2bmk") (f (quote (("stubs" "syn/full") ("derive" "synstructure") ("default" "derive"))))))

(define-public crate-derive-ocaml-0.1.2 (c (n "derive-ocaml") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0p8i9w50rryf6qa9nzz27dlaazrq618qnilrcdy5v9drbsqalgji") (f (quote (("stubs" "syn/full") ("derive" "synstructure") ("default" "derive"))))))

(define-public crate-derive-ocaml-0.1.3 (c (n "derive-ocaml") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0lqizhwqdghc8ig35dsrnmlapapc97pgjj46amw7zhj9s532z82a") (f (quote (("stubs" "syn/full") ("derive" "synstructure") ("default" "derive"))))))

