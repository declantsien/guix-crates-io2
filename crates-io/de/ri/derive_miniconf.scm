(define-module (crates-io de ri derive_miniconf) #:use-module (crates-io))

(define-public crate-derive_miniconf-0.1.0 (c (n "derive_miniconf") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "019di2prvn59v335bg40008l0k96vvkxl8sj9ajylmgyvlfwy8hn")))

(define-public crate-derive_miniconf-0.1.1 (c (n "derive_miniconf") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g7raxcaq4ynra8wyd14895c9sdwavy4bcjssi7780168yli1550")))

(define-public crate-derive_miniconf-0.3.0 (c (n "derive_miniconf") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jij16hs25d1rxajbzd51zj0xia919anr97wprxcpk6yzrpp9w2b")))

(define-public crate-derive_miniconf-0.4.0 (c (n "derive_miniconf") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1aj2p2kb73a1ix1qg1izgzca2vqjdhg2q0zsx537hyb9anjwnwa2")))

(define-public crate-derive_miniconf-0.5.0 (c (n "derive_miniconf") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qd1xp59z0smpqma5401ljlzvild4wrsrs61bw1y8j5rccipnn4w")))

(define-public crate-derive_miniconf-0.6.0 (c (n "derive_miniconf") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01i35b6w929rnrdzn84i9s1k5q89m7d5l0djk8cqmvjcjy1q0w4k")))

