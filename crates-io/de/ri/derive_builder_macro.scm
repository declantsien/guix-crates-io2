(define-module (crates-io de ri derive_builder_macro) #:use-module (crates-io))

(define-public crate-derive_builder_macro-0.10.0-alpha (c (n "derive_builder_macro") (v "0.10.0-alpha") (d (list (d (n "derive_builder_core") (r "=0.10.0-alpha") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03v5xqi4hjh83aswisph36kj10s3yhawn6xwcrx8p1x5s6w0bx4f") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.10.0 (c (n "derive_builder_macro") (v "0.10.0") (d (list (d (n "derive_builder_core") (r "=0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p7hfz2ikzjizfld3d31y853k17kjixv5bilwz8hip7gvn5h18aw") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.10.1 (c (n "derive_builder_macro") (v "0.10.1") (d (list (d (n "derive_builder_core") (r "=0.10.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xmg8x8r2p4g7s0lsszzwbkifd8ssrzkpvgx3ik9zfk0mnz74w3x") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.10.2 (c (n "derive_builder_macro") (v "0.10.2") (d (list (d (n "derive_builder_core") (r "=0.10.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wwdm4cgd4vlvabj5xsjjr4vvkqhnd3fi9wp3v5mlb09jp74maaq") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.11.0 (c (n "derive_builder_macro") (v "0.11.0") (d (list (d (n "derive_builder_core") (r "=0.11.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h262s9jrv77p8wb5rylq35xsr5nxh73sax5xpmd2kyagg249bjw") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.11.1 (c (n "derive_builder_macro") (v "0.11.1") (d (list (d (n "derive_builder_core") (r "=0.11.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bibwkqyb7cmb9y2kvpl0v7fg9dchx3cmqyfwvfr19d9niqsr8m8") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.11.2 (c (n "derive_builder_macro") (v "0.11.2") (d (list (d (n "derive_builder_core") (r "=0.11.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s6xfgsybd9wbk39hbgqjcn7d1l36a33q6v7d0x5y17d5fvi80wg") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.12.0 (c (n "derive_builder_macro") (v "0.12.0") (d (list (d (n "derive_builder_core") (r "=0.12.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17p71qzh7x1q2yxzz3xrg73zw3xl0h479b7ybyjm0s1rg9fa7kgb") (f (quote (("clippy" "derive_builder_core/clippy"))))))

(define-public crate-derive_builder_macro-0.13.0 (c (n "derive_builder_macro") (v "0.13.0") (d (list (d (n "derive_builder_core") (r "=0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fv6xw866f2lsq30mvx0qzgjlrdsial19jpp50s1bj8awbbpfpvs") (f (quote (("lib_has_std" "derive_builder_core/lib_has_std") ("clippy" "derive_builder_core/clippy") ("alloc" "derive_builder_core/alloc"))))))

(define-public crate-derive_builder_macro-0.13.1 (c (n "derive_builder_macro") (v "0.13.1") (d (list (d (n "derive_builder_core") (r "=0.13.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09q17rzgf8bsj8n1bhlf4f93nmqg8va6321ppcd07f1mzg1nh0w7") (f (quote (("lib_has_std" "derive_builder_core/lib_has_std") ("clippy" "derive_builder_core/clippy") ("alloc" "derive_builder_core/alloc"))))))

(define-public crate-derive_builder_macro-0.20.0 (c (n "derive_builder_macro") (v "0.20.0") (d (list (d (n "derive_builder_core") (r "=0.20.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yq9hnyayys16rzmiwjd6gfx1ysph7c9zh94w76cw9rg4jw6hs10") (f (quote (("lib_has_std" "derive_builder_core/lib_has_std") ("clippy" "derive_builder_core/clippy") ("alloc" "derive_builder_core/alloc"))))))

