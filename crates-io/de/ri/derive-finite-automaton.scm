(define-module (crates-io de ri derive-finite-automaton) #:use-module (crates-io))

(define-public crate-derive-finite-automaton-0.1.0 (c (n "derive-finite-automaton") (v "0.1.0") (d (list (d (n "derive-finite-automaton-derive") (r "^0.1.0") (d #t) (k 0)))) (h "16ydn64ffygn5vyllm6sz17wxvg8vsn48ikkwf2477nvyyn20xal")))

(define-public crate-derive-finite-automaton-0.1.1 (c (n "derive-finite-automaton") (v "0.1.1") (d (list (d (n "derive-finite-automaton-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1zaqzmwnx7hr902sjsp975zfwlhgpcwwhx12qxz31yhs0x56bn45")))

(define-public crate-derive-finite-automaton-0.1.2 (c (n "derive-finite-automaton") (v "0.1.2") (d (list (d (n "derive-finite-automaton-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0wbx728ynqx7j490fpq5dwpwm5cbxkqpp7l0992zhrkm7ap4l6fq")))

(define-public crate-derive-finite-automaton-0.1.3 (c (n "derive-finite-automaton") (v "0.1.3") (d (list (d (n "derive-finite-automaton-derive") (r "^0.1.3") (d #t) (k 0)))) (h "08c264y40iglgidf60bvic6nbgxckpm1y5i72hlgq9i0l0s37351") (f (quote (("special"))))))

(define-public crate-derive-finite-automaton-0.2.0 (c (n "derive-finite-automaton") (v "0.2.0") (d (list (d (n "derive-finite-automaton-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0gfcyijljkjsip58sml0mrznj4jj5m812259q76hz8vgvns24x4w") (f (quote (("special"))))))

