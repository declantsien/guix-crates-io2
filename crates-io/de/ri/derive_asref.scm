(define-module (crates-io de ri derive_asref) #:use-module (crates-io))

(define-public crate-derive_asref-0.1.0 (c (n "derive_asref") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ck76dmqbkq4ddf43i8s8wdmgmcc9mg7r5g7arz4py3iv9dx3j14")))

