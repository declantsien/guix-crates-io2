(define-module (crates-io de ri derive-adhoc) #:use-module (crates-io))

(define-public crate-derive-adhoc-0.0.1 (c (n "derive-adhoc") (v "0.0.1") (d (list (d (n "derive-adhoc-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0280lg6srcd01d2j50ca0ginzn0b3zcvi5ip0k4j57bfsbqq5cwv")))

(define-public crate-derive-adhoc-0.1.0 (c (n "derive-adhoc") (v "0.1.0") (d (list (d (n "derive-adhoc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)))) (h "172amvnlslyf694mrm54hirrs16xc8bn71b2mzacmmjjhi14hxr2")))

(define-public crate-derive-adhoc-0.2.0 (c (n "derive-adhoc") (v "0.2.0") (d (list (d (n "derive-adhoc-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)))) (h "1g1avrh1rd17q8vdi8xfkgx1h5jc5cwwahdwlzwvjfpkgkwxw965") (r "1.54")))

(define-public crate-derive-adhoc-0.2.1 (c (n "derive-adhoc") (v "0.2.1") (d (list (d (n "derive-adhoc-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)))) (h "0k7yh76vz5prq5rdyqwy9f7dn7g0gfmpq5ibv5jq44zz48qd37w1") (r "1.54")))

(define-public crate-derive-adhoc-0.2.2 (c (n "derive-adhoc") (v "0.2.2") (d (list (d (n "derive-adhoc-macros") (r "=0.2.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)))) (h "17js50axqwk82vhnnbx0k8z33ijz5gqgpkcbk2j862fhjj68jyyx") (r "1.54")))

(define-public crate-derive-adhoc-0.3.0 (c (n "derive-adhoc") (v "0.3.0") (d (list (d (n "derive-adhoc-macros") (r "=0.3.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0l2m8fbp3n5mczz3aqiskrs68mzxs9jwpc66hba3fyll56lgvsh7") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.4.0 (c (n "derive-adhoc") (v "0.4.0") (d (list (d (n "derive-adhoc-macros") (r "=0.4.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "18k31if7q2sy9bzg4xp6x8pw5lck2q5dpm1wlb4y0c5hkw7vyiq0") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.5.0 (c (n "derive-adhoc") (v "0.5.0") (d (list (d (n "derive-adhoc-macros") (r "=0.5.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0aq43h378swkh1g1ih4szqrhrvfxqry0lx6z0cxg4f8j4i1q7z6v") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.6.0 (c (n "derive-adhoc") (v "0.6.0") (d (list (d (n "derive-adhoc-macros") (r "=0.6.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "09awsbsyb41wpbr74r5xiwyy3ic59f7ra5x8g105xvvlcl781xna") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.6.1 (c (n "derive-adhoc") (v "0.6.1") (d (list (d (n "derive-adhoc-macros") (r "=0.6.1") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1m3cjjd6axx7dw8cgbddzzijx55qqg4lz1a3cipvmd445azg3x1v") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.7.0 (c (n "derive-adhoc") (v "0.7.0") (d (list (d (n "derive-adhoc-macros") (r "=0.7.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "08fnbyi9fglnrr6nh11sch7iqh3f9p9crgw0d3kgva9lvg49jw7n") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.7.1 (c (n "derive-adhoc") (v "0.7.1") (d (list (d (n "derive-adhoc-macros") (r "=0.7.1") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1wd054ns3vrn5fxq86balm64rfxx8wfpmjx0vdn70p4zgd334yp4") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.7.2 (c (n "derive-adhoc") (v "0.7.2") (d (list (d (n "derive-adhoc-macros") (r "=0.7.2") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1zdc8jqz900hnpv3a2w4a63xsiksm2zkwvfhcbngqpxipry15zq2") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.7.3 (c (n "derive-adhoc") (v "0.7.3") (d (list (d (n "derive-adhoc-macros") (r "=0.7.3") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1f7d052qyh6kcj4y30czchqapnghadap01i4i707cg3mh4lar0sj") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.54")))

(define-public crate-derive-adhoc-0.8.0 (c (n "derive-adhoc") (v "0.8.0") (d (list (d (n "derive-adhoc-macros") (r "=0.8.0") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1m9piwmvwwd7zx8hpgdrbdpn67xx3wjidignlbd4j727p957mnhr") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.56")))

(define-public crate-derive-adhoc-0.8.1 (c (n "derive-adhoc") (v "0.8.1") (d (list (d (n "derive-adhoc-macros") (r "=0.8.1") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1yqriq88625n1pkcnzj9rziwnmdncvlbzr6fnp842ixagwc3kmdc") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.56")))

(define-public crate-derive-adhoc-0.8.2 (c (n "derive-adhoc") (v "0.8.2") (d (list (d (n "derive-adhoc-macros") (r "=0.8.2") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0qpx0ykv5g8wzwz9lhxcf5gv3f3s9bas9viyj2bzys45rxl5rjjp") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.56")))

(define-public crate-derive-adhoc-0.8.3 (c (n "derive-adhoc") (v "0.8.3") (d (list (d (n "derive-adhoc-macros") (r "=0.8.3") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00mxnxrlmcpr7cx4ph1pmp1v43c8x4plkcs78kjk7nd0l62z3hwz") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.56")))

(define-public crate-derive-adhoc-0.8.4 (c (n "derive-adhoc") (v "0.8.4") (d (list (d (n "derive-adhoc-macros") (r "=0.8.4") (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1f04acvlzngfbvyk42zqwkqqmvgr1g8k5dzh7963ch3s45bqfxhc") (f (quote (("minimal-1") ("full" "case" "expect" "minimal-1") ("expect" "derive-adhoc-macros/expect") ("default" "full") ("case" "derive-adhoc-macros/case" "heck")))) (r "1.56")))

