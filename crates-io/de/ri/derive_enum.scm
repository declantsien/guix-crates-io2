(define-module (crates-io de ri derive_enum) #:use-module (crates-io))

(define-public crate-derive_enum-0.1.0 (c (n "derive_enum") (v "0.1.0") (d (list (d (n "derive_enum_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "12hds628whm9v4dxacxhnmc9ypy1p88zp0hjb3611iq49k8sd4w7") (f (quote (("name" "derive_enum_macros" "derive_enum_macros/name") ("from_str" "derive_enum_macros" "derive_enum_macros/from_str") ("default" "name" "from_str"))))))

(define-public crate-derive_enum-0.2.0 (c (n "derive_enum") (v "0.2.0") (d (list (d (n "derive_enum_macros") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0avfkkqj15dy8zxdjchckdwcrmzmm75nvl34w7zldkvq7grl5rym") (f (quote (("name" "derive_enum_macros" "derive_enum_macros/name") ("iter" "derive_enum_macros" "derive_enum_macros/iter") ("from_str" "derive_enum_macros" "derive_enum_macros/from_str") ("default" "name" "from_str" "iter"))))))

(define-public crate-derive_enum-0.2.2 (c (n "derive_enum") (v "0.2.2") (d (list (d (n "derive_enum_macros") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mmqgagxwr35bjmcky047im2i5r185vgpg8aipsvdginhlghjh5f") (f (quote (("name" "derive_enum_macros" "derive_enum_macros/name") ("iter" "derive_enum_macros" "derive_enum_macros/iter") ("from_str" "derive_enum_macros" "derive_enum_macros/from_str") ("default" "name" "from_str" "iter"))))))

