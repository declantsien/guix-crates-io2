(define-module (crates-io de ri derive-visitor-macros) #:use-module (crates-io))

(define-public crate-derive-visitor-macros-0.1.0 (c (n "derive-visitor-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16h78plhssxqx4iscy5i2np10bnq9pqwqm5znp75qvz97imv0ql2")))

(define-public crate-derive-visitor-macros-0.1.2 (c (n "derive-visitor-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "085qn6gp9yyrlvff50a2d53s7m3fhn7h9m0kghfx4hry7rwj5dza")))

(define-public crate-derive-visitor-macros-0.2.1 (c (n "derive-visitor-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wd73y54imzskf4ysl84kk0kbw5q4rqq70y821c49nfpsk6bd9af")))

(define-public crate-derive-visitor-macros-0.3.0 (c (n "derive-visitor-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04l0mys2xnfj84j1kw4cg73i5yfdp16fbi99l0czp05lnwma9ii2")))

(define-public crate-derive-visitor-macros-0.4.0 (c (n "derive-visitor-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xnyz3vxp2ph0y0h7zmlbvij440m6w2gbwx5n4bfmbzcbyl3jys2")))

