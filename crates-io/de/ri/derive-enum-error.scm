(define-module (crates-io de ri derive-enum-error) #:use-module (crates-io))

(define-public crate-derive-enum-error-0.0.1 (c (n "derive-enum-error") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0vidryiy7ygpkv06hagjpw2vf8ql84kf73jr85lp7z4cb4k4rbas")))

