(define-module (crates-io de ri derive-tokio-io) #:use-module (crates-io))

(define-public crate-derive-tokio-io-0.1.0 (c (n "derive-tokio-io") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "14frvids7v6ralfm14f026rdnhcdp2ivr2zmnh75n2416gdmadfq")))

