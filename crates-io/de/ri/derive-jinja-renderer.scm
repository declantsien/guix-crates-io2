(define-module (crates-io de ri derive-jinja-renderer) #:use-module (crates-io))

(define-public crate-derive-jinja-renderer-0.1.0 (c (n "derive-jinja-renderer") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gvwr15374c2xmwqgwhgwq6hczy5hl7nzkim61v9rqnsmza91jxf")))

(define-public crate-derive-jinja-renderer-0.3.0 (c (n "derive-jinja-renderer") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "jinja-renderer") (r "^0.4") (f (quote ("minify"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s03d62d07b5r7j81g1b2032z1bargz8309ky8q0s4rjn5zvr6s3")))

