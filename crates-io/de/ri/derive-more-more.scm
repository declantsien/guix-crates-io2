(define-module (crates-io de ri derive-more-more) #:use-module (crates-io))

(define-public crate-derive-more-more-0.1.0 (c (n "derive-more-more") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03cf0f1jvvlks2c3ydnx1s480imdgzb2hkalrxdh5gkvfkp0li11")))

