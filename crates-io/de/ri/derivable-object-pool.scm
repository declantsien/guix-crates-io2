(define-module (crates-io de ri derivable-object-pool) #:use-module (crates-io))

(define-public crate-derivable-object-pool-0.1.0 (c (n "derivable-object-pool") (v "0.1.0") (d (list (d (n "derivable-object-pool-macros") (r "^0.1.0") (d #t) (k 0)))) (h "07zg28y4fac3hgqypfna8cddyinj4jx6gm60ysdbi6ynyfb3dz94")))

(define-public crate-derivable-object-pool-0.1.1 (c (n "derivable-object-pool") (v "0.1.1") (d (list (d (n "derivable-object-pool-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0rffa2zhwvkcbrdvrq9qm9q2p49j0b0a3k5b5k8b5qgnr2ifq32j")))

