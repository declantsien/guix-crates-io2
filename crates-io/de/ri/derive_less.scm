(define-module (crates-io de ri derive_less) #:use-module (crates-io))

(define-public crate-derive_less-0.1.0 (c (n "derive_less") (v "0.1.0") (h "0rf84ll705gwkxqcwhv80qmgbs3is6dsdrmh0hxylq079syg42sx")))

(define-public crate-derive_less-0.1.1 (c (n "derive_less") (v "0.1.1") (h "0w2szvggd27c8f2r2v9yz5qcs47sqa24d6wdzlj7cyzk2ajr9vyn")))

(define-public crate-derive_less-0.1.2 (c (n "derive_less") (v "0.1.2") (h "18y8pz4pvhw3fqyxqrdfdsjf805y7g0kb9k66sjlgcg90c8lb7z9")))

(define-public crate-derive_less-0.1.3 (c (n "derive_less") (v "0.1.3") (h "0y19qjpzf4yvaip0n5gljsd2a7rlz78mvw34w3gqlvh1vhi6mjv0")))

(define-public crate-derive_less-0.1.4 (c (n "derive_less") (v "0.1.4") (h "0fdc1l3w344i8h6ikilpjj5wc9kfrzn76lb7p1jhvabw2vzxw13l")))

(define-public crate-derive_less-0.1.5 (c (n "derive_less") (v "0.1.5") (h "1xw1hwzms6kyiqkn4z8szbzjq44286k6dapk53xvrzdd62y1qq74")))

(define-public crate-derive_less-0.1.6 (c (n "derive_less") (v "0.1.6") (h "0l7f78rybpp9bpaam0xcgz2g65bg6zrx32m5drrxq9xwvw2v6kfc")))

(define-public crate-derive_less-0.1.7 (c (n "derive_less") (v "0.1.7") (h "0bbapmyk9msmrrrhf70j57g3n70jjs4sh1gil3kxxzxjxaqdl9hw")))

(define-public crate-derive_less-0.2.0 (c (n "derive_less") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "15lcd82ndlb414x49aba6fym2id4jg68pfd6777kk3yhnnz2zs7r")))

