(define-module (crates-io de p_ dep_crusher) #:use-module (crates-io))

(define-public crate-dep_crusher-0.1.0 (c (n "dep_crusher") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 2)) (d (n "same-file") (r "^1.0.6") (d #t) (k 2)))) (h "0xfp4l4f3mi41kkqc7sqvalyj4xzn4pflq2iy9qwa97zzm74pkdi")))

(define-public crate-dep_crusher-0.1.1 (c (n "dep_crusher") (v "0.1.1") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 2)) (d (n "same-file") (r "^1.0.6") (d #t) (k 2)))) (h "0v4q1bj7qaw0dqa66s2yzq4yznvlx9fxfcfqd1jp74laknq1hlrz")))

(define-public crate-dep_crusher-0.1.2 (c (n "dep_crusher") (v "0.1.2") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 2)) (d (n "same-file") (r "^1.0.6") (d #t) (k 2)))) (h "1bg1wrk6kyw8fs6qajwzngd623a8pq6li1lrh7sw1j32ab51vdyv")))

