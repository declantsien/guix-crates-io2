(define-module (crates-io de p_ dep_d) #:use-module (crates-io))

(define-public crate-dep_d-2.0.0 (c (n "dep_d") (v "2.0.0") (h "1zg3g0r87aj0zgjlxc81c99js9hp5dssyx4fd0x8q2a644jixq5l") (y #t)))

(define-public crate-dep_d-3.0.0 (c (n "dep_d") (v "3.0.0") (h "15cc4af08qjiffg2ks93y18a1vb472z09ld93xpnd7nhzq2dpx4p") (y #t)))

(define-public crate-dep_d-3.1.0 (c (n "dep_d") (v "3.1.0") (h "1194djkhjkqfhcii90r93y0qb2v23clbikjcyji36w24hjkw5v6v") (y #t)))

