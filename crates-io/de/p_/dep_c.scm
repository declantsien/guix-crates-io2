(define-module (crates-io de p_ dep_c) #:use-module (crates-io))

(define-public crate-dep_c-1.0.0 (c (n "dep_c") (v "1.0.0") (d (list (d (n "dep_d") (r "^2.0.0") (d #t) (k 0)))) (h "0gbvpizcvy7vanzq9xxm9jhdm8a63n5bm51fmq8q1yqa1jppbiza") (y #t)))

(define-public crate-dep_c-2.0.0 (c (n "dep_c") (v "2.0.0") (d (list (d (n "dep_d") (r "^3.0.0") (d #t) (k 0)))) (h "1yg3qw6inp686cl058a8qs88prbavkzcva3bzz02bcr1f34nih59") (y #t)))

