(define-module (crates-io de p_ dep_doc) #:use-module (crates-io))

(define-public crate-dep_doc-0.1.0 (c (n "dep_doc") (v "0.1.0") (h "01iiyrzw4havar4q2p678nyj302v6mp46fqrsaw6i4nxjvl1z4fr") (r "1.54")))

(define-public crate-dep_doc-0.1.1 (c (n "dep_doc") (v "0.1.1") (h "1sajgbadp16ljaig57cdl19rr36naaxhr1v16glnm91ix3a1frhr") (r "1.54")))

