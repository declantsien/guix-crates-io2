(define-module (crates-io de fu defunctionalize-proc-macro) #:use-module (crates-io))

(define-public crate-defunctionalize-proc-macro-0.2.0 (c (n "defunctionalize-proc-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qliasd8dg7mbzli5r6m5lwl5y5nmsyz9gn18ibynkxq9n420n3x")))

