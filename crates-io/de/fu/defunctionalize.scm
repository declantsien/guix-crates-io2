(define-module (crates-io de fu defunctionalize) #:use-module (crates-io))

(define-public crate-defunctionalize-0.1.0 (c (n "defunctionalize") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b7qhyc7lvxqhbqfsrkbngqbci3jmrxzss7y5kxy9sskywns2kh2")))

(define-public crate-defunctionalize-0.2.0 (c (n "defunctionalize") (v "0.2.0") (d (list (d (n "defunctionalize-proc-macro") (r "= 0.2.0") (o #t) (d #t) (k 0)))) (h "13iyvgy5zckb2vsshpqicqz9smxd3m91ya84lq297hzph1h9728f") (f (quote (("proc-macro" "defunctionalize-proc-macro") ("default" "proc-macro"))))))

