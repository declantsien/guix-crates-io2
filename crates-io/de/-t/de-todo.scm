(define-module (crates-io de -t de-todo) #:use-module (crates-io))

(define-public crate-de-todo-0.1.0 (c (n "de-todo") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "144cc88n718gbnp7zazxfmhxaynipnlhl77kdxi2821xscblxgsd")))

(define-public crate-de-todo-0.1.1 (c (n "de-todo") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0zs5md6skncbk6z20klrxjii68y9y8dlqgpprjvi3ppq36crmbdi") (y #t)))

(define-public crate-de-todo-0.1.2 (c (n "de-todo") (v "0.1.2") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0zb13k9sc9wmyqv0xg366hi5sryvm09ym5w4d3gifnvlfdy6xafr")))

