(define-module (crates-io de as deasync) #:use-module (crates-io))

(define-public crate-deasync-0.1.0 (c (n "deasync") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "02m2bncykdi85rhwzk7yhk9csgsqykcq50kd8xf8sm2c3r4sf7jn") (f (quote (("default") ("bypass"))))))

