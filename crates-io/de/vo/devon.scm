(define-module (crates-io de vo devon) #:use-module (crates-io))

(define-public crate-devon-0.1.0 (c (n "devon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12yws13p2m7f7vkjs5p3h16l5q96w9k7kapv1g9m185wjh3zkbfv")))

