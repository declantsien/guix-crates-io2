(define-module (crates-io de vo devotee-backend-softbuffer) #:use-module (crates-io))

(define-public crate-devotee-backend-softbuffer-0.1.0 (c (n "devotee-backend-softbuffer") (v "0.1.0") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3.1") (d #t) (k 0)))) (h "184pyyzv3ah8pbz48xm7x6339ksbg1rhi2abx83mpbraj7y58kl5")))

(define-public crate-devotee-backend-softbuffer-0.1.1 (c (n "devotee-backend-softbuffer") (v "0.1.1") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3.1") (d #t) (k 0)))) (h "003sy5l9x8s8ckyr3nkgf6r1q7qmm3igbyx1si9x3wc617a3v9n3")))

(define-public crate-devotee-backend-softbuffer-0.2.0-beta (c (n "devotee-backend-softbuffer") (v "0.2.0-beta") (d (list (d (n "devotee-backend") (r "^0.2.0-beta") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_06"))) (k 0)))) (h "1cq49q6q4hp0lwfxd3flls573lj6xwx6q7s6912zvf4j35kdr5cq") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

(define-public crate-devotee-backend-softbuffer-0.2.0-beta.1 (c (n "devotee-backend-softbuffer") (v "0.2.0-beta.1") (d (list (d (n "devotee-backend") (r "^0.2.0-beta.1") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_06"))) (k 0)))) (h "14f3hsd9rifr29vam3p8a5lbfrvwlbhrc605i1rhp7pra9bw09mm") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

(define-public crate-devotee-backend-softbuffer-0.2.0-beta.2 (c (n "devotee-backend-softbuffer") (v "0.2.0-beta.2") (d (list (d (n "devotee-backend") (r "^0.2.0-beta.1") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_06"))) (k 0)))) (h "09cn6mz3frmfj97z1rml1ms1r7llp6rgygqr7milfszsjy1x6zwl") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

