(define-module (crates-io de vo devotee-backend) #:use-module (crates-io))

(define-public crate-devotee-backend-0.1.0 (c (n "devotee-backend") (v "0.1.0") (d (list (d (n "winit") (r "^0.28.7") (d #t) (k 0)))) (h "1kd3pjfjpalirw23vzhr4wmpm2mgn0c8953jijd4ziqk9l5gcm4w")))

(define-public crate-devotee-backend-0.2.0-beta (c (n "devotee-backend") (v "0.2.0-beta") (h "1rd5g6gcax0p3vw33l4qghpzf2hh0k0prrr9j1f3faxfmhcn096y") (f (quote (("input-context"))))))

(define-public crate-devotee-backend-0.2.0-beta.1 (c (n "devotee-backend") (v "0.2.0-beta.1") (h "1dd734mq56ws7wgb0ngaaz17j3axfmsgayy1mmx06znmdi8pw21n") (f (quote (("input-context"))))))

