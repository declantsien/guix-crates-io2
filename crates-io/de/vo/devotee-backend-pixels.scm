(define-module (crates-io de vo devotee-backend-pixels) #:use-module (crates-io))

(define-public crate-devotee-backend-pixels-0.1.0 (c (n "devotee-backend-pixels") (v "0.1.0") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1k3bvv0n7ni4iv96m171zrskpiifbbsdj942rglxmk44igwwrwmd")))

(define-public crate-devotee-backend-pixels-0.1.1 (c (n "devotee-backend-pixels") (v "0.1.1") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "08g6l98dqq15zwgrrjcc0bg2gxldzcydnlzx6bv4zzj27hfawlcl")))

(define-public crate-devotee-backend-pixels-0.1.2 (c (n "devotee-backend-pixels") (v "0.1.2") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1k6frf07rd36jbgylnpbfbdbf9haglr4vmcwz96aw6af9wxad0n0")))

(define-public crate-devotee-backend-pixels-0.1.3 (c (n "devotee-backend-pixels") (v "0.1.3") (d (list (d (n "devotee-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0nvyhlxnqwv496aw9n5s89lp5z6imckyj5g4ygv9c9d8bqh495hg")))

(define-public crate-devotee-backend-pixels-0.2.0-beta (c (n "devotee-backend-pixels") (v "0.2.0-beta") (d (list (d (n "devotee-backend") (r "^0.2.0-beta") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_05"))) (k 0)))) (h "0sza7a4csxb0jv5sr0s48ap6szbnwq5x61h9mchc0vh7awqj61wi") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

(define-public crate-devotee-backend-pixels-0.2.0-beta.1 (c (n "devotee-backend-pixels") (v "0.2.0-beta.1") (d (list (d (n "devotee-backend") (r "^0.2.0-beta.1") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_05"))) (k 0)))) (h "055bgpkzjdyhkbaaszd0x99q58pjn8df5iw35v77vy2hl424kin1") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

(define-public crate-devotee-backend-pixels-0.2.0-beta.2 (c (n "devotee-backend-pixels") (v "0.2.0-beta.2") (d (list (d (n "devotee-backend") (r "^0.2.0-beta.1") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_05"))) (k 0)))) (h "1wd5692jmayd6y0a4g7q3jcr8xfb5fngpyszxhbfh6gbrpqaz5a0") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

(define-public crate-devotee-backend-pixels-0.2.0-beta.3 (c (n "devotee-backend-pixels") (v "0.2.0-beta.3") (d (list (d (n "devotee-backend") (r "^0.2.0-beta.1") (f (quote ("input-context"))) (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.11") (f (quote ("rwh_05"))) (k 0)))) (h "1shhyz8fhipc8m8imyin7w6gggb64d0x86wlbl1sclxn4qaqa3mf") (f (quote (("x11" "winit/x11") ("wayland" "winit/wayland") ("default" "x11"))))))

