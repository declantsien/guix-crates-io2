(define-module (crates-io de vo devone) #:use-module (crates-io))

(define-public crate-devone-0.1.0 (c (n "devone") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "12g1myaihx83n8ahn8w2ang8kdy6281bwmpwpcwsdsgf8rvf7x59")))

(define-public crate-devone-0.1.1 (c (n "devone") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "06i8nf3v9schh30fw42n9krs4fl9y9r4fl78rwxihv2wr4liwyq8")))

(define-public crate-devone-0.2.0 (c (n "devone") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ipaz3b025fdgkzrypw9fchzby1mzfwmpp90y4vz3x7ksjjh0kxn")))

(define-public crate-devone-0.3.0 (c (n "devone") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cimy3ir0v0rclgq9a9ikv7z03k4bas84ycm3zc0bfiq9vva8sjd")))

