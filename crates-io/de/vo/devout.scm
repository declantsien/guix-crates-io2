(define-module (crates-io de vo devout) #:use-module (crates-io))

(define-public crate-devout-0.0.1 (c (n "devout") (v "0.0.1") (h "0qy5fd45lahn220z0rqrzshdzx303kb9byk8bc5yd925yzs7i5n5")))

(define-public crate-devout-0.1.0 (c (n "devout") (v "0.1.0") (d (list (d (n "cala_core") (r "^0.1") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0m039gi4iwqbw6axi8nzrrx9ajqdn0ws0krmhrpfw7arxs5qmkpa") (f (quote (("wasm-bindgen" "cala_core/wasm-bindgen") ("stdweb" "cala_core/stdweb") ("dev") ("default") ("cala" "cala_core/cala"))))))

(define-public crate-devout-0.2.0 (c (n "devout") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r ">=0.3.0, <0.4.0") (f (quote ("console"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "00mzqjk2krai52l0s0i7nqz6yfycjw0cflciy1cdggv6v79ldxzk")))

