(define-module (crates-io de vo devops-lib) #:use-module (crates-io))

(define-public crate-devops-lib-0.1.0 (c (n "devops-lib") (v "0.1.0") (h "03mspvama2pdwi78fg95z5ks6b3wimd7mcqg8aq1axxjr6misp1d")))

(define-public crate-devops-lib-0.2.0 (c (n "devops-lib") (v "0.2.0") (h "0149qr91irpjxqzvyli2i63fx6mwlqv900d8cdq7bgp48817gy8k")))

(define-public crate-devops-lib-1.0.0 (c (n "devops-lib") (v "1.0.0") (h "1a5ac78j0v35wdjv1dyfh1n1dawg510gx9c7c2mxlk36ybkm3lrh")))

(define-public crate-devops-lib-1.0.1 (c (n "devops-lib") (v "1.0.1") (h "0vxsmx3yyj8dlw5pc76abr2sp1d0vbvjhfm3cjk0q8qjd4mkrrqm")))

(define-public crate-devops-lib-1.0.2 (c (n "devops-lib") (v "1.0.2") (h "1g0jxhlzsjl2b0m84fxl55mzz10pxh0jw6yhgkzwa5xhg4n03jwc")))

