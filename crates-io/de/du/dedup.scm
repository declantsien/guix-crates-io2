(define-module (crates-io de du dedup) #:use-module (crates-io))

(define-public crate-dedup-0.2.0 (c (n "dedup") (v "0.2.0") (d (list (d (n "clap") (r "~2.30") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fastchr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "~2") (d #t) (k 0)) (d (n "memmap") (r "~0.6.2") (d #t) (k 0)))) (h "0qagaajws6rry70w19v8xbz17xh421z8f9z3zs5bdhsxqqhjin9q") (f (quote (("simd-accel" "fastchr") ("default" "simd-accel"))))))

