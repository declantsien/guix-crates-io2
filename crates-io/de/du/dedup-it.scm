(define-module (crates-io de du dedup-it) #:use-module (crates-io))

(define-public crate-dedup-it-0.1.0 (c (n "dedup-it") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "odht") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z4adj9j522fh5cpmpzpccdiqqhc76775vzlqaprlf6r952j4y1f")))

