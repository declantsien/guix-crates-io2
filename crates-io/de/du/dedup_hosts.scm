(define-module (crates-io de du dedup_hosts) #:use-module (crates-io))

(define-public crate-dedup_hosts-0.1.0 (c (n "dedup_hosts") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0prxlycd83alv9lkdvmh5jmmxgkqf0a37qjzmnaw1l2x4lsw3f87")))

(define-public crate-dedup_hosts-0.1.1 (c (n "dedup_hosts") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0y8k4fr6wx51wxabz4rqx0mv905pmfm3kihr2yqhz8333y4c7n5l")))

