(define-module (crates-io de du dedup-advanced) #:use-module (crates-io))

(define-public crate-dedup-advanced-1.0.0 (c (n "dedup-advanced") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "grafix-toolbox") (r "^0.7.7") (d #t) (k 0)) (d (n "hamming") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.8") (d #t) (k 0)))) (h "1xnjczda0jzrdgbxzpgzsy2kfclh29s2bwqjsd4b756131r2nk9r")))

(define-public crate-dedup-advanced-1.1.0 (c (n "dedup-advanced") (v "1.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "grafix-toolbox") (r "^0.7.7") (d #t) (k 0)) (d (n "hamming") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.8") (d #t) (k 0)))) (h "0cxzr09fw66j7wldmvm6nsrfs8nn23iz2z67rz4navwm5997znsr")))

(define-public crate-dedup-advanced-1.1.1 (c (n "dedup-advanced") (v "1.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "grafix-toolbox") (r "=0.7.7") (d #t) (k 0)) (d (n "hamming") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.8") (d #t) (k 0)))) (h "0ynnpkhfb90xkpygl918wjrxd6a5xddcr8xmkd2nqv6sl0jm500x")))

(define-public crate-dedup-advanced-1.2.0 (c (n "dedup-advanced") (v "1.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "grafix-toolbox") (r "=0.7.7") (d #t) (k 0)) (d (n "hamming") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1ip0ykbyjxglgmxkks8agfn4n47z54lxx9r8vvbbw5x99z2py1q7")))

