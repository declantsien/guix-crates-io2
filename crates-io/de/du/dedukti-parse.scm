(define-module (crates-io de du dedukti-parse) #:use-module (crates-io))

(define-public crate-dedukti-parse-0.1.0 (c (n "dedukti-parse") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)))) (h "0scdl86473zi229xxczmy0w54qkp6q499yyllzwbd4ss0llsqw3h") (f (quote (("default" "itertools"))))))

(define-public crate-dedukti-parse-0.2.0 (c (n "dedukti-parse") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)))) (h "15477v9yl7n03qlr50a1jzsk4qyp61idxgv7ffqfanda9yfxr1zx") (f (quote (("default" "itertools"))))))

(define-public crate-dedukti-parse-0.3.0 (c (n "dedukti-parse") (v "0.3.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)))) (h "02v53ajw5hnzj5pk243w6k8l2p4hj60zvip28qwc6cj281mc6m7s")))

(define-public crate-dedukti-parse-0.3.1 (c (n "dedukti-parse") (v "0.3.1") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)))) (h "1ckq5nnl17slh0gcngn1b8iwk9k1z01dnbdx6ih57hicf1kgx7b6")))

