(define-module (crates-io de du dedup_signature) #:use-module (crates-io))

(define-public crate-dedup_signature-0.1.0 (c (n "dedup_signature") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.4") (d #t) (k 2)))) (h "0bbczn0z5jncgbj5hdrkdpdbx0dhrbfjfgb5d90qbmswgcbjnxha")))

(define-public crate-dedup_signature-0.2.1 (c (n "dedup_signature") (v "0.2.1") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 2)))) (h "0897pf2jfdybaf2k653lfbnzla6zbr1p6bn8r8spdzv9z6rzyf1w")))

