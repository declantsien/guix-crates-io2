(define-module (crates-io de du deduplicating_array) #:use-module (crates-io))

(define-public crate-deduplicating_array-0.1.0 (c (n "deduplicating_array") (v "0.1.0") (d (list (d (n "postcard") (r "^0.7") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qr0hx04iqzmcdibsf9dja2xsn5vjvapbpbgxlrl1fqp2xlhzdjy") (f (quote (("std") ("bench"))))))

(define-public crate-deduplicating_array-0.1.1 (c (n "deduplicating_array") (v "0.1.1") (d (list (d (n "postcard") (r "^0.7") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n654c1d5myjzk3zb2lzg70qaml85whisd2d6j1h4xr72z3j9252") (f (quote (("std") ("bench"))))))

(define-public crate-deduplicating_array-0.1.2 (c (n "deduplicating_array") (v "0.1.2") (d (list (d (n "postcard") (r "^1.0.0-alpha.4") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dw9b1wbhwm9ydxl5yf886cg6l7jczs4r2wy6s3xmvpyn83hhzqy") (f (quote (("std") ("bench"))))))

(define-public crate-deduplicating_array-0.1.3 (c (n "deduplicating_array") (v "0.1.3") (d (list (d (n "postcard") (r "^1.0.0") (f (quote ("alloc"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nvgqzxxib8ma1pnd1z33ac8ya2794dh58qmii1magi60y5jfnhk") (f (quote (("std") ("bench"))))))

(define-public crate-deduplicating_array-0.1.4 (c (n "deduplicating_array") (v "0.1.4") (d (list (d (n "postcard") (r "^1.0.0") (f (quote ("alloc"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c8k9jvbxzgx4hsiw33ar436ciylvljhmi7jnalfq331lzl2cm0b")))

(define-public crate-deduplicating_array-0.1.5 (c (n "deduplicating_array") (v "0.1.5") (d (list (d (n "postcard") (r "^1.0.0") (f (quote ("alloc"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s2qcdcw3hcvwvim9bhdrafhh96hhjrvyx81mh83l2fahrjhjdm6") (r "1.66")))

(define-public crate-deduplicating_array-0.1.6 (c (n "deduplicating_array") (v "0.1.6") (d (list (d (n "postcard") (r "^1.0.1") (f (quote ("alloc"))) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 2)))) (h "163xkdlgvqxmbsx1abarhkxdlppz3gfq796kw9w2lq93pc6rymqx") (r "1.67")))

