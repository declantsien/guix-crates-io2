(define-module (crates-io de us deus-nqueens) #:use-module (crates-io))

(define-public crate-deus-nqueens-0.1.0 (c (n "deus-nqueens") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0w22y15jlpw1wxshq7pqkvhfi8a9l4gmpjrr2qaz3d1xb3nyzlhj")))

(define-public crate-deus-nqueens-0.2.0 (c (n "deus-nqueens") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0qisic8bssi4ai6hrbzyv1s46bfn7jw2z6gxvkbin2644sz6s3ij")))

(define-public crate-deus-nqueens-0.3.0 (c (n "deus-nqueens") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0mlj78s523ykk6mbapnh2x816n8mkvhr7fb40xvf3z1vldryb7s3")))

(define-public crate-deus-nqueens-0.4.0 (c (n "deus-nqueens") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "wolfram-expr") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1kj2h23pfha4nsdykjpa2qk03ckfsazq6sq14wy211sfxlc98mcz") (f (quote (("default"))))))

(define-public crate-deus-nqueens-0.4.1 (c (n "deus-nqueens") (v "0.4.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1xijcpm428v2478bjbl6y9jxcy90rn785f84n21fm17krlanzn5i")))

(define-public crate-deus-nqueens-0.4.2 (c (n "deus-nqueens") (v "0.4.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "13l4bb81p4fn1jagcmnghk2y4pjb5hy7jp2ijpzw9fbkizyhfglv")))

