(define-module (crates-io de vp devp2p-secp256k1) #:use-module (crates-io))

(define-public crate-devp2p-secp256k1-0.5.6 (c (n "devp2p-secp256k1") (v "0.5.6") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cq9lbsknxsbg6k4l309w1rxwl0w1a80n90q68b7w93ah9xi9jpn") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

