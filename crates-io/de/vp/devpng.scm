(define-module (crates-io de vp devpng) #:use-module (crates-io))

(define-public crate-devpng-0.1.0 (c (n "devpng") (v "0.1.0") (d (list (d (n "devker") (r "^0") (d #t) (k 0)))) (h "14nsrs87b2m2cv1d61bhyzba72nljrjx3y4s1zzk7zi0nrma99p2")))

(define-public crate-devpng-0.1.1 (c (n "devpng") (v "0.1.1") (d (list (d (n "devker") (r "^0") (d #t) (k 0)))) (h "0pikb84ggzzlhigba7zbnly7ny5zwv9hmj1mwmw8nd5wdpp96afw")))

(define-public crate-devpng-0.1.2 (c (n "devpng") (v "0.1.2") (d (list (d (n "devker") (r "^0") (d #t) (k 0)))) (h "0fsyvr3m3swngma4k8nji8akw9184k57mzb9nwxsfk37lb8fr8iy")))

