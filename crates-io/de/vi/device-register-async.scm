(define-module (crates-io de vi device-register-async) #:use-module (crates-io))

(define-public crate-device-register-async-0.1.0 (c (n "device-register-async") (v "0.1.0") (d (list (d (n "device-register") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1jsqxm18vgznwyhh1ylhypsrw822lk1i2zgbdyzrsgkyx7jzibix")))

(define-public crate-device-register-async-0.1.1 (c (n "device-register-async") (v "0.1.1") (d (list (d (n "device-register") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "07m1f7552jx63s4ndzzr2xj2kfgn6avv2mvg3nzb0sd7mk3nxg47")))

(define-public crate-device-register-async-0.2.0 (c (n "device-register-async") (v "0.2.0") (d (list (d (n "device-register") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0ppnbcv0kb0vr59p8r6sfz38ly5xwn758yab6xsipdxd3fim701q")))

(define-public crate-device-register-async-0.3.0 (c (n "device-register-async") (v "0.3.0") (d (list (d (n "device-register") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1jp28h2i03ipa54zszkn73xmm90wxfyj8y9sxdhc7clxmd6h5fsa")))

(define-public crate-device-register-async-0.3.1 (c (n "device-register-async") (v "0.3.1") (d (list (d (n "device-register") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.23") (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0z6lh2d6w892f6gfm66mgvlwynipkjhhpjb8lahasmid6422320v")))

(define-public crate-device-register-async-0.4.0 (c (n "device-register-async") (v "0.4.0") (d (list (d (n "device-register") (r "^0.4.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "090266jabfz51cfpqanlqqllqqhbpcxyyyzjgnlqn2cfh1d286j3")))

