(define-module (crates-io de vi device-register) #:use-module (crates-io))

(define-public crate-device-register-0.1.0 (c (n "device-register") (v "0.1.0") (d (list (d (n "device-register-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)))) (h "1bsfa1zf3wsf8cwri9ajjasvixz8mhm4na2qqmyvmbb32n7lyhs6")))

(define-public crate-device-register-0.1.1 (c (n "device-register") (v "0.1.1") (d (list (d (n "device-register-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)))) (h "1v5p3180a3xpyqa6p2clsh9yd0alrnxmkhghy4gyi6if27ijlqbw")))

(define-public crate-device-register-0.1.2 (c (n "device-register") (v "0.1.2") (d (list (d (n "device-register-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0vrl8nwqg3x6g85yi3rwb930wz2bdmzvj5aj5bvhaxfkf535xmlp")))

(define-public crate-device-register-0.2.0 (c (n "device-register") (v "0.2.0") (d (list (d (n "device-register-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1lklsqigfzn61fpb828r0xgv9nl9wih4myfhqwjc700vlrs1iwbm")))

(define-public crate-device-register-0.3.0 (c (n "device-register") (v "0.3.0") (d (list (d (n "device-register-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0crcpbjy81yqdmcjwc9xss6y9im4xlpi82l5f0aqsxmi99fsqcl3")))

(define-public crate-device-register-0.3.1 (c (n "device-register") (v "0.3.1") (d (list (d (n "device-register-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1k4pspbr5zb1ryn6mq2lvz0imiaa19n8zj9bz6mz63zm7swybwlh")))

(define-public crate-device-register-0.4.0 (c (n "device-register") (v "0.4.0") (d (list (d (n "device-register-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "17xlds4lmrhnlz6jm02lc9rcdl87aq06sq3s2zy1bhaaba1vyyip")))

