(define-module (crates-io de vi device_status) #:use-module (crates-io))

(define-public crate-device_status-0.1.1 (c (n "device_status") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "moebius-tools") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1cw4l5i1xhrfzzp5c9sdx6g8m80ggyp445x4ghbzhgjar1sf33bp")))

(define-public crate-device_status-0.1.2 (c (n "device_status") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "moebius-tools") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "100gqrhngav0c9ln73108k8jjfmjsi35y4v91clbf143s1rvmhj4")))

