(define-module (crates-io de vi devise) #:use-module (crates-io))

(define-public crate-devise-0.1.0 (c (n "devise") (v "0.1.0") (d (list (d (n "devise_codegen") (r "^0.1") (d #t) (k 0)) (d (n "devise_core") (r "^0.1") (d #t) (k 0)))) (h "0fq8pzd261whcmfmzla15kmhzk6855yzi97nm2kmgddf0437x3jg")))

(define-public crate-devise-0.2.0 (c (n "devise") (v "0.2.0") (d (list (d (n "devise_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "devise_core") (r "^0.2.0") (d #t) (k 0)))) (h "1lryvr39ia3rfswfnwn2zynsv2r8kj6gqqf0akcs0prws2i4pq3l")))

(define-public crate-devise-0.3.0 (c (n "devise") (v "0.3.0") (d (list (d (n "devise_codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "devise_core") (r "^0.3.0") (d #t) (k 0)))) (h "0ci80i5522vrpf01f5yv8sw0f9n1kmj1d5k8g4vdy04gqddg8721")))

(define-public crate-devise-0.3.1 (c (n "devise") (v "0.3.1") (d (list (d (n "devise_codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "devise_core") (r "^0.3.1") (d #t) (k 0)))) (h "15dmibnykic2a1ndi66shyvxmpfysnhf05lg2iv8871g0w5miish")))

(define-public crate-devise-0.2.1 (c (n "devise") (v "0.2.1") (d (list (d (n "devise_codegen") (r "^0.2.1") (d #t) (k 0)) (d (n "devise_core") (r "^0.2.1") (d #t) (k 0)))) (h "09p52f54givb0g9l7clj11z755vldk8758y2lwm5mp3sa156qwfx")))

(define-public crate-devise-0.4.0 (c (n "devise") (v "0.4.0") (d (list (d (n "devise_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "devise_core") (r "^0.4.0") (d #t) (k 0)))) (h "199rjawamggv2a55qjfggvkbhfkbzjdf0j276nirmj8l41ywxj6m")))

(define-public crate-devise-0.4.1 (c (n "devise") (v "0.4.1") (d (list (d (n "devise_codegen") (r "^0.4.1") (d #t) (k 0)) (d (n "devise_core") (r "^0.4.1") (d #t) (k 0)))) (h "1y45iag4hyvspkdsf6d856hf0ihf9vjnaga3c7y6c72l7zywxsnn")))

