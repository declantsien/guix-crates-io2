(define-module (crates-io de vi device-register-macro) #:use-module (crates-io))

(define-public crate-device-register-macro-0.1.0 (c (n "device-register-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qmhcjx1fci747760ymsz76b61h4dph2z7n2szgyagl44c98z04d")))

(define-public crate-device-register-macro-0.1.1 (c (n "device-register-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wbb28n45cgnjwvwqy90b2vwnq7n5xwdaghbgv9ldvgdqnn1qd3n")))

(define-public crate-device-register-macro-0.2.0 (c (n "device-register-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08ybxg8d1x6isy6ahdxg1ykgpzhxi5qi1hhms0pgvcc10y2w8sll")))

(define-public crate-device-register-macro-0.3.0 (c (n "device-register-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jl070c8q3dl5i337skbkl0ylb385p4jpfqc4k0fimd1j7rnmwr4")))

(define-public crate-device-register-macro-0.4.0 (c (n "device-register-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "177chja9ix49xyndn6z7yfxljgrvgk99g4vn579fbhpbrx7i4ah9")))

