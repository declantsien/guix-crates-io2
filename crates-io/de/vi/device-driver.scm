(define-module (crates-io de vi device-driver) #:use-module (crates-io))

(define-public crate-device-driver-0.1.0 (c (n "device-driver") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19.2") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "0kdp8gmh4xcflwc73b9xilfng4lg2x6qxag0xp5hza4gs3g45kj1")))

(define-public crate-device-driver-0.1.1 (c (n "device-driver") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.20.1") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "1ajwpc2bj42m7wbs3gri05i48c34s41wz6ldka0lqy1jd4nn3ffd")))

(define-public crate-device-driver-0.1.2 (c (n "device-driver") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.20.1") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "0w3w8mkyqqpvk9fyh79vaik30bib74q6aqfipsb6i3hck1g488a1")))

(define-public crate-device-driver-0.2.0 (c (n "device-driver") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.20.1") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "1jylbfw2yljjycxp6nnpkbssl0i0470wm8zmxk3bchfxz2vyz7d3")))

(define-public crate-device-driver-0.3.0 (c (n "device-driver") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.20.1") (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "1f9377n45g0bdg08hwchlhl5b4y85d2gpdyczb3j8idldwy7ylmq")))

(define-public crate-device-driver-0.3.1 (c (n "device-driver") (v "0.3.1") (d (list (d (n "bitvec") (r "^0.20.1") (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)))) (h "0ar65ih2cim3xwywhb55w269lb3fyr1skyi7jvfqm9pbwv8cnvqy")))

(define-public crate-device-driver-0.4.0 (c (n "device-driver") (v "0.4.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 2)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "13fr7iyrczll8vx8x9gfmhyd4sqg95hswrbw3s3qk2zlwynmm6kc") (f (quote (("default" "async") ("async")))) (y #t)))

(define-public crate-device-driver-0.4.1 (c (n "device-driver") (v "0.4.1") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 2)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0gavjw3fy780nn8kjvv9872mq70cfxp3i9h6dy19syxc4x73djp0") (f (quote (("default") ("async"))))))

(define-public crate-device-driver-0.5.0 (c (n "device-driver") (v "0.5.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "device-driver-generation") (r "^0.5.0") (d #t) (k 0)) (d (n "device-driver-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "num_enum") (r "^0.7.1") (k 0)))) (h "11zb7259z9nx88g434whpkxy2l2g68zchxgi6nvqy90x1s45vvvx") (f (quote (("yaml" "device-driver-macros/yaml") ("json" "device-driver-macros/json"))))))

(define-public crate-device-driver-0.5.1 (c (n "device-driver") (v "0.5.1") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "device-driver-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "num_enum") (r "^0.7.1") (k 0)))) (h "05hqz0zh3a04vpxkpwgkd9n6z6s5i9ayyblknxhfq00fw7y1jyxw") (f (quote (("yaml" "device-driver-macros/yaml") ("json" "device-driver-macros/json"))))))

(define-public crate-device-driver-0.5.2 (c (n "device-driver") (v "0.5.2") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "device-driver-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "num_enum") (r "^0.7.1") (k 0)))) (h "0n51f1fd95fagva48h45rcmwc0j91mfra71rrilly70zqws1fq5c") (f (quote (("yaml" "device-driver-macros/yaml") ("json" "device-driver-macros/json"))))))

(define-public crate-device-driver-0.5.3 (c (n "device-driver") (v "0.5.3") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "device-driver-macros") (r "^0.5.3") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "num_enum") (r "^0.7.1") (k 0)))) (h "13sspwx0aqgaxlwxkriki01qxrdv8kxlri7y4viq35vv22494bkv") (f (quote (("yaml" "device-driver-macros/yaml") ("json" "device-driver-macros/json"))))))

(define-public crate-device-driver-0.6.0 (c (n "device-driver") (v "0.6.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)) (d (n "device-driver-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "num_enum") (r "^0.7.1") (k 0)))) (h "16ahlil30fnlcbykhpnd39g8m8r8dxr11h1nr73r3kzcfpnx8qir") (f (quote (("yaml" "device-driver-macros/yaml") ("json" "device-driver-macros/json"))))))

