(define-module (crates-io de vi devicetree-tool) #:use-module (crates-io))

(define-public crate-devicetree-tool-0.1.0 (c (n "devicetree-tool") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)))) (h "1dkj9xnwngfncv12kj4i5l5800c4fci9g5vm7jf59r978fjyih5s")))

(define-public crate-devicetree-tool-0.1.1 (c (n "devicetree-tool") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)))) (h "1m5hl1fzrcfhlxnmnxsylgxcla9v8d106hyd10g13mrnhhdr8kxz")))

(define-public crate-devicetree-tool-0.1.2 (c (n "devicetree-tool") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)))) (h "1ywlm58xk9q3qg7sirx6rz12mvj109s50p7biq4w3h6gy4arw2zf")))

