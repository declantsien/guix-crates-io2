(define-module (crates-io de vi devise_codegen) #:use-module (crates-io))

(define-public crate-devise_codegen-0.1.0 (c (n "devise_codegen") (v "0.1.0") (d (list (d (n "devise_core") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0dml6s9ar1nh412227g30k55gx2fllsyml4qypmgnb9wzm72b22c")))

(define-public crate-devise_codegen-0.2.0 (c (n "devise_codegen") (v "0.2.0") (d (list (d (n "devise_core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1msmq0h19l03n9qmkxxi1a5h8904i8m623kdvjzak4ya51wynv06")))

(define-public crate-devise_codegen-0.3.0 (c (n "devise_codegen") (v "0.3.0") (d (list (d (n "devise_core") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1l9a8qqqd127yy7plzcprhrghb0k4r23anckwa3ifbw20qghixrc")))

(define-public crate-devise_codegen-0.3.1 (c (n "devise_codegen") (v "0.3.1") (d (list (d (n "devise_core") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1cp7nnfwvjp6wfq11n0ffjjrwfa1wbsb58g1bz3ha6z5lvkp6g0j")))

(define-public crate-devise_codegen-0.2.1 (c (n "devise_codegen") (v "0.2.1") (d (list (d (n "devise_core") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0lxbixni2v6snx2mkgi0kyq5dv8v6c5s57b6wc47q4hqs6884yza")))

(define-public crate-devise_codegen-0.4.0 (c (n "devise_codegen") (v "0.4.0") (d (list (d (n "devise_core") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0lv3mya8cnp8c5xsg2c6pjjl6c9a6cl0zjx9cb4y4gc01wssb3aa")))

(define-public crate-devise_codegen-0.4.1 (c (n "devise_codegen") (v "0.4.1") (d (list (d (n "devise_core") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1mpy5mmsigkj5f72gby82yk4advcqj97am2wzn0dwkj8vnwg934w")))

