(define-module (crates-io de vi deviz) #:use-module (crates-io))

(define-public crate-deviz-0.1.0 (c (n "deviz") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1hdijl6likbs3sid18pkpvhsi3p7fkmw41b16l5wdxg3vrbkc08h")))

(define-public crate-deviz-0.1.1 (c (n "deviz") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "14cnir8rkc3hymixkgl3z8kvy0y032kg2xjhgx3sj19jrycdhnhx")))

(define-public crate-deviz-0.1.2 (c (n "deviz") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "0hi2plrp27yj5mfjvfrz94mhiyrvn3r1rv7h8m23r3qjpdwfbi22")))

(define-public crate-deviz-0.1.3 (c (n "deviz") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0bp75gacs6vi3zcci0wfzj6q0y3db80hk8mhhdlbw43qgq7bzp46")))

