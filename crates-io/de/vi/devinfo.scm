(define-module (crates-io de vi devinfo) #:use-module (crates-io))

(define-public crate-devinfo-0.1.0 (c (n "devinfo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdevinfo-sys") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)))) (h "065lzr5inmixi4cqji7mp7wczs3aa48szp7ks36g48jv5ylrdd9g") (f (quote (("private" "libdevinfo-sys/private") ("default"))))))

