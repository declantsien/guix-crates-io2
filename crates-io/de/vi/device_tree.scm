(define-module (crates-io de vi device_tree) #:use-module (crates-io))

(define-public crate-device_tree-1.0.0 (c (n "device_tree") (v "1.0.0") (h "191mdmg993pxjfswiw8973alsj2cpryvq8vfnlzwrcys2075v7d8")))

(define-public crate-device_tree-1.0.1 (c (n "device_tree") (v "1.0.1") (h "0i1ckg9wd8b21hb7x7iqs0r1yblzls7ihnm77nbbwk529mwk1542")))

(define-public crate-device_tree-1.0.3 (c (n "device_tree") (v "1.0.3") (h "17ff7xsxaxvs522pvy8kb5p6msixh3xn1s5s6gds7xgsdqf3sy27")))

(define-public crate-device_tree-1.1.0 (c (n "device_tree") (v "1.1.0") (h "0g1mjn2x6hfk1zdw41whs7b5l93ps3mwqk5nzs1k8bkwbiy733zi")))

