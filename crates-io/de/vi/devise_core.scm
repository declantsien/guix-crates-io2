(define-module (crates-io de vi devise_core) #:use-module (crates-io))

(define-public crate-devise_core-0.1.0 (c (n "devise_core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jmwj57nr3grdlyr0lpmq5xxwc87m8irgxjmyznkqwck0p06jdp3")))

(define-public crate-devise_core-0.2.0 (c (n "devise_core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11v4z5kljmpiyif3mmfnm3rl1lsqygjlfy2wll7frqxm4adwahfg")))

(define-public crate-devise_core-0.3.0 (c (n "devise_core") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0ir18k2pmps7bgl7h27r5jvh2h3b55zq93z0gifqfdl7flqkdhl0")))

(define-public crate-devise_core-0.3.1 (c (n "devise_core") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1l00qiih4z14ai0c3s16nlvw0kv4p07ygi6a0ms0knc78xpz87l4")))

(define-public crate-devise_core-0.2.1 (c (n "devise_core") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wr3jdzzibpafz73hcca83wnzdgjinvm7axmxnyfkbasbnfkw1fi")))

(define-public crate-devise_core-0.4.0 (c (n "devise_core") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "129a2lmrdx3xpzg34mfsdcb593gz4971mmwgpwx0xa41xmv4pb2i")))

(define-public crate-devise_core-0.4.1 (c (n "devise_core") (v "0.4.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0sp5idq0idng9i5kwjd8slvc724s97r28arrhyqq1jpx1ax0vd9m")))

