(define-module (crates-io de vi devicemapper-sys) #:use-module (crates-io))

(define-public crate-devicemapper-sys-0.1.0 (c (n "devicemapper-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.22") (d #t) (k 0)))) (h "0wp9pzynxrqkysasmhp6v199vlp2g967xg5s0pzb5d11i3pi514j")))

(define-public crate-devicemapper-sys-0.1.1 (c (n "devicemapper-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.22") (d #t) (k 0)))) (h "03i5s901zzb8yrvqyamxcgnslbh4pcq2i080v758rvzizglqfjnm")))

(define-public crate-devicemapper-sys-0.1.2 (c (n "devicemapper-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "1pf1d0ybva4k2vzwybxf6aqmfdl3kwdgr15aj8bj5ci38xzd7jrb")))

(define-public crate-devicemapper-sys-0.1.3 (c (n "devicemapper-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)))) (h "11c2qig241in8cix57gi6nynr5i2f5rwb663z0bs5j3bmrdssald")))

(define-public crate-devicemapper-sys-0.1.4 (c (n "devicemapper-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.26.0") (d #t) (k 0)))) (h "0679r06hpca4pvq37n02pbb3chczvxjgvqp2bvanrgygj8ygwy01") (r "1.66.1")))

(define-public crate-devicemapper-sys-0.1.5 (c (n "devicemapper-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "nix") (r "^0.26.0") (d #t) (k 0)))) (h "0k5wpkkx4dfrlxx9xww45hfmdi1kfc0nkdwhdsp31y30cp8zkc7h") (r "1.66.1")))

(define-public crate-devicemapper-sys-0.2.0 (c (n "devicemapper-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)))) (h "0yrn52r4769599fm1gvb16xi22wqar7xv9wy8fb16mbb5r6vlkvk") (r "1.71.1")))

(define-public crate-devicemapper-sys-0.3.0 (c (n "devicemapper-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)))) (h "1d9lrcl0m3p51zi5w287kxbw0j7mk617pac10d49881y87sq2rix") (r "1.71.1")))

