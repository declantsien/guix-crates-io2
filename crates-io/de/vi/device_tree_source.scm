(define-module (crates-io de vi device_tree_source) #:use-module (crates-io))

(define-public crate-device_tree_source-0.1.0 (c (n "device_tree_source") (v "0.1.0") (d (list (d (n "nom") (r "^2.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "17z6fn88zc248zv79z394cn8x368p9m4mwn140ih75xvaj9fddar")))

(define-public crate-device_tree_source-0.2.0 (c (n "device_tree_source") (v "0.2.0") (d (list (d (n "mktemp") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^3.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "rustc-test") (r "^0.1.4") (d #t) (k 2)) (d (n "walkdir") (r "^1") (d #t) (k 2)))) (h "0mj35mi7g2ijc8f55ynwxj8dpc73pwzxhy8zhzzbh56jc1ir8ija")))

