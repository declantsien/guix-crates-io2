(define-module (crates-io de vi devicetree) #:use-module (crates-io))

(define-public crate-devicetree-0.1.0 (c (n "devicetree") (v "0.1.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y5n3bqzf7ildn11msgqn1gx3kh4x4pqk4zbnzdpzazgffswp7x0")))

(define-public crate-devicetree-0.1.1 (c (n "devicetree") (v "0.1.1") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hfzdm1mdm7q96zg3jsslzvafd94ng7bzy4jxmmrgy1an7j3q0nh")))

