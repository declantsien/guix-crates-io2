(define-module (crates-io de ob deobfuscator) #:use-module (crates-io))

(define-public crate-deobfuscator-0.1.0 (c (n "deobfuscator") (v "0.1.0") (d (list (d (n "base85") (r "^1.1.1") (d #t) (k 0)))) (h "1m2iqqli54zbfj9k2rgxr6gfbiqiqi6chypa83sr07dss9d2p9bj")))

(define-public crate-deobfuscator-0.1.1 (c (n "deobfuscator") (v "0.1.1") (d (list (d (n "base85") (r "^1.1.1") (d #t) (k 0)))) (h "0jppb464gc6hn9rp1ilmsm55bh4s6cgr44rm5dv5shyav74c4cvc")))

