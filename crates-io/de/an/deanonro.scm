(define-module (crates-io de an deanonro) #:use-module (crates-io))

(define-public crate-deanonro-0.1.0 (c (n "deanonro") (v "0.1.0") (d (list (d (n "ark-bn254") (r "^0.4") (d #t) (k 0)) (d (n "ark-ec") (r "^0.4") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4") (d #t) (k 0)) (d (n "bech32") (r "^0.11") (d #t) (k 0)) (d (n "cbor4ii") (r "^0.3") (f (quote ("use_alloc"))) (d #t) (k 0)) (d (n "chacha20") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vj5p8hypysvh4pc2bc05ymfpvaa9gifs4ix1krwqz0h7v49viv1")))

