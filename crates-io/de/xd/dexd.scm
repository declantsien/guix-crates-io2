(define-module (crates-io de xd dexd) #:use-module (crates-io))

(define-public crate-dexd-0.1.0 (c (n "dexd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0bjh680kxg26pza2rqcn24fyfjlmvh444grk19w6x5b8bks18cwh")))

