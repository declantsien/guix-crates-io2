(define-module (crates-io ul n2 uln2003) #:use-module (crates-io))

(define-public crate-uln2003-0.1.0 (c (n "uln2003") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0479fnd8c597gd1dxb2jpjvva9nfmsz28v12s56b72ci390pydai")))

(define-public crate-uln2003-0.1.1 (c (n "uln2003") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "13pr08slmprrnpflmi3xd2md37cpcniag26djlz88qfvqfih2bwn")))

(define-public crate-uln2003-0.2.0 (c (n "uln2003") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1qpqg4lxfwfgfd5kqkaqhskw5fpfxsg60j8psa42l3c9xvv7nxlw")))

(define-public crate-uln2003-0.2.1 (c (n "uln2003") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0fhqffm89hrfsm14fp2kn0skhjj1fh6wq71pdj8f3yc7sj569jww")))

