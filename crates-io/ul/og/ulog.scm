(define-module (crates-io ul og ulog) #:use-module (crates-io))

(define-public crate-ulog-0.1.0 (c (n "ulog") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "19zq5gvgyr6w3ds4257fckg7749bk84qhraf7dvj8700j72vh9za")))

(define-public crate-ulog-0.1.1 (c (n "ulog") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "09pyfis3ik3j45xb3rykr8li73mfzrkqhns9lwd7qmvd2xv2yp9p")))

(define-public crate-ulog-0.1.2 (c (n "ulog") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "07sf2pmvkb7m8snx04b6kyppbvjf18g63cqvhf718hiycb92k69i")))

(define-public crate-ulog-0.2.0 (c (n "ulog") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "1prq8ly6a73aggm3w2z070mkglwpsgx4nimqjbhx20kbgdf0h4dq")))

(define-public crate-ulog-0.3.0 (c (n "ulog") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_local") (r "^0.3") (d #t) (k 0)))) (h "07w61w9w6z7r0wl0m2a4vqgnq8sx9wwhbnkbdssqh0s55ylr2pdi")))

