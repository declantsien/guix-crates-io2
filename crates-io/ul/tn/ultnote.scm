(define-module (crates-io ul tn ultnote) #:use-module (crates-io))

(define-public crate-ultnote-0.0.1 (c (n "ultnote") (v "0.0.1") (h "0i4w3s1ax7m6d815nf2qp73d2vsanapik2yfwl91dfx7bims94r7")))

(define-public crate-ultnote-0.0.20 (c (n "ultnote") (v "0.0.20") (h "1myppz7fa6k50pzndvlgldfhrnm40ssshrzv2b1b9kz14j3iscif")))

