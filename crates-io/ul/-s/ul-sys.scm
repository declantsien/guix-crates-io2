(define-module (crates-io ul -s ul-sys) #:use-module (crates-io))

(define-public crate-ul-sys-0.1.0-alpha (c (n "ul-sys") (v "0.1.0-alpha") (h "1rw5w0lzlhdis0vqccqk05qj21sfh2m9p548nw6a05p382kbd2wq")))

(define-public crate-ul-sys-0.1.0-rc1 (c (n "ul-sys") (v "0.1.0-rc1") (h "1insisgpafa3qjzd002cd5yg840ypb4zvjz9s3qwjdwb9x3j763m")))

(define-public crate-ul-sys-0.1.0-rc1-2 (c (n "ul-sys") (v "0.1.0-rc1-2") (h "1bffrk616nwvfp3x42fcry2lq1x2pxfjkhchcfsvm4jg2p36vph2")))

(define-public crate-ul-sys-0.1.0-rc2 (c (n "ul-sys") (v "0.1.0-rc2") (h "0srzy5bh6189zhcipz6mjjcfjkgh8dap43ybgdyq11ffx797g8dq")))

(define-public crate-ul-sys-1.0.0 (c (n "ul-sys") (v "1.0.0") (h "0rqiha9g37h56bas92q2fvcizplvjvc0f04m4i9gb7aivnm9xg1i")))

(define-public crate-ul-sys-1.0.1 (c (n "ul-sys") (v "1.0.1") (h "0j87nm69hb1qzb639rx0hz529jdjn3zmh01sh6dqjdsvfqdksvsk")))

(define-public crate-ul-sys-1.1.0 (c (n "ul-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "11wpbrqp2dm27zil580hc7169vi2vvba0mpjm6m131r5ldrjic2k")))

(define-public crate-ul-sys-1.1.1 (c (n "ul-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1b8mxmm4k05cddk8qh6ivmbh1na7gjs9dmvnnw512i4fkryb33x8")))

(define-public crate-ul-sys-1.1.2 (c (n "ul-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "18ax0vl6plk45xwd1nwhzkb3c248z7ymgp6k90xrr0xx3l1mcbf9")))

(define-public crate-ul-sys-1.2.0 (c (n "ul-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "0lh7kdblvzwlrklp4l2sx9afjr3d4l01fzwikrnn3m5jlmx5qv0g")))

(define-public crate-ul-sys-1.2.1 (c (n "ul-sys") (v "1.2.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "02lmqxnbnqfxafcchy7i3w5nhw7vvl7f4nf6zlam8p28wnabh09r")))

(define-public crate-ul-sys-1.2.2 (c (n "ul-sys") (v "1.2.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1avk2zqpacx2fs7vbgb5v00r76j6gkx6c9qsksklzfq0m98mcnrb")))

(define-public crate-ul-sys-1.3.0 (c (n "ul-sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)))) (h "1ka3p4m18h4cifxpas65qmp05x53s4pc1vsnbwzjwmcaf15z2pi0")))

(define-public crate-ul-sys-1.3.1 (c (n "ul-sys") (v "1.3.1") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)))) (h "0yn9bjmsndbi695m42r52m18908nkcf735j9s9x11639qkndmj03")))

