(define-module (crates-io ul tr ultron-core) #:use-module (crates-io))

(define-public crate-ultron-core-0.3.0 (c (n "ultron-core") (v "0.3.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "ultron-syntaxes-themes") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1avjs7lagf8wdjyyph75b1m60ppzdicvqqp23valm5fmq4kfqv2c") (f (quote (("callback"))))))

(define-public crate-ultron-core-0.4.0 (c (n "ultron-core") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "ultron-syntaxes-themes") (r "^0.4.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0pxsgbrgy83r0d7pvxhmy4nfb1mwaq96k2pxqgjqhainyl64l2c9") (f (quote (("callback"))))))

