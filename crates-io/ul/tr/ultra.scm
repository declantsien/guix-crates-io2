(define-module (crates-io ul tr ultra) #:use-module (crates-io))

(define-public crate-ultra-0.1.0 (c (n "ultra") (v "0.1.0") (d (list (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1zjhcnzrf5i9jwyyk0x9xnvxxf568yhahacpjcfkfhrzcw5bcccq")))

(define-public crate-ultra-0.2.0 (c (n "ultra") (v "0.2.0") (d (list (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1178imr1xfka6dkx9qrrc9vp9yl4ghmn5d63yl9v72lxqancm392")))

(define-public crate-ultra-0.3.0 (c (n "ultra") (v "0.3.0") (d (list (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16yk1yzjyirwnb7n5w2b6aivxdar6lyg4h46p6k9l3wrda6vk2lc")))

(define-public crate-ultra-0.3.1 (c (n "ultra") (v "0.3.1") (d (list (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13kaki1byhbcxl3ri536z7z1dki9mn48p5jh16l8jb6psrz6aigr")))

(define-public crate-ultra-0.4.0 (c (n "ultra") (v "0.4.0") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.6") (d #t) (k 0)))) (h "0i62plmsxkqv6cqm902w45wapg4jhdhag5pd3bgc8ilhk12m3ckv")))

(define-public crate-ultra-0.5.0 (c (n "ultra") (v "0.5.0") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.6") (d #t) (k 0)))) (h "0aalv7m57ndbs6grzm50d20amqh0wljydmh0zflfxic40j2vhg9m")))

(define-public crate-ultra-0.6.0 (c (n "ultra") (v "0.6.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0jjzrvfgc6a914nmff36hkzk7chm47m5gand4zhiq4kwdasg3004")))

(define-public crate-ultra-0.6.1 (c (n "ultra") (v "0.6.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1bg9c2avlnxzv48vs2nv9fbnalbifl2p5pbgcwn38ni0sji2n5nc")))

