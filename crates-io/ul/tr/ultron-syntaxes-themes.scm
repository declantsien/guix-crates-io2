(define-module (crates-io ul tr ultron-syntaxes-themes) #:use-module (crates-io))

(define-public crate-ultron-syntaxes-themes-0.1.0 (c (n "ultron-syntaxes-themes") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "0w97hjzzf41jg2lbcd85n3hxhjy1vf0dfm4j6mszplhgpijg1p9h")))

(define-public crate-ultron-syntaxes-themes-0.1.2 (c (n "ultron-syntaxes-themes") (v "0.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "1q015naaz9b0smza81rxdfijy1bwqclkxmf35dxp4cw78d3a9vzp")))

(define-public crate-ultron-syntaxes-themes-0.2.0 (c (n "ultron-syntaxes-themes") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "0p67ln4a8gjvd4gjjh9p59nyvxacrsbqib6mxg8m1nas59wi6ng9")))

(define-public crate-ultron-syntaxes-themes-0.2.1 (c (n "ultron-syntaxes-themes") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "0bq2m9waq4izwfyimi2hw815kiabszzzfzwfs2iqfc57h3cnd5qi")))

(define-public crate-ultron-syntaxes-themes-0.2.3 (c (n "ultron-syntaxes-themes") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "1f3j36jabnnfxs8mnw54ncpzq911m3wk6spzlb10kciwd0baxyr5")))

(define-public crate-ultron-syntaxes-themes-0.2.4 (c (n "ultron-syntaxes-themes") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "13y2z95l9k3gap0i1sikcmi8ivzmj9d7rrsk6d2anyy5k24ydm68")))

(define-public crate-ultron-syntaxes-themes-0.2.5 (c (n "ultron-syntaxes-themes") (v "0.2.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "02xls6pgcrna6wqr0ans4m1a0smqrc2k5j4bwkimyh60qj1qk64s")))

(define-public crate-ultron-syntaxes-themes-0.2.6 (c (n "ultron-syntaxes-themes") (v "0.2.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^4") (f (quote ("default-fancy"))) (k 0)))) (h "181r8h2mdkwahs9x2h11g872rkg7frcm1qdhvbm6yk1drfzimzzj")))

(define-public crate-ultron-syntaxes-themes-0.3.0 (c (n "ultron-syntaxes-themes") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^5") (f (quote ("default-fancy"))) (k 0)))) (h "1jfmjjr5rngwslni87nnbvdgj8r9g96p77qpj2s83m2vis0yljk5")))

(define-public crate-ultron-syntaxes-themes-0.4.0 (c (n "ultron-syntaxes-themes") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^5") (f (quote ("default-fancy"))) (k 0)))) (h "1vssx1zw9w5i20agccpkz8l2pn5rsvf5d7mfk8hl3ri22gii5188")))

