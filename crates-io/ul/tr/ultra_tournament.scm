(define-module (crates-io ul tr ultra_tournament) #:use-module (crates-io))

(define-public crate-ultra_tournament-0.1.0 (c (n "ultra_tournament") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "enum-map") (r "^0.6.2") (d #t) (k 0)) (d (n "enum-map-derive") (r "^0.4.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.43") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (f (quote ("petgraph"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "specs") (r "^0.16.1") (d #t) (k 0)))) (h "0jkh0l7gagkhv29hmwlfqznm5amswshblvh5h2c6bvc8pgzffw86")))

