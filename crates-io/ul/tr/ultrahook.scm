(define-module (crates-io ul tr ultrahook) #:use-module (crates-io))

(define-public crate-ultrahook-0.1.0 (c (n "ultrahook") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "05g5qbl4z6jp884r9swy6cpxdlfjyzhivwifcmi7a54hmaa7c03s")))

(define-public crate-ultrahook-0.1.1 (c (n "ultrahook") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0xs069nwap6xzdnjpnxpsmhcbyyrcs7qvxj67ak34nby6q9d5058")))

(define-public crate-ultrahook-0.1.2 (c (n "ultrahook") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0g17gx8xaf9afz78g6zxxrshx3kd9pdgj719d80b7iq6vix2k8lc")))

