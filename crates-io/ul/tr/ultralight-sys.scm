(define-module (crates-io ul tr ultralight-sys) #:use-module (crates-io))

(define-public crate-ultralight-sys-0.1.0 (c (n "ultralight-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 1)) (d (n "ultralight-build") (r "^0.1.1") (d #t) (k 1)))) (h "112pz159740ci9y4sx3afibzp205g324iflgk7riapzydnvqh5ci")))

(define-public crate-ultralight-sys-0.1.1 (c (n "ultralight-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 1)) (d (n "ultralight-build") (r "^0.1.4") (d #t) (k 1)))) (h "0mx93yhpaphzxhq9yhcvqywpy7vhrb2sgam27xlxaxn1yr38gspy")))

