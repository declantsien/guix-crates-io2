(define-module (crates-io ul tr ultralight-build) #:use-module (crates-io))

(define-public crate-ultralight-build-0.1.0 (c (n "ultralight-build") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 0)))) (h "04k5308ljjykp3zz06r2hr4c4djj8xhn4l9qg228hlidvjqf8s7i")))

(define-public crate-ultralight-build-0.1.1 (c (n "ultralight-build") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 0)))) (h "0hpjllblxzcj23qpsnfvf21f37yn5wd365kcrknvhk6fnjx88cf1")))

(define-public crate-ultralight-build-0.1.2 (c (n "ultralight-build") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 0)))) (h "1b6nbz2k1fdknbmp5m3py2skcqnwf2xpncipq2wqhf7dsc7w6cq1")))

(define-public crate-ultralight-build-0.1.3 (c (n "ultralight-build") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 0)))) (h "03s5si4abfsq0fh16gkry3qj26lj0xaf82w2rnv2ihvildx4nppz")))

(define-public crate-ultralight-build-0.1.4 (c (n "ultralight-build") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.3") (d #t) (k 0)))) (h "15xi1g7q7rdpgsvkizl5yb5f8jvs0g9x3q9y4kf8y3pcf2gh7qw3")))

