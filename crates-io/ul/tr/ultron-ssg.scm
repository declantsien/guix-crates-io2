(define-module (crates-io ul tr ultron-ssg) #:use-module (crates-io))

(define-public crate-ultron-ssg-0.1.0 (c (n "ultron-ssg") (v "0.1.0") (d (list (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "ultron") (r "^0.1") (k 0)))) (h "1fzbsxdvlr41qrw9j145x1jna9rgc6f7py05zcnz2l0782227fm2")))

(define-public crate-ultron-ssg-0.1.2 (c (n "ultron-ssg") (v "0.1.2") (d (list (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "ultron") (r "^0.1") (k 0)))) (h "0ysrhay8791zx7lzjf8jg10av7a6xb4r3gc0x5axaillxbx6mbir")))

(define-public crate-ultron-ssg-0.2.1 (c (n "ultron-ssg") (v "0.2.1") (d (list (d (n "ultron") (r "^0.2.1") (k 0)))) (h "0w2zwj61jsqsiq4wv987npmlx11maac2z448pxvnfgv7klbk1cdr")))

(define-public crate-ultron-ssg-0.2.3 (c (n "ultron-ssg") (v "0.2.3") (d (list (d (n "ultron") (r "^0.2.3") (k 0)))) (h "1nk9v1sp6zrr46ha7arfxk9y4lhns0bshnx73iw5griz54afn83w")))

(define-public crate-ultron-ssg-0.2.4 (c (n "ultron-ssg") (v "0.2.4") (d (list (d (n "ultron") (r "^0.2.4") (k 0)))) (h "02wkic9ncl7madwm1igkq21kx25lg5awcnm8d9ckg44p44j8gwnx")))

(define-public crate-ultron-ssg-0.2.5 (c (n "ultron-ssg") (v "0.2.5") (d (list (d (n "ultron") (r "^0.2.5") (k 0)))) (h "1j06kf5ri2j52lnr986fiq8ijvzvp59bk9g5s5mkds5qk5ix8vp6")))

(define-public crate-ultron-ssg-0.2.6 (c (n "ultron-ssg") (v "0.2.6") (d (list (d (n "ultron") (r "^0.2.6") (k 0)))) (h "1p4d4w3myy324g8crzzla1lfxw0hzjcfvp48nxb9gffi5zb97knq")))

(define-public crate-ultron-ssg-0.3.0 (c (n "ultron-ssg") (v "0.3.0") (d (list (d (n "ultron-web") (r "^0.3.0") (k 0)))) (h "16i1243yikr2s6rm66b9v0w2avsj21a4v9rkyfq7mz31s9w5zpz0")))

