(define-module (crates-io ul tr ultragraph) #:use-module (crates-io))

(define-public crate-ultragraph-0.4.4 (c (n "ultragraph") (v "0.4.4") (d (list (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "1l25i13z5ah7z78s4ix7k962fb0ma98ckfhpg7fh8yrg6rk17ws8") (r "1.65")))

(define-public crate-ultragraph-0.4.5 (c (n "ultragraph") (v "0.4.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0aphglxwf8i9w0i2zxzgnsphiqzg8mr766qyfafmd78781ns1rv0") (r "1.65")))

(define-public crate-ultragraph-0.4.6 (c (n "ultragraph") (v "0.4.6") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1v4rcxjj6kwlam4jnrkq4183ncb83bgz063iijy4fy164y5ih37s") (r "1.65")))

(define-public crate-ultragraph-0.5.0 (c (n "ultragraph") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0yls7wdv548ivxjj0ivwl2afnsmzqdb0pz86bxvvaiylzkx7v2x8") (r "1.65")))

(define-public crate-ultragraph-0.5.1 (c (n "ultragraph") (v "0.5.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1ni3mzdaqykjbdj2pnnir9f4rwyxlgqp9g1kdlz4780q6pnhljwh") (r "1.65")))

(define-public crate-ultragraph-0.5.2 (c (n "ultragraph") (v "0.5.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "deep_causality_macros") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1lyb8npfv01z12f5dcafjik87m584i4mryqhwybfks4qmhq086a0") (r "1.65")))

