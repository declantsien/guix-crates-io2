(define-module (crates-io ul tr ultrametric_matrix_tools) #:use-module (crates-io))

(define-public crate-ultrametric_matrix_tools-0.1.1 (c (n "ultrametric_matrix_tools") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "numpy") (r "^0.14.1") (d #t) (k 0)) (d (n "ptree") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.14.5") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ck7648wiv5bs67cng2lr50avgbfzvkkglcnvbq76vfxbh3a3n28")))

