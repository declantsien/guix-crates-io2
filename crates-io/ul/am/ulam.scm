(define-module (crates-io ul am ulam) #:use-module (crates-io))

(define-public crate-ulam-0.1.0 (c (n "ulam") (v "0.1.0") (h "0c0ghwp250zfiyfg80hyfsaf0yzhzz426hs1348yn0qp396k06sr")))

(define-public crate-ulam-0.1.1 (c (n "ulam") (v "0.1.1") (h "0ny0srizlmhkpq69a7nmfqasqvvn26ykgrzafiig3g2b20hvxmkr")))

(define-public crate-ulam-0.2.0 (c (n "ulam") (v "0.2.0") (h "095i0c0xi7n7b3k5bjykshl44kbr6v4ikxzh5zk5vyy2ibyfwkza")))

(define-public crate-ulam-0.2.1 (c (n "ulam") (v "0.2.1") (d (list (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1nqpsl8s2g87zgqyx13ry8wlh3ljvf0pg4rfwsflkbvysl610jhj") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.3.0 (c (n "ulam") (v "0.3.0") (d (list (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1vzirvmdggmmlzrn8qjx2znl08y31bkcywdqspgi09wasw0fz1qd") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.3.1 (c (n "ulam") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "033g191yb8f616p10gxjlryhwlnj8mjdhn2a00wly7rn7wgykv9s") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.4.0 (c (n "ulam") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0d640pqzbc0apx8sq7va5hfzar2bv253rlkvm8f0yajpsd20lv6r") (f (quote (("img" "image" "primal") ("default" "img")))) (y #t)))

(define-public crate-ulam-0.4.1 (c (n "ulam") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1db4ni7w61klc1bg1r2idm8zcv5sjkjcpvh92vy64d9ly6hrn6ky") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.4.2 (c (n "ulam") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1c6im81bmb89331dikvcqr2jmy42r031xjsvg8s87jklwpn27x84") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.4.3 (c (n "ulam") (v "0.4.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0r352hi3m1cbsqx87984bdkdnrvj2l8gpyl37q3f9pc5wa8gywgk") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.4.4 (c (n "ulam") (v "0.4.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kl7r7210dyj7gnmqdshkgrfa8m71fdaw79f5bb5fp7px5xbwg1d") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.4.5 (c (n "ulam") (v "0.4.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0045ww21jbka6mcgx9y9919iw4nix7nmmnc5x7li0xfg840rmjs2") (f (quote (("img" "image" "primal") ("default" "img"))))))

(define-public crate-ulam-0.5.0 (c (n "ulam") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i7fja4360b07781zxsf89rm08xwhx45371s0r36qvzgscfmrnlv") (f (quote (("prime" "primal") ("img" "image" "primal") ("default" "img" "prime"))))))

(define-public crate-ulam-0.5.1 (c (n "ulam") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1s4rmjg116jas9mzi6yrlnwrk0mq2cbczp9npf6ck0djbwp3pqc4") (f (quote (("prime" "primal") ("img" "image" "primal") ("default" "img" "prime"))))))

(define-public crate-ulam-0.5.2 (c (n "ulam") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ih74dwp147bsfp3cjh137bjjncnqfgf0cvkaznr76549icl6ig1") (f (quote (("prime" "primal") ("img" "image" "primal") ("default" "img" "prime"))))))

