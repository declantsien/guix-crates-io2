(define-module (crates-io ul ib ulib-derive) #:use-module (crates-io))

(define-public crate-ulib-derive-0.1.0 (c (n "ulib-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1vf1rcrin62rc16qrxhjlchwvmpn6ywcdvdh24g7zz6d42j23qmb") (f (quote (("cuda"))))))

