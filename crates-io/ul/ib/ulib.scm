(define-module (crates-io ul ib ulib) #:use-module (crates-io))

(define-public crate-ulib-0.1.0 (c (n "ulib") (v "0.1.0") (h "0yyxm3cbqvya465y98vx6xhfv6060j542ldnv46j9zqpykaqdwdf")))

(define-public crate-ulib-0.2.0 (c (n "ulib") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1h0g4cncmshpirbqszk6xx6szihy3g87vfcfj2mhsfpn86wgf4b0") (s 2) (e (quote (("cuda" "dep:cust"))))))

(define-public crate-ulib-0.2.1 (c (n "ulib") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19llz0qjvg50mcg7n26zgynbdvhknvs2vrv64lkml4v4av61xxnq") (s 2) (e (quote (("cuda" "dep:cust"))))))

(define-public crate-ulib-0.3.0 (c (n "ulib") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ucc") (r "^0.2.0") (d #t) (k 1)))) (h "0zr3fp96c75c9ag8b8q5x5yvd26r871466l8q1bj6kvc43k4f9wf") (y #t) (l "ulib") (s 2) (e (quote (("cuda" "dep:cust"))))))

(define-public crate-ulib-0.3.1 (c (n "ulib") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ucc") (r "^0.2.0") (d #t) (k 1)))) (h "1g2c99rl42nhrg27y786bzjhwcsqgili0n499bpij6jr2lpx3kb1") (l "ulib") (s 2) (e (quote (("cuda" "dep:cust"))))))

(define-public crate-ulib-0.3.2 (c (n "ulib") (v "0.3.2") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ucc") (r "^0.2.0") (d #t) (k 1)))) (h "05zjvx24f3db8fzj0pcmj4hyrlrscq6k6v1b4y87j7649vy4sw6g") (l "ulib") (s 2) (e (quote (("cuda" "dep:cust"))))))

(define-public crate-ulib-0.3.3 (c (n "ulib") (v "0.3.3") (d (list (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "cust") (r "^0.3.2") (f (quote ("bytemuck"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ucc") (r "^0.2.0") (d #t) (k 1)) (d (n "ulib-derive") (r "^0.1.0") (d #t) (k 0)))) (h "14gdpp1r52lx4q0ps0fbx5lzm664gaw56cyxgqrnkv0pmfxwjfxz") (l "ulib") (s 2) (e (quote (("cuda" "dep:cust" "ulib-derive/cuda"))))))

