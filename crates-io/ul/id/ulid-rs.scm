(define-module (crates-io ul id ulid-rs) #:use-module (crates-io))

(define-public crate-ulid-rs-0.1.0 (c (n "ulid-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "10xzg37n9sbjzv8f9ba83n7d612vili22kzzq8a55adqy7i1vqgw")))

