(define-module (crates-io ul id ulid-lite) #:use-module (crates-io))

(define-public crate-ulid-lite-0.6.0 (c (n "ulid-lite") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "xorshift") (r "^0.1.3") (d #t) (k 0)))) (h "08r2mvcsvl6isszmn3jimw0rqid80kaig0i53vinmc09m117v45q") (f (quote (("ffi"))))))

(define-public crate-ulid-lite-0.6.1 (c (n "ulid-lite") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "xorshift") (r "^0.1.3") (d #t) (k 0)))) (h "0mnmlhsm5336hn4p1bjc143zhwp8m78wlkb7myzs7vwananrw0ys") (f (quote (("ffi"))))))

