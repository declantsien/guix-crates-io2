(define-module (crates-io ul ur uluru) #:use-module (crates-io))

(define-public crate-uluru-0.1.0 (c (n "uluru") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)))) (h "163fmmlwz0zwjkx2hlmhs8gv2im6yvcd7c0aq7ig8j7x8pjgqa6d")))

(define-public crate-uluru-0.2.0 (c (n "uluru") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)))) (h "1fcx8yszwafvc5za1vb5skqjc8lcfdri7byqm50aajwnxbq314ai")))

(define-public crate-uluru-0.3.0 (c (n "uluru") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)))) (h "17vdfjab08vdykcyjmprzxq8mqh06jzx3idky32ds27kja8nwq6j")))

(define-public crate-uluru-0.4.0 (c (n "uluru") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)))) (h "1jp16mgi5kg7w8damvsf4raygxhd2pflnkik5pamgfifqg83jyvd")))

(define-public crate-uluru-1.0.0 (c (n "uluru") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)))) (h "0nkz17zpcf7dmb7nb8mi9n05782b8cqwp9d95pa9pj2sh29mx0p8")))

(define-public crate-uluru-1.1.0 (c (n "uluru") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "17w8pwphqmsrjmwlsck49ba6jvwnqp6a1cg2cln7dc4mb6308mj9")))

(define-public crate-uluru-1.1.1 (c (n "uluru") (v "1.1.1") (d (list (d (n "arrayvec") (r "^0.5") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1nzmkq149yqn5pmxj6pk6ga6k2qanpfszf3n1qi6xjsf552nzvmn")))

(define-public crate-uluru-2.0.0 (c (n "uluru") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "15zqgxkhqfych2n41rwn91bm487y851y7yrfb05cfi58441gq7p8")))

(define-public crate-uluru-2.1.0 (c (n "uluru") (v "2.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "08g371x51i3mzvycjdc87szby6mv4halijy6cgs847xkzipj5rak")))

(define-public crate-uluru-2.1.1 (c (n "uluru") (v "2.1.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "1ism59zf6srw3z3qasgadl732sldd2pn3kh4ll7m04w0agd6fyff")))

(define-public crate-uluru-2.2.0 (c (n "uluru") (v "2.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "0widdhxllh1836f2q04wm20hc1pv6scvkbaiz2b7f8kvjjfwr39h")))

(define-public crate-uluru-3.0.0 (c (n "uluru") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "1nr6s90iqy7wr0ddv9zfbmyw5dc7xjfbb09c8sjbcphz38k34jkr")))

(define-public crate-uluru-3.1.0 (c (n "uluru") (v "3.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "1njp6vvy1mm8idnsp6ljyxx5znfsk3xkmk9cr2am0vkfwmlj92kw")))

