(define-module (crates-io ul td ultdocs) #:use-module (crates-io))

(define-public crate-ultdocs-0.1.1 (c (n "ultdocs") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1rp2q3xr94vmq2nk57v2q131hrx9s46v1s2d6bnp89mwsnsh6znr")))

