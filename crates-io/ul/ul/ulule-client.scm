(define-module (crates-io ul ul ulule-client) #:use-module (crates-io))

(define-public crate-ulule-client-0.1.0 (c (n "ulule-client") (v "0.1.0") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "ulule") (r "^0.0.3") (d #t) (k 0)))) (h "1xmi22dwmqg43mkr074panpcj6vk23qmm0cfb4j9h0fxssd83pwr")))

(define-public crate-ulule-client-0.1.1 (c (n "ulule-client") (v "0.1.1") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "ulule") (r "^1.0.0") (d #t) (k 0)))) (h "1ac19l6q1gxkxpwja19g8p7qcmbn9jnsmgf8f8ym0xkar56c1pkm")))

(define-public crate-ulule-client-1.0.0 (c (n "ulule-client") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10.0-alpha.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "ulule") (r "^1.0.0") (d #t) (k 0)))) (h "1h7varlz00pshs10j4swld5fhwx9kbc0pypb671vy48r87il918f")))

