(define-module (crates-io ul ul ulule) #:use-module (crates-io))

(define-public crate-ulule-0.0.0 (c (n "ulule") (v "0.0.0") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 0)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0j0jx0whjm19fdlangghph01vjbw681rh54xw5by7ypk9ficrffd")))

(define-public crate-ulule-0.0.1 (c (n "ulule") (v "0.0.1") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 0)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1zwrk1jhv54i2f08967vbys1hgzsk1vn3yx6f76sqvamry0zazz9")))

(define-public crate-ulule-0.0.2 (c (n "ulule") (v "0.0.2") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0wxgsw22kqcxsll62d5lx40vwi90a07mc0s99n8hc6yzmd2prjid")))

(define-public crate-ulule-0.0.3 (c (n "ulule") (v "0.0.3") (d (list (d (n "actix-http") (r "^0.2.5") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2.2") (d #t) (k 2)) (d (n "awc") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "00pj1zv38x0w92x87h21d4q77z5brqag673z6xh347fzc9m16s13")))

(define-public crate-ulule-1.0.0 (c (n "ulule") (v "1.0.0") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "18hpbd3xy750r0c40fsvd9d6xagqx6p9xhk8paadisp79gr1phmv")))

