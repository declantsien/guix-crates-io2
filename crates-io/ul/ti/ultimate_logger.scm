(define-module (crates-io ul ti ultimate_logger) #:use-module (crates-io))

(define-public crate-ultimate_logger-1.0.0 (c (n "ultimate_logger") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "01rhq7gb79ncpx37f4jy7gx07ymf83c0w968kpyhx20nb7q6vmk0") (y #t) (r "1.56.0")))

(define-public crate-ultimate_logger-1.0.1 (c (n "ultimate_logger") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1frzkpn4vjfksxqy42gxxwhybg0y22cd9h1jvmrcv3fx0l90jiai") (y #t) (r "1.56.0")))

(define-public crate-ultimate_logger-1.0.2 (c (n "ultimate_logger") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qyx2ba6b1wafah0l1zscr5i09pkl1dj9rkljfn4rzs0lcd0zsy1") (y #t) (r "1.56.0")))

(define-public crate-ultimate_logger-1.0.3 (c (n "ultimate_logger") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "19mmdzcdncgdl96j3xv4rfhm59bmdi776ikfjy23n5xd934r11cm") (y #t) (r "1.56.0")))

(define-public crate-ultimate_logger-1.0.4 (c (n "ultimate_logger") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1b48g7ig7ab8s9di3snikp4z42m0nqpirkjcr7cz1xdd503rf85i") (r "1.56.0")))

