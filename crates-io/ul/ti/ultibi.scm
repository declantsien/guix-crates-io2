(define-module (crates-io ul ti ultibi) #:use-module (crates-io))

(define-public crate-ultibi-0.1.6 (c (n "ultibi") (v "0.1.6") (d (list (d (n "ultibi_core") (r "^0.1") (d #t) (k 0)) (d (n "ultibi_server") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hbcqjrrfn90mgnb8848414mgcsicyr4b1cfy1jwrzs833n10b5h") (f (quote (("ui" "ultibi_server") ("default") ("aws_s3" "ultibi_core/aws_s3"))))))

