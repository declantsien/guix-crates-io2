(define-module (crates-io ul la ullage) #:use-module (crates-io))

(define-public crate-ullage-0.1.0 (c (n "ullage") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^80") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1p57h99grmwjm4qw4g1k2ljynpd637ljzcqqzsahc1gydw7k13ar")))

