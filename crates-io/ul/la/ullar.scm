(define-module (crates-io ul la ullar) #:use-module (crates-io))

(define-public crate-ullar-0.1.0 (c (n "ullar") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minimap2") (r "^0.1.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "segul") (r "^0.21.1") (d #t) (k 0)))) (h "146wv75ma6i1kkipqiba9yxx0fp8ivvwaxmqdr0nc56iddmfffl4")))

