(define-module (crates-io ul -n ul-next-sys) #:use-module (crates-io))

(define-public crate-ul-next-sys-1.4.0-alpha.0 (c (n "ul-next-sys") (v "1.4.0-alpha.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ixj0p3gm9039wsnmiddhcrbxk5h1ixsy51f1vw0mjgf8jzcs03p") (f (quote (("only-ul-deps"))))))

(define-public crate-ul-next-sys-1.4.0-alpha.1 (c (n "ul-next-sys") (v "1.4.0-alpha.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "13igf47dx2p7lrnjxr5r5gm2d66yvma2h6mi736yky5d0p51dbg5") (f (quote (("only-ul-deps"))))))

(define-public crate-ul-next-sys-1.4.0-alpha.2 (c (n "ul-next-sys") (v "1.4.0-alpha.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zhxv0xjdijvnrr6l52idxcalw31s8h8l9x67fpifldc082fzcg7") (f (quote (("only-ul-deps") ("docs_only"))))))

(define-public crate-ul-next-sys-1.4.0-alpha.3 (c (n "ul-next-sys") (v "1.4.0-alpha.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ysg2smc7jf1frvbw06r8pf9ak8xgwkdhxiyjglh9y33x1nlp8cf") (f (quote (("only-ul-deps") ("docs_only"))))))

