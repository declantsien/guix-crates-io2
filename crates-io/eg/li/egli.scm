(define-module (crates-io eg li egli) #:use-module (crates-io))

(define-public crate-egli-0.1.0 (c (n "egli") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "x11") (r "^2.3") (f (quote ("xlib"))) (d #t) (k 2)))) (h "0ggacr79g61mvh2jfgv3i9kwrwlz43h8h9x8nwkdnw3n27yqqmhq") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.1.1 (c (n "egli") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.3") (f (quote ("xlib"))) (d #t) (k 2)))) (h "1a7i6j5v474sl5akznqvzxia6fx8xm8rb5nsgc5qnscgmcmz0q0g") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.1.2 (c (n "egli") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.3") (f (quote ("xlib"))) (d #t) (k 2)))) (h "1naa1pf215xzk9h3d62siv1cpxdi391vghzrllv2bdwzyf95w5f8") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.2.0 (c (n "egli") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.12") (f (quote ("xlib"))) (d #t) (k 2)))) (h "0a0k7xga683bc74g5hdxc2viwhnyihgpxvvpy53m7v364i74hdai") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.2.1 (c (n "egli") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.12") (f (quote ("xlib"))) (d #t) (k 2)))) (h "0pw6sc70nrr85yxscr6cxys9zarcdzl6w2mz3c8jx1w0xdi8mwy5") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.4.0 (c (n "egli") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.12") (f (quote ("xlib"))) (d #t) (k 2)))) (h "1s75dkb4igmfizp7fr9hksiaa9zr0l16lw293nsxgccf23x1ip2z") (f (quote (("egl_1_5"))))))

(define-public crate-egli-0.5.0 (c (n "egli") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib"))) (d #t) (k 2)))) (h "0y5k3fbhryi7xfvzqwcghkhbz8dw6m9i2nr2baxpd0k1w1gka4w7") (f (quote (("egl_1_5"))))))

