(define-module (crates-io eg _d eg_derive) #:use-module (crates-io))

(define-public crate-eg_derive-0.1.0 (c (n "eg_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1csqdjri8c9ifbpqry72kn75pgy3njwwdw1hamm9kfcqcs8rrsil")))

