(define-module (crates-io eg ak egak) #:use-module (crates-io))

(define-public crate-egak-0.1.0 (c (n "egak") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0xblmnpfqd1wc006911aaj2raxfs00xm18ss7bvb9022pkry75hf")))

(define-public crate-egak-0.1.1 (c (n "egak") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0ayw4n2fsfn800vhjj0r4qs64biyaf6nzasrvfg06px8y5mrsswv")))

(define-public crate-egak-0.1.2 (c (n "egak") (v "0.1.2") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1mrcjlhi6f5506zcizxh3f8hqabsxvyn77svdqmgmb86snw2zvkd")))

(define-public crate-egak-0.1.3 (c (n "egak") (v "0.1.3") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1d4f2qa9h6vls763bmxm0x4g4gizcpfp1f1hfpsrqrsa0j489ay5")))

