(define-module (crates-io eg ak egaku2d_core) #:use-module (crates-io))

(define-public crate-egaku2d_core-0.1.0 (c (n "egaku2d_core") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0fz19j590xj1nppijl74mr7bbffwy8b7g2b3lyi6m0gscpjd2zzq")))

(define-public crate-egaku2d_core-0.1.1 (c (n "egaku2d_core") (v "0.1.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0dm6nv4fsbh3gwnmmd5y0cdxnaj0j8mz69bwhyfjajvcrq7gfp0x")))

(define-public crate-egaku2d_core-0.1.2 (c (n "egaku2d_core") (v "0.1.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1bd9h3n0v5sbfirjy5159jy4mhrab628vz97fb21q4zxynmzsl3s")))

(define-public crate-egaku2d_core-0.1.3 (c (n "egaku2d_core") (v "0.1.3") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "03pmf7bffc19vhavxg8al0c5l9bwl21p4jfw8gnxs57g964c5kvk")))

(define-public crate-egaku2d_core-0.2.0 (c (n "egaku2d_core") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0hc0km9qxs65pfxwk0lxd8hkg8y2xajch7s3yzv2ic15lyzya8av")))

(define-public crate-egaku2d_core-0.2.1 (c (n "egaku2d_core") (v "0.2.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1lhzm3rkbxbsm9a2yi50wafbly87lvwyaiij8ld0grdrvkc1p7rr")))

(define-public crate-egaku2d_core-0.2.2 (c (n "egaku2d_core") (v "0.2.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1d91szakcrialv1d402lra6i2p65cdfmkpds3pc1if6nlcz85nbj")))

(define-public crate-egaku2d_core-0.3.0 (c (n "egaku2d_core") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1z489r2sngh8f2l3k2d9xa2fwkf7rli6i01my91ykfdd9q10v27z")))

(define-public crate-egaku2d_core-0.3.1 (c (n "egaku2d_core") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1cn4mns267c02a9v4j9w41l27x7l6c9r5qrbgjn9wknczxqhwasc")))

(define-public crate-egaku2d_core-0.4.0 (c (n "egaku2d_core") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "0c1dhiih6cq4p3b7zy09km1k5gianzijp195fqd1rr1f0nhhzsri")))

(define-public crate-egaku2d_core-0.4.1 (c (n "egaku2d_core") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "1fhz46dyf4g7im4vn9jg8q5lmg2a16yfnddy4238w07gmwxwalwn")))

(define-public crate-egaku2d_core-0.5.0 (c (n "egaku2d_core") (v "0.5.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "16h1nf8zxcc0955xn54ljvmvl7bwiaqm38b1rz1ll2dsrglxmlws")))

(define-public crate-egaku2d_core-0.5.1 (c (n "egaku2d_core") (v "0.5.1") (d (list (d (n "axgeom") (r "^1.6") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "065s27qxd1jk6zshhcgq0lbmm7rjqhkbfv5wmdiq9zlmpi2msk5y")))

(define-public crate-egaku2d_core-0.6.0 (c (n "egaku2d_core") (v "0.6.0") (d (list (d (n "axgeom") (r "^1.9") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13.0") (d #t) (k 1)))) (h "12jxsa0cl8wh30q2j1j7hv4n9dyam35q907wr17yvmfjawqdh1s4")))

