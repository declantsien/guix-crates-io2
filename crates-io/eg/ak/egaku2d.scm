(define-module (crates-io eg ak egaku2d) #:use-module (crates-io))

(define-public crate-egaku2d-0.1.0 (c (n "egaku2d") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.1.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0l647b2hrp3vdqah68vhd48lyjwl0563avc3mmpmh4m0v7x8wk71") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1.1 (c (n "egaku2d") (v "0.1.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.1.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "17c2r7n5lmhbi3szpvqzvn72ib58wdkfpi0q9vy76jbnzb3y6jza") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1.2 (c (n "egaku2d") (v "0.1.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.1.2") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0pigqjq6vav86za44sd01mn1w09rw0i9mcjshqrk88v0rprjxji7") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1.3 (c (n "egaku2d") (v "0.1.3") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.1.3") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "10ps07ydgrrm9qr564xknrq6ra2garhz3d8sy3f1f7vcx995fiwq") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2.0 (c (n "egaku2d") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.2.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0a6a9xrgmn4xbd453sqj00s2bzlfhqqfsrrz5j1x582159xhs0hv") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2.1 (c (n "egaku2d") (v "0.2.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.2.1") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "11izdagg7pmfcinckp53w3mfqs7l5zfl56jrvqkbda2ilx5icrw6") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2.2 (c (n "egaku2d") (v "0.2.2") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.2.1") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0rfgqahfd9hhnwav1m405q738l7vnz6ixyis7s3fh6piq6dkafl5") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2.3 (c (n "egaku2d") (v "0.2.3") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.2.2") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1qcidyymjzp826wgjh0aqpjh9mfmwrc8nrr3pf46wq88wqwqnhpc") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.3.0 (c (n "egaku2d") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.3.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1w5dbi05x5dwycv9kvbr56ihrm0bqc0vmwp3xlgn8qyq29mj5xdd") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.3.1 (c (n "egaku2d") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.3.1") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1ncb5jk7p2qcw9xkf2qygdf4w7xf9vz8gij8dqn8b2y8yrb8xzrp") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.4.0 (c (n "egaku2d") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.4.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0bb6z7is3nxy7mlh54crb3p00ywlb5211wjiffqrk3f3dcqfzibn") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.4.1 (c (n "egaku2d") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.4.1") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0vngwnm00shb1xzcyykd03kpmvmi694r1gpldi1zf9rwwysrz66g") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5.0 (c (n "egaku2d") (v "0.5.0") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.5.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0rbikgraxlx08dvg4jq2gyvgahpsyndjlhhjwzfx2avi0bic2b5l") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5.1 (c (n "egaku2d") (v "0.5.1") (d (list (d (n "axgeom") (r "^1.5") (d #t) (k 0)) (d (n "egaku2d_core") (r "^0.5.0") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0f8mhw3al56wacxbybgdgjvfh6xdlb482c8ribf4m0jhy379zg5h") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5.2 (c (n "egaku2d") (v "0.5.2") (d (list (d (n "egaku2d_core") (r "^0.5.1") (d #t) (k 0)) (d (n "glutin") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "0895zdcp2xn22na9kg0idp0hrw5b47cq9zg9yql27zipx46qaw75") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5.3 (c (n "egaku2d") (v "0.5.3") (d (list (d (n "egaku2d_core") (r "^0.5.1") (d #t) (k 0)) (d (n "glutin") (r "^0.25") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "11dgfamrc1z65svvzaxgl6x0m3zlnh9ibmblrc95mpy37v6r3k92") (f (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5.4 (c (n "egaku2d") (v "0.5.4") (d (list (d (n "egaku2d_core") (r "^0.6") (d #t) (k 0)) (d (n "glutin") (r "^0.25") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)))) (h "1cwpnjfikg6nixqqgqamp716wn3zz65nmrgdd29r1r3p1mm8v078") (f (quote (("fullscreen"))))))

