(define-module (crates-io eg a_ ega_palette) #:use-module (crates-io))

(define-public crate-ega_palette-0.1.0 (c (n "ega_palette") (v "0.1.0") (h "11s4298fmc21rf22n2h1z7rhr8pwdgkxl4fplaw4bjiw56a0s699")))

(define-public crate-ega_palette-0.1.1 (c (n "ega_palette") (v "0.1.1") (h "0r9hbvgzandmh9ib743n9yssq3g1v4ccp07yd62l8glmb4swh815")))

