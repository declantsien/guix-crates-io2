(define-module (crates-io eg re egress) #:use-module (crates-io))

(define-public crate-egress-0.1.0 (c (n "egress") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.47") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1mz60h58vlf087ljx5k7jsjgpy67pv73ivl8vh2cqqk4fz62habs")))

(define-public crate-egress-0.1.1 (c (n "egress") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0mgzw83w6sl40m3vq7ibxb3rbw7a47vjk40kcwf1s0csnwghc3j6")))

