(define-module (crates-io eg ui egui_code_editor) #:use-module (crates-io))

(define-public crate-egui_code_editor-0.1.2 (c (n "egui_code_editor") (v "0.1.2") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "1l47wxw3pn7rmyxfkx8l8803vm0s59ry4kffb66ihdfwxx2bjrmb")))

(define-public crate-egui_code_editor-0.1.3 (c (n "egui_code_editor") (v "0.1.3") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "0c24d8hdrg3xik2ipblv6gp3wb0d1nxhir45b6pgknip11mg2q2w")))

(define-public crate-egui_code_editor-0.1.4 (c (n "egui_code_editor") (v "0.1.4") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "0fjavrjkr6q6a7w97paga64prap5skwp0c42bgbclvlq61rfinfh")))

(define-public crate-egui_code_editor-0.1.5 (c (n "egui_code_editor") (v "0.1.5") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "0l6z54242qhrfnbswwc49ggbp9pjvmpaa7c2kmjb11rxn8psnn4z")))

(define-public crate-egui_code_editor-0.1.6 (c (n "egui_code_editor") (v "0.1.6") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "0sgwf10ghxib5v4f1p03jncs1sd2pv6fcvic9l0wvfxi8ccj97fc")))

(define-public crate-egui_code_editor-0.1.7 (c (n "egui_code_editor") (v "0.1.7") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "00p9l6yc13hrhl8zgys0wb8rav7hhczj2bkzmc5llxw25gd2pxrl")))

(define-public crate-egui_code_editor-0.1.8 (c (n "egui_code_editor") (v "0.1.8") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)))) (h "1khsxhz4d410xk3nr02zdc0h73bfw1p71vf69sp96mawnlwz4jis")))

(define-public crate-egui_code_editor-0.1.9 (c (n "egui_code_editor") (v "0.1.9") (d (list (d (n "egui") (r "^0.24.0") (d #t) (k 0)))) (h "0a1bcjfnz2mgy44mk76g3z7byz05qfiv77d2cbx75q0nbvk0wjb3")))

(define-public crate-egui_code_editor-0.2.0 (c (n "egui_code_editor") (v "0.2.0") (d (list (d (n "eframe") (r "^0.24.0") (d #t) (k 2)) (d (n "egui") (r "^0.24.0") (d #t) (k 0)))) (h "0q69bp6qll0qbhscsajmjwgvxpcv8qkck1hjlqv210ms7vg2ajr4") (y #t)))

(define-public crate-egui_code_editor-0.2.1 (c (n "egui_code_editor") (v "0.2.1") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)))) (h "16m1719ll99ghymcb9lf8pbwk3by1lcf5z6yypfakdj0lhn4kw0r")))

(define-public crate-egui_code_editor-0.2.2 (c (n "egui_code_editor") (v "0.2.2") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "0mjv8025a3fzb51rxg8ywb6pcs2d50ni4d6fyd231ni3yxx7frm9") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-egui_code_editor-0.2.3 (c (n "egui_code_editor") (v "0.2.3") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1g13y1sfvziwj50nxpa6zaa3vxipjsqlippgjkg7y0dz9h242ggx") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-egui_code_editor-0.2.4 (c (n "egui_code_editor") (v "0.2.4") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (o #t) (d #t) (k 0)))) (h "1i8ws4a23p4lwilibx781ziahcw69xsqwz1rf0i9q3jv7pl1w0vi") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-egui_code_editor-0.2.5 (c (n "egui_code_editor") (v "0.2.5") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1wl0ivfkddr6rnzv3szadyf9mnxbds8wicyvkjm7s1ilgwqskn0b") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-egui_code_editor-0.2.6 (c (n "egui_code_editor") (v "0.2.6") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (o #t) (d #t) (k 0)))) (h "0clqv7m0wxrcsd8478g0wqrq0nzy1ix291ii6pnd4q93j1m3vzlx") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

(define-public crate-egui_code_editor-0.2.7 (c (n "egui_code_editor") (v "0.2.7") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (o #t) (d #t) (k 0)))) (h "04s49mmjaqggahh43j6sw61valq7yv8y58dv1qk2g1lmd90lsm6w") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

