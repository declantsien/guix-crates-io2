(define-module (crates-io eg ui egui_file) #:use-module (crates-io))

(define-public crate-egui_file-0.2.0 (c (n "egui_file") (v "0.2.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "1idmnb71sw1bcg4immw5hgs9fjpfiv89sw9hmkx3zdf5cak1pwsi")))

(define-public crate-egui_file-0.3.0 (c (n "egui_file") (v "0.3.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "18p195ydjnnhsbvbhw2hhhh8svjw4yc4lc7bswb001dxca59dya8")))

(define-public crate-egui_file-0.3.1 (c (n "egui_file") (v "0.3.1") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "0kwzwwrpzhz4zayww4iipk3c9063mj5ns1flvpwx8ds9cdympya4")))

(define-public crate-egui_file-0.4.0 (c (n "egui_file") (v "0.4.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "0w71n4qz48fdwk1bm0ddvlal6kn341nfz7n56lpz5vz21v8w1gcz")))

(define-public crate-egui_file-0.4.1 (c (n "egui_file") (v "0.4.1") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "089fp0xva63l3zi84zqj48yrl2ik51275zm47lv4m8wwpkhnazfy")))

(define-public crate-egui_file-0.5.0 (c (n "egui_file") (v "0.5.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "1v4b2fgbyc2wpxfag23q2lh37kg59rj27qswdxjsd7clfqp1b6ij")))

(define-public crate-egui_file-0.5.1 (c (n "egui_file") (v "0.5.1") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "19n3j0g1lkc3vck751y5flgdrc7y2sxsdnjgv52klhswzzh5cfsb")))

(define-public crate-egui_file-0.5.2 (c (n "egui_file") (v "0.5.2") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "166ayf7658j8hi3cdvr29w4l5yc0ai91q2bnywqx89lnpmdws30v")))

(define-public crate-egui_file-0.5.3 (c (n "egui_file") (v "0.5.3") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "094fzn9d9lanlgrzazvcvfm5vlmvgrg5lgf5j54vc52j217xwl39")))

(define-public crate-egui_file-0.5.4 (c (n "egui_file") (v "0.5.4") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "08r7i3iiyd74fzixhxdfjwp8pjhffvj4qwj3c5i5b9r9crfzpn2d")))

(define-public crate-egui_file-0.6.0 (c (n "egui_file") (v "0.6.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "119yldaniv7070i311yjl0iia9878vdq2v0jf2dgppffa2dr22p5")))

(define-public crate-egui_file-0.7.0 (c (n "egui_file") (v "0.7.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "1s8zd40jsznn56scyfmplsl5b6fsj7x6ymg23ydssbkalyffb9ys")))

(define-public crate-egui_file-0.8.0 (c (n "egui_file") (v "0.8.0") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "1yn289wp8p90bmfqfyy4axbr4yw74zm1my061w0kzsyknsyn2y36")))

(define-public crate-egui_file-0.8.1 (c (n "egui_file") (v "0.8.1") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "032a1949cikzx3d1bc8aqygk4dmmx8kz373l6k3al5yx8svg06ah")))

(define-public crate-egui_file-0.8.2 (c (n "egui_file") (v "0.8.2") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "1r4gidwvchkq7q8x9rl44fsxmsj11srsa8i4f2fqja2f40sghlza")))

(define-public crate-egui_file-0.8.3 (c (n "egui_file") (v "0.8.3") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "0371a5shaw3ylh4wkwf8rg3nabwl71myiph9lir1lr03f214brkz")))

(define-public crate-egui_file-0.9.0 (c (n "egui_file") (v "0.9.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1mjh4gnvzg15j8h3k0686ainz8w78k01pmcy30j1h92aw9413bl2")))

(define-public crate-egui_file-0.9.1 (c (n "egui_file") (v "0.9.1") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "0k7qkvndj7qi45a49jhbyvmfz9916bkgp0p28l31c25ynw32aw7q")))

(define-public crate-egui_file-0.9.2 (c (n "egui_file") (v "0.9.2") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1k3k20yw197qc9ii71c2i0mpdbylfp0xg31w703cw35j636rwfjr") (y #t)))

(define-public crate-egui_file-0.10.0 (c (n "egui_file") (v "0.10.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1h8cy9q3wy960vbx658ym0v6y9jxv4486gxyxh0snmdfwnm9dysp")))

(define-public crate-egui_file-0.10.1 (c (n "egui_file") (v "0.10.1") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "1li3r0isrj6pfxnwkadn17k09bi8sfsav54f009hwv1yzl7hq3dx")))

(define-public crate-egui_file-0.10.2 (c (n "egui_file") (v "0.10.2") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "0cfngg7sp5b4k6g7lb42pnyczk5xaddyls9nddyzx0vk0dy8c9g6")))

(define-public crate-egui_file-0.11.0 (c (n "egui_file") (v "0.11.0") (d (list (d (n "egui") (r "^0.23") (d #t) (k 0)))) (h "0bcdm7yyslpl8xlkfiw57av2s4xzby4vaa456ws4xmclbckbndkx")))

(define-public crate-egui_file-0.11.1 (c (n "egui_file") (v "0.11.1") (d (list (d (n "egui") (r "^0.23") (d #t) (k 0)))) (h "1iv1pmk8sz0ip76x2ci90rsh51vjwphp0m3pf1wy3kk2z6f6361p")))

(define-public crate-egui_file-0.12.0 (c (n "egui_file") (v "0.12.0") (d (list (d (n "egui") (r "^0.24") (d #t) (k 0)))) (h "106y641bg69ayd3dzw6iw21p239dzv2dfxv5axbc726qnd28vjwx")))

(define-public crate-egui_file-0.13.0 (c (n "egui_file") (v "0.13.0") (d (list (d (n "egui") (r "^0.24") (d #t) (k 0)))) (h "0yzlbwc0xpr7v5mqlrin4afax9aqpj80l627ffg1r34ccg0rr604")))

(define-public crate-egui_file-0.13.1 (c (n "egui_file") (v "0.13.1") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0zj41bn63cw54gfpfdxxalmvgj8ds054i245i5cl3196xhwmr76q") (y #t)))

(define-public crate-egui_file-0.14.0 (c (n "egui_file") (v "0.14.0") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0cvjzbslbgznyrqp88iz0ckkfg986h0v7s1fzci2b0m1973hvrrn")))

(define-public crate-egui_file-0.14.1 (c (n "egui_file") (v "0.14.1") (d (list (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "017qj256r6m27j6aca5kc32c376a0lxf7y3a9mpj8i5xm8mdnjhp")))

(define-public crate-egui_file-0.15.0 (c (n "egui_file") (v "0.15.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "0ziln8rppf9pb6bhk1rjr32s9phl439l24xw37d4dx2qsw8rbay7")))

(define-public crate-egui_file-0.16.0 (c (n "egui_file") (v "0.16.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "15880gwdqg1z6b7afyg3fv3a5ai5fy3j5zahrkh681dkybqma7a5")))

(define-public crate-egui_file-0.16.1 (c (n "egui_file") (v "0.16.1") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "00lymyvd3n3il80ij6wrmrh2cajnr3zwwaxrxd141j3d1ml2n7ii")))

(define-public crate-egui_file-0.16.2 (c (n "egui_file") (v "0.16.2") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "0lfm523wci88dzvgrzmfixf2ifbghcbs330j7f33np7d3fr57iny")))

(define-public crate-egui_file-0.16.3 (c (n "egui_file") (v "0.16.3") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)))) (h "08q1d23vwvvha09jqpdkw7z8d1kl7fvhfw4wazy1q4mmys902j82")))

(define-public crate-egui_file-0.17.0 (c (n "egui_file") (v "0.17.0") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)))) (h "0zzpqxwzswps7mw7z4wfjg76zfpg6ngrd78r5pldn286cwsbqwnp")))

