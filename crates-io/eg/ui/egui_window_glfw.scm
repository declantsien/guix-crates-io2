(define-module (crates-io eg ui egui_window_glfw) #:use-module (crates-io))

(define-public crate-egui_window_glfw-0.1.0 (c (n "egui_window_glfw") (v "0.1.0") (d (list (d (n "egui_backend") (r "^0.1") (d #t) (k 0)) (d (n "glfw") (r "^0.45") (k 0)))) (h "0mckqazd8sc08nkjr5pbiwgrhr9hb352q54d0pac8g64z3vycfxc") (f (quote (("glfw_wayland" "glfw/wayland") ("glfw_vulkan" "glfw/vulkan") ("glfw_all" "glfw/all") ("default" "glfw/default"))))))

(define-public crate-egui_window_glfw-0.2.0 (c (n "egui_window_glfw") (v "0.2.0") (d (list (d (n "egui_backend") (r "^0.2") (d #t) (k 0)) (d (n "glfw") (r "^0.45") (k 0)))) (h "1f3s3kz18xda831n1a3mnvx95lzbh1dqw15xd3kii1rzyjmdlmh5") (f (quote (("glfw_wayland" "glfw/wayland") ("glfw_vulkan" "glfw/vulkan") ("glfw_all" "glfw/all") ("default" "glfw/default"))))))

