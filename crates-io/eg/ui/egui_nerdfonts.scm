(define-module (crates-io eg ui egui_nerdfonts) #:use-module (crates-io))

(define-public crate-egui_nerdfonts-0.1.0 (c (n "egui_nerdfonts") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (k 0)))) (h "1dk6fy0vq50v1ymljyq4i4f6s9c5v7x3cvcy4ks67jclrr4d15lq")))

(define-public crate-egui_nerdfonts-0.1.1 (c (n "egui_nerdfonts") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (k 0)))) (h "11xyki87jq35cs0xhyfzgal3x6fj2s1vj0dwjkzav4pj46hd40gr")))

(define-public crate-egui_nerdfonts-0.1.2 (c (n "egui_nerdfonts") (v "0.1.2") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (k 0)))) (h "16m5ivs2dsj6s5kf43s1pljph7kqgx4yk2hadn9v4k3arxnpdynz")))

