(define-module (crates-io eg ui egui-aesthetix) #:use-module (crates-io))

(define-public crate-egui-aesthetix-0.1.0 (c (n "egui-aesthetix") (v "0.1.0") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)))) (h "0yis039d12fl499aqx4vp26v7s8b4cchj87j97r2js6fm2gz3ca5")))

(define-public crate-egui-aesthetix-0.2.0 (c (n "egui-aesthetix") (v "0.2.0") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)))) (h "0g7nc0cazrz8xrcgfwwqlg1fqpcpzdh5rx8yhpjsra253gwr3h17") (f (quote (("standard") ("default" "standard") ("carl") ("all_themes" "standard" "carl"))))))

(define-public crate-egui-aesthetix-0.2.1 (c (n "egui-aesthetix") (v "0.2.1") (d (list (d (n "egui") (r "^0.26.2") (d #t) (k 0)))) (h "1vx210l0c6y0mc61s1vwbby6qp6x2x10436bsj6gan6m3nnj9lnc") (f (quote (("standard") ("default" "standard") ("carl") ("all_themes" "standard" "carl")))) (y #t)))

(define-public crate-egui-aesthetix-0.2.2 (c (n "egui-aesthetix") (v "0.2.2") (d (list (d (n "egui") (r "^0.26.2") (d #t) (k 0)))) (h "0b607xqcp0xiszqh4fb9mvwqj19szfjz2yx1ximja1zlgjl6gpj8") (f (quote (("standard") ("default" "standard") ("carl") ("all_themes" "standard" "carl"))))))

(define-public crate-egui-aesthetix-0.2.3 (c (n "egui-aesthetix") (v "0.2.3") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)))) (h "0j0yvlpd2m9z7vv9f9581wm5mmkn7zk6b2cbrrwg4aiz6xwdk24i") (f (quote (("tokyo_night") ("standard") ("nord") ("default" "standard") ("carl") ("all_themes" "standard" "carl" "nord" "tokyo_night"))))))

