(define-module (crates-io eg ui egui-datepicker) #:use-module (crates-io))

(define-public crate-egui-datepicker-0.1.0 (c (n "egui-datepicker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eframe") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "111cnpbq59vvvkddjvx95qqyhb89rd13r15llffmk9c2qr1xrlad")))

(define-public crate-egui-datepicker-0.1.1 (c (n "egui-datepicker") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eframe") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "014r1smsqmnvvksbzkpla70wmmha5ndb8y5n1pbkyks23n9zwhi3")))

(define-public crate-egui-datepicker-0.2.0 (c (n "egui-datepicker") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11r98nhywh02q8ii5dr5z33k561nzjpvll7wiqjf9mq17ljijvv0")))

(define-public crate-egui-datepicker-0.3.0 (c (n "egui-datepicker") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eframe") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0sr3fmij4y71nhfg4arba1m4x665f9c6cqm72yl59z1gvl6mj0n7")))

