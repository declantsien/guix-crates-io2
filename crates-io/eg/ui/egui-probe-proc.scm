(define-module (crates-io eg ui egui-probe-proc) #:use-module (crates-io))

(define-public crate-egui-probe-proc-0.1.0 (c (n "egui-probe-proc") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f02igxcngv9181vqgjyzsaz96az5jf07vhrr0aqrlz1cvrrd9ih")))

(define-public crate-egui-probe-proc-0.2.0 (c (n "egui-probe-proc") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1168y5q5lnlg7wz2gzyrsrvmd1ii7ay7jcm77vr15xfrld13xgac")))

(define-public crate-egui-probe-proc-0.3.0 (c (n "egui-probe-proc") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n1x23bhzppd43pn9xsqnw8nwpxj1v28h31apcbhmapzj3vvvzik")))

(define-public crate-egui-probe-proc-0.3.3 (c (n "egui-probe-proc") (v "0.3.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05pawn5nskgzglbl1jw0sd5288b4ipzzaxisldn7ddv9n5108yqq")))

(define-public crate-egui-probe-proc-0.3.6 (c (n "egui-probe-proc") (v "0.3.6") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12hv99ljb2kyiwcirzxgw0gjl6h4gnxlmninxf1gy6s884fp1kpx")))

(define-public crate-egui-probe-proc-0.4.0 (c (n "egui-probe-proc") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12jdk0x05hrh8dx0x3gigsdd4hxqnxqvpsy94p3hisjmd6rzc3a1")))

