(define-module (crates-io eg ui egui-any) #:use-module (crates-io))

(define-public crate-egui-any-0.1.0 (c (n "egui-any") (v "0.1.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "egui-probe") (r "^0.3.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qp1wmiz664j7vj3280zn6wq2i91s0y4ajwgdvshwz8fi7dhpc9v") (s 2) (e (quote (("serde" "dep:serde"))))))

