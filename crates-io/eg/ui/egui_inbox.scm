(define-module (crates-io eg ui egui_inbox) #:use-module (crates-io))

(define-public crate-egui_inbox-0.1.0 (c (n "egui_inbox") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (k 0)))) (h "13sdjni5wjq6fnc3nh75dxap2bms617zkvbhn7xq7qj3khkb4ibv")))

(define-public crate-egui_inbox-0.1.1 (c (n "egui_inbox") (v "0.1.1") (d (list (d (n "eframe") (r ">=0.22") (d #t) (k 2)) (d (n "egui") (r ">=0.22") (k 0)))) (h "1h59xblfig81jy22h0brg1vjk088pb2jfckig4yglpff4cyzmlc8")))

(define-public crate-egui_inbox-0.2.0 (c (n "egui_inbox") (v "0.2.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)))) (h "1r7iyys943y1lb1073x2jf717iya0iyb9zghndlg30wphg4pbwpr")))

(define-public crate-egui_inbox-0.3.0 (c (n "egui_inbox") (v "0.3.0") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)))) (h "162gca5dkfkky418nr30wsyk96yv3gwmvc7kz4jwdnfm1dh4zn4r")))

(define-public crate-egui_inbox-0.4.0 (c (n "egui_inbox") (v "0.4.0") (d (list (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (o #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hello_egui_utils") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14yqmykm5vy9bkn29f8920inm782r129h4abq1pi14d8ilvn2xj0") (f (quote (("tokio" "async" "hello_egui_utils/tokio") ("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui") ("async" "dep:hello_egui_utils" "hello_egui_utils/async" "dep:futures-channel" "dep:futures"))))))

(define-public crate-egui_inbox-0.4.1 (c (n "egui_inbox") (v "0.4.1") (d (list (d (n "derive-new") (r "^0.6") (d #t) (k 2)) (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (o #t) (k 0)) (d (n "ehttp") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hello_egui_utils") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "type-map") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1fm44276h2lvb70v8fzqqk7agcw9rmgx73r9ji8yzf50qpkkh69v") (f (quote (("tokio" "async" "hello_egui_utils/tokio") ("default" "egui") ("broadcast")))) (s 2) (e (quote (("type_inbox" "dep:type-map" "dep:hello_egui_utils") ("type_broadcast" "dep:type-map" "broadcast" "dep:hello_egui_utils") ("egui" "dep:egui") ("async" "dep:hello_egui_utils" "hello_egui_utils/async" "dep:futures-channel" "dep:futures"))))))

