(define-module (crates-io eg ui egui_ui_refresh) #:use-module (crates-io))

(define-public crate-egui_ui_refresh-0.1.0 (c (n "egui_ui_refresh") (v "0.1.0") (d (list (d (n "eframe") (r "^0.23") (f (quote ("glow"))) (k 2)) (d (n "egui") (r "^0.23") (k 0)))) (h "0760r0grjagmkvfd128q4fivfqjyb9js8bspgm9gsw1b9a689mcl") (r "1.70")))

