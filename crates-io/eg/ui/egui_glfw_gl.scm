(define-module (crates-io eg ui egui_glfw_gl) #:use-module (crates-io))

(define-public crate-egui_glfw_gl-0.13.1 (c (n "egui_glfw_gl") (v "0.13.1") (d (list (d (n "clipboard") (r "^0.2") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.41") (d #t) (k 0)))) (h "1cq92q76aj6k4vd3vmdq0frxacws7cdn7lp8anq1iblma1igd3h9") (f (quote (("default" "clipboard"))))))

