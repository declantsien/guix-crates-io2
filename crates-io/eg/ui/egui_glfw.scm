(define-module (crates-io eg ui egui_glfw) #:use-module (crates-io))

(define-public crate-egui_glfw-0.13.1 (c (n "egui_glfw") (v "0.13.1") (d (list (d (n "clipboard") (r "^0.4.0") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)))) (h "1vfycz29c0vzclk962fddg49lgi59lbmxy5iskjmqy66xnafvpm8") (f (quote (("default" "clipboard"))))))

(define-public crate-egui_glfw-0.55.0 (c (n "egui_glfw") (v "0.55.0") (d (list (d (n "clipboard") (r "^0.4.0") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)))) (h "1jzhq4g4f6spi0zlr19pxm63hk31mz1wr2kd2b6yxyn23i74ixl3") (f (quote (("default" "clipboard"))))))

(define-public crate-egui_glfw-0.55.1 (c (n "egui_glfw") (v "0.55.1") (d (list (d (n "clipboard") (r "^0.4.0") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.27.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)))) (h "0qbvk7xbrayal6wlykqnxc4zx2x7s0j10rqzgxkjrmg1j4azg08z") (f (quote (("default" "clipboard"))))))

