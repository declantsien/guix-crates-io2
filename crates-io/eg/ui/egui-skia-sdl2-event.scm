(define-module (crates-io eg ui egui-skia-sdl2-event) #:use-module (crates-io))

(define-public crate-egui-skia-sdl2-event-0.3.0 (c (n "egui-skia-sdl2-event") (v "0.3.0") (d (list (d (n "egui") (r ">=0.19") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0jkd676dn7lzr9692j6gm7c08h5xwh8zi7sy3c2k9a7x8ngid1yh")))

(define-public crate-egui-skia-sdl2-event-0.4.0 (c (n "egui-skia-sdl2-event") (v "0.4.0") (d (list (d (n "egui") (r ">=0.21") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0x3icfjg1dfc7yz27akhs9vqpw50vdf8gxyq490v713hcch0lsxl")))

