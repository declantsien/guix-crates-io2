(define-module (crates-io eg ui egui-macroquad) #:use-module (crates-io))

(define-public crate-egui-macroquad-0.1.0 (c (n "egui-macroquad") (v "0.1.0") (d (list (d (n "egui") (r "^0.10.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.2.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.10.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "0qhm7nm2qjfrhq5f0gsyyyakgaqnq6iq3akb86764ryrinark3ab")))

(define-public crate-egui-macroquad-0.2.0 (c (n "egui-macroquad") (v "0.2.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.3.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.11.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "1h2kq2qlrpp79y7yj75yc6c902024wd4qkbl3k2qv6hib0cg6k42")))

(define-public crate-egui-macroquad-0.3.0 (c (n "egui-macroquad") (v "0.3.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.3.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.11.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.0") (d #t) (k 0)))) (h "1q85wfyzd38glajwkcbpznr9y8m4y591w3hh6hh6cc8yhpihay8w")))

(define-public crate-egui-macroquad-0.4.0 (c (n "egui-macroquad") (v "0.4.0") (d (list (d (n "egui") (r "^0.12.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.4.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.12.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.3") (d #t) (k 0)))) (h "1glsj9wakc9xwx7x45f0ly5npm3b2pjj4xdly6wpvckfz7pxb6rj")))

(define-public crate-egui-macroquad-0.5.0 (c (n "egui-macroquad") (v "0.5.0") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.5.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.13.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.6") (d #t) (k 0)))) (h "0bqazfqq0k4pgb8drkx99nb98rzrrglc6wv1z5ij1xg8n2l9shf4")))

(define-public crate-egui-macroquad-0.5.1 (c (n "egui-macroquad") (v "0.5.1") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.5.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.13.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.6") (d #t) (k 0)))) (h "1v0f2d6shmp6awn7cxjfljw11hfiinrrx7nv3qvdkvx5k3a7jhrw")))

(define-public crate-egui-macroquad-0.6.0 (c (n "egui-macroquad") (v "0.6.0") (d (list (d (n "egui") (r "^0.14.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.6.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.14.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.6") (d #t) (k 0)))) (h "17a0mazcvsil6dq4winn8sqyg2vlfp879pzc4pc4c8fkcln5hqxm")))

(define-public crate-egui-macroquad-0.6.1 (c (n "egui-macroquad") (v "0.6.1") (d (list (d (n "egui") (r "^0.14.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.6.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.14.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.6") (d #t) (k 0)))) (h "14z4yaxasgwi0wz5vm9mvd91wbn3l4a9zghm9yfy96ylp21psi0i")))

(define-public crate-egui-macroquad-0.7.0 (c (n "egui-macroquad") (v "0.7.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.7.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.15.0") (k 2)) (d (n "macroquad") (r "^0.3.6") (d #t) (k 0)))) (h "05rxwq47vcxgi44p72m9nzf3l0h724z8n17f1z9ig0fc87p9bb41")))

(define-public crate-egui-macroquad-0.8.0 (c (n "egui-macroquad") (v "0.8.0") (d (list (d (n "egui") (r "^0.16.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.8.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.16.0") (k 2)) (d (n "macroquad") (r "^0.3.6") (k 0)))) (h "0dp70za57p0hfjzkapkv47ismxcklm2yznqpssh59n4h0f8xh6df")))

(define-public crate-egui-macroquad-0.9.0 (c (n "egui-macroquad") (v "0.9.0") (d (list (d (n "egui") (r "^0.17.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.9.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.17.0") (k 2)) (d (n "macroquad") (r "^0.3.6") (k 0)))) (h "02iqqi3z2n1zf3dfxh6ql82dphjlz52c3shph3zv6x97vwpf6056")))

(define-public crate-egui-macroquad-0.10.0 (c (n "egui-macroquad") (v "0.10.0") (d (list (d (n "egui") (r "^0.17.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.10.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.17.0") (k 2)) (d (n "macroquad") (r "^0.3.16") (k 0)))) (h "08rhwib1p0s7nds2i99dnpbm5kfbbkd47bdq9234mnxwfw7g6pmw")))

(define-public crate-egui-macroquad-0.11.0 (c (n "egui-macroquad") (v "0.11.0") (d (list (d (n "egui") (r "^0.18.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.18.0") (k 2)) (d (n "macroquad") (r "^0.3.16") (k 0)))) (h "1ycsm61c8bmca038kdr69s2lzfl7fsz9h3jrahnkgf9gm4caaj4i")))

(define-public crate-egui-macroquad-0.12.0 (c (n "egui-macroquad") (v "0.12.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.12.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.19.0") (d #t) (k 2)) (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)))) (h "184j3l8m6s0mihv9hly73dmkrcx0znp2y8mkxvprpdhm2wclpfcp")))

(define-public crate-egui-macroquad-0.13.0 (c (n "egui-macroquad") (v "0.13.0") (d (list (d (n "egui") (r "^0.20.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.13.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.20.0") (d #t) (k 2)))) (h "1bzb2ym48g5g87s820vc2fvmdzdl36l1yi6aphhjr5hpxnpcpxs1")))

(define-public crate-egui-macroquad-0.14.0 (c (n "egui-macroquad") (v "0.14.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.14.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.21.0") (d #t) (k 2)))) (h "1ahc07ld9a7b845l5pi73k65ybr35k4gf95hh6hh7m1lkm4mnryg")))

(define-public crate-egui-macroquad-0.15.0 (c (n "egui-macroquad") (v "0.15.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.14.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (k 0)) (d (n "egui_demo_lib") (r "^0.21.0") (d #t) (k 2)))) (h "0x98rq42v79z0a89g6kk0b80sb42gg8cbninx1jfw724mfa40s04") (f (quote (("default" "audio") ("audio" "macroquad/audio"))))))

