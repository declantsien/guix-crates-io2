(define-module (crates-io eg ui egui-modal) #:use-module (crates-io))

(define-public crate-egui-modal-0.1.0 (c (n "egui-modal") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "0z7z38ak42a88yq8jhq8a8pmx37b4ka9cwnxbmzhpqclwz6svg43") (y #t)))

(define-public crate-egui-modal-0.1.1 (c (n "egui-modal") (v "0.1.1") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1dnn577l2mchamy9i8gqpc24splfs9bm6z04ld178hwbm5jlhpqs") (y #t)))

(define-public crate-egui-modal-0.1.2 (c (n "egui-modal") (v "0.1.2") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "112lgh44q4161q3r1nwvri7c2m2bf8zfhfblmh26d0q9vvjjn4ja") (y #t)))

(define-public crate-egui-modal-0.1.3 (c (n "egui-modal") (v "0.1.3") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "0r6xbsmr0dwm9vrr9i2yz04hijpbck79my21b29ldl23finwgjsp") (y #t)))

(define-public crate-egui-modal-0.1.4 (c (n "egui-modal") (v "0.1.4") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "19llfaxryyr96w9kkvy4s7wjwp45ihn5jr64mjr428p58mxvi019")))

(define-public crate-egui-modal-0.1.5 (c (n "egui-modal") (v "0.1.5") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1y6ii4gkqy1b33zicma4iz1cq9cipf6wzblbnlag1hgr628n637p")))

(define-public crate-egui-modal-0.1.6 (c (n "egui-modal") (v "0.1.6") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1qs9kr0vsh4h9z654vrzv9g0dcyb0ly1rnbxggfw3hpjb3bhh13r")))

(define-public crate-egui-modal-0.1.7 (c (n "egui-modal") (v "0.1.7") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1l0rvny3ja3021cvar2zcsffadg9i82jn483mmakr28rz4f9z7m9")))

(define-public crate-egui-modal-0.1.8 (c (n "egui-modal") (v "0.1.8") (d (list (d (n "eframe") (r "^0.20.0") (d #t) (k 2)) (d (n "egui") (r "^0.20.0") (d #t) (k 0)))) (h "1kickphv7h74b468ld1dxrvh3nbqdbk8s05lr93z06rd8dpbaklw")))

(define-public crate-egui-modal-0.2.0 (c (n "egui-modal") (v "0.2.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 2)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)))) (h "1q3klxzpjxjdi4z3bk3g0d84nfbagzb7f6k92q6mr14b90jc6c86")))

(define-public crate-egui-modal-0.2.1 (c (n "egui-modal") (v "0.2.1") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 2)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1xa14qpi8jn2gkfz55k1njc4w4s28v2iy5w56gl7h4qchx5r24da")))

(define-public crate-egui-modal-0.2.2 (c (n "egui-modal") (v "0.2.2") (d (list (d (n "eframe") (r "^0.21.3") (d #t) (k 2)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1pjb26kdxzbw73ic96ygkvgb9gljbl4z75vwmjpkmpzpifl5nppi")))

(define-public crate-egui-modal-0.2.3 (c (n "egui-modal") (v "0.2.3") (d (list (d (n "eframe") (r "^0.21.3") (d #t) (k 2)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1fk4gv35c98y2yj3a5zr748wmjhv7910z55m416yd4kq6496q3wa")))

(define-public crate-egui-modal-0.2.4 (c (n "egui-modal") (v "0.2.4") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "0fb6ggpwpgnfd4ilbjlrbj73mhq1zmwbxclnivffa1ns3m1330w6")))

(define-public crate-egui-modal-0.2.5 (c (n "egui-modal") (v "0.2.5") (d (list (d (n "eframe") (r "^0.23.0") (d #t) (k 2)) (d (n "egui") (r "^0.23.0") (d #t) (k 0)))) (h "1ci6abzvf075sw8wc67hfn3dawkmvglv42hy4pcvsravv7di00lk")))

(define-public crate-egui-modal-0.3.0 (c (n "egui-modal") (v "0.3.0") (d (list (d (n "eframe") (r "^0.24.0") (d #t) (k 2)) (d (n "egui") (r "^0.24.0") (d #t) (k 0)))) (h "15msnkj9zwhng8m54xfzz0wkgz5qx42jhlavqljbwsyg7nbb3r3w")))

(define-public crate-egui-modal-0.3.1 (c (n "egui-modal") (v "0.3.1") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)))) (h "07fhy7q9akflky2khvwbxgs3piim8j2s44jnfknsy9dfx2g0j1pn")))

(define-public crate-egui-modal-0.3.2 (c (n "egui-modal") (v "0.3.2") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)))) (h "12y7k4v2h357dynczb6ps7k6i9f6nnpdzkk9f9564qw8jjad6kay")))

(define-public crate-egui-modal-0.3.3 (c (n "egui-modal") (v "0.3.3") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)))) (h "1izp0g2rb7fw7xq79g1vb7cqh3r5jn4y0sj36m7q70751hp4pr17")))

(define-public crate-egui-modal-0.3.4 (c (n "egui-modal") (v "0.3.4") (d (list (d (n "eframe") (r "^0.26.1") (d #t) (k 2)) (d (n "egui") (r "^0.26.1") (k 0)))) (h "0cc2y9qbr39z8s9hijgd9bs4bhkfiyvb2jk2nkc4zc5ibgjxn5ys")))

(define-public crate-egui-modal-0.3.5 (c (n "egui-modal") (v "0.3.5") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)))) (h "05s1hf2xdjdhb50w8j99ky1cwladd28l4mcw27rg1kc4xqvc4bn4")))

(define-public crate-egui-modal-0.3.6 (c (n "egui-modal") (v "0.3.6") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)))) (h "0wx6kx8l5pnp4hff8igspfafiac2xs712pgpzyjvdnqmzpzdz33k")))

