(define-module (crates-io eg ui egui-dropdown) #:use-module (crates-io))

(define-public crate-egui-dropdown-0.1.0 (c (n "egui-dropdown") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "0gsmnd9pfgscg5lh26v9q04rla4d8hs1jawbnqlc8v2yf80p9y49")))

(define-public crate-egui-dropdown-0.2.0 (c (n "egui-dropdown") (v "0.2.0") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "1dkavc6lvpvnnavqiwinixzaxqbi6kp1z8pghpf2rfpxv80r66v3")))

(define-public crate-egui-dropdown-0.2.1 (c (n "egui-dropdown") (v "0.2.1") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (d #t) (k 0)))) (h "0y5pilzaml7kwrgn875ba2b31i97jc1k85gdzjzn6wz2qjg5f042")))

(define-public crate-egui-dropdown-0.3.0 (c (n "egui-dropdown") (v "0.3.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "15g06j83l3d61md0w755dkz91v1rsm1z96l05qhziis34cfd93v3")))

(define-public crate-egui-dropdown-0.4.0 (c (n "egui-dropdown") (v "0.4.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (d #t) (k 0)))) (h "1xzj2wdapf37jp795zqbb0f26gjiiwr73l9qz68ah59anr2f5x85")))

(define-public crate-egui-dropdown-0.5.0 (c (n "egui-dropdown") (v "0.5.0") (d (list (d (n "eframe") (r "^0.24") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.24") (d #t) (k 0)))) (h "0dfhx7l501xhab4mrq3w8qaxxb0gy8g1k3frj8rh1pz77nacpwbn")))

(define-public crate-egui-dropdown-0.6.0 (c (n "egui-dropdown") (v "0.6.0") (d (list (d (n "eframe") (r "^0.25") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.25") (d #t) (k 0)))) (h "0dnnhbmk56k6grnj1d3y4gscwqb7h4dj67zdw9bwkk5z0q65kl1j")))

(define-public crate-egui-dropdown-0.7.0 (c (n "egui-dropdown") (v "0.7.0") (d (list (d (n "eframe") (r "^0.26.0") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)))) (h "1bcy8qfqp2ib3y7195k8x93x85c3sc2x0agf31swbd3zprkx7m0m")))

(define-public crate-egui-dropdown-0.8.0 (c (n "egui-dropdown") (v "0.8.0") (d (list (d (n "eframe") (r "^0.26.0") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)))) (h "1i013iappk3w5xr6nbcwqibhz4w9lmda0wk99d6m30sny6hlb3y6")))

(define-public crate-egui-dropdown-0.9.0 (c (n "egui-dropdown") (v "0.9.0") (d (list (d (n "eframe") (r "^0.27.0") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.27.0") (d #t) (k 0)))) (h "1c3xyljx2mb5rhq6vh1njqgqgb0124ml92r0fa5d2vscshir83i4")))

