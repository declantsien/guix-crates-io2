(define-module (crates-io eg ui egui_logger) #:use-module (crates-io))

(define-public crate-egui_logger-0.1.0 (c (n "egui_logger") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)))) (h "03rfg4b2f228hm5ssyvyr25lgqmrzwmyv4dvvf20qpflgnla0004")))

(define-public crate-egui_logger-0.2.0 (c (n "egui_logger") (v "0.2.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)))) (h "0jc7gz8jmvvqqjgj8c0dz7wwqw0yn07v4rv4gshx6jy8fk96bw8w")))

(define-public crate-egui_logger-0.2.1 (c (n "egui_logger") (v "0.2.1") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)))) (h "08sy9hnr40frbd0paxkmvfaxyhwn4fx832881b3mbda1y49hys0a")))

(define-public crate-egui_logger-0.2.2 (c (n "egui_logger") (v "0.2.2") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)))) (h "18p2551ajf7rj7rckgf02j45nffsyyrp4mvb32lfdi3lhrzmf951")))

(define-public crate-egui_logger-0.2.3 (c (n "egui_logger") (v "0.2.3") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)))) (h "0xi7lfhy2yq946k6qrlpl4rjvfz9fi3n5v8n2v36z5x59pirfbfm")))

(define-public crate-egui_logger-0.3.0 (c (n "egui_logger") (v "0.3.0") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1w7izvqjaw0zgjgzakqgydnqf6mj2qaswbfn0dsdwlng6dhprckl")))

(define-public crate-egui_logger-0.3.1 (c (n "egui_logger") (v "0.3.1") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1gqn1qgkvm79z6wxrwv2m6asdffhpnxpc5jbxqivmlajfc4p6dh5")))

(define-public crate-egui_logger-0.4.0 (c (n "egui_logger") (v "0.4.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0bacldjsc51if75nwnswwzy288yr9ls5m5j2y28vx6jgian4gsfr")))

(define-public crate-egui_logger-0.4.1 (c (n "egui_logger") (v "0.4.1") (d (list (d (n "eframe") (r "^0.24") (d #t) (k 2)) (d (n "egui") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "01vdlwis52vmy48xwrqd3irysl6vk93gjcjhykbk8q81hadrhgah")))

(define-public crate-egui_logger-0.4.2 (c (n "egui_logger") (v "0.4.2") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1m0kssyri7m2ncdymlyr0xxvc7zy95v16yb9j1f0na3c3qsgspvv")))

(define-public crate-egui_logger-0.4.3 (c (n "egui_logger") (v "0.4.3") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multi_log") (r "^0.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1rk9bkgq6i0ss9nriw11lrwsfw0m3v3hb7n801cgikhpndj4ijv4")))

(define-public crate-egui_logger-0.4.4 (c (n "egui_logger") (v "0.4.4") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multi_log") (r "^0.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "07cfg030zgh3mbgpcfvjkd22naqhmbqa0a7kb8ly0pg5b3wqbklz")))

