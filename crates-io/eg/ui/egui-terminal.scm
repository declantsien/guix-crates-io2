(define-module (crates-io eg ui egui-terminal) #:use-module (crates-io))

(define-public crate-egui-terminal-0.1.0 (c (n "egui-terminal") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "1xdj5594ipfl3lcm2bkhbrfq52d1k6vyvci3y2cifsaxc1kmd0ki")))

