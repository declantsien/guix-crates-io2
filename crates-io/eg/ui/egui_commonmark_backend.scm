(define-module (crates-io eg ui egui_commonmark_backend) #:use-module (crates-io))

(define-public crate-egui_commonmark_backend-0.16.0-alpha.1 (c (n "egui_commonmark_backend") (v "0.16.0-alpha.1") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "egui_extras") (r "^0.27") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10") (o #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("default-fancy"))) (o #t) (k 0)))) (h "1bzp1119iyla6byr06jqbm3i4n0p8hjsfc5ld8rwmzbkjhcdf60m") (s 2) (e (quote (("pulldown-cmark" "dep:pulldown-cmark") ("better_syntax_highlighting" "dep:syntect")))) (r "1.76")))

(define-public crate-egui_commonmark_backend-0.16.0 (c (n "egui_commonmark_backend") (v "0.16.0") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "egui_extras") (r "^0.27") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10") (o #t) (k 0)) (d (n "syntect") (r "^5.0.0") (f (quote ("default-fancy"))) (o #t) (k 0)))) (h "0iqfdwn2vjzwj6wz6n21r31564k8hgrwxkca67lifswz8dl3pnvf") (s 2) (e (quote (("pulldown-cmark" "dep:pulldown-cmark") ("better_syntax_highlighting" "dep:syntect")))) (r "1.76")))

