(define-module (crates-io eg ui egui_minipng) #:use-module (crates-io))

(define-public crate-egui_minipng-0.1.0 (c (n "egui_minipng") (v "0.1.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "minipng") (r "^0.1") (d #t) (k 0)))) (h "10f3mm5c81swc9v41cy2mnkhjp0frhphr33mr3aaiqjdc5jw436c")))

(define-public crate-egui_minipng-0.1.1 (c (n "egui_minipng") (v "0.1.1") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "minipng") (r "^0.1") (d #t) (k 0)))) (h "0bf66hisrws3jsvbs94c8lkb6wzls0m0kdg882gfylm3qdvb51w5") (y #t)))

(define-public crate-egui_minipng-0.2.0 (c (n "egui_minipng") (v "0.2.0") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "minipng") (r "^0.1") (d #t) (k 0)))) (h "0rpz1w61s5d9fzvdapssm8xkz9j89rz2jvqgr6c9f0hb7hpgk17h")))

