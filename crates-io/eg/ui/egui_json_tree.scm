(define-module (crates-io eg ui egui_json_tree) #:use-module (crates-io))

(define-public crate-egui_json_tree-0.1.0 (c (n "egui_json_tree") (v "0.1.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "04y1yp8s5pkx406n607zy79rzldsjjcifv6b5qnpp106gcw2ff5b") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json"))))))

(define-public crate-egui_json_tree-0.2.0 (c (n "egui_json_tree") (v "0.2.0") (d (list (d (n "eframe") (r "^0.24") (d #t) (k 2)) (d (n "egui") (r "^0.24") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1l06kyh7dgw4wjc14qgqfi71l194gmhnih4y986hzxz5mbdlkidn") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json")))) (r "1.72")))

(define-public crate-egui_json_tree-0.3.0 (c (n "egui_json_tree") (v "0.3.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "048krfh9h9s70z1697m05652vyj7sln1k5jm1y54cxndfs57w50z") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json")))) (r "1.72")))

(define-public crate-egui_json_tree-0.4.0 (c (n "egui_json_tree") (v "0.4.0") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0m6pggj2j250g9bsm17ryhipwm72cfqd6vr7zfn75vfwhdm8826d") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json")))) (r "1.72")))

(define-public crate-egui_json_tree-0.5.0 (c (n "egui_json_tree") (v "0.5.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "14hxd7zmidzb5hpza4i6qc6f8djbmvl99lx8ijlqfnxcq8z0r0kv") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("serde_json" "dep:serde_json")))) (r "1.72")))

(define-public crate-egui_json_tree-0.5.1 (c (n "egui_json_tree") (v "0.5.1") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)) (d (n "serde_json") (r "^1.0.104") (o #t) (d #t) (k 0)) (d (n "simd-json") (r "^0.13.8") (o #t) (d #t) (k 0)))) (h "04b11ym48m0z9pn11jb4hqbd6fq7km3l8mgaj91lc0xpm2d0jssl") (f (quote (("default" "serde_json")))) (s 2) (e (quote (("simd_json" "dep:simd-json") ("serde_json" "dep:serde_json")))) (r "1.72")))

