(define-module (crates-io eg ui egui-controls) #:use-module (crates-io))

(define-public crate-egui-controls-0.1.0 (c (n "egui-controls") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0p0nzgfwdcr48bl9vi0rqi9358jlacdcihm5z6vln96z70irp9i4")))

(define-public crate-egui-controls-0.1.1 (c (n "egui-controls") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "05245v6gfb43s8q4yal6a2a50f3a6dizg6j5w2j0g96mrdmvzq6z")))

