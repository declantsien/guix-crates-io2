(define-module (crates-io eg ui egui-tetra) #:use-module (crates-io))

(define-public crate-egui-tetra-0.1.0 (c (n "egui-tetra") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "egui") (r "^0.13.0") (d #t) (k 0)) (d (n "open") (r "^1.7.0") (d #t) (k 0)) (d (n "tetra") (r "^0.6.1") (k 0)))) (h "0g8xddx6w1dk40i9z7ch5awg0qnq2ccajmcacqsyr1gxj1caqrk2")))

(define-public crate-egui-tetra-0.2.0 (c (n "egui-tetra") (v "0.2.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "egui") (r "^0.14.0") (d #t) (k 0)) (d (n "open") (r "^2.0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.6.1") (k 0)))) (h "0fsc020bcp3h96ldmlxi06skf41b0vinq8pyjnpdn6q1hgnv1ycj")))

(define-public crate-egui-tetra-0.2.1 (c (n "egui-tetra") (v "0.2.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "egui") (r "^0.14.0") (d #t) (k 0)) (d (n "open") (r "^2.0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.6.1") (k 0)))) (h "0wvvpbha8lzi89bnknjxrb7qi73lds7mhhfbp0f08idghsxj0n4h")))

(define-public crate-egui-tetra-0.3.0 (c (n "egui-tetra") (v "0.3.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "egui") (r "^0.16.1") (d #t) (k 0)) (d (n "open") (r "^2.0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.6.1") (k 0)))) (h "1yf1vnwrcksf5xwj64fii939bm9ikrsbcv66ifkyd4rhk7apcxn6")))

