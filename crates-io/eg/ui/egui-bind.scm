(define-module (crates-io eg ui egui-bind) #:use-module (crates-io))

(define-public crate-egui-bind-0.1.0 (c (n "egui-bind") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "184npxnigrlvqyszzxxwzcj6dbj8ivfg1w8i64b8nm91iiicb84b")))

(define-public crate-egui-bind-0.2.0 (c (n "egui-bind") (v "0.2.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "0p1sid02yxhij6nkk89is5p0k4i3jb4p6f9k0hcsn0ni1j0y87ar")))

(define-public crate-egui-bind-0.3.0 (c (n "egui-bind") (v "0.3.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "18l3vsj7n3q0iljwxlgzp98011h9dj7ycc8w8vb2ws7649gg9lyq")))

(define-public crate-egui-bind-0.4.0 (c (n "egui-bind") (v "0.4.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v6lpmpj6i86jljbza0r047vzixjm1yzw6ba2pmbkvl1fz1bjw53") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.5.0 (c (n "egui-bind") (v "0.5.0") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jif0i65c5y8ljyhqddzy4vpzqdf76a57ab3spz21k3v3acphgv4") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.5.1 (c (n "egui-bind") (v "0.5.1") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1brwiy403vk595sqb6bgp7mylngiz0gl71ag565wrd67mri96bxi") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.5.2 (c (n "egui-bind") (v "0.5.2") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11qy9sxx81b8q5qhj13ibfydrlbcb136hjrlnnqnhmjr935a4z5v") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.6.0 (c (n "egui-bind") (v "0.6.0") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 2)) (d (n "egui") (r "^0.21") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xvv7vvr2kfg3bdfs80ch3325mxbcmqy1ir9l8mn26c41bvbvk2j") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.7.0 (c (n "egui-bind") (v "0.7.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x0hnsxw3bhd86hrilydwy337gcpr0h5qsh7r5n2gcazdi9f8iqv") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.8.0 (c (n "egui-bind") (v "0.8.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qmb8cb1h439nw3y59df772hfrmk4j6rpzpdpf17jppllvyi76i7") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.9.0 (c (n "egui-bind") (v "0.9.0") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kl5nr0x9qjqp1km0r5frd3sk2l69li04abysqavcy5zazg2zd8r") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.10.0 (c (n "egui-bind") (v "0.10.0") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wxbg4pwr6lq42j1qambsbs4ysr7p8wgjxc7syz8c9ga4ypm1jwc") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.11.0 (c (n "egui-bind") (v "0.11.0") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "115yqa54hxwaiia41yp9rywgphl88szd91angi1w6vahw2y8ywc0") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-bind-0.12.0 (c (n "egui-bind") (v "0.12.0") (d (list (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j3n53cz8c0qia3032rbvyjqppnkmbypr6778pqzycszf1zv2iz4") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

