(define-module (crates-io eg ui egui_gauge) #:use-module (crates-io))

(define-public crate-egui_gauge-0.1.0 (c (n "egui_gauge") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "emath") (r "^0.22") (d #t) (k 0)) (d (n "epaint") (r "^0.22") (d #t) (k 0)))) (h "1g2nis23dy97jy9x109wvwppvrbz3dhcgmciv07cx4n4mc1r8ks1")))

(define-public crate-egui_gauge-0.1.1 (c (n "egui_gauge") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "emath") (r "^0.22") (d #t) (k 0)) (d (n "epaint") (r "^0.22") (d #t) (k 0)))) (h "19bb5xnc0q5q5imi7pj7ibahx1pgz33bngn1cc8r49wvdx82jwa8")))

(define-public crate-egui_gauge-0.1.2 (c (n "egui_gauge") (v "0.1.2") (d (list (d (n "eframe") (r "^0") (d #t) (k 2)) (d (n "egui") (r "^0") (d #t) (k 0)) (d (n "emath") (r "^0") (d #t) (k 0)) (d (n "epaint") (r "^0") (d #t) (k 0)))) (h "04nnkahg1j794wdbrx1s073ddwn0mqhbx0ryz71v07i2d3vpj5sj")))

(define-public crate-egui_gauge-0.1.3 (c (n "egui_gauge") (v "0.1.3") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (d #t) (k 0)) (d (n "emath") (r "^0.25") (d #t) (k 0)) (d (n "epaint") (r "^0.25") (d #t) (k 0)))) (h "1i0l0099cwy8v2bmfci37f1fpf0zjp7c2sjmd26pnahdp8s19fyc")))

(define-public crate-egui_gauge-0.1.4 (c (n "egui_gauge") (v "0.1.4") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "emath") (r "^0.26") (d #t) (k 0)) (d (n "epaint") (r "^0.26") (d #t) (k 0)))) (h "08c9z50b5b33pb2brvapp4hk9hcpgr17jb03gbvghg4sik7nhq6z")))

