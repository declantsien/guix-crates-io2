(define-module (crates-io eg ui egui-notify) #:use-module (crates-io))

(define-public crate-egui-notify-0.1.0 (c (n "egui-notify") (v "0.1.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "1iwkadpxb2j5qckfh3bsrq9qgxsdwrprmjbsxrlmfkyr1n1m985f")))

(define-public crate-egui-notify-0.2.0 (c (n "egui-notify") (v "0.2.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "09llfcwljnxpv08wiqbqy3zqc058xx4ybgdlmlha53a3949f8kh7")))

(define-public crate-egui-notify-0.3.0 (c (n "egui-notify") (v "0.3.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "185mzg9p7w9p2far1kjw6gy1x6w2i837cww6ra5nlsrlndlsg8al")))

(define-public crate-egui-notify-0.4.0 (c (n "egui-notify") (v "0.4.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "1rxcsrlm3654l2lk75fxsy0z074nrr5gdb9qqld72g5lcl7hshy4")))

(define-public crate-egui-notify-0.4.1 (c (n "egui-notify") (v "0.4.1") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "06y0gllympms4baa78gdn76xibn4drwaym3dylcn5bhad10xy2y7")))

(define-public crate-egui-notify-0.4.2 (c (n "egui-notify") (v "0.4.2") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "15zrb49yhbzm643xraq4yilbj9b3bk3ky530zz1kbhf4449fbw1p")))

(define-public crate-egui-notify-0.4.3 (c (n "egui-notify") (v "0.4.3") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "06w83q0jmx0m7m2dmjvyvc6z9cgjcg1bb0cyy764sy477528z58k")))

(define-public crate-egui-notify-0.4.4 (c (n "egui-notify") (v "0.4.4") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)))) (h "1g1pgl71q93vida6s3l3q4cj90srwyn4y92nx00bvimk8wih9xhr")))

(define-public crate-egui-notify-0.5.0 (c (n "egui-notify") (v "0.5.0") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "1m4flr51z9m9wx5yixhxwbxx04cjvsmv6jrfllbbs9grxik7xjv0")))

(define-public crate-egui-notify-0.5.1 (c (n "egui-notify") (v "0.5.1") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 2)) (d (n "egui") (r "^0.20") (d #t) (k 0)))) (h "1xmq99d6wj57nqww2wsp145q2y2xvfzisjp54al2vld00hi5bn53")))

(define-public crate-egui-notify-0.6.0 (c (n "egui-notify") (v "0.6.0") (d (list (d (n "eframe") (r "^0.21") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.21") (k 0)))) (h "15g1c38zp1ghqfmzd1kz21115fgrd7w6hw4362p1dady3vcx51zs")))

(define-public crate-egui-notify-0.7.0 (c (n "egui-notify") (v "0.7.0") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.22") (k 0)))) (h "0wnyin74y783n0piq7pnixnldn68v6s4sy17d81s6arp3bm36dc9")))

(define-public crate-egui-notify-0.8.0 (c (n "egui-notify") (v "0.8.0") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.22") (k 0)))) (h "1j8fxsi2y4fws6d3ld5s01xia7l0yvxm6nw8q0b57hpclk6ikxi2")))

(define-public crate-egui-notify-0.9.0 (c (n "egui-notify") (v "0.9.0") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.22") (k 0)) (d (n "egui-phosphor") (r "^0.2.0") (d #t) (k 2)))) (h "0ykj8yy84i95dlba04psklfwnafx0p1wslicmygink72k31xjznm")))

(define-public crate-egui-notify-0.10.0 (c (n "egui-notify") (v "0.10.0") (d (list (d (n "eframe") (r "^0.23") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.23") (k 0)) (d (n "egui-phosphor") (r "^0.2.0") (d #t) (k 2)))) (h "1z04xpn3m8qhssmrb6vb6cgqnfi3vdy6g64drfpazckv3kr10fyw")))

(define-public crate-egui-notify-0.11.0 (c (n "egui-notify") (v "0.11.0") (d (list (d (n "eframe") (r "^0.24") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.24") (k 0)))) (h "1qd8lkwafvrm1fi2k41ycx2c78cafqnq1blajmlms7b61i8jdnxy")))

(define-public crate-egui-notify-0.12.0 (c (n "egui-notify") (v "0.12.0") (d (list (d (n "eframe") (r "^0.25") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.25") (k 0)))) (h "1s6ibhxzns6dqnljh12p2ndnqq84k1d8hb9jfddcnd9rv488k5sy")))

(define-public crate-egui-notify-0.13.0 (c (n "egui-notify") (v "0.13.0") (d (list (d (n "eframe") (r "^0.26.0") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)))) (h "15dvqc5zmzsy95xfnl74bsvgly72ij9w1wj7ld8i9d412brgzfsa")))

(define-public crate-egui-notify-0.14.0 (c (n "egui-notify") (v "0.14.0") (d (list (d (n "eframe") (r "^0.27.0") (f (quote ("default_fonts" "glow"))) (k 2)) (d (n "egui") (r "^0.27.0") (k 0)))) (h "1pkcszxrm50jw0skvcnvkmfxq1parfwg2fp4vfy1dcbvxvx2g4ri")))

