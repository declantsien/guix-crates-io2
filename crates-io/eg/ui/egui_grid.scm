(define-module (crates-io eg ui egui_grid) #:use-module (crates-io))

(define-public crate-egui_grid-0.0.1 (c (n "egui_grid") (v "0.0.1") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)))) (h "01lz7hmpp36n5gcrxvylgrp958qdzcjsin9i6xx4wqfaypzmh23g") (y #t)))

(define-public crate-egui_grid-0.1.0 (c (n "egui_grid") (v "0.1.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)))) (h "0x1i987f2h6s3xwxik4yjfxm77lbcmka8n0y64a710whvj8qizz2")))

(define-public crate-egui_grid-0.2.0 (c (n "egui_grid") (v "0.2.0") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.22.0") (d #t) (k 0)))) (h "1fjwxwk36s3x78im7b16q031cq33ma72jnrkh4i5yh8qfilvyskz")))

(define-public crate-egui_grid-0.3.0 (c (n "egui_grid") (v "0.3.0") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.23.0") (d #t) (k 0)))) (h "119f2xhrkdy9vsvsa0pjmh7fh9jmmr3sz7qknyzif6yd4zswprix")))

(define-public crate-egui_grid-0.4.0 (c (n "egui_grid") (v "0.4.0") (d (list (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.24.1") (d #t) (k 0)))) (h "0q08g33013wwgz2vfyhmwk9am2ij41264l5f0w7m18h0z4n6iq2x")))

(define-public crate-egui_grid-0.5.0 (c (n "egui_grid") (v "0.5.0") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "egui_extras") (r "^0.27") (d #t) (k 0)))) (h "0qzwb52amzfdhz16pp1mxyr48ahvqch099f863h277kqhin81r3a")))

(define-public crate-egui_grid-0.5.1 (c (n "egui_grid") (v "0.5.1") (d (list (d (n "egui") (r "^0.27.2") (d #t) (k 0)) (d (n "egui_extras") (r "^0.27.2") (d #t) (k 0)))) (h "0ddj3vsgmkr1ryd1ahcmpfc5yzrv6xa43xksv8p3ixgzlmvif9n0")))

