(define-module (crates-io eg ui egui_node_graph) #:use-module (crates-io))

(define-public crate-egui_node_graph-0.1.0 (c (n "egui_node_graph") (v "0.1.0") (d (list (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lavadq4zg322m7znjq11fj0cz0r9hqn3mnkqmbwqsgaq9lpzz27") (f (quote (("persistence" "serde" "slotmap/serde" "smallvec/serde"))))))

(define-public crate-egui_node_graph-0.2.0 (c (n "egui_node_graph") (v "0.2.0") (d (list (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wj7am47swvjxnbj2mvf5br78kbr6wdwwrnyfq2yc9dlml0phs0x") (f (quote (("persistence" "serde" "slotmap/serde" "smallvec/serde"))))))

(define-public crate-egui_node_graph-0.3.0 (c (n "egui_node_graph") (v "0.3.0") (d (list (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d90ipy64ypaqwwc8ccd3srp0bjhklj7wmnby232nbdwb4fh0yhp") (f (quote (("persistence" "serde" "slotmap/serde" "smallvec/serde" "egui/persistence"))))))

(define-public crate-egui_node_graph-0.4.0 (c (n "egui_node_graph") (v "0.4.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wcsap17ddy52rbg2gz18acfri2ridny4dkn2f3mfymjyf69wxfh") (f (quote (("persistence" "serde" "slotmap/serde" "smallvec/serde" "egui/persistence"))))))

