(define-module (crates-io eg ui egui_animation) #:use-module (crates-io))

(define-public crate-egui_animation-0.1.0 (c (n "egui_animation") (v "0.1.0") (d (list (d (n "eframe") (r ">=0.22") (d #t) (k 2)) (d (n "egui") (r ">=0.22") (k 0)) (d (n "hello_egui_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simple-easing") (r "^1") (d #t) (k 0)))) (h "0vsz02rkqkmsamx373003fhk0v1ppyr2jll311kgi0zxcv7yyq3b")))

(define-public crate-egui_animation-0.2.0 (c (n "egui_animation") (v "0.2.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)) (d (n "hello_egui_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simple-easing") (r "^1") (d #t) (k 0)))) (h "0x51i0hk64jxlkqp555pyz4yggibw26g17d8ng71x4l56vcfpk79")))

(define-public crate-egui_animation-0.3.0 (c (n "egui_animation") (v "0.3.0") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "hello_egui_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simple-easing") (r "^1") (d #t) (k 0)))) (h "0pcpwgifvz00qzppc0nkilqbv56hvfs90czvqv281mx6rf02byj0")))

(define-public crate-egui_animation-0.4.0 (c (n "egui_animation") (v "0.4.0") (d (list (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "hello_egui_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simple-easing") (r "^1") (d #t) (k 0)))) (h "1143abk4cp2yama9jp3sscpi9xfba083swph2lvw0hw20sl0hj6a")))

