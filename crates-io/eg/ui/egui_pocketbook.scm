(define-module (crates-io eg ui egui_pocketbook) #:use-module (crates-io))

(define-public crate-egui_pocketbook-0.1.0 (c (n "egui_pocketbook") (v "0.1.0") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "epi") (r "^0.13.0") (d #t) (k 0)) (d (n "inkview-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0hz4zlslcjbf5qbqn8ivfqz2ygxl96kkc471l6v25n5ayfnf6m2v")))

(define-public crate-egui_pocketbook-0.1.1 (c (n "egui_pocketbook") (v "0.1.1") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "epi") (r "^0.13.0") (d #t) (k 0)) (d (n "inkview-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1hisd55z824gc40f7b68bhmaa4a5pm2nr8vc3zk6lhyl4ds1gnhh")))

