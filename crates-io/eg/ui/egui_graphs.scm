(define-module (crates-io eg ui egui_graphs) #:use-module (crates-io))

(define-public crate-egui_graphs-0.0.1 (c (n "egui_graphs") (v "0.0.1") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "15630r7dp11hbvsrcdgpn5s26mlk866h36c7invrvjcwmpiqpryb")))

(define-public crate-egui_graphs-0.0.2 (c (n "egui_graphs") (v "0.0.2") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0mh1w0r74aq2rmfszabq15gyl76rkbcryrj3hibn28inrn1ashsl")))

(define-public crate-egui_graphs-0.0.3 (c (n "egui_graphs") (v "0.0.3") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0adb91ap8dq2mr8nlhcczzsrcb3jnjn588f8k0d05xdb1r8cbk4g")))

(define-public crate-egui_graphs-0.0.4 (c (n "egui_graphs") (v "0.0.4") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1dqw7479zzqcb7f2cb1fshb9y4vwryc4zrm7b1gdjbxflkapv3nq")))

(define-public crate-egui_graphs-0.0.5 (c (n "egui_graphs") (v "0.0.5") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "03rsv220khhr5fir5qk021r3baqgpw2kkfw5l25jkilqv3fakcwl")))

(define-public crate-egui_graphs-0.0.6 (c (n "egui_graphs") (v "0.0.6") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1bn425d98z68rmg5j1l9nk7gsskd6n4647zs11xrjjaar9qyq95x")))

(define-public crate-egui_graphs-0.0.7 (c (n "egui_graphs") (v "0.0.7") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "02nqm6la5hkzagzzzcn23d6dyyppc4vwa1hw6jws04y58hi8cxpz")))

(define-public crate-egui_graphs-0.0.8 (c (n "egui_graphs") (v "0.0.8") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "fdg-sim") (r "^0.9.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "19phv9xws1sgvghz5fmnddzhw4hkd0dq7x2d0kqg5m22p942rhzz")))

(define-public crate-egui_graphs-0.0.9 (c (n "egui_graphs") (v "0.0.9") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0k800idpc5v6j5m06wycd9fbispdpz3srr1l3n3izgbwxdg9lnn2")))

(define-public crate-egui_graphs-0.0.10 (c (n "egui_graphs") (v "0.0.10") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0vsgw0df4qbp8wrw6f5x2b0a6v09jnb81141ab074ppgb9fn4hgf")))

(define-public crate-egui_graphs-0.0.11 (c (n "egui_graphs") (v "0.0.11") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0s889w70gqplws8gri0sgjj0biabk2wy8xn2ccbkv5s2cyfd1vl9")))

(define-public crate-egui_graphs-0.0.12 (c (n "egui_graphs") (v "0.0.12") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0y09mwjfdjv8xl7qlf882zmzc0bkjwvnkxnk7ggwkrp3mvhfbfmd")))

(define-public crate-egui_graphs-0.0.13 (c (n "egui_graphs") (v "0.0.13") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1r1hbl7qfjr9gn71ckmfdqhrb32mjmhyn4zsd7r7598s11rgd94q")))

(define-public crate-egui_graphs-0.0.14 (c (n "egui_graphs") (v "0.0.14") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "03xgwvvamnwcynppzmqxa9h92mpk8xis22zm7zsigrvq4zgympi2")))

(define-public crate-egui_graphs-0.0.15 (c (n "egui_graphs") (v "0.0.15") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0vm1lr62fv2nxx7lfpsbqxax8pb4hnj44cqhqzav254d2vn27fn7")))

(define-public crate-egui_graphs-0.0.16 (c (n "egui_graphs") (v "0.0.16") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1h2zj3j3p75q3naq49nhdyk4kz98wwq1j0a3rx8rkjqwbqaafma1")))

(define-public crate-egui_graphs-0.0.17 (c (n "egui_graphs") (v "0.0.17") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "0hhf1n5mp7ypivnv5xb7dgx0d98pjg9nkgmnv06y4g4adxs7p5p4")))

(define-public crate-egui_graphs-0.0.18 (c (n "egui_graphs") (v "0.0.18") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1l8cb96n508wwsd9k19b62cpcadamv95nfr3rnx68b6rw8ws748p")))

(define-public crate-egui_graphs-0.0.19 (c (n "egui_graphs") (v "0.0.19") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1x5ci0b6pbal5pqnl4dckkvkpi54kn1lmkzi6alzbjn9qrn409vl")))

(define-public crate-egui_graphs-0.0.20 (c (n "egui_graphs") (v "0.0.20") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "0f0yqiwzchabsa631c2vwbabrdb06prn8mgzk3nbsia3m7v12dnz")))

(define-public crate-egui_graphs-0.0.21 (c (n "egui_graphs") (v "0.0.21") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "11gpvvkha10pwh022gvkxa726cfpyh0rr8xh9mdx1zr64wjji5qw")))

(define-public crate-egui_graphs-0.0.22 (c (n "egui_graphs") (v "0.0.22") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "0nsz8iyh2qsl8p5lzrkcpgx61ri6h07ra2kcrbxpimdjyga63a62")))

(define-public crate-egui_graphs-0.0.23 (c (n "egui_graphs") (v "0.0.23") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "0qkvkzziwlwn9xhwh8yznymrb32f12icxmdpgqfwypir927ahqim")))

(define-public crate-egui_graphs-0.1.0 (c (n "egui_graphs") (v "0.1.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "1ximda19rrfx9r1j6zb0ddsl24ykxc4flk3f5gligylra1hnln54")))

(define-public crate-egui_graphs-0.1.1 (c (n "egui_graphs") (v "0.1.1") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1dwgfy2xlvxzcjss4cajsa9vcmqf6hn8a1a74cpk36ykkvw70j83")))

(define-public crate-egui_graphs-0.1.2 (c (n "egui_graphs") (v "0.1.2") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1v17m374j2vy03y7rb797j8279niypxc9lk2ygm92dgkqi7b9imn")))

(define-public crate-egui_graphs-0.1.3 (c (n "egui_graphs") (v "0.1.3") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0r16gl3ip0hjqczcl1rwfv0sszw87zafmkrwsmr2k4cl3wg6lgli")))

(define-public crate-egui_graphs-0.1.4 (c (n "egui_graphs") (v "0.1.4") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "14wv30dap3lm9cybspwzn1s0h2q94c2zwq6122q0527qr70wcmp6")))

(define-public crate-egui_graphs-0.2.0 (c (n "egui_graphs") (v "0.2.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0zhibl397cgfd6a0gy3g5zdz9yd6a0h4rc62fayharw7q53gxzqs")))

(define-public crate-egui_graphs-0.3.0 (c (n "egui_graphs") (v "0.3.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "16mrp21hsdjxp9l8wjy8rbivlav7lmy1p1kix99zfi38a5rjzcc4")))

(define-public crate-egui_graphs-0.3.1 (c (n "egui_graphs") (v "0.3.1") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0gvmb374982jkk1vxr16gxi26a32fzkfsiq9w23n5qram237dbps")))

(define-public crate-egui_graphs-0.4.0 (c (n "egui_graphs") (v "0.4.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0vapcwmsiqlvlxs578wn6d9f3rdlp8zngmckyjmqjc9a3sg49bdn")))

(define-public crate-egui_graphs-0.4.1 (c (n "egui_graphs") (v "0.4.1") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06wv6b5jy62p71hskqb4rxqzg5dlyr1qd0ppw210cnrnn2rmqp12")))

(define-public crate-egui_graphs-0.4.2 (c (n "egui_graphs") (v "0.4.2") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0srym8yg1idhghjsfmjf07nm2slwxbq5a6vqlhzj65br7z7hzxka")))

(define-public crate-egui_graphs-0.4.3 (c (n "egui_graphs") (v "0.4.3") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1dcsskqsgamrzqn9gyfs8vxwb9z4kz24h3lpfhy07qwxxlslcsw5")))

(define-public crate-egui_graphs-0.4.4 (c (n "egui_graphs") (v "0.4.4") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "11dkdmhh313w0z5i13bykik2km4f5xflwj5qd5r15b42vgpb1hsx")))

(define-public crate-egui_graphs-0.4.5 (c (n "egui_graphs") (v "0.4.5") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ay20l7drcdi1ajznp091hjq8v0wc6x5isqn7nzkxkbr6i2lw40x")))

(define-public crate-egui_graphs-0.4.6 (c (n "egui_graphs") (v "0.4.6") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06bnnra8m4yzr5jddlcf6npz8r802qi7svhyfmzysvv86s9wfvd9")))

(define-public crate-egui_graphs-0.4.7 (c (n "egui_graphs") (v "0.4.7") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1dg4n6c9ksgb8x7glm1k829pizlmwrwjx70rgl2vhl7j47pisk9s")))

(define-public crate-egui_graphs-0.5.0 (c (n "egui_graphs") (v "0.5.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0lp9wyfwcmv4rw7hnw3jz64hyj7p62vqf6wx9hsim2370sygcbca")))

(define-public crate-egui_graphs-0.5.1 (c (n "egui_graphs") (v "0.5.1") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16kp5nbx79fvy88lmzczrimyhaccrsqqyiddzlvpys4ac0jhym23")))

(define-public crate-egui_graphs-0.5.2-beta.0 (c (n "egui_graphs") (v "0.5.2-beta.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1rbg93a4h87bxbidsxh76gvxiyi3m3q6pax4igwrw657gvilj4rg")))

(define-public crate-egui_graphs-0.5.2-beta.1 (c (n "egui_graphs") (v "0.5.2-beta.1") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16l6vhxxkq8zc9dhg60fjjyarwqhjr42cxspnp6wgjy29sb87q6y")))

(define-public crate-egui_graphs-0.5.2 (c (n "egui_graphs") (v "0.5.2") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06lmfsdcy2a0syqia0mgv5wxmh1ycilf2fwgd639377azj3j3d6r")))

(define-public crate-egui_graphs-0.5.3-beta.0 (c (n "egui_graphs") (v "0.5.3-beta.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kbcg1ssjr7f98xg261qndh8jdx5j7p3dzjblkybkvyc9i113gdl")))

(define-public crate-egui_graphs-0.5.3 (c (n "egui_graphs") (v "0.5.3") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dq3g9hi6sfh9dc7cjcg0mk919cbbaxi4li68x3flpsf4zd78fib")))

(define-public crate-egui_graphs-0.5.4-beta.0 (c (n "egui_graphs") (v "0.5.4-beta.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1196xs7wi7hrgg2488msx8rl6hz9fjzdgyr7yyydzljl8vbqsbby")))

(define-public crate-egui_graphs-0.5.4-beta.1 (c (n "egui_graphs") (v "0.5.4-beta.1") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0slgds80c05vnrk26c0yjqliqzi5850iy48a302is37ydpk4vb5v")))

(define-public crate-egui_graphs-0.5.4-beta.2 (c (n "egui_graphs") (v "0.5.4-beta.2") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0p54zkk8lq8hv93hlkv7znlmif4w5jyz3g50gzz86f12cyjvas83")))

(define-public crate-egui_graphs-0.5.4-beta.3 (c (n "egui_graphs") (v "0.5.4-beta.3") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0w9awhbmapv6qrskwbc3hzjqsda8zsi52y04vj0rlsl18j89j5lb")))

(define-public crate-egui_graphs-0.5.4 (c (n "egui_graphs") (v "0.5.4") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19wfafmfjhpvn7gm4bw3qvlwk17088nxbqxv8rpmc8hnx6xn8gdj")))

(define-public crate-egui_graphs-0.6.0 (c (n "egui_graphs") (v "0.6.0") (d (list (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bf2r52d6gc677dg9x7dq2szcg8hxsklbwgg0farasgl2sxjdx1h")))

(define-public crate-egui_graphs-0.7.0 (c (n "egui_graphs") (v "0.7.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1d1jyxcl781cpbc7rsyxy79fw4h898442wrh0g76rv6n9hfvb71m")))

(define-public crate-egui_graphs-0.7.1 (c (n "egui_graphs") (v "0.7.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0c3h9gfzys6akiz0pwy1wf616jx8w7gynwi7gqlzx6fmdb7gawm9")))

(define-public crate-egui_graphs-0.7.2 (c (n "egui_graphs") (v "0.7.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0g8gmmplnhaa3y3d7ffnhgb7r864in51pc23amkbip97j1wlbk8a")))

(define-public crate-egui_graphs-0.7.3 (c (n "egui_graphs") (v "0.7.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ynxfn96flj6h49fimw92g44slbck34sh1alm1aapdmhih4abil2")))

(define-public crate-egui_graphs-0.7.4 (c (n "egui_graphs") (v "0.7.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19lbdy671xc29s4x22g4jw15wfm32pilpgjbx35p4s1bwnqbw7b4")))

(define-public crate-egui_graphs-0.7.5 (c (n "egui_graphs") (v "0.7.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yi7f12nrrpgq8jmd72rvsjw07br1mdw9sp9igv0fgyc4zixi884")))

(define-public crate-egui_graphs-0.7.6 (c (n "egui_graphs") (v "0.7.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04fxqykjmjhh5d61wcvdn83pwkmi4b3p9h1phwaf3pv2risy2fxc")))

(define-public crate-egui_graphs-0.8.0 (c (n "egui_graphs") (v "0.8.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "032w474dikiai3b2dmxj07hjm9kg00p961mpph3943r47596cvf0")))

(define-public crate-egui_graphs-0.9.0 (c (n "egui_graphs") (v "0.9.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rxs2k7jk0k3z9grm9ic12l4pd5f92czpwxmv3p0z8dj5fymr40q")))

(define-public crate-egui_graphs-0.10.0 (c (n "egui_graphs") (v "0.10.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0zxaf0scwm9ykih7v2pzqh1fz75lmgl0z3ykm231y9d8bif02r4d")))

(define-public crate-egui_graphs-0.11.0 (c (n "egui_graphs") (v "0.11.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1hdddiqyp092ba4sjzp3rvzs4717c9jwfqiprjj46drsh42ndflq")))

(define-public crate-egui_graphs-0.12.0 (c (n "egui_graphs") (v "0.12.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0r286sdxb6dw1lr2n3mampmbwhijlkaack87xxdn0smlb3nnn0m0")))

(define-public crate-egui_graphs-0.13.0 (c (n "egui_graphs") (v "0.13.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0657ijx0afvh5b01avdryigffhwrgm5zpygvb9965ndryzzk8xna")))

(define-public crate-egui_graphs-0.13.1 (c (n "egui_graphs") (v "0.13.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1dqgskpvv8j68i5x3jzaxxz18zjsvlji86bxp0n822na0raigybq")))

(define-public crate-egui_graphs-0.13.2 (c (n "egui_graphs") (v "0.13.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fpv19r3nhxdcf8mxznnav0r4y83gq4ns8idmfc9hhbzpqlfncb3")))

(define-public crate-egui_graphs-0.13.3 (c (n "egui_graphs") (v "0.13.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bgcsxdxyislnbp9cb8208mbccv1nha2psiqj6i2481d8cynmf99") (s 2) (e (quote (("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.14.0 (c (n "egui_graphs") (v "0.14.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17sg6gz8m8m1fai9lnj4yzzydrn5495181pxfj39ywkwm97aa57i") (s 2) (e (quote (("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.15.0 (c (n "egui_graphs") (v "0.15.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17f5gircq3n5hbknhbjilym7wgfjfrai6dqi9p85s5hrsnmgy6ck") (s 2) (e (quote (("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.16.0-beta.0 (c (n "egui_graphs") (v "0.16.0-beta.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1l8r858fq3854hlnvkd9hl59wdgj9shb352mlzrvqywx60xivibx") (s 2) (e (quote (("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.16.0 (c (n "egui_graphs") (v "0.16.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0sv0igd652b5jxc7xjd456577xyagcvzwri304wvlyc3ydq2d2pi") (s 2) (e (quote (("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.17.0 (c (n "egui_graphs") (v "0.17.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.24") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0f4z4lj5glc8dxjzhcjhq4dizn5i2khfn9s7bkkdfmzr7wiv9a21") (s 2) (e (quote (("serde" "dep:serde" "egui/serde" "petgraph/serde" "petgraph/serde-1") ("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.17.1 (c (n "egui_graphs") (v "0.17.1") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.24") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04r5wcyg6v5vb6qzssshnwadqin15cjbjcs1l92jpm2pjvprklvf") (s 2) (e (quote (("serde" "dep:serde" "egui/serde" "petgraph/serde" "petgraph/serde-1") ("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.18.0 (c (n "egui_graphs") (v "0.18.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.25") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16amc1dw0kizw4867mn9lkpl4fh0n5yi0b69dd8h5m70r00b7ngl") (s 2) (e (quote (("serde" "dep:serde" "egui/serde" "petgraph/serde" "petgraph/serde-1") ("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.19.0 (c (n "egui_graphs") (v "0.19.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "107293jxq4n5nb6g1mrxdffikzygl5yxczg0fchlvrfm8jdlmp0j") (s 2) (e (quote (("serde" "dep:serde" "egui/serde" "petgraph/serde" "petgraph/serde-1") ("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

(define-public crate-egui_graphs-0.20.0 (c (n "egui_graphs") (v "0.20.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.27") (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("stable_graph" "matrix_graph"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1419q99z3q6yh9ar988476g9pdhzjann04szl7xrxbyb2c4xdwvz") (s 2) (e (quote (("serde" "dep:serde" "egui/serde" "petgraph/serde" "petgraph/serde-1") ("events" "dep:serde" "dep:serde_json" "dep:crossbeam") ("egui_persistence" "dep:serde"))))))

