(define-module (crates-io eg ui egui_pull_to_refresh) #:use-module (crates-io))

(define-public crate-egui_pull_to_refresh-0.1.0 (c (n "egui_pull_to_refresh") (v "0.1.0") (d (list (d (n "eframe") (r ">=0.22") (d #t) (k 2)) (d (n "egui") (r ">=0.22") (k 0)) (d (n "egui_inbox") (r "^0.1.1") (d #t) (k 2)) (d (n "ehttp") (r "^0.3.1") (d #t) (k 2)))) (h "027795dxfrf20hy6fzjln3w6vmx0hi871rl9ib21cffq5niqvkaq")))

(define-public crate-egui_pull_to_refresh-0.1.1 (c (n "egui_pull_to_refresh") (v "0.1.1") (d (list (d (n "eframe") (r ">=0.22") (d #t) (k 2)) (d (n "egui") (r ">=0.22") (k 0)) (d (n "egui_inbox") (r "^0.1.1") (d #t) (k 2)) (d (n "ehttp") (r "^0.3.1") (d #t) (k 2)))) (h "0gacmnqi6wlysj7g389am6a11yrz3lp435mlkcdc83q4p464yr2h")))

(define-public crate-egui_pull_to_refresh-0.2.0 (c (n "egui_pull_to_refresh") (v "0.2.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)) (d (n "egui_inbox") (r "^0.2.0") (d #t) (k 2)) (d (n "ehttp") (r "^0.3.1") (d #t) (k 2)))) (h "1ln9a1wfw4snk8g56rq293522jnk63nsjlva5mjr8jvrss742q5f")))

(define-public crate-egui_pull_to_refresh-0.3.0 (c (n "egui_pull_to_refresh") (v "0.3.0") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "egui_inbox") (r "^0.3.0") (d #t) (k 2)) (d (n "ehttp") (r "^0.3.1") (d #t) (k 2)))) (h "01iix081y0260h1d6862azgl9q9dryhar1cxpvcpza4v7p1kg05z")))

(define-public crate-egui_pull_to_refresh-0.4.0 (c (n "egui_pull_to_refresh") (v "0.4.0") (d (list (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "egui_inbox") (r "^0.4.0") (d #t) (k 2)) (d (n "ehttp") (r "^0.3.1") (d #t) (k 2)))) (h "0x9wwdlcy2r83x7p7v7k3pl6h9wz30wvfx5dsvv0sxpw2c2xajf6")))

