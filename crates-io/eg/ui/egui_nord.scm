(define-module (crates-io eg ui egui_nord) #:use-module (crates-io))

(define-public crate-egui_nord-0.1.0 (c (n "egui_nord") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "1yk8cfrad2ghpgkxzw32l4zybrsivw95gk040lzghzd2r5aln5xz")))

(define-public crate-egui_nord-0.1.1 (c (n "egui_nord") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "0fzy30mcgvzzkv27h09cvl8bfmgppj5rbmf8x8ah7w0xa2dvpq3n")))

(define-public crate-egui_nord-0.1.2 (c (n "egui_nord") (v "0.1.2") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "0wicw5i30xhf7lxwvj5abwp6ydpk2zkd8kn1dg3a4v8ppi6cbvmr")))

(define-public crate-egui_nord-0.1.3 (c (n "egui_nord") (v "0.1.3") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "1pf2gggj8gyjj9d61ni3x8xwxcwv25l3k2c9y1fgf50ff0hyyhvg")))

(define-public crate-egui_nord-0.1.4 (c (n "egui_nord") (v "0.1.4") (d (list (d (n "eframe") (r "^0.23.0") (d #t) (k 0)))) (h "0ijz79ifp19bnb43j0f6iidkmcimhgkxckfjp8300j2nq1al97g2")))

(define-public crate-egui_nord-0.1.5 (c (n "egui_nord") (v "0.1.5") (d (list (d (n "eframe") (r "^0.23.0") (d #t) (k 0)))) (h "04s1qhn0b2gx8pbn5v6ll6yv04l2p7yk88jyiwg6xwha132jw783")))

