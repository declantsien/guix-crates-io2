(define-module (crates-io eg ui egui-macroquad-fork) #:use-module (crates-io))

(define-public crate-egui-macroquad-fork-0.1.0 (c (n "egui-macroquad-fork") (v "0.1.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-miniquad") (r "^0.14.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)))) (h "133vfh3l1474dly3w551hqvzpj2l7pr0jg51v8y998v2xz7jq10b")))

