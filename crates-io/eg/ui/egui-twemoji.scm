(define-module (crates-io eg ui egui-twemoji) #:use-module (crates-io))

(define-public crate-egui-twemoji-0.1.0 (c (n "egui-twemoji") (v "0.1.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)) (d (n "egui_extras") (r "^0.27") (f (quote ("svg"))) (d #t) (k 2)) (d (n "emojis") (r "^0.6.1") (d #t) (k 0)) (d (n "twemoji-assets") (r "^1.3.0") (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1v3322kz800sf7r5ql0j2f4irc12jldh1bxdmbjrhx3ky76pal7k") (f (quote (("svg" "twemoji-assets/svg") ("png" "twemoji-assets/png") ("default" "svg"))))))

(define-public crate-egui-twemoji-0.2.0 (c (n "egui-twemoji") (v "0.2.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)) (d (n "egui_extras") (r "^0.27") (f (quote ("svg"))) (d #t) (k 2)) (d (n "twemoji-assets") (r "^1.3.0") (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1iyai2b2lr17h1nsmrly84rzmbxrdfbzfkdibvhk02kc5bgmy25v") (f (quote (("svg" "twemoji-assets/svg") ("png" "twemoji-assets/png") ("default" "svg"))))))

(define-public crate-egui-twemoji-0.3.0 (c (n "egui-twemoji") (v "0.3.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)) (d (n "egui_extras") (r "^0.27") (f (quote ("svg"))) (d #t) (k 2)) (d (n "twemoji-assets") (r "^1.3.0") (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0zbk1f9y550c72gkk6vqvy8ywm9zsvq00l901qcikpazfzggkc9p") (f (quote (("svg" "twemoji-assets/svg") ("png" "twemoji-assets/png") ("default" "svg"))))))

