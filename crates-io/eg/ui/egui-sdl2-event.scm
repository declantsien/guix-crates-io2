(define-module (crates-io eg ui egui-sdl2-event) #:use-module (crates-io))

(define-public crate-egui-sdl2-event-0.1.0 (c (n "egui-sdl2-event") (v "0.1.0") (d (list (d (n "egui") (r "^0.18.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0c5qf5acc6nwm4lsc0kbzqp1mwhif41iirk9q601i6kh1rdngqbr")))

(define-public crate-egui-sdl2-event-0.2.0 (c (n "egui-sdl2-event") (v "0.2.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "01gfcq3jgri9z845f05hlvbkjvnk9ksaii1mlyv4p4w8kgni8m4c")))

(define-public crate-egui-sdl2-event-0.2.1 (c (n "egui-sdl2-event") (v "0.2.1") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0dylzpqxv3p9rpp393f4cajf3dwh1ky8s2apjc1dwbkcc24cijdk")))

(define-public crate-egui-sdl2-event-0.2.2 (c (n "egui-sdl2-event") (v "0.2.2") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0nad1vqwcv6qm7f39c4fxybsrylsxpsbxmbahllg6b7g5imciv3q")))

(define-public crate-egui-sdl2-event-0.3.0 (c (n "egui-sdl2-event") (v "0.3.0") (d (list (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)))) (h "0m2rprjn162l6726hpr0i4lhkf87ccv98wihy11lmj6grmqnr349")))

