(define-module (crates-io eg ui egui_inspect_derive) #:use-module (crates-io))

(define-public crate-egui_inspect_derive-0.1.0 (c (n "egui_inspect_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "derive" "parsing"))) (d #t) (k 0)))) (h "1gjz1jq4h05mfhldnmmhq18q1vwc3bcpkfnjl2lh4pg1ssdmxmyb")))

(define-public crate-egui_inspect_derive-0.1.1 (c (n "egui_inspect_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "derive" "parsing"))) (d #t) (k 0)))) (h "02y1jda2h9nmbr28dgzk8wpzv3jwrdzxcxj09ncxwnavq3wr6kfs")))

(define-public crate-egui_inspect_derive-0.1.2 (c (n "egui_inspect_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "derive" "parsing"))) (d #t) (k 0)))) (h "126whik0qpml6z4asb91jc15ij4xp6ylhhfhvdl6kjh2dkll4kds")))

(define-public crate-egui_inspect_derive-0.1.3 (c (n "egui_inspect_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "derive" "parsing"))) (d #t) (k 0)))) (h "0svs6mv8m8bgpb1fpp4lfflxn99bxxij2qqr5lfldz83r3p88kv7")))

