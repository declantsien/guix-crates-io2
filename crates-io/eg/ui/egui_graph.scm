(define-module (crates-io eg ui egui_graph) #:use-module (crates-io))

(define-public crate-egui_graph-0.1.0 (c (n "egui_graph") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.20.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 2)))) (h "0cdbmmgp47hjpmm3jdm5zan873hh04kaz3qq18r7px8n21fk7nig")))

(define-public crate-egui_graph-0.2.0 (c (n "egui_graph") (v "0.2.0") (d (list (d (n "bevy") (r "^0.11.0") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.21.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 2)))) (h "1hb71xdg2mr1qky0m6hrjk60iiwnf7hljicqyfsfjfj9f2q1q6hs")))

(define-public crate-egui_graph-0.3.0 (c (n "egui_graph") (v "0.3.0") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (o #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "layout-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lcvdx25mbvfn6qv4rqi8jb7r3s0njzx382n3nhbvhy62vkg2lr3") (f (quote (("layout" "layout-rs") ("default" "layout" "serde")))) (s 2) (e (quote (("serde" "egui/serde" "dep:serde"))))))

