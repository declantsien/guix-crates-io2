(define-module (crates-io eg ui egui-keybind) #:use-module (crates-io))

(define-public crate-egui-keybind-0.1.0 (c (n "egui-keybind") (v "0.1.0") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "05kybjas1ycp915mqy10vmiywxpwahs30r1z6xxg6iaq1rjx2ns6")))

(define-public crate-egui-keybind-0.2.0 (c (n "egui-keybind") (v "0.2.0") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "05pl69zys3sjx53b8i6b0b2zl9wjwfz0hzb62ymkilksbw0r0ph8")))

(define-public crate-egui-keybind-0.2.1 (c (n "egui-keybind") (v "0.2.1") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p1f62wjr7dx8av8hi189hanf6a75kf1k00simn90ak7siwhg7nl")))

(define-public crate-egui-keybind-0.2.2 (c (n "egui-keybind") (v "0.2.2") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00584gbk9gfw1p7jkmk4rac24jnw84804cw53s18x3bd3hnhj95m") (f (quote (("serde-support" "serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.3 (c (n "egui-keybind") (v "0.2.3") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dqxjj1dplqkz5aixiw7h0mmv1v5nsgbhv30pfvhn6ccm70h51pp") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.4 (c (n "egui-keybind") (v "0.2.4") (d (list (d (n "eframe") (r "^0.24.1") (d #t) (k 2)) (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15fp7fzpbhmln2x2ywkpbi1xachb9xggid5kbm70fgj85sp77hk1") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.5 (c (n "egui-keybind") (v "0.2.5") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06q7p9375kr4idbl6visg2vx5bscxjs070mdfk3icpd8sayzrn2a") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.6 (c (n "egui-keybind") (v "0.2.6") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kj786p5bqj06lhiwxahgzsac0lhj896zh1viflihv0sjdj87wp9") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.7 (c (n "egui-keybind") (v "0.2.7") (d (list (d (n "eframe") (r "^0.26.1") (d #t) (k 2)) (d (n "egui") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06q8vnpmswiy6si4x4r37czwq28gsjazvmm91wyydl6i3rrz2vpi") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.8 (c (n "egui-keybind") (v "0.2.8") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yfhzzpzj2ipqqk8807d1ngf8p7gbgyml27agji2xggqi924v00i") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.2.9 (c (n "egui-keybind") (v "0.2.9") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f1jd76ig0ap1jwr3ll6mqflh5f3nz9pg4kbdyp72c3sm94zjhr5") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui-keybind-0.3.0 (c (n "egui-keybind") (v "0.3.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k57a8ca7bnr5jx7jnp2wrzqjjm1qvv5w44g0l0181vgrarz08l7") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

