(define-module (crates-io eg ui egui_gl_glfw) #:use-module (crates-io))

(define-public crate-egui_gl_glfw-0.1.0 (c (n "egui_gl_glfw") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.4") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.55") (d #t) (k 0)))) (h "0x8l25d9rfh18k7xqrp8kcdadikw8g85rxvzacx0d1miavs4vgmp") (f (quote (("default" "clipboard"))))))

(define-public crate-egui_gl_glfw-0.1.1 (c (n "egui_gl_glfw") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.4") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.55") (d #t) (k 0)))) (h "1riqy2si50x8ijzwggwnrmc96435268djdy2xpy1cd3mdf4b1nlk") (f (quote (("default" "clipboard"))))))

(define-public crate-egui_gl_glfw-0.1.2 (c (n "egui_gl_glfw") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.4") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.55") (k 0)))) (h "117c92yzc6gxczwamvmga5n2iqmzdfq8bfpxks26g0hxhhh8ibns") (f (quote (("wayland" "glfw/glfw-sys" "glfw/wayland") ("rwh-06" "glfw/raw-window-handle-v0-6") ("default" "clipboard"))))))

