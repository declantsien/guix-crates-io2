(define-module (crates-io eg ui egui-gizmo) #:use-module (crates-io))

(define-public crate-egui-gizmo-0.1.0 (c (n "egui-gizmo") (v "0.1.0") (d (list (d (n "egui") (r "^0.14.2") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "1xbm3whja51zspwlzzhlqg5w124p4ch4lxlhf7lqlpmzzxw93wjc")))

(define-public crate-egui-gizmo-0.1.1 (c (n "egui-gizmo") (v "0.1.1") (d (list (d (n "egui") (r "^0.14.2") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "11by7s37mpgwjc6ab0xx48pgg2nzj3fwaq46gi7cy8fdnan4pry9")))

(define-public crate-egui-gizmo-0.2.0 (c (n "egui-gizmo") (v "0.2.0") (d (list (d (n "egui") (r "^0.14.2") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "048998949qb5rb449d83c0dv42nwpa4vx1j93kgzza9bjp94n9rj")))

(define-public crate-egui-gizmo-0.3.0 (c (n "egui-gizmo") (v "0.3.0") (d (list (d (n "egui") (r "^0.14.2") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "0kb6qb1szca9nh0j7vg9ddidx3cnxxls779jwjlm694w1k1w6475")))

(define-public crate-egui-gizmo-0.4.0 (c (n "egui-gizmo") (v "0.4.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "13wwx7kv6kzpmqsa71r5884751hmr5r6zjzbp8hzh6yfmz1p2mlh")))

(define-public crate-egui-gizmo-0.4.1 (c (n "egui-gizmo") (v "0.4.1") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "16z37zyvxsyi00m0axv38s9li3gb17nnlvx1ssv3ih7ic8a584h1")))

(define-public crate-egui-gizmo-0.5.0 (c (n "egui-gizmo") (v "0.5.0") (d (list (d (n "egui") (r "^0.16.1") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "1s2rcnalnvbblzwrac8mn5vnlkmg8hn6n9cv1zzb80i0nrgp8n72")))

(define-public crate-egui-gizmo-0.6.0 (c (n "egui-gizmo") (v "0.6.0") (d (list (d (n "egui") (r "^0.17.0") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)))) (h "16mqqlvlllzf8cz6nd5yarbd0y5c15kdrv174rbwr4agixv0h07m")))

(define-public crate-egui-gizmo-0.7.0 (c (n "egui-gizmo") (v "0.7.0") (d (list (d (n "egui") (r "^0.18.1") (d #t) (k 0)) (d (n "glam") (r "^0.20.5") (d #t) (k 0)))) (h "1v6cbryljk06br44q37vsfz8py0lm90dqm79i5ll1kpdjzfjjynh")))

(define-public crate-egui-gizmo-0.8.0 (c (n "egui-gizmo") (v "0.8.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "09p79g38yzqzvdhqwklvv5ydxzzlslip32si8wj7vdw8ma1ikq4i")))

(define-public crate-egui-gizmo-0.8.1 (c (n "egui-gizmo") (v "0.8.1") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "1ffcwh32mp61fj9xknsra9gd94a5bvd2j3101wqqb3ijqz5sfapx")))

(define-public crate-egui-gizmo-0.8.2 (c (n "egui-gizmo") (v "0.8.2") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "1w7wwgq1xzy88wsy7ivyj693nf8xxwhs74yxycngs2y1r4rvh7j6")))

(define-public crate-egui-gizmo-0.9.0 (c (n "egui-gizmo") (v "0.9.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "glam") (r "^0.22") (d #t) (k 0)))) (h "0z91g42hwj3g3rhbjsjkcykwmq5772a6agm0yadg98q9iabd7k6m")))

(define-public crate-egui-gizmo-0.10.0 (c (n "egui-gizmo") (v "0.10.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "glam") (r "^0.22") (d #t) (k 0)))) (h "181gir49mjnay1zmbr2vqzynvhqlhdqwz5ysj4mfc875mzrl7h5i")))

(define-public crate-egui-gizmo-0.11.0 (c (n "egui-gizmo") (v "0.11.0") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "0fjzqhxccqzncl7j4bdp7klw54kr6nrvia1id633lnibim0f6axn")))

(define-public crate-egui-gizmo-0.12.0 (c (n "egui-gizmo") (v "0.12.0") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "0hal98diz1x6czk84kcggh03bmss08sg3q01z5n5s9zyg8jascpp")))

(define-public crate-egui-gizmo-0.13.0 (c (n "egui-gizmo") (v "0.13.0") (d (list (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0srid672ggmix97nx12j8g2spyaqwxz9x61c8wcac3ls3c7q7018")))

(define-public crate-egui-gizmo-0.14.0 (c (n "egui-gizmo") (v "0.14.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1jkp6kwg7h9nh7jpiq345g4ww4swgamqfrv292319yyr6i2whi6k")))

(define-public crate-egui-gizmo-0.14.1 (c (n "egui-gizmo") (v "0.14.1") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0i2l08q2pbqfkw6a4igs3pmkr664syv5hp1zmgwk5l7lgz8l7xyg")))

(define-public crate-egui-gizmo-0.15.0 (c (n "egui-gizmo") (v "0.15.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "13hhzzqykyfzragq03lax7wb4696q8xq2gdb69iarpwkjsjl0zna")))

(define-public crate-egui-gizmo-0.16.0 (c (n "egui-gizmo") (v "0.16.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1pal2ivbhxm2k784l8hg7d7rpvhs81lwxmd1awrjwd1wwhsdjji4")))

(define-public crate-egui-gizmo-0.16.1 (c (n "egui-gizmo") (v "0.16.1") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0d3nwbv9l3akhba209kw2v20mda4pfvqvq1zq3dsgmb2pjl9dp9c")))

(define-public crate-egui-gizmo-0.16.2 (c (n "egui-gizmo") (v "0.16.2") (d (list (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0h7p0pr71ipgn5grlii1ab7a4pwcizyj27rp4ih2avvz0c8ifdv5")))

