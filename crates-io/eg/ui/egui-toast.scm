(define-module (crates-io eg ui egui-toast) #:use-module (crates-io))

(define-public crate-egui-toast-0.1.0 (c (n "egui-toast") (v "0.1.0") (d (list (d (n "eframe") (r "^0.18.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "0f8vd173phjhy4jvns6pfha5ayxqss2sfbkvw170jvjibdrk20ps")))

(define-public crate-egui-toast-0.1.1 (c (n "egui-toast") (v "0.1.1") (d (list (d (n "eframe") (r "^0.18.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "0awfb7lyyp8f8w5clkgpa2kz7z6152ra3f0nbiajdy41skk84db2")))

(define-public crate-egui-toast-0.2.0 (c (n "egui-toast") (v "0.2.0") (d (list (d (n "eframe") (r "^0.18.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "09lgvpfd07fs245vfq1jim634958frnzjs0fqn35sd5lyfpi8i2q")))

(define-public crate-egui-toast-0.3.0 (c (n "egui-toast") (v "0.3.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1mg498kwpzwwi3cbqn6c4m9ac47lbplpjkfiycqfy5bav0d4vbb4")))

(define-public crate-egui-toast-0.4.0 (c (n "egui-toast") (v "0.4.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)))) (h "1icvigh9s5108iamnx7vs92d939xwkdjyss1wdkpgzxjgm1108cl")))

(define-public crate-egui-toast-0.5.0 (c (n "egui-toast") (v "0.5.0") (d (list (d (n "eframe") (r "^0.20.1") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)))) (h "1d2a3lmnv452mdssz5h7hfij13mg51lxjadibfy7n82d1d276h43")))

(define-public crate-egui-toast-0.6.0 (c (n "egui-toast") (v "0.6.0") (d (list (d (n "eframe") (r "^0.21.0") (f (quote ("dark-light"))) (d #t) (k 2)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "05waw09z7fw8kpiwyqqb2mwp6wn2dwipn73phzbrk6hhrlcbvbmh")))

(define-public crate-egui-toast-0.7.0 (c (n "egui-toast") (v "0.7.0") (d (list (d (n "egui") (r "^0.21.0") (d #t) (k 0)))) (h "0kp5nlamwliwkrmvli2rfbb5crx6svxv37dm16fsr0dhjd4v6pn7")))

(define-public crate-egui-toast-0.8.0 (c (n "egui-toast") (v "0.8.0") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)))) (h "18ydc0h1a3h07ls7b5dq6yj025x9ngczr38z9if0f3rj99m3d6wh")))

(define-public crate-egui-toast-0.8.1 (c (n "egui-toast") (v "0.8.1") (d (list (d (n "egui") (r "^0.22.0") (k 0)))) (h "0g76kdhx785qly3y7l69n8jjbrsi9ffaqcnwm9v6kwlds3gsa2jy")))

(define-public crate-egui-toast-0.9.0 (c (n "egui-toast") (v "0.9.0") (d (list (d (n "egui") (r "^0.23.0") (k 0)))) (h "1094mkal9dqws1m1w2v7nfk2mp9gkxlqdglyk65g8d09mfs7w80q")))

(define-public crate-egui-toast-0.10.0 (c (n "egui-toast") (v "0.10.0") (d (list (d (n "egui") (r "^0.24.0") (k 0)))) (h "17arjab8jgzph7sfdzx35iz581fvcnk6i5vgd2w3jmhanwgjzfpa")))

(define-public crate-egui-toast-0.10.1 (c (n "egui-toast") (v "0.10.1") (d (list (d (n "egui") (r "^0.24.1") (k 0)))) (h "1zbjyb7wdlvnqb3x9braaz6pv3amagvpc1x66j8bknvmr4ngll9z")))

(define-public crate-egui-toast-0.10.2 (c (n "egui-toast") (v "0.10.2") (d (list (d (n "egui") (r "^0.25") (k 0)))) (h "0dgl6i0vx8fzh06r9nx47vvz7vnhxxl25bl9mza27pgrxpknn9zm") (y #t)))

(define-public crate-egui-toast-0.11.0 (c (n "egui-toast") (v "0.11.0") (d (list (d (n "egui") (r "^0.25") (k 0)))) (h "1rb3g7l2b2x1ycmwx2mi5pc72vwxr3a7dzq2p6j1psvzwskjw2bp")))

(define-public crate-egui-toast-0.12.0 (c (n "egui-toast") (v "0.12.0") (d (list (d (n "egui") (r "^0.26.0") (k 0)))) (h "17jwhd3fjds78i9wazv95y3rz9nksq720fcv20gnb4isylq3z36v")))

(define-public crate-egui-toast-0.12.1 (c (n "egui-toast") (v "0.12.1") (d (list (d (n "egui") (r "^0.26.1") (k 0)))) (h "1fiax7z004h03bnpmkhi9fbhas3r5v33vnvynngp89y8s9z47q3z")))

(define-public crate-egui-toast-0.13.0 (c (n "egui-toast") (v "0.13.0") (d (list (d (n "egui") (r "^0.27") (k 0)))) (h "09m8l4kks4kw0j9497y8wm39agzj8h8vs6nbhph351s8s8z84nyk")))

