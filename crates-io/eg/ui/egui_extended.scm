(define-module (crates-io eg ui egui_extended) #:use-module (crates-io))

(define-public crate-egui_extended-0.3.0 (c (n "egui_extended") (v "0.3.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0380f8db59d228wa1kwpqzb24xnsbb6x2jsy0qx02dh7f48lxbzi")))

(define-public crate-egui_extended-0.3.1 (c (n "egui_extended") (v "0.3.1") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (d #t) (k 0)))) (h "0si0gfs6zv93chv0qnckg1fqrk3z986bwbngzcrn7iycs5pgkcd7")))

