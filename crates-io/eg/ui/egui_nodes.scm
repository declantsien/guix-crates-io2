(define-module (crates-io eg ui egui_nodes) #:use-module (crates-io))

(define-public crate-egui_nodes-0.1.0 (c (n "egui_nodes") (v "0.1.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "egui") (r "^0.11") (d #t) (k 0)))) (h "1f5c1iqraj23f2a48km9jp2k58sph96ywsf4i22mmhls7cmlv0ni")))

(define-public crate-egui_nodes-0.1.1 (c (n "egui_nodes") (v "0.1.1") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "egui") (r "^0.11") (d #t) (k 0)))) (h "0wmfdrb4ysm23mr4dl9i2cpq6knfaw37n7jm8bcza0j5zipaic8a")))

(define-public crate-egui_nodes-0.1.2 (c (n "egui_nodes") (v "0.1.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "eframe") (r "^0.13") (d #t) (k 2)) (d (n "egui") (r "^0.13") (d #t) (k 0)))) (h "0212dwc18qb7mi4fn6xg89yvzbybfp1b5k7zmm1sfr9b7aly6bh7")))

(define-public crate-egui_nodes-0.1.3 (c (n "egui_nodes") (v "0.1.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "eframe") (r "^0.14") (d #t) (k 2)) (d (n "egui") (r "^0.14") (d #t) (k 0)))) (h "1biv7p7wq4chil7qv1a66y577l1phxzdvsf0s82gxwl5r68h32r2")))

