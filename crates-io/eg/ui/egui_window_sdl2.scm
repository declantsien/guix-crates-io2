(define-module (crates-io eg ui egui_window_sdl2) #:use-module (crates-io))

(define-public crate-egui_window_sdl2-0.1.0 (c (n "egui_window_sdl2") (v "0.1.0") (d (list (d (n "egui_backend") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle"))) (d #t) (k 0)))) (h "13jnj2vl8nl5q0ypkdwjn7f6rxgrldp4ckagjmg31xgswwfyhmda") (y #t)))

(define-public crate-egui_window_sdl2-0.2.0 (c (n "egui_window_sdl2") (v "0.2.0") (d (list (d (n "egui_backend") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle"))) (d #t) (k 0)))) (h "15mhcrhg3xnm9jvn7p1k9fdlfd599sf9j3kswwq045x7gzj6jnmp") (y #t)))

(define-public crate-egui_window_sdl2-0.3.0 (c (n "egui_window_sdl2") (v "0.3.0") (d (list (d (n "egui_backend") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle"))) (d #t) (k 0)))) (h "1l9pd90rxmmbmpmp7jdvbfi9fy672m59hhzydy3k7ln9h3sxc4iw")))

