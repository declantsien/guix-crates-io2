(define-module (crates-io eg ui egui_cable) #:use-module (crates-io))

(define-public crate-egui_cable-0.0.0 (c (n "egui_cable") (v "0.0.0") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "1wqjal0prd4hddkj1lmghizd9hxvd707xncmhwqf1l42x8lqi2a2")))

(define-public crate-egui_cable-0.1.0 (c (n "egui_cable") (v "0.1.0") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "1gwh3pz983a1x4ywfwq9frazj03b27l9gnckywg989jq3yk7lwzn")))

(define-public crate-egui_cable-0.2.0 (c (n "egui_cable") (v "0.2.0") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "162x7q26yn30rgsfql46q8dhl14djzqvg8grz941znckzyimkqaa")))

(define-public crate-egui_cable-0.2.1 (c (n "egui_cable") (v "0.2.1") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "1cgz8aag66ymgzmfpixbqbgqkr2bq94j1idsr481vg5dbs1ydrwk")))

(define-public crate-egui_cable-0.2.2 (c (n "egui_cable") (v "0.2.2") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "0r64fz1bv6rlf6lk8v5nnqx2zkd6rif4zya1d2ziyvrw0iw547nj")))

(define-public crate-egui_cable-0.3.0 (c (n "egui_cable") (v "0.3.0") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.18") (d #t) (k 0)))) (h "163yyd1nsjzvbvm8wd7max8rbhmva5g698qzx86kf6rlmy89kfdy")))

(define-public crate-egui_cable-0.4.0 (c (n "egui_cable") (v "0.4.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "eframe") (r "^0.20") (d #t) (k 2)))) (h "0qfbnyipfkr766qi054f6d9viswbawl5r9xhdzqwkbcay3brivdz")))

(define-public crate-egui_cable-0.3.1 (c (n "egui_cable") (v "0.3.1") (d (list (d (n "egui") (r "^0") (d #t) (k 0)) (d (n "epaint") (r "^0") (d #t) (k 0)) (d (n "eframe") (r "^0") (d #t) (k 2)))) (h "0wfa2qf8ql29mir44wl0k2hz8mklf90qmmk580d919rfjcwhzl9q")))

(define-public crate-egui_cable-0.5.0 (c (n "egui_cable") (v "0.5.0") (d (list (d (n "egui") (r "^0") (d #t) (k 0)) (d (n "epaint") (r "^0") (d #t) (k 0)) (d (n "eframe") (r "^0") (d #t) (k 2)))) (h "07gz35jhx4f81jpvy14mqs87n0jjnhsj5938222lq9475gi0pvs3")))

