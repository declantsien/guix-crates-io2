(define-module (crates-io eg ui egui_glfw_gl2) #:use-module (crates-io))

(define-public crate-egui_glfw_gl2-0.1.0 (c (n "egui_glfw_gl2") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.4.0") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.13") (o #t) (d #t) (k 0) (p "webbrowser")) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "090bqpzfka1ab9hh3akxy6v436ijri8whcyn183hdnr2ivq7lml8") (f (quote (("default" "clipboard" "webbrowser"))))))

(define-public crate-egui_glfw_gl2-0.1.1 (c (n "egui_glfw_gl2") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.4.0") (o #t) (d #t) (k 0) (p "cli-clipboard")) (d (n "egui") (r "^0.27.0") (d #t) (k 0)) (d (n "gl33") (r "^0.2.1") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.13") (o #t) (d #t) (k 0) (p "webbrowser")) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "1yal4nlxnf9rvvrsmrgjzw4njs081ks869xxmd23a3mrj1na13pf") (f (quote (("default" "clipboard" "webbrowser"))))))

(define-public crate-egui_glfw_gl2-0.1.2 (c (n "egui_glfw_gl2") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "egui") (r "^0.27.0") (d #t) (k 0)) (d (n "gl33") (r "^0.2.1") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "1iy5wbk1x1z1np7fk4x0cvl68jhss0vh5cdwrkbn9xr681wl21mb")))

