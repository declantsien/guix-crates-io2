(define-module (crates-io eg ui egui-remixicon) #:use-module (crates-io))

(define-public crate-egui-remixicon-0.1.0 (c (n "egui-remixicon") (v "0.1.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (k 0)))) (h "17d59s8nclhw29q4zbaj2jflhk34gyckszxfan8hhw8flfh32zrx")))

(define-public crate-egui-remixicon-0.2.0 (c (n "egui-remixicon") (v "0.2.0") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 2)) (d (n "egui") (r "^0.25.0") (k 0)))) (h "1i8q4bybarb9prpyhi4j585awixzwmi3y4dd7fsj5gah9af5l3gs")))

(define-public crate-egui-remixicon-0.26.2 (c (n "egui-remixicon") (v "0.26.2") (d (list (d (n "eframe") (r "^0.26.2") (d #t) (k 2)) (d (n "egui") (r "^0.26.2") (k 0)))) (h "1jybzyldx305pn6whpx5bm5hya3a3rqb88g315hyisg4ympg190f")))

(define-public crate-egui-remixicon-0.27.2 (c (n "egui-remixicon") (v "0.27.2") (d (list (d (n "eframe") (r "^0.27.2") (d #t) (k 2)) (d (n "egui") (r "^0.27.2") (o #t) (k 0)))) (h "0b5cna16wvhi4bykipk22n19v9h8n3xs2hk9b6qjc00y1xrk0fv7") (f (quote (("default" "egui")))) (s 2) (e (quote (("egui" "dep:egui"))))))

