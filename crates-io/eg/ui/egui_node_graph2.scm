(define-module (crates-io eg ui egui_node_graph2) #:use-module (crates-io))

(define-public crate-egui_node_graph2-0.5.0 (c (n "egui_node_graph2") (v "0.5.0") (d (list (d (n "egui") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s32wzjbrc74dbdhall2gc7hbl5hrd1cfw7ny0sfrs49kkmnl9zr") (f (quote (("persistence" "serde" "slotmap/serde" "smallvec/serde" "egui/persistence"))))))

