(define-module (crates-io eg ui egui-plotter) #:use-module (crates-io))

(define-public crate-egui-plotter-0.1.0 (c (n "egui-plotter") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "emath") (r "^0.22.0") (d #t) (k 0)) (d (n "epaint") (r "^0.22.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)) (d (n "plotters-backend") (r "^0.3.4") (d #t) (k 0)))) (h "0996lf3964i2qf53ns464rinvnxzhfqfkvw5p9ddvc8bdqcyvl6c")))

(define-public crate-egui-plotter-0.1.1 (c (n "egui-plotter") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "emath") (r "^0.22.0") (d #t) (k 0)) (d (n "epaint") (r "^0.22.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)) (d (n "plotters-backend") (r "^0.3.4") (d #t) (k 0)))) (h "0j39l7p5wpzl25mnah1rhal5k75pp6icfnk6k21lm91p6ir4ksxf")))

(define-public crate-egui-plotter-0.2.0 (c (n "egui-plotter") (v "0.2.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "emath") (r "^0.22.0") (d #t) (k 0)) (d (n "epaint") (r "^0.22.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.4") (d #t) (k 0)))) (h "1bp9wcbph4q1w4qdqmy160ksmd1nyrfhgfjpnhl7c3qirxalpvrz")))

(define-public crate-egui-plotter-0.2.1 (c (n "egui-plotter") (v "0.2.1") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.4") (d #t) (k 0)))) (h "12ypqsp2v484n24v8kmjc81n6rzymff2chxacamd65n95vsp3m3i")))

(define-public crate-egui-plotter-0.3.0 (c (n "egui-plotter") (v "0.3.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 2)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.4") (d #t) (k 0)))) (h "1n05pjaqbiwyi9xmf7sm5rhpjgir6c61naxlxnh5z5avn5f2kzz1")))

