(define-module (crates-io eg ui egui-phosphor) #:use-module (crates-io))

(define-public crate-egui-phosphor-0.1.0 (c (n "egui-phosphor") (v "0.1.0") (d (list (d (n "egui") (r "^0.21") (k 0)))) (h "1fl2zdqmr5l3wwz10xdyjzgzhk0j0c7cwnnibv8bgd3jhg80277v")))

(define-public crate-egui-phosphor-0.1.1 (c (n "egui-phosphor") (v "0.1.1") (d (list (d (n "egui") (r "^0.22") (k 0)))) (h "17rsywgqr2wvsx385hli7qvgb4ha5glcknykmxdrb55r667fdak3")))

(define-public crate-egui-phosphor-0.2.0 (c (n "egui-phosphor") (v "0.2.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 2)) (d (n "egui") (r "^0.22") (k 0)))) (h "0gxjl1dr82w9zn1r6kyhv2q99239gbcb7g2pb0b6lscj7l1362y2") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

(define-public crate-egui-phosphor-0.3.0 (c (n "egui-phosphor") (v "0.3.0") (d (list (d (n "eframe") (r "^0.23") (d #t) (k 2)) (d (n "egui") (r "^0.23") (k 0)))) (h "1nxg6k093rp5881ddqhy985i8vm83dlg4byg6wb06ix6yja60f03") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

(define-public crate-egui-phosphor-0.3.1 (c (n "egui-phosphor") (v "0.3.1") (d (list (d (n "eframe") (r "^0.24") (d #t) (k 2)) (d (n "egui") (r "^0.24") (k 0)))) (h "06z6r5bzxbw15pwq0xih1a464js8qx9pmsi16361pa87dg3v8fqg") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

(define-public crate-egui-phosphor-0.3.2 (c (n "egui-phosphor") (v "0.3.2") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)))) (h "19x8xdx50s4521fn6ils5hlq2kavcd1f5z2n9sv9xiw3yzm2dhdl") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

(define-public crate-egui-phosphor-0.4.0 (c (n "egui-phosphor") (v "0.4.0") (d (list (d (n "eframe") (r "^0.26") (d #t) (k 2)) (d (n "egui") (r "^0.26") (k 0)))) (h "0h5xxvdnlkh97bs5sh30r2rxva5d04355hvai9ks53z4636y1vlr") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

(define-public crate-egui-phosphor-0.5.0 (c (n "egui-phosphor") (v "0.5.0") (d (list (d (n "eframe") (r "^0.27") (d #t) (k 2)) (d (n "egui") (r "^0.27") (k 0)))) (h "0fs694c6bzahlsdkiv5ll5zrb674l76n3lxvvha4mjcvg22kg1vc") (f (quote (("thin") ("regular") ("light") ("fill") ("default" "regular") ("bold"))))))

