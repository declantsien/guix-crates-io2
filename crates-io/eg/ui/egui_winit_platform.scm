(define-module (crates-io eg ui egui_winit_platform) #:use-module (crates-io))

(define-public crate-egui_winit_platform-0.1.0 (c (n "egui_winit_platform") (v "0.1.0") (d (list (d (n "egui") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 0)))) (h "1l6an6ckjw4njc98r50pjx5y0xc7jggpjp6k06sfp3vccwq5y9d9")))

(define-public crate-egui_winit_platform-0.2.0 (c (n "egui_winit_platform") (v "0.2.0") (d (list (d (n "egui") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 0)))) (h "0hmh8x6d0d1wakb7ly74p0bk8c5q6hilw7dyzwmc2y1wr3sn2c6p")))

(define-public crate-egui_winit_platform-0.3.0 (c (n "egui_winit_platform") (v "0.3.0") (d (list (d (n "egui") (r "^0.5") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1q09b9kg97f8wnqpp7kw5nqskh1i8jf5b6ydbrgjaqikwhyw5kra")))

(define-public crate-egui_winit_platform-0.4.0 (c (n "egui_winit_platform") (v "0.4.0") (d (list (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.8") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1az7pcj2v2b3sxqigbs3xgr6ivp7magci2rhymwi9s1cfc82r06n")))

(define-public crate-egui_winit_platform-0.5.0 (c (n "egui_winit_platform") (v "0.5.0") (d (list (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.10.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "0wpnwbl8p8mh4dz91j71mb5mkzmm70k2njvphy5520gkga088y5b")))

(define-public crate-egui_winit_platform-0.6.0 (c (n "egui_winit_platform") (v "0.6.0") (d (list (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "112d2i13yklif6ij65a8y6x65h81mb57iamr6hg3kmv471npdcmb")))

(define-public crate-egui_winit_platform-0.7.0 (c (n "egui_winit_platform") (v "0.7.0") (d (list (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.12") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1dm64y8cw205fa74l07ldg30yhgyrgk2a8fz3lv7f2xncgs17zv9")))

(define-public crate-egui_winit_platform-0.8.0 (c (n "egui_winit_platform") (v "0.8.0") (d (list (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.12") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "0w83asw23id5icnk1ivn0nbzsz4v5dcs6mggiwpwznyl1iyg2k6d")))

(define-public crate-egui_winit_platform-0.9.0 (c (n "egui_winit_platform") (v "0.9.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.13") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "1bl15my8zdmj7acsq810fs2i0cxfqggyha5xl0c3fmbg4kr6qgvq") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.10.0 (c (n "egui_winit_platform") (v "0.10.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.14") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (k 0)))) (h "0zwpj9jih7k5f4csjvkalwxihq49kk984ya8b1in21fpnj7qv548") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.11.0 (c (n "egui_winit_platform") (v "0.11.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.15") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (k 0)))) (h "0y2j3mgiq1dpfzzk3zpxdhbmzx7dv788ylcd2z9nwafdx5m31bm0") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.12.0 (c (n "egui_winit_platform") (v "0.12.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.15") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (k 0)))) (h "0wwr0fvha90rn4w7j28447b56qfcpzl61s5d0a7zza055wy7h2h6") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.13.0 (c (n "egui_winit_platform") (v "0.13.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.16") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (k 0)))) (h "0z9m39bzm5q00s0z1b7j2kbkx2yffvx18j6jwchmdqiq5m9lbw8j") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.14.0 (c (n "egui_winit_platform") (v "0.14.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.17") (f (quote ("single_threaded"))) (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (k 0)))) (h "19wga67yq9qvccddmyyzkpfn4jdphm718g3db49wcdda3qkrdyyf") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.15.0 (c (n "egui_winit_platform") (v "0.15.0") (d (list (d (n "copypasta") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.18") (k 0)) (d (n "webbrowser") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (k 0)))) (h "0s3iyj46d5yg7kihp1ypdvakg1q9ghslw606s7l3vklh8vphsl4v") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.16.0 (c (n "egui_winit_platform") (v "0.16.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.19") (k 0)) (d (n "webbrowser") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.27") (k 0)))) (h "0044p1xvwfjy7annwldkjq6rmgnp1xvainbyzfclph870a3iryrl") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.17.0 (c (n "egui_winit_platform") (v "0.17.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.20") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.27") (k 0)))) (h "1skcqngxbaz83xap9xpl7qy3fcdwkb9bjw8n0ir81dw31ppm7axb") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.18.0 (c (n "egui_winit_platform") (v "0.18.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.21") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0lvn1bb6gwky5shaxcqwnknyvp75xn1kqgm5vcajajwa7pwwv4za") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.19.0 (c (n "egui_winit_platform") (v "0.19.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.22") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1cz85gq17k0qsfwiip43aajd3aazpk3gmfpvrvfhd1y8wwaxcza2") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.20.0 (c (n "egui_winit_platform") (v "0.20.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "196f2ah08x948qljpckn6k8a3l9j6i6am5aazgby22kwkxrb1wng") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.21.0 (c (n "egui_winit_platform") (v "0.21.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 0)))) (h "18p943rld3q2c0ylps50hcf44f6ggs1qb63rkn1ahr2hnczbzz8n") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

(define-public crate-egui_winit_platform-0.22.0 (c (n "egui_winit_platform") (v "0.22.0") (d (list (d (n "copypasta") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.27") (k 0)) (d (n "webbrowser") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 0)))) (h "1phan2kb1x8bn519fhsbk29d68alrd29mxcg8gq8dkcdsp4b9j48") (f (quote (("default_fonts" "egui/default_fonts") ("default" "default_fonts") ("clipboard" "copypasta"))))))

