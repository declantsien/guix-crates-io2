(define-module (crates-io eg ui egui_speedy2d) #:use-module (crates-io))

(define-public crate-egui_speedy2d-0.1.0 (c (n "egui_speedy2d") (v "0.1.0") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "epaint") (r "^0.19") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^1.8") (d #t) (k 0)))) (h "19kx601l6swp3jf6k6wrq9alpddrd4ffbimkvcm2mkf8v67nslln")))

(define-public crate-egui_speedy2d-0.1.1 (c (n "egui_speedy2d") (v "0.1.1") (d (list (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "epaint") (r "^0.19") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^1.8") (d #t) (k 0)))) (h "06mm963fpp3cfdwb2fmpla758s7i9p34zrp87qbcxq8qdq6v272f")))

(define-public crate-egui_speedy2d-0.2.0 (c (n "egui_speedy2d") (v "0.2.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "epaint") (r "^0.20") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^1.9") (d #t) (k 0)))) (h "16xdfkq1zjhimdyhw0mkcxqm32hjjialzpp2406iriqpjf1wyplz")))

(define-public crate-egui_speedy2d-0.3.0 (c (n "egui_speedy2d") (v "0.3.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "epaint") (r "^0.20") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^1.10") (d #t) (k 0)))) (h "1qs6s17s12ak8jh8svphwl7321z11zi6162nq6ihh1c184rbbdn1")))

(define-public crate-egui_speedy2d-0.4.0 (c (n "egui_speedy2d") (v "0.4.0") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)) (d (n "epaint") (r "^0.21") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^1.10") (d #t) (k 0)))) (h "00xv1npdy81bcbg5rh47c52jrygn3k57q8zjxafx8xnmny3dyl1z")))

(define-public crate-egui_speedy2d-0.5.0 (c (n "egui_speedy2d") (v "0.5.0") (d (list (d (n "egui") (r "^0.27.2") (d #t) (k 0)) (d (n "epaint") (r "^0.27.2") (d #t) (k 0)) (d (n "simple_logger") (r "^5.0.0") (f (quote ("colors"))) (k 2)) (d (n "speedy2d") (r "^2.1.0") (d #t) (k 0)))) (h "1grmn6wcyqxrw2qyqw7bhbvlv6b5dqp3k8mr5lgf5qziwmhhr4bf")))

