(define-module (crates-io eg ui egui_hotkey) #:use-module (crates-io))

(define-public crate-egui_hotkey-0.1.0 (c (n "egui_hotkey") (v "0.1.0") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08mkm7qwlmy43lzl23y1apgyndgv9zs6n6w2cvdg1l63qpymzzsc") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui_hotkey-0.1.1 (c (n "egui_hotkey") (v "0.1.1") (d (list (d (n "eframe") (r "^0.18") (d #t) (k 2)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "061d26986x68xcb8748y7910qf67cmvvg2iqg7w2wbb68lmhiq6k") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

(define-public crate-egui_hotkey-0.2.0 (c (n "egui_hotkey") (v "0.2.0") (d (list (d (n "eframe") (r "^0.19") (d #t) (k 2)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07sqhm38mncd968kfbkqnbnv0rb7c9vf95lgixpazc3l7xp664pa") (s 2) (e (quote (("serde" "dep:serde" "egui/serde"))))))

