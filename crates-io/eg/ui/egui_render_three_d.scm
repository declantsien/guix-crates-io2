(define-module (crates-io eg ui egui_render_three_d) #:use-module (crates-io))

(define-public crate-egui_render_three_d-0.2.0 (c (n "egui_render_three_d") (v "0.2.0") (d (list (d (n "egui_backend") (r "^0.2.0") (d #t) (k 0)) (d (n "egui_render_glow") (r "^0.2.0") (d #t) (k 0)) (d (n "three-d") (r "^0.13.0") (d #t) (k 0)))) (h "1wk60g24lqpyr8y1n5g1z0za5cpj42k31vdvnanvbsbfhq4bhla4") (y #t)))

(define-public crate-egui_render_three_d-0.3.0 (c (n "egui_render_three_d") (v "0.3.0") (d (list (d (n "egui_backend") (r "^0.2.0") (d #t) (k 0)) (d (n "egui_render_glow") (r "^0.2.0") (d #t) (k 0)) (d (n "three-d") (r "^0.13.0") (d #t) (k 0)))) (h "1a8shf7ma5ajybym357fwwqdch8xc885f3k0nc337kszvslicklm")))

(define-public crate-egui_render_three_d-0.4.0 (c (n "egui_render_three_d") (v "0.4.0") (d (list (d (n "egui_backend") (r "^0.4") (d #t) (k 0)) (d (n "egui_render_glow") (r "^0.4") (d #t) (k 0)) (d (n "three-d") (r "^0.15") (k 0)))) (h "0brdgxnxcwn0c19pbaarn5x09qviybyfgpr1bzrwijqlpqif0b65")))

(define-public crate-egui_render_three_d-0.5.0 (c (n "egui_render_three_d") (v "0.5.0") (d (list (d (n "egui") (r "^0.23") (k 0)) (d (n "egui_render_glow") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "1bynkm0v6bsdw6r96sn84bcbxwz385zzgl4zdy80r5s2vqbb00mw")))

(define-public crate-egui_render_three_d-0.5.1 (c (n "egui_render_three_d") (v "0.5.1") (d (list (d (n "egui") (r "^0.23") (k 0)) (d (n "egui_render_glow") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "18my7npsv7vi4l2imnww2n5cmmc9xlf6blyvrfv31xp7dsny5x9d")))

(define-public crate-egui_render_three_d-0.5.2 (c (n "egui_render_three_d") (v "0.5.2") (d (list (d (n "egui") (r "^0.23") (k 0)) (d (n "egui_render_glow") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "0ahyy1jbzhqsmxidd105j5whn1w6jh6x5azzi3mmd4rzr6xfff20")))

(define-public crate-egui_render_three_d-0.6.0 (c (n "egui_render_three_d") (v "0.6.0") (d (list (d (n "egui") (r "^0.24.1") (k 0)) (d (n "egui_render_glow") (r "^0.6") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "00wzgmjwlbga2afi2g1k8cwdvw4jd19n6p5b6gdr00yxv0x9s65n")))

(define-public crate-egui_render_three_d-0.7.0 (c (n "egui_render_three_d") (v "0.7.0") (d (list (d (n "egui") (r "^0.25") (k 0)) (d (n "egui_render_glow") (r "^0.7") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "0i8xm7120rsvvgmb2qlk8hflkwy00xhxrcpqbrms68a7xx3li53k")))

(define-public crate-egui_render_three_d-0.8.0 (c (n "egui_render_three_d") (v "0.8.0") (d (list (d (n "egui") (r "^0.26") (k 0)) (d (n "egui_render_glow") (r "^0.8") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (k 0)))) (h "1a57nah1b83b4bf6wd01nyjdr2x0jhx7b8djacn45bc5mdd7zg1r")))

