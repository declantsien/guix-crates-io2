(define-module (crates-io eg ui egui_sdl2_platform) #:use-module (crates-io))

(define-public crate-egui_sdl2_platform-0.1.0 (c (n "egui_sdl2_platform") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle" "static-link" "bundled"))) (d #t) (k 0)))) (h "106pg14nazqr0hg6xy0j3yivryq85h4v24hk60jdglk2fgccff3s")))

(define-public crate-egui_sdl2_platform-0.2.0 (c (n "egui_sdl2_platform") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("raw-window-handle"))) (d #t) (k 0)))) (h "0zx9736ry950j19nkr976i1vd4s70p1i9d7327h1zmi2qqn2xrym")))

