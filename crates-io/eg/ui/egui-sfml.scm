(define-module (crates-io eg ui egui-sfml) #:use-module (crates-io))

(define-public crate-egui-sfml-0.1.0 (c (n "egui-sfml") (v "0.1.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.19.0") (d #t) (k 2)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "sfml") (r "^0.18.0") (f (quote ("graphics"))) (k 0)))) (h "1wfqhg1g4zy5cwk25aq1kgpk8a40l9mg8hq2s2jd8hasw583hwbm")))

(define-public crate-egui-sfml-0.1.1 (c (n "egui-sfml") (v "0.1.1") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.19.0") (d #t) (k 2)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "sfml") (r "^0.18.0") (f (quote ("graphics"))) (k 0)))) (h "1kh9c00jl7bxjq7lx2nxqzp001p1n6lpppdlv4dw222l0v8fp3c9")))

(define-public crate-egui-sfml-0.2.0 (c (n "egui-sfml") (v "0.2.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.19.0") (d #t) (k 2)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "sfml") (r "^0.19.0") (f (quote ("graphics"))) (k 0)))) (h "13fin9y06a3v6j38brqxg1c6nyfb8597nbx5nnl47cqi382ika7f")))

(define-public crate-egui-sfml-0.3.0 (c (n "egui-sfml") (v "0.3.0") (d (list (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (k 0)) (d (n "egui_demo_lib") (r "^0.20") (d #t) (k 2)))) (h "0vy4bhh4p7hfasdnlbf2689wpszsr6dfxhrq0ns17cb4dg7h7kxm")))

(define-public crate-egui-sfml-0.4.0 (c (n "egui-sfml") (v "0.4.0") (d (list (d (n "egui") (r "^0.21") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (k 0)) (d (n "egui_demo_lib") (r "^0.21") (d #t) (k 2)))) (h "1p11zflnrj7zh1c9az6r08aj9m23nrrp306bym1j075wi3mz4a9c")))

