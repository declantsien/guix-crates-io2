(define-module (crates-io eg ui egui-file-dialog) #:use-module (crates-io))

(define-public crate-egui-file-dialog-0.1.0 (c (n "egui-file-dialog") (v "0.1.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "1csyqmcybx4p4q7ghkqxi0qp8cfgpl9kp42qif6qallk2rkc8cv7")))

(define-public crate-egui-file-dialog-0.2.0 (c (n "egui-file-dialog") (v "0.2.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "1zdy340dyz2yzhh0kix4wlk0sfhx152wqalvkn8za52n0b583b6p")))

(define-public crate-egui-file-dialog-0.3.0 (c (n "egui-file-dialog") (v "0.3.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "0vmimbvz68flr3il7253kyckln0kzxiyh7ass4yq5sdcqpby2wwh")))

(define-public crate-egui-file-dialog-0.3.1 (c (n "egui-file-dialog") (v "0.3.1") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "1pfs7y2vgqm48kywbx8qp0jxywawss8y7d6v2yip7gmpj9qrqggz")))

(define-public crate-egui-file-dialog-0.4.0 (c (n "egui-file-dialog") (v "0.4.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "0c92yq558jig0sdfw8vqwai5yy7kr0b96qm1qx5fpnjja2irp0h3")))

(define-public crate-egui-file-dialog-0.5.0 (c (n "egui-file-dialog") (v "0.5.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "egui") (r "^0.27.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (k 0)))) (h "1570gkihrhi6nmrqis0sjy6dcf9lg18x8gcyv7apfcg9mwnp2apl")))

