(define-module (crates-io eg ui egui_struct_macros) #:use-module (crates-io))

(define-public crate-egui_struct_macros-0.2.0 (c (n "egui_struct_macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "01ha8qn3ngcf5k5yxxn64cnp0vp9dl9iryydm9p87mbdyzk2fm1w") (f (quote (("i18n"))))))

(define-public crate-egui_struct_macros-0.3.0 (c (n "egui_struct_macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0i856rz66d50zzh7l772713x2m1dskqdyzv4298jw6ywwzm45d26") (f (quote (("i18n"))))))

(define-public crate-egui_struct_macros-0.4.1 (c (n "egui_struct_macros") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "09p4r5n83vhqfyzgwdpqdfkyqiv21nnsqh9cq4rvs42c1l4mc4a6") (f (quote (("i18n"))))))

