(define-module (crates-io eg ui egui_extras_lib) #:use-module (crates-io))

(define-public crate-egui_extras_lib-0.13.0 (c (n "egui_extras_lib") (v "0.13.0") (d (list (d (n "asynchron") (r "^0.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "09jq95zya3psrgkim13pwr7d73mvr1knqfpjx41nxbsi4yi65888") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.1 (c (n "egui_extras_lib") (v "0.13.1") (d (list (d (n "asynchron") (r "^0.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "00fqjaqv9f6pyal2p6n8cwpgfqdysmc3md8l8fhv62w9smxwpzrd") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.2 (c (n "egui_extras_lib") (v "0.13.2") (d (list (d (n "asynchron") (r "^0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "12w5hga4nxm30dyhcgqn5hs8bw9nb4g7w9gm2nb20c1s799cdvsn") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.3 (c (n "egui_extras_lib") (v "0.13.3") (d (list (d (n "asynchron") (r "^0.3") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "1mcrilpnyljn87b07qgh6jm17sgjlx5ib0cwdwq2c4qxm904m3wz") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.4 (c (n "egui_extras_lib") (v "0.13.4") (d (list (d (n "asynchron") (r "^0.4") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "163rwp02dvwmmn8k2sjmx8a6fw448548s7vh53mzqym8d8xl65rw") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.5 (c (n "egui_extras_lib") (v "0.13.5") (d (list (d (n "asynchron") (r "^0.4") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "1l2bjly8zjkzdf4v48pjj3r9ipgmjnhaddlj4shx5a9zidw70dhr") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.6 (c (n "egui_extras_lib") (v "0.13.6") (d (list (d (n "asynchron") (r "^0.5") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "05dn75dv5hdy0knjw1p170fc5xqv5i0gcamnyrygqs6qgh05m5jw") (f (quote (("staticlib")))) (y #t)))

(define-public crate-egui_extras_lib-0.13.7 (c (n "egui_extras_lib") (v "0.13.7") (d (list (d (n "asynchron") (r "^0.8") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 1)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "epi") (r "^0.13") (d #t) (k 0)))) (h "1j567jka43brp73prbcdhk6s1qk05b1hnflpp5zd4ilpxisb11v0") (f (quote (("staticlib")))) (y #t)))

