(define-module (crates-io eg ui egui_plot) #:use-module (crates-io))

(define-public crate-egui_plot-0.23.0 (c (n "egui_plot") (v "0.23.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.23.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0az7dd3bp09wbxyaj24969d3d8f7xk6vlgavadbbmccfzq03mwy7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.70")))

(define-public crate-egui_plot-0.24.0 (c (n "egui_plot") (v "0.24.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.24.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wp2yf85sr2n6d7a4dc6xv934ly1cvvihzllx8kmiw6pqlhxfnbn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.24.1 (c (n "egui_plot") (v "0.24.1") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.24.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lmr7qrfn1dcfliiah3bxfi1ynfpkfbyfsi64lcb8z8rvpf2wyxk") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.24.2 (c (n "egui_plot") (v "0.24.2") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.24.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04p4m8z7hx0gz260vv2nmgqz1gjj1ya1l59l1bs6lcsv3a189d19") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.25.0 (c (n "egui_plot") (v "0.25.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.25.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gh6sm7c2blq19y8z06f5zn1z8h6i7k4hvfj3yfzflphxgzry5ba") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.26.0-alpha.1 (c (n "egui_plot") (v "0.26.0-alpha.1") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26.0-alpha.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zlv09kv28wh1w8h8fzsbcdyzac9shmcr2i863km5s27z3r3g8ba") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.26.0-alpha.2 (c (n "egui_plot") (v "0.26.0-alpha.2") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26.0-alpha.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bmjnys6pj90m547fnadmmfj255wl50m9xwjnkgh6cjcbd19ip0y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.26.0 (c (n "egui_plot") (v "0.26.0") (d (list (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xw54vfxkb99j0f40xsimicw7912zkc86dxjl0s081wfb1mb57vc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.26.1 (c (n "egui_plot") (v "0.26.1") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18hbfl1pv1c2k44gw0m5j9galx3bbyjxi4xfn9k8794prmfz8wv0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.26.2 (c (n "egui_plot") (v "0.26.2") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.26.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n6z8gmjjrfv1yqndnvd958n1l9hjwcwjsp20vqxfk99mnqzqfw0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.27.0 (c (n "egui_plot") (v "0.27.0") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mwn5dhhk3bzdgc7lw072b1vm1vm2f6y8wp59awpn5f2hp18b0yz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.27.1 (c (n "egui_plot") (v "0.27.1") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.27.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n2iljj93jfm3n20gnmrj1620qhfqiczas30g59jj561ff4z12b9") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

(define-public crate-egui_plot-0.27.2 (c (n "egui_plot") (v "0.27.2") (d (list (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.27.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18c868r635wk0cd18m5msq9sknhi01h3vnvha8n3ab8wvj34p1d7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "egui/serde")))) (r "1.72")))

