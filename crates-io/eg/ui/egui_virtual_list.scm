(define-module (crates-io eg ui egui_virtual_list) #:use-module (crates-io))

(define-public crate-egui_virtual_list-0.1.0 (c (n "egui_virtual_list") (v "0.1.0") (d (list (d (n "eframe") (r "^0.25") (d #t) (k 2)) (d (n "egui") (r "^0.25") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "web-time") (r "^1") (d #t) (k 0)))) (h "1ryc1czzhizwnhrdjz1khs3xi5im13v2pkmqzd7qyw0xh4s96i32")))

(define-public crate-egui_virtual_list-0.2.0 (c (n "egui_virtual_list") (v "0.2.0") (d (list (d (n "eframe") (r "^0.26.0") (d #t) (k 2)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "web-time") (r "^1") (d #t) (k 0)))) (h "0wxa2wbcbq5q95429h0iaqmhqfybl3hcqlygmq6znw702fy2a5wm")))

(define-public crate-egui_virtual_list-0.3.0 (c (n "egui_virtual_list") (v "0.3.0") (d (list (d (n "eframe") (r "^0.27.0") (d #t) (k 2)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "web-time") (r "^1") (d #t) (k 0)))) (h "06mgfr6jn34qy0rrhvxcdp1snigxpy2b675d4gil6ixfs853lb8l")))

