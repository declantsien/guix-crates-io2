(define-module (crates-io eg ui egui_clip_textedit) #:use-module (crates-io))

(define-public crate-egui_clip_textedit-0.1.0 (c (n "egui_clip_textedit") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "eframe") (r "^0.21.0") (f (quote ("accesskit" "default_fonts" "wgpu"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "10yx97vazbh03dvby7pjqvxb3s4x01ml0plg8vfflsd27k19rzv5") (y #t)))

(define-public crate-egui_clip_textedit-0.1.1 (c (n "egui_clip_textedit") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "eframe") (r "^0.21.0") (f (quote ("accesskit" "default_fonts" "wgpu"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0f23h12s609d02mix1zvrfxk972a5dar209k5m1had5147k3njb0")))

(define-public crate-egui_clip_textedit-0.1.2 (c (n "egui_clip_textedit") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "eframe") (r "^0.21.0") (f (quote ("accesskit" "default_fonts" "wgpu"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0n1ivp2iyp3sdfbmgfz1smscmp76aj232hpvjnmm10rdb3jyr6dz")))

(define-public crate-egui_clip_textedit-0.1.5 (c (n "egui_clip_textedit") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "eframe") (r "^0.21.0") (f (quote ("accesskit" "default_fonts" "wgpu"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "06pv7wn3ny05pa2vrlms5z0z994bw6c6d79sxadabpclykdpla6w")))

