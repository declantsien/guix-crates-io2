(define-module (crates-io eg ui egui-keybinds) #:use-module (crates-io))

(define-public crate-egui-keybinds-0.1.0 (c (n "egui-keybinds") (v "0.1.0") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.0") (d #t) (k 0)))) (h "1a8b4n78lj2qp64a5p7p040calglhjgwzxi15b1qa4l7x7hjg4k5")))

(define-public crate-egui-keybinds-0.1.1 (c (n "egui-keybinds") (v "0.1.1") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.0") (d #t) (k 0)))) (h "04cl8nsblvpmjxmk9743wrwp3g3x0qvbkrhbp5jq3c79rqk3pwfq")))

(define-public crate-egui-keybinds-0.1.2 (c (n "egui-keybinds") (v "0.1.2") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.2") (d #t) (k 0)))) (h "0zccn9fjdm6gf44q4kl7k5hw1dj3sfjqb3ygsrpiniqb9kmn1bj3")))

(define-public crate-egui-keybinds-1.1.2 (c (n "egui-keybinds") (v "1.1.2") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0pgga9djdcdhlv6g4n24vkjy6nv70842b6z4jprh87c3zcjgnsq1")))

(define-public crate-egui-keybinds-1.1.3 (c (n "egui-keybinds") (v "1.1.3") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0pb8v04jfq8jl6f47d0a0yby9m8na3fql3vamyirvi70h015hxai")))

(define-public crate-egui-keybinds-1.2.3 (c (n "egui-keybinds") (v "1.2.3") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0kycv0n73r604vn4xrc5ran2fx7qx3srzcmg992yqq3cn36vxmzi")))

(define-public crate-egui-keybinds-1.2.4 (c (n "egui-keybinds") (v "1.2.4") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-modal") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "06a3zmhxhsp97m5fxa5i3c71h7y8pc62rincaxg503p17x60sg3j")))

(define-public crate-egui-keybinds-2.2.4 (c (n "egui-keybinds") (v "2.2.4") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1la4synm7600y7gkhzlgkg0sz32q8jdsbzqqnz7qdy8inhy99w01")))

(define-public crate-egui-keybinds-2.2.5 (c (n "egui-keybinds") (v "2.2.5") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0rdjmnmwxa3fcxlgwyf8gnr7iyigjbb02q1qrqbrzzm5d8jlgr99")))

(define-public crate-egui-keybinds-2.2.6 (c (n "egui-keybinds") (v "2.2.6") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "14c64zhvj7c2fmy0w5babz1lk3rljy7js4jaahwwh0h14a08zjys")))

(define-public crate-egui-keybinds-3.2.6 (c (n "egui-keybinds") (v "3.2.6") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "01mc4cha02lags8prxvq1i7kvac93banv454zvdxpding1vqzscp")))

(define-public crate-egui-keybinds-3.2.7 (c (n "egui-keybinds") (v "3.2.7") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1yf1xll4m8cn4i9w25v4365xk0a1ahvb4hx6a3v5c0wg9jqwq0sv")))

(define-public crate-egui-keybinds-3.3.7 (c (n "egui-keybinds") (v "3.3.7") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0ijdyx5ay5rvpalcf664jfm3k6nxg3h6jj40dfq7bj8kwpqj28a9")))

(define-public crate-egui-keybinds-3.3.8 (c (n "egui-keybinds") (v "3.3.8") (d (list (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "05jdi70nscl9ivawckbryssx6gcw2by6zwahh2dsq2ahzy08nqng")))

