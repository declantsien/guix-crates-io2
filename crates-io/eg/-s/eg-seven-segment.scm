(define-module (crates-io eg -s eg-seven-segment) #:use-module (crates-io))

(define-public crate-eg-seven-segment-0.1.0 (c (n "eg-seven-segment") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)))) (h "1kq8nqnjjyirfpyzsvlj5xkl79jcaqi9w6g806gl2als2md57fhj")))

(define-public crate-eg-seven-segment-0.2.0 (c (n "eg-seven-segment") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)))) (h "0yxq4jsp9vcdbfx38r9npxc0snhp0qcn8p46f4y36jr3xma2yzby")))

