(define-module (crates-io eg o_ ego_types) #:use-module (crates-io))

(define-public crate-ego_types-0.1.0 (c (n "ego_types") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06gr1ar5h56pw6vlp3ax3vig2m6a3qdsxi0k8ijlf8s5l6ngb1c8")))

(define-public crate-ego_types-0.1.1 (c (n "ego_types") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gyhd542jfz1a4nbjrv3qslbmgqz75195gkrq3cparbwkmrl4w0j")))

(define-public crate-ego_types-0.1.2 (c (n "ego_types") (v "0.1.2") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10yc394adawbbqhz31zw40kmcrv1403w95d2dnz6k60d5vl2r92c")))

(define-public crate-ego_types-0.1.3 (c (n "ego_types") (v "0.1.3") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0mhyn4hm4vyf0l7z104007yks714wnpjagwmkpbi894vy8fcv4ff")))

(define-public crate-ego_types-0.1.4 (c (n "ego_types") (v "0.1.4") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1gpdz8vy75shhwvg10b5jq7g02r8ldzmcls58m8s40dgccs1b0hd")))

(define-public crate-ego_types-0.1.5 (c (n "ego_types") (v "0.1.5") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1n84vb8pmlhq8b7wjkrfsh5d66k7zk70wzlx89an0dvyx0hd3k6g")))

(define-public crate-ego_types-0.1.6 (c (n "ego_types") (v "0.1.6") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1parjq6km8fsa8nfn6i5j3i4cqcx5kxyh2wldnx6i996m396vwvj")))

(define-public crate-ego_types-0.1.7 (c (n "ego_types") (v "0.1.7") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "13g8vdz6rmsjqav04hw46wf7hr91b1kkx30k8plhk0wd19q6ynv7") (y #t)))

(define-public crate-ego_types-0.1.8 (c (n "ego_types") (v "0.1.8") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "01agbm22sb8bznjlpffbxh9a2g8mzi0jv0zb0mpbvj5xmy54dwzs")))

(define-public crate-ego_types-0.2.1 (c (n "ego_types") (v "0.2.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0smlbivhw7nrp8qwxichjf0dk5l9bc0gqyhknbq4rgdwcgmnsh8a")))

(define-public crate-ego_types-0.2.2 (c (n "ego_types") (v "0.2.2") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0ar8ql7y50z5bkwpjkg56ffb0z9jcsc6r4yfmpqcyr9i8nkkb5yr") (f (quote (("test_mode"))))))

(define-public crate-ego_types-0.2.3 (c (n "ego_types") (v "0.2.3") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0lh3vyap7y929hlc9v1sqn4w6rqi29hrhcsjglfga44b9jh5znxn") (f (quote (("test_mode"))))))

