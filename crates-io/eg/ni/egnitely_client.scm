(define-module (crates-io eg ni egnitely_client) #:use-module (crates-io))

(define-public crate-egnitely_client-0.1.0 (c (n "egnitely_client") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.47.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03y35ajsx0qfblrrlnb3bv9212s21sfcsn04dnzbgvamldjr537z") (y #t)))

(define-public crate-egnitely_client-0.1.1 (c (n "egnitely_client") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6l27kjka6lxyypgabjwr559j15qg8zwj07fpdy8997kdhd06ay") (y #t)))

(define-public crate-egnitely_client-0.1.2 (c (n "egnitely_client") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bqj95ffan625yapjbi7sk4pkk3fgjvcf5l9yfzjmiwah2js9c88") (y #t)))

(define-public crate-egnitely_client-0.1.3 (c (n "egnitely_client") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "multer") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g411nybbchywmkql63h9wwq515jpm4z4bp7d2h8wdlmxkkbb6bv") (y #t)))

(define-public crate-egnitely_client-0.1.4 (c (n "egnitely_client") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "multer") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1iklczjdz6qvkmg56a5aa2m1nir51aqv6yi7922x8dlxmpclcpkd") (y #t)))

(define-public crate-egnitely_client-0.1.5 (c (n "egnitely_client") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "multer") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "016safq2iy9n5qzkk70pwyaf4dbd9bcq76gq0jsdjdqj7s4gcjvd")))

