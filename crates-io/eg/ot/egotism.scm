(define-module (crates-io eg ot egotism) #:use-module (crates-io))

(define-public crate-egotism-0.1.0 (c (n "egotism") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.3") (d #t) (k 0)))) (h "07fwc1qnfijyagscj900mganiz2fbbl9gmz416w48yj849ad5lw0")))

