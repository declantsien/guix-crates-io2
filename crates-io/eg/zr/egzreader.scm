(define-module (crates-io eg zr egzreader) #:use-module (crates-io))

(define-public crate-egzreader-0.1.0 (c (n "egzreader") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0pj6y2svdc9wljzrnj9jbsfdjybsg9qm0cbn8c637ca9c609hzgv")))

(define-public crate-egzreader-1.0.0 (c (n "egzreader") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0p2jqbdd6ld7vfxw0i1c6hdwpwdy8hsgk9i2jax11dib92i6nw7f")))

(define-public crate-egzreader-1.0.1 (c (n "egzreader") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0sifs75666rkby00hn6aqjdyhyryhm814h3g5h31pm1m0yfqlf3z")))

(define-public crate-egzreader-1.0.2 (c (n "egzreader") (v "1.0.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0rqijmjwc2jqy75iaq72i0v29g0mlbn6m7sy9c3jqf0lj4jpw0dl")))

(define-public crate-egzreader-1.0.3 (c (n "egzreader") (v "1.0.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1zdcgxhanyhg0fr5fa497z7yrdh2if60knrj237rmv0aj2nzin84")))

(define-public crate-egzreader-2.0.0 (c (n "egzreader") (v "2.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1nffwpab9d1h0y0q1w0k247ciq7gng8fm2x3bfddikv5hdx3amdx")))

(define-public crate-egzreader-2.0.1 (c (n "egzreader") (v "2.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1xzmvgx4wjba89xywrafar2vkbj5a3p6wkyk2b8v7rx7nbkyb5fi")))

(define-public crate-egzreader-2.0.2 (c (n "egzreader") (v "2.0.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "16b61kh56iwm014bvzrkr6z36ib023nr7q1cdfrkm4y2gicrwxib")))

(define-public crate-egzreader-2.0.3 (c (n "egzreader") (v "2.0.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "032yrwqdiy7fkibnwjgf4x4ahil8dpf1xalkbzh23nl408nvql40")))

(define-public crate-egzreader-2.0.4 (c (n "egzreader") (v "2.0.4") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1zd52a6q57wwhbgijm5ac5dnbhypc8h6yjgzx5r2k8dabqg6b3s5")))

