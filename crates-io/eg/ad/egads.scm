(define-module (crates-io eg ad egads) #:use-module (crates-io))

(define-public crate-egads-0.9.0 (c (n "egads") (v "0.9.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oauth2") (r "^4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1lpibzsj7vpiipb9gxvfxwxkqyvl0rgj24f6kpn8zv8b1ll434gp")))

(define-public crate-egads-0.10.0 (c (n "egads") (v "0.10.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.1") (d #t) (k 0)) (d (n "reqwest-retry") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)))) (h "095crdjzcd3k3m51mqlalxwva9qzg0qajh7q2x7c7zm996v77ngk")))

