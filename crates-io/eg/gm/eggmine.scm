(define-module (crates-io eg gm eggmine) #:use-module (crates-io))

(define-public crate-eggmine-1.0.0 (c (n "eggmine") (v "1.0.0") (d (list (d (n "indicatif") (r "^0.14") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "0v72sazihhyhfr4ps069xxmzxjhndsm77l9ym5ccafnp63zyp066")))

(define-public crate-eggmine-1.0.1 (c (n "eggmine") (v "1.0.1") (d (list (d (n "indicatif") (r "^0.14") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "0jk6zjdmaw6v0djip5pbnv9mrj5mdlrhhyqlmfm6fmrbn7jvlsna")))

(define-public crate-eggmine-1.0.2 (c (n "eggmine") (v "1.0.2") (d (list (d (n "indicatif") (r "^0.14") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "1ym4ynmb8q6142h43knnzfvnqj7scf6s9xsgc440blcd765gz8ak")))

(define-public crate-eggmine-2.0.0 (c (n "eggmine") (v "2.0.0") (d (list (d (n "indicatif") (r "^0.15") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1pcb99msg5jbravlfixvx4f8ka1rr44nfpwar3vf2ij4qchvwwpr")))

(define-public crate-eggmine-2.0.1 (c (n "eggmine") (v "2.0.1") (d (list (d (n "indicatif") (r "^0.15") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "17b24w1kz2kg4qj5l60fchcjb64bmvpk75jycckslc49nwzphv85")))

(define-public crate-eggmine-2.0.2 (c (n "eggmine") (v "2.0.2") (d (list (d (n "indicatif") (r "^0.15") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0zzn1yadz3fxmhvx7jmsskhs6lxi8dg4j7199lggx0armb4a0fqy")))

(define-public crate-eggmine-2.0.3 (c (n "eggmine") (v "2.0.3") (d (list (d (n "indicatif") (r "^0.15") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1fdv07g55z6rf0adb85ri02fvxcbdwqp544ggxa4ama53j0wa3yv")))

(define-public crate-eggmine-2.0.4 (c (n "eggmine") (v "2.0.4") (d (list (d (n "indicatif") (r "^0.15") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "14igcv1w5ma3aab4zjyzshwmdpg4wcp5d1zn8la9igvj1rsj8yim")))

(define-public crate-eggmine-3.0.0 (c (n "eggmine") (v "3.0.0") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0khsckigy8qn8dm3cm298vgq32qi83wpwh9mf42q477l3pv5z4gd")))

(define-public crate-eggmine-3.0.1 (c (n "eggmine") (v "3.0.1") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0bb1ra67igxhpvwwwkiixq4pczgdw961jwyyyqhz63dwyjfc0h2w")))

(define-public crate-eggmine-3.1.0 (c (n "eggmine") (v "3.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1yjgbmkg7jqhbr1sqi2z4l3xb5sxx5mzb2lb03fcvkm5kx3lrr7q")))

(define-public crate-eggmine-3.1.1 (c (n "eggmine") (v "3.1.1") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0mdb657mpgdrj3v2rw12m2fc1gi4q0w0jqazphdfis06sg895p13")))

(define-public crate-eggmine-3.1.2 (c (n "eggmine") (v "3.1.2") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "18pm56lxzgii62jy811nsklmb7id6nfdsnhd9vrmvjjjp4fpjqg4")))

(define-public crate-eggmine-3.1.3 (c (n "eggmine") (v "3.1.3") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0m1liqnzb41vlgk0h8df04xskjn4hxgc1wcqfpsn992wrz6fx27l")))

(define-public crate-eggmine-3.1.4 (c (n "eggmine") (v "3.1.4") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1lshqd2s282n44rd3mjqpj73izf2r7w54ydkm3qg0jbifkyxzgk5")))

(define-public crate-eggmine-3.1.5 (c (n "eggmine") (v "3.1.5") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0gdgz2837v945z81mg779lh0cjak8fnv8if5a3mjyi0z1jxzs9gs")))

(define-public crate-eggmine-3.1.6 (c (n "eggmine") (v "3.1.6") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "126a4gv4iq3vmz5d8c0fn1nq9mk873kxazgk3d015sgyjnkjb276")))

(define-public crate-eggmine-3.1.7 (c (n "eggmine") (v "3.1.7") (d (list (d (n "crossbeam-utils") (r "^0.8.10") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0vj1npqfml1xxg6mgx8k06lhczc8rnfhq236xq5dsrl905acw106")))

(define-public crate-eggmine-3.1.8 (c (n "eggmine") (v "3.1.8") (d (list (d (n "crossbeam-utils") (r "^0.8.10") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1f32ss1g35z8n17x4hb1ma2nj8m89np5308l1pjn4aiypvnfxicb")))

(define-public crate-eggmine-3.1.9 (c (n "eggmine") (v "3.1.9") (d (list (d (n "crossbeam-utils") (r "^0.8.10") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1ig0f6j33yjl2qx4lza2apx8jc089xag2lnqkb0qlfmzml7d5sdm")))

(define-public crate-eggmine-3.2.0 (c (n "eggmine") (v "3.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "09y880b9pq7xfbbmg0rf336pqadqyyg6nyssbyx53sl332wqj61y")))

(define-public crate-eggmine-3.2.1 (c (n "eggmine") (v "3.2.1") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (f (quote ("with_rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1gxvfirknrm1jbhw5chpgqknyfrid86r4aw71f5gdfj0aaniv6p6")))

(define-public crate-eggmine-3.2.2 (c (n "eggmine") (v "3.2.2") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "19099i0zmgxg6ick4xs4358j9kjiqqbswdm5x7fi5pm7nxv3vgi3")))

(define-public crate-eggmine-3.3.0 (c (n "eggmine") (v "3.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0vkig4afmkdyx9bdchhbzk2vgv2bh737w1hf4fbbgwfsc4wb1w20")))

(define-public crate-eggmine-3.3.1 (c (n "eggmine") (v "3.3.1") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0i6j1d415bqq3yja07ynffapsq56bq1ckw6dqqrkqxl7pfg9f71g")))

(define-public crate-eggmine-3.4.0-alpha (c (n "eggmine") (v "3.4.0-alpha") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "09fjbcxldhs81zqdn38za7xymqs82my904wmj4wjmmndw7v3mk4b")))

(define-public crate-eggmine-3.4.0-alpha.1 (c (n "eggmine") (v "3.4.0-alpha.1") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "16rrkymn4jb56iv7j7xbak32in0hhab73cp8imzbvvfjm4xk4m00")))

(define-public crate-eggmine-3.4.0-alpha.2 (c (n "eggmine") (v "3.4.0-alpha.2") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "19kpjzc4zklh1za85g430ww796dz7v1gjq6gb2s4mlzna6l3lgjq")))

(define-public crate-eggmine-3.4.0 (c (n "eggmine") (v "3.4.0") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1pd6swvfa3smina94yl3bcj84b5b5ps7d0bvspcwhbnn1cvahrk7")))

(define-public crate-eggmine-3.4.1 (c (n "eggmine") (v "3.4.1") (d (list (d (n "crossbeam-utils") (r "^0.8.15") (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "03win3w6n8ijzkqs0s7zdrw4r3hzbalwz2licqsmxmxgnhs51z5a")))

