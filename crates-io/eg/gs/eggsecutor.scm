(define-module (crates-io eg gs eggsecutor) #:use-module (crates-io))

(define-public crate-eggsecutor-1.0.0 (c (n "eggsecutor") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "0zz3xdzzlikr08g7my0zy6aw6qgf9f0381pg7xw75x24lakacj1q")))

(define-public crate-eggsecutor-1.1.0 (c (n "eggsecutor") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1iwvys3rhj29ycpks4klwh05mj91ivwmm2jn25ikdr33x87h50zd")))

(define-public crate-eggsecutor-1.2.0 (c (n "eggsecutor") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "00ni8ap9wy6q92nzhi5kxqwdszlnf9k5k0x7yyvlml460qmd863f")))

(define-public crate-eggsecutor-2.0.0 (c (n "eggsecutor") (v "2.0.0") (d (list (d (n "clap") (r "^3.1.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "02s3w09qahd0fa8xh6l5xgp4f6qmkmbsz6206z0pxbqfvlal3qhr")))

