(define-module (crates-io eg gs eggshell) #:use-module (crates-io))

(define-public crate-eggshell-0.1.0 (c (n "eggshell") (v "0.1.0") (d (list (d (n "async-trait") (r ">=0") (d #t) (k 0)) (d (n "bollard") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=0") (d #t) (k 0)) (d (n "tokio") (r ">=0") (f (quote ("full"))) (d #t) (k 0)))) (h "012qyy8gjr9wifx8y4j0sbgfdnybccxlgql7g5fy0sz7jg54bld4") (y #t)))

(define-public crate-eggshell-0.1.1 (c (n "eggshell") (v "0.1.1") (d (list (d (n "async-trait") (r ">=0") (d #t) (k 0)) (d (n "bollard") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=0") (d #t) (k 0)) (d (n "tokio") (r ">=0") (f (quote ("full"))) (d #t) (k 0)))) (h "14834nb2rf63d689rd16cnc1zbydq0na47hkd2biq5w9msz706m2")))

(define-public crate-eggshell-0.1.2 (c (n "eggshell") (v "0.1.2") (d (list (d (n "async-trait") (r ">=0") (d #t) (k 0)) (d (n "bollard") (r ">=0") (d #t) (k 0)) (d (n "lazy_static") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=0") (d #t) (k 0)) (d (n "tokio") (r ">=0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p309cvi1w783mi8pn2f2i16x7m2q2g8kablx5ca416lyxg3fnyh")))

(define-public crate-eggshell-0.1.3 (c (n "eggshell") (v "0.1.3") (d (list (d (n "async-trait") (r ">=0") (d #t) (k 0)) (d (n "bollard") (r ">=0") (d #t) (k 0)) (d (n "lazy_static") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=0") (d #t) (k 0)) (d (n "tokio") (r ">=0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qcb4zx946jdhwni3ak5x85lyfrdgk6ibjcmihj8qjb4sbf7y38f")))

