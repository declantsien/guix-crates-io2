(define-module (crates-io eg gl egglang) #:use-module (crates-io))

(define-public crate-egglang-0.1.0 (c (n "egglang") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08g9r7pak8b1rq0w8gxxhcsln2rmsvwv8vvg3xwbnr6f05f642b4")))

(define-public crate-egglang-0.1.5 (c (n "egglang") (v "0.1.5") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1v8c007wxf7hlxn5g8fanjmhlcvx8sxz81n9dr7bc1gkwbwdz3rk")))

(define-public crate-egglang-0.2.0 (c (n "egglang") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ppw9p1ms6pm247fi6pwdhssmbqy3fczfhlch8jcp9iam9y287ny")))

(define-public crate-egglang-0.3.0 (c (n "egglang") (v "0.3.0") (d (list (d (n "arcstr") (r "^1.1.5") (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1x1nh7v88yf8ikv4lbbjsk24qfs9vhnf9aqy9285012nf1zj3688") (f (quote (("std"))))))

(define-public crate-egglang-0.3.1 (c (n "egglang") (v "0.3.1") (d (list (d (n "arcstr") (r "^1.2.0") (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1fx5bx9qgljfnm22rz9z4sf2s46b8idxhjwm1hy4fjvckbfr1ik4") (f (quote (("std"))))))

(define-public crate-egglang-0.3.2 (c (n "egglang") (v "0.3.2") (d (list (d (n "arcstr") (r "^1.2.0") (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "15qi5bwd7i7cr4bq0jj0iw4jsff2bid2l0nlw3573sg8bxj6jsi7") (f (quote (("std"))))))

