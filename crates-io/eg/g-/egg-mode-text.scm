(define-module (crates-io eg g- egg-mode-text) #:use-module (crates-io))

(define-public crate-egg-mode-text-0.1.0 (c (n "egg-mode-text") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 2)))) (h "00qsn5c3prsakh17zjwyrpvs22i4r492yslridjk78g2hjcwlyi4")))

(define-public crate-egg-mode-text-1.14.7 (c (n "egg-mode-text") (v "1.14.7") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 2)))) (h "1jsd5cdhwydw9w53prym8jmhf1g2fjvz3ss7d7nvw86csdpybcb3")))

(define-public crate-egg-mode-text-1.15.0 (c (n "egg-mode-text") (v "1.15.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.15") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 2)))) (h "1b3pdma7w2s1v58xrwf1r8hwc2jr2mjnz1djpag7vcmwlipzx0wc")))

(define-public crate-egg-mode-text-1.15.1 (c (n "egg-mode-text") (v "1.15.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.15") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (d #t) (k 2)))) (h "09qws8j15ziv9ayywl2g9452474yzc0g9561dy87irw1f1q6r59j")))

