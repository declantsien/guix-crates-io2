(define-module (crates-io kj ps kjps) #:use-module (crates-io))

(define-public crate-kjps-0.1.0 (c (n "kjps") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "04myryvb99bx4i0ghzzzyvpw1vdpnxbiw36jlkm5in830111pa8b")))

(define-public crate-kjps-0.1.1 (c (n "kjps") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0y7k3h17a21isq35z705fsyg5lv287gsgjipx63rzghnyh5z09nb")))

(define-public crate-kjps-0.1.2 (c (n "kjps") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1j7khj7mzyxs7qanr721clvffa8cclb4rpljmfikvl7w5qklr68f")))

