(define-module (crates-io ac e1 ace128_driver) #:use-module (crates-io))

(define-public crate-ace128_driver-0.1.0 (c (n "ace128_driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "0w955qdhdxfrim58vj3z477wwvkx7968kdn928g2vhxy0hkwf5wv")))

(define-public crate-ace128_driver-0.1.1 (c (n "ace128_driver") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "07d04g34973apyx97vcw5k962w30v558ja71igq22q4yrkdw9jpn")))

(define-public crate-ace128_driver-0.1.2 (c (n "ace128_driver") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "00f25c71ycaafa0fckfj3bj283q4xd2hg1p3nhqwmnsjh8y7125j")))

