(define-module (crates-io ac _c ac_crawler_types) #:use-module (crates-io))

(define-public crate-ac_crawler_types-0.1.0 (c (n "ac_crawler_types") (v "0.1.0") (d (list (d (n "mungos") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "09y6n9bqpc4qg0v3q92g03r3wkbfy6s8qda5jz1pkkicablny5z9")))

(define-public crate-ac_crawler_types-0.1.1 (c (n "ac_crawler_types") (v "0.1.1") (d (list (d (n "mungos") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0ng0mgv4f73fiqrc2sfmlz8h8a964k9p260j1zxy38qwgcas534y")))

(define-public crate-ac_crawler_types-0.1.2 (c (n "ac_crawler_types") (v "0.1.2") (d (list (d (n "mungos") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "01v7mg263wy205s0b0xbxrndh3n0jlp8ryh710qp6b1wfrm80aig")))

(define-public crate-ac_crawler_types-0.1.3 (c (n "ac_crawler_types") (v "0.1.3") (d (list (d (n "mungos") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "15rpnhz85qmszfadbavgwkqd3x38cwhla6kpv55g4ii2cdya47b4")))

(define-public crate-ac_crawler_types-0.1.4 (c (n "ac_crawler_types") (v "0.1.4") (d (list (d (n "mungos") (r "^0.3.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1m7bjrzn7hsjvixlh3rxw6y2gshr1g0l2l785nrr9hajfd6ddv48")))

(define-public crate-ac_crawler_types-0.1.5 (c (n "ac_crawler_types") (v "0.1.5") (d (list (d (n "bson") (r "^2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "13sbv0kwqlrjrqrf4kfsjya83pra7j83lxlhqpxwzhp3l46m9fmz")))

