(define-module (crates-io ac -f ac-ffmpeg) #:use-module (crates-io))

(define-public crate-ac-ffmpeg-0.15.0 (c (n "ac-ffmpeg") (v "0.15.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09r4zjzfb5xziw7007q1jlg4j98syr95irb0zwbripp8qkpw31r6")))

(define-public crate-ac-ffmpeg-0.15.1 (c (n "ac-ffmpeg") (v "0.15.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f15798r7bfvg6m3fcxbcprbb1k2jr4c98anmyhm1vsafihcycpj")))

(define-public crate-ac-ffmpeg-0.15.2 (c (n "ac-ffmpeg") (v "0.15.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0aaqc3j34mflfgxhkbkdiql5k9v5m87gg4v152q60qfpxc89l8l9")))

(define-public crate-ac-ffmpeg-0.15.3 (c (n "ac-ffmpeg") (v "0.15.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1znx250kxmchalc7z37h2lcq8brsfxradsjmx8zkaklvajjpf3hz")))

(define-public crate-ac-ffmpeg-0.15.4 (c (n "ac-ffmpeg") (v "0.15.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1yqcadi9f85dyq3278r6xgwmxx35i8zcfzmas7rj1pikfqj9dfl3")))

(define-public crate-ac-ffmpeg-0.15.5 (c (n "ac-ffmpeg") (v "0.15.5") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "17s5xv2q1g6ibx02lxvisjnmqm9iypbyjy26vzani8yk7zfkxj64")))

(define-public crate-ac-ffmpeg-0.15.6 (c (n "ac-ffmpeg") (v "0.15.6") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1pacn57nynx75bc4jagvfwmji1qh98d28xhidjs6hvgfykfcpb74")))

(define-public crate-ac-ffmpeg-0.16.0 (c (n "ac-ffmpeg") (v "0.16.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0z88499igmzz1y6vy7f6ykdd06msgv2bb2zvpl8330r578hasafj")))

(define-public crate-ac-ffmpeg-0.17.0 (c (n "ac-ffmpeg") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0jr4kn3y8v1f21idy5xb9b3187wxjgyxnxgwvl08r9c43djrf055")))

(define-public crate-ac-ffmpeg-0.17.1 (c (n "ac-ffmpeg") (v "0.17.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0s97f262mz0x0in1y5vad1cx3qyqcd61slwszffdbmnc3085y2q3")))

(define-public crate-ac-ffmpeg-0.17.2 (c (n "ac-ffmpeg") (v "0.17.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "17gxa0f9aigcm8pshdkx79hwxbp25a7ib0y6ng0znwsgh5qmyw3a") (y #t)))

(define-public crate-ac-ffmpeg-0.17.3 (c (n "ac-ffmpeg") (v "0.17.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1lk5hqbn5d9s1kqiw659v8lxjddnpgl547wdm2k0wg4b7k54bdjd") (y #t)))

(define-public crate-ac-ffmpeg-0.17.4 (c (n "ac-ffmpeg") (v "0.17.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "027xk4kd8ak93wd4b6nq9lfijvdpqqziv2zpwl5hvppl26jlmj6n")))

(define-public crate-ac-ffmpeg-0.18.0 (c (n "ac-ffmpeg") (v "0.18.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0xhwa3dp1lipbv1igiwy8xjvc3kki9b9zik98j7d1mhcx2lpqxxj")))

(define-public crate-ac-ffmpeg-0.18.1 (c (n "ac-ffmpeg") (v "0.18.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0z53dp5a7xw0yydbywvn05a2wm2wpnwcqa0xanh971h3v5ydgla3")))

