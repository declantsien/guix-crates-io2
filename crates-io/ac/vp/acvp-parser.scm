(define-module (crates-io ac vp acvp-parser) #:use-module (crates-io))

(define-public crate-acvp-parser-0.1.0 (c (n "acvp-parser") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0yyk0iazdzqqcfswpwpbi67qi1g4c24kjcvjixpcjbwi23z42y68")))

(define-public crate-acvp-parser-0.1.1 (c (n "acvp-parser") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08qmlqg94m6hgm5wq6xk00cg9ibyhp6ja6z1kkzdjxzxwhjxdnib")))

