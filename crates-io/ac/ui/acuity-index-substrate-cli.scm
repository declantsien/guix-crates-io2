(define-module (crates-io ac ui acuity-index-substrate-cli) #:use-module (crates-io))

(define-public crate-acuity-index-substrate-cli-0.2.0 (c (n "acuity-index-substrate-cli") (v "0.2.0") (d (list (d (n "acuity-index-substrate-api") (r "^0.2.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "byte-unit") (r "^5.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "subxt") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fcxbxmd5kylaggw29hf2wm9xjm78sgly23g2nk04qmf2lj2vxvk")))

