(define-module (crates-io ac ui acuity-index-substrate-api) #:use-module (crates-io))

(define-public crate-acuity-index-substrate-api-0.2.0 (c (n "acuity-index-substrate-api") (v "0.2.0") (d (list (d (n "acuity-index-substrate") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "06py106w79d7h5ipdfw2i0d59pg9zb8xfqld3g5s0z5n4qfgddi5")))

