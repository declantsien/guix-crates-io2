(define-module (crates-io ac li aclivo) #:use-module (crates-io))

(define-public crate-aclivo-0.1.0 (c (n "aclivo") (v "0.1.0") (h "1n6s5h68qkl1hcdsj48wljhy4jaimhspl203pidzm2bw9ivxlvwy")))

(define-public crate-aclivo-0.1.1 (c (n "aclivo") (v "0.1.1") (h "1nn9vb2q50qff5rz5vr51rxvgs8b64vgfs10gbyq99s9fx8pc8q2")))

