(define-module (crates-io ac co accord) #:use-module (crates-io))

(define-public crate-accord-0.1.0 (c (n "accord") (v "0.1.0") (h "0zj61w4n131k481s6k05b3fgx8mrsc3v7f46d0y8ywzfwpl2viaf")))

(define-public crate-accord-0.1.1 (c (n "accord") (v "0.1.1") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "16xzr481p8qgqg1fvq62bgfncdd0ch5nrh8gdinsvvn5fhg3v5av")))

(define-public crate-accord-0.1.2 (c (n "accord") (v "0.1.2") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1n324d22pk935vlr6w7sjf6dfsfk2i1zvc54whbayxx2r22d4gg9")))

(define-public crate-accord-0.2.0 (c (n "accord") (v "0.2.0") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "19cc4b7cailq6wyz136a2bzyb7a0mz4b5lss2qq4m1yl77gf9kca") (y #t)))

(define-public crate-accord-0.2.1 (c (n "accord") (v "0.2.1") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1dmjhvd88gq8vf4pfkpna9vn8j00lmq6d6iqfpmgijsgwxrvf20p")))

(define-public crate-accord-0.2.2 (c (n "accord") (v "0.2.2") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1v02ybxwjqashhwh5a6hgqkpqyjvyj9jy3sz0cjzwhlsz0yc9n1i") (f (quote (("inclusive_range"))))))

