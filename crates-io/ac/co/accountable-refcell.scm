(define-module (crates-io ac co accountable-refcell) #:use-module (crates-io))

(define-public crate-accountable-refcell-0.1.0 (c (n "accountable-refcell") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1ch4cl79gfxkf8w2dy01rf4s9vw6y9bwwdm87zjb1ax2qz58fc4w")))

(define-public crate-accountable-refcell-0.2.0 (c (n "accountable-refcell") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "191wcb4p44dgyzyrg0jpmf9zkjk4nlvi4ci8nzliwd4924r9g6if")))

(define-public crate-accountable-refcell-0.2.1 (c (n "accountable-refcell") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1wn3x0s17kazl39jjwzsc5vq0hkw73cvc3kmhbihmkqzyakbpqpn")))

