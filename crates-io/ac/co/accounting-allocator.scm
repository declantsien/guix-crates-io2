(define-module (crates-io ac co accounting-allocator) #:use-module (crates-io))

(define-public crate-accounting-allocator-0.1.0 (c (n "accounting-allocator") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "0vqb0vlrw8a8x50inf70ksdkyvjwq4sbr0cyvivbp05d0m6308cl") (r "1.61.0")))

(define-public crate-accounting-allocator-0.2.0 (c (n "accounting-allocator") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.14") (d #t) (k 2)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "0j6yj57hsjgazs9msllbfgbf9j780i34jjbcfschw8j2ipsr20lp") (r "1.61.0")))

