(define-module (crates-io ac co accounting) #:use-module (crates-io))

(define-public crate-accounting-0.1.0 (c (n "accounting") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rxzqgnjypr1a5q1y4a5i42niakdp2509k74sb3prkkf4xijs2k7")))

(define-public crate-accounting-0.1.1 (c (n "accounting") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s2bmln5h0zfybd3df728qhah6x2hcg3b5hanfgfg9rawiifnglk")))

(define-public crate-accounting-0.2.0 (c (n "accounting") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.21") (o #t) (d #t) (k 0)))) (h "03zmhz32aqqzadbc9yszlfxdvfspalx470583a0vhkr973pc3l82") (f (quote (("decimal" "rust_decimal"))))))

