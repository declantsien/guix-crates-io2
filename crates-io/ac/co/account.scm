(define-module (crates-io ac co account) #:use-module (crates-io))

(define-public crate-account-0.1.1-beta1 (c (n "account") (v "0.1.1-beta1") (d (list (d (n "bip32") (r "^0.5.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std" "getrandom"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0") (f (quote ("static_secrets"))) (d #t) (k 0)))) (h "085s8c6vzz05frd9bz2xpzks329yx18d9fxz8csgs2fyzj8k03yl")))

