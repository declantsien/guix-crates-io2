(define-module (crates-io ac co accordion) #:use-module (crates-io))

(define-public crate-accordion-0.1.0 (c (n "accordion") (v "0.1.0") (h "03fws70g1x0afgv8vis6ary1gcjlpaqyr73bl7sb8r6d4lrf7vlf")))

(define-public crate-accordion-0.2.0 (c (n "accordion") (v "0.2.0") (h "0y501h2z081hsdgpza9g2lv28ifjfbacxzi8flk1rr8saarbb8h4")))

