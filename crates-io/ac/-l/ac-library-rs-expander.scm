(define-module (crates-io ac -l ac-library-rs-expander) #:use-module (crates-io))

(define-public crate-ac-library-rs-expander-0.1.0 (c (n "ac-library-rs-expander") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bipyvis2difbigzpkz8l2nlp36hff8bd3h1yy05q7cyl96n1mww")))

