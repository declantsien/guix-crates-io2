(define-module (crates-io ac -l ac-library-rs) #:use-module (crates-io))

(define-public crate-ac-library-rs-0.1.0 (c (n "ac-library-rs") (v "0.1.0") (d (list (d (n "proconio") (r "=0.3.6") (d #t) (k 2)) (d (n "proconio-derive") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1v79qjs291lq476m49fdvzsrhfhplz7lig3brf892haccd937rjz") (r "1.42")))

(define-public crate-ac-library-rs-0.1.1 (c (n "ac-library-rs") (v "0.1.1") (d (list (d (n "proconio") (r "=0.3.6") (d #t) (k 2)) (d (n "proconio-derive") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0wzgfw0nd93sljmzan0807icj03daz04cs96jy7nji1gnzbymq89") (r "1.42")))

