(define-module (crates-io ac tl actl) #:use-module (crates-io))

(define-public crate-actl-0.1.1 (c (n "actl") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)) (d (n "sta") (r "= 0.1.10") (d #t) (k 0)))) (h "0j1n4dc1506gq4scaqq520cczrs026kfvh54nyy1xsp529c0xmyi")))

(define-public crate-actl-0.1.2 (c (n "actl") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)) (d (n "sta") (r "= 0.1.10") (d #t) (k 0)))) (h "1s378aqkl7p3zd6hpfbm9jmmazs4n4sb7x0yxdkd7q46032xw3rh")))

(define-public crate-actl-0.1.3 (c (n "actl") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)) (d (n "sta") (r "= 0.1.10") (d #t) (k 0)))) (h "0gqz40vbf27wv3ic3mjmk18f3wxhm4q8l04zdx3ix3xsrx822f5n")))

(define-public crate-actl-0.1.4 (c (n "actl") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)) (d (n "sta") (r "= 0.1.10") (d #t) (k 0)))) (h "0shgsnrp892dzbsi99l86h81rm7vcjlmy9lhz5pmfhwcx0kj619m")))

(define-public crate-actl-1.0.0 (c (n "actl") (v "1.0.0") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "ignite") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)))) (h "136jzb1x3mvm52ihzvvf43rzhdchj240y5f5c1f4qwm0z1hvxavj")))

(define-public crate-actl-1.0.1 (c (n "actl") (v "1.0.1") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "ignite") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.13") (d #t) (k 0)))) (h "0w46d66nmiqkbs4km0qanywxig432b1kqklixj0jidg4wbng8dqn")))

