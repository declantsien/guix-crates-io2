(define-module (crates-io ac ly aclysma_spirv_cross) #:use-module (crates-io))

(define-public crate-aclysma_spirv_cross-0.0.1 (c (n "aclysma_spirv_cross") (v "0.0.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0brc258psj31hwmi3xg66ffk35g1pbw9kqz4snzvp7qzlrv1iib4") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

