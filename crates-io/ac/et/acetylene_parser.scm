(define-module (crates-io ac et acetylene_parser) #:use-module (crates-io))

(define-public crate-acetylene_parser-0.1.0 (c (n "acetylene_parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0sgf2pxmg1jnd4lazsa8s0wrsfk7lrcfvpy9g2k97c29gl3kxisz")))

