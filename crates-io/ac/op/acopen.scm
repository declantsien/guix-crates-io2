(define-module (crates-io ac op acopen) #:use-module (crates-io))

(define-public crate-acopen-0.1.0 (c (n "acopen") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.6.0") (d #t) (k 0)))) (h "18j04iyp2jg577j5ldwwniyndgkgs7ykig91y3k6v7lg89c9n1p4")))

(define-public crate-acopen-0.1.1 (c (n "acopen") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.6.0") (d #t) (k 0)))) (h "1bg28dmr9i3iv1r9z1lc1fxxcfp4z31dkj7a75rl2d0vij6qm3kh")))

