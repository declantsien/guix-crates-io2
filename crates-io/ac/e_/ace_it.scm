(define-module (crates-io ac e_ ace_it) #:use-module (crates-io))

(define-public crate-ace_it-0.1.0 (c (n "ace_it") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8a0fp4b19r29pm4gnivs4q2221y83yx23da44xn1njspsi2h3j")))

(define-public crate-ace_it-0.1.1 (c (n "ace_it") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xkv7g9ifzzv0dk58fcmv81vpr8153rlagpxgqjvvg0f3f7y1rzz")))

