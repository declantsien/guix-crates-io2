(define-module (crates-io ac ho achoo) #:use-module (crates-io))

(define-public crate-achoo-1.0.0 (c (n "achoo") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vf0fvy5x81r06a5xg25qiymqgrxkbpy4qw3hmvn9njxq2ps7xz4")))

(define-public crate-achoo-1.0.1 (c (n "achoo") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ky879v0xmvfy6rwa0m9lh5vrvsxn4nrz8n8lgw6i7ir8h0v3gq")))

