(define-module (crates-io ac cu accursed-unutterable-type-id-derive) #:use-module (crates-io))

(define-public crate-accursed-unutterable-type-id-derive-0.0.0-pre.cursed (c (n "accursed-unutterable-type-id-derive") (v "0.0.0-pre.cursed") (d (list (d (n "fslock") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "16zrl339n2shahp5il9km3pl6947q0yg09wjxf1wr9764z289slh")))

(define-public crate-accursed-unutterable-type-id-derive-0.0.0-pre.ohno (c (n "accursed-unutterable-type-id-derive") (v "0.0.0-pre.ohno") (d (list (d (n "fslock") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "1n0ss6fyhgclnd3vz514a9g3k62mkxvqkrfagd5xrcph77chb1iw")))

