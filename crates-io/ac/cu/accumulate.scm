(define-module (crates-io ac cu accumulate) #:use-module (crates-io))

(define-public crate-accumulate-3.0.0 (c (n "accumulate") (v "3.0.0") (d (list (d (n "ink_env") (r "^3.0.0") (k 0)) (d (n "ink_lang") (r "^3.0.0") (k 0)) (d (n "ink_metadata") (r "^3.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ink_primitives") (r "^3.0.0") (k 0)) (d (n "ink_storage") (r "^3.0.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (o #t) (k 0)))) (h "1w0hn7gqxxqhv9zyvxc2ij2j30214x3lrm76yxf04s0cnq22q2n9") (f (quote (("std" "ink_primitives/std" "ink_metadata/std" "ink_env/std" "ink_storage/std" "ink_lang/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

