(define-module (crates-io ac cu accursed-unutterable-type-id) #:use-module (crates-io))

(define-public crate-accursed-unutterable-type-id-0.0.0-pre.cursed (c (n "accursed-unutterable-type-id") (v "0.0.0-pre.cursed") (d (list (d (n "accursed-unutterable-type-id-derive") (r "^0.0.0-pre.cursed") (d #t) (k 0)))) (h "1wc3x31cfzzv587dw535kzw689cnfmhr2kahfbgyrfpdj8rq4byq")))

(define-public crate-accursed-unutterable-type-id-0.0.0-pre.ohno (c (n "accursed-unutterable-type-id") (v "0.0.0-pre.ohno") (d (list (d (n "accursed-unutterable-type-id-derive") (r "^0.0.0-pre.ohno") (d #t) (k 0)))) (h "00w8kf43kr8bsvx1rglngjrrlh9ffijx3djrhj46xl1q68a631lb")))

(define-public crate-accursed-unutterable-type-id-0.0.1-pre.ohno (c (n "accursed-unutterable-type-id") (v "0.0.1-pre.ohno") (d (list (d (n "accursed-unutterable-type-id-derive") (r "^0.0.0-pre.ohno") (d #t) (k 0)))) (h "10l2mpab3wsw31xn3v5lpbrvi1vjgyv2jrxkmggc4kqqcjgh6pzj")))

(define-public crate-accursed-unutterable-type-id-0.0.1-pre.ohyes (c (n "accursed-unutterable-type-id") (v "0.0.1-pre.ohyes") (d (list (d (n "accursed-unutterable-type-id-derive") (r "^0.0.0-pre.ohno") (d #t) (k 0)))) (h "0bl01zg41d6vny3c6i6akl19kb5qf4ji6cx24x4xbdhm4812djf1")))

