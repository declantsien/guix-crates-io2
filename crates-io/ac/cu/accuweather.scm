(define-module (crates-io ac cu accuweather) #:use-module (crates-io))

(define-public crate-accuweather-0.1.0 (c (n "accuweather") (v "0.1.0") (d (list (d (n "mockito") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "15adpybjhj6jwjvcvd0b85h8y0m12as9lkh0nbq1n133gfv9qmnk")))

(define-public crate-accuweather-0.1.1 (c (n "accuweather") (v "0.1.1") (d (list (d (n "mockito") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1qy66ivwyi6s5978ga8gszd822v03rrncrr53zb41997kcbp9hk1")))

