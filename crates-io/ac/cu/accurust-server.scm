(define-module (crates-io ac cu accurust-server) #:use-module (crates-io))

(define-public crate-accurust-server-0.0.1 (c (n "accurust-server") (v "0.0.1") (h "077q7di5kw817l1xwby23s2l6g210b6spfb42s7gj0adc2j1lngg")))

(define-public crate-accurust-server-0.0.2 (c (n "accurust-server") (v "0.0.2") (h "1hrs4cl6sszwclya7cb1glp96fw7kacpijcr9aqy8811z0qhrppb")))

