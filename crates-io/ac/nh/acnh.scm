(define-module (crates-io ac nh acnh) #:use-module (crates-io))

(define-public crate-acnh-0.1.0 (c (n "acnh") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "04961aqjn5n60np85qhz0gc11nxpqss70l9jy1bn7gmgl8r66j38")))

(define-public crate-acnh-0.2.0 (c (n "acnh") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "01hcw381ni0ydk7icazcd69pzlbb8ql3lvbgv1z8kbva6i59ng6s")))

(define-public crate-acnh-0.3.0 (c (n "acnh") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0xk44d0ghs0l1cp9q4riz403c1c7yyrdnkzsh10gwfhw2h2gq63n")))

