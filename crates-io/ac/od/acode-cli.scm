(define-module (crates-io ac od acode-cli) #:use-module (crates-io))

(define-public crate-acode-cli-1.0.5 (c (n "acode-cli") (v "1.0.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1") (d #t) (k 0)))) (h "0apazyjfgd77q8814047rqrzr7ykjx22h7n3i1hvzpz1g2xq8a2q")))

(define-public crate-acode-cli-1.0.6 (c (n "acode-cli") (v "1.0.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1") (d #t) (k 0)))) (h "0yipzh5mf4cbs4l1ixy07gxrsiijcj16rbbjlc59x3i1l3vn7dd2")))

