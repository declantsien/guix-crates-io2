(define-module (crates-io ac ts acts-tag) #:use-module (crates-io))

(define-public crate-acts-tag-0.1.0 (c (n "acts-tag") (v "0.1.0") (d (list (d (n "acts-tag-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "acts-tag-value") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 2)))) (h "1wr5as31hlan11fa60pwbr35qvl7zd4w2cpcf706bq44ddl0wx2n")))

(define-public crate-acts-tag-0.2.0 (c (n "acts-tag") (v "0.2.0") (d (list (d (n "acts-tag-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "acts-tag-value") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 2)))) (h "1rghidnxzmczj05axrmd00fxg5f4lqcvdwihd7gq96xdvy6kr396")))

