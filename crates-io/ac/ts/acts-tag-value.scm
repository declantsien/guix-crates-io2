(define-module (crates-io ac ts acts-tag-value) #:use-module (crates-io))

(define-public crate-acts-tag-value-0.1.0 (c (n "acts-tag-value") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "1296lh229zlsjyrb587hk63ngx0rkmrldanx1kqghwxjz417b15i")))

(define-public crate-acts-tag-value-0.2.0 (c (n "acts-tag-value") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "17aa2zw33gf5f9iknmvvq6sly12f9v07d444q2wisxgk5pjzbyld")))

