(define-module (crates-io ac ts acts-tag-derive) #:use-module (crates-io))

(define-public crate-acts-tag-derive-0.1.0 (c (n "acts-tag-derive") (v "0.1.0") (d (list (d (n "acts-tag-value") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "07dd6mjpagjbzrfg81la0khmr3jwlm79sw06j7wbvfbazkqmvk59")))

(define-public crate-acts-tag-derive-0.2.0 (c (n "acts-tag-derive") (v "0.2.0") (d (list (d (n "acts-tag-value") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "0bd9nhc6yw13ba1kxidpzqya8zcc2dp98h994c9jsg995iazwjps")))

