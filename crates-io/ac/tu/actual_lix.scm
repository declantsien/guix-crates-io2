(define-module (crates-io ac tu actual_lix) #:use-module (crates-io))

(define-public crate-actual_lix-0.1.0 (c (n "actual_lix") (v "0.1.0") (h "16nlg1n141h40wwz2kglvksxn2zmyx10zjky7gnzya5gg4idwzx8") (y #t)))

(define-public crate-actual_lix-0.1.1 (c (n "actual_lix") (v "0.1.1") (h "15xj2pwpkvgkcs7yzji6gwhiy774gkj71nljbynbsw91q391gqhn")))

