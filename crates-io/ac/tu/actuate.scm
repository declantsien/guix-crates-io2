(define-module (crates-io ac tu actuate) #:use-module (crates-io))

(define-public crate-actuate-0.1.0 (c (n "actuate") (v "0.1.0") (h "095c05r8x7azk0ps6wsjay09dh5i91k5jdmqr7mp7gmfhprsbcz3")))

(define-public crate-actuate-0.2.0-alpha.1 (c (n "actuate") (v "0.2.0-alpha.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "186assa7ja1gfkymdr89abwv18ylp78ychm57d5bm9a4rlcr57nd")))

(define-public crate-actuate-0.2.0 (c (n "actuate") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "0v4g911h5c6nm8id4yvd2r7p859ygiqidpgc7rl0vpm29ihrybjg")))

(define-public crate-actuate-0.3.0-alpha.1 (c (n "actuate") (v "0.3.0-alpha.1") (d (list (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12w3yslx553nc4v75lfcw04smrimgay75ar5a3hnga115qb3mgyh")))

(define-public crate-actuate-0.3.0-alpha.2 (c (n "actuate") (v "0.3.0-alpha.2") (d (list (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18x0kai1l6d3ivp9jdnzh9zs22k3fvbnsi7x9j2q3ayb4zbv13j1")))

(define-public crate-actuate-0.3.0-alpha.3 (c (n "actuate") (v "0.3.0-alpha.3") (d (list (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13iylifgbqr3072091lv9q44psqnpx0168my8fqn12c816a1dxbm")))

(define-public crate-actuate-0.3.0-alpha.4 (c (n "actuate") (v "0.3.0-alpha.4") (d (list (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0fy6cdjd4ymgl47wsv9h0daqi5iq2q3cpcdl6z79nwh9fn2nnf3n")))

