(define-module (crates-io ac h- ach-once) #:use-module (crates-io))

(define-public crate-ach-once-0.1.0 (c (n "ach-once") (v "0.1.0") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0l79yrybjq2gma0hzsrn0nlhbq6s5411apzq3vy4k3iddv6izs17")))

(define-public crate-ach-once-0.1.1 (c (n "ach-once") (v "0.1.1") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0x1gnimb8m40dfnkwmvhhwb35xnvsqqb552nsfslx1vs308d49f2")))

(define-public crate-ach-once-0.1.2 (c (n "ach-once") (v "0.1.2") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1dg8nifzzi02wbln8b1hwi6fnzf5lmy3a1c5m9gdd2lkd80ld7kw")))

(define-public crate-ach-once-0.1.3 (c (n "ach-once") (v "0.1.3") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1zwjfk3zlrl9wrbhgnbiqs2ppr0ya41747v4wz79j78p5bsz7v4w")))

(define-public crate-ach-once-0.1.4 (c (n "ach-once") (v "0.1.4") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1nwva0r6pv2m50gv40lgr2j1iq604sdqjrwm3s2nw9wm6hsd5gaa")))

(define-public crate-ach-once-0.1.5 (c (n "ach-once") (v "0.1.5") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "03dpasz4mznjhhqawh4sf56gm4dvxhpm8hfmykjwbmn82l6yjh4g")))

