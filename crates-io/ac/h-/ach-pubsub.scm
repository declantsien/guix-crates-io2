(define-module (crates-io ac h- ach-pubsub) #:use-module (crates-io))

(define-public crate-ach-pubsub-0.1.0 (c (n "ach-pubsub") (v "0.1.0") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "02flac8rvw5a19iy8ghrqymjvkqxvwcpbp7jbg4zdl22zsljqjmg")))

(define-public crate-ach-pubsub-0.1.1 (c (n "ach-pubsub") (v "0.1.1") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "110rdac3xz6552jg56yfbdavdn8kiwv66zdann1rclw8hkqhvmrl")))

(define-public crate-ach-pubsub-0.1.2 (c (n "ach-pubsub") (v "0.1.2") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "16xxb4l059y2wfm9d7iw8g4fpvmi19h7013jz5927alsqj9lvnwr")))

(define-public crate-ach-pubsub-0.1.3 (c (n "ach-pubsub") (v "0.1.3") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "0fziw174md70yvhgi0ba1yvl4sagzmgj962hifk5b1j95jjf0nwp")))

(define-public crate-ach-pubsub-0.1.4 (c (n "ach-pubsub") (v "0.1.4") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "0ivs9lx12zpb8x46apbx3nrz6i40mvfmvcasf00syihs7fb6n20i")))

(define-public crate-ach-pubsub-0.1.5 (c (n "ach-pubsub") (v "0.1.5") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "18i2k0qjb6i4ghvwb14qyb675x464i6r55n8wf8pldy46k4jl7pd")))

(define-public crate-ach-pubsub-0.1.6 (c (n "ach-pubsub") (v "0.1.6") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0lhdmpn1mzrlkknc7yfdp4819yvjmsfwbic7x5wqb1d5id84spww")))

(define-public crate-ach-pubsub-0.1.7 (c (n "ach-pubsub") (v "0.1.7") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0vbqick4rbjf716fxxifibshrfrlm1a8cvn0gl0gpch1afl8apyz")))

(define-public crate-ach-pubsub-0.1.8 (c (n "ach-pubsub") (v "0.1.8") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0mmxri8fygcf3s3kn8f722k2bh6caqjmv4fmwc83ljzmlrmr0jxm")))

(define-public crate-ach-pubsub-0.1.9 (c (n "ach-pubsub") (v "0.1.9") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1skcx1bvnza5jkvhkzfmzci4nl6mvs88iag4hxjg9z2py6qlfx8j")))

(define-public crate-ach-pubsub-0.2.0 (c (n "ach-pubsub") (v "0.2.0") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0wzd7v02nfiw4q3v14wk0z9zvwf09kx20k19hv12s0462qv3jgwj") (f (quote (("default") ("alloc"))))))

