(define-module (crates-io ac h- ach-ring) #:use-module (crates-io))

(define-public crate-ach-ring-0.1.0 (c (n "ach-ring") (v "0.1.0") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1zk3cnyzq1ryxx35x69g41p9109k413mxr1mzldyhcd3wj9clag2")))

(define-public crate-ach-ring-0.1.1 (c (n "ach-ring") (v "0.1.1") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1p3nrgmgk6rnac9fcz2r2hxzk6dygbhj72lqy3d5ynaqgrkxlvy8")))

(define-public crate-ach-ring-0.1.2 (c (n "ach-ring") (v "0.1.2") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "09bx4mvk4qx8laaba9nrz4mbddpgskpmzwwsk3nzhg9p5z3r44bm")))

(define-public crate-ach-ring-0.1.3 (c (n "ach-ring") (v "0.1.3") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0lziznzb37ml4xskzlyazf9r9y3ddiw7nrd9imb0zcprli0913ml")))

(define-public crate-ach-ring-0.1.4 (c (n "ach-ring") (v "0.1.4") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0f723g1yxq0zdj5p6bjgrz4wp8zcjl4d0y19fddwnfc1bdxin1j6")))

(define-public crate-ach-ring-0.1.5 (c (n "ach-ring") (v "0.1.5") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1q2yqpqisy9nz85vrm3qbnkrcjx56chbv2f122fgg1cb88m88s7j")))

(define-public crate-ach-ring-0.1.6 (c (n "ach-ring") (v "0.1.6") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "125ak0d35f2qacwr7bg4wpablpakyp9zpmm7dxyp94cmb3grgprx")))

(define-public crate-ach-ring-0.1.7 (c (n "ach-ring") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 2)) (d (n "flume") (r "^0.10.11") (d #t) (k 2)) (d (n "interrupt") (r "^0.1") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1hpcy3zicnw7cm60nqazd7giwz7ipl7ykck2hr10cimicgv1z4x4")))

(define-public crate-ach-ring-0.1.8 (c (n "ach-ring") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 2)) (d (n "flume") (r "^0.10.11") (d #t) (k 2)) (d (n "interrupt") (r "^0.1") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0448gfslsc3y8igzq8nqvs7mskgdlh13fhp92063a73vsnphcwc5")))

(define-public crate-ach-ring-0.1.9 (c (n "ach-ring") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 2)) (d (n "flume") (r "^0.10.11") (d #t) (k 2)) (d (n "interrupt") (r "^0.1") (d #t) (t "cfg(target_os = \"none\")") (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1rwfxbvn9pylaqrq1gbikq594xi37ysrbhz6l2dvv2hn9vx1bdrw")))

