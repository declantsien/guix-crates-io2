(define-module (crates-io ac h- ach-mpmc) #:use-module (crates-io))

(define-public crate-ach-mpmc-0.1.0 (c (n "ach-mpmc") (v "0.1.0") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "1rbmq1n927zix791jvw58zd8qrv0n57qrav8nzb9w6103zz0nhcy")))

(define-public crate-ach-mpmc-0.1.1 (c (n "ach-mpmc") (v "0.1.1") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "0zcr1g9jclpy860vx3q130nz4qsiniivny5xlx0z9437ww0yhrl8")))

(define-public crate-ach-mpmc-0.1.2 (c (n "ach-mpmc") (v "0.1.2") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "0hf8jsk2kfkgzlajzz1agy5qs0glbfgz5nxpn3i8j68fc7jzmkvv")))

(define-public crate-ach-mpmc-0.1.3 (c (n "ach-mpmc") (v "0.1.3") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "0kw2b1r2hzhm3vi9904i5b2rhnyvwyl7cmbcvhpwwhihz73ymdw8")))

(define-public crate-ach-mpmc-0.1.4 (c (n "ach-mpmc") (v "0.1.4") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1kqbnjhx5wxxh6skcj3kv2qxrfg4q5a645bhiy4j29lvw8zzzyfq")))

(define-public crate-ach-mpmc-0.2.0 (c (n "ach-mpmc") (v "0.2.0") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "032qa1aj4mrplf9azmwbm6ygj0vfn1pb9wcj9pl2qigpdz1h28m8") (f (quote (("default") ("alloc"))))))

