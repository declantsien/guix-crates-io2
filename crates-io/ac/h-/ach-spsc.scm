(define-module (crates-io ac h- ach-spsc) #:use-module (crates-io))

(define-public crate-ach-spsc-0.1.0 (c (n "ach-spsc") (v "0.1.0") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "08r41xa5hmmj85n2vwgww61vrp9b3krvbsjfypnlxnf557i594r8")))

(define-public crate-ach-spsc-0.1.1 (c (n "ach-spsc") (v "0.1.1") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "06z03iwwvmdi3dffd42iqpzd7fmxajfpqiv7h33rhjffh53gqm0m")))

(define-public crate-ach-spsc-0.1.2 (c (n "ach-spsc") (v "0.1.2") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "0v69rq1vq1zghihz75b03d0cj85k1lmviaww1ldvigvp6bcwsw3c")))

(define-public crate-ach-spsc-0.2.0 (c (n "ach-spsc") (v "0.2.0") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "1xi9vbqld5r0k57yxx8ilrfcmirwvkvgb77fq726v0cwm0llry1y") (f (quote (("default") ("alloc"))))))

