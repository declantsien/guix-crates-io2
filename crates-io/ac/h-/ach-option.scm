(define-module (crates-io ac h- ach-option) #:use-module (crates-io))

(define-public crate-ach-option-0.1.0 (c (n "ach-option") (v "0.1.0") (d (list (d (n "interrupt") (r "^0.1") (d #t) (k 0)) (d (n "on_drop") (r "^0.1") (d #t) (k 2)) (d (n "spin_loop") (r "^0.1") (d #t) (k 0)) (d (n "util") (r "^0.1") (d #t) (k 0) (p "ach-util")))) (h "1pgwly4hspyrr7cipzw975p9p9k067ql11zzjl8vyq29fhy8622d")))

