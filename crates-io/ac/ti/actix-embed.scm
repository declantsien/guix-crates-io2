(define-module (crates-io ac ti actix-embed) #:use-module (crates-io))

(define-public crate-actix-embed-0.1.0 (c (n "actix-embed") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-test") (r "^0.1.0-beta.13") (d #t) (k 2)) (d (n "actix-web") (r "^4") (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "rust-embed") (r "^6") (d #t) (k 0)))) (h "1xyw0wn82a5nykb71vydmxabxs3pabydcy0chl8vjmyx4kh151y9")))

