(define-module (crates-io ac ti actix-default-responder) #:use-module (crates-io))

(define-public crate-actix-default-responder-0.1.0 (c (n "actix-default-responder") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1aizw754sdhqbdmz03ww0aa4amx6rzm2py9bndrj0r6rlvysapnw")))

