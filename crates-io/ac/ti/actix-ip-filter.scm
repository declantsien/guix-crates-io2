(define-module (crates-io ac ti actix-ip-filter) #:use-module (crates-io))

(define-public crate-actix-ip-filter-0.1.0 (c (n "actix-ip-filter") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1sybnl2lha7a3s4insffnyi979c22q66308i6ikk2rx1h15albd2")))

(define-public crate-actix-ip-filter-0.1.1 (c (n "actix-ip-filter") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "12ljcqnhrl3raffwmhzasbmwah8cxclqmnw5fm67xn6dinhphzli")))

(define-public crate-actix-ip-filter-0.1.2 (c (n "actix-ip-filter") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "17wib7f0kgx8yc5305hmppavkbmdzdkb451bdy2p2p3wj917nnrh")))

(define-public crate-actix-ip-filter-0.1.3 (c (n "actix-ip-filter") (v "0.1.3") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1srcwi4vm5f7qnx8s3qwhch6lxjqpxrb5a4x972qg841pq2wyj9d")))

(define-public crate-actix-ip-filter-0.2.0 (c (n "actix-ip-filter") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0c6rl1s7y0fj69f5akhw872gr0q7119bv2as7pfv8x1ia215yxgd")))

(define-public crate-actix-ip-filter-0.3.0-beta.0 (c (n "actix-ip-filter") (v "0.3.0-beta.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "10ka3lpkffxsn6mkl0kakamkprwkhxll3fhda7vbav8058rgkqa7") (y #t)))

(define-public crate-actix-ip-filter-0.3.0 (c (n "actix-ip-filter") (v "0.3.0") (d (list (d (n "actix-rt") (r "^2.5.0") (d #t) (k 2)) (d (n "actix-service") (r "^2.0.1") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.13") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0q8pqbwcs8m2dwrfxq1d3apx1mk4myggmm3g74i1819pdrj1mlw8")))

(define-public crate-actix-ip-filter-0.3.1 (c (n "actix-ip-filter") (v "0.3.1") (d (list (d (n "actix-rt") (r "^2.5.0") (d #t) (k 2)) (d (n "actix-service") (r "^2.0.1") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.14") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "00l637s4y3cy3wk6xfyg5a8f1jibkcqasm8xd77h4nqm6zmji8bl")))

