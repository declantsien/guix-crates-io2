(define-module (crates-io ac ti actix-route-config) #:use-module (crates-io))

(define-public crate-actix-route-config-0.1.0 (c (n "actix-route-config") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.3") (k 0)))) (h "137a5ilwz42mzxvi9pkq0a15lsk19kfpkh5n5s0s6a3hqz68iqq4")))

(define-public crate-actix-route-config-0.1.1 (c (n "actix-route-config") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.3") (k 0)))) (h "05b8y6rf942irpnii0i1s2ckfr3v9kr5m9mv90ym8rrw3jl9dny9")))

