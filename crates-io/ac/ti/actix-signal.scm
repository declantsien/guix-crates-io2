(define-module (crates-io ac ti actix-signal) #:use-module (crates-io))

(define-public crate-actix-signal-0.1.0 (c (n "actix-signal") (v "0.1.0") (d (list (d (n "actix") (r "^0.12") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (d #t) (k 2)) (d (n "actix-signal-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "actix-signal-derive") (r "^0.1") (d #t) (k 2)))) (h "1a9g4yn5kcj16xi6gmpjgszczvmyb82nyazcvkh4wh6mv1lqa0fg") (f (quote (("derive" "actix-signal-derive"))))))

(define-public crate-actix-signal-0.1.1 (c (n "actix-signal") (v "0.1.1") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (d #t) (k 2)) (d (n "actix-signal-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "actix-signal-derive") (r "^0.1") (d #t) (k 2)))) (h "1cd16rmjzz1hnrj06y716ympwaw08plyvk2ix4i9yvfhlyp5qpl1") (f (quote (("derive" "actix-signal-derive")))) (y #t)))

(define-public crate-actix-signal-0.2.0 (c (n "actix-signal") (v "0.2.0") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (d #t) (k 2)) (d (n "actix-signal-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "actix-signal-derive") (r "^0.1") (d #t) (k 2)))) (h "0dl0lmz7l0j24ypib21naf23nz3dd4v3i3klmid94wsxkipb7zzl") (f (quote (("derive" "actix-signal-derive"))))))

(define-public crate-actix-signal-0.3.0 (c (n "actix-signal") (v "0.3.0") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "actix-rt") (r "^2.9") (d #t) (k 2)) (d (n "actix-signal-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "actix-signal-derive") (r "^0.1") (d #t) (k 2)))) (h "0grq7r1ksh4304wh8a6mha0vfmsjwspyz516qp4d24jh6n4c9dnq") (f (quote (("derive" "actix-signal-derive"))))))

