(define-module (crates-io ac ti actix-interop) #:use-module (crates-io))

(define-public crate-actix-interop-0.1.0 (c (n "actix-interop") (v "0.1.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.9") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)))) (h "0vx2w6hnl1f1kp23q6j0wppgn29r02qvc2pzv3x96gvaf1s7rc2s")))

(define-public crate-actix-interop-0.1.1 (c (n "actix-interop") (v "0.1.1") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.9") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)))) (h "0x3j1yin3yq7wjwk73d7js2d858j0ar4f0gkh4mzxp811fyrspq7")))

(define-public crate-actix-interop-0.2.0 (c (n "actix-interop") (v "0.2.0") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.21") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)))) (h "1hd0jz6xpm8j7i1pwjj9cljackiiv1yyy994jjk0jnc9rwmd2bkg")))

(define-public crate-actix-interop-0.3.0 (c (n "actix-interop") (v "0.3.0") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)))) (h "1wfbpw5fac6pqs79sjviq24ncjclx26080n5hcjxqkpm47kglify")))

(define-public crate-actix-interop-0.4.0 (c (n "actix-interop") (v "0.4.0") (d (list (d (n "actix") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)) (d (n "scoped-tls-hkt") (r "^0.1.2") (d #t) (k 0)))) (h "0sb63aka2g952629h47s6vnwzd12lk52mmc31kmj1hw70agdxnqq")))

