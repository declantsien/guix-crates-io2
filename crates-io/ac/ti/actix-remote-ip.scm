(define-module (crates-io ac ti actix-remote-ip) #:use-module (crates-io))

(define-public crate-actix-remote-ip-0.1.0-alpha (c (n "actix-remote-ip") (v "0.1.0-alpha") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15fa8y708i4r5d9g1fc13hirf6ck8lrrx24dxkf9xav2yr2lhjlv")))

(define-public crate-actix-remote-ip-0.1.0 (c (n "actix-remote-ip") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13jz0sby2vnrdpqdqlkx5w8jfq6mxi49z60zwgqz6p3hsibv6abn")))

