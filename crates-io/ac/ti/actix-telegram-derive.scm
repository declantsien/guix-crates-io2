(define-module (crates-io ac ti actix-telegram-derive) #:use-module (crates-io))

(define-public crate-actix-telegram-derive-0.1.0 (c (n "actix-telegram-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "11xr5xc1w04c609d0fi0xmwncc4lx2qqjqrdrsc7xa8rgwwvgxki")))

(define-public crate-actix-telegram-derive-0.1.1 (c (n "actix-telegram-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.12") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "1i0c0h60pd25wm8b10md8qpj9j9nq65mr04ira5kmypbd0yf6x1v")))

(define-public crate-actix-telegram-derive-0.2.0 (c (n "actix-telegram-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vqbzlpnn95q7jlc8gxqba3ixskw015ww4ljfphbhxas49q3iglp")))

(define-public crate-actix-telegram-derive-0.2.1 (c (n "actix-telegram-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "005pkzzvhlpyk83pfhb07vzdbfbjcy5942d2kxbxmjijfgv2j7gk")))

(define-public crate-actix-telegram-derive-0.2.2 (c (n "actix-telegram-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0j02wgbllxm93wz24qv05dwsxx41157gsw88jfmq0g66gga12c5z")))

