(define-module (crates-io ac ti actix_tera_page) #:use-module (crates-io))

(define-public crate-actix_tera_page-0.1.0 (c (n "actix_tera_page") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "126f4fgqkkidl6ig2jyzws08qmvlnzhyds4srr392varqqp9hmiq")))

