(define-module (crates-io ac ti actix-web-prom) #:use-module (crates-io))

(define-public crate-actix-web-prom-0.1.0 (c (n "actix-web-prom") (v "0.1.0") (d (list (d (n "actix-service") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometheus") (r "^0.6") (d #t) (k 0)))) (h "12hv71m4jwws8a312ffbdgdz4h5j5krm0xkriz1y702dkvpyn1mi")))

(define-public crate-actix-web-prom-0.1.1 (c (n "actix-web-prom") (v "0.1.1") (d (list (d (n "actix-service") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometheus") (r "^0.6") (d #t) (k 0)))) (h "051m1cdmvgchav2fm1x6w0y7i1f0xnd1f0k4gndr863lzw316kgn")))

(define-public crate-actix-web-prom-0.1.2 (c (n "actix-web-prom") (v "0.1.2") (d (list (d (n "actix-service") (r "^0.4") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometheus") (r "^0.6") (d #t) (k 0)))) (h "0646sz9d5yxav979i2y8imlldghqjw02fwd34knnn5an35p37p69")))

(define-public crate-actix-web-prom-0.1.4 (c (n "actix-web-prom") (v "0.1.4") (d (list (d (n "actix-service") (r "^0.4") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (d #t) (k 0)))) (h "0zxiydripc7xwvs5vdzy521jyb3zaibv7plyaq6cdlnv3f1b4gwl")))

(define-public crate-actix-web-prom-0.2.0 (c (n "actix-web-prom") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (k 0)))) (h "1snhfr02w9gic3h59ihg52gr9gx1xxq15ys590kpzkz163pqdwmb")))

(define-public crate-actix-web-prom-0.2.1 (c (n "actix-web-prom") (v "0.2.1") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "prometheus") (r "^0.9.0") (k 0)))) (h "0gcx9chmicwqpx56r30q017b6v2xng3rg48n4551z3kchzmvzzcj") (y #t)))

(define-public crate-actix-web-prom-0.3.0 (c (n "actix-web-prom") (v "0.3.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "prometheus") (r "^0.9.0") (k 0)))) (h "0qcph1jvw1a16sxdaxwp41h6gsx3c9h4gmfbdd4kxi3hqrrrmjrr")))

(define-public crate-actix-web-prom-0.5.0 (c (n "actix-web-prom") (v "0.5.0") (d (list (d (n "actix-http") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.8") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "1idr8m0d0nisww0djmkpdkhj8kb0b3zf5slmmznv77k0sdkmdh86")))

(define-public crate-actix-web-prom-0.5.1 (c (n "actix-web-prom") (v "0.5.1") (d (list (d (n "actix-http") (r "^2.1") (d #t) (k 0)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.2") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.11") (k 0)))) (h "1kindjbbk5c4ckgrpl3fjd5v8zq2ws2w8fznx2d6ns1fsmylp6gc")))

(define-public crate-actix-web-prom-0.6.0-beta.2 (c (n "actix-web-prom") (v "0.6.0-beta.2") (d (list (d (n "actix-web") (r "^4.0.0-beta.10") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "1qgr6c2dd0n0g2bn2psh7xpf33glrf0990sriilxhmwyil4vd3ql")))

(define-public crate-actix-web-prom-0.6.0-beta.3 (c (n "actix-web-prom") (v "0.6.0-beta.3") (d (list (d (n "actix-web") (r "^4.0.0-beta.10") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "0zy59xfa3a7w8g3kibc64qllk2say9qc1bapp97niza3xw9ijcmx")))

(define-public crate-actix-web-prom-0.6.0-beta.5 (c (n "actix-web-prom") (v "0.6.0-beta.5") (d (list (d (n "actix-web") (r "=4.0.0-beta.21") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "1izhjsfdxc3bjiz0hx1wbv2xk23wz1n6aafh4whd9ylqx7qkfxkm")))

(define-public crate-actix-web-prom-0.6.0-beta.6 (c (n "actix-web-prom") (v "0.6.0-beta.6") (d (list (d (n "actix-web") (r "^4.0.0-beta.21") (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "11dyfsgbxjv3ppvy8983nqhmlv29p5nnkbn68ah8zdyp96y41750")))

(define-public crate-actix-web-prom-0.6.0-rc.1 (c (n "actix-web-prom") (v "0.6.0-rc.1") (d (list (d (n "actix-web") (r "^4.0.0-rc.2") (f (quote ("macros"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "1p4a7jq3vl8fnbzhkx9hqkd1xax2fp6m864vbwlrw0hdkvlq8mcv")))

(define-public crate-actix-web-prom-0.6.0-rc.2 (c (n "actix-web-prom") (v "0.6.0-rc.2") (d (list (d (n "actix-web") (r "^4.0.0-rc.3") (f (quote ("macros"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "1zl449l731g2jg3csw6b5lr76a1nwczqfxxanyfr92q7b462wlh2")))

(define-public crate-actix-web-prom-0.6.0 (c (n "actix-web-prom") (v "0.6.0") (d (list (d (n "actix-web") (r "^4.0") (f (quote ("macros"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)))) (h "08s9m1lwfmlgbbam92mlw5knf6nk72d9dsxcr6girl5541yi5wwx")))

(define-public crate-actix-web-prom-0.7.0 (c (n "actix-web-prom") (v "0.7.0") (d (list (d (n "actix-web") (r "^4.0") (f (quote ("macros"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "126lr3av2mvzbnmpk9wzwv83dm4y1k3h6qc7lvrvhdi8clm36gzj")))

(define-public crate-actix-web-prom-0.8.0 (c (n "actix-web-prom") (v "0.8.0") (d (list (d (n "actix-web") (r "^4.0") (f (quote ("macros"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "strfmt") (r "^0.2.4") (d #t) (k 0)))) (h "1b66mxvv18w13m7k66cfcs9aab3vvph3s4n75byakvz7sikkwx3n") (f (quote (("process" "prometheus/process"))))))

