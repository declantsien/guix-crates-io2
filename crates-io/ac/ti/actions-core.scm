(define-module (crates-io ac ti actions-core) #:use-module (crates-io))

(define-public crate-actions-core-0.0.1 (c (n "actions-core") (v "0.0.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "134k0d6bgd130v174dl95q2d9sl2scja5035sf71v5a819c0mfa9")))

(define-public crate-actions-core-0.0.2 (c (n "actions-core") (v "0.0.2") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "10g5g7bnvlhp9z01sdslfn2a8mpkraph29i2d5y6ivkvvb7wav6m")))

