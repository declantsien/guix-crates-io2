(define-module (crates-io ac ti actix-proc-macros) #:use-module (crates-io))

(define-public crate-actix-proc-macros-0.1.0 (c (n "actix-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ndxl91q0y3n65va9ggrp5n4y6cwwcwaymawvla42im1ci101lmy")))

(define-public crate-actix-proc-macros-0.2.1 (c (n "actix-proc-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbj4qqcn1h6rv3yq20rvl7xczmq5pap3rc5n0jlmrsd8xgl7whw")))

(define-public crate-actix-proc-macros-0.2.2 (c (n "actix-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "050fnwijgcfmddlm2va49hxk065rn3dkqgsm2pr0p1kivf1k6bvn") (y #t)))

(define-public crate-actix-proc-macros-0.2.3 (c (n "actix-proc-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nmam271jrf7906iml31ldzmir7zjzscbpxmpj1fqvq2d67jwk9w")))

(define-public crate-actix-proc-macros-0.3.0 (c (n "actix-proc-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlrzwwv369mrqw8shwi6k6gxx9pm44mijza820ixjrrm1whjgmj")))

(define-public crate-actix-proc-macros-0.4.0 (c (n "actix-proc-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "09dw0bphlvmdrj5pljcr6ai9sbqhxzirsfc9jzysv092qdznakkc")))

(define-public crate-actix-proc-macros-3.0.0 (c (n "actix-proc-macros") (v "3.0.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5s92mh5dv9bzx3m1mp7aspwd6br9wj426v8cbi8y94qw7s07a1")))

(define-public crate-actix-proc-macros-4.0.0-beta.3 (c (n "actix-proc-macros") (v "4.0.0-beta.3") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "02jhcszxmvjdgc5qfs7fp3nr6pl2fja0zzjpmpdrsvrx9x6zggwn")))

(define-public crate-actix-proc-macros-4.0.0-beta.3.1 (c (n "actix-proc-macros") (v "4.0.0-beta.3.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f6j30i7541dc7b7a5fkabb1m9z1smrhk9plpsav9jqvq53h7ldh")))

(define-public crate-actix-proc-macros-3.1.0 (c (n "actix-proc-macros") (v "3.1.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "17mrgl2pr7rwsaqb1jqln6avcmcmyxj9lqmwnlfg6lvp9c0a6nch")))

(define-public crate-actix-proc-macros-4.0.0-beta.9 (c (n "actix-proc-macros") (v "4.0.0-beta.9") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gm7sdq2qd81y6ch4lrdp9dq82ir2jrkkzf0awwlksfd16bhn2fz")))

(define-public crate-actix-proc-macros-4.0.0-beta.10 (c (n "actix-proc-macros") (v "4.0.0-beta.10") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ir8ysz5s6jy90c7gyfslglbzxirx8iwg91nx5fpy6pd8p07gfc8")))

(define-public crate-actix-proc-macros-4.0.0-beta.12 (c (n "actix-proc-macros") (v "4.0.0-beta.12") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yydc5ywr5vjs1bdsisz3gfr0jhirrwd4w4vy3qxx7g9gvlpp5wx")))

(define-public crate-actix-proc-macros-4.0.0-beta.11 (c (n "actix-proc-macros") (v "4.0.0-beta.11") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bqr1yxfyq0r9awxg3d79bxrw4n01rbxlsl42layhhdakhj1a0s0")))

(define-public crate-actix-proc-macros-4.0.0-beta.13 (c (n "actix-proc-macros") (v "4.0.0-beta.13") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "05cnh0vj6glmm3a407i29d22ynh0alhlisg1n6hifmqsqi3n9cng")))

(define-public crate-actix-proc-macros-4.0.0-beta.14 (c (n "actix-proc-macros") (v "4.0.0-beta.14") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z6r4g5x34hg7gibwb1qaww149qld5v0aia0qgg2kl9l8kpxhznn")))

(define-public crate-actix-proc-macros-4.0.0-beta.15 (c (n "actix-proc-macros") (v "4.0.0-beta.15") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "11jd4fvaz4hhk8rrgrlp6n8aga9y29yqpivipfsjr8r1c677g59n")))

(define-public crate-actix-proc-macros-4.0.0-rc.2 (c (n "actix-proc-macros") (v "4.0.0-rc.2") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1039pqm2zfqnp5c9mkn8xxlc836c7lcxi931s8q1q94adb8pm4s1")))

(define-public crate-actix-proc-macros-4.0.0 (c (n "actix-proc-macros") (v "4.0.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lpikkmdzhkm3aiwfiamn3y15idq3dnf14h17mck31r3zbv0ba5d") (y #t)))

(define-public crate-actix-proc-macros-4.0.1 (c (n "actix-proc-macros") (v "4.0.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4fiacagn8w5md18h3klfml7m9kzdsci97wy4qn00iifpq7ywna")))

