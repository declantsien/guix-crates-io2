(define-module (crates-io ac ti actix-taskqueue) #:use-module (crates-io))

(define-public crate-actix-taskqueue-0.1.0 (c (n "actix-taskqueue") (v "0.1.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "15vglrx792n4qpdqina36kwlg2ipwcq4fxicqxfqrm12pfsff9sw")))

