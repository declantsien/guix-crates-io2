(define-module (crates-io ac ti actix-dynamic-threadpool) #:use-module (crates-io))

(define-public crate-actix-dynamic-threadpool-0.1.0 (c (n "actix-dynamic-threadpool") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.9") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "0smn29nv0wq952xd60c5pq2afa7p449k60p2ypdab38qh25yjlmi")))

