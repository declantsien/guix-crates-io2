(define-module (crates-io ac ti activity-macros) #:use-module (crates-io))

(define-public crate-activity-macros-0.1.1 (c (n "activity-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.92") (d #t) (k 0)))) (h "0c3n8ca74wfh5b031iva3mg0sl2ygfd4l6v1rjp4wvwhc9c7xf02")))

(define-public crate-activity-macros-0.1.2 (c (n "activity-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.92") (d #t) (k 0)))) (h "10p8ws78vv69d67zg65r1whbjyn8s99jhasb4655dr3kws0l55n7")))

(define-public crate-activity-macros-0.1.3 (c (n "activity-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "wasm-bindgen-macro-support") (r "^0.2.92") (d #t) (k 0)))) (h "1siay8by6w4fbjmqpp7l7vsv1p1ydg2s7qlwbsb9cam1lkgfnpxb")))

