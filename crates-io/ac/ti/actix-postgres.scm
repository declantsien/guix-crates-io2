(define-module (crates-io ac ti actix-postgres) #:use-module (crates-io))

(define-public crate-actix-postgres-0.1.0 (c (n "actix-postgres") (v "0.1.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "bb8-postgres") (r "^0.4") (d #t) (k 0)))) (h "0d7n4f8jzkq0ssbda5ln8v1zbc9bacqphxzr7cfg8d66svq5ixys")))

(define-public crate-actix-postgres-0.1.1 (c (n "actix-postgres") (v "0.1.1") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "bb8-postgres") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ky6yw1kyai0md9x5pfpp9jaambww6k0ikm6rzgi2cgccj4jpmgp")))

(define-public crate-actix-postgres-0.2.0 (c (n "actix-postgres") (v "0.2.0") (d (list (d (n "actix") (r "~0.11.0") (d #t) (k 0)) (d (n "bb8-postgres") (r "~0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.24") (d #t) (k 0)))) (h "0mx5vqc0yn3djqb9hi50md4j7wxm0dddi5a3xgz1czdp485aphv0")))

(define-public crate-actix-postgres-0.3.0 (c (n "actix-postgres") (v "0.3.0") (d (list (d (n "actix") (r "~0.12.0") (d #t) (k 0)) (d (n "bb8-postgres") (r "~0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.25") (d #t) (k 0)))) (h "1rmh5vxk9wx2hq3ynilcmfh7mxl807va7iz3nrvviln7jlypfq0x")))

(define-public crate-actix-postgres-0.4.0 (c (n "actix-postgres") (v "0.4.0") (d (list (d (n "actix") (r "~0.13.0") (d #t) (k 0)) (d (n "bb8-postgres") (r "~0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "1zsqyqggc2ax0wianra2hss07y38npvc96b0810ny3wq7a93mcjh")))

