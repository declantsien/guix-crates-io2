(define-module (crates-io ac ti actix-web-middleware-redirect-scheme) #:use-module (crates-io))

(define-public crate-actix-web-middleware-redirect-scheme-1.0.0 (c (n "actix-web-middleware-redirect-scheme") (v "1.0.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "0lq0v4a84a5cmqc7l72q8g8lq4pmdjkrb6l55hkmrna70psywr4h")))

(define-public crate-actix-web-middleware-redirect-scheme-1.1.0 (c (n "actix-web-middleware-redirect-scheme") (v "1.1.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "14v9ap3a75hcwzcr6p8f4b1byfgms6yw45zzd59v2854fwar2vby")))

(define-public crate-actix-web-middleware-redirect-scheme-1.1.1 (c (n "actix-web-middleware-redirect-scheme") (v "1.1.1") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1xv9fl99iq6wdhsv2pnpvl23wq5wx0w4cawygda0dsnpxfd8r7v1")))

(define-public crate-actix-web-middleware-redirect-scheme-2.0.0 (c (n "actix-web-middleware-redirect-scheme") (v "2.0.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1d2vppc7mpzsimmhpbnf34qjcy2lsr8lkjazmnj2hbabhnbqmfsy")))

(define-public crate-actix-web-middleware-redirect-scheme-2.1.0 (c (n "actix-web-middleware-redirect-scheme") (v "2.1.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0shhka0rijaj2wvhmvhi7mxrh6l55318k7xw98bjwxqvzy9q4qc9")))

(define-public crate-actix-web-middleware-redirect-scheme-2.1.1 (c (n "actix-web-middleware-redirect-scheme") (v "2.1.1") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "100l96fq59z9qknyqwizh4b13vnr54xylh0ycy4ym4x8ak63ahqk")))

(define-public crate-actix-web-middleware-redirect-scheme-2.1.2 (c (n "actix-web-middleware-redirect-scheme") (v "2.1.2") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "12s5560v6x1pdx9fgjm6g7g0jinxrjhg6ihvxk27lwfywkzm6071")))

(define-public crate-actix-web-middleware-redirect-scheme-2.1.3 (c (n "actix-web-middleware-redirect-scheme") (v "2.1.3") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1i8a1kryg1970n1a843mk4wrqgj09n9p52br968xczslw6bnvwc5")))

(define-public crate-actix-web-middleware-redirect-scheme-2.2.0 (c (n "actix-web-middleware-redirect-scheme") (v "2.2.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0b8s7bfkqllxhjp4s5dk81xzbf0icqj6rb4hqg7kwva7ihbn8a39")))

(define-public crate-actix-web-middleware-redirect-scheme-2.3.0 (c (n "actix-web-middleware-redirect-scheme") (v "2.3.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "15q8iljfc43f1ngrkkk8pnr39i2vzh5c67w93lahg0jkrzjr1mx7")))

(define-public crate-actix-web-middleware-redirect-scheme-2.3.1 (c (n "actix-web-middleware-redirect-scheme") (v "2.3.1") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0h1pr30zvkvlnwlafnqj1a4rpf0bnqxpkf7zlhnz0vw195gfx80r")))

(define-public crate-actix-web-middleware-redirect-scheme-2.3.2 (c (n "actix-web-middleware-redirect-scheme") (v "2.3.2") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "11b27cimrj83ckxaq15ld002lh6sddbaf8f8s18xy8ybzkjd4a5y")))

(define-public crate-actix-web-middleware-redirect-scheme-2.3.3 (c (n "actix-web-middleware-redirect-scheme") (v "2.3.3") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0nkni761zajk5z1r66bpvv7lmpgzvr30p1x0fqrf4ppn4ylc8s69")))

(define-public crate-actix-web-middleware-redirect-scheme-3.0.0 (c (n "actix-web-middleware-redirect-scheme") (v "3.0.0") (d (list (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1gakypqqkmjw36ys4qgq51gqp96rx4l3r7mqnbrss6prf46s073i")))

