(define-module (crates-io ac ti actix-mock-helper) #:use-module (crates-io))

(define-public crate-actix-mock-helper-0.1.0 (c (n "actix-mock-helper") (v "0.1.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 2)))) (h "1f0sma8f7gzaa3g53q9pqfy80ifiw0210x47jx2sivdslzzkndfa")))

(define-public crate-actix-mock-helper-0.2.0 (c (n "actix-mock-helper") (v "0.2.0") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 2)))) (h "0hjcxkdflsw0b7ymfkf4qls8dwlniry5q0b6mvvzgj4cxw43dpr6")))

(define-public crate-actix-mock-helper-0.3.0 (c (n "actix-mock-helper") (v "0.3.0") (d (list (d (n "actix") (r "^0.11.0") (d #t) (k 0)) (d (n "actix-rt") (r "^2") (d #t) (k 2)))) (h "15jdh9qpr447g115ln7chpm2p9g63ylr8vws7rpkmk9n4iikyh28")))

