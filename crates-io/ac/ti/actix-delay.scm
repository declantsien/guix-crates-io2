(define-module (crates-io ac ti actix-delay) #:use-module (crates-io))

(define-public crate-actix-delay-0.1.0 (c (n "actix-delay") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.1") (d #t) (k 0)))) (h "1lyinnpkwk8qkilfjbgbd0fhbl1kx9k8dxb1a8dy8ky5ng4p6x3j")))

