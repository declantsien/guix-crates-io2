(define-module (crates-io ac ti actix-actor-expect) #:use-module (crates-io))

(define-public crate-actix-actor-expect-0.1.0-beta.1 (c (n "actix-actor-expect") (v "0.1.0-beta.1") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)))) (h "0ykchsc4bjjylpqngldp4bjwz43cg4jhw38057bmalfzg8cpmzfg")))

(define-public crate-actix-actor-expect-0.1.0-beta.2 (c (n "actix-actor-expect") (v "0.1.0-beta.2") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)))) (h "1apmyb2bgc4xxxsh0anm2rjl297ysx6rcq8yhaqlhx37j1ia45qd")))

(define-public crate-actix-actor-expect-0.1.0-beta.3 (c (n "actix-actor-expect") (v "0.1.0-beta.3") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)))) (h "00g2sjx39wa4zm29sv3gcg7kdlkyd3qqlln7jbclc0ib6x6ghq15")))

(define-public crate-actix-actor-expect-0.1.0-beta.4 (c (n "actix-actor-expect") (v "0.1.0-beta.4") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)))) (h "024gs4db92xydglfzn20z3nv59ic9f8knjlkxjip3vilxpdvf82b")))

(define-public crate-actix-actor-expect-0.1.0 (c (n "actix-actor-expect") (v "0.1.0") (d (list (d (n "actix") (r "^0.12.0") (d #t) (k 0)))) (h "1xsr79k6yvpbja72xifyg85gx0xcgnc79b5z87wmaygv86bdfshh")))

