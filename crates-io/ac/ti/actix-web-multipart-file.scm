(define-module (crates-io ac ti actix-web-multipart-file) #:use-module (crates-io))

(define-public crate-actix-web-multipart-file-0.1.0 (c (n "actix-web-multipart-file") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.7.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "1har5v1hrhj1cxd7q8ikz8x94sdqbaap37h4l0mjyf52barmrivp")))

(define-public crate-actix-web-multipart-file-0.1.1 (c (n "actix-web-multipart-file") (v "0.1.1") (d (list (d (n "actix-web") (r "^0.7.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "1dh34fphja9azswl1w8ld3vjr9ps90jgn2zqraqlri7xnd6bwsib")))

