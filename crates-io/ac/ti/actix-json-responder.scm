(define-module (crates-io ac ti actix-json-responder) #:use-module (crates-io))

(define-public crate-actix-json-responder-0.1.0 (c (n "actix-json-responder") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "1m42wb9g18irwcn6wcslnhd8xli9rl17gvir196wbb849pjli0b9")))

(define-public crate-actix-json-responder-0.2.0-beta.1 (c (n "actix-json-responder") (v "0.2.0-beta.1") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.19") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "145if1d8linzivzwcvbc3qg8nz4lhfjbshq691jjc9v2y8kjnjq0")))

(define-public crate-actix-json-responder-0.2.0 (c (n "actix-json-responder") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "13rmx9i058yiadprk96yd3qfjva89ahchvlbhi1nj6ky3c542lyx")))

(define-public crate-actix-json-responder-0.2.1 (c (n "actix-json-responder") (v "0.2.1") (d (list (d (n "actix-rt") (r "^2.7.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "095k021x547611nb7pryb47v1hlk3plki4s264wcmffw5mqval3c")))

