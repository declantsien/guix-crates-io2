(define-module (crates-io ac ti actix-diesel-actor) #:use-module (crates-io))

(define-public crate-actix-diesel-actor-0.1.0 (c (n "actix-diesel-actor") (v "0.1.0") (d (list (d (n "actix") (r "^0.7.9") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0d7w5ns94k52xicclhyxiyya9j02vf4h0hxyr4p0dgvysfwrybzp")))

(define-public crate-actix-diesel-actor-0.1.1 (c (n "actix-diesel-actor") (v "0.1.1") (d (list (d (n "actix") (r "^0.8") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0xg6rv4az8kygj8jgspzmbsbqvfncchyskzzxw95150aaz19p6hh")))

(define-public crate-actix-diesel-actor-0.1.2 (c (n "actix-diesel-actor") (v "0.1.2") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0qq39pvk2w4a8nv6wmj6q2lkfyg302nx4gmr6m85wknl7q1gix86")))

