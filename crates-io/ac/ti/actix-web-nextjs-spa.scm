(define-module (crates-io ac ti actix-web-nextjs-spa) #:use-module (crates-io))

(define-public crate-actix-web-nextjs-spa-0.1.0 (c (n "actix-web-nextjs-spa") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-service") (r "^2.0.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0") (f (quote ("macros"))) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "path-tree") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0r2nd6vb98snfcyksmz3djlnnz85a4x38vdbjjjgi05nrgcg9k9b")))

(define-public crate-actix-web-nextjs-spa-0.1.1 (c (n "actix-web-nextjs-spa") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-service") (r "^2.0.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0") (f (quote ("macros"))) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "path-tree") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "17z84zdffwa9pfg8b0xi4fi95p4anjsg2raikbvb78b9y7g2bxvx")))

