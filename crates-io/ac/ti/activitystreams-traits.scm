(define-module (crates-io ac ti activitystreams-traits) #:use-module (crates-io))

(define-public crate-activitystreams-traits-0.1.0 (c (n "activitystreams-traits") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hl55vz4mz4n6hdf11wn6cmpf1h5nkcabrs2wb5b0177d0qz03k7")))

(define-public crate-activitystreams-traits-0.2.0 (c (n "activitystreams-traits") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05x55hx4y5n07rm8h4k8l7i4q3bfk9kmy6zky7pl03r4inas7hm0")))

(define-public crate-activitystreams-traits-0.4.0-alpha.0 (c (n "activitystreams-traits") (v "0.4.0-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "07zigrlf07h150scdr384fj87bdy23hjwf7w9xq3gbcngx6s1s01")))

