(define-module (crates-io ac ti actix-handler-macro) #:use-module (crates-io))

(define-public crate-actix-handler-macro-0.1.0 (c (n "actix-handler-macro") (v "0.1.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 2)) (d (n "actix_derive") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p91vixh382caj1a1cm1xsv0a1icknb2cckyp2dq04272aa4am8k")))

(define-public crate-actix-handler-macro-0.2.0 (c (n "actix-handler-macro") (v "0.2.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 2)) (d (n "actix_derive") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h2hxki0avvprh2b38hxpb929g6kglnq7y74js14vd867xwqqxpi")))

