(define-module (crates-io ac ti actividadsemana20) #:use-module (crates-io))

(define-public crate-actividadSemana20-0.1.0 (c (n "actividadSemana20") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dummy-queue") (r "^1.1.0") (d #t) (k 0)))) (h "0asba163mb6gxkl0aglpl8lq4iiaz1cksqbjzcg650vd6yim7agr") (y #t)))

(define-public crate-actividadSemana20-0.1.1 (c (n "actividadSemana20") (v "0.1.1") (h "0ll1chqbi84hd58nhcjbm28wxy89ajb4q2kpy57d0f589vx21413") (y #t)))

(define-public crate-actividadSemana20-0.2.0 (c (n "actividadSemana20") (v "0.2.0") (h "0qr6xps0a31pxi38g529wnxmvirrglmg25iamf7g3gwdxqzgig5w")))

