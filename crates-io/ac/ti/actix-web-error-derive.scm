(define-module (crates-io ac ti actix-web-error-derive) #:use-module (crates-io))

(define-public crate-actix-web-error-derive-0.1.0 (c (n "actix-web-error-derive") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g3vbq5h9ak6rk226nnpsf7rbh1dl7rjrc6r9ymbx0gf7n4qjdiw")))

(define-public crate-actix-web-error-derive-0.2.0 (c (n "actix-web-error-derive") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hrcya59plc1l3pflspl6vz39qwhcrv215h0fk92l31gc2f7iqk1")))

