(define-module (crates-io ac ti action_maps) #:use-module (crates-io))

(define-public crate-action_maps-0.1.0 (c (n "action_maps") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12.0") (d #t) (k 0)))) (h "1z682ydxqgsgmjb98fnvqwv5a164vfbpw7nibf6bryg9l8dspfqa")))

(define-public crate-action_maps-0.2.0 (c (n "action_maps") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12.0") (d #t) (k 0)))) (h "1gwh7b95k7aj8xdc1263zzvn8kjkl3d7wylhls7iicwiyv03k942")))

(define-public crate-action_maps-0.2.1 (c (n "action_maps") (v "0.2.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12.0") (d #t) (k 0)))) (h "1vm9gxdip5jwanwnrhifwqcvchjssvys8mb7g1758ssgbin1m41r")))

(define-public crate-action_maps-0.3.0 (c (n "action_maps") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12.0") (d #t) (k 0)))) (h "086adx4iggwyzfcjnf3s1a0bd324ibrhh27fywnsn4c73bwzixgl")))

(define-public crate-action_maps-0.3.1 (c (n "action_maps") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "bevy_app") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_input") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_log") (r "^0.12.1") (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "084r8h19ryja1vrvzkgcz4vx1yl83hcnlwcg0f9p15fpbwrbc0vc")))

