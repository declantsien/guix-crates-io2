(define-module (crates-io ac ti actions-github) #:use-module (crates-io))

(define-public crate-actions-github-0.1.0 (c (n "actions-github") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0ym6njm1nl5niflrpc14di3jk1kzb4vlllnwca87wy89pbhyw8ng") (y #t)))

(define-public crate-actions-github-0.0.2 (c (n "actions-github") (v "0.0.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1xi5rxa5wyxs0ppkxzdib4k4w4qx4is6xfihpcl1shk8w5ls1ksl")))

(define-public crate-actions-github-0.0.3 (c (n "actions-github") (v "0.0.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1w8hbfc7ligdzkrrz57fpfrzfsb3k0wksz7zgr687pyrk0n28l93")))

(define-public crate-actions-github-0.0.4 (c (n "actions-github") (v "0.0.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "12wmf9c6ya5pqvpqb6bcazbdb967q7f7l7bp4nd2m943c8mpzyzm")))

(define-public crate-actions-github-0.0.5 (c (n "actions-github") (v "0.0.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ficwj831wss2af47k1smq9hpnhj3gkai89nikghfmr3dj38irvf")))

(define-public crate-actions-github-0.2.0 (c (n "actions-github") (v "0.2.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hs7bxdlwkf0w242rk8wlzvzfw1mc7mb9jhglv703x7288sdmnz9")))

