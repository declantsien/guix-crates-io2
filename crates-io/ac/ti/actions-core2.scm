(define-module (crates-io ac ti actions-core2) #:use-module (crates-io))

(define-public crate-actions-core2-0.1.0 (c (n "actions-core2") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jc7dyrdhdk2csxh3jzxghpc1dpwnbghvs8c8i2698n1mvz0k77r")))

