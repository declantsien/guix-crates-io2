(define-module (crates-io ac ti actionidmap-updater) #:use-module (crates-io))

(define-public crate-actionidmap-updater-0.1.0 (c (n "actionidmap-updater") (v "0.1.0") (d (list (d (n "nanoserde") (r "^0.1.32") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1c2d5x6yq34szp4496m1s6c7dvpy9a0x57bjrr4s1x61icnixcdr")))

(define-public crate-actionidmap-updater-0.2.0 (c (n "actionidmap-updater") (v "0.2.0") (d (list (d (n "nanoserde") (r "^0.1.32") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1f5fnkbsvmwrzlsgm8f3nczfbql8bq69v5dsdwm4g1a3h4q73ipb")))

