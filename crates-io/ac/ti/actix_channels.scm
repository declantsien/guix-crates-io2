(define-module (crates-io ac ti actix_channels) #:use-module (crates-io))

(define-public crate-actix_channels-0.1.1 (c (n "actix_channels") (v "0.1.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "actix-web-actors") (r "^3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0x5zsgvgpv6i03jpdkhd2sszgp7jiqvjwy2j6gp9h1v0a84hl7cx")))

(define-public crate-actix_channels-0.4.0-beta.1 (c (n "actix_channels") (v "0.4.0-beta.1") (d (list (d (n "actix") (r "^0.12") (d #t) (k 0)) (d (n "actix-rt") (r "^2.5") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4.0.0-beta") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jll63vgah6lcm1mflifif5ccd0xbjv0nr3w91n40w5lv69axvbz")))

(define-public crate-actix_channels-0.4.1 (c (n "actix_channels") (v "0.4.1") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "actix-rt") (r "^2.5") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4.0.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11wg89wjhi8hl4vwjcg5iirsqxwr4fwlfssch7mrx5blhic4mi3w")))

