(define-module (crates-io ac ti activesupport) #:use-module (crates-io))

(define-public crate-activesupport-0.0.1-alpha.1 (c (n "activesupport") (v "0.0.1-alpha.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0m6bksj5nmjhg4n0dxgg9ngbsi1hvqlbr2cj8yry522pxybbk2cl") (y #t) (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.2 (c (n "activesupport") (v "0.0.1-alpha.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1rf1kr66ypnw90g5lnwvjpahkdv48jjby8bnsgz8x53afmhdd0jq") (y #t) (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.3 (c (n "activesupport") (v "0.0.1-alpha.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1khmvzbv3zh69hm96bs92ix9rw0jr8as4fwfxvnbxllfagfpl1gj") (y #t) (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.4 (c (n "activesupport") (v "0.0.1-alpha.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0h6qnf2yjcy6g24aq3ddfx1svqyrmp2ri785j74fi317w95mzxlp") (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.5 (c (n "activesupport") (v "0.0.1-alpha.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1vdgss22zy893ffa1q04fnbs9vig3rrzsmm1wj335fa80dic7rwl") (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.6 (c (n "activesupport") (v "0.0.1-alpha.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0giz8s7zzx7nlyq44g7glcnrhk5n01awdar6ii2z39ppfwd6n77n") (r "1.70.0")))

(define-public crate-activesupport-0.0.1-alpha.7 (c (n "activesupport") (v "0.0.1-alpha.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0zns4z5kp845f91yrbqpdkq6nfbfd11iwkcmc4a51nlz3qww2p51") (r "1.70.0")))

