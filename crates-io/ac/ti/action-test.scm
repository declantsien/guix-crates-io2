(define-module (crates-io ac ti action-test) #:use-module (crates-io))

(define-public crate-action-test-0.1.0 (c (n "action-test") (v "0.1.0") (h "1axvsh3j3w7gb31jjsidfjciclh23h31vwb2fhfl6bwsa9bg5mhg")))

(define-public crate-action-test-0.1.3 (c (n "action-test") (v "0.1.3") (h "16qmc3gaarifyrpsaf9mlqj25qn8xnp94620d9adxbrgrp4ncd8d")))

(define-public crate-action-test-0.1.4 (c (n "action-test") (v "0.1.4") (h "0q3gf2w09livzl95mzlx23zh0vnfgkzbph2i52xhi6zny12x90wv")))

(define-public crate-action-test-0.1.5 (c (n "action-test") (v "0.1.5") (h "18r9kmdqjxgva8ca56xq9b5pwdagj6kamgd2s1xlq2z5rc82iqfc")))

(define-public crate-action-test-0.3.0 (c (n "action-test") (v "0.3.0") (h "1jvdjz3awya4yl6zqwj5h26rr8cfjwsr2rbamvqci19bisva1avl")))

