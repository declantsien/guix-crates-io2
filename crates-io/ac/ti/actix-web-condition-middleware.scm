(define-module (crates-io ac ti actix-web-condition-middleware) #:use-module (crates-io))

(define-public crate-actix-web-condition-middleware-0.1.0 (c (n "actix-web-condition-middleware") (v "0.1.0") (d (list (d (n "actix-service") (r "^0.4") (d #t) (k 0)) (d (n "actix-web") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "06icbp34wgxxr4dl0d6y82hdzdhpgddalhy2c7xi29qvyfd78jih")))

