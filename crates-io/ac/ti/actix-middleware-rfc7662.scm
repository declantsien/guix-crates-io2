(define-module (crates-io ac ti actix-middleware-rfc7662) #:use-module (crates-io))

(define-public crate-actix-middleware-rfc7662-0.1.0 (c (n "actix-middleware-rfc7662") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)))) (h "04hfsbnw8m8hkjf4cvjdw4yk5q4jj1m6sg9gb08fgj84ml1zqrh7")))

(define-public crate-actix-middleware-rfc7662-0.2.0 (c (n "actix-middleware-rfc7662") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c4lc3d6nbwsawbgwfqyh8pmspm6h1zvw4dbf9pxxysfc8bw7jcp") (f (quote (("indieauth" "serde"))))))

