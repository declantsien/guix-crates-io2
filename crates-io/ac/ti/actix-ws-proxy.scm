(define-module (crates-io ac ti actix-ws-proxy) #:use-module (crates-io))

(define-public crate-actix-ws-proxy-0.1.1 (c (n "actix-ws-proxy") (v "0.1.1") (d (list (d (n "actix") (r "^0.13.1") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4") (d #t) (k 0)) (d (n "awc") (r "^3.2.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)))) (h "0ik85jjp6nl655699paavd0pybw51p5xzfpn1g2zjgqj0y6j94mq")))

(define-public crate-actix-ws-proxy-0.1.2 (c (n "actix-ws-proxy") (v "0.1.2") (d (list (d (n "actix") (r "^0.13.1") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-actors") (r "^4") (d #t) (k 0)) (d (n "awc") (r "^3.2.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 2)))) (h "1hs22ah513pbcgsnnvz1mgm41qmkxdxslryxqdf79g41y8v1n7rq")))

