(define-module (crates-io ac ti actix-ogn) #:use-module (crates-io))

(define-public crate-actix-ogn-0.1.0 (c (n "actix-ogn") (v "0.1.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "aprs-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "18d8jyik6hywsp6nh8d5lm89935f748c8a1xz9qxij9h5c3i0212")))

(define-public crate-actix-ogn-0.2.0 (c (n "actix-ogn") (v "0.2.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "backoff") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1") (d #t) (k 0)))) (h "1bcgmzy1r4m9jr4lncyfw7wbnkwhjglsrgvxi0fmih5ax5mmh5xm")))

(define-public crate-actix-ogn-0.3.0 (c (n "actix-ogn") (v "0.3.0") (d (list (d (n "actix") (r "^0.8") (d #t) (k 0)) (d (n "backoff") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1") (d #t) (k 0)))) (h "05xgv6cgvcw8laihqi80qv5av5030y760afj6nk24937h6d69bqm")))

(define-public crate-actix-ogn-0.4.0 (c (n "actix-ogn") (v "0.4.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "backoff") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "time" "signal" "io-util"))) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "07a8kxw573w8nzf21xi41q81gc8xgis67syh57mp2vp8xjnvkcv0")))

