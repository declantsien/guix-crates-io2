(define-module (crates-io ac ti actix-socks) #:use-module (crates-io))

(define-public crate-actix-socks-0.1.0 (c (n "actix-socks") (v "0.1.0") (d (list (d (n "actix-connect") (r "^1.0.2") (d #t) (k 0)) (d (n "actix-http") (r "^1.0.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio-socks") (r "^0.2.2") (d #t) (k 0)))) (h "0jvgwis0c9zlbasgvz8lyr7wpm5z714z2i2igh4yjqxg8kl88w0y")))

