(define-module (crates-io ac ti actix-plus-static-files) #:use-module (crates-io))

(define-public crate-actix-plus-static-files-0.1.0 (c (n "actix-plus-static-files") (v "0.1.0") (d (list (d (n "actix-service") (r "^1.0.6") (k 0)) (d (n "actix-web") (r "^3.0") (k 0)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)))) (h "0zwncdqjk5d3zg703wmvig9imay813nsbvrfv741hpw5s2ds2nmm")))

