(define-module (crates-io ac ti actix-web-middleware-requestid) #:use-module (crates-io))

(define-public crate-actix-web-middleware-requestid-1.0.0 (c (n "actix-web-middleware-requestid") (v "1.0.0") (d (list (d (n "actix-service") (r "^0.4.2") (d #t) (k 0)) (d (n "actix-web") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0apcwlym16h29ncn1fmbf59nn28m1cpy9bhik6acamy4q6ab4jaw")))

(define-public crate-actix-web-middleware-requestid-2.0.0 (c (n "actix-web-middleware-requestid") (v "2.0.0") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "17j0kx9lc2hnxzfkhbgd078whwkmmjgl86f159rrhvzc22fqmkqv")))

(define-public crate-actix-web-middleware-requestid-3.0.0 (c (n "actix-web-middleware-requestid") (v "3.0.0") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1i38f6s6xhqcji3hmr71rm83js07ij2mfzxqg9lnfi4d5nhy4cnn")))

(define-public crate-actix-web-middleware-requestid-3.0.1 (c (n "actix-web-middleware-requestid") (v "3.0.1") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "rand") (r "^0.7") (f (quote ("std"))) (k 0)))) (h "00hibrg9qibra19vrb60q4pjdan14bbz3wj5wg8abiny47cvmnjg")))

