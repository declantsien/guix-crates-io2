(define-module (crates-io ac ti actix-web-async-compat) #:use-module (crates-io))

(define-public crate-actix-web-async-compat-0.1.0 (c (n "actix-web-async-compat") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (f (quote ("compat"))) (d #t) (k 0)))) (h "1m453v8igldc0w32ks1fsbd41v2s1ai27iwfllkw5skqqkbppvnk")))

