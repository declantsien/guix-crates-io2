(define-module (crates-io ac ti actify-macros) #:use-module (crates-io))

(define-public crate-actify-macros-0.1.0 (c (n "actify-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q69zx7yhj2c2npna0nggfz53v49jwjp02yn9zck5yksr436idva")))

(define-public crate-actify-macros-0.1.1 (c (n "actify-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0alpj3mnxzsvpggx547c3fkkxk2q0al0m5dbjf4xml2hjf8860la")))

(define-public crate-actify-macros-0.2.0 (c (n "actify-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k7v6xiiry2hq1fbwqh71fnfi7ls01d9yigiqcwsrwzwdid8fj7x")))

(define-public crate-actify-macros-0.2.1 (c (n "actify-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10hamydzyrhcpi4y8a3nxbhbhcsbh8s5nbsqh7nv9pibs69r9z95")))

(define-public crate-actify-macros-0.3.0 (c (n "actify-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06rbp4p4a18m705pyrd1sv29jp8aa2fklpbw3r52f1dv32wm9hfs")))

(define-public crate-actify-macros-0.3.1 (c (n "actify-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jkpsrybd73lq355af03v80yqizi66hfv88mvb1p3w22qk433zdp")))

