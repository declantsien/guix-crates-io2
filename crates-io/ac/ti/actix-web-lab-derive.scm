(define-module (crates-io ac ti actix-web-lab-derive) #:use-module (crates-io))

(define-public crate-actix-web-lab-derive-0.15.0 (c (n "actix-web-lab-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1kakl07q6mwvfc0pdx2aclkkgcfm5rnhi6bikhisj9fqxmvlgsiq") (r "1.59")))

(define-public crate-actix-web-lab-derive-0.16.0 (c (n "actix-web-lab-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1ljs6f1n5spr7q9yh8316bxikrall51zf8ndvr0ly6mkvpciajm4") (r "1.59")))

(define-public crate-actix-web-lab-derive-0.18.0 (c (n "actix-web-lab-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1rpsg96qvifjdkhbhcdfnyq85lsd6bmfz9h0rly4gb14g76rlhg4") (r "1.64")))

(define-public crate-actix-web-lab-derive-0.19.0 (c (n "actix-web-lab-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1lmwd59gh9glk8zxn855c87g6kp8rbkjh7viwnqkjjbrqy24aa8n") (r "1.64")))

(define-public crate-actix-web-lab-derive-0.20.0 (c (n "actix-web-lab-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0s79npywpc69a1rziskpsxwmppg8a5abp7gjj6v7cjnyr23v584s") (r "1.70")))

