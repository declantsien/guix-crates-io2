(define-module (crates-io ac ti actix-proxy) #:use-module (crates-io))

(define-public crate-actix-proxy-0.1.0 (c (n "actix-proxy") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (f (quote ("v110"))) (d #t) (k 0)))) (h "1fgknk351rs1jjhrsdzb2309nz2qz49iljg9hi44r422aya4kpnj")))

(define-public crate-actix-proxy-0.2.0 (c (n "actix-proxy") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.3.0") (d #t) (k 0)) (d (n "awc") (r "^3.1.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)))) (h "0g39ngi626g255wwcdlhaskjvr5acagvgzrdc1fq6ydxb09bmc9y")))

