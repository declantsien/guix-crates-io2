(define-module (crates-io ac ti actix-jwt-auth-middleware-derive) #:use-module (crates-io))

(define-public crate-actix-jwt-auth-middleware-derive-0.1.0 (c (n "actix-jwt-auth-middleware-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06hm69fikswqjk1304bkxnajcc6ygr4w2y9kvv3dbp37d336722m")))

(define-public crate-actix-jwt-auth-middleware-derive-0.1.1 (c (n "actix-jwt-auth-middleware-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mds3s0fz1rwbqf8l1312rx8c9mwmvwdi36k8bymrwbavg17sav9")))

(define-public crate-actix-jwt-auth-middleware-derive-0.2.0 (c (n "actix-jwt-auth-middleware-derive") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (k 2)) (d (n "derive-elves") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jxs13m5qkycp7qn0z07gyy4ikk1x7mn5mknlly73rjnjy300xkb")))

