(define-module (crates-io ac ti actix-web-middleware-slack) #:use-module (crates-io))

(define-public crate-actix-web-middleware-slack-0.1.0 (c (n "actix-web-middleware-slack") (v "0.1.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0rcz2k39kczwgqlb5kk7k51hj2kh62bqky1w64hq4q90xk6n9gmr")))

