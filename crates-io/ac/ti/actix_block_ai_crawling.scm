(define-module (crates-io ac ti actix_block_ai_crawling) #:use-module (crates-io))

(define-public crate-actix_block_ai_crawling-0.1.0 (c (n "actix_block_ai_crawling") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)))) (h "0jg18bjwpjrgqls5hjhs50b9kzh8yyxla6fc0sagj499rmbka4a9")))

(define-public crate-actix_block_ai_crawling-0.2.0 (c (n "actix_block_ai_crawling") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)))) (h "1amhz8qcamw9bhv18p5irj96pcgp1pr4qb0nsvqnkyxrnyy24jw8")))

(define-public crate-actix_block_ai_crawling-0.2.1 (c (n "actix_block_ai_crawling") (v "0.2.1") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)))) (h "0yay715jnddjhs7yj97g5xp0b4z2bnmsr9vwfznrbzcx88jjv5f8")))

(define-public crate-actix_block_ai_crawling-0.2.3 (c (n "actix_block_ai_crawling") (v "0.2.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)))) (h "0dld6xpkg7wg9w7j5w2xza1dvs9mm4gv5412xysy0cjhww3lmzhb")))

(define-public crate-actix_block_ai_crawling-0.2.4 (c (n "actix_block_ai_crawling") (v "0.2.4") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)))) (h "0xfvsfkk9dygb4wb2qbfsg1bdw02ic3l3ly09r0m1q1kxl3s2dw8")))

(define-public crate-actix_block_ai_crawling-0.2.5 (c (n "actix_block_ai_crawling") (v "0.2.5") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "iprange") (r "^0.6.7") (d #t) (k 0)))) (h "1yy4g2lnrchjsxppsd7brb8rdpcb3wicszd5zysb7nd10h8hhd6d")))

(define-public crate-actix_block_ai_crawling-0.2.6 (c (n "actix_block_ai_crawling") (v "0.2.6") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "iprange") (r "^0.6.7") (d #t) (k 0)))) (h "1sl512qc5z4p7gds1ih6kvgxwjz1ydcwh23zr6zzvbifffyfqlng")))

(define-public crate-actix_block_ai_crawling-0.2.7 (c (n "actix_block_ai_crawling") (v "0.2.7") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "iprange") (r "^0.6.7") (d #t) (k 0)))) (h "0b341yn4w1wswfvgab57r8d8bnz1kfzjqav5sq67siff141rqbvm")))

(define-public crate-actix_block_ai_crawling-0.2.8 (c (n "actix_block_ai_crawling") (v "0.2.8") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "iprange") (r "^0.6.7") (d #t) (k 0)))) (h "11hpa5f440s7dpgykz8w9lhskjvkxjmlgsdclj4f1sw9ls71dljh")))

