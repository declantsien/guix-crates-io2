(define-module (crates-io ac ti active_tx) #:use-module (crates-io))

(define-public crate-active_tx-0.1.0 (c (n "active_tx") (v "0.1.0") (d (list (d (n "activeledger") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1kvcq0qhfpqp2dj3dysxcyf4jbdx65n9z50ar4cd8z4bgd8qnwyq")))

(define-public crate-active_tx-0.1.1 (c (n "active_tx") (v "0.1.1") (d (list (d (n "activeledger") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1kpcp994rg4l35qh2xw716ba08ay6lrcw4ry8ad2fs06ix8fg0rd")))

