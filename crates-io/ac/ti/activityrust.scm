(define-module (crates-io ac ti activityrust) #:use-module (crates-io))

(define-public crate-activityrust-0.1.0 (c (n "activityrust") (v "0.1.0") (d (list (d (n "ambassador") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1brkpx26j4zrm1dki64426infkhsw3cxmrfrm50mf3nlb163dj5x")))

