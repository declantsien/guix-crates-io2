(define-module (crates-io ac ti actix-broker) #:use-module (crates-io))

(define-public crate-actix-broker-0.1.0 (c (n "actix-broker") (v "0.1.0") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)))) (h "13n5cp5w6bqz1jh47qvkganr1285i11awg3v4crh7v2h6p80i5p6")))

(define-public crate-actix-broker-0.1.1 (c (n "actix-broker") (v "0.1.1") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1mpwz3sn6zpjg2kha0478jm1p36bqxsgbmirr8k5xhjrxfxfy6i5")))

(define-public crate-actix-broker-0.1.2 (c (n "actix-broker") (v "0.1.2") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)))) (h "01ymgcrz03196fi0f66c677pndqdi87m2q13spn214gzab6ry6gv")))

(define-public crate-actix-broker-0.1.3 (c (n "actix-broker") (v "0.1.3") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "actix-web") (r "^0.7.7") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0c10a8as2br27d417idrykjdqi73f20j6r67fhi92f61lpbjrwy6")))

(define-public crate-actix-broker-0.1.4 (c (n "actix-broker") (v "0.1.4") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "actix-web") (r "^0.7.7") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0j19b6n8sp12npf0wkfw859whcnxc59aiaabhbwjsc59zg3bs8sw")))

(define-public crate-actix-broker-0.1.5 (c (n "actix-broker") (v "0.1.5") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "actix-web") (r "^0.7.7") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "12zashnycfrfij463kgpph6dckjw456fjm3a10s7qx4iipnr7dj3")))

(define-public crate-actix-broker-0.1.6 (c (n "actix-broker") (v "0.1.6") (d (list (d (n "actix") (r "^0.7.4") (d #t) (k 0)) (d (n "actix-web") (r "^0.7.7") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0yqq9g611g2sxg0w11whh6gj1vh902sis9zw4dgpac6nf5mm2dbf")))

(define-public crate-actix-broker-0.1.7 (c (n "actix-broker") (v "0.1.7") (d (list (d (n "actix") (r "^0.7.9") (d #t) (k 0)) (d (n "actix-web") (r "^0.7.7") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "1bdn707sj14dy1pz2d4pmdbwidyzkx5v6k17hik5q7fzrlnsyn24")))

(define-public crate-actix-broker-0.1.8 (c (n "actix-broker") (v "0.1.8") (d (list (d (n "actix") (r "^0.8.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "1cq7afj841gv65x3v02pgi16cw66hr4nqxr5dfay2f2glpm0vf1g")))

(define-public crate-actix-broker-0.2.0 (c (n "actix-broker") (v "0.2.0") (d (list (d (n "actix") (r "^0.8.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0x6w6m69k85p8rhmr3g35dalhw5dzhmznvisjpz3i6413skq2qxz")))

(define-public crate-actix-broker-0.2.1 (c (n "actix-broker") (v "0.2.1") (d (list (d (n "actix") (r "^0.8.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "1ijxszsprn0bm2wsp7zvmsf5sizwlzgw12s9zlpviagwpfwcjc22")))

(define-public crate-actix-broker-0.3.0 (c (n "actix-broker") (v "0.3.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "07bw4ds8s8iaj0fdzgdvdw9n694n73f6c49gy4fxlxwrbyxiqfii")))

(define-public crate-actix-broker-0.3.1 (c (n "actix-broker") (v "0.3.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0czcpsjsnawck8rzim98bxmpyivxiwrzl0g8djrs3adm0jm5w1sy")))

(define-public crate-actix-broker-0.4.0-beta.1 (c (n "actix-broker") (v "0.4.0-beta.1") (d (list (d (n "actix") (r "^0.11.0-beta.3") (d #t) (k 0)) (d (n "ahash") (r "^0.7") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xydm7iglkm6ms1g25kpxqri6fvg6bap60admn6g0dvirp51inp4")))

(define-public crate-actix-broker-0.4.0 (c (n "actix-broker") (v "0.4.0") (d (list (d (n "actix") (r "^0.11.0") (d #t) (k 0)) (d (n "ahash") (r "^0.7") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10pavnswzd5wnzpdh58xp9rjivh1c8nrswdyyca4lpi20f7hj5vg")))

(define-public crate-actix-broker-0.4.1 (c (n "actix-broker") (v "0.4.1") (d (list (d (n "actix") (r ">=0.11.0, <0.13") (d #t) (k 0)) (d (n "ahash") (r "^0.7") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c663xnblvbyv8jv5xya7nc95yrz89k5bca5shk30sxfgiza6c60")))

(define-public crate-actix-broker-0.4.2 (c (n "actix-broker") (v "0.4.2") (d (list (d (n "actix") (r ">=0.11.0, <0.14") (d #t) (k 0)) (d (n "ahash") (r "^0.7") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "068wh4dr3ivl5kcvyy9ykj6lyjgs8mix330z5v93sjp6jl2255pk")))

(define-public crate-actix-broker-0.4.3 (c (n "actix-broker") (v "0.4.3") (d (list (d (n "actix") (r ">=0.11.0, <0.14") (d #t) (k 0)) (d (n "ahash") (r "^0.7") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sf5kpryzfmryz8q7779s79wrwdzms06q2yakqmdf0ngs32bb2g0")))

