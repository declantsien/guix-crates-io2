(define-module (crates-io ac ti activation_functions) #:use-module (crates-io))

(define-public crate-activation_functions-0.1.0 (c (n "activation_functions") (v "0.1.0") (h "0m1zzfd8zv4c8kncbbp7naqf3gkgk96g0cm0h5djcxk9iqd1s99j")))

(define-public crate-activation_functions-0.1.1 (c (n "activation_functions") (v "0.1.1") (h "0slcg39mb40inxyvj6anyqraml1s17w6gk0wcds700bxgval3flk")))

