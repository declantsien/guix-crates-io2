(define-module (crates-io ac ti activity-build) #:use-module (crates-io))

(define-public crate-activity-build-0.1.2 (c (n "activity-build") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "01fv4ambyp0zz529d6dhldg58qm9xbr1a0lqwdzjhgx8bxmg5bav")))

(define-public crate-activity-build-0.1.3 (c (n "activity-build") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "0pplzng588wkz8yyw7ydvwc9d10w5hqhcwd83b236wn4nvw9inpb")))

