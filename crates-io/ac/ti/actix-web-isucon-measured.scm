(define-module (crates-io ac ti actix-web-isucon-measured) #:use-module (crates-io))

(define-public crate-actix-web-isucon-measured-0.1.0 (c (n "actix-web-isucon-measured") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.7") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lb4gad9vw1m0fak87cif33xhvgbh3h2ypzk97y7q1ks5smp135a")))

(define-public crate-actix-web-isucon-measured-0.1.1 (c (n "actix-web-isucon-measured") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.7") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1kymc7faa6vm02j9q7gk5iymasblxr73n0s6ycqkils7fszcwfhd")))

(define-public crate-actix-web-isucon-measured-0.1.2 (c (n "actix-web-isucon-measured") (v "0.1.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.7") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qb7i8f0j6kr5mpb8gwilb1qs7idcz6vrv1yqwnckgbwnr00hsch")))

