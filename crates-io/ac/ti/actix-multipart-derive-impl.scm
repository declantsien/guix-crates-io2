(define-module (crates-io ac ti actix-multipart-derive-impl) #:use-module (crates-io))

(define-public crate-actix-multipart-derive-impl-0.1.0 (c (n "actix-multipart-derive-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "080jmqasw4bdyzyq1rrpc7kdh1jabrnza8rj6313y7bd6q4fyd9x")))

