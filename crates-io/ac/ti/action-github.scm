(define-module (crates-io ac ti action-github) #:use-module (crates-io))

(define-public crate-action-github-0.0.2 (c (n "action-github") (v "0.0.2") (h "0206rpy5h31pl3lid9cxpn393isqmc6hdkw8bba43sycszfjp59q") (f (quote (("default"))))))

(define-public crate-action-github-0.0.3 (c (n "action-github") (v "0.0.3") (h "0gi0z55x0kx4spdjmm8lv8jy06yg6jghv0szxzhbrrj4rpjv4y9i") (f (quote (("default"))))))

