(define-module (crates-io ac ti actix-service) #:use-module (crates-io))

(define-public crate-actix-service-0.1.0 (c (n "actix-service") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "10cz27w9pb6s5sr0gydsqqldiypgddqd4r0f4xzib43nv8zrr5h1")))

(define-public crate-actix-service-0.1.1 (c (n "actix-service") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1dj0gcyyy8zsnsah17l301njy3ffs8kq2ca6lj4w8z5gr2fnjdp7")))

(define-public crate-actix-service-0.1.2 (c (n "actix-service") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1ygpl506dwsbsky6xbz5x2d66bs688v3dlq1ainsyc6hy1awrcyd")))

(define-public crate-actix-service-0.1.3 (c (n "actix-service") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "0br069wq60lf19qxql20djn01aaghx51paywlsfzv23nvpy47v3x")))

(define-public crate-actix-service-0.1.4 (c (n "actix-service") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "0jpai7cjb5197ibrr5b73xi2j3k4ngnczda5c2m2lcvw5g5b3ns6")))

(define-public crate-actix-service-0.1.5 (c (n "actix-service") (v "0.1.5") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1abgk1m8lx7x028hg6vsfz2llq3kl6fpcwgswkz56whk15rniv5w")))

(define-public crate-actix-service-0.1.6 (c (n "actix-service") (v "0.1.6") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1dvi9fxncpbs4pmngf9ckxr3i5cd4lac29y51wvxblpf8kk83h8y")))

(define-public crate-actix-service-0.2.0 (c (n "actix-service") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1xd7sxlyalkgvvw0l9x6rxf5ifpgf1cvk96n3c8adjfkfsi58xz1")))

(define-public crate-actix-service-0.2.1 (c (n "actix-service") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "1mb3nws6py43ss43fid38k3wbdha3969c0abf019pwyaj50m7gx8")))

(define-public crate-actix-service-0.2.2 (c (n "actix-service") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)))) (h "0d0mv7ny5c87srh0vgnxl2fajlwsmf6hs3nigbiczhzicjq2cfqm")))

(define-public crate-actix-service-0.3.0 (c (n "actix-service") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0ls13gpgrg14wvib3nz68qfqlp4lrhv2a8qwg8gsw4vy4rb7kr32")))

(define-public crate-actix-service-0.3.1 (c (n "actix-service") (v "0.3.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "02yyhvhcrmc0ss6dlbxnvw6jpdcnig82n4cqbkqqvpq5gjn6bq1d")))

(define-public crate-actix-service-0.3.2 (c (n "actix-service") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "111xl126zz51hvw4h4xz0xzbqyr40n8bc5163i7cpk3izrp65cbs")))

(define-public crate-actix-service-0.3.3 (c (n "actix-service") (v "0.3.3") (d (list (d (n "actix-rt") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0cgh5qwn8q8yfq0fyi9fwx229j2g2dfw1bq53xpxd679k0lfrrb1")))

(define-public crate-actix-service-0.3.4 (c (n "actix-service") (v "0.3.4") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0l0pvb9inj292xx8msv0mnda3kj77ka5rk800my2fff7n2a8dfnl")))

(define-public crate-actix-service-0.3.5 (c (n "actix-service") (v "0.3.5") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "19vh4lsw1jkdzn0d4ij1dzfvf06nrghrxkpr8lb2l686z2xbv8nq")))

(define-public crate-actix-service-0.3.6 (c (n "actix-service") (v "0.3.6") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0szk469hck0vw5r8kcb3ghpsr8wsbyd036hnvqlc2j0slqsa1gyw")))

(define-public crate-actix-service-0.4.0 (c (n "actix-service") (v "0.4.0") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)))) (h "1xk8bwg2qpz6imdc0caigia9brhjb4kllsk359qfmrzw7478hqyp")))

(define-public crate-actix-service-0.4.1 (c (n "actix-service") (v "0.4.1") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)))) (h "1wlca0k2rc5hcbrwcwsjnm5rc2vxc254yz9sarxbspjrphdw1v5a")))

(define-public crate-actix-service-0.4.2 (c (n "actix-service") (v "0.4.2") (d (list (d (n "actix-rt") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)))) (h "0gvpw11hcr1zmi5qzq3np6qzd0j51mdxn7yfgmzgyhc8ja7b99dw")))

(define-public crate-actix-service-1.0.0-alpha.1 (c (n "actix-service") (v "1.0.0-alpha.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "05i0jvbccah9c8vsgmff49i88i59v3vaknxdw5y8hnmzs10pcl2i")))

(define-public crate-actix-service-1.0.0-alpha.2 (c (n "actix-service") (v "1.0.0-alpha.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1vwzncmj3r366a17bx22f0mf46mal27jfpsb8j9vja1rfq4731wx")))

(define-public crate-actix-service-1.0.0-alpha.3 (c (n "actix-service") (v "1.0.0-alpha.3") (d (list (d (n "actix-rt") (r "^1.0.0-alpha.2") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "0b3sww485g7arzrvs725xsb9zd133qvdjwpyzqmap0kxr7ah7s04")))

(define-public crate-actix-service-1.0.0-alpha.4 (c (n "actix-service") (v "1.0.0-alpha.4") (d (list (d (n "actix-rt") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "1w3fxzqzvqlnbvzsvv9x0h6nd313l8c2yavvhs4dy6ffjww8bw0x")))

(define-public crate-actix-service-1.0.0 (c (n "actix-service") (v "1.0.0") (d (list (d (n "actix-rt") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "0z88pf2r8y05qxp033zn6ryfvprybi2rxsif74j607r75zgilqay")))

(define-public crate-actix-service-1.0.1 (c (n "actix-service") (v "1.0.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "1py2zrqzhnap8jw4scxy7zzrhy2vxcc74k532cngxz4kjvscwpks")))

(define-public crate-actix-service-1.0.2 (c (n "actix-service") (v "1.0.2") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "1vn7bafpzaq7lj24yxg8n4883lc8qg5gl47l7igs2almy5ql3flz")))

(define-public crate-actix-service-1.0.3 (c (n "actix-service") (v "1.0.3") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "09fm1sskvm492hnvcl5xli4pkl81hhp9gm55055ksh8v2j2rhn1p") (y #t)))

(define-public crate-actix-service-1.0.4 (c (n "actix-service") (v "1.0.4") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "03gmnhcmxqw6fhjq9xl89896m7swwxncb2fwmr4yqz1nsvfw5kjr")))

(define-public crate-actix-service-1.0.5 (c (n "actix-service") (v "1.0.5") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "0r3m51fackpwj73awg2fkr4yic4abyw4dfyhn9ql3qm7vyazrr6k")))

(define-public crate-actix-service-1.0.6 (c (n "actix-service") (v "1.0.6") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.17") (d #t) (k 0)))) (h "1fw2b1cpxrpqk778mpvxv0cazj0pwjyb6khzs4syhnqvb1fl6lh0")))

(define-public crate-actix-service-2.0.0-beta.1 (c (n "actix-service") (v "2.0.0-beta.1") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "12dvpifbpzhycql3m2r6pkahdvghika70wxv6dm4brrdwqf773a6")))

(define-public crate-actix-service-2.0.0-beta.2 (c (n "actix-service") (v "2.0.0-beta.2") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1rwwrp16la2vlc08zmr3pbin8brnz6pxw5wzn9kw9zqbda32qgi3")))

(define-public crate-actix-service-2.0.0-beta.3 (c (n "actix-service") (v "2.0.0-beta.3") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0wfn89qd8g5vxk141q9pfh43lpwn666wg0ln4q273rfizh4ynnb6")))

(define-public crate-actix-service-2.0.0-beta.4 (c (n "actix-service") (v "2.0.0-beta.4") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0qqxyirdwz68lc63zgcp8519prkbdwkml5g3992sr119sgs5d5ya")))

(define-public crate-actix-service-2.0.0-beta.5 (c (n "actix-service") (v "2.0.0-beta.5") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "019bpm065bv0bxkm9p2w05la4y4pk6y3pz9p6zscmr7lv45390ng")))

(define-public crate-actix-service-2.0.0 (c (n "actix-service") (v "2.0.0") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0d2hf15clvxbnx08lql6h18yalap9x1g69icwvxd0c47dbbgkxbp")))

(define-public crate-actix-service-2.0.1 (c (n "actix-service") (v "2.0.1") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "07pyxdhnlrg6xfzmj02ha6x1qsfl4hfpi96p1159g0mh32kccgcd")))

(define-public crate-actix-service-2.0.2 (c (n "actix-service") (v "2.0.2") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0fipjcc5kma7j47jfrw55qm09dakgvx617jbriydrkqqz10lk29v")))

