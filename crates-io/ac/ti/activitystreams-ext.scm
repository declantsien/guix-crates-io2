(define-module (crates-io ac ti activitystreams-ext) #:use-module (crates-io))

(define-public crate-activitystreams-ext-0.1.0-alpha.0 (c (n "activitystreams-ext") (v "0.1.0-alpha.0") (d (list (d (n "activitystreams") (r "^0.7.0-alpha.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jbqgz6fcplvr769klm9pv25jfm58mq8zxln43wxdmp38shsm7sb")))

(define-public crate-activitystreams-ext-0.1.0-alpha.1 (c (n "activitystreams-ext") (v "0.1.0-alpha.1") (d (list (d (n "activitystreams") (r "^0.7.0-alpha.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pwg8qn0zbml55b8db8zyw6aaihdds1gygv2162wirnw6mbr6bpy")))

(define-public crate-activitystreams-ext-0.1.0-alpha.2 (c (n "activitystreams-ext") (v "0.1.0-alpha.2") (d (list (d (n "activitystreams") (r "^0.7.0-alpha.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ps5x57wxm9awf4394y616p72lggn5s0qahagrmvhhch6h1k3mv")))

(define-public crate-activitystreams-ext-0.1.0-alpha.3 (c (n "activitystreams-ext") (v "0.1.0-alpha.3") (d (list (d (n "activitystreams") (r "^0.7.0-alpha.21") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cq5lk4cajjvhb5ifwsr1gizlkq9jis0yh3j0qpbrwmbbbwwak6d")))

