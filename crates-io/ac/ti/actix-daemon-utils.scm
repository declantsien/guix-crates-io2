(define-module (crates-io ac ti actix-daemon-utils) #:use-module (crates-io))

(define-public crate-actix-daemon-utils-0.1.0 (c (n "actix-daemon-utils") (v "0.1.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0yyfzw34cgb6vigcjpfszp9jdfwa2r1gagc71l13scpc4p0bdy9x")))

(define-public crate-actix-daemon-utils-0.2.0 (c (n "actix-daemon-utils") (v "0.2.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0sdjn40y342z5196f0gsfa922p56kn50vyzmn3blbbba8phdxj6v")))

(define-public crate-actix-daemon-utils-0.2.1 (c (n "actix-daemon-utils") (v "0.2.1") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0gx06bvwwdx5d0dprcd41afyv8ja0bnss03sr3d4gww8z9s7r850")))

(define-public crate-actix-daemon-utils-0.3.0 (c (n "actix-daemon-utils") (v "0.3.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "11p9qzgql1i2k3vfw4dk8qvybzkd8dszb1dd0xw3z5a0hfz9j9p8")))

(define-public crate-actix-daemon-utils-0.3.1 (c (n "actix-daemon-utils") (v "0.3.1") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream"))) (d #t) (k 0)))) (h "1902i5ddn43jhhhbvx5lr5i07ks1hbdssgcb1qan6hjwbgvaf16w")))

(define-public crate-actix-daemon-utils-0.4.0 (c (n "actix-daemon-utils") (v "0.4.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream"))) (d #t) (k 0)))) (h "0s2lpyy4pnspiqr1qrpz9v962mwkcl6h56vmb1xjz4zbv6s5aksm")))

(define-public crate-actix-daemon-utils-0.4.1 (c (n "actix-daemon-utils") (v "0.4.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream"))) (d #t) (k 0)))) (h "11h74pfpjv3z46m9mh5mpp0rfl6w1kg7w6if6zk8cjxpbxh78m54")))

(define-public crate-actix-daemon-utils-0.4.2 (c (n "actix-daemon-utils") (v "0.4.2") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream"))) (d #t) (k 0)))) (h "1c6f4ww6a2zhry6kb4g9gv9yb2z1q12568vf7sxm9zs8ldjjqxdr")))

(define-public crate-actix-daemon-utils-0.5.0 (c (n "actix-daemon-utils") (v "0.5.0") (d (list (d (n "actix") (r "~0.11.0") (d #t) (k 0)) (d (n "actix-rt") (r "~2") (d #t) (k 0)) (d (n "futures-util") (r "~0.3.13") (d #t) (k 0)))) (h "1rr6gbsnq07qpm8ys00fkaz96bv0w83r8dp46ngy49k4iviy5ls0")))

(define-public crate-actix-daemon-utils-0.6.0 (c (n "actix-daemon-utils") (v "0.6.0") (d (list (d (n "actix") (r "~0.12.0") (d #t) (k 0)) (d (n "actix-rt") (r "~2") (d #t) (k 0)) (d (n "futures-util") (r "~0.3.13") (d #t) (k 0)))) (h "1g0q44vybmxjk4cijhwzajr81jw1l5f2s66av9iii05wi68rmvqx")))

(define-public crate-actix-daemon-utils-0.7.0 (c (n "actix-daemon-utils") (v "0.7.0") (d (list (d (n "actix") (r "~0.13.0") (d #t) (k 0)) (d (n "actix-rt") (r "~2") (d #t) (k 0)) (d (n "futures-util") (r "~0.3") (d #t) (k 0)))) (h "1wgg2csc96f7s5i50srry7b61bk60a2hvvgkh2wm3pkg8sci5kfq")))

