(define-module (crates-io ac ti actix-tracing) #:use-module (crates-io))

(define-public crate-actix-tracing-0.1.0 (c (n "actix-tracing") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-service") (r "^1.0.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)))) (h "088cyfxvp55rhpb1x5brc3d9x2bdw8jj11jdggq9143hwwrhj1f5")))

