(define-module (crates-io ac ti actix-bincode) #:use-module (crates-io))

(define-public crate-actix-bincode-0.1.0 (c (n "actix-bincode") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "12ciir61gz3ssvac2y71kpj31llpv2w5r0k4liny9k7zr4gzl34q")))

(define-public crate-actix-bincode-0.1.1 (c (n "actix-bincode") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wq680sh1fs31pfk22d63xcam63dv0qakc9rqd7xm0b770w7k98h")))

(define-public crate-actix-bincode-0.2.0 (c (n "actix-bincode") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iypgxs9r79xpr7rxsp7lx4mb5l9ibkvykh3z459nnw8n9vpya7i") (f (quote (("serde"))))))

(define-public crate-actix-bincode-0.2.1 (c (n "actix-bincode") (v "0.2.1") (d (list (d (n "actix-web") (r "^4.2.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "149p3z0g1rp3h81kznx9qwvqxshm1idgd62jpk1bi8n5k890nala") (f (quote (("serde"))))))

(define-public crate-actix-bincode-0.2.2 (c (n "actix-bincode") (v "0.2.2") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cwb8nazanbhic4ayqh6jxqfaa88wpdyn7i0vljaqpyzmg1d6j5n") (f (quote (("serde"))))))

(define-public crate-actix-bincode-0.2.3 (c (n "actix-bincode") (v "0.2.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h94vai9pm1c5l8c7c6ja23wgjm97mywr5lcvxxrz82620jjr7sf") (f (quote (("serde"))))))

(define-public crate-actix-bincode-0.2.4 (c (n "actix-bincode") (v "0.2.4") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lmdkpg30zfxzfskyv7fgvs4ssmwbvbda11sq0q19v2ipssyfmiv") (s 2) (e (quote (("serde" "dep:serde"))))))

