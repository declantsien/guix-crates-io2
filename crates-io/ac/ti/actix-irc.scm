(define-module (crates-io ac ti actix-irc) #:use-module (crates-io))

(define-public crate-actix-irc-0.0.0 (c (n "actix-irc") (v "0.0.0") (h "1yasv8q4vzgb12ncp63zqrdm93gg3dgqk4bjw90i3sirdl3ipsql")))

(define-public crate-actix-irc-0.0.1 (c (n "actix-irc") (v "0.0.1") (h "0djyvgdigbvhp05vljdlqbi3s1i5lyyqaa5cg9nh35vj1j0v1n68") (f (quote (("external_doc"))))))

