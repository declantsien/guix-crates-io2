(define-module (crates-io ac ti actix-admin-macros) #:use-module (crates-io))

(define-public crate-actix-admin-macros-0.1.0 (c (n "actix-admin-macros") (v "0.1.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12j6jqnkvmj7bdv2v3bh8q50dqz01v1km7ry3zsaa1klp05ili8c")))

(define-public crate-actix-admin-macros-0.2.0 (c (n "actix-admin-macros") (v "0.2.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19w6p0gqbw69s6mn6aq6ain4ncmk6xlalpg9g8sn0wc7w9qqaxwb")))

(define-public crate-actix-admin-macros-0.3.0 (c (n "actix-admin-macros") (v "0.3.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xngyh0g7bfq5m2c29zrsry0idlmn776b5r8knkbr4kn7515nbkw")))

(define-public crate-actix-admin-macros-0.4.0 (c (n "actix-admin-macros") (v "0.4.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m44c7vb426b4jmxxar4xkxw6zd2pl1yiknz810kiz6lca0kfl90")))

(define-public crate-actix-admin-macros-0.5.0 (c (n "actix-admin-macros") (v "0.5.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f4ibyczlnprf7bwn126ayhpplksvw0hzrk3ln0hgi4kiis0shlz")))

(define-public crate-actix-admin-macros-0.6.0 (c (n "actix-admin-macros") (v "0.6.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kc4xjj84x8yid042w0mccngmgfm2ppzxsisyc8qm5q4kd0ca85w")))

