(define-module (crates-io ac ti activitypub_federation_derive) #:use-module (crates-io))

(define-public crate-activitypub_federation_derive-0.1.0 (c (n "activitypub_federation_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.57") (f (quote ("diff"))) (d #t) (k 2)))) (h "0apcimk9r2bp3mb5jabvcnkba6559ysbpxpd1lxrsrkbcxcayajs")))

(define-public crate-activitypub_federation_derive-0.2.0 (c (n "activitypub_federation_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (f (quote ("diff"))) (d #t) (k 2)))) (h "09j1wliyhzigzaijqr7h7l8mqvlwd2ib5683p4qas8hgzia0nlh7")))

