(define-module (crates-io ac ti actix-web-jwtauth) #:use-module (crates-io))

(define-public crate-actix-web-jwtauth-0.1.0 (c (n "actix-web-jwtauth") (v "0.1.0") (d (list (d (n "actix-rt") (r "1.*") (d #t) (k 2)) (d (n "actix-web") (r "3.*") (k 0)) (d (n "actix-web") (r "3.*") (k 2)) (d (n "anyhow") (r "1.*") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "jsonwebtoken") (r "7.*") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g2y7j6d1h3sb6dhd0d3wsdqznh9l33y7qn4zgdlki36w6mi2cpz") (f (quote (("encode") ("default") ("decode"))))))

