(define-module (crates-io ac ti action-core) #:use-module (crates-io))

(define-public crate-action-core-0.0.6 (c (n "action-core") (v "0.0.6") (d (list (d (n "action-derive") (r "=0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "00kprrv0fiqi3is2xlzxgrk2gk48sizzp9wrxgi4wkx8yjg7jwys") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_yaml") ("derive" "dep:action-derive"))))))

(define-public crate-action-core-0.0.7 (c (n "action-core") (v "0.0.7") (d (list (d (n "action-derive") (r "=0.0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nahzm9myyi3fbi7zzfmz2xfb4h0dhaxpz84gzb8kai4y6l18dmw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_yaml") ("derive" "dep:action-derive"))))))

