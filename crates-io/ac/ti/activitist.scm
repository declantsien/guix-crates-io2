(define-module (crates-io ac ti activitist) #:use-module (crates-io))

(define-public crate-activitist-0.1.0 (c (n "activitist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.3") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "125mcp3nw5n0mijx4r8pzpmlra3q7h9dvrgin4p0prg25vv40bmn")))

(define-public crate-activitist-0.2.0 (c (n "activitist") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.3") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1hlmq4b8h09xxdxj6qh75lylki4had8w9lp6wilylz3mzyqcndlx")))

(define-public crate-activitist-0.3.0 (c (n "activitist") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.3") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1fjg4mvnwknf3nbs3gl59lfjnmlz84llsa1g54v41r57gha8ipd5")))

(define-public crate-activitist-0.4.0 (c (n "activitist") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.3") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "04dq0j0aq1l7kf537yrj1jv5md1inxlpxb7af176dh6z9bgmh3f9")))

(define-public crate-activitist-0.5.0 (c (n "activitist") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.6") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1vf3ciisq6d8c5wmwrg35yp6zr4aw3ya7l2bi1gar5dv579y3fpw")))

(define-public crate-activitist-0.6.0 (c (n "activitist") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "std"))) (k 0)) (d (n "serde_with") (r "^3.6") (f (quote ("macros"))) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0pg3bk2pf53bif8wgbbwmsp6sivby35fmjlbyjb2wf85wyj2p93m")))

