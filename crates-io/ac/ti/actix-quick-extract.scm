(define-module (crates-io ac ti actix-quick-extract) #:use-module (crates-io))

(define-public crate-actix-quick-extract-0.1.0-beta.0 (c (n "actix-quick-extract") (v "0.1.0-beta.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("display" "into" "deref" "as_ref" "from"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02n3ngbnaxq3ikxfdpfs2mhh2r4f4mdxz86hmf5s7syc2c6rm8da")))

