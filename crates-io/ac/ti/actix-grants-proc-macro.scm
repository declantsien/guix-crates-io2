(define-module (crates-io ac ti actix-grants-proc-macro) #:use-module (crates-io))

(define-public crate-actix-grants-proc-macro-0.1.0 (c (n "actix-grants-proc-macro") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0dp4pn108lzvr35q0hyy4yg2wka7ax6cly6c0cmn434mvlr5fh23")))

(define-public crate-actix-grants-proc-macro-0.1.1 (c (n "actix-grants-proc-macro") (v "0.1.1") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0xpw6bjam6lj6bx27jq2qgsxclvwqc3z1r37kml94nrqmsdpmscb")))

(define-public crate-actix-grants-proc-macro-0.1.2 (c (n "actix-grants-proc-macro") (v "0.1.2") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "05w93irzz1ai8p3aky2wjsqf39ksxi2sxb12vn8cwi6i7h8pix73")))

(define-public crate-actix-grants-proc-macro-0.1.3 (c (n "actix-grants-proc-macro") (v "0.1.3") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "05didbsbf27i9rk249zs1vhpmaa6g5g38fidc2lcv8hc51b9g94r")))

(define-public crate-actix-grants-proc-macro-1.0.0 (c (n "actix-grants-proc-macro") (v "1.0.0") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "14w4w46ipz4ss0vbqh7m41xqhbvng7xccsf6x6z9ba7lgy8nphxz")))

(define-public crate-actix-grants-proc-macro-1.0.1 (c (n "actix-grants-proc-macro") (v "1.0.1") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "041psc3n5ybhgy294zpjw88pm19fn1nkkg4rb62v8frs9d3bnz2c")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.1 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.1") (d (list (d (n "actix-web") (r "^4.0.0-beta.5") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "189d4kfyikbw1536yq6xhfwkmf6rqvv85m4j8wx5qbz7mghspa08")))

(define-public crate-actix-grants-proc-macro-1.1.0 (c (n "actix-grants-proc-macro") (v "1.1.0") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0jaxrn57bz15g2y5hqc4s81fgl26m43jmh8psf22v600k5cx2c2c")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.2 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.2") (d (list (d (n "actix-web") (r "^4.0.0-beta.8") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0z28vp9075m0mv8zqy6ikqp8qq8vbf5y3q26ac2xprhl78xkh63g")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.3 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.3") (d (list (d (n "actix-web") (r "^4.0.0-beta.9") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0n3b13r5mnipilg7niqsgh79kp304wzs6carcnh0jy22mddwqrkp")))

(define-public crate-actix-grants-proc-macro-1.2.0 (c (n "actix-grants-proc-macro") (v "1.2.0") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1aawnn5aq26dvh02fwmdnwqp831jz6fiy0qbcglazjfj2wzrkxda")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.4 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.4") (d (list (d (n "actix-web") (r "^4.0.0-beta.11") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0kg40nfqpl97v06ajznkdvsmgks1nygq3b0b6xvzv81d8fl5jwg6")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.5 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.5") (d (list (d (n "actix-web") (r "^4.0.0-beta.18") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0d8a7mm4zms7zhq5qqgkjb2a8i5d0k2fnm20cwbh3sq0h88cwxy8")))

(define-public crate-actix-grants-proc-macro-2.0.0-beta.6 (c (n "actix-grants-proc-macro") (v "2.0.0-beta.6") (d (list (d (n "actix-web") (r "^4.0.0-beta.19") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "14n2b89y6mhs5nddx8fhbkmnjwq3cznc0qxwwncmx2slry8q1hp5")))

(define-public crate-actix-grants-proc-macro-1.3.0 (c (n "actix-grants-proc-macro") (v "1.3.0") (d (list (d (n "actix-web") (r "^3.0.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0zi1mfmzwvprzav8fbr9y37mvm3xa81kmk0i4qzxqxzvgj2hb98x")))

(define-public crate-actix-grants-proc-macro-2.0.0 (c (n "actix-grants-proc-macro") (v "2.0.0") (d (list (d (n "actix-web") (r "^4.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0cp336q4pbd164p77ds5llhfab1k94g18pi80s5qbxpdfz3kx5zm")))

(define-public crate-actix-grants-proc-macro-2.0.1 (c (n "actix-grants-proc-macro") (v "2.0.1") (d (list (d (n "actix-web") (r "^4.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1hnaixj0blzx34a1dmgmv767dx48jqrrnzi7ydmz9rhr00g3rps8")))

(define-public crate-actix-grants-proc-macro-2.0.2 (c (n "actix-grants-proc-macro") (v "2.0.2") (d (list (d (n "actix-web") (r "^4.3") (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0cjg3q3ni5d1syhzh6ln2rnv5b3c879i9v35mlsc806x2p9shy62")))

