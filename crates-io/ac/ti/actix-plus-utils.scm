(define-module (crates-io ac ti actix-plus-utils) #:use-module (crates-io))

(define-public crate-actix-plus-utils-0.1.0 (c (n "actix-plus-utils") (v "0.1.0") (d (list (d (n "actix-plus-error") (r "^0.1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "unic-ucd-category") (r "^0.9.0") (d #t) (k 0)))) (h "0ai5lp3v379ivzmgqfcm8qpiq3wchngndw4kkwnncpx5j8mv2cj3")))

