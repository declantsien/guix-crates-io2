(define-module (crates-io ac ti actix-flash) #:use-module (crates-io))

(define-public crate-actix-flash-0.1.0 (c (n "actix-flash") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-service") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1jn9a1jbdhw041fqkv9ncrfb98s8kq4q4wm06ysx1qxlnxpl0i1r")))

(define-public crate-actix-flash-0.2.0 (c (n "actix-flash") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-service") (r "^1") (d #t) (k 0)) (d (n "actix-web-v2") (r "^2") (o #t) (d #t) (k 0) (p "actix-web")) (d (n "actix-web-v3") (r "^3") (o #t) (k 0) (p "actix-web")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kwl6p06znq51hwwavja2dws8dypvvdh1y0l58vaar7gn4piqrp6") (f (quote (("v3" "actix-web-v3") ("v2" "actix-web-v2") ("default" "v3"))))))

