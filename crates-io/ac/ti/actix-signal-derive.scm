(define-module (crates-io ac ti actix-signal-derive) #:use-module (crates-io))

(define-public crate-actix-signal-derive-0.1.0 (c (n "actix-signal-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19sjfpip5vx01zkprbnvfk19a38vhg0brdd5b4vf47jgla8y85zr")))

