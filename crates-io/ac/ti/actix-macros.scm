(define-module (crates-io ac ti actix-macros) #:use-module (crates-io))

(define-public crate-actix-macros-0.1.0-alpha.1 (c (n "actix-macros") (v "0.1.0-alpha.1") (d (list (d (n "actix-rt") (r "^1.0.0-alpha.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v8nv9aqc6c3r587p0lhlkbampw591cyv1snl4g4xsavfsmbjphp")))

(define-public crate-actix-macros-0.1.0 (c (n "actix-macros") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h6zrfc0pyn7yiqsx2hrwh3zr5gyva7yznski9b95llynnv7iqr1")))

(define-public crate-actix-macros-0.1.1 (c (n "actix-macros") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhn4fvw79pfnq5sc12pbq0wc06di4xfg4288fcbrr5vfvf5lw11")))

(define-public crate-actix-macros-0.1.2 (c (n "actix-macros") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 2)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1liydadas4i955zhvvdsvi4qmnfp1ifbn56bmbrrgpz6qjkrn3x6")))

(define-public crate-actix-macros-0.1.3 (c (n "actix-macros") (v "0.1.3") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 2)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0mfnprr8gy1gb5xcr18iwsv781hysvh7sr5xxg6ghyi61gh8rjml") (f (quote (("actix-reexport"))))))

(define-public crate-actix-macros-0.2.0-beta.1 (c (n "actix-macros") (v "0.2.0-beta.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "113hmpy1nm5kc5iv832amjk7k8fbszpszckwjgns6w8lkj051m9j") (f (quote (("actix-reexport"))))))

(define-public crate-actix-macros-0.2.0 (c (n "actix-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vq4dfbcyr2q0f3i8msvwi7dj4yfjhhqgpgkpksw5k0aixh2pjyv")))

(define-public crate-actix-macros-0.2.1 (c (n "actix-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dw88g9lzrhkzq8hxh1lvykr87wdm0cicyz5kxp5w4vwhpb6ry62")))

(define-public crate-actix-macros-0.2.2 (c (n "actix-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ci58xh50g0g8wz17r0m5hnd3k17rcyy8q5vnm142hgqypm9kjf8")))

(define-public crate-actix-macros-0.2.3 (c (n "actix-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhk2bdp6rj67j5zgi4b76hpy2xw567js0hig28n1fb9rxr62nj6")))

(define-public crate-actix-macros-0.2.4 (c (n "actix-macros") (v "0.2.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jsmhq9k5nsms8sci2lqkihym5nrhlpfv8dgd0n4539g1cad67p0") (r "1.65")))

