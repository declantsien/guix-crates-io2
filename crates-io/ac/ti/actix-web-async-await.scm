(define-module (crates-io ac ti actix-web-async-await) #:use-module (crates-io))

(define-public crate-actix-web-async-await-0.1.0 (c (n "actix-web-async-await") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.7.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-async-await") (r "^0.1.4") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "1fwd9k1a7c82zclby854hwsz5i9jwhavsnbpd7jflfpxq5njy2ya")))

(define-public crate-actix-web-async-await-0.1.1 (c (n "actix-web-async-await") (v "0.1.1") (d (list (d (n "actix-web") (r "^0.7.13") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-async-await") (r "^0.1.4") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "1w7hq0wk06q9j5q9b6fjh4aljmj3ins8cxmydan5f18nw3db9kv7")))

(define-public crate-actix-web-async-await-0.2.0 (c (n "actix-web-async-await") (v "0.2.0") (d (list (d (n "actix-web") (r "^0.7.13") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-async-await") (r "^0.1.4") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "1p0jskbk50j11ak8gibn9xl8z6yhyc98ycgka1zpxxsxbfjckskf")))

