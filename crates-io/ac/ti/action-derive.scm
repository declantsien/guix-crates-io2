(define-module (crates-io ac ti action-derive) #:use-module (crates-io))

(define-public crate-action-derive-0.0.1 (c (n "action-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r96dwm0h9clj7aiqb60jdrxhay5xiz8802d6jp4x6a19sq27b7f") (f (quote (("default"))))))

(define-public crate-action-derive-0.0.6 (c (n "action-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0izzmssd0d5bckp0xigwysj4aiiy45vlx33p4lxrjbxi8di78c4w") (f (quote (("default"))))))

(define-public crate-action-derive-0.0.7 (c (n "action-derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jabfz3fa0cwa0246mczfiwdacbjqwmav63q3h3z7zfvkb00lm7q") (f (quote (("default"))))))

