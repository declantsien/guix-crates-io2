(define-module (crates-io ac ti actix-webpack-proxy) #:use-module (crates-io))

(define-public crate-actix-webpack-proxy-0.1.0 (c (n "actix-webpack-proxy") (v "0.1.0") (d (list (d (n "actix") (r "^0.8") (d #t) (k 0)) (d (n "actix-codec") (r "^0.1") (d #t) (k 0)) (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^1.0") (d #t) (k 0)) (d (n "awc") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zzmng6qayxf2bp5mxfv69hx9pgplr6n8z5c0nr55knji3w0z7qx")))

