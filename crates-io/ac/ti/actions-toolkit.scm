(define-module (crates-io ac ti actions-toolkit) #:use-module (crates-io))

(define-public crate-actions-toolkit-0.0.1 (c (n "actions-toolkit") (v "0.0.1") (d (list (d (n "actions-core") (r "= 0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cp1qlmblav9rxnhfbszp5h3lnf2bq0lz6pysr6ki3f4sph2ga12")))

(define-public crate-actions-toolkit-0.0.2 (c (n "actions-toolkit") (v "0.0.2") (d (list (d (n "actions-core") (r "= 0.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "10cn1y2slfpcc130m6jd5sr0alg1dv9qxz78wvls57gkri20g253")))

