(define-module (crates-io ac ti actix-web-pagination) #:use-module (crates-io))

(define-public crate-actix-web-pagination-0.1.0 (c (n "actix-web-pagination") (v "0.1.0") (d (list (d (n "actix-rt") (r "1.*") (d #t) (k 2)) (d (n "actix-web") (r "3.*") (k 0)) (d (n "actix-web") (r "3.*") (k 2)) (d (n "form_urlencoded") (r "1.*") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "0jybjmn7afny6cv9r9n7i3kq98818167n9x2gryjslzfxmlan5rq")))

(define-public crate-actix-web-pagination-0.1.1 (c (n "actix-web-pagination") (v "0.1.1") (d (list (d (n "actix-rt") (r "1.*") (d #t) (k 2)) (d (n "actix-web") (r "3.*") (k 0)) (d (n "actix-web") (r "3.*") (k 2)) (d (n "form_urlencoded") (r "1.*") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qypvh2dzyf1y2bs24ikanww1azdvrf47lvprmapz0ci4jaaf7zk")))

(define-public crate-actix-web-pagination-0.1.2 (c (n "actix-web-pagination") (v "0.1.2") (d (list (d (n "actix-rt") (r "1.*") (d #t) (k 2)) (d (n "actix-web") (r "3.*") (k 0)) (d (n "actix-web") (r "3.*") (k 2)) (d (n "form_urlencoded") (r "1.*") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "13lfdg69qs6iw7g6hm3yg3d0pl6iip2bzv22i8ndhk41pdqqd47y")))

