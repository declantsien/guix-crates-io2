(define-module (crates-io ac ti action_msgs) #:use-module (crates-io))

(define-public crate-action_msgs-1.2.1 (c (n "action_msgs") (v "1.2.1") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "unique_identifier_msgs") (r "^2.2.1") (d #t) (k 0)))) (h "0n9r2awvfb095ijax0b0ad60sgs238256s7ihi1r4q2inwi90hfm") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "unique_identifier_msgs/serde" "builtin_interfaces/serde"))))))

