(define-module (crates-io ac ti actix-web-middleware-redirect-https) #:use-module (crates-io))

(define-public crate-actix-web-middleware-redirect-https-0.1.0 (c (n "actix-web-middleware-redirect-https") (v "0.1.0") (d (list (d (n "actix-service") (r "^0.4.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "16kzmdh5xdf6dbzi5dri2z0jx31bb8xpjg8g2s2iz1z63z0cq88i")))

(define-public crate-actix-web-middleware-redirect-https-0.1.1 (c (n "actix-web-middleware-redirect-https") (v "0.1.1") (d (list (d (n "actix-service") (r "^0.4.2") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "19lfx3a7ppc01q4d2zbqca41ir0ja9jkcc0icybcchfd6b17yrla")))

(define-public crate-actix-web-middleware-redirect-https-1.0.0 (c (n "actix-web-middleware-redirect-https") (v "1.0.0") (d (list (d (n "actix-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1a3wz7pni8kpm2ylkh90ihn6jkjrcxi8qxh4lw6gv70m80vfd56a")))

(define-public crate-actix-web-middleware-redirect-https-3.0.0 (c (n "actix-web-middleware-redirect-https") (v "3.0.0") (d (list (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1sgrqdzz3rfqh5hbb6vdc2q360dxpqadgmlr1qzskkwsbc78w921")))

(define-public crate-actix-web-middleware-redirect-https-3.0.1 (c (n "actix-web-middleware-redirect-https") (v "3.0.1") (d (list (d (n "actix-service") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1ikd4mjz52akdjjw728bww35b5y3r76ls7kd1w92jg10hr3ldq9w")))

