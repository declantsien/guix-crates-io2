(define-module (crates-io ac ti active_sse) #:use-module (crates-io))

(define-public crate-active_sse-0.1.0 (c (n "active_sse") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "sse-client") (r "^1.0.0") (d #t) (k 0)))) (h "1nk0rqx9ibr4icfyii868wzp0v5vavg04gczcxvkwbgpqrpvvwfd")))

(define-public crate-active_sse-0.1.1 (c (n "active_sse") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "sse-client") (r "^1.0.0") (d #t) (k 0)))) (h "0lfn0jx5a1fvsma04axiks8gj1zxmrhdahdlql4c0gyy22z425pi")))

