(define-module (crates-io ac ti actions-templates) #:use-module (crates-io))

(define-public crate-actions-templates-0.1.0 (c (n "actions-templates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (f (quote ("no_logging"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1l0fnrfci8l97xigzrk6awkszp5pni0v5blblx7avw3x1gai1w3l") (f (quote (("default" "dialoguer")))) (s 2) (e (quote (("dialoguer" "dep:dialoguer"))))))

