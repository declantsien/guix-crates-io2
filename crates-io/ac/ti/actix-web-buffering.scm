(define-module (crates-io ac ti actix-web-buffering) #:use-module (crates-io))

(define-public crate-actix-web-buffering-0.1.0 (c (n "actix-web-buffering") (v "0.1.0") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lhh92ymdrc9vs12lfa6fwdvsf8qw3k3036aaj0f2kshyig3hmzh")))

(define-public crate-actix-web-buffering-0.1.1 (c (n "actix-web-buffering") (v "0.1.1") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "17qn53l20m6giw5wpx3m5qk6b5ng0kf2lzmac1w1w6cdijmgayfz")))

(define-public crate-actix-web-buffering-0.1.2 (c (n "actix-web-buffering") (v "0.1.2") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "00v3zq4hwv5gnm2pmyrrlqhqpw7ayggdipfz8jclw2kpf32r1bk7")))

