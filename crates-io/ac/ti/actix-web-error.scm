(define-module (crates-io ac ti actix-web-error) #:use-module (crates-io))

(define-public crate-actix-web-error-0.1.0 (c (n "actix-web-error") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.1.0") (k 2)) (d (n "actix-web-error-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0lls1kc5l8i6i08hb466y4fpca3y916sy4d21qlr8amanlib29mg")))

(define-public crate-actix-web-error-0.2.0 (c (n "actix-web-error") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.1.0") (k 2)) (d (n "actix-web-error-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1v1xpm1fq1wvg30fixwhn7xx2177gkfiahd9x1542livixvbjz1b")))

