(define-module (crates-io ac ti actix-clean-path) #:use-module (crates-io))

(define-public crate-actix-clean-path-1.0.0 (c (n "actix-clean-path") (v "1.0.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-web") (r "^3.0.0-alpha.3") (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)))) (h "1nvldf6bixawdgqi9aw8w8inr2fw00djjyhhsy4fwrb4r2ny3qn7")))

