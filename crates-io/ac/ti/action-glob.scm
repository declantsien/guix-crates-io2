(define-module (crates-io ac ti action-glob) #:use-module (crates-io))

(define-public crate-action-glob-0.0.2 (c (n "action-glob") (v "0.0.2") (h "17krw072ylkdhpznp587mh85y3zl2ga6vlwa1lynjwv2slv626da") (f (quote (("default"))))))

(define-public crate-action-glob-0.0.3 (c (n "action-glob") (v "0.0.3") (h "1cyqvmd9x5j63s9pbj7abwf549hxy8xcylxjdk89xhb8xc9gqxsg") (f (quote (("default"))))))

