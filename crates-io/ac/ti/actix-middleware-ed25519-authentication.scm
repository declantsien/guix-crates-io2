(define-module (crates-io ac ti actix-middleware-ed25519-authentication) #:use-module (crates-io))

(define-public crate-actix-middleware-ed25519-authentication-0.1.0 (c (n "actix-middleware-ed25519-authentication") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0c7ajikzv31jjlscxkkq1996xb59rpkpkvlvzn78xrgnbprzx1bz")))

(define-public crate-actix-middleware-ed25519-authentication-0.1.1 (c (n "actix-middleware-ed25519-authentication") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0n9bx9ipwhyrd9q5wgrmxai61i1ayqh7gmh15ib01w0akdq92xkp")))

(define-public crate-actix-middleware-ed25519-authentication-0.1.2 (c (n "actix-middleware-ed25519-authentication") (v "0.1.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1zzhhy15fwsfpksrnpkvl33m25ciwpy1mvy59pr2v14d7kk6b2fh")))

(define-public crate-actix-middleware-ed25519-authentication-0.2.0 (c (n "actix-middleware-ed25519-authentication") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1h4j9a6g8k0zl57wdzw7p59gb5z2rkc6b1j9y80wxb30vk3rigql")))

(define-public crate-actix-middleware-ed25519-authentication-0.2.1 (c (n "actix-middleware-ed25519-authentication") (v "0.2.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1vwa72f8d2qn8xbafly3z6ywbazlq8wapwg55p9i65r9mmzqshmb")))

(define-public crate-actix-middleware-ed25519-authentication-0.2.2 (c (n "actix-middleware-ed25519-authentication") (v "0.2.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1ncncswc2lxmg4chbw991056qbypkqk1nq26k33jdjpp913qv1nb")))

(define-public crate-actix-middleware-ed25519-authentication-0.3.0 (c (n "actix-middleware-ed25519-authentication") (v "0.3.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1wkapm3c1zbw3h3312gpfcs4y1s7rva5lhaban82lcswxk0s6083")))

