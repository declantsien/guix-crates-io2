(define-module (crates-io ac ti activity-vocabulary-core) #:use-module (crates-io))

(define-public crate-activity-vocabulary-core-0.0.2 (c (n "activity-vocabulary-core") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "19grgdz2vhfqv211xx9qqi9v1zy5lmwy6pag35xmx5s6xq4yz4wp")))

(define-public crate-activity-vocabulary-core-0.0.3 (c (n "activity-vocabulary-core") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "10wmdi9xc09i0z3bdnjq9p14mqlmf8syz21m3gkzhj95k2zvpbl4") (y #t)))

(define-public crate-activity-vocabulary-core-0.0.4 (c (n "activity-vocabulary-core") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pk8jyiw9lz2hfx4g45liswia9jy470l842k5nksww48pldpss5j")))

(define-public crate-activity-vocabulary-core-0.0.5 (c (n "activity-vocabulary-core") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "098q5clyvjgjabnmph53f033sia7l44l82nsfcnr61jfc9abd426")))

