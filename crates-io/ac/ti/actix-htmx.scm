(define-module (crates-io ac ti actix-htmx) #:use-module (crates-io))

(define-public crate-actix-htmx-0.1.0 (c (n "actix-htmx") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z3vhlkwfhnz594a7izyrcww9zw8p6agcrk81fwsbjd2hh9c57zd")))

(define-public crate-actix-htmx-0.2.0 (c (n "actix-htmx") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k278hpbb1kdnb7xa3yxask0yr958fg2pl68lpcvhxzvr0iza12w")))

(define-public crate-actix-htmx-0.3.0 (c (n "actix-htmx") (v "0.3.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yicsmqaqh3dy9lwjnkn272cng1c086qll5nvj5vbqpmnpxb4nmq")))

