(define-module (crates-io ac ti actix-multipart-derive) #:use-module (crates-io))

(define-public crate-actix-multipart-derive-0.1.0 (c (n "actix-multipart-derive") (v "0.1.0") (d (list (d (n "actix-multipart-derive-impl") (r "^0.1") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)))) (h "1i6ybzma59kb3snms6hrigf3pgb1skcqkiy0p328zv8dwahnim0j")))

(define-public crate-actix-multipart-derive-0.6.0 (c (n "actix-multipart-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "parse-size") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rqhl525lv4ad20flgj2602sgj078im28c85z0y2b2nv6kr95i9f")))

(define-public crate-actix-multipart-derive-0.6.1 (c (n "actix-multipart-derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "parse-size") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0kdx7vpap6319jan3j5fwj1brj5rr31wfymlwl0gfsfq6vw7f2ha")))

