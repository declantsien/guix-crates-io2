(define-module (crates-io ac ti activeledger) #:use-module (crates-io))

(define-public crate-activeledger-0.1.0 (c (n "activeledger") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.24") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0sdx0l1zn7klwkbcc6c5rs40khv9akx2f32wzzhl5l3g9aksks6c")))

(define-public crate-activeledger-0.1.1 (c (n "activeledger") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.24") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0nqihb9n81p2x1kparyvcxrmpzl898wk2mrd66xg57hvvq1svphd")))

