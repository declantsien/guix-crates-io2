(define-module (crates-io ac ti actix-web-detached-jws-middleware) #:use-module (crates-io))

(define-public crate-actix-web-detached-jws-middleware-0.1.0 (c (n "actix-web-detached-jws-middleware") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.5.0") (d #t) (k 2)) (d (n "actix-rt") (r "^2.0.2") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "actix-web-buffering") (r "^0.1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "detached-jws") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.32") (d #t) (k 2)))) (h "1cm0rdjfnq5rpyi6k8nlp5xympg542zljx9vaaq5qsxsmfd8njkw")))

