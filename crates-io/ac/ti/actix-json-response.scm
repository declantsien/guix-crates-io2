(define-module (crates-io ac ti actix-json-response) #:use-module (crates-io))

(define-public crate-actix-json-response-0.1.0 (c (n "actix-json-response") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ywp2aib3rwhvi4dg9dwvxrfwy5sz83mlxw7rwpxcxvrmnpfvmkf")))

(define-public crate-actix-json-response-0.1.1 (c (n "actix-json-response") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jdsrm3ngp25cwp451fzqihs6lizfxfy1y7jxb22yjfxflqq051s")))

(define-public crate-actix-json-response-0.1.2 (c (n "actix-json-response") (v "0.1.2") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06w447d7911rb8b21m2cppq788s48jmwvyqyzgyasxn41l48zndw")))

(define-public crate-actix-json-response-0.1.3 (c (n "actix-json-response") (v "0.1.3") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ciawca2ag56dzr0x2h2235j4f7862lyx8r5zzlgjax1zlzvznkg")))

