(define-module (crates-io ac ti actix-middleware-macro) #:use-module (crates-io))

(define-public crate-actix-middleware-macro-0.1.0 (c (n "actix-middleware-macro") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^2.9.6") (d #t) (k 2)))) (h "0f9g14fr7isf3hh4cbp57ppz7vy0cj1c5x5h1ikj0clpq75wk5n6")))

