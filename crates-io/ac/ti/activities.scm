(define-module (crates-io ac ti activities) #:use-module (crates-io))

(define-public crate-activities-0.1.0 (c (n "activities") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "isolang") (r "^1.0.0") (f (quote ("serde_serialize"))) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "00qmv1nvc7cxk3xx1ajql830fp2m5jsgrzqikdyxl5ch75v9vmbq")))

