(define-module (crates-io ac ti actix-web-extras) #:use-module (crates-io))

(define-public crate-actix-web-extras-0.1.0 (c (n "actix-web-extras") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.6") (d #t) (k 2)) (d (n "actix-service") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1sl1fw2ks30bh6za5w9wasng8gp1r566pcjl5qafswcv9qizl7gg")))

