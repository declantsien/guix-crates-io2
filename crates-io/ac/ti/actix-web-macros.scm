(define-module (crates-io ac ti actix-web-macros) #:use-module (crates-io))

(define-public crate-actix-web-macros-0.1.0 (c (n "actix-web-macros") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-web") (r "^3.0.0-alpha.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "09dvjs01s5jfzc2wyjcvrz9akryh3gw3r8w1hdjfn6in9dg7zv6m")))

