(define-module (crates-io ac ti actix-sitemaps-rs) #:use-module (crates-io))

(define-public crate-actix-sitemaps-rs-0.1.0 (c (n "actix-sitemaps-rs") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.5") (d #t) (k 0)) (d (n "actix-web") (r "^4.5.1") (f (quote ("macros"))) (k 0)))) (h "0wjxd1mzi25pfrgdxdgv7j7fy6b66rzvricgjk3vmpyznnk2ygnf")))

