(define-module (crates-io ac ti action-io) #:use-module (crates-io))

(define-public crate-action-io-0.0.2 (c (n "action-io") (v "0.0.2") (h "03qjnp024srjy5ycxc9wjscxc0mdxc68zns5padcxyy0hqy31bcy") (f (quote (("default"))))))

(define-public crate-action-io-0.0.3 (c (n "action-io") (v "0.0.3") (h "0a5z2bdn52dywk4snysskpk8zmvkff5zkfvck8wbjjcgz3ri8kf0") (f (quote (("default"))))))

