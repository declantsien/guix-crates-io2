(define-module (crates-io ac ti actix-type-handler) #:use-module (crates-io))

(define-public crate-actix-type-handler-0.1.0 (c (n "actix-type-handler") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0gxdz43jn0fz3ya5lzhjdy6qk7rpf1xv9a99j4mvk7vzdy4qiwa5")))

