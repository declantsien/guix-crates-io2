(define-module (crates-io ac ti actix-inertia) #:use-module (crates-io))

(define-public crate-actix-inertia-0.1.0 (c (n "actix-inertia") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-service") (r "^2.0.2") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cxhwnk1689g0w9krrh8mc0095ybb7fnby45zcfrqi2kz6gaycb9")))

