(define-module (crates-io ac ti actiondb) #:use-module (crates-io))

(define-public crate-actiondb-0.2.1 (c (n "actiondb") (v "0.2.1") (d (list (d (n "clap") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1n7ygnka454cz6sn0r353jbfchwbw5xvwy05ghx2hnxc9jfwm3a2")))

(define-public crate-actiondb-0.3.0 (c (n "actiondb") (v "0.3.0") (d (list (d (n "clap") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0qf2l1n536ql1p5xkjj9zpzczxhn78chckyp2n5s2n9s8wpwzk0y")))

(define-public crate-actiondb-0.3.1 (c (n "actiondb") (v "0.3.1") (d (list (d (n "clap") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1sbg6mzvs16p5n8hddcgpmyi8bj6rfyfpkvy7ddknl7lp26xsr11")))

(define-public crate-actiondb-0.4.0 (c (n "actiondb") (v "0.4.0") (d (list (d (n "clap") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0sxgz43axcaszgkdm8vgsvj6ly9qs85ql3vvhw8xnnhv2xdwsb22")))

(define-public crate-actiondb-0.6.0 (c (n "actiondb") (v "0.6.0") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "130ihzdfw86q9mhx5473di19a5b7vxv66yw29p10k6xp4wmb5xlj")))

(define-public crate-actiondb-0.6.1 (c (n "actiondb") (v "0.6.1") (d (list (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1g1fjv9rcarnfkjxqqxfpqwgrdnsjibvb05l3wmzyqsxyyp177hb")))

(define-public crate-actiondb-0.7.0 (c (n "actiondb") (v "0.7.0") (d (list (d (n "clap") (r "^2.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0amim1vgwgafyhyc257h425l4i1xd4g1vj71666cmmy228079wdq") (f (quote (("nightly" "clippy") ("default"))))))

