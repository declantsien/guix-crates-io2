(define-module (crates-io ac ti actix-ws-ng) #:use-module (crates-io))

(define-public crate-actix-ws-ng-0.2.5 (c (n "actix-ws-ng") (v "0.2.5") (d (list (d (n "actix-codec") (r "^0.5.0") (d #t) (k 0)) (d (n "actix-http") (r "^3.0") (f (quote ("ws"))) (k 0)) (d (n "actix-web") (r "^4.0") (k 0)) (d (n "bytestring") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0vm9g0l604pa08sa3a6mlcyggg887pgcbk4bl8ia2850bafdaix0") (y #t)))

(define-public crate-actix-ws-ng-0.3.0 (c (n "actix-ws-ng") (v "0.3.0") (d (list (d (n "actix-codec") (r "^0.5.0") (d #t) (k 0)) (d (n "actix-http") (r "^3.0") (f (quote ("ws"))) (k 0)) (d (n "actix-web") (r "^4.0") (k 0)) (d (n "bytestring") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0pdrqrv0jk009y306lqavz65yxp2anfn54gv1ylhzl7l0q06pc8l") (r "1.68")))

