(define-module (crates-io ac ti actix-protobuf) #:use-module (crates-io))

(define-public crate-actix-protobuf-0.1.0 (c (n "actix-protobuf") (v "0.1.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.4") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 2)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 2)))) (h "1g5c8y1gdd4llb4391yknp2z4gw6wphkg5r3311qyna7fvfxjvpk")))

(define-public crate-actix-protobuf-0.2.0 (c (n "actix-protobuf") (v "0.2.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 2)) (d (n "prost") (r "^0.2") (d #t) (k 0)) (d (n "prost-derive") (r "^0.2") (d #t) (k 2)))) (h "055d9n8vnx59ds33hx98cqr452qd1cp98rwh36da55grga04fxgr")))

(define-public crate-actix-protobuf-0.4.0 (c (n "actix-protobuf") (v "0.4.0") (d (list (d (n "actix") (r "^0.8.1") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 2)) (d (n "prost") (r "^0.4") (d #t) (k 0)) (d (n "prost-derive") (r "^0.4") (d #t) (k 2)))) (h "1ng47d3mnqw976px06mipn35qhmi90p2373ah5s1qsh72gss69n6")))

(define-public crate-actix-protobuf-0.4.1 (c (n "actix-protobuf") (v "0.4.1") (d (list (d (n "actix") (r "^0.8.1") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-rc") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 2)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.5.0") (d #t) (k 2)))) (h "1w6pw75xm4bjrwa8dh1kad93nzw1an8q2p95kyvf4j58yx8x2vba")))

(define-public crate-actix-protobuf-0.5.0 (c (n "actix-protobuf") (v "0.5.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.6.0") (d #t) (k 2)))) (h "087jdnjxqjgx0n579flgpw3dqp0z2bycpi0n4c5djlgg0gz59zmk")))

(define-public crate-actix-protobuf-0.5.1 (c (n "actix-protobuf") (v "0.5.1") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.6.0") (d #t) (k 2)))) (h "1kzg8vbj6589ixy63r53lrp440sm1ydqpg1ri6lqs41jii9qdryc")))

(define-public crate-actix-protobuf-0.6.0-alpha.1 (c (n "actix-protobuf") (v "0.6.0-alpha.1") (d (list (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0-alpha.3") (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "prost") (r "^0.6.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.6.0") (d #t) (k 2)))) (h "0x3vy5hldf8ah0cr3ac6y1cmmkyxb9jr6pcngilyarp0bwy3dqvk")))

(define-public crate-actix-protobuf-0.6.0 (c (n "actix-protobuf") (v "0.6.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0") (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (k 0)) (d (n "prost") (r "^0.6.0") (d #t) (k 0)))) (h "0rv9qrwkvfj4dsrrmg1clbks9bxjbi4bc66jh22jffprn8ajcdmg")))

(define-public crate-actix-protobuf-0.7.0-beta.1 (c (n "actix-protobuf") (v "0.7.0-beta.1") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)))) (h "1aq614p2d07ik3apw2payq1j9qhy1m6ha44k568d367wzi7k94mm")))

(define-public crate-actix-protobuf-0.7.0-beta.2 (c (n "actix-protobuf") (v "0.7.0-beta.2") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.10") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.8") (k 0)))) (h "116ihalhwzxgcg9xkcbzykivgryfxhryvgj7r187fanki0r9kyfy")))

(define-public crate-actix-protobuf-0.7.0-beta.3 (c (n "actix-protobuf") (v "0.7.0-beta.3") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.14") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.8") (k 0)))) (h "14bjagfb6jqfxx0iamlg5kmzzbq2jld1zl7d07xr14rzlz6dwmr4")))

(define-public crate-actix-protobuf-0.7.0-beta.4 (c (n "actix-protobuf") (v "0.7.0-beta.4") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.17") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.8") (k 0)))) (h "09dsr1p2rsza4qr243ifpvprrsk4jjzqf97azz127qzfx3imr032")))

(define-public crate-actix-protobuf-0.7.0-beta.5 (c (n "actix-protobuf") (v "0.7.0-beta.5") (d (list (d (n "actix-web") (r "^4.0.0-rc.1") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.9") (k 0)))) (h "1b1vflprv2dnx986p0r6jsz9l7mjjq01xsxajxqksw255qcfchn1")))

(define-public crate-actix-protobuf-0.7.0 (c (n "actix-protobuf") (v "0.7.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.9") (k 0)))) (h "10jbvhmicd3729wwrqqdllwcshy56ddmng2y94vvbra295036qrn")))

(define-public crate-actix-protobuf-0.8.0 (c (n "actix-protobuf") (v "0.8.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.10") (k 0)))) (h "1lhsz1wqb1gajllipwd25g20dc5islx4g7hirq0hwb6j4rjsi5ak")))

(define-public crate-actix-protobuf-0.9.0 (c (n "actix-protobuf") (v "0.9.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.11") (k 0)))) (h "0kx1wgxfii278qspagcpjv9vdpzsrvjf7j57bl8k0pqxvwf1bqfj")))

(define-public crate-actix-protobuf-0.10.0 (c (n "actix-protobuf") (v "0.10.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "prost") (r "^0.12") (k 0)))) (h "0kdjyn335qzknmi2a272gg08fa13jamsin91dz519f2hz2bd4kd1") (r "1.68")))

