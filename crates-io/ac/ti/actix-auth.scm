(define-module (crates-io ac ti actix-auth) #:use-module (crates-io))

(define-public crate-actix-auth-0.1.0 (c (n "actix-auth") (v "0.1.0") (d (list (d (n "actix-identity") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1d9lr1rpvkhcmkx0ifssabx9y36iicfhn86zzrcnv8h4vsqnhy02")))

