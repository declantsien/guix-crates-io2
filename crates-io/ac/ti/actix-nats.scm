(define-module (crates-io ac ti actix-nats) #:use-module (crates-io))

(define-public crate-actix-nats-0.3.0 (c (n "actix-nats") (v "0.3.0") (d (list (d (n "actix") (r "~0.7.1") (d #t) (k 0)) (d (n "nats") (r "~0.3.0") (d #t) (k 0)))) (h "0kpj93y33ifnzp00lzbzk74a0463z5x02x8n2azqyxy4wv0l0kv4")))

(define-public crate-actix-nats-0.4.0 (c (n "actix-nats") (v "0.4.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "nitox") (r "^0.1") (d #t) (k 0)))) (h "1p782ywh18xm872xqvkhpw1zaqa471l9r73j18cbs2wvznbrpkgz")))

(define-public crate-actix-nats-0.4.1 (c (n "actix-nats") (v "0.4.1") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nitox") (r "^0.1") (d #t) (k 0)))) (h "19w53csdxbiw206c1i594dpcavxa7lfrkfsv55mivvhyzgjxrg23")))

