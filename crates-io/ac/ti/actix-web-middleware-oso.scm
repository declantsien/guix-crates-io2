(define-module (crates-io ac ti actix-web-middleware-oso) #:use-module (crates-io))

(define-public crate-actix-web-middleware-oso-0.1.0 (c (n "actix-web-middleware-oso") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (k 0)) (d (n "actix-web") (r "^4.0.1") (f (quote ("macros"))) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.7") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "oso") (r "^0.26.0") (d #t) (k 0)))) (h "1wd61pz2bmppnk3ij1098ancxl5c83hai5xglcn4rbnjpz5af1cq")))

