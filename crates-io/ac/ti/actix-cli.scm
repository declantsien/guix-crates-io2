(define-module (crates-io ac ti actix-cli) #:use-module (crates-io))

(define-public crate-actix-cli-0.1.0 (c (n "actix-cli") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0dr0lnq71yzi2xd820cr4ymp1fqfwndb8vg3ghvcdnsgk68anm6g")))

