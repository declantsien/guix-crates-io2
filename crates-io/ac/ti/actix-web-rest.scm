(define-module (crates-io ac ti actix-web-rest) #:use-module (crates-io))

(define-public crate-actix-web-rest-0.1.0 (c (n "actix-web-rest") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-rest-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xi9ja4nbk450brvcwy390k7nh41c2q1hxk0rdcrry8sna4ywn4z")))

(define-public crate-actix-web-rest-0.2.0 (c (n "actix-web-rest") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-rest-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "09k06qn8s1nm4zbprsg1grsmfmk4l45rmfsyi1ayikhx962baaca")))

(define-public crate-actix-web-rest-0.3.0 (c (n "actix-web-rest") (v "0.3.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-rest-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "utoipa") (r "^3") (d #t) (k 0)))) (h "0pkqgjskrmppm01wxwavfqs2z7lwz88w5w0n9iraxgj9268nsd2i")))

