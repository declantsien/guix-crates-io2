(define-module (crates-io ac ti action-artifact) #:use-module (crates-io))

(define-public crate-action-artifact-0.0.2 (c (n "action-artifact") (v "0.0.2") (h "0c4il1cgxx4jwva9rsc470z05xvzrrhnixgdw2r4wdv1hf352jv4") (f (quote (("default"))))))

(define-public crate-action-artifact-0.0.3 (c (n "action-artifact") (v "0.0.3") (h "0mlnhkvzh3a6zv8mi754vlvr80km6m5cxvrqrnm2n5pd1madcja6") (f (quote (("default"))))))

