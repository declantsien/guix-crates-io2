(define-module (crates-io ac ti actix_derive) #:use-module (crates-io))

(define-public crate-actix_derive-0.0.1 (c (n "actix_derive") (v "0.0.1") (d (list (d (n "actix") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1vc2pgnzcik9lp1wvivxnc90l7vwk5apybdn2n6y9v2854x4fc3s")))

(define-public crate-actix_derive-0.0.2 (c (n "actix_derive") (v "0.0.2") (d (list (d (n "actix") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "18ip8r5b5qrsyjbnild55dy4ir0p94iyxsgxrgapj38i34dxyd09")))

(define-public crate-actix_derive-0.1.0 (c (n "actix_derive") (v "0.1.0") (d (list (d (n "actix") (r "^0.3.4") (d #t) (k 1)) (d (n "actix") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0dxb2a9x047r124bww6gg2zfrnl14bai36a3g092rvaj3nsba056")))

(define-public crate-actix_derive-0.1.1 (c (n "actix_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0lfrgm692wzk8k7jrzplzfkf9rr5a133z583pwvjy9jmsljwii01")))

(define-public crate-actix_derive-0.2.0 (c (n "actix_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1wlh6nr8fxs55q7j4zynzfllz06yby3sn7m3m2ksrfal4s9drcf4")))

(define-public crate-actix_derive-0.3.0 (c (n "actix_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "14mvw9jpsy0k6a7wfk6b59wmvaywazhsqgdry0hy1ra5xwjib7av")))

(define-public crate-actix_derive-0.3.1 (c (n "actix_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "14gy3dlimpri3jh0g194vl3syq4k7n45jqi00jgmfzxx2mnphxny")))

(define-public crate-actix_derive-0.3.2 (c (n "actix_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0sdsnvgzx7i1jsqj7z60fc2hif4nxyhjnfnljgijlcjm2i1yj023")))

(define-public crate-actix_derive-0.4.0 (c (n "actix_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0mvq883aq5z6d0893bh32bfddvfajh5bm7nkl0l8idpzbzdx8b")))

(define-public crate-actix_derive-0.5.0 (c (n "actix_derive") (v "0.5.0") (d (list (d (n "actix") (r "^0.8.3") (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0k1kg4gkp2jhi5fgdfd0cq2qfbyy3gfgwqjrvzq1hzrjmynwwnmr")))

(define-public crate-actix_derive-0.6.0-beta.1 (c (n "actix_derive") (v "0.6.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gy1wjinh4yahq6alsagpkhxq3v00jblxpcf53ykxf1fb3r9qx5f")))

(define-public crate-actix_derive-0.6.0-beta.2 (c (n "actix_derive") (v "0.6.0-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07vqrxj791r06ji6qf8qgnb2lng88p5da7s4f3036mhqq8pxcis5")))

(define-public crate-actix_derive-0.6.0 (c (n "actix_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19rp2xcwqf5p4q5h6xxzb44xsfgpvvnnsis3l0dngnffw7zbhi3d")))

(define-public crate-actix_derive-0.6.1 (c (n "actix_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lzna349llpv6w10slc1fkzydrkhf3ypqlsarzj6i1bim7av6zbw") (r "1.68")))

