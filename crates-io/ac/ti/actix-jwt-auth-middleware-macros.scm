(define-module (crates-io ac ti actix-jwt-auth-middleware-macros) #:use-module (crates-io))

(define-public crate-actix-jwt-auth-middleware-macros-0.1.0 (c (n "actix-jwt-auth-middleware-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14nddyjxxlxpgrf0424q9i5ih81sshn1nnig97fcdh8lx3h5yg68") (y #t)))

