(define-module (crates-io ac ti activity-sys) #:use-module (crates-io))

(define-public crate-activity-sys-0.1.1 (c (n "activity-sys") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("console"))) (d #t) (k 0)))) (h "1ldxp90qaph2vh1xfmgfj9if3wkl3gbkjlwsakf6l7qj1gcy8lba")))

(define-public crate-activity-sys-0.1.2 (c (n "activity-sys") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("console"))) (d #t) (k 0)))) (h "1qbhrxhf6mx74b1nqrii9hf5m5w54iifsdgps0cmjylpwh8j4gqx")))

(define-public crate-activity-sys-0.1.3 (c (n "activity-sys") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.42") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("console"))) (d #t) (k 0)))) (h "0xqvbfalnzmyckqrfmcga3p1brch4qff1yymp5qbmayga8zc63sk")))

