(define-module (crates-io ac ti actix-multipart-test) #:use-module (crates-io))

(define-public crate-actix-multipart-test-0.0.1 (c (n "actix-multipart-test") (v "0.0.1") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "19yn5jbf1s1fdniyhq80v4s8icf8mkgm67n3z3zmyyas31mfc6i0")))

(define-public crate-actix-multipart-test-0.0.2 (c (n "actix-multipart-test") (v "0.0.2") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cpb7mpkis13653c08ahl9k2k8mnqr2ri5f3yif81pi0xarl32ah")))

(define-public crate-actix-multipart-test-0.0.3 (c (n "actix-multipart-test") (v "0.0.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pa4bd3lyyav9sx3m57la48vlacc0agsw7imi1kkv74b4swd68nn")))

