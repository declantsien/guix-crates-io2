(define-module (crates-io ac ti actix-easy-multipart-derive) #:use-module (crates-io))

(define-public crate-actix-easy-multipart-derive-0.1.0 (c (n "actix-easy-multipart-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0rdbd4xd9yrxfsaam2ca864jxrhyg0rsz93gjvpz1mib0dwmnpsr")))

(define-public crate-actix-easy-multipart-derive-0.2.0 (c (n "actix-easy-multipart-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1xbsjpls704fp5pa9v9kgs2xij508xcx2x0bbgyv7dhzjgp5dznr")))

(define-public crate-actix-easy-multipart-derive-3.0.0 (c (n "actix-easy-multipart-derive") (v "3.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0xp8ky3gp7pfrccpj374rj215nx9d6zdc0whr0g0535slh9a4qpk")))

