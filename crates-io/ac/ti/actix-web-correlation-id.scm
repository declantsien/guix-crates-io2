(define-module (crates-io ac ti actix-web-correlation-id) #:use-module (crates-io))

(define-public crate-actix-web-correlation-id-0.1.0 (c (n "actix-web-correlation-id") (v "0.1.0") (d (list (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1psak7qzkl2cc532p7pblmxadj1ck94k5a9qwfi6mz3xv2wxzdr7")))

(define-public crate-actix-web-correlation-id-1.0.0 (c (n "actix-web-correlation-id") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "awc") (r "^3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p4iygijbj99w65sv57k6r02522wd3zwi6hz4ri2wnp9k33sg6mw") (f (quote (("logger") ("default" "logger")))) (s 2) (e (quote (("client" "dep:awc"))))))

