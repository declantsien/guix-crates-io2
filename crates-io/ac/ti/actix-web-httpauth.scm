(define-module (crates-io ac ti actix-web-httpauth) #:use-module (crates-io))

(define-public crate-actix-web-httpauth-0.0.1 (c (n "actix-web-httpauth") (v "0.0.1") (d (list (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)))) (h "1wii3czx39g71jdv3qsyln6c6i3pkmgx8d0lcxq1b2439d47pzrh")))

(define-public crate-actix-web-httpauth-0.0.2 (c (n "actix-web-httpauth") (v "0.0.2") (d (list (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)))) (h "0q9igcf34mx680wmyllyl5ny080rpjp4zcgv3d7jinpws08zljf8")))

(define-public crate-actix-web-httpauth-0.0.3 (c (n "actix-web-httpauth") (v "0.0.3") (d (list (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1gyyg91wlan8qfsicr89yzzfmvq6zd05mdv9a4lskivnndlaxpan") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.0.4 (c (n "actix-web-httpauth") (v "0.0.4") (d (list (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0858x0jyy36frhnqf287cskwqa7wj8qrms6bwrxjh8awvw1dg8wy") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.1.0 (c (n "actix-web-httpauth") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "19zk1z4ahyvhfkj5cq161n3yld4qnpkya9hb3i8x40lxdcp8ng44") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.2.0 (c (n "actix-web-httpauth") (v "0.2.0") (d (list (d (n "actix-web") (r "^0.7") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0gwnlr6f8a818rlkg4rf2a53bryclcildncxwan5325nscdzmmf0") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.3.0-alpha.1 (c (n "actix-web-httpauth") (v "0.3.0-alpha.1") (d (list (d (n "actix-service") (r "^0.4.0") (d #t) (k 2)) (d (n "actix-web") (r "^1.0.0-beta.5") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)))) (h "1ar5919w60vc384avv63kn3qv2whhlr52kwg70zhss84yskn33pp") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.3.0-alpha.2 (c (n "actix-web-httpauth") (v "0.3.0-alpha.2") (d (list (d (n "actix-service") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-beta.5") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0liwrwq68c0lybb092jm7hwg7ficahl98n9abq55xhqib39zqncz") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.3.0 (c (n "actix-web-httpauth") (v "0.3.0") (d (list (d (n "actix-service") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^1.0") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "04nffmdzj1ifr4pzawccz39k3hrvlgwkzwdais0q7fqkiqzx503f") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-actix-web-httpauth-0.3.1 (c (n "actix-web-httpauth") (v "0.3.1") (d (list (d (n "actix-service") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^1.0") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-locks") (r "^0.3.3") (d #t) (k 0)))) (h "0s5hcv3f4w81db6jm4h9w8w0q26s45xwlp3qfxb31xypg1gb78r7") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.3.2 (c (n "actix-web-httpauth") (v "0.3.2") (d (list (d (n "actix-service") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^1.0") (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-locks") (r "^0.3.3") (d #t) (k 0)))) (h "163kxps7k10i6aivgfwlm22bpziq3p58qihnc34538857l1haw0f") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.4.0 (c (n "actix-web-httpauth") (v "0.4.0") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (k 0)) (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1v3kmlgwbwdshi9qya10fid3kr4h9cw6dx1qam3w1j4fr3xcrk3w") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.4.1 (c (n "actix-web-httpauth") (v "0.4.1") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (k 0)) (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "00vs0jp5rihwqg4nn8hzl1cn9shgi5sy7k6f64vz1y7kjvmc9np0") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.4.2 (c (n "actix-web-httpauth") (v "0.4.2") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (k 0)) (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "11a2i7gyis3rnkcpcdfhvcvl89gn2yk3pg5zfjp08g8i7qx5p8hf") (f (quote (("nightly") ("default"))))))

(define-public crate-actix-web-httpauth-0.5.0 (c (n "actix-web-httpauth") (v "0.5.0") (d (list (d (n "actix-service") (r "^1.0.6") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0") (k 0)) (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "08hvlimhlac8kha2vj50b69n1mwfychrcmiz9pkb7hn5czbpasjk")))

(define-public crate-actix-web-httpauth-0.5.1 (c (n "actix-web-httpauth") (v "0.5.1") (d (list (d (n "actix-web") (r "^3.0.0") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)))) (h "0n335p43p13f9k34ri87ym4mb2wrcv63igfq1ybzgwrxgah12fqc")))

(define-public crate-actix-web-httpauth-0.6.0-beta.1 (c (n "actix-web-httpauth") (v "0.6.0-beta.1") (d (list (d (n "actix-service") (r "^2.0.0-beta.5") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.5") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)))) (h "19ang31nzjsjkqml8d7ka5d2i9vskwrn489sbqgvc6sbsbbfwhvr")))

(define-public crate-actix-web-httpauth-0.6.0-beta.2 (c (n "actix-web-httpauth") (v "0.6.0-beta.2") (d (list (d (n "actix-service") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)))) (h "0n7p1x05aaqcg6il142fiigi2dwqj71m8rf9mwy4jncdd6s0wk96")))

(define-public crate-actix-web-httpauth-0.6.0-beta.3 (c (n "actix-web-httpauth") (v "0.6.0-beta.3") (d (list (d (n "actix-service") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.10") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)))) (h "0i4wjgc6whhba5cf56cnd0d9q8sv4an5b4injzpkx5d88s0mnw45")))

(define-public crate-actix-web-httpauth-0.6.0-beta.4 (c (n "actix-web-httpauth") (v "0.6.0-beta.4") (d (list (d (n "actix-service") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.10") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0yzl0sd56hgmfrjs63iyp1408z1ldd79w06hknc61zpknibgi0jl")))

(define-public crate-actix-web-httpauth-0.6.0-beta.5 (c (n "actix-web-httpauth") (v "0.6.0-beta.5") (d (list (d (n "actix-service") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.14") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0hkmjvh7dilm9x3jb5i4ywdq9va5asj6p6xh7bjdgvqm35nz6bnb")))

(define-public crate-actix-web-httpauth-0.6.0-beta.6 (c (n "actix-web-httpauth") (v "0.6.0-beta.6") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.15") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "11nm3cqq5awbdr6d8al9xg0sy57i3mz38mahdcqpz9sbwlmynakl")))

(define-public crate-actix-web-httpauth-0.6.0-beta.7 (c (n "actix-web-httpauth") (v "0.6.0-beta.7") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.17") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "192bqxx89kwklm0bsjwd8m5d14536g5hlsacv5awxxg0mqmk7zwm")))

(define-public crate-actix-web-httpauth-0.6.0-beta.8 (c (n "actix-web-httpauth") (v "0.6.0-beta.8") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-rc.1") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0pvs6ww57rr0ppkmvviffh5nyrba3q956z5nl4c5sdmb18ig466q")))

(define-public crate-actix-web-httpauth-0.6.0 (c (n "actix-web-httpauth") (v "0.6.0") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "1rnvzih4zzn009fsfpw30ncpwkszirl1mkc305990kv8ni45mhh8")))

(define-public crate-actix-web-httpauth-0.7.0 (c (n "actix-web-httpauth") (v "0.7.0") (d (list (d (n "actix-service") (r "^2") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "03fl8f9a62i3ab0n4svw0hdp06bqsydmxhchzswlhdcby4ii3p56")))

(define-public crate-actix-web-httpauth-0.8.0 (c (n "actix-web-httpauth") (v "0.8.0") (d (list (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.1") (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "14892yxwfxqfsa2fki6fv96dpksi35r4yccgxb99lfmw0k7n5nkd")))

(define-public crate-actix-web-httpauth-0.8.1 (c (n "actix-web-httpauth") (v "0.8.1") (d (list (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4.1") (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)))) (h "0xd7l1jx0x0ivjx1svyifxm6f6sfw47x6hf9cilcqb5413gkwq8x") (r "1.68")))

