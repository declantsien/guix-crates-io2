(define-module (crates-io ac ti active_standby) #:use-module (crates-io))

(define-public crate-active_standby-0.1.0 (c (n "active_standby") (v "0.1.0") (h "0qgp1v60gh3hs2caqc04y4pjicz264a9hlfd9gax49kyrz5slk2i")))

(define-public crate-active_standby-0.2.0 (c (n "active_standby") (v "0.2.0") (h "0hcngf82hi8z6skn6g6a48g48bhf7izld9vkhlpl27yjpfimk5xz")))

(define-public crate-active_standby-0.2.1 (c (n "active_standby") (v "0.2.1") (h "0cz3bwnz1sa57g3jsjgb36bypmnb4wx9zmcvzypi7b37lywha1sn")))

(define-public crate-active_standby-0.3.0 (c (n "active_standby") (v "0.3.0") (h "0mc8clc1bfx189hh9w78y7h10syx3sqdgygrwz5jrsyw93rjbhrj")))

(define-public crate-active_standby-0.3.1 (c (n "active_standby") (v "0.3.1") (h "0zanz9mzb6c0affcb7n0w53drkfbazpmgg1mhgc2vqkg5a7a9hxq")))

(define-public crate-active_standby-0.4.0 (c (n "active_standby") (v "0.4.0") (h "14vybiva50mn2fcmbklw972mrgbd9y2ag5l1zrsk3y1phw5nbmkn")))

(define-public crate-active_standby-0.4.1 (c (n "active_standby") (v "0.4.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "062p75qx3fk520d7chl2gmwknajjx72w4ra9rq6zqj3mc21dm1jk")))

(define-public crate-active_standby-0.4.2 (c (n "active_standby") (v "0.4.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "1w8j0vd9fkgxwfxj6mlbx4msklv1h5vp0qi5iakpdsbm3wx1qqrz")))

(define-public crate-active_standby-0.4.3 (c (n "active_standby") (v "0.4.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "1r28sni0p71jvwv5w5sy5960nb43xrxcww9jpk600y0yrn63xiqf")))

(define-public crate-active_standby-0.5.0 (c (n "active_standby") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "0bg9qanc3q2x6lj7b87if1zcqcgi19vmhhkgy3nba6nir47bzp5d")))

(define-public crate-active_standby-0.5.1 (c (n "active_standby") (v "0.5.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "10pvvjncg4pn9i1m5mmnr7qhjr8dkyjy55cj9jga3d2aghnhn14a")))

(define-public crate-active_standby-0.5.2 (c (n "active_standby") (v "0.5.2") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)))) (h "0arg4j4cqyvwnp1dg3b3ni2ysyhchwybf51kz4hxhc5ins88lxzj")))

(define-public crate-active_standby-0.6.0 (c (n "active_standby") (v "0.6.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0b96ah9wdwa3lpbf56pa25g7v9wxkz6laj3nhyx4k4fa6lkd8s63")))

(define-public crate-active_standby-0.6.1 (c (n "active_standby") (v "0.6.1") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1vr24mw6wmjk57s8vz5adkk4zpwdm562f4rjdj82jly28nnfr2pa")))

(define-public crate-active_standby-0.6.3 (c (n "active_standby") (v "0.6.3") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1v0by5bqqcq0jsgfyyhc5884wpgn7372qf4iivcm4zk77ac9b2q4")))

(define-public crate-active_standby-0.7.0 (c (n "active_standby") (v "0.7.0") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1ihhqiqgmd5km4ilhmp9qb4wi96jq3vlbrc32yasygcdnh0d7xvn")))

(define-public crate-active_standby-0.8.0 (c (n "active_standby") (v "0.8.0") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "04hax2i2za18g96zdap3fb4sin074nqymhzcs7nnd5qgk11ly4nl")))

(define-public crate-active_standby-0.8.1 (c (n "active_standby") (v "0.8.1") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0f3m0c78z6v79y862f43443v6fg6rc7a1vgm3k8aycaq1nq5f8rs")))

(define-public crate-active_standby-0.8.2 (c (n "active_standby") (v "0.8.2") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0yax2h566cdcgg2gh3kv97f8max94w00xn66rv1dkhxfhm23gfbp")))

(define-public crate-active_standby-0.8.3 (c (n "active_standby") (v "0.8.3") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0y00kff167gdr308nm6jrwm257giyx7c543x50fx25bdyv9nnb6m")))

(define-public crate-active_standby-0.8.4 (c (n "active_standby") (v "0.8.4") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1v755ngwfpqqlf3yhb4b14v1qw4n325372va7wiqqd47q1fbhl7x")))

(define-public crate-active_standby-0.8.5 (c (n "active_standby") (v "0.8.5") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0xk5gxj2q8r238j2wyjrxr59g9fflzghpp45nsq55cy76x2as8gg")))

(define-public crate-active_standby-0.9.0-rc.1 (c (n "active_standby") (v "0.9.0-rc.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "00kyj3mlq3gsmxbnlkw25lcxbggrjlkqyj47pw01s85400l2pn32")))

(define-public crate-active_standby-0.9.0 (c (n "active_standby") (v "0.9.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1lxfvcv14kja3gm7prhxdp6hwg169bddzzi9x949by5n568k4mr5")))

(define-public crate-active_standby-0.9.1 (c (n "active_standby") (v "0.9.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "11r979xhwpxx83q58p1hg3vqnn86bg5abnlrij10h0r4zjwaah9h")))

(define-public crate-active_standby-0.9.2 (c (n "active_standby") (v "0.9.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "0zji1ksr0p52awiy2v8yac4fxiij0kvk8jz9agx8y07a1yyrbijl")))

(define-public crate-active_standby-1.0.0 (c (n "active_standby") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "18w355yan9azs2xazyiirhs4g0iqbyxfwrj3hxayripz94hmwz7n")))

(define-public crate-active_standby-2.0.0 (c (n "active_standby") (v "2.0.0") (d (list (d (n "loom") (r "^0.5.2") (d #t) (t "cfg(loom)") (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.3") (d #t) (k 0)))) (h "1pv1by48fjsdbv91kxhvfrjz3zpyycppsi4csrnipxhbpx58mni6")))

