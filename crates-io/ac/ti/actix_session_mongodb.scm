(define-module (crates-io ac ti actix_session_mongodb) #:use-module (crates-io))

(define-public crate-actix_session_mongodb-0.1.0 (c (n "actix_session_mongodb") (v "0.1.0") (d (list (d (n "actix-session") (r "^0.9.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bson") (r "^2.9.0") (f (quote ("chrono-0_4"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde"))) (d #t) (k 0)))) (h "0a18nvxlbd4l2w5bnrhi1j4n1kwr3ci7sv4q40b8z6yygyfpx87y") (r "1.63")))

