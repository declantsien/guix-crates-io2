(define-module (crates-io ac ti actix-permissions) #:use-module (crates-io))

(define-public crate-actix-permissions-0.1.0-beta.1 (c (n "actix-permissions") (v "0.1.0-beta.1") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1npwwgisgma0rlb2d5avi3lvf8ww556l7zfx53jc8d3iqklk0ms1")))

(define-public crate-actix-permissions-0.1.0 (c (n "actix-permissions") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16qz7s7f2vi7pva4waq3fswgr2j84h0qqy0lbb0nxqcxrdmfrmg8")))

(define-public crate-actix-permissions-0.1.1 (c (n "actix-permissions") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.7") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lqj1mjsci8hffdaxz40wxapb7jbp74dqhja1xzglyzapbgbaq72")))

(define-public crate-actix-permissions-1.0.0 (c (n "actix-permissions") (v "1.0.0") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c0pd3a2ixzyxgs8n1b33ryb4rdq4calhv2lxv1q4cw7n4i4y36r")))

(define-public crate-actix-permissions-1.0.1 (c (n "actix-permissions") (v "1.0.1") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nnbx0f6qv2x6wij4x8ksrqwi57qx9kz3wvcpcsgfh4iwqmhbm2j")))

(define-public crate-actix-permissions-1.0.2 (c (n "actix-permissions") (v "1.0.2") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cc7z1d3ycfak5rbk0z74y5q2skhqlqx7yzgimqs1hzx7lxd3i7g")))

(define-public crate-actix-permissions-2.0.0-beta.1 (c (n "actix-permissions") (v "2.0.0-beta.1") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lmvbfccjlp44syqbj9y9h6ymf5553w65i28zk99spckgvn1sc75")))

(define-public crate-actix-permissions-2.0.0-beta.2 (c (n "actix-permissions") (v "2.0.0-beta.2") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "160wldbpzhws987cvn0fgz65gxvg813smab4swdz6918ai4a1y4p")))

(define-public crate-actix-permissions-2.0.0-beta.3 (c (n "actix-permissions") (v "2.0.0-beta.3") (d (list (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "001dkfy9mf5kcx99yhq5wlj5jnv39q3ck9irndyadjr3p2654kj7")))

(define-public crate-actix-permissions-2.0.0-beta.4 (c (n "actix-permissions") (v "2.0.0-beta.4") (d (list (d (n "actix-http") (r "^3.0.0-beta.18") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ix9dkvk9rrwg5mj3d5n5hxyqkvx739rd49p6267qgs5c3gxxi4q")))

(define-public crate-actix-permissions-2.0.0 (c (n "actix-permissions") (v "2.0.0") (d (list (d (n "actix-http") (r "^3.0.0-beta.18") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hsigdl97y2pmr92rk282gscixyms3glbmh93xj7w9kggm8r5q07")))

