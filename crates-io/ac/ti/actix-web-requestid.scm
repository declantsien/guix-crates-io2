(define-module (crates-io ac ti actix-web-requestid) #:use-module (crates-io))

(define-public crate-actix-web-requestid-0.1.0 (c (n "actix-web-requestid") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "07cdjsx0w66lh1xpxkfa7b2wpmwi18xqikm1ilc16ywshznz2spi")))

(define-public crate-actix-web-requestid-0.1.1 (c (n "actix-web-requestid") (v "0.1.1") (d (list (d (n "actix-web") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0c2ijvxzj7006l3v0hqdqch4zvvjvjpphj52d68a559d4bmbvhk0")))

(define-public crate-actix-web-requestid-0.1.2 (c (n "actix-web-requestid") (v "0.1.2") (d (list (d (n "actix-web") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0v0hmwq4vdyfvy31p0040dwqld7jbv0g2mx3bfxsfv952qgfvl3y")))

(define-public crate-actix-web-requestid-0.2.0 (c (n "actix-web-requestid") (v "0.2.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0znsmln6ksd3vq55scl4917yi5yhz9nd05k28kaa2nxbipp9dhnl")))

(define-public crate-actix-web-requestid-0.3.0 (c (n "actix-web-requestid") (v "0.3.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-web") (r "^3.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0cryx498s62qn61z57j3w7da4k3fdc1bjrbjkiq0gv1w5w7m87zp")))

(define-public crate-actix-web-requestid-0.4.0 (c (n "actix-web-requestid") (v "0.4.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hlzhjl45zk81hs11nzbf6bz5k088xhgv5445xwhcvrhrgiammyi")))

(define-public crate-actix-web-requestid-1.0.0 (c (n "actix-web-requestid") (v "1.0.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1xgl22pxifn157v5gk2rfp9zaprc2rh2lhhayc8wq7ah3zfl24g1")))

(define-public crate-actix-web-requestid-1.1.0 (c (n "actix-web-requestid") (v "1.1.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0x1kw2qm5v4l69cpzwjrp1kp1m6zq7z86ffp3l9dc9qfiylfrhgd")))

(define-public crate-actix-web-requestid-2.0.0-beta.1 (c (n "actix-web-requestid") (v "2.0.0-beta.1") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "043k7jyb7psj81mlcm5cw8mdgzb6p3cmaf32mbv58csdq94wrvw0")))

(define-public crate-actix-web-requestid-2.1.0 (c (n "actix-web-requestid") (v "2.1.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2.0") (d #t) (k 2)))) (h "0fvzqn6c5if9v8p3njygz3izm9lbdg0mqqhmd4a0648392jwnxqz")))

