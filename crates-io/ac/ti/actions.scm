(define-module (crates-io ac ti actions) #:use-module (crates-io))

(define-public crate-actions-0.0.1 (c (n "actions") (v "0.0.1") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "1xscgq59al48d0c6r4cx29bxc46dyz86wn5zdr762x3czq0qjm6s")))

(define-public crate-actions-0.1.0 (c (n "actions") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "1cld5l9ck2f6halb0641mlk8wn62ypaz0zxrw9adg7vavq7svmpk")))

(define-public crate-actions-0.2.0 (c (n "actions") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0rjg7my91qpnjg14apwb0nxyqgrxg2l6vgl2q2mnrk68gmvnfkri")))

