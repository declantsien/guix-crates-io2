(define-module (crates-io ac ti actix-helper-macros) #:use-module (crates-io))

(define-public crate-actix-helper-macros-0.1.0 (c (n "actix-helper-macros") (v "0.1.0") (d (list (d (n "actix-proc-macros") (r "~0.1") (d #t) (k 0)) (d (n "actix-web") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)))) (h "0lpza343qvdg4cl7i8rnsja1j1s9i3nl6vqrv3g1z6vjshja490b")))

(define-public crate-actix-helper-macros-0.2.0 (c (n "actix-helper-macros") (v "0.2.0") (d (list (d (n "actix-proc-macros") (r "~0.1") (d #t) (k 0)) (d (n "actix-web") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)))) (h "04wpn6l6xlah45bnql36jcfp1mc0my584kvc1vp11swmps900h4w")))

(define-public crate-actix-helper-macros-0.2.1 (c (n "actix-helper-macros") (v "0.2.1") (d (list (d (n "actix-proc-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "actix-web") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)))) (h "0hwaxaqpb5mj062aarqdnjcxfykpkmzk3ql8fwmlkygxq4cvybsq")))

(define-public crate-actix-helper-macros-0.2.3 (c (n "actix-helper-macros") (v "0.2.3") (d (list (d (n "actix-proc-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "actix-web") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)))) (h "0s9dczv674fpvlqra86xhxgybqjkg3rbd8hakd41c0fbkkvaj5wq")))

(define-public crate-actix-helper-macros-0.3.0 (c (n "actix-helper-macros") (v "0.3.0") (d (list (d (n "actix-proc-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "actix-web") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)))) (h "0c2h4syayipc9j9wl9p1mgsin2imm73x993r5y59z36yr2avvb2f")))

(define-public crate-actix-helper-macros-0.4.0 (c (n "actix-helper-macros") (v "0.4.0") (d (list (d (n "actix-proc-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "01kjm1hc2z9dm4rnvk2vlp9954n03vb36ax6q6n3lzi7ys46mna9")))

(define-public crate-actix-helper-macros-3.0.0 (c (n "actix-helper-macros") (v "3.0.0") (d (list (d (n "actix-proc-macros") (r "^3.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0xnaasf9v0giab0y1m2r46grs96ynyzj2kr3pd26nrvgzhxvb3jw")))

(define-public crate-actix-helper-macros-4.0.0-beta.3 (c (n "actix-helper-macros") (v "4.0.0-beta.3") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0qbqb6pk94p9wqi0j2jhllvmn13s7vz63p9k2dfbh1zahy0phsn5")))

(define-public crate-actix-helper-macros-4.0.0-beta.3.1 (c (n "actix-helper-macros") (v "4.0.0-beta.3.1") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.3.1") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "07i2xh06hla5p7pxbyr7yn02061g83lx0j1slpjjdkh4pdkqmi73")))

(define-public crate-actix-helper-macros-3.1.0 (c (n "actix-helper-macros") (v "3.1.0") (d (list (d (n "actix-proc-macros") (r "^3.1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1z7gy65dhf2m167fh571jy7hx4m0fb3wma0br15i983sv0iamrnz")))

(define-public crate-actix-helper-macros-4.0.0-beta.9 (c (n "actix-helper-macros") (v "4.0.0-beta.9") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.9") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14a13l85ywq1cs1s3rhk19vfrqhr16xajfqpk1d0mdgxzjsyb63q")))

(define-public crate-actix-helper-macros-4.0.0-beta.10 (c (n "actix-helper-macros") (v "4.0.0-beta.10") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.10") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14plwhkcxhj81ymw7gnjy3ai3m09i4z8pbv6wb7pk5j73wbdshqd")))

(define-public crate-actix-helper-macros-4.0.0-beta.12 (c (n "actix-helper-macros") (v "4.0.0-beta.12") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.12") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0v0x316x6vriks7c1ad7ra8pn1p047jy7k9qwgyxazfiqx6qm8ls")))

(define-public crate-actix-helper-macros-4.0.0-beta.11 (c (n "actix-helper-macros") (v "4.0.0-beta.11") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.11") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1m8g1r4maj44cl02k44cpccbz27bsqpmq2x2rnp89530fja7cfqk")))

(define-public crate-actix-helper-macros-4.0.0-beta.12.1 (c (n "actix-helper-macros") (v "4.0.0-beta.12.1") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.12") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.12") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0b5kn26blqajvpxnx1vl3fcgbn56iscsqqqba2szcqz0h4b02xlb")))

(define-public crate-actix-helper-macros-4.0.0-beta.13 (c (n "actix-helper-macros") (v "4.0.0-beta.13") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.13") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.13") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1r9176l9dn1g433qgb40yvqdgin336w4z8y6cn2hql3bcbv5678i")))

(define-public crate-actix-helper-macros-4.0.0-beta.14 (c (n "actix-helper-macros") (v "4.0.0-beta.14") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.14") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.14") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "05bbgkx1nifig6rzpmlnqnzvbrgx5nxcp99pzr2my3m3kq1vzs69")))

(define-public crate-actix-helper-macros-4.0.0-beta.15 (c (n "actix-helper-macros") (v "4.0.0-beta.15") (d (list (d (n "actix-proc-macros") (r "^4.0.0-beta.15") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.15") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "09kqd7rhd0mjv0m7xiv9a4a0gdg33plsdn4x91rvrkdhirgag99m")))

(define-public crate-actix-helper-macros-4.0.0-rc.2 (c (n "actix-helper-macros") (v "4.0.0-rc.2") (d (list (d (n "actix-proc-macros") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-rc.2") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0rwxjiz6abcak4xzy1k21dyi3pglgnb8fkhpbqysqzk9dk0x8175")))

(define-public crate-actix-helper-macros-4.0.0 (c (n "actix-helper-macros") (v "4.0.0") (d (list (d (n "actix-proc-macros") (r "^4.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1374m9afni8hzcwdd4zqk1v611si6q8vkqxb49jm1jipf2qavprl") (y #t)))

(define-public crate-actix-helper-macros-4.0.1 (c (n "actix-helper-macros") (v "4.0.1") (d (list (d (n "actix-proc-macros") (r "^4.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1qx3445y6j6lgdylr5nz9hz5zhzhzcyb09wqwyr84jvr3ydi6rvi")))

