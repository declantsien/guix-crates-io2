(define-module (crates-io ac ti actix_validated_forms_derive) #:use-module (crates-io))

(define-public crate-actix_validated_forms_derive-0.1.0 (c (n "actix_validated_forms_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0x5kfxaaf8hdznbwbz1149jr92aygv6i9kycp205p7y2350a4fhw") (y #t)))

(define-public crate-actix_validated_forms_derive-0.1.1 (c (n "actix_validated_forms_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1smqvvagp7ckxv8ffjz38zq6488kvj3dc3v28wlnbcryhpri3z4q")))

