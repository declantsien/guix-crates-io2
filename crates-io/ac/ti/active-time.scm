(define-module (crates-io ac ti active-time) #:use-module (crates-io))

(define-public crate-active-time-0.1.0 (c (n "active-time") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("realtimeapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1znc8fdd22gfzh043qhb7s5q6a1acbzv8nqnc5dwq5cawdas8y4s")))

(define-public crate-active-time-0.1.1 (c (n "active-time") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("realtimeapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ydm69flplfskjwcz7xmipmvz9lrq241q3x15bjm55mhl94znf87")))

(define-public crate-active-time-0.1.2 (c (n "active-time") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("realtimeapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y9n94rxn1vvq5hs45hql95f6z9ngjy2crljkhy161ggb4q2dw3i")))

