(define-module (crates-io ac ti actix-jobs) #:use-module (crates-io))

(define-public crate-actix-jobs-0.1.0 (c (n "actix-jobs") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0v1bld3kk4nvai3hj6fqwnkgv2hhnh3xf0gb65mhdhcfkzp50rq9")))

(define-public crate-actix-jobs-0.1.1 (c (n "actix-jobs") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "03izda9mzh5c0rdxl86vsjw3y25x3pz63zlvzj0h4b1iq2x17379")))

(define-public crate-actix-jobs-0.1.2 (c (n "actix-jobs") (v "0.1.2") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0nkpnlii7nryldcx4yhhf1i25rzvl5fahffn11f9kmwfgn4ia68c")))

(define-public crate-actix-jobs-0.1.6 (c (n "actix-jobs") (v "0.1.6") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "1n7f9pxbgsd7q7qk2f8rhlf1yvn11nf6wdsnssjfnsrwsiqml77n")))

(define-public crate-actix-jobs-0.1.7 (c (n "actix-jobs") (v "0.1.7") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "19lqgh29s4y72agkmb23ia659jigi8z9553a6dlcz3pcm3bnk2if")))

