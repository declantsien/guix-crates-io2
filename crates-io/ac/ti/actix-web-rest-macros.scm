(define-module (crates-io ac ti actix-web-rest-macros) #:use-module (crates-io))

(define-public crate-actix-web-rest-macros-0.1.0 (c (n "actix-web-rest-macros") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ycq368vixa9343k6srxcvqjz7x2hvvcivm8av5vbz47y389hmab")))

(define-public crate-actix-web-rest-macros-0.2.0 (c (n "actix-web-rest-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19hdrfji76ff4pw9c7maa2gjm6dq92m0hjg9rgpmnmrqcldhchr0")))

(define-public crate-actix-web-rest-macros-0.3.0 (c (n "actix-web-rest-macros") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vwvi4mfamf9q7rcpyjc1hy5jda3qkmiykw0204fs54w1miialpa")))

