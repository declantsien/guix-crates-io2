(define-module (crates-io ac ti actix-rest) #:use-module (crates-io))

(define-public crate-actix-rest-0.0.0 (c (n "actix-rest") (v "0.0.0") (d (list (d (n "actix-web") (r "^0.7.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mzad9c8c3b0in0my978n2f4ajqk064bjryy2f6yfsc0d37ypjap")))

