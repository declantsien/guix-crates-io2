(define-module (crates-io ac ti activitystreams-kinds) #:use-module (crates-io))

(define-public crate-activitystreams-kinds-0.1.1 (c (n "activitystreams-kinds") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x5pdsrfpg7rl8cpp46p2r1g2cwg7g4xih5v7hqyn4h03q6myvka")))

(define-public crate-activitystreams-kinds-0.1.2 (c (n "activitystreams-kinds") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "17h7nqmy3i1ks1l4wb3zbzqsxld8nf7gpkkhxp9rj883zndfk107")))

(define-public crate-activitystreams-kinds-0.1.3 (c (n "activitystreams-kinds") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "iri-string") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1wk6snhbq88s10ds84cf48wl1f4fskj5qp0pwyhgjycy73fgqc2b") (f (quote (("default" "url"))))))

(define-public crate-activitystreams-kinds-0.2.0 (c (n "activitystreams-kinds") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "iri-string") (r "^0.5.0-beta.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1z7w50b21mx5bn7vl3gma1cij16d6lvl3hcm0v6m22q584s2wxja") (f (quote (("default" "url"))))))

(define-public crate-activitystreams-kinds-0.2.1 (c (n "activitystreams-kinds") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "iri-string") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1n4pqv418kx93cw3bm78i8c0d7d8p5bn5vkbnjvp1242p17ll0bd") (f (quote (("default" "url"))))))

(define-public crate-activitystreams-kinds-0.3.0 (c (n "activitystreams-kinds") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "iri-string") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1449g64srjc1w36bzyrw5rk4gfmcyjssd01mrh9v3h6qxxvgwzg9") (f (quote (("default" "url"))))))

