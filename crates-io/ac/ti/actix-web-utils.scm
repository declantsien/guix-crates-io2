(define-module (crates-io ac ti actix-web-utils) #:use-module (crates-io))

(define-public crate-actix-web-utils-0.1.0 (c (n "actix-web-utils") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b81w6a9pa1n841n0r6zqf0zk5m1n09fbns4pgcb2ns9iam0v0ns")))

(define-public crate-actix-web-utils-0.1.1 (c (n "actix-web-utils") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wrfnfc2rm6pixsa1y11h6rkykasl7i1d52kl9px4fwhy3n0cjgp")))

(define-public crate-actix-web-utils-0.1.2 (c (n "actix-web-utils") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kzffrqjdip64fgn0p3paxqs7lr2l1lmxq837vg3qqg22nclmqjf")))

(define-public crate-actix-web-utils-0.1.3 (c (n "actix-web-utils") (v "0.1.3") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0n2gs6yxbiymxgqzmvprvzm32hk88zpccwq3js9zha9qr26pgpk6")))

(define-public crate-actix-web-utils-0.1.4 (c (n "actix-web-utils") (v "0.1.4") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0y1w7kz06w13k7ni6lyp2qb5x7wnlxwxckmyfcbska92498gl7gc")))

(define-public crate-actix-web-utils-0.1.5 (c (n "actix-web-utils") (v "0.1.5") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1a5vp18cr6iglrj8imadshqsc2fb00mlbjiq84kg349vqn75q4a0")))

(define-public crate-actix-web-utils-0.1.6 (c (n "actix-web-utils") (v "0.1.6") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b4b7cfn191nmzni4grmksz8x2mlmdjy944yja3rc6wdz3d5vhiq")))

(define-public crate-actix-web-utils-0.1.7 (c (n "actix-web-utils") (v "0.1.7") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1lm19g79svz4hak5h4sx221qcdijaqpmmbsz9ymks8848gck6dq7")))

(define-public crate-actix-web-utils-0.1.8 (c (n "actix-web-utils") (v "0.1.8") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01s85fzr0himqh7xagnx7mcbdfgns1d2f5n6wgmvjs7xl62w974g")))

(define-public crate-actix-web-utils-0.1.9 (c (n "actix-web-utils") (v "0.1.9") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16jdda5jznj7z0h3hkg09l7yvnrznywzh3fcxbbicfxdjw5x9q61")))

(define-public crate-actix-web-utils-0.2.0 (c (n "actix-web-utils") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0y7vq3nyxs5qjq7vxnxj87pnxp4nxap4wappav0wcpv1c85jcmsv")))

(define-public crate-actix-web-utils-0.2.1 (c (n "actix-web-utils") (v "0.2.1") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kmsi7lckqwdiscp6s41byxn8g0h56cpkbpjcx8f4dvf4v3phcld")))

(define-public crate-actix-web-utils-0.2.2 (c (n "actix-web-utils") (v "0.2.2") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fd43ddcakzzic2iy08xf5z0zqhxxxh2dsdmwkxivi5r9wmrhzyf")))

(define-public crate-actix-web-utils-0.2.3 (c (n "actix-web-utils") (v "0.2.3") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dzp9ad4fyi0c99spzdj0rxyv8339dmsicx0n5pl9g2bxv297p8q")))

(define-public crate-actix-web-utils-0.2.4 (c (n "actix-web-utils") (v "0.2.4") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rs38nycnfy2x8nky93saj78zjm64mlx17n7q95yyknp40hnzvm7")))

(define-public crate-actix-web-utils-0.2.5 (c (n "actix-web-utils") (v "0.2.5") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i3gp0yd8rgaydwnizbndaq1544w3i7nra9rdwlnz4w0gp96kybi")))

(define-public crate-actix-web-utils-0.2.6 (c (n "actix-web-utils") (v "0.2.6") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04sb6jn0gj4qk6wixsq9ngn1dhqwjrslhgywsjdn1shmwhz7kmby")))

(define-public crate-actix-web-utils-0.2.7 (c (n "actix-web-utils") (v "0.2.7") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14yxii41268lq2ld6rp1pcqb60d6afibvay845nm84ckwyn0s8lh")))

(define-public crate-actix-web-utils-0.2.8 (c (n "actix-web-utils") (v "0.2.8") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ly08hfa9nfqsw2p2rvjfirzk74nwgh6cnd93p961khh2qc5psjy")))

(define-public crate-actix-web-utils-0.2.9 (c (n "actix-web-utils") (v "0.2.9") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10pw5735vcvlv3qqp0yiarivaxaikw7i6w4kzc164hhb3i6mf7gq")))

(define-public crate-actix-web-utils-0.2.10 (c (n "actix-web-utils") (v "0.2.10") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1npsn6az14rm5arw5l2js1jqy0ggbqkv1cc4g3wz717wdpxs2s2r")))

(define-public crate-actix-web-utils-0.2.11 (c (n "actix-web-utils") (v "0.2.11") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1c7sgj4i93q4s2bksapyyckv5zn9pdkfhhrznpidlp1d8xwwhh8j")))

(define-public crate-actix-web-utils-0.2.12 (c (n "actix-web-utils") (v "0.2.12") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0x1skn2zqwm4k71g4kkj1fzp2c9k1vhj5xn5sapipff0n5g6w57v")))

(define-public crate-actix-web-utils-0.2.13 (c (n "actix-web-utils") (v "0.2.13") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0symd3c453441gvwr9220zcv13i4x2j31hjfbnd34v8nbzf2mqcf")))

(define-public crate-actix-web-utils-0.2.14 (c (n "actix-web-utils") (v "0.2.14") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "059f0dwhanyxidsr5klgx3r0v62l9y413c05r2v0ycpmamk13fry")))

(define-public crate-actix-web-utils-0.2.15 (c (n "actix-web-utils") (v "0.2.15") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m011f16c8c0zh7r825mziw6wg2ps30493jz7laiinh645qrrych")))

(define-public crate-actix-web-utils-0.2.16 (c (n "actix-web-utils") (v "0.2.16") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06h3kkancrjwrzhyj24kw5pjk9yvf703lrj6m44q9lhb64bn88vl")))

(define-public crate-actix-web-utils-0.2.17 (c (n "actix-web-utils") (v "0.2.17") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ism55b54pax61y78lw5spppbdi46j2pkwd724q4604g4nmvl6bi")))

(define-public crate-actix-web-utils-0.2.18 (c (n "actix-web-utils") (v "0.2.18") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04shjpqyr6nvk1f8y0xib2r06ql8gds37zn53nv9qvlsx1a4bfdc")))

(define-public crate-actix-web-utils-0.2.19 (c (n "actix-web-utils") (v "0.2.19") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fmfj9rkg5wpbz2fn9bznj07g2zx6d5hwv567f6ig4g9xzhmwqdg")))

(define-public crate-actix-web-utils-0.2.20 (c (n "actix-web-utils") (v "0.2.20") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "063fkwpirjrpfzy7ygjzam4lbwsj7b64qz5rgl14cic9v5z1sg4g")))

