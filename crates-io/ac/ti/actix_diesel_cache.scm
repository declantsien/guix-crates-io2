(define-module (crates-io ac ti actix_diesel_cache) #:use-module (crates-io))

(define-public crate-actix_diesel_cache-0.1.0 (c (n "actix_diesel_cache") (v "0.1.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (d #t) (k 0)))) (h "178j5dsl59nnjclabybip0sv8dyiqw87i61k7z677ajyfnr0ac9f")))

