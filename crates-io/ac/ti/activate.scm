(define-module (crates-io ac ti activate) #:use-module (crates-io))

(define-public crate-activate-0.1.0 (c (n "activate") (v "0.1.0") (h "19ya2v9f37fkn2xxp809025hbjfzz4qnzsms8z294sbr9yls70xk")))

(define-public crate-activate-0.2.0 (c (n "activate") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0hiklz2fxn4wrcf8r3xyj5ix59i8xdhg9892f4iip4qsmc56b641")))

(define-public crate-activate-0.3.0 (c (n "activate") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "15vh9w5xd69daw31ay1q1q0gkcqajcqk05fwc0y9g0gsv9ck57g3")))

