(define-module (crates-io ac ti actix-redisactor) #:use-module (crates-io))

(define-public crate-actix-redisactor-0.3.1 (c (n "actix-redisactor") (v "0.3.1") (d (list (d (n "actix") (r "^0.8.3") (d #t) (k 0)) (d (n "backoff") (r "^0.1.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "redis-async") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1.3") (d #t) (k 0)))) (h "0xv3fkbcqzvi06k0q3h0zvwc80lvk21akfjsc97cih711mmfi8vg")))

