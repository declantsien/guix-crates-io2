(define-module (crates-io ac ti actix-web-upload-example) #:use-module (crates-io))

(define-public crate-actix-web-upload-example-0.0.1 (c (n "actix-web-upload-example") (v "0.0.1") (d (list (d (n "actix-multipart") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.1") (d #t) (k 0)) (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "envy") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "0vbjilxndxzximw4ngcar93778pxnjdfm1x59b3d3b38ky9fyahj")))

(define-public crate-actix-web-upload-example-0.0.2 (c (n "actix-web-upload-example") (v "0.0.2") (d (list (d (n "actix-multipart") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0-alpha.1") (d #t) (k 0)) (d (n "concat-string") (r "^1.0.1") (d #t) (k 0)) (d (n "envy") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "0ikph3cqxicfah3ldhfng1kh91qp9kxd0h6i0kwx6m9lcyxhhipp")))

