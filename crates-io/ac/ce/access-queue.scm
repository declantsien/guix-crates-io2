(define-module (crates-io ac ce access-queue) #:use-module (crates-io))

(define-public crate-access-queue-1.0.0 (c (n "access-queue") (v "1.0.0") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.5") (d #t) (k 2)))) (h "1dvfbvbsg5ag7n68yv6nfz693frlzv3vpndjrbysn8z6c32nbpy9")))

(define-public crate-access-queue-1.1.0 (c (n "access-queue") (v "1.1.0") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-task") (r "^0.3.5") (d #t) (k 2)))) (h "1w482wy6lfhcid73sl6kybqkc0yp2qzsw8bk7j83y9nd8x7flly9")))

