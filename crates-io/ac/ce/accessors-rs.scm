(define-module (crates-io ac ce accessors-rs) #:use-module (crates-io))

(define-public crate-accessors-rs-0.1.0 (c (n "accessors-rs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0r1sj3a36c74zgw5y2d71riwmpanpgi264gbjwp14nbgfkdh1w8w")))

