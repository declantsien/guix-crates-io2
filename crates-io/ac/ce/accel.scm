(define-module (crates-io ac ce accel) #:use-module (crates-io))

(define-public crate-accel-0.1.0 (c (n "accel") (v "0.1.0") (d (list (d (n "cuda-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "procedurals") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "04zcf25pjn8id6qsjg15wkpwxa2n330m5j2ri960y3726snb59xh")))

(define-public crate-accel-0.2.0 (c (n "accel") (v "0.2.0") (d (list (d (n "accel-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "cuda-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "procedurals") (r "^0.2.3") (d #t) (k 0)))) (h "1jn88mc8lfj392n6air5brfrf41qwly1s89gx36rnlpckv8mxqvz")))

(define-public crate-accel-0.3.0-alpha.1 (c (n "accel") (v "0.3.0-alpha.1") (d (list (d (n "cuda-driver-sys") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "cuda-runtime-sys") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "1b81zhiz2k7bjdrsg6gy1nvp6dgaigbyapz2bj73j170l8bcksxx")))

(define-public crate-accel-0.3.0-alpha.2 (c (n "accel") (v "0.3.0-alpha.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-driver-sys") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ij9fpxz8221mcpihpf6zfpsk4g2p621qjfwk1wggbmqsyn5bdbi")))

(define-public crate-accel-0.3.0 (c (n "accel") (v "0.3.0") (d (list (d (n "accel-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-driver-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0afw2p98sb2lwczpw3vpfwfsjzlsg7y0f4rnwrq2c4hp64fkrbah")))

(define-public crate-accel-0.3.1 (c (n "accel") (v "0.3.1") (d (list (d (n "accel-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "cuda-driver-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "paste") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "01v1xnyd2jb7azfz01x4d8r103xj444c1pjv039j5ps911ryryrf")))

