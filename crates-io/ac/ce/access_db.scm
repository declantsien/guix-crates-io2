(define-module (crates-io ac ce access_db) #:use-module (crates-io))

(define-public crate-access_db-0.1.0 (c (n "access_db") (v "0.1.0") (d (list (d (n "mysql") (r "^22.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1w98xwbvi2s3hd5sr1k14sgkxqysgg8r5ls5w9wymxarl1i7wp94")))

(define-public crate-access_db-0.1.1 (c (n "access_db") (v "0.1.1") (d (list (d (n "mysql") (r "^22.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1acvz34maqqz5fkh53v9n2026j9lryihbxsi1hx0pfy006ff2z9x")))

(define-public crate-access_db-0.2.0 (c (n "access_db") (v "0.2.0") (d (list (d (n "mysql") (r "^22.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "04ckxv8haw2r6h2zsc5sy20qacm6pajj8jsxbbgbzfzl7wp96h6f")))

(define-public crate-access_db-0.2.1 (c (n "access_db") (v "0.2.1") (d (list (d (n "mysql") (r "^22.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0bmvxh6x3frz34i05chvz6ikhp8xvn4fiads0bjn318c0pr726ws")))

