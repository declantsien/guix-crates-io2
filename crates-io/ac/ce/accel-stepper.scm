(define-module (crates-io ac ce accel-stepper) #:use-module (crates-io))

(define-public crate-accel-stepper-0.1.0 (c (n "accel-stepper") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "libm") (r "^0.1.4") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1k4g3b8ziqmw7zzhsdswfk9d8vg1s9day94q91w2i6phxmsh0hkb") (f (quote (("std") ("hal" "embedded-hal") ("default"))))))

