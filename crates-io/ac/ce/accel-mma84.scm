(define-module (crates-io ac ce accel-mma84) #:use-module (crates-io))

(define-public crate-accel-mma84-0.1.0 (c (n "accel-mma84") (v "0.1.0") (d (list (d (n "tessel") (r "^0.2.0") (d #t) (k 0)))) (h "021qc296cxjrrkibxkynvas53y7h8nwx1jx3qc7y5hvnxwv9w33w") (y #t)))

(define-public crate-accel-mma84-0.1.0-beta.1 (c (n "accel-mma84") (v "0.1.0-beta.1") (d (list (d (n "tessel") (r "^0.2.0") (d #t) (k 0)))) (h "0dd2asi0b4lfaaq1yqwbh751clislxpgrg18qybbplm6h4z1yg6k")))

(define-public crate-accel-mma84-0.1.1 (c (n "accel-mma84") (v "0.1.1") (d (list (d (n "tessel") (r "^0.2.0") (d #t) (k 0)))) (h "1kw4wh4xj7qbzykbxm0kmvrhg7mc2w770myfw39dxr1m9b2y6zzv")))

(define-public crate-accel-mma84-0.2.0-beta.1 (c (n "accel-mma84") (v "0.2.0-beta.1") (d (list (d (n "tessel") (r "^0.3.0-beta.1") (d #t) (k 0)))) (h "1khj47d49gwdvn8wj5shrgslzg740ckfl3pjhyikhgnrakwmla4c")))

(define-public crate-accel-mma84-0.2.0-beta.2 (c (n "accel-mma84") (v "0.2.0-beta.2") (d (list (d (n "tessel") (r "^0.3.0-beta.2") (d #t) (k 0)))) (h "19lgjfgz054ypsrwqm9gbf51v7pwpak9pl266g3n67rrxma81xig")))

(define-public crate-accel-mma84-0.2.0-beta.3 (c (n "accel-mma84") (v "0.2.0-beta.3") (d (list (d (n "tessel") (r "^0.3.0-beta.3") (d #t) (k 0)))) (h "1w0m2g3dsa5nzm8bp2nl630lfv4h08dfh9ya1rd7ghsbfnzdpnz6")))

(define-public crate-accel-mma84-0.2.0 (c (n "accel-mma84") (v "0.2.0") (d (list (d (n "tessel") (r "^0.3.0") (d #t) (k 0)))) (h "109qlj9mjc6n4g6zrkcfr6avhgiciahiq9cw21zc2kjfr629jpac")))

