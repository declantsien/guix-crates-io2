(define-module (crates-io ac ce accelerometer) #:use-module (crates-io))

(define-public crate-accelerometer-0.1.0 (c (n "accelerometer") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "0lscl4391nwgwdbk0yshasx5mbhz3lmgah9zzj5mb8rxg0dv9vs9")))

(define-public crate-accelerometer-0.2.0 (c (n "accelerometer") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)))) (h "0r6wr4wpqzwn090zr8yfjfayjgj1qmvfhzxbg09fxqv07vk54mw8")))

(define-public crate-accelerometer-0.3.0 (c (n "accelerometer") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.11") (k 0)) (d (n "heapless") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17xny7ihk5naqbf7mqx4nwnmrpras1s5ackq5ldw020xpc81fn21") (f (quote (("tracking" "heapless") ("default" "tracking"))))))

(define-public crate-accelerometer-0.4.0 (c (n "accelerometer") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.12") (k 0)))) (h "1cp4rnpkxl7cijk1iax9zxd87ss1zgf7v6lcdp9887yghp9af8qy") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.5.0 (c (n "accelerometer") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.12") (o #t) (k 0)) (d (n "micromath") (r "^0.1") (d #t) (k 0)))) (h "0gi8mznq4dqj5yq72sgg1lkx20w605sr458xh9whhzlx6yr8j35z") (f (quote (("orientation" "generic-array") ("default" "orientation"))))))

(define-public crate-accelerometer-0.5.1 (c (n "accelerometer") (v "0.5.1") (d (list (d (n "micromath") (r "^0.2") (d #t) (k 0)))) (h "02p92137kkzq6rv88scngxz6gw0wns1hg3c5idiir5ylagwy13nd") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.5.2 (c (n "accelerometer") (v "0.5.2") (d (list (d (n "micromath") (r "^0.2") (d #t) (k 0)))) (h "12li4wrnnajnplds2k2a5jj6glla54jmrpglcsg2pa2crdlkdpr9") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.6.0 (c (n "accelerometer") (v "0.6.0") (d (list (d (n "micromath") (r "^0.3") (d #t) (k 0)))) (h "1w0hdc6gfnp8wk174rzvxw9d7bg2n8x13pw2c1q0nbrvw5y4dlcz") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.6.1 (c (n "accelerometer") (v "0.6.1") (d (list (d (n "micromath") (r "^0.3") (d #t) (k 0)))) (h "092s37awj31cvigmw1cfzl3braq8g48h19fy5nzq3anr2k05v7bb") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.7.0 (c (n "accelerometer") (v "0.7.0") (d (list (d (n "micromath") (r "^0.4") (d #t) (k 0)))) (h "0m3zw7m0i09xss43k40bxl36x64lprf9xvwl9zrqvkijhnw7cnnr") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.8.0 (c (n "accelerometer") (v "0.8.0") (d (list (d (n "micromath") (r "^0.4") (d #t) (k 0)))) (h "13ii4fj5b49qvqqjm9rhicma9lx2bgvdyb0qkx2wwidd1wfn53gx") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.8.1 (c (n "accelerometer") (v "0.8.1") (d (list (d (n "micromath") (r "^0.4") (d #t) (k 0)))) (h "1a989ks1lajn3kghljh0r53y9nn0p3sn0ibylf3xih06dlq65wql") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.9.0 (c (n "accelerometer") (v "0.9.0") (d (list (d (n "micromath") (r "^0.5") (f (quote ("vector"))) (d #t) (k 0)))) (h "1lgc3i5k1bqpp7q4ykjzrgvhswq5csllc05p6066xyp8j72gx5ga") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.10.0 (c (n "accelerometer") (v "0.10.0") (d (list (d (n "micromath") (r "^1") (f (quote ("vector"))) (d #t) (k 0)))) (h "1566al16yvdimkc5332g8lgnrgdv7kgxyq0s8mgp5qvll8h42v97") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.11.0 (c (n "accelerometer") (v "0.11.0") (d (list (d (n "micromath") (r "^1") (f (quote ("vector"))) (d #t) (k 0)))) (h "0sj8f18l2xkc9i6s3wwbkr3zv87pzzmxfqqpwk10zmcjfyv5inwz") (f (quote (("orientation") ("default" "orientation"))))))

(define-public crate-accelerometer-0.12.0 (c (n "accelerometer") (v "0.12.0") (d (list (d (n "micromath") (r "^1") (f (quote ("vector"))) (d #t) (k 0)))) (h "0ylzcw4bckgil2907cmicn1bdsqy2i8sgjb0hxs5wsdhbkcqci8s") (f (quote (("orientation") ("default" "orientation"))))))

