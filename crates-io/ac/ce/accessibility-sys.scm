(define-module (crates-io ac ce accessibility-sys) #:use-module (crates-io))

(define-public crate-accessibility-sys-0.1.0 (c (n "accessibility-sys") (v "0.1.0") (h "0iwb3k26ka1lnfxqa8an06r6f92d4a82fm475m4f6by60z1dz20j")))

(define-public crate-accessibility-sys-0.1.1 (c (n "accessibility-sys") (v "0.1.1") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)))) (h "027v62kjyzi0g94wk0cpmg2lg53hymy1y683whvjk14z2srq0xjq")))

(define-public crate-accessibility-sys-0.1.2 (c (n "accessibility-sys") (v "0.1.2") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)))) (h "0bs5a1p23n56d1dic6i360j99i08drl39zlw96fc8p4afalx69yr")))

(define-public crate-accessibility-sys-0.1.3 (c (n "accessibility-sys") (v "0.1.3") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)))) (h "1hnlgz2s37ay18qqlly13pdlgffav21rp2ybgfi7s5slv96ka2dz")))

