(define-module (crates-io ac ce access_control_near) #:use-module (crates-io))

(define-public crate-access_control_near-0.1.0 (c (n "access_control_near") (v "0.1.0") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "0j526g60c2p92fkzz50rivdjg9nmi4nkg3l0xy1slhs3a1w7rn1x") (y #t)))

(define-public crate-access_control_near-0.1.1 (c (n "access_control_near") (v "0.1.1") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "0x3yk4ykhph5n8drb422ass77b6jp26p3sh0c4anlrnslvfprhl3") (y #t)))

(define-public crate-access_control_near-0.1.2 (c (n "access_control_near") (v "0.1.2") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "0lldrn2zxqag93i3cgb5jbq54mw8d0kphf9jsscz9rgdvavrn5k2") (y #t)))

(define-public crate-access_control_near-0.1.3 (c (n "access_control_near") (v "0.1.3") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "0rk73rh5b4fnfphqlzlq4x7ll3zl3p7bbi0zrrzzq0q28higzwkv") (y #t)))

(define-public crate-access_control_near-0.1.4 (c (n "access_control_near") (v "0.1.4") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "1rbxl7p3pa8gcrhbm4cq6d08giiqy0y0l2qihgpx8a281ph3wfjb") (y #t)))

(define-public crate-access_control_near-0.1.5 (c (n "access_control_near") (v "0.1.5") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "0nm5ffw7r29yhyilyj9vp1gdaa5p30mxpmmldyk96rysrz9l6ky1") (y #t)))

(define-public crate-access_control_near-0.1.6 (c (n "access_control_near") (v "0.1.6") (d (list (d (n "access_control_near_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "1mw5kp6wxgqgh51fsxnqrsg5y6dzkbpk7s7v1s5mm605mm9r31kg") (y #t)))

(define-public crate-access_control_near-0.1.7 (c (n "access_control_near") (v "0.1.7") (d (list (d (n "access_control_near_attribute") (r "^0.1.1") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "1wil0nwqvz3f0vyfd9ljhdb7kixb5qpih7dydxcm2834jfr0aw62") (y #t)))

(define-public crate-access_control_near-0.1.8 (c (n "access_control_near") (v "0.1.8") (d (list (d (n "access_control_near_attribute") (r "^0.1.1") (d #t) (k 0)) (d (n "near-sdk") (r "^3.1.0") (d #t) (k 0)))) (h "09mqgxd2nv9j9xz6wncis1mmfdpszkfvck1prd83cbrlyidqzjm7") (y #t)))

