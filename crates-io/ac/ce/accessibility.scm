(define-module (crates-io ac ce accessibility) #:use-module (crates-io))

(define-public crate-accessibility-0.1.0 (c (n "accessibility") (v "0.1.0") (d (list (d (n "accessibility-sys") (r "^0.1") (d #t) (k 0)))) (h "1lhpn0arvpad1nxiadaafv0xi6n5p4650079505di905pkksmd5p")))

(define-public crate-accessibility-0.1.2 (c (n "accessibility") (v "0.1.2") (d (list (d (n "accessibility-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "0280fmw03vn1z91v694slzgcnfbn6592i2060iyz4r0hdq9d78hp")))

(define-public crate-accessibility-0.1.3 (c (n "accessibility") (v "0.1.3") (d (list (d (n "accessibility-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "1frqplc7d41idm5dlg95s9gx4g2rxw1fgs03c0wp8iv9qng006h1")))

(define-public crate-accessibility-0.1.4 (c (n "accessibility") (v "0.1.4") (d (list (d (n "accessibility-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "1qaibm8srhf106i6n7w28j97rixai33a74szkfhnlgiwvzp00gx4")))

(define-public crate-accessibility-0.1.5 (c (n "accessibility") (v "0.1.5") (d (list (d (n "accessibility-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "0nkni49c5khn70h5494bhxj34f40rl4wkfycas5fncmjhcvfnga8")))

(define-public crate-accessibility-0.1.6 (c (n "accessibility") (v "0.1.6") (d (list (d (n "accessibility-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)))) (h "1w6fzvfzh6dssbv0z3vli4qmjxn0pcgbnqm2n6kkak81xp6lrbb8")))

