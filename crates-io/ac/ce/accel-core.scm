(define-module (crates-io ac ce accel-core) #:use-module (crates-io))

(define-public crate-accel-core-0.1.0 (c (n "accel-core") (v "0.1.0") (h "0f1lh4p95jbv15wf5ji7pzzm0kbmh89jylbqy347jdlm4x64ylil")))

(define-public crate-accel-core-0.2.0-alpha (c (n "accel-core") (v "0.2.0-alpha") (h "03c27x2dsp80hc4ds0jhpyf38y0ad65v72r3pf99pwpp87v9sps5")))

(define-public crate-accel-core-0.3.0-alpha.1 (c (n "accel-core") (v "0.3.0-alpha.1") (h "0h65i0h01186aymq1y8y1r81iinpx3w972gxwgbyr0qmcypv3bzv")))

(define-public crate-accel-core-0.3.0-alpha.2 (c (n "accel-core") (v "0.3.0-alpha.2") (h "15b7g21p8fb36avnsmhikpv6qs8giha878g66afa53z2jirk88s4")))

(define-public crate-accel-core-0.3.0-alpha.3 (c (n "accel-core") (v "0.3.0-alpha.3") (h "0xfia6cj6f7k2ndzyjxp3yf5xdwmwblvrvya534x8283g3wrsi3q")))

(define-public crate-accel-core-0.3.0-alpha.4 (c (n "accel-core") (v "0.3.0-alpha.4") (h "02n5w429wgq4z04icr2y0g21c013d9k0cniz3jkqymd8wdr3yjzk")))

(define-public crate-accel-core-0.3.0 (c (n "accel-core") (v "0.3.0") (h "18bkyn9m02znfrmbi21n5qq2r54pd6ipgb7zwhmqvcbs8a3x523l")))

