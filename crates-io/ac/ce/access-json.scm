(define-module (crates-io ac ce access-json) #:use-module (crates-io))

(define-public crate-access-json-0.1.0 (c (n "access-json") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pzrirqmbj4f56nvpljv7dd32sqlhz57ljj2shqq7y56ryq1zmcf")))

