(define-module (crates-io ac ce accept-header) #:use-module (crates-io))

(define-public crate-accept-header-0.1.0 (c (n "accept-header") (v "0.1.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "1lhr3v5gdx55vkjzmiz7rs3zz0304003x0mzlybn9lfgm7h3dc8w") (y #t)))

(define-public crate-accept-header-0.1.1 (c (n "accept-header") (v "0.1.1") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "1wv0ll7jf14vbi71qj76md2942zqyqb3l1vx23mvxgljc0q01bvm") (y #t)))

(define-public crate-accept-header-0.1.2 (c (n "accept-header") (v "0.1.2") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "07d1x2ffc3vw4aa31yzbdhnh7vqab3i1vcbmcwqgpxsqzlg26rcq") (y #t)))

(define-public crate-accept-header-0.2.0 (c (n "accept-header") (v "0.2.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "17xh2xwchxysi0iwpr5znk5m0ms1zgkhhqgsr19iiikxsmw92hfm")))

(define-public crate-accept-header-0.2.1 (c (n "accept-header") (v "0.2.1") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "1rc9qhavkpnars2fwkfqc7rpz9rvbk43n9xk2fpngm3l1q7109nw")))

(define-public crate-accept-header-0.2.2 (c (n "accept-header") (v "0.2.2") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "1lpj1qin0dma3gq84hb1d5n85029js5ihmlmnd676hmczxxzfni4")))

(define-public crate-accept-header-0.2.3 (c (n "accept-header") (v "0.2.3") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)))) (h "10yxw15hl2qjlhchjwpjy13mr06iyxjrzz81mx7qafxag3xh148d")))

