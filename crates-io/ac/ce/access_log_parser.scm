(define-module (crates-io ac ce access_log_parser) #:use-module (crates-io))

(define-public crate-access_log_parser-0.1.0 (c (n "access_log_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0acshrjxrvw1wwmjbhks8nm12lq789q1hgg3xasz0zafpldl1c04")))

(define-public crate-access_log_parser-0.2.0 (c (n "access_log_parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "06lapmw6x28azchwiicrdyrfj0ziw1agi3s1kj7bw7j6wfhijmd3")))

(define-public crate-access_log_parser-0.3.0 (c (n "access_log_parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "18ij29m4wlr6g7qr1gvakrhwgn758vd7p8bzkj7cac7qylkvyqdg")))

(define-public crate-access_log_parser-0.4.0 (c (n "access_log_parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "08vp13as97s314f1mqi95ilxwh6r9wg4m2a242p47g1ajfzwgj0v")))

(define-public crate-access_log_parser-0.5.0 (c (n "access_log_parser") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0jkmpx3mwl3m55rqw8dvx1kbfd1jillm008annmg907dmd7iidl6")))

(define-public crate-access_log_parser-0.6.0 (c (n "access_log_parser") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1s8np4j6q1bc421mjdmhiwws86h466sajbbmqc85p0qyjs98w4sr")))

(define-public crate-access_log_parser-0.7.0 (c (n "access_log_parser") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jncpx3dqa3g04zvydc8y5fdxd9rz0lk8qvq1ggqcimsx0ak09wf")))

(define-public crate-access_log_parser-0.8.0 (c (n "access_log_parser") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1641bpvs9zy5kihd86hjlplvs12pk8nmyyd5c7ga33xg3yp9dvin")))

