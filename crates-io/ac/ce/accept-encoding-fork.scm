(define-module (crates-io ac ce accept-encoding-fork) #:use-module (crates-io))

(define-public crate-accept-encoding-fork-0.2.0-alpha.3 (c (n "accept-encoding-fork") (v "0.2.0-alpha.3") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)))) (h "15s5vfqgismv9rqgk25cqkghb949bsbc6qndnn87ajmkxnzg0jhh")))

