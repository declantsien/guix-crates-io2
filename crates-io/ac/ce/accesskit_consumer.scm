(define-module (crates-io ac ce accesskit_consumer) #:use-module (crates-io))

(define-public crate-accesskit_consumer-0.1.0 (c (n "accesskit_consumer") (v "0.1.0") (d (list (d (n "accesskit") (r "^0.1.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1yp4kgmpcwwwr8q1219sv6xh145xcnm00fkdwvz6373bl3dqqav7")))

(define-public crate-accesskit_consumer-0.1.1 (c (n "accesskit_consumer") (v "0.1.1") (d (list (d (n "accesskit") (r "^0.1.1") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "16h59gjk7000ry08j3nyaaxzciwg1lmyamk0cw4fskllm828m6n7")))

(define-public crate-accesskit_consumer-0.2.0 (c (n "accesskit_consumer") (v "0.2.0") (d (list (d (n "accesskit") (r "^0.2.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1qsbvk651dvvf6c6xaxwinfj2zwni03k2p9prx1vkzcv27brims0")))

(define-public crate-accesskit_consumer-0.3.0 (c (n "accesskit_consumer") (v "0.3.0") (d (list (d (n "accesskit") (r "^0.3.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0n2n86f6kyprxs3g592x5i4qp6bybhs2n5b2b7x481215ja3n5aa")))

(define-public crate-accesskit_consumer-0.4.0 (c (n "accesskit_consumer") (v "0.4.0") (d (list (d (n "accesskit") (r "^0.4.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1qgjlbsmkx4gi5ls0ml4i9bz8bi1cfjjvc1rrmpdrikaar1lq93p")))

(define-public crate-accesskit_consumer-0.5.0 (c (n "accesskit_consumer") (v "0.5.0") (d (list (d (n "accesskit") (r "^0.5.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1qm5mr9vkdp3ps2k6d9s3v7pb180ps6gxyi0csw57g8ndk8wkzgw")))

(define-public crate-accesskit_consumer-0.5.1 (c (n "accesskit_consumer") (v "0.5.1") (d (list (d (n "accesskit") (r "^0.5.1") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "14g2qhvg7zv8xlgzkfxkl0j1znc9myj2yr93sx51psxwq04v6jbi")))

(define-public crate-accesskit_consumer-0.6.0 (c (n "accesskit_consumer") (v "0.6.0") (d (list (d (n "accesskit") (r "^0.6.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1yyfyxxcnhpdbbf5hy4dm2qg9biya8ic1v5gc8wx3fhyvsz55r6m")))

(define-public crate-accesskit_consumer-0.6.1 (c (n "accesskit_consumer") (v "0.6.1") (d (list (d (n "accesskit") (r "^0.6.1") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0i2mymirdhnaja7659pm01ambl4zz115214v8pn05ppq1g0l442a")))

(define-public crate-accesskit_consumer-0.7.0 (c (n "accesskit_consumer") (v "0.7.0") (d (list (d (n "accesskit") (r "^0.7.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "08sxy2s9ngsbxcl22rlsmmvzsm0vh8w387wg49dr85z37mmlxxzv")))

(define-public crate-accesskit_consumer-0.7.1 (c (n "accesskit_consumer") (v "0.7.1") (d (list (d (n "accesskit") (r "^0.7.0") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1z0q7yzd465bak7dpfrmdbx9wx7hvwc1qd5awbvmmiq7paz7jcql")))

(define-public crate-accesskit_consumer-0.8.0 (c (n "accesskit_consumer") (v "0.8.0") (d (list (d (n "accesskit") (r "^0.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "14827f2kqsn2sbp5lm547g7fc1knwshxwkanqpvfb0wwyfgjshqh")))

(define-public crate-accesskit_consumer-0.9.0 (c (n "accesskit_consumer") (v "0.9.0") (d (list (d (n "accesskit") (r "^0.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "03avza493xlrqlg35ahfbls6s890zrhw37lyxqyai98k34clb7wa")))

(define-public crate-accesskit_consumer-0.9.1 (c (n "accesskit_consumer") (v "0.9.1") (d (list (d (n "accesskit") (r "^0.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0lmpkmcrq7ynw5ahbn3dgay1vyz0qh3zyl3bh8qargk9l87wj8kw")))

(define-public crate-accesskit_consumer-0.10.0 (c (n "accesskit_consumer") (v "0.10.0") (d (list (d (n "accesskit") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "042f5k93cmnr4bwyvjnv8s49qdwbbydaabd4ys9sp8sc4hh244nz")))

(define-public crate-accesskit_consumer-0.11.0 (c (n "accesskit_consumer") (v "0.11.0") (d (list (d (n "accesskit") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1kzhq9ms0msk4jpdj5gnzral6f8cdc53fbj67n4l5ppmmxdqlmdl")))

(define-public crate-accesskit_consumer-0.12.0 (c (n "accesskit_consumer") (v "0.12.0") (d (list (d (n "accesskit") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0cy4mghch0n9kp64qbb78fdynjn9az7xzz37iw8lz78l8pkngjjj")))

(define-public crate-accesskit_consumer-0.12.1 (c (n "accesskit_consumer") (v "0.12.1") (d (list (d (n "accesskit") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1qi5lr9hiv3d3a30h8wxl1bd5bw6z18r645x3sfjybbaf0zkjirz")))

(define-public crate-accesskit_consumer-0.13.0 (c (n "accesskit_consumer") (v "0.13.0") (d (list (d (n "accesskit") (r "^0.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1v3lzlzfiaq36w4rwwarpxjppivm1amh46bzhcqlvyd4089czs6f")))

(define-public crate-accesskit_consumer-0.14.0 (c (n "accesskit_consumer") (v "0.14.0") (d (list (d (n "accesskit") (r "^0.10.0") (d #t) (k 0)))) (h "0max5j4as6w29mhknd45yy12xn8k54hzvp8g7linrx2828v7z3p7")))

(define-public crate-accesskit_consumer-0.14.1 (c (n "accesskit_consumer") (v "0.14.1") (d (list (d (n "accesskit") (r "^0.10.1") (d #t) (k 0)))) (h "1n8z9mmwn9yrdgnhwwjc0c740yh708fcgrg25q0l8ffarwiqpfj8")))

(define-public crate-accesskit_consumer-0.14.2 (c (n "accesskit_consumer") (v "0.14.2") (d (list (d (n "accesskit") (r "^0.11.0") (d #t) (k 0)))) (h "1zrraaw9h8bkkvrnza7pn0dgfrygvv4dzpwsxx727ls1zjcy0z7b") (y #t)))

(define-public crate-accesskit_consumer-0.15.0 (c (n "accesskit_consumer") (v "0.15.0") (d (list (d (n "accesskit") (r "^0.11.0") (d #t) (k 0)))) (h "0hyvk4klz0fpjhwg2vifzl2di1lvz66j4h59jv8ic2b0vc7iwm6a")))

(define-public crate-accesskit_consumer-0.15.1 (c (n "accesskit_consumer") (v "0.15.1") (d (list (d (n "accesskit") (r "^0.11.1") (d #t) (k 0)))) (h "1cqpk2aqmnp165llfqp9ncjw9nj6g6bm657j88zx124ykdnnsd0l")))

(define-public crate-accesskit_consumer-0.15.2 (c (n "accesskit_consumer") (v "0.15.2") (d (list (d (n "accesskit") (r "^0.11.2") (d #t) (k 0)))) (h "007vb2z7cphhdgdivbip5xgd0pfqwpdxbl2pvx3hvzkj8yg4vfq4")))

(define-public crate-accesskit_consumer-0.16.0 (c (n "accesskit_consumer") (v "0.16.0") (d (list (d (n "accesskit") (r "^0.12.0") (d #t) (k 0)))) (h "1x8llw0jl05z2lg4j4yp5k12k99grnnh4jxgv8jvbhdh5v9wzprr")))

(define-public crate-accesskit_consumer-0.16.1 (c (n "accesskit_consumer") (v "0.16.1") (d (list (d (n "accesskit") (r "^0.12.1") (d #t) (k 0)))) (h "1rj5vsaxn9m5aazr22vzlb5bxfbl28h2mck7hqldgyq97jjwq5wc")))

(define-public crate-accesskit_consumer-0.17.0 (c (n "accesskit_consumer") (v "0.17.0") (d (list (d (n "accesskit") (r "^0.12.2") (d #t) (k 0)))) (h "167mc5pkalcgw66r1y6dx6g32j48zm2fma60d7932spjhi76ailq")))

(define-public crate-accesskit_consumer-0.17.1 (c (n "accesskit_consumer") (v "0.17.1") (d (list (d (n "accesskit") (r "^0.12.3") (d #t) (k 0)))) (h "054w6q8s3zpkvfgx9rvc276gkwi4jglsyag9715y40jvglalq4fn") (r "1.68.2")))

(define-public crate-accesskit_consumer-0.18.0 (c (n "accesskit_consumer") (v "0.18.0") (d (list (d (n "accesskit") (r "^0.13.0") (d #t) (k 0)))) (h "1v6yhakl8f8x22rxv9qw7fsciamp6jyxf6h57c5j9b7xq2f04cg9") (r "1.68.2")))

(define-public crate-accesskit_consumer-0.19.0 (c (n "accesskit_consumer") (v "0.19.0") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)))) (h "1xmn90j56ngdrq4a4pkcgf7jk1i9q0j4ip5sjxj3a90bx76c56rj") (r "1.70")))

(define-public crate-accesskit_consumer-0.19.1 (c (n "accesskit_consumer") (v "0.19.1") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)))) (h "0520dqc4g133022pps6a8fz8x49f6im1cwjz76llc3fkac3x6gpj") (r "1.70")))

(define-public crate-accesskit_consumer-0.20.0 (c (n "accesskit_consumer") (v "0.20.0") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)) (d (n "immutable-chunkmap") (r "^2.0.4") (d #t) (k 0)))) (h "0lz1vpdl536q0rix1dhbc4cqmv25iy7cq3faxxa47gihxg7xy4a1") (r "1.70")))

(define-public crate-accesskit_consumer-0.21.0 (c (n "accesskit_consumer") (v "0.21.0") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)) (d (n "immutable-chunkmap") (r "^2.0.5") (d #t) (k 0)))) (h "187xksixpncbmyjyjmrsq4va1zgwajxai0cjbzgzl2cjly3qcvpw") (r "1.70")))

(define-public crate-accesskit_consumer-0.22.0 (c (n "accesskit_consumer") (v "0.22.0") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)) (d (n "immutable-chunkmap") (r "^2.0.5") (d #t) (k 0)))) (h "1ad83hh3ny7bs9b92bi5vwrdhflbvxl5byfyab3vjvnximis3prv") (r "1.70")))

