(define-module (crates-io ac ce access-control) #:use-module (crates-io))

(define-public crate-access-control-0.0.1 (c (n "access-control") (v "0.0.1") (h "1y5yij5pn86k4zbglcn5kanjgfnr8r5zfyhxn2gl8c11ccqvkvg4")))

(define-public crate-access-control-0.0.2 (c (n "access-control") (v "0.0.2") (h "02s2lj99zdxb6ij5cxch9cla8fkw43alz4jvxpslbrp3rvd83hbb")))

(define-public crate-access-control-0.0.3 (c (n "access-control") (v "0.0.3") (h "070h6wc5ng86f2chj0j1rphs4xk7iyyyncy20xw1dli3s56diwvc")))

