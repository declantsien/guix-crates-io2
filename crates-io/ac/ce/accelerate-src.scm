(define-module (crates-io ac ce accelerate-src) #:use-module (crates-io))

(define-public crate-accelerate-src-0.3.0 (c (n "accelerate-src") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1vlgxclndwhgrj17hdvlyr0ycirz09qap6v41fzlkf5flfq9y3fi")))

(define-public crate-accelerate-src-0.3.1 (c (n "accelerate-src") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1skidig30hm4i49c6bdlcasplbg3122lxwigvws7hmc5p85v67v6")))

(define-public crate-accelerate-src-0.3.2 (c (n "accelerate-src") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "17fiqyq7f9k41pbsyrvk9pxyx9z6fw399wq036cvwkbmb14xcpj1") (l "Accelerate.framework")))

