(define-module (crates-io ac ce accept-encoding) #:use-module (crates-io))

(define-public crate-accept-encoding-0.1.0 (c (n "accept-encoding") (v "0.1.0") (d (list (d (n "derive_is_enum_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "1c5nbq53n6bxww34sk29fdrw34hhdyiw92xsi83lfc4asqjif74x")))

(define-public crate-accept-encoding-0.2.0-alpha.1 (c (n "accept-encoding") (v "0.2.0-alpha.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "0bx84ygb9kl0vf3a5gv2g68l1mc0vwsikmk7z0mzzzdqrixq9vvi")))

(define-public crate-accept-encoding-0.2.0-alpha.2 (c (n "accept-encoding") (v "0.2.0-alpha.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "01hmxvcfxid8za8cjgkfpj6i4pxqhir7rd4mcrmxlrv25v2j58rh")))

