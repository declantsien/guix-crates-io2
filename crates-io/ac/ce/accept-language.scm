(define-module (crates-io ac ce accept-language) #:use-module (crates-io))

(define-public crate-accept-language-1.0.0 (c (n "accept-language") (v "1.0.0") (h "0za763jvzwhalslarhm1gqy5jwgpv971qjzrhp2fdq26i8dcxgdn")))

(define-public crate-accept-language-1.0.1 (c (n "accept-language") (v "1.0.1") (h "04sgsm5n0lqvq2sazgyiyik5pb2imskps52qsz4n25kn824jbzdg")))

(define-public crate-accept-language-1.1.0 (c (n "accept-language") (v "1.1.0") (h "0j4h9j3fs575795f0iqf20biy5rk0m2wdv4qshp23byp5gh3yh48")))

(define-public crate-accept-language-1.1.1 (c (n "accept-language") (v "1.1.1") (h "1v2nghmpybn51bbk0mg9wqr0hw90hx31s9rbaq0q6ds63lwynd0s")))

(define-public crate-accept-language-1.2.0 (c (n "accept-language") (v "1.2.0") (h "1wrssh2xqdb2rjyns86j6xy7ax43alwfqrp0qnh7nlgf8k1d1qap")))

(define-public crate-accept-language-1.2.1 (c (n "accept-language") (v "1.2.1") (h "1b2p2cm27461lb7r0bvhvax1hblqa9i1q6wm1zjc52ads9lhrzkn")))

(define-public crate-accept-language-1.2.2 (c (n "accept-language") (v "1.2.2") (h "1fcc5nxhi7m5r4hirh17zgykkyqa95inlkghzr31gvvndmlpdz47")))

(define-public crate-accept-language-2.0.0 (c (n "accept-language") (v "2.0.0") (h "1g4ykfm10hbkjjzq0kxpgcazdp0igj9pxfrkwwqrqzmpr7zj3g62")))

(define-public crate-accept-language-3.0.0 (c (n "accept-language") (v "3.0.0") (h "0g1ldgvpvzrv70dxbji567ialb88smagqffjxi3n8pp16bc90rw5")))

(define-public crate-accept-language-3.0.1 (c (n "accept-language") (v "3.0.1") (h "1x8hb5j137p2r4kw0b8n3z2vdjx6p4gsd7bvc74lclrb904nyhfa")))

(define-public crate-accept-language-3.1.0 (c (n "accept-language") (v "3.1.0") (h "0wip25cma82kw6c1c5aaiw26vg14npd21qv6dymzqc2855sx09wg")))

