(define-module (crates-io ac ce accessory) #:use-module (crates-io))

(define-public crate-accessory-1.0.0 (c (n "accessory") (v "1.0.0") (d (list (d (n "macroific") (r "^1.0.1") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mrjrs6kkj69xp9rqhi8x1f0ipmgwaw6bwgfan7pbsir6w7whjzc") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

(define-public crate-accessory-1.1.0 (c (n "accessory") (v "1.1.0") (d (list (d (n "macroific") (r "^1.0.1") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ba95gizpxvg6kzsfzjr6f56x5bm3craxsa408a21fzs4c49rykx") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

(define-public crate-accessory-1.2.0 (c (n "accessory") (v "1.2.0") (d (list (d (n "macroific") (r "^1.2.1") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wp5j5092l0mikx83l1g340x2cn2xwm1c91b8crfns3mj3n1wrkf") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

(define-public crate-accessory-1.2.1 (c (n "accessory") (v "1.2.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "14qfv3nm30189g52hwdg2zvd50msdl3h57r554s7y7dzspmbmc0z") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

(define-public crate-accessory-1.3.0 (c (n "accessory") (v "1.3.0") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02sfmgmr4fpgh4y24gn3dp7y99r5dnnk07dppd7p816wp4sba2w5") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

(define-public crate-accessory-1.3.1 (c (n "accessory") (v "1.3.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1piwnv22yid58kmxir3b28vygxarb6r1s6mxirdqv9ygwyd7ylw7") (f (quote (("nightly" "macroific/nightly") ("_debug" "syn/extra-traits")))) (r "1.60.0")))

