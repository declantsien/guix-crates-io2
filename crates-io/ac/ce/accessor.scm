(define-module (crates-io ac ce accessor) #:use-module (crates-io))

(define-public crate-accessor-0.1.0 (c (n "accessor") (v "0.1.0") (h "04fcg83qi47h39m2zxiy58hgzys3dvf99ff2rj3j4q5adl0z1fpy")))

(define-public crate-accessor-0.2.0 (c (n "accessor") (v "0.2.0") (h "012brw89032qly7sq0q9cdqxjzjhlvdsf7xgs499932133p02zla")))

(define-public crate-accessor-0.3.0 (c (n "accessor") (v "0.3.0") (h "0mx88m94jf82705py15ha4y0m0lgvrdh2h1g2fsaacv98yl31y47")))

(define-public crate-accessor-0.3.1 (c (n "accessor") (v "0.3.1") (h "0ngqzd4arwdsp71qxfl8jrcqf828yd3vq4jzb2iwnyyk6i86ydma")))

(define-public crate-accessor-0.3.2 (c (n "accessor") (v "0.3.2") (h "1vf43kqz19izw4lr8a5461zmqpnksn6lzpbkl83cxd4c68afxhhl")))

(define-public crate-accessor-0.3.3 (c (n "accessor") (v "0.3.3") (h "11hh6nl9qc61414jhlr580qrs6n5cr2mkz80pzzrq7xzanyjm2xx")))

