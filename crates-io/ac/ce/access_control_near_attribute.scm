(define-module (crates-io ac ce access_control_near_attribute) #:use-module (crates-io))

(define-public crate-access_control_near_attribute-0.1.0 (c (n "access_control_near_attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10vw2ng8v2h8y8gcpw5gwhnar5zfszfypybjgclsayrv1205zy7f") (y #t)))

(define-public crate-access_control_near_attribute-0.1.1 (c (n "access_control_near_attribute") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1miysx3p8hbpdsxw281l55dqils2pc2gx4gzdwcsxc7b5w46lmmv") (y #t)))

