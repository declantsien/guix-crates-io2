(define-module (crates-io ac ce accelerate-provider) #:use-module (crates-io))

(define-public crate-accelerate-provider-0.1.0 (c (n "accelerate-provider") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 2)))) (h "199da9i0j7dqw2j4jrca0jdxh7c8al7q0b51jzmk71hz0w27z4v1")))

(define-public crate-accelerate-provider-0.1.1 (c (n "accelerate-provider") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 2)))) (h "15bbpls3869avffklp91fh9xpcqsh794qn5kqy63qy8564cnf88v")))

(define-public crate-accelerate-provider-0.1.2 (c (n "accelerate-provider") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0hs4l77hh5hvibc582xnzv2pl6pd5ijv5hghbwq9zngh5jqj7ky0")))

(define-public crate-accelerate-provider-0.2.0 (c (n "accelerate-provider") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0hkbv6p4j0m79plh92hg71qdj41wwyhdclpqhqa4791hs9g5h8fw")))

(define-public crate-accelerate-provider-0.2.1 (c (n "accelerate-provider") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yg8k6y41bps0np5lxkrkj4vlblny3gnvcfhhq3q7qv7rphk0lbi")))

