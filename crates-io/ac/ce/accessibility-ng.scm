(define-module (crates-io ac ce accessibility-ng) #:use-module (crates-io))

(define-public crate-accessibility-ng-0.1.6 (c (n "accessibility-ng") (v "0.1.6") (d (list (d (n "accessibility-sys-ng") (r "^0.1.3") (d #t) (k 0)) (d (n "cocoa") (r "^0.24") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (d #t) (k 0)) (d (n "core-graphics-types") (r "^0.1.1") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05m77w617nv2g4jjzxpgjqsl5689w087a8mjl0r5yklkkka2ijyp")))

