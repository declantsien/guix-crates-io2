(define-module (crates-io ac ry acrylic-png) #:use-module (crates-io))

(define-public crate-acrylic-png-0.1.26 (c (n "acrylic-png") (v "0.1.26") (d (list (d (n "acrylic") (r "^0.1.26") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1s0bzgzkyazjk770xc691ivz41zk9gdpjwnwxlgl508c9ynxvwn0")))

(define-public crate-acrylic-png-0.1.28 (c (n "acrylic-png") (v "0.1.28") (d (list (d (n "acrylic") (r "^0.1.28") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1kkhzk92a5r8mz2i6zxcybmkl72pz82wv8i1729dhx1i93gsy374")))

(define-public crate-acrylic-png-0.1.29 (c (n "acrylic-png") (v "0.1.29") (d (list (d (n "acrylic") (r "^0.1.29") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "086k1sjd6f4s773z9bvsz7mcdcgrp703s3gxqqmaz7afb3jhk053")))

(define-public crate-acrylic-png-0.1.30 (c (n "acrylic-png") (v "0.1.30") (d (list (d (n "acrylic") (r "^0.1.30") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "03qxkhyj3rwm7bl9x8ykm3397gjf44lfaakhibsa7d2kwm7dgwrv")))

