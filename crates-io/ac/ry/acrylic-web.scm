(define-module (crates-io ac ry acrylic-web) #:use-module (crates-io))

(define-public crate-acrylic-web-0.1.9 (c (n "acrylic-web") (v "0.1.9") (d (list (d (n "acrylic") (r "^0.1.9") (d #t) (k 0)))) (h "1595jns921c2g9fcr7kxjg3ywqa6pjsqjrsw4yfqixpy678in2q6")))

(define-public crate-acrylic-web-0.1.10 (c (n "acrylic-web") (v "0.1.10") (d (list (d (n "acrylic") (r "^0.1.10") (d #t) (k 0)))) (h "0rp2z3d50j9mhzkyvdcwq9f2bw0yl71y57f23ky91831p6zzjn08")))

(define-public crate-acrylic-web-0.1.11 (c (n "acrylic-web") (v "0.1.11") (d (list (d (n "acrylic") (r "^0.1.11") (d #t) (k 0)))) (h "195izvp0dnhh49l88l4qphjgm6z3b62g2n0w239wfmzqi5cx4rxa")))

(define-public crate-acrylic-web-0.1.12 (c (n "acrylic-web") (v "0.1.12") (d (list (d (n "acrylic") (r "^0.1.12") (d #t) (k 0)))) (h "1al4lb18160yi4q651vxyz89pf1i5bk312dlwbxyw796mj6pjnf4")))

(define-public crate-acrylic-web-0.1.13 (c (n "acrylic-web") (v "0.1.13") (d (list (d (n "acrylic") (r "^0.1.13") (d #t) (k 0)))) (h "1bpvayph8qq4w46k7w9r8d6lzf3j6b7bkpcyhwnzj4pzawy5ajdb")))

(define-public crate-acrylic-web-0.1.14 (c (n "acrylic-web") (v "0.1.14") (d (list (d (n "acrylic") (r "^0.1.14") (d #t) (k 0)))) (h "14zv0hpp1qy5h81lkh386gcfch3qg261x3ymczvhm7dfdcghsrff")))

(define-public crate-acrylic-web-0.1.15 (c (n "acrylic-web") (v "0.1.15") (d (list (d (n "acrylic") (r "^0.1.15") (d #t) (k 0)))) (h "168kkxk8z2i0hi6qf3sank2bj3lqly42556b4qsr1j1vs4r2jvkg")))

(define-public crate-acrylic-web-0.1.16 (c (n "acrylic-web") (v "0.1.16") (d (list (d (n "acrylic") (r "^0.1.16") (d #t) (k 0)))) (h "0xxhcm9dxs4hjw9s11f00mdd9shvrifaq9qyp4q601hm09wwnn5r")))

(define-public crate-acrylic-web-0.1.17 (c (n "acrylic-web") (v "0.1.17") (d (list (d (n "acrylic") (r "^0.1.17") (d #t) (k 0)))) (h "1ymn6y3flsalbk2yl292zgp9s7ncspbpqqrdbw8lw6jxhab1ix12")))

(define-public crate-acrylic-web-0.1.18 (c (n "acrylic-web") (v "0.1.18") (d (list (d (n "acrylic") (r "^0.1.18") (d #t) (k 0)))) (h "0yfv51z9xmywc87srcy9vynbzzkij65ymxvlmrqbym5g5qv6mdcj")))

(define-public crate-acrylic-web-0.1.19 (c (n "acrylic-web") (v "0.1.19") (d (list (d (n "acrylic") (r "^0.1.19") (d #t) (k 0)))) (h "145lh5085jgjldyv1z97hbzm6x5n78b88vgxd1vp13mcj05p10q9")))

(define-public crate-acrylic-web-0.1.20 (c (n "acrylic-web") (v "0.1.20") (d (list (d (n "acrylic") (r "^0.1.20") (d #t) (k 0)))) (h "1d9kldxvx193fbv2pgybiw0y49f2bh4j3i9p5hxp8wclqvssjil8")))

(define-public crate-acrylic-web-0.1.21 (c (n "acrylic-web") (v "0.1.21") (d (list (d (n "acrylic") (r "^0.1.21") (d #t) (k 0)))) (h "0dksgh3pzl34iag2xfl13jq97lcbgdwp0sgym1ic3pz4afn63iyn")))

(define-public crate-acrylic-web-0.1.22 (c (n "acrylic-web") (v "0.1.22") (d (list (d (n "acrylic") (r "^0.1.22") (d #t) (k 0)))) (h "196qxj32k19jkzl1387hxshqh4pb8sqwi896wf9pdi4zikyr4r67")))

(define-public crate-acrylic-web-0.1.23 (c (n "acrylic-web") (v "0.1.23") (d (list (d (n "acrylic") (r "^0.1.23") (d #t) (k 0)))) (h "1g6iw62x0lmipb985mwh0dj899v3whbwyy1bx52bals3zza9xq61")))

(define-public crate-acrylic-web-0.1.24 (c (n "acrylic-web") (v "0.1.24") (d (list (d (n "acrylic") (r "^0.1.24") (d #t) (k 0)))) (h "1cm9p24ay2zp253yhn9iqp9gq6nxq5fsmc75l6azvqvjaqwzpz0f")))

(define-public crate-acrylic-web-0.1.25 (c (n "acrylic-web") (v "0.1.25") (d (list (d (n "acrylic") (r "^0.1.25") (d #t) (k 0)))) (h "0irvqrss64rah4i03swl790gmqqfr4mmlrrncjw5jgmsxgi8w8c2")))

(define-public crate-acrylic-web-0.1.26 (c (n "acrylic-web") (v "0.1.26") (d (list (d (n "acrylic") (r "^0.1.26") (d #t) (k 0)))) (h "1fab64x6i5gshy5w68ka8a9df5ckn2swwhfkshlsjg1mvm6h958z")))

(define-public crate-acrylic-web-0.1.28 (c (n "acrylic-web") (v "0.1.28") (d (list (d (n "acrylic") (r "^0.1.28") (d #t) (k 0)))) (h "12ayk81lgpyykg9hhv96cjgqr9k8r8jnjfnmh27w8jrwgal2bm98")))

(define-public crate-acrylic-web-0.1.29 (c (n "acrylic-web") (v "0.1.29") (d (list (d (n "acrylic") (r "^0.1.29") (d #t) (k 0)))) (h "0bv9s9diqgiirw46faxwkqcg357hsfrh28zrfwg6hjgycv2lqrki")))

(define-public crate-acrylic-web-0.1.30 (c (n "acrylic-web") (v "0.1.30") (d (list (d (n "acrylic") (r "^0.1.30") (d #t) (k 0)))) (h "0b9vp8jjx2b7nh7bk2gjgjzxrb7b1r9avv5a96xg50n1ikgaplpm")))

(define-public crate-acrylic-web-0.2.0 (c (n "acrylic-web") (v "0.2.0") (d (list (d (n "acrylic") (r "^0.2.0") (d #t) (k 0)))) (h "0k89pkwfck7x60nw4l2pas30gzvjpb5q5k1i2sazdbp7laz0irrz")))

(define-public crate-acrylic-web-0.2.1 (c (n "acrylic-web") (v "0.2.1") (d (list (d (n "acrylic") (r "^0.2.1") (d #t) (k 0)))) (h "0zp0rny13fjbms18w0xdikridxsf45c5db415w7z9wm2kkpm7b31")))

(define-public crate-acrylic-web-0.2.2 (c (n "acrylic-web") (v "0.2.2") (d (list (d (n "acrylic") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1flwv1f1nqrr9ifsxw29bmw2war3939blw7gaapb94axk9lqqm3y")))

(define-public crate-acrylic-web-0.2.3 (c (n "acrylic-web") (v "0.2.3") (d (list (d (n "acrylic") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12v0dvzsripm8fa6p06cx0l6k0pj548bwfz1qx9sk51f5880292s")))

(define-public crate-acrylic-web-0.2.4 (c (n "acrylic-web") (v "0.2.4") (d (list (d (n "acrylic") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0cj12vy11yfzq6b5dr934ghxnhn0npwxpxz2cng9qysly8pm24mr")))

(define-public crate-acrylic-web-0.3.1 (c (n "acrylic-web") (v "0.3.1") (d (list (d (n "acrylic") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "197bawwm296k7qzw25ajnlhlsfxn26y9vg3clbkgs1rlc4hk676v")))

(define-public crate-acrylic-web-0.3.4 (c (n "acrylic-web") (v "0.3.4") (d (list (d (n "acrylic") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0fdrw5rmx751p6l53gyhdjp3888vdkv0hd99ssh1v7aix40b2bgx")))

(define-public crate-acrylic-web-0.3.5 (c (n "acrylic-web") (v "0.3.5") (d (list (d (n "acrylic") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "08f5j0mri7pd1l7vr1r0rcvk26ipj3b46mpcl0dsr8gkph7pviya")))

