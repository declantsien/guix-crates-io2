(define-module (crates-io ac al acall-cli) #:use-module (crates-io))

(define-public crate-acall-cli-0.1.0 (c (n "acall-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^2.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1zr6x8yl776dxf9nc0qkx2cgpxz5sv57vak29yr0a7l779bnjs3q")))

