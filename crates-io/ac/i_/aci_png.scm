(define-module (crates-io ac i_ aci_png) #:use-module (crates-io))

(define-public crate-aci_png-0.1.0 (c (n "aci_png") (v "0.1.0") (d (list (d (n "png") (r "^0.9") (d #t) (k 0)))) (h "1ms55rxdsw09xx6k77fdjypdkz04n6nv2572rb9g4nhjk9l1dld8")))

(define-public crate-aci_png-0.2.0 (c (n "aci_png") (v "0.2.0") (d (list (d (n "png") (r "^0.9") (d #t) (k 0)))) (h "0wpl77fcjn6547s0dd6nd38f3ini7zrn2825xynps2i3czvp4v3g")))

(define-public crate-aci_png-0.3.0 (c (n "aci_png") (v "0.3.0") (d (list (d (n "afi") (r "^0.1") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "01j0zqjgbhdz78zfd5hy72f28wrimcxlfzjn7qcgpi5aqcv5x20h")))

(define-public crate-aci_png-0.4.0 (c (n "aci_png") (v "0.4.0") (d (list (d (n "afi") (r "^0.2") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "1xa0s0b54h8312a5fcg89pa8687nni8kh0bxl997b52wh81c2p1j")))

(define-public crate-aci_png-0.5.0 (c (n "aci_png") (v "0.5.0") (d (list (d (n "afi") (r "^0.3") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "1pl60snvic0qnar358bqc8pcnrl843nawlb3g3ka0bqai4d07i9p")))

(define-public crate-aci_png-0.5.1 (c (n "aci_png") (v "0.5.1") (d (list (d (n "afi") (r "^0.3.1") (d #t) (k 0)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "1n92v3cnqd5qydzc7bkqlk2cdz96fz1746hjjwx04s8hrr94lwyq")))

(define-public crate-aci_png-0.5.2 (c (n "aci_png") (v "0.5.2") (d (list (d (n "afi") (r "^0.3.2") (d #t) (k 0)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "0ghkk9qzx82y2nsh2cy5kqmh30k18a1wifhqv6na63q1bg831rqr")))

(define-public crate-aci_png-0.6.0 (c (n "aci_png") (v "0.6.0") (d (list (d (n "afi") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0xv8ivc3dns9hzjgyd4smy7wh1cigd9qcwr2xmsi7xphnfp0iry9")))

(define-public crate-aci_png-0.6.1 (c (n "aci_png") (v "0.6.1") (d (list (d (n "afi") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "15says7vhan2v6ypdpyj707jcip48bwinn9j6fscir4i6863iiy8")))

(define-public crate-aci_png-0.6.2 (c (n "aci_png") (v "0.6.2") (d (list (d (n "afi") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "deflate") (r "^0.7") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1msn2d93v4kfh93x7b7z4bbw05csgj7hh9wnyffwaq62dqdh7w8d")))

(define-public crate-aci_png-0.7.0 (c (n "aci_png") (v "0.7.0") (d (list (d (n "afi") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "deflate") (r "^0.7") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1gmz5bq9a2nw6qm1az4llyshk6m753q24hb3bhnkl9mhxw96jl08")))

(define-public crate-aci_png-0.8.0-pre0 (c (n "aci_png") (v "0.8.0-pre0") (d (list (d (n "afi") (r "^0.8.0-pre0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "deflate") (r "^0.7") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1r7h76m3f6mcr700j3gpxwil42l3l2jr0rs6hmqfghas7vay6fr0")))

