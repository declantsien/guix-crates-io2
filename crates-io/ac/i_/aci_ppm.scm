(define-module (crates-io ac i_ aci_ppm) #:use-module (crates-io))

(define-public crate-aci_ppm-0.1.0 (c (n "aci_ppm") (v "0.1.0") (h "0qi58msad98nlg737flxxqnj21j6mg3pxzl68g0dss2n2dk4by92")))

(define-public crate-aci_ppm-0.2.0 (c (n "aci_ppm") (v "0.2.0") (d (list (d (n "afi") (r "^0.1") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "0x5g273vfr6pnlk39hy7lnlvmynp02y8fsqgfqr6cxkrm5p2ma23")))

(define-public crate-aci_ppm-0.3.0 (c (n "aci_ppm") (v "0.3.0") (d (list (d (n "afi") (r "^0.2") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "0mgq1rdravz704v2s8l3vcjcndapv6ynj6bxh7m7sf4qrps6752h")))

(define-public crate-aci_ppm-0.4.0 (c (n "aci_ppm") (v "0.4.0") (d (list (d (n "afi") (r "^0.3") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "05f3r919l6a79c9rgn9wb92ns3pbz5lwdg993f31rkph299qzkgb")))

(define-public crate-aci_ppm-0.4.1 (c (n "aci_ppm") (v "0.4.1") (d (list (d (n "afi") (r "^0.3.1") (d #t) (k 0)))) (h "18ajawf8z380x5yyadar0xnk2xgs3xip2xasb6mlh1dm4h3hnmwz")))

(define-public crate-aci_ppm-0.4.2 (c (n "aci_ppm") (v "0.4.2") (d (list (d (n "afi") (r "^0.3.2") (d #t) (k 0)))) (h "147zn5apnzm35lh4j4n3czccy5b53146vj2k9ps1559dp7kcw9z0")))

(define-public crate-aci_ppm-0.5.0 (c (n "aci_ppm") (v "0.5.0") (d (list (d (n "afi") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)))) (h "0q09yxmp9d399x5xw6s0401mimihwgz0vv025a7bpw8fi013ns2c")))

(define-public crate-aci_ppm-0.7.0 (c (n "aci_ppm") (v "0.7.0") (d (list (d (n "aci_png") (r "^0.7") (d #t) (k 2)) (d (n "afi") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "09kaqgavizi1pms266gvqkzyg9cgqasdfs2mx8y8c8dhhg00gzdc")))

