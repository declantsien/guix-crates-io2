(define-module (crates-io ac vm acvm_stdlib) #:use-module (crates-io))

(define-public crate-acvm_stdlib-0.3.0 (c (n "acvm_stdlib") (v "0.3.0") (d (list (d (n "acir") (r "^0.3.0") (d #t) (k 0)) (d (n "acir_field") (r "^0.3.0") (d #t) (k 0)))) (h "0vlvry28xxmiay7k30ldsalq4n8zg0dw4v286f2i47ljl5zss9wv")))

(define-public crate-acvm_stdlib-0.3.1 (c (n "acvm_stdlib") (v "0.3.1") (d (list (d (n "acir") (r "^0.3.1") (d #t) (k 0)) (d (n "acir_field") (r "^0.3.1") (d #t) (k 0)))) (h "0521a9mn1dlz6810kzvxpf7rw2q9zlvklp6jayib842a7wcfzxdl")))

(define-public crate-acvm_stdlib-0.4.0-alpha (c (n "acvm_stdlib") (v "0.4.0-alpha") (d (list (d (n "acir") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "acir_field") (r "^0.3.1") (d #t) (k 0)))) (h "008yada8y6bmpj7baa6lyzg56svj9ys4kxz6lrxlp6w9f57sdhvy")))

(define-public crate-acvm_stdlib-0.4.0 (c (n "acvm_stdlib") (v "0.4.0") (d (list (d (n "acir") (r "^0.4.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "0brrxs9flx9cidc0nv30q25c1d6mjg5ymhsllwj88kwd86cfyhj0")))

(define-public crate-acvm_stdlib-0.4.1 (c (n "acvm_stdlib") (v "0.4.1") (d (list (d (n "acir") (r "^0.4.1") (f (quote ("bn254"))) (d #t) (k 0)))) (h "0yndprkmkk6c16y7rqx7hpj07a3ppdlcfns2bnb4xkf2f9xn3xpa")))

(define-public crate-acvm_stdlib-0.5.0 (c (n "acvm_stdlib") (v "0.5.0") (d (list (d (n "acir") (r "^0.5.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "0z5a8k1ai7f27i90r3p3wkz1cm3n5anga725j7apxprllm9czaxa")))

(define-public crate-acvm_stdlib-0.6.0 (c (n "acvm_stdlib") (v "0.6.0") (d (list (d (n "acir") (r "^0.6.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "1f2337vq6z9wk1vzi2qfd9s07lx85nrdxcfx3gj55dkdn0hrq9dj")))

(define-public crate-acvm_stdlib-0.7.0 (c (n "acvm_stdlib") (v "0.7.0") (d (list (d (n "acir") (r "^0.7.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "08r2j85nnpxzl0wcla7nqbz8j1msp1bp1wdmr6b414wjhk4nwzjb")))

(define-public crate-acvm_stdlib-0.7.1 (c (n "acvm_stdlib") (v "0.7.1") (d (list (d (n "acir") (r "^0.7.1") (f (quote ("bn254"))) (d #t) (k 0)))) (h "0bm31w476mhmjaqsmss4x6pqpx50mfj3l313dm8ar92bchvg6hxh")))

(define-public crate-acvm_stdlib-0.8.0 (c (n "acvm_stdlib") (v "0.8.0") (d (list (d (n "acir") (r "^0.8.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "13wy8ikrixk7bjnw2ysyaqxh16nr38d8zgx2jcywlclpzqcbqaxa")))

(define-public crate-acvm_stdlib-0.8.1 (c (n "acvm_stdlib") (v "0.8.1") (d (list (d (n "acir") (r "^0.8.1") (f (quote ("bn254"))) (d #t) (k 0)))) (h "1cbam9dpbqckagh9dy4ppa99lr94j785gffnmqddzh64j1lsh0rn")))

(define-public crate-acvm_stdlib-0.9.0 (c (n "acvm_stdlib") (v "0.9.0") (d (list (d (n "acir") (r "^0.9.0") (f (quote ("bn254"))) (d #t) (k 0)))) (h "12b97rxq3c8b8w7ff6zjdppk67h38fdr1v8myhdzzryim4cjvkik")))

(define-public crate-acvm_stdlib-0.10.2 (c (n "acvm_stdlib") (v "0.10.2") (d (list (d (n "acir") (r "^0.10.2") (k 0)))) (h "1qfwmxzxf9iknkqq41ya8h54vw0n1r5igwvxkfig94x61w72yhxn") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.10.3 (c (n "acvm_stdlib") (v "0.10.3") (d (list (d (n "acir") (r "^0.10.3") (k 0)))) (h "1pa89b38d8w9im3rff2v77yqclv7j4iql0jsq5fsgsv61hb53v55") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.11.0 (c (n "acvm_stdlib") (v "0.11.0") (d (list (d (n "acir") (r "^0.11.0") (k 0)))) (h "1aa4d85mcil8rpiqx2q7z0hh614fbmazhl7zq1027hbss59syc9i") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.12.0 (c (n "acvm_stdlib") (v "0.12.0") (d (list (d (n "acir") (r "^0.12.0") (k 0)))) (h "0jbaljwf820h11gh69xrgn0yabrz8h30168237mkwnc7p54pbs52") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.13.0 (c (n "acvm_stdlib") (v "0.13.0") (d (list (d (n "acir") (r "^0.13.0") (k 0)))) (h "0kv7b1g139qq4kici5yqllc9isap340ijk7cspkbjkd9ay0n5j1g") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.13.1 (c (n "acvm_stdlib") (v "0.13.1") (d (list (d (n "acir") (r "^0.13.1") (k 0)))) (h "08cw1ygrshj7h8ccv4cj8c09r88znccpn67c1rlci36yf60dm18r") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.13.2 (c (n "acvm_stdlib") (v "0.13.2") (d (list (d (n "acir") (r "^0.13.2") (k 0)))) (h "13pp06v5a8j3m3va738wrlmdid5q8iplrdjjscl2fh1hxx568mnq") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.13.3 (c (n "acvm_stdlib") (v "0.13.3") (d (list (d (n "acir") (r "^0.13.3") (k 0)))) (h "11538yxpggv2rj2hbpdy3a8xghj9i843w231rp713gxw35xq7qzz") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.14.0 (c (n "acvm_stdlib") (v "0.14.0") (d (list (d (n "acir") (r "^0.14.0") (k 0)))) (h "1sk7w36ra39rqj3dvdap8aigfd5h2c88ih6vm7dzffp8m26hd2r1") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.14.1 (c (n "acvm_stdlib") (v "0.14.1") (d (list (d (n "acir") (r "^0.14.1") (k 0)))) (h "1v0acyxk79pn1b8v47ip09fmph0qh6j2kl2i2mbvhqhjb0bdr6lw") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.14.2 (c (n "acvm_stdlib") (v "0.14.2") (d (list (d (n "acir") (r "^0.14.2") (k 0)))) (h "0w85mi4f0vfqs4bq3x9aabdvzyj7glgfzh511h4nyj1v3sdz76sc") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.14.3 (c (n "acvm_stdlib") (v "0.14.3") (d (list (d (n "acir") (r "^0.14.3") (k 0)))) (h "1ssbd0lyra9i8ibnh0bfmwyk5fabzcvz7g3wxii1dyzcr7alhbxl") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.14.4 (c (n "acvm_stdlib") (v "0.14.4") (d (list (d (n "acir") (r "^0.14.4") (k 0)))) (h "0bz7ysg48zqi8gc8vwzh5fbivf61xc91ajikfi34pis6n5zl26ja") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.15.0 (c (n "acvm_stdlib") (v "0.15.0") (d (list (d (n "acir") (r "^0.15.0") (k 0)))) (h "077i0yicdy7008cd9ghv3ykn3f6vldaxdqnd67nylynqa2irwy49") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.15.1 (c (n "acvm_stdlib") (v "0.15.1") (d (list (d (n "acir") (r "^0.15.1") (k 0)))) (h "17mnim8mmk2jb3vkjqppskbma4xk0pd0scpbc0zrrpj7vkq1hsx1") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.16.0 (c (n "acvm_stdlib") (v "0.16.0") (d (list (d (n "acir") (r "^0.16.0") (k 0)))) (h "1p3ycv61aj8acalqyqmwny59bb50g8qz2di6g1k8ip4xdylzxc2p") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.17.0 (c (n "acvm_stdlib") (v "0.17.0") (d (list (d (n "acir") (r "^0.17.0") (k 0)))) (h "0qjgk7bm0xn4vi5b072kds87ypjj10s7h7dgg1hc4np50y22sz8g") (f (quote (("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.18.1 (c (n "acvm_stdlib") (v "0.18.1") (d (list (d (n "acir") (r "^0.18.1") (k 0)))) (h "0cln3y9pawpf8q5i7318sdlwhkrpz4ia8mz99kiq7h5d3wib4dlh") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.18.2 (c (n "acvm_stdlib") (v "0.18.2") (d (list (d (n "acir") (r "^0.18.2") (k 0)))) (h "1paa5vjfqgzhbnfibf581hjgd4gw4r6i3kl4s7gfjdvv3mrplb4s") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.19.0 (c (n "acvm_stdlib") (v "0.19.0") (d (list (d (n "acir") (r "^0.19.0") (k 0)))) (h "0phxqw0wmlr04iyk7841xq1b7hkkw6vpmykn2m2qn197r7w2xdg4") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.19.1 (c (n "acvm_stdlib") (v "0.19.1") (d (list (d (n "acir") (r "^0.19.1") (k 0)))) (h "11jkhcim8b91cpk0hs28kjak9fm87bzmzqwk4ypi7jr1ss13iz47") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.20.0 (c (n "acvm_stdlib") (v "0.20.0") (d (list (d (n "acir") (r "^0.20.0") (k 0)))) (h "1w1gl9naig1r2f1mlxxisrkp58cd2bvldsyai5vb1apvg0fqgd9i") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.20.1 (c (n "acvm_stdlib") (v "0.20.1") (d (list (d (n "acir") (r "^0.20.1") (k 0)))) (h "0x5sbxgd11h2hzw5mikzny7ak9llvvm9hq326g2pwp3ld8j4vk4h") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.21.0 (c (n "acvm_stdlib") (v "0.21.0") (d (list (d (n "acir") (r "^0.21.0") (k 0)))) (h "1byb3qcjavy0xxkmvrb4fzlwa33b57q73p4i2xxjb7h9mj9qd5g7") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.22.0 (c (n "acvm_stdlib") (v "0.22.0") (d (list (d (n "acir") (r "^0.22.0") (k 0)))) (h "0hmb8s1d82zvi7nfffjwjg43cn972k595c1gr45s3sii6fyb71g4") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.23.0 (c (n "acvm_stdlib") (v "0.23.0") (d (list (d (n "acir") (r "^0.23.0") (k 0)))) (h "07yywl6zarad7j4pdx55lvqvqq70glqralszbv0v0pkvq22bi28k") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.24.0 (c (n "acvm_stdlib") (v "0.24.0") (d (list (d (n "acir") (r "^0.24.0") (k 0)))) (h "09vpzbyljgdqrrd88zv0v16r6yh8381rkb3qjap1dvkyqcanjh1l") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.24.1 (c (n "acvm_stdlib") (v "0.24.1") (d (list (d (n "acir") (r "^0.24.1") (k 0)))) (h "1ayb1q6s06d7izrg1v27cmg3vflc1qak2pzblhj7gmd73x85ypyp") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.25.0 (c (n "acvm_stdlib") (v "0.25.0") (d (list (d (n "acir") (r "^0.25.0") (k 0)))) (h "0bsj5hwv7dw4sy041br6pixpckpw6inja460vv0wqp3r82q4hy59") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.26.0 (c (n "acvm_stdlib") (v "0.26.0") (d (list (d (n "acir") (r "^0.26.0") (k 0)))) (h "0z5rfyd655zaavm7lq7a515cy7462f4yvnclgs5kwjyh2pvpi01z") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.26.1 (c (n "acvm_stdlib") (v "0.26.1") (d (list (d (n "acir") (r "^0.26.1") (k 0)))) (h "1nffscnv3fl2gnpy3b3fq0aqz5yrqz6m2qhfvdydan9azs042d73") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

(define-public crate-acvm_stdlib-0.27.0 (c (n "acvm_stdlib") (v "0.27.0") (d (list (d (n "acir") (r "^0.27.0") (k 0)))) (h "0l1l7ghhijykvhyr8bb40kxq68gx174yqgw40h3dajflqbygzmj4") (f (quote (("testing" "bn254") ("default" "bn254") ("bn254" "acir/bn254") ("bls12_381" "acir/bls12_381")))) (r "1.66")))

