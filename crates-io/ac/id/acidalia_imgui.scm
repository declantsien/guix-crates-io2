(define-module (crates-io ac id acidalia_imgui) #:use-module (crates-io))

(define-public crate-acidalia_imgui-0.1.0 (c (n "acidalia_imgui") (v "0.1.0") (d (list (d (n "acidalia") (r "^0.2") (d #t) (k 0)) (d (n "imgui") (r "^0.8") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.19") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8") (f (quote ("winit-26"))) (k 0)))) (h "1v05wxxm7br827n6x8l168hzmp96jpp208qr3c8nw8mchmw03lca")))

