(define-module (crates-io ac id acid_io) #:use-module (crates-io))

(define-public crate-acid_io-0.1.0 (c (n "acid_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "windows") (r "^0.29") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b5drmx2j6l4n1whfm0169wpil3ypyh0lkxssdpx9nqiidyaf21y") (f (quote (("std" "byteorder/std") ("default") ("alloc"))))))

