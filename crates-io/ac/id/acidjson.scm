(define-module (crates-io ac id acidjson) #:use-module (crates-io))

(define-public crate-acidjson-0.1.0 (c (n "acidjson") (v "0.1.0") (d (list (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fxz01ksxf80j0m0l41skpspvcxrpni1bg3yd56380790328b2la")))

(define-public crate-acidjson-0.1.1 (c (n "acidjson") (v "0.1.1") (d (list (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b5g8spm9rnggjf7wxa81fd44xiv96qzysbp133866fx4ywgpjmv")))

(define-public crate-acidjson-0.1.2 (c (n "acidjson") (v "0.1.2") (d (list (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mjzmaiw8cw2bw3fjyf7s65vd3cq53lwv00p1s3fzlyjvzzn5pbi")))

(define-public crate-acidjson-0.1.3 (c (n "acidjson") (v "0.1.3") (d (list (d (n "atomicwrites") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04x1cavmd8zgnpwyzfjsfl71sdfawpvgvp1jzz6inavkyq0k214z")))

