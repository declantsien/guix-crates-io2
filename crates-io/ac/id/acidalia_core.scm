(define-module (crates-io ac id acidalia_core) #:use-module (crates-io))

(define-public crate-acidalia_core-0.1.0 (c (n "acidalia_core") (v "0.1.0") (h "0g6dq12j6bxjmmdq7jpjrh98wrzlr4anpzzymqchm1slp9qm3s8j")))

(define-public crate-acidalia_core-0.2.0 (c (n "acidalia_core") (v "0.2.0") (h "14mlf7289b31pyhhcmcayl8fwm41fs8s26pq6hmkm4d27cqhgy4y")))

