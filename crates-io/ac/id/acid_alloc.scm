(define-module (crates-io ac id acid_alloc) #:use-module (crates-io))

(define-public crate-acid_alloc-0.1.0 (c (n "acid_alloc") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "sptr") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0j43mq6l3sljjj909xl6q1iga93mbzyj46pp8l88z74w1jf4z35j") (f (quote (("unstable") ("default" "sptr") ("alloc"))))))

