(define-module (crates-io ac id acid2) #:use-module (crates-io))

(define-public crate-acid2-0.1.0 (c (n "acid2") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0z92qd4gahmga84akz0f42j8ymqby119a4yjw3nv3343l7kjf3sx") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

(define-public crate-acid2-0.1.1 (c (n "acid2") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "05phxk5r9abaa972xbmchxwvxpyk3ks3wragp1yp5zj13m6hkl1a") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

(define-public crate-acid2-0.1.2 (c (n "acid2") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1l22ny4m9nlrd7ds70h67h6gpgj95a2v482z2fb8zbj5ifxwivq1") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

(define-public crate-acid2-0.1.3 (c (n "acid2") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0w19ybbxs6d78cidqxrahz2744l57w30q2li656pww05fwcj6r2p") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

(define-public crate-acid2-0.2.0 (c (n "acid2") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0ak0lj7awfz56vln1gkkdpda5m2xmnh48xrjizd0lhh8k9dgdrpf") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

(define-public crate-acid2-0.2.1 (c (n "acid2") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1b2yx7d0g9xpmi14fyxn9dh2pa4yn83hk2aawq5f4lprd0pwykmf") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand" "dep:rand_distr"))))))

