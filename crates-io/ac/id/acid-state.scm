(define-module (crates-io ac id acid-state) #:use-module (crates-io))

(define-public crate-acid-state-0.1.0 (c (n "acid-state") (v "0.1.0") (d (list (d (n "bincode") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1njl0p57cx5rv4g8lf3xlbqiqv3h19ardsza2hac4ix89gs5dazh")))

(define-public crate-acid-state-0.2.3 (c (n "acid-state") (v "0.2.3") (d (list (d (n "bincode") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b4a4wz9xms1fy59xi6x31z1nrl8dyrl0y7pxr5ifh1pb3za80p0")))

