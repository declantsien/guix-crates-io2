(define-module (crates-io ac id acid) #:use-module (crates-io))

(define-public crate-acid-0.1.0 (c (n "acid") (v "0.1.0") (h "1rp0rlj7s3iypsmy2p7n8akd01svkmksnzcg6a799il5avnjv03r")))

(define-public crate-acid-0.1.1 (c (n "acid") (v "0.1.1") (h "0kw1jbxxj4pxbd0aq8mhav8rb2d4b0nnj0h3i6p58vf6zznqyxai")))

