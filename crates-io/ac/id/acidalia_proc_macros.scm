(define-module (crates-io ac id acidalia_proc_macros) #:use-module (crates-io))

(define-public crate-acidalia_proc_macros-0.1.0 (c (n "acidalia_proc_macros") (v "0.1.0") (d (list (d (n "acidalia_core") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mifimaa5xlmlii6bvwl166z30v69ic2a8ssa2why946gdrvi2x5")))

(define-public crate-acidalia_proc_macros-0.2.0 (c (n "acidalia_proc_macros") (v "0.2.0") (d (list (d (n "acidalia_core") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fvbwijbj7yvpn8s2fr4ad23104pb1n31mpx5g18h7181n53v9yn")))

