(define-module (crates-io ac ty actypes) #:use-module (crates-io))

(define-public crate-actypes-0.1.0 (c (n "actypes") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.31.18") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.107.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.137.4") (f (quote ("typescript"))) (d #t) (k 0)))) (h "05d7c2qgc52az2d9axahgw37yv8v6pz063xndvmq0ggkb0hpnkwc")))

(define-public crate-actypes-0.2.0 (c (n "actypes") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.32.1") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.109.1") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.140.0") (f (quote ("typescript"))) (d #t) (k 0)))) (h "0jbbg70vwj31v6yj7k2gfhahx7v9981n6i9dl7l57b9y9cralr7x")))

(define-public crate-actypes-0.3.0 (c (n "actypes") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.32.1") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.109.1") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.140.0") (f (quote ("typescript"))) (d #t) (k 0)))) (h "0baib5q7c4dckjbbldbfaxyshji40df3556qbkrhf0pvql60kj0b")))

(define-public crate-actypes-1.1.0 (c (n "actypes") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.33.15") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.111.1") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.142.0") (f (quote ("typescript"))) (d #t) (k 0)))) (h "15imj881iyj65pvivcam7gifxn1cjdy9dsa314hzwiw6hsgqm7nv")))

(define-public crate-actypes-1.2.0 (c (n "actypes") (v "1.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.33.15") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.112.4") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.143.5") (f (quote ("typescript"))) (d #t) (k 0)))) (h "0jij0wsjy95hqmm0rcc1wmnn471mwwl6gbjby15g0i74zmbhz8rk")))

