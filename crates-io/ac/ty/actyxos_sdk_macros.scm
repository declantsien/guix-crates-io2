(define-module (crates-io ac ty actyxos_sdk_macros) #:use-module (crates-io))

(define-public crate-actyxos_sdk_macros-0.1.0 (c (n "actyxos_sdk_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19pqxn724zgyzbzgjgixvpvhyxdakbjjrb0nnh8cjhazkx5by6i3")))

(define-public crate-actyxos_sdk_macros-0.2.0 (c (n "actyxos_sdk_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1niylp8mgysssj4wlczl22zs9blz6hjwb6gyxxb3xh6q67wzrjzi")))

(define-public crate-actyxos_sdk_macros-0.2.2 (c (n "actyxos_sdk_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0marw8hhjacjxd2di4m29ppnb49573li80vwy60mknrkvkzpaf70")))

(define-public crate-actyxos_sdk_macros-0.2.3 (c (n "actyxos_sdk_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i8w9av2pmyxg7xrhz7v9ddkhl8mjvgxrij7z61jn2yv2pjnp3vf")))

