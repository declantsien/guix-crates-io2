(define-module (crates-io ac ty actyx_sdk_macros) #:use-module (crates-io))

(define-public crate-actyx_sdk_macros-0.1.0 (c (n "actyx_sdk_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vqdwspgkm411sxjyyi30s0l63nxy21xg06sn8bjip0848kdfmrz")))

