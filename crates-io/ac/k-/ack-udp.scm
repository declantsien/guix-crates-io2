(define-module (crates-io ac k- ack-udp) #:use-module (crates-io))

(define-public crate-ack-udp-0.1.0 (c (n "ack-udp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11n0mhbyjpsmqa9iqvdbi0bqf358g68kpd719r14kbdz717fzm2g")))

(define-public crate-ack-udp-0.1.1 (c (n "ack-udp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kjdbiicbjbjdxprcy251sn84fvasn7ksy00ch17mzmsmfb363r8")))

(define-public crate-ack-udp-0.1.2 (c (n "ack-udp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08yc3r3pjdd956prg9jmyaxkick19xw0g062lw63qcis83z8bywc")))

(define-public crate-ack-udp-0.1.3 (c (n "ack-udp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qr2qzx7rpjdwd79rbjh7ivhbj62ws6vqj6cnip1f2abrdnypfvv")))

(define-public crate-ack-udp-0.1.4 (c (n "ack-udp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s66pya7x5v33ax3r1r5x4sypjkk5il7r7lmvnhw6qdafcizvwfg")))

(define-public crate-ack-udp-0.1.5 (c (n "ack-udp") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11m2xvvlzav6izsvg7sfavhlirp5ss1rph5canpi5h366sl6abip")))

(define-public crate-ack-udp-0.1.6 (c (n "ack-udp") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nb0c3k9c7l5x68s3hxwih3hl3kmpbij7zclmafbagzaq31lbxqn")))

(define-public crate-ack-udp-0.1.7 (c (n "ack-udp") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nin02br9hzy7rns9nkqnq8s2r0zzl96svxhgiskcdqq78d05qwy")))

(define-public crate-ack-udp-0.1.8 (c (n "ack-udp") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "199fm0klhsswk8ydd1l8ahr1kwcja177cw93zb0c2lv65fzjq7qv")))

(define-public crate-ack-udp-0.1.9 (c (n "ack-udp") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14c54xbf6ykmdfc650cq5kdck7s77nwnmxnpasi3ppz2yix3n152")))

(define-public crate-ack-udp-0.1.10 (c (n "ack-udp") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xbl7wbd07jiqa9rkwfacxppf26378x06jq2fc9l1wb7q9abz33s")))

(define-public crate-ack-udp-0.1.11 (c (n "ack-udp") (v "0.1.11") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gwnhg171cays6q60z3j8mp0pqc8dk1ds1j8a0hhjwxcrqbajsig")))

(define-public crate-ack-udp-0.1.12 (c (n "ack-udp") (v "0.1.12") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jq37cifn1770n69izaj0h0kjnpfmm04mfl2wihdi9l954a99bj5")))

(define-public crate-ack-udp-0.1.13 (c (n "ack-udp") (v "0.1.13") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x90bl7r9qj83sgl0ly9xkr39qxln0kgq17y4v8qvxkrm2idxlb0")))

(define-public crate-ack-udp-0.1.14 (c (n "ack-udp") (v "0.1.14") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2fhyvb6p2dm7gzv9l10k7h6vlnvk75mnfa09v3wyal5aq30n13")))

