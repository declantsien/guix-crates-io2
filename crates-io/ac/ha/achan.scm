(define-module (crates-io ac ha achan) #:use-module (crates-io))

(define-public crate-achan-0.1.0 (c (n "achan") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (f (quote ("realloc"))) (o #t) (k 0)))) (h "1irpjy1kh4vv9bwag6h37y31f4qhjxxb37gm6fcgb50ma1hsmgv6") (s 2) (e (quote (("wasm-component" "dep:wit-bindgen") ("serde" "dep:serde"))))))

(define-public crate-achan-0.2.0 (c (n "achan") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (f (quote ("realloc"))) (o #t) (k 0)))) (h "10cqa49ywp7zh6dmj4941amwcc6v882x2xz8yg3dnz73hivw8nzq") (s 2) (e (quote (("wasm-component" "dep:wit-bindgen") ("serde" "dep:serde"))))))

