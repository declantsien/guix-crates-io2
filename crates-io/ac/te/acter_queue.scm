(define-module (crates-io ac te acter_queue) #:use-module (crates-io))

(define-public crate-acter_queue-0.1.0 (c (n "acter_queue") (v "0.1.0") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "deque") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ls1dhmkjk2gr8jh52gfr9hnl57dmgz0gyhpgmvpp71kfmzz2fci") (y #t)))

