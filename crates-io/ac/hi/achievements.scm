(define-module (crates-io ac hi achievements) #:use-module (crates-io))

(define-public crate-achievements-0.1.0 (c (n "achievements") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "1i9s07ygdmwmk63858y03jqmz4if7rjdimqx10qicjj70aa75i04")))

(define-public crate-achievements-0.1.1 (c (n "achievements") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "1zklmlm4188zl8ikp7pr68rqg4hczsydv8gs69jcbp7pv1jsn6sq")))

(define-public crate-achievements-0.1.2 (c (n "achievements") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "1g3p9mxzs9gmdy6m4fymaxsg1wsswjsp3p6cjw5hciadbdidwmwl")))

(define-public crate-achievements-0.2.0 (c (n "achievements") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "formatting" "parsing" "macros"))) (d #t) (k 0)))) (h "0af7mbx9nqcw9z3097g9g6f7ags18cq8adll8m2mlzjvn7y48h8g")))

