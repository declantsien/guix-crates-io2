(define-module (crates-io ac ke ackerman) #:use-module (crates-io))

(define-public crate-ackerman-0.1.1 (c (n "ackerman") (v "0.1.1") (d (list (d (n "lsp-types") (r "^0.93.1") (f (quote ("proposed"))) (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "06615q9q8gnq81c3kh28v676f2psphjpnhddm2hxydfaysrxihk7") (f (quote (("default"))))))

