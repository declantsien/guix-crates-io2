(define-module (crates-io ac me acme-macros) #:use-module (crates-io))

(define-public crate-acme-macros-0.1.0 (c (n "acme-macros") (v "0.1.0") (h "1ib0m0829xjs23w80yvvnawlqh99yjjfilh7wm3m5apnqr1dibs9")))

(define-public crate-acme-macros-0.1.1 (c (n "acme-macros") (v "0.1.1") (h "07m173nz28wvs7x4ya74j1s8l05jg73c6mi2b3y2jcid9a3gkamm")))

(define-public crate-acme-macros-0.1.2 (c (n "acme-macros") (v "0.1.2") (h "0ivpfs0wpvq1ak9p0xakifgjvk5sykplmjbsnz2k2y69fz760sdw")))

(define-public crate-acme-macros-0.1.3 (c (n "acme-macros") (v "0.1.3") (h "0v2qg5yx4xg4j06ggd76y923lrc3db06x84641i9gnnc8r2b4ki5")))

(define-public crate-acme-macros-0.1.4 (c (n "acme-macros") (v "0.1.4") (h "1362kw6p2xalhb8rk9vhxwc0wr4lf6r512n40hbfbwwmdf3dxr9z")))

(define-public crate-acme-macros-0.1.5 (c (n "acme-macros") (v "0.1.5") (h "03l2c6rnhk3i1pazrdg6w1vsdkyz0z65fcd5369xq5hw3rgvpj0g")))

(define-public crate-acme-macros-0.1.6 (c (n "acme-macros") (v "0.1.6") (h "17src92k8xj8152mipzmzngr9964chrj59cp0qfm762krdhayllx")))

(define-public crate-acme-macros-0.1.7 (c (n "acme-macros") (v "0.1.7") (h "0k4fdi9i8vsgy4v6z2abn8l431c3m65k80avllk7cnnkbhysrz1q") (f (quote (("default"))))))

(define-public crate-acme-macros-0.1.8 (c (n "acme-macros") (v "0.1.8") (d (list (d (n "acme") (r "^0.1.22") (d #t) (k 2)))) (h "1b9ywwqqy5npsfwq0w1pf6f25ma23w5lnd5jw0rm3kcj6ahs6fki") (f (quote (("default"))))))

(define-public crate-acme-macros-0.1.9 (c (n "acme-macros") (v "0.1.9") (d (list (d (n "acme") (r "^0.1.22") (d #t) (k 2)))) (h "0ss24k6gdvw6532qb9ijxsx84n9rb4gb8yw0m30nj00mp7hs7gsd") (f (quote (("default"))))))

(define-public crate-acme-macros-0.1.10 (c (n "acme-macros") (v "0.1.10") (d (list (d (n "acme") (r "^0.1.22") (d #t) (k 2)))) (h "1mmws806ls2a9jm565h9gicl2i24hyx6jlc7rm9bqnjfr7fcrg9i") (f (quote (("default"))))))

(define-public crate-acme-macros-0.1.11 (c (n "acme-macros") (v "0.1.11") (h "18fl52lk5lnzy5qd7wpmyichxska5m1037jzmgdxhrsk5yygfndy")))

(define-public crate-acme-macros-0.1.12 (c (n "acme-macros") (v "0.1.12") (h "0zbqmb2sca2f7l05gxv7lpv8gb28qap0fw2gslzmqxc4cfjfl1x4")))

(define-public crate-acme-macros-0.1.13 (c (n "acme-macros") (v "0.1.13") (h "1nfpqlamcgq7clgznldnxify2bp08h06d4yvq21rycjcgglfmg4f")))

(define-public crate-acme-macros-0.1.14 (c (n "acme-macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvfa1zhci2gyrmbnj8qn6n298r9kxbv6f14hznm3736saxndl0b")))

(define-public crate-acme-macros-0.1.15 (c (n "acme-macros") (v "0.1.15") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzr5ks0yrdy064cv5vs46wl7xyri3mhypb1kvlhzmzpqgjzwy7m")))

(define-public crate-acme-macros-0.1.16 (c (n "acme-macros") (v "0.1.16") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "10l15x5lhfp0r5zvn1xyssndzdnh673v97lb1ivdz8b2hmn9msh6")))

(define-public crate-acme-macros-0.1.18 (c (n "acme-macros") (v "0.1.18") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0pbsi6l8gjv5i0qyjwy03xwx01abml3y9yzmvvz6cv4w4wdn7c33")))

(define-public crate-acme-macros-0.1.35 (c (n "acme-macros") (v "0.1.35") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmzf2sa4s84vnh9qhp9nysll8lll9f5brhaq5w9sn0542rvmsmg")))

(define-public crate-acme-macros-0.1.36 (c (n "acme-macros") (v "0.1.36") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1sj4rx5nzqalkch43y53kpdgysq6w0rcp84wbrknzhv8y9q8lxmv")))

(define-public crate-acme-macros-0.1.37 (c (n "acme-macros") (v "0.1.37") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0nyj8qqlsipdx5pxjvj33a3glb57mad8nlb0mvwbhsywq1jk30nb")))

(define-public crate-acme-macros-0.1.40 (c (n "acme-macros") (v "0.1.40") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1pijk5ih4y86s1jmhgad2q5fl95d81sh4fcsdkskg4lzjbz6h9lw")))

(define-public crate-acme-macros-0.1.41 (c (n "acme-macros") (v "0.1.41") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0jvflyg7qjsq9vym54zxb3fvckk8z4iz5nbw79mg0lql6d7bk8hx")))

(define-public crate-acme-macros-0.1.42 (c (n "acme-macros") (v "0.1.42") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0ffmgq5qz7vfwyng95qkp02mdfv1qyrjxbwcxyy2z372pcfl8c1q")))

(define-public crate-acme-macros-0.1.43 (c (n "acme-macros") (v "0.1.43") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1idayhg7qydb0vczixb7kprcy45hlsdijd5qyjvy589hnfzlhwjw")))

(define-public crate-acme-macros-0.1.44 (c (n "acme-macros") (v "0.1.44") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "10dxp490fnqpvmj0hfmninh70mnvrp65mzjddlbw1f35p841rgdm")))

(define-public crate-acme-macros-0.1.45 (c (n "acme-macros") (v "0.1.45") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "14yhgy8jyv0z1a2imwsibs1xb265d8ql9q0ps6l2ghamaz537nhl")))

(define-public crate-acme-macros-0.1.46 (c (n "acme-macros") (v "0.1.46") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "13hbsfjpnanc47jhzywf7ym6yz81nqqv4rbnsrgdai00n66dzpn0")))

(define-public crate-acme-macros-0.1.47 (c (n "acme-macros") (v "0.1.47") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0qys2mf9c23nx41xqayyl1q7w68qfj5wi47q007pl062kqb4bywz")))

(define-public crate-acme-macros-0.1.48 (c (n "acme-macros") (v "0.1.48") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1wzqh8rm7rkvgi3wqdzspcycyg0fc7ymmk0wb83injr0l21m72ns")))

(define-public crate-acme-macros-0.1.49 (c (n "acme-macros") (v "0.1.49") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "18xwlafipagxh2kw5rzz4kivcfb60y06wy8c34h8zzzmvk6qyl20")))

(define-public crate-acme-macros-0.1.50 (c (n "acme-macros") (v "0.1.50") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1m976cdbxzh8b2nv5x513fq9i8czaw64bfb2fs4blgn2r5al1w7j")))

(define-public crate-acme-macros-0.2.0 (c (n "acme-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8z4i05qxasp7yjq00w77nkajjrmzyhpsfjz1vl0xr3nlvbzif6")))

(define-public crate-acme-macros-0.2.1 (c (n "acme-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0n7sjr4in5z6r452n4z2zqlhzgblcimpnb5vxnhgl1h9sanx3rhg")))

(define-public crate-acme-macros-0.2.5 (c (n "acme-macros") (v "0.2.5") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zcfrlvdzljj48rra148n7kqd1n3msj6vdzzcm04k6knqx4nmz4m") (f (quote (("default"))))))

(define-public crate-acme-macros-0.3.0-nightly (c (n "acme-macros") (v "0.3.0-nightly") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "0cfnzq8ms9lli9c0njcrg2a9caig1h7lksp0h4g1z4dhh9dhmlwf")))

(define-public crate-acme-macros-0.3.0-nightly.1 (c (n "acme-macros") (v "0.3.0-nightly.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "1xhxil331j8nr53prv1rbbmwa4zlvlkbqqajs60swcayw4nj69sc")))

(define-public crate-acme-macros-0.3.0-nightly.2 (c (n "acme-macros") (v "0.3.0-nightly.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "09mc6xzlfjk1y1zdj3snwg4g7ny1kayjyf4k0h1r06mj0zsa0lsl")))

(define-public crate-acme-macros-0.3.0-nightly.3 (c (n "acme-macros") (v "0.3.0-nightly.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "11ax1l1hr7gd0bgd3a9saslgjbwpzazy11l3f045yqplbh3va91h")))

(define-public crate-acme-macros-0.3.0-nightly.4 (c (n "acme-macros") (v "0.3.0-nightly.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "1ys072592zv0gcpbbgg2clvf4gjvvyq5b6kq59jkk9znslizy65w")))

(define-public crate-acme-macros-0.3.0 (c (n "acme-macros") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "12lrnd4qgkkyysns60g8x3lqd3gpgpahb659wi5i23j9n97y16jf")))

(define-public crate-acme-macros-0.3.1-nightly (c (n "acme-macros") (v "0.3.1-nightly") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "00r11i7l7gzg3p4q9g4q9x2p83w52jw27wpa450n0sagkik661j5")))

(define-public crate-acme-macros-0.3.1 (c (n "acme-macros") (v "0.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "12mw0cw9xsis5796pf7j9q4l9xnipgm3jid3y3q9dsn920h1jw1s")))

