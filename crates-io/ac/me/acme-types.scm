(define-module (crates-io ac me acme-types) #:use-module (crates-io))

(define-public crate-acme-types-0.1.0 (c (n "acme-types") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "190ay5vmdwx9s0mwlrb8svx25ydpcs75hrf6xhw8vi2q851i3mzp") (f (quote (("json" "serde/derive" "serde_json"))))))

(define-public crate-acme-types-0.1.1 (c (n "acme-types") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1aik7hgkzg9xk9hig60w5ziz2z9mhkjzzld8lxzi21zka2m4p0ny") (f (quote (("json" "serde/derive" "serde_json"))))))

