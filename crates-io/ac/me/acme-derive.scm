(define-module (crates-io ac me acme-derive) #:use-module (crates-io))

(define-public crate-acme-derive-0.1.0 (c (n "acme-derive") (v "0.1.0") (d (list (d (n "acme") (r "^0.1.24") (d #t) (k 2)))) (h "1afbj28337akw7nsb486chsvz950iv9xb25baygk9aw0hcznkpcz") (f (quote (("default"))))))

(define-public crate-acme-derive-0.1.2 (c (n "acme-derive") (v "0.1.2") (h "0dgp2xp0r48fafr9dzmjfiwpq0kpq6xsw33k5hpf3a53rrp50wc1")))

(define-public crate-acme-derive-0.1.12 (c (n "acme-derive") (v "0.1.12") (h "1dbjhj1akdx8rgqbk0vchqmfnqbs77ny5q1jgpd15pnbc9ljp5ly")))

(define-public crate-acme-derive-0.1.13 (c (n "acme-derive") (v "0.1.13") (h "0qdd6xpnc1h9m6zvzgay9nbk4x3j5m0kpalfh4zsizba7vsq54q4")))

(define-public crate-acme-derive-0.1.14 (c (n "acme-derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0y9hs08hcdllf0gz47vn6jg8lnm9868rz429fkyq6n8ly5kk2q5h")))

(define-public crate-acme-derive-0.1.15 (c (n "acme-derive") (v "0.1.15") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "08kndsnwzylmf5m33ihf0bzcnsfa925rhxacbw0x9pmi95lhb5b4")))

(define-public crate-acme-derive-0.1.16 (c (n "acme-derive") (v "0.1.16") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3ma8m508afb8yqygc5rhpskkmpi01cw3fhv0qhm3r4wc75dyrk")))

(define-public crate-acme-derive-0.1.18 (c (n "acme-derive") (v "0.1.18") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "06sfl5327q7sbbwsxg7wl2yg9zwczvl9xq4vzszzf8w8g7y6kqs8")))

(define-public crate-acme-derive-0.1.35 (c (n "acme-derive") (v "0.1.35") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0id3i0snmhz4xg8kmvj0jc3m03lkjn8ichsp07am4acz1rlm04vx")))

(define-public crate-acme-derive-0.1.36 (c (n "acme-derive") (v "0.1.36") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "05pbs2w6dblw8w2vb4sq19b8gal5f97nxnawrcfki46v6sfqszdw")))

(define-public crate-acme-derive-0.1.37 (c (n "acme-derive") (v "0.1.37") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "104m4l6g9qj5wn3rprycbp9k4irnr4ygwx1cqr7i6skdll7n4kxw")))

(define-public crate-acme-derive-0.1.40 (c (n "acme-derive") (v "0.1.40") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0pxjl2h0xlczyi9rq7k7h099bld952lyx1122hkkiypg1mm6bc0p")))

(define-public crate-acme-derive-0.1.41 (c (n "acme-derive") (v "0.1.41") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0jpz6npaz2k9wc1p0lgplm5f4780bdkp83vrrw9rvh6pxc0bnqr5")))

(define-public crate-acme-derive-0.1.42 (c (n "acme-derive") (v "0.1.42") (d (list (d (n "proc-macro2") (r ">=1.0.40") (d #t) (k 0)) (d (n "quote") (r ">=1.0.20") (d #t) (k 0)) (d (n "syn") (r ">=1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0r4za7ydxy3v96433yfy6bgxkarkfd6hcs03ppfpg6cpbk1xnnwv")))

(define-public crate-acme-derive-0.1.43 (c (n "acme-derive") (v "0.1.43") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "07mrzbnvb0s1iqfrql203fa7z8siv2i2i38smm26rjimdvzlcia4")))

(define-public crate-acme-derive-0.1.44 (c (n "acme-derive") (v "0.1.44") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1kj6kh7l0hm6jsspc128nl9vpvc3xzv7nqmdx2njf9my7bzbk0jc")))

(define-public crate-acme-derive-0.1.45 (c (n "acme-derive") (v "0.1.45") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "04gq89psjciakmznrimdyrykckfdpsj4lsxzyljp5bi18lx8y59x")))

(define-public crate-acme-derive-0.1.46 (c (n "acme-derive") (v "0.1.46") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "12i7qiiyjkcvk66a8iqwf997jn6cjnq8p7yhkr5b6asbm9hh9lfy")))

(define-public crate-acme-derive-0.1.47 (c (n "acme-derive") (v "0.1.47") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1w6q8bb89hhmkslpnfkkbdhf61czx7azzqjdaq0s53dl10wbymx9")))

(define-public crate-acme-derive-0.1.48 (c (n "acme-derive") (v "0.1.48") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0xliz9a9f0fmi2bsz9kqcbdfi72ps630yzxg972n7wlax6rkj1nc")))

(define-public crate-acme-derive-0.1.49 (c (n "acme-derive") (v "0.1.49") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "11pgcrd3zh072ppdhgb5l8l9nzxibwbjvc4fqrf1lzs4n93zv147")))

(define-public crate-acme-derive-0.1.50 (c (n "acme-derive") (v "0.1.50") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0nddn0h8w05l5jcjb3d1ia54phk42ay2cz2i3i6fs1flf3nmabg5")))

(define-public crate-acme-derive-0.2.0 (c (n "acme-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1zcywwrd5sjvck5ssf926s56x2dcd7kkhippc9n0j2a19wrqiy9p")))

(define-public crate-acme-derive-0.2.1 (c (n "acme-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnj0agr0h9kswn9b8rayzyig1zcl436gd83fi1v50n2yq3swr5z")))

(define-public crate-acme-derive-0.2.5 (c (n "acme-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11s77lzzflmjva2q98qxv5lk3d5cfp16anxf31d4qfmj85fm1zc6") (f (quote (("default"))))))

(define-public crate-acme-derive-0.3.0-nightly (c (n "acme-derive") (v "0.3.0-nightly") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00ji4nl2icj791fbj27dw0gfl0akdq6w769nfpf1jkwkqyfzz1vx")))

(define-public crate-acme-derive-0.3.0-nightly.1 (c (n "acme-derive") (v "0.3.0-nightly.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "17cxwaldwb1hwhmjxfgv0821f3nc6ryayv5m2snq112l094xwz8i")))

(define-public crate-acme-derive-0.3.0-nightly.2 (c (n "acme-derive") (v "0.3.0-nightly.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ln0jg8ac57aj6yf5llprqdb7aifq68alkqz9dq0y3f0048jrb1p")))

(define-public crate-acme-derive-0.3.0-nightly.3 (c (n "acme-derive") (v "0.3.0-nightly.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16xq58n3yp5k72z4nyq6bjkq8climag94dym247bp4jjp4lsi98i")))

(define-public crate-acme-derive-0.3.0-nightly.4 (c (n "acme-derive") (v "0.3.0-nightly.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s6m912pdcxn69zs7xzwaf3479vh74s7m1kbz8wivvjsaa63d237")))

(define-public crate-acme-derive-0.3.0 (c (n "acme-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9d3vsqjfspzy28l664pw353dxwbvlg0j6nakfh5lm33riwl4p3")))

(define-public crate-acme-derive-0.3.1-nightly (c (n "acme-derive") (v "0.3.1-nightly") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kxbcjx0dmrrsvsw8rbav7ln3rdl0k6hybfh1zk7npb1l0v6azk1")))

(define-public crate-acme-derive-0.3.1 (c (n "acme-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l806ydrhmg0cvh5lvmpx95si1al78pv8c2j5pp8s5z6pnvvqb9c")))

