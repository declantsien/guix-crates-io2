(define-module (crates-io ac ur acursor) #:use-module (crates-io))

(define-public crate-acursor-0.0.1 (c (n "acursor") (v "0.0.1") (h "1rlpzpkgrzck4dr8pqrkiv538g5kj4s0ygc0cll54kjrkzq65kf0")))

(define-public crate-acursor-0.0.2 (c (n "acursor") (v "0.0.2") (d (list (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)))) (h "13qf6xnz1a6xim8np061fq4kvfw9qm7v71hcmi1g1kvxm0qyhv9l")))

