(define-module (crates-io ac ut acute_core) #:use-module (crates-io))

(define-public crate-acute_core-0.1.0 (c (n "acute_core") (v "0.1.0") (d (list (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty_timer") (r "^1.0.3") (d #t) (k 0)))) (h "0n57lp6gdnaabrx1lyd099n4ykpjd98jmsfc1m3k4rbqnhkb1x3z")))

(define-public crate-acute_core-0.1.1 (c (n "acute_core") (v "0.1.1") (d (list (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty_timer") (r "^1.0.3") (d #t) (k 0)))) (h "127bk3s70hnm04cqsr0lqjr1lscbc3cpl2lpwxkvbyswdks66saf")))

