(define-module (crates-io ac ut acute_render_backend) #:use-module (crates-io))

(define-public crate-acute_render_backend-0.1.0 (c (n "acute_render_backend") (v "0.1.0") (d (list (d (n "wgpu") (r "^0.5.2") (d #t) (k 0)))) (h "0k22mvhnk713q34iqxiqziy2949sanbgiwx29igr3p905rgv83ml")))

(define-public crate-acute_render_backend-0.1.1 (c (n "acute_render_backend") (v "0.1.1") (d (list (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_window") (r "^0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.5.2") (d #t) (k 0)))) (h "11clsvxr467jnb8q12biwd3q41xw088lnqdci38ijii2s7rrq8r8")))

(define-public crate-acute_render_backend-0.1.2 (c (n "acute_render_backend") (v "0.1.2") (d (list (d (n "acute_ecs") (r "^0.1") (d #t) (k 0)) (d (n "acute_window") (r "^0.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.5.2") (d #t) (k 0)))) (h "0mwwqa3rxka27gy90kkr5ki7as36a3ikr4n6sbq5ak922hipmk09")))

