(define-module (crates-io ac ut acute_input) #:use-module (crates-io))

(define-public crate-acute_input-0.1.0 (c (n "acute_input") (v "0.1.0") (h "06rk3skrdcjk4z5xx6vska8nq0q681jhnjjq7sd7zz3npd3mal1y")))

(define-public crate-acute_input-0.1.2 (c (n "acute_input") (v "0.1.2") (d (list (d (n "acute_ecs") (r "^0.1") (d #t) (k 0)) (d (n "acute_window") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.0-rc6") (d #t) (k 0)))) (h "0zni4rcwr4wxmk1a8gvswy3kg0sxs7xm3bmj18nwi51d436k1z85")))

