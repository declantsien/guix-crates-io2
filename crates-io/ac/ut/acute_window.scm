(define-module (crates-io ac ut acute_window) #:use-module (crates-io))

(define-public crate-acute_window-0.1.0 (c (n "acute_window") (v "0.1.0") (d (list (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "03894cz73ii6zywh05v2hp1lqq4wcalyp8bimhnch4mh1iqaw48s")))

(define-public crate-acute_window-0.1.1 (c (n "acute_window") (v "0.1.1") (d (list (d (n "acute_ecs") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "028yvcsjwqpkmhc4ymwvd4vdj9j1knibx9x8ny0vj4yaggbbwx2r")))

