(define-module (crates-io ac ut acute_app) #:use-module (crates-io))

(define-public crate-acute_app-0.1.0 (c (n "acute_app") (v "0.1.0") (h "1vwrqn85k8phcm57v8209alc76mdw5vqkppivckm7sdg94f9xiry")))

(define-public crate-acute_app-0.1.1 (c (n "acute_app") (v "0.1.1") (d (list (d (n "acute_core") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_scenes") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_window") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "18wgp2a26p41z8s0s6w58wfziqj0k4vmdn5wj3havnrqivacx868")))

(define-public crate-acute_app-0.1.2 (c (n "acute_app") (v "0.1.2") (d (list (d (n "acute_core") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_render_backend") (r "^0.1.1") (d #t) (k 0)) (d (n "acute_scenes") (r "^0.1.0") (d #t) (k 0)) (d (n "acute_window") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "12934zc8pq5ybkx3x0vjz8w0cfw9qlsgk1n99z6l0h4i0d14v3wx")))

(define-public crate-acute_app-0.1.3 (c (n "acute_app") (v "0.1.3") (d (list (d (n "acute_core") (r "^0.1") (d #t) (k 0)) (d (n "acute_ecs") (r "^0.1") (d #t) (k 0)) (d (n "acute_input") (r "^0.1") (d #t) (k 0)) (d (n "acute_render_backend") (r "^0.1") (d #t) (k 0)) (d (n "acute_scenes") (r "^0.1") (d #t) (k 0)) (d (n "acute_window") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "0bs6ga48skjczx752rrbxlw62c53v0nwwdvq5sxv7w6k1hpsiv25")))

