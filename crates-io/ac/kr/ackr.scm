(define-module (crates-io ac kr ackr) #:use-module (crates-io))

(define-public crate-ackr-1.0.0 (c (n "ackr") (v "1.0.0") (h "02ldapx0wsbjjdxsm003i44x0cpzr54badxc26iw7bnmgw6r68xm")))

(define-public crate-ackr-1.1.0 (c (n "ackr") (v "1.1.0") (h "0f0yzqfycdi33mpxsdfvgpgpxkxr6glhivys6gzy13imvg31zyfm")))

