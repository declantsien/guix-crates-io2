(define-module (crates-io ac on acon) #:use-module (crates-io))

(define-public crate-acon-0.3.3 (c (n "acon") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0.58") (d #t) (k 0)))) (h "1ckb9bx1i3rdgya7mvjh1d7xisg56cf8daq2msi9pwiazhr0isdj")))

(define-public crate-acon-0.4.0 (c (n "acon") (v "0.4.0") (h "13g5kpjf7d6cy0l94vaxx4s9y3w9sp670cdjpycrvwqw46x2slm2")))

(define-public crate-acon-0.5.0 (c (n "acon") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.67") (d #t) (k 0)))) (h "08sl9iz24i6v7m5v2an68jl8vf26xhavxsf03llygd4yg45c04nc")))

(define-public crate-acon-0.5.1 (c (n "acon") (v "0.5.1") (h "0vi3zv8j4li9h11p0k3jxrba2xjns9jmsq4hq5na755kpa2wcmv9") (f (quote (("default"))))))

(define-public crate-acon-0.5.2 (c (n "acon") (v "0.5.2") (h "0d4vdw0z1s6rjwdg8zix731fd76a89vxr9ws9gbwwscmjljnx7xs") (f (quote (("default"))))))

