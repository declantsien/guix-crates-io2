(define-module (crates-io ac c_ acc_reader) #:use-module (crates-io))

(define-public crate-acc_reader-1.0.0 (c (n "acc_reader") (v "1.0.0") (h "18mfmn11a3jn918dqx4vrswjgml8fnkpmf0335nmpycrx5njayc1")))

(define-public crate-acc_reader-2.0.0 (c (n "acc_reader") (v "2.0.0") (h "1697ivh4rbkp0b1603nrcr36vnjayrihbgijms8dmfx2zspj7l0g")))

