(define-module (crates-io ac at acat) #:use-module (crates-io))

(define-public crate-acat-0.1.1 (c (n "acat") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0i31a1jcd0wd8281m2n6ngwn4rpshrpqxpg9b3cbnjv0ynpw24ph")))

(define-public crate-acat-0.1.2 (c (n "acat") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "random_color") (r "^0.4.4") (d #t) (k 0)))) (h "12na2ymccz8d20dk8a6pwjf5waq6jchxam0g3pz941ij592fb63i")))

