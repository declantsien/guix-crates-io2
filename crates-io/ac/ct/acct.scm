(define-module (crates-io ac ct acct) #:use-module (crates-io))

(define-public crate-acct-0.1.0 (c (n "acct") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "0dmfkvpnyb6c3j9zanhl16qxgpccczj9kh7ff5mwwa6m6yaa8qy9")))

(define-public crate-acct-0.1.1 (c (n "acct") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "0rz0ksj010ax2yrx344fviz7411l11s1m5xhqr3k6g8sjpay4w2m")))

(define-public crate-acct-0.2.0 (c (n "acct") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)))) (h "0959498zv8pb2ggg44g25926rdzc3jsraq8w6gx9kc6d8nwws16d")))

(define-public crate-acct-0.3.0 (c (n "acct") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)))) (h "1b0zx0b8khjpds4zlyg44gdbcjm3idpzx09rj1bgs7kwly4ixf60")))

(define-public crate-acct-0.4.0 (c (n "acct") (v "0.4.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)))) (h "1p3cp24qfp2bjjkl4a646ld96z7nchw1z27dvrna3l6zcpldg8fj")))

(define-public crate-acct-0.5.0 (c (n "acct") (v "0.5.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)))) (h "1lb2x6x6bz8sy3wydfspd77cpk1s4j4ipz4c34w4957vfahrh5vb")))

(define-public crate-acct-0.6.0 (c (n "acct") (v "0.6.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "0r5fca5nlyfxn7dasrpkgrp5cdcwdz7yhl8iy9yx2swiaa11xbq9")))

