(define-module (crates-io ac ac acacia_net) #:use-module (crates-io))

(define-public crate-acacia_net-0.1.0 (c (n "acacia_net") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.4") (d #t) (k 0)))) (h "1s1wrhqknsmz0pn1kgnn2yfranfn3fjs1jf5nnzzb4dh8a4bdb0v")))

