(define-module (crates-io ac l- acl-sys) #:use-module (crates-io))

(define-public crate-acl-sys-1.0.0 (c (n "acl-sys") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "00awisbcyn71kj5qlqq4yash775iya1isppw1r8qqfnqiyizg8cc")))

(define-public crate-acl-sys-1.0.1 (c (n "acl-sys") (v "1.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07380z53gw3019y5l2kw6v4fg4rgcqjnmnk6n3xmdadppac4mkiq")))

(define-public crate-acl-sys-1.2.0 (c (n "acl-sys") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1r7ivaiwl5kdq599bz2pnfj1gqy1gwl0wnm435rsadhs09n4721q")))

(define-public crate-acl-sys-1.2.1 (c (n "acl-sys") (v "1.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0d591bswrhgivax00p1js2x9vpa0k1lblm9hii4abrygzfbh7147")))

(define-public crate-acl-sys-1.2.2 (c (n "acl-sys") (v "1.2.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1ciygbhavz4ms3zxqlg139sj97gpw3vnfg7jip8ly4nkppwpkh5v")))

