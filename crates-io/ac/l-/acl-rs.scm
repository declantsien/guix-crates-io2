(define-module (crates-io ac l- acl-rs) #:use-module (crates-io))

(define-public crate-acl-rs-0.1.0 (c (n "acl-rs") (v "0.1.0") (d (list (d (n "acl-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "1qbk835zg80bdqxbdsbz9anv3jjmpwpb80sy841r4fcqlh72ckky")))

(define-public crate-acl-rs-0.1.1 (c (n "acl-rs") (v "0.1.1") (d (list (d (n "acl-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1557d5dqzalf6by1slq8aq4sxk565cyfl9d4m7bxd9nx65yaip45")))

