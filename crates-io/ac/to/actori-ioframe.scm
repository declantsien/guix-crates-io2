(define-module (crates-io ac to actori-ioframe) #:use-module (crates-io))

(define-public crate-actori-ioframe-0.5.0 (c (n "actori-ioframe") (v "0.5.0") (d (list (d (n "actori-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actori-connect") (r "^1.0.2") (d #t) (k 2)) (d (n "actori-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actori-testing") (r "^1.0.0") (d #t) (k 2)) (d (n "actori-utils") (r "^1.0.6") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "0kplycb6jvly15aw9xk8rki6nsmybdbc7g8i610hrbr15rjg76i1")))

