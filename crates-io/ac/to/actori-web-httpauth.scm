(define-module (crates-io ac to actori-web-httpauth) #:use-module (crates-io))

(define-public crate-actori-web-httpauth-0.4.0 (c (n "actori-web-httpauth") (v "0.4.0") (d (list (d (n "actori-rt") (r "^1.0") (d #t) (k 2)) (d (n "actori-service") (r "^1.0") (d #t) (k 0)) (d (n "actori-web") (r "^2.0") (k 0)) (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1wq5mvv177xf0a5lsdadyfb3yl6fcixpyarhwgzwc1vx57fvwn9h") (f (quote (("nightly") ("default"))))))

