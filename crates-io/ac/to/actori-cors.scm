(define-module (crates-io ac to actori-cors) #:use-module (crates-io))

(define-public crate-actori-cors-0.2.0 (c (n "actori-cors") (v "0.2.0") (d (list (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actori-web") (r "^2.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1ls3yijixdsyqhzjfsv0gsbdkvak364xlmdzlzdp0z0x01vj1nwy")))

