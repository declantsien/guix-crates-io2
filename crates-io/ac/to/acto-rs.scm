(define-module (crates-io ac to acto-rs) #:use-module (crates-io))

(define-public crate-acto-rs-0.2.9 (c (n "acto-rs") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.16") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0yhxqm013256prs1gmnv0y7y3z8khszzhk7x0bdgmvkdk22glpf0")))

(define-public crate-acto-rs-0.2.11 (c (n "acto-rs") (v "0.2.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "125qmr51bwlvq6qlzihz985g8nkgfkhckya1s0yzgp8ccgg7dsiz")))

(define-public crate-acto-rs-0.3.2 (c (n "acto-rs") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1mmd6df8gngm1snsdigzlhwa5hl6qgbzfik8mc4lrlm2541y41j7")))

(define-public crate-acto-rs-0.3.3 (c (n "acto-rs") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "02s5x5cxmqb4lx8jd011yny13kjxkzqk6a2shra7p50wnhfcngwi")))

(define-public crate-acto-rs-0.3.4 (c (n "acto-rs") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1c0vl6c1m8gyyf5crpllp692zj2grrv6c4ba1wbc0kz5mivcr4w6") (f (quote (("default") ("bench"))))))

(define-public crate-acto-rs-0.3.5 (c (n "acto-rs") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "06lxv6lbjcnjn7w2fjgr4lafz64qgzblxdcq8333c14y5y30z29j") (f (quote (("default") ("bench"))))))

(define-public crate-acto-rs-0.3.6 (c (n "acto-rs") (v "0.3.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1i4s8hnwg89dhvvyb4kpxpbb0pzbmvnafyjsw84rdbq522qw8sil") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.3.7 (c (n "acto-rs") (v "0.3.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1mprlghq5drpvlhk7kml8s9y4g9wv4i9nv8g9yqy8l5f7q3ps6hw") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.3.9 (c (n "acto-rs") (v "0.3.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.18") (d #t) (k 0)))) (h "0vpphdkh1bqd7h7fqb843x9i1rrzypydmd42qs0kgm5swswikr5y") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.4.0 (c (n "acto-rs") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.19") (d #t) (k 0)))) (h "0vqqq4gnrf67247ga2rj73hhrx8n7bizvxi2za49kkf2sn1jmc8q") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.5.0 (c (n "acto-rs") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.19") (d #t) (k 0)))) (h "0vswvq168xr7x4fj5jn7bh9i0331p5fyjznrp96l0jwqb0sd1n6b") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.5.1 (c (n "acto-rs") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.19") (d #t) (k 0)))) (h "0npklql4vnk6yzdqqpwwib97r84zjsgnilbc3pd2w4f3ca4hn4m9") (f (quote (("printstats") ("default") ("bench"))))))

(define-public crate-acto-rs-0.5.2 (c (n "acto-rs") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lossyq") (r "^0.1.19") (d #t) (k 0)))) (h "0dnc7g1492y0mkvxzbbw517yn6yyvym6qfwshl935zlaz5b7xqhl") (f (quote (("printstats") ("default") ("bench"))))))

