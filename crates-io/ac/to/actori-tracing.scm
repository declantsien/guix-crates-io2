(define-module (crates-io ac to actori-tracing) #:use-module (crates-io))

(define-public crate-actori-tracing-0.1.0 (c (n "actori-tracing") (v "0.1.0") (d (list (d (n "actori-rt") (r "^1.0") (d #t) (k 2)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)))) (h "11561jw6rc9vssaim8zqc4n9vk7w23zpqi4nzdqv7qfvrxaq0bsw")))

