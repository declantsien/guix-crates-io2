(define-module (crates-io ac to actors-macros) #:use-module (crates-io))

(define-public crate-actors-macros-0.1.0 (c (n "actors-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0i86p5qdbxqbsh2jd07nj3p7ac98iliac5aybxw1171frsjfp1ks")))

(define-public crate-actors-macros-0.1.1 (c (n "actors-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1lq4izzpn4amk210czr2bi5yjm9yy08815kfvkjxa594x8qxvnph")))

