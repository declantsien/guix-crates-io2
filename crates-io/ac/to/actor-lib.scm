(define-module (crates-io ac to actor-lib) #:use-module (crates-io))

(define-public crate-actor-lib-0.0.1 (c (n "actor-lib") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sj1iwfcdsj2wz1sf69l5nqbj16n5y0h3qcah6ngnwm4gaa4q9f6") (y #t)))

(define-public crate-actor-lib-0.0.2 (c (n "actor-lib") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "190k4xb7g5icvrbpm25shsr9ayh29vpwl7cnf4v1p1gwxs34fk1y") (y #t)))

(define-public crate-actor-lib-0.1.0 (c (n "actor-lib") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10pgyjwxzjvl2pw7spr7dilhd0kydb8g73bpkpjzy07pry2cdkhs") (y #t)))

