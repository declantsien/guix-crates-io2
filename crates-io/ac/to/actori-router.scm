(define-module (crates-io ac to actori-router) #:use-module (crates-io))

(define-public crate-actori-router-0.2.4 (c (n "actori-router") (v "0.2.4") (d (list (d (n "bytestring") (r "^0.1.2") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kgiz3rxfivxayjplqh2vcibz49x1fnzhgpqp4kyplwdhq58cgj2") (f (quote (("default" "http"))))))

