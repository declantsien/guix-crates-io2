(define-module (crates-io ac to actorlib) #:use-module (crates-io))

(define-public crate-actorlib-0.1.0 (c (n "actorlib") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q0jhx0ivy0dyymwjjy56y98srspxzia7inw26imgm6039sv5r7i") (y #t)))

(define-public crate-actorlib-0.1.1 (c (n "actorlib") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12k1xg9j4gyn6150f077xg70q47kqsn1inh9i1hpqsdcisy1pc4z") (y #t)))

(define-public crate-actorlib-0.1.2 (c (n "actorlib") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dvqfv0n1fxd7bydsakd28shhq2b2i4rcq94xz81q7ssv3mxgpxv") (y #t)))

(define-public crate-actorlib-1.0.0 (c (n "actorlib") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1snz7yjg9h3y9abqjf3qi4ddi8nspr14phg9971sfmr9sz3g5zv6") (y #t)))

(define-public crate-actorlib-1.1.0 (c (n "actorlib") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iwms4vx18d6mnq1sdlmfqd8ckvxzw73avqhdf4pwvwqpz7dpsl1") (y #t)))

(define-public crate-actorlib-1.2.0 (c (n "actorlib") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w59xfgxf1j42lczi4rs7a9vv5rp505kabv5zgpbl8yy92985v9r") (y #t)))

(define-public crate-actorlib-1.2.1 (c (n "actorlib") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pimh6z61ar85gp01vvx8hyfjz1v4ccyiri3dscs5zlbkzrhjrc9") (y #t)))

(define-public crate-actorlib-1.2.2 (c (n "actorlib") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wcw0b3irv0v5k88qykxir8n2y66d0x5b5a4b1dhjh4ccsmcbd8s") (y #t)))

(define-public crate-actorlib-1.2.3 (c (n "actorlib") (v "1.2.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yvb61sjip52yfpah43c68zxa7zzczfhgmmk6xy8v9arrhw74m09") (y #t)))

(define-public crate-actorlib-1.2.4 (c (n "actorlib") (v "1.2.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "032h9pliixmqyh2705klv3pczvb6qz606nxn4mdxw020wqzsn3z8") (y #t)))

(define-public crate-actorlib-1.2.5 (c (n "actorlib") (v "1.2.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6c9czzn5hdyq9lj975ca984aa1v79aqdywy9qdn07l6kbvgcgx") (y #t)))

(define-public crate-actorlib-1.2.6 (c (n "actorlib") (v "1.2.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0011ivizji27a0r3qrxg8b2cyakx0ncdc9852znc6k78fr9fy15y") (y #t)))

(define-public crate-actorlib-1.2.7 (c (n "actorlib") (v "1.2.7") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1whd7kmgm1pvh7rgrmbrb4q2mlpizfd8mv4ys2iv8m13yrs67i") (y #t)))

(define-public crate-actorlib-1.2.8 (c (n "actorlib") (v "1.2.8") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n8kw5q1zxjaavb80lnbvdv7nhypnnqqgvs25pjglz32yzdv85cx") (y #t)))

(define-public crate-actorlib-1.2.9 (c (n "actorlib") (v "1.2.9") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09abvq8f7bj6ci2xd7nsw7bbgqnf838kclbxxdmmqcnxgvdnir0p") (y #t)))

(define-public crate-actorlib-1.3.1 (c (n "actorlib") (v "1.3.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q3wyhdwnalsxparhrja8l5pqw03pfk6v7cz1nfnwy26jiff9agl") (y #t)))

