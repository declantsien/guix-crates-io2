(define-module (crates-io ac to actori-session) #:use-module (crates-io))

(define-public crate-actori-session-0.3.0 (c (n "actori-session") (v "0.3.0") (d (list (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actori-web") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1ysfz1ibj9yanwb9ipp3yqc59jzdj7rqh152b89w2a97wna0dp98") (f (quote (("default" "cookie-session") ("cookie-session" "actori-web/secure-cookies"))))))

