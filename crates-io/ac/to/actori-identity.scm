(define-module (crates-io ac to actori-identity) #:use-module (crates-io))

(define-public crate-actori-identity-0.2.1 (c (n "actori-identity") (v "0.2.1") (d (list (d (n "actori-http") (r "^1.0.1") (d #t) (k 2)) (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "actori-web") (r "^2.0.0") (f (quote ("secure-cookies"))) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1rbxskhi79l2ypphj0zg554b9j5jfvfayflrgnjwg7ywz85fck40")))

