(define-module (crates-io ac to actori-codec) #:use-module (crates-io))

(define-public crate-actori-codec-0.2.0 (c (n "actori-codec") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (k 0)) (d (n "tokio-util") (r "^0.2.0") (f (quote ("codec"))) (k 0)))) (h "1mjlsai2xpj3pr9gwj3abw07g5nq93bi013ypi7w4mv7vxkrszkx")))

