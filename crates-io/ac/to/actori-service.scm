(define-module (crates-io ac to actori-service) #:use-module (crates-io))

(define-public crate-actori-service-1.0.5 (c (n "actori-service") (v "1.0.5") (d (list (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "1vyipk96s81pbdmigmfyiwngrkm46mfyibbbmsyxs5iyd2vd8mr7")))

