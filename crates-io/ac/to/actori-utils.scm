(define-module (crates-io ac to actori-utils) #:use-module (crates-io))

(define-public crate-actori-utils-1.0.6 (c (n "actori-utils") (v "1.0.6") (d (list (d (n "actori-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actori-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actori-service") (r "^1.0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "14qjqgncaijnpvgjmxglxxs0y97qyxph0z1ak9ygbxsw2fz85xdr")))

