(define-module (crates-io ac to actor) #:use-module (crates-io))

(define-public crate-actor-0.1.0 (c (n "actor") (v "0.1.0") (h "0gn5fzq7js3wg900pr9bpd19s0g1r072hya80d8a8sj1c0fkswh4")))

(define-public crate-actor-0.1.1 (c (n "actor") (v "0.1.1") (h "1saai471d6jfmzjjqqqx36sxpp2qz6ighh368yav53wnnx074qzc")))

