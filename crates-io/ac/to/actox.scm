(define-module (crates-io ac to actox) #:use-module (crates-io))

(define-public crate-actox-0.1.0 (c (n "actox") (v "0.1.0") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "14xpmdv02zfgjnk0yjn0zjhsr8iyy6rkrbsjljc4p0j2hypxr28w") (y #t)))

(define-public crate-actox-0.1.1 (c (n "actox") (v "0.1.1") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "075iw4scva0nzdicwz63n86j3gz23k3pfiy08m906c5kjmsrchs8") (y #t)))

(define-public crate-actox-0.1.2 (c (n "actox") (v "0.1.2") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0zbd5fksah0gyb294jmv116f1msff2dlgwzp4bk087abr0xl428b") (y #t)))

(define-public crate-actox-0.1.3 (c (n "actox") (v "0.1.3") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0h0rfjdmdc06f9cca1i83gpg19rji6j9cq1j74i97vmn4227kbil") (y #t)))

(define-public crate-actox-0.1.4 (c (n "actox") (v "0.1.4") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0q2g4mm3mk4xg7ar7wyij20kirg5y53p2lc47v8amzxbgvaqppnf") (y #t)))

(define-public crate-actox-0.1.5 (c (n "actox") (v "0.1.5") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "134cd18kkcgb5smr3w9rq0lfqvzgciips1pfinzsmxfp8dg88cp2") (y #t)))

(define-public crate-actox-0.2.0 (c (n "actox") (v "0.2.0") (h "1qn3v0zlgd9fgx3sjcwmy3a67gw0gh57pwxyf8lsf05zf1vc7dkl") (f (quote (("default")))) (y #t)))

(define-public crate-actox-0.2.1 (c (n "actox") (v "0.2.1") (h "1mv1arlnszf0wzwyikq23mvpw52d8y92s1206j3yca5zxij6qbgc") (f (quote (("default")))) (y #t)))

