(define-module (crates-io ac to actori-web-codegen) #:use-module (crates-io))

(define-public crate-actori-web-codegen-0.2.0 (c (n "actori-web-codegen") (v "0.2.0") (d (list (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0xhmrsj4nxzvk9km3c2qz59ym1n4zklfdi451n9m5zb78i853rjw")))

