(define-module (crates-io ac to actori-web-actors) #:use-module (crates-io))

(define-public crate-actori-web-actors-2.0.0 (c (n "actori-web-actors") (v "2.0.0") (d (list (d (n "actori") (r "^0.9.0") (d #t) (k 0)) (d (n "actori-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actori-http") (r "^1.0.1") (d #t) (k 0)) (d (n "actori-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actori-web") (r "^2.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "183zj12nxpgrah4n21ispwyy29v067ygd54200kfb1h1zly5hgcr")))

