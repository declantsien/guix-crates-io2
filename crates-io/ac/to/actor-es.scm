(define-module (crates-io ac to actor-es) #:use-module (crates-io))

(define-public crate-actor-es-0.15.0 (c (n "actor-es") (v "0.15.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "bson") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "riker") (r "^0.4.1") (d #t) (k 0)) (d (n "riker-es-macros") (r "^0.1") (d #t) (k 0)) (d (n "riker-patterns") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "v5"))) (d #t) (k 0)))) (h "04842axrnjpx2x5ljq0is3wjjsin0q1nsgg5ai7xid0z8vbayzg8")))

