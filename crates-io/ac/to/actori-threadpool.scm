(define-module (crates-io ac to actori-threadpool) #:use-module (crates-io))

(define-public crate-actori-threadpool-0.3.1 (c (n "actori-threadpool") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "16a7dw51ix9kiwn1q5i0fnnnjyp2mbq94vidsx7hmn8dzs2svvav")))

