(define-module (crates-io ac to actori-rt) #:use-module (crates-io))

(define-public crate-actori-rt-1.0.0 (c (n "actori-rt") (v "1.0.0") (d (list (d (n "actori-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "actori-threadpool") (r "^0.3") (d #t) (k 0)) (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("rt-core" "rt-util" "io-driver" "tcp" "uds" "udp" "time" "signal" "stream"))) (k 0)))) (h "0rnla2lw1a1zv0vsyxzami9xw9xzzzvl48qh5xlin6hbja0n70li")))

