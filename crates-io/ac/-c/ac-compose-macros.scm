(define-module (crates-io ac -c ac-compose-macros) #:use-module (crates-io))

(define-public crate-ac-compose-macros-0.3.0 (c (n "ac-compose-macros") (v "0.3.0") (d (list (d (n "ac-primitives") (r "^0.6") (k 0)) (d (n "log") (r "^0.4.14") (k 0)))) (h "05n6i30r8qv4fgh8abdmfcjac6zmnsjv5472l3384ypg64y8v2na") (f (quote (("std" "log/std" "ac-primitives/std") ("disable_target_static_assertions" "ac-primitives/disable_target_static_assertions") ("default" "std"))))))

(define-public crate-ac-compose-macros-0.5.0 (c (n "ac-compose-macros") (v "0.5.0") (d (list (d (n "ac-primitives") (r "^0.9.1") (k 0)) (d (n "codec") (r "^3.6.1") (d #t) (k 2) (p "parity-scale-codec")) (d (n "frame-metadata") (r "^16.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)))) (h "00nk8rjlaz3823ad38ax17v3lsxzf2v5k2yh22hrc3iz6ms2kmib") (f (quote (("sync-api" "maybe-async/is_sync") ("std" "log/std" "ac-primitives/std") ("disable_target_static_assertions" "ac-primitives/disable_target_static_assertions") ("default" "std" "sync-api"))))))

