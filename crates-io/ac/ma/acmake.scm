(define-module (crates-io ac ma acmake) #:use-module (crates-io))

(define-public crate-acmake-0.1.0 (c (n "acmake") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "01s80i39sg5rpb3svhnfxm2kinl2a10zx2z22nhwblrnyd3q92mq")))

(define-public crate-acmake-0.1.1 (c (n "acmake") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "05wvqg98sp3mgncmr8719d219a2v16n51h6p4mb7zmrjp1djkshw")))

