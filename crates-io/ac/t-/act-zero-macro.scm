(define-module (crates-io ac t- act-zero-macro) #:use-module (crates-io))

(define-public crate-act-zero-macro-0.1.0 (c (n "act-zero-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pj35xnpk8l4mpxckdybpxky05wvarnqw8ybv44245l6hiybqb1j")))

(define-public crate-act-zero-macro-0.2.0 (c (n "act-zero-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "065sn0923akp3m1ygvjwgx7xjkcl76l69syn3z5n2x2h78lbj7m4")))

(define-public crate-act-zero-macro-0.2.1 (c (n "act-zero-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "02rxj41ba5h2fay7z0wlj8adqi3bdhwjj37hi3x2iw0jz2d4rk00")))

