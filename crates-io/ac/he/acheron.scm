(define-module (crates-io ac he acheron) #:use-module (crates-io))

(define-public crate-acheron-0.1.0 (c (n "acheron") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "im") (r "^12.3.3") (d #t) (k 0)))) (h "015p2dndg8qpqbb48d9n9hcchnw5y2kyxap349mm1y81ymyz6z1j")))

