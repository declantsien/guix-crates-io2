(define-module (crates-io ac i- aci-registry) #:use-module (crates-io))

(define-public crate-aci-registry-1.0.0 (c (n "aci-registry") (v "1.0.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 1)))) (h "1j3w9ravvsk8w89gjwnyhzwcqiz1szsd349m71sfzjq53dv3wjrj") (f (quote (("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1.0.1 (c (n "aci-registry") (v "1.0.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 1)))) (h "002shlkw0xj9y7mxf01lk4l7pf1hww4cmz38159g8a1378pzkxdi") (f (quote (("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1.1.0 (c (n "aci-registry") (v "1.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 1)))) (h "1ln4lrwdbngk0xf8a3cjxks2pv2nvx2hlg7wvp97llv7b6ya3gxq") (f (quote (("non-authorative") ("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1.1.1 (c (n "aci-registry") (v "1.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 1)))) (h "0vw43mdd768hfziqv9q1ll1ydb3vh332pgja0n4fkmycnhckg172") (f (quote (("non-authorative") ("fixed-repr") ("extended-info"))))))

