(define-module (crates-io ac ro acro) #:use-module (crates-io))

(define-public crate-acro-0.1.0 (c (n "acro") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "0ff61s7csxhkmf264yydzy199x4bnrm9v9gcg08r6g44gz3bv0c1") (y #t)))

(define-public crate-acro-0.2.0 (c (n "acro") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)))) (h "1xyx0hr86s09y6q7np85g744sy5rmrksfcn4liiagc0pd1y45ggd")))

