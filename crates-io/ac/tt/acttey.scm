(define-module (crates-io ac tt acttey) #:use-module (crates-io))

(define-public crate-acttey-0.0.0 (c (n "acttey") (v "0.0.0") (d (list (d (n "acttey_app") (r "^0.0.0") (d #t) (k 0)) (d (n "acttey_ecs") (r "^0.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.37") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "19xsgh7yvx30ga62axfl5jd047j7pjirg3y16p5dk8lx3i2xz5nq")))

