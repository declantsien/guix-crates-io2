(define-module (crates-io ac tt acttey_app) #:use-module (crates-io))

(define-public crate-acttey_app-0.0.0 (c (n "acttey_app") (v "0.0.0") (d (list (d (n "acttey_render") (r "^0.0.0") (d #t) (k 0)) (d (n "acttey_util") (r "^0.0.0") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.37") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)) (d (n "winit") (r "^0.29.1-beta") (d #t) (k 0)))) (h "1ylav9dahgdhr7d8p190a4ib2g760dvf1bbsiwcwg9cfgwhgy2m7")))

