(define-module (crates-io ac tt acttey_render) #:use-module (crates-io))

(define-public crate-acttey_render-0.0.0 (c (n "acttey_render") (v "0.0.0") (d (list (d (n "acttey_math") (r "^0.0.0") (d #t) (k 0)) (d (n "acttey_primitive") (r "^0.0.0") (d #t) (k 0)) (d (n "acttey_util") (r "^0.0.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Window" "Document" "Element" "HtmlCanvasElement" "MouseEvent"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.17.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.1-beta") (d #t) (k 0)))) (h "0h061jfsxc96gj2jdj5v8irjh7bdh1hgnv74xiad7gczb0ahnar2")))

