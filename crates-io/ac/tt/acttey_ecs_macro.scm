(define-module (crates-io ac tt acttey_ecs_macro) #:use-module (crates-io))

(define-public crate-acttey_ecs_macro-0.0.0 (c (n "acttey_ecs_macro") (v "0.0.0") (d (list (d (n "acttey_ecs_trait") (r "^0.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xxz7g7xfw4ws2aa8xry619iybj1jl8zbxkgff1c9hp84dz0rnca")))

