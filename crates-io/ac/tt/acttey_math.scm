(define-module (crates-io ac tt acttey_math) #:use-module (crates-io))

(define-public crate-acttey_math-0.0.0 (c (n "acttey_math") (v "0.0.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive" "min_const_generics"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1ih9g27id1pr093yddbmx1a85j0dsm03c5sgm0g4km8lga384pqn")))

