(define-module (crates-io ac yc acyclic-network) #:use-module (crates-io))

(define-public crate-acyclic-network-0.0.1 (c (n "acyclic-network") (v "0.0.1") (d (list (d (n "fixedbitset") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zhsc2qb76w0ir5w2zr3dwww4qar39mh6npfm5n4yahzdffanlxc")))

(define-public crate-acyclic-network-0.1.0 (c (n "acyclic-network") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "02hhmajly1fy0pracqr9zm2ggss17006p0p59vp4m6552cscj2q0")))

(define-public crate-acyclic-network-0.2.0 (c (n "acyclic-network") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1w2wlsxp2xc0w33qkhmc4cx7cap17xim5n3nz0mzpslyv9qfh57w")))

