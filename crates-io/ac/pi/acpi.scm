(define-module (crates-io ac pi acpi) #:use-module (crates-io))

(define-public crate-acpi-0.1.0 (c (n "acpi") (v "0.1.0") (h "1am481xpc6jawz99dvpjvnbvi263cbpli4rgl106xd7rz2pnn9cs")))

(define-public crate-acpi-0.2.0 (c (n "acpi") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1axqllxjbjy1xyyyxgmd6p3qq6ws9azipja9fm9rbfvs3h8959d2")))

(define-public crate-acpi-0.2.1 (c (n "acpi") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r61cc68jckxs8b2iv4db5mkmn6xfylsns4w08fif47xihpa7rkq")))

(define-public crate-acpi-0.3.0 (c (n "acpi") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "0jzykz9m546jnvbawhaidgxvhxjcl48660yy5p33amxbiw5zwm3z")))

(define-public crate-acpi-0.4.0 (c (n "acpi") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1smcd6gjziqraj6nfaj5siw3vpyqka3sac3rix7xs8n3pl3df61c")))

(define-public crate-acpi-0.5.0 (c (n "acpi") (v "0.5.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "0rxk7m3qhx8072qxrnvk9vgqfl76ifh23zynk97f6fada76fn525")))

(define-public crate-acpi-0.6.0 (c (n "acpi") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ppzc36l4zg11xnlyvzprms42aaf4341hzqjy6c2vfnhswjw1n4j")))

(define-public crate-acpi-0.7.0 (c (n "acpi") (v "0.7.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fa2jh6xfzv3lkb255rbw3zvrsajpcndg7x2gqhljk9208jk1sna")))

(define-public crate-acpi-0.8.0 (c (n "acpi") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15c9kgk83g67bil5qj99m38ri52d98pr9ibpfayxssq9gs55lhiz")))

(define-public crate-acpi-1.0.0 (c (n "acpi") (v "1.0.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "013zada7fs8wjni4a74994jcss4l9p9mw95f6cvxkn2n7361qnrz")))

(define-public crate-acpi-1.1.0 (c (n "acpi") (v "1.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fk9qbvni9hbhgpghlj1z83j15xzknks2h7c4qbh7d0cl82fxhwz")))

(define-public crate-acpi-2.0.0-pre1 (c (n "acpi") (v "2.0.0-pre1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "1z6zd0gmf222m7cfc4v8wyhm9sah6abba8pf2iczcvvnnicwdqpi")))

(define-public crate-acpi-2.0.0 (c (n "acpi") (v "2.0.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "1gqf01cqmqy48a2bnfb6dmharqqwmlg6cl19r621wjwxnlwkiayk")))

(define-public crate-acpi-2.1.0 (c (n "acpi") (v "2.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "0g7k8sv6fwncxsy4k14lkbndz7ar44xynzb8fb5rp9va9fd4r4a6")))

(define-public crate-acpi-2.2.0-pre0 (c (n "acpi") (v "2.2.0-pre0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "=1.1.0-pre0") (d #t) (k 0)))) (h "1awpdcrbjq42v13gmd6j83lx9iw6hdsml51h7g1f6xbydxqqvy8z")))

(define-public crate-acpi-2.2.0 (c (n "acpi") (v "2.2.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "1447k53dj7c8jjh9yibv5ijrv9xdrvipjhm0nchbvvylav4dx3jq")))

(define-public crate-acpi-2.3.0 (c (n "acpi") (v "2.3.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "1cbs1y8h87lyidii9d0g5d086snsrv2pkdnb0gj3b2dwi2kzc0bl")))

(define-public crate-acpi-2.3.1 (c (n "acpi") (v "2.3.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^1") (d #t) (k 0)))) (h "17y044dan4lim9c7nj2w6bg88k83yg2zjmyfrswg06jchwm8pk0k")))

(define-public crate-acpi-3.0.0 (c (n "acpi") (v "3.0.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^2") (d #t) (k 0)))) (h "1ixh8b3klpq0bndj5vi19gcl5ysajwn1hz9sjv1k7fhqb3xsppih")))

(define-public crate-acpi-3.1.0 (c (n "acpi") (v "3.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^2") (d #t) (k 0)))) (h "11nm1gapajvwbya6q6p8bccwa886xix5kjk1w44al8gk35bg295q")))

(define-public crate-acpi-4.0.0 (c (n "acpi") (v "4.0.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^2") (d #t) (k 0)))) (h "1bnrywq1qdpnjiydrlrpimqi8wfyvivwlycvvfj8zv3m9wr7m0gf")))

(define-public crate-acpi-4.1.0 (c (n "acpi") (v "4.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^2") (d #t) (k 0)))) (h "016hbfarl15nbscl8phc4nhmx8z6ymd14igdwmkyliz7qr4pc8sm")))

(define-public crate-acpi-4.1.1 (c (n "acpi") (v "4.1.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rsdp") (r "^2") (d #t) (k 0)))) (h "05bdq9872pcpf5b7w2ppcb5ra24r7c3na5xy6njjwqvq66mlhkv5")))

(define-public crate-acpi-5.0.0 (c (n "acpi") (v "5.0.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hz4mc7dd69k9ga6k5hiysms6i9ayql2dfir3gv22h1hjn8l0j72") (f (quote (("default" "allocator_api" "alloc") ("allocator_api") ("alloc" "allocator_api"))))))

