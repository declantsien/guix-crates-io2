(define-module (crates-io ac pi acpica-sys) #:use-module (crates-io))

(define-public crate-acpica-sys-0.0.1 (c (n "acpica-sys") (v "0.0.1") (h "1kvgxp4y6l2m9ykpqhaj0s9lzflzsqj3769pzs2x90zqvh0cc91x")))

(define-public crate-acpica-sys-0.0.2 (c (n "acpica-sys") (v "0.0.2") (h "000szjb3k2hymzflvfp2sg8f6kl1m6akphzmzj0y1l9472ibb71v")))

(define-public crate-acpica-sys-0.0.3 (c (n "acpica-sys") (v "0.0.3") (h "01w8n5skwbp2c231h2np183nmf8lgdw26w7psw9kmj14n27cfmx2")))

(define-public crate-acpica-sys-0.0.4 (c (n "acpica-sys") (v "0.0.4") (h "1kdq475nhigz6glfnqgd9dsfw6i21hazbdk9igbqylly3p371mrz")))

