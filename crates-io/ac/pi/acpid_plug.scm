(define-module (crates-io ac pi acpid_plug) #:use-module (crates-io))

(define-public crate-acpid_plug-0.1.0 (c (n "acpid_plug") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1x8aav58zvvy8yvkn413w9icr059llx1s16r7808cr43bcd4m72n") (y #t)))

(define-public crate-acpid_plug-0.1.1 (c (n "acpid_plug") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0xi2cyk5p69klsh9kv6g2zxjlb595jmsfprj1c244qhwiizfff8z")))

(define-public crate-acpid_plug-0.1.2 (c (n "acpid_plug") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1c5l2xmxd7a793m4zy8n8lpi4czy1izbg99n6avqgr661g6b8ja7")))

