(define-module (crates-io ac pi acpitool) #:use-module (crates-io))

(define-public crate-acpitool-1.0.0 (c (n "acpitool") (v "1.0.0") (d (list (d (n "acpi_client") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0g662qcl96yai9lciliqm25xsvh6ccg595zsh8s7nkm1x9dk5k35")))

