(define-module (crates-io ac pi acpi_client) #:use-module (crates-io))

(define-public crate-acpi_client-0.1.0 (c (n "acpi_client") (v "0.1.0") (h "1lw5qwk981a5pdksadimadlgqdlf2h1vjm2b321ipjm87xlxgids")))

(define-public crate-acpi_client-0.2.0 (c (n "acpi_client") (v "0.2.0") (h "1hg3c3087bkg8961ja3kw96n9vdhh0hqydd8mnxwp1x94f2x6icj")))

(define-public crate-acpi_client-1.0.0 (c (n "acpi_client") (v "1.0.0") (h "1r36krzh0hb5diay256iikxppbrrq8jfrxpzvnz1jxhqdvz0i8sn")))

(define-public crate-acpi_client-1.1.0 (c (n "acpi_client") (v "1.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0ac9sjgvc1jb226qhynmbi7pr137rykx9f8b6jcdjify89vibw5i")))

(define-public crate-acpi_client-2.0.0 (c (n "acpi_client") (v "2.0.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1659lqc986i6asaw3zfalb0dvgj34lcdx6449j4g9z3g6mxas4wl")))

