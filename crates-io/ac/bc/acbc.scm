(define-module (crates-io ac bc acbc) #:use-module (crates-io))

(define-public crate-acbc-0.1.0 (c (n "acbc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (d #t) (k 0)))) (h "1w2fr6hkdfhdnivqxlnpyjirhv16617lpjs08xxbjb3zd8wk7iam")))

