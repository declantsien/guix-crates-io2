(define-module (crates-io ac cr accrete) #:use-module (crates-io))

(define-public crate-accrete-0.0.1 (c (n "accrete") (v "0.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k83c3ki2cnn47fzjqbx877faym4xc579pxmhfszwkkwxkdjdaig")))

(define-public crate-accrete-0.0.2 (c (n "accrete") (v "0.0.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z5hdydmrwmvn4a81985jq0gfbkvlyd5mp0q7987wkagcjhyxrbr") (y #t)))

(define-public crate-accrete-0.0.3 (c (n "accrete") (v "0.0.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19x6zy81rgqmyj60f1x73m9hqndx5xns6d8jgd2cpm3s0794h4wg") (y #t)))

(define-public crate-accrete-0.0.4 (c (n "accrete") (v "0.0.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08x3in42v2lclknvpq15nmmvqdn9hz7k0impygbl2rp0qrdqbdyc")))

(define-public crate-accrete-0.0.5 (c (n "accrete") (v "0.0.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c6fjlxy39phjrhifvf1waf38p6xcg1kqalc3b20cx1gj3mx1pfi")))

(define-public crate-accrete-0.0.6 (c (n "accrete") (v "0.0.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10b1hwl4bfaldiqvx12i6nl45nlx02d96ynpiy78i56q5psrqjjz")))

(define-public crate-accrete-0.0.7 (c (n "accrete") (v "0.0.7") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cligpb3d33acgrzfdnjxj02xh73wm2ld94i97rb3fm7b2c0p5g4")))

(define-public crate-accrete-0.1.0 (c (n "accrete") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fw663w5z6fd2q7v0kv9dfw3ljjrqx51c9m9jsri8gd1b643x91s")))

(define-public crate-accrete-0.1.1 (c (n "accrete") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r69d2xfcwbagh58xq9an210x3xl8igg24798zxz2yr6jkgf7x00")))

(define-public crate-accrete-0.1.2 (c (n "accrete") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ggjra0rrpabcmyjvdsa1dn3y4zgx89qmg2w46zwc0087xdr9zph")))

(define-public crate-accrete-0.1.3 (c (n "accrete") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c7p1awhrimi46xiph3byf4l2xjdkb9kplglk41y7r3micv7565j")))

(define-public crate-accrete-0.1.4 (c (n "accrete") (v "0.1.4") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0s9akhk2nhj23yz9jah0wgfpyrrvrzbia9z4c3b7376vrkxnf9jk") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-accrete-0.1.5 (c (n "accrete") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02qap9r2k7l65whdd8rab2rcmhnxhclzb4ifrdjllpa4lwsacxsg")))

(define-public crate-accrete-0.1.6 (c (n "accrete") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ddxmg8mpc70r61y25psk5gg7ad6sqm42jx4xayn3ycah69gpmj")))

(define-public crate-accrete-0.1.7 (c (n "accrete") (v "0.1.7") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "118xb3jrw96isa05rfjpls9rlsd2cyjdj436933ksyghg2hxya2l") (f (quote (("default" "console_error_panic_hook" "wee_alloc"))))))

(define-public crate-accrete-0.2.0 (c (n "accrete") (v "0.2.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "11ffbqy18zzlczhnqasrm1fq3lkkspmzxa0clm80lf1zclbd2ni7") (f (quote (("default" "console_error_panic_hook" "wee_alloc"))))))

