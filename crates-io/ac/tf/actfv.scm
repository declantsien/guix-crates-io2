(define-module (crates-io ac tf actfv) #:use-module (crates-io))

(define-public crate-actfv-0.1.0 (c (n "actfv") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)))) (h "1pibcvsr58j33v6nblpd94sbvlpddwk07vhigcj8lnibbj5fz3fq")))

(define-public crate-actfv-0.1.1 (c (n "actfv") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)))) (h "11i1rdn1h14djsr879ppzmsn0rlw2hjvqpqpl0wyaqpgsb91w5wa")))

(define-public crate-actfv-0.1.2 (c (n "actfv") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)))) (h "01k8whmhp7avryqnp2avb7pfnpsqxgh3jrmyhryqfd3axl47dcmb")))

(define-public crate-actfv-0.2.0 (c (n "actfv") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.1.0") (d #t) (k 0)))) (h "1hpjds5yahmf2rxihdny33fi2sncilvsnk2dz6m1zggick01b0cn")))

(define-public crate-actfv-0.3.0 (c (n "actfv") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.1.0") (d #t) (k 0)))) (h "06bnl210mxw8iybnyg4kjgz05zf619zapalanan03gp446y1bp94")))

