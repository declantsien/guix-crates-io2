(define-module (crates-io ac ri acril-rt) #:use-module (crates-io))

(define-public crate-acril-rt-0.1.0 (c (n "acril-rt") (v "0.1.0") (d (list (d (n "acril") (r "^0.1.0") (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "0p5w0mv7wzdcx82ydq08lzz6dyxfqblqp6vx8ig4vy1pl1kq2jh2")))

