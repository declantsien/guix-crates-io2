(define-module (crates-io ac ri acril-macros) #:use-module (crates-io))

(define-public crate-acril-macros-0.1.0 (c (n "acril-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg7gjw2vnvp8pfw2f499amy1mlpx6w0k7qn1iyssicwdqs3zqgr")))

