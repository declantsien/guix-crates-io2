(define-module (crates-io ac -p ac-power) #:use-module (crates-io))

(define-public crate-ac-power-0.1.0 (c (n "ac-power") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "idsp") (r "^0.15.1") (d #t) (k 0)))) (h "0w77rrni3slyib0vp662f95b58r188fgapr9yqflyxb7587wwlc3")))

