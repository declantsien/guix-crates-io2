(define-module (crates-io ac es acestream_client) #:use-module (crates-io))

(define-public crate-acestream_client-0.1.0 (c (n "acestream_client") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "1ykcpzjaw5d6mzabi5ljgd6vnhajnw857265n0jnlx9d9s48hh0s")))

(define-public crate-acestream_client-0.1.1 (c (n "acestream_client") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0g74gcwys8pnnmgm6v019pc12fhfbs1h9hfckxax0wqy4y9wcdmi")))

