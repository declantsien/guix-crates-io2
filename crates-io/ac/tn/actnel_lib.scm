(define-module (crates-io ac tn actnel_lib) #:use-module (crates-io))

(define-public crate-actnel_lib-0.1.0 (c (n "actnel_lib") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0nsmcavwbnlrbgw6xcaqqchwdkp29ndnkaiaxlk2damjrbzpfcm4")))

(define-public crate-actnel_lib-0.1.1 (c (n "actnel_lib") (v "0.1.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "008n65b9kaawaixfnvf5hlf09vrcsj3k8afb5ry8c290rps99aq6")))

(define-public crate-actnel_lib-0.1.2 (c (n "actnel_lib") (v "0.1.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1fr3mryjjxycrw6p6xvl7z0pfxn288c0r236rpqpc0dd1pcmk45w")))

(define-public crate-actnel_lib-0.1.4 (c (n "actnel_lib") (v "0.1.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1hdn36r0hyc88f1zndn1d1p1ilq6wfhwqzbz3nvn9rsf09r8clgf")))

(define-public crate-actnel_lib-0.1.5 (c (n "actnel_lib") (v "0.1.5") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "machineid-rs") (r "^1.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0laxhqnv7v2r3h4jqyka0p2d227kc8xkkxwvxbkp68p8w6imssim")))

