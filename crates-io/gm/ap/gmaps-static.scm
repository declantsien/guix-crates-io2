(define-module (crates-io gm ap gmaps-static) #:use-module (crates-io))

(define-public crate-gmaps-static-0.0.1 (c (n "gmaps-static") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "querystring") (r "^1.1.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "157ahj4nyvrjy6418piaq6nvwx5sq7j7h85v7ysrpa40fig2pnc9")))

