(define-module (crates-io gm se gmserverplugin) #:use-module (crates-io))

(define-public crate-gmserverplugin-1.0.0 (c (n "gmserverplugin") (v "1.0.0") (d (list (d (n "gmod") (r "^8") (f (quote ("hax"))) (k 0)))) (h "0dmkchxxzs2ibn79rlpk8mh46cpmgzlh1jdk830wj2kxhv7ca7a8")))

(define-public crate-gmserverplugin-2.0.0 (c (n "gmserverplugin") (v "2.0.0") (d (list (d (n "gmod") (r "^12.0.1") (f (quote ("hax"))) (k 0)))) (h "0a5qkcv42ds1jymgnzmw2xrsh32g9j0qmv40p72fhvhxy87bs7v7")))

(define-public crate-gmserverplugin-2.0.1 (c (n "gmserverplugin") (v "2.0.1") (d (list (d (n "gmod") (r "^14.0.1") (f (quote ("hax"))) (k 0)))) (h "1f9b6p62hy9jmsxi1pm22b5j4rwrzn9938q1pibpl0i89hc8yq00")))

