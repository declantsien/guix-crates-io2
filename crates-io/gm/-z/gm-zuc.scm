(define-module (crates-io gm -z gm-zuc) #:use-module (crates-io))

(define-public crate-gm-zuc-0.9.0 (c (n "gm-zuc") (v "0.9.0") (h "0h0fyqfy4hs5ch3v3xpjp7jkbqy6ix0v5sfsgj8rn1r0g4v2ndr7")))

(define-public crate-gm-zuc-0.10.0 (c (n "gm-zuc") (v "0.10.0") (h "043rlzcx06dhbv6vyvlg5x204gc2wyzfanwj8ac3iss95x86g6l8")))

(define-public crate-gm-zuc-0.10.1 (c (n "gm-zuc") (v "0.10.1") (h "0sn5l0vfsm691ywnx8f89dmcwiq1kjb9h667gvxfqwm8d1bvlbhk")))

