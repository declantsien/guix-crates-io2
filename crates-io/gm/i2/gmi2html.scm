(define-module (crates-io gm i2 gmi2html) #:use-module (crates-io))

(define-public crate-gmi2html-0.1.6 (c (n "gmi2html") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "05q4dl65j31knlqz499v5sz5x0qpd76x4qkpybk6ww3941sx9fl1")))

(define-public crate-gmi2html-0.1.7 (c (n "gmi2html") (v "0.1.7") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0alr08clf89ylan4f10qrhvlvr4vdd836c1pfq2g1k3d68bq3vni")))

(define-public crate-gmi2html-0.1.8 (c (n "gmi2html") (v "0.1.8") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0p9vxk6vc0l0aj0dbdb4n1vd5s3pg69bnx5r4d19ss70k7pjzyip")))

