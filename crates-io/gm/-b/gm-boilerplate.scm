(define-module (crates-io gm -b gm-boilerplate) #:use-module (crates-io))

(define-public crate-gm-boilerplate-0.1.0 (c (n "gm-boilerplate") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "glitch-in-the-matrix") (r "^0.13") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0apf4ip7jvr5w7hchr6hjiqnki5mngpwfnilbn2sxxqarfycwh6v")))

