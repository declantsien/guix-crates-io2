(define-module (crates-io gm bm gmbm) #:use-module (crates-io))

(define-public crate-gmbm-0.6.0 (c (n "gmbm") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "pelite") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0b2v7dnwlbvans3irgi9167lip1kfhzfv4qgs48iwzhivs257m50")))

