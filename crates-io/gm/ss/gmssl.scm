(define-module (crates-io gm ss gmssl) #:use-module (crates-io))

(define-public crate-gmssl-0.1.0 (c (n "gmssl") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "gmssl-sys")) (d (n "foreign-types") (r "^0.3.1") (d #t) (k 0)) (d (n "gmssl-macros") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1ml1k0wrx1dlxl0lsjh9m7bgmw6hx0nna9i7r907h6w957zw6713") (f (quote (("vendored" "ffi/vendored") ("v111") ("v110") ("v102") ("v101") ("unstable_boringssl" "ffi/unstable_boringssl") ("default") ("bindgen" "ffi/bindgen"))))))

