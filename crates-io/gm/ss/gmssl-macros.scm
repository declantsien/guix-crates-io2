(define-module (crates-io gm ss gmssl-macros) #:use-module (crates-io))

(define-public crate-gmssl-macros-0.1.0 (c (n "gmssl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11073plj2ham3kjp0lgp9pjxz3hi598brl3rr221m4nkm6fshy2w")))

