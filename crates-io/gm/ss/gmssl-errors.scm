(define-module (crates-io gm ss gmssl-errors) #:use-module (crates-io))

(define-public crate-gmssl-errors-0.1.0 (c (n "gmssl-errors") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "gmssl-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wgixi5y2nq8ii0lffgmb4ndfc919v9rgh574lbay4x13yl7vlyg")))

