(define-module (crates-io gm ss gmssl-src) #:use-module (crates-io))

(define-public crate-gmssl-src-0.1.0 (c (n "gmssl-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1grgrwh4hrqbqlgwa8wkwgy23l016094w30nx15cn7lcn6p5yldj") (f (quote (("weak-crypto") ("seed") ("legacy") ("idea") ("default") ("camellia"))))))

