(define-module (crates-io gm pr gmprs) #:use-module (crates-io))

(define-public crate-gmprs-0.1.0 (c (n "gmprs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "055arl6hdzg4g646micp9ap3jmynnj0n50mlwsgnpg8q5lgw1lrw") (y #t)))

(define-public crate-gmprs-0.1.1 (c (n "gmprs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "03n7pwcagzqlzjck2h66fvs971l5jqcz54s9jcglcv8cwa4k1rpq") (y #t)))

