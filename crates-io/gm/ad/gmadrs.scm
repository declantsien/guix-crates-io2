(define-module (crates-io gm ad gmadrs) #:use-module (crates-io))

(define-public crate-gmadrs-0.1.0 (c (n "gmadrs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "gma") (r "^0.3.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "wildmatch") (r "^1.0.11") (d #t) (k 0)))) (h "0zsxsqfh736ski53c1jv148a2yvyvdd7aw5vcs9li8ahq4rgn4ig")))

(define-public crate-gmadrs-0.1.1 (c (n "gmadrs") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "gma") (r "^0.3.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "wildmatch") (r "^1.0.11") (d #t) (k 0)))) (h "0bw3aab0y2iz1p66mxrl731s2hr8b9j5hdap9f4s4j7swgr08qfb")))

(define-public crate-gmadrs-0.1.2 (c (n "gmadrs") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "gma") (r "^0.3.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)) (d (n "wildmatch") (r "^1.0.11") (d #t) (k 0)))) (h "17x82dmyscvr4dxzr31j64rl4ajvbc06fn17ccny688cxhcp2axb")))

