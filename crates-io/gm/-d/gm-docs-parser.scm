(define-module (crates-io gm -d gm-docs-parser) #:use-module (crates-io))

(define-public crate-gm-docs-parser-1.0.0 (c (n "gm-docs-parser") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "selectors") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "13bc6sppzn8rcgqd6z1hi2c21pq4hmj4m5vvbwqh0p6qxmby5z08")))

