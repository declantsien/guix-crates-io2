(define-module (crates-io gm p- gmp-sys) #:use-module (crates-io))

(define-public crate-gmp-sys-0.0.1 (c (n "gmp-sys") (v "0.0.1") (h "0ips8n7agwjmd6wbgk9dvvmlk64l15bc93cb1373pif766g1ghlj")))

(define-public crate-gmp-sys-0.0.2 (c (n "gmp-sys") (v "0.0.2") (h "1s5i5bzbcvr1vgjbv4fwd8x1k7kdbvj9lnprpdrw13il2rpic1y6")))

(define-public crate-gmp-sys-0.0.3 (c (n "gmp-sys") (v "0.0.3") (h "0jqia0rwjpkkm7rhiyz2yi26xc4dmzadryjfdnbvxxhwxsv9qiya")))

(define-public crate-gmp-sys-0.0.4 (c (n "gmp-sys") (v "0.0.4") (h "1hpiac9438g1g216gjh9a76ha3rjrwm9xfyaikxhqsdplkrawclq")))

(define-public crate-gmp-sys-0.0.5 (c (n "gmp-sys") (v "0.0.5") (h "0kfmv4a1cxx1y2bgds5pbw56rs6wdvpmsfik0pzznc8xkbgqd2yb")))

(define-public crate-gmp-sys-0.0.6 (c (n "gmp-sys") (v "0.0.6") (h "16anvs44pwck7f6vv9m8fpglhs5y87va3xz9bk8zsz9mx82fdy2c")))

(define-public crate-gmp-sys-0.0.7 (c (n "gmp-sys") (v "0.0.7") (h "1s9khrlxnvqhcn2cfyk8pv20i1l0sfp28drc8b0d9xnfwh79ddnx")))

(define-public crate-gmp-sys-0.1.0 (c (n "gmp-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g42fvhbfgjzwylk3h0mw98jj7sla5wn53vbgrglaaspx03h0rg5")))

