(define-module (crates-io gm p- gmp-mpfr) #:use-module (crates-io))

(define-public crate-gmp-mpfr-0.1.0 (c (n "gmp-mpfr") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "0p2l784sq97pifj0zzyqbxj7scjzd73lj4g22k0w3lrykalxnzwk")))

(define-public crate-gmp-mpfr-0.1.1 (c (n "gmp-mpfr") (v "0.1.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "17rlcwrgf5fqxfdpx2midfv5ssjjqydm6jds031f6mpkigylg9m6")))

(define-public crate-gmp-mpfr-0.2.0 (c (n "gmp-mpfr") (v "0.2.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "1x7k2v156zqlpgd1gfchv224q7vj1j6ay1icn1pg3bz7p2y3g9xn")))

(define-public crate-gmp-mpfr-0.3.0 (c (n "gmp-mpfr") (v "0.3.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "19z80jf7q1g55wdnpa71hb35cgsi37g9gm6w9mv11i6qam3cchvg")))

(define-public crate-gmp-mpfr-0.3.1 (c (n "gmp-mpfr") (v "0.3.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "1mbqnsnmn1ci1nikwp0ygv3f43pb8hw4psir05xin7n2m4ca3r67")))

(define-public crate-gmp-mpfr-0.3.2 (c (n "gmp-mpfr") (v "0.3.2") (d (list (d (n "gmp-mpfr-sys") (r "^0.2") (d #t) (k 0)))) (h "1c8hlbdr60ypyka0g69sx2kdfazi348h99hrk0p76maj79gb4i6p")))

(define-public crate-gmp-mpfr-0.4.0 (c (n "gmp-mpfr") (v "0.4.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)))) (h "1vkfca5j1n4lj9r4bwp9k6mgqvzbsq9k9n1j3zp11fs23bjqd2iw")))

(define-public crate-gmp-mpfr-0.4.1 (c (n "gmp-mpfr") (v "0.4.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)))) (h "0ck8jlj1piwzcx4xr0z51pdl8hcnckpybb54vwb2wwahx4zarg0a")))

(define-public crate-gmp-mpfr-0.5.0 (c (n "gmp-mpfr") (v "0.5.0") (h "0y7469bjpms0y8yagqgcs9rnw8396vqx5s169mks7haszppmb0gc")))

(define-public crate-gmp-mpfr-0.5.1 (c (n "gmp-mpfr") (v "0.5.1") (h "1f1jpry3cx1w6933y5q203ls8jd4d2c221cm76nk8qgadja27bws")))

(define-public crate-gmp-mpfr-0.5.2 (c (n "gmp-mpfr") (v "0.5.2") (h "1fm9hxa767xjrqic7z735zrp4mrx7jdd57qwif9423vx5968hz54")))

