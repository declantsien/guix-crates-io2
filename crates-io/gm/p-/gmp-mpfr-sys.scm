(define-module (crates-io gm p- gmp-mpfr-sys) #:use-module (crates-io))

(define-public crate-gmp-mpfr-sys-0.1.0 (c (n "gmp-mpfr-sys") (v "0.1.0") (h "1mahhqb0yj5ahvihcifx2cr152bkyv5y2rh5ixz9xpxsjav1n41w")))

(define-public crate-gmp-mpfr-sys-0.2.0 (c (n "gmp-mpfr-sys") (v "0.2.0") (h "1qq82vn2236v2h6d0vw371jkcbpd6jyym3y0b0lasdriblqyqkc3")))

(define-public crate-gmp-mpfr-sys-0.2.1 (c (n "gmp-mpfr-sys") (v "0.2.1") (h "1ih606sndqf4nx5y30f2kvxh2mn3j3i6bi315pqrpb72j50814jm")))

(define-public crate-gmp-mpfr-sys-0.2.2 (c (n "gmp-mpfr-sys") (v "0.2.2") (h "10hpy5jlw34fx3li9q9rk4s8cf4lgi66vgl54r29b0n1hk3hx620")))

(define-public crate-gmp-mpfr-sys-0.2.3 (c (n "gmp-mpfr-sys") (v "0.2.3") (h "0i4ya6bk1rd0ik72z0m57hppmlczbwh0wwqwwqcmyl0p7wgj6xa3")))

(define-public crate-gmp-mpfr-sys-0.3.0 (c (n "gmp-mpfr-sys") (v "0.3.0") (h "1986hbsgj5y2p5f83ckpzwgy4xjyg9a495ki5wfw5icgxz6h1n8w")))

(define-public crate-gmp-mpfr-sys-0.3.1 (c (n "gmp-mpfr-sys") (v "0.3.1") (h "09m3abjqxkv4wng8xawv718lilyjnk7wcp2ashjb0ifdmxfsizdh")))

(define-public crate-gmp-mpfr-sys-0.3.2 (c (n "gmp-mpfr-sys") (v "0.3.2") (h "1l8s04dskdydgldvic7r2l412rf0v2z3r4k6ys85kgz4bxf0m1ca")))

(define-public crate-gmp-mpfr-sys-0.4.0 (c (n "gmp-mpfr-sys") (v "0.4.0") (h "1dyd0jnb6cx3ka64vnr5z6yng6qds51m2w52q06yq7lbhzqimygw")))

(define-public crate-gmp-mpfr-sys-0.5.0 (c (n "gmp-mpfr-sys") (v "0.5.0") (h "05ha2jmv0j44vb79i9pi4ykw2frfwhmqipdfx3w3bzifl2jgg8ld")))

(define-public crate-gmp-mpfr-sys-0.5.1 (c (n "gmp-mpfr-sys") (v "0.5.1") (h "189mj2cxh803hkqxm4lvsic3741y1sgjx8xddrmb4lypd7va7jnn")))

(define-public crate-gmp-mpfr-sys-0.5.2 (c (n "gmp-mpfr-sys") (v "0.5.2") (h "0pq5lcqrz00s7r84yfqa25bgild37z8jp01cfgjsm4ch578b2a0s")))

(define-public crate-gmp-mpfr-sys-0.6.0 (c (n "gmp-mpfr-sys") (v "0.6.0") (h "1n9fyqmsfdh3xl2xvz8q7z70g4bqi20qajqj4n9fqcrq1s7qp1lq")))

(define-public crate-gmp-mpfr-sys-1.0.0 (c (n "gmp-mpfr-sys") (v "1.0.0") (h "1388zr91mbq0pi2pbcfjfpda4x74v4jgd75r2gnajx17c3751zlk")))

(define-public crate-gmp-mpfr-sys-1.0.1 (c (n "gmp-mpfr-sys") (v "1.0.1") (h "1k4jggdyb8a57i2vbzhjpqa7qkdhxf7qpznms24drizqzwwrh5pi")))

(define-public crate-gmp-mpfr-sys-1.0.2 (c (n "gmp-mpfr-sys") (v "1.0.2") (h "0120yqlhl05dxb7pywifdckqkbvs7ylpr8db8pd7lhhk160bqvgb") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc"))))))

(define-public crate-gmp-mpfr-sys-1.0.3 (c (n "gmp-mpfr-sys") (v "1.0.3") (h "02cgyknvx2zngzk33x2426hh9n2k1lr4y0w4dkmlff7kn94j23qg") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc"))))))

(define-public crate-gmp-mpfr-sys-1.0.4 (c (n "gmp-mpfr-sys") (v "1.0.4") (h "08f6i5apkjdm0ajynrqcys74c7mhl9fsnwrzhkyyx9kq556mc7h3") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.0.5 (c (n "gmp-mpfr-sys") (v "1.0.5") (h "04r14x514khwb36fk855jkrp67n9fi8dcr51r8fmfdqv7yq5hbbp") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.0.6 (c (n "gmp-mpfr-sys") (v "1.0.6") (h "1brj7qpdlpnpa8085197asp60hkgh5ag7lgzybm696nbyyvj71gg") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.0.7 (c (n "gmp-mpfr-sys") (v "1.0.7") (h "19w8nq6n4ywr0xqs4p955qrmq0zn02ph30yvsfcag0spy2vh82g7") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.0.8 (c (n "gmp-mpfr-sys") (v "1.0.8") (h "1zh6vkg60gaibjiz1qhzn7ij8ld6xcw4cya8b6nys2xjgkq5hdkz") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.0 (c (n "gmp-mpfr-sys") (v "1.1.0") (h "1hnja1nvwmsd6qlckbki473iwc6r2adwrvxyc7ll1xr8kr95vpzs") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.1 (c (n "gmp-mpfr-sys") (v "1.1.1") (h "02jr04bydkk1zk0scn1y65qxrfiq0fxy8ch93v9bvn5wqgqqd1n0") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.2 (c (n "gmp-mpfr-sys") (v "1.1.2") (h "1p4gnn6b62gay2livdlb06l9sk2i7j9np3vkaygyprl5sphzla0j") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.3 (c (n "gmp-mpfr-sys") (v "1.1.3") (h "1xa6amh1024m4lk1xlgdb9f7nr3p8wmcf453p9pxh3a2164d3g27") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.4 (c (n "gmp-mpfr-sys") (v "1.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hls17dl4n674w08pi0x375p2ymhhl719aafsxkmiwinhhr2ywxr") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.5 (c (n "gmp-mpfr-sys") (v "1.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z3rqyshh49rj9vkqwwx4dn2adb6xjhyls79f17nr2hz9mlycpg5") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1.1.6 (c (n "gmp-mpfr-sys") (v "1.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ajh6i7c45wgsgvsrgfbr7wfmg4l6bm491bk9b6wmlcvl5c8n5fj") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.7 (c (n "gmp-mpfr-sys") (v "1.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q2xhyfxlljr9bqz9zgvqc92k8a5qr8ipw9pr6c9jhcf76xvcza8") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.8 (c (n "gmp-mpfr-sys") (v "1.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10032sh5y41p0cfyqqzsajn74ka4jbb3256hcf1lfrjfzf314xqj") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.9 (c (n "gmp-mpfr-sys") (v "1.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ckapkjcc7gpfpg1ka7hkcv18pbynr68xl1mz8qsrbyc63qk9ha4") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.10 (c (n "gmp-mpfr-sys") (v "1.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gm2nvbgnvp0vf2gkvl77bw0j82z7qzac1zgzb878sxadknsqc3l") (f (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.11 (c (n "gmp-mpfr-sys") (v "1.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13n16zgkj969p85mb8n21kgisqdhpw8psag39d9c8b40bhl1qrhx") (f (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.12 (c (n "gmp-mpfr-sys") (v "1.1.12") (d (list (d (n "dirs") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q0adc8w3x8nvh7wigxk2hbj9la8p3mhg51rkhkf396yvcs752lq") (f (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.13 (c (n "gmp-mpfr-sys") (v "1.1.13") (d (list (d (n "dirs") (r "~1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11x34a37q0qfj58pqai66qwmc0n8f8p0hfwzr8aww9h96710vgc1") (f (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.1.14 (c (n "gmp-mpfr-sys") (v "1.1.14") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ysm543ndl05844wc44yy39214c35v22aj2185ggmviza7bjqnbj") (f (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.2.0 (c (n "gmp-mpfr-sys") (v "1.2.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (k 0)))) (h "0b3hahv63fzabbisar999s5l53zmqkrv7klfip8kanyiym3j7jsa") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.2.1 (c (n "gmp-mpfr-sys") (v "1.2.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.44") (k 0)))) (h "1lriv7qf9a354y7w092iwdw591fq5aq1zs8wsqaik0sx7z18cblk") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.2.2 (c (n "gmp-mpfr-sys") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "16z8iixz4j8crl8qwahnwwy8x2ys9f364gs67ml83w4xrw2zimv3") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.2.3 (c (n "gmp-mpfr-sys") (v "1.2.3") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "06rvnn91dyrj5vn9r5n1dichg1dbbbk2dpmrciqvx80niz3cq11w") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.2.4 (c (n "gmp-mpfr-sys") (v "1.2.4") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0kpbdvs0dkcdj641av759z56as83zmk1klcxiajbfmy3iw1dm8z2") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.3.0 (c (n "gmp-mpfr-sys") (v "1.3.0") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1kgva9ij8lppv40zch9rgwxqbx4j5yykhdmf8lxqsvfa2ya9z94h") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.3.1 (c (n "gmp-mpfr-sys") (v "1.3.1") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "07pcdxdqj2blj930b1v6i3i41wnmr745cwf7rwqc1zb5yy8vc6xc") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.0 (c (n "gmp-mpfr-sys") (v "1.4.0") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "024m2mdh9ahq8hamdvsb6fhkng9639xv3vjdass05fczxj2qwkmd") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.1 (c (n "gmp-mpfr-sys") (v "1.4.1") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1425q4fykrqfbjldhw02s1iax4sryxxfqjkzzasq8hc7y3hcmwsb") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.2 (c (n "gmp-mpfr-sys") (v "1.4.2") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0vpiyx6qizqxlq8mb7ch6dz91bj0s8761vgxn4hk10s9klrxnzx5") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.3 (c (n "gmp-mpfr-sys") (v "1.4.3") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1n0big4im2ndcck7430hlgzxq3clk1lh6aqv0dnw0ai808s83s66") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.4 (c (n "gmp-mpfr-sys") (v "1.4.4") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1k4dny6kl8g0jdsh3ihndhc8p1c3qqbhk59vnp2vay13py5cyjgd") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.5 (c (n "gmp-mpfr-sys") (v "1.4.5") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0r00n7fbc99pnbr9qw1dkm4xdncq1a4mxb6qz175h79bg52h1rqq") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.6 (c (n "gmp-mpfr-sys") (v "1.4.6") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "06likcvshw0cxpgqqqf9azk9c97hhj3w5irn7bvpz1j3ajsrx40w") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.7 (c (n "gmp-mpfr-sys") (v "1.4.7") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1ysvdf352vcnb5ygmbwf5pkndqb0p6clmz0nqkf3nmz9ghssfim1") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.8 (c (n "gmp-mpfr-sys") (v "1.4.8") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0vihd6440ml1zckap9d404jhp6zglv26h5la94l10lb5z470pl5k") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.9 (c (n "gmp-mpfr-sys") (v "1.4.9") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1hfayswhlqpjyvprr4avhv2wq2fk5hqx3bvkvgdxhvk61q8p2xid") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.10 (c (n "gmp-mpfr-sys") (v "1.4.10") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "06cx1ybwihm9frj2x7sccqpjim0ixxbyg1xax4ig2xbcvgd44gza") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp")))

(define-public crate-gmp-mpfr-sys-1.4.11 (c (n "gmp-mpfr-sys") (v "1.4.11") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "026nhirq53w14whnrhf2bacnj2nz5yvw0sgqhh9g8wgkxmq5p401") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.37")))

(define-public crate-gmp-mpfr-sys-1.4.12 (c (n "gmp-mpfr-sys") (v "1.4.12") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0i8hiy712afmf048vbsrdz42y7vm7gk89dckvdrci5g1qshvzb9c") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.37")))

(define-public crate-gmp-mpfr-sys-1.5.0 (c (n "gmp-mpfr-sys") (v "1.5.0") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1xhcphky9fsj1s8a25500sivjndbbbw5pp67hxwhb8v8wpj105vm") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.4.13 (c (n "gmp-mpfr-sys") (v "1.4.13") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1mxm72rmnvc3sgh8s3nii5kxas50za64dmxwqlw7vq768f3ja7bq") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.37")))

(define-public crate-gmp-mpfr-sys-1.5.1 (c (n "gmp-mpfr-sys") (v "1.5.1") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0b364vbmgssb8r7iybx1zafrfivnjf57vjn56fxjbyr65h4nha53") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.5.2 (c (n "gmp-mpfr-sys") (v "1.5.2") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0pxpz1z47rharmdhppmqlpn7vaa455zligzr5sfcxa7zw9ih0mjv") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.5.3 (c (n "gmp-mpfr-sys") (v "1.5.3") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0pd9zrq76hbx3rkma29q4knslz5v3v3ijfay94dn4jkfs4lvrshk") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.6.0 (c (n "gmp-mpfr-sys") (v "1.6.0") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0rl4qrcqab701y78x6jqw8s9zlss0rxlw4br0dz8ha1xnd0hy0c7") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.6.1 (c (n "gmp-mpfr-sys") (v "1.6.1") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "06vj8lli8l9mxxak7icn8kgy9rj817b1ssbf4viqgzi9iiywdi8r") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

(define-public crate-gmp-mpfr-sys-1.6.2 (c (n "gmp-mpfr-sys") (v "1.6.2") (d (list (d (n "libc") (r "^0.2.44") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1qq85s45m5pdyqrhbf9gsdlzra4gxqdbj0wwwkmimx79rg1nqain") (f (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (l "gmp") (r "1.65")))

