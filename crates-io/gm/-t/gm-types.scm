(define-module (crates-io gm -t gm-types) #:use-module (crates-io))

(define-public crate-gm-types-0.1.0 (c (n "gm-types") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1jn2167wwvsrj2qwscrsbiggz0mn0q4gvr72mwybzr470ic4wpy7")))

(define-public crate-gm-types-0.2.0 (c (n "gm-types") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1q311piqdwhn427w58l9fhmgyqx072icn7brafaz4psac1fhbz6j")))

(define-public crate-gm-types-0.3.0 (c (n "gm-types") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01ljlglmb1p1q898xrnl4xlk4ahqy1nzsazccv32548svj9566ay")))

(define-public crate-gm-types-0.4.0-beta1 (c (n "gm-types") (v "0.4.0-beta1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18vd8vj50rl2iv4cvimsxabr2g5sy33jay72d7xvxmqw3six8cbn")))

(define-public crate-gm-types-0.4.0-pre (c (n "gm-types") (v "0.4.0-pre") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05df1fwx588jw2aj1rx6fflp1s5x73m7rjhxdvw746bpd2lnrcrn")))

