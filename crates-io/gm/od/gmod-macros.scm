(define-module (crates-io gm od gmod-macros) #:use-module (crates-io))

(define-public crate-gmod-macros-0.1.0 (c (n "gmod-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v6ndk0hvcflnv963d5hpkh2ghpqhm96mcx9b9z07wk1n4fpm8gd")))

(define-public crate-gmod-macros-1.0.0 (c (n "gmod-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ycjn9cl5lw5lkbr9bia1svyjhd6l76b0fw01qk1d6mhk1cp94dp")))

(define-public crate-gmod-macros-1.0.1 (c (n "gmod-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m3jn1q37hkrdsi07aah0nrk96y0jwysn22qgh62ghn3w5hsyxkg") (y #t)))

(define-public crate-gmod-macros-1.0.2 (c (n "gmod-macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03fb35mmf7sbfxyr69g32vdsk3fz3p62vk53dz8i2pgm9skr4hfw")))

(define-public crate-gmod-macros-1.0.3 (c (n "gmod-macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j71c1cdr8dz9bnj7yh981jk3pycibbfgvgvd143i1g3hjryv64v") (f (quote (("gmcl"))))))

(define-public crate-gmod-macros-1.0.4 (c (n "gmod-macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v18r18da2mq8zlsk0cgzm79350kjyhkx8vha7hy8y461j8xra9s") (f (quote (("gmcl"))))))

(define-public crate-gmod-macros-2.0.0 (c (n "gmod-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qq7gqxf4xf14h7y52qfd2xijyni1sfks0xrdl23b76bdh4dcbfi") (f (quote (("gmcl"))))))

(define-public crate-gmod-macros-2.0.1 (c (n "gmod-macros") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a14j92idp35hcfsfrmqi64il6i1cwd4xb5qxjdwhzdkw9j9wdsg") (f (quote (("gmcl"))))))

