(define-module (crates-io gm od gmod-lzma) #:use-module (crates-io))

(define-public crate-gmod-lzma-0.1.0 (c (n "gmod-lzma") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "03rzbzn5xwhqj7ipkhsw38h8qn977i32hvck31ai3m7gg6an44vr")))

(define-public crate-gmod-lzma-0.1.1 (c (n "gmod-lzma") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "0mhrd3vilb9593dz1f6qa0qw5ifssc956z8phi5jkvag6qnb1342")))

(define-public crate-gmod-lzma-0.2.0 (c (n "gmod-lzma") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "1py01z4nnggw1fwyd4i2r0ifbzv16lah4yxgnfvgz2dil9dgvd0l")))

(define-public crate-gmod-lzma-1.0.0 (c (n "gmod-lzma") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "0195dcg20cbqdcvqv9bgh2p1qgc63smzds3pbwkclrih4ddh33b0") (y #t)))

(define-public crate-gmod-lzma-1.0.1 (c (n "gmod-lzma") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "07x1byiq7jwihzrpcfkv1vv076ybqi9mhlczvam5ypvhpll06n9y")))

