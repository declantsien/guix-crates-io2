(define-module (crates-io gm re gmres) #:use-module (crates-io))

(define-public crate-gmres-0.1.0 (c (n "gmres") (v "0.1.0") (d (list (d (n "rsparse") (r "^0.2.3") (d #t) (k 0)))) (h "01aygrs87b54vz7fjrp6m9pai52b1jx1r2nhj4pczizqq3vfsqnv")))

(define-public crate-gmres-0.1.1 (c (n "gmres") (v "0.1.1") (d (list (d (n "rsparse") (r "^0.2.3") (d #t) (k 0)))) (h "17rvxrsbp6lxcqw9cj8knnl2qc0shdcyiv1s7g6kjrasjpk84dzf")))

(define-public crate-gmres-0.1.2 (c (n "gmres") (v "0.1.2") (d (list (d (n "rsparse") (r "^0.2.3") (d #t) (k 0)))) (h "17df2gs133sfvi9aav0gb7z5qwvwj99bc1l4dmx3m21pd0g22ngj")))

(define-public crate-gmres-1.0.0 (c (n "gmres") (v "1.0.0") (d (list (d (n "rsparse") (r "^1.0.0") (d #t) (k 0)))) (h "1wfqlffm23zry7mzwqlhhzhmal2n8dxsaxz39dfrq80m6kv9ihpz")))

