(define-module (crates-io gm t_ gmt_dos-clients_io) #:use-module (crates-io))

(define-public crate-gmt_dos-clients_io-0.1.0 (c (n "gmt_dos-clients_io") (v "0.1.0") (d (list (d (n "dos-actors") (r "^4.0.0") (d #t) (k 0) (p "gmt_dos-actors")))) (h "08ap4gyrqhrgakcqqpiw7sk3kkf3dz4qqv58zw0lkpghrwgz821v")))

(define-public crate-gmt_dos-clients_io-0.2.0 (c (n "gmt_dos-clients_io") (v "0.2.0") (d (list (d (n "dos-actors") (r "^4.0.0") (d #t) (k 0) (p "gmt_dos-actors")))) (h "1vlm43g7l15qcah7p7dfmhm6g671jm03p57f3cr3a0486lw6xzjq")))

(define-public crate-gmt_dos-clients_io-1.0.0 (c (n "gmt_dos-clients_io") (v "1.0.0") (d (list (d (n "gmt_dos-actors") (r "^5.0.1") (d #t) (k 0)))) (h "1zciibw3i24clvvpsfwqmabm5zv3w3n3smb00wli79zbzrirgwbd")))

(define-public crate-gmt_dos-clients_io-1.1.0 (c (n "gmt_dos-clients_io") (v "1.1.0") (d (list (d (n "gmt_dos-actors") (r "^6.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^1.0.0") (f (quote ("interface"))) (d #t) (k 0)))) (h "0rmv51yzxfpyq8crw7445aj73klmxf5362bs0ynsjba25dywq7mz")))

(define-public crate-gmt_dos-clients_io-1.1.1 (c (n "gmt_dos-clients_io") (v "1.1.1") (d (list (d (n "gmt_dos-actors") (r "^6.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^1.0.0") (f (quote ("interface"))) (d #t) (k 0)))) (h "1lvdpz51380advlrgnmd9gsfjvjf1bgbbyj1ygpii17jcin6y7xb")))

(define-public crate-gmt_dos-clients_io-2.0.0 (c (n "gmt_dos-clients_io") (v "2.0.0") (d (list (d (n "gmt_dos-actors") (r "^7.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.0.0") (f (quote ("interface"))) (k 0)))) (h "0ac2n2zmbw1fv1frv336i0vkx9sj2z0d4hh1skk2qv2r9wa5aad4")))

(define-public crate-gmt_dos-clients_io-2.1.0 (c (n "gmt_dos-clients_io") (v "2.1.0") (d (list (d (n "gmt_dos-actors") (r "^7.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.0.0") (f (quote ("interface"))) (k 0)))) (h "16ni0v382lyh8h8q52d04da396l2gwbz1ccdlll7xhalgggfrhrz")))

(define-public crate-gmt_dos-clients_io-2.2.0 (c (n "gmt_dos-clients_io") (v "2.2.0") (d (list (d (n "gmt_dos-actors") (r "^7.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.1.0") (f (quote ("interface"))) (k 0)))) (h "1hgiyliwvysdfky6x5z51ydj1k3vk31p59xd2lygdc8xh84xbi5q")))

(define-public crate-gmt_dos-clients_io-2.3.0 (c (n "gmt_dos-clients_io") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^0.1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^7.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.1.0") (f (quote ("interface"))) (k 0)))) (h "0q5grspwqsbvafmps4qrb3afgzxfv2ijiizjwcgaldbp5dhj6wsb")))

(define-public crate-gmt_dos-clients_io-2.4.0 (c (n "gmt_dos-clients_io") (v "2.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^0.1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^7.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.1.0") (f (quote ("interface"))) (k 0)))) (h "0x9yi379pvn3ymhmprql7grbnfdnzmd9z4cc5sbricdd342ryhyz")))

(define-public crate-gmt_dos-clients_io-2.5.0 (c (n "gmt_dos-clients_io") (v "2.5.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^7.1.1") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.2.0") (f (quote ("interface"))) (k 0)))) (h "1a23xfghv5gp25r9si7ralrsz0pflcfj8xwj2bgrfnjzgqip6zcv")))

(define-public crate-gmt_dos-clients_io-2.6.0 (c (n "gmt_dos-clients_io") (v "2.6.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^8.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.3.0") (f (quote ("interface"))) (k 0)))) (h "0rw9r5bzn0xgy006xgpn9ip6nqilc359h1sjjl5iwdd9jnxxf5ka")))

(define-public crate-gmt_dos-clients_io-2.6.1 (c (n "gmt_dos-clients_io") (v "2.6.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^8.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.3.0") (f (quote ("interface"))) (k 0)))) (h "1vnbdl41mlc1cmfki8rr33q986w9dj7w76k4jcb769plkcqv7q6h")))

(define-public crate-gmt_dos-clients_io-2.6.2 (c (n "gmt_dos-clients_io") (v "2.6.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^1.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^8.0.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.3.0") (f (quote ("interface"))) (k 0)))) (h "08vsjlygvab53bmljqqdiz7nwg5qxnb4rnpvbzbnwhfk9yq4yq30")))

(define-public crate-gmt_dos-clients_io-3.0.0 (c (n "gmt_dos-clients_io") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^2.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^9.0") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")))) (h "16bvlsy4ccn9f4smzvfx18y3zhm6qx4n4klvgq0flxz41yy49r7d")))

(define-public crate-gmt_dos-clients_io-3.1.0 (c (n "gmt_dos-clients_io") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^2.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^10.1") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")))) (h "0zlxjm008fl931lanzlyydkz4dp87cwaqc5iyn7xi3jzcl4baj6j")))

(define-public crate-gmt_dos-clients_io-3.1.1 (c (n "gmt_dos-clients_io") (v "3.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^2.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^10.1") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")))) (h "18lxmn0vvp8gs14y0sgr41gm905s1xsn6rhkp9k56rvh51alyf15")))

(define-public crate-gmt_dos-clients_io-4.1.0 (c (n "gmt_dos-clients_io") (v "4.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^2.0") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^10.1") (d #t) (k 0)) (d (n "interface") (r "^1.0") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")))) (h "0hbixv6902g591v5bvmkwsk8mz514243bq8acl4zpjnjb9gf1f5a")))

(define-public crate-gmt_dos-clients_io-4.2.0 (c (n "gmt_dos-clients_io") (v "4.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "gmt-fem-code-builder") (r "^2.1") (d #t) (k 1)) (d (n "gmt_dos-actors") (r "^11.0") (d #t) (k 0)) (d (n "interface") (r "^1.3") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")))) (h "1pqw23i72kcrl4rcyb7yma36vck4c8wckznqv7pqi0z95j78wxsm")))

