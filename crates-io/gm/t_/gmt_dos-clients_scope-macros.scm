(define-module (crates-io gm t_ gmt_dos-clients_scope-macros) #:use-module (crates-io))

(define-public crate-gmt_dos-clients_scope-macros-0.1.0 (c (n "gmt_dos-clients_scope-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "11pw8j9yryvfdwlwvccjzncm7lj35hw0dbhs57p8n59kb5avc1mb")))

(define-public crate-gmt_dos-clients_scope-macros-0.2.0 (c (n "gmt_dos-clients_scope-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0qp50fxdry2blw4x4nvjrp6ng0wixs88sj8racwlfzbb0760hnmh")))

(define-public crate-gmt_dos-clients_scope-macros-0.3.0 (c (n "gmt_dos-clients_scope-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1bz49cq770mr193wwzs0z1v2gv5ly94i3d194i7vj4mdzk9xdyjx")))

(define-public crate-gmt_dos-clients_scope-macros-0.4.0 (c (n "gmt_dos-clients_scope-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1sswjnxqq8m5hbrm56nrjs73zs7frw61h5k2gmxzrir06s9qk0s5")))

