(define-module (crates-io gm t_ gmt_mount-ctrl_sampling8000-damping002_ze00) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_sampling8000-damping002_ze00-0.1.0 (c (n "gmt_mount-ctrl_sampling8000-damping002_ze00") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "17qxvyxjwii8q83imcjvj96f90cghd4sy50j0hz5fv78v7g8m786")))

