(define-module (crates-io gm t_ gmt_dos-clients_lom) #:use-module (crates-io))

(define-public crate-gmt_dos-clients_lom-0.1.0 (c (n "gmt_dos-clients_lom") (v "0.1.0") (d (list (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.1.0") (f (quote ("interface"))) (k 0)) (d (n "gmt_dos-clients_io") (r "^2.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "1m3nnl6f1n13fa94q6nly10mq8hvvzw2ppl4an4yb8ja697pcqjy") (y #t)))

(define-public crate-gmt_dos-clients_lom-0.1.1 (c (n "gmt_dos-clients_lom") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.1.0") (f (quote ("interface"))) (k 0)) (d (n "gmt_dos-clients_io") (r "^2.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "07f2yz0qcy40r6w1ci1fghgdbf5virhnc714iy7dy4rjf3rygkva")))

(define-public crate-gmt_dos-clients_lom-0.2.0 (c (n "gmt_dos-clients_lom") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients") (r "^2.2.0") (f (quote ("interface"))) (k 0)) (d (n "gmt_dos-clients_io") (r "^2.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "00l9zji0rf6kspzx0schac7rgcqd7626yw0axix61h5hrp0g8hmj")))

(define-public crate-gmt_dos-clients_lom-1.0.0 (c (n "gmt_dos-clients_lom") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^3.0") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "1azw5b2jaqsdszs9wygiwaib8h1c4n0icyij5sn4krh56n9qp0z9")))

(define-public crate-gmt_dos-clients_lom-1.0.1 (c (n "gmt_dos-clients_lom") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^3.0") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "0hr8waq8fpdry7k97yisnrxa6rab3vlfyk3rnr2d179qyb6kyg95")))

(define-public crate-gmt_dos-clients_lom-1.0.2 (c (n "gmt_dos-clients_lom") (v "1.0.2") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.1.0") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^3.0") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "0lcyccpgapbnzfkj00k67gkdkv2xg8riwdgw4bssp13f4dlamh05")))

(define-public crate-gmt_dos-clients_lom-1.1.0 (c (n "gmt_dos-clients_lom") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.3") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^3.1") (d #t) (k 0)) (d (n "interface") (r "^0.1") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "1zgbh9yhj4b7w4lxnykqpddamy0rd4vp79bvyf5n3bs8h8afnr8v")))

(define-public crate-gmt_dos-clients_lom-1.1.1 (c (n "gmt_dos-clients_lom") (v "1.1.1") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.3") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^4.1") (d #t) (k 0)) (d (n "interface") (r "^1.0") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "1640rq4pmgp3x924asnf9gdaflg4hfcblby6ahij2h8xrcwdi0c7")))

(define-public crate-gmt_dos-clients_lom-1.1.2 (c (n "gmt_dos-clients_lom") (v "1.1.2") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.3") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^4.1") (d #t) (k 0)) (d (n "interface") (r "^1.0") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "0qd81i91gianrglahzb77243jywac02sf8az229y02wx02k92ai3")))

(define-public crate-gmt_dos-clients_lom-1.2.0 (c (n "gmt_dos-clients_lom") (v "1.2.0") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gmt-lom") (r "^2.4") (d #t) (k 0)) (d (n "gmt_dos-clients_io") (r "^4.2") (d #t) (k 0)) (d (n "interface") (r "^1.3") (d #t) (k 0) (p "gmt_dos-actors-clients_interface")) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "1g9gn1kx26wagva0i8krgxqphk7rwxj68ia3h94ikw3f4m9451n1")))

