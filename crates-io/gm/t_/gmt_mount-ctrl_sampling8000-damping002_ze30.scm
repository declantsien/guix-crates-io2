(define-module (crates-io gm t_ gmt_mount-ctrl_sampling8000-damping002_ze30) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_sampling8000-damping002_ze30-0.1.0 (c (n "gmt_mount-ctrl_sampling8000-damping002_ze30") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jqw883lrwr22iihp1rgw0ncgl1fdfylrdg49zzd0b4j0l1n1qlb")))

