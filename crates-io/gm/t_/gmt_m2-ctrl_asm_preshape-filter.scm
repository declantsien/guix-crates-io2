(define-module (crates-io gm t_ gmt_m2-ctrl_asm_preshape-filter) #:use-module (crates-io))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.1.0 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "0z27q1c6whfrxw4wcgai50fls198a0cfz0prkjpz0zdaichyl8ip")))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.1.1 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.1.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "0xpw16f65fvvha1pki9kb8jw1p775zn6rqk7fniymg9fsb1lss87")))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.1.2 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.1.2") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "0cgq862za3fza247lklwzp0z86k039f2h7yl7ykagclvxwwawn53")))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.1.3 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.1.3") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 1)))) (h "1kmgnqig0ll3k5dc2qgbndm32fl3jkpbh3n1l3ad6sa4m5awm9q4")))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.1.4 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.1.4") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "1llsbhcl2wxh54gc61fy2ymm5lwl61bg426wx778l39nmqljfx2r")))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.2.0 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.2.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "148wb2a33g4vg512d78s95jmcdi5cw6y7js6qv2a2ihzkdc88j34") (y #t)))

(define-public crate-gmt_m2-ctrl_asm_preshape-filter-0.2.1 (c (n "gmt_m2-ctrl_asm_preshape-filter") (v "0.2.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "0qdclkd1lr55g1gw9g1d0majvpi2x7r5l7a4lnandmw14w6xs782")))

