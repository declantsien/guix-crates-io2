(define-module (crates-io gm t_ gmt_dos-clients_scope-client) #:use-module (crates-io))

(define-public crate-gmt_dos-clients_scope-client-0.1.0 (c (n "gmt_dos-clients_scope-client") (v "0.1.0") (d (list (d (n "gmt_dos-clients_scope") (r "^2.0") (f (quote ("client"))) (d #t) (k 0)))) (h "1bj77n0hmpr73frharg8wmsff118vpxdi8l1pkhjrl3xmvcj7g4s")))

(define-public crate-gmt_dos-clients_scope-client-0.2.0 (c (n "gmt_dos-clients_scope-client") (v "0.2.0") (d (list (d (n "gmt_dos-clients_scope") (r "^2.3") (f (quote ("client"))) (d #t) (k 0)))) (h "0jpj6qr4x65b318ckhc484h45vmxw7igrdcaj3p7vxgr25b7zmyy")))

(define-public crate-gmt_dos-clients_scope-client-0.1.1 (c (n "gmt_dos-clients_scope-client") (v "0.1.1") (d (list (d (n "gmt_dos-clients_scope") (r "^2.3") (f (quote ("client"))) (d #t) (k 0)))) (h "0vrbqmyjfhwxdfc4zldlrfhq6xr7gczaaf4kfv50r6vgikfvj0gc") (f (quote (("compress" "gmt_dos-clients_scope/compress")))) (y #t)))

(define-public crate-gmt_dos-clients_scope-client-0.2.1 (c (n "gmt_dos-clients_scope-client") (v "0.2.1") (d (list (d (n "gmt_dos-clients_scope") (r "^2.3") (f (quote ("client"))) (d #t) (k 0)))) (h "0s7ak26vlxp9cjlzab4j5x45z8jwya0pys144kbiy1yiwgkhmksv") (f (quote (("compress" "gmt_dos-clients_scope/compress"))))))

(define-public crate-gmt_dos-clients_scope-client-0.2.2 (c (n "gmt_dos-clients_scope-client") (v "0.2.2") (d (list (d (n "gmt_dos-clients_scope") (r "^3.0") (f (quote ("client"))) (d #t) (k 0)))) (h "1mcvppf5qk6ddp9ilyx7iwcgq83s92p5kaqnxz03kfwnpw750q76") (f (quote (("compress" "gmt_dos-clients_scope/compress"))))))

