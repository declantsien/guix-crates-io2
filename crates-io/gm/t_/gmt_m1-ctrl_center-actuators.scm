(define-module (crates-io gm t_ gmt_m1-ctrl_center-actuators) #:use-module (crates-io))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.0 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)))) (h "1jwvksvzs2vz670z2dfbvq9laym9mc879gdrx78wspqw4kpcx512")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.1 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "132y2cad9zc6vv3wafvkvd7niffzwnwd58v581bphsz82vyhajpf")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.2 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "0zy9j9aczpa1iqqbrlpwl78k0frv14a4j7j9q6shq0svfi7ym8hw")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.3 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "1bfgzjfkicsizg44ygmlhhbql90520rvvvig8rihvmq3h4pdih7p")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.4 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.4") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 1)))) (h "07ivhrbkc97g22bgk9r5dczi9x45i7pmh0vq724ymdp1p61wd126")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.1.5 (c (n "gmt_m1-ctrl_center-actuators") (v "0.1.5") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "0nhmm9nhmjar766mw1q4whqay7abkn82qka2k9661ci05bgnlkrp")))

(define-public crate-gmt_m1-ctrl_center-actuators-0.2.0 (c (n "gmt_m1-ctrl_center-actuators") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "0jy0nkbkjx79d1ckcrv40037w0qh83rd6qar60ijz5c9asa0604i") (y #t)))

(define-public crate-gmt_m1-ctrl_center-actuators-0.2.1 (c (n "gmt_m1-ctrl_center-actuators") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "1ki1ywxz061fy1psgywnrh335j9qsbqk8m7wxwslx89vf8p4v896")))

