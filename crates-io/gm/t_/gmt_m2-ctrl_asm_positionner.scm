(define-module (crates-io gm t_ gmt_m2-ctrl_asm_positionner) #:use-module (crates-io))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.1.0 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "09i3zgr7ylh5pprwifra4zqlyqf0sjffrx82xl19sq5mh1937xw2")))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.1.1 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.1.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "19g0jsi99f8a1xn1dh1hmyh16lil8n8795pa7rhv7p1bsfyn9g8b")))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.1.2 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.1.2") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "1hb81l3wqqxf1anj20b083g55v60b507l5p0v23rqwblz7n80asm")))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.1.3 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.1.3") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 1)))) (h "070wd541pdaaiy7z0a30sc4ara72r1j388ljhrh8mz87cziprpvg")))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.1.4 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.1.4") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "1b371ml8q0p9liyh6023jrh99xwb0lgira71hwk8k79jy0zzhwsx")))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.2.0 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.2.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "0mmw8ixahzz8m7cd9pyyk0fwfk4fxphgvz23sc7l0hcxsxi5mdr3") (y #t)))

(define-public crate-gmt_m2-ctrl_asm_positionner-0.2.1 (c (n "gmt_m2-ctrl_asm_positionner") (v "0.2.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "0wcgzd2s2wxvxg7xpj5sjrcn3nnap5c6w92yjxz6pllgxjlc4597")))

