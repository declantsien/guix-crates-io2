(define-module (crates-io gm t_ gmt_mount-ctrl_driver) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_driver-0.0.2 (c (n "gmt_mount-ctrl_driver") (v "0.0.2") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "1q9dywxiwdzas7p3xpgzy9lr8cljf5q0v3fs61vp8qryc5qhpksf")))

(define-public crate-gmt_mount-ctrl_driver-0.1.0 (c (n "gmt_mount-ctrl_driver") (v "0.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "180b8wvsgagqync1lr2h8822jj46j3kqhgk71gvbm9hqr98ibvwg") (y #t)))

(define-public crate-gmt_mount-ctrl_driver-0.1.1 (c (n "gmt_mount-ctrl_driver") (v "0.1.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "0imi244hkng0bmh7dsynys0l1iql12xcysm6jixvprqiqxx4qi9y")))

(define-public crate-gmt_mount-ctrl_driver-1.0.0 (c (n "gmt_mount-ctrl_driver") (v "1.0.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "0smgx2wyb97iqa32mk07rg7n2kqsn6fdbi6vazahbxwkm6vccpwz")))

(define-public crate-gmt_mount-ctrl_driver-1.1.0 (c (n "gmt_mount-ctrl_driver") (v "1.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "1kzgyzy49pypwslm4ldvnyslmdgfg6ca6iwc1fy19ikzwcgv4nfp")))

