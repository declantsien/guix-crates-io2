(define-module (crates-io gm t_ gmt_m1-ctrl_hardpoints-dynamics) #:use-module (crates-io))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.0 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)))) (h "137sf3ldaqliaq7mw32li9smsl932s11nzy3j4kvl4na8558jwlh")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.1 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "11fxyarnms4i3lcf5jnd574993p4jmx161vg1w3hky2fk8awd30f")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.2 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.2") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "080h6yh3bhx1yaqwj13k5ac36118312zsnsj2d4r2a7xlb78vr58")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.3 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.3") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "1x4rp9w4maywz0x01y3b00wqxz14cw1d8pilw5m0xq5hx349m86a")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.4 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.4") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 1)))) (h "1rn9ysjhsd9134900kx9vkrbbwpbd0pd57bqc8z479vidz2faxss")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.1.5 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.1.5") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "1jvdn88bx7naph1afdn5jrz30xdj7sg93ryry38ni7x8f0hci1hi")))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.2.0 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.2.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "1mhdwrxzmhf65s9vll6fsvycp31dan7zj2778j885axl1chrdkfi") (y #t)))

(define-public crate-gmt_m1-ctrl_hardpoints-dynamics-0.2.1 (c (n "gmt_m1-ctrl_hardpoints-dynamics") (v "0.2.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "1j2c1k2y3kaprjdi054wrmyd9k8ch9ywqphk17l4313dvkjay9h5")))

