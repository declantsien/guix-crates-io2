(define-module (crates-io gm t_ gmt_m1-ctrl_outer-actuators) #:use-module (crates-io))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.1.0 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)))) (h "0ksc8gj9qsia1zrxykiwzp110y0dsrimaq6l47hy6ffkzpbyf58q")))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.1.1 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "05w8a0n613q6ka8w61plfxq666jn8xgxx42zkcp1b2ri0igm7p0q")))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.1.2 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "0lzq2mm25vj2r223ii0lbnc9wfl4k83cmqr63h329796vlz2dvcp")))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.1.3 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "1w855mrj8dl2jg3xbanlpkg9g9a05mak4gsdaahnpln2vfy3bwsz")))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.1.5 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.1.5") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "1h68kh6nca9mrwfvfy5fv6bkmw7pzvs4bdfv7xn60jizicbalbz8")))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.2.0 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "1lkpj6pdz8b57igird3gr99jhpwcz4wklqbg9wq8bb3gld9nk27s") (y #t)))

(define-public crate-gmt_m1-ctrl_outer-actuators-0.2.1 (c (n "gmt_m1-ctrl_outer-actuators") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "05ll4fag7rcfv12s1b8vbz27kf21pbk44iapp2qg18ly1ji7xldk")))

