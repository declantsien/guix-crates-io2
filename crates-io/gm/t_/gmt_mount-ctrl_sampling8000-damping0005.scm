(define-module (crates-io gm t_ gmt_mount-ctrl_sampling8000-damping0005) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_sampling8000-damping0005-1.0.0 (c (n "gmt_mount-ctrl_sampling8000-damping0005") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0bw4c52nj3qg3y20i03fph8vw2f5063mdarqk25gvhy50b6054v9")))

