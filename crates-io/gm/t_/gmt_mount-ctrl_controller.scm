(define-module (crates-io gm t_ gmt_mount-ctrl_controller) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_controller-0.1.0 (c (n "gmt_mount-ctrl_controller") (v "0.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "03gm5i3d46lzqrsi14fyjymk8423054h9738a5b3wdqr2jw3sjry")))

(define-public crate-gmt_mount-ctrl_controller-0.2.0 (c (n "gmt_mount-ctrl_controller") (v "0.2.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "1cgvbzicfq7cfamj78xcxifl8wmwjc0phf4vmxvdm4496k654ay6") (y #t)))

(define-public crate-gmt_mount-ctrl_controller-0.2.1 (c (n "gmt_mount-ctrl_controller") (v "0.2.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "1kv0ahjdilr870fivv6wjrjy9qhhf9v6r6078wc5gc5hc5pf642f")))

(define-public crate-gmt_mount-ctrl_controller-1.0.0 (c (n "gmt_mount-ctrl_controller") (v "1.0.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "18nz5y9yaqicmk38gr96i2a4zdvfwl1m9b03x1icasq44skyysxk")))

(define-public crate-gmt_mount-ctrl_controller-1.1.0 (c (n "gmt_mount-ctrl_controller") (v "1.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "0pla6gf6b4vp3gw9dzqzsdn5md1wdm8lnkjzv5hfnwfxgldmkqnf")))

