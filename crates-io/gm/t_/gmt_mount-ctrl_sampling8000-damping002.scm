(define-module (crates-io gm t_ gmt_mount-ctrl_sampling8000-damping002) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_sampling8000-damping002-0.0.1 (c (n "gmt_mount-ctrl_sampling8000-damping002") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "ze00") (r "^0.1.0") (o #t) (d #t) (k 0) (p "gmt_mount-ctrl_sampling8000-damping002_ze00")) (d (n "ze30") (r "^0.1.0") (o #t) (d #t) (k 0) (p "gmt_mount-ctrl_sampling8000-damping002_ze30")) (d (n "ze60") (r "^0.1.0") (o #t) (d #t) (k 0) (p "gmt_mount-ctrl_sampling8000-damping002_ze60")))) (h "1jbg4zvm8g3ravmkncyxr2l3qc5rsvi0lamcj6nav2786pzpqnhm")))

