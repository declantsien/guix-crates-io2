(define-module (crates-io gm t_ gmt_m2-ctrl_asm_pid-damping) #:use-module (crates-io))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.0 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^1.0.0") (d #t) (k 1)))) (h "1rp8b71z9havjqw2492wws8l5z9cxzq58gay1a2z2h48m05bj225")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.1 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^2.0.0") (d #t) (k 1)))) (h "0krmcp1mdaaf38sng4m1d145ic99p9g0g7kbkczbv4nk1yaiihiz")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.2 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.2") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "0g79ab534dcimgswp4ysbvpwnjlaasvw79bz0lk9pqpzvqjsfpf0")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.3 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.3") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.0") (d #t) (k 1)))) (h "1mb1s5b54b17yi3wnis5f1qdiwnzq2y6j70w0kb7wzmc2ps0y37j")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.4 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.4") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.1") (d #t) (k 1)))) (h "1ndkpvk487dn6nn5w0fx2cznrj7fr6vv57cxwgfkwaycrc6m2pdh")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.1.5 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.1.5") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.0.2") (d #t) (k 1)))) (h "02vv64lycc9x3lag1dkhc1qyn63ylk715xkhlky3nh8f61fa2dhj")))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.2.0 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.2.0") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^3.1.0") (d #t) (k 1)))) (h "1vvm27avgx75hl43y15jhrm9s2j9zfx53hap3zpkln9rfhsy51kc") (y #t)))

(define-public crate-gmt_m2-ctrl_asm_pid-damping-0.2.1 (c (n "gmt_m2-ctrl_asm_pid-damping") (v "0.2.1") (d (list (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^4.0.0") (d #t) (k 1)))) (h "1aw8v3v1dpm1bxy13jjfbx5kqhvf9w3g9azxjm6y4slv04kan30y")))

