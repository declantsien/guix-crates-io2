(define-module (crates-io gm t_ gmt_mount-ctrl_sampling1000-damping002_ze30-500hz) #:use-module (crates-io))

(define-public crate-gmt_mount-ctrl_sampling1000-damping002_ze30-500Hz-0.1.0 (c (n "gmt_mount-ctrl_sampling1000-damping002_ze30-500Hz") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10xsbrklmnlg66r30mq8ndgqwih48y8q7jyi6452i2cdmqrzq2ka")))

