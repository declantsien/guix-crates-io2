(define-module (crates-io gm t_ gmt_m1-ctrl_actuators) #:use-module (crates-io))

(define-public crate-gmt_m1-ctrl_actuators-0.1.0 (c (n "gmt_m1-ctrl_actuators") (v "0.1.0") (d (list (d (n "center") (r "^0.1.0") (d #t) (k 0) (p "gmt_m1-ctrl_center-actuators")) (d (n "dos-clients_io") (r "^1.0.0") (d #t) (k 0) (p "gmt_dos-clients_io")) (d (n "gmt_dos-actors") (r "^5.0.1") (k 0)) (d (n "outer") (r "^0.1.0") (d #t) (k 0) (p "gmt_m1-ctrl_outer-actuators")) (d (n "matio-rs") (r "^1.1.2") (d #t) (k 2)))) (h "15y40q49334im692xcrw5pqpajjwink7zy6gwfb757s6ckqki9s7")))

