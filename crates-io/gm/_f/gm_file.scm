(define-module (crates-io gm _f gm_file) #:use-module (crates-io))

(define-public crate-gm_file-0.1.0 (c (n "gm_file") (v "0.1.0") (h "1yn6b7kdlwjm56r032yk6c7a6wbpa7sy9kfvblw32zakglvdyc1n")))

(define-public crate-gm_file-0.1.1 (c (n "gm_file") (v "0.1.1") (h "11r7mgjxakbpcl2bvvr4c536qnyzn3jhr6vrsnnzqwndi0qd1irj")))

