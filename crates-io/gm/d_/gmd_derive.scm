(define-module (crates-io gm d_ gmd_derive) #:use-module (crates-io))

(define-public crate-gmd_derive-0.1.0 (c (n "gmd_derive") (v "0.1.0") (h "0m1r1gaq5x356a5l7cjzw7lfb6a291xiiah739hcx7d43jf1f0f3")))

(define-public crate-gmd_derive-0.1.1 (c (n "gmd_derive") (v "0.1.1") (h "04sxgfchkvw473pr6xk2b68fsxa5nlkpa0xnzgcwcav1av1hgwha")))

