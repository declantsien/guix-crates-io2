(define-module (crates-io gm oc gmocli) #:use-module (crates-io))

(define-public crate-gmocli-1.0.0-alpha.1 (c (n "gmocli") (v "1.0.0-alpha.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "1k93bilzlrg0nly879rk5izqmf87sqzsja40055ij353d49f49n3")))

(define-public crate-gmocli-1.0.0 (c (n "gmocli") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "02a4smiz02j5k01hbss4wybs7hy93afprni5syjvgl631plk703c")))

(define-public crate-gmocli-1.1.0 (c (n "gmocli") (v "1.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "0flj7brzazsis2pskhgg47v3fx6qwshib4m3z72r91nc17jq2bs8")))

