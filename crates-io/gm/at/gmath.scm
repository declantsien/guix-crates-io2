(define-module (crates-io gm at gmath) #:use-module (crates-io))

(define-public crate-gmath-0.1.0 (c (n "gmath") (v "0.1.0") (d (list (d (n "aabb2") (r "^0.2") (d #t) (k 0)) (d (n "aabb3") (r "^0.2") (d #t) (k 0)) (d (n "mat2") (r "^0.2") (d #t) (k 0)) (d (n "mat3") (r "^0.2") (d #t) (k 0)) (d (n "mat32") (r "^0.2") (d #t) (k 0)) (d (n "mat4") (r "^0.2") (d #t) (k 0)) (d (n "quat") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.2") (d #t) (k 0)))) (h "0ip4bqbhxhy5l849344b1mycrk4111dhfycr48k6p8va2ci5n9d9")))

(define-public crate-gmath-0.1.1 (c (n "gmath") (v "0.1.1") (d (list (d (n "aabb2") (r "^0.2") (d #t) (k 0)) (d (n "aabb3") (r "^0.2") (d #t) (k 0)) (d (n "mat2") (r "^0.2") (d #t) (k 0)) (d (n "mat3") (r "^0.2") (d #t) (k 0)) (d (n "mat32") (r "^0.2") (d #t) (k 0)) (d (n "mat4") (r "^0.2") (d #t) (k 0)) (d (n "quat") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)) (d (n "vec3") (r "^0.2") (d #t) (k 0)) (d (n "vec4") (r "^0.2") (d #t) (k 0)))) (h "1kizqcwj6ar33rv18y6p29r1z68q6idbjnhra2qmsdg9x1yw44ab")))

