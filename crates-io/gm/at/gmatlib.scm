(define-module (crates-io gm at gmatlib) #:use-module (crates-io))

(define-public crate-gmatlib-0.0.0 (c (n "gmatlib") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1ck8n4qn6hwi125wirzkqgv20qrbd077lkng9mrbdili1j4y2xf5")))

(define-public crate-gmatlib-0.0.1 (c (n "gmatlib") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "14db167b11cb6awj75rwvzhbx85hrsdkinbcvcbjmji88vva45kq")))

(define-public crate-gmatlib-0.0.3 (c (n "gmatlib") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0qppn8zpydchdpl40n6rpm0yibsw2qjzjrq3y4clq7r9canbxlih")))

(define-public crate-gmatlib-0.1.0 (c (n "gmatlib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "12bclr01fmji068qxmdbrcsraq8my69hr6rh4cvy8ksmdlvw5gk8")))

