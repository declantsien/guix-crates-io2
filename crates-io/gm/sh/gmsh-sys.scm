(define-module (crates-io gm sh gmsh-sys) #:use-module (crates-io))

(define-public crate-gmsh-sys-0.1.0 (c (n "gmsh-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1931nzlclpdxy9k8qs5nyf6mdbd29z04sv73rzi76b8n43iq7c6x") (l "gmsh")))

(define-public crate-gmsh-sys-0.1.1 (c (n "gmsh-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "13mkcq4ckpkz3rm6cs7xr5z3rry7lisjv2zbxy8mkv4x9prp3dvr") (l "gmsh")))

(define-public crate-gmsh-sys-0.1.2 (c (n "gmsh-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0rq7xvmyf4cvfq58wpv2gib0cwx4b1qgadlnny8lvwrm9yivnd26") (l "gmsh")))

