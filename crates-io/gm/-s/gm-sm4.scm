(define-module (crates-io gm -s gm-sm4) #:use-module (crates-io))

(define-public crate-gm-sm4-0.9.0 (c (n "gm-sm4") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1vh60nkxgpi4ncn8bgs4r2nwcw5bgvymrvdwg71hybja3sg23cdh")))

(define-public crate-gm-sm4-0.10.0 (c (n "gm-sm4") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1xljxyh938iqq3vwqxbajjfrf4zazhgizqkhvvbvqajjxy5q2mkv")))

