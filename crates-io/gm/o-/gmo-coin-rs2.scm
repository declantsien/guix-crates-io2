(define-module (crates-io gm o- gmo-coin-rs2) #:use-module (crates-io))

(define-public crate-gmo-coin-rs2-0.2.0 (c (n "gmo-coin-rs2") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1zxpvcyidl3vxxfhnpxkbgmdqmriq9dbs6dqxrppidz61im2y9iv")))

