(define-module (crates-io gm ar gmarkov-lib) #:use-module (crates-io))

(define-public crate-gmarkov-lib-0.1.0 (c (n "gmarkov-lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0cjizcf35ricci86a9d6b1s9hn4xaf4rfiyndw28s4g57qnz121g")))

(define-public crate-gmarkov-lib-0.1.1 (c (n "gmarkov-lib") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1k26jay65dhp4wwfmjrykij251cx618xmhqgrgvqvl1dpqmyhr7k")))

