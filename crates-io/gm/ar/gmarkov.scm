(define-module (crates-io gm ar gmarkov) #:use-module (crates-io))

(define-public crate-gmarkov-0.0.1 (c (n "gmarkov") (v "0.0.1") (h "14q1dmgscw1zpfw4jsfipmbcr5fddrg6v1dx8j70by7a2phm2rvv")))

(define-public crate-gmarkov-1.0.0 (c (n "gmarkov") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gmarkov-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0nxcapp1qpfkxfibv0143sbng177amfhr8s4475yhz1h656gc2yl")))

