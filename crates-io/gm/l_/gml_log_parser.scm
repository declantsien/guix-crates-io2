(define-module (crates-io gm l_ gml_log_parser) #:use-module (crates-io))

(define-public crate-gml_log_parser-0.1.1 (c (n "gml_log_parser") (v "0.1.1") (d (list (d (n "chompy") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "10wjm1fnih3fms36rcqccyp2pc3yq0njy8jf81v6g48gy8dq24rx")))

