(define-module (crates-io gm l_ gml_fmt_lib) #:use-module (crates-io))

(define-public crate-gml_fmt_lib-1.0.1 (c (n "gml_fmt_lib") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "0d42knd0px1j8zjh3354bmpdlrsxwdjl46fqwrhh0dcaqj855jgv")))

