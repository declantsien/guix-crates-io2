(define-module (crates-io gm l_ gml_parser) #:use-module (crates-io))

(define-public crate-gml_parser-0.1.0 (c (n "gml_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s8whm9sfdshbxa6rxgjjgclycsal48i8fjl4yhmrqqnysql418p")))

(define-public crate-gml_parser-0.1.1 (c (n "gml_parser") (v "0.1.1") (d (list (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12jjy75mjh6bv7pnfpkf2af8mqd6sdsvw54ccl9918a8sfqpnsij")))

