(define-module (crates-io gm c_ gmc_bind64) #:use-module (crates-io))

(define-public crate-gmc_bind64-0.1.0 (c (n "gmc_bind64") (v "0.1.0") (d (list (d (n "ink_env") (r "^3.0.0-rc2") (k 0)) (d (n "ink_lang") (r "^3.0.0-rc2") (k 0)) (d (n "ink_metadata") (r "^3.0.0-rc2") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ink_primitives") (r "^3.0.0-rc2") (k 0)) (d (n "ink_storage") (r "^3.0.0-rc2") (k 0)) (d (n "scale") (r "^1.3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^0.4.1") (f (quote ("derive"))) (o #t) (k 0)))) (h "07q1hpgrq19si1d4k8904r546dxrbaj7s5n1k9vk3bl8xdhbwdqd") (f (quote (("std" "ink_metadata/std" "ink_env/std" "ink_storage/std" "ink_primitives/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

