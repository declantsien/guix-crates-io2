(define-module (crates-io gm qt gmqtt) #:use-module (crates-io))

(define-public crate-gmqtt-0.1.0 (c (n "gmqtt") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (k 0)))) (h "062gggwq2yvzm9zw145zx2zynnn44j658ivic2k1a0nzwyks4c6p")))

(define-public crate-gmqtt-0.1.1 (c (n "gmqtt") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (k 0)))) (h "1amwyikn0pbm1sxc7myinrpnsym7has796kff22s8d366ph25vqv") (r "1.56")))

