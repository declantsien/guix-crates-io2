(define-module (crates-io gm sm gmsm) #:use-module (crates-io))

(define-public crate-gmsm-0.0.1 (c (n "gmsm") (v "0.0.1") (h "15021qdm9dm4q8djxci51lng5j5z66iyvmyj5nq3kqpcaljzckg9")))

(define-public crate-gmsm-0.0.2 (c (n "gmsm") (v "0.0.2") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1xhpcs06m2g7zs7n4z68dz9mqxmi3y4lf8j8p4950rfhf5jxr1j5")))

(define-public crate-gmsm-0.0.3 (c (n "gmsm") (v "0.0.3") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "11i28j1scflgxfvicrgwwc2694w2da573w0w24n5gsdjassrjyl3")))

(define-public crate-gmsm-0.0.4 (c (n "gmsm") (v "0.0.4") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1bp70wk5hbyzcs0npqglxqdgda8k6jc6i079rmjkr1pgdhnj5knd")))

(define-public crate-gmsm-0.0.5 (c (n "gmsm") (v "0.0.5") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1bkxbwaq6ysdfl9w1b3h0g7abn7ysz3c38vph71yhah68c2gf15w")))

(define-public crate-gmsm-0.1.0 (c (n "gmsm") (v "0.1.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0y3fb8pvyxiz2py8j44aywhbq0vvqf6a56jcinczrs5l1sh5g70p")))

