(define-module (crates-io gm dp gmdp) #:use-module (crates-io))

(define-public crate-gmdp-0.1.0 (c (n "gmdp") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1s6lx695qk5akz3a87vwcxiaiq5sln062yp8paialrx8aa0s5gfl")))

(define-public crate-gmdp-0.1.1 (c (n "gmdp") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "11a1k357w7kaijm78n7l5vim9x1g7qwl1p0ghkxmngqjsr635ghj")))

(define-public crate-gmdp-0.1.2 (c (n "gmdp") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "1dhcmiyv4bbpj55r15ihns8dg4j2f97wz446lmlwnhndjl7dkrd4")))

(define-public crate-gmdp-0.1.4 (c (n "gmdp") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "10548q4l4k4mvpwnqkn8didqywr0kp9ip4dypv9g6ra23l7kkzrg")))

(define-public crate-gmdp-0.1.5 (c (n "gmdp") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "0z6sby1nfah81f5z1i41iyvgd1zm1k9yffpz4hfzqwarhdhrrsr6")))

(define-public crate-gmdp-0.1.6 (c (n "gmdp") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "01p06vkfvybpkqs7fz4mrddfgqcrq3d1d3d17s1d25nqp7fn1yk7")))

