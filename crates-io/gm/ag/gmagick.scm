(define-module (crates-io gm ag gmagick) #:use-module (crates-io))

(define-public crate-gmagick-0.1.1 (c (n "gmagick") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0ll9ajvmnclr2cqb3kkfkj029wzcb55qnqafqliicjq7id5ckdzl")))

