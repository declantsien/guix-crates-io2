(define-module (crates-io er ad eradicate) #:use-module (crates-io))

(define-public crate-eradicate-0.1.0 (c (n "eradicate") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k5p25lp07yh50v4bbxyvyk2c3k4sl44fa0ha1335ra8blsyhs5x")))

(define-public crate-eradicate-0.1.1 (c (n "eradicate") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "1p6v1kbb2wn0hfi68g247q0fqw4ywhd332jj6w13g7b8b14h7i7x")))

(define-public crate-eradicate-0.1.2 (c (n "eradicate") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "1c40ysds3hbnqpq11nkb34wiwy57nj366wgvpcpagks1n0vppbhj")))

(define-public crate-eradicate-0.1.3 (c (n "eradicate") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "1zxh5cbl9sbl3gpyyg7hv7j6yjlc4z2xn9az5zdkmlc1m1lhfb8c")))

(define-public crate-eradicate-0.1.4 (c (n "eradicate") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "1cp1nlkn9dx1lsmz7wf6dk53ahkbvnif11g42w8s76a0d2ypkgyh")))

