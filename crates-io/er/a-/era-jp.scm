(define-module (crates-io er a- era-jp) #:use-module (crates-io))

(define-public crate-era-jp-0.1.0 (c (n "era-jp") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.0") (d #t) (k 0)))) (h "1qdsybjmpcsqlg34a606gplsqb044k1wah3g263pw4220nmkiryq")))

(define-public crate-era-jp-0.1.1 (c (n "era-jp") (v "0.1.1") (d (list (d (n "chrono") (r "~0.4.0") (d #t) (k 0)))) (h "0b8sj47gvvh94q9irl5h00qky0523m8afsfvbirgxn7yx284j014")))

(define-public crate-era-jp-0.1.2 (c (n "era-jp") (v "0.1.2") (d (list (d (n "chrono") (r "~0.4.0") (d #t) (k 0)))) (h "13liz9zidk0r7rfs81fhpqzxnjvgggy9f1lqlr1dcg2cg0rwyicz")))

(define-public crate-era-jp-0.1.3 (c (n "era-jp") (v "0.1.3") (d (list (d (n "chrono") (r "~0.4.0") (d #t) (k 0)))) (h "11779gn3b4bsy3qp7wzp76dcabm2dcv793avak60l46bj8nzbxpf")))

