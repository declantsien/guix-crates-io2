(define-module (crates-io er r- err-handler) #:use-module (crates-io))

(define-public crate-err-handler-0.1.0 (c (n "err-handler") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)))) (h "08k982g8hi5pnyszrxq68mi4zn5bablr7j7gm9dyl5vjd6m70hai") (y #t)))

(define-public crate-err-handler-0.1.1 (c (n "err-handler") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)))) (h "16fbhm315q8m19r9kbmzyjfwygy83r4nwbwhv9q1cd1v8xi5dp7g")))

(define-public crate-err-handler-1.0.0 (c (n "err-handler") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)))) (h "1bmlhik3582mv1xvkqgqppmhx6jb901m62v2kpd58n382883kswx")))

