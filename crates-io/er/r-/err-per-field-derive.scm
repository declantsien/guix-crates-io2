(define-module (crates-io er r- err-per-field-derive) #:use-module (crates-io))

(define-public crate-err-per-field-derive-0.1.0 (c (n "err-per-field-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b561n3wvc21ys5qxz0cfx6gy9ymlqh5ysj6vnj7pz6psl1hhb4z")))

