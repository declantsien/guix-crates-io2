(define-module (crates-io er r- err-into) #:use-module (crates-io))

(define-public crate-err-into-1.0.0 (c (n "err-into") (v "1.0.0") (h "00d5nv3nr892rhfdwhwyfbjmclx92avjjzb5mjxp5lgfv84ag02w") (r "1.6.0")))

(define-public crate-err-into-1.0.1 (c (n "err-into") (v "1.0.1") (h "0bdhj26vwgajqiafiqr52869y8n8qnl9x10sxf5jjac0g91kn03z") (r "1.6.0")))

