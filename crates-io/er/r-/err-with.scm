(define-module (crates-io er r- err-with) #:use-module (crates-io))

(define-public crate-err-with-0.1.0 (c (n "err-with") (v "0.1.0") (h "1bvx8wqn5nmny4gfqdhs9giv2raaz4fa9mykc6072y6dd4kdnfwf")))

(define-public crate-err-with-0.1.1 (c (n "err-with") (v "0.1.1") (h "1vz7m0b7nbqgsd0v09akn1jlwri444s35w9pq36r4d9gmqzfpqpf")))

