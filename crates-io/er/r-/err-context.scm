(define-module (crates-io er r- err-context) #:use-module (crates-io))

(define-public crate-err-context-0.1.0 (c (n "err-context") (v "0.1.0") (d (list (d (n "failure") (r "~0.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "17n80zklsw187zvlgyv50320bmzg0iagal5zydzr4kinn4iav6j4")))

