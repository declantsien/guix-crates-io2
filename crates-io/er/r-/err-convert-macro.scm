(define-module (crates-io er r- err-convert-macro) #:use-module (crates-io))

(define-public crate-err-convert-macro-0.0.1 (c (n "err-convert-macro") (v "0.0.1") (h "1j88ghk6vbnd5dmjxjr07s57bxd916d6jzn12ax2ilpvvjcw2fgl")))

(define-public crate-err-convert-macro-0.1.0 (c (n "err-convert-macro") (v "0.1.0") (h "0diypawisq7dvmf4axpd7bf1cr42l08ayd0bj4wjxsr7yzqjny5f")))

(define-public crate-err-convert-macro-0.1.1 (c (n "err-convert-macro") (v "0.1.1") (h "0wwhx61xyi9hsxggkkcs8q70ylz0pv9i0k7cfp4r11lrd20ifx1a")))

