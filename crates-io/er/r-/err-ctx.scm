(define-module (crates-io er r- err-ctx) #:use-module (crates-io))

(define-public crate-err-ctx-0.1.0 (c (n "err-ctx") (v "0.1.0") (h "058kpz578rsxqfrv2alnz80yp0qc080737p483h3kvlkd6r5yrgf")))

(define-public crate-err-ctx-0.2.0 (c (n "err-ctx") (v "0.2.0") (h "1n4nnb07vj362y6ibfnn3xj2jyiz7kmfx9xgb4xcrq73649nhc33")))

(define-public crate-err-ctx-0.2.1 (c (n "err-ctx") (v "0.2.1") (h "0il86yb3dfm62d0n8yn98wa16s0qhn146pjnqz9q5xsr0igdk597")))

(define-public crate-err-ctx-0.2.2 (c (n "err-ctx") (v "0.2.2") (h "1ll0x0m051lg259f7yh67n7ycrv48lnfyldkca67j2dl57lcr0dd")))

(define-public crate-err-ctx-0.2.3 (c (n "err-ctx") (v "0.2.3") (h "017na74l1wp784wdz011pf8b0vy5dimximr6yp97nf5d0sfmzblj")))

