(define-module (crates-io er r- err-gen) #:use-module (crates-io))

(define-public crate-err-gen-0.1.0 (c (n "err-gen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0ligb6xs36vqrbzdm93vaad3bif7czppn5y2k1k06fmaqfkf0n3d") (y #t)))

(define-public crate-err-gen-0.1.1 (c (n "err-gen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0md1by3jxsz6ji9xkm4wm4sf2068b71rh4xlgkaa0i9i7s1sr5rs") (y #t)))

(define-public crate-err-gen-0.1.2 (c (n "err-gen") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0clsrd7nbxwm2jxg740mp4rjv0f7k44bd8b4i4i8hcwnsq519zp0") (y #t)))

(define-public crate-err-gen-0.1.3 (c (n "err-gen") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "02r1278k0874ngpm565gzn3h6vzzfdl85mqv7ixg3c9fy5bfnkry")))

(define-public crate-err-gen-0.1.4 (c (n "err-gen") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1g8692vibwml3a20k4zpyhjkc1ma5vfk0lryww1yrv97kmvy279i")))

