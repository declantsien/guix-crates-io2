(define-module (crates-io er in erin) #:use-module (crates-io))

(define-public crate-erin-0.1.0 (c (n "erin") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "popol") (r "^2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1379c3z8hprsxw0qkw73n663mxb124s6yfli5xz1ycridca998g2")))

