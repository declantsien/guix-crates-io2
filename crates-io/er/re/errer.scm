(define-module (crates-io er re errer) #:use-module (crates-io))

(define-public crate-errer-0.1.0 (c (n "errer") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1f50dd482n9c9q4rnv71q2swh23dn7a1q4q7d5azcm3p521vyn8h")))

(define-public crate-errer-0.1.1 (c (n "errer") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0z8w2dcyl8x01nj0sx3ly1nbl82py12swdx1yy1m2p9iya4h10sv")))

(define-public crate-errer-0.12.1 (c (n "errer") (v "0.12.1") (d (list (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0h8lqi6gbh4rdjhpb9cbl26745d60vr3bg8h3ihw9l0z0zpli445")))

(define-public crate-errer-0.13.0 (c (n "errer") (v "0.13.0") (d (list (d (n "termcolor") (r "^1.0.5") (o #t) (d #t) (k 0)))) (h "1haizd2kp5abqdfimxf9flc88qaf8mvx7ghldb25xvhwdllg2c4v") (f (quote (("print-error" "termcolor"))))))

