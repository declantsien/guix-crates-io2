(define-module (crates-io er re erreur) #:use-module (crates-io))

(define-public crate-erreur-0.1.0 (c (n "erreur") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1agmyf2dxgqnj77biypbfvjyq8629vz7jrwssfs4jlpvlw8nzyrd")))

(define-public crate-erreur-0.1.1 (c (n "erreur") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0iw93ccndig6f3aj6b157afsls8b28k9zpp20iwx72cs8mbzfngv")))

(define-public crate-erreur-0.1.2 (c (n "erreur") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1j1abzvlca13rphyxz4mqlrw8w0zilg5k75k7k3g65b68wvc6a5p")))

