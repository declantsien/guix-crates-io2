(define-module (crates-io er re erreport) #:use-module (crates-io))

(define-public crate-erreport-0.1.0 (c (n "erreport") (v "0.1.0") (h "1ymsmg37ffizb87li9hii86kdp3srdr3v1yz6rg8sa74zv7ip1mn")))

(define-public crate-erreport-0.1.1 (c (n "erreport") (v "0.1.1") (h "056bil62zwdlwd7rizmy1db27dq2g8ijdlxl4kmfqr89hlh68nx3")))

(define-public crate-erreport-0.2.1 (c (n "erreport") (v "0.2.1") (h "1gb1028jfn453z7vny94z5z17j3ypi79djmy2lc5hlpbqihm0krs")))

(define-public crate-erreport-0.3.0 (c (n "erreport") (v "0.3.0") (h "1xlxbqyqgbds77cf4ccxdfpxw48imzxw9706mcmkcvssa4p62b4i")))

