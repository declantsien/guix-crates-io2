(define-module (crates-io er re errer_derive) #:use-module (crates-io))

(define-public crate-errer_derive-0.1.0 (c (n "errer_derive") (v "0.1.0") (d (list (d (n "errer") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1rsa4l3n5hh27f5jz435f65k7si0d3zcymgqdl8v940pykfz2n6m")))

(define-public crate-errer_derive-0.1.1 (c (n "errer_derive") (v "0.1.1") (d (list (d (n "errer") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1sqpafhqr4dz6gagvl00adm5a4nh2v0v7j9xbwpg7zx1mbvdlpz8")))

(define-public crate-errer_derive-0.12.1 (c (n "errer_derive") (v "0.12.1") (d (list (d (n "errer") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1i52snsy00fa3rzh2smcsv7jb287li7jysh8iwgx0qzcvv90nadr")))

(define-public crate-errer_derive-0.12.2 (c (n "errer_derive") (v "0.12.2") (d (list (d (n "errer") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0izcn5mvzliwm2nxmy9bf2is1fdxw36qiw3iy585bp10sdg4kfn8")))

(define-public crate-errer_derive-0.13.0 (c (n "errer_derive") (v "0.13.0") (d (list (d (n "errer") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04hy0rrgl9pxpcyzr26syhrhflp6xw97ccv3bwgdp0kli3j4jc2h")))

(define-public crate-errer_derive-0.13.1 (c (n "errer_derive") (v "0.13.1") (d (list (d (n "errer") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1kw0glg9qrn95lhxzhg7b92749f6mx98qxn5sfb3kfnjc0d7dvsv")))

