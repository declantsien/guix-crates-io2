(define-module (crates-io er ns ernst) #:use-module (crates-io))

(define-public crate-ernst-0.1.0 (c (n "ernst") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.5.1") (d #t) (k 0)) (d (n "ftree") (r "^1.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03gg90jms7g1ga4pq1i0czckz2k7pbrz8yvy9lmyz6sva1gxcz0c")))

