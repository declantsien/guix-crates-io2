(define-module (crates-io er sp erspan) #:use-module (crates-io))

(define-public crate-erspan-0.1.0 (c (n "erspan") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1amy5m5nra2x38xlq6wjlw9safqgp3lbmg8ikq4irsfp25vlvh4k")))

(define-public crate-erspan-0.1.1 (c (n "erspan") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0har4f4k8q9fbw4b0pam0ck0x11d4m1c8c4klnf4jjcvw38py52h")))

(define-public crate-erspan-0.1.2 (c (n "erspan") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1w3q7gh3g89k7h7ybpxjlcim2aydxds2c3xz2mh7yb17abk3x8cm")))

(define-public crate-erspan-0.2.0 (c (n "erspan") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hyd85lmg8kicd3hwplmmqbnfv9v6hwic0d8b0jmk4mfbawxydfm")))

(define-public crate-erspan-0.2.1 (c (n "erspan") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1v287l3a796242jawam5hgi8qm52lzyiggyx9i87wkdpv4d15krg")))

(define-public crate-erspan-0.2.2 (c (n "erspan") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "pcap-file") (r "^2.0.0") (d #t) (k 2)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13vm3xpjrwi4rh3s5dcszzy2fsk2x3wl8fbx1jq9rwmapacv389b")))

