(define-module (crates-io er g_ erg_proc_macros) #:use-module (crates-io))

(define-public crate-erg_proc_macros-0.6.21-nightly.2 (c (n "erg_proc_macros") (v "0.6.21-nightly.2") (d (list (d (n "erg_common") (r "^0.6.21-nightly.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16bicw80cs2knzc2ms1gpb04ligyb6cxiysasa5di5la1v2nkznd")))

(define-public crate-erg_proc_macros-0.6.21-nightly.4 (c (n "erg_proc_macros") (v "0.6.21-nightly.4") (d (list (d (n "erg_common") (r "^0.6.21-nightly.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1378rwysizxgxcssiyk2j5lra26mmxdk7arw6n0sik1nrv2vr79h")))

(define-public crate-erg_proc_macros-0.6.21 (c (n "erg_proc_macros") (v "0.6.21") (d (list (d (n "erg_common") (r "^0.6.21") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8zxvdbs96i7n2rhyipf48n6jjwk0cliar5a4wy0hl0l4yv80h9")))

(define-public crate-erg_proc_macros-0.6.22-nightly.0 (c (n "erg_proc_macros") (v "0.6.22-nightly.0") (d (list (d (n "erg_common") (r "^0.6.22-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kbpgqjxlc3kagbfvwdbjp5sl3fla2r14rgk8k0djngqghz2fl1z")))

(define-public crate-erg_proc_macros-0.6.22-nightly.1 (c (n "erg_proc_macros") (v "0.6.22-nightly.1") (d (list (d (n "erg_common") (r "^0.6.22-nightly.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14yy2x2qxfb5nrvr9cryj6kfrz8cr1mb93disj7w934x4bpm0g27")))

(define-public crate-erg_proc_macros-0.6.22 (c (n "erg_proc_macros") (v "0.6.22") (d (list (d (n "erg_common") (r "^0.6.22") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08grgqncxn9q28kjn5611nxq90y7nscdfhckgxclicvvyibi67gz")))

(define-public crate-erg_proc_macros-0.6.23 (c (n "erg_proc_macros") (v "0.6.23") (d (list (d (n "erg_common") (r "^0.6.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ky011wcp766pzrldljfdln72fk27wagf85ji97rf6s5b7jwgwi8")))

(define-public crate-erg_proc_macros-0.6.24 (c (n "erg_proc_macros") (v "0.6.24") (d (list (d (n "erg_common") (r "^0.6.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00829h7g18bkzww5lza151amamzyydlc2cmx46s3g95w5vi8kh46")))

(define-public crate-erg_proc_macros-0.6.25-nightly.0 (c (n "erg_proc_macros") (v "0.6.25-nightly.0") (d (list (d (n "erg_common") (r "^0.6.25-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i36awg132360pglpynanb69fiyvm00qdpv3ywx7qxxf1f8jss49")))

(define-public crate-erg_proc_macros-0.6.25-nightly.1 (c (n "erg_proc_macros") (v "0.6.25-nightly.1") (d (list (d (n "erg_common") (r "^0.6.25-nightly.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07myg6l9yc4w7gld40bc91lclcrvl44fw3cl7wdlp9ixlvvkhjqk")))

(define-public crate-erg_proc_macros-0.6.25 (c (n "erg_proc_macros") (v "0.6.25") (d (list (d (n "erg_common") (r "^0.6.25") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04j90rgnvwrg74g5c4mkk7fqxl9igpgyi6nvcyvlbiwnjbxlfj4k")))

(define-public crate-erg_proc_macros-0.6.26-nightly.0 (c (n "erg_proc_macros") (v "0.6.26-nightly.0") (d (list (d (n "erg_common") (r "^0.6.26-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "133nnbcd6n00i8hxmifd9zwg5fhfm1x55865lqmxrrqz01z2fv63")))

(define-public crate-erg_proc_macros-0.6.26 (c (n "erg_proc_macros") (v "0.6.26") (d (list (d (n "erg_common") (r "^0.6.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1va1p3040fbrc1hji54yxqd1mwar79s305f4jxg4cgrdrw5cbzgy")))

(define-public crate-erg_proc_macros-0.6.27-nightly.0 (c (n "erg_proc_macros") (v "0.6.27-nightly.0") (d (list (d (n "erg_common") (r "^0.6.27-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aljccgqgf0iam4c63392364ng1yqhc2x090vz6nxc0577psw162")))

(define-public crate-erg_proc_macros-0.6.27-nightly.1 (c (n "erg_proc_macros") (v "0.6.27-nightly.1") (d (list (d (n "erg_common") (r "^0.6.27-nightly.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gq79zy12jhk57dkw2084qn56ymx7b9m9xw8pbzy99ggba5fkjdl")))

(define-public crate-erg_proc_macros-0.6.27 (c (n "erg_proc_macros") (v "0.6.27") (d (list (d (n "erg_common") (r "^0.6.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w6g0f1k89knrrbijz6kxdqmc9ckrmwd5gvadvf8f8k0f0ispn8m")))

(define-public crate-erg_proc_macros-0.6.28-nightly.0 (c (n "erg_proc_macros") (v "0.6.28-nightly.0") (d (list (d (n "erg_common") (r "^0.6.28-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13vvph6iw4idc0qd43vd4v6f1nfq5m3zvnby7ca9a7g8vcjv8pka")))

(define-public crate-erg_proc_macros-0.6.28 (c (n "erg_proc_macros") (v "0.6.28") (d (list (d (n "erg_common") (r "^0.6.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qgirdshz138wjkkv6j9wgz60jydd67skyxws7qia4aq96ryv2wc")))

(define-public crate-erg_proc_macros-0.6.29 (c (n "erg_proc_macros") (v "0.6.29") (d (list (d (n "erg_common") (r "^0.6.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09fidpk1b7hrnjl7ckp04p3wd3aw35c0la3a6j6gd3vmd35v7amd")))

(define-public crate-erg_proc_macros-0.6.30-nightly.0 (c (n "erg_proc_macros") (v "0.6.30-nightly.0") (d (list (d (n "erg_common") (r "^0.6.30-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03sq3xmxr1dwnqkkp5fn7q5y0f8i9mg2dvzn8mqzsb5vvfkh9gig")))

(define-public crate-erg_proc_macros-0.6.30 (c (n "erg_proc_macros") (v "0.6.30") (d (list (d (n "erg_common") (r "^0.6.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y11k1skx5654qs4x36mcyybgh95wn49spdwpdbsch8xqldahxlp")))

(define-public crate-erg_proc_macros-0.6.31-nightly.0 (c (n "erg_proc_macros") (v "0.6.31-nightly.0") (d (list (d (n "erg_common") (r "^0.6.31-nightly.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yz4jlj3gbjhmpm8rxf98lk8g8i972whrcglfim42yf3rsv4hfp5")))

(define-public crate-erg_proc_macros-0.6.31 (c (n "erg_proc_macros") (v "0.6.31") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvl95yzcizxjqyyxnqasj5agi6a3ndy94a6lzrkwa2agr1mz657")))

(define-public crate-erg_proc_macros-0.6.32-nightly.0 (c (n "erg_proc_macros") (v "0.6.32-nightly.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlfdghkc93dqa0mgg43nh2xn6fnvg57mznqh4d480fm8qa11hxy")))

(define-public crate-erg_proc_macros-0.6.32-nightly.1 (c (n "erg_proc_macros") (v "0.6.32-nightly.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01rwx58dpflzjwiq821gvqrg5ndannawskc67qsh2bl5zxsh4w1d")))

(define-public crate-erg_proc_macros-0.6.32-nightly.2 (c (n "erg_proc_macros") (v "0.6.32-nightly.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hsd070chgfip02zs1qc29aw2b84wrm7g62jjr29yzgdh8vk3ga7")))

(define-public crate-erg_proc_macros-0.6.32-nightly.3 (c (n "erg_proc_macros") (v "0.6.32-nightly.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqvpigvhlyxnpvamxjgla77bm61qpmf8gvm14bqj08vmjdyb674")))

(define-public crate-erg_proc_macros-0.6.32 (c (n "erg_proc_macros") (v "0.6.32") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bm3sw8cg5l9g73ik1l2n3rd5igj247anyzgqyzixjbckcjykld4")))

(define-public crate-erg_proc_macros-0.6.33-nightly.0 (c (n "erg_proc_macros") (v "0.6.33-nightly.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wsffdxb03rnlirlv279md8dbd9j35cwvl85fyvcgv4zahn0g50f")))

(define-public crate-erg_proc_macros-0.6.33-nightly.1 (c (n "erg_proc_macros") (v "0.6.33-nightly.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1di420wmxkza4yjsh4pg473gw7g4smzmyinn8lim9g64dzlvczbj")))

(define-public crate-erg_proc_macros-0.6.33-nightly.2 (c (n "erg_proc_macros") (v "0.6.33-nightly.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13d4pgdzvdr5768vhzgjki0s46sa0pbw89irali6s7yxssra22ld")))

(define-public crate-erg_proc_macros-0.6.33 (c (n "erg_proc_macros") (v "0.6.33") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ciqmajhhjzz0dvf8rls2cn84h53b2rgv86snbfs1skgj3r88ghz")))

(define-public crate-erg_proc_macros-0.6.34 (c (n "erg_proc_macros") (v "0.6.34") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lncq9ki63zqzq156j5dd991vq4f61w6ybfyyv4qx1gzk8mc6yyk")))

(define-public crate-erg_proc_macros-0.6.35-nightly.0 (c (n "erg_proc_macros") (v "0.6.35-nightly.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18qklcq7vbifszqrzca9jx18jsxbfvm8bjnlm668h1yy7qz4gia5")))

(define-public crate-erg_proc_macros-0.6.35 (c (n "erg_proc_macros") (v "0.6.35") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cv96a2nqq7mx6rxj8sk3dml60bys0avlmycr63yazsxpmyv2kvf")))

(define-public crate-erg_proc_macros-0.6.36-nightly.0 (c (n "erg_proc_macros") (v "0.6.36-nightly.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sfpzq9psrmfsr6ng59mcqqpbvn9s2xv1hbnc3dlxwbnbns3zxs0")))

(define-public crate-erg_proc_macros-0.6.36-nightly.1 (c (n "erg_proc_macros") (v "0.6.36-nightly.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12sh35mpbgqj9nhsyli0vmqjzgw8m76lg1s6fnl0gyaqgwyrqzi7")))

(define-public crate-erg_proc_macros-0.6.36-nightly.2 (c (n "erg_proc_macros") (v "0.6.36-nightly.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xsvwnlzv269vbflyvdwcmw6qzvrf4m3nbmsmnrcd15i3gx5g60i")))

(define-public crate-erg_proc_macros-0.6.36 (c (n "erg_proc_macros") (v "0.6.36") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x09ry8w6qvm1p09y5x9x4wxhk2ndfm581lzpdiscgr0svnrbpnm")))

(define-public crate-erg_proc_macros-0.6.37 (c (n "erg_proc_macros") (v "0.6.37") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0962bykn4y17x0bs8by3alq1i9d2s782kj9i04dm6rvbp47mb39l")))

