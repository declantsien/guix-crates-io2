(define-module (crates-io er g_ erg_bits) #:use-module (crates-io))

(define-public crate-erg_bits-0.1.0 (c (n "erg_bits") (v "0.1.0") (h "120ahi1zggclnagh1vsg0r89z8x5nw473h5y9f1ramxp25a3ld8h") (y #t)))

(define-public crate-erg_bits-0.1.1 (c (n "erg_bits") (v "0.1.1") (h "191nk9bvxzjshlhi2xwzxq2pfxi0pqf9m6ffk9k4fj3ak3fm4418") (y #t)))

