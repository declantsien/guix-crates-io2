(define-module (crates-io er fa erfa-sys) #:use-module (crates-io))

(define-public crate-erfa-sys-0.1.0 (c (n "erfa-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "pkg-config") (r "0.3.*") (d #t) (k 1)))) (h "06kyknqs3qlzzn803kk6x5r5gyq7a74lc7g240336s0ndqh1c0lz") (f (quote (("static" "autotools")))) (y #t) (l "erfa")))

(define-public crate-erfa-sys-0.1.1 (c (n "erfa-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "pkg-config") (r "0.3.*") (d #t) (k 1)))) (h "1gf67zjs9fqg2qd0r45qzvqwl19ndlnc6jcz1zdx6y9v83gdwpxk") (f (quote (("static" "autotools")))) (l "erfa")))

(define-public crate-erfa-sys-0.1.2 (c (n "erfa-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "pkg-config") (r "0.3.*") (d #t) (k 1)))) (h "01mnyds9cmdc3xm2mny353sx043acsr66c3jbk1k58j15xaw8cqj") (f (quote (("static" "autotools")))) (l "erfa")))

(define-public crate-erfa-sys-0.2.0 (c (n "erfa-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2.5") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0yki0vyclz7yypqcrnh0mkb9yi66r0w8rfszwhffvzlp87wq5r78") (f (quote (("static" "autotools")))) (l "erfa")))

(define-public crate-erfa-sys-0.2.1 (c (n "erfa-sys") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2.5") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "12akx25l3i7s13098xbhigp6z1y62m1rkdg58jwdj3jjh2pd6a1k") (f (quote (("static" "autotools")))) (l "erfa")))

