(define-module (crates-io er fa erfa) #:use-module (crates-io))

(define-public crate-erfa-0.2.1 (c (n "erfa") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1r8mb5aswvjr4hfq73gq2ya98q89vgsld44zp09ddmvvz3g80f7n")))

