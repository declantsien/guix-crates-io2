(define-module (crates-io er up erupt-bootstrap) #:use-module (crates-io))

(define-public crate-erupt-bootstrap-0.0.0 (c (n "erupt-bootstrap") (v "0.0.0") (h "1lvyxjw65q881m3z88m53qgvn4df0g623hnlax0r99a5j0fw3y8y") (y #t)))

(define-public crate-erupt-bootstrap-0.1.0 (c (n "erupt-bootstrap") (v "0.1.0") (d (list (d (n "erupt") (r "^0.22") (k 0)) (d (n "erupt") (r "^0.22") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "02ssq0i43aarxdk6wbnw9x7iss96cfsznh8c6jp7n16wxkcfk68c") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

(define-public crate-erupt-bootstrap-0.2.0 (c (n "erupt-bootstrap") (v "0.2.0") (d (list (d (n "erupt") (r "^0.22") (k 0)) (d (n "erupt") (r "^0.22") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "1h2y3xc0kc8cni9vc7bz7mb5qngbpxd6l0djwmqpbkw1rsa9713v") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

(define-public crate-erupt-bootstrap-0.2.1 (c (n "erupt-bootstrap") (v "0.2.1") (d (list (d (n "erupt") (r "^0.22") (k 0)) (d (n "erupt") (r "^0.22") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "10pmhymr4lbkwsrm17ygl922mwp8gzpwf7qc5hhyl232af70vbvq") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

(define-public crate-erupt-bootstrap-0.2.2 (c (n "erupt-bootstrap") (v "0.2.2") (d (list (d (n "erupt") (r "^0.22") (k 0)) (d (n "erupt") (r "^0.22") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "13vn408nhzrdk4zpmp438p2fsv2wyfq2p8l1lkiz14msj9mw4qqq") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

(define-public crate-erupt-bootstrap-0.2.3 (c (n "erupt-bootstrap") (v "0.2.3") (d (list (d (n "erupt") (r "^0.22") (k 0)) (d (n "erupt") (r "^0.22") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "03prrxs3n3g8zr9p8qagzspw42v26dkj2drc7dcm3njw44q2vijs") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

(define-public crate-erupt-bootstrap-0.3.0 (c (n "erupt-bootstrap") (v "0.3.0") (d (list (d (n "erupt") (r "^0.23") (k 0)) (d (n "erupt") (r "^0.23") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 2)))) (h "1xb1kh4md0hq6y3sibbswmplszk9j6gwh51kg7nqcs77rv3mza1z") (f (quote (("surface" "raw-window-handle" "erupt/surface") ("default" "surface"))))))

