(define-module (crates-io er ra erratum) #:use-module (crates-io))

(define-public crate-erratum-0.1.0 (c (n "erratum") (v "0.1.0") (h "19rj6i1qgbaw7kdh7kmhjcl479c22d6q0w5p49jq7pkc0swpqfcd") (y #t)))

(define-public crate-erratum-0.0.0 (c (n "erratum") (v "0.0.0") (h "1p2km5xgwm7dnjnpwwxi1b4xigp5c0vp3zdwkv56qmgckz8g8ng3")))

