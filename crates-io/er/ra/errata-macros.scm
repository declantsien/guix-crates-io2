(define-module (crates-io er ra errata-macros) #:use-module (crates-io))

(define-public crate-errata-macros-0.1.0 (c (n "errata-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2vyg2111sagccxa6q0rb35ck07q20na82bz7p9ppy28hnygf4d")))

(define-public crate-errata-macros-0.2.0 (c (n "errata-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0mrksz5blm7zj1s31lrm73qg74crr0jxvxlv8cyk0jfw38r1wsw4")))

