(define-module (crates-io er ra errata) #:use-module (crates-io))

(define-public crate-errata-1.1.0 (c (n "errata") (v "1.1.0") (d (list (d (n "errata-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0skg0c49an6civxjdcj431nwl70qf41vyydqp1h1l1dvchw266p6")))

(define-public crate-errata-1.1.1 (c (n "errata") (v "1.1.1") (d (list (d (n "errata-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0azsz5cgxd331gi13i6vc7byiizih12q3w3wzs96xhki2chxhpbl")))

(define-public crate-errata-2.0.0 (c (n "errata") (v "2.0.0") (d (list (d (n "errata-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1gzrks4czjvca5aqd4cxfzjdkpfcchqyjchpqggx9y5wl8blm91v") (f (quote (("default") ("color"))))))

(define-public crate-errata-2.1.0 (c (n "errata") (v "2.1.0") (d (list (d (n "errata-macros") (r "^0") (d #t) (k 0)))) (h "07qz93mzac5w3gpqvbx6hgcc2hri23rkhfmvfn8lmsnm3knlil7c") (f (quote (("default") ("color"))))))

(define-public crate-errata-2.1.1 (c (n "errata") (v "2.1.1") (d (list (d (n "errata-macros") (r "^0") (d #t) (k 0)))) (h "07d89xc0c1c5xacds223v6nmhfwsm5y5iw0fld4izqasiykibgfh") (f (quote (("default") ("color"))))))

