(define-module (crates-io er fs erfs-rt) #:use-module (crates-io))

(define-public crate-erfs-rt-0.1.0 (c (n "erfs-rt") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0zn7nls0g3c3z5ljw2i10ivnm2qx2wq1n3xnqcrp1w6krqbsbymw") (y #t)))

(define-public crate-erfs-rt-0.1.1 (c (n "erfs-rt") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)))) (h "08zl9kvjapa00wmjjlc7248ls4qd32bxbf63rz17586560g06sw3")))

(define-public crate-erfs-rt-0.1.2 (c (n "erfs-rt") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)))) (h "09b9c5pxbbppjqgm1bjbfwgpaarz8gb4m2isfvfsglz0gb2ccw99")))

