(define-module (crates-io er fs erfs-gen) #:use-module (crates-io))

(define-public crate-erfs-gen-0.1.0 (c (n "erfs-gen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "deflate") (r "^0.8.2") (d #t) (k 0)))) (h "1xy7dkjab6v2zlqgl4714r83xkg29kkw6gzl3z86kqwk8qj9zayh") (y #t)))

(define-public crate-erfs-gen-0.1.1 (c (n "erfs-gen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "deflate") (r "^0.8.2") (d #t) (k 0)))) (h "1waps62gmd5l09g05qwhi8dv712w0jgpmlkdfwzbfikygnj76mcg")))

(define-public crate-erfs-gen-0.1.2 (c (n "erfs-gen") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "deflate") (r "^0.8.2") (d #t) (k 0)))) (h "0jgrljya60p8f45f1im1vc20v37yhps1p33y1bdczjg8vflpgj1s")))

