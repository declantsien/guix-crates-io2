(define-module (crates-io er dp erdp) #:use-module (crates-io))

(define-public crate-erdp-0.1.0 (c (n "erdp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)))) (h "0fr0wvkcv7l872xlgiba4dc6kj4ciyx8mc915y8fccmxwrvadsm5")))

(define-public crate-erdp-0.1.1 (c (n "erdp") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 2)))) (h "073c1zqiv15l2hklwbph4pmcq7bk87bfjwxpypp0q36z0ks5l8ra")))

