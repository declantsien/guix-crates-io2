(define-module (crates-io er id eridani) #:use-module (crates-io))

(define-public crate-eridani-0.0.0 (c (n "eridani") (v "0.0.0") (h "1ldqb9ccrfsyysssi4hcrdhvvbhh9wv3f12vp0bjy8857zi4sw3z") (y #t)))

(define-public crate-eridani-0.1.0 (c (n "eridani") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6") (k 0)) (d (n "generic-array") (r "^0.14.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (k 0)))) (h "14xrvnqpqdswd673n0n3bdq9a6wc4s2vqz26qqihq5ny3lcr461x") (f (quote (("web" "runtime") ("target_web" "compiler") ("target_std" "compiler") ("std") ("runtime") ("ffi") ("error_trait") ("default" "compiler" "runtime" "std" "target_std") ("compiler")))) (s 2) (e (quote (("serialise" "dep:serde"))))))

