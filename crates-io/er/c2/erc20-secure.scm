(define-module (crates-io er c2 erc20-secure) #:use-module (crates-io))

(define-public crate-erc20-secure-0.1.0 (c (n "erc20-secure") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-erc20-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "casperlabs-ownable") (r "^0.1.0") (d #t) (k 0)))) (h "1ayb8pdqb12dvwv1zqyyaaxwpma4dbsxj5niljfwv4chdxndvzz6")))

(define-public crate-erc20-secure-0.1.1 (c (n "erc20-secure") (v "0.1.1") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-erc20-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "casperlabs-ownable") (r "^0.2.0") (d #t) (k 0)))) (h "1nkiaz7vf0z5vrsqgdyyamfv3s20wmz5ipm960lhvigxqxx5rsla")))

