(define-module (crates-io er c2 erc20-crate) #:use-module (crates-io))

(define-public crate-erc20-crate-0.2.0 (c (n "erc20-crate") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.3") (d #t) (k 0)) (d (n "casper-types") (r "^1.4.4") (d #t) (k 0)) (d (n "contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "renvm-sig") (r "^0.1.1") (d #t) (k 0)))) (h "1jffivzc5p7vzhm2bzbpwb02b8x08qd13yibxldbhf0brl050cxl") (f (quote (("default" "casper-contract/std" "casper-types/std"))))))

