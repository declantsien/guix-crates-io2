(define-module (crates-io er c2 erc20) #:use-module (crates-io))

(define-public crate-erc20-0.1.0 (c (n "erc20") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web3") (r "^0.13") (d #t) (k 0)))) (h "17vyqgfrzfm89fxgh8byzjba78l1hg5rl7yxj85pk2sk3snfpqdb")))

(define-public crate-erc20-0.1.1 (c (n "erc20") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "web3") (r "^0.13") (d #t) (k 0)))) (h "0ffvkqhzg10sncfiihc6df8px2annahz92di9nairh6zq9i6n8qf")))

