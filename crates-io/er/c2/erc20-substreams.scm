(define-module (crates-io er c2 erc20-substreams) #:use-module (crates-io))

(define-public crate-erc20-substreams-0.1.0 (c (n "erc20-substreams") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "substreams") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "08jadi4lx25mvbcbdbnmlrvgmnlbzmxmhrbfmwpggmlzcsv3wq0d") (y #t)))

(define-public crate-erc20-substreams-0.1.1 (c (n "erc20-substreams") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "substreams") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0j4frbvzb670vci15yiiq2qcpi48svri2dd9rny2g3n77l6gd2n2")))

