(define-module (crates-io er eb erebus-sdk-rust) #:use-module (crates-io))

(define-public crate-erebus-sdk-rust-0.1.0 (c (n "erebus-sdk-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.3") (f (quote ("json" "rustls-tls" "gzip" "brotli" "rustls-tls-native-roots"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "macros" "time" "net"))) (d #t) (k 0)))) (h "1qil2psa9nach304q911vgmq8vv2rmk1wwi0c1g4hg2651l5fmmf")))

