(define-module (crates-io er ro erroneous-derive) #:use-module (crates-io))

(define-public crate-erroneous-derive-1.0.0 (c (n "erroneous-derive") (v "1.0.0") (d (list (d (n "matches2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (d #t) (k 0)))) (h "0ac67hgik510w7bk8vn3qmgqzii8l1bqqny5anyd1p5ys4h6x9kg")))

(define-public crate-erroneous-derive-1.0.1 (c (n "erroneous-derive") (v "1.0.1") (d (list (d (n "matches2") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (d #t) (k 0)))) (h "1vhjcf7a92bfc7rwrv7kcb95xk2ic66ikjh1ikkwshn4jaba5q9k")))

