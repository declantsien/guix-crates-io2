(define-module (crates-io er ro errorderive) #:use-module (crates-io))

(define-public crate-errorderive-0.0.1 (c (n "errorderive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^0.15.39") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0kvk7xqrrghr5igz07gxb9znksfsaki449y088sylsyfsq2inbay")))

