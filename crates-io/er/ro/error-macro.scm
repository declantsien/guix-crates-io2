(define-module (crates-io er ro error-macro) #:use-module (crates-io))

(define-public crate-error-macro-0.1.0 (c (n "error-macro") (v "0.1.0") (h "199sckig1fnv01438dw10061cdrvflpgmzbihh8xywx1192linw6") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-error-macro-0.1.1 (c (n "error-macro") (v "0.1.1") (h "03avymmfxgl13zwv06x4788ymgviyp7i81p5c9adpcc894kcrxhp") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-error-macro-0.2.0 (c (n "error-macro") (v "0.2.0") (h "1zsidppkwjdfm9q4h5nsm7j5k5hkscfxzd859n1k07hmf3zky3kh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-error-macro-0.2.1 (c (n "error-macro") (v "0.2.1") (h "1h92yh333fm8ihifh6c06hj0p3h8q8fgrdaj0pkfyw0jnlrq7yjn") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

