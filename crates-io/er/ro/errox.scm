(define-module (crates-io er ro errox) #:use-module (crates-io))

(define-public crate-errox-0.1.0 (c (n "errox") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1pljhnglbb58k97imvxii8r8s2k87lg8jd8vp4wwzrgnvqf81zas")))

(define-public crate-errox-0.1.1 (c (n "errox") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1cnc8qdkv9qawh5d9qzsmw47rnil8f9xqqwh8fv94xdxlng1gsja")))

