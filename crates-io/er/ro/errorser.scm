(define-module (crates-io er ro errorser) #:use-module (crates-io))

(define-public crate-errorser-1.0.1 (c (n "errorser") (v "1.0.1") (d (list (d (n "bincode") (r "^0.4.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.37") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^0.6.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.11") (d #t) (k 0)))) (h "05iv667wvj70h96sgyxvagprwc16jiy3x36xav5lwlg0jxgcywn2")))

