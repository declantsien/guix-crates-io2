(define-module (crates-io er ro errorfunctions) #:use-module (crates-io))

(define-public crate-errorfunctions-0.1.0 (c (n "errorfunctions") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0r1y0dc0bqvpz41j58lw2n25zfrp9sxiv5wi7r79glzj73jxhw6j")))

(define-public crate-errorfunctions-0.1.1 (c (n "errorfunctions") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1164q2zzi2i1pgp2fmmsypbl9hm875rbix8zw6shqdbq4p771lq0")))

(define-public crate-errorfunctions-0.2.0 (c (n "errorfunctions") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)))) (h "1rpi24yhpinab7ky4r5wbxq16ya0yiyw5njclgis8nk6sj1lhqdj")))

