(define-module (crates-io er ro error_union) #:use-module (crates-io))

(define-public crate-error_union-0.1.0 (c (n "error_union") (v "0.1.0") (d (list (d (n "const_panic") (r "^0.2.8") (d #t) (k 0)) (d (n "error_union_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0cdgbf71mpipkfw6b1lnnimb78qkhh6xwgsyf2jwsdx7wqz5kfij")))

(define-public crate-error_union-0.2.0 (c (n "error_union") (v "0.2.0") (d (list (d (n "const_panic") (r "^0.2.8") (d #t) (k 0)) (d (n "error_union_derive") (r "^0.2.0") (d #t) (k 0)))) (h "16qsckzibrf4k21p8nbx3wzbp1nakgpa6p6dirvx1w3vqphjffii")))

