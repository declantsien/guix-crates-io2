(define-module (crates-io er ro error_hook) #:use-module (crates-io))

(define-public crate-error_hook-0.1.0 (c (n "error_hook") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "error_hook_attr") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1zjkz4vaknw1wlwlxjbia06hdbk7s788svqgjd86g8sa4nhdpjch") (f (quote (("default") ("attribute"))))))

(define-public crate-error_hook-0.2.0 (c (n "error_hook") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "error_hook_attr") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1s9zhl27xylhxkm5bpixp6l8z3k1bj0x5vxbiijdcdsc51jb3wmd") (f (quote (("default") ("attribute"))))))

(define-public crate-error_hook-0.2.1 (c (n "error_hook") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "error_hook_attr") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "01fk7kqhg1na9dkqc52vz3mjf09r0as9x3yv8vmqd70s7fc6xj4l") (f (quote (("default") ("attribute"))))))

(define-public crate-error_hook-0.2.2 (c (n "error_hook") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "error_hook_attr") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1n5zy3mzxrmnfvnd3ccxldp7hafqh8lis1aw22xgvsk6s0b72ga0") (f (quote (("default") ("attribute"))))))

(define-public crate-error_hook-0.2.3 (c (n "error_hook") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "error_hook_attr") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "11ynz9vp1wq7p0f14d9xp1lif0zhh5l6zlxl5p8li85x39vqh62v") (f (quote (("default") ("attribute"))))))

