(define-module (crates-io er ro error-gen) #:use-module (crates-io))

(define-public crate-error-gen-0.1.0 (c (n "error-gen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.28") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gxa1q2x6763c851f6f70lpncj0hhs2j46avx5gyr3jx5ws4xfqh")))

