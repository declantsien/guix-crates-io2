(define-module (crates-io er ro error-ext) #:use-module (crates-io))

(define-public crate-error-ext-0.1.0 (c (n "error-ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1nygjpppqz2w40j1r4drbcvkbzp95agi1b7lyi09rhxkx442vihh")))

(define-public crate-error-ext-0.1.1 (c (n "error-ext") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0vqg9g9yqsj9g89ipnwkwxcnsgsxqik4hvw5anifiyvgyf0sl530")))

(define-public crate-error-ext-0.2.0 (c (n "error-ext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "047xpyr80z736jpq515s5f0lwkigwrjfqy4w7a2zh7h7kr9k2k89")))

(define-public crate-error-ext-0.2.1 (c (n "error-ext") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "utoipa") (r "^4.1") (f (quote ("axum_extras"))) (o #t) (d #t) (k 0)))) (h "0grj1kfy6457jnxsz1b2y3q3nnjshfjz94r96wkbp0nfh5n1z19p")))

