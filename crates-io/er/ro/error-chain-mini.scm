(define-module (crates-io er ro error-chain-mini) #:use-module (crates-io))

(define-public crate-error-chain-mini-0.0.1 (c (n "error-chain-mini") (v "0.0.1") (d (list (d (n "error-chain-mini-derive") (r "^0.0.1") (d #t) (k 2)))) (h "1y9ik0b8imz1fzfacsk37afk8fl0kndb1kp3hl52dyxg71npggxw")))

(define-public crate-error-chain-mini-0.0.2 (c (n "error-chain-mini") (v "0.0.2") (d (list (d (n "error-chain-mini-derive") (r "^0.0.2") (d #t) (k 2)))) (h "1b6i1cl42kqgr67xm6pnmp9i00akpzacs6vqknpfglmfc99l51jc")))

(define-public crate-error-chain-mini-0.0.3 (c (n "error-chain-mini") (v "0.0.3") (d (list (d (n "error-chain-mini-derive") (r "^0.0.3") (d #t) (k 2)))) (h "0gy6g1qdzqkrwsl62gwbcdbbcl9l37jivp43vaik89d90c9pc9j7")))

(define-public crate-error-chain-mini-0.0.4 (c (n "error-chain-mini") (v "0.0.4") (d (list (d (n "error-chain-mini-derive") (r "^0.0.4") (d #t) (k 2)))) (h "1phyqp81g6s0qas7lvd1w60381vfmw4xg5b4qsid7hbfpapfh2hz")))

(define-public crate-error-chain-mini-0.1.0 (c (n "error-chain-mini") (v "0.1.0") (d (list (d (n "error-chain-mini-derive") (r "^0.1.0") (d #t) (k 2)))) (h "01y4gvvmifdkqa4py5a6awnh56rmv71j70k6dbin1jsvc58f8755")))

(define-public crate-error-chain-mini-0.1.1 (c (n "error-chain-mini") (v "0.1.1") (d (list (d (n "error-chain-mini-derive") (r "^0.1.1") (d #t) (k 2)))) (h "0bbgj11shkaxcwj1w1jzpmgqm72cz0c6jbd9jzj6y5h3idpsg8zw")))

(define-public crate-error-chain-mini-0.1.2 (c (n "error-chain-mini") (v "0.1.2") (d (list (d (n "error-chain-mini-derive") (r "^0.1.2") (d #t) (k 2)))) (h "100008d417lzh1831229h3k74gm9i7vcwhrwdqqfsg17w5vj2ws0")))

(define-public crate-error-chain-mini-0.2.0 (c (n "error-chain-mini") (v "0.2.0") (d (list (d (n "error-chain-mini-derive") (r "^0.2.0") (d #t) (k 2)))) (h "1ilagvwid2c978p7xi7dbmq8z5qif32bxm35j7y3krf12x7v0fai")))

