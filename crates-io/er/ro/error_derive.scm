(define-module (crates-io er ro error_derive) #:use-module (crates-io))

(define-public crate-error_derive-0.1.0 (c (n "error_derive") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "075a0k9ph7ypqnsqx387g7crvsyhjxgpbmc0vxm5cjhj05362vqc")))

(define-public crate-error_derive-0.1.1 (c (n "error_derive") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "02f4swmy8550nh6riapd3bp6vidl3w339qkywkmfyxb2x47zgyxr")))

(define-public crate-error_derive-0.2.0 (c (n "error_derive") (v "0.2.0") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "1hqnpncyb2f33v396xig3vbnmfk2pas8wp2ingz8fa39r6wwnmsv")))

