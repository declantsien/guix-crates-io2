(define-module (crates-io er ro error-utils) #:use-module (crates-io))

(define-public crate-error-utils-0.1.0 (c (n "error-utils") (v "0.1.0") (d (list (d (n "error-utils-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.5.9") (d #t) (k 2)))) (h "051pfzykkbfcqya3y5v9k4nspxwchglp1w3ma75ganmg3yy76q19") (f (quote (("derive" "error-utils-derive") ("default"))))))

(define-public crate-error-utils-0.1.1 (c (n "error-utils") (v "0.1.1") (d (list (d (n "error-utils-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.5.9") (d #t) (k 2)))) (h "1j4cb0ayvwlnwzc452wv1hdmxyzsm7hk2c90qs7zn783by6gfz9s") (f (quote (("derive" "error-utils-derive") ("default"))))))

(define-public crate-error-utils-0.1.2 (c (n "error-utils") (v "0.1.2") (d (list (d (n "error-utils-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.5.9") (d #t) (k 2)))) (h "1hj8nsh78fh0nqh4awdyzkkaqx1r3vjam8vwdja99025yvzwazs4") (f (quote (("derive" "error-utils-derive") ("default" "derive"))))))

(define-public crate-error-utils-0.1.3 (c (n "error-utils") (v "0.1.3") (d (list (d (n "error-utils-derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.5.9") (d #t) (k 2)))) (h "1qmk84g4s6yzyf6aa4n1706fxbr2a491frj6sgp87a0mlp77abc7") (f (quote (("derive" "error-utils-derive") ("default" "derive"))))))

(define-public crate-error-utils-0.1.4 (c (n "error-utils") (v "0.1.4") (d (list (d (n "error-utils-derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.5.9") (d #t) (k 2)))) (h "1vr8q25lc9sbk1pxjwi0p88f6f4kid9pgyinxnws2zqd6jjx8gab") (f (quote (("derive" "error-utils-derive") ("default" "derive"))))))

