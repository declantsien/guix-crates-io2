(define-module (crates-io er ro error-stack-macros) #:use-module (crates-io))

(define-public crate-error-stack-macros-0.0.0-reserved (c (n "error-stack-macros") (v "0.0.0-reserved") (d (list (d (n "error-stack") (r "^0.1.1") (k 2)))) (h "1pr7ygczyrhmhhmhaxapa0q2zvlpdpazk6mjsav40v7jnr3q4ljj") (r "1.63.0")))

