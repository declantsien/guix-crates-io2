(define-module (crates-io er ro error-chain-mini-derive) #:use-module (crates-io))

(define-public crate-error-chain-mini-derive-0.0.1 (c (n "error-chain-mini-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "1xn7048xqklnl1i9g2584wj766szshaq3r0ff3hwv7pa4m5bp0fc")))

(define-public crate-error-chain-mini-derive-0.0.2 (c (n "error-chain-mini-derive") (v "0.0.2") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "1lm5gpy918mc7fq00hvp1hwvpsyikflqlij8jbzwvzj9rzidw3sg")))

(define-public crate-error-chain-mini-derive-0.0.3 (c (n "error-chain-mini-derive") (v "0.0.3") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "030f1vbygqc6msbghyg7g45b09aqm98zmf501j0wg8q8w6g04ygb")))

(define-public crate-error-chain-mini-derive-0.0.4 (c (n "error-chain-mini-derive") (v "0.0.4") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "1gc8dq1mi9vvf5mgsf86b8d7r67rhm3lq95jqs76778lnlczlqaf")))

(define-public crate-error-chain-mini-derive-0.1.0 (c (n "error-chain-mini-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "0hapi8c1c6nlcrccvxz301ac29ybj33my34x69i102zycgnh4h07")))

(define-public crate-error-chain-mini-derive-0.1.1 (c (n "error-chain-mini-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.7") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "082pvj33a2189nfjrdjc9qgk3518gm30ai4nh7ll8sc95yi04p8k")))

(define-public crate-error-chain-mini-derive-0.1.2 (c (n "error-chain-mini-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "0z6xq0slk4c175gzgj3b1iwgkwjydysd0j9mjwbz1rg04hi8dj6k")))

(define-public crate-error-chain-mini-derive-0.2.0 (c (n "error-chain-mini-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.8.1") (d #t) (k 0)))) (h "13g6v90y6q3i6xdbjbqk2xk73k8jf0d26q665mkbc07jw7f8qsfd")))

