(define-module (crates-io er ro error_def) #:use-module (crates-io))

(define-public crate-error_def-0.1.0 (c (n "error_def") (v "0.1.0") (h "1zghyrbrgiasnnqscw341xam7kc8qjhm0m072xiw9pnbknycdfkj")))

(define-public crate-error_def-0.2.0 (c (n "error_def") (v "0.2.0") (h "1djh8xmgrn19s7xa2kpc2rwx4jqscknrmnmgcfismy6ysw9lqz43")))

(define-public crate-error_def-0.3.0 (c (n "error_def") (v "0.3.0") (h "176r01clbq1fqp94nyvvr8vybv8dw2g783nzaraxrbci3krwkqcd")))

(define-public crate-error_def-0.3.1 (c (n "error_def") (v "0.3.1") (h "14vbsa60c5mkr98ic68ich7qx7inv9vmnl6hsn24js36l8qig3ij")))

(define-public crate-error_def-0.3.2 (c (n "error_def") (v "0.3.2") (h "0zdvm77g7pjy7zs0j7dcbkk0r8n3cinrylfccdshlb4sxy7y1axz")))

(define-public crate-error_def-0.3.3 (c (n "error_def") (v "0.3.3") (h "1jnbq7rdmzdlhszhq45m367ng2vfjh9d0r3axh0b52llpvpkqb1q")))

(define-public crate-error_def-0.3.4 (c (n "error_def") (v "0.3.4") (h "1rplgjdiznp1wrxc8gkinb06viscvy2mcrkwzqp7b2n0550argnm")))

(define-public crate-error_def-0.3.5 (c (n "error_def") (v "0.3.5") (h "0ir4l8h4q1vind6s0pg6krk7c23jljvl9sa63v178qh258qh044r")))

(define-public crate-error_def-0.3.6 (c (n "error_def") (v "0.3.6") (h "0s55534xx3g8v7dka45frp18nhzm8ibda5lhqh8gbpgp45625p4b")))

(define-public crate-error_def-0.3.7 (c (n "error_def") (v "0.3.7") (h "0wjsfiyzpp9fmvlkh6sw9pln0k7icm34f46i9hs4279y11z1xzfy")))

(define-public crate-error_def-0.3.8 (c (n "error_def") (v "0.3.8") (h "0zh126z74ja3rp90z4cdarvzqinplb3wqix7afpbhicglr6zjgw6")))

(define-public crate-error_def-0.3.9 (c (n "error_def") (v "0.3.9") (h "0ibvjvw675zh4zws2i7269h879akszpm4brc6cmsy3kvs3vsfzli")))

(define-public crate-error_def-0.3.10 (c (n "error_def") (v "0.3.10") (h "1br8hhz58lmnhx7nmk147kxq6d17r5ylws0xag4zy5kzsz6ac11c")))

(define-public crate-error_def-0.3.11 (c (n "error_def") (v "0.3.11") (h "1ksk4gfwyky81mv2znr8fxcq5r0ldzn517h4x4vqgn1azn5j0ch5")))

(define-public crate-error_def-0.3.12 (c (n "error_def") (v "0.3.12") (h "1yls54whgfq800br8q3hg0823ybbw2igql1mgbh20syzlmlb7yff")))

(define-public crate-error_def-0.3.13 (c (n "error_def") (v "0.3.13") (h "0gc4bkg3fzajrzi1pw13qm6svfw33h2vnd5pbg6yxrjkw4yh8j0x")))

(define-public crate-error_def-0.3.14 (c (n "error_def") (v "0.3.14") (h "0s3wgsmz535z445l97b0hrq16mnqdz7j0l940f942mwsrg0w84df")))

(define-public crate-error_def-0.3.15 (c (n "error_def") (v "0.3.15") (h "0dk1pdgmy09n8jcgw4jvj47clrkv71q3r8ajbyrzx5svf7yr9mmc")))

(define-public crate-error_def-0.3.16 (c (n "error_def") (v "0.3.16") (h "01h054rlaq8hpn4p25r89py3vxjf2ymi9ypql9vv7wkjbzdg4pw9")))

