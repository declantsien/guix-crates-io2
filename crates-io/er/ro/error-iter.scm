(define-module (crates-io er ro error-iter) #:use-module (crates-io))

(define-public crate-error-iter-0.1.0 (c (n "error-iter") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "02kn4ah7sr281grdl54yfanv9w6frla31lbyh5lxrbnilfc0f301")))

(define-public crate-error-iter-0.2.0 (c (n "error-iter") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1a86cvd1jbrqimc6srhwfc3gygg8ipryi41c9n8g5ap500qgx6z0")))

(define-public crate-error-iter-0.3.0 (c (n "error-iter") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1pwjhx852wfsfixmxilry2dpgdjzgfdia4c6jk296n1hgyd2k3ym") (r "1.37")))

(define-public crate-error-iter-0.4.0 (c (n "error-iter") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "194zlfqs17fjdl8lcwz4j1dzi46wb89l3i3x1blqa2c69axwzfbq") (r "1.37")))

(define-public crate-error-iter-0.4.1 (c (n "error-iter") (v "0.4.1") (d (list (d (n "thiserror") (r "=1.0.39") (d #t) (k 2)))) (h "1mncbr4nkdsy99llgw3qpcmr95y88bbj2r32nvmqvffij1ym8w40") (r "1.37")))

