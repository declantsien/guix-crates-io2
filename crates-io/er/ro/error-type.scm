(define-module (crates-io er ro error-type) #:use-module (crates-io))

(define-public crate-error-type-0.1.0 (c (n "error-type") (v "0.1.0") (h "08dp6w4w7ss3figmw48921lqz9zkvx5als1vshdfw7jsjlp53y06")))

(define-public crate-error-type-0.1.1 (c (n "error-type") (v "0.1.1") (h "14sqzb3qcb8s334p1fi0wld0xrn4npj0vbgd9sjd120d3rsjziq2")))

(define-public crate-error-type-0.1.2 (c (n "error-type") (v "0.1.2") (h "0kkx32rrih0a5krpxm6yjkdhyr8frsks2h6wf4s2himls907dwhz")))

