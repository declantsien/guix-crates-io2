(define-module (crates-io er ro errors_rust) #:use-module (crates-io))

(define-public crate-errors_rust-0.1.0 (c (n "errors_rust") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1l78mnrg31s6z4f0h1aks9kvkpvzhqxc9hslg3jjdqclzrpla9fg")))

