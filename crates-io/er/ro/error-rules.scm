(define-module (crates-io er ro error-rules) #:use-module (crates-io))

(define-public crate-error-rules-0.1.1 (c (n "error-rules") (v "0.1.1") (h "07wngnbmcnw5y9rzr2g3qqhwp1x9xysr3s55y2r1q8jnxm8gsvq3")))

(define-public crate-error-rules-0.1.2 (c (n "error-rules") (v "0.1.2") (h "17ns9vbm4ij2vqan53ikfbgxd3v9y2m9f3yxzcd6xj74x5z54a9d")))

(define-public crate-error-rules-0.1.3 (c (n "error-rules") (v "0.1.3") (h "19nr6rignz8jnvgiyfdwibr94fpa9rkzssfhmnybqs62sdjr9lh3")))

(define-public crate-error-rules-0.2.1 (c (n "error-rules") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1j9wkn763hapdk97pk89p3z8dvnyjg33sqg265h7xbll46x26qm6")))

(define-public crate-error-rules-0.2.2 (c (n "error-rules") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vmb439jn6p5vn6vyby2p5r4w1m5wksvkg8arjbgc36y8q5dz9af")))

(define-public crate-error-rules-0.2.4 (c (n "error-rules") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18k5ljw57nskb5dd0qsb3mkiagdrqx0dm0by7mrv5ibsa1m2vssj")))

(define-public crate-error-rules-0.2.5 (c (n "error-rules") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1nhvms0aqj51xcc0fdwv9p2mmkfcz4hb0h2rj4gi4dkmh8b5f0zh")))

(define-public crate-error-rules-1.0.0 (c (n "error-rules") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "124s4abb4xrb85bv2l9lbw9h2gblx4rv866m4qf60l5w9s8k32fh")))

(define-public crate-error-rules-1.0.1 (c (n "error-rules") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r1z6g37ibmzrysjbabvh266qm6xxj68910614s96rbk03fnhlyd")))

