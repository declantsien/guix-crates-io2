(define-module (crates-io er ro errorjson) #:use-module (crates-io))

(define-public crate-errorjson-0.0.1 (c (n "errorjson") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d51979b7rhrcc1ssapy3rrg57mjwwvq2j4ka57n1hrpn8v0j8yp")))

