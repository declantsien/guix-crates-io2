(define-module (crates-io er ro error_spanned) #:use-module (crates-io))

(define-public crate-error_spanned-0.1.0 (c (n "error_spanned") (v "0.1.0") (d (list (d (n "error_spanned_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1ydks7fkrdjs2kj66hn8xw7cj0ifj4djckr43yp2l0hrwjp9yfv4")))

