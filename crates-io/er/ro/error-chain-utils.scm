(define-module (crates-io er ro error-chain-utils) #:use-module (crates-io))

(define-public crate-error-chain-utils-0.1.0 (c (n "error-chain-utils") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "error-chain-utils-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1xxlsnjqvs26gvr73wk3qc818bkdzmh1kx0r6kckk4prcwv5wzdi")))

