(define-module (crates-io er ro error_union_derive) #:use-module (crates-io))

(define-public crate-error_union_derive-0.1.0 (c (n "error_union_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0prdr75hif1wn7wblz5m75zl2a9n4bfrnfr5il1k3jw44rr4d66i")))

(define-public crate-error_union_derive-0.2.0 (c (n "error_union_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nw4gd11jy6n70clrzg3i3rk6ac92q487kfmzzjwhdhkcqrwn72c")))

