(define-module (crates-io er ro error-utils-derive) #:use-module (crates-io))

(define-public crate-error-utils-derive-0.1.0 (c (n "error-utils-derive") (v "0.1.0") (d (list (d (n "quote") (r "~1.0.20") (d #t) (k 0)) (d (n "syn") (r "~1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gqr7b5iwkxz1cq8b3zkzmsnly3gz6n48qaz0pagzc9yijljn36c")))

(define-public crate-error-utils-derive-0.1.1 (c (n "error-utils-derive") (v "0.1.1") (d (list (d (n "quote") (r "~1.0.20") (d #t) (k 0)) (d (n "syn") (r "~1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xm26y2d6dsc6bvjvg8pqf2m75bgdwabkvsx9c8r2sachkzkggvh")))

(define-public crate-error-utils-derive-0.1.2 (c (n "error-utils-derive") (v "0.1.2") (d (list (d (n "quote") (r "~1.0.20") (d #t) (k 0)) (d (n "syn") (r "~1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0aynkz0vh5w81z17nyvlqypyacjk0lklljirjccr2ybwanprgzrx")))

(define-public crate-error-utils-derive-0.1.3 (c (n "error-utils-derive") (v "0.1.3") (d (list (d (n "quote") (r "~1.0.20") (d #t) (k 0)) (d (n "syn") (r "~1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gh3ngyprnfq1y002vyski2s8jms9mzwvsbv9wq2ldvbyx8vixx4")))

