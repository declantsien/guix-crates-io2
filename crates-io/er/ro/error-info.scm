(define-module (crates-io er ro error-info) #:use-module (crates-io))

(define-public crate-error-info-0.1.0 (c (n "error-info") (v "0.1.0") (d (list (d (n "error-info-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "linkme") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15c0gbpihhb5n3gmlvpl4wjnww1l5qmwxn5i13pmx30xdid6grak")))

(define-public crate-error-info-0.2.0 (c (n "error-info") (v "0.2.0") (d (list (d (n "error-info-macros") (r "=0.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "linkme") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "161hkh9iq869x971735scmx32x75n7ygkxqx4m6f04sprzv8c47l") (f (quote (("default")))) (s 2) (e (quote (("summary" "error-info-macros/summary" "dep:linkme" "dep:serde"))))))

(define-public crate-error-info-0.3.0 (c (n "error-info") (v "0.3.0") (d (list (d (n "error-info-macros") (r "=0.3.0") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "linkme") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hhmijx8fxpf9cpfdm41bqyd2vbv1akb82840sim2g5bzgsnc7mj") (f (quote (("default")))) (s 2) (e (quote (("summary" "error-info-macros/summary" "dep:linkme" "dep:serde"))))))

