(define-module (crates-io er ro error-tree) #:use-module (crates-io))

(define-public crate-error-tree-0.1.0 (c (n "error-tree") (v "0.1.0") (d (list (d (n "cpal") (r "^0.15.3") (d #t) (k 2)) (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "export-magic") (r "^0.1.0") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "std" "env-filter"))) (k 0)))) (h "04vhlnd0q9za79vms2n8ljgi89h7ckmnr18l3jdndwvixpqg3qnw")))

