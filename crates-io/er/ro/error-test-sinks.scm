(define-module (crates-io er ro error-test-sinks) #:use-module (crates-io))

(define-public crate-error-test-sinks-0.1.0 (c (n "error-test-sinks") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "17v5h0fyifqxbjim86y7b7f952vhikg4a81bgv4bns9pw7z2w5l1")))

(define-public crate-error-test-sinks-0.1.1 (c (n "error-test-sinks") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0l4nma2mnqs3qr1jxxcvhvv39g9hdm08c652jl3y5qbddpcr4441")))

