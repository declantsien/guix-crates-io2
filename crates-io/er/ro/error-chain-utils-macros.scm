(define-module (crates-io er ro error-chain-utils-macros) #:use-module (crates-io))

(define-public crate-error-chain-utils-macros-0.1.0 (c (n "error-chain-utils-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("proc-macro" "parsing" "full"))) (d #t) (k 0)))) (h "195s5y6wm4b2k8mx5cajkfzqa8qqy566mf4m21dfr80bvd09fkf7")))

