(define-module (crates-io er ro erroneous) #:use-module (crates-io))

(define-public crate-erroneous-1.0.0 (c (n "erroneous") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.13") (d #t) (k 2)) (d (n "erroneous-derive") (r "^1") (d #t) (k 0)))) (h "04w40g5qx4fk8134pd378r5vh6j21ysww76f9k0s9vw7hm72cd9h")))

(define-public crate-erroneous-1.0.1 (c (n "erroneous") (v "1.0.1") (d (list (d (n "derive_more") (r "^0.13") (d #t) (k 2)) (d (n "erroneous-derive") (r "^1.0.1") (d #t) (k 0)))) (h "0f6i94p1qx7amh7f49g7fb5k35rn1fzy439il37vx5qm4yyjw25s")))

