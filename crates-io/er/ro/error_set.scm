(define-module (crates-io er ro error_set) #:use-module (crates-io))

(define-public crate-error_set-0.1.0 (c (n "error_set") (v "0.1.0") (h "19n9whmh80sswvqchqhwdk4vigv4a3dabxiak55wbbs9jkl0dgs6")))

(define-public crate-error_set-0.2.0 (c (n "error_set") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1clpnzzibh5yflh1i2ijhlhwgig2bqsyc3rxj6yj4qx9xnmznhf3")))

(define-public crate-error_set-0.3.0 (c (n "error_set") (v "0.3.0") (d (list (d (n "error_set_impl") (r "=0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "1im5gpi5wvwi6w9hyk8r6fjp8pbv1hbg24sws6m2yljd8s8yls94")))

(define-public crate-error_set-0.3.1 (c (n "error_set") (v "0.3.1") (d (list (d (n "error_set_impl") (r "=0.3.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "1q58w17300za08fakbhixwkys039g3lghsf1x6rqxh7irhp1lyn1")))

(define-public crate-error_set-0.3.2 (c (n "error_set") (v "0.3.2") (d (list (d (n "error_set_impl") (r "=0.3.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "10bz9pyjvz44q6i0c1xqp069v61ysakkg4wiwri687wn9ndmg1hl")))

