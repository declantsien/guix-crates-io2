(define-module (crates-io er ro error_hook_attr) #:use-module (crates-io))

(define-public crate-error_hook_attr-0.1.0 (c (n "error_hook_attr") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "05fhkl0zw61zybiral1450w5zpqyq2vkyh6yd82r0jbnmfiph4qj")))

(define-public crate-error_hook_attr-0.1.1 (c (n "error_hook_attr") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "08grzq68sprichnrli98f8d9chvrhdzn5mmp517pqdpqv1jzdgyy")))

(define-public crate-error_hook_attr-0.1.2 (c (n "error_hook_attr") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhvk775hpczy54pcnqsw2h8gp9v5j24nw42f2lplxvsb69745q0")))

