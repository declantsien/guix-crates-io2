(define-module (crates-io er ro error) #:use-module (crates-io))

(define-public crate-error-0.0.0 (c (n "error") (v "0.0.0") (d (list (d (n "typeable") (r "> 0.0.0") (d #t) (k 0)))) (h "113swlpn3b4ykbrh51p2z7vkv8l15izgaajaw7cq01acmaybyg0g")))

(define-public crate-error-0.0.1 (c (n "error") (v "0.0.1") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "09kf099z0bq54h55arrj6g5pva5kwjqx6f0nm8c70g7zzmbf94hf")))

(define-public crate-error-0.0.2 (c (n "error") (v "0.0.2") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "142r8gxripy264raf2a6k0x6gn695riyia1qc60v9y3l7kgay2p6")))

(define-public crate-error-0.0.3 (c (n "error") (v "0.0.3") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "1d9gx9i13wmfrpczwv9mpgwkjmdmbnlc71nlhwrwdbav9xvr17nd")))

(define-public crate-error-0.1.0 (c (n "error") (v "0.1.0") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "1hw70kzricvx7fsambig1qmsbf3g74hjka8klxr3zwh2mgbvb8qv")))

(define-public crate-error-0.1.1 (c (n "error") (v "0.1.1") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "17lg18m93lrhbmn7mfmk5y000xs3zqaxbyp13g4g244lm9n5kl46")))

(define-public crate-error-0.1.2 (c (n "error") (v "0.1.2") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "1hg0jrqvq6j45wn9qvbygab44nrd2c0vh8k7j9gr0j6b7yk4a1i0")))

(define-public crate-error-0.1.3 (c (n "error") (v "0.1.3") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0bsj8kf9wj9ibxcmfvcksgay3xc7da36d2qs2v69vc4zm0dvhb8b")))

(define-public crate-error-0.1.4 (c (n "error") (v "0.1.4") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0x8irxzid9sv7jjmqrk9ram8cpr501ygrbjl56cj4nxr496xl36c")))

(define-public crate-error-0.1.5 (c (n "error") (v "0.1.5") (d (list (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0jph27ppx0wbr83wr6vxj25j1pi7f8fqy3s0cp90rqv2frqdbw4q")))

(define-public crate-error-0.1.6 (c (n "error") (v "0.1.6") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)) (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0p5pyx4g0y50gqw7vfsij2gkhlfxjxm6lxpfh8hw0iwiayks4a39")))

(define-public crate-error-0.1.7 (c (n "error") (v "0.1.7") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)) (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0jkvki9p0zprrm5siq560nb04yv08h6ymnagzq0jj9mmihpg665a")))

(define-public crate-error-0.1.8 (c (n "error") (v "0.1.8") (d (list (d (n "traitobject") (r "*") (d #t) (k 0)) (d (n "typeable") (r "*") (d #t) (k 0)))) (h "0dswxi4z0pfdhw5l58sskagv8rcq5jgah0qra3kpd41z2gf2y9jh")))

(define-public crate-error-0.1.9 (c (n "error") (v "0.1.9") (d (list (d (n "traitobject") (r "^0") (d #t) (k 0)) (d (n "typeable") (r "^0.1") (d #t) (k 0)))) (h "1k47g7ymkivv6xlpxn32dznr5awhdkdi8spg0b68gfs283qhdrm6")))

