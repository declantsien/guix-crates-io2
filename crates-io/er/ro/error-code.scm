(define-module (crates-io er ro error-code) #:use-module (crates-io))

(define-public crate-error-code-0.1.0 (c (n "error-code") (v "0.1.0") (d (list (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0.1") (d #t) (k 2)))) (h "0kwa97xg3af3li23iffljiz5nam8108zk1rjxpyrrdn02m1nsc78") (f (quote (("std")))) (y #t)))

(define-public crate-error-code-0.1.1 (c (n "error-code") (v "0.1.1") (d (list (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0.1") (d #t) (k 2)))) (h "0827n8y3rq61a1jjqb5h6laqzh16phpgzkyv2k7py68jmkdxh912") (f (quote (("std"))))))

(define-public crate-error-code-0.1.2 (c (n "error-code") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0.1") (d #t) (k 2)))) (h "0dz4xq7jyqcy91zxqzhcfaj6dnv2kaf4jpar4ddw5h4f0s3bni0q") (f (quote (("std"))))))

(define-public crate-error-code-0.2.0 (c (n "error-code") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0.1") (d #t) (k 2)))) (h "03dp7pl1y1rpdl3xwf2ywzw12fl7fw1d1grwzpnw606iaamsf53n") (f (quote (("std"))))))

(define-public crate-error-code-1.0.0 (c (n "error-code") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1z0zjs0diaijyiqx86raq0k9y7giwb7ijz2zp5qrb6cj66avbz0g") (f (quote (("std"))))))

(define-public crate-error-code-1.0.1 (c (n "error-code") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "05zysn20942pjiy6cyr7xs5i5kqy6fb7izjlwbba9imaaj56xb9x") (f (quote (("std"))))))

(define-public crate-error-code-2.0.0 (c (n "error-code") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)))) (h "0zzl50sc216sjijx4clw0pnmidxghc6s4i8vynxn930374q1fi37") (f (quote (("std"))))))

(define-public crate-error-code-2.0.1 (c (n "error-code") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)))) (h "1vjkmrqiacpgi00b88ngvacwaj8yb69pdldqzdwk5fyzv9n1yman") (f (quote (("std"))))))

(define-public crate-error-code-2.0.2 (c (n "error-code") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)))) (h "0lmblmk2kr0cy6x3d1y2ywxg3jg6nicf8fahd3l5wb1ddzv9975l") (f (quote (("std"))))))

(define-public crate-error-code-2.1.0 (c (n "error-code") (v "2.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^2") (d #t) (k 0)))) (h "1g1jm037izimadlhzb5r1v8g1222cj87c49ihlailf25klb6x1gj") (f (quote (("std"))))))

(define-public crate-error-code-2.2.0 (c (n "error-code") (v "2.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^2") (d #t) (k 0)))) (h "1zz827pf9p8qrkfhw6bkm9icmq8rjpw9iii39b6y4f9k3w272j0x") (f (quote (("std")))) (y #t)))

(define-public crate-error-code-2.3.0 (c (n "error-code") (v "2.3.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)))) (h "1zxi3pfrmj7hmv2bv94ax8vpylsfs49vwwp48c04wrr5mikma4dm") (f (quote (("std"))))))

(define-public crate-error-code-2.3.1 (c (n "error-code") (v "2.3.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "str-buf") (r "^1") (d #t) (k 0)))) (h "08baxlf8qz01lgjsdbfhs193r9y1nlc566s5xvzyf4dzwy8qkwb4") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.1 (c (n "error-code") (v "3.0.0-beta.1") (h "0gjmf75c6qigg138lzi3h4ib21b69xb3ljpzxkxzv347s0gbh8rb") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.2 (c (n "error-code") (v "3.0.0-beta.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(windows, unix))") (k 0)))) (h "1schmnc7034jrw9ky4v1l7fkrzldaywwyhxhclas8wp84jpzw7pd") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.3 (c (n "error-code") (v "3.0.0-beta.3") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(windows, unix))") (k 0)))) (h "1npi3qsbypsdwmgr4mvvm1397v72qzxq8c08zqfyksj96q4wc5sz") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.4 (c (n "error-code") (v "3.0.0-beta.4") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(windows, unix))") (k 0)))) (h "1zky5wxinsajapccjnq8vb92yy58llckxf2idy7d0k115k9hwjgc") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.5 (c (n "error-code") (v "3.0.0-beta.5") (d (list (d (n "libc") (r "^0.2") (t "cfg(any(windows, unix))") (k 0)))) (h "19jrz9b1win58sv57as081narsgb9d70liy2y2rvkv78jls2iprq") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0-beta.6 (c (n "error-code") (v "3.0.0-beta.6") (h "1ll0zvzwmf2w9aq4rmyh561k31lqq10lp5mgk0lhv80xv8ca7q6j") (f (quote (("std"))))))

(define-public crate-error-code-3.0.0 (c (n "error-code") (v "3.0.0") (h "1z27hg1c3xr7vchrw28h1qlc3xflrxfbmk8nci10ah5d7cnla7i8") (f (quote (("std"))))))

(define-public crate-error-code-3.1.0 (c (n "error-code") (v "3.1.0") (h "0fa1w2wg36wcmxiv629ccn6hq64v9pxfkr5kjkcma6k4lvhlg896") (f (quote (("std"))))))

(define-public crate-error-code-3.2.0 (c (n "error-code") (v "3.2.0") (h "0nqpbhi501z3ydaxg4kjyb68xcw025cj22prwabiky0xsljl8ix0") (f (quote (("std"))))))

