(define-module (crates-io er ro error-trees) #:use-module (crates-io))

(define-public crate-error-trees-0.1.0 (c (n "error-trees") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "18xh6iv46kbj7h8h12jgnwd4hapm2jdh2h2g9ymaqxc152zy69j7") (y #t)))

(define-public crate-error-trees-0.1.1 (c (n "error-trees") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "01c830v1a6jbi6snm7650znd76dv617qkj6z4jg3vr507bs6yqqi") (y #t)))

(define-public crate-error-trees-0.2.0 (c (n "error-trees") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "10r0mlam4nnsdqld5y7m7dd29qjyy41ifpim3ypmzwwl85rcaii7")))

