(define-module (crates-io er ro error-window) #:use-module (crates-io))

(define-public crate-error-window-1.0.0 (c (n "error-window") (v "1.0.0") (d (list (d (n "dialog") (r "^0.3.0") (d #t) (k 0)) (d (n "error-window-macro") (r "^1.0.0") (d #t) (k 0)))) (h "1nz55di0p5xrgxib8yb0xk2jwr4dsy3ha36mbbl4g6w363zqwc1z")))

