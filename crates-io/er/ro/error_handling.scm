(define-module (crates-io er ro error_handling) #:use-module (crates-io))

(define-public crate-error_handling-0.1.1 (c (n "error_handling") (v "0.1.1") (h "0ajbd02rb7myc1ks1kcfivhsryxykc7l2f6kwmdsvv14a7808zac")))

(define-public crate-error_handling-1.0.0 (c (n "error_handling") (v "1.0.0") (h "05zjkr2i8siszlq8c2d0dy55j8p9yjq22zx3p0g0124j0klkppxz")))

