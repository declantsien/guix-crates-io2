(define-module (crates-io er ro error-window-macro) #:use-module (crates-io))

(define-public crate-error-window-macro-1.0.0 (c (n "error-window-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "022092i8v48ind3s5j6r9z3r6lxa7vxqp1pjc5wq17divsi41jv5")))

