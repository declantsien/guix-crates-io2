(define-module (crates-io er ro error_generator) #:use-module (crates-io))

(define-public crate-error_generator-1.2.0 (c (n "error_generator") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "077fl4zk5kypdclhsqiz7pkw2n7wm1hx5zsy372r5aw2syadj8hw")))

(define-public crate-error_generator-1.2.1 (c (n "error_generator") (v "1.2.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "1r795xfjda7jp6kxpqwg9gm2ji6fdarnrzb2vaiyfwyhh2jywfwq")))

