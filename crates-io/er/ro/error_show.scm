(define-module (crates-io er ro error_show) #:use-module (crates-io))

(define-public crate-error_show-0.1.0 (c (n "error_show") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt" "minwindef" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h0axp9bxf1i62mr39vzri6b77fadmyygcygzf3rwnvbkijk3cp3")))

(define-public crate-error_show-0.1.1 (c (n "error_show") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt" "minwindef" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0arg0k13rz6jj1b2lw9h34z1qi60s8gm322asi8sahw82h3f05d0")))

(define-public crate-error_show-0.1.2 (c (n "error_show") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ntapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt" "minwindef" "libloaderapi" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mnax1fy075k04pqlc3fs8yxqa6v641x6xfhvskvidyk7x3ikzgw")))

(define-public crate-error_show-0.1.3 (c (n "error_show") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ntapi") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt" "minwindef" "libloaderapi" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jymfmj2s66snh05s0kwxiimh1idmbk2ps3gjp70svbnnnh1p0n9")))

