(define-module (crates-io er ro error-ex) #:use-module (crates-io))

(define-public crate-error-ex-0.1.0 (c (n "error-ex") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0rna6h8an30jm0f6159sa6b2x2d73l2v2jgcxbcjzg5xhz4svsj3") (y #t) (r "1.56")))

(define-public crate-error-ex-0.1.1 (c (n "error-ex") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1ivd5k7m1chkzg6dixgw9hi6mbi6c9aqs4i0q823ahjzlaqw1vgq") (y #t) (r "1.56")))

(define-public crate-error-ex-0.1.2 (c (n "error-ex") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "14v4jz4mjhnarc1ri9bgp6h8yp873kycyfqn0n61gg4a8mih4haw") (y #t) (r "1.56")))

(define-public crate-error-ex-0.1.3 (c (n "error-ex") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "02sd0j9iwcl3cb3jvnzhs2mkdpgs6sca3mvf0lpdbb3xzr772hsj") (r "1.56")))

