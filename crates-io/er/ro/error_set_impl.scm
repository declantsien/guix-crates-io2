(define-module (crates-io er ro error_set_impl) #:use-module (crates-io))

(define-public crate-error_set_impl-0.3.0 (c (n "error_set_impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0yaw7xbd3y5rmr79ljnrs98zbj2kpx4mdy4q6xq414mwqjwrsnhl")))

(define-public crate-error_set_impl-0.3.1 (c (n "error_set_impl") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("parsing" "derive" "printing" "proc-macro" "clone-impls"))) (k 0)))) (h "1vayjmhq1si3xmp4a8c6fkvqdd8fyxmmmpkvknsbg3qbyy76dvxk")))

(define-public crate-error_set_impl-0.3.2 (c (n "error_set_impl") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("parsing" "derive" "printing" "proc-macro" "clone-impls"))) (k 0)))) (h "1xzyv09qrp6pam8z33hdwbqq02ffripmwbssi41ddni9cjdiv8fa")))

