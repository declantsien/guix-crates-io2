(define-module (crates-io er ro error-chain) #:use-module (crates-io))

(define-public crate-error-chain-0.1.8 (c (n "error-chain") (v "0.1.8") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "12ay2668qqc9z3wf41n322c8vxnkcn5saq975z2wvra8yiarqsa2")))

(define-public crate-error-chain-0.1.9 (c (n "error-chain") (v "0.1.9") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "0jaikkh4jga6679z5mll2r6np6rdw0gy12mljgwdz1v49lflb7rn")))

(define-public crate-error-chain-0.1.12 (c (n "error-chain") (v "0.1.12") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "1k0vhy1xadgqzk4n32yywcylv531ksqp8s287wpjnk1fzns7dags")))

(define-public crate-error-chain-0.2.1 (c (n "error-chain") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "1g9lj28r90x1a7b904wjlp05c8bfavkha3hskjb0icprd90nzhca")))

(define-public crate-error-chain-0.2.2 (c (n "error-chain") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "0avmh0lax13pyxnfswkfgvvzli5j15m8i2r76s78pm792zv3iwg9")))

(define-public crate-error-chain-0.3.0 (c (n "error-chain") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "18zns0xn7abzdj9cr5qvawxlr352zpz1z5hjmjcw8vj20j27gfn8")))

(define-public crate-error-chain-0.4.0 (c (n "error-chain") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "0mh92hci9m4lr35dmwsjhfdg7d7hh7haknnblw40zb9v0c05xyhl") (y #t)))

(define-public crate-error-chain-0.4.1 (c (n "error-chain") (v "0.4.1") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "0jcz9k7l46r8xd2hyka4sxxzq9n4ssdah3x7mh023llq7wqxd467") (y #t)))

(define-public crate-error-chain-0.4.2 (c (n "error-chain") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "1nr0xcdxn4ads6g2gl3wjs8mmpx9msm6zg15x4arphlfrhbvkdm8")))

(define-public crate-error-chain-0.5.0 (c (n "error-chain") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.2.1") (d #t) (k 0)))) (h "0hxi83fy7wll87m9sgj052szz9kyy94zrvydin5jg3hk2p484p5x")))

(define-public crate-error-chain-0.6.0 (c (n "error-chain") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "12dkk1qxwzxhcn67c1rpvla5xrgb73cvw7rcyhpx411m1d7vb75y") (f (quote (("default" "backtrace"))))))

(define-public crate-error-chain-0.6.1 (c (n "error-chain") (v "0.6.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1r6axw1xf4kbfi35fkl0xd5ini9xgap187yslxiacbz3sydxg9lr") (f (quote (("default" "backtrace"))))))

(define-public crate-error-chain-0.6.2 (c (n "error-chain") (v "0.6.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0pjifk7szmz40dm3xkg20hh1al24dz675klhgylwswqx9nqh9nl9") (f (quote (("default" "backtrace"))))))

(define-public crate-error-chain-0.7.0 (c (n "error-chain") (v "0.7.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1m2axfcn8alqmdj0fs9h90pwwka71dd4klil89bz31j0qw4c2x27") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.7.1 (c (n "error-chain") (v "0.7.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "16aplxankbsg4a66vn7q84wlfw4ffvva80czsvalr834adrq3mhw") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.7.2 (c (n "error-chain") (v "0.7.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "03qjh6l2a9fkiyg0428p7q3dcpi47cbmrqf9zmlymkg43v3v731i") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.8.0 (c (n "error-chain") (v "0.8.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "11m146mv1slmf3hfc9k50djrbd7zxz6arf963qixnr39hhx043my") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.8.1 (c (n "error-chain") (v "0.8.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0ijxazr966gjvckl3yrkcziazk3ryrfc466m84p9m2iq314y0c39") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.9.0 (c (n "error-chain") (v "0.9.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0j5j60rpbsf5fipxpi6zsj588544lc7zxa3g1r60g3lca05cybp9") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.10.0 (c (n "error-chain") (v "0.10.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1y1gyj9g5c3k1nzkvxrgry8v9k86kcc585mczrm3qz019s35shyr") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.11.0-rc.1 (c (n "error-chain") (v "0.11.0-rc.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1319lfjvl16d2fjc27kjnjxxxid10wiwvp3hx9llxpr7j6hqb9jp") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.11.0-rc.2 (c (n "error-chain") (v "0.11.0-rc.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0b8hvhh88sw3r71v65gb0s0adnd5844jr4d3y946fibsk9fsblrq") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.11.0 (c (n "error-chain") (v "0.11.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1wykkr0naizbkwxjwia1rch8xhwvgij9khqvjzs07mrmqifislgz") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.0 (c (n "error-chain") (v "0.12.0") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "00kfj096lq5y19rlhandl9nn110kxxjvciiqqiviq94npv9r3rq7") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.1-rc.1 (c (n "error-chain") (v "0.12.1-rc.1") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1.5") (d #t) (k 1)))) (h "1xsvsz4shwcz624rkpxjy7sp83x368psim640fiby4q2jpg4jpsq") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.1 (c (n "error-chain") (v "0.12.1") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1.5") (d #t) (k 1)))) (h "1ndpw1ny2kxqpw6k1shq8k56z4vfpk4xz9zr8ay988k0rffrxd1s") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.2 (c (n "error-chain") (v "0.12.2") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1ka5y0fmymxzx3gz2yrd7rpz2i555m1iw4fpmcggpzcgr1n10wfk") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.3 (c (n "error-chain") (v "0.12.3") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0ssn0r6jh01ajq96n94v3g4v0va62ilk5m5x2rrc2mjnwx8fsldy") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

(define-public crate-error-chain-0.12.4 (c (n "error-chain") (v "0.12.4") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1z6y5isg0il93jp287sv7pn10i4wrkik2cpyk376wl61rawhcbrd") (f (quote (("example_generated") ("default" "backtrace" "example_generated"))))))

