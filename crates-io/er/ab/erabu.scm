(define-module (crates-io er ab erabu) #:use-module (crates-io))

(define-public crate-erabu-0.1.0 (c (n "erabu") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0ik20bq2j4vcd7sb6bs7c9nj0wp9l42hlfpzyiihs58cavgnvxfl")))

(define-public crate-erabu-0.1.1 (c (n "erabu") (v "0.1.1") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1gwwfr21nalzcfphgy5a8rpvhbyqkbqpckfzqwfpb2n0ydiwpwgx")))

