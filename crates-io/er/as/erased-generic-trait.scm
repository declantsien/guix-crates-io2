(define-module (crates-io er as erased-generic-trait) #:use-module (crates-io))

(define-public crate-erased-generic-trait-0.0.1 (c (n "erased-generic-trait") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1sqb2mdghnpdhrclkz46aj4hva3ig98lnj7fb9lbg1v1wrkp4fyy")))

(define-public crate-erased-generic-trait-0.0.2 (c (n "erased-generic-trait") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0611wa7l3qj6i939zf45px2j1gs8xvhgyy4qxh6p6na2nz6a7856")))

(define-public crate-erased-generic-trait-0.0.3 (c (n "erased-generic-trait") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0h09z6ql4ai7j1hrjzc0wjkzhdkr5l1mjwm5x1bs5fp7wmlhhv8a")))

(define-public crate-erased-generic-trait-0.0.4 (c (n "erased-generic-trait") (v "0.0.4") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "09m6b45dhj8081ypai5c3inq0rly6bkc5yhd7mgxk6qg5v18yv33")))

