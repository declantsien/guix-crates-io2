(define-module (crates-io er as erase-pe-header) #:use-module (crates-io))

(define-public crate-erase-pe-header-0.1.0 (c (n "erase-pe-header") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("memoryapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fw29lamk3bvkzzl2y61hss04vfx5019yjqpd84aksx2rdh5bbaw")))

