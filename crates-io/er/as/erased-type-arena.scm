(define-module (crates-io er as erased-type-arena) #:use-module (crates-io))

(define-public crate-erased-type-arena-0.1.0 (c (n "erased-type-arena") (v "0.1.0") (h "0mm9vn9b381g3v68z2jzqz35j0dmcqyhsaz2grhh1r55wrdvndig")))

(define-public crate-erased-type-arena-0.2.0 (c (n "erased-type-arena") (v "0.2.0") (h "019y8k2ygw1spcbvs4dr00s5fsxcbdkg8si6s5kqlmd3cnc80jdm")))

(define-public crate-erased-type-arena-0.3.0 (c (n "erased-type-arena") (v "0.3.0") (h "10iw7qw71r04zy7yzsdhwb25jsbdg7r4b4ivfshjhjs1977qb8yg")))

(define-public crate-erased-type-arena-0.3.1 (c (n "erased-type-arena") (v "0.3.1") (h "049gcpzgqs1rylby9fbhxyqc00vnhvq8j6vbv0crxczqbbpmk0q5")))

