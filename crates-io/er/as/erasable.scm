(define-module (crates-io er as erasable) #:use-module (crates-io))

(define-public crate-erasable-1.0.0 (c (n "erasable") (v "1.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1.0.0") (k 0)))) (h "1v17j9hr74lvvym6cc1vikk07f8v4yvkfnszx3ph6w1rs32cp3wp") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-erasable-1.1.0 (c (n "erasable") (v "1.1.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1.0.0") (k 0)))) (h "17x4wz3b8hhllvcxb0vs5vsrwyzkssacisd0gf7d4qga871pdjfk") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-erasable-1.1.1 (c (n "erasable") (v "1.1.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (k 0)))) (h "13lfjknp00dyyc1jr24ww32n4vs6766sznajyxfnxdqcnkc3rzij") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-erasable-1.2.0 (c (n "erasable") (v "1.2.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (k 0)))) (h "1n3wqp12c9k43za4qdrs9m7wg54bl7qpwhs8i5cidvndkvch9161") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-erasable-1.2.1 (c (n "erasable") (v "1.2.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (k 0)))) (h "0z8b6k8aan6h80vp4mm327lvmrx0mdn4psyiwmj7mm41w468j4az") (f (quote (("default" "alloc") ("alloc"))))))

