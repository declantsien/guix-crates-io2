(define-module (crates-io er as eraserhead-derive) #:use-module (crates-io))

(define-public crate-eraserhead-derive-1.0.0 (c (n "eraserhead-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0xdcbzbd4mihqddgmgd8x2xhvs35g54h1ij7675zych1kjys7i19")))

