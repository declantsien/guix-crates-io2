(define-module (crates-io er as erased_set) #:use-module (crates-io))

(define-public crate-erased_set-0.6.0 (c (n "erased_set") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "02lxyawh9xx18sd7c1v6r7ca1rczi2vzcxls042sp45ir7qgfaz1") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

(define-public crate-erased_set-0.6.1 (c (n "erased_set") (v "0.6.1") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "02ra8lqyxha5cy0jkasp095a9ph6ja82j571zwkl1cx6swn4bfsh") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

(define-public crate-erased_set-0.7.0 (c (n "erased_set") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "iai") (r "^0.1.1") (d #t) (k 2)))) (h "09c1ly7d5igf7kc9bbl3j36wg1sqnmvf60fl18czi0vhawjam9bn") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

(define-public crate-erased_set-0.8.0 (c (n "erased_set") (v "0.8.0") (d (list (d (n "calliper") (r "^0.1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1i956p0ywg22akaalirsh36js7swzaf1lppry4hwpwbvdlc5sam0") (f (quote (("sync") ("send") ("default" "send" "sync")))) (r "1.60")))

