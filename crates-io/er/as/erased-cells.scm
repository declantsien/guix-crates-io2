(define-module (crates-io er as erased-cells) #:use-module (crates-io))

(define-public crate-erased-cells-0.1.0 (c (n "erased-cells") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nzxxb8w22c8cdzgshldr15p65dkayfhcm95ypjhiwp6drmrbvmb") (f (quote (("masked") ("default" "masked" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-erased-cells-0.1.1 (c (n "erased-cells") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z6zznyqrxxdiqnkanbcb1dblbskrcy8nbbjfd8kd9mdmm1i42qh") (f (quote (("masked") ("default" "masked" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

