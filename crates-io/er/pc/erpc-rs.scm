(define-module (crates-io er pc erpc-rs) #:use-module (crates-io))

(define-public crate-erpc-rs-0.1.1 (c (n "erpc-rs") (v "0.1.1") (d (list (d (n "erpc-sys") (r "^0.1") (d #t) (k 0)))) (h "1gfqapagf953licrbr4s0mwkp78p0q0a530mkz3sb22ghbmrvh7j")))

(define-public crate-erpc-rs-0.1.2 (c (n "erpc-rs") (v "0.1.2") (d (list (d (n "erpc-sys") (r "^0.1") (d #t) (k 0)))) (h "0in7xramlzjs1747lwixg3iy5abjg0n5ayj9iqkcq5p79fi6ky92")))

(define-public crate-erpc-rs-0.2.0 (c (n "erpc-rs") (v "0.2.0") (d (list (d (n "erpc-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1yf1im3qf45yl2jihi04caxa4cjfi36pzb5skmzbz8110zbi80l8")))

