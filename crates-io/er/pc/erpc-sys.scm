(define-module (crates-io er pc erpc-sys) #:use-module (crates-io))

(define-public crate-erpc-sys-0.1.1 (c (n "erpc-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dirs") (r "^2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q81x9zzgnxqnr5ah14g6q00iycxmyp6cnvmkqcrdi4f45irs97h")))

(define-public crate-erpc-sys-0.2.0 (c (n "erpc-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dirs") (r "^2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0990ppf82jb044zzl0zbiznr7vvpydzdaq2ydwfz62avqg80ihac")))

