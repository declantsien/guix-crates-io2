(define-module (crates-io er od erode) #:use-module (crates-io))

(define-public crate-erode-0.1.0 (c (n "erode") (v "0.1.0") (d (list (d (n "deno_ast") (r "^0.22.0") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.187.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ca8q70gbgpdk7afjf2lx4kbgpcwgm8rlxbmms7bngj3b1iibwkp")))

