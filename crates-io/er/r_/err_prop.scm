(define-module (crates-io er r_ err_prop) #:use-module (crates-io))

(define-public crate-err_prop-0.0.1 (c (n "err_prop") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.12") (d #t) (k 0)))) (h "15zr4n1phh82r3rjl9xvjmha6zl1hr6mjv6a7r8w3vqmmqcrwmng")))

(define-public crate-err_prop-0.0.2 (c (n "err_prop") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.12") (d #t) (k 0)))) (h "186lc7h95db9nhxqkhrff1zjfyb2mlr6lxqjajclbdgvfx5x1ksx")))

