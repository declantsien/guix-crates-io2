(define-module (crates-io er r_ err_tools) #:use-module (crates-io))

(define-public crate-err_tools-0.1.0 (c (n "err_tools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0s5lsschf0dk4vxxl3l33axzalwfwqgxg74dalylbh15nyvyp2l8")))

(define-public crate-err_tools-0.1.1 (c (n "err_tools") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "08rmajx5b7z4j12m6j2r4szv1qw9j4nqjdkqmz5vqfl2yrgxs944")))

