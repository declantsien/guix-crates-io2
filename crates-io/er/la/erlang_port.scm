(define-module (crates-io er la erlang_port) #:use-module (crates-io))

(define-public crate-erlang_port-0.1.0 (c (n "erlang_port") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_eetf") (r "^0") (d #t) (k 0)))) (h "0i34gxd7c38p5ixhalvwbgfzvsr654hd72scji8vqwvcrd9w481r")))

(define-public crate-erlang_port-0.2.0 (c (n "erlang_port") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_eetf") (r "^0") (d #t) (k 0)))) (h "046bqd8784ri1rp8pmi2fdk82pp7svrd3l9dbriq26zgacgv93jn")))

