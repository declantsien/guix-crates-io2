(define-module (crates-io er la erlang_nif-sys) #:use-module (crates-io))

(define-public crate-erlang_nif-sys-0.4.1 (c (n "erlang_nif-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0pswj6pw2s07l9jr8dsbx05d5n0yryzvgcrkpi8w29bfr8dmhlqd")))

(define-public crate-erlang_nif-sys-0.4.2 (c (n "erlang_nif-sys") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "12mbzsmzhdi9alzxisvx4pqiw0b3sksy2x8a5lvdfcx1c3ak23h9")))

(define-public crate-erlang_nif-sys-0.5.0 (c (n "erlang_nif-sys") (v "0.5.0") (h "0rhd2y2zs3hzhd6w11liy6iq06llirqpfss25xdz0z37bx4axl1r")))

(define-public crate-erlang_nif-sys-0.5.1 (c (n "erlang_nif-sys") (v "0.5.1") (h "0635zm04yxcy7m6g5ddxkclqymz6n81kipyj5s5lbqw3x4dn42zx")))

(define-public crate-erlang_nif-sys-0.5.2 (c (n "erlang_nif-sys") (v "0.5.2") (h "0prh7jq2fyardv2rngv3lwrzw7fr91akmydx974r8dk34zdzrrb9")))

(define-public crate-erlang_nif-sys-0.5.3 (c (n "erlang_nif-sys") (v "0.5.3") (h "0d3fsm0k5g7sy60j64gfa0ds4rnj3l5z1vp9z6s2dynkx6gircmq")))

(define-public crate-erlang_nif-sys-0.5.4 (c (n "erlang_nif-sys") (v "0.5.4") (h "0948kp27cswwv9csdf0jc915nm1kklx3sqjaysjsnbryhjy3hij4")))

(define-public crate-erlang_nif-sys-0.6.0 (c (n "erlang_nif-sys") (v "0.6.0") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "0bcfki49jz352in36i28m8qfb5gfq2lq5y8zbb6mm2087zj6b56p")))

(define-public crate-erlang_nif-sys-0.6.1 (c (n "erlang_nif-sys") (v "0.6.1") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "1i0sgak3x48pz2m7y59jjaids7fd6xwqm0sg8h64ysfy744wp32y")))

(define-public crate-erlang_nif-sys-0.6.2 (c (n "erlang_nif-sys") (v "0.6.2") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "0h98lcczib6v5imixkx6c0gi20g43n26mg82xpjb8lrfyccxd5b8")))

(define-public crate-erlang_nif-sys-0.6.3 (c (n "erlang_nif-sys") (v "0.6.3") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "18zahrq4h706s48afhs3whl2322smzjdbzda7ycpsy1rwdmcn118")))

(define-public crate-erlang_nif-sys-0.6.4 (c (n "erlang_nif-sys") (v "0.6.4") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "0dlmk4y6c0xrzr1xy0xpbp6aakhlbiq9gv09l2i961i25jy2viwv")))

(define-public crate-erlang_nif-sys-0.6.5 (c (n "erlang_nif-sys") (v "0.6.5") (d (list (d (n "cargo-erlangapp") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 2)))) (h "099yqcdiva8baw9vh8igz77jj1bkii0rp0sxqgv4lqnq8zlsnkdf") (y #t)))

