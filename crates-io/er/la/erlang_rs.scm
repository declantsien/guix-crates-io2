(define-module (crates-io er la erlang_rs) #:use-module (crates-io))

(define-public crate-erlang_rs-0.1.0 (c (n "erlang_rs") (v "0.1.0") (h "1y2p8f5d5sn808m8qxz31ixp7vrw1bm1r444vvn97p1ggjxqar2d") (r "1.56.1")))

(define-public crate-erlang_rs-0.2.0 (c (n "erlang_rs") (v "0.2.0") (h "04askygl6q1vina627y0zqc0k4djrpwdxvq86w1rkqizd28mlblc") (r "1.56.1")))

(define-public crate-erlang_rs-2.0.6 (c (n "erlang_rs") (v "2.0.6") (h "17g8psbijs5bc0r9vk3q3r0jx8wps476cfg5n5vnlj0vd7dknn89") (r "1.56.1")))

(define-public crate-erlang_rs-2.0.7 (c (n "erlang_rs") (v "2.0.7") (h "1b6502fjnb0c7bqiw4a4cazc2m5llny7i0pfppsgfvxh71f90srd") (r "1.56.1")))

