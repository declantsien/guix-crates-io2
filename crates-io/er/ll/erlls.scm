(define-module (crates-io er ll erlls) #:use-module (crates-io))

(define-public crate-erlls-0.0.1 (c (n "erlls") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0yvnf3c3gh6wllv34xn89yvhy45c203g3qsij0d5mnrawqz8735i")))

(define-public crate-erlls-0.0.3 (c (n "erlls") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1s600c9p11j5rq54aypd7lyx7yzxwh5vrxbkmq8a08md4xrb5glh")))

(define-public crate-erlls-0.0.4 (c (n "erlls") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vi2vbf9rnqaknkxjxn9b9zyrk05yg05mgvkvzaqca44wp8f8pyh")))

(define-public crate-erlls-0.0.5 (c (n "erlls") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "13fk4dr7q7fymlmjncbkph021h0yqi6y52vdabnxnczz96asiyhk")))

(define-public crate-erlls-0.0.7 (c (n "erlls") (v "0.0.7") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pk4ap4m9qzb08zjzbzhg5d2qrpvv8j143bi6xa1x1rnj7rww0xv")))

(define-public crate-erlls-0.0.10 (c (n "erlls") (v "0.0.10") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "10py6y9lqz130wkasal16n6ay435sib8p3fl3z4k5misvprj9if2")))

(define-public crate-erlls-0.0.11 (c (n "erlls") (v "0.0.11") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0cl3xfvnyfbkpb6gzvhabindrpd8rarv5aq2rkd6kv10hz9cwvm0")))

(define-public crate-erlls-0.0.12 (c (n "erlls") (v "0.0.12") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yn901m2kjn0xjqhr3pi1j4ai0ipy1gbn6kq4rvijr70kqk8drl7")))

(define-public crate-erlls-0.0.19 (c (n "erlls") (v "0.0.19") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "erlls_core") (r "^0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("executor"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "orfail") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kdr47fci57vq6z718kw4c0pl1myx7cgic4gapf349ac1ljqya5f")))

