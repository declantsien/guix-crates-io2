(define-module (crates-io er rn errno-dragonfly) #:use-module (crates-io))

(define-public crate-errno-dragonfly-0.1.0 (c (n "errno-dragonfly") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g5qzg3hqm48b79zddy5arhpygjl7lw14wls455p2xi9nwpc1jk4")))

(define-public crate-errno-dragonfly-0.1.1 (c (n "errno-dragonfly") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rshlc00nv45f14v2l1w0ma2nf1jg5j7q9pvw7hh018r6r73bjhl")))

(define-public crate-errno-dragonfly-0.1.2 (c (n "errno-dragonfly") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1grrmcm6q8512hkq5yzch3yv8wafflc2apbmsaabiyk44yqz2s5a")))

