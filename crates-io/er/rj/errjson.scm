(define-module (crates-io er rj errjson) #:use-module (crates-io))

(define-public crate-errjson-0.0.1 (c (n "errjson") (v "0.0.1") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18qs2fz2s2c7gz7r93vw7pwsf4gmb4dfcpif03wxiw0bhbgv4k8s")))

(define-public crate-errjson-0.0.2 (c (n "errjson") (v "0.0.2") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17352sszy3ibyzas99i4mx9fcy7nan326pkr7x7xkw3crl4aybp6")))

(define-public crate-errjson-0.0.3 (c (n "errjson") (v "0.0.3") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "jsonerr") (r "^0.0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k5rydjm2jgv2fbwhshkjf232v38hlgwskms2mf33ys21fp923ay")))

(define-public crate-errjson-0.0.4 (c (n "errjson") (v "0.0.4") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "jsonerr") (r "^0.0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vmh63w6rf3jnbm70dz7n3nd4hqvyjz6d980hf1xnnyvygjayqk2")))

(define-public crate-errjson-0.0.5 (c (n "errjson") (v "0.0.5") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "jsonerr") (r "^0.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bc9mz1hdwzk0cjjmjy2njyw6zzhw0d4id6cgnbjyp281sxy69n8")))

(define-public crate-errjson-0.0.6 (c (n "errjson") (v "0.0.6") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "jsonerr") (r "^0.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gr9jsni5zxxgn2xfx7jbx9g03ly68l1c6anda78nrgxnw8j7qvj")))

