(define-module (crates-io er ic eric) #:use-module (crates-io))

(define-public crate-eric-1.0.0 (c (n "eric") (v "1.0.0") (h "0jf6bykhs5dxf4r35zdbqp8wzp32g6vw0kij7lcdiyf3x343wzxx")))

(define-public crate-eric-1.1.0 (c (n "eric") (v "1.1.0") (h "122lmwbmxd6y047rqysjks0i1zyz1a727yj85fh007620kk1j4vn") (y #t)))

(define-public crate-eric-1.2.0 (c (n "eric") (v "1.2.0") (h "1nrzr9n9nys383r82d3wnbsrvcr6dcvppskb344arayd4bz5wjb2")))

