(define-module (crates-io er gu erguotou) #:use-module (crates-io))

(define-public crate-erguotou-0.0.1 (c (n "erguotou") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0cnf9q3ljq5y6lvnksh9pmxpvxpav6br0hzgfyll08wlx71rlvh1")))

(define-public crate-erguotou-0.0.2 (c (n "erguotou") (v "0.0.2") (d (list (d (n "bodyparser") (r "*") (d #t) (k 2)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 2)) (d (n "maplit") (r "*") (d #t) (k 2)) (d (n "persistent") (r "*") (d #t) (k 2)) (d (n "router") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dhp9ss0v60q260a9kmgznkgmgmsglywmisb7yni4nwbkb0iyb3x")))

(define-public crate-erguotou-0.0.3 (c (n "erguotou") (v "0.0.3") (d (list (d (n "bodyparser") (r "*") (d #t) (k 2)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 2)) (d (n "maplit") (r "*") (d #t) (k 2)) (d (n "persistent") (r "*") (d #t) (k 2)) (d (n "router") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)) (d (n "urlencoded") (r "*") (d #t) (k 2)))) (h "0dw50i3ah2155kh3r1zxwgagzj44m2y66zvf78nyp0zvcl5ajv5r")))

