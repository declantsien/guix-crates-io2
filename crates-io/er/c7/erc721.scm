(define-module (crates-io er c7 erc721) #:use-module (crates-io))

(define-public crate-erc721-3.0.0 (c (n "erc721") (v "3.0.0") (d (list (d (n "ink_env") (r "^3.0.0") (k 0)) (d (n "ink_lang") (r "^3.0.0") (k 0)) (d (n "ink_metadata") (r "^3.0.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ink_primitives") (r "^3.0.0") (k 0)) (d (n "ink_storage") (r "^3.0.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (o #t) (k 0)))) (h "15nzf86hxw801mpcxd6xz2wnn8b2yrvwid4vnsqx3ayc9zibcmpi") (f (quote (("std" "ink_primitives/std" "ink_metadata/std" "ink_env/std" "ink_storage/std" "ink_lang/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

