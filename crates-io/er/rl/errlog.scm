(define-module (crates-io er rl errlog) #:use-module (crates-io))

(define-public crate-errlog-0.0.0 (c (n "errlog") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)))) (h "1p33lpnhfm377qmvqj67c6468jfp4b59za004r2pikyq44jjlv27") (y #t)))

(define-public crate-errlog-0.0.1 (c (n "errlog") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)))) (h "1ii5628mxqs7wvbv8zc6wz6s4g758s1kiqchv8i4a7pr8vlgzmmz") (y #t)))

(define-public crate-errlog-0.0.2 (c (n "errlog") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)))) (h "16y5mbhpg4kw7wqd862gq26rpjpvbrnra1w71l4wvq7ckd5pnwys")))

