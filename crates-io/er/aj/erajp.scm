(define-module (crates-io er aj erajp) #:use-module (crates-io))

(define-public crate-erajp-0.1.0 (c (n "erajp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "1jmhsz0a1vryzv43ha37cw94bi1k0qq398gmj2sb9f8inv5il8mg")))

(define-public crate-erajp-0.2.0 (c (n "erajp") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "0b6bim1lgi7vd7wlh6lfnzxsrfbdz2c7kvqr4zydfhgdlwf8yc9h")))

(define-public crate-erajp-0.3.0 (c (n "erajp") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "1r385njjw4lhal5axcydmd60q26i64pbc5mxi14ipv98625xbj04")))

