(define-module (crates-io er l_ erl_nif) #:use-module (crates-io))

(define-public crate-erl_nif-0.1.0 (c (n "erl_nif") (v "0.1.0") (d (list (d (n "erl_nif_macro") (r "^0.1") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0x2hrrvn3p0k7vzwk0ydp7xbcw9zwa67yqgys5d00v6g7xq7n9hl") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-erl_nif-0.2.0 (c (n "erl_nif") (v "0.2.0") (d (list (d (n "erl_nif_macro") (r "^0.2") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "16qc0j4n48chpx9npap1i128b2kbib6hhd407sj6mzg9x5w0vi5p") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-erl_nif-0.3.0 (c (n "erl_nif") (v "0.3.0") (d (list (d (n "erl_nif_macro") (r "^0.3") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1fnw181d5ka58nfabv80k6pmmnzj8rm61xgi9rfrr75ylhkdyyvd") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-erl_nif-0.3.1 (c (n "erl_nif") (v "0.3.1") (d (list (d (n "erl_nif_macro") (r "^0.3") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0wg96msp1l2p5sfh68phvl15kxrldbi6h5inwa237blw2hynb059") (f (quote (("serde_1" "serde") ("default"))))))

(define-public crate-erl_nif-0.4.0 (c (n "erl_nif") (v "0.4.0") (d (list (d (n "erl_nif_macro") (r "^0.4") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1isciv6ngvbgmpfpm56ri4vv2ppzsjihx72vq8d0lmr430fjcjwl")))

(define-public crate-erl_nif-0.5.0 (c (n "erl_nif") (v "0.5.0") (d (list (d (n "erl_nif_macro") (r "^0.5") (d #t) (k 0)) (d (n "erl_nif_sys") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ay4qbm7lcknn8r94awjzmckl6fcdwvqsimi90vjjgm5dz5y7f0h")))

