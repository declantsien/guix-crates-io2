(define-module (crates-io er l_ erl_tokenize) #:use-module (crates-io))

(define-public crate-erl_tokenize-0.1.0 (c (n "erl_tokenize") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1whrjpkr27sngh47azlmmg1gbwy1h5wid75xqizfxc69vr9griwc")))

(define-public crate-erl_tokenize-0.1.1 (c (n "erl_tokenize") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "0d14jckkmijkqa0zj4mb0pa4gk4hdhmn2l1h9c6dvc0ghz4qy5kw")))

(define-public crate-erl_tokenize-0.1.2 (c (n "erl_tokenize") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "10y4la35zmjaail5s28didir71gz0cm8h8qs6hvspq0yakinkiqx")))

(define-public crate-erl_tokenize-0.1.3 (c (n "erl_tokenize") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1kqs31i77zhmzzl66j6k16r4wf5wcscvv9zyf3r46lkpqdkyzjcm")))

(define-public crate-erl_tokenize-0.1.4 (c (n "erl_tokenize") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "0a43r0gsj2b4fiwsyhpbyvgw8s93xhkafx7f3ix3s0mvb3nwrby1")))

(define-public crate-erl_tokenize-0.2.0 (c (n "erl_tokenize") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "135irghwkh9mi9ij1vkqsfm3ggqdfvjqyj4ih9zm5vnaq8wgmcql")))

(define-public crate-erl_tokenize-0.3.0 (c (n "erl_tokenize") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "022j7bi68qqxpy59w9kvcx73driazpjqb2vn3fnyczqbzp1ycwky")))

(define-public crate-erl_tokenize-0.3.1 (c (n "erl_tokenize") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1smqi4vmvikv654sa23952j2ccv1qg0vp9n2v5n8ykclfmbrnxnv")))

(define-public crate-erl_tokenize-0.3.2 (c (n "erl_tokenize") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1rsfr2wd48ify4pfnkd8djg6kk777hzhbablig35ipsx8zcwxpdp")))

(define-public crate-erl_tokenize-0.3.3 (c (n "erl_tokenize") (v "0.3.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1fr6hkav92y3ilq4zz0j5hp4brlv9ahbvcbiilbjnsm0kngw8xic")))

(define-public crate-erl_tokenize-0.3.4 (c (n "erl_tokenize") (v "0.3.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "0pbc62vi27iqvx7w4c9miv4y54v9dbdq6hqz0nnm5aj4hipn2wlk")))

(define-public crate-erl_tokenize-0.3.5 (c (n "erl_tokenize") (v "0.3.5") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0ypfnymdx24ij2ggga1w8l81p3m95bh2gxkig43vymgg8qhflyv3")))

(define-public crate-erl_tokenize-0.3.6 (c (n "erl_tokenize") (v "0.3.6") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "09pqs1i3wh9h3qdkdpnf464snmlv84b5227mnlgpp56pbc5z9qsl")))

(define-public crate-erl_tokenize-0.3.7 (c (n "erl_tokenize") (v "0.3.7") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "07qa35jps9lkx8ri9nahx4p0zvs2rcf9lj86mhx3amvp66241m91")))

(define-public crate-erl_tokenize-0.3.8 (c (n "erl_tokenize") (v "0.3.8") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0xx01x0yjnhynl842byns2ai7z288qfscxs6nvbq8j6nh20dv1g0")))

(define-public crate-erl_tokenize-0.3.9 (c (n "erl_tokenize") (v "0.3.9") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0dgdl5jnlhxxnkiyv7bddmmxc2q7vq5l5qzl1jinn25j96ysmhfy")))

(define-public crate-erl_tokenize-0.4.0 (c (n "erl_tokenize") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kz72jl5f8x6qh7gwh3rk85056rr7mc9d8pnqnk3gsd3lyqr82ca")))

(define-public crate-erl_tokenize-0.4.1 (c (n "erl_tokenize") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jdhjz1pmyd9f7ggi0049hamh84w834yim13mkhxlia5vh9hhc76")))

(define-public crate-erl_tokenize-0.4.2 (c (n "erl_tokenize") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1n2mhxzqq8wsd9bc80alskrkv4rm7q9id6gznk9g01fw0avrnrhl")))

(define-public crate-erl_tokenize-0.4.3 (c (n "erl_tokenize") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0iy12x7d9xpj535zfgw0m44q1dwc0rn19v0dlf008ybv4ry2zqx2")))

(define-public crate-erl_tokenize-0.4.4 (c (n "erl_tokenize") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gscgc0y8s6y6hjmg32agajc3f7v085rrs7mmjp1y7hkxw8jj776")))

(define-public crate-erl_tokenize-0.4.5 (c (n "erl_tokenize") (v "0.4.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cd6bbikps8vlb9b8gg1ph5nffc7dgjqdpj80sjhrqisdr742z4n")))

(define-public crate-erl_tokenize-0.4.6 (c (n "erl_tokenize") (v "0.4.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18kkx0bs31wbg01grjr02zsmdfgm4k7rvz90idwzqpy0xqkkq1cq")))

(define-public crate-erl_tokenize-0.4.7 (c (n "erl_tokenize") (v "0.4.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v2zx6k0c46899yakbmailky7kj89vxc3n3ahh3c54kmh9ygsz38")))

(define-public crate-erl_tokenize-0.5.0 (c (n "erl_tokenize") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wmnmfd0d2w3d1a5gl22zlj60lzr473c1sb2k9m8d77v6bnxriaa")))

(define-public crate-erl_tokenize-0.5.1 (c (n "erl_tokenize") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cnriid6ajshq7yh4gml43g5582wj91n94x4g055cpmwvrj6l55j")))

(define-public crate-erl_tokenize-0.6.0 (c (n "erl_tokenize") (v "0.6.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "orfail") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15vxmvk3gs4ginl9h3vh0knf02sk9x9rx7rjnjc1jjd7bc9w2km2")))

(define-public crate-erl_tokenize-0.6.1 (c (n "erl_tokenize") (v "0.6.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "orfail") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12ymkm63rwh9shp68ddqki21yd0qhlsnl6d0slzmypc7hg2n2dn8")))

