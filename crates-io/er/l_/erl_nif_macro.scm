(define-module (crates-io er l_ erl_nif_macro) #:use-module (crates-io))

(define-public crate-erl_nif_macro-0.1.0 (c (n "erl_nif_macro") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fgfi22if4m87757xjg8rylfljr710vycvwip4rblw8jzx7bxhdl")))

(define-public crate-erl_nif_macro-0.2.0 (c (n "erl_nif_macro") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1058akka1pdpipwqcnq94slzdbmvdl0q0p8gar2g8niff6mvq6v4")))

(define-public crate-erl_nif_macro-0.3.0 (c (n "erl_nif_macro") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06vzfl3j0pv5k033zqk93m48nnkfl7x52d6lj0hcpgn853f4f82r")))

(define-public crate-erl_nif_macro-0.3.1 (c (n "erl_nif_macro") (v "0.3.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dn949bk7zy2ppggydpsqnhdqysg6rmc8wsais4pmm1cz2xl29mi")))

(define-public crate-erl_nif_macro-0.4.0 (c (n "erl_nif_macro") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18fk4mxrgci9mz6rpsbcfinn1ygvvzgp6ba49lhyvyn7kfwanrfr")))

(define-public crate-erl_nif_macro-0.5.0 (c (n "erl_nif_macro") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ab44x8f2l6y0vmlvjyssw1zswrcvvbc9qh4qpbnzhrldqq8c955")))

