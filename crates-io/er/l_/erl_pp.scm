(define-module (crates-io er l_ erl_pp) #:use-module (crates-io))

(define-public crate-erl_pp-0.1.0 (c (n "erl_pp") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0s7s9yn26xscdpi677bychh3ip1a62kz2sxsdpbjd6gyiv0ffvpb")))

(define-public crate-erl_pp-0.1.1 (c (n "erl_pp") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1n5n7fa703hdfsf6wjwp2w1pd28b28r5gbbllx2x5yf5l2nbikba")))

(define-public crate-erl_pp-0.1.2 (c (n "erl_pp") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "16jrw4wrm6gb8qxnjk76ghnvx3hrmfz0vlihk6fyd2sbvcimj615")))

(define-public crate-erl_pp-0.1.3 (c (n "erl_pp") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "16hyx5fmpm3zb793ha9frkbyj81alszlx9vfyf8vxfnis3ng28d0")))

(define-public crate-erl_pp-0.1.4 (c (n "erl_pp") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1zrdd7m3pazr3a9ry462d34mbj88vlhz1ks4ij2f5dpsc307sibb")))

(define-public crate-erl_pp-0.2.0 (c (n "erl_pp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ipcbczhc1vs03gq97yydi9avx55lcik4kiislxj2yccxmap1a7j")))

