(define-module (crates-io er l_ erl_parse) #:use-module (crates-io))

(define-public crate-erl_parse-0.0.1 (c (n "erl_parse") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "02pq504p6j68iwh1jwp286gw16v7lm0f2vs86ajl5gq15i0kg202")))

(define-public crate-erl_parse-0.0.2 (c (n "erl_parse") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_tokenize") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "1cpwpyymy46gbi9aq51njp1shxc1r9nwdhrgv4fbpcrmm0c825zj")))

(define-public crate-erl_parse-0.0.4 (c (n "erl_parse") (v "0.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_pp") (r "^0.1") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "19vprhvq91a82zsqr5g5ipkq3qcvkphici6wm17nafqvmv0a23nw")))

(define-public crate-erl_parse-0.0.5 (c (n "erl_parse") (v "0.0.5") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_pp") (r "^0.1") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1baq8xnav8am827fhiapgx2gc93yifcxnzazi546c8iqivargxl9")))

(define-public crate-erl_parse-0.0.6 (c (n "erl_parse") (v "0.0.6") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_pp") (r "^0.1") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0si47qpwq1mnqqdqi7l0p2imyhrdyp4q37a4avyfb3b05brsa7i9")))

(define-public crate-erl_parse-0.0.7 (c (n "erl_parse") (v "0.0.7") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_pp") (r "^0.1") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1h08lcdd3rl73wf48slz8kr14din9dn5z4jksxqqqlv2hpnnapxa")))

(define-public crate-erl_parse-0.0.8 (c (n "erl_parse") (v "0.0.8") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "erl_pp") (r "^0.1") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "09jrjmfaidf81jglh5c2msj5q2zs9bfnzw44i90hcnxy6ncpq40d")))

