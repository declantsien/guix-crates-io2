(define-module (crates-io er l_ erl_ast) #:use-module (crates-io))

(define-public crate-erl_ast-0.0.1 (c (n "erl_ast") (v "0.0.1") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0y727f7lwda4p7m6w0ffwqf3yk47msw42n2cjbvpx5v5warwrzsz")))

(define-public crate-erl_ast-0.0.2 (c (n "erl_ast") (v "0.0.2") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0g84y1h12f4prmq4hkbnfdmhv3v21imr055x8nqy640n2yjmf0jy")))

(define-public crate-erl_ast-0.0.3 (c (n "erl_ast") (v "0.0.3") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1wpx4fnnja2gl0bbwf6hxy3p0w7prd9fqi617nyz2vy874bp5g8m")))

(define-public crate-erl_ast-0.0.4 (c (n "erl_ast") (v "0.0.4") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "09ibchdn3shrybcw2n5c9yiwav57bs0b8srxwjdx8vzvikjrvfyy")))

(define-public crate-erl_ast-0.0.5 (c (n "erl_ast") (v "0.0.5") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1dh1rl9ssi4bbhjy49g9kd9hyqqad4jrh51g94waj0v3qp3q4wlv")))

(define-public crate-erl_ast-0.0.6 (c (n "erl_ast") (v "0.0.6") (d (list (d (n "beam_file") (r "^0.2") (d #t) (k 0)) (d (n "eetf") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0b5kzmybs97x4zvvv0wq5b2sv4mckqq6p3zzn87rcqk2llb0nm07")))

(define-public crate-erl_ast-0.1.0 (c (n "erl_ast") (v "0.1.0") (d (list (d (n "beam_file") (r "^0.2.4") (d #t) (k 0)) (d (n "eetf") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pz07i368h0llrc59jc6m66g3awma2vcrdhciw6g2f96nanlrshm")))

(define-public crate-erl_ast-0.1.1 (c (n "erl_ast") (v "0.1.1") (d (list (d (n "beam_file") (r "^0.2.4") (d #t) (k 0)) (d (n "eetf") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ag6nd4shfy8hi3lm7jywdw5iirhpyxmfs95wm66a5b77597dg1f")))

