(define-module (crates-io er l_ erl_nif_sys) #:use-module (crates-io))

(define-public crate-erl_nif_sys-0.1.0 (c (n "erl_nif_sys") (v "0.1.0") (d (list (d (n "erl_nif_macro") (r "^0.1.0") (d #t) (k 0)))) (h "05s3akw7pmkbavhj3vn69wy6y1wwdwafbhpf3a93h8samzx0yj32")))

(define-public crate-erl_nif_sys-0.2.0 (c (n "erl_nif_sys") (v "0.2.0") (d (list (d (n "erl_nif_macro") (r "^0.2") (d #t) (k 0)))) (h "0grb6bwkbv4g0cmpn4jhz63szzq3v27ipfgqmv0pz0fhd6jq649h")))

(define-public crate-erl_nif_sys-0.3.0 (c (n "erl_nif_sys") (v "0.3.0") (d (list (d (n "erl_nif_macro") (r "^0.3") (d #t) (k 0)))) (h "1kiay3c7fb5b52rrb4q5440ah27kniiy022dp95j930v0ksr9q4v")))

(define-public crate-erl_nif_sys-0.3.1 (c (n "erl_nif_sys") (v "0.3.1") (d (list (d (n "erl_nif_macro") (r "^0.3") (d #t) (k 0)))) (h "0w2w8wgij079i4wiapcdxrhxlqyr9fm5k6qk79gq3llcnhr4aa3m")))

(define-public crate-erl_nif_sys-0.4.0 (c (n "erl_nif_sys") (v "0.4.0") (d (list (d (n "erl_nif_macro") (r "^0.4") (d #t) (k 0)))) (h "02a2i8fc38azhfnmf1cdr33a6qrdxdfyy215ysh32s0ll73bv35l")))

(define-public crate-erl_nif_sys-0.5.0 (c (n "erl_nif_sys") (v "0.5.0") (d (list (d (n "erl_nif_macro") (r "^0.5") (d #t) (k 0)))) (h "02i4h5mbnhsg688rai19c9hvz2ia5jp1j9kdw1bps619p3sjs25x")))

