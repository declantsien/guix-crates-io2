(define-module (crates-io er ud erudite) #:use-module (crates-io))

(define-public crate-erudite-0.1.0 (c (n "erudite") (v "0.1.0") (h "0wkdcgc0dkcdysfkfcxzsm9rjz1sqkdz2fdz34ki24ssbf4jyya0") (y #t)))

(define-public crate-erudite-0.1.1 (c (n "erudite") (v "0.1.1") (h "0xcfxk4m5lk3anmsya1dvzxbchj9i3kvdf5v7vwvjiqwakbida32") (y #t)))

(define-public crate-erudite-0.0.0 (c (n "erudite") (v "0.0.0") (h "1f03hfz3cgzcg19qw08nil8bwipds3nrqkjq6hax03axb8cnfbwn")))

