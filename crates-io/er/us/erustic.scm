(define-module (crates-io er us erustic) #:use-module (crates-io))

(define-public crate-erustic-0.1.0 (c (n "erustic") (v "0.1.0") (h "0sp7wannnd8b2higm4si0dc962v2qmm5ki6k60l2vz51wfv5z3m7")))

(define-public crate-erustic-0.1.1 (c (n "erustic") (v "0.1.1") (h "1gpg96ddzr3bqj37qgq8y2g3g3dqmga890v92yn2r036xafblllw")))

