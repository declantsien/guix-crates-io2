(define-module (crates-io er ga ergast_rust) #:use-module (crates-io))

(define-public crate-ergast_rust-0.1.0 (c (n "ergast_rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1ci2plk86v7yag5pcccaxbpssbcf5gmf8mdnzscpdcz8jp86l1gq")))

