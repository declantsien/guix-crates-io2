(define-module (crates-io er is eris-rs) #:use-module (crates-io))

(define-public crate-eris-rs-0.1.0 (c (n "eris-rs") (v "0.1.0") (d (list (d (n "base32") (r "~0.4.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "chacha20") (r "~0.8.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 2)))) (h "0ldhrw7clp85dr72y11x7gyz83yqkhpzry4z1785khhnp32ly3s7") (y #t)))

(define-public crate-eris-rs-0.1.1 (c (n "eris-rs") (v "0.1.1") (d (list (d (n "base32") (r "~0.4.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "chacha20") (r "~0.8.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 2)))) (h "10mnwl0fjjm4k5nqkf6j79csljzx6bch5pc5khnv5fa93l6cc5pa")))

(define-public crate-eris-rs-1.0.0 (c (n "eris-rs") (v "1.0.0") (d (list (d (n "base32") (r "~0.4.0") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "chacha20") (r "~0.9.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 2)))) (h "1fnji5kzzqc7c06v7rpadi2wxf0byvks88p04gq35pkav22zvmxn")))

