(define-module (crates-io er st erst-derive) #:use-module (crates-io))

(define-public crate-erst-derive-0.2.0 (c (n "erst-derive") (v "0.2.0") (d (list (d (n "erst-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi63ryjq3rf6d4kxadcdwrxz6xh6pbd05cd23sqqxvyjq3k8ws4")))

(define-public crate-erst-derive-0.2.2 (c (n "erst-derive") (v "0.2.2") (d (list (d (n "erst-shared") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0kqmlgglbdb1qlakjkalmmalaaf3sdj3m7jlfa0fq9r4znjfgwjs") (f (quote (("dynamic") ("default"))))))

(define-public crate-erst-derive-0.2.3 (c (n "erst-derive") (v "0.2.3") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07h74kb2gbbsf9l0k03jzswymccprls244gih5z3yj8lqsykyxv0") (f (quote (("dynamic") ("default"))))))

(define-public crate-erst-derive-0.2.5 (c (n "erst-derive") (v "0.2.5") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14q494pn0hkcc1ayvprw9i3w7r8s2qycm1pddvamlg8w29mv358g") (f (quote (("dynamic") ("default"))))))

(define-public crate-erst-derive-0.2.6 (c (n "erst-derive") (v "0.2.6") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wwras3gpdjb5c7ar6fykgh03nl9z70k6k0gakggzgnzwa6sghpv") (f (quote (("dynamic") ("default"))))))

(define-public crate-erst-derive-0.3.0 (c (n "erst-derive") (v "0.3.0") (d (list (d (n "erst-shared") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12zyjbysda4f874ijva7yixaphh1ylrb4wn2rah0fn726n89zvd0") (f (quote (("dynamic" "erst-shared/dynamic") ("default"))))))

