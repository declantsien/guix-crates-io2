(define-module (crates-io er st erst) #:use-module (crates-io))

(define-public crate-erst-0.1.0 (c (n "erst") (v "0.1.0") (h "0gs1bbv48757vvdvspwhsr2357sksk5wlag66crbdnlgy8lqq1j6")))

(define-public crate-erst-0.2.0 (c (n "erst") (v "0.2.0") (d (list (d (n "erst-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "erst-shared") (r "^0.2.0") (d #t) (k 0)))) (h "1y15whyklsxbxpwy5plx3g4bkb8bidb7saljd86hbcwkn0al9iak")))

(define-public crate-erst-0.2.2 (c (n "erst") (v "0.2.2") (d (list (d (n "erst-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "erst-shared") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0bwm6irc5szfsrill0l0h9yfmn2dgr295h4zjnniwbhgq9wifcp1") (f (quote (("dynamic" "erst-derive/dynamic" "erst-shared/dynamic" "lazy_static") ("default"))))))

(define-public crate-erst-0.2.3 (c (n "erst") (v "0.2.3") (d (list (d (n "erst-derive") (r "^0.2") (d #t) (k 0)) (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1xysnzyvl5lbjh9sf30dgc9anp95wbz2acdvswx1c5pzhh10b4bk") (f (quote (("dynamic" "erst-derive/dynamic" "erst-shared/dynamic" "lazy_static") ("default"))))))

(define-public crate-erst-0.2.5 (c (n "erst") (v "0.2.5") (d (list (d (n "erst-derive") (r "^0.2") (d #t) (k 0)) (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1ymyq2rqwybq21yzk1r7if8m6fmaly0avbl8d3cx7rqgzxw8a548") (f (quote (("dynamic" "erst-derive/dynamic" "erst-shared/dynamic" "lazy_static") ("default"))))))

(define-public crate-erst-0.2.6 (c (n "erst") (v "0.2.6") (d (list (d (n "erst-derive") (r "^0.2") (d #t) (k 0)) (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0q2ilns0ny75920ynvdg57zkbyzxppwwid345h4kd4lvq0nhga8h") (f (quote (("dynamic" "erst-derive/dynamic" "erst-shared/dynamic" "lazy_static") ("default"))))))

(define-public crate-erst-0.3.0 (c (n "erst") (v "0.3.0") (d (list (d (n "erst-derive") (r "^0.3") (d #t) (k 0)) (d (n "erst-shared") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "18kfj40k2pqss3v73fkm2213d0j16c984snj0xzx4jdh8bzamcj0") (f (quote (("dynamic" "erst-derive/dynamic" "erst-shared/dynamic" "lazy_static") ("default"))))))

