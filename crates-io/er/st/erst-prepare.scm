(define-module (crates-io er st erst-prepare) #:use-module (crates-io))

(define-public crate-erst-prepare-0.2.2 (c (n "erst-prepare") (v "0.2.2") (d (list (d (n "erst-shared") (r "^0.2.2") (d #t) (k 0)))) (h "0x8h4xpwknz8z46jn519q0l36p9a55d292hl1dznfv3a8qq4l9qa") (f (quote (("default" "erst-shared/dynamic"))))))

(define-public crate-erst-prepare-0.2.3 (c (n "erst-prepare") (v "0.2.3") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1d6wy204z5anmrzfnj4fc1vbvyh8f0g86a0bqfdj1mvprn269yar") (f (quote (("default" "erst-shared/dynamic"))))))

(define-public crate-erst-prepare-0.2.5 (c (n "erst-prepare") (v "0.2.5") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1l7cbadzf1vak21y2zng9db74wlslkn2l9sc7yh3866c5fnbdvf0") (f (quote (("default" "erst-shared/dynamic"))))))

(define-public crate-erst-prepare-0.2.6 (c (n "erst-prepare") (v "0.2.6") (d (list (d (n "erst-shared") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1ml6ffbb8p84rw2vcvjz48p9f6fbi92l01p56c294r3cgf1sxh8b") (f (quote (("default" "erst-shared/dynamic"))))))

