(define-module (crates-io er go ergo_config) #:use-module (crates-io))

(define-public crate-ergo_config-0.0.1 (c (n "ergo_config") (v "0.0.1") (d (list (d (n "configure") (r "^0.1.1") (d #t) (k 0)) (d (n "ron") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "10imljbjiq74chf0m45v5hb3sxyak5kc2jw1pjwwc89m4m3w7rgz")))

