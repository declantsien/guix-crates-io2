(define-module (crates-io er go ergo) #:use-module (crates-io))

(define-public crate-ergo-0.1.0 (c (n "ergo") (v "0.1.0") (h "1jfzmxfjlcbqk5s6bfw4f7w8n05ydmimwl02w0kdkphvvhlxdyn3") (y #t)))

(define-public crate-ergo-0.1.1 (c (n "ergo") (v "0.1.1") (h "0ns2wyli31n8hgrkqqmw4cpwa14z2hfh08l7rk6r0z35vlgiq0kq") (y #t)))

(define-public crate-ergo-0.1.2 (c (n "ergo") (v "0.1.2") (h "1sampwyi46kxknnlfzchlb568x0fkjhirrznd9xvyzsq24vr3qrn") (y #t)))

(define-public crate-ergo-0.0.1 (c (n "ergo") (v "0.0.1") (d (list (d (n "ergo_config") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_fs") (r "^0.1.5") (d #t) (k 0)) (d (n "ergo_std") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "ergo_sys") (r "^0.0.1") (d #t) (k 0)))) (h "0wdr0qc1x7crrgsj9yg09sw6qylbahfxnjicd6yk18yjlij7r4m3")))

(define-public crate-ergo-0.0.2 (c (n "ergo") (v "0.0.2") (d (list (d (n "ergo_config") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_fs") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo_std") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "ergo_sys") (r "^0.0.1") (d #t) (k 0)))) (h "01hf2dpi4yd7h8dg2ys6vf49zgvprw0i7p9w4wf3wnvzl2c2ggal")))

(define-public crate-ergo-0.0.3 (c (n "ergo") (v "0.0.3") (d (list (d (n "ergo_config") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_fs") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo_std") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "ergo_sys") (r "^0.0.1") (d #t) (k 0)))) (h "18fkzvnk4h0jmppa6r11h5093yvqmzy757y3ykiv90kd9ypd2jbb")))

(define-public crate-ergo-0.0.4 (c (n "ergo") (v "0.0.4") (d (list (d (n "ergo_config") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_fs") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo_std") (r "^0.0.3") (d #t) (k 0)) (d (n "ergo_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "ergo_sys") (r "^0.0.1") (d #t) (k 0)))) (h "1g0k9qmhpbg5782jxwf13nh39bssv40mn96cglq4nyqxfvmqmrv1")))

(define-public crate-ergo-0.0.5 (c (n "ergo") (v "0.0.5") (d (list (d (n "ergo_config") (r "^0.0.1") (d #t) (k 0)) (d (n "ergo_fs") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo_std") (r "^0.0.3") (d #t) (k 0)) (d (n "ergo_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "ergo_sys") (r "^0.0.1") (d #t) (k 0)))) (h "0nkd3asm9x8y8a9q5j7ihqd6pdl3f7c8bibfbk8a1l9cccmxkkih")))

