(define-module (crates-io er go ergo-lib-c) #:use-module (crates-io))

(define-public crate-ergo-lib-c-0.1.0 (c (n "ergo-lib-c") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.14") (d #t) (k 1)) (d (n "ergo-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ffi_helpers") (r "^0.2") (d #t) (k 0)))) (h "0nlzigjligzw36f9i5bl8sq1g45kfjcnqfxg2rb25bqfvxvj6hll")))

(define-public crate-ergo-lib-c-0.3.0 (c (n "ergo-lib-c") (v "0.3.0") (d (list (d (n "ergo-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.3.0") (d #t) (k 0)))) (h "02bl0x4wnrrm8d8y7xnsibw1dxk77vjz4x5k3yhr80fvbbjyann4")))

(define-public crate-ergo-lib-c-0.4.0 (c (n "ergo-lib-c") (v "0.4.0") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)))) (h "0zvfqmspk958l6j9g29bhmvdy6ahjmkm7zg3j42g94xjf9hr22lx")))

(define-public crate-ergo-lib-c-0.4.1 (c (n "ergo-lib-c") (v "0.4.1") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)))) (h "0n35prvr3kkpsbgg3ncja1nwbw66g0hrry6ixcn12lxcrlb34ng3")))

(define-public crate-ergo-lib-c-0.4.2 (c (n "ergo-lib-c") (v "0.4.2") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)))) (h "14ria10syqs31bmj5qfxyvjzzpd7a5vyrw86ip1lgn5pcyxcrz8s")))

(define-public crate-ergo-lib-c-0.4.3 (c (n "ergo-lib-c") (v "0.4.3") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)))) (h "0ngad3ypcj3iypl4vy7kkx7dabf24rkzm94irs2xxyddkrrz5c6b")))

(define-public crate-ergo-lib-c-0.4.4 (c (n "ergo-lib-c") (v "0.4.4") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)))) (h "0iwvigh91l607dx31kpvp4vcbqf31v77hyl56k1g17xyb514dyd3")))

(define-public crate-ergo-lib-c-0.5.0 (c (n "ergo-lib-c") (v "0.5.0") (d (list (d (n "ergo-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.5.0") (d #t) (k 0)))) (h "1sjlckyafjc8fjd8xs1dldmscindb32izi9sr0xdq14pxkb71yim")))

(define-public crate-ergo-lib-c-0.5.1 (c (n "ergo-lib-c") (v "0.5.1") (d (list (d (n "ergo-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.5.0") (d #t) (k 0)))) (h "18k6v54js60ibgd9faj1ixhs07frzm5ixp5xv0q1lm1lyxb9srhk")))

(define-public crate-ergo-lib-c-0.7.0 (c (n "ergo-lib-c") (v "0.7.0") (d (list (d (n "ergo-lib") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.7.0") (d #t) (k 0)))) (h "0lrz5akv5p9xx5rb36n7pvah0svizkwp44037q36n7dl9jqnhkd3")))

(define-public crate-ergo-lib-c-0.8.0 (c (n "ergo-lib-c") (v "0.8.0") (d (list (d (n "ergo-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.8.0") (d #t) (k 0)))) (h "1vrjhb9s5ijz1l0qj9c885rzq1x2lfx62xaqjzvvak2n32r96k08")))

(define-public crate-ergo-lib-c-0.9.0 (c (n "ergo-lib-c") (v "0.9.0") (d (list (d (n "ergo-lib") (r "^0.9.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.9.0") (d #t) (k 0)))) (h "0w3f1z6gxhnmyjrwp61rvmk8mcydxn2abzcr6p6zip8i2siwa5y2")))

(define-public crate-ergo-lib-c-0.10.0 (c (n "ergo-lib-c") (v "0.10.0") (d (list (d (n "ergo-lib") (r "^0.10.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.10.0") (d #t) (k 0)))) (h "0ya62shd3jgj2iagr566g1i0a3qp8lz5n9pzph0vwdwqk9cx9x5z")))

(define-public crate-ergo-lib-c-0.11.0 (c (n "ergo-lib-c") (v "0.11.0") (d (list (d (n "ergo-lib") (r "^0.11.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.11.0") (d #t) (k 0)))) (h "0am0m12nv119gs4bxd52np5mq3jl8h5x0mkmqjf50q15qm3dccdz")))

(define-public crate-ergo-lib-c-0.12.0 (c (n "ergo-lib-c") (v "0.12.0") (d (list (d (n "ergo-lib") (r "^0.12.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.12.0") (d #t) (k 0)))) (h "16vv6gyyd6zn0ffi98hjm2jkdm2bd67qgndvrykq0f8jzbf83rah")))

(define-public crate-ergo-lib-c-0.13.0 (c (n "ergo-lib-c") (v "0.13.0") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)))) (h "1vdvcrddh104vl1v0hl524xfd8va0ra0r4wrdaab8xfgwgcvyrmb")))

(define-public crate-ergo-lib-c-0.13.1 (c (n "ergo-lib-c") (v "0.13.1") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)))) (h "1qrgfn6hv1al0f05b9favxrg0vda35xazg7sw3ypg7frkzd0096g")))

(define-public crate-ergo-lib-c-0.13.2 (c (n "ergo-lib-c") (v "0.13.2") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)))) (h "10il4hfmzff2nbfjm39hj7dhd1wjw8rpjg3kzdb0x2a620srk2qs")))

(define-public crate-ergo-lib-c-0.13.3 (c (n "ergo-lib-c") (v "0.13.3") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)))) (h "1lv08cxhi56nrdnlxnz7300c9035fv3rbjzgwh5nm4005bqyhnlv")))

(define-public crate-ergo-lib-c-0.14.0 (c (n "ergo-lib-c") (v "0.14.0") (d (list (d (n "ergo-lib") (r "^0.14.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.14.0") (d #t) (k 0)))) (h "0ybgwzjvw8g3ihwhbj4h8hhmr666cnz821yiqwsa0s2917valjv8")))

(define-public crate-ergo-lib-c-0.15.0 (c (n "ergo-lib-c") (v "0.15.0") (d (list (d (n "ergo-lib-c-core") (r "^0.15.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0i64mc6a33xagqh9w09dh1gpp5wx785c7xwf1bza2bs4srjj88ff")))

(define-public crate-ergo-lib-c-0.16.0 (c (n "ergo-lib-c") (v "0.16.0") (d (list (d (n "ergo-lib-c-core") (r "^0.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0y0wkmr3v8hghpq5sd9cc5iy7277dn8v5jqp900kj7zj4qpag42k") (f (quote (("rest" "ergo-lib-c-core/rest") ("default"))))))

(define-public crate-ergo-lib-c-0.16.1 (c (n "ergo-lib-c") (v "0.16.1") (d (list (d (n "ergo-lib-c-core") (r "^0.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "00jwfwwg7ks8gq5j7lb7p4pvrraq0yn3ih3zsx4b8llqkncqi2hm") (f (quote (("rest" "ergo-lib-c-core/rest") ("default"))))))

(define-public crate-ergo-lib-c-0.17.0 (c (n "ergo-lib-c") (v "0.17.0") (d (list (d (n "ergo-lib-c-core") (r "^0.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1aik4rqsxlfa0r49l6c39vx3ydgfb4ws8ck4945lz976ls48qldc") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.18.0 (c (n "ergo-lib-c") (v "0.18.0") (d (list (d (n "ergo-lib-c-core") (r "^0.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0nbl0sdm5knz5yaaary27jmg9j4yyssmf3a3ljlj5jf0305h0m0m") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.19.0 (c (n "ergo-lib-c") (v "0.19.0") (d (list (d (n "ergo-lib-c-core") (r "^0.19.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "18krhp126livfzg6d6b5fi3ky75z05vy0104527s5mzkyyfz39xj") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.20.0 (c (n "ergo-lib-c") (v "0.20.0") (d (list (d (n "ergo-lib-c-core") (r "^0.20.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0b0wfyg3m16f3h4ajhbgw6xz259rhwwh8vyn165hz9j1kvvjx4hl") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.21.0 (c (n "ergo-lib-c") (v "0.21.0") (d (list (d (n "ergo-lib-c-core") (r "^0.21.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1700d7x205n6lya80fkp6mfbnygz2g8v6197r865w0pp2v1h6phd") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.21.1 (c (n "ergo-lib-c") (v "0.21.1") (d (list (d (n "ergo-lib-c-core") (r "^0.21.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1any4w7c7sh4j3xbk025f63d6kjh50q7z669pm88l90q51752824") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.22.0 (c (n "ergo-lib-c") (v "0.22.0") (d (list (d (n "ergo-lib-c-core") (r "^0.22.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1v6zh0vg581jj7gns0yjlfq5gzaf6sd3shyjc89fwl8brb5dq2i4") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.23.0 (c (n "ergo-lib-c") (v "0.23.0") (d (list (d (n "ergo-lib-c-core") (r "^0.23.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1i41aq54gc4cdkj7a853cg8607ia35cbiqpyl0sxpsql0klzhwzb") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.24.0 (c (n "ergo-lib-c") (v "0.24.0") (d (list (d (n "ergo-lib-c-core") (r "^0.24.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0crqywrs037i991wg3lxckgc9zswx9jjwnfgksb5hd2972wpizlm") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.24.1 (c (n "ergo-lib-c") (v "0.24.1") (d (list (d (n "ergo-lib-c-core") (r "^0.24.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0w3vzihimj12zsx9456p8slkkb9bcglzdphhp76dqn5r9xlrfqyd") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.25.0 (c (n "ergo-lib-c") (v "0.25.0") (d (list (d (n "ergo-lib-c-core") (r "^0.25.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1155msqcrnids9fdw6cf5cbylqmxs223fs3sjsix96qprdg9qr16") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.26.0 (c (n "ergo-lib-c") (v "0.26.0") (d (list (d (n "ergo-lib-c-core") (r "^0.26.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0kw65kr7lkbdk61gcf3p9plxs2hshjym0f9llzja59rdc14s2a2k") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

(define-public crate-ergo-lib-c-0.27.1 (c (n "ergo-lib-c") (v "0.27.1") (d (list (d (n "ergo-lib-c-core") (r "^0.27.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1snds0jgj1jq645r4kf0z1rqkwqf41m655w9mncv2mkgd16rl1bf") (f (quote (("rest" "ergo-lib-c-core/rest") ("mnemonic_gen" "ergo-lib-c-core/mnemonic_gen") ("default" "mnemonic_gen"))))))

