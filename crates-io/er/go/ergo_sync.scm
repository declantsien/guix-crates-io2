(define-module (crates-io er go ergo_sync) #:use-module (crates-io))

(define-public crate-ergo_sync-0.1.0 (c (n "ergo_sync") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.2.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 2)) (d (n "std_prelude") (r "^0.2.11") (d #t) (k 0)) (d (n "taken") (r "^0.1.0") (d #t) (k 0)))) (h "1y90bwhq0mghhc2hvjgih9cmz178c7n6rk0xmyhfm9hbh5ga2nks")))

