(define-module (crates-io er go ergo_fs) #:use-module (crates-io))

(define-public crate-ergo_fs-0.1.0 (c (n "ergo_fs") (v "0.1.0") (d (list (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1vm96l4da4clhvrm7drwz0x4jlivmf249wj1na29yx2nfdsimyg9")))

(define-public crate-ergo_fs-0.1.1 (c (n "ergo_fs") (v "0.1.1") (d (list (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "09nli3h7j9d29avdjx8kc2iy6mmd219g2nxxh8yky4xzrlv6sqjw")))

(define-public crate-ergo_fs-0.1.2 (c (n "ergo_fs") (v "0.1.2") (d (list (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0sq7p5bwzldj5khvrpzwfknwh8g5b8r9cnfljqmhd77sg5ak47kl")))

(define-public crate-ergo_fs-0.1.3 (c (n "ergo_fs") (v "0.1.3") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0n2ai0j48za477bv7x1w4vrbzr35bcpmab2ja4v0n5y92lfi38js")))

(define-public crate-ergo_fs-0.1.4 (c (n "ergo_fs") (v "0.1.4") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1wff86ny91vyfx6cpnkw9xxr0w5cfy4pb9ghga0cdsh98x24y867")))

(define-public crate-ergo_fs-0.1.5 (c (n "ergo_fs") (v "0.1.5") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "path_abs") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1kwm16w3pjcxlwg7s3rr3sxf2czd22yndcd6sih7py98k568blaw")))

(define-public crate-ergo_fs-0.2.0 (c (n "ergo_fs") (v "0.2.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "path_abs") (r "^0.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.9") (d #t) (k 0)) (d (n "tar") (r "^0.4.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0rmsq2rxvlfdna2jxczgn16k52j6z2z2j3jdzr6zlyb3i3g0a2cz")))

