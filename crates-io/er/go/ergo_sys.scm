(define-module (crates-io er go ergo_sys) #:use-module (crates-io))

(define-public crate-ergo_sys-0.0.1 (c (n "ergo_sys") (v "0.0.1") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1dxv6y7zhgk64q4137n5ddr2imzjfddzdff7s955znji46imylhi")))

