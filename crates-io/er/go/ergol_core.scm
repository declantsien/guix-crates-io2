(define-module (crates-io er go ergol_core) #:use-module (crates-io))

(define-public crate-ergol_core-0.1.0 (c (n "ergol_core") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "06jzyvgd49g5nk556461dq5787858v0r8bx7hm5qg0zipln6fnnx")))

(define-public crate-ergol_core-0.1.1 (c (n "ergol_core") (v "0.1.1") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "08fw1npzyjq2xz6409nark3b3fc0ancflnf29b4f72hy5lah5v9n")))

(define-public crate-ergol_core-0.1.2 (c (n "ergol_core") (v "0.1.2") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1j7nhfmmnpbj37vf3xxh5g8r2llyjn5m9dg117bhjishgc0zcaxv")))

(define-public crate-ergol_core-0.1.3 (c (n "ergol_core") (v "0.1.3") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0v5l0yhwa2z7bj3axvsbs28mib3vl18nzyssm9azb4c0lv0m5c36")))

(define-public crate-ergol_core-0.1.4 (c (n "ergol_core") (v "0.1.4") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "01pn0dvliarcy20ws5y5kavprmvvqy3vpr5jzd5m4vbp5bnm26c4")))

