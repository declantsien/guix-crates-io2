(define-module (crates-io er go ergo_std) #:use-module (crates-io))

(define-public crate-ergo_std-0.0.1 (c (n "ergo_std") (v "0.0.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2") (d #t) (k 0)))) (h "0g34fcxf2j2zz8wjzvngz9x91g7z663b01i3w3s4jqgb37gkmfxa")))

(define-public crate-ergo_std-0.0.2 (c (n "ergo_std") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2") (d #t) (k 0)))) (h "0b8a9g6zqazp6vq2288bh0dx8dwjrryyicmcm4qmzfsxqs0ykpi4")))

(define-public crate-ergo_std-0.0.3 (c (n "ergo_std") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.0.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2") (d #t) (k 0)))) (h "13pmi4gnc4xav7mryjqnn8jw3q2jd3ijrxr93xp4m697apcjji1x")))

