(define-module (crates-io er go ergo-types) #:use-module (crates-io))

(define-public crate-ergo-types-0.1.0 (c (n "ergo-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "ergo-chain-types") (r "^0.14.1") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.27.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "07glzgl7sl0phax0p3m1b3v6ddzmsw6xcbl3iqgq5nli6fb3i763")))

