(define-module (crates-io er go ergo_avltree_rust) #:use-module (crates-io))

(define-public crate-ergo_avltree_rust-0.1.0 (c (n "ergo_avltree_rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base16") (r "^0.2.1") (d #t) (k 0)) (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "debug-cell") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1mfq3zj32qbqb0fqwani00i7az1j122cdbl1z94mhfqdvarfpfs9")))

