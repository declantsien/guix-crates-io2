(define-module (crates-io er go ergo-pin) #:use-module (crates-io))

(define-public crate-ergo-pin-0.1.0 (c (n "ergo-pin") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.3") (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("parsing" "fold" "proc-macro" "full" "printing"))) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (k 2)))) (h "02x3yj05aq10q4js5yx074m86vmz8kbfb95zdr5qzxdyma4fphbb") (f (quote (("nightly-tests"))))))

