(define-module (crates-io er go ergo-lib-jni) #:use-module (crates-io))

(define-public crate-ergo-lib-jni-0.1.0 (c (n "ergo-lib-jni") (v "0.1.0") (d (list (d (n "ergo-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bhlmgkwj8hd31gghg66z64ms4w0jklv96ka8kcrbnbfl6vq1px4")))

(define-public crate-ergo-lib-jni-0.3.0 (c (n "ergo-lib-jni") (v "0.3.0") (d (list (d (n "ergo-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00sjjrs9qnw38jip0mg3p32ps22pz5sg86fblz2zrmmc0nd7p4wb")))

(define-public crate-ergo-lib-jni-0.4.0 (c (n "ergo-lib-jni") (v "0.4.0") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f233amkhpkvy5ybn558dqifglmlbr07sm8lgyadbjg553841m4r")))

(define-public crate-ergo-lib-jni-0.4.1 (c (n "ergo-lib-jni") (v "0.4.1") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1a2lyxg0zwjy4advphph45qbbb992cd1ycz51c4zjh7vny9fp5gs")))

(define-public crate-ergo-lib-jni-0.4.2 (c (n "ergo-lib-jni") (v "0.4.2") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sgpa6km43klk99c4flg6d4x3ny54ib0cb1f8h2ri6n04yphj3yw")))

(define-public crate-ergo-lib-jni-0.4.3 (c (n "ergo-lib-jni") (v "0.4.3") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bs3xm3fad62hgmpxiy4zs96lwhm131lgrpy4lxrd0cvr2921z4g")))

(define-public crate-ergo-lib-jni-0.4.4 (c (n "ergo-lib-jni") (v "0.4.4") (d (list (d (n "ergo-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dhqqrlcqm2dlfcirr28kcwa26aqc99iv9lnbq9cnqh29v8ywvac")))

(define-public crate-ergo-lib-jni-0.5.0 (c (n "ergo-lib-jni") (v "0.5.0") (d (list (d (n "ergo-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xf21jz6ya4yl5pfalwnmqrc7n1z7k0fzkc6z1fn6vjmq3mf9fgx")))

(define-public crate-ergo-lib-jni-0.5.1 (c (n "ergo-lib-jni") (v "0.5.1") (d (list (d (n "ergo-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14bj286ppp0ws8kv9nw8hzkh1lzg58rsqb8sp25gknqli79dxamz")))

(define-public crate-ergo-lib-jni-0.7.0 (c (n "ergo-lib-jni") (v "0.7.0") (d (list (d (n "ergo-lib") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05npc5w301im0d9z22s3m6f90aa0vzizfkn42qfn21cx4xip3d3l")))

(define-public crate-ergo-lib-jni-0.8.0 (c (n "ergo-lib-jni") (v "0.8.0") (d (list (d (n "ergo-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.8.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zf4dzbkja5a9wxazyizrvd5n9ryfigxna7i6fj2dxk6989faphp")))

(define-public crate-ergo-lib-jni-0.9.0 (c (n "ergo-lib-jni") (v "0.9.0") (d (list (d (n "ergo-lib") (r "^0.9.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ypiy02ccy5dk8351xykhbadrf8f6683w9mxi4ym7qd282x09gg2")))

(define-public crate-ergo-lib-jni-0.10.0 (c (n "ergo-lib-jni") (v "0.10.0") (d (list (d (n "ergo-lib") (r "^0.10.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1d17rj5k189w0xn2yy4qh4z6ymm19m5ap035ddmbflcmicnbzqac")))

(define-public crate-ergo-lib-jni-0.11.0 (c (n "ergo-lib-jni") (v "0.11.0") (d (list (d (n "ergo-lib") (r "^0.11.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.11.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vccwfjz5sq22xwak99p94hadw1gz39bq570mrslx6r0hnwcibzp")))

(define-public crate-ergo-lib-jni-0.12.0 (c (n "ergo-lib-jni") (v "0.12.0") (d (list (d (n "ergo-lib") (r "^0.12.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.12.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zcgafzk9l4cgzk89rpfm5686qh891wmhlg2n63ng25ypp7nq85k")))

(define-public crate-ergo-lib-jni-0.13.0 (c (n "ergo-lib-jni") (v "0.13.0") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ngixhiakrb2gg8wnqwm1bzlwsnlxm9pga8gkwbaq3rki5z5nylc")))

(define-public crate-ergo-lib-jni-0.13.1 (c (n "ergo-lib-jni") (v "0.13.1") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vgyp4iwwxn2iay9bgjhj2dwg37qdl0kj1n02q32aslz435shki2")))

(define-public crate-ergo-lib-jni-0.13.2 (c (n "ergo-lib-jni") (v "0.13.2") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wa3fpmj3mjrp4c2xgvf9bxgpk8k2wwnfl22yv3f5bm4nmyd6kbq")))

(define-public crate-ergo-lib-jni-0.13.3 (c (n "ergo-lib-jni") (v "0.13.3") (d (list (d (n "ergo-lib") (r "^0.13.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sb554153qal71sz7rm87mlmyliyz0s7z7ac1k8cmffgd6p9an2j")))

(define-public crate-ergo-lib-jni-0.14.0 (c (n "ergo-lib-jni") (v "0.14.0") (d (list (d (n "ergo-lib") (r "^0.14.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.14.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a0gk23m3k505f1flrnrcmblj137z4a5bihmg8ina8yydyp1ngf7")))

(define-public crate-ergo-lib-jni-0.15.0 (c (n "ergo-lib-jni") (v "0.15.0") (d (list (d (n "ergo-lib") (r "^0.15.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.15.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jcfajb594qbmrm9nnrh2l0fyxk2yx58lbdf3fy2zh72slkpvp8a")))

(define-public crate-ergo-lib-jni-0.16.0 (c (n "ergo-lib-jni") (v "0.16.0") (d (list (d (n "ergo-lib") (r "^0.16.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.16.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0sibaw6adhmkqns0wqw1ksm6cc8bildribl7m7c8pz6ig1vqiwzg")))

(define-public crate-ergo-lib-jni-0.16.1 (c (n "ergo-lib-jni") (v "0.16.1") (d (list (d (n "ergo-lib") (r "^0.16.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.16.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0qvn6vwfmf621cscql202naf8fc69xgsnfp94nib0s3hxkih47rg")))

(define-public crate-ergo-lib-jni-0.17.0 (c (n "ergo-lib-jni") (v "0.17.0") (d (list (d (n "ergo-lib") (r "^0.17.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.17.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1y761c2m2g0zs1znb2bj67a8dg1wqsc32r5ipvajapnli3hsf3rn")))

(define-public crate-ergo-lib-jni-0.18.0 (c (n "ergo-lib-jni") (v "0.18.0") (d (list (d (n "ergo-lib") (r "^0.18.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.18.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "152nnd4czkphrzmh08msn0gxb0v4wcznmskl3pklmlh03jy0vwgl")))

(define-public crate-ergo-lib-jni-0.19.0 (c (n "ergo-lib-jni") (v "0.19.0") (d (list (d (n "ergo-lib") (r "^0.19.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.19.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "19r1i2ywrv092i2z47nvckxarnqwz7l51zy2qc2p3j7vjdmd05by")))

(define-public crate-ergo-lib-jni-0.20.0 (c (n "ergo-lib-jni") (v "0.20.0") (d (list (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.20.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "00rywgswkvr9c9sv71r3k974m9ckfy0nb6bpyc5k4hi2y8shpkcv")))

(define-public crate-ergo-lib-jni-0.21.0 (c (n "ergo-lib-jni") (v "0.21.0") (d (list (d (n "ergo-lib") (r "^0.21.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.21.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0dnxcq12hfnxl8gpjkvlm386z9k39b7xhd3zk2gadfpvzr2sch45")))

(define-public crate-ergo-lib-jni-0.21.1 (c (n "ergo-lib-jni") (v "0.21.1") (d (list (d (n "ergo-lib") (r "^0.21.1") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.21.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1yzfij2qvvnn3hw16qpg0pvmrpzjp8b02ik52hikr6mw48hxlcwx")))

(define-public crate-ergo-lib-jni-0.22.0 (c (n "ergo-lib-jni") (v "0.22.0") (d (list (d (n "ergo-lib") (r "^0.22.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.22.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0m97lcjhmvmwakzbfq36xqp5840mxlcl6zzhya4jrllvpzc67vy4")))

(define-public crate-ergo-lib-jni-0.23.0 (c (n "ergo-lib-jni") (v "0.23.0") (d (list (d (n "ergo-lib") (r "^0.23.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.23.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1lk1mq9gl32hwaw2cvnj5fj84pdm911227gfw6cl53czildll2b4")))

(define-public crate-ergo-lib-jni-0.24.0 (c (n "ergo-lib-jni") (v "0.24.0") (d (list (d (n "ergo-lib") (r "^0.24.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.24.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0cq7wr08klffc9c9j85daqc0lhlw46nkwb46dk8dkkw6bzn074ck")))

(define-public crate-ergo-lib-jni-0.24.1 (c (n "ergo-lib-jni") (v "0.24.1") (d (list (d (n "ergo-lib") (r "^0.24.1") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.24.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0r5iycxpzqif43wy5941inzc6v4r14zazlpnhfmlqn2dix54l1bq")))

(define-public crate-ergo-lib-jni-0.25.0 (c (n "ergo-lib-jni") (v "0.25.0") (d (list (d (n "ergo-lib") (r "^0.25.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.25.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0ipk9zkzcpb0mzj968ydzgfr1fky26kxk9q71p7c4xykskq4hiaa")))

(define-public crate-ergo-lib-jni-0.26.0 (c (n "ergo-lib-jni") (v "0.26.0") (d (list (d (n "ergo-lib") (r "^0.26.0") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.26.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1glzk75ynny3r281c1p0j0iqz7pnh8wn4hsgscgnv21ns4p0c0z0")))

(define-public crate-ergo-lib-jni-0.27.1 (c (n "ergo-lib-jni") (v "0.27.1") (d (list (d (n "ergo-lib") (r "^0.27.1") (d #t) (k 0)) (d (n "ergo-lib-c-core") (r "^0.27.1") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1wppppnqvs2frl39zqwwgj8chvmrq8q99l2f3gndfvbz2y2s0571")))

