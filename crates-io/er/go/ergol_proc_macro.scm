(define-module (crates-io er go ergol_proc_macro) #:use-module (crates-io))

(define-public crate-ergol_proc_macro-0.0.0 (c (n "ergol_proc_macro") (v "0.0.0") (h "10dbisvfm2w2a4wcpz9vymi1g3q60fk67gl1d9d3jqyyjzmn0s13")))

(define-public crate-ergol_proc_macro-0.0.1 (c (n "ergol_proc_macro") (v "0.0.1") (d (list (d (n "case") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0mi0pdhqc657zj7fvd1fxz1q8a35nljnsr3chgm6zp8vrp2397qg")))

(define-public crate-ergol_proc_macro-0.1.0 (c (n "ergol_proc_macro") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "ergol_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1x259qshvzc7qbifnyqgj7h5al0ab4p3m359rlr0mzllxgdbsqws")))

(define-public crate-ergol_proc_macro-0.1.1 (c (n "ergol_proc_macro") (v "0.1.1") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "ergol_core") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "11wd2f9szm17dczy1qxi33yj2i3rrmjl44ylrkgp6pywclhvy9x6")))

(define-public crate-ergol_proc_macro-0.1.2 (c (n "ergol_proc_macro") (v "0.1.2") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "ergol_core") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1a7y9qqpk9rsj9aawky4ajwgs3w0hcl0ziibwbyv8l4321qxxr9q")))

(define-public crate-ergol_proc_macro-0.1.3 (c (n "ergol_proc_macro") (v "0.1.3") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "ergol_core") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "04g0bp0im1s5vfi75sfvbg93nqsxc7kjkyk1fbk3h5vh28bcw44s")))

(define-public crate-ergol_proc_macro-0.1.4 (c (n "ergol_proc_macro") (v "0.1.4") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "ergol_core") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "16ldhmzs1dk3pz461qh7jjsl9pkbkqfjfavmdb0c846s0am8mjaj")))

