(define-module (crates-io er go ergo-rustkit-endpoints) #:use-module (crates-io))

(define-public crate-ergo-rustkit-endpoints-0.1.0 (c (n "ergo-rustkit-endpoints") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "ergo-chain-types") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ap1nmp6ymg87bbr31qxxrw0py8jf7bmvw9ss4gb2wmr857v20rm")))

(define-public crate-ergo-rustkit-endpoints-0.1.1 (c (n "ergo-rustkit-endpoints") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "ergo-chain-types") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02i44mqki70236s1w0sghvan1xd7pqhc6dm5f31w24vj58sby878")))

(define-public crate-ergo-rustkit-endpoints-0.1.2 (c (n "ergo-rustkit-endpoints") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "ergo-chain-types") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ngjvf82g7j2xw6qfv6n64yaxqhqg8jwh6mryrs8pbkb7vbasvdq")))

(define-public crate-ergo-rustkit-endpoints-0.1.3 (c (n "ergo-rustkit-endpoints") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "ergo-chain-types") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ky33hiz1k61ypz9czczq9xwvpvp0h44717nqxwdi5phbz2dwb84")))

(define-public crate-ergo-rustkit-endpoints-0.1.4 (c (n "ergo-rustkit-endpoints") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "ergo-chain-types") (r "^0.7.0") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.20.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wvlh0b88x8j9k4qxra91hl0sz9i5539iqxqrrhkrycfnm8vfxqx")))

