(define-module (crates-io er go ergonames-rust-testnet) #:use-module (crates-io))

(define-public crate-ergonames-rust-testnet-0.1.0 (c (n "ergonames-rust-testnet") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bycbjjb3nqdbv8bj7h34c0k2xrwgbpmd3diiyqxhsbpwlwnmc6c") (y #t)))

