(define-module (crates-io er go ergonames) #:use-module (crates-io))

(define-public crate-ergonames-0.1.0 (c (n "ergonames") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p1l9j5cs7ls476mf64i7dvs4w9dgqbgbm0yllb49rg1c9kgvlpf")))

(define-public crate-ergonames-0.1.1 (c (n "ergonames") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12xs2xaqswhqd8bcxjvl9z3sb0byk5dy0lkakpkazbsk7dxv8cg0")))

(define-public crate-ergonames-0.1.2 (c (n "ergonames") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1spc4rasl62r83wr6w8gi490988556pbjdhybl8qy3bjnpwfzmyx")))

(define-public crate-ergonames-0.1.3 (c (n "ergonames") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09n2mqkxk20yjhv9sihdkmcfsdcs2l4n1ynsmm8dk69pklgz9kj1")))

(define-public crate-ergonames-0.1.4 (c (n "ergonames") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01hcx86yjva2c9p6yci5iyprk7kgprd8l7qpw8f6jagc4xavphxd")))

(define-public crate-ergonames-0.1.5 (c (n "ergonames") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1imy4109d16dnjyy9yvkvn658vxyphmlksmj5m8pj6gh502zkxx5")))

(define-public crate-ergonames-0.2.0 (c (n "ergonames") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v7d58x79hdvz88ild64kz4h6rndz17x60kri14xk3mx34zqf7ig")))

(define-public crate-ergonames-0.2.1 (c (n "ergonames") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ybqdxz66p145awbjys1gl60cfd4jr3rwidpk55chnpjbivrrd81")))

(define-public crate-ergonames-0.2.2 (c (n "ergonames") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ivr8c71l6ddhkrmswdqajpw38vbvbyy0a12kafiy6f3fapg36js")))

(define-public crate-ergonames-0.3.0 (c (n "ergonames") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04m791zrbi55wkphpl3ypj3czmi08pssflx5x9rqwj2hbzq0vlpr")))

(define-public crate-ergonames-0.3.1 (c (n "ergonames") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yffxk2fpdh1r8nxz9hi4zy32cffi7i2mc0sk7mzr13z8bfs9gf9")))

(define-public crate-ergonames-0.3.2 (c (n "ergonames") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s478i9dsripk5p8z4gwcpk0yyb3fvbb7qfz1yybm4wh1qbc1a7d")))

(define-public crate-ergonames-0.3.3 (c (n "ergonames") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12zvj45cgyg10d6xjis7zp68wppwin5ym05vf8pii35di82cli02")))

(define-public crate-ergonames-0.3.4 (c (n "ergonames") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1isjf77c8l499w46zfz2yjgfvf22zqcpbv0y0lwmr7i9q59maksm")))

(define-public crate-ergonames-0.3.5 (c (n "ergonames") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hnxb0qg7ddrgwp5j7091zyrd83p7jhzlhk7a2pnwhxxn3y69sz9")))

(define-public crate-ergonames-0.3.6 (c (n "ergonames") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d21qwi5560yrvn130k0z60gzjfivx9szdnp0i41wbr74dczl2jd")))

(define-public crate-ergonames-0.4.0 (c (n "ergonames") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12azvgkj23nqky27p76hgmaj1lcx344i9rz9qy8w2l0fwbjdwhya")))

(define-public crate-ergonames-0.4.1 (c (n "ergonames") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18m7x68gx15qhsgz845cd7hand90g6x9hb7szk1iqc67qb3nn543")))

(define-public crate-ergonames-0.4.2 (c (n "ergonames") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kayapd13a7p8mrdkzdy0rlkbgdxind169hm7dd81mpc9cpbzszl")))

(define-public crate-ergonames-0.4.3 (c (n "ergonames") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fh363kjl9v1if225ng1kli35f5lzqf6h6npzkcfrpwsrzpcaqja")))

(define-public crate-ergonames-0.4.4 (c (n "ergonames") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18jzfrqy5y5fnwfd1yc4rl7bfci3qfmi2xma6wzj9mc2kjlwqq7d")))

(define-public crate-ergonames-0.4.5 (c (n "ergonames") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02rridw67m7a4x0dw6zzh0bw8qz7qiwldbi2iqipfxzx82af34j0")))

(define-public crate-ergonames-0.4.6 (c (n "ergonames") (v "0.4.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g37dw4v8i5if3jshsrkmiinmql3pqb1jiagfbaw5ywsmw5944yj")))

(define-public crate-ergonames-0.4.7 (c (n "ergonames") (v "0.4.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p8a4k5ah14j5gk1kdzdabs51dh6rdmxag1rf6r0lqp0d5v3yfz2")))

(define-public crate-ergonames-0.4.8 (c (n "ergonames") (v "0.4.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mc71j0ri39yxh3b1djajx2zxidfqkxbjs4jzfl6vws9ps5jihnv")))

(define-public crate-ergonames-0.5.0 (c (n "ergonames") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.21.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02nk44x4awv8125c7b8cvhp11559zgz6qr1qjmzzcmrr47hlkrfh")))

(define-public crate-ergonames-0.5.1 (c (n "ergonames") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "ergo-lib") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wkhafv4321vz906zm6bixr2zn6da2ksir9iv63wccxi835q2k4x")))

(define-public crate-ergonames-0.6.0 (c (n "ergonames") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1jhk2vwp4yyixqhr63li7ik0242b2carb62xyi5daampdi2h791v")))

