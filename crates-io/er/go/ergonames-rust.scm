(define-module (crates-io er go ergonames-rust) #:use-module (crates-io))

(define-public crate-ergonames-rust-0.1.0 (c (n "ergonames-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nyf3aljpvy4dri6gl9n75i1d0p9bf6bjzqdhm01kf4b6xy249jw") (y #t)))

(define-public crate-ergonames-rust-0.1.1 (c (n "ergonames-rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "026mfprwj6yj42x8d80ypcaxdi9988waihris54hp6km2ppwz6h4") (y #t)))

