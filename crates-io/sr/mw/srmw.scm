(define-module (crates-io sr mw srmw) #:use-module (crates-io))

(define-public crate-srmw-0.1.0 (c (n "srmw") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "genawaiter") (r "^0.2") (f (quote ("futures03"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01k93qv1nvbilxj0p5kfma0vzds10izl7bgnkg4z12x9qcqq07rr")))

(define-public crate-srmw-0.1.1 (c (n "srmw") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "genawaiter") (r "^0.2") (f (quote ("futures03"))) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hksfp5pac26ssvml11nyj278c2frrxrcp06y9d98mcmdp1xhm7m")))

