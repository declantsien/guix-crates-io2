(define-module (crates-io sr cp srcpos_get) #:use-module (crates-io))

(define-public crate-srcpos_get-1.0.0 (c (n "srcpos_get") (v "1.0.0") (d (list (d (n "srcpos") (r "^1.1.1") (d #t) (k 0)) (d (n "srcpos_get_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0aylybgy24jxgjrf6n8klywacfmyxaxn1dk6qv0d2nl64imyxvdj") (f (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

(define-public crate-srcpos_get-1.1.0 (c (n "srcpos_get") (v "1.1.0") (d (list (d (n "srcpos") (r "^1.1.1") (d #t) (k 0)) (d (n "srcpos_get_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1kgqzv53gm46m94yvmjjwwh63p33rm3ssjk7nlaj10fav72l09sp") (f (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

(define-public crate-srcpos_get-1.1.1 (c (n "srcpos_get") (v "1.1.1") (d (list (d (n "srcpos") (r "^1.1.1") (d #t) (k 0)) (d (n "srcpos_get_derive") (r "^1.1") (o #t) (d #t) (k 0)))) (h "17kcfpg3ihq5a9d2ihbrrcika9b0y15pd8j7z3m6knkk6ad0a50p") (f (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

