(define-module (crates-io sr cp srcpos_get_derive) #:use-module (crates-io))

(define-public crate-srcpos_get_derive-1.0.0 (c (n "srcpos_get_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "srcpos") (r "^1.1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03wv1x78sksfbnmk1h990kdpdwiql247prwypfchppxvxn3ay6nj")))

(define-public crate-srcpos_get_derive-1.1.0 (c (n "srcpos_get_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "srcpos") (r "^1.1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16ydaw3x504hd73ywhqzls5d6c47ccbc3dc0b6b8hj2z7dfm9ii7")))

(define-public crate-srcpos_get_derive-1.1.1 (c (n "srcpos_get_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "srcpos") (r "^1.1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lwbmj0bghlhs4llxj14c5qjniqrpmf71v8d6zsnkvp8npaswc1x")))

