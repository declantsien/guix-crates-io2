(define-module (crates-io sr cp srcpos) #:use-module (crates-io))

(define-public crate-srcpos-1.0.0 (c (n "srcpos") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "19dzsqins9s1pyz893j1vh51fjsarvqrb3c5xmll6qzi90nbd4rn") (f (quote (("default" "serde"))))))

(define-public crate-srcpos-1.1.0 (c (n "srcpos") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "10bh5i7r56al8s9lah62wsfskfz39dhrk0d9q3q002vhcf6mn3b6") (f (quote (("default" "serde"))))))

(define-public crate-srcpos-1.1.1 (c (n "srcpos") (v "1.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0gzqajx5qbcxlj46aq8q4chnjs3slrgrad64k379c3w0q2vng7gf") (f (quote (("default" "serde"))))))

