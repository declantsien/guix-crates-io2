(define-module (crates-io sr c- src-dst-clarifier) #:use-module (crates-io))

(define-public crate-src-dst-clarifier-0.1.0 (c (n "src-dst-clarifier") (v "0.1.0") (d (list (d (n "kalavor") (r "~0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0gsfzpbxivmsy678phs0lxb301slx3jb1c6sq7bj0sqdf275jkr5") (y #t)))

(define-public crate-src-dst-clarifier-0.2.0 (c (n "src-dst-clarifier") (v "0.2.0") (d (list (d (n "kalavor") (r "~0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0vpqamm7fhgkvb23xayiqs2zf6jknxbbxcf9cwsjfidwgspf0gsd")))

