(define-module (crates-io sr c- src-graph) #:use-module (crates-io))

(define-public crate-src-graph-0.1.0 (c (n "src-graph") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1lnkn54y7v70nr8qb7gygg6pm5xqzham1x1fk269bi352pprm2ls") (y #t)))

(define-public crate-src-graph-0.1.1 (c (n "src-graph") (v "0.1.1") (d (list (d (n "rustc-ap-graphviz") (r "^662.0.0") (d #t) (k 0)))) (h "1irvnz2isch3qz6j06awcgrdaxd4xz4ks01ln3adrdfx2ql8xn4b") (y #t)))

(define-public crate-src-graph-0.1.2 (c (n "src-graph") (v "0.1.2") (d (list (d (n "rustc-ap-graphviz") (r "^662.0.0") (d #t) (k 0)))) (h "1vwz0p1yf0ss7szn73ww47m6m5mdwdgsng1x1l1zhagzzq343npr") (y #t)))

(define-public crate-src-graph-0.1.3 (c (n "src-graph") (v "0.1.3") (d (list (d (n "rustc-ap-graphviz") (r "^662.0.0") (d #t) (k 0)))) (h "157flkb9r8mlqv4yiwcdj7nlzdysn14vpj1mv3r1svmcag0f971i") (y #t)))

(define-public crate-src-graph-0.1.4 (c (n "src-graph") (v "0.1.4") (d (list (d (n "rustc-ap-graphviz") (r "^662.0.0") (d #t) (k 0)))) (h "0s4xqaxxpjd9gi0h9qmkvd53gp9xnj7300mgf87vkvk9dz36azsg")))

