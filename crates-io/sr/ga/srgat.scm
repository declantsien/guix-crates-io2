(define-module (crates-io sr ga srgat) #:use-module (crates-io))

(define-public crate-srgat-0.1.0 (c (n "srgat") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "10f9i15s1iq5rkvnfqag1wkk8abzgv152m2p9lrk7ifviyjwrmvf")))

