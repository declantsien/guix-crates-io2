(define-module (crates-io sr tp srtparse) #:use-module (crates-io))

(define-public crate-srtparse-0.1.0 (c (n "srtparse") (v "0.1.0") (h "0697m661kid729m74r6pqjxzhivdqfhgwpxd1q9m3y1nbjrhla0k")))

(define-public crate-srtparse-0.1.1 (c (n "srtparse") (v "0.1.1") (h "1zykqcg3ilqminkivv9iyg2bzjwxidqlsrqy2cqs1vwxr37zgdqn")))

(define-public crate-srtparse-0.2.0 (c (n "srtparse") (v "0.2.0") (h "0z7l057gcqn3z2b2ghlmgv6fmms31f3zlx1ixfjvlx80s3w2kps7")))

