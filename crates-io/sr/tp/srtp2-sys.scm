(define-module (crates-io sr tp srtp2-sys) #:use-module (crates-io))

(define-public crate-srtp2-sys-2.2.0 (c (n "srtp2-sys") (v "2.2.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "08dka79k5hzvbgxdx9984dblc62mbpac8kpcfb5b10r22604592y") (y #t) (l "srtp2")))

(define-public crate-srtp2-sys-2.20.0 (c (n "srtp2-sys") (v "2.20.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "11frnn4dm05sy60r5gaixas4sg4w6pi2wyr7dpv7xn9av59cnzpw") (f (quote (("enable-log-stdout") ("enable-debug-logging")))) (y #t) (l "srtp2")))

(define-public crate-srtp2-sys-2.20.1 (c (n "srtp2-sys") (v "2.20.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0aigdfqj5lh212110fxl5dy0ziwjhqjw7p3qcbdwipn6lawlijv7") (f (quote (("enable-log-stdout") ("enable-debug-logging")))) (l "srtp2")))

(define-public crate-srtp2-sys-2.20.2 (c (n "srtp2-sys") (v "2.20.2") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1709la0ansmdj46p8mnkfd2ic66sp5zrigybc2wqygri2zs3c92z") (f (quote (("enable-log-stdout") ("enable-debug-logging")))) (l "srtp2")))

(define-public crate-srtp2-sys-2.30.0 (c (n "srtp2-sys") (v "2.30.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "10ls4ynajhyaw7d2i64fqady1rqh6qjfdmwp2m5610px2ykn0bqz") (f (quote (("enable-log-stdout") ("enable-debug-logging")))) (l "srtp2")))

(define-public crate-srtp2-sys-3.0.0 (c (n "srtp2-sys") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1fn4q0i5l5zbmxzsvv75hypzqcbvxqi4vmxah6b7i97d1igp946m") (f (quote (("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (y #t) (l "srtp2")))

(define-public crate-srtp2-sys-3.0.1 (c (n "srtp2-sys") (v "3.0.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "07q5flg0r9i1dba6m3isq1affncd0b2m2jz4zxd1l4zji1f1lz6m") (f (quote (("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (l "srtp2")))

(define-public crate-srtp2-sys-3.0.2 (c (n "srtp2-sys") (v "3.0.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "10bssaj7srffhihak0hjcmcjn88bv8bjzjw9nfskxl9czzqdpjwc") (f (quote (("skip-linking") ("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (l "srtp2")))

