(define-module (crates-io sr tp srtp) #:use-module (crates-io))

(define-public crate-srtp-0.1.0 (c (n "srtp") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.20") (d #t) (k 0)))) (h "19bk5qlxrywmh0yv0zysjmjwbwn7ax4wjhzqj7hb2db276g2zxfx")))

(define-public crate-srtp-0.2.0 (c (n "srtp") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.20") (d #t) (k 0)))) (h "04rqgrg39xhxpp8ahzkgs9wva0q9jwxacsgfg8z3p5ii97mfdjbb")))

(define-public crate-srtp-0.3.0 (c (n "srtp") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.20") (d #t) (k 0)))) (h "1855izkdc01sf32j6aas7flhpb70fwxhh49pl9fc328mzmy3x6gs")))

(define-public crate-srtp-0.4.0 (c (n "srtp") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.20") (d #t) (k 0)))) (h "024nlc6lwnjr4y5pjc3c56va5armmlhwfahvp5y5dq04rcaq4zia")))

(define-public crate-srtp-0.5.0 (c (n "srtp") (v "0.5.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.30") (d #t) (k 0)))) (h "1plm88wa0d8ybsfxr891lmvh9h7q9k6f2f815zqgnj9hf9wgysrq")))

(define-public crate-srtp-0.6.0 (c (n "srtp") (v "0.6.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "srtp2-sys") (r "^2.30") (d #t) (k 0)))) (h "05vky1qh582dbgryzd7vr8qnsmhsv7lx9p2kjqgm5zqfp4rp7i1c")))

(define-public crate-srtp-0.7.0 (c (n "srtp") (v "0.7.0") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "srtp2-sys") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zv9n818pl9lm7cwq5c82v0knr432xlq70jv7s8wb0mgr8r0sm7v") (f (quote (("skip-linking" "srtp2-sys/skip-linking") ("enable-openssl" "openssl" "srtp2-sys/enable-openssl") ("default" "bytes" "log" "enable-openssl" "build-libsrtp") ("build-openssl" "enable-openssl" "srtp2-sys/build-openssl") ("build-libsrtp" "srtp2-sys/build"))))))

