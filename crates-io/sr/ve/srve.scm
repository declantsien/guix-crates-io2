(define-module (crates-io sr ve srve) #:use-module (crates-io))

(define-public crate-srve-0.1.0 (c (n "srve") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 2)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)))) (h "0hlxzzsi9k427szb2b22glj9dqg5h4qjjfjm4rg9ypq14adcp5gk")))

(define-public crate-srve-0.1.1 (c (n "srve") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 2)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)))) (h "1fjpc8llha1lbdvlxcdxrlf8pwrpxk2bqm4hxwpsawr4clqc2kx5")))

(define-public crate-srve-0.1.2 (c (n "srve") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 2)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)))) (h "1x39fq9fimk91mnk2a6kaz7g1afngyqnxh0gfy27paj4biijp73p")))

(define-public crate-srve-0.1.3 (c (n "srve") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 2)) (d (n "simple_logger") (r "^1.11") (d #t) (k 2)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)))) (h "0c6w5344b5ji4x5p8pm8gpbb3sd2fhb66n01fjrri8wqic9fkk3s")))

