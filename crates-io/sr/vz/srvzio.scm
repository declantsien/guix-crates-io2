(define-module (crates-io sr vz srvzio) #:use-module (crates-io))

(define-public crate-srvzio-1.0.0 (c (n "srvzio") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (f (quote ("termination"))) (d #t) (k 0)))) (h "13qwgh0iybypqsq122i05bpwlx506wnxfyi5690pypaayya7k9fi")))

(define-public crate-srvzio-1.0.1 (c (n "srvzio") (v "1.0.1") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (f (quote ("termination"))) (d #t) (k 0)))) (h "0hwbkp6kcnxn3fjb2sx54457dggn1sj2dvjklzmsi5g660wy7yb2")))

(define-public crate-srvzio-1.1.0 (c (n "srvzio") (v "1.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.2") (f (quote ("termination"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0mj1zjr8vs89sjc5zhwgqjph0mjhzk3byj71dbacgmylsn8hli8g")))

(define-public crate-srvzio-1.1.1 (c (n "srvzio") (v "1.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1mska63gy7p0bgv0isyifss6qdbflvw3cprcybwdh2rinx6w9zhc")))

