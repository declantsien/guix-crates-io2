(define-module (crates-io sr cs srcsrv) #:use-module (crates-io))

(define-public crate-srcsrv-0.1.0 (c (n "srcsrv") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)))) (h "0jii9mhqv307ask7plx6q50ndims5r40pjbipqi8h4padr5h0if6")))

(define-public crate-srcsrv-0.2.0 (c (n "srcsrv") (v "0.2.0") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "pdb") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mkpphnr2cfpd4vrnhfvp2b2q9q8yj2kjdsysjb1wjpz5ly7rabr")))

(define-public crate-srcsrv-0.2.1 (c (n "srcsrv") (v "0.2.1") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "pdb") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18n05xk7d4z3m4vi96pdahbhjz4ggigwrcccxjxq5nxg5r4f127s")))

(define-public crate-srcsrv-0.2.2 (c (n "srcsrv") (v "0.2.2") (d (list (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "pdb") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w5cx028r4yfcmnvazp9mss9qqlxb0ixkc9cjnqljbjj1skkf902")))

