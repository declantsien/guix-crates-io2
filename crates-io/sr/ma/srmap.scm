(define-module (crates-io sr ma srmap) #:use-module (crates-io))

(define-public crate-srmap-0.1.0 (c (n "srmap") (v "0.1.0") (h "1296rbqnnwx1ydp9x9qw5h0bp4yxkawjjc419lkwwh361gz2wl32")))

(define-public crate-srmap-0.1.1 (c (n "srmap") (v "0.1.1") (h "1x0kxp8ia3yzz900nm4i51avhjxf0l5h3csg322mkc8gjgca9scq")))

(define-public crate-srmap-0.1.2 (c (n "srmap") (v "0.1.2") (h "0wg8w0w6cx4ms8nbcdw56484dvhrkh4hag4h8rz74sc308ialcwa")))

(define-public crate-srmap-0.1.3 (c (n "srmap") (v "0.1.3") (h "18cwcj7rcp4dzripkpyx5f7gwv4ww1nq3r8cy6aflz15wf16q39l")))

(define-public crate-srmap-0.1.5 (c (n "srmap") (v "0.1.5") (h "0s04gbf3c0nm2svvc3hnj723ra3qpi7wm0nk6ba61y5xk6lkmzcq")))

(define-public crate-srmap-0.1.7 (c (n "srmap") (v "0.1.7") (h "0cz9gp1spq6bj91h8dmm0jshfb6pzhr24mcvlq3dqw9n3s809a6f")))

(define-public crate-srmap-0.1.8 (c (n "srmap") (v "0.1.8") (h "0paz9s44j6y5infjwszfvfbhxwzw7xjz3r22w2pcjnfja6ibzg71")))

(define-public crate-srmap-0.1.9 (c (n "srmap") (v "0.1.9") (h "1vk4yjl49lnh12nr9i9w7xygbvlg1v8l0ihdhwbm0bmjz7w23mmp")))

(define-public crate-srmap-0.1.10 (c (n "srmap") (v "0.1.10") (h "0a1djzdfd6z73yfh6bhsn5p97lwicz00sqs5q6vh9r61rss9mrd4")))

(define-public crate-srmap-0.1.12 (c (n "srmap") (v "0.1.12") (h "0vkkp0y586piqg3ha4pm4xd090c54m6ld4a5v7gk9m5a8466h7bi")))

(define-public crate-srmap-0.1.13 (c (n "srmap") (v "0.1.13") (h "1m3g2lsrpviy6rsn2bdihq1phz6pcfd2cc415p5xm33f351klpkf")))

