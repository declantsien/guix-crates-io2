(define-module (crates-io sr ws srws) #:use-module (crates-io))

(define-public crate-srws-0.1.0 (c (n "srws") (v "0.1.0") (h "14qwnvdc788kz224yyq8m6w9vj38ijp65xzcb8nhqfbrg6vcbyz7")))

(define-public crate-srws-0.1.1 (c (n "srws") (v "0.1.1") (h "10fgr7jpvv5mrn0b8ya7bc8b1vrwq1k782jqs09i3nyk1izsjx2q")))

(define-public crate-srws-0.1.2 (c (n "srws") (v "0.1.2") (h "1g3jx4jjvbqlxars1nnss608df2r5cpiv3nl68hglmwmlmlxm3pp")))

(define-public crate-srws-0.2.2 (c (n "srws") (v "0.2.2") (d (list (d (n "readconfig") (r "^0.2.4") (d #t) (k 0)))) (h "19cwpg3x165s16g4rfjzxj9rz96swh8h706jac3dwpxyaysfmr8s")))

(define-public crate-srws-0.2.3 (c (n "srws") (v "0.2.3") (d (list (d (n "readconfig") (r "^0.2.5") (d #t) (k 0)))) (h "0vpfkmah9dp3r6xd4lkc968bg8xr5v123vj63kghpq8xg8zcb2k0")))

