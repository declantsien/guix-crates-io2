(define-module (crates-io sr lo srlog) #:use-module (crates-io))

(define-public crate-srlog-0.1.0 (c (n "srlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "leon") (r "^2.0.1") (d #t) (k 0)))) (h "0m62zn82z7n7yxlpxw304f5vwhlq0aii03s66ys0mp8wvqh6x0l6")))

