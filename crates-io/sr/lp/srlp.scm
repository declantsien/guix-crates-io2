(define-module (crates-io sr lp srlp) #:use-module (crates-io))

(define-public crate-srlp-0.2.4 (c (n "srlp") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)) (d (n "s-types") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0757p7rj4239z9lsavz6kzr7xc6x1bgh8nhanda4xnmh6bfair29") (f (quote (("sophon" "s-types") ("default" "sophon"))))))

(define-public crate-srlp-0.3.0 (c (n "srlp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)) (d (n "s-types") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1xqck9nx5b7081vamv02j9jiag7344dqqi03hkirihi7kl6igra6") (f (quote (("sophon" "s-types") ("default" "sophon"))))))

