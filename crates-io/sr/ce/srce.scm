(define-module (crates-io sr ce srce) #:use-module (crates-io))

(define-public crate-srce-0.1.0 (c (n "srce") (v "0.1.0") (d (list (d (n "generativity") (r "^1.0.1") (d #t) (k 0)) (d (n "qcell") (r "^0.5.2") (f (quote ("generativity"))) (k 0)) (d (n "selfref") (r "^0.4") (d #t) (k 0)))) (h "16agqxl3vx1vq42r540iq4vsap2lnxcbmlarjnfypr1z8ka8yh94")))

(define-public crate-srce-0.1.1 (c (n "srce") (v "0.1.1") (d (list (d (n "selfref") (r "^0.4.1") (f (quote ("qcell"))) (d #t) (k 0)) (d (n "qcell") (r "^0.5.2") (d #t) (k 2)))) (h "08ybkba9qrq5c2vfg72ndkrn1wk5z4zpdz2fhrh5p21qsi2lagdb")))

