(define-module (crates-io sr ce srcerr) #:use-module (crates-io))

(define-public crate-srcerr-0.1.0 (c (n "srcerr") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1bh2j2iqji5fmsbia0v1yb6hi8y6ycv7llicbq7dnqjzj3r60h1y") (f (quote (("default" "ansi_color") ("ansi_color" "ansi_term"))))))

(define-public crate-srcerr-0.2.0 (c (n "srcerr") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)))) (h "1wpdhi2a5qks4rsnhggnvx43fbr5lsw27rf76zfbj2ql75y2k21p") (f (quote (("serialization" "codespan-reporting/serialization"))))))

(define-public crate-srcerr-0.3.0 (c (n "srcerr") (v "0.3.0") (d (list (d (n "codespan") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "1ny790cnbzx2wq5d30yr03lm91h29953xi2124yv9mmk4jfjn8a3") (f (quote (("serialization" "codespan-reporting/serialization"))))))

(define-public crate-srcerr-0.4.0 (c (n "srcerr") (v "0.4.0") (d (list (d (n "codespan") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "01wn25l0hzbmk155fmbkk06bbpg28fry83sdvsqx9lzlychs58qx") (f (quote (("serialization" "codespan-reporting/serialization"))))))

