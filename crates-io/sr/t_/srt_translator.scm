(define-module (crates-io sr t_ srt_translator) #:use-module (crates-io))

(define-public crate-srt_translator-0.1.2 (c (n "srt_translator") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "async-openai") (r "^0.10.2") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16vmc96zrm4158ydiahg7v6rgp0yi721738xcfnjgqfnap1im607") (r "1.67")))

