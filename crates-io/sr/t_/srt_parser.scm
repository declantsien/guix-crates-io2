(define-module (crates-io sr t_ srt_parser) #:use-module (crates-io))

(define-public crate-srt_parser-0.1.0 (c (n "srt_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "13jc401aq5b3jd734w952zcr5f7fyiciqg2rlsyyk8cbzxm1m6h8")))

(define-public crate-srt_parser-0.1.1 (c (n "srt_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "09m1skbby75r3rb1pc4kmhd2x9lzx18myv9rpcp5si66knrx9l84")))

(define-public crate-srt_parser-0.1.12 (c (n "srt_parser") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "100sz6r8kvbczkl9pn2f6k4r2xv8g00k67l7mj52nq577dpwanch")))

(define-public crate-srt_parser-0.1.13 (c (n "srt_parser") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "1zqysrngy4r2ry2l7q450x3y0i188ww39yknp9zgyvg23gfr093s")))

(define-public crate-srt_parser-0.1.14 (c (n "srt_parser") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "1if54ri0843jmzffjn7bdmvndn0z66ba6a81h5hfhvkzdw5fwrg3")))

(define-public crate-srt_parser-0.1.14-beta (c (n "srt_parser") (v "0.1.14-beta") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "05qwdg5hkky00px22rchfiisfilvfmww35hkkx3lfl7f5vnc9xgw")))

(define-public crate-srt_parser-0.1.15 (c (n "srt_parser") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (d #t) (k 0)))) (h "0gzdzc2xp8ys3qhxay2mfi53dxzckw65wmbckp1ir52wacvh05c4")))

