(define-module (crates-io sr gb srgb) #:use-module (crates-io))

(define-public crate-srgb-0.1.0 (c (n "srgb") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)))) (h "1z9ah99d90j5pnyr85kkg05wbdb5k56lg3g4ip1n9l3ysf7ii8ss")))

(define-public crate-srgb-0.1.1 (c (n "srgb") (v "0.1.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.1") (d #t) (k 1)))) (h "1znc55aihdhvlr206fn106kykhbhby6axyr21mf2hgzzr7y1f2l3")))

(define-public crate-srgb-0.1.3 (c (n "srgb") (v "0.1.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.1") (d #t) (k 1)))) (h "0lvrf8b59qs8if045ib8n0pjfcqmfx56djapm0361qyx9zcbj1gs")))

(define-public crate-srgb-0.1.4 (c (n "srgb") (v "0.1.4") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.1.2") (d #t) (k 1)))) (h "17nli88wvc462ivy85dagz1kwlnr9giz1j8flkarhp9zd7ngyf7m") (f (quote (("prefer-fma") ("no-fma"))))))

(define-public crate-srgb-0.2.0 (c (n "srgb") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)))) (h "1vw1haprw8ichklm4n7sblgz5bpj8rp1sy754al0y6s7741pbcwn") (f (quote (("prefer-fma") ("no-fma")))) (y #t)))

(define-public crate-srgb-0.2.1 (c (n "srgb") (v "0.2.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.12") (f (quote ("float" "rational"))) (k 1)))) (h "0i5pvd20fl46510srk87lkmnm1hpa7xgy4iffj8cvzqjdd774y0f") (f (quote (("prefer-fma") ("no-fma"))))))

(define-public crate-srgb-0.2.2 (c (n "srgb") (v "0.2.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "delta_e") (r "^0.2") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.12") (f (quote ("float" "rational"))) (k 1)))) (h "0s0hw5wnfjnvg7pzn7qn35zbvsfirc18mryhql9xvcn5mxs15nv3")))

(define-public crate-srgb-0.2.3 (c (n "srgb") (v "0.2.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.12") (f (quote ("float" "rational"))) (k 1)))) (h "0d3yzzyksl7a8r1yzhpmsxzw5ym3i8nabmx8ig2h6963zmyk1b1l")))

(define-public crate-srgb-0.3.0 (c (n "srgb") (v "0.3.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "float_next_after") (r "^0.1") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.12") (f (quote ("float" "rational"))) (k 1)))) (h "1bla4b6ll3dwxdzwaffw48k0g5y6zfvxbkigpv4fab5hnyxifyd8")))

(define-public crate-srgb-0.3.1 (c (n "srgb") (v "0.3.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "float_next_after") (r "^0.1") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 2)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.12") (f (quote ("float" "rational"))) (k 1)))) (h "1jma2w95jjf5qhdshyxhr58yyx7g475qgkdajkrsl8j562hsj8nc")))

(define-public crate-srgb-0.3.2 (c (n "srgb") (v "0.3.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "float_next_after") (r "^0.1") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 2)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.17") (f (quote ("float" "rational"))) (k 1)))) (h "0kpdp5ldni16vv3nvy3d6k8xgb3dsllf5xkg6jh0rx27a9xm7kb8")))

(define-public crate-srgb-0.3.3 (c (n "srgb") (v "0.3.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "float_next_after") (r "^0.1") (d #t) (k 2)) (d (n "kahan") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 2)) (d (n "rgb_derivation") (r "^0.2") (d #t) (k 1)) (d (n "rug") (r "^1.17") (f (quote ("float" "rational" "std"))) (k 1)))) (h "1kvy4qfnw3nn0p98qq1abjjgl1yan5j8vx6g0zx71lc91qnr5q5l")))

