(define-module (crates-io sr fs srfs) #:use-module (crates-io))

(define-public crate-srfs-0.1.0 (c (n "srfs") (v "0.1.0") (d (list (d (n "bitflags") (r "2.4.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "srfs-core") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)))) (h "05xbnmyk7bavb3hqm3q774r6naw2gl4knm5wmf1z59inljazzl1j")))

(define-public crate-srfs-0.1.1 (c (n "srfs") (v "0.1.1") (d (list (d (n "bitflags") (r "2.4.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "srfs-core") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)))) (h "1378llhjl8rd62zz2r5syzvg5isn0n9nr75r50andazhbrvxq9kk")))

(define-public crate-srfs-0.1.2 (c (n "srfs") (v "0.1.2") (d (list (d (n "bitflags") (r "2.4.*") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "srfs-core") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)))) (h "03qzczlg7kr7dgbsmi907nrwmxj4cp8wz5shalizgq2p71szx6k7")))

