(define-module (crates-io sr fs srfs-core) #:use-module (crates-io))

(define-public crate-srfs-core-0.1.0 (c (n "srfs-core") (v "0.1.0") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jb59q9p1cl77251dc8acz3lh1xi6xrlfkbh0xybh1mrlirpjr8b") (f (quote (("std") ("default"))))))

(define-public crate-srfs-core-0.1.1 (c (n "srfs-core") (v "0.1.1") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1x72cm4axcxjn27r0i7f1y67y0nxaz5zxydfwr6w6li94z4x0k2w") (f (quote (("std") ("default"))))))

(define-public crate-srfs-core-0.1.2 (c (n "srfs-core") (v "0.1.2") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "18726r6bsf4zvb8h57drcwc7lsq9rdi7iqpgp3qggjcfh724yfvf") (f (quote (("std") ("default"))))))

