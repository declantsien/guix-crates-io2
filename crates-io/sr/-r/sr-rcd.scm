(define-module (crates-io sr -r sr-rcd) #:use-module (crates-io))

(define-public crate-sr-rcd-0.5.0 (c (n "sr-rcd") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "04f805d1bznlcs9zyhvk4kc0jwbxam9vx6x9mj9v9czzkac7k6wy")))

(define-public crate-sr-rcd-0.5.1 (c (n "sr-rcd") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "07pysfp1fm05mga9xjana310iv73y5sr69zkg12davvz3sk2c9xl")))

(define-public crate-sr-rcd-0.5.2 (c (n "sr-rcd") (v "0.5.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "15vwy12mvk05f82670lxg8v1g0qv7rv80mpwl6np3j8g251xqdjc")))

(define-public crate-sr-rcd-0.6.0 (c (n "sr-rcd") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0pzgg5pk1vfijpzcya6jh5rnx8mxvcr3ahr84g0w4zygda9lbxps")))

