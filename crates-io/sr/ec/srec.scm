(define-module (crates-io sr ec srec) #:use-module (crates-io))

(define-public crate-srec-0.1.0 (c (n "srec") (v "0.1.0") (h "1pywghjvr1vg2gshfm3bah8m28kx4qyahffzzdnrcd723pm4kyaz")))

(define-public crate-srec-0.1.1 (c (n "srec") (v "0.1.1") (h "02pn6d5705m8v5819nahacypmhqzk7ggzh19mfcbkqhgpg8ncf85")))

(define-public crate-srec-0.1.2 (c (n "srec") (v "0.1.2") (h "10mdg8py3y1ahqbni4k22mffjwq9hd21n5k3nwsq37ixfkimz7qa")))

(define-public crate-srec-0.2.0 (c (n "srec") (v "0.2.0") (h "1yxd0crpsgpn3z7l9x6pm6h2zahgfa5xrp1w6g6y6hn2ir9s1hqp")))

