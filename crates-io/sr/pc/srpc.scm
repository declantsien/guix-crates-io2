(define-module (crates-io sr pc srpc) #:use-module (crates-io))

(define-public crate-srpc-0.0.2 (c (n "srpc") (v "0.0.2") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "sia") (r "^0.0.4") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.0.2") (d #t) (k 0)))) (h "004riidy2rpqmr6ismv1cjv64ccfv25p269axq16rg6gh40rba7z")))

(define-public crate-srpc-0.0.3 (c (n "srpc") (v "0.0.3") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.0.3") (d #t) (k 0)))) (h "0ziav221xvba7j9wlwqzsxy6pp935m7fh5h3ds7y6jh10k75bzl2")))

(define-public crate-srpc-0.1.0 (c (n "srpc") (v "0.1.0") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.0.3") (d #t) (k 0)))) (h "0amh3jvfml7j5jyx2mlv8crxl5pqivic443zifv23s3kin6yn4gx")))

(define-public crate-srpc-0.1.1 (c (n "srpc") (v "0.1.1") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1r9qdl4gdl4521811cv3wbnbf6nsdy15a2s5xpp2hajbbvmjj9bw")))

(define-public crate-srpc-0.1.2 (c (n "srpc") (v "0.1.2") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0vawx32x5gqsr3jm8r8l4dxfgmk2dnzbs9fw10528ca80kdw91kf")))

(define-public crate-srpc-0.1.3 (c (n "srpc") (v "0.1.3") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1ns29s844z5ns982z3kin22vvk25phvxya57vpfcv66pa640s39w")))

(define-public crate-srpc-0.1.4 (c (n "srpc") (v "0.1.4") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0yv6fkq1167ypdbfdna94174zif0jqc74ggs1nxv8a7r17d7yzpl")))

(define-public crate-srpc-0.1.6 (c (n "srpc") (v "0.1.6") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "canary") (r "^0.1.6") (d #t) (k 0)) (d (n "compact_str") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "srpc-macro") (r "^0.1.5") (d #t) (k 0)))) (h "12q15vdr6ar33iy9p6gn2vdn0cxg02qjxz6886fi1w7al60928pq")))

