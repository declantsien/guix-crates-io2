(define-module (crates-io sr pc srpc-macro) #:use-module (crates-io))

(define-public crate-srpc-macro-0.0.2 (c (n "srpc-macro") (v "0.0.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ni925wmz5abylwldwrmvmi197pjr3bpv4fsa13wl8yqb0d7b833")))

(define-public crate-srpc-macro-0.0.3 (c (n "srpc-macro") (v "0.0.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b52n0kv5d797asf80fwb5n6pmbbpj496palgsa58gxz0d26qg8y")))

(define-public crate-srpc-macro-0.1.1 (c (n "srpc-macro") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g72mf1iv8rl2fcbmmzprh6nh0rk581irpcqn6f5nffjsqp7rk7x")))

(define-public crate-srpc-macro-0.1.3 (c (n "srpc-macro") (v "0.1.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zmn25rfhr602bxsf0lnjlckl114myms9ax3vlpw3h1ysn3vgxrk")))

(define-public crate-srpc-macro-0.1.4 (c (n "srpc-macro") (v "0.1.4") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05yykfvqbr2493jsja7fna2376fpfkzy9amnh306njpzax61652i")))

(define-public crate-srpc-macro-0.1.5 (c (n "srpc-macro") (v "0.1.5") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0smlmmr2dav1s9s0rq23pzabrlllqiinqdggaaav1nfjricdcb8f")))

