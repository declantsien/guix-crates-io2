(define-module (crates-io sr am sram23x) #:use-module (crates-io))

(define-public crate-sram23x-0.0.0 (c (n "sram23x") (v "0.0.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "1kinqq61l5kyni7kyd0hn1arx6pniq13ml45czm5llb9ylk1d8pw")))

(define-public crate-sram23x-0.1.0 (c (n "sram23x") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "09cbi727i6ww3v6f179xgm899p6ymkz4ynpbffdxxc6qvf63b0sr")))

(define-public crate-sram23x-0.2.0 (c (n "sram23x") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "0gjfsawyw10li4sfk877dcmh914pls2n3r803d0jvl5llrf5jjxf")))

(define-public crate-sram23x-0.2.1 (c (n "sram23x") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "0sn2wmsqk0zknjqid85kag0vz3pp21nbb6r1q0nllb476aikyqj4")))

(define-public crate-sram23x-0.2.2 (c (n "sram23x") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "044bnr8hsp7s4j90wvwy16nhxhnvaja8vwgawx05n6ywyy3rwahy")))

(define-public crate-sram23x-0.3.0 (c (n "sram23x") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "01jvk50cv102j66c5hhbzjqmkrpp9b7ac6r7wnq0r1qshzy94icf")))

(define-public crate-sram23x-0.3.1 (c (n "sram23x") (v "0.3.1") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "04zalaihmf1spl9vak80l5qd74x7zns2y5wi0j0l1bzh3i973xbc")))

