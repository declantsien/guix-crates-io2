(define-module (crates-io sr c2 src20_sdk) #:use-module (crates-io))

(define-public crate-src20_sdk-0.0.1 (c (n "src20_sdk") (v "0.0.1") (d (list (d (n "fuels") (r "^0.43.0") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1qfvkcgf4w62cy65dw719iky10hnd41ari6qpd21pfakiby1nxnc")))

(define-public crate-src20_sdk-0.0.2 (c (n "src20_sdk") (v "0.0.2") (d (list (d (n "fuels") (r "^0.43.0") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0gdfkph94his2slxr32hkg70a71339jk9sv427y5wdcazrrrkx0h")))

(define-public crate-src20_sdk-0.0.3 (c (n "src20_sdk") (v "0.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fuels") (r "^0.41.1") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1080gixvjg30nf667gw9s7ma3igadk135b5gg0m98sv8ykk5y5wy")))

