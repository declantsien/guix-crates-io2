(define-module (crates-io sr tr srtree) #:use-module (crates-io))

(define-public crate-srtree-0.1.0 (c (n "srtree") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03vq84ppkhqc5ivvsmsnfhg4cnrzg0yzkbbzf48aiswanw7ha8sf") (r "1.63")))

(define-public crate-srtree-0.2.0 (c (n "srtree") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b0dbadf1g4fmpvmbxkvxlgd0r8v3i7dgcnfa59icwir3pg5gxxy") (r "1.63")))

(define-public crate-srtree-0.2.1 (c (n "srtree") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0b640gg47rzj7ppphhvnmm5r3wdb5nmjbvws3ifn3zyzvnbrz3q6") (r "1.63")))

