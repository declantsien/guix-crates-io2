(define-module (crates-io sr tr srtresync) #:use-module (crates-io))

(define-public crate-srtresync-0.1.0 (c (n "srtresync") (v "0.1.0") (h "0alzsj76yzfrslfvmbznwbw5ks3cia3l24f6rw6g2v5a9gfbraq0")))

(define-public crate-srtresync-0.1.1 (c (n "srtresync") (v "0.1.1") (h "1f0rmq7z8qiap4hskslgbzxhapqw3ypma28lp48x6czc0sj8l4rg")))

