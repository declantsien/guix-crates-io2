(define-module (crates-io sr tm srtm) #:use-module (crates-io))

(define-public crate-srtm-0.1.0 (c (n "srtm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0x748apwgy5hicgbrbib8jk2x1zbkhmz4kz7wlv6k3q7n6kq9h5f")))

(define-public crate-srtm-0.1.1 (c (n "srtm") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "16kp36ffbk7qmzjwsw87bhizn5lfwsqsyw6fm99cmcvk8vxk99ns")))

