(define-module (crates-io sr e- sre-engine) #:use-module (crates-io))

(define-public crate-sre-engine-0.1.0 (c (n "sre-engine") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "0v7sq4bd4mdxsq7vcn0zwkd5cy06x3v91sg9pf6wc77gf8xwjnj0")))

(define-public crate-sre-engine-0.1.1 (c (n "sre-engine") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "0rf2fybjdf4gs90mjlvyhcymksl5kgg6lydw923627xpg8zqfls9")))

(define-public crate-sre-engine-0.1.2 (c (n "sre-engine") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1h8zd0qj37p825g4yxqkhb43lqj1c1zwnwy79gnlya3w52cj71z5")))

(define-public crate-sre-engine-0.2.0 (c (n "sre-engine") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "00qsyld8fzrq62a1d2i97jrri1hanwbkqblp1ffp7234xkq87qjm")))

(define-public crate-sre-engine-0.2.1 (c (n "sre-engine") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "091bf626q15ickp369jcz60fraszi54y9w9yi6hwvjwa568aj08l")))

(define-public crate-sre-engine-0.3.0 (c (n "sre-engine") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1r2vsx02npaarx5agbhhgbjb18fq54am7hpp27h01xadql0ijiia")))

(define-public crate-sre-engine-0.4.1 (c (n "sre-engine") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)))) (h "0a1dmk0y244d1dsibwd05lnynxg7cx6qxvp7ynkaknrmdk2cb454")))

(define-public crate-sre-engine-0.4.2 (c (n "sre-engine") (v "0.4.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)))) (h "12k2z2jiz6lra51c4qa6h7xbyhr5axmd9vjhni8cl47bbiqa83dw") (y #t)))

(define-public crate-sre-engine-0.4.3 (c (n "sre-engine") (v "0.4.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)))) (h "00iihj2429j3k5nqn7wqacc8w4pjmr2s5s6j69amvn6lrflfc80i")))

