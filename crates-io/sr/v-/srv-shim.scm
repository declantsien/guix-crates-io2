(define-module (crates-io sr v- srv-shim) #:use-module (crates-io))

(define-public crate-srv-shim-0.1.0 (c (n "srv-shim") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "plumber") (r "^0.0.3") (d #t) (k 0)))) (h "0larfls6p6i9rrbj0047if2f5dcm59yp12hhdq48vfcxa9pjdzbl")))

(define-public crate-srv-shim-0.1.1 (c (n "srv-shim") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "plumber") (r "^0.0.5") (d #t) (k 0)))) (h "13j21n698gq8czxi1bzn0bvhzsdlsi7nffzxf87z579klfh6bqvy")))

(define-public crate-srv-shim-0.1.2 (c (n "srv-shim") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "plumber") (r "^0.0.6") (d #t) (k 0)))) (h "0k910x66g1ra172fvs5yzkz4jcv7ryyp8fh2z1mshj1c08yjqyzk")))

