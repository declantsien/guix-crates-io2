(define-module (crates-io sr s2 srs2dge-presets) #:use-module (crates-io))

(define-public crate-srs2dge-presets-0.2.0 (c (n "srs2dge-presets") (v "0.2.0") (d (list (d (n "srs2dge-core") (r "^0.2") (d #t) (k 0)) (d (n "srs2dge-res") (r "^0.2") (d #t) (k 0)))) (h "0vfpkn9hn9a8493prngbxrdzsjaf19760wymvfb0bkgnlxspvasx")))

