(define-module (crates-io sr s2 srs2dge-link-static) #:use-module (crates-io))

(define-public crate-srs2dge-link-static-0.2.0 (c (n "srs2dge-link-static") (v "0.2.0") (d (list (d (n "srs2dge-core") (r "^0.2") (d #t) (k 0)) (d (n "srs2dge-ecs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "srs2dge-gizmos") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "srs2dge-presets") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "srs2dge-res") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "srs2dge-text") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1z7gcr16hd8khbqkbi4b1ahgp8b19izib3pvx9hcwdvv0cazal78") (f (quote (("text" "srs2dge-text") ("spirv" "srs2dge-core/spirv") ("res" "srs2dge-res") ("presets" "srs2dge-presets") ("glsl" "srs2dge-core/glsl") ("gizmos" "srs2dge-gizmos") ("ecs" "srs2dge-ecs") ("default"))))))

