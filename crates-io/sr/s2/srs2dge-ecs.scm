(define-module (crates-io sr s2 srs2dge-ecs) #:use-module (crates-io))

(define-public crate-srs2dge-ecs-0.2.0 (c (n "srs2dge-ecs") (v "0.2.0") (d (list (d (n "legion") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "legion") (r "^0.4") (f (quote ("codegen" "wasm-bindgen" "serialize"))) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "srs2dge-core") (r "^0.2") (d #t) (k 0)))) (h "0dnk5gq691ds25nj2gd4cmwczb6rk52p7rdyqczn9gg2w9h4wsci")))

