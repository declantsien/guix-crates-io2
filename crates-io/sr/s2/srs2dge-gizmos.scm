(define-module (crates-io sr s2 srs2dge-gizmos) #:use-module (crates-io))

(define-public crate-srs2dge-gizmos-0.2.0 (c (n "srs2dge-gizmos") (v "0.2.0") (d (list (d (n "srs2dge-core") (r "^0.2") (d #t) (k 0)) (d (n "srs2dge-presets") (r "^0.2") (d #t) (k 0)) (d (n "srs2dge-text") (r "^0.2") (d #t) (k 0)))) (h "0bx39wfnc0k3gdsp8ihrz8mzkmznv8cq6w3k60aflxlwfd6sdv0i")))

