(define-module (crates-io sr t- srt-rs) #:use-module (crates-io))

(define-public crate-srt-rs-0.1.0 (c (n "srt-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)))) (h "1m9xkrilvdbg4yiw9rk9sgrn62gqn7il9s7jjhp8am6wwpn5cq2h")))

(define-public crate-srt-rs-0.1.1 (c (n "srt-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)))) (h "0bxvpdayjb35y14wzm06s253a2234qqk7lzw2dwp43xx1xsrm6v8")))

(define-public crate-srt-rs-0.1.2 (c (n "srt-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)))) (h "0163ka55g6n3r7z1n77y35x2hykbxgflk9ipkhy4ww5yx9wprcyi")))

(define-public crate-srt-rs-0.1.3 (c (n "srt-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)))) (h "02mwnx42d3bmcp2iapzp3n0z6inf0ck1kfz7mimwllxfqpj1aw95")))

(define-public crate-srt-rs-0.1.4 (c (n "srt-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09pbrwykgqjh8d2wdn8l51kqx6lnm8wpqh5q2pzxrgv8ggnsyc7x")))

(define-public crate-srt-rs-0.1.5 (c (n "srt-rs") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "003wdp805bpk6a07g9vm9m5sbadlr7jgfnhf2kc6z3dhwlvxqvhy")))

(define-public crate-srt-rs-0.1.6 (c (n "srt-rs") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "175km6zqyiad8h5209ik7w0vw8v8klhxxlx45x9qjyv8xyy2m4r1")))

(define-public crate-srt-rs-0.1.7 (c (n "srt-rs") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0176yj2vgnpa1lz4bhjqx3lk3hjhzqj2ryx54r7r0pbzf12m1vgm")))

(define-public crate-srt-rs-0.1.8 (c (n "srt-rs") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jc531mcb9bgn2kbybj4b4pwgh9p2hd1cspy1cr4rx4hh0b0sy0n")))

(define-public crate-srt-rs-0.1.9 (c (n "srt-rs") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.12") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z2781cdq8irpzpa6pj5cwl7wnf08mivrrlfsadb2c9vi2dvkzy9")))

(define-public crate-srt-rs-0.1.10 (c (n "srt-rs") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sk1dkbn5q39h9l8p49v0cc79cgi1k231s7318sgia21cl71fqhp")))

(define-public crate-srt-rs-0.1.11 (c (n "srt-rs") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xb47aik1accw35vig8hw03636ckwl29yjggxy1k0j2gh5bxscrl")))

(define-public crate-srt-rs-0.1.12 (c (n "srt-rs") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "139z934nwdcl7dxj1p2bicazjfm5921abzj9g3wmjmairycqzg3a")))

(define-public crate-srt-rs-0.2.0 (c (n "srt-rs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zh8lyyvi9bjwhhzbhdjwxpkx2bdzab31jfxah787wnwclc4xjaj")))

(define-public crate-srt-rs-0.2.1 (c (n "srt-rs") (v "0.2.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1q3q79jylckfypx3fi8damvq1cyi4fmwd0na9z2vwfbf4zp71dnv")))

(define-public crate-srt-rs-0.2.2 (c (n "srt-rs") (v "0.2.2") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0571kaz6f7vish4rlcqdlhijd3d6sp6bz9aj06qcfr5ldwagv6wd")))

(define-public crate-srt-rs-0.2.3 (c (n "srt-rs") (v "0.2.3") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2" "ws2def" "ws2ipdef" "inaddr" "in6addr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sb5dw3wada096pcwc67695vd0c5gyr8607chhiczzb5wz9i2fvd")))

(define-public crate-srt-rs-0.2.4 (c (n "srt-rs") (v "0.2.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsrt-sys") (r "^1.4.13") (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mwh2yv8fvln71z6q0qj7384c8j748rw0faavyyhazm0jqikxd0q") (y #t)))

