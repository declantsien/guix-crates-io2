(define-module (crates-io sr to srtool-cli) #:use-module (crates-io))

(define-public crate-srtool-cli-0.10.0 (c (n "srtool-cli") (v "0.10.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "srtool-lib") (r "^0") (d #t) (k 0)))) (h "0cpllhnmjf99vqm44y8z5s5ddz5svddgf6xnkbn3yn50h5srym29")))

(define-public crate-srtool-cli-0.11.0 (c (n "srtool-cli") (v "0.11.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "srtool-lib") (r "^0") (d #t) (k 0)))) (h "083anb5i0fl4w9l8qb30ksldqap2xjjsl22l06kjz5msq5d8r6dr")))

(define-public crate-srtool-cli-0.12.0 (c (n "srtool-cli") (v "0.12.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "srtool-lib") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cm4dpmbwzh31bd4xxfnisa63yhxf3xijipmjcn72zfk4523ch6j")))

