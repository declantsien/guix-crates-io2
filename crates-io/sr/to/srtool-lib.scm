(define-module (crates-io sr to srtool-lib) #:use-module (crates-io))

(define-public crate-srtool-lib-0.9.0 (c (n "srtool-lib") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "19q3zzxw2nbls2vc5h6cz2k274x66z32swi9ggnqs44rjd674hv0")))

(define-public crate-srtool-lib-0.11.0 (c (n "srtool-lib") (v "0.11.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "174rvzv0hfg7kkgkrvdzyrij5c0n401m3d722zgvdbmd75xchwpx")))

(define-public crate-srtool-lib-0.12.0 (c (n "srtool-lib") (v "0.12.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "15lw6l136sfrb8aprwxyf5agz9789ynks5rqnli4s22915rdgqpr")))

