(define-module (crates-io sr fn srfng) #:use-module (crates-io))

(define-public crate-srfng-0.1.0 (c (n "srfng") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0fipxg9vsqk1nrshjjr5lv9h23v4qx1zcv86090v3qsx191g5jsb")))

(define-public crate-srfng-1.0.0 (c (n "srfng") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w9arqi2y1ji3cixnziiq8md8zma95nclsc3mbwz4xpimpscfsg9")))

(define-public crate-srfng-1.0.1 (c (n "srfng") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dcffs0v9s1yhqg8cjwjbi8n4195b54ahbszgrmnkfm9nyvaz4a7")))

