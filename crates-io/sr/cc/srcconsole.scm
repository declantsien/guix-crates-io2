(define-module (crates-io sr cc srcconsole) #:use-module (crates-io))

(define-public crate-srcconsole-0.1.0 (c (n "srcconsole") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "winnt" "minwindef" "processthreadsapi" "psapi" "errhandlingapi" "memoryapi" "winbase" "synchapi" "handleapi" "minwinbase"))) (d #t) (k 0)))) (h "0y5bzra9r3d2k24lpjfz7dyxddlggs9shfxww6r5x07dwpzspnif")))

(define-public crate-srcconsole-0.2.0 (c (n "srcconsole") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "winnt" "minwindef" "processthreadsapi" "psapi" "errhandlingapi" "memoryapi" "winbase" "synchapi" "handleapi" "minwinbase"))) (d #t) (k 0)))) (h "11510wgavbffxl57isc869l0d8v25q0hihkr5al95k0nsm16z9r3")))

