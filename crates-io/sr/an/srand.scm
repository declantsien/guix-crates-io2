(define-module (crates-io sr an srand) #:use-module (crates-io))

(define-public crate-srand-0.1.0 (c (n "srand") (v "0.1.0") (h "1fd0nb4p466rw5rw7pj212lz9f16f24lnh1jxa51vg85p01kylqh")))

(define-public crate-srand-0.2.0 (c (n "srand") (v "0.2.0") (h "173i5iakmm4rqr9fr8s8n5icqdrsf62j5kd3mbpjya29r5ksk98n")))

(define-public crate-srand-0.2.1 (c (n "srand") (v "0.2.1") (h "0lycp7pnsvsvc1aqjzrdp9251b8956pxma462wnh4nwwml9k8jsy")))

(define-public crate-srand-0.3.0 (c (n "srand") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "17zl7qw711yyvwh25cq8rni07xf67lavlmlz686m64lcy0xma95k")))

(define-public crate-srand-0.3.1 (c (n "srand") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fm3glh31jxpza97khn88pfz5737cfyqmkapzzwm8wpc7gclsq9b")))

(define-public crate-srand-0.3.2 (c (n "srand") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yf3w4kphx4rpghkv3016p8b03xr4vpxgksab28flyxbhmvxsql9")))

(define-public crate-srand-0.4.0 (c (n "srand") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "140v8f177cfn8fizaw1bwkr5f549pyk8ba6hwmh982xhv835qjxz")))

