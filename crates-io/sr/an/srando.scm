(define-module (crates-io sr an srando) #:use-module (crates-io))

(define-public crate-srando-0.1.0 (c (n "srando") (v "0.1.0") (h "0ybrxh7w012k908prhc5f4nkg5301i3g3zmqxzzrvh7n7kdn8l49")))

(define-public crate-srando-1.0.0 (c (n "srando") (v "1.0.0") (h "1ahgwc2f4j9zgq4d4lnzq1q57g61y61qn4z5z41qz7vxdlqhaqnm") (y #t)))

(define-public crate-srando-0.2.0 (c (n "srando") (v "0.2.0") (h "0nxh7140njgisw1ckim8dc0jsf7wkgqli7xdc3pw30jildcpqsg5")))

