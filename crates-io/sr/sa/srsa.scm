(define-module (crates-io sr sa srsa) #:use-module (crates-io))

(define-public crate-srsa-0.1.0 (c (n "srsa") (v "0.1.0") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "1avyfi8yqm88qxp57r7rn7adw4fjwf25igryhkm4g24zzvxqhxva")))

(define-public crate-srsa-0.1.1 (c (n "srsa") (v "0.1.1") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "133qhs6680jjjcfy23rsn3d76lv5h9xypd0vvij8vhhkhfppipc5")))

(define-public crate-srsa-0.1.2 (c (n "srsa") (v "0.1.2") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "1jiq3fxkk0z4jv7441pdqvccli3350sy544964n6zfvlzsdp0n85")))

(define-public crate-srsa-0.1.3 (c (n "srsa") (v "0.1.3") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "009hl4c55xnqy3vsnki1vdxzyc825ib6l5fifbyi3sxayaqihf85")))

(define-public crate-srsa-0.1.4 (c (n "srsa") (v "0.1.4") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "0rhzy44rnbcw5m7mb3qknabjyx3n7pdi1jphkx2sw092csbzpifq")))

(define-public crate-srsa-0.1.5 (c (n "srsa") (v "0.1.5") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "1qwg1q95p4ddvm0lhl1f31vdl1fpnsg80qccnw92r5fv4xhsvcwv")))

(define-public crate-srsa-0.1.6 (c (n "srsa") (v "0.1.6") (d (list (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "1kys6smpx4wf5vkwqsv9fydhzslkcnyq8f209b55mwj1w17xwz1k")))

(define-public crate-srsa-0.1.7 (c (n "srsa") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "pkcs8") (r "^0.9") (f (quote ("encryption" "pem" "std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (f (quote ("serde" "pem"))) (d #t) (k 0)))) (h "0bx50nxwv01fgsfxjw8bcfwjxig39bfqfy69zvm3ha74hb1azkvw")))

