(define-module (crates-io sr is srisum) #:use-module (crates-io))

(define-public crate-srisum-1.0.1 (c (n "srisum") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "ssri") (r "^1.3.0") (d #t) (k 0)))) (h "0b2s4w5ivawq9b3wq0y0995izng6mdysccjiyjldqk8g48sbp1sl")))

(define-public crate-srisum-2.0.0 (c (n "srisum") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "ssri") (r "^5.0.0") (d #t) (k 0)))) (h "1ih1k31xf0hc4ng3q6p1zhvfccfg7ppvhiblapbfcmhh1j19fx77")))

(define-public crate-srisum-3.0.0 (c (n "srisum") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssri") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "04cqr242rn75ilynsabp7qp1h6qm5b45f1sdpxn77dbj555xkl17")))

(define-public crate-srisum-4.0.0 (c (n "srisum") (v "4.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^4.7.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "ssri") (r "^7.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qz7wxcck4i3iwvlml0q94hsyikxyhg7hj8hsm2vj5wcljgvi0m4")))

(define-public crate-srisum-5.0.0 (c (n "srisum") (v "5.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "ssri") (r "^7.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hdh4zzxcv6dy8lg4pa810x2xj745318s5ffqmgysqhb6f7rhbcm")))

