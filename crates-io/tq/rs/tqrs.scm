(define-module (crates-io tq rs tqrs) #:use-module (crates-io))

(define-public crate-tqrs-0.1.0 (c (n "tqrs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.8") (d #t) (k 0)) (d (n "tqdm") (r "^0.5.0") (d #t) (k 0)))) (h "0jm417y3vxk60xpgx3f05h5aip84g9pjysgjhqx126agb2gxvzhr") (y #t)))

