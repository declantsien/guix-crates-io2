(define-module (crates-io tq -c tq-code-generator-core) #:use-module (crates-io))

(define-public crate-tq-code-generator-core-0.1.0 (c (n "tq-code-generator-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yxdvld444f1zr26vsbl403nasniarvdh76wm47swrsc8cr20nb7")))

(define-public crate-tq-code-generator-core-0.1.1 (c (n "tq-code-generator-core") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yb5wgrljkjgrw96yd3cp0wb9yqnf2wmd0xgd82ssigqc3m85m6g")))

