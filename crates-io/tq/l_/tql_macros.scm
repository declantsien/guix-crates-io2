(define-module (crates-io tq l_ tql_macros) #:use-module (crates-io))

(define-public crate-tql_macros-0.0.1 (c (n "tql_macros") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0.33") (d #t) (k 0)))) (h "1cs08my3dal6qzm8zz50f6c54rn980s1hjsysc9lggza6h8f4yhk")))

(define-public crate-tql_macros-0.1.0 (c (n "tql_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "printing" "full"))) (d #t) (k 0)))) (h "1114ayb2888mgrc3j7f9mamz5js47y31avqb2k9xqj04n77ishva") (f (quote (("unstable" "proc-macro2/nightly") ("rusqlite") ("postgres"))))))

