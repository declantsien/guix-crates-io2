(define-module (crates-io tq dm tqdm) #:use-module (crates-io))

(define-public crate-tqdm-0.1.0 (c (n "tqdm") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ykjyn4dggsglb234mba998b4wdv9pyjnf5w95y1dzhvlji5nbbj")))

(define-public crate-tqdm-0.1.1 (c (n "tqdm") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1i0j75syr65lwcq23rrvdhf2snjxm62xpwz1ylkb0lvab5jc28w4")))

(define-public crate-tqdm-0.2.0 (c (n "tqdm") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0c0p2bp6nv7n5gw44c93dm2r5w47mkfap8ksp2ypw59x1nml3gk0")))

(define-public crate-tqdm-0.2.1 (c (n "tqdm") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0jfsa5iarnmcpzc6g3xl2z15d8l50j9kbdgacpigb9y6apfr1w3k")))

(define-public crate-tqdm-0.2.2 (c (n "tqdm") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1chrr22c86iy1khf792kahh8yss7jcfh6b5y2ycxlrxqi12rla8f")))

(define-public crate-tqdm-0.2.3 (c (n "tqdm") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1fa1fgf5hf9wn1kh6r187b9dpi753ihpmsswm228v9z4qk796kjr") (y #t)))

(define-public crate-tqdm-0.3.0 (c (n "tqdm") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1q7np8cjk9f5kyg3znxipmpy238166wf8vx388p9jb502mwx7ihq")))

(define-public crate-tqdm-0.3.1 (c (n "tqdm") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1y9pdxmc4ayr4l33dm627zxvz0qrzw17bwvg4x3zvrwsj7hh7gb5")))

(define-public crate-tqdm-0.4.0 (c (n "tqdm") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1pxp8np98lgs345hbpcrrv5pm8bwvp5j8v8agv32gl7wazgyf20g")))

(define-public crate-tqdm-0.4.2 (c (n "tqdm") (v "0.4.2") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "18zbkxxs9y77hnzd9zjzmpfln6zaiy1nszf6dfmja36ng98wh4rv")))

(define-public crate-tqdm-0.4.3 (c (n "tqdm") (v "0.4.3") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "1bn5g6jhjppy4vizwa7f1h8db78f2hbdq4sljgbbvy20izf8fah7")))

(define-public crate-tqdm-0.4.4 (c (n "tqdm") (v "0.4.4") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "08vkggsi8vq4n49a369ipmpc7n2vzjpiifiznaalbzdzwv5ns1bg") (r "1.63.0")))

(define-public crate-tqdm-0.5.0 (c (n "tqdm") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "1kqagvrs6f5fz1f873im1whn4hhrb608br0b2laq73c0jzkk85kh") (r "1.70.0")))

(define-public crate-tqdm-0.5.1 (c (n "tqdm") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)))) (h "1zp6293lm0b3f9ca183r9xkj2s5l2f137wc80hbjb0hx7sxfgccd") (r "1.70.0")))

(define-public crate-tqdm-0.5.2 (c (n "tqdm") (v "0.5.2") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1bpzdqwswv4zsl6374xryhzl1980zm2k6f47bmma297vczwzfipb") (r "1.60")))

(define-public crate-tqdm-0.6.0 (c (n "tqdm") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07sjznak9i74d0n1fzg4msa8nccim9xn6br7pf5vc3l38vm4m7lw") (r "1.60")))

(define-public crate-tqdm-0.7.0 (c (n "tqdm") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.18") (f (quote ("alloc" "std"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07v1vjv2va8p3wx0na7vr6wgp59ck5hrhpgicndsj1824hr2jbda") (r "1.60")))

