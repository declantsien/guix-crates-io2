(define-module (crates-io tq dm tqdm_show) #:use-module (crates-io))

(define-public crate-tqdm_show-0.1.0 (c (n "tqdm_show") (v "0.1.0") (d (list (d (n "tapciify") (r "^3.3.0") (d #t) (k 0)) (d (n "tqdm") (r "^0.7.0") (d #t) (k 0)))) (h "0ryl0d0kzmxcbyhzh6ahx1vn3vwcp815aadj8siq46d0h5c0hsf0")))

