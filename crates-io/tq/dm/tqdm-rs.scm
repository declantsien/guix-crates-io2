(define-module (crates-io tq dm tqdm-rs) #:use-module (crates-io))

(define-public crate-tqdm-rs-0.0.1 (c (n "tqdm-rs") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)))) (h "0x8c3sp3nwjjifzxdpcf6jwjnndd9dmfcy7gci4bazpra409wipm")))

(define-public crate-tqdm-rs-0.1.0 (c (n "tqdm-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)))) (h "1mz21i2grvsrgjph967ddf9lxj0s5915j77zgli6df15f75fm78g")))

(define-public crate-tqdm-rs-0.1.1 (c (n "tqdm-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)))) (h "1fn2bs5w17prm40r37drjwhvwg4b3rbk9g04yp0ji1jlsaifm7p5")))

