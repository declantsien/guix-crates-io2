(define-module (crates-io tq db tqdb) #:use-module (crates-io))

(define-public crate-tqdb-1.0.0 (c (n "tqdb") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wr783dhn64n0jxqlf01h0dlgi3mzwh10ndcd19n5kfg5i05931s") (y #t)))

(define-public crate-tqdb-1.0.1 (c (n "tqdb") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jyjfmrizdpg4ygdid7h8sy9qj2my1fj27xvhj6zjcrfhfmwrvk8") (y #t)))

(define-public crate-tqdb-1.0.2 (c (n "tqdb") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p2j4fb9zdnwhjxwky0yzyfkn1p9lcb5b40f5mdhgxs0q67mv4xq") (y #t)))

(define-public crate-tqdb-1.0.3 (c (n "tqdb") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1baqxsp1f2b72q2b338rpnyvm2azzwkp35nxmmwl2ijhhyqmnff7") (y #t)))

(define-public crate-tqdb-1.0.4 (c (n "tqdb") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f3yk5l6xyxl7l16cbbbmssy3d9dq6361dlf19krxv4wn9slp6qn") (y #t)))

(define-public crate-tqdb-1.0.5 (c (n "tqdb") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q3md2yv3icvz9yyxia3y757qa9a0immyjynqbljk7vx01jh9myd") (y #t)))

(define-public crate-tqdb-1.0.6 (c (n "tqdb") (v "1.0.6") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "099la6jcx03qmkip7ckk8virsi1lqxqzwxlas6qyj65rajnghrzm")))

