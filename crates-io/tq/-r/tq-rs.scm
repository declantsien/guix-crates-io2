(define-module (crates-io tq -r tq-rs) #:use-module (crates-io))

(define-public crate-tq-rs-0.0.0 (c (n "tq-rs") (v "0.0.0") (h "1ys7shn99f3yp33jw41dxnljz7h95lrkpy4nf7pcg6m40kdclqr7")))

(define-public crate-tq-rs-0.1.0 (c (n "tq-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "usage" "help"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1h9jjxmx95lgcmgpfpp0dvx5pcycva7f2qva9gaayd9j1wyg5rfi")))

(define-public crate-tq-rs-0.1.1 (c (n "tq-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "usage" "help"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0hskc501dlrf0vj479wrwc20zrffhs20iqlhlj5wvwz19qhanzfs")))

(define-public crate-tq-rs-0.1.2 (c (n "tq-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "usage" "help"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "16vkfiz0b4wz8avkzza9bpxzzai5l660pap6m8qml1jcrif87x37")))

