(define-module (crates-io r3 ma r3ma_backup) #:use-module (crates-io))

(define-public crate-r3ma_backup-0.1.0 (c (n "r3ma_backup") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "salsa20") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "138k4bv4j91sah98d2hfh9fw5x14xd4xsp4rzmblv48r4188mcbq")))

