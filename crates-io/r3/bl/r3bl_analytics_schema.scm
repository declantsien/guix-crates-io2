(define-module (crates-io r3 bl r3bl_analytics_schema) #:use-module (crates-io))

(define-public crate-r3bl_analytics_schema-0.0.1 (c (n "r3bl_analytics_schema") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "001fmr2ddxvyxmald9zx20wyw50pmdpwj3apavw9wgrjy6lslyd2")))

