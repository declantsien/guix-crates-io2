(define-module (crates-io r3 bl r3bl_simple_logger) #:use-module (crates-io))

(define-public crate-r3bl_simple_logger-0.1.0 (c (n "r3bl_simple_logger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.3.29") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "14fcvv4iamjnavw2fcj8653lw2bzvrml99iar4l48gjvdrsngcl3")))

(define-public crate-r3bl_simple_logger-0.1.1 (c (n "r3bl_simple_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "r3bl_ansi_color") (r "^0.6.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.3.29") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0smq69xii7xnzq3abih1zx80w2iadn5m75cz7gjbi5jf2xxfsnv6")))

(define-public crate-r3bl_simple_logger-0.1.2 (c (n "r3bl_simple_logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "r3bl_ansi_color") (r "^0.6.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1cg20954bkl1faabc19a3yi7hjdgv191sjbl4lm4k62nr7py6xhf")))

(define-public crate-r3bl_simple_logger-0.1.3 (c (n "r3bl_simple_logger") (v "0.1.3") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "r3bl_ansi_color") (r "^0.6.9") (d #t) (k 0)) (d (n "termcolor") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1m64092lbv777yxncyq8f70is5b1pm2v486b84h179x6zdpk5syz")))

