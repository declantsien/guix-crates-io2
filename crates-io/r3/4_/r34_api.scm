(define-module (crates-io r3 #{4_}# r34_api) #:use-module (crates-io))

(define-public crate-r34_api-0.2.0 (c (n "r34_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pi42hzn2sl5ykfq6ja93vmi50382rbl42kscbp54vgh5icism4p") (y #t)))

(define-public crate-r34_api-0.2.1 (c (n "r34_api") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0lhk42mcar9j1vkv740spmw00i22jqxh7dfq6pbww20s8wdk5csh") (y #t)))

(define-public crate-r34_api-0.2.4 (c (n "r34_api") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0xib49fxj9c3d2l4gg38ppsp128qaacbhmf3sx2hnjpbzmdd24gg")))

(define-public crate-r34_api-0.2.5 (c (n "r34_api") (v "0.2.5") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0nz6aajwns0nwkzl6b41lli6wy323lmsjiwzswp82s0qqllqp7cp")))

(define-public crate-r34_api-0.2.6 (c (n "r34_api") (v "0.2.6") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1q4qfa2vl88lj4hscm2gipxq0h27kbk6755mwas2y1a1gyy2ks4q")))

(define-public crate-r34_api-0.2.61 (c (n "r34_api") (v "0.2.61") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "162xxrw96m0pmh7gmn489689ksbak8wrsh72y26bqbk9ydr3fbk8") (y #t)))

(define-public crate-r34_api-0.2.62 (c (n "r34_api") (v "0.2.62") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "13kjgh8hhwxf70ajigpyqwb22j5ryl597kk3a2s9ncibc75cdg2a")))

(define-public crate-r34_api-0.2.7 (c (n "r34_api") (v "0.2.7") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0s777039lmlpgr5mp55fgrs6b1kd7n3rshrjd6qxk2dqjc0m2qg6") (y #t)))

(define-public crate-r34_api-0.3.0 (c (n "r34_api") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0hgy4qqvc9pbxnfrl2i1p753lc94cp18nz0cxrwgkakw7121i4jw")))

