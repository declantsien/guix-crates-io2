(define-module (crates-io r3 me r3mem) #:use-module (crates-io))

(define-public crate-r3mem-0.1.0 (c (n "r3mem") (v "0.1.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "111yk6wwymzjvrxfs0zwz6b2dllscq7vq7yjhjna7zx931cjz9ih") (y #t)))

(define-public crate-r3mem-1.0.0 (c (n "r3mem") (v "1.0.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "1n89bn08y1grf7nnv25zy56wjch9x0i8k8lpgbhp7fcdpg5g9w5y") (y #t)))

(define-public crate-r3mem-1.0.1 (c (n "r3mem") (v "1.0.1") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "1vi8xffqwl7w9fza9qd5j5f10gyxza02h0wai8rzzk1gwpjq7bad") (y #t)))

(define-public crate-r3mem-0.0.1 (c (n "r3mem") (v "0.0.1") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "0hdi13dr57g5i9xkd9cfdbdn6akkkxpz5ni2kv9g16zv5pnj04jx") (y #t)))

(define-public crate-r3mem-0.1.1 (c (n "r3mem") (v "0.1.1") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_Diagnostics_ToolHelp" "Win32_System_Threading" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "1lm6i2s19r6z85x906a19342vda9bayvgwwjaqnpd8wq0g3cd5sz")))

