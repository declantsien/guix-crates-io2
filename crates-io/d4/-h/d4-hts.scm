(define-module (crates-io d4 -h d4-hts) #:use-module (crates-io))

(define-public crate-d4-hts-0.1.1 (c (n "d4-hts") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1fgy3ns4plp1f3xhz2bb61zk1ad7pspvha67zv6dn7828r2r4ikp")))

(define-public crate-d4-hts-0.1.2 (c (n "d4-hts") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1j80jr8jkrxii9n570xm23vg4qs5ysd424d6nmjsw8kbgw8rl4q7")))

(define-public crate-d4-hts-0.1.8 (c (n "d4-hts") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0fsi5kdwslbnq6n21x4w0ppfyxmvvcxm684gpcww7h3xd7n9kal0")))

(define-public crate-d4-hts-0.1.9 (c (n "d4-hts") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1qvjsf2ivj0vv7645zv1wnn2j4qfdiaf705xvzjndvfjqnh0alss")))

(define-public crate-d4-hts-0.1.11 (c (n "d4-hts") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1sy2y80x65w5x4kx5hqc50ahnhb1mgz0rh76ni2x6mfidrcd3b7r")))

(define-public crate-d4-hts-0.1.12 (c (n "d4-hts") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1vs4mjp1131ksw7rr2ah9hhfyjnvviis67mz0czczsx56pac7k64")))

(define-public crate-d4-hts-0.1.13 (c (n "d4-hts") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1ml3aiwj13gnn1rc6ngkwrdds6f6vgi4j2saz26sydrsc35knagy")))

(define-public crate-d4-hts-0.1.14 (c (n "d4-hts") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "03mg1br9gmpsjqnyb6c18b1j978zpz2bwr2wmk0512vjxkj6gbwc")))

(define-public crate-d4-hts-0.1.15 (c (n "d4-hts") (v "0.1.15") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1xmmwdc1f1wmdk8k34s052apv69nf8n7wrs4xfjsy62qx12m2shi")))

(define-public crate-d4-hts-0.1.16 (c (n "d4-hts") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1hbk0rafmk8qiy6ry1wrs3ssyf5l4kgbx2bwv42pswmh5qs4x0yn")))

(define-public crate-d4-hts-0.1.17 (c (n "d4-hts") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0qq1nyrk14y6nx1sl61hv7fpqy6w3f6xshijs5w3clnl2ppjclpf")))

(define-public crate-d4-hts-0.1.18 (c (n "d4-hts") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1sw0a84gs41rzckkr9cvw8sm5pl2g77hv8p5i28r8ld1sj05n11z")))

(define-public crate-d4-hts-0.2.17 (c (n "d4-hts") (v "0.2.17") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0virpy8gjc1vv28rwh6fi5k1wr1p34hrvlfrmpqwicx0gj86kkli")))

(define-public crate-d4-hts-0.2.18 (c (n "d4-hts") (v "0.2.18") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "12w03jywpi8rph7f4qah2826q5gn3n0sbc6l7h0ym7zipjm5l4gk")))

(define-public crate-d4-hts-0.3.0 (c (n "d4-hts") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0pyqmjkfa9jc0y3ir2gpim45gr6qh2v8mlcdlja3ifrpcg5ifjfm")))

(define-public crate-d4-hts-0.3.1 (c (n "d4-hts") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1fylg3j8aq6nb9bwf8rivzhp174abmg8ggndpdpkli24vn28aidc")))

(define-public crate-d4-hts-0.3.2 (c (n "d4-hts") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1n40pn7rawcgjmhmxs4f893kkgww1v1314i2pvsi48c998ncvyj0")))

(define-public crate-d4-hts-0.3.3 (c (n "d4-hts") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0cqd2yljw55fvf2v8xd94n1vwyyw9ni9dr9xk9a5pm3628l5zrch")))

(define-public crate-d4-hts-0.3.4 (c (n "d4-hts") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1avaphpy92ay2hvvmnrdj31qhhivx7a7dfxbh2hkqql4rgma8a3q")))

(define-public crate-d4-hts-0.3.5 (c (n "d4-hts") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "132975rscs8wp2zzhrdvhwyvg1wcz07095ivl9b8rqdfcmsibrsj")))

(define-public crate-d4-hts-0.3.6 (c (n "d4-hts") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1fy0pszq2ldhydr7blm4kzgzl642fsfsgwh14ixmag4pd5aaj8y1")))

(define-public crate-d4-hts-0.3.7 (c (n "d4-hts") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "15xkx38avn49vr8vl04wixajgjkfwqpjdsjha7c0vp600p0ww9i9")))

(define-public crate-d4-hts-0.3.9 (c (n "d4-hts") (v "0.3.9") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "10fkksyqx9qnza7dbp02lhnzhq71g9mzb72s879h4lmmx1nczfaq")))

