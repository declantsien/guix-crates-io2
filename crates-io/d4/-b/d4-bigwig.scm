(define-module (crates-io d4 -b d4-bigwig) #:use-module (crates-io))

(define-public crate-d4-bigwig-0.1.0 (c (n "d4-bigwig") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1adw32ad1gfgy4iax8c6h74z159nh7z55bgkh06zh4rd5al62ac1")))

(define-public crate-d4-bigwig-0.1.1 (c (n "d4-bigwig") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1ryw5k89chh7qvw0ggqar58078xbpgpwsv4s4vvk85vhzkmwjig7") (f (quote (("docs_build"))))))

(define-public crate-d4-bigwig-0.1.2 (c (n "d4-bigwig") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "02ll5ymvc6j8044pqbyrsvajgbsqsj5836rwjfrdvfkcgv3ydxip")))

(define-public crate-d4-bigwig-0.1.8 (c (n "d4-bigwig") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0bi21b38qy6yp2a5s9a7i0as0j6yl141jj02qxj5x7ixl315szi5")))

(define-public crate-d4-bigwig-0.1.9 (c (n "d4-bigwig") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1kyvcxlhbjs6hwqdgjwj2ca61yswdr5c1pqb1ss5xkdxvjja75p2")))

(define-public crate-d4-bigwig-0.1.11 (c (n "d4-bigwig") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0r06p9n4dzjbsn5ys4z5g4a84sbz8kanwp8hzk5ddr29zgzrig6j")))

(define-public crate-d4-bigwig-0.1.12 (c (n "d4-bigwig") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1a0c7kymwknf0j1sxdxygdaq2i9cqhva7fd7wb1zc0sm50v8n53k")))

(define-public crate-d4-bigwig-0.1.13 (c (n "d4-bigwig") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0s9cn3rcvg8kfgslypfmppz787xj698jvrnj9ks3w6hfn4za2lbr")))

(define-public crate-d4-bigwig-0.1.14 (c (n "d4-bigwig") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "035brhjh9rmwyhysccwararnh9bzqz1n8462lg8hxb94m9zwh2bw")))

(define-public crate-d4-bigwig-0.1.15 (c (n "d4-bigwig") (v "0.1.15") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0y2q154l4snb7ml7bd09sa4s2746ghr51aqb34hkgbadwd4axwgj")))

(define-public crate-d4-bigwig-0.1.16 (c (n "d4-bigwig") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "073g2yf9c72g55ily60brh6jixpc2cdpzis1l1ragx1xp27h9wpi")))

(define-public crate-d4-bigwig-0.1.17 (c (n "d4-bigwig") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1hv8n02jchj55n548hy5ikcv0j686kx8f5alxcbdg7chwyg36x4n")))

(define-public crate-d4-bigwig-0.1.18 (c (n "d4-bigwig") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0k78bn9h2hdkw15qsai51w47kw4aqqqmkwwr26m7z54zdifz5csi")))

(define-public crate-d4-bigwig-0.2.17 (c (n "d4-bigwig") (v "0.2.17") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0ypg04qb4s0k0yx9shl7wwl7mw0l8ji8rgf3hadjjkdyfdjw7v16")))

(define-public crate-d4-bigwig-0.2.18 (c (n "d4-bigwig") (v "0.2.18") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "00h30kq0cn4g297dd9p7i80nh153qkxkk2s94za13arc2k8yr22v")))

(define-public crate-d4-bigwig-0.3.0 (c (n "d4-bigwig") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0dj8b3f1qs7imv2g1my4mbnq2rv27jz3bim1rldg5ziz32vspr6h")))

(define-public crate-d4-bigwig-0.3.1 (c (n "d4-bigwig") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "00dffpihqxiahpw1lmw3xr0c0jhyxawqpyniawrz3s1q54w5wsh9")))

(define-public crate-d4-bigwig-0.3.2 (c (n "d4-bigwig") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1sjflwsdv7kbp149aby8ixi7j6kcr55qmglpvsblpc7wq14xlak8")))

(define-public crate-d4-bigwig-0.3.3 (c (n "d4-bigwig") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0zxzzjikmbgy597ma48ghqv5zxy08a8sc7h2sybrfhgzbmn338ak")))

(define-public crate-d4-bigwig-0.3.4 (c (n "d4-bigwig") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "077yk3pq6s1b8i0947cbyqsn511y0y4jsys4pn7wfp631rp1rgkr")))

(define-public crate-d4-bigwig-0.3.5 (c (n "d4-bigwig") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "13pmmqh4850djgl5d3wih5h3wygq6zwjli6bbck5m72g428fmvcx")))

(define-public crate-d4-bigwig-0.3.6 (c (n "d4-bigwig") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0n1pwbs220754q3q6crrjn0ycqab6vhhs35prlhxhyz93zd997fp")))

(define-public crate-d4-bigwig-0.3.9 (c (n "d4-bigwig") (v "0.3.9") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1yrcwzc9gp24xba8vp535m151339pi5lal75lgzwaqjplplw6lvy")))

