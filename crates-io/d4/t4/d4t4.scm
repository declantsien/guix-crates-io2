(define-module (crates-io d4 t4 d4t4) #:use-module (crates-io))

(define-public crate-d4t4-0.1.0 (c (n "d4t4") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0aqzvyciqshn982rqyh0hy37cpi6aflb99l7iv3vgk1zy14p2c64")))

(define-public crate-d4t4-0.2.0 (c (n "d4t4") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0n9376mfj0zzbjmm5m4rv158rc2xxb587yxwcmghz0b9kknfnb4n")))

(define-public crate-d4t4-0.2.1 (c (n "d4t4") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1h9kqwvg5pswmjn7z3a402m44wgciqs666vypz8w32nqz24c47m1")))

(define-public crate-d4t4-0.2.2 (c (n "d4t4") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "12pwrq59bpdjslykfkj9iyrscnhjlaxm8zgvj439bml1dr0n3b68")))

