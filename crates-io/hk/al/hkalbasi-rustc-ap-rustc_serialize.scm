(define-module (crates-io hk al hkalbasi-rustc-ap-rustc_serialize) #:use-module (crates-io))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20221110 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20221110") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "0ima47nxjgy3fqcbb6g23wrqakc0lf0rym3ivd1h9g8hwllm3ylw")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20221111 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20221111") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "1s8zqy6wc53jmcyix1p4nf4v02bk8lsdgyl90q2j7s263sfbjmzw")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20221115 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20221115") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "0rwvmdk2y1wn7gvm4wp5ihwi7slny6a1mfm8h1iflzd5g8pgz7j7")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20221125 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20221125") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "1gv5h98qni83qzcifp2ndisxpvillgkfdam9w4gn69kjbzlddp6w")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20221221 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20221221") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "0lfxm50xk4faimwiawqrs42j38w1hnxjx62ig1ca89ybml67m8q4")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20230516 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20230516") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.9") (d #t) (k 0)))) (h "1j79ck1z4g9d8xhxjjwbh8gha6sfng2r89irdwidlr1mzn78r1y0")))

(define-public crate-hkalbasi-rustc-ap-rustc_serialize-0.0.20230517 (c (n "hkalbasi-rustc-ap-rustc_serialize") (v "0.0.20230517") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thin-vec") (r "^0.2.12") (d #t) (k 0)))) (h "1a9p22l2c4c899k2saj3b4llyi07j13bs5ncb3zx6wmln0q1pm73")))

