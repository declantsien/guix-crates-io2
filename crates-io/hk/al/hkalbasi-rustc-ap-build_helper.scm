(define-module (crates-io hk al hkalbasi-rustc-ap-build_helper) #:use-module (crates-io))

(define-public crate-hkalbasi-rustc-ap-build_helper-0.0.20231214 (c (n "hkalbasi-rustc-ap-build_helper") (v "0.0.20231214") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "07x33rrvq6qrq9i6dib7z1lbqvfzi3gh8gppk6arlyhm7pnrgppc")))

(define-public crate-hkalbasi-rustc-ap-build_helper-0.0.20231215 (c (n "hkalbasi-rustc-ap-build_helper") (v "0.0.20231215") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1s0bj309d5jmjzs2l99hqkg9r8vgpkknssnvgnxadkr9393j3g7n")))

