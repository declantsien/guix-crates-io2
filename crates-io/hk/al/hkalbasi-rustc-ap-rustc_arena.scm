(define-module (crates-io hk al hkalbasi-rustc-ap-rustc_arena) #:use-module (crates-io))

(define-public crate-hkalbasi-rustc-ap-rustc_arena-0.0.20230517 (c (n "hkalbasi-rustc-ap-rustc_arena") (v "0.0.20230517") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0rwzbq4jjv57w2fhhdh71xc3dbp5pkinml98fqggpl1sl6k82b9a")))

