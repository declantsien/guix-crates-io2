(define-module (crates-io hk vd hkvdb) #:use-module (crates-io))

(define-public crate-hkvdb-0.1.0 (c (n "hkvdb") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vcvpqxgc0yqyrsb520kys1la8a7gvy4p9ak080aq02vpg4phqg6")))

(define-public crate-hkvdb-0.2.0 (c (n "hkvdb") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ihb1dmf1fiifnz5rd3j15h2m99vj1xy2xw3063gq2bf8vqh7x84")))

(define-public crate-hkvdb-0.2.1 (c (n "hkvdb") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k8kb4i4w5sxcp27rn33ab60mfvib9zdzw0byq28z5wp4a8lx69d")))

(define-public crate-hkvdb-0.3.0 (c (n "hkvdb") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06vknqz39vi358sfflq6jp73sigvs47vj2d031phbzf3788g0qd0")))

