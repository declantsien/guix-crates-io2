(define-module (crates-io hk q- hkq-art) #:use-module (crates-io))

(define-public crate-hkq-art-0.1.0 (c (n "hkq-art") (v "0.1.0") (h "0jcdhwxx1yh2xll7qp0j2m64ahiakdl0zw12g2w5ayw9iha33y1k")))

(define-public crate-hkq-art-0.1.1 (c (n "hkq-art") (v "0.1.1") (h "14708gx54wzfp45gaxgbns8znk08bnqrl05hidrm767kk1m4cpcv")))

(define-public crate-hkq-art-0.1.2 (c (n "hkq-art") (v "0.1.2") (h "0215pfifpi83d20n5x0j2w10095x1p98h047b07ysmbfj6vzak2n")))

