(define-module (crates-io hk ul hkulibrary) #:use-module (crates-io))

(define-public crate-hkulibrary-0.1.0 (c (n "hkulibrary") (v "0.1.0") (d (list (d (n "authku") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1k009k8j9qnzha4pjiil083fxmc00qnbdf90vqvba7w7gq1jw7f2")))

(define-public crate-hkulibrary-0.2.0 (c (n "hkulibrary") (v "0.2.0") (d (list (d (n "authku") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0c70m77kkvg54b0d17h81ck7x3wl0c04nr957kjwgllj8w06h6a6")))

