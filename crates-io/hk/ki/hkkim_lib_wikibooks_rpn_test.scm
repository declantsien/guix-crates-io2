(define-module (crates-io hk ki hkkim_lib_wikibooks_rpn_test) #:use-module (crates-io))

(define-public crate-hkkim_lib_wikibooks_rpn_test-0.1.0 (c (n "hkkim_lib_wikibooks_rpn_test") (v "0.1.0") (h "1r8nc6xfhrgzb667fy9nsk6287jqwcbqwh256jgvvys4dbl77a9v")))

(define-public crate-hkkim_lib_wikibooks_rpn_test-0.1.1 (c (n "hkkim_lib_wikibooks_rpn_test") (v "0.1.1") (h "14f1pqj9qfwx54kvj918bjiph2x9f28i5ywpdjbim4x5cdjfk2m9")))

