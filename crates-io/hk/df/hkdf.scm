(define-module (crates-io hk df hkdf) #:use-module (crates-io))

(define-public crate-hkdf-0.1.0 (c (n "hkdf") (v "0.1.0") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0sgv2kbzacrsw1bsyfz794vc3nc7cd7qrh3xq6aad91dcmm2yzh4")))

(define-public crate-hkdf-0.1.2 (c (n "hkdf") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "060ghi18sxvb3scx0y55g3y16znsb8i83aq34cbyfr1z2mwra0bi")))

(define-public crate-hkdf-0.2.0 (c (n "hkdf") (v "0.2.0") (d (list (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "0.6.*") (d #t) (k 0)) (d (n "generic-array") (r "0.9.*") (d #t) (k 0)) (d (n "hex") (r "0.2.*") (d #t) (k 2)) (d (n "hmac") (r "0.4.*") (d #t) (k 0)) (d (n "sha-1") (r "0.4.*") (d #t) (k 2)) (d (n "sha2") (r "0.6.*") (d #t) (k 2)))) (h "00p7xcwiiqxgdggn3jdxif9g4pn42k46gy4fpm3ahmjd4gbhd4p8")))

(define-public crate-hkdf-0.3.0 (c (n "hkdf") (v "0.3.0") (d (list (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "0.7.*") (d #t) (k 0)) (d (n "generic-array") (r "0.9.*") (d #t) (k 0)) (d (n "hex") (r "0.2.*") (d #t) (k 2)) (d (n "hmac") (r "0.5.*") (d #t) (k 0)) (d (n "sha-1") (r "0.7.*") (d #t) (k 2)) (d (n "sha2") (r "0.7.*") (d #t) (k 2)))) (h "0c6wzckmss1dycab0xm23xbbzfr8xrvsz4ac4glra63jfj4iavbh")))

(define-public crate-hkdf-0.4.0 (c (n "hkdf") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "0.7.*") (d #t) (k 0)) (d (n "generic-array") (r "0.9.*") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "0.5.*") (d #t) (k 0)) (d (n "sha-1") (r "0.7.*") (d #t) (k 2)) (d (n "sha2") (r "0.7.*") (d #t) (k 2)))) (h "0v1spk10b175ynd5v2k4qaz5vpc3fhaaz6hxskcixd4j7m75cmbf")))

(define-public crate-hkdf-0.5.0 (c (n "hkdf") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "0.7.*") (d #t) (k 0)) (d (n "generic-array") (r "0.9.*") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "0.5.*") (d #t) (k 0)) (d (n "sha-1") (r "0.7.*") (d #t) (k 2)) (d (n "sha2") (r "0.7.*") (d #t) (k 2)))) (h "00cybdmmzl2hzaiis22v40pgwbbz9bqngkwqkmph15ha0xmiwdfw")))

(define-public crate-hkdf-0.6.0 (c (n "hkdf") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "sha-1") (r "0.7.*") (d #t) (k 2)) (d (n "sha2") (r "0.7.*") (d #t) (k 2)))) (h "0b5hq9z03zw5lkc3p0hsla11gpwrazwhr6s55rbadlvif2vvmar0") (f (quote (("std"))))))

(define-public crate-hkdf-0.7.0 (c (n "hkdf") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "sha-1") (r "0.8.*") (d #t) (k 2)) (d (n "sha2") (r "0.8.*") (d #t) (k 2)))) (h "1a5xz2hwhgk9sivnyw88nl0r0ggi5afnal4pnafjvq7liiiw928s") (f (quote (("std"))))))

(define-public crate-hkdf-0.7.1 (c (n "hkdf") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha-1") (r "0.8.*") (d #t) (k 2)) (d (n "sha2") (r "0.8.*") (d #t) (k 2)))) (h "00z5sfqgcma99hy4cy03ipzvf3ql376gfla9y8gkzs5vfvbzks1m") (f (quote (("std"))))))

(define-public crate-hkdf-0.8.0 (c (n "hkdf") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "0.3.*") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha-1") (r "0.8.*") (d #t) (k 2)) (d (n "sha2") (r "0.8.*") (d #t) (k 2)))) (h "1qzsmqrvcmgnrb109qr2mvsmr5c4psm1702vrpcqnj02c408m81z") (f (quote (("std"))))))

(define-public crate-hkdf-0.9.0-alpha.0 (c (n "hkdf") (v "0.9.0-alpha.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.8") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "1bg3mjlxp07frrxsyxzgm7m8h5aps00b3hhb4wrgj799fsddllg5") (f (quote (("std"))))))

(define-public crate-hkdf-0.9.0 (c (n "hkdf") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.8") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "1jdvmf8aadk3s0kn9kk3dj00nprjk9glks5f8dm55r43af34j4gy") (f (quote (("std"))))))

(define-public crate-hkdf-0.10.0-alpha.0 (c (n "hkdf") (v "0.10.0-alpha.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "1h7g1nns311jxw7z5l1whj5plmllvnplp4rpaf50vhc0za1x3gv2") (f (quote (("std"))))))

(define-public crate-hkdf-0.10.0 (c (n "hkdf") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "0kwn3scjvv2x8zc6nz3wrnzxp9shpsdxnjqiyv2r65r3kiijzasi") (f (quote (("std"))))))

(define-public crate-hkdf-0.11.0 (c (n "hkdf") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "crypto-tests") (r "0.5.*") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "0sw8bz79xqq3bc5dh6nzv084g7va13j3lrqf91c10a2wimbnsw01") (f (quote (("std"))))))

(define-public crate-hkdf-0.12.0 (c (n "hkdf") (v "0.12.0") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)))) (h "1lcmyrl1iyzp7ii1w3mjqnxsjhjrsnm61fcpfjshbz5nfyf1xx4l") (f (quote (("std" "hmac/std"))))))

(define-public crate-hkdf-0.12.1 (c (n "hkdf") (v "0.12.1") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)))) (h "139ikabpl47r9jsxvl2m7hrhz2y5ni3sn4hnnbvwrg8dnwk3cv3r") (f (quote (("std" "hmac/std"))))))

(define-public crate-hkdf-0.12.2 (c (n "hkdf") (v "0.12.2") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)))) (h "1xnhlnfc9l6643zacyng1hmiz1jzf6ccak4hhq13i3m600gc72qm") (f (quote (("std" "hmac/std"))))))

(define-public crate-hkdf-0.12.3 (c (n "hkdf") (v "0.12.3") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)))) (h "0dyl16cf15hka32hv3l7dwgr3xj3brpfr27iyrbpdhlzdfgh46kr") (f (quote (("std" "hmac/std"))))))

(define-public crate-hkdf-0.12.4 (c (n "hkdf") (v "0.12.4") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)))) (h "1xxxzcarz151p1b858yn5skmhyrvn8fs4ivx5km3i1kjmnr8wpvv") (f (quote (("std" "hmac/std"))))))

(define-public crate-hkdf-0.13.0-pre.0 (c (n "hkdf") (v "0.13.0-pre.0") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.0") (d #t) (k 0)) (d (n "sha1") (r "=0.11.0-pre.0") (k 2)) (d (n "sha2") (r "=0.11.0-pre.0") (k 2)))) (h "0wd4pqmp570pdxnda8xyhnjnk0iwdjs6qdc7rlkyvnb1a1ifsfwg") (f (quote (("std" "hmac/std")))) (r "1.71")))

(define-public crate-hkdf-0.13.0-pre.1 (c (n "hkdf") (v "0.13.0-pre.1") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.1") (d #t) (k 0)) (d (n "sha1") (r "=0.11.0-pre.1") (k 2)) (d (n "sha2") (r "=0.11.0-pre.1") (k 2)))) (h "1ywlhjmp4hjlh1bsz8a91jj1phq87jxsidgwsy1x3wp07fglfc40") (f (quote (("std" "hmac/std")))) (r "1.71")))

(define-public crate-hkdf-0.13.0-pre.2 (c (n "hkdf") (v "0.13.0-pre.2") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.2") (d #t) (k 0)) (d (n "sha1") (r "=0.11.0-pre.2") (k 2)) (d (n "sha2") (r "=0.11.0-pre.2") (k 2)))) (h "1sd87l8ja1a8w4b3j7qhflmbf2qz8qqmjfr954g5dn9picdb6965") (f (quote (("std" "hmac/std")))) (r "1.72")))

(define-public crate-hkdf-0.13.0-pre.3 (c (n "hkdf") (v "0.13.0-pre.3") (d (list (d (n "blobby") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "=0.13.0-pre.3") (d #t) (k 0)) (d (n "sha1") (r "=0.11.0-pre.3") (k 2)) (d (n "sha2") (r "=0.11.0-pre.3") (k 2)))) (h "032k041bsnyzz4bzf4hkk2j2ac1pkyqh0flv61ngjqn4nmd62pgx") (f (quote (("std" "hmac/std")))) (r "1.72")))

