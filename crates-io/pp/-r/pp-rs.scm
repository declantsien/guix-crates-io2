(define-module (crates-io pp -r pp-rs) #:use-module (crates-io))

(define-public crate-pp-rs-0.1.0 (c (n "pp-rs") (v "0.1.0") (h "0v8cj7rpgl6d2zr1dlayc002cd2782820khar5lw8c5a9rf3g9v6")))

(define-public crate-pp-rs-0.2.1 (c (n "pp-rs") (v "0.2.1") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1vkd9lgwf5rxy7qgzl8mka7vnghaq6nnn0nmg7mycl72ysvqnidv")))

