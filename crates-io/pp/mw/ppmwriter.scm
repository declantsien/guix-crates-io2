(define-module (crates-io pp mw ppmwriter) #:use-module (crates-io))

(define-public crate-ppmwriter-0.1.0 (c (n "ppmwriter") (v "0.1.0") (h "1dgrvpksdfxl9why56naj4m938g3s70mpd1sqb8dprh97ypdickl") (y #t)))

(define-public crate-ppmwriter-0.1.1 (c (n "ppmwriter") (v "0.1.1") (h "080lf3pix0cliixspf2vdn070fphmdz80z0h3wis4l41a91l4d49") (y #t)))

(define-public crate-ppmwriter-0.1.2 (c (n "ppmwriter") (v "0.1.2") (h "1izdni7zklz3ycfd2mmhjs7xhpp9f533mx9504a3i3qs70fby3ji") (y #t)))

