(define-module (crates-io pp js ppjson) #:use-module (crates-io))

(define-public crate-ppjson-0.1.0 (c (n "ppjson") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "json-color") (r "^0.7.1") (d #t) (k 0)) (d (n "pager") (r "^0.14.0") (d #t) (k 0)))) (h "1jwr1frjna7zj14w1x06xw09j05nynvmb9mdysixxdzi4iyq3ja7")))

