(define-module (crates-io pp gg ppgg) #:use-module (crates-io))

(define-public crate-ppgg-0.1.4 (c (n "ppgg") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "jfs") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b3f00j4cnlkkw3g44r8w0ayw6v35bs9vppcmksmivk9x4cphqb7")))

