(define-module (crates-io pp cl ppclient) #:use-module (crates-io))

(define-public crate-ppclient-0.2.2 (c (n "ppclient") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "pprexec") (r "^0.2") (f (quote ("client"))) (k 0)) (d (n "ppserver_def") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rhwaa6f4p46wl41xpyg84ddk9ws8jw9g5cyjj8n1dpkw4vwhp3d")))

