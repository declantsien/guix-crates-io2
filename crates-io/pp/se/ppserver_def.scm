(define-module (crates-io pp se ppserver_def) #:use-module (crates-io))

(define-public crate-ppserver_def-0.2.2 (c (n "ppserver_def") (v "0.2.2") (d (list (d (n "ppcore_def") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bq7m6y8vg8zl103wb930hr5bczz40m86q8m8j09zry0k9gmrll5")))

