(define-module (crates-io pp fs ppfs) #:use-module (crates-io))

(define-public crate-ppfs-0.1.0 (c (n "ppfs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (k 0)))) (h "03cvijcsm6n11kyh59dnzcijzp94czk6ff8pp7h9rq394p5px5js")))

(define-public crate-ppfs-0.1.1 (c (n "ppfs") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (k 0)))) (h "0q7hrz7h5jnj0yfkaczlxmgw5lpjd5da4n4bpa37gm20s4jpxn0l")))

(define-public crate-ppfs-0.1.2 (c (n "ppfs") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (k 0)))) (h "0alwm5n5sy142r5wv51bd6r3zmq7j0qs03h5laj7hg921h5m2rqn")))

(define-public crate-ppfs-0.1.3 (c (n "ppfs") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (k 0)))) (h "0wdjrkwyqbid7kiwj41c4jk5dyaj9aj8fvj5njpdb04144fn1703")))

