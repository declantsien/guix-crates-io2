(define-module (crates-io pp ap ppapi) #:use-module (crates-io))

(define-public crate-ppapi-0.0.0 (c (n "ppapi") (v "0.0.0") (h "14fsp1ima8k0wh4b35wjq6h2pdnphpparc6rsqmb0nr2y402d8zn") (y #t)))

(define-public crate-ppapi-0.1.0 (c (n "ppapi") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "1gi66a14dcjjis9cri2dwmbgxxhihp5nam30wrdzyp4dvpiw86bz")))

(define-public crate-ppapi-0.1.1 (c (n "ppapi") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "17j53s38h8mdwkv5g8hnr1139q5nw3y9vqavrl9sdvib4pn1m88i")))

(define-public crate-ppapi-0.1.2 (c (n "ppapi") (v "0.1.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0r00xgsn0pgi79c8xyqj10ngp7780pmpdr0psvdqq8r91x4fngjz") (f (quote (("pepper") ("default" "pepper"))))))

