(define-module (crates-io pp ap ppapi-tester) #:use-module (crates-io))

(define-public crate-ppapi-tester-0.0.0 (c (n "ppapi-tester") (v "0.0.0") (h "1vrhky7qnjg0fzm3lnzpxh7wdg5s85drygvp6z3f8dbpnn817z4k") (y #t)))

(define-public crate-ppapi-tester-0.0.1 (c (n "ppapi-tester") (v "0.0.1") (h "16bh6l75qg3d8yv8l60mqk6jv1c6vjx0jckvnd2h9mr48wbw92vv")))

