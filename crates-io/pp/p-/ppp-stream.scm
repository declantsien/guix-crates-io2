(define-module (crates-io pp p- ppp-stream) #:use-module (crates-io))

(define-public crate-ppp-stream-0.1.0 (c (n "ppp-stream") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "ppp") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io-util"))) (d #t) (k 2)))) (h "006y9njrshs0f78wc1xikr5by9kxpk3nf4dbbmw3c7qbmhs562l3")))

(define-public crate-ppp-stream-0.1.1 (c (n "ppp-stream") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "ppp") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("io-util"))) (d #t) (k 2)))) (h "0qlra35br9cak3i9nvj7yj4ls0fzj8nvh6mmbf7b6fcf6fw6dfr2")))

