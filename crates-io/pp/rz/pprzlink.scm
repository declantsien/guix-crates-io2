(define-module (crates-io pp rz pprzlink) #:use-module (crates-io))

(define-public crate-pprzlink-0.1.0 (c (n "pprzlink") (v "0.1.0") (h "14m2vfq2psv3js1wr29g1jcmmjizscqvjd4xx6yn9x8xrbjiwz5r")))

(define-public crate-pprzlink-0.2.0 (c (n "pprzlink") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "ivyrust") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "06mnj1vac92pdwvlh2l6va95fj6mifayhychvxn1x056nzswbpjj")))

(define-public crate-pprzlink-0.3.0 (c (n "pprzlink") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "ivyrust") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0nmr9cn411d35c6851rq8j2k1x1r7dj3pzfqvah4pylrap7p2z09")))

(define-public crate-pprzlink-0.3.1 (c (n "pprzlink") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "ivyrust") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1wrl6h3bfz12k4dlb00y7sz5vkjg9yd01wqdragk5fpgg9dzwq0y")))

