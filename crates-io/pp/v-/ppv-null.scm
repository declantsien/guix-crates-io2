(define-module (crates-io pp v- ppv-null) #:use-module (crates-io))

(define-public crate-ppv-null-0.1.1 (c (n "ppv-null") (v "0.1.1") (d (list (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)))) (h "1b9c96x72c0bwsm1nl1x7vsv9jgcyvrx4amr92lgf2wxak78smy0")))

(define-public crate-ppv-null-0.1.2 (c (n "ppv-null") (v "0.1.2") (d (list (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)))) (h "07j2bx3m1k7dbrwjh8qnlmbvdwx81q6z96wmwmxp4q605crx6vsm")))

