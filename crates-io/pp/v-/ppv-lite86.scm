(define-module (crates-io pp v- ppv-lite86) #:use-module (crates-io))

(define-public crate-ppv-lite86-0.1.1 (c (n "ppv-lite86") (v "0.1.1") (d (list (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)))) (h "1b2r7bsn272v6y3ag8874582hin9n9hnpz2gkxhxw2d4lyhag0f2")))

(define-public crate-ppv-lite86-0.1.2 (c (n "ppv-lite86") (v "0.1.2") (d (list (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)))) (h "1lh2nr0gr1g8fmwsg531xcg6hnnwyg08zk1ir2iydbkwwi8cywz1")))

(define-public crate-ppv-lite86-0.2.0 (c (n "ppv-lite86") (v "0.2.0") (h "0j03hsyawkzbcllgxhaki7gi3sm4pkcd6my5vn4rdhv50pgrkdyy") (f (quote (("std") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.1 (c (n "ppv-lite86") (v "0.2.1") (h "134m6pd3dw3j5ksi8p1rmh6jrac7ixqjl2nn80fmcqqfh7sjd142") (f (quote (("std") ("simd") ("default" "std" "simd")))) (y #t)))

(define-public crate-ppv-lite86-0.2.2 (c (n "ppv-lite86") (v "0.2.2") (h "1jkbqxd7ckkjaghpc3smrpw0291lj9df5gki24ssilbs6kk3f77y") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.3 (c (n "ppv-lite86") (v "0.2.3") (h "0xvpb2bzql00cj77380rg5s1q4jypfllijm3bh98a28l4awwgj3g") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.4 (c (n "ppv-lite86") (v "0.2.4") (h "143j7hz0a2vwyr746i7lmn5k6grzvxchb8rsks6i3c21hd2aazjq") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.5 (c (n "ppv-lite86") (v "0.2.5") (h "06snnv338w341nicfqba2jgln5dsla72ndkgrw7h1dfdb3vgkjz3") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.6 (c (n "ppv-lite86") (v "0.2.6") (h "06zs492wbms7j5qhy58cs3976c7kyc47rx0d6fn63rgvp580njbl") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.7 (c (n "ppv-lite86") (v "0.2.7") (h "1w72wnbfx88w86h955gc8fa8dz4gr5h5kzij1v7cgi5yjqydzr6i") (f (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2.8 (c (n "ppv-lite86") (v "0.2.8") (h "1shj4q7jwj0azssr8cg51dk3kh7d4lg9rmbbz1kbqk971vc5wyi3") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.9 (c (n "ppv-lite86") (v "0.2.9") (h "080sa1pllwljxyl3i5b1i7746sh1s16m8lmn6fkn4p0z253sjvy3") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.10 (c (n "ppv-lite86") (v "0.2.10") (h "0ms8198kclg4h96ggbziixxmsdl847s648kmbx11zlmjsqjccx5c") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.11 (c (n "ppv-lite86") (v "0.2.11") (h "1m4zyfm48rc4qlq8kinvdsn60qya2y0wwz465mflk9jqsjdgc2gj") (f (quote (("std") ("simd") ("no_simd") ("default" "std")))) (y #t)))

(define-public crate-ppv-lite86-0.2.12 (c (n "ppv-lite86") (v "0.2.12") (h "1h0lgd7g75rwz8gh8r47n06xgqjxa6z522hsx4lg1my8i850r8la") (f (quote (("std") ("simd") ("no_simd") ("default" "std")))) (y #t)))

(define-public crate-ppv-lite86-0.2.13 (c (n "ppv-lite86") (v "0.2.13") (h "18n33pk7i6l1iimr12i8b7f882cq7dl3fp86aa0i7x5iclbqhdd3") (f (quote (("std") ("simd") ("no_simd") ("default" "std")))) (y #t)))

(define-public crate-ppv-lite86-0.2.14 (c (n "ppv-lite86") (v "0.2.14") (h "0hdp3m8f1d0rlfmlf7n2a5ndzjh2ybpc816d2npg97qjs0dh3jn3") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.15 (c (n "ppv-lite86") (v "0.2.15") (h "1fimwnyyh3wx33r5s77lw5g5vcxhw1p5j60pdvbbwr8l374gn37d") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.16 (c (n "ppv-lite86") (v "0.2.16") (h "0wkqwnvnfcgqlrahphl45vdlgi2f1bs7nqcsalsllp1y4dp9x7zb") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2.17 (c (n "ppv-lite86") (v "0.2.17") (h "1pp6g52aw970adv3x2310n7glqnji96z0a9wiamzw89ibf0ayh2v") (f (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

