(define-module (crates-io pp re pprexec) #:use-module (crates-io))

(define-public crate-pprexec-0.2.2 (c (n "pprexec") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rl6y7fqka5ndvm8b99nsjvyfb4iby8gjsk0mql9z2f7ikg8wqwm") (f (quote (("server" "lazy_static" "futures" "num_cpus") ("default" "server" "client") ("client"))))))

