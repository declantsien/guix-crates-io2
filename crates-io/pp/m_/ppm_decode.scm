(define-module (crates-io pp m_ ppm_decode) #:use-module (crates-io))

(define-public crate-ppm_decode-0.1.0 (c (n "ppm_decode") (v "0.1.0") (h "03ywfqrvw2dshrlxrm4ryrpdlplsjjdm7iv52amj10nxpd0kc899")))

(define-public crate-ppm_decode-0.1.1 (c (n "ppm_decode") (v "0.1.1") (h "1fggmq97yv4chgdng7wj2qhlqwlnsyajkf4bk99xxy4374fdkl6x")))

(define-public crate-ppm_decode-0.1.2 (c (n "ppm_decode") (v "0.1.2") (h "0xwr55wlnbfznl33ixwnbgar9yxw0hpan047i4834ci3llq4v51i")))

(define-public crate-ppm_decode-0.1.3 (c (n "ppm_decode") (v "0.1.3") (h "1mvmgvxjjinapsxd7hrnscshjnk9rph08zcyckizicj8dj1dzs6n")))

