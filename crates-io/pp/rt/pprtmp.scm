(define-module (crates-io pp rt pprtmp) #:use-module (crates-io))

(define-public crate-pprtmp-0.1.0 (c (n "pprtmp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rtmp") (r "^0.4.0") (d #t) (k 0)) (d (n "streamhub") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "091l0slwgf1k0nkywjlj7d7q1abra1dkm6z3w3df7xxf5wkj43jq")))

