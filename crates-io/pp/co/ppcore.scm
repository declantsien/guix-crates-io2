(define-module (crates-io pp co ppcore) #:use-module (crates-io))

(define-public crate-ppcore-0.2.2 (c (n "ppcore") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "ppcore_def") (r "^0.2") (d #t) (k 0)))) (h "14b6f0w5y6c49lwk3yg5v5l4167hf2gijsaaf21zyf9ghr0s3p2a") (f (quote (("zfs_snapshot") ("testmock") ("default" "backing_file") ("backing_file"))))))

