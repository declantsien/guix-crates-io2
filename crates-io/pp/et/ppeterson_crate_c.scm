(define-module (crates-io pp et ppeterson_crate_c) #:use-module (crates-io))

(define-public crate-ppeterson_crate_c-0.1.0 (c (n "ppeterson_crate_c") (v "0.1.0") (d (list (d (n "ppeterson_crate_a") (r "^2.0.0") (d #t) (k 0)))) (h "1widiisrxbbraz3xj495wh8b1wy2jnajaygwcdvwr86ksha5yyly") (y #t)))

(define-public crate-ppeterson_crate_c-0.3.0 (c (n "ppeterson_crate_c") (v "0.3.0") (d (list (d (n "ppeterson_crate_a") (r "^2.0.0") (d #t) (k 0)))) (h "0gbg0lxvx7z4fhd327qgxfsv3c07m1hf5gjxiypvyahsdkwvvfi5") (y #t)))

