(define-module (crates-io pp et ppeterson_crate_b) #:use-module (crates-io))

(define-public crate-ppeterson_crate_b-0.1.0 (c (n "ppeterson_crate_b") (v "0.1.0") (d (list (d (n "ppeterson_crate_a") (r "^1.0.0") (d #t) (k 0)))) (h "1yw3c7x3q9kswk19mnfz1wdynym866hm6g86zy35dikwsrvkajw8") (y #t)))

(define-public crate-ppeterson_crate_b-0.2.0 (c (n "ppeterson_crate_b") (v "0.2.0") (d (list (d (n "ppeterson_crate_a") (r "^1.0.0") (d #t) (k 0)))) (h "0q2yyf4fha6iawl0l0k4jldqn4kg1z8750az496ff72vr7fgvysm") (y #t)))

(define-public crate-ppeterson_crate_b-0.3.0 (c (n "ppeterson_crate_b") (v "0.3.0") (d (list (d (n "ppeterson_crate_a") (r "^1.0.0") (d #t) (k 0)))) (h "1qrz51hy724sn19p8xrrhzf66b1s234p8hpsvjzxlqc91pnjsmzl") (y #t)))

(define-public crate-ppeterson_crate_b-0.8.0 (c (n "ppeterson_crate_b") (v "0.8.0") (h "1rrxxjvpwg5kkcya0dzgfi533q043i5g5bm6hfg2496qr945cw40")))

