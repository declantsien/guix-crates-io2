(define-module (crates-io pp cp ppcp) #:use-module (crates-io))

(define-public crate-ppcp-0.1.0 (c (n "ppcp") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.9") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "path_abs") (r "^0.5.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1s2cy00lm0ac1vas7sd0qggb9hhz4cd1ymnp7xnxpr27fx4mzmhn")))

