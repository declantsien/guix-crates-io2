(define-module (crates-io pp pr ppproxy) #:use-module (crates-io))

(define-public crate-ppproxy-0.2.2 (c (n "ppproxy") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "ppserver") (r "^0.2") (d #t) (k 2)) (d (n "ppserver_def") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18f9lnmp9sclvp4ih2q44xqnij8bxwvx7pcpiyql5vq2fkx01ra0") (f (quote (("testmock" "ppserver/testmock") ("default"))))))

