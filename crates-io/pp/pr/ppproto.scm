(define-module (crates-io pp pr ppproto) #:use-module (crates-io))

(define-public crate-ppproto-0.0.0 (c (n "ppproto") (v "0.0.0") (h "0sk9h1a328yjhxn40k52an5bfd819j7524krij85b7cr1fwqabfc")))

(define-public crate-ppproto-0.1.0 (c (n "ppproto") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)))) (h "069ccjji6m35k5izzmplrkihah1s2nnq5kr6262xnvcr0hpdcgfi")))

(define-public crate-ppproto-0.1.1 (c (n "ppproto") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)))) (h "11ng3yvgm9g8g9haq027i720hblg4z6h1x4jv0rdwakwqvjhjsx8")))

(define-public crate-ppproto-0.1.2 (c (n "ppproto") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (k 0)))) (h "0clsij227kv5f437ziy87hfizwyzxymv71fln9k95gkzvax273xh")))

