(define-module (crates-io pp fu ppfuzz) #:use-module (crates-io))

(define-public crate-ppfuzz-0.0.1 (c (n "ppfuzz") (v "0.0.1") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1al861ydzxbnk7jiiygyhqvkkghjhmfysmqg958lzsykmbz61szl")))

(define-public crate-ppfuzz-0.1.0 (c (n "ppfuzz") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1n89kwd08iv03y2bnsbjy5kgv18qf1d00yxn7dz5x09kgyvqih8z")))

(define-public crate-ppfuzz-1.0.0 (c (n "ppfuzz") (v "1.0.0") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0payis51aycyg72prplviwlwslmx7xy06gb3l11nncwwh9vg5jnh")))

(define-public crate-ppfuzz-1.0.1 (c (n "ppfuzz") (v "1.0.1") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rnk42w86r827q81gmjk6px62cw8w6nqi188483i2aixgp78z2d6")))

