(define-module (crates-io pp t- ppt-kit) #:use-module (crates-io))

(define-public crate-ppt-kit-0.1.0 (c (n "ppt-kit") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "13rri9hk53pj5j3kw1xiwpam28ifw2zqkl3zvvhj6lf6lfm0asvs") (y #t) (r "1.64.0")))

(define-public crate-ppt-kit-0.1.1 (c (n "ppt-kit") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "00r2x92lkvbbpck096w2dj9ac197w804s5g4bnbjh7qj07by05fv") (r "1.64.0")))

(define-public crate-ppt-kit-0.1.2 (c (n "ppt-kit") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)))) (h "18qqrskax3cch0bx8778cli6h4iq2a6q1dw4v3q2lxpb575i8cjg") (r "1.64.0")))

