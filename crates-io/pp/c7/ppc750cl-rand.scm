(define-module (crates-io pp c7 ppc750cl-rand) #:use-module (crates-io))

(define-public crate-ppc750cl-rand-0.1.0 (c (n "ppc750cl-rand") (v "0.1.0") (d (list (d (n "ppc750cl") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sfmt") (r "^0.6") (d #t) (k 0)))) (h "0pasq7rm8f3bjlj4jm4w989m0qm9rpgc3cl55x3jnhkq08zzw9fs") (y #t)))

