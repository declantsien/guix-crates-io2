(define-module (crates-io pp c7 ppc750cl-macros) #:use-module (crates-io))

(define-public crate-ppc750cl-macros-0.1.0 (c (n "ppc750cl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0m4hf2lv1w03w2hzll8rn5xncvx8j8m7dipr6sqdy80wain5lxyb")))

