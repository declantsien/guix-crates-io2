(define-module (crates-io pp c7 ppc750cl-fuzz) #:use-module (crates-io))

(define-public crate-ppc750cl-fuzz-0.1.0 (c (n "ppc750cl-fuzz") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "ppc750cl") (r "^0.1.0") (d #t) (k 0)))) (h "16ikvxpwr2jr2aswih56fnwgriab7vs7g8a19n45rx0cpg2xmgwd") (y #t)))

