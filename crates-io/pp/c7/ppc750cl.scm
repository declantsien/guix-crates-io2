(define-module (crates-io pp c7 ppc750cl) #:use-module (crates-io))

(define-public crate-ppc750cl-0.1.0 (c (n "ppc750cl") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ppc750cl-macros") (r "^0.1.0") (d #t) (k 0)))) (h "112rmg8wsncagq0ahbqbx2b8fq70d25sd2sb0vz4brbf53avccia")))

