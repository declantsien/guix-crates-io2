(define-module (crates-io pp om ppom) #:use-module (crates-io))

(define-public crate-ppom-0.5.0 (c (n "ppom") (v "0.5.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mkit") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)))) (h "14j4bc5ashp35x27jh577s1sr6va5axps3mxim3sghrh7myw7kmp") (f (quote (("perf" "structopt"))))))

(define-public crate-ppom-0.6.0 (c (n "ppom") (v "0.6.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mkit") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)))) (h "1vcg8qnrnsqybfh7vv7c4xg8qpyny20r5qasgn97ah3zybq9ln17") (f (quote (("perf" "structopt"))))))

(define-public crate-ppom-0.7.0 (c (n "ppom") (v "0.7.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)))) (h "1mic4dddb9j6k3ajmblcxi74w7ync99i96s12px81x7ws4c6s443") (f (quote (("perf" "structopt" "arbitrary"))))))

