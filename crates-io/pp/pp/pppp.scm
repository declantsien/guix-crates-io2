(define-module (crates-io pp pp pppp) #:use-module (crates-io))

(define-public crate-pppp-0.1.2 (c (n "pppp") (v "0.1.2") (h "16yfwv0w8ck5n948zc5rwnvwmdy7fb2iq2cvf4qs21gdcmb183jv")))

(define-public crate-pppp-0.1.3 (c (n "pppp") (v "0.1.3") (h "1v6zp58lfni73fi6kgy0rvagin51jzlrimih3s0y632lb69v5rm2")))

