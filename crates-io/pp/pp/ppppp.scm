(define-module (crates-io pp pp ppppp) #:use-module (crates-io))

(define-public crate-ppppp-0.1.0 (c (n "ppppp") (v "0.1.0") (h "19pyg8p2rynxj9l4l92ci6zk2gxd6h99f2gb13mz1zmm1c2fyww6")))

(define-public crate-ppppp-0.1.1 (c (n "ppppp") (v "0.1.1") (d (list (d (n "ppppp") (r "^0.1.0") (d #t) (k 0)))) (h "1z4hgfiavcs5fi1q63nsb036s9mvhl64q87gyr1hrv7lazap0jdl")))

(define-public crate-ppppp-0.1.3 (c (n "ppppp") (v "0.1.3") (d (list (d (n "ppppp") (r "^0.1.0") (d #t) (k 0)))) (h "0m0zvcivyqrzwmxqym0jrx53b4pivni9jwbcbxz3shys6cp8xh3j")))

(define-public crate-ppppp-0.1.4 (c (n "ppppp") (v "0.1.4") (h "12j4ngf7i81al2zsbfv9f91jcxz0ai62d98fgprvf3lwkhbx09jl")))

