(define-module (crates-io pp ot ppot-rs) #:use-module (crates-io))

(define-public crate-ppot-rs-0.1.0 (c (n "ppot-rs") (v "0.1.0") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0arv6gk4cksi9xf2j2v5f04n374yh4ccz2013fgfbhw1ymdxwkr7")))

(define-public crate-ppot-rs-0.1.1 (c (n "ppot-rs") (v "0.1.1") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "106p848nwgbw3jza56adqr9zc7ka3bhykvcawx7aqqkrfvaqm07m")))

