(define-module (crates-io pp er pperf) #:use-module (crates-io))

(define-public crate-pperf-0.1.0 (c (n "pperf") (v "0.1.0") (h "0dsgcxsqzl0wv4qjvm48352zsz0gq3bx8cklq72chgs8mnaklijh")))

(define-public crate-pperf-0.1.1 (c (n "pperf") (v "0.1.1") (h "0xpldahyyffh52jwjkc0w841kqrd9h1lgv1c9vk7qd6wwhlvwd6z")))

(define-public crate-pperf-0.1.2 (c (n "pperf") (v "0.1.2") (h "15crfczmn7r8myyhvhjyicnz6yp4pjd9ixl0i2f7h0bi6kaziwm8")))

