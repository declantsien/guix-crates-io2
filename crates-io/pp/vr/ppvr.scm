(define-module (crates-io pp vr ppvr) #:use-module (crates-io))

(define-public crate-ppvr-0.1.0 (c (n "ppvr") (v "0.1.0") (h "1v2j7jbrp4n97njqh8jxfcxa6rask95kcjgg7w9fa16jgj8m0pb4") (r "1.59")))

(define-public crate-ppvr-0.1.1 (c (n "ppvr") (v "0.1.1") (h "0hn018r63s45mjj3812ga9psjlmcblfj5h0vqlqyislxdbphxqah") (r "1.59")))

(define-public crate-ppvr-0.1.2 (c (n "ppvr") (v "0.1.2") (h "0j784liyrycq0b5ldmc6zv3zfakn04dlpn785armja59z1d40lim") (r "1.59")))

(define-public crate-ppvr-0.1.3 (c (n "ppvr") (v "0.1.3") (h "026xqgm3zf6h6i6mayiamhzv2kk62gngzq26xg85ydsk0dmxqwf8") (r "1.59")))

(define-public crate-ppvr-0.1.4 (c (n "ppvr") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "028bkjihbxd89c02fbn1waw1w7z3pzs8vrss3hdk3v4szymhi8lr") (r "1.59")))

(define-public crate-ppvr-0.1.5 (c (n "ppvr") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rys7k8c1aipi1fbkvc81121pm4zvssifv1l33yva54scysc2780") (r "1.59")))

(define-public crate-ppvr-0.2.0 (c (n "ppvr") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yprx706rnl9lclphvmdpyqbnzp1gd8nsa6jyk8sjapkkm81ldfy") (r "1.59")))

(define-public crate-ppvr-0.2.1 (c (n "ppvr") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rihkf5cbcv873bmpxhmffl2dlb738w9w31jyvy1x0vk2dpys97l") (r "1.59")))

