(define-module (crates-io pp at ppatch) #:use-module (crates-io))

(define-public crate-ppatch-0.1.0 (c (n "ppatch") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (d #t) (k 0)))) (h "0z35mv5jgc9x70wa4f68h1bwfx9cr16xvbrks9z7qzdaaxfbvdm3")))

