(define-module (crates-io pp ip ppipe) #:use-module (crates-io))

(define-public crate-ppipe-0.1.0 (c (n "ppipe") (v "0.1.0") (h "07px4j62q6sdbqcvls5k05l8pdv23q9n7ac5bvkgkf1kbsprgivf")))

(define-public crate-ppipe-0.2.0 (c (n "ppipe") (v "0.2.0") (h "021wavl9m42iirj43ijy717x3cj18b3avf41ymiyw2i6f9h0wif4")))

