(define-module (crates-io pp ri pprint) #:use-module (crates-io))

(define-public crate-pprint-0.1.0 (c (n "pprint") (v "0.1.0") (d (list (d (n "pprint_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "07r8ail9ylznanwr5vwray7rx8dcvwnzsbcx780gjf98c3042g41")))

(define-public crate-pprint-0.2.0 (c (n "pprint") (v "0.2.0") (d (list (d (n "pprint_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "1cl97qsggsh5zw5r6h0zvldxi8iwlipq86w5q4h86nqydwy15pxc")))

(define-public crate-pprint-0.2.1 (c (n "pprint") (v "0.2.1") (d (list (d (n "pprint_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0cs246bxrhipk1lb6v7il42hbakdrkcqfp4a8a6blvayw6rxbhm4")))

