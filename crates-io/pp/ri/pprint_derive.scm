(define-module (crates-io pp ri pprint_derive) #:use-module (crates-io))

(define-public crate-pprint_derive-0.1.0 (c (n "pprint_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07sl68xa3yp1ay6h1npzjg8d86wvywgr9h537bl3mb5gjjkw1bsv")))

