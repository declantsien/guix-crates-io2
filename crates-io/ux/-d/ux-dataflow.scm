(define-module (crates-io ux -d ux-dataflow) #:use-module (crates-io))

(define-public crate-ux-dataflow-0.1.0 (c (n "ux-dataflow") (v "0.1.0") (d (list (d (n "intmap") (r "^0.7") (d #t) (k 0)))) (h "1r8bbyr9vghwk2b68z3x6ljcjl5s4ik0ipz1dj42rd39swjbpf5w")))

(define-public crate-ux-dataflow-0.1.1 (c (n "ux-dataflow") (v "0.1.1") (d (list (d (n "intmap") (r "^0.7") (d #t) (k 0)))) (h "1gjh770g92pmgn4zdri2gncryzkzgxh5ns3i4j8hcclf1pzdclgj")))

(define-public crate-ux-dataflow-0.1.2 (c (n "ux-dataflow") (v "0.1.2") (d (list (d (n "intmap") (r "^0.7") (d #t) (k 0)))) (h "0w0v15j3is28xwqyx1yhf743gckgzg7rhalr92k6ww69phfkgk1v")))

