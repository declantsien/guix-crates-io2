(define-module (crates-io ux ui uxui) #:use-module (crates-io))

(define-public crate-uxui-0.0.1-alpha (c (n "uxui") (v "0.0.1-alpha") (h "05pwri2iihdk2lhp196v76jf3gzs6jyv7v20ivv4y5s7dc89gwm2") (y #t)))

(define-public crate-uxui-0.0.2-alpha (c (n "uxui") (v "0.0.2-alpha") (d (list (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "12ldwqsldwgiaxnsnxdhyn7qnmzyfpczmhqfjb04kmb28j62xj3q") (y #t)))

(define-public crate-uxui-0.0.3-alpha (c (n "uxui") (v "0.0.3-alpha") (d (list (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1m6swlr7bbqsrzcgi2im9wigk49jp5r48vs7g32yph3v2l1ifa0d") (y #t)))

(define-public crate-uxui-0.1.0-alpha.0 (c (n "uxui") (v "0.1.0-alpha.0") (d (list (d (n "freetype-rs") (r "^0.34.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (f (quote ("spirv"))) (k 0)) (d (n "winit") (r "^0.29.4") (f (quote ("rwh_05"))) (k 0)))) (h "1vss8kqf6p7dmgp2bj6fbypllyjjmmhym7apff8rw1jd7msdvx3r")))

