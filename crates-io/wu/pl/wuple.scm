(define-module (crates-io wu pl wuple) #:use-module (crates-io))

(define-public crate-wuple-0.1.0 (c (n "wuple") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02zhpbvlkgjl9z75jllvwd9j97h6irgdxr0wl8l9clvh7fr7fbcw")))

(define-public crate-wuple-0.2.0 (c (n "wuple") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1d23pkag0ia7214fyndbd41446bjb4sz8gj771vamrznwnx13w97") (y #t)))

(define-public crate-wuple-0.2.1 (c (n "wuple") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04gan8s4rp9ii1xdk457p7gayx8anmfn056cvkjyn9m6gknrnhlj")))

(define-public crate-wuple-0.3.0 (c (n "wuple") (v "0.3.0") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "image") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dvgxzk3akjl80p1206zycnlrdm6l9f3m1rkc7w990vjirjps0pz")))

(define-public crate-wuple-0.4.0 (c (n "wuple") (v "0.4.0") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "07qrxll6pivf5x6j2ldqj7lb9gvwa3k2l78z4dbqlzinkml2qnc0")))

