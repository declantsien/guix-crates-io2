(define-module (crates-io wu x- wux-cli) #:use-module (crates-io))

(define-public crate-wux-cli-0.1.0 (c (n "wux-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "wux") (r "^0.1.0") (d #t) (k 0)))) (h "03kkd95m18yn6c1i029fvmb80c08agpag8jka7xikq38qw13whah")))

