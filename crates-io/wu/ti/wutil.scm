(define-module (crates-io wu ti wutil) #:use-module (crates-io))

(define-public crate-wutil-0.0.1 (c (n "wutil") (v "0.0.1") (h "1jsjzbablm3kbss8b5lcn6pjxwgwj5jzfxw2106i135dal5h2q2n")))

(define-public crate-wutil-0.0.2 (c (n "wutil") (v "0.0.2") (h "0l7zqhfjxbyfmbk4xm8w832f1nqb396w015jppzjd8xl00wwwxm1")))

(define-public crate-wutil-0.0.3 (c (n "wutil") (v "0.0.3") (h "1af02nx5gc9faq8lym3afwcw237jiny4kbnwl4xm7nna1mhs8y3x")))

