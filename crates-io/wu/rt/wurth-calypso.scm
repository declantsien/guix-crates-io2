(define-module (crates-io wu rt wurth-calypso) #:use-module (crates-io))

(define-public crate-wurth-calypso-0.1.0 (c (n "wurth-calypso") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.6") (d #t) (k 0)))) (h "16xk8aamwdq89dj6cjhssiqzz66c5vmiy32gq2gvq66sqnf9ciry")))

(define-public crate-wurth-calypso-0.1.1 (c (n "wurth-calypso") (v "0.1.1") (d (list (d (n "atat") (r "^0.20.0") (f (quote ("async" "log"))) (d #t) (k 0)) (d (n "embassy-time") (r "^0.2") (f (quote ("std" "generic-queue"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-io") (r "^0.6") (f (quote ("std"))) (d #t) (k 2)) (d (n "embedded-io-adapters") (r "^0.6") (f (quote ("tokio-1"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 2)))) (h "09wmx0s8bjkbwmd46s9d893q8a1lhisq0b6fcbyhv10s1f94lbn3") (f (quote (("defmt" "heapless/defmt"))))))

