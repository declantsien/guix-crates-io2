(define-module (crates-io wu da wudao) #:use-module (crates-io))

(define-public crate-wudao-0.1.0 (c (n "wudao") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02baf7v6p2cqf4qn1hficmv5f3plsmh5mv3aywvjx0axf7gad8y2")))

(define-public crate-wudao-0.1.1 (c (n "wudao") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l2av8q0vqgx97nkrcfxbwiinc0p6ak92r2flshdd0c7s9421air")))

