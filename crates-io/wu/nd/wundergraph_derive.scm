(define-module (crates-io wu nd wundergraph_derive) #:use-module (crates-io))

(define-public crate-wundergraph_derive-0.1.0 (c (n "wundergraph_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "121hr1yslsh8dwz61xhspdaa60yrz5n8i4mg25bqrsf7nh4xvz2k") (f (quote (("sqlite") ("postgres") ("nightly" "proc-macro2/nightly") ("default") ("debug"))))))

