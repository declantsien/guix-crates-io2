(define-module (crates-io wu rl wurl) #:use-module (crates-io))

(define-public crate-wurl-0.1.0 (c (n "wurl") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 1)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 1)) (d (n "rprompt") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "rprompt") (r "^1.0.3") (d #t) (k 1)) (d (n "stderrlog") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "stderrlog") (r "^0.3.0") (d #t) (k 1)) (d (n "url") (r "^1.7.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.6") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "ws") (r "^0.7.6") (f (quote ("ssl"))) (d #t) (k 1)))) (h "1d9ld06nw91gg6z9gsnj05nmvwi79yxmh6n01lhf2f2jllcahmva") (f (quote (("default" "cli") ("cli" "clap" "rprompt" "stderrlog"))))))

