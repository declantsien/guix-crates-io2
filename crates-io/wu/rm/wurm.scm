(define-module (crates-io wu rm wurm) #:use-module (crates-io))

(define-public crate-wurm-1.0.0 (c (n "wurm") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 2)))) (h "1v05y1f1azw33562x3s3ivnrg99m9injm1ncw5bmib0v5dyghvax")))

(define-public crate-wurm-1.0.1 (c (n "wurm") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 2)))) (h "0riq31jh1d19qrq9ggkmjfzb7hniy1gpxnz4c1289mmaivryyihp")))

(define-public crate-wurm-1.1.0 (c (n "wurm") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 2)))) (h "1qyfl269rkw8lwsa8bdc5jxbcmq6yg4q0223hdfb13vg0pma1mwb")))

