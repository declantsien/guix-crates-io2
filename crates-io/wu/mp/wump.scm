(define-module (crates-io wu mp wump) #:use-module (crates-io))

(define-public crate-wump-0.1.0 (c (n "wump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "promptly") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1r8yalwm5hdi55r0612ryk86icc8qc9100xbchsl3iq4ghllm73j")))

(define-public crate-wump-0.1.1 (c (n "wump") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "promptly") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0d8ihvrbl4pp1w7w4y6kzw0g7g2snaqflpyh9yfk6q9s7mvja9ky")))

