(define-module (crates-io wu ff wuffs) #:use-module (crates-io))

(define-public crate-wuffs-0.1.0 (c (n "wuffs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wuffs-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0f0h2lqws3qp4df7jivxwp2fvzzl3hq1qzpikbxif221c14n3r1b")))

(define-public crate-wuffs-0.2.0 (c (n "wuffs") (v "0.2.0") (d (list (d (n "wuffs-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0mvn00z8ipab8icm69ijz8lvabkcx2gxw8ccp9f06qavi09njxfj")))

