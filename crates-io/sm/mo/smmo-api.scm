(define-module (crates-io sm mo smmo-api) #:use-module (crates-io))

(define-public crate-smmo-api-0.1.0 (c (n "smmo-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (d #t) (k 0)))) (h "077a3whda4b5nf507r45lhqv15qnqfpmikp17dd01xvp70rgx99k") (f (quote (("logging" "log") ("env" "dotenv") ("default" "logging"))))))

