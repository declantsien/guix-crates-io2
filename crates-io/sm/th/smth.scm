(define-module (crates-io sm th smth) #:use-module (crates-io))

(define-public crate-smth-0.1.0 (c (n "smth") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16nyj4r0hchg45nkz0kqwdzqyx7di1la7snqp23ls5k7kld2hryx")))

(define-public crate-smth-0.1.1 (c (n "smth") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1765m7wrp0vpbf63vmds8xld87vq5aa1h59iri4rlhhss7p7yswc")))

(define-public crate-smth-0.1.2 (c (n "smth") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aqb7dili9rv9dncia6cghymcpmf0fi3zli81m9b7sl7cx3f7ifi")))

(define-public crate-smth-0.1.3 (c (n "smth") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nvg2ggzlgmb05wgymv0pa0v9m0cfv3bj8bdadik3yjca3wzx2zi")))

(define-public crate-smth-0.1.4 (c (n "smth") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mpq9lfcrrg1gsjbwbdh4g9770yhg72z27j93rnmwhabmk48an8k")))

(define-public crate-smth-0.1.5 (c (n "smth") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nan91rgcidf6nvggbckany1ivmkk4qpp743c6b2paryy77mjda1")))

(define-public crate-smth-0.1.6 (c (n "smth") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cdkq1d1hjl7yrxhgxqmycns9c3c24mn0qdkfaik9yh0yfcnhif7")))

(define-public crate-smth-0.2.0 (c (n "smth") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p5hmjcqjlx20v288szzfzjsvjq4jakqa4hi9wcwybhf1jcncyag")))

(define-public crate-smth-0.3.0 (c (n "smth") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qba5va76bv01bnn701y99nv4k26x93rxhygmynrmig6mfdhp0hv")))

