(define-module (crates-io sm ar smartdeploy-macros) #:use-module (crates-io))

(define-public crate-smartdeploy-macros-0.1.0 (c (n "smartdeploy-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1c8vyf7r69vnprij3hygjhqdf0myf2n0lxzzmgz2l1v0wsrspfvi") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.1 (c (n "smartdeploy-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.1.0") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "00gnvm4vx1pbyrik9w4kkslid87mkrpd5m4p7wafx29jqr4dz0pz") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.2 (c (n "smartdeploy-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.1.1") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "189s1i3dc3zcwq414i3h798s69h9kjpbscflhf0ajf7h28r50sa1") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.3 (c (n "smartdeploy-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.2.0") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0sj5kz64mw98iwm73akl0bh10wdr4wvw4km0z85vd7vl623gl1is") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.4 (c (n "smartdeploy-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.2.1") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0fq9fjqqqbm335kznr3jf3ap0hh61mnxxhhq2yik895jmxnk6lk7") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.5 (c (n "smartdeploy-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.2.1") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "074qhsyyg5j4j8hwlgdwmf844hyaascsvaqppzv0qh6v23y0qs92") (r "1.69")))

(define-public crate-smartdeploy-macros-0.1.6 (c (n "smartdeploy-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smartdeploy-build") (r "^0.2.1") (d #t) (k 0)) (d (n "stellar-strkey") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1aikjma5yw8mj7lxlbm39a4w4in3v03lz52fknxmzmivhg10k05x") (r "1.69")))

