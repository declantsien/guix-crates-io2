(define-module (crates-io sm ar smart-string) #:use-module (crates-io))

(define-public crate-smart-string-0.1.1 (c (n "smart-string") (v "0.1.1") (h "1p103rylcap6rsh6pm5cnzlqk3px96lnfc59xq1gmp6bg5sszdpc")))

(define-public crate-smart-string-0.1.2 (c (n "smart-string") (v "0.1.2") (d (list (d (n "rustversion") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0vkvfzvqkxacq56mrm01b97qkfxcm1kgzyif51k1j677y0ig62p1") (f (quote (("default" "serde"))))))

(define-public crate-smart-string-0.1.3 (c (n "smart-string") (v "0.1.3") (d (list (d (n "rustversion") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ic6vwca20hcixggr11ijw78mkqzx5dlw5vqd5ql027byyd2llpg") (f (quote (("default" "serde"))))))

