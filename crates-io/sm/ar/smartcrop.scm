(define-module (crates-io sm ar smartcrop) #:use-module (crates-io))

(define-public crate-smartcrop-0.0.1 (c (n "smartcrop") (v "0.0.1") (h "18qjdvdddhp9prah6jf0qqijg2yix80maj1xgzb6hkypc4vqrrr2")))

(define-public crate-smartcrop-0.1.0 (c (n "smartcrop") (v "0.1.0") (d (list (d (n "image") (r ">= 0.17.0, < 0.20.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.7.0") (d #t) (k 2)))) (h "0c9vnik2xcsrpinh3wqdzf6v2kp41ssqx2fz7pcn9xqwjvja70sa") (f (quote (("default" "image"))))))

