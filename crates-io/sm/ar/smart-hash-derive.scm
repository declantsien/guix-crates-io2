(define-module (crates-io sm ar smart-hash-derive) #:use-module (crates-io))

(define-public crate-smart-hash-derive-0.1.0 (c (n "smart-hash-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "smart-hash") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zzgd86bk74i5q638zz8dhkxb7khiz84kvwz7ml1gd218q7j5nal")))

(define-public crate-smart-hash-derive-0.1.1 (c (n "smart-hash-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "smart-hash") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0krwzdccs59skagixsmh66949c1gd6jlm1b2p0fz96nvzq9ln3yx")))

(define-public crate-smart-hash-derive-0.1.2 (c (n "smart-hash-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "smart-hash") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "142pqz4lcqz1sfyy4jdcpg1nlsziaa7ac3wf24dlgx8j0zx541xh")))

