(define-module (crates-io sm ar smart-leds) #:use-module (crates-io))

(define-public crate-smart-leds-0.1.0 (c (n "smart-leds") (v "0.1.0") (d (list (d (n "smart-leds-trait") (r "^0.1") (d #t) (k 0)))) (h "17vyq1929srvksp27qb2qhghyv3fdh0wh4abi2rgx20qb3q5n7qz")))

(define-public crate-smart-leds-0.2.0 (c (n "smart-leds") (v "0.2.0") (d (list (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "106h9q9z0djzynrjrbznf59n78y8girc5m10q2zm02766klk02mb")))

(define-public crate-smart-leds-0.3.0 (c (n "smart-leds") (v "0.3.0") (d (list (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "02r92mqgvjn3m6wnhaxjigw89lqsc6152pxc1q8v8w2z4zx4bp9q")))

(define-public crate-smart-leds-0.4.0 (c (n "smart-leds") (v "0.4.0") (d (list (d (n "smart-leds-trait") (r "^0.3.0") (d #t) (k 0)))) (h "1w3v0pbf13s5jfnbmybm9dlkvjjq9lvil4wrdzx976gsf7jk9pv6") (f (quote (("serde" "smart-leds-trait/serde"))))))

