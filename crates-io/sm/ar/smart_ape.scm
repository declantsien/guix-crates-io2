(define-module (crates-io sm ar smart_ape) #:use-module (crates-io))

(define-public crate-smart_ape-0.1.0 (c (n "smart_ape") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fy44jcz2a93jsmkvf8lvlw2ir70qmh5n5iz1qczk7dljlrlg0ms")))

(define-public crate-smart_ape-0.1.1 (c (n "smart_ape") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ig24ax2ivjdbj9yclb0lnksz14lrfq4myqp28k8dkhxrpmbf772")))

(define-public crate-smart_ape-0.1.2 (c (n "smart_ape") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "061vp9920zp61wjbafw673zxz4y3838n2cazf7rc3nylmjf26jqd")))

