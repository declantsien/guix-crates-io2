(define-module (crates-io sm ar smartcard) #:use-module (crates-io))

(define-public crate-smartcard-0.1.0 (c (n "smartcard") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0x9j4zdsh5xhfwwv5n8pvgc2vshfy5g2l9aq4qr1z8knvyb9y4h6")))

(define-public crate-smartcard-0.2.0 (c (n "smartcard") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1a0hn5fs9bqhk4jxr0xhqg6ksbq1s079br02dph9iarx47ivci04")))

(define-public crate-smartcard-0.3.0 (c (n "smartcard") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1") (d #t) (k 0)))) (h "00jprkvif5dzcl15dlw3nywq8i164v1nppg0b2in92wbvmkwpx7q")))

(define-public crate-smartcard-0.3.1 (c (n "smartcard") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1") (d #t) (k 0)))) (h "1wy792pgg60w456g6c5mkazzrr71ra7nvb822075496hbrykf10w")))

(define-public crate-smartcard-0.3.2 (c (n "smartcard") (v "0.3.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1") (d #t) (k 0)))) (h "0n59v6v9pz50hjpjbqnw3jkk2v6g5z40h2d6ywz1m2ccmn34hin9")))

(define-public crate-smartcard-0.3.3 (c (n "smartcard") (v "0.3.3") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1") (d #t) (k 0)))) (h "0758m2wjclqqc76fhl5blsacvafbgwzd40alcx1r89155rssqcd6")))

(define-public crate-smartcard-0.3.4 (c (n "smartcard") (v "0.3.4") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1") (d #t) (k 0)))) (h "1ar5s91926rv3hkbc0jnxzxpcaw5h57jmagylv635mkln64m7rs9")))

