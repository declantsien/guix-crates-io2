(define-module (crates-io sm ar smart) #:use-module (crates-io))

(define-public crate-smart-0.0.0 (c (n "smart") (v "0.0.0") (h "1l3kmxrfr4cp9sdj989xhwwvlv8c8ma9f663vws1p2f6ym0zmx57")))

(define-public crate-smart-0.1.0 (c (n "smart") (v "0.1.0") (h "1m1s5fxcc521b22xdg0m94bbb0bgs4ic783zyjw738p93mda2k5s")))

(define-public crate-smart-0.1.1 (c (n "smart") (v "0.1.1") (h "17ms22pa8xl75a32r45sryyyxr2scr6w9pkwbi5lgndvprbvk1hf") (f (quote (("nightly"))))))

