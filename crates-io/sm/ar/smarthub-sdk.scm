(define-module (crates-io sm ar smarthub-sdk) #:use-module (crates-io))

(define-public crate-smarthub-sdk-0.1.0 (c (n "smarthub-sdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "near-abi") (r "^0.1.0-pre.0") (d #t) (k 2)) (d (n "near-sdk") (r "^4.1.0-pre.3") (f (quote ("abi"))) (d #t) (k 0)) (d (n "near-units") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "workspaces") (r "^0.4.1") (d #t) (k 2)) (d (n "zstd") (r "^0.11") (d #t) (k 2)))) (h "1dg2y3730f6b1wpy1pry6az4z0qaz3jp2dprnm8v2gavqdi2ry01")))

