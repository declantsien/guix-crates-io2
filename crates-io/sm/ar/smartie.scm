(define-module (crates-io sm ar smartie) #:use-module (crates-io))

(define-public crate-smartie-0.1.0 (c (n "smartie") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "10ihla5rxc3wfrbjfak46cs4ch393535kk3ypc4a8ifph7w1fv39")))

