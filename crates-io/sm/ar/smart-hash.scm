(define-module (crates-io sm ar smart-hash) #:use-module (crates-io))

(define-public crate-smart-hash-0.1.0 (c (n "smart-hash") (v "0.1.0") (h "1cbgd18ixf8ndwfx1vyx825gi6qb61w4pk1kqnd4f7klwc0pm9qw")))

(define-public crate-smart-hash-0.1.1 (c (n "smart-hash") (v "0.1.1") (d (list (d (n "smart-hash-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1xxlfsq40lnb04n6v8ln3zq24x7xjz4l0j3y32k47sd5kkz90686") (f (quote (("manual") ("derive" "smart-hash-derive") ("default" "smart-hash-derive"))))))

(define-public crate-smart-hash-0.1.2 (c (n "smart-hash") (v "0.1.2") (d (list (d (n "smart-hash-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qlx0di23lhsihwilqnb4rnvj6skl9v3mscwx74fvc2fpmd07nqd") (f (quote (("manual") ("derive" "smart-hash-derive") ("default" "smart-hash-derive"))))))

