(define-module (crates-io sm ar smartalloc-sys) #:use-module (crates-io))

(define-public crate-smartalloc-sys-0.0.1 (c (n "smartalloc-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bn6x61kl5nvfvfhnasybhdhscnb2x71gb79s8bianh8pkd7xi9h") (y #t)))

(define-public crate-smartalloc-sys-0.1.1 (c (n "smartalloc-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0n8vsgb1z093lwzhfc16n3df61z66kjdjkqik711prl1di6lvddl") (y #t)))

(define-public crate-smartalloc-sys-0.2.0 (c (n "smartalloc-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07hmsp9v6ihqp18qh0d4vynqrf32a11i4q8i6j50s9x5fa24gxxc")))

