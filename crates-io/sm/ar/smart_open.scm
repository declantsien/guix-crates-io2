(define-module (crates-io sm ar smart_open) #:use-module (crates-io))

(define-public crate-smart_open-0.1.0 (c (n "smart_open") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "17a34bf93ax5bjfjszswr90ap3jzippv3j0ahbfzns5jxyvjg4rq")))

(define-public crate-smart_open-0.1.1 (c (n "smart_open") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1fr1ib67myq01np3m6l7m889pxk2ivwanp05kpx3mhfl0sirc53f")))

(define-public crate-smart_open-0.1.2 (c (n "smart_open") (v "0.1.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-s3") (r "^0.11.0") (d #t) (k 0)))) (h "1i0qpm2brbbj7g5plqndhw03bafghxfyimd754v3sap04wv5m9h7")))

(define-public crate-smart_open-0.1.3 (c (n "smart_open") (v "0.1.3") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "rust-s3") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0l3cnwm9nrd1wb11ak0qhk9i9yrrc2flc3ssvh4q16iqknh2sihm")))

