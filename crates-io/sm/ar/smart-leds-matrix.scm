(define-module (crates-io sm ar smart-leds-matrix) #:use-module (crates-io))

(define-public crate-smart-leds-matrix-0.0.1 (c (n "smart-leds-matrix") (v "0.0.1") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "033z7x94j577hnnhqs81f53vlbzzfdj6ci9si3vi0x98aqs6h17v")))

(define-public crate-smart-leds-matrix-0.1.0 (c (n "smart-leds-matrix") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "1sz58vwjn24iyf4hb99gjhla35lyvwbccjqh0xw8aqxw1c729zs7")))

(define-public crate-smart-leds-matrix-0.2.0 (c (n "smart-leds-matrix") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.4.0") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.3.0") (d #t) (k 0)))) (h "1ylf15kl7hspskhqa0sc5jjbhljwxdj2pxvqy4vb69zlbb38g5lb")))

