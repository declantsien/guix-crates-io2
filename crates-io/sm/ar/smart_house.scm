(define-module (crates-io sm ar smart_house) #:use-module (crates-io))

(define-public crate-smart_house-0.1.0 (c (n "smart_house") (v "0.1.0") (h "19prd2fh6gpgkwgbs0q45xblyb3r4bv05d44mx8i4m6ssbd713vb")))

(define-public crate-smart_house-0.1.1 (c (n "smart_house") (v "0.1.1") (h "1ahqgk7bj0y6980kncl8lq2kbajpsf06rafp5j60m4wysbx83p13")))

(define-public crate-smart_house-0.1.2 (c (n "smart_house") (v "0.1.2") (h "19irr3glkk0ks6y77np4m0dcws904k79892982qhpja52lijvhg2")))

(define-public crate-smart_house-0.2.0 (c (n "smart_house") (v "0.2.0") (d (list (d (n "simple-log") (r "^1.4.0") (d #t) (k 0)) (d (n "tcp_wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "udp_wrapper") (r "^0.1.0") (d #t) (k 0)))) (h "0s8nkbasclkrdnlhi664v4pjggp652a2karmwqk7yjkjzgmkw7p4")))

(define-public crate-smart_house-0.2.2 (c (n "smart_house") (v "0.2.2") (d (list (d (n "simple-log") (r "^1.4.0") (d #t) (k 0)) (d (n "tcp_wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "udp_wrapper") (r "^0.1.0") (d #t) (k 0)))) (h "1ah3fpwjva5p6apgi0zc9ly5nqmg8ylq9k19kn1q11h48ris89dw") (r "1.56")))

