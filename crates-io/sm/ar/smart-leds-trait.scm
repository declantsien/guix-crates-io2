(define-module (crates-io sm ar smart-leds-trait) #:use-module (crates-io))

(define-public crate-smart-leds-trait-0.1.0 (c (n "smart-leds-trait") (v "0.1.0") (h "09x464ych9x9nlj4l6l02fdkb822y9cf30arp9a3zrg95n498is5")))

(define-public crate-smart-leds-trait-0.1.1 (c (n "smart-leds-trait") (v "0.1.1") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1sni17cs41dhnjfi7dncprcb3kmjs46337cigdsrvp7m0y12s58d")))

(define-public crate-smart-leds-trait-0.2.0 (c (n "smart-leds-trait") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1ryf21s8s0l5vbj61gs666i7mcizq287bka0wqcxwvqssih8xwgy")))

(define-public crate-smart-leds-trait-0.2.1 (c (n "smart-leds-trait") (v "0.2.1") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1qx8yyv5iliyajzmjva3vmdpwmp8xcm2rrkl30f6mwckz8rxixpb")))

(define-public crate-smart-leds-trait-0.3.0 (c (n "smart-leds-trait") (v "0.3.0") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0wn5fprvzbgbnx4vnvhb51iz496j1rn79pqn61h6kx5v5gh4xihb") (f (quote (("serde" "rgb/serde"))))))

