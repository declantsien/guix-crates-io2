(define-module (crates-io sm ar smart-default) #:use-module (crates-io))

(define-public crate-smart-default-0.1.0 (c (n "smart-default") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1r936ax43nz0k1lkd5r0wkxh32pmzfvjhpsjvgi01dzf7brlm8n1")))

(define-public crate-smart-default-0.2.0 (c (n "smart-default") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "13g82c1j1lks3nv0p8zqvs3rh8gp98kbyw41r6644yfzijp94wry")))

(define-public crate-smart-default-0.3.0 (c (n "smart-default") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0scvlyiimh0kgqgd5ashcx267ggka20d68v6sc4lhjfsv8nw1rbh")))

(define-public crate-smart-default-0.4.0 (c (n "smart-default") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0jm80aqryhmyw4q7nm15hqrlf1cvani0wj2jll56rmmhzmrpxzvv")))

(define-public crate-smart-default-0.5.0 (c (n "smart-default") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "13dvybkw7arm4r8j3r00j6jfcm5c8mnrky6rq13vcs2zv5a2p0v4")))

(define-public crate-smart-default-0.5.1 (c (n "smart-default") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1v9jw15a3ss315h7zgx6rh8z70144hqkdx1kzib53yc84w0r1q7s")))

(define-public crate-smart-default-0.5.2 (c (n "smart-default") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1apd8id5vsfnskih4m2pmhg44p7ny5fcxqxvbhsq0knh0dgvv7as")))

(define-public crate-smart-default-0.6.0 (c (n "smart-default") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xnvxz9wilj4d5b8kg4wbs0yk48wm41fnwkmn3p6wi9rafhmjdhk")))

(define-public crate-smart-default-0.7.0 (c (n "smart-default") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wdlghfjdh1hma6awcc53nsx20386zziws16b9gs2w5lwql79rwi")))

(define-public crate-smart-default-0.7.1 (c (n "smart-default") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hgzs1250559bpayxmn46gzas5ycqn39wkf4srjgqh4461k1ic0f")))

