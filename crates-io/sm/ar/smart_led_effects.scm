(define-module (crates-io sm ar smart_led_effects) #:use-module (crates-io))

(define-public crate-smart_led_effects-0.1.0 (c (n "smart_led_effects") (v "0.1.0") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1whyps1w1xlsq1j7qbyz4nfp0gk87d0abga2kqv7v2xr9dn5v64d")))

(define-public crate-smart_led_effects-0.1.1 (c (n "smart_led_effects") (v "0.1.1") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "031ydysrpifdh12v4gp3wp31pjw5j30lhhg248657l1d40vfl3wn")))

(define-public crate-smart_led_effects-0.1.2 (c (n "smart_led_effects") (v "0.1.2") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ym4iizj54cq1gv8jyh0lpw39xn0y9faj87qnnw9zhw4sc7cb2ic")))

(define-public crate-smart_led_effects-0.1.3 (c (n "smart_led_effects") (v "0.1.3") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fpbgqpk3b1smvsjfn6bvaxrbmkfq62hjglj2900wvr1hvn0xiwn")))

(define-public crate-smart_led_effects-0.1.4 (c (n "smart_led_effects") (v "0.1.4") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0khi582q9867iw9yga7h5hppab1gab7r8w7kavdbmx1xmjq25kbm")))

(define-public crate-smart_led_effects-0.1.5 (c (n "smart_led_effects") (v "0.1.5") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zgypikpaz8q9wn3wjnq53gc8rj1dz5661dd8q6qq7dkvv5rkq7d")))

(define-public crate-smart_led_effects-0.1.6 (c (n "smart_led_effects") (v "0.1.6") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k3qdrb9v0drkwjd6q4lznxpl42sr3vkh8rl9s5r3blsybpgmvld")))

(define-public crate-smart_led_effects-0.1.7 (c (n "smart_led_effects") (v "0.1.7") (d (list (d (n "palette") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ww5dv17z7l7gld380abfv9jsynbbs4bj2bk8jm6dbfhp4hixdrw")))

