(define-module (crates-io sm ar smart-adder) #:use-module (crates-io))

(define-public crate-smart-adder-0.1.0 (c (n "smart-adder") (v "0.1.0") (d (list (d (n "smart-add-one-new") (r "^0.1.0") (d #t) (k 0)))) (h "0n7jjr1xx20mcm1vbz86af7imcdnb92cgkq4hdsx7b8i1rbjv6m8")))

(define-public crate-smart-adder-0.1.1 (c (n "smart-adder") (v "0.1.1") (d (list (d (n "smart-add-one-new") (r "^0.1.0") (d #t) (k 0)))) (h "0ixq1y9bcm20d0hhx56x4v1igrz9qnrkkq9ywjfxqm3pf4xwpxvr")))

(define-public crate-smart-adder-0.2.0 (c (n "smart-adder") (v "0.2.0") (d (list (d (n "smart-add-one-new") (r "^0.1.0") (d #t) (k 0)))) (h "16zxwhdkr095isns937xa1aiisy4xcyj8kalfyc6idhgmxn7pgr2")))

(define-public crate-smart-adder-0.3.0 (c (n "smart-adder") (v "0.3.0") (d (list (d (n "smart-add-one-new") (r "^0.1.0") (d #t) (k 0)))) (h "100f9jld6nbr39c8srxk40al10pqb30fvkn3p7g4zkhyd7s23l7d")))

(define-public crate-smart-adder-0.4.0 (c (n "smart-adder") (v "0.4.0") (d (list (d (n "smart-add-one-new") (r "^0.1.0") (d #t) (k 0)))) (h "0d21qg2jap16lddyjh4fp6jwk33j3r5lal4cdm3iw607a89lfacw")))

(define-public crate-smart-adder-0.5.0 (c (n "smart-adder") (v "0.5.0") (d (list (d (n "smart-add-one-new") (r "^0.2.0") (d #t) (k 0)))) (h "1iz0z83m89xfnm30p9s6w4xj4p97lsmjyvz5p2b3b1rnv8vxhhih")))

(define-public crate-smart-adder-0.6.0 (c (n "smart-adder") (v "0.6.0") (d (list (d (n "smart-add-one-new") (r "^0.2.0") (d #t) (k 0)))) (h "0svizx7qjcfw6rhxfs84ndspb9q6pav71jkc4zh2085c4xbkjwlj")))

(define-public crate-smart-adder-0.7.0 (c (n "smart-adder") (v "0.7.0") (d (list (d (n "smart-add-one-new") (r "^0.2.0") (d #t) (k 0)))) (h "07xpb61xmdgc2shcjz0kp32f86j7akgbpq7m7qzp581npq9jyq4h")))

(define-public crate-smart-adder-0.8.0 (c (n "smart-adder") (v "0.8.0") (d (list (d (n "smart-add-one-new") (r "^0.3.0") (d #t) (k 0)))) (h "0rfiw1k1f899plbi9fldkkn9f5h32k75307lbhwwa23y21ynzpkz")))

