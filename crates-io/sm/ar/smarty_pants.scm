(define-module (crates-io sm ar smarty_pants) #:use-module (crates-io))

(define-public crate-smarty_pants-0.1.0 (c (n "smarty_pants") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16i0ncycscgi2mgrz5c6pcc68aldw7wy3lmb4jbvbazqxpqgkikj")))

(define-public crate-smarty_pants-0.1.1 (c (n "smarty_pants") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06h38xgnpc4h5g4g5pvxcxiwbfvflxi4gxfbz6cp0ky3mmlc2grf")))

(define-public crate-smarty_pants-0.1.2 (c (n "smarty_pants") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wkc6ndz3q0nph9zwfx2crnxg6hml8b4l1lb2ya0359b4w1wnr6j")))

(define-public crate-smarty_pants-0.2.0 (c (n "smarty_pants") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "19n61zkfsxsi1rh2rx5iysmh339b4wbrsdi1d5pk6cp6916g0h0d")))

(define-public crate-smarty_pants-0.2.1 (c (n "smarty_pants") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzm6pz1g1l5ai2jb2vpnc2nahmj8iqi1ja86bgfbrhvkkxn20vw")))

(define-public crate-smarty_pants-0.2.2 (c (n "smarty_pants") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0brizivy1n30xb74bafj48pwpll31xzpvrsw442gxg985i6aaspq")))

(define-public crate-smarty_pants-1.0.0 (c (n "smarty_pants") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g0fx0mg3rmpy4rz62k6flmd08vaik0a7y6958qxkp47ym4nngw7")))

(define-public crate-smarty_pants-1.0.1 (c (n "smarty_pants") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b2xi8fbhbv742p6r5igp7mvdfrlk4yr50vna3hphscq7dr6w2sk")))

