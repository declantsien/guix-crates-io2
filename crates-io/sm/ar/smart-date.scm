(define-module (crates-io sm ar smart-date) #:use-module (crates-io))

(define-public crate-smart-date-0.1.0 (c (n "smart-date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1abxbwvpy7bqxfx4lqhry893y4y6pz5cd7zgpmhm6m4cwmizbz76")))

(define-public crate-smart-date-0.1.1 (c (n "smart-date") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1i60fala54ryw1w9lxak3k7khavc5vps816ar6im0zrjfmxj9g28")))

