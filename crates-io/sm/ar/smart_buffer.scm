(define-module (crates-io sm ar smart_buffer) #:use-module (crates-io))

(define-public crate-smart_buffer-0.1.0 (c (n "smart_buffer") (v "0.1.0") (d (list (d (n "array-macro") (r "^2.0.0") (d #t) (k 0)))) (h "10kmbfyas80gsdx5s4xx5ja2733r773ji1bx3r166hmsal9lk82g")))

(define-public crate-smart_buffer-0.1.1 (c (n "smart_buffer") (v "0.1.1") (d (list (d (n "array-macro") (r "^2.0.0") (d #t) (k 0)))) (h "1bwdhap9aqq4n6m9l6awa06im0rj826vlib7ibg26861qlg0rjwk")))

(define-public crate-smart_buffer-0.1.2 (c (n "smart_buffer") (v "0.1.2") (d (list (d (n "array-macro") (r "^2.0.0") (d #t) (k 0)))) (h "1fwh0w75pxhnnq7flrnwyaashnr43zvi06s4vrz2d9bfgcr41xvf")))

(define-public crate-smart_buffer-0.1.3 (c (n "smart_buffer") (v "0.1.3") (h "00v96qxq0isazlxg5f4v1dr5y2zvr8xrvpnzpz7ggjr2kgav5ff7")))

