(define-module (crates-io sm ar smart-read) #:use-module (crates-io))

(define-public crate-smart-read-0.1.0 (c (n "smart-read") (v "0.1.0") (h "12pwxi18sg3hbpy2lijvnp34z8nih6z2vcvd7smiprn1chgh2q1n")))

(define-public crate-smart-read-0.1.1 (c (n "smart-read") (v "0.1.1") (h "1b2bdhaylnlagzrl14m0yx9znzwn9f26agknlrnbjwm1z321ir7c")))

(define-public crate-smart-read-0.2.0 (c (n "smart-read") (v "0.2.0") (h "1fbxp8a5c23lqi2id16in1n50jknv3l2014bma9wa6x2499c801y")))

(define-public crate-smart-read-0.2.1 (c (n "smart-read") (v "0.2.1") (h "0aj241i4js6w2ap8brrrnynhqwfh5pbq7djxym8rxb2mkmdmfhpq")))

(define-public crate-smart-read-0.2.2 (c (n "smart-read") (v "0.2.2") (h "0snjxg7a31cwmhhy1k9wz7l86bivlb1d7wkb47j8siz9qn8rkbsl")))

(define-public crate-smart-read-0.3.0 (c (n "smart-read") (v "0.3.0") (h "13fkaq3xay9x4pv8kpxilmmnplmghq8m6vl94rhjhal9kwi679fk")))

(define-public crate-smart-read-0.3.1 (c (n "smart-read") (v "0.3.1") (h "0n9nq7952as3sbh1lhdqm9igfn7pkb01qawaw5m23jaxzpwc37a5")))

(define-public crate-smart-read-0.3.2 (c (n "smart-read") (v "0.3.2") (h "0dvc2b0papiq3m05jv7cjs0x3whh60cyqi175cwrnlx1l99gs2rl")))

(define-public crate-smart-read-0.4.0 (c (n "smart-read") (v "0.4.0") (h "0fcbdjhwa27r1pqvqzqkz6cws7wry5ndh23ci91kqhp8n4pqgyy4")))

(define-public crate-smart-read-0.4.1 (c (n "smart-read") (v "0.4.1") (h "0x63gch1k359580gm0iddgyj7rx26cqng8i6nsfq4czqwhan6hpa")))

(define-public crate-smart-read-0.5.0 (c (n "smart-read") (v "0.5.0") (h "115vwgd72b0jhfh4nlinizdbn0d3agspmxjd2m4xljmgsr34l7qf")))

(define-public crate-smart-read-0.5.1 (c (n "smart-read") (v "0.5.1") (h "1ddwn2i8jrzzfp10b9zmd62dgvlbbbkkjy5qr44ll0sa3rhd148p")))

(define-public crate-smart-read-0.5.2 (c (n "smart-read") (v "0.5.2") (h "13c6cz578m1g05yyxbc2v6p6j09nb818g6qhsg9wbq0w6nkyivqb")))

(define-public crate-smart-read-0.5.3 (c (n "smart-read") (v "0.5.3") (h "1ygcsh8xb6vrymrr54c1pk272965i4cdc1b6kxarkgc5xgbf7kyn")))

