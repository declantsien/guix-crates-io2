(define-module (crates-io sm ar smart_ir_macro) #:use-module (crates-io))

(define-public crate-smart_ir_macro-0.0.1 (c (n "smart_ir_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "1cv9h2bw15r89cmfqdpkinq25kpgwz3s3fkn4cp5iv9c06frl018")))

(define-public crate-smart_ir_macro-0.3.0 (c (n "smart_ir_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "0q6p5k1sfn7fgmr2k4yb619l7wrz0ai5kbzg94px3wf5l21183p0")))

