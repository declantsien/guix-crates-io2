(define-module (crates-io sm ar smarty-rust-proc-macro) #:use-module (crates-io))

(define-public crate-smarty-rust-proc-macro-0.2.3 (c (n "smarty-rust-proc-macro") (v "0.2.3") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0b7lhpazm3i9z5l3bvfgbhsc08hslplrq2cg0qr6xjs4c99nnzyk")))

(define-public crate-smarty-rust-proc-macro-0.2.4 (c (n "smarty-rust-proc-macro") (v "0.2.4") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0apmksjp39drzk87qcysdq7227fm56lprvjlpyxxdmix5q0y5csf")))

(define-public crate-smarty-rust-proc-macro-0.2.5 (c (n "smarty-rust-proc-macro") (v "0.2.5") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "16ww30bcknzrsibvjkvw2n2k68a0mz0hwiwwwhhvbyrdfidv7bfm")))

(define-public crate-smarty-rust-proc-macro-0.2.6 (c (n "smarty-rust-proc-macro") (v "0.2.6") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0w1aswmxfvbzg8hrlfw1akzrrbvx6bpahr0kf9rsjzhr7mzdarzg")))

(define-public crate-smarty-rust-proc-macro-0.2.7 (c (n "smarty-rust-proc-macro") (v "0.2.7") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0jrdvbpvw2zjm9hfdgk66rbq7csrd497k7mln9x85001g11mk78p") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.2.9 (c (n "smarty-rust-proc-macro") (v "0.2.9") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1wpzsb3qp88dvhlm7igzdd9xnhyh2yhgmlk9iy5ixd4j1a6pacdy") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.2.10 (c (n "smarty-rust-proc-macro") (v "0.2.10") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0p704z1320rd5dir6gx7hxy0dzd8jf8rbgyif3ag3wrkl3jnfj3p") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.2.12 (c (n "smarty-rust-proc-macro") (v "0.2.12") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0f5dpykvfp5v4h6x8spqq9phc5jf3xfh0rxs3b4bbi900h1irq30") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.3.0 (c (n "smarty-rust-proc-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0rrgqv8p65q28fkfp6yzrr2m66zsd1867mskd37b3dvsim2in1lc") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.3.1 (c (n "smarty-rust-proc-macro") (v "0.3.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1zs611h9hwdmgi2krqa0qhy4kkx3v24ci8hsx8vp9smn4xcl31w4") (r "1.63.0")))

(define-public crate-smarty-rust-proc-macro-0.4.0 (c (n "smarty-rust-proc-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1hsdmdrncil4nbgydjd8yr2cjjpxgzqw471dspyd6i26gcwaz54w") (r "1.63.0")))

