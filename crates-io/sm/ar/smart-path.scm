(define-module (crates-io sm ar smart-path) #:use-module (crates-io))

(define-public crate-smart-path-0.3.0 (c (n "smart-path") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18b5wn4dd6vwkml48iszvisqp9dlyz4p6877jkxs3qi3z6ph6ssr") (f (quote (("unstable") ("default"))))))

(define-public crate-smart-path-0.3.1 (c (n "smart-path") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1qc4b45n18wldidw3n7ascf2p47ppf9x4cxqjqcr7xwddyvwq6hb") (f (quote (("unstable") ("default"))))))

(define-public crate-smart-path-0.4.0 (c (n "smart-path") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1s47y9f9vc68dqns6s3x1wkr0sm0lngh4a22ybdc788i0dcrqw39") (f (quote (("unstable") ("default"))))))

(define-public crate-smart-path-0.4.1 (c (n "smart-path") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wbxd5418rz0gjj1rhpf9kp6ihlvcjp2937gpv2isdpw9x44irqz") (f (quote (("unstable") ("default"))))))

