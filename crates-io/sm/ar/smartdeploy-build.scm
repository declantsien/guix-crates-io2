(define-module (crates-io sm ar smartdeploy-build) #:use-module (crates-io))

(define-public crate-smartdeploy-build-0.1.0 (c (n "smartdeploy-build") (v "0.1.0") (d (list (d (n "loam-build") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1sysf79wvq7xrd2ah9ww9kajs6pgg2glr5rjfbb30hcdy8axyxhk") (r "1.70")))

(define-public crate-smartdeploy-build-0.1.1 (c (n "smartdeploy-build") (v "0.1.1") (d (list (d (n "loam-build") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hd47fv68fwp0nf9kgjd9m02knsd43l9ad1vg1892gxvk854xnkk") (r "1.70")))

(define-public crate-smartdeploy-build-0.2.0 (c (n "smartdeploy-build") (v "0.2.0") (d (list (d (n "loam-build") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0kqyfmd5jk19c4pz50yb7f7z9220x8kl1z06m901vd6pr2h7vg02") (r "1.70")))

(define-public crate-smartdeploy-build-0.2.1 (c (n "smartdeploy-build") (v "0.2.1") (d (list (d (n "loam-build") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vfz4m9j1d8r7jzkjvymm3i2w9q7d89604afzxlpa58lmc16y84l") (r "1.70")))

