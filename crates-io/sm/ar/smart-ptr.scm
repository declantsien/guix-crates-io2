(define-module (crates-io sm ar smart-ptr) #:use-module (crates-io))

(define-public crate-smart-ptr-0.1.0 (c (n "smart-ptr") (v "0.1.0") (h "06dgkaq44sk98vdxa5xfxn3z9ranzys4hzqqkjs9gciz99h149rm") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.2.0 (c (n "smart-ptr") (v "0.2.0") (h "0ikr7babv81i3dc1lw115aj451vmb1ps6c6gfmpqdhb01vai4852") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.3.0 (c (n "smart-ptr") (v "0.3.0") (h "14zrxdri3llkip5msxyflqir036qljm6rqh38f657s7fbla3013c") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.3.1 (c (n "smart-ptr") (v "0.3.1") (h "1fq1xkjpgh0f35lx66vqjngb14gw5vd4fwxffm4wb2k9ic63ihhj") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.4.0 (c (n "smart-ptr") (v "0.4.0") (h "00dlqrbp39l6g7vii7ahjd8y1pasyzr6yg7w21929dfq8qjhhkx9") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.4.1 (c (n "smart-ptr") (v "0.4.1") (h "0vr5xm35abms73gd92llm8psd1r5ryx66y74xdacjjmzjxqdqspj") (f (quote (("alloc"))))))

(define-public crate-smart-ptr-0.5.0 (c (n "smart-ptr") (v "0.5.0") (h "0nnxbk3s2n0s05a4if6zr3xqk01n6q04g4mz32h49qc0aw1mq4hc") (f (quote (("alloc"))))))

