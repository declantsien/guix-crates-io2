(define-module (crates-io sm ar smart_meter) #:use-module (crates-io))

(define-public crate-smart_meter-0.1.0 (c (n "smart_meter") (v "0.1.0") (d (list (d (n "dlms_cosem") (r "^0.2") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "mbusparse") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (o #t) (k 0)))) (h "14mijrprxydfs5l9npkrcp9rcg19622cz94a7cgzqfd7y9ryh1v4")))

(define-public crate-smart_meter-0.2.0 (c (n "smart_meter") (v "0.2.0") (d (list (d (n "dlms_cosem") (r "^0.2") (d #t) (k 0)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "mbusparse") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (o #t) (k 0)))) (h "0algaz0fa2n55pl9bdxlymabyh693gg7x9gi4bbfxxdzqkagviwb")))

