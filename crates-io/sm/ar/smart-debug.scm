(define-module (crates-io sm ar smart-debug) #:use-module (crates-io))

(define-public crate-smart-debug-0.0.1 (c (n "smart-debug") (v "0.0.1") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kpi8s2860i8qq8r5j8myzld9f0jmb6dymrb3lpyyy57ikyxrpci") (r "1.65.0")))

(define-public crate-smart-debug-0.0.2 (c (n "smart-debug") (v "0.0.2") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-debug-derive") (r "^0.0.2") (d #t) (k 0)))) (h "0h9jzww63adad5vm3jjk0rym5cwkmjfa9r5jgk9dx9a5l9hjql75") (r "1.65.0")))

(define-public crate-smart-debug-0.0.3 (c (n "smart-debug") (v "0.0.3") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-debug-derive") (r "^0.0.3") (d #t) (k 0)))) (h "12yr21vrizrcph0kk5qccghlkja9sqmzsx87p5f0mvbjhj8qlw2l") (r "1.65.0")))

