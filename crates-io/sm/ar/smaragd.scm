(define-module (crates-io sm ar smaragd) #:use-module (crates-io))

(define-public crate-smaragd-0.1.0 (c (n "smaragd") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.13") (d #t) (k 0)) (d (n "conrod") (r "^0.51") (f (quote ("glium" "winit"))) (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 0)))) (h "0ckv0r4xdx0y75hkjbamrv48m25bzi9zlc6hfzx1j9wjvzp3vkwg")))

(define-public crate-smaragd-0.2.0 (c (n "smaragd") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "conrod") (r "^0.51") (f (quote ("glium" "winit"))) (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 0)))) (h "1q2li7cw9wbmx88lc25bzj7032mzqvz7w62yzkicaz3qvg7qcvkm")))

