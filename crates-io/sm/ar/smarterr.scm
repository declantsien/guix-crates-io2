(define-module (crates-io sm ar smarterr) #:use-module (crates-io))

(define-public crate-smarterr-0.1.0 (c (n "smarterr") (v "0.1.0") (h "1fdy0gz49a54l80j701flb4b6vn6msqmrn4pacraqkn60df2nfz1")))

(define-public crate-smarterr-0.1.1 (c (n "smarterr") (v "0.1.1") (h "1kxp00khf8vijfpyrvyz45m56ym0xdhlxg2j9k54qh4lslnwxig3")))

(define-public crate-smarterr-0.1.2 (c (n "smarterr") (v "0.1.2") (h "0dxpmqy45if3gm64ngd0cab6q7qvdfmca4r1wsj0diks27dvzm1w")))

(define-public crate-smarterr-0.1.3 (c (n "smarterr") (v "0.1.3") (h "049di8p1vrn1g7qqrx6yc6y8wrq1p0nn3qfd3lfxzdv854dk1w7j")))

(define-public crate-smarterr-0.1.4 (c (n "smarterr") (v "0.1.4") (h "0ywhxlxpv83qh7jk52038k4y5mzysn3iax1r4r3b01cdxvvqbizz")))

(define-public crate-smarterr-0.1.5 (c (n "smarterr") (v "0.1.5") (h "16ccjrlhp74jh2zjj2y6wa0byzcaj1p7rz66h5vj873w2dkbs0ng")))

(define-public crate-smarterr-0.1.6 (c (n "smarterr") (v "0.1.6") (h "0jpy0jd02qhiw6yvgzcw5w9ycb4dmz3ncd12h6fy87mf9prjbp93")))

(define-public crate-smarterr-0.1.7 (c (n "smarterr") (v "0.1.7") (h "1zdjhmknzwcw7pm0xfsxlbprxvdd7qkybh284vd12b93nvm0h10b")))

