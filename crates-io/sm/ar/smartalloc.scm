(define-module (crates-io sm ar smartalloc) #:use-module (crates-io))

(define-public crate-smartalloc-0.0.1 (c (n "smartalloc") (v "0.0.1") (d (list (d (n "smartalloc-sys") (r "=0.0.1") (d #t) (k 0)))) (h "0j732gg2hiq6bjbhkg2jgg1w9yhcj05r84hf4wgjm0lqpjy4whwx") (y #t) (r "1.64")))

(define-public crate-smartalloc-0.1.1 (c (n "smartalloc") (v "0.1.1") (d (list (d (n "smartalloc-sys") (r "=0.1.1") (d #t) (k 0)))) (h "1z0vgbyiwyl7nmlp1ggzcj34sq5fqjkdb71ryijaiiiihkx7q9mr") (y #t) (r "1.64")))

(define-public crate-smartalloc-0.2.0 (c (n "smartalloc") (v "0.2.0") (d (list (d (n "smartalloc-sys") (r "=0.2.0") (d #t) (k 0)))) (h "0qcqswf74ils4rb55pv21ak52bmf2g1vmrnm8vzbpli7sjfm6sh2") (r "1.64")))

