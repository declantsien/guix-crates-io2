(define-module (crates-io sm ar smartpoll) #:use-module (crates-io))

(define-public crate-smartpoll-0.1.0 (c (n "smartpoll") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (f (quote ("hardware-lock-elision"))) (d #t) (k 0)))) (h "17kbdyy1pbdb94frmj8fk8cri71mlqfnq2vs2mnwziwjd6nyjn34")))

(define-public crate-smartpoll-0.1.1 (c (n "smartpoll") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (f (quote ("hardware-lock-elision"))) (d #t) (k 0)))) (h "0vf4wn90llmvzxcj8mh6xfyh9sl887rr3v3g981i5x47amfsicmb")))

(define-public crate-smartpoll-1.0.0 (c (n "smartpoll") (v "1.0.0") (h "1jx6xrjhw7cjv3v1bdfx8a3ccz8c8c6q5z1hnf065a5rxmrbnzkv") (y #t)))

(define-public crate-smartpoll-2.0.0 (c (n "smartpoll") (v "2.0.0") (h "0gannr28n8kgvblfq6mbx8asidy0rlnxmlig1cg59bydjrd5rs99")))

