(define-module (crates-io sm ar smart-contract-macros) #:use-module (crates-io))

(define-public crate-smart-contract-macros-0.1.0 (c (n "smart-contract-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1b9hj9ic55bqffhck1ziikrr5li2fd2d9fk3ww3m7rbv7s84cj4d")))

(define-public crate-smart-contract-macros-0.2.0 (c (n "smart-contract-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "08vp3m8a5yn1aw99cakfn25pi5brzjzwdzmxr441262bwyrw07p1")))

(define-public crate-smart-contract-macros-0.2.1 (c (n "smart-contract-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1qif791krlg3xd9xs7iihjfm74d0gmpysn54pinrhp675iibpwwq")))

(define-public crate-smart-contract-macros-0.2.2 (c (n "smart-contract-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0rmgbs3ci2i1vw04dy2j3sqx4pm9gixwfi7hkmhhfqvrjw8zb00v")))

