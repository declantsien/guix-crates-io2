(define-module (crates-io sm ar smart_access) #:use-module (crates-io))

(define-public crate-smart_access-0.1.0 (c (n "smart_access") (v "0.1.0") (h "17ns2f9lyhpl73j69jdvb25n92lxp41w9pxzlxvzr1zkvggd4a22") (f (quote (("std_collections") ("default" "std_collections" "batch_rt" "batch_ct") ("batch_rt") ("batch_ct"))))))

(define-public crate-smart_access-0.1.1 (c (n "smart_access") (v "0.1.1") (h "1dsxpxxcjsf5ac6zlhidk6fsy0m2fb54xjdfa9wfbywdaj1dv0xj") (f (quote (("std_collections") ("default" "std_collections" "batch_rt" "batch_ct") ("batch_rt") ("batch_ct"))))))

(define-public crate-smart_access-0.1.2 (c (n "smart_access") (v "0.1.2") (h "04bdxdxbvhfxgbynn7mb3p4yi7y2gp5cjz5rl377m83nmp28jqrf") (f (quote (("std_collections") ("default" "std_collections" "batch_rt" "batch_ct") ("batch_rt") ("batch_ct"))))))

(define-public crate-smart_access-0.2.0 (c (n "smart_access") (v "0.2.0") (h "04ky0n6zcpgxyc7j68frd7lm2x06d5w7m2jwvdqw1d84j2yxxms9") (f (quote (("std_collections") ("default" "std_collections" "batch_rt" "batch_ct") ("batch_rt") ("batch_ct"))))))

(define-public crate-smart_access-0.2.1 (c (n "smart_access") (v "0.2.1") (h "0f3x3dwrqljvy1bkz31bzxh5g70aawsaf8hn3lkzw42j1n7g01jx") (f (quote (("std_collections" "std") ("std") ("default" "std" "std_collections" "batch_rt" "batch_ct") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.2.2 (c (n "smart_access") (v "0.2.2") (h "1dnczq73phv1pmx1gl2aqy17dbn479awr3kg22w6mbx0qz3pzl1g") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.3.0 (c (n "smart_access") (v "0.3.0") (h "11ccwy135cjgghyciar0rg5h7098k44fi4wasc71fs6sr5hdl1vk") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.4.0 (c (n "smart_access") (v "0.4.0") (h "0q5p9zkwyc35g7im6qr70l0a36wg8kfq4wjgz0c32sm8qmwdff90") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.4.1 (c (n "smart_access") (v "0.4.1") (h "0m9cmlj6sybang5cmrddkhyqwdgm62ksjj036l39p0bjmr917a98") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.5.0 (c (n "smart_access") (v "0.5.0") (h "0zalrs96g1bms6x1cd5qxrdphj445pcjh9alzs5zg53c0hlaayr6") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.5.1 (c (n "smart_access") (v "0.5.1") (h "11vvk5x9klbln5qms49c1ysqxrb5nvgs4f0gdi1003fw93pv5p6y") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.5.2 (c (n "smart_access") (v "0.5.2") (h "1a0pjw073lzwaqjq6i1qn2qhwl0p15jaxm7j8y2gs5phf6lv0qjk") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.5.3 (c (n "smart_access") (v "0.5.3") (h "054ssnnhqaf0c8d46dwlhqrf3wgh94yzlw9wmw1jvh8l52dqi9s8") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.5.4 (c (n "smart_access") (v "0.5.4") (h "1ssm2647y8a27fhzrv5srmia9xsxvp522yp11banywi8zs698qj6") (f (quote (("std_collections" "std") ("std") ("detach") ("default" "std" "std_collections" "batch_rt" "batch_ct" "detach") ("batch_rt" "std") ("batch_ct"))))))

(define-public crate-smart_access-0.6.0 (c (n "smart_access") (v "0.6.0") (d (list (d (n "multiref") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0xyj14wbyd39sm9v1r0i9j1acg9bfcjgjp6y2scpqmivmp36kjig") (f (quote (("std") ("iter_mut" "multiref" "std") ("detach") ("default" "collections" "batch" "detach" "iter_mut") ("collections" "std") ("batch_rt" "std") ("batch_ct") ("batch" "batch_ct" "batch_rt"))))))

(define-public crate-smart_access-0.6.1 (c (n "smart_access") (v "0.6.1") (d (list (d (n "multiref") (r "^0.1") (o #t) (d #t) (k 0)))) (h "14b80yz4j00r4z8amllrq5y04rpcnv3ifrqmmvm443k30dz8zli7") (f (quote (("std") ("iter_mut" "multiref" "std") ("detach") ("default" "collections" "batch" "detach" "iter_mut") ("collections" "std") ("batch_rt" "std") ("batch_ct") ("batch" "batch_ct" "batch_rt"))))))

(define-public crate-smart_access-0.6.2 (c (n "smart_access") (v "0.6.2") (d (list (d (n "multiref") (r "^0.1") (o #t) (d #t) (k 0)))) (h "05z2qxhdvfdg21jfimn76jb6xqw3dxx9im7r5ikshngdj0ynwz3c") (f (quote (("traversal") ("std") ("iter_mut" "multiref" "std") ("detach") ("default" "collections" "batch" "detach" "iter_mut") ("collections" "std") ("batch_rt" "std") ("batch_ct") ("batch" "batch_ct" "batch_rt"))))))

(define-public crate-smart_access-0.7.0 (c (n "smart_access") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "multiref") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ips7xqmggvq80xidy5y35zkn9f7wcw4gyyl6iw20zs92p4q3dp1") (f (quote (("traversal") ("std_hashmap") ("iter_mut" "multiref" "alloc") ("detach") ("default" "collections" "hashbrown" "batch" "detach" "iter_mut" "traversal") ("collections" "alloc") ("batch_rt" "alloc") ("batch_ct") ("batch" "batch_ct" "batch_rt") ("alloc"))))))

