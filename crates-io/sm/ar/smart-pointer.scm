(define-module (crates-io sm ar smart-pointer) #:use-module (crates-io))

(define-public crate-smart-pointer-0.1.0 (c (n "smart-pointer") (v "0.1.0") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "16amj0f4k168a99vwp5nd6fdl7j94xv734yxfcn55xlp7p77n4v3") (f (quote (("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-smart-pointer-0.1.1 (c (n "smart-pointer") (v "0.1.1") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "1m2lznng06qjs7hd591324dw3dw1zz9d9pizbs3i7qpg9bb4hshv") (f (quote (("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-smart-pointer-0.2.0 (c (n "smart-pointer") (v "0.2.0") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "04cxfs81n2fk149pcbs040h5d9fr7380mhlmrmjn0m1xhsw61i7g") (f (quote (("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

