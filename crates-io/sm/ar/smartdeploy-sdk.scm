(define-module (crates-io sm ar smartdeploy-sdk) #:use-module (crates-io))

(define-public crate-smartdeploy-sdk-0.1.0 (c (n "smartdeploy-sdk") (v "0.1.0") (d (list (d (n "smartdeploy-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1fr1al73xhf7a3lgp5fg7fpn2m81485gxy9x7fqbnpqggf8ijsz5")))

(define-public crate-smartdeploy-sdk-0.1.1 (c (n "smartdeploy-sdk") (v "0.1.1") (d (list (d (n "smartdeploy-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1i3lbiwhykzahmwwhkzbvnjy4pwnk8x1l13pfr10ixhbrgqrlm10")))

(define-public crate-smartdeploy-sdk-0.1.2 (c (n "smartdeploy-sdk") (v "0.1.2") (d (list (d (n "smartdeploy-macros") (r "^0.1.3") (d #t) (k 0)))) (h "1p8vcwi3viy245zy9h4ifv8i5isnixm2p9fffmjngqpx5kdv10kv")))

(define-public crate-smartdeploy-sdk-0.1.3 (c (n "smartdeploy-sdk") (v "0.1.3") (d (list (d (n "smartdeploy-macros") (r "^0.1.4") (d #t) (k 0)))) (h "0b35r2vdsb0czz54gisim4rdarad2g4r7qwj1c743g8xvc5mqdwd")))

(define-public crate-smartdeploy-sdk-0.1.4 (c (n "smartdeploy-sdk") (v "0.1.4") (d (list (d (n "smartdeploy-macros") (r "^0.1.5") (d #t) (k 0)))) (h "117jxrlwnvx3sgx0rw1j2dv0wc0914jx9429m3dg6nvsdzf5r0nx")))

(define-public crate-smartdeploy-sdk-0.1.5 (c (n "smartdeploy-sdk") (v "0.1.5") (d (list (d (n "smartdeploy-macros") (r "^0.1.6") (d #t) (k 0)))) (h "1ab49qmc6ar4rh9gwqvz9x37h3prdiyi90s1awyi3pfdsz9hzjcq")))

