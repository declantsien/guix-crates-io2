(define-module (crates-io sm ar smart-add-one-new) #:use-module (crates-io))

(define-public crate-smart-add-one-new-0.1.0 (c (n "smart-add-one-new") (v "0.1.0") (h "031l0bwa993v23dj30a0xj2wyjl3xywihqwkalcym7hxp8ci4kxb")))

(define-public crate-smart-add-one-new-0.2.0 (c (n "smart-add-one-new") (v "0.2.0") (h "0y35h1244q7dfslrya81w5p55zs7xfjcpbqjxl46hvj5dppjg85x")))

(define-public crate-smart-add-one-new-0.3.0 (c (n "smart-add-one-new") (v "0.3.0") (h "0hi9rkdgv3k5z9d73r2xppm5qj1a923hy33rvhb42jk7hicvyilc")))

