(define-module (crates-io sm ar smart-query) #:use-module (crates-io))

(define-public crate-smart-query-0.1.0 (c (n "smart-query") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (f (quote ("staking" "stargate"))) (d #t) (k 0)) (d (n "generic-query") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ykjmg8s6dpvkxk4mqd8dah24md6b73mxlpjv6xphshwa87fx457") (y #t)))

