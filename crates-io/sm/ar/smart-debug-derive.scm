(define-module (crates-io sm ar smart-debug-derive) #:use-module (crates-io))

(define-public crate-smart-debug-derive-0.0.2 (c (n "smart-debug-derive") (v "0.0.2") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fmiqzzy6kagz6h86c592z9sjphhdd7li43lk0vwh2k5qj8g9r9l") (r "1.65.0")))

(define-public crate-smart-debug-derive-0.0.3 (c (n "smart-debug-derive") (v "0.0.3") (d (list (d (n "insta") (r "^1.31.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mj066fwid11dq99xqzdjlyd9ck8pi7cgihmmjwz0pd80s6nf5z2") (r "1.65.0")))

