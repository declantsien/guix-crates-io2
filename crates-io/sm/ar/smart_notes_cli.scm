(define-module (crates-io sm ar smart_notes_cli) #:use-module (crates-io))

(define-public crate-smart_notes_cli-0.1.0 (c (n "smart_notes_cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("std"))) (k 0)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm" "serde"))) (k 0)))) (h "0ri9l70w8h12aafsirva8dsakpxr2smw79wh5p3jyi5csan9vrj0")))

