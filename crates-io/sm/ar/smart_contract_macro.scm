(define-module (crates-io sm ar smart_contract_macro) #:use-module (crates-io))

(define-public crate-smart_contract_macro-0.1.0 (c (n "smart_contract_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "182fd58rlp8c10ddpx7ls80whdjlgwvcf6ik25biccs3kcv577vr") (y #t)))

