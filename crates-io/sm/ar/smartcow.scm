(define-module (crates-io sm ar smartcow) #:use-module (crates-io))

(define-public crate-smartcow-0.1.0 (c (n "smartcow") (v "0.1.0") (d (list (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)))) (h "0gk9rsjf16k7kc5kc7s9glk183f97wd2dqp50xal5iwkrwyfvqq5")))

(define-public crate-smartcow-0.2.0 (c (n "smartcow") (v "0.2.0") (d (list (d (n "smartstring") (r "^1.0.0") (d #t) (k 0)))) (h "1ahgbpw8b0z6vpjyqm4q53n60hqlvi1m018s78xib7xz748bxm1c")))

(define-public crate-smartcow-0.2.1 (c (n "smartcow") (v "0.2.1") (d (list (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)))) (h "18jxicfi9q2b666vxflyjk2mxpxgv23wwd116xald36a3wfcnvv5")))

