(define-module (crates-io sm s4 sms4) #:use-module (crates-io))

(define-public crate-sms4-0.1.0 (c (n "sms4") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1crd4qbc1870phkiqvz6cx8wqnsh3fvv101a6kxrhp8qcipd1fkq")))

