(define-module (crates-io sm tp smtp-codec) #:use-module (crates-io))

(define-public crate-smtp-codec-0.1.0 (c (n "smtp-codec") (v "0.1.0") (d (list (d (n "abnf-core") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1dbd7xgzzdjsa7v4jhi48xdyp462kg070qlmgfpkvbykjwibjnqf")))

(define-public crate-smtp-codec-0.2.0 (c (n "smtp-codec") (v "0.2.0") (d (list (d (n "abnf-core") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hqyrmb2lx3rpzl4y90nk6vdbcn9vnsips2p474y8vcpsp9z43wy") (f (quote (("serdex" "serde") ("default"))))))

