(define-module (crates-io sm tp smtp-client) #:use-module (crates-io))

(define-public crate-smtp-client-0.1.0 (c (n "smtp-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "duplexify") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("write-all-vectored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 0)) (d (n "smtp-message") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.22") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (k 0)))) (h "1qiip21di87w65zdwna99h7vb3n71a5cj7db9qk2brl3yf82jwpn")))

