(define-module (crates-io sm tp smtp-queue-types) #:use-module (crates-io))

(define-public crate-smtp-queue-types-0.1.0 (c (n "smtp-queue-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "time") (r "^0.1.43") (d #t) (k 0)))) (h "13qfwvgb87szy3f0q3ds75scvw5nd7k057qr4x9jiaazb9cdj2bb")))

