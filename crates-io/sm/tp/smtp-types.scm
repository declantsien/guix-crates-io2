(define-module (crates-io sm tp smtp-types) #:use-module (crates-io))

(define-public crate-smtp-types-0.2.0 (c (n "smtp-types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jcw3j2b75z1km8zb533318qnnmjcg6h8807a9ax315ibdj7m22s") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

