(define-module (crates-io sm tp smtp) #:use-module (crates-io))

(define-public crate-smtp-0.0.1 (c (n "smtp") (v "0.0.1") (h "1ahxjj2jkpfdwhypgcwzxvraqcpjkz0si61846gg6d19mkndcfjy")))

(define-public crate-smtp-0.0.2 (c (n "smtp") (v "0.0.2") (h "0hzwyn63hsb1rz86l5xdh68dz90cai8c1x54by9lqzsybcpgzv1g")))

(define-public crate-smtp-0.0.3 (c (n "smtp") (v "0.0.3") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0al5fa3riznh41d3888x4ixlw4h1vg2jhl7wvizk2z49x8aj9x04")))

(define-public crate-smtp-0.0.4 (c (n "smtp") (v "0.0.4") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0s6cx7girnn924qx0m0ci6xw7hmzj9k7vxi1hhb5gjkmk6w3p0g8")))

(define-public crate-smtp-0.0.5 (c (n "smtp") (v "0.0.5") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1s9h368nd8zb6abkxkprbqiiahkry0ixjffs7xh0ldhgpfq54m8z")))

(define-public crate-smtp-0.0.6 (c (n "smtp") (v "0.0.6") (d (list (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1kp5p165b2nw80wpymkwsbjm1xn6pkr6yxsganfjf8ddvqdgnl5h")))

(define-public crate-smtp-0.0.7 (c (n "smtp") (v "0.0.7") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0yjgfk2arsqqzny83jk30mf0qlrmychiggm880vb5l1napa6xhgb")))

(define-public crate-smtp-0.0.8 (c (n "smtp") (v "0.0.8") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1sqnhi9jnm679plj3smyq72dgdbaxcfb4rspzrgkyxz143ki1829")))

(define-public crate-smtp-0.0.9 (c (n "smtp") (v "0.0.9") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1zwfx5lva05w1x1nh505h5ksblind0a3x37dkncx1yr4yr3qqd1a")))

(define-public crate-smtp-0.0.10 (c (n "smtp") (v "0.0.10") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1d5qd128pkz8rml59jchqnsvifqaaz1408vqvpz1h8d0rbs0bsfj")))

(define-public crate-smtp-0.0.11 (c (n "smtp") (v "0.0.11") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1490hgmmjqj8i2b1fqp5dsrajdg00058mv03g635w7kx36br5y4z")))

(define-public crate-smtp-0.0.12 (c (n "smtp") (v "0.0.12") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1gwpksj8k44l65rba72l7naw61jhz1shlc3b1jjaqspjm8z5xm6j")))

(define-public crate-smtp-0.0.14 (c (n "smtp") (v "0.0.14") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "12knwiw88zmfkyws2za4f5v8qc8yxisqyknlfldi6i1jnihy17l4") (f (quote (("unstable"))))))

(define-public crate-smtp-0.1.0 (c (n "smtp") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "02dsg02a5276cjwyvjknfibzxc2bjf3paydid6z50x54ksq94bv9") (f (quote (("unstable"))))))

(define-public crate-smtp-0.1.1 (c (n "smtp") (v "0.1.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0f2hzpz06232gc385nlrfjvf9vq7vd7ql0krl05lxmbvb8fp28ad") (f (quote (("unstable"))))))

(define-public crate-smtp-0.1.2 (c (n "smtp") (v "0.1.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "04bkzi7h506i62w0639wfh78fkwi2vn845f49b59hxn4vm938ilh") (f (quote (("unstable"))))))

(define-public crate-smtp-0.2.0 (c (n "smtp") (v "0.2.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0z5mk0dpwlpzmbscgwc5ph8nkdpy4jlwag9qr6cf2xdsdyfk1r03") (f (quote (("unstable"))))))

(define-public crate-smtp-0.3.0 (c (n "smtp") (v "0.3.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1k1a5pfyaqy5r3rp2blwfnfa86r7jfa1s9l0wmlzqd7qfbx6dg1z") (f (quote (("unstable"))))))

(define-public crate-smtp-0.3.1 (c (n "smtp") (v "0.3.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0lczcac421hgiry4237k9bjgam11nmlh1kgyr6ysy8l8dk5ialqy") (f (quote (("unstable"))))))

(define-public crate-smtp-0.3.2 (c (n "smtp") (v "0.3.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "email") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "01wmgq7sn5vxrcv9212azlw71ws3iwiljjjwirkl4bv96v0q0v2c") (f (quote (("unstable"))))))

