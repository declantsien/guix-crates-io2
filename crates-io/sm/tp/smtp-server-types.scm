(define-module (crates-io sm tp smtp-server-types) #:use-module (crates-io))

(define-public crate-smtp-server-types-0.1.0 (c (n "smtp-server-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smtp-message") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "06gvmk00jdxl6f99hibvd0qcgf7xkhhrksww6qffwlrrxki4f2cz")))

