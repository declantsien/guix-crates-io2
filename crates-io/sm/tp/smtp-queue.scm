(define-module (crates-io sm tp smtp-queue) #:use-module (crates-io))

(define-public crate-smtp-queue-0.1.0 (c (n "smtp-queue") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 0)) (d (n "smtp-message") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "smtp-queue-types") (r "^0.1.0") (d #t) (k 0)))) (h "0ja07mj75mavd1d8nqqh4s58vs99abamni5h6a97y2gcn7yacafn")))

