(define-module (crates-io sm tp smtp-proto) #:use-module (crates-io))

(define-public crate-smtp-proto-0.1.0 (c (n "smtp-proto") (v "0.1.0") (h "139b6lyhyn30pssqk0sfrgi7giglij5m556zjk7l83rfxbb312rz")))

(define-public crate-smtp-proto-0.1.1 (c (n "smtp-proto") (v "0.1.1") (h "0qpqszgxyj90jzrfyha5pva881qbbzm9nd30ngjs14ifcsn5ddyl")))

(define-public crate-smtp-proto-0.1.2 (c (n "smtp-proto") (v "0.1.2") (h "06a66b9h6niqv1l7phn30xif495frh1hsxixpsx48s7dak87hwhg")))

(define-public crate-smtp-proto-0.1.3 (c (n "smtp-proto") (v "0.1.3") (h "0hxkf9761whp6d9hzc9z9l586m01hwlk03nxfhhsrp7y2vh7mcr0")))

(define-public crate-smtp-proto-0.1.4 (c (n "smtp-proto") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10qc1q1lh3vp5wrf9y6dv5325zwz05gqxmjg4blks2yigp64sd1l") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-smtp-proto-0.1.5 (c (n "smtp-proto") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fm5cc87hn52cjyjbkkv9xm9v4d90madcamhpbgd9w47s4ysvf2i") (f (quote (("serde_support" "serde") ("default"))))))

