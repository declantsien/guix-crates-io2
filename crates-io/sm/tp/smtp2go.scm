(define-module (crates-io sm tp smtp2go) #:use-module (crates-io))

(define-public crate-smtp2go-0.1.0 (c (n "smtp2go") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1hml66r84pjj8akdvhsc86wnjnhb4lnkl05m5wb07gln4aiy0v3s")))

(define-public crate-smtp2go-0.1.1 (c (n "smtp2go") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0ac11lam8p5cg7k1q8m1aqppxdv8b2dvjgps7k11x42dxa6d77y3")))

(define-public crate-smtp2go-0.1.2 (c (n "smtp2go") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1ds2d3glbxzsi43bgivndgqbvpqfh83fl1vzvc2dcszmw692kbi3")))

(define-public crate-smtp2go-0.1.3 (c (n "smtp2go") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1wy25bi3a1b61jihvplp8rngm3vk6k2d3pimv38d5bw0m9qvg6c2")))

(define-public crate-smtp2go-0.1.4 (c (n "smtp2go") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1akhlzq9dq46mngz9k8dn9ff19ww1fpgqx54kn4yhkjymgnsysrp")))

(define-public crate-smtp2go-0.1.5 (c (n "smtp2go") (v "0.1.5") (d (list (d (n "actix-rt") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1a32k9y3cxvxhbal3d5xicrjyfaps27fvsql459bsdscdja1qhpm")))

(define-public crate-smtp2go-0.1.6 (c (n "smtp2go") (v "0.1.6") (d (list (d (n "actix-rt") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0askhzl9mhm3q490f8ndl2g752qf8fmwsgrpal5wg70q49xm05ha")))

