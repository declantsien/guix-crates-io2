(define-module (crates-io sm tp smtpapi) #:use-module (crates-io))

(define-public crate-smtpapi-0.1.0 (c (n "smtpapi") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1aszfsxhwsircgqm0csl7zlfbzs4m8rmyzaxsgx9p5cvdf1ihpfg")))

(define-public crate-smtpapi-0.1.1 (c (n "smtpapi") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1vc90l8r6hmigclmzkcgjzxbf5v06k5ixmdgl5chb0gjwxnqvjba")))

(define-public crate-smtpapi-0.1.2 (c (n "smtpapi") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0xvbgq04d3wc1vqfgzrc68p5m5r5qn916q0ca0y81z7yy8xk5d11")))

(define-public crate-smtpapi-0.1.3 (c (n "smtpapi") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "00wfg57n94h1s6ak3chylgk9j4wf3bblx6fil5g24f2s89p0ywm1")))

