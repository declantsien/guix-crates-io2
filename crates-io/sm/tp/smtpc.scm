(define-module (crates-io sm tp smtpc) #:use-module (crates-io))

(define-public crate-smtpc-0.1.0 (c (n "smtpc") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07838hbgddidmy25gjygc0jxkjpayiwj3qnwkc079dv9phab7lp5") (f (quote (("serialize" "serde" "serde_derive") ("default" "serialize")))) (y #t)))

