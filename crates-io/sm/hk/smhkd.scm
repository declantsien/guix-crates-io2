(define-module (crates-io sm hk smhkd) #:use-module (crates-io))

(define-public crate-smhkd-0.1.0 (c (n "smhkd") (v "0.1.0") (d (list (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hd0lycrq4can5vhv7lnl39dbcqgxlq4cpzda597yxw6cg921f1f")))

(define-public crate-smhkd-0.2.0 (c (n "smhkd") (v "0.2.0") (d (list (d (n "alsa") (r "^0.7") (d #t) (k 0)) (d (n "directories-next") (r "^2.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gp8j071kj8hi7gahvc47wmwamwnx86xkjcq85az856915fs6rrr")))

