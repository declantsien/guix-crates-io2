(define-module (crates-io sm bu smbus-request-parser) #:use-module (crates-io))

(define-public crate-smbus-request-parser-0.1.0 (c (n "smbus-request-parser") (v "0.1.0") (h "0khi57ljzh8s0xvrm4pw25ywp95xjxsz32pgza6jdazbwmlja43x")))

(define-public crate-smbus-request-parser-0.1.1 (c (n "smbus-request-parser") (v "0.1.1") (h "06pn53x3clq8amgsvpv5z7cvavlnzzvzf5dn1j7x6ww4l78h9g8w")))

(define-public crate-smbus-request-parser-0.1.2 (c (n "smbus-request-parser") (v "0.1.2") (h "1cpipm1nc0fki3wj6f2kfm4svj08m50hypj4n58jxcply5a80mci")))

(define-public crate-smbus-request-parser-0.2.0 (c (n "smbus-request-parser") (v "0.2.0") (h "0zbg9z5xcpx12dzpcwyn2x52sjdcczgk5q8n9j67fvakrawrdvlp")))

