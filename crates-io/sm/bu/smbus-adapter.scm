(define-module (crates-io sm bu smbus-adapter) #:use-module (crates-io))

(define-public crate-smbus-adapter-0.1.0 (c (n "smbus-adapter") (v "0.1.0") (d (list (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)))) (h "1h0qlfxmmw9zxjxklgh9hzg6gs4y6h0mxhxkcwgjivakgkb5ir0b")))

(define-public crate-smbus-adapter-0.1.1 (c (n "smbus-adapter") (v "0.1.1") (d (list (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)))) (h "1sm6zkkp5jypfma340wmlay4mhsrgc2dhla4gfmdvf51v7vw18bv")))

