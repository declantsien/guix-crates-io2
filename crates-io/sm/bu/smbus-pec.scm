(define-module (crates-io sm bu smbus-pec) #:use-module (crates-io))

(define-public crate-smbus-pec-0.1.0 (c (n "smbus-pec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "embedded-crc-macros") (r "^0.1") (d #t) (k 0)) (d (n "embedded-crc-macros") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0hyih0xb2k4nkksksjnb8rxhcyr75r38fmdmzg1d8r41fln4yw17") (f (quote (("lookup-table"))))))

(define-public crate-smbus-pec-1.0.0 (c (n "smbus-pec") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "embedded-crc-macros") (r "^1") (d #t) (k 0)) (d (n "embedded-crc-macros") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1nm84rhjr5pjmxdqw9rpwdp6ygj72jck98fsfpsm4l4yjk9fwxrr") (f (quote (("lookup-table"))))))

(define-public crate-smbus-pec-1.0.1 (c (n "smbus-pec") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "embedded-crc-macros") (r "^1") (d #t) (k 0)) (d (n "embedded-crc-macros") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0gkx44s2mg855zvx8sha710qhz8i9h2qmz3viyr74pfdh2k661ya") (f (quote (("lookup-table"))))))

