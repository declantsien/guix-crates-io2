(define-module (crates-io sm el smelter) #:use-module (crates-io))

(define-public crate-smelter-0.0.1 (c (n "smelter") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "0bfkyw4py20ak9n538k5m78p297fg9ykl96ikz2j0bk0fcyqncbg")))

(define-public crate-smelter-0.0.2 (c (n "smelter") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0.165") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "03p7sy6syn48ay5p89qi1a6pp8vhrq85p20f7blyivdn634wgk0p")))

