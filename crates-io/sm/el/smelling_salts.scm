(define-module (crates-io sm el smelling_salts) #:use-module (crates-io))

(define-public crate-smelling_salts-0.1.0 (c (n "smelling_salts") (v "0.1.0") (d (list (d (n "pasts") (r "^0.1") (d #t) (k 2)))) (h "0d1glh7705il2caapkhwa77djkxva0j49jwn2jzlk9xwhkq949lz") (f (quote (("docs-rs") ("default"))))))

(define-public crate-smelling_salts-0.2.0 (c (n "smelling_salts") (v "0.2.0") (d (list (d (n "pasts") (r "^0.7") (d #t) (k 2)))) (h "0xh099w7pgrzw99zmykzgqpmg856j8wlph1c321zs53071izhz4b")))

(define-public crate-smelling_salts-0.2.1 (c (n "smelling_salts") (v "0.2.1") (d (list (d (n "pasts") (r "^0.7") (d #t) (k 2)))) (h "09pfhq734sjmyph12hqfshw2aaxc61i2a04y6czcja8j2imj2r9y")))

(define-public crate-smelling_salts-0.2.2 (c (n "smelling_salts") (v "0.2.2") (d (list (d (n "pasts") (r "^0.7") (d #t) (k 2)))) (h "0fyg8wi1q8mzhg0ls0nkqv21322hnlmi52614845yg186fmjbj90")))

(define-public crate-smelling_salts-0.2.3 (c (n "smelling_salts") (v "0.2.3") (d (list (d (n "pasts") (r "^0.7") (d #t) (k 2)))) (h "11lwxdzjf70yapvmys6dsvlw406mw79nranv8ids37fbbccgxczm")))

(define-public crate-smelling_salts-0.2.4 (c (n "smelling_salts") (v "0.2.4") (d (list (d (n "pasts") (r "^0.7") (d #t) (k 2)))) (h "0s7hpbk3vs8cd3dlfa5aq58xx6aibzfy8h7ya8h0kbm79b4w6gr7")))

(define-public crate-smelling_salts-0.3.0 (c (n "smelling_salts") (v "0.3.0") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "0mh2w1iyd65xy1rr2nv8g073qdwyz1850vyw4xngas313giasi2w")))

(define-public crate-smelling_salts-0.3.1 (c (n "smelling_salts") (v "0.3.1") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "16bi5bcncwdyc1hycrxa50wjpqhw9h7hv0vn1h081vsqcyapafwg") (y #t)))

(define-public crate-smelling_salts-0.4.0 (c (n "smelling_salts") (v "0.4.0") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "0yv4q4aa7zbx4z2kvx3h7r0ij798knwxah5xmdirdmfnmwj3pb5h")))

(define-public crate-smelling_salts-0.5.0 (c (n "smelling_salts") (v "0.5.0") (d (list (d (n "flume") (r "^0.10") (f (quote ("async"))) (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "16022mrza3yd22bhya6hygcbk4scihi8x51mspah69j6jbx74ql9")))

(define-public crate-smelling_salts-0.5.1 (c (n "smelling_salts") (v "0.5.1") (d (list (d (n "flume") (r "^0.10") (f (quote ("async"))) (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "1bdzij5w3s9hyl92hrbkqxlkicg9scgsdgrjq15pzq89rffpgckc")))

(define-public crate-smelling_salts-0.6.0 (c (n "smelling_salts") (v "0.6.0") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)))) (h "1h41jm7zjhdkz1qdkjk15scxqp6xi2x25alhvjmh11zrjpg471g2")))

(define-public crate-smelling_salts-0.7.0 (c (n "smelling_salts") (v "0.7.0") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.7") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1rq8aqid8s9ryhqndcd4f3lk2zv1yjsdh6fnphxcgxlzd0r9zcr5") (f (quote (("runloop") ("kqueue") ("epoll")))) (r "1.60")))

(define-public crate-smelling_salts-0.7.1 (c (n "smelling_salts") (v "0.7.1") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.7") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1h44f8rfdwwbp4z9bhjv7xby15jnr4zq0jcz1ix3v5lq6rj79l7n") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.60")))

(define-public crate-smelling_salts-0.7.2 (c (n "smelling_salts") (v "0.7.2") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.7") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1db5b7rjcsqrv7ykm6pm5mcblx23bc74005krb1xg0289ld2jk2w") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.60")))

(define-public crate-smelling_salts-0.8.0 (c (n "smelling_salts") (v "0.8.0") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.8") (f (quote ("pasts"))) (d #t) (k 0)))) (h "07kihjm62jd5g3gwqjqg5qq53j97dp6bjhf7rwl48pyf68pca2z4") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.64")))

(define-public crate-smelling_salts-0.9.0 (c (n "smelling_salts") (v "0.9.0") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.8") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1ham1v1wgm5xiylfw2j72ld80qqwff7qaad4pjkn60v00b6wlrb4") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (y #t) (r "1.64")))

(define-public crate-smelling_salts-0.9.1 (c (n "smelling_salts") (v "0.9.1") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.9") (f (quote ("pasts"))) (d #t) (k 0)))) (h "0854v2rlbxn5s33lbpb4q4y9j9ganlh58r3373yhg91zrbjm7h43") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.65")))

(define-public crate-smelling_salts-0.9.2 (c (n "smelling_salts") (v "0.9.2") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.9") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1wjs7lvbjhncwxbvhianyvdr38y5ngnq0a2kwix4y94zjxry3b6q") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.65")))

(define-public crate-smelling_salts-0.10.0 (c (n "smelling_salts") (v "0.10.0") (d (list (d (n "async_main") (r "^0.2") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.13") (d #t) (k 0)) (d (n "whisk") (r "^0.10") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1dvgx6rq5lc6ghbyjqrsczvyryy1wrv8771y7w0sn9ii5iq84mvq") (f (quote (("runloop") ("kqueue") ("epoll") ("default")))) (r "1.65")))

(define-public crate-smelling_salts-0.11.0 (c (n "smelling_salts") (v "0.11.0") (d (list (d (n "async_main") (r "^0.2") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.13") (d #t) (k 0)) (d (n "whisk") (r "^0.10") (f (quote ("pasts"))) (d #t) (k 0)))) (h "102b1qj71p64ms7ggp08g6f3s96dh9v8nzgy4qpqw0maws5z6aza") (f (quote (("examples-platform-linux") ("default")))) (r "1.66")))

(define-public crate-smelling_salts-0.12.0 (c (n "smelling_salts") (v "0.12.0") (d (list (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 0)) (d (n "whisk") (r "^0.11") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1129r0wpg70lhylcpjbmnnmhfh822dlc00ai2s8p40d4il2d0xdi") (f (quote (("examples-platform-linux") ("default")))) (r "1.66")))

(define-public crate-smelling_salts-0.12.1 (c (n "smelling_salts") (v "0.12.1") (d (list (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 0)) (d (n "whisk") (r "^0.11") (f (quote ("pasts"))) (d #t) (k 0)))) (h "0sp1i2rdn39lqabgx4c93gp8hh6kncnb3qjp968akg3qylb4lr7g") (f (quote (("examples-platform-linux") ("default")))) (r "1.66")))

