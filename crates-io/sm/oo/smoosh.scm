(define-module (crates-io sm oo smoosh) #:use-module (crates-io))

(define-public crate-smoosh-0.1.0 (c (n "smoosh") (v "0.1.0") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "1dk3f6l4lq7l5ikf9w76c1m1a48inv97jrzdfcizh3m9fs23xcl4")))

(define-public crate-smoosh-0.1.1 (c (n "smoosh") (v "0.1.1") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "10ks7rvzcgm85kczmz780dyvz7g7nlhk0xmwbyg1ndn15ybb8abf")))

(define-public crate-smoosh-0.1.2 (c (n "smoosh") (v "0.1.2") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "02psap9ya7pp85mbl629jka8x58aqyjqpxyl2zjmwvpkn5y1fddp")))

(define-public crate-smoosh-0.1.3 (c (n "smoosh") (v "0.1.3") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "057dyrz0f3a12an0fb4ljsg3fcn265pcnl07q7qjb6lviv885nky")))

(define-public crate-smoosh-0.2.0 (c (n "smoosh") (v "0.2.0") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "00077sd6k3vkvzwyf3b3jsa2w4khgfvq3y187lb0n6qqf29nhg6f")))

(define-public crate-smoosh-0.2.1 (c (n "smoosh") (v "0.2.1") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("all-algorithms" "tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (d #t) (k 0)))) (h "0kp21s1zxds6ch94ks85yd0kwyd77wm5d5drivfsdmlcnfrllnqc")))

