(define-module (crates-io sm oo smoothy) #:use-module (crates-io))

(define-public crate-smoothy-0.1.0 (c (n "smoothy") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0k80myj7mpgcihhaymgzhxw69pjpw61i151hm5pysvpr1rz0wvym") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.2.0 (c (n "smoothy") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0la0d9239c6w4n77nxwjmjpbsl4njpa2vqn1naqxw1f8hcgn0ki1") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3.0 (c (n "smoothy") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "11qpiyz627w8kf5g0v2qqzqi75l7fa858llsyw86ibd1vw3dlmdz") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3.1 (c (n "smoothy") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0vyr80vl4zrjq2fjx3kplskyphgx4gj653gqwhyw6z64n5v16m1q") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3.2 (c (n "smoothy") (v "0.3.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0c1pfl8xpddmr699i8sdbjkk10k5ybx6kgas38d68g6ps383v2b1") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3.3 (c (n "smoothy") (v "0.3.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "14a6kcg4nxlp160hi5mh181z45wmh1nvv90iqmjc24j4kfhhyfja") (f (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.4.0 (c (n "smoothy") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0bpahqhfhqbnhfzsdqqdvvzfbrx4rnj7x82rpj7iqlklb0dp3s7i") (f (quote (("default"))))))

(define-public crate-smoothy-0.4.1 (c (n "smoothy") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0580hv26m8x5f72sdz39wn7lpg5hj1xd4kqkj76qwv67dpc5w0q5") (f (quote (("default"))))))

(define-public crate-smoothy-0.4.2 (c (n "smoothy") (v "0.4.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "104hjvxqni623zmclvf4dapri7bm2w9lpsn8pjjpx6vra93z7x34") (f (quote (("default"))))))

(define-public crate-smoothy-0.4.3 (c (n "smoothy") (v "0.4.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i5d1p55i7jx9rwi1qb2j6aa559a4jyrg835zhp7b77mqrc3k3nc") (f (quote (("default"))))))

(define-public crate-smoothy-0.4.4 (c (n "smoothy") (v "0.4.4") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1akanyn8r3yfiqbph8fd2lgnyc7z7l0qkh3cjzizz6mv8rjhi3aj") (f (quote (("default"))))))

