(define-module (crates-io sm oo smooth-json) #:use-module (crates-io))

(define-public crate-smooth-json-0.1.0 (c (n "smooth-json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0c0pv2qxqaw3bpgx8mf8cwmnj1fjyk1ryxxv0v5mbysqnhvn3mgn")))

(define-public crate-smooth-json-0.2.0 (c (n "smooth-json") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0p26s5bfmf8jyf4zz0jj2qk9kaz9761wyrpg07mr55fprf5js3dl")))

(define-public crate-smooth-json-0.2.1 (c (n "smooth-json") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0i9awllzq898nv9dwnl8dqkfw28511wrcv2pnglbwwsdzn06h89p")))

(define-public crate-smooth-json-0.2.2 (c (n "smooth-json") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1ynx25wdvkm897067r24bbpgw9kpjgzay0b9kxrc0hb3153jcs99")))

(define-public crate-smooth-json-0.2.3 (c (n "smooth-json") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "164n59vrbmp9svghb3c4rzbzzr0brk7401v52cjylksjcv6ix7vi")))

(define-public crate-smooth-json-0.2.4 (c (n "smooth-json") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "18ybnfcyr9hs34f5z8smcvn5nm9bgx51nrv0vcw3nljdv7k5jvki")))

(define-public crate-smooth-json-0.2.5 (c (n "smooth-json") (v "0.2.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nxn9vbi2v15bi21cnw5snjngzz7sg24m4g7r8sfwjhgq323xv0w")))

(define-public crate-smooth-json-0.2.6 (c (n "smooth-json") (v "0.2.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yjr27d1mrv6v40p79xc7gcml0mm27swncya4f6hy7vky8bi41c2")))

