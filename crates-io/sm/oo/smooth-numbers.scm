(define-module (crates-io sm oo smooth-numbers) #:use-module (crates-io))

(define-public crate-smooth-numbers-0.1.0 (c (n "smooth-numbers") (v "0.1.0") (d (list (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "11i67xg2qia59i37vj3xqf34dmag97vsks667aqw0bng1w0r0ych")))

(define-public crate-smooth-numbers-0.2.0 (c (n "smooth-numbers") (v "0.2.0") (d (list (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "1ra6q07mkiqjjw7kfkcyjhsdibsia7vqdkd8wrc9jgxc760mmz8s")))

(define-public crate-smooth-numbers-0.2.1 (c (n "smooth-numbers") (v "0.2.1") (d (list (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "1nzwjcd28khsnqd6ss46vqaj27mxdrwa1fn7638mkja4g2yk3y6m")))

(define-public crate-smooth-numbers-0.3.0 (c (n "smooth-numbers") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "0ardvv2qmx5zssjrmd0azal9222jkaiig7bpxv6hc1w9s6nc49f8")))

(define-public crate-smooth-numbers-0.3.1 (c (n "smooth-numbers") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "04fh7ziax4d6aa2nm3b78vj6f6j0dnfg5mlj9fnagbjc029zxrff")))

(define-public crate-smooth-numbers-0.4.0 (c (n "smooth-numbers") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "1cd86dniqffab5fx8754vv2mg2mjz6zc9vqdydynfg4w0r2zc2sm")))

(define-public crate-smooth-numbers-0.4.1 (c (n "smooth-numbers") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "1igl5cylf0n1kvxwy0wfv4ig5va919npgcgr20s5ijf0qkp6k3kp")))

(define-public crate-smooth-numbers-0.4.2 (c (n "smooth-numbers") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "primal") (r "^0.3.2") (d #t) (k 0)))) (h "1dks3bb4dvliig3ni1xp8fgx740bwn0c8bl9dbr70l3ay56ppj93")))

