(define-module (crates-io sm oo smooth-stream) #:use-module (crates-io))

(define-public crate-smooth-stream-0.1.0 (c (n "smooth-stream") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vay4bxfr5cpvvi0rbjysbk2p1qp7fzvzmkbhvi9si6n04zr44n9")))

(define-public crate-smooth-stream-0.1.1 (c (n "smooth-stream") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "059agm2hh368gkadp0818bnxg05d7q9ynrsv0y3f5yw46mjr4iya")))

