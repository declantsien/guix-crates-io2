(define-module (crates-io sm oo smooth) #:use-module (crates-io))

(define-public crate-smooth-0.1.0 (c (n "smooth") (v "0.1.0") (h "0skvp5q8z25l6rf3abcldi2m9k8qm6cgkw6rlm9iqycw1y4jbw6a")))

(define-public crate-smooth-0.1.1 (c (n "smooth") (v "0.1.1") (h "0j02nhv0kmz6bmk2x2g823m4hhdyssy1ampaf6597sprjvsiwps5")))

(define-public crate-smooth-0.2.0 (c (n "smooth") (v "0.2.0") (h "0s4br71lgwg6mhkm993igas6mjr34npmyhc81m4ab2ba3ba1nja0") (r "1.56.1")))

