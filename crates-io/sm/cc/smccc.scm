(define-module (crates-io sm cc smccc) #:use-module (crates-io))

(define-public crate-smccc-0.1.0 (c (n "smccc") (v "0.1.0") (h "1psca08511rnhg9iik6gbbxzqwyfqk57ayc45hivd59iwzryz74p")))

(define-public crate-smccc-0.1.1 (c (n "smccc") (v "0.1.1") (h "137nwvmcqlfsdp0n2q3dhmp4whg1rkj6rnl6ddd3wwzci3q1fzb1")))

