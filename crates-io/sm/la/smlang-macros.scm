(define-module (crates-io sm la smlang-macros) #:use-module (crates-io))

(define-public crate-smlang-macros-0.1.0 (c (n "smlang-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12cf8ap0qphv4s0vrjsd8pjfybfkgabkicaa411wvhckhmv2pfwi") (y #t)))

(define-public crate-smlang-macros-0.1.1 (c (n "smlang-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bw4k599y5rk7axl9dbh13gys0vg58nrbqlcpg6rjj73sh16gigh")))

(define-public crate-smlang-macros-0.1.2 (c (n "smlang-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qnpyarlir5zbzp8gvihlmrni1fzzalvw9dhi2b5x7543kwm9rkx")))

(define-public crate-smlang-macros-0.1.3 (c (n "smlang-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10xrc20i0kgxakdjsy64bii7qm6za0h3g815g6f3qnmnqcwz113v") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.4 (c (n "smlang-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zc322a2m69v8iz6zgsldqslfbw0sb92msmij8844jzl1pz4hna7") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.5 (c (n "smlang-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pypvgii7p2lwb14gm9xc1w7g4zjgbc0c6m5md9lsf4iq6r3isvp") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.6 (c (n "smlang-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1b6vp6w7qgmw7i5yfjdxi4hfwyd6i81mnj4grmf0qz2jzybyqpfv") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.7 (c (n "smlang-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "063774fvghfcbvhlr8bjpy2hsixnmb91a2fa3mla51yqhflrz8wd") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.8 (c (n "smlang-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1z6jw414zrwxjiqcki0by871gjyxybjwzixldl1gcz8ihpy6yryj") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.9 (c (n "smlang-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zvxb6szyhqnxm1ycnh1rr0pr0zxdq8s3zcnbcwg597a1zmnmrn9") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.10 (c (n "smlang-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18h9nr9ywr7dr3qfmagcayx3mr15q7c7b2d0sp777cqzhy2c0qn2") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.1.11 (c (n "smlang-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "188pz0mksblyax4xkna6vcg8y7cm015v07hi7z2i7m5dj1df5flv") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.2.0 (c (n "smlang-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hkmjlcmwrh1c4vymkd87l1zix2w3bjd6jg0f41zx17rrdnr7w1j") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.2.1 (c (n "smlang-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l3jmjqk4yqdh2al9sdk8p1pwm8g7y9si2a8vciq8izq2pcvfvdc") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.5.0 (c (n "smlang-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bc8yi2v6r9lhc1ci7bfzvrnmh04qcxsqi551qn433lzbilbrzf5") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.5.1 (c (n "smlang-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13sdv9rhzgy3rnvm88116nrk20a9982wq1z3dg2l4m4ixqwfq3x2") (f (quote (("graphviz"))))))

(define-public crate-smlang-macros-0.6.0 (c (n "smlang-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0s8l13gr24xkjjpk925m4x5psgh2xwd4nrbdcmhww3acrl2wr0n6") (f (quote (("graphviz"))))))

