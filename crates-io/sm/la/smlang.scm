(define-module (crates-io sm la smlang) #:use-module (crates-io))

(define-public crate-smlang-0.1.0 (c (n "smlang") (v "0.1.0") (d (list (d (n "smlang-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0scdnl0z17pvzlr9ssk27qg2kx8nyd76fi6qrsxns98w0rxfp22g")))

(define-public crate-smlang-0.1.1 (c (n "smlang") (v "0.1.1") (d (list (d (n "smlang-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1z6i1nk20nxhrzga347sj1qg3k7qsvvnzq2n60wiam1qpgcmr6as")))

(define-public crate-smlang-0.1.2 (c (n "smlang") (v "0.1.2") (d (list (d (n "smlang-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1g3fbf2idj6cx0kl8bkil4nsilwhyn12by7zbibrc7nkypdin7dq")))

(define-public crate-smlang-0.1.3 (c (n "smlang") (v "0.1.3") (d (list (d (n "smlang-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0g6x93vm9a9li5274kzdk6ixxr9jg0kj8lxw7acrjx6svni7il94")))

(define-public crate-smlang-0.2.0 (c (n "smlang") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.3") (d #t) (k 0)))) (h "1g1cw8l69k3b3diz1dliwp6qlc8r53mhiwp20kbybrksfbvijq47") (f (quote (("graphviz" "smlang-macros/graphviz"))))))

(define-public crate-smlang-0.2.1 (c (n "smlang") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.4") (d #t) (k 0)))) (h "0c436a14rqf6nrz11c78xnyicxkijidizq6smmzq1pwy3afkn0il") (f (quote (("graphviz" "smlang-macros/graphviz"))))))

(define-public crate-smlang-0.2.2 (c (n "smlang") (v "0.2.2") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.5") (d #t) (k 0)))) (h "1k5c14sgai1y4fnz2insiyy6jh6sbp58jbkz77r6qlimlq4pfq0i") (f (quote (("graphviz" "smlang-macros/graphviz"))))))

(define-public crate-smlang-0.3.0 (c (n "smlang") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0vgyywyqcka469qaibrmwsb2f8ddjc3mjf1xvg44kxl2hfz4dinr") (f (quote (("graphviz" "smlang-macros/graphviz"))))))

(define-public crate-smlang-0.3.1 (c (n "smlang") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.7") (d #t) (k 0)))) (h "0w11hkvzm845c9rljlj5j0p1avxpar7sc5116vyc950gaqpmh0p8") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.3.2 (c (n "smlang") (v "0.3.2") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.8") (d #t) (k 0)))) (h "0p8i5yay0s6pb03d3iw2bbyzy7cz277f765790wav1qnn0knk1ym") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.3.3 (c (n "smlang") (v "0.3.3") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)) (d (n "smlang-macros") (r "^0.1.9") (d #t) (k 0)))) (h "1mh1bri7fgl6a7a7r7lvpz6xx9hk5c42piddd8fmv2vh0cc3i6r2") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.3.4 (c (n "smlang") (v "0.3.4") (d (list (d (n "smlang-macros") (r "^0.1.10") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "086mlvpp9ljw464dxdkmj9jp1cfmf83cpkfgpag4d82ix20xkmfb") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.3.5 (c (n "smlang") (v "0.3.5") (d (list (d (n "smlang-macros") (r "^0.1.11") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "01fi243s5q1kbxkji0h7ckgbrs54wxcmm1w1pyd33hb25i9n42jp") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.4.0 (c (n "smlang") (v "0.4.0") (d (list (d (n "smlang-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "1xczjkgbnwf2i665ll1ag4vnkdham3gkfnxr0dmx5zrw7paq0s6y") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.4.1 (c (n "smlang") (v "0.4.1") (d (list (d (n "smlang-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "13pmsbidngg4dxygpjhzaldc2s0wly9iippy39ss5xm6pbj5rbhp") (f (quote (("graphviz" "smlang-macros/graphviz") ("default")))) (y #t)))

(define-public crate-smlang-0.4.2 (c (n "smlang") (v "0.4.2") (d (list (d (n "smlang-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "15l8h6gpc8l56rf01wr7izf1fxl00gf4smz0r26c6c43ci0sdxkm") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.5.0 (c (n "smlang") (v "0.5.0") (d (list (d (n "smlang-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "1n7s1n3a0xgag2zzpwv2xq73256ipi2hm8iyk3lc11l8847b385j") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.5.1 (c (n "smlang") (v "0.5.1") (d (list (d (n "smlang-macros") (r "^0.5.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "0kplcwhyis8qahjps9v7vw8mkidh3cd8f5zanrx8nw8a7ic212ka") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

(define-public crate-smlang-0.6.0 (c (n "smlang") (v "0.6.0") (d (list (d (n "smlang-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "1s9jqp9a99z5nb1qdsbg1l3nys6i2d28sn2hbfigx6g9fwqjcaxn") (f (quote (("graphviz" "smlang-macros/graphviz") ("default"))))))

