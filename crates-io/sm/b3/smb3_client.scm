(define-module (crates-io sm b3 smb3_client) #:use-module (crates-io))

(define-public crate-smb3_client-0.1.0 (c (n "smb3_client") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "cmac") (r "^0.7") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_smb") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "smb3") (r "^0.1") (d #t) (k 0)) (d (n "sspi-bobbobbio") (r "^0.10.1") (d #t) (k 0)))) (h "1aqk45xp2cwimb60qiz4wcipah6zr49vbrnsiclm26kdank6ifxa")))

