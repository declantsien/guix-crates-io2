(define-module (crates-io sm b3 smb3) #:use-module (crates-io))

(define-public crate-smb3-0.1.0 (c (n "smb3") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bitflags_serde_shim") (r "^0.2") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_dis") (r "^0.1.3") (d #t) (k 0)))) (h "0hj9fkp16wb55prdgk1fz37qpws5q19zf1ixq9vii80yxiv0pr0p")))

