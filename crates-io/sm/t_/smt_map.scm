(define-module (crates-io sm t_ smt_map) #:use-module (crates-io))

(define-public crate-smt_map-0.0.1 (c (n "smt_map") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "uint") (r "^0.5.0") (d #t) (k 0)))) (h "1175gn5g8zr1xfavc0z1p8xfm2dhrmwnpbs1l3mab41chm5wss1g")))

(define-public crate-smt_map-0.0.2 (c (n "smt_map") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "uint") (r "^0.5.0") (d #t) (k 0)))) (h "09llx09bdpn35r76k0x3f0vqszv8zhs7yx7vyyiyg3anp695i7nq") (f (quote (("test" "std") ("std"))))))

(define-public crate-smt_map-0.0.3 (c (n "smt_map") (v "0.0.3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "1nc47lizikjdvqwmzj7cj8gg2qpjz6vqny9fq01q2dcz3wk97znh") (f (quote (("test" "std") ("std"))))))

(define-public crate-smt_map-0.0.4 (c (n "smt_map") (v "0.0.4") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "1yxdpa4cs8nbd2l2vk5rcwn5jy4r3krars6nvq760xn9jydi1w3v")))

(define-public crate-smt_map-0.0.5 (c (n "smt_map") (v "0.0.5") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "01d61z2is3bk4mj625avlnd794q3ss5zy6dh27vam5h3aadrpy4m")))

