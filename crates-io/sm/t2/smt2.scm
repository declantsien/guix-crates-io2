(define-module (crates-io sm t2 smt2) #:use-module (crates-io))

(define-public crate-smt2-0.1.0 (c (n "smt2") (v "0.1.0") (d (list (d (n "source-span") (r "^1.3.0") (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.1") (d #t) (k 0)))) (h "1knnwwym2y55xq0wig9399yzs6c2gq01z9kx8bpsr17fgmrcybwc")))

(define-public crate-smt2-0.2.0 (c (n "smt2") (v "0.2.0") (d (list (d (n "source-span") (r "^2.2") (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "1cm85b6dmj5p1gpcfb9ycjxh8vfmj6h603h4if3igqplf42fb6b5")))

