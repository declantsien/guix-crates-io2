(define-module (crates-io sm t2 smt2patch) #:use-module (crates-io))

(define-public crate-smt2patch-0.0.0 (c (n "smt2patch") (v "0.0.0") (h "1lhjpy391fyszs6a1i6v0csd6z35zhxqnn8cwvm6i349an7lh9ac")))

(define-public crate-smt2patch-0.1.0 (c (n "smt2patch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0nh459zrwfhy656vf1n87bw7lpdz17bfrx79zm8bnb1gf4bma7bc")))

(define-public crate-smt2patch-0.1.1 (c (n "smt2patch") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "14ln9299ch4jr63wfv59b8zxpijv5x9wd5957f5d8kgsg8d54xjg")))

