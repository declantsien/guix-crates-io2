(define-module (crates-io sm t2 smt2proxy) #:use-module (crates-io))

(define-public crate-smt2proxy-0.0.0 (c (n "smt2proxy") (v "0.0.0") (h "1zdl3b5nssk0rm9q09cyx15ridgnaaa4bp7sm2cw7830q0prhms3")))

(define-public crate-smt2proxy-0.1.0 (c (n "smt2proxy") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "084gq2knpgcdn3pdps9ld4ip1xmray3fkh2qqx9dm0nrp2rya65z")))

(define-public crate-smt2proxy-0.2.0 (c (n "smt2proxy") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "02h6ha6ldjnp6b5p428gp7sdxixasgibcmxm4hdmbs19746672nq")))

(define-public crate-smt2proxy-0.2.1 (c (n "smt2proxy") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0inrz7x91a54c996shwq549cvn622846x8npdfagngmlwn9j34az")))

(define-public crate-smt2proxy-0.2.2 (c (n "smt2proxy") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0gx5vj171snq983h303bil3y3jay6dr7m2kza8ji8vkrypq2k15x")))

(define-public crate-smt2proxy-0.2.3 (c (n "smt2proxy") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.5.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0qq6qzs0swn7cxidjc7hf1a5vagdwdmw47k7h8505d4k5y6nhvvy")))

(define-public crate-smt2proxy-0.2.4 (c (n "smt2proxy") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "smt2parser") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)))) (h "02fcyq2ifqic3aysqvml73qwc4pnzrwzw7l35sskq1i9gdcgqr5r")))

