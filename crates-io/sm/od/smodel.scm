(define-module (crates-io sm od smodel) #:use-module (crates-io))

(define-public crate-smodel-0.1.0 (c (n "smodel") (v "0.1.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^0.1") (d #t) (k 0)))) (h "1fraiqxflmrj7ma9cghw9293l5pzi8bnpb2y7ck1w9cpwkfk61q4")))

(define-public crate-smodel-1.0.0 (c (n "smodel") (v "1.0.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "02ggnlq2r7dak1cqc086jh4n84kbvnbxmky55qm83kygq4xg3n20")))

(define-public crate-smodel-1.0.1 (c (n "smodel") (v "1.0.1") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1ij02hqpdg9nmcaxfd9ml8dap087q8z7knvmqanhhwcc3zb6nqbq")))

(define-public crate-smodel-1.0.2 (c (n "smodel") (v "1.0.2") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "11v4sbz3p921378sdnfhycmc4jbycj08y7kdwnjdrh4l5k0gkliv")))

(define-public crate-smodel-1.0.3 (c (n "smodel") (v "1.0.3") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "02ig6rmfnpxbr90mhl694s9by20j8lgl5idzml67br7ldi6jzknv")))

(define-public crate-smodel-1.0.4 (c (n "smodel") (v "1.0.4") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1bikf6wmgy0i2qcp3plb409455cx0yv41kl793mzp7a3bmnlqpbg")))

(define-public crate-smodel-1.0.5 (c (n "smodel") (v "1.0.5") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "14plsyjknp0nqcvcpc1mv36j1256rmc3qx6gzfqn8rfagdg4v2dh")))

(define-public crate-smodel-1.0.6 (c (n "smodel") (v "1.0.6") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1z9c9d2ykllac9m6a5nnxm3w47n0qqqikjgmj74pksfs0dmiz77m")))

(define-public crate-smodel-1.0.7 (c (n "smodel") (v "1.0.7") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1k2lgb40i0xsf72gys0mprpv907k5rqxg2v25nxr6wl7s43jghkm")))

(define-public crate-smodel-1.0.8 (c (n "smodel") (v "1.0.8") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1r7abr8a3jwnhydxr9hhkbbljmksdh7hd79sxjagn25gwm1s17l8")))

(define-public crate-smodel-1.0.9 (c (n "smodel") (v "1.0.9") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "141vvz1jdmwhhpdl833jhysx6z1qgw6b3p2s99piw0gbdmvyyi78")))

(define-public crate-smodel-1.0.10 (c (n "smodel") (v "1.0.10") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1p45ab01qwrn85sjjr37xacg281c1gdrk6h1m9j0hra235s4js3j")))

(define-public crate-smodel-1.0.11 (c (n "smodel") (v "1.0.11") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1ca0r1030lzgy7bi05gn4zb1gldw64hj23fzawgh3ss6g2gqa279")))

(define-public crate-smodel-1.0.12 (c (n "smodel") (v "1.0.12") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "1azv5acshs3xsy9nycfsijgq7lp5kpay4g0n0rlgjvb93vlfyi5l")))

(define-public crate-smodel-1.0.13 (c (n "smodel") (v "1.0.13") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "smodel-proc") (r "^1.0") (d #t) (k 0)))) (h "134gqbxbfj94svspy0gy2jhd5p2a4nv587ayx7slzd2j575bnsyz")))

