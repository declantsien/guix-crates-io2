(define-module (crates-io sm od smodel-proc) #:use-module (crates-io))

(define-public crate-smodel-proc-0.1.0 (c (n "smodel-proc") (v "0.1.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "09i5sflzi41w2v2nvkbr5q1dj413ngki1yjdkdv6fm9nd5416pfl")))

(define-public crate-smodel-proc-1.0.0 (c (n "smodel-proc") (v "1.0.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "043fk0mjpb06ldmjvqwskxdmmdysvnj90fnazv8fay3fjbhvwnq3")))

(define-public crate-smodel-proc-1.0.1 (c (n "smodel-proc") (v "1.0.1") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0w5pfid8fxb4x9mm6pizy62213lmj8jfk2xvmhl39h56mimaszwr")))

(define-public crate-smodel-proc-1.0.2 (c (n "smodel-proc") (v "1.0.2") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0ca5jh5b14swh1i0la8xyb1lg40wx8n3x7qwnid3r16492fcyjad")))

(define-public crate-smodel-proc-1.0.3 (c (n "smodel-proc") (v "1.0.3") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "111h0d8nka2fzqa1yjxskq285id6az9bgim82hwj4v41mkxk9p00")))

(define-public crate-smodel-proc-1.0.4 (c (n "smodel-proc") (v "1.0.4") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1swcnmli02yp4ggrd75zrgazngv4ikpcz3xd0mllwzydz2i1rzpy")))

(define-public crate-smodel-proc-1.0.5 (c (n "smodel-proc") (v "1.0.5") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "02fzd8q9z5lh4hfb6y802cw1q5ci61i1qn7w97g6v6gfa602k4bw")))

(define-public crate-smodel-proc-1.0.6 (c (n "smodel-proc") (v "1.0.6") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "15j0x0c7xl9slyxpal8pkzwx422rk0qmh86gqvsmqbas0chl2mf6")))

(define-public crate-smodel-proc-1.0.7 (c (n "smodel-proc") (v "1.0.7") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "19ailqm5cf5c2ifph9lxp7gw36gl3hhx63j3gdvalg08mrisd4wz")))

(define-public crate-smodel-proc-1.0.8 (c (n "smodel-proc") (v "1.0.8") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1xw1kj7bdnz9l204zpl9b2f0yfzkn3jkcrbsndd269lrwdqsmqnp")))

(define-public crate-smodel-proc-1.0.9 (c (n "smodel-proc") (v "1.0.9") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8jx3ydnap3if4j9mhwn0d0fiarn3k8y787b1ydgjbn52f6h30j")))

(define-public crate-smodel-proc-1.0.10 (c (n "smodel-proc") (v "1.0.10") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0pbkbb9d2s31fdblj7rwq8fa4swxg6k5g1ka9fpmaivz01wf3gx3")))

(define-public crate-smodel-proc-1.0.11 (c (n "smodel-proc") (v "1.0.11") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "15kxn16xgmvhdjqsvysxm0blhg636ffd99x6z54xda0nh3g895lv")))

