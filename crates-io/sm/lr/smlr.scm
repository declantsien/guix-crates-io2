(define-module (crates-io sm lr smlr) #:use-module (crates-io))

(define-public crate-smlr-0.1.0 (c (n "smlr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "eddie") (r "^0.4") (d #t) (k 0)))) (h "1200fsaspk971lgp079m8pyfrl1hd8awldhan6mgc2nbnzl4da8h")))

(define-public crate-smlr-0.1.1 (c (n "smlr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "eddie") (r "^0.4") (d #t) (k 0)))) (h "0ax30fhcb5lnm2hhqdhxfy83rmmdqsg31rhmgmmsb6mxkm9wzagg")))

(define-public crate-smlr-0.1.2 (c (n "smlr") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "eddie") (r "^0.4") (d #t) (k 0)))) (h "0mr2vly53ldzjiprgvyr4g0p84368zz2dp38j8krainbln7k4w9d")))

(define-public crate-smlr-0.1.3 (c (n "smlr") (v "0.1.3") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "eddie") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zb5xkzw8y69ld3j6skqaq9p72i38lhmhjj42xc0dwhq8lvv7x90")))

