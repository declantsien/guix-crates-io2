(define-module (crates-io sm bc smbclient-sys) #:use-module (crates-io))

(define-public crate-smbclient-sys-0.1.0 (c (n "smbclient-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.4.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1bs198x8s7kd8r6i6mzsl50aiffb4kh7b6gm4h81civc0qrsbfav")))

