(define-module (crates-io sm bc smbc) #:use-module (crates-io))

(define-public crate-smbc-0.1.0 (c (n "smbc") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "smbclient-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0s0lbn0dk34fp0z4l2gzrdxx9m7mfc3pqncs1kpzqmvnaz73nns4")))

