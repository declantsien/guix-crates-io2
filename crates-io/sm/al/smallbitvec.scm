(define-module (crates-io sm al smallbitvec) #:use-module (crates-io))

(define-public crate-smallbitvec-1.0.0 (c (n "smallbitvec") (v "1.0.0") (h "1yaqf9nhbmizp65afyb1a7nmfhn2gjhy458dl0r2zv0mx50kh4in")))

(define-public crate-smallbitvec-1.0.1 (c (n "smallbitvec") (v "1.0.1") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0mzr7gbd57413lq4i6gmc409algdragalplbrb7gdd6s2b22ymfi")))

(define-public crate-smallbitvec-1.0.2 (c (n "smallbitvec") (v "1.0.2") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0axpjhdh585hb0b2xk7h1khdy7c08di7b6cqw5r9vybjjj2j02q8")))

(define-public crate-smallbitvec-1.0.3 (c (n "smallbitvec") (v "1.0.3") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0gihil1qgrz00w0mma4yfk127apqpmmrhj3jzcpc51623mlg9sy1")))

(define-public crate-smallbitvec-1.0.4 (c (n "smallbitvec") (v "1.0.4") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1ysxmp30368h7nwf7dnh7h0la12xypary1jbha9qd773a9gn7qi9")))

(define-public crate-smallbitvec-1.0.5 (c (n "smallbitvec") (v "1.0.5") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0365lhqxnpmkv7w2j5i6nwd8xb825prhr7w1868nndc80aw131nd")))

(define-public crate-smallbitvec-1.0.6 (c (n "smallbitvec") (v "1.0.6") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1kxxqh2j4qki84rv39y47ygfj8lmcnhymcm3by8dy0gy1pq7ddvr")))

(define-public crate-smallbitvec-1.0.7 (c (n "smallbitvec") (v "1.0.7") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1pphx7zirn3fd98yhdgc1xgs6kb32s1sdv9a2vwpfn39gnvnqb7r")))

(define-public crate-smallbitvec-1.1.0 (c (n "smallbitvec") (v "1.1.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1q3ad15llnzbyk7jyg9x690ska32w88hqcwfzj2lyidldzajicwg")))

(define-public crate-smallbitvec-2.0.0 (c (n "smallbitvec") (v "2.0.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "19msird2hngy1bxx70f940hdh7nqrcy62gibsbz5vwga4gaksh08")))

(define-public crate-smallbitvec-2.1.0 (c (n "smallbitvec") (v "2.1.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0rm93iw6mbf658b0z8dp0ffizzzgn6ja5nj8qmaynqgrhj1vqpv6")))

(define-public crate-smallbitvec-2.1.1 (c (n "smallbitvec") (v "2.1.1") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0g51nfs3s9k5ak32mdqnky7z4x9575zy8wvq8s49y1ph55h74qsw")))

(define-public crate-smallbitvec-2.2.0 (c (n "smallbitvec") (v "2.2.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1h1li7zh4ic65yrh8yhxrji17hgcvjsa88is34r7xavmq34a1qak")))

(define-public crate-smallbitvec-2.3.0 (c (n "smallbitvec") (v "2.3.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0bjnfg3dbm0mjw3y04mv90qhnncdkmjb4dwv7gz3ny7f60mzwr0p")))

(define-public crate-smallbitvec-2.4.0 (c (n "smallbitvec") (v "2.4.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0azhgcvlvnycbfjc9dy614cja2kay56w65jzjksdnzbigw3njymi")))

(define-public crate-smallbitvec-2.5.0 (c (n "smallbitvec") (v "2.5.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1yh9l8xmjm6zlyb4jmyp6qcsfh7rz5v5dm4qjvr9dn4hzfplwykr")))

(define-public crate-smallbitvec-2.5.1 (c (n "smallbitvec") (v "2.5.1") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0plrbldsjpwip3afbzd8fgrnvdhizcg5z4ncfqs4q6x4qjflzkkm")))

(define-public crate-smallbitvec-2.5.2 (c (n "smallbitvec") (v "2.5.2") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0866v5f0xbjzaz4z2rwj7qlwm4b6ncch4afgmvv11fpn4m8msn49")))

(define-public crate-smallbitvec-2.5.3 (c (n "smallbitvec") (v "2.5.3") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0w54ms7ixxmmhs0y32z7pn8jsq2pzvx8wql9hlggslsb99bgrhzw") (r "1.56")))

