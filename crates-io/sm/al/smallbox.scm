(define-module (crates-io sm al smallbox) #:use-module (crates-io))

(define-public crate-smallbox-0.2.1 (c (n "smallbox") (v "0.2.1") (h "1zyszrmmpkl6amcgimhdj381k0d63pzc9fijq65z7ff70hv5vnd1")))

(define-public crate-smallbox-0.2.2 (c (n "smallbox") (v "0.2.2") (h "1f15xg1970y3574akspdzq74pfw355zpx1l5c1yyyj02dgg1drn8") (f (quote (("std" "heap") ("heap") ("default" "std"))))))

(define-public crate-smallbox-0.3.1 (c (n "smallbox") (v "0.3.1") (h "15k33qpa8hzb2khy4wmy7mbiqv3a410m8p36hdqbzgqgw3xpd26y") (f (quote (("std" "heap") ("heap") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.4.1 (c (n "smallbox") (v "0.4.1") (h "06g45p870j40mnv3i5ngpv69gh6z2blrdwa36ahfmhcwx3lijqlj") (f (quote (("std" "heap") ("heap") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.4.2 (c (n "smallbox") (v "0.4.2") (d (list (d (n "nodrop-union") (r "^0.1.9") (d #t) (k 0)))) (h "1fpi5ylrc5wxq8wrdjnamqh99bl86h5rk0fbvp3q0fgbc47yzxqi") (f (quote (("std" "heap") ("heap") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.4.3 (c (n "smallbox") (v "0.4.3") (d (list (d (n "nodrop-union") (r "^0.1.9") (d #t) (k 0)))) (h "0hfkcpsky5kcgzfx955bprjw98qjbcbigxp9gkbcralwazscvl8v") (f (quote (("std" "heap") ("heap") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.4.4 (c (n "smallbox") (v "0.4.4") (d (list (d (n "nodrop-union") (r "^0.1.9") (d #t) (k 0)))) (h "0zsy54fmz5z96sz4d318r40arzmm7gb703fz98y26fjikgqijqkr") (f (quote (("std" "heap") ("heap") ("default" "std"))))))

(define-public crate-smallbox-0.5.0 (c (n "smallbox") (v "0.5.0") (h "0xciw1vjiycrna3nfkj4cjs6369cdzz3xbm4svv0swdxididr7gp") (f (quote (("unsize") ("std" "heap") ("heap") ("default" "std"))))))

(define-public crate-smallbox-0.5.1 (c (n "smallbox") (v "0.5.1") (h "1jqxadd963rhhd9n9a8bv20q7wsba9crkglfz6v69n5cmqiw71r5") (f (quote (("unsize") ("std" "heap") ("heap") ("default" "std"))))))

(define-public crate-smallbox-0.6.0 (c (n "smallbox") (v "0.6.0") (h "17268rxqrk2jcrs8bk5xmrcx4659ky1rdfq8082qp6ljpss53bzf") (f (quote (("unsize") ("std") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.6.1 (c (n "smallbox") (v "0.6.1") (h "1d4qk77xvh92gjl1qw45wa0bqjvgycvi9ygsm6xs25aw243qi4ch") (f (quote (("unsize") ("std") ("default" "std")))) (y #t)))

(define-public crate-smallbox-0.7.0 (c (n "smallbox") (v "0.7.0") (h "0v7k1lrn5dj2x6ay68hgqq01bs78q03cy16p32vzhplgn3h53r8y") (f (quote (("std") ("default" "std") ("coerce")))) (y #t)))

(define-public crate-smallbox-0.7.1 (c (n "smallbox") (v "0.7.1") (h "1c7s3n2vzim9vm19a5pzjjr4gjf62w53i0pxs14jmi4sjg1sq3r9") (f (quote (("std") ("default" "std") ("coerce")))) (y #t)))

(define-public crate-smallbox-0.7.2 (c (n "smallbox") (v "0.7.2") (h "0akicza7s07bkfad8d703c3h2dja9hlh734k4pyhlrkxjazxiwpm") (f (quote (("std") ("default" "std") ("coerce"))))))

(define-public crate-smallbox-0.7.3 (c (n "smallbox") (v "0.7.3") (h "145kjmac7zqdcl8mvsajh33d48v2zcx89qiaqj5aihidw0hzbg03") (f (quote (("std") ("default" "std") ("coerce"))))))

(define-public crate-smallbox-0.7.4 (c (n "smallbox") (v "0.7.4") (h "112c5cbma09r5wb0w7ai0scwlbszba655q04gj1s5lp3l1jyqjcv") (f (quote (("std") ("default" "std") ("coerce")))) (y #t)))

(define-public crate-smallbox-0.7.5 (c (n "smallbox") (v "0.7.5") (h "1wa8ps8gxd9rr2kch2j9356z3svj9n0s1p6r487mw0m3ziyi31ky") (f (quote (("std") ("default" "std") ("coerce"))))))

(define-public crate-smallbox-0.8.0 (c (n "smallbox") (v "0.8.0") (h "1gmfgnm0rplpkd7w3gm1bwpwdv1fd90srbrm398mb6i14mjfq3ma") (f (quote (("std") ("default" "std") ("coerce"))))))

(define-public crate-smallbox-0.8.1 (c (n "smallbox") (v "0.8.1") (h "1algcircp1vxb1bp3pn5hq4fk2fyd6bw17v1b00h51cbybpdcya6") (f (quote (("std") ("default" "std") ("coerce"))))))

(define-public crate-smallbox-0.8.2 (c (n "smallbox") (v "0.8.2") (h "1bq6i2xnbxkzrdr7szymzc2b2k849bq0r5wa6aj7shbbgvwmj8yr") (f (quote (("std") ("default" "std") ("coerce"))))))

