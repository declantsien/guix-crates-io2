(define-module (crates-io sm al smallstring) #:use-module (crates-io))

(define-public crate-smallstring-0.1.0 (c (n "smallstring") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "06q7w4jgwiaya8krzb0bfd27wlj7l8sfnjb8p8nxbsj7d42jikl7")))

(define-public crate-smallstring-0.1.1 (c (n "smallstring") (v "0.1.1") (d (list (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "092ayynpvzzxcsiwckm3aria24x5xipsl29ly24a41afkl57avlw")))

(define-public crate-smallstring-0.1.2 (c (n "smallstring") (v "0.1.2") (d (list (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "0zq8xgxcfg908x8k59kylx7vcfd5cknmdbl1w6h5d3xknnyhm59h") (f (quote (("default") ("as-mut"))))))

