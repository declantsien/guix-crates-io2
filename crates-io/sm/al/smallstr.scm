(define-module (crates-io sm al smallstr) #:use-module (crates-io))

(define-public crate-smallstr-0.1.0 (c (n "smallstr") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (k 0)))) (h "13kfxkf0v00iy06ivyl38xhsm27p0acf4r5g6q6wkfxjsns5p9ka") (f (quote (("std" "smallvec/std") ("default" "std"))))))

(define-public crate-smallstr-0.2.0 (c (n "smallstr") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0kxfwlpjg6l33glnckl1cnf2250sfa173q67zwlqfrv8s6a2g4hy") (f (quote (("union" "smallvec/union") ("ffi"))))))

(define-public crate-smallstr-0.3.0 (c (n "smallstr") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "17fzs6nnp4hj5b002v11f0gsypdhm8qmzcfhvvw5yww0ygysxcb3") (f (quote (("union" "smallvec/union") ("std" "serde/std") ("ffi"))))))

