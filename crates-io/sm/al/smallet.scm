(define-module (crates-io sm al smallet) #:use-module (crates-io))

(define-public crate-smallet-0.11.1 (c (n "smallet") (v "0.11.1") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0dfby5ig27mg93rp6rab0wlnqq760slgyxv6m4phmpmh1wv6p4cc") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

