(define-module (crates-io sm al small-card-deck) #:use-module (crates-io))

(define-public crate-small-card-deck-0.1.0 (c (n "small-card-deck") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14998pzci1xpg3flry661z5nqgqmszfxdiagw9wn0blkn0wyifbd")))

(define-public crate-small-card-deck-0.1.1 (c (n "small-card-deck") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g4c8hw6dc1mhx5r536hdi0jn8jqwl4kybz1p19b18zdypccf29c")))

