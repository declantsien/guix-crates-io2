(define-module (crates-io sm al small_ctor) #:use-module (crates-io))

(define-public crate-small_ctor-0.1.0 (c (n "small_ctor") (v "0.1.0") (h "07ijb453qmnrxbw2k7snfh62ag26q8mjgvvl9a6j8nzrfcdd5isr")))

(define-public crate-small_ctor-0.1.1 (c (n "small_ctor") (v "0.1.1") (h "0xbmz40hz57qxrwblbjv8qi6dj6n8vrx9mgfmxwz6kfq4r096fsk")))

