(define-module (crates-io sm al small-bwt) #:use-module (crates-io))

(define-public crate-small-bwt-0.1.0 (c (n "small-bwt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1gi0vszwb17726f3ghsc4kryqqz4jhmxcj7yxqjwxclmdcvnzaq3")))

(define-public crate-small-bwt-0.2.0 (c (n "small-bwt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "1lw320wz22cc3ar99f6k5k0y5ga1lzm83az8v51dgwljaawivvzi") (r "1.60.0")))

