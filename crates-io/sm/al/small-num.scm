(define-module (crates-io sm al small-num) #:use-module (crates-io))

(define-public crate-small-num-0.1.0 (c (n "small-num") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "0dsljxh763i6qyh15n71kl1g7xfz02n5n9npm053sk3n3fpjii4l")))

(define-public crate-small-num-0.1.1 (c (n "small-num") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1qcl3c8ilplxrasavqfy281malfw139hq7q61qrlxbsmcjal9ms2")))

(define-public crate-small-num-0.2.0 (c (n "small-num") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)))) (h "1jpnvmrmra3mkmjv1ckkbvng7sjm8akw1f5kpv1jriiqld4bzgai") (s 2) (e (quote (("serde" "dep:serde"))))))

