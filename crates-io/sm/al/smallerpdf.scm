(define-module (crates-io sm al smallerpdf) #:use-module (crates-io))

(define-public crate-smallerpdf-0.1.0 (c (n "smallerpdf") (v "0.1.0") (h "0lwmxdlk3saf0dlm6r8qszl30pj64i8xiy244j0nzxzfm9hy1z62")))

(define-public crate-smallerpdf-0.1.1 (c (n "smallerpdf") (v "0.1.1") (h "0j00gc9wp6kml519bban0fyqgmgi2plrx4dhhd45r5fiwb1vrpy8")))

(define-public crate-smallerpdf-0.1.2 (c (n "smallerpdf") (v "0.1.2") (h "0k86bjapr87g7svlzlz2zgdqmllyn6m5qbq45d3yc0s18rl43d2c")))

(define-public crate-smallerpdf-0.1.3 (c (n "smallerpdf") (v "0.1.3") (h "009xgknbwm5qn4wrmmpvw6vakrs9zkrh3xbsda0ascsll5y3y364")))

