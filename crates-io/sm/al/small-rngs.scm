(define-module (crates-io sm al small-rngs) #:use-module (crates-io))

(define-public crate-small-rngs-0.0.1 (c (n "small-rngs") (v "0.0.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.1") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "15xvswwsgnxj3q9swi2qc80z72r5bq8rfvn00wz29qfrvwgmc33x")))

