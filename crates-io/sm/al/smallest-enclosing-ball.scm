(define-module (crates-io sm al smallest-enclosing-ball) #:use-module (crates-io))

(define-public crate-smallest-enclosing-ball-0.1.0 (c (n "smallest-enclosing-ball") (v "0.1.0") (d (list (d (n "ball") (r "^0.1.1") (k 0)) (d (n "circle") (r "^0.1.3") (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "087dhzwslyvkx2hr4gbin0gc3xhsmijjsawan7gylfcfr84bjddb") (f (quote (("f64" "circle/f64" "ball/f64") ("f32" "circle/f32" "ball/f32") ("default" "f32"))))))

