(define-module (crates-io sm al smallvectune) #:use-module (crates-io))

(define-public crate-smallvectune-0.0.1 (c (n "smallvectune") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6.5") (d #t) (k 0)))) (h "0spqi0f1gm77v7i80qs2pwp7i4kpfx35zwxg2gk1ikinh2hhpb83") (f (quote (("union" "smallvec/union") ("std" "smallvec/std") ("default" "std"))))))

