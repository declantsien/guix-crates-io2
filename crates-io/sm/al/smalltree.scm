(define-module (crates-io sm al smalltree) #:use-module (crates-io))

(define-public crate-smalltree-0.0.0 (c (n "smalltree") (v "0.0.0") (d (list (d (n "smallgraph") (r "^0.0") (d #t) (k 0)))) (h "08rp6p70czhgmmjy9dwipz7cjfzriqhiq6iaz7zkh2hbg9va50kk")))

(define-public crate-smalltree-0.0.1 (c (n "smalltree") (v "0.0.1") (d (list (d (n "smallgraph") (r "^0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1j55bcmc4cyawm35vbddl4gz7qbiqbnamyf1hmpzya0x6qj5wl5j")))

