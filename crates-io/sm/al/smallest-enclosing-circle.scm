(define-module (crates-io sm al smallest-enclosing-circle) #:use-module (crates-io))

(define-public crate-smallest-enclosing-circle-0.1.0 (c (n "smallest-enclosing-circle") (v "0.1.0") (d (list (d (n "geometry-predicates") (r "^0.3.0") (d #t) (k 0)))) (h "13x3pwcpx102vcf4370ymiz3szllxbqpsmxmara2x4vznxy4j4yr")))

(define-public crate-smallest-enclosing-circle-0.2.0 (c (n "smallest-enclosing-circle") (v "0.2.0") (d (list (d (n "geometry-predicates") (r "^0.3.0") (d #t) (k 0)))) (h "1qfhp2sl7pc4w2j44nngwyd7vyjyrjbn78lyg2wn8dqgjjn3y4y3")))

