(define-module (crates-io sm al small-logger) #:use-module (crates-io))

(define-public crate-small-logger-0.0.2 (c (n "small-logger") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0wfb458592l97q79ark5m7i9gk4rsgmk0c44kx8cxabw9hv0g84l")))

(define-public crate-small-logger-0.1.0 (c (n "small-logger") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "iron") (r "^0.2.6") (d #t) (k 0)) (d (n "mount") (r "^0.0.10") (d #t) (k 0)) (d (n "router") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.17") (d #t) (k 0)) (d (n "staticfile") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1401liydjw2kk3s464bj7jgxi7dck0c9mlbcxq7ndxwzimyvm68h")))

(define-public crate-small-logger-0.2.0 (c (n "small-logger") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.17") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "07b9rfnp6l5m3hakgnkg2qjk3nwmh5jrvkm8p5kl4ws36nqyfbjh")))

(define-public crate-small-logger-0.2.1 (c (n "small-logger") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.17") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1q4lvbi34mw16bgi5wf056kliwvjnp5yy3s6dfgfbjfxzbbv5mfi")))

