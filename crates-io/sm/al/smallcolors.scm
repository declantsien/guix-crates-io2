(define-module (crates-io sm al smallcolors) #:use-module (crates-io))

(define-public crate-smallcolors-0.1.0 (c (n "smallcolors") (v "0.1.0") (h "1mdfawls2d9i7378phpx24rnmmybykw47iv1njld9cyrkpk0x3z0")))

(define-public crate-smallcolors-0.1.1 (c (n "smallcolors") (v "0.1.1") (h "0y8pnl22qnf1i6n5ws1ghqd7047hxkk546w5ng6dm0msr35asay3")))

(define-public crate-smallcolors-0.1.2 (c (n "smallcolors") (v "0.1.2") (h "0d9g6c989dcml188s5gjywpmsml2pc7277j8cshvvzbik61s43nn")))

