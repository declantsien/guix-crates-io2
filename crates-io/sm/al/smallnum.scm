(define-module (crates-io sm al smallnum) #:use-module (crates-io))

(define-public crate-smallnum-0.1.0 (c (n "smallnum") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1ds0yzp4fazy6v033kvrx444fyhrmi6g0gr4vyxmf9xsddjrvj4m")))

(define-public crate-smallnum-0.2.0 (c (n "smallnum") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "13zf4vlyxxdcan17604krl2zy4brgk0k1hhdb2f7cbaydf417viq")))

(define-public crate-smallnum-0.2.1 (c (n "smallnum") (v "0.2.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0khympg0gxv43b0zgbynqbv3dbacwpjicrnngab1a9q9b4lzl1dj")))

(define-public crate-smallnum-0.2.2 (c (n "smallnum") (v "0.2.2") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0a56rzpzw7wmyywf5jbzwqlsr6cbc820sp37ms7a272q6jid3q6r")))

(define-public crate-smallnum-0.3.0 (c (n "smallnum") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1fdcz2cna2n2jrhyi6z30igwvw3lczmh9hj3zf69brfpjsf9amm0")))

(define-public crate-smallnum-0.4.0 (c (n "smallnum") (v "0.4.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1kwxclaxnw6lm5ri10m7imgwaqrcgdawzbq3p3y3yhrz7bbc0b5f")))

(define-public crate-smallnum-0.4.1 (c (n "smallnum") (v "0.4.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1jggipipm2irqaxvbvp31qa7jb4yzih3ljsib4m271xhjvp9rd5d")))

