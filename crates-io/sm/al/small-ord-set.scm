(define-module (crates-io sm al small-ord-set) #:use-module (crates-io))

(define-public crate-small-ord-set-0.1.0 (c (n "small-ord-set") (v "0.1.0") (d (list (d (n "smallvec") (r "~1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0vb8x920agw5vwp6wvq6affvm97v49r7z4i5i4gdyv04pafghwzh")))

(define-public crate-small-ord-set-0.1.1 (c (n "small-ord-set") (v "0.1.1") (d (list (d (n "smallvec") (r "~1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "07hwywajq2yl11w6f3imji6bcd2595816k1avi9izaij65by6ii7")))

(define-public crate-small-ord-set-0.1.2 (c (n "small-ord-set") (v "0.1.2") (d (list (d (n "smallvec") (r "~1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0fdngsy16wn7q71za2jz38vdxx2ypjgcbsigb6mgw7nb2r3cql44")))

(define-public crate-small-ord-set-0.1.3 (c (n "small-ord-set") (v "0.1.3") (d (list (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0zhfg896ph7grlhj1qcpc21xmgh6bdb3hmrrq7l5p2i6nai3aw6g") (f (quote (("union" "smallvec/union"))))))

