(define-module (crates-io sm al small_str) #:use-module (crates-io))

(define-public crate-small_str-0.1.0 (c (n "small_str") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 2)) (d (n "smallvec") (r "^2.0.0-alpha.5") (d #t) (k 0)))) (h "0i24r9cgbcsh46b9yspc38hr2l6k7fnbvsiybq7pxarkq6chaqxp") (f (quote (("unstable") ("std" "serde/std" "serde_json/std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

