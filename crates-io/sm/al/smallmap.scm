(define-module (crates-io sm al smallmap) #:use-module (crates-io))

(define-public crate-smallmap-1.0.0 (c (n "smallmap") (v "1.0.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0wgarhk4lgi77nbf6s5vlbjyyhbw8gwgs37yi87znbmm3c3pqg3y")))

(define-public crate-smallmap-1.0.1 (c (n "smallmap") (v "1.0.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0v4sqkz7srnfd7ym1c577alc4wkxxc4gkhkjgyiybz7nfahcwd5v")))

(define-public crate-smallmap-1.0.2 (c (n "smallmap") (v "1.0.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1p1gg9kr8xllj9qfq12v867wfklki7vn1bk86sll133pdb1nz6zy")))

(define-public crate-smallmap-1.1.2 (c (n "smallmap") (v "1.1.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "08r89layj9qqadgq5bb0b84h0w6q0bk74w3h2mi24vv36jxg5h1m")))

(define-public crate-smallmap-1.1.3 (c (n "smallmap") (v "1.1.3") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "03r3m1728jfx51jmrc0j5b9rs3n9p2zmwhhiyy2gzhxvb4ragp9a")))

(define-public crate-smallmap-1.1.4 (c (n "smallmap") (v "1.1.4") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kgx95jc6756njvfvvkjl2jpq449pgldrjpm7ly34hgd8ac71iff")))

(define-public crate-smallmap-1.1.5 (c (n "smallmap") (v "1.1.5") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kai4xnzji2hyn543qs0ryg88f141hn981ni72sg63gvi2wpiklp")))

(define-public crate-smallmap-1.1.6 (c (n "smallmap") (v "1.1.6") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0wjpmxdrm8hj5q1vyw7ks9i5vgi6jfxlxb0wbw5hnyxb0qp4v961")))

(define-public crate-smallmap-1.2.0 (c (n "smallmap") (v "1.2.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1qhhj4dyvg4flgxxyqwlnfn4hlkv7dxxd9s5s1jl2k2mmgv0jnvk")))

(define-public crate-smallmap-1.2.1 (c (n "smallmap") (v "1.2.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0lzg0m79anv90s7kvrzvqgscxdhlqiji1pqjcmhhi1m7vzy6v5xj")))

(define-public crate-smallmap-1.3.0 (c (n "smallmap") (v "1.3.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0pc7ggy3nn569jax4hwhvm2alvw2yxhcld32lwlggdw6b4yxfq8x")))

(define-public crate-smallmap-1.3.1 (c (n "smallmap") (v "1.3.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0lwvxagfiyv0yzghdyz7p5cq0rw6455zk066m9fzkv71p4inb17l")))

(define-public crate-smallmap-1.3.2 (c (n "smallmap") (v "1.3.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1q436zqbmasbxsnggiis27avg01jz0a4nz9hkydx7pvh88bq63w2")))

(define-public crate-smallmap-1.3.3 (c (n "smallmap") (v "1.3.3") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0q4r4k1wnmpr6ngn5lwkh3g8xxwi5krbi8f967v1x12z0g7wyqgq")))

(define-public crate-smallmap-1.3.4 (c (n "smallmap") (v "1.3.4") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "01hhj662dj6r4ccjzg4dwapiz95wjpgcvgzjq53bajpjjdfq0r2a")))

(define-public crate-smallmap-1.4.0 (c (n "smallmap") (v "1.4.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0vi9ssji1mgah17isvbd4bar7fk0d36339bbpq06x7mgnn7axw1d")))

(define-public crate-smallmap-1.4.1 (c (n "smallmap") (v "1.4.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.116") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1fx4srf624spkda8bj1bgdkxpi3ws37s75h2hivvj84qdv1hm7k3") (f (quote (("std") ("default" "std"))))))

(define-public crate-smallmap-1.4.2 (c (n "smallmap") (v "1.4.2") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "16f18k9czhpnzrbdz8sq94icmwf70zisqlbyl3zbps8xamfj1l6n") (f (quote (("std") ("default" "std"))))))

