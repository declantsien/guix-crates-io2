(define-module (crates-io sm al smallest-uint) #:use-module (crates-io))

(define-public crate-smallest-uint-0.1.0 (c (n "smallest-uint") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0a0caw7x9dr68b9c0zma3ickalh4yvkhpdbnlcf1lsr2av6hin5v") (f (quote (("u128") ("default" "u128"))))))

(define-public crate-smallest-uint-0.1.1 (c (n "smallest-uint") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0i6jbavqrzm33wkxshsvsslbwd2y1ynj175c20ga2g7awbai1lmc") (f (quote (("u128") ("default" "u128"))))))

(define-public crate-smallest-uint-0.1.2 (c (n "smallest-uint") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "1xrq8b7hi1mqjf1zcsldlavrkzaq6r54gdjmyfkjibm3pwm0dc2s") (f (quote (("u128") ("default" "u128"))))))

(define-public crate-smallest-uint-0.1.3 (c (n "smallest-uint") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0y6i3nbdswz38izkvrnnnd5lm5b28ixqgnkqbjvfmdawd4adbrsm") (f (quote (("u128") ("default" "u128"))))))

(define-public crate-smallest-uint-0.1.4 (c (n "smallest-uint") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "1mccni02bjzj5zn0d9krwfz0h8pls15b61imb8z0lj97v191fxyp") (f (quote (("u128") ("default" "u128"))))))

(define-public crate-smallest-uint-0.1.5 (c (n "smallest-uint") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0qbg75nd8500ad870178z12r27smidj2rkwa3k5hz2vsjw83k137") (f (quote (("u128") ("default" "u128")))) (r "1.37")))

