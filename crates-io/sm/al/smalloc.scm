(define-module (crates-io sm al smalloc) #:use-module (crates-io))

(define-public crate-smalloc-0.1.0 (c (n "smalloc") (v "0.1.0") (h "1c0inxiicp1qshc908n1asb6my61g09qnzl4ziiiykfjxryv343a") (f (quote (("with_static") ("default"))))))

(define-public crate-smalloc-0.1.1 (c (n "smalloc") (v "0.1.1") (d (list (d (n "solana-program") (r "^1.14.5") (d #t) (k 0)))) (h "14svqmnxb7mnny3n69l0njzwghj85nsnq2r4mcar6qsz45nwxilf") (f (quote (("dynamic_start") ("default")))) (y #t)))

(define-public crate-smalloc-0.1.2 (c (n "smalloc") (v "0.1.2") (h "164280kiins2p8kwgixaqkgljccngqwhmi1n5v0yzsk52xkwhkgy") (f (quote (("dynamic_start") ("default"))))))

