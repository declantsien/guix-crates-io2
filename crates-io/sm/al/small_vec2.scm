(define-module (crates-io sm al small_vec2) #:use-module (crates-io))

(define-public crate-small_vec2-0.1.0 (c (n "small_vec2") (v "0.1.0") (h "1qggv9jprcfl24a1q4mdw5dmsvmn2r7dryjsp0wdpxhh8z8a45jd") (y #t)))

(define-public crate-small_vec2-0.1.1 (c (n "small_vec2") (v "0.1.1") (h "1z87wz0i4w8f9m0vj8bq300b9hpsdwp2had1izlkxs7ycpfwsfzq") (y #t)))

(define-public crate-small_vec2-0.1.3 (c (n "small_vec2") (v "0.1.3") (h "1qldm40g8cr77g0v1czf1d93lvr4ig39rp57lm4xb2xnllwzb69p") (y #t)))

(define-public crate-small_vec2-0.1.4 (c (n "small_vec2") (v "0.1.4") (h "0kqjalvdj652985pzigx5jmcbmcdkfaw643165ypl080cs80m9jc")))

(define-public crate-small_vec2-0.1.5 (c (n "small_vec2") (v "0.1.5") (h "0yi047sdx14s1fy83ql22q93hhnhdacr8ychxcbrppw8b20kp7mw")))

