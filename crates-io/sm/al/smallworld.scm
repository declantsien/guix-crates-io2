(define-module (crates-io sm al smallworld) #:use-module (crates-io))

(define-public crate-smallworld-0.1.0 (c (n "smallworld") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libsmallworld") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "10fsng3rsis489cyg4i0n25wsnkdx705gnfscn2flvd547cyb9nl")))

