(define-module (crates-io sm al smallgraph) #:use-module (crates-io))

(define-public crate-smallgraph-0.0.0 (c (n "smallgraph") (v "0.0.0") (d (list (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "06qxqjygnkmzi7fx1y7d0xj6h6swf5vcv0zrgk8q394ap7dccxyn")))

(define-public crate-smallgraph-0.0.1 (c (n "smallgraph") (v "0.0.1") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "082izljfscm0wxp5s7f2mgib2fycl0zas74bgw383665k1fvpnih")))

(define-public crate-smallgraph-0.0.3 (c (n "smallgraph") (v "0.0.3") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "02pwmvl49xwygy2vg5zr2y0zp6m58g5qwyn1nvzfcvcm51i1a78w")))

(define-public crate-smallgraph-0.0.4 (c (n "smallgraph") (v "0.0.4") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0693ik84c6iiziy3bnh4vn6z69h8dqdsyzxbrwzlmmqr2xphqq9b")))

(define-public crate-smallgraph-0.0.5 (c (n "smallgraph") (v "0.0.5") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "02na03ppgnxpv62ds7krwxa89hcwy8sryasbk6ri3xd5s9am16y5")))

(define-public crate-smallgraph-0.0.6 (c (n "smallgraph") (v "0.0.6") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "15i9vy1qyhzmldykrpnrfzvx51xkmhkr1ljw51vf5n55xbv0z8s2")))

(define-public crate-smallgraph-0.0.7 (c (n "smallgraph") (v "0.0.7") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0604zphs94k6r1x2z622xfrm9gmi618z0119qrmsihzdf0c09zkj")))

