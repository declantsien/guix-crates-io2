(define-module (crates-io sm al smallbitset) #:use-module (crates-io))

(define-public crate-smallbitset-0.1.0 (c (n "smallbitset") (v "0.1.0") (h "102xhlh6i2sf1b42gbfh30lkwzx8iiq77mw5rbz98464pxn21753")))

(define-public crate-smallbitset-0.2.0 (c (n "smallbitset") (v "0.2.0") (h "0km084lv9vl52h90d9844kai42kk99jr09im0id2hzfkkm7k2flx")))

(define-public crate-smallbitset-0.3.0 (c (n "smallbitset") (v "0.3.0") (h "01h467p742y00k2dn2n00m8lwdannk359lxmy5axzzi7r90yfk3x")))

(define-public crate-smallbitset-0.4.0 (c (n "smallbitset") (v "0.4.0") (h "1xkfkap2nc7gzvh0ac1qbpwriwqy66r1f1sxwppimb617mhx10qg")))

(define-public crate-smallbitset-0.4.1 (c (n "smallbitset") (v "0.4.1") (h "008w4yk8bzw74qc8l6hzys24hg5992qz0k1v0svaq8f4n444z7vv")))

(define-public crate-smallbitset-0.5.0 (c (n "smallbitset") (v "0.5.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1hcknlj8m1mlws0ac9njjnx0y51ll1wcxsxb351x1zk05xa53w5y") (y #t)))

(define-public crate-smallbitset-0.5.1 (c (n "smallbitset") (v "0.5.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0xlnw938akb2g095i9n5if45vn8msmcavcvzxbx74im2qhlg4pwc")))

(define-public crate-smallbitset-0.6.0 (c (n "smallbitset") (v "0.6.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0mhwgs06pwh5s172yh1hcdmc6hakj9y9a9wy3pz20hyh0d211708")))

(define-public crate-smallbitset-0.6.1 (c (n "smallbitset") (v "0.6.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0z4syh6v4m2g91wxm9kdn6wl9x0mh1sdj73v95yj9ybba3fbcgyr")))

(define-public crate-smallbitset-0.7.0 (c (n "smallbitset") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1rgcclm7gkyzpjg254v9xhjds52xl38k2phkxf3bbpdp374q84l5") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-smallbitset-0.7.1 (c (n "smallbitset") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "16q4vqflpgp8gnd1qiqc5bdqsxfl6pcgcwkdq6b6nw3h55pc3dp0") (f (quote (("default" "alloc") ("alloc"))))))

