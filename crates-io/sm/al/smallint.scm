(define-module (crates-io sm al smallint) #:use-module (crates-io))

(define-public crate-smallint-0.1.0 (c (n "smallint") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "0zv3hnaj2qvvfhy20n2dzrbzdm3k99841l1x4b2xlc91indqbjrd")))

(define-public crate-smallint-0.2.0 (c (n "smallint") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "1rzsbnp4mslg4i1faqw630syyvs48dwi3a6jf9391pl8207iwa7r")))

(define-public crate-smallint-0.2.1 (c (n "smallint") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "05nqk98s97spdannh1wzgqqi9fsvrpiy2i02lnlbq6h4dwh25hdn")))

(define-public crate-smallint-0.2.2 (c (n "smallint") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)))) (h "0yz34mqq0hqmlq3qynkaldq5hsy3ca0avin6763r4z7wrdb53kq4")))

