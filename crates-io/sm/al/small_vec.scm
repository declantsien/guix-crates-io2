(define-module (crates-io sm al small_vec) #:use-module (crates-io))

(define-public crate-small_vec-0.1.0 (c (n "small_vec") (v "0.1.0") (h "1mli2fay82w2kniccxwvqyn069xmc31l7iqkbk2ygl3gc5p2xian")))

(define-public crate-small_vec-0.1.1 (c (n "small_vec") (v "0.1.1") (d (list (d (n "local_vec") (r "~0.1.1") (d #t) (k 0)))) (h "1gzvfp59a0ww3qryhaxz3bvpl3fj1yaxmkbj02sj4zdxakjvwhff")))

(define-public crate-small_vec-0.1.2 (c (n "small_vec") (v "0.1.2") (d (list (d (n "local_vec") (r "~0.2.1") (d #t) (k 0)))) (h "11d7pqslbc3q72zq65km80l3wifd5hk5qbbiiqbx63cp3936sgrp")))

