(define-module (crates-io sm al smallset) #:use-module (crates-io))

(define-public crate-smallset-0.1.0 (c (n "smallset") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "1fi5mfrj8m4c995pghp1qycp9wjxn5vnfs58qb221aggmb5w4p8h")))

(define-public crate-smallset-0.1.1 (c (n "smallset") (v "0.1.1") (d (list (d (n "smallvec") (r "^0.1") (d #t) (k 0)))) (h "16prisnricxjy849sq22lzmhfm77r9vm61pq8dzn640fgiqaha7l")))

