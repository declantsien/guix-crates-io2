(define-module (crates-io sm al smallvec-stableunion) #:use-module (crates-io))

(define-public crate-smallvec-stableunion-0.6.10 (c (n "smallvec-stableunion") (v "0.6.10") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0s6y8c659inmx066clq1c2kvzb9d1wl6rb291bs8hisf1j3is3m3") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

