(define-module (crates-io sm al smallvec) #:use-module (crates-io))

(define-public crate-smallvec-0.1.0 (c (n "smallvec") (v "0.1.0") (h "13x77r9vxcxwpzb6c60mvilqda6lbd4gcb405pgnw9bdpanca6jl")))

(define-public crate-smallvec-0.1.1 (c (n "smallvec") (v "0.1.1") (h "18zb84688qyblwfi4fhp76yidsi7l84s7jcxj4piprljwmnihaxf")))

(define-public crate-smallvec-0.1.2 (c (n "smallvec") (v "0.1.2") (h "1sysrmazilf1zd95nl3168h8n95b3a50sq925db9v8w7g27qy8cc")))

(define-public crate-smallvec-0.1.3 (c (n "smallvec") (v "0.1.3") (h "040sqpspa5fqma4nsjnj4bfssmh53q9z85qywakszwfpm2q2rddf")))

(define-public crate-smallvec-0.1.4 (c (n "smallvec") (v "0.1.4") (h "1c7n0r9jspgdy82pmjkrrifc4hag8p885xa1raxabr53dnzqqqzn")))

(define-public crate-smallvec-0.1.5 (c (n "smallvec") (v "0.1.5") (h "0y7f920higx0p7wj7m5brjrnp15cxbvgcf17xq8avx6bja37bl0w")))

(define-public crate-smallvec-0.1.6 (c (n "smallvec") (v "0.1.6") (h "0ii7j0jx6asnp9891n50v20w988s8hzjbi6amb8hz0ib0nwsc3pp")))

(define-public crate-smallvec-0.1.7 (c (n "smallvec") (v "0.1.7") (h "1zd8iflvdxsyy54awag9d3kp62wnd35b6cwbjygbg7rfm4xwixn6")))

(define-public crate-smallvec-0.1.8 (c (n "smallvec") (v "0.1.8") (h "042lhr2f1lxh0lgz0p4i59rvkbpp4sdig8m7wjawzkma2a9d3j7w")))

(define-public crate-smallvec-0.2.0 (c (n "smallvec") (v "0.2.0") (h "0v81klbkccm8jyxhgp67q1rlxi8yppjhpys752y6rx30zxzcc1wr")))

(define-public crate-smallvec-0.2.1 (c (n "smallvec") (v "0.2.1") (h "04z0bv5pcnwnvij8kfzw56lnib9mjq8bafp120i7q48yvzbbr32c")))

(define-public crate-smallvec-0.3.1 (c (n "smallvec") (v "0.3.1") (h "0hf8qqxqkm6sdvm6vk5kwlxyxqi3if78c6bflihzx2i79jc88g1s")))

(define-public crate-smallvec-0.3.2 (c (n "smallvec") (v "0.3.2") (d (list (d (n "heapsize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0vi3qkali1cj8x0rv3zrvkil1vcbzwyih5hdmkw9w2jh88pkz86w") (f (quote (("heapsizeof" "heapsize")))) (y #t)))

(define-public crate-smallvec-0.3.3 (c (n "smallvec") (v "0.3.3") (d (list (d (n "heapsize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "07n1x51ql0j9dwvzc26784l5cpb2jl9c5xhnbc5pvlf1kd8nd0jg") (f (quote (("heapsizeof" "heapsize")))) (y #t)))

(define-public crate-smallvec-0.4.0 (c (n "smallvec") (v "0.4.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1q5y8kn67l3jzqp8fr6kcmy71l2wia6kis2a57cb967ym88ayh1f") (f (quote (("heapsizeof" "heapsize")))) (y #t)))

(define-public crate-smallvec-0.4.1 (c (n "smallvec") (v "0.4.1") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)))) (h "094pj94imh6cd8lkgy5dmm2gvxaab96y0p9iy0lzl9jjhbxjmai6") (f (quote (("std") ("heapsizeof" "heapsize" "std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.4.2 (c (n "smallvec") (v "0.4.2") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1birlrpvn0245151sjcc1hcyhz5jw5xdbn7bmz5dxjjcysp4z1mm") (f (quote (("std") ("heapsizeof" "heapsize" "std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.4.3 (c (n "smallvec") (v "0.4.3") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0pb94l0bz2jkb75pziqwq7498zyp66bclx2d6fq0l4bqy7x07kcg") (f (quote (("std") ("heapsizeof" "heapsize" "std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.4.4 (c (n "smallvec") (v "0.4.4") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0721c8hwrb6chjiamy140idcpqirznb4x5hv5s1ghyykiiz3akzf") (f (quote (("std") ("heapsizeof" "heapsize" "std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.5.0 (c (n "smallvec") (v "0.5.0") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09f0mvmmq1kgnyx4dxpcn253i3c57f41x9fc432l20004zr0yb47") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.0 (c (n "smallvec") (v "0.6.0") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1fdlsac793096am02ajzzy1l2y0mdlzkmqbss68gf7lj4b5hxns4") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.1 (c (n "smallvec") (v "0.6.1") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "19sahj9f2d8wbpwnrm7il0m0p3b0jh8pb01c7fsailyynn5bknh3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.2 (c (n "smallvec") (v "0.6.2") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0545qnz1mnca0aqrpz9bzl7g7m38x1bij55gpgb76bh923q7saii") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.3 (c (n "smallvec") (v "0.6.3") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1j6d38c8ynglh5i7cz0fhvhb3han3zaj7dwj87kc5sm57jq3ppr6") (f (quote (("union") ("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.3.4 (c (n "smallvec") (v "0.3.4") (d (list (d (n "heapsize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "121d6ab60b2yi67gn82p9yw9ir9qa7g98qrkr0iwx3nc27pawhz1") (f (quote (("heapsizeof" "heapsize"))))))

(define-public crate-smallvec-0.4.5 (c (n "smallvec") (v "0.4.5") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "047g7arsfik4p4k2pib08sfz6d9r68fn3z4lmc3qir1mwmgmw37r") (f (quote (("std") ("heapsizeof" "heapsize" "std") ("default" "std"))))))

(define-public crate-smallvec-0.5.1 (c (n "smallvec") (v "0.5.1") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1vh8zj5gwnf6ii99rb8220wpchwkdl5q1n93jcmb7i4bdx5lhiqk") (f (quote (("std") ("default" "std"))))))

(define-public crate-smallvec-0.6.4 (c (n "smallvec") (v "0.6.4") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0j777h27wfz2zqag83pa2pavbkd12pjll1fj4qwi0jz9cng4h6i1") (f (quote (("union") ("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.5 (c (n "smallvec") (v "0.6.5") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0p9fc3xgiqczripp8r7wq724wxaahbgqx0z0yx29j3hpzlrglgqm") (f (quote (("union") ("std") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.6 (c (n "smallvec") (v "0.6.6") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "11549m8vfbk5cy7vyzaqkrmhvnfp89xv5hqdnf4lv6n2akag4bb2") (f (quote (("union") ("std") ("specialization") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.7 (c (n "smallvec") (v "0.6.7") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1nrn4gxri3kgk65vw83lq2l9j1z0kgk2ajc1xw1khmj7idrs6gmp") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.8 (c (n "smallvec") (v "0.6.8") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "05gd0lb9amvh7dm1iswym8cgnqnndbwzm4slvdp9zcjsjrrs1bl8") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.9 (c (n "smallvec") (v "0.6.9") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1gh2j3546vxvz60zw0sj98sxmyj8ixv5f8lq64vl17f4a3lqlj64") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std")))) (y #t)))

(define-public crate-smallvec-0.6.10 (c (n "smallvec") (v "0.6.10") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1dyl43rgzny79jjpgzi07y0ly2ggx1xwsn64csxj0j91bsf6lq5b") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

(define-public crate-smallvec-0.6.11 (c (n "smallvec") (v "0.6.11") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1wvzlx1kx4b3hivnfl46hi0bvbwyffqhwr3fyf30pwd6fq7abynf") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

(define-public crate-smallvec-0.6.12 (c (n "smallvec") (v "0.6.12") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0hpm90ypyfyi1g23h2z5m21y8wsdnk5gfkxgzf5g4j07bphjjgjk") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

(define-public crate-smallvec-1.0.0 (c (n "smallvec") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "11pwzjbkiw1jsy1q4w8bws3r61sk27dsp9asankvm2lfys2kpksf") (f (quote (("write") ("union") ("specialization") ("may_dangle"))))))

(define-public crate-smallvec-0.6.13 (c (n "smallvec") (v "0.6.13") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "maybe-uninit") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1dl219vnfkmsfx28lm3f83lyw24zap6fdsli6rg8nnp1aa67bc7p") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

(define-public crate-smallvec-1.1.0 (c (n "smallvec") (v "1.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1x78v1z931bxvkp0d7b9fmg4a17yqkiydr765a8if250kw69xra4") (f (quote (("write") ("union") ("specialization") ("may_dangle"))))))

(define-public crate-smallvec-1.2.0 (c (n "smallvec") (v "1.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1z6f47i3qpg9pdjzzvb0g5i1vvdm2ymk3kqc1mdnl8fdkgnb4bsw") (f (quote (("write") ("union") ("specialization") ("may_dangle"))))))

(define-public crate-smallvec-1.3.0 (c (n "smallvec") (v "1.3.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "02jf46c35hzb0bhhd95cqp1jc0nha2pcx6ba9xry86arc4i0wwh5") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.4.0 (c (n "smallvec") (v "1.4.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1r2yxn8qamzb1jn91zbphl0x49scbfxxjr629ql58mv1w5w5djy7") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics")))) (y #t)))

(define-public crate-smallvec-1.4.1 (c (n "smallvec") (v "1.4.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0gqgmbfj8228lc55xxg331flizzwq6hfyy6gw4j2y6hni6fwnmrp") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.4.2 (c (n "smallvec") (v "1.4.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0lp2ax6hfbcsqa92xkdz8vnlfc0fhpqczv62l64kvgsbp2b7dvpv") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.5.0 (c (n "smallvec") (v "1.5.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "11gwjrrkr0bkrijmz2fl8dcl3mrl3n61wg98sdcs5s5r9vrxdjks") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.5.1 (c (n "smallvec") (v "1.5.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0xcxvc2lh2fj02d91v4dx1l4q14m5rb4yac788bhwxvxdl2lylmf") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.6.0 (c (n "smallvec") (v "1.6.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1h0s7dbhf96k1g726h7rp16wm4m83mpnmi7qkfbirr387dgwlm8s") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-0.6.14 (c (n "smallvec") (v "0.6.14") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "maybe-uninit") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1q4hz0ssnv24s6fq5kfp2wzrrprrrjiwc42a0h7s7nwym3mwlzxr") (f (quote (("union") ("std") ("specialization") ("may_dangle") ("default" "std"))))))

(define-public crate-smallvec-1.6.1 (c (n "smallvec") (v "1.6.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0kk08axr0ybfbjzk65a41k84mb6sfhyajmfndaka9igkx34kf3zy") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_generics"))))))

(define-public crate-smallvec-1.7.0 (c (n "smallvec") (v "1.7.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "02gka690j8l12gl50ifg7axqnx1m6v6d1byaq0wl3fx66p3vdjhy") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.8.0 (c (n "smallvec") (v "1.8.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "10zf4fn63p2d6sx8qap3jvyarcfw563308x3431hd4c34r35gpgj") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.8.1 (c (n "smallvec") (v "1.8.1") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "18jwgycb2r06awf37a2xvzzf60029an0qdwk509w7ihzsqjwg26c") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.9.0 (c (n "smallvec") (v "1.9.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1lfss4vs5z5njm3ac9c499s5m1gphzm5a7gxcbw1zncpjmsdpl1g") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.10.0 (c (n "smallvec") (v "1.10.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1q2k15fzxgwjpcdv3f323w24rbbfyv711ayz85ila12lg7zbw1x5") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.11.0 (c (n "smallvec") (v "1.11.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1y9g8jcsizjbsiilgplrnavy8pd3cliy40pqgrq9zpczwkp4zfv2") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.11.1 (c (n "smallvec") (v "1.11.1") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0nmx8aw3v4jglqdcjv4hhn10d6g52c4bhjlzwf952885is04lawl") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-2.0.0-alpha.1 (c (n "smallvec") (v "2.0.0-alpha.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "12c01bbn64zsj1knlal2lqg8cqppcshd1ilsr4zmjnplh242pw9a") (f (quote (("write") ("specialization") ("may_dangle")))) (r "1.57")))

(define-public crate-smallvec-1.11.2 (c (n "smallvec") (v "1.11.2") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0w79x38f7c0np7hqfmzrif9zmn0avjvvm31b166zdk9d1aad1k2d") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-1.12.0 (c (n "smallvec") (v "1.12.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "17h0f8f02m6xnjzk82jbsdfypwncq9j3mllb3nbdzn7ah8gx74r5") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-2.0.0-alpha.2 (c (n "smallvec") (v "2.0.0-alpha.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0jsrvg9f7nhhd6l9kw3nig3sg38v657lg6ndyxladd1097ajwqiw") (f (quote (("write") ("specialization") ("may_dangle")))) (r "1.57")))

(define-public crate-smallvec-1.13.0 (c (n "smallvec") (v "1.13.0") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "get-size") (r "^0.1") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1s4370p4pv1zpi0xwrjq5913dwrbvlclh0qjzcdy8vym6417y61v") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics")))) (y #t)))

(define-public crate-smallvec-1.13.1 (c (n "smallvec") (v "1.13.1") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1mzk9j117pn3k1gabys0b7nz8cdjsx5xc6q7fwnm8r0an62d7v76") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-2.0.0-alpha.3 (c (n "smallvec") (v "2.0.0-alpha.3") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hlb7zgw1db15g4jwpw9rpgyygi92zrfa3sin9bzh66rzaj8h0q3") (f (quote (("write") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter")))) (r "1.57")))

(define-public crate-smallvec-2.0.0-alpha.4 (c (n "smallvec") (v "2.0.0-alpha.4") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0rx6fqqhfvdbpxd5xsfgsxn4hgmgzas0z5r09wvlyd9a3znmyd1z") (f (quote (("write") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter")))) (r "1.57")))

(define-public crate-smallvec-1.13.2 (c (n "smallvec") (v "1.13.2") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "debugger_test") (r "^0.1.0") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0rsw5samawl3wsw6glrsb127rx6sh89a8wyikicw6dkdcjd1lpiw") (f (quote (("write") ("union") ("specialization") ("may_dangle") ("drain_keep_rest" "drain_filter") ("drain_filter") ("debugger_visualizer") ("const_new" "const_generics") ("const_generics"))))))

(define-public crate-smallvec-2.0.0-alpha.5 (c (n "smallvec") (v "2.0.0-alpha.5") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hfsrink0m3jkfa37cpsa4jq94ri1wigcfz7x5vs56fmpcz47g94") (f (quote (("write") ("specialization") ("may_dangle") ("extract_if")))) (r "1.57")))

(define-public crate-smallvec-2.0.0-alpha.6 (c (n "smallvec") (v "2.0.0-alpha.6") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1gxjdf7biryg1d5j33ksn05zqfkq918fb42q6vlz9r2ays4qcxv6") (f (quote (("write") ("specialization") ("may_dangle") ("extract_if")))) (r "1.57")))

