(define-module (crates-io sm al smalltalk) #:use-module (crates-io))

(define-public crate-smalltalk-0.1.0 (c (n "smalltalk") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (d #t) (k 0)))) (h "040kbmqkcv7zf0hksqjkdlbyp746v3w0s6g28jlpw2jj7xa4pw5m") (y #t)))

(define-public crate-smalltalk-0.1.1 (c (n "smalltalk") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0m77bh6y1brl748f1xgr39rzxlq82r5rzla629c0hrr3gxw0smvj")))

(define-public crate-smalltalk-0.2.0 (c (n "smalltalk") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0xd4k3ibbgx3csm8dnnpjr9cvx6lic39j4hv5ckjy3q7l04ln3r0")))

(define-public crate-smalltalk-0.3.0 (c (n "smalltalk") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1g1sjippchghbi6jlaz4xdw72c4bpvhy6gfqz9crwvakx2v5wlq6")))

