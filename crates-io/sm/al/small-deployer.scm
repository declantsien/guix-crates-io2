(define-module (crates-io sm al small-deployer) #:use-module (crates-io))

(define-public crate-small-deployer-0.0.1 (c (n "small-deployer") (v "0.0.1") (d (list (d (n "hyper") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "slack-hook") (r "^0.0.2") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "14f41qhry8li25disln5llwkahgnqdlc8yi5w5wdnxzrzb51nf5d")))

(define-public crate-small-deployer-0.0.2 (c (n "small-deployer") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "slack-hook") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0x7naxq5cd6rzp1nvlpsyfwa2ilghy6yh1nxgm5qxlvny4wlzn4q")))

(define-public crate-small-deployer-0.0.3 (c (n "small-deployer") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "slack-hook") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0wbr3j1qca047f9rfb4c2w5wvnva3by6vdj7ycc6n1bvcflhsq01")))

(define-public crate-small-deployer-0.0.4 (c (n "small-deployer") (v "0.0.4") (d (list (d (n "hyper") (r "~0.6.15") (d #t) (k 0)) (d (n "serde") (r "~0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.6.1") (d #t) (k 0)) (d (n "slack-hook") (r "~0.1.2") (d #t) (k 0)) (d (n "time") (r "~0.1.33") (d #t) (k 0)))) (h "15a1242507m14mxj72jj4z3ssvppawzd59qdf7lfp92bg0z9hr9z")))

(define-public crate-small-deployer-0.1.0 (c (n "small-deployer") (v "0.1.0") (d (list (d (n "hyper") (r "~0.7.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slack-hook") (r "^0.1.2") (d #t) (k 0)) (d (n "small-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "~0.1.33") (d #t) (k 0)))) (h "0pwfkcm5m2rmizy5f2d2661kmgc49vsl2dxhca6ppz7hirxv5njk")))

(define-public crate-small-deployer-0.1.1 (c (n "small-deployer") (v "0.1.1") (d (list (d (n "hyper") (r "~0.7.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slack-hook") (r "^0.1.2") (d #t) (k 0)) (d (n "small-logger") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "~0.1.33") (d #t) (k 0)))) (h "0s9d4cqaj01i05i0m6q88n4h4krpccvrnr1dnl5ifjmh5pk4r11x")))

(define-public crate-small-deployer-0.1.2 (c (n "small-deployer") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.10") (f (quote ("security-framework"))) (d #t) (k 0)) (d (n "openssl") (r "^0.8.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slack-hook") (r "^0.1.2") (d #t) (k 0)) (d (n "small-logger") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "~0.1.33") (d #t) (k 0)))) (h "1l8mfz967y1qz8aavfs6ah46myyrcx1bvdkaq4xdxg4qsdqs495n")))

