(define-module (crates-io sm l- sml-rs) #:use-module (crates-io))

(define-public crate-sml-rs-0.0.1 (c (n "sml-rs") (v "0.0.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "17f1h5kz2rjipad5a74fgvvphgr1y2drgw4w7c1f99dv9mgz070s")))

(define-public crate-sml-rs-0.0.2 (c (n "sml-rs") (v "0.0.2") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1af1apfhzx2crz31iskwpxqnc4g325bm47c4iakl5r26c7lhzf4v") (f (quote (("alloc"))))))

(define-public crate-sml-rs-0.0.3 (c (n "sml-rs") (v "0.0.3") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1gyvi31yix4zxvzjz6gsz0x37spb5pcjrh5x8ixwdja3qwqx4786") (f (quote (("alloc"))))))

(define-public crate-sml-rs-0.0.4 (c (n "sml-rs") (v "0.0.4") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1pd8ks966v1k661mv2vycppsaa98winx1bl52i66d9xa8ni0kkz1") (f (quote (("alloc"))))))

(define-public crate-sml-rs-0.0.5 (c (n "sml-rs") (v "0.0.5") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1ii302rvssqxc9jgb2rfvzs2zf8c27miljmj9hwdrib5qvch1ddr") (f (quote (("alloc"))))))

(define-public crate-sml-rs-0.1.0 (c (n "sml-rs") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "glob"))) (d #t) (k 2)))) (h "19nbwk6p17kj5g250xjdlc9pd4bnhdx4y9lhfsj65qmyzfd67p0m") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-sml-rs-0.2.0 (c (n "sml-rs") (v "0.2.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "glob"))) (d #t) (k 2)))) (h "0fa00wmilvfhga3l3f7c6jn5y7h5iv2qzlmq32xaywa6j1ynalr1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-sml-rs-0.2.1 (c (n "sml-rs") (v "0.2.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "glob"))) (d #t) (k 2)))) (h "1kzl0v9m23j71akr40wbpsicz2s139kirr8972vydw752xpn0sis") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-sml-rs-0.3.0 (c (n "sml-rs") (v "0.3.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "glob"))) (d #t) (k 2)))) (h "09k6b0ms5fxh2h00bv64yaphh6ylw3n9r344nzsrcjrfzniadblq") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

