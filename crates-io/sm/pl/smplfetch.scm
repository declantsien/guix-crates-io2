(define-module (crates-io sm pl smplfetch) #:use-module (crates-io))

(define-public crate-smplfetch-0.1.0 (c (n "smplfetch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "1ihrk9m44m3xvc7m0qghq2hxrrvfgb33nn4h90djhf0pxz545px4")))

