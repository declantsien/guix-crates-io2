(define-module (crates-io sm pl smpl_fuzz) #:use-module (crates-io))

(define-public crate-smpl_fuzz-0.1.0 (c (n "smpl_fuzz") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lq1n1qj9rwadhizxi07gwdqk0br0gdjk7w8hf3390syssn7iind")))

(define-public crate-smpl_fuzz-0.1.1 (c (n "smpl_fuzz") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15yki5dv9na61p3d2l57jfg8ngpvjhx6n5irdbcj15qf8lvp0ndy")))

(define-public crate-smpl_fuzz-0.1.2 (c (n "smpl_fuzz") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1swbh179ky06vqsag6lk9q8lmm5kw5vxji999s6npr9dhhwfvkpf")))

(define-public crate-smpl_fuzz-0.2.0 (c (n "smpl_fuzz") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vzj05v9k7537m4xrf553zxzc9062j4kcdgzbxyhf3hqj768rigj") (y #t)))

(define-public crate-smpl_fuzz-0.1.3 (c (n "smpl_fuzz") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17sa67r3c1rlryg7nmsp13r9rzkwv41hbldl6hgialpk655dphy4")))

