(define-module (crates-io sm it smith_waterman) #:use-module (crates-io))

(define-public crate-smith_waterman-0.0.1 (c (n "smith_waterman") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.2.17") (d #t) (k 0)))) (h "1ksa1bzid01r1wyxslz8xs10hkcj4hynx3ka9xd9gxrwqh9xzmd5")))

(define-public crate-smith_waterman-0.1.0 (c (n "smith_waterman") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.2.17") (d #t) (k 0)))) (h "16j44zyw8986l5i2cfqwabgign6xxdsyli3i3ixr68adxvv2icik")))

(define-public crate-smith_waterman-0.1.1 (c (n "smith_waterman") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.2.17") (d #t) (k 0)))) (h "0bz5by0x8x9yy0fz9rpbgmvz3zihscxwy7s1ikw6r7xnpgs5bjkq")))

