(define-module (crates-io sm it smithay-clipboard) #:use-module (crates-io))

(define-public crate-smithay-clipboard-0.1.0 (c (n "smithay-clipboard") (v "0.1.0") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "1amgk03yf8m1h14alkbbvxzi2r537smlsbafk2l9sd02vrbvy660")))

(define-public crate-smithay-clipboard-0.1.1 (c (n "smithay-clipboard") (v "0.1.1") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "171fmsamypk6asafy3g9v2lclakpkysb238z2l8gzjnr15wlbvwk")))

(define-public crate-smithay-clipboard-0.2.0 (c (n "smithay-clipboard") (v "0.2.0") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "194p216w48n7h5v09lxwq1f4i2gwzcpxrnnxvfqdnqlkm0df53kq") (y #t)))

(define-public crate-smithay-clipboard-0.2.1 (c (n "smithay-clipboard") (v "0.2.1") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "15viqx58i5l99c6zq7ggsc00yfhxjc0pa73wacwhwvxqxjrzqi3r")))

(define-public crate-smithay-clipboard-0.3.0 (c (n "smithay-clipboard") (v "0.3.0") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "0g1yb6652i2c963s4qgvdvii2l1rnb8ps90k16wqa8m4qhhy8b88")))

(define-public crate-smithay-clipboard-0.3.1 (c (n "smithay-clipboard") (v "0.3.1") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "sctk") (r "^0.5") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "1rk9frl3fp4y46pbh7cv645m25n843bb7wgp94bc034yxbg09qvg")))

(define-public crate-smithay-clipboard-0.3.2 (c (n "smithay-clipboard") (v "0.3.2") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "1i4jz63r5bn1qvclp95s001x495y79mfr6al0jhkhw4mmzpz7ijh")))

(define-public crate-smithay-clipboard-0.3.3 (c (n "smithay-clipboard") (v "0.3.3") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "07kmlkkmqm7hydyxq8r717bb4xip8afs7d19ik9mp8gpks51zrnq")))

(define-public crate-smithay-clipboard-0.3.4 (c (n "smithay-clipboard") (v "0.3.4") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "0i79aqjzadj53lrx9m3cxqmfjizmzgkk1izm5mf8bdkxhyry15pk")))

(define-public crate-smithay-clipboard-0.3.5 (c (n "smithay-clipboard") (v "0.3.5") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "0gcyhs9iq3q5h7ay3ww37pj7dwfri1jbmndfrcylhrfk2x23yp1i")))

(define-public crate-smithay-clipboard-0.3.6 (c (n "smithay-clipboard") (v "0.3.6") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "1h7qhcx44cgwncgpn5llky0c56vgsg9mqrkybb2z37vsxxia4rwn")))

(define-public crate-smithay-clipboard-0.3.7 (c (n "smithay-clipboard") (v "0.3.7") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "17pn816q2yimfl0y5nls6xhazx4pcb203pzf9mk6yndmd1p4hnaa")))

(define-public crate-smithay-clipboard-0.4.0 (c (n "smithay-clipboard") (v "0.4.0") (d (list (d (n "andrew") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "sctk") (r "^0.6.1") (d #t) (k 0) (p "smithay-client-toolkit")))) (h "1lbvq2sym0m1i278ri0am14carsdq9k8i73lpxn1mk9myp3qwzli")))

(define-public crate-smithay-clipboard-0.5.0 (c (n "smithay-clipboard") (v "0.5.0") (d (list (d (n "sctk") (r "^0.9.1") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.9.1") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.26.3") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "19c07d46rbrq4q6cqvnb21qd7bznsy6lqxq3dzl8i2161pbifyzl")))

(define-public crate-smithay-clipboard-0.5.1 (c (n "smithay-clipboard") (v "0.5.1") (d (list (d (n "sctk") (r "^0.10") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.10") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.27") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "1nhqmv42bmc83nbk34ph02rzm86zddam1wgkqx909vl48fj0jqms")))

(define-public crate-smithay-clipboard-0.5.2 (c (n "smithay-clipboard") (v "0.5.2") (d (list (d (n "sctk") (r "^0.11") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.11") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.27") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "1m9lh21fhw9qp6fh9x6y2mrl0bzjj491lwvvfs5r6wmjm58dpsdr")))

(define-public crate-smithay-clipboard-0.6.0 (c (n "smithay-clipboard") (v "0.6.0") (d (list (d (n "sctk") (r "^0.12") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.12") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.28") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "0czk49ck54y0jds39m1aw9nwl6hycf96jmb6xl9fslja7yh00ln5")))

(define-public crate-smithay-clipboard-0.6.1 (c (n "smithay-clipboard") (v "0.6.1") (d (list (d (n "sctk") (r "^0.12") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.12") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.28") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "0hncg8a57riyr6yirbiy0181xvkmjn2ch944ndvf4afrh0sfq3kf")))

(define-public crate-smithay-clipboard-0.6.2 (c (n "smithay-clipboard") (v "0.6.2") (d (list (d (n "sctk") (r "^0.12") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.12") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.28") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "14dwisd56cbr80zf719l3fh0n8pm1fjmvry9lsbhdbccf8cv525b")))

(define-public crate-smithay-clipboard-0.6.3 (c (n "smithay-clipboard") (v "0.6.3") (d (list (d (n "sctk") (r "^0.12") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.12") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.28") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "09z59l0h7r28m6h5w8mhxkpr5kzn77nj9bknv4h85425yvx4sf06")))

(define-public crate-smithay-clipboard-0.6.4 (c (n "smithay-clipboard") (v "0.6.4") (d (list (d (n "sctk") (r "^0.14") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.14") (d #t) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.28") (f (quote ("dlopen"))) (d #t) (k 0)))) (h "1wwmjadi02vqjcrgd70g6s4b8p2k8m3q87zqa560vxflgd55nv4q")))

(define-public crate-smithay-clipboard-0.6.5 (c (n "smithay-clipboard") (v "0.6.5") (d (list (d (n "sctk") (r "^0.15") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.15") (f (quote ("calloop"))) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.29") (f (quote ("use_system_lib"))) (d #t) (k 0)))) (h "0mfx96cqws3h1yp4f354yvrfh9ylhfyzr877p39byy2ks8dma2v1") (f (quote (("dlopen" "sctk/dlopen" "wayland-client/dlopen") ("default" "dlopen"))))))

(define-public crate-smithay-clipboard-0.6.6 (c (n "smithay-clipboard") (v "0.6.6") (d (list (d (n "sctk") (r "^0.16") (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.16") (f (quote ("calloop"))) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-client") (r "^0.29") (f (quote ("use_system_lib"))) (d #t) (k 0)))) (h "1s5hyhbmnk75i0sm14wy4dy7c576a4dyi1chfwdhpbhz1a3mqd0a") (f (quote (("dlopen" "sctk/dlopen" "wayland-client/dlopen") ("default" "dlopen"))))))

(define-public crate-smithay-clipboard-0.7.0 (c (n "smithay-clipboard") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "sctk") (r "^0.18.0") (f (quote ("calloop"))) (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.18.0") (f (quote ("calloop" "xkbcommon"))) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-backend") (r "^0.3.0") (f (quote ("client_system"))) (k 0)))) (h "19m1rqw4fsp9x92cji9qz169004djjh376b68ylcp9g51hl2pdhb") (f (quote (("dlopen" "wayland-backend/dlopen") ("default" "dlopen")))) (r "1.65.0")))

(define-public crate-smithay-clipboard-0.7.1 (c (n "smithay-clipboard") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "sctk") (r "^0.18.0") (f (quote ("calloop"))) (k 0) (p "smithay-client-toolkit")) (d (n "sctk") (r "^0.18.0") (f (quote ("calloop" "xkbcommon"))) (k 2) (p "smithay-client-toolkit")) (d (n "wayland-backend") (r "^0.3.0") (f (quote ("client_system"))) (k 0)))) (h "0bc84l2pkzz7lsll0bf71axyvpixs5ny1b4yv5m9s1d89qsyg4f0") (f (quote (("dlopen" "wayland-backend/dlopen") ("default" "dlopen")))) (r "1.65.0")))

