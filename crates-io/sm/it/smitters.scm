(define-module (crates-io sm it smitters) #:use-module (crates-io))

(define-public crate-smitters-0.1.0 (c (n "smitters") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "kurbo") (r "^0.9.5") (d #t) (k 0)))) (h "0vhdr3dnqggzwlwwim34gr4lcfa9iqpdkpqbbsfirdr8y6gz5fwg")))

