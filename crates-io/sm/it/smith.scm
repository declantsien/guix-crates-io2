(define-module (crates-io sm it smith) #:use-module (crates-io))

(define-public crate-smith-0.3.0 (c (n "smith") (v "0.3.0") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.0.8") (d #t) (k 0)))) (h "1p8yc3m7kiglmhlnilnslpw2abf07zap7xpaz4i3jnwwbjhpfmda")))

(define-public crate-smith-0.3.1 (c (n "smith") (v "0.3.1") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.0.8") (d #t) (k 0)))) (h "0yfjd257ag5g6irfkzvalhicybpvkl2r0qp7xhy36skan50s5bdg")))

(define-public crate-smith-0.3.2 (c (n "smith") (v "0.3.2") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.0.8") (d #t) (k 0)))) (h "1l6rnx9jprs20bgzrqv2jgxvdk7pn9nkkqmn4fz7fjffkx3mp359")))

(define-public crate-smith-0.3.3 (c (n "smith") (v "0.3.3") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.0.8") (d #t) (k 0)))) (h "1ghv5x53r5p7y8bawzr4i5zkzy8lmsr81dnvnbdyd2q2fy8169b7")))

(define-public crate-smith-0.3.5 (c (n "smith") (v "0.3.5") (d (list (d (n "clipboard") (r "^0.1") (d #t) (k 0)) (d (n "ropey") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0kjhiff4bxb9kq93mvfbdk5m1gfqvi4866w7pwxlnn71zvri70a2")))

(define-public crate-smith-0.3.6 (c (n "smith") (v "0.3.6") (d (list (d (n "clipboard") (r "^0.1") (d #t) (k 0)) (d (n "ropey") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "171b99qbwhywcns4kr40bjpxnr292dw4vpg7iln8827lgrg5kz2n")))

(define-public crate-smith-0.3.7 (c (n "smith") (v "0.3.7") (d (list (d (n "clipboard") (r "^0.1") (d #t) (k 0)) (d (n "ropey") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0kdjg2c330xwq9zplz53ij7sb74n2c8hmahhmysq4pi99gihls2a")))

(define-public crate-smith-0.3.8 (c (n "smith") (v "0.3.8") (d (list (d (n "clipboard") (r "^0.1") (d #t) (k 0)) (d (n "ropey") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0iq432hilc9rc726dpdrg9qcafxf018qvaa0nj2sgj9rm9glcwyz")))

(define-public crate-smith-0.3.9 (c (n "smith") (v "0.3.9") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.1.4") (d #t) (k 0)))) (h "0wmn52bd67rkdax5f9cnnpn0hqbi7c15aj4mfh6l5a721xhlgy7q")))

(define-public crate-smith-0.3.10 (c (n "smith") (v "0.3.10") (d (list (d (n "clipboard") (r "^0.1.2") (d #t) (k 0)) (d (n "ropey") (r "^0.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "0fhxax1c4ccm4g19hqhjs7bbp2p3m7d0fkga4wlnfz76hfrl88wf")))

(define-public crate-smith-0.4.0 (c (n "smith") (v "0.4.0") (d (list (d (n "clipboard") (r "^0.1") (d #t) (k 0)) (d (n "ropey") (r "^0.5") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0kpyq5ylyv3l49zfvb6zaasy1xdi3ja5m4gr9rnvh5gbjg31llhp")))

(define-public crate-smith-0.5.0 (c (n "smith") (v "0.5.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1dg4w8wqsh0bf4pzb2s8lffv0wr4sbgzw086l35dvady1h8dh0ln")))

(define-public crate-smith-0.5.1 (c (n "smith") (v "0.5.1") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0jmsl7731dwrans2hfp3n8s667xzxz826g70fi9b8r666idjbjrc")))

(define-public crate-smith-0.6.0 (c (n "smith") (v "0.6.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0wk3jy6ccyzxjf7jp88jrv7xs23rpvb8mikcgxmh74wm373jkkb4")))

(define-public crate-smith-1.0.0 (c (n "smith") (v "1.0.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "syntect") (r "^3.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1abhxmzq9g998gqhz0kxnjfxnvianlp12mpdr4akjmmxswm6j6df")))

(define-public crate-smith-1.1.0 (c (n "smith") (v "1.1.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "syntect") (r "^3.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1kc6rm4ly6cp3l7fs6f9d5i1rgvdnkr2b9riqnvz18gjffn5kyqi")))

(define-public crate-smith-1.1.1 (c (n "smith") (v "1.1.1") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "syntect") (r "^3.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1wz8nyb1vf49bk8y7jl37hgg0y0zqrl17cd2v5aia3fyr2c5148c")))

(define-public crate-smith-1.2.0 (c (n "smith") (v "1.2.0") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "syntect") (r "^3.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0ml6vcwd19hvyyp6g2lfw8lbz2pnvh9l0azfsdsy5jqy1p1k7l83")))

(define-public crate-smith-1.2.1 (c (n "smith") (v "1.2.1") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^0.9.2") (d #t) (k 0)) (d (n "syntect") (r "^3.0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1ncqg7q76fdpnpnqgrh12y6d3d6yz6vcic9ibjazkz5ykynsdnwf")))

(define-public crate-smith-1.2.2 (c (n "smith") (v "1.2.2") (d (list (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "delegate") (r "^0.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.0.0") (d #t) (k 0)) (d (n "syntect") (r "^3.0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "08f1gwv2fy28mz71g91a459milx2wmkqj5f9hpfvsw6rp8il64d2")))

(define-public crate-smith-1.3.0 (c (n "smith") (v "1.3.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "delegate") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ropey") (r "^1.1.0") (d #t) (k 0)) (d (n "syntect") (r "^3.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "19w08n15c7xyacczbf52d7fblr69kj17zi0cvj9aiys881a9pj11")))

(define-public crate-smith-2.0.0 (c (n "smith") (v "2.0.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "delegate") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ropey") (r "^1.1.0") (d #t) (k 0)) (d (n "syntect") (r "^3.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "14raixj7q44l3ll6515hz4z7j83d2fcdakb5kykil4yd086l923x")))

(define-public crate-smith-2.0.1 (c (n "smith") (v "2.0.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "delegate-attr") (r "^0.2.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ropey") (r "^1.1.0") (d #t) (k 0)) (d (n "syntect") (r "^3.3.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0myrwx9ni14xwgyd799acxbdyxbc0bmdb3dby5r93xwdcsihnbyj")))

