(define-module (crates-io sm li smlibrbase) #:use-module (crates-io))

(define-public crate-smlibrbase-0.1.0 (c (n "smlibrbase") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.0") (d #t) (k 0)) (d (n "smdton") (r "^0.1.0") (d #t) (k 0)))) (h "0r6k7yadnl11yk5whh0v36xl55541xafy912asdrjlg7pg90w107") (y #t)))

(define-public crate-smlibrbase-0.1.1 (c (n "smlibrbase") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.1") (d #t) (k 0)) (d (n "smdton") (r "^0.1.1") (d #t) (k 0)))) (h "058ww14rg1nxmmwh7ggz9r3s262k7fggrlldxcqs9dd1v4np39kp")))

(define-public crate-smlibrbase-0.1.2 (c (n "smlibrbase") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.2") (d #t) (k 0)) (d (n "smdton") (r "^0.1.2") (d #t) (k 0)))) (h "0qip12y3nmnhb0ykkzl4yilhw0qdkqm3cfjc4c1chlsxgh45v1vf")))

