(define-module (crates-io sm s_ sms_splitter) #:use-module (crates-io))

(define-public crate-sms_splitter-0.1.0 (c (n "sms_splitter") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1a3sqdqrvdaavpmy1pc235nyinqclc9fgv31fhbb6swd29fdvagy") (y #t)))

(define-public crate-sms_splitter-0.1.1 (c (n "sms_splitter") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0klibv9b2pp1gfjdlrqp12zjqi6qaziq0azw2g593izpn5i6pbcw") (y #t)))

(define-public crate-sms_splitter-0.1.2 (c (n "sms_splitter") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0ji46jrhxd61d4d11n8p5f4y9ycikafbkxixhn01wdsdcjkdpgs8") (y #t)))

(define-public crate-sms_splitter-0.1.3 (c (n "sms_splitter") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1dnra2lgf7nviynyj6abp61vx3hvyc0ybjbxzx4hg116jbms0310") (y #t)))

(define-public crate-sms_splitter-0.1.4 (c (n "sms_splitter") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1j7mjd4qicm3vkd2bdvs3bvlzyg4kmwsvi9m67rnkqfhgp3lfhfb") (y #t)))

(define-public crate-sms_splitter-0.1.5 (c (n "sms_splitter") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "159lnvvnkynwqy0bnyqa2zgpyd63wp122sqprnwyzv54vsb9jdjs") (y #t)))

(define-public crate-sms_splitter-0.1.6 (c (n "sms_splitter") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1nhgm26dzjy1ijxfiw1hpbhxmr5k712yfd1g5sik8pkx6icxwnvb") (y #t)))

(define-public crate-sms_splitter-0.1.7 (c (n "sms_splitter") (v "0.1.7") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0ap2dhzwzm3b2y3905s4r9zdvy273w49hnhd38p0c0z6dcfkxn4j")))

(define-public crate-sms_splitter-0.1.8 (c (n "sms_splitter") (v "0.1.8") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "1h951afbs0vma823bc6y460pm64az6bjrpdx45z2fkiyhvywsr7w")))

(define-public crate-sms_splitter-0.1.9 (c (n "sms_splitter") (v "0.1.9") (d (list (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0mhy11w22qk725qzk3qwyk77qjk30k6n7mvpi5yqi6m09xi0jazg")))

