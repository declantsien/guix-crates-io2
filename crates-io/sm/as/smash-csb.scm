(define-module (crates-io sm as smash-csb) #:use-module (crates-io))

(define-public crate-smash-csb-0.9.0 (c (n "smash-csb") (v "0.9.0") (d (list (d (n "binrw") (r "^0.11") (d #t) (k 0)) (d (n "hash40") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qvmd28s07g78fl1jqxkglkayl6gmvxdc72xhdm86vnhimvl1xs6") (s 2) (e (quote (("serde" "dep:serde"))))))

