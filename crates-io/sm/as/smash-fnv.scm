(define-module (crates-io sm as smash-fnv) #:use-module (crates-io))

(define-public crate-smash-fnv-0.10.0 (c (n "smash-fnv") (v "0.10.0") (d (list (d (n "binrw") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1w7nlbgwh4wfkkr5cz8m7r1yqkhmxbb5mq2ndfcw564hc2vm6sml") (s 2) (e (quote (("serde" "dep:serde"))))))

