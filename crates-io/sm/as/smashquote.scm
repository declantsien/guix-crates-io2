(define-module (crates-io sm as smashquote) #:use-module (crates-io))

(define-public crate-smashquote-0.1.0 (c (n "smashquote") (v "0.1.0") (h "1k7jdfgpbhlqj556wdq46ig224d21abgwr6ladxg6rbhinwp2yhb")))

(define-public crate-smashquote-0.1.1 (c (n "smashquote") (v "0.1.1") (h "18vimc0p163kvx26cxpvcdccnz51hn5yjai69hk01sqihaqdw2gc")))

(define-public crate-smashquote-0.1.2 (c (n "smashquote") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "00yzlpmaax3g9mc4biny5809jmxpkwjd1xlsda3c5b3klvs0b7rn")))

