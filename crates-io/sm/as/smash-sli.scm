(define-module (crates-io sm as smash-sli) #:use-module (crates-io))

(define-public crate-smash-sli-0.8.1 (c (n "smash-sli") (v "0.8.1") (d (list (d (n "binread") (r "^1.4.0") (d #t) (k 0)) (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1kvxyjzwinhvhvis13lgdxb0yvxklhaczdansiy1fisvjp6h069l") (f (quote (("derive_serde" "serde" "lazy_static") ("cli" "structopt" "derive_serde" "serde_yaml"))))))

(define-public crate-smash-sli-0.9.0 (c (n "smash-sli") (v "0.9.0") (d (list (d (n "binrw") (r "^0.11") (d #t) (k 0)) (d (n "hash40") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a1msz9sin3r6z93fqxx9n11g97f15hbsav0x9xnbsdry7zp4kvk") (s 2) (e (quote (("serde" "dep:serde"))))))

