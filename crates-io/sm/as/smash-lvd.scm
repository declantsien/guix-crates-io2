(define-module (crates-io sm as smash-lvd) #:use-module (crates-io))

(define-public crate-smash-lvd-0.3.0 (c (n "smash-lvd") (v "0.3.0") (d (list (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "07v2k4wmm98qsx3zm4jnds6ms1qcyxcyp05ynjixllmsdsil3ckn") (f (quote (("serde_support" "serde") ("cli" "serde_support" "clap" "serde_yaml"))))))

(define-public crate-smash-lvd-0.4.0 (c (n "smash-lvd") (v "0.4.0") (d (list (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0lbikfwx44gw8a1hxvp3bh3bc5045w9nl3fw4srcqi34lqyli6rb") (f (quote (("serde_support" "serde") ("cli" "serde_support" "clap" "serde_yaml"))))))

