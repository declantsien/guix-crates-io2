(define-module (crates-io sm as smashnet) #:use-module (crates-io))

(define-public crate-smashnet-0.1.0 (c (n "smashnet") (v "0.1.0") (d (list (d (n "skyline") (r "^0.2.1") (d #t) (k 0)))) (h "1yhmbsiw23q77bkj2m5xrq7hyw7qphr1bqa5gsjfyfb0gda0j6av")))

(define-public crate-smashnet-0.2.0 (c (n "smashnet") (v "0.2.0") (d (list (d (n "skyline") (r "^0.2.1") (d #t) (k 0)))) (h "1rjja6l0xcngkp1jqqvrb5rlflmfgqwywxbzdb54kxhrng4vswck")))

(define-public crate-smashnet-0.2.1 (c (n "smashnet") (v "0.2.1") (d (list (d (n "skyline") (r "^0.2.1") (d #t) (k 0)))) (h "07cwn8322b5x8xja0d5yylnssmxlz7wg8lawvjywvaix668ii8xj")))

