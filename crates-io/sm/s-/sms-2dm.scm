(define-module (crates-io sm s- sms-2dm) #:use-module (crates-io))

(define-public crate-sms-2dm-0.1.0 (c (n "sms-2dm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1h2hfvrh58akskfm0clayqd8xzkxgd5x8wa4y0lf0z6zldsig9an")))

(define-public crate-sms-2dm-0.2.0 (c (n "sms-2dm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1h6qj43z65dvl1q5p9aqjiqrlkjklkhq5vxs3anj9knh0hfyggsm") (f (quote (("warn" "log") ("strict") ("default" "warn"))))))

