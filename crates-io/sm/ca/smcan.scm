(define-module (crates-io sm ca smcan) #:use-module (crates-io))

(define-public crate-smcan-0.1.0 (c (n "smcan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "1wydsll185sr8waa32zm2asc73wwdzxqfkv45xglc06mr6b2znps")))

(define-public crate-smcan-0.1.1 (c (n "smcan") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "1fv73gyd0f64an7q541zlqk3i2p9xlmslx8lyprv6n4hj1hda60j")))

(define-public crate-smcan-0.1.2 (c (n "smcan") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "0la2parac6wp8mnkvzav55xl1vawna0h1ws666gfp26bwhp9rnca")))

