(define-module (crates-io sm lo smloadwasm) #:use-module (crates-io))

(define-public crate-smloadwasm-0.1.0 (c (n "smloadwasm") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.0") (d #t) (k 0)) (d (n "smdton") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^17.0.0") (f (quote ("default"))) (k 0)))) (h "0y2hcqkk4j2v6i7ai83yirqw172yqb9qa5qr7frsgwnbx6bbsxbn") (y #t)))

(define-public crate-smloadwasm-0.1.1 (c (n "smloadwasm") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.1") (d #t) (k 0)) (d (n "smdton") (r "^0.1.1") (d #t) (k 0)) (d (n "wasmtime") (r "^17.0.0") (f (quote ("default"))) (k 0)))) (h "1r7hqxiqwaa5gczdb11hfim9qrj5mch9gm38d22ls5fzfx5qxin4")))

(define-public crate-smloadwasm-0.1.2 (c (n "smloadwasm") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smcore") (r "^0.1.2") (d #t) (k 0)) (d (n "smdton") (r "^0.1.2") (d #t) (k 0)) (d (n "wasmtime") (r "^17.0.0") (f (quote ("default"))) (k 0)))) (h "1ibim66d2cmywkymyvzycn8rgpi1qjbj98hg9mj893035nby5jsg")))

