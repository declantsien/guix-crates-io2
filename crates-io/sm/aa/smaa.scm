(define-module (crates-io sm aa smaa) #:use-module (crates-io))

(define-public crate-smaa-0.1.0 (c (n "smaa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "1n4z5s1ysq3in6fwprbpxbm3pbyfisk9mvhbhyxsaz18s6gyrvzb")))

(define-public crate-smaa-0.2.1 (c (n "smaa") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "0s2w31cjbnbbmgzl5p8sjv8q0m12cp90jf9sjmz7jilnj3vf84k1")))

(define-public crate-smaa-0.2.2 (c (n "smaa") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "1bmvz2rwyj1jfy3d17l89fdnm556gw2nn9xrb209xj9sr56pm1ql")))

(define-public crate-smaa-0.3.0 (c (n "smaa") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "03rxwzh74gbf9kw2m1h91b4a4la70886m3gk6zjnhph2x15xvk5k")))

(define-public crate-smaa-0.4.0 (c (n "smaa") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "0f1iv5pjjxwl33hvnzgs23xgqga33k4b6n775gc6lk3jgkfvbnrz")))

(define-public crate-smaa-0.5.0 (c (n "smaa") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "shaderc") (r "^0.7.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.11") (f (quote ("spirv"))) (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "01rb23lkgxg4fw29dff48bdhmvk5liy9r3nsyzvfn2pmgy8whnjh")))

(define-public crate-smaa-0.6.0 (c (n "smaa") (v "0.6.0") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "naga") (r "^0.8.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 2)))) (h "0mqfjrg5bzw0618qphhfvcnpwdq5adscjwkj59x7xrchbxn2vqy6")))

(define-public crate-smaa-0.7.0 (c (n "smaa") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "naga") (r "^0.9") (d #t) (k 0)) (d (n "wgpu") (r "^0.13") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "0jmhkb3g5inv2vpbdvvgakzkcpc30j975hvn8mwagfz35c37b0xx")))

(define-public crate-smaa-0.8.0 (c (n "smaa") (v "0.8.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "naga") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.27.3") (d #t) (k 2)))) (h "1qr3fjaxjbbhdfzh7xr27pz1xa83yxl78mbvwibxz80vszqgzp76")))

(define-public crate-smaa-0.9.0 (c (n "smaa") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "naga") (r "^0.11.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "1ixywbybb93hjjapbigc55sw0ix46lyq19ibxw7hq0ns5yfyl3ra")))

(define-public crate-smaa-0.10.0 (c (n "smaa") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "naga") (r "^0.12") (d #t) (k 0)) (d (n "wgpu") (r "^0.16") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "1gx6mk7m3kzlikp6zm5xf5kv9df7zyd1sw1i90g797fwcs0ilnbk")))

(define-public crate-smaa-0.11.0 (c (n "smaa") (v "0.11.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "naga") (r "^0.13") (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "052nzznlfcxdkblv2yc878zhs4dvzhkhaa1yaznpm1j68khczgkk")))

(define-public crate-smaa-0.12.0 (c (n "smaa") (v "0.12.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "wgpu") (r "^0.18") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "1scqraadavsh3lq7rnc4x8a2b12pdf2s58w7m9zxkp9r4j26zqvj")))

(define-public crate-smaa-0.13.0 (c (n "smaa") (v "0.13.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "wgpu") (r "^0.19") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "07lb8b6rx02lspwnfhsyzhbvgc7cwkm0yqhllzzxmdziksc1r2k9")))

(define-public crate-smaa-0.14.0 (c (n "smaa") (v "0.14.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "wgpu") (r "^0.20") (f (quote ("glsl"))) (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0sqlb0blllh9s8z3jmhhrpkccb2qviv0d4jf1sm9lyrk8blgkpq8")))

