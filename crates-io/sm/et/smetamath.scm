(define-module (crates-io sm et smetamath) #:use-module (crates-io))

(define-public crate-smetamath-3.0.0 (c (n "smetamath") (v "3.0.0") (d (list (d (n "clap") (r "^2.5.2") (d #t) (k 0)) (d (n "filetime") (r "^0.1.10") (d #t) (k 0)) (d (n "fnv") (r "^1.0.2") (d #t) (k 0)))) (h "131f5c9b3pnpa31djbdz20iv8kvvvbmbjb7bd053an90dvbry4s9") (f (quote (("sysalloc"))))))

