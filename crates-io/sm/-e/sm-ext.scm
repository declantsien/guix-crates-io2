(define-module (crates-io sm -e sm-ext) #:use-module (crates-io))

(define-public crate-sm-ext-0.1.0 (c (n "sm-ext") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "sm-ext-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1q19j1s83wzg9kxijrlqww1kj2fj69qgdd0ni1x6ygxwrbhzarq7") (f (quote (("default") ("abi_thiscall" "sm-ext-derive/abi_thiscall"))))))

(define-public crate-sm-ext-0.2.0 (c (n "sm-ext") (v "0.2.0") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "sm-ext-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1f5hgi15asdnlb8xvkfwmkw0y128dwi6f8paqnnc64a831nldk2d") (f (quote (("default") ("abi_thiscall" "sm-ext-derive/abi_thiscall"))))))

(define-public crate-sm-ext-0.3.0 (c (n "sm-ext") (v "0.3.0") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 2)) (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "sm-ext-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1nlg7kbz0ghwk58sm52gmg7cv76mj281mp543ymr4vbzbgadkp5w") (f (quote (("default") ("abi_thiscall" "sm-ext-derive/abi_thiscall"))))))

