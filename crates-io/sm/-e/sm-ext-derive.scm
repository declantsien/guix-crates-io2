(define-module (crates-io sm -e sm-ext-derive) #:use-module (crates-io))

(define-public crate-sm-ext-derive-0.1.0 (c (n "sm-ext-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11blpl86jxzwb51hkkh9as8jf7b78cx5b2r63bghvzb931kaz05k") (f (quote (("default") ("abi_thiscall"))))))

(define-public crate-sm-ext-derive-0.2.0 (c (n "sm-ext-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhzb2ch14g9s85lrlklqnjkx99xlivjf75qrzf2bn5lrw3psxg0") (f (quote (("default") ("abi_thiscall"))))))

(define-public crate-sm-ext-derive-0.3.0 (c (n "sm-ext-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hkgwm83zx0c3xwsbzvld9vgbrsl0ycl04psc02db0gsp6067sbm") (f (quote (("default") ("abi_thiscall"))))))

