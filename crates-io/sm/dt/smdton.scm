(define-module (crates-io sm dt smdton) #:use-module (crates-io))

(define-public crate-smdton-0.1.0 (c (n "smdton") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0f7czgq0vkhbhmg5xh99x339j94465j319sb8wr8zgj0lwdjnlld") (y #t)))

(define-public crate-smdton-0.1.1 (c (n "smdton") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ypprmlgxvkywc2rkay168agq7p28lr1jgvbw9bjkmfdv73876sv")))

(define-public crate-smdton-0.1.2 (c (n "smdton") (v "0.1.2") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0zfc4hgpmxpkx7w0a4i6357590ilq2jw72hi69d7i83wydw2xcyd")))

