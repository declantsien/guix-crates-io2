(define-module (crates-io sm ec smecs) #:use-module (crates-io))

(define-public crate-smecs-0.1.1 (c (n "smecs") (v "0.1.1") (h "044jyx4n10awhsgkgjjqiw9999bb08fcr2m0d2a0pylb0ilbkj7w")))

(define-public crate-smecs-0.1.2 (c (n "smecs") (v "0.1.2") (h "1nhz3xprzzqppz76338nplpy60z53f9vayxpr4vj1597az82zr9c")))

(define-public crate-smecs-0.1.3 (c (n "smecs") (v "0.1.3") (h "1j7swacwqjs2b78n5yhcyipk9zar767vmm9ywqfv8lmg6qnhfhfw")))

(define-public crate-smecs-0.1.4 (c (n "smecs") (v "0.1.4") (h "0pdgyridiijn0cy9vsy20vc2lphkix697gaal207i3clxcraqz62")))

(define-public crate-smecs-0.1.5 (c (n "smecs") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0727c599dwbvj7dkzd5m99z1jm3cvxnca5975yx0d242mdbqpvaa") (f (quote (("static-dispatch") ("deafault-fatures"))))))

