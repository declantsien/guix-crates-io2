(define-module (crates-io sm aw smawk) #:use-module (crates-io))

(define-public crate-smawk-0.1.0 (c (n "smawk") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand_derive") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "01dnwj3h90629flzaywz0y2jkjic9j2vnfgamj2myclq9llk690q")))

(define-public crate-smawk-0.2.0 (c (n "smawk") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ghri6nghp8p0yp292fjan303ipm5rg4cb216r86xqgmz86fgjcx")))

(define-public crate-smawk-0.3.0 (c (n "smawk") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0va6kj2lxmw6rvq2bkc0hp71gmr2kdnr5x375svzx4yhjxy77g71")))

(define-public crate-smawk-0.3.1 (c (n "smawk") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hv0q1mw1r1brk7v3g4a80j162p7g1dri4bdidykrakzfqjd4ypn")))

(define-public crate-smawk-0.3.2 (c (n "smawk") (v "0.3.2") (d (list (d (n "ndarray") (r "^0.15.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0344z1la39incggwn6nl45k8cbw2x10mr5j0qz85cdz9np0qihxp")))

