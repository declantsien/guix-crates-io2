(define-module (crates-io sm or smores) #:use-module (crates-io))

(define-public crate-smores-0.0.0 (c (n "smores") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.15") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1svmclja83h6kidwjnsi8zdj2xpr2khhmr0ay6pi9j4rxs8vbfl7") (r "1.70.0")))

