(define-module (crates-io sm il smileypyramid) #:use-module (crates-io))

(define-public crate-smileypyramid-1.0.0 (c (n "smileypyramid") (v "1.0.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ncadrx2dlski6b7g51l4yc63b3c9iryxc52q3b1lbmd2xyq7990")))

(define-public crate-smileypyramid-1.0.1 (c (n "smileypyramid") (v "1.0.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "135w7dx33pihp8k2afq52qd3hn4krw8zsr4lz761bbvpxs0lhdj4")))

