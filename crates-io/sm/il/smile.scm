(define-module (crates-io sm il smile) #:use-module (crates-io))

(define-public crate-smile-0.1.0 (c (n "smile") (v "0.1.0") (h "1va9v1wmmdfwx2bblnipmy1g37j00rn774fwjx2d971ix5r95bkv")))

(define-public crate-smile-0.1.1 (c (n "smile") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1j93fl9dd8asxskwhkxp67kgx420zz4brp8rr2m02s2ngn0krnw4")))

