(define-module (crates-io sm il smile-marco) #:use-module (crates-io))

(define-public crate-smile-marco-0.1.0 (c (n "smile-marco") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1pi5xya8p8w2ybgg8b1g0mdnzdyn8qvwyl71wm7wzfnqzmpi8pgi") (f (quote (("wither") ("setter") ("getter") ("full" "getter" "setter" "wither" "builder") ("default" "full") ("builder"))))))

(define-public crate-smile-marco-1.0.0 (c (n "smile-marco") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1x0amlgsqd0lpk5i3shl6027b1n3i2blmz5pidphw97217f416k5") (f (quote (("wither") ("setter") ("getter") ("full" "getter" "setter" "wither" "builder") ("default" "full") ("builder"))))))

