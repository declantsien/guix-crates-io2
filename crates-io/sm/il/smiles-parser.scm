(define-module (crates-io sm il smiles-parser) #:use-module (crates-io))

(define-public crate-smiles-parser-0.1.0 (c (n "smiles-parser") (v "0.1.0") (d (list (d (n "basechem") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "18zx4wkm2csnkab8smp15csbaghs5d80p4x0l91kxmc2vy5qnra1")))

(define-public crate-smiles-parser-0.2.0 (c (n "smiles-parser") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "080z2nsf23sjq6pxsvvvpbjl6y0znqc2nbd08rz8afbv2qzy39rv")))

(define-public crate-smiles-parser-0.2.1 (c (n "smiles-parser") (v "0.2.1") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1sqwbl8mza55dw8r69xc26q6jc5sgcdswmqf9v9ri3fb77v0z0q9")))

(define-public crate-smiles-parser-0.3.0 (c (n "smiles-parser") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "ptable") (r "^0.3") (d #t) (k 0) (p "periodic-table-on-an-enum")))) (h "1i38mhhqlv045l87d8sc0fmj0v52g0b5sryqyrh8sqnil4mk4cx0") (f (quote (("graph" "petgraph" "itertools" "derive_more"))))))

(define-public crate-smiles-parser-0.4.0 (c (n "smiles-parser") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "ptable") (r "^0.3") (d #t) (k 0) (p "periodic-table-on-an-enum")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0542npwxy6zlf5i0k305klmsnbb2179cyz6y6gxn7nc5bjn54lay") (f (quote (("graph" "petgraph" "itertools" "derive_more"))))))

(define-public crate-smiles-parser-0.4.1 (c (n "smiles-parser") (v "0.4.1") (d (list (d (n "derive_more") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "ptable") (r "^0.3") (d #t) (k 0) (p "periodic-table-on-an-enum")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "16kn9i1nnmivx753k8ivnvchq2k5spa8d0c0wvnii9izhdj9zs6p") (f (quote (("graph" "petgraph" "itertools" "derive_more"))))))

