(define-module (crates-io sm s7 sms77-client) #:use-module (crates-io))

(define-public crate-sms77-client-0.1.0 (c (n "sms77-client") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (f (quote ("json"))) (d #t) (k 0)))) (h "070p6is0l86hag6zs3c5nwz6wnwsalkyqrrms9z8zd2r4jc6ql2b")))

(define-public crate-sms77-client-0.2.0 (c (n "sms77-client") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0w4hz0hb26qwy79ji1sgb5xnnj3g6xf9mknnhz29cykm6wdcqx93")))

