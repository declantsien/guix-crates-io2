(define-module (crates-io sm #{4-}# sm4-gcm) #:use-module (crates-io))

(define-public crate-sm4-gcm-0.0.0 (c (n "sm4-gcm") (v "0.0.0") (d (list (d (n "sm4") (r "^0.5.1") (d #t) (k 0)))) (h "1lc7gf2nlnzz5pddcsqd42468a9hdqpzd2mrw671lqsyz8ww43bs") (y #t)))

(define-public crate-sm4-gcm-0.1.0 (c (n "sm4-gcm") (v "0.1.0") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "ghash") (r "^0.5.0") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sm4") (r "^0.5.1") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "05d3p54nl3fvlmnamdzjs09b9747gvy0sgzk5j8d5927pylag17x")))

(define-public crate-sm4-gcm-0.1.2 (c (n "sm4-gcm") (v "0.1.2") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "ghash") (r "^0.5.0") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sm4") (r "^0.5.1") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "09z7jmg7jcnvh959bm9lhfqn1d9y6j3hzhgf9a7jkprkrbffkmn3")))

