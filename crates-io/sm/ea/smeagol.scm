(define-module (crates-io sm ea smeagol) #:use-module (crates-io))

(define-public crate-smeagol-0.1.0 (c (n "smeagol") (v "0.1.0") (d (list (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "0mfiip3mv3qlwz56vafsnwbzfqhf0z15m0z2z8ds3myas2rg76xj")))

(define-public crate-smeagol-0.1.1 (c (n "smeagol") (v "0.1.1") (d (list (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "0mszlimcyam5cgfk9fqca4nn4w0x5sdd33207swxhva52kw1fw3g")))

(define-public crate-smeagol-0.1.2 (c (n "smeagol") (v "0.1.2") (d (list (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "0vq0r86s6j1bhkizqs0c87g673775cg2n8scszpypxjq70nfz4rw")))

