(define-module (crates-io sm _m sm_motion_photo) #:use-module (crates-io))

(define-public crate-sm_motion_photo-0.1.0 (c (n "sm_motion_photo") (v "0.1.0") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 0)))) (h "0nc9i4yhl65jx0w95iyr02292c2yiqqq3xg12n7p23a60brnif4l")))

(define-public crate-sm_motion_photo-0.1.1 (c (n "sm_motion_photo") (v "0.1.1") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 0)))) (h "1p8snjgjnai125j5g4a2f252ar1x46cj2m6qmswh568c85ndmi6v")))

(define-public crate-sm_motion_photo-0.1.2 (c (n "sm_motion_photo") (v "0.1.2") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 0)))) (h "07lnzxbp5ay05d0rj635fdi7xrygnayym99danli4igln04nalaa")))

(define-public crate-sm_motion_photo-0.1.3 (c (n "sm_motion_photo") (v "0.1.3") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 0)))) (h "04m0rikvk9ysbg0bl8hyx4171gr7b0d7xv83j32rm92gp79pixqm")))

(define-public crate-sm_motion_photo-0.1.4 (c (n "sm_motion_photo") (v "0.1.4") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "^0.11.4") (d #t) (k 0)))) (h "15bwx08f1r788j80bzbm2kijbams7ly3f7b9zrj8zpsfq1812b9n")))

(define-public crate-sm_motion_photo-0.1.5 (c (n "sm_motion_photo") (v "0.1.5") (d (list (d (n "boyer-moore-magiclen") (r "^0.2.11") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "mp4parse") (r "= 0.11.2") (d #t) (k 0)))) (h "0rfkdc0z8nc9wjmj5d7l5wq308wvyln8sv6llfxs9kdqxnjnl029")))

