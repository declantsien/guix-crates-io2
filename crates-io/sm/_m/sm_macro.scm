(define-module (crates-io sm _m sm_macro) #:use-module (crates-io))

(define-public crate-sm_macro-0.6.1 (c (n "sm_macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0x8dy69g4hi7grhphw6n70b44v82dfjpzhy7flypnvsd7lk151s2")))

(define-public crate-sm_macro-0.7.0 (c (n "sm_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1x5rr3af31qdfrs2nrpr3gjbv496lzvajr7346954bhr9r7sdq37")))

(define-public crate-sm_macro-0.9.0 (c (n "sm_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qg8kvqfflc55cnchwvjrscrx6lj152z16lb9lp4adbl0v4mn95s")))

