(define-module (crates-io sm ol smoljson) #:use-module (crates-io))

(define-public crate-smoljson-0.1.0 (c (n "smoljson") (v "0.1.0") (h "1vps2b836mymsc4hrq5q1bmd787hlb91aymb5ikqnp2s0f1bkfq7")))

(define-public crate-smoljson-0.1.1 (c (n "smoljson") (v "0.1.1") (h "0i9ridgk4v80dn0lbf6n54d6fs259hbvrzq2ijjmyl9dlc5v7h3p") (f (quote (("default_allow_comments"))))))

