(define-module (crates-io sm ol smolhttp) #:use-module (crates-io))

(define-public crate-smolhttp-1.0.0 (c (n "smolhttp") (v "1.0.0") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "08l6ddgz8j1332ggwwf30kmjs11ik9px0dll71dsby4wgvq9ldvp")))

(define-public crate-smolhttp-1.1.0 (c (n "smolhttp") (v "1.1.0") (d (list (d (n "minihttpse") (r "^0.1.6") (d #t) (k 0)) (d (n "miniurl") (r "^0.1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "05yg1i22nsng16wzqp776jdb88gfx5a9zf3x2l4f6nm4bfcir3z1")))

