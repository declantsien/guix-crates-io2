(define-module (crates-io sm ol smol-macros) #:use-module (crates-io))

(define-public crate-smol-macros-0.1.0 (c (n "smol-macros") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.6.0") (d #t) (k 0)) (d (n "async-io") (r "^2.2.0") (d #t) (k 0)) (d (n "async-lock") (r "^3.1.2") (d #t) (k 0)) (d (n "async-lock") (r "^3.1.2") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.1") (k 0)) (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "unsend") (r "^0.2.1") (f (quote ("alloc"))) (k 2)))) (h "1f2npx1kn42sqn6sndicc7wpcbd1czc6zw06qqhxgpyxmzm4zwrz") (r "1.63")))

(define-public crate-smol-macros-0.1.1 (c (n "smol-macros") (v "0.1.1") (d (list (d (n "async-executor") (r "^1.6.0") (d #t) (k 0)) (d (n "async-io") (r "^2.2.0") (d #t) (k 0)) (d (n "async-lock") (r "^3.1.2") (d #t) (k 0)) (d (n "async-lock") (r "^3.1.2") (d #t) (k 2)) (d (n "event-listener") (r "^5.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.1") (k 0)) (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "unsend") (r "^0.2.1") (f (quote ("alloc"))) (k 2)))) (h "1hmvkxjd0nbv5jqjp9v7c48ms7kvxjaqj4w8i64scx845svfvjmz") (r "1.63")))

