(define-module (crates-io sm ol smolheed) #:use-module (crates-io))

(define-public crate-smolheed-0.1.0 (c (n "smolheed") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "lmdb-rkv-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "synchronoise") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1s1i2xyg3rkkjaaslxa63y44afcj75bzbcvmnx1kni71mf7ayzvh") (f (quote (("sync-read-txn") ("default"))))))

