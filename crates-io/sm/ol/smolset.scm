(define-module (crates-io sm ol smolset) #:use-module (crates-io))

(define-public crate-smolset-1.0.0 (c (n "smolset") (v "1.0.0") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "0fph24f5pc397jlqkqw8dipryi5czpc8b86szy68xzba7wdgqq19")))

(define-public crate-smolset-1.0.1 (c (n "smolset") (v "1.0.1") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "0zdwvy7c3hxzrs6f99jbbjsdz8h0hbdsvk4c646b9b5w00rp2cpr")))

(define-public crate-smolset-1.1.0 (c (n "smolset") (v "1.1.0") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "1vk2bwq0b4wzzbh8ppzpzfx8vwsznqccxri9kys406h2nd13b9zm")))

(define-public crate-smolset-1.1.1 (c (n "smolset") (v "1.1.1") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "0c9zywi11v2d608jcvxzvq6q4gkfps3k2f46ffhw7map3ianysi1") (y #t)))

(define-public crate-smolset-1.2.0 (c (n "smolset") (v "1.2.0") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "1yj5f2ax15j94agbhalsvbpr28q149d3zgvdbsjsmjc1sd0xcv08")))

(define-public crate-smolset-1.3.0 (c (n "smolset") (v "1.3.0") (d (list (d (n "smallvec") (r "^1.4.2") (d #t) (k 0)))) (h "07s3ciw31niygpyhhf9bby2wq14if8ddm6glnr33z20b80fi8q22")))

(define-public crate-smolset-1.3.1 (c (n "smolset") (v "1.3.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1q3vkj3k5qb38bbdky94kvkqb4w421c5zinnwwlm5p0mzvl75lx8")))

