(define-module (crates-io sm ol smol-mpc) #:use-module (crates-io))

(define-public crate-smol-mpc-0.1.0 (c (n "smol-mpc") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y3jzy7rpj1fjkb7p2dzz0nf667h0b3vr5wmlbbnqhzx5garf3db")))

(define-public crate-smol-mpc-0.1.1 (c (n "smol-mpc") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1q9gw5qv7989w47yj5v42m4l6dk0lbwwpnkr38r720l8kdaylq6v")))

