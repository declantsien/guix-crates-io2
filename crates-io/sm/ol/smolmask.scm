(define-module (crates-io sm ol smolmask) #:use-module (crates-io))

(define-public crate-smolmask-0.1.0 (c (n "smolmask") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1qjjzsjbj77d2025rlm75250bx8f13vvd3wz80lr7db3vaajbigb")))

(define-public crate-smolmask-0.1.1 (c (n "smolmask") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0ylb9xmc9dvxfmih8w4ng1a3sav4vnnaasddd3sycjhc5fawpbyz")))

