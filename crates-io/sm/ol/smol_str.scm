(define-module (crates-io sm ol smol_str) #:use-module (crates-io))

(define-public crate-smol_str-0.1.0 (c (n "smol_str") (v "0.1.0") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "1rgiyzhqc0azh1yg0pwhbqk80brkn6b4jc07scfhg5wmd2jwg7ds")))

(define-public crate-smol_str-0.1.1 (c (n "smol_str") (v "0.1.1") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "0644d7pbmvxkyp9nfmfl18zbwp124raywfl5qnq8gm8s4izkgjw8")))

(define-public crate-smol_str-0.1.2 (c (n "smol_str") (v "0.1.2") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "17mkk0p6nv4xzjnd9z9bsb2ssggn90sikdkyw9x5pwj90f2jwl8a")))

(define-public crate-smol_str-0.1.3 (c (n "smol_str") (v "0.1.3") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "01mms25pg2ri3hyh6xn6iga4q6lf232cmpfyyn89cl2rnd0nsrm4")))

(define-public crate-smol_str-0.1.4 (c (n "smol_str") (v "0.1.4") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "0ca4mixjzs9k2mqghgdwd5bv9rh58kr5cmafsijdvwppyslj4wvb")))

(define-public crate-smol_str-0.1.5 (c (n "smol_str") (v "0.1.5") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l4pj7pbii02hfv0wxg3mzrgr92a1s05ry2a5z4lys34w0firywg")))

(define-public crate-smol_str-0.1.6 (c (n "smol_str") (v "0.1.6") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1fa4brrq3m74j6072c366350q38rlmwabs28f3pghlsc3za5b014")))

(define-public crate-smol_str-0.1.7 (c (n "smol_str") (v "0.1.7") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "034dmh4qyi42m4rqvh2q5dnaigidirgqsibfj9s6bmq0p0cnzvgk")))

(define-public crate-smol_str-0.1.8 (c (n "smol_str") (v "0.1.8") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "10g1fl8sdgnqf4i2jwp2zfqxr2kjzh87x8gp10w3flzwp7lp8sj8")))

(define-public crate-smol_str-0.1.9 (c (n "smol_str") (v "0.1.9") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0zh0a9hd3clnsqyg9xklnj6fgzy23kj16rxig9msnhnpqmdh7wcs")))

(define-public crate-smol_str-0.1.10 (c (n "smol_str") (v "0.1.10") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1vyadm2fcn4vfcvl3bjibamq03af8i681yqjw3icds8if8vb6xyh")))

(define-public crate-smol_str-0.1.11 (c (n "smol_str") (v "0.1.11") (d (list (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0agsz7df4812d9423ab6vw4j3kciamh8gddaiahcmpx8i80psl76")))

(define-public crate-smol_str-0.1.12 (c (n "smol_str") (v "0.1.12") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yqlk7vkhksgsibl1n3c0zbnliw1k2ihwg67z1b7qi9h6sz001sr")))

(define-public crate-smol_str-0.1.13 (c (n "smol_str") (v "0.1.13") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15p58a0y2sy2fyqhrw8lw13w74836b1gr6qw6ny6p7psl3m8d0ip")))

(define-public crate-smol_str-0.1.14 (c (n "smol_str") (v "0.1.14") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mnmvqbas9j8s2635i4076dp4bws3ylwz2jvbaj137lrjfbp5jwf")))

(define-public crate-smol_str-0.1.15 (c (n "smol_str") (v "0.1.15") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12sddibm004ivnpjl6s2x28rh9nbqlbi2iql6p7c4qjw56d6r0rl")))

(define-public crate-smol_str-0.1.16 (c (n "smol_str") (v "0.1.16") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1c9djz11js99cakcqz0msd7fm85x27f4zn144636l5mwv2hhjy9g")))

(define-public crate-smol_str-0.1.17 (c (n "smol_str") (v "0.1.17") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1srj7gd14cllfwh55jwmid425rz2idpvbw7ly08448r97b7gg83c")))

(define-public crate-smol_str-0.1.18 (c (n "smol_str") (v "0.1.18") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1c53aiylg3h1b4wfn2xbv8md8cqmf2pwg5qw5hkr8mchj2gff0xj")))

(define-public crate-smol_str-0.1.19 (c (n "smol_str") (v "0.1.19") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jhadrfg52chrr3fk8biak5m5blp9gxgz65vp5ixlk1avx3g372g") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-smol_str-0.1.20 (c (n "smol_str") (v "0.1.20") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "05gfqcapr3i3cip1rbmkq76hxj99skbj43hc7f9n2h7ca8s1g6sm") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-smol_str-0.1.21 (c (n "smol_str") (v "0.1.21") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1gb14a85k6mzpn6s78flwvfl5vy1czsrzlwcgidy7k00wf1mrlb1") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-smol_str-0.1.22 (c (n "smol_str") (v "0.1.22") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1lnr3vqla1hs0kdrz73qmwab7mhk3grcijffjd448y90q60y2zhn") (f (quote (("default" "std")))) (y #t) (s 2) (e (quote (("std" "serde?/std"))))))

(define-public crate-smol_str-0.1.23 (c (n "smol_str") (v "0.1.23") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0i5b6mm2hbmvyvch3rhfx6bfl9jmijx320ffazhs5qxp52512xbl") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-smol_str-0.1.24 (c (n "smol_str") (v "0.1.24") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1j891lgnflvnzgbs7fhwd6sxrrx47ii5mj0yy3f2f9mbrdbwimps") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-smol_str-0.1.25 (c (n "smol_str") (v "0.1.25") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "10mndmcljjrqk96ndy7hffccjglaxkv4l8bd8sn2r4rbi4j8s9lx") (f (quote (("default" "std")))) (y #t) (s 2) (e (quote (("std" "serde?/std"))))))

(define-public crate-smol_str-0.2.0 (c (n "smol_str") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1779hpx5ipbcvkdj5zw8zqk3ynn160qvls1gkcr54hwsprmjw8bl") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std"))))))

(define-public crate-smol_str-0.2.1 (c (n "smol_str") (v "0.2.1") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0jca0hyrwnv428q5gxhn2s8jsvrrkyrb0fyla9x37056mmimb176") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std"))))))

(define-public crate-smol_str-0.2.2 (c (n "smol_str") (v "0.2.2") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1bfylqf2vnqaglw58930vpxm2rfzji5gjp15a2c0kh8aj6v8ylyx") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std"))))))

