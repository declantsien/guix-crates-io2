(define-module (crates-io sm ol smoldb) #:use-module (crates-io))

(define-public crate-smoldb-0.1.0 (c (n "smoldb") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smoldb_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "smoldb_traits") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "16hwh1p7d4il5p1v00v929fdgdkvgr0sxy26cmpdk8l047i60fkx")))

