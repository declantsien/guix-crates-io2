(define-module (crates-io sm ol smol-attributes) #:use-module (crates-io))

(define-public crate-smol-attributes-0.1.0 (c (n "smol-attributes") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4gmvs9ijv9h336bhf26xxb3pmrfc5xh2nnw3cy91f7zsm8m4wy")))

(define-public crate-smol-attributes-0.1.3 (c (n "smol-attributes") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13l5g4706y31b3azrc68p8cjmdaz2grcq668zvsh236mgr9bkby9")))

