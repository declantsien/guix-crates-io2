(define-module (crates-io sm ol smolder-tests) #:use-module (crates-io))

(define-public crate-smolder-tests-0.1.0 (c (n "smolder-tests") (v "0.1.0") (h "166an5088xczjb49wqpqa4zds5a0i64vjdr62q10rd96429liywa")))

(define-public crate-smolder-tests-0.2.0 (c (n "smolder-tests") (v "0.2.0") (h "01b5gm88gjb13p21ddaz764y7v0nsxwbsan2i0mimgdw1c3s1xgl")))

(define-public crate-smolder-tests-0.2.1 (c (n "smolder-tests") (v "0.2.1") (h "0lqyw7b9vhxzc4vj7jjpnsb5c0ysx6k090a7s13qx3szgmmkgx73")))

