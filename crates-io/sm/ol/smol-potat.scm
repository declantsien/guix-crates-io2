(define-module (crates-io sm ol smol-potat) #:use-module (crates-io))

(define-public crate-smol-potat-0.1.0 (c (n "smol-potat") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07i1gpmbfdfa8p2ani9cw3wp68ngf6z2ywnp53lxzvd7k9dql4g2")))

(define-public crate-smol-potat-0.1.1 (c (n "smol-potat") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8qliw8amf2s7p4ah7dvl8yr9vcqdh19wxbappbnmjnwf3r3bsm")))

(define-public crate-smol-potat-0.1.2 (c (n "smol-potat") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "150qclriyvlwjz350nfxigqknard1a6jyif83a4cc2nmmn0plfyp")))

(define-public crate-smol-potat-0.1.3 (c (n "smol-potat") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10awq2zkcg1mzckxxlpnnc6l8dybh5adnrcfi5k2dnvipbshqhvz")))

(define-public crate-smol-potat-0.2.0 (c (n "smol-potat") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pyacjrh67dkzm4l9pgwi64mgzhq90bi52mki4khbf1k86z49q4k")))

(define-public crate-smol-potat-0.2.1 (c (n "smol-potat") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vq5ql0l7ffrgrw2mkfin1qkcs0fdrpi1ivbdlcwvcqplm4pvbzm")))

(define-public crate-smol-potat-0.3.0 (c (n "smol-potat") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "smol") (r "^0.1.8") (d #t) (k 0)) (d (n "smol-potat-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0q9bqj6wjh25xx04whj9fzbgzg99q84dmmz3r0cv7d19kczms1a4") (f (quote (("auto" "smol-potat-derive/auto" "num_cpus"))))))

(define-public crate-smol-potat-0.3.1 (c (n "smol-potat") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "smol") (r "= 0.1.7") (d #t) (k 0)) (d (n "smol-potat-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0j53z1qaibgj0lvwwpzjv0yj7sgvzh7q653484cg57pldkang7i8") (f (quote (("auto" "smol-potat-derive/auto" "num_cpus"))))))

(define-public crate-smol-potat-0.3.2 (c (n "smol-potat") (v "0.3.2") (d (list (d (n "num_cpus") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "smol") (r "^0.1.9") (d #t) (k 0)) (d (n "smol-potat-derive") (r "^0.1.0") (d #t) (k 0)))) (h "06b37xkqwfl13wcnjg7xdl7cxzbyhzl4ng9rh278iz7l9znpy19b") (f (quote (("auto" "smol-potat-derive/auto" "num_cpus"))))))

(define-public crate-smol-potat-0.3.3 (c (n "smol-potat") (v "0.3.3") (d (list (d (n "num_cpus") (r "^1.13.0") (o #t) (d #t) (k 0)) (d (n "smol") (r "^0.1.9") (d #t) (k 0)) (d (n "smol-potat-derive") (r "^0.1.0") (d #t) (k 0)))) (h "02lrqikdgnprad314sygdfynbhyk3ykwvhda4cfsb8dl8dw6g2w8") (f (quote (("auto" "smol-potat-derive/auto" "num_cpus"))))))

(define-public crate-smol-potat-0.4.0 (c (n "smol-potat") (v "0.4.0") (d (list (d (n "async-io") (r "^0.1.4") (d #t) (k 0)) (d (n "async-net") (r "^0.1.1") (d #t) (k 0)) (d (n "blocking") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.6") (d #t) (k 0)) (d (n "multitask") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "smol") (r "^0.2.0") (d #t) (k 0)) (d (n "smol-potat-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0s4318g7pmwg8nk9qkd2miy94zd4fjvdx53rv51xkg78rniz9g53")))

(define-public crate-smol-potat-0.5.0 (c (n "smol-potat") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 0)) (d (n "smol-potat-macro") (r "^0.3") (d #t) (k 0)))) (h "1dvmsd3wvvgbc4lz6na70vzxl66nfhsxa6rfjaw013hb837ld9gr")))

(define-public crate-smol-potat-1.1.0 (c (n "smol-potat") (v "1.1.0") (d (list (d (n "async-io") (r "^1.1.10") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 2)) (d (n "smol-potat-macro") (r "^0.5") (d #t) (k 0)))) (h "0i0h3h3v12x3g5rjhhysl10k964hrg3k0xdl1m404mar6cikkfrh") (f (quote (("auto" "smol-potat-macro/auto" "num_cpus"))))))

(define-public crate-smol-potat-1.1.1 (c (n "smol-potat") (v "1.1.1") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "smol-potat-macro") (r "^0.5") (d #t) (k 0)))) (h "047kq0i3bd9s6ffvfnlafz9yqfhz0dyxq1sbspnjq3h8qj407cq8") (f (quote (("auto" "smol-potat-macro/auto" "num_cpus"))))))

(define-public crate-smol-potat-1.1.2 (c (n "smol-potat") (v "1.1.2") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "smol-potat-macro") (r "^0.6") (d #t) (k 0)))) (h "13nqzzqjscav3flc9jhwiabw8vnb22mv2accgilsn3swmxhzlkw9") (f (quote (("auto" "smol-potat-macro/auto" "num_cpus"))))))

