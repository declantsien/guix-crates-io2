(define-module (crates-io sm ol smol_buf) #:use-module (crates-io))

(define-public crate-smol_buf-0.1.0 (c (n "smol_buf") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1galqycg1pg4v6pj9n8gw0sg5din85kc99n10wyzwlr4s4dbjvkn") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std"))))))

