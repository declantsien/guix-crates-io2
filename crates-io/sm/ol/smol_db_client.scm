(define-module (crates-io sm ol smol_db_client) #:use-module (crates-io))

(define-public crate-smol_db_client-1.0.0 (c (n "smol_db_client") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.0.0") (d #t) (k 0)))) (h "10cammbjdky2956m5rz6yb8zf7mx0mbm2l2d8fxyj7i9g436ps18")))

(define-public crate-smol_db_client-1.1.0 (c (n "smol_db_client") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.0.0") (d #t) (k 0)))) (h "0r2fz2563swxnvs2k42j1d6rawywayxvr96qijld5ih4f7ga5vb9")))

(define-public crate-smol_db_client-1.1.1 (c (n "smol_db_client") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.1.0") (d #t) (k 0)))) (h "01ix3s3szm9zxdqswc0sfz77ljwxcy7329mcd5wjrk36zggb9r07")))

(define-public crate-smol_db_client-1.2.0 (c (n "smol_db_client") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.2.0") (d #t) (k 0)))) (h "1x6nc00f1qssmsmvhx3q78zkvh1n8njv6cvfdwbzd0sn49b5g2kd") (f (quote (("statistics" "smol_db_common/statistics"))))))

(define-public crate-smol_db_client-1.2.1 (c (n "smol_db_client") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.2.0") (d #t) (k 0)))) (h "1cjw4mlm9c4y1ap4rzhzvhirxvb032fx2k7dxv32s5vbh34hns6v") (f (quote (("statistics" "smol_db_common/statistics"))))))

(define-public crate-smol_db_client-1.3.0 (c (n "smol_db_client") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.2.0") (d #t) (k 0)))) (h "0xb5x7hc7ykirjlc7dwp2crkvgd17hns4qvfb55vc7n0baagv0g0") (f (quote (("statistics" "smol_db_common/statistics"))))))

(define-public crate-smol_db_client-1.3.1 (c (n "smol_db_client") (v "1.3.1") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.2.0") (d #t) (k 0)))) (h "0rwyn3pg4n4whab9y586jb7mhhfbkfcybbakz7qz7yv70abv6q9d") (f (quote (("statistics" "smol_db_common/statistics"))))))

(define-public crate-smol_db_client-1.4.0 (c (n "smol_db_client") (v "1.4.0") (d (list (d (n "serde") (r "^1.0.177") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("test-util" "full"))) (d #t) (k 2)))) (h "0wfb0k2qn5xsvxxd9x20x0s5pj6bvn54rfydwf357bzw18s8rlgx") (f (quote (("statistics" "smol_db_common/statistics") ("async"))))))

(define-public crate-smol_db_client-1.5.0-beta.0 (c (n "smol_db_client") (v "1.5.0-beta.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.5.0-beta.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("test-util" "full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (d #t) (k 0)))) (h "0hzizv8pz34jjl12b38036np6wmsklipjr45z6ahgap881gr5c8y") (f (quote (("statistics" "smol_db_common/statistics") ("async"))))))

(define-public crate-smol_db_client-1.5.0-beta.1 (c (n "smol_db_client") (v "1.5.0-beta.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smol_db_common") (r "^1.5.0-beta.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (d #t) (k 0)))) (h "1a73f7grkrmck9ignz6g41fmh2kcqrrzziwvvbz7ar7rd23hz45h") (f (quote (("statistics" "smol_db_common/statistics"))))))

