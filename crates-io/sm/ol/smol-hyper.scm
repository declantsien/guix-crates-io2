(define-module (crates-io sm ol smol-hyper) #:use-module (crates-io))

(define-public crate-smol-hyper-0.1.0 (c (n "smol-hyper") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "async-io") (r "^2.2.2") (o #t) (d #t) (k 0)) (d (n "futures-io") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "hyper") (r "^1.1.0") (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "0jb3apcai84svpd9i84s2mja8f1pi464riyssscgpzw1kd2z9av7") (f (quote (("smol" "async-executor" "async-io") ("default" "smol")))) (r "1.63")))

(define-public crate-smol-hyper-0.1.1 (c (n "smol-hyper") (v "0.1.1") (d (list (d (n "async-executor") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "async-io") (r "^2.2.2") (o #t) (d #t) (k 0)) (d (n "futures-io") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "hyper") (r "^1.1.0") (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "06mw449jf0gi25wi2j9vq6gg6101df5b15rbs4n70rrq6afs8a3l") (f (quote (("smol" "async-executor" "async-io") ("default" "smol")))) (r "1.63")))

