(define-module (crates-io sm ol smolprng) #:use-module (crates-io))

(define-public crate-smolprng-0.1.0 (c (n "smolprng") (v "0.1.0") (h "1lhbvkdsp8aszlldwyg2dmn01aysjmmxsczrrsr6snfqf1bvl6xl")))

(define-public crate-smolprng-0.1.1 (c (n "smolprng") (v "0.1.1") (h "123mdkbsxgxzv5sgak60kwimkif05jnngia6pmhyj8njcxpn2pfr")))

(define-public crate-smolprng-0.1.2 (c (n "smolprng") (v "0.1.2") (h "0xgsjccixcawvc2r04pip8i96mpxaxljccv7r1mwarzcaxrnjryh")))

(define-public crate-smolprng-0.1.3 (c (n "smolprng") (v "0.1.3") (h "1mz29zdivvph4gadak8mlswpg4nb6a03m7vqvqnh27mp5lnqi8wd")))

(define-public crate-smolprng-0.1.4 (c (n "smolprng") (v "0.1.4") (h "0pabpsra44lawqiwsrdzg14i4k2jw9f56zmz86rm7564bqjbgckx")))

(define-public crate-smolprng-0.1.5 (c (n "smolprng") (v "0.1.5") (h "11iypmwkkklp0p04pr33njif7l1h5y4szsdzz7lr7y91k18bwziz")))

(define-public crate-smolprng-0.1.6 (c (n "smolprng") (v "0.1.6") (d (list (d (n "alloc_counter") (r "^0.0.4") (d #t) (k 2)))) (h "16bfamg2mp4m254ms4mi8rdpwrfhynx4kkavh8c7amscapj832gx") (f (quote (("std") ("no_std"))))))

