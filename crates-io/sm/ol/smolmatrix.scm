(define-module (crates-io sm ol smolmatrix) #:use-module (crates-io))

(define-public crate-smolmatrix-0.1.0 (c (n "smolmatrix") (v "0.1.0") (h "1fkky5q3zh8fq3ssyp234l503l4y8nymvr9nys10rv7jgn7j9dbq")))

(define-public crate-smolmatrix-0.1.1 (c (n "smolmatrix") (v "0.1.1") (h "1vqbpd93j09kx2x2jnn7wzdnyzgawmx9pvfbsa42whs7pdkvy4xi")))

(define-public crate-smolmatrix-0.1.2 (c (n "smolmatrix") (v "0.1.2") (h "0ym7chlizxrnlg75w11qyx3nbh5lbqi0cx39zqq25n89f0n6s04f")))

(define-public crate-smolmatrix-0.1.3 (c (n "smolmatrix") (v "0.1.3") (h "0659jbzkil1fh9ngchrdpspw7wbsgcq5rsvh1s9p9kny755230v7")))

(define-public crate-smolmatrix-0.1.4 (c (n "smolmatrix") (v "0.1.4") (h "1g2rxfxg1pci7gal6viq9z3sxgrx7zjnq7y1y7r5d6wipjfawl9b")))

(define-public crate-smolmatrix-0.1.5 (c (n "smolmatrix") (v "0.1.5") (h "0azknxqjv64h49sa1gcv1s3nijw2isbmq8b0344nwr7y09hl99a9") (f (quote (("std") ("default" "std"))))))

(define-public crate-smolmatrix-0.1.6 (c (n "smolmatrix") (v "0.1.6") (h "0y02775hffvl2syqppw17x0562z9giyf03zd7q22n532ccg264ya") (f (quote (("std") ("default" "std"))))))

