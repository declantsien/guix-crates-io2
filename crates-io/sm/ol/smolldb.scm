(define-module (crates-io sm ol smolldb) #:use-module (crates-io))

(define-public crate-smolldb-0.1.0 (c (n "smolldb") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "yazi") (r "^0.1.4") (d #t) (k 0)))) (h "0fj3va96hbrlj4hpblyl2k72g87fjb5xwqlw0hpn7nhjrh861f1m") (r "1.65.0")))

(define-public crate-smolldb-0.2.0 (c (n "smolldb") (v "0.2.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "yazi") (r "^0.1.4") (d #t) (k 0)))) (h "0qm4k7d3bkmmykrwimxzxmmpp2qvmnp2gnrsrd32jw0qk3bxp0jb") (r "1.65.0")))

(define-public crate-smolldb-0.3.0 (c (n "smolldb") (v "0.3.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "yazi") (r "^0.1.4") (d #t) (k 0)))) (h "1hpp6j3vaqrjgngswsl396l6637zi34y18ikjvj4wiix0v5fkd9g") (r "1.65.0")))

(define-public crate-smolldb-0.4.0 (c (n "smolldb") (v "0.4.0") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "yazi") (r "^0.1.4") (d #t) (k 0)))) (h "1hd4x06czjjlw49s7hx00bxhp8r3jz2675y0jbq7m9ckb39g8a5v") (y #t) (r "1.65.0")))

(define-public crate-smolldb-0.4.1 (c (n "smolldb") (v "0.4.1") (d (list (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "yazi") (r "^0.1.4") (d #t) (k 0)))) (h "15690n3cnvgc2i602krrz9mn24zzfrn169yvkjvjp56ad2riwxnp") (r "1.65.0")))

(define-public crate-smolldb-0.4.2 (c (n "smolldb") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "yazi") (r "^0.1.6") (d #t) (k 0)))) (h "05sjm927fh0zwdlva65vbc339b8dknsm35776k9gxx6r4aw1r2wl") (r "1.65.0")))

