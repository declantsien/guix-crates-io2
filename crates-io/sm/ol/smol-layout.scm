(define-module (crates-io sm ol smol-layout) #:use-module (crates-io))

(define-public crate-smol-layout-0.1.0 (c (n "smol-layout") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sy734hpxxqwxx640v11cb6p770acqhs1h6k8ymnlzsb0mwvgdmm")))

(define-public crate-smol-layout-0.1.1 (c (n "smol-layout") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1fafrzg7xaqb894ivbm001jdilwv1g7jz3q2npqngjzsvjyazdly")))

