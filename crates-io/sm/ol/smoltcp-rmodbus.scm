(define-module (crates-io sm ol smoltcp-rmodbus) #:use-module (crates-io))

(define-public crate-smoltcp-rmodbus-0.1.0 (c (n "smoltcp-rmodbus") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "rmodbus") (r "^0.9.3") (f (quote ("heapless"))) (k 0)) (d (n "smoltcp") (r "^0.10.0") (f (quote ("socket-tcp" "proto-ipv4" "medium-ethernet"))) (k 0)))) (h "01gf46mpw724i75w3i9cj3la9g693f22b55k2i4av43b3iggs698") (s 2) (e (quote (("defmt-03" "dep:defmt" "smoltcp/defmt" "heapless/defmt-03" "rmodbus/defmt"))))))

