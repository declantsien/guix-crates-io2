(define-module (crates-io sm ol smol-jsonrpc) #:use-module (crates-io))

(define-public crate-smol-jsonrpc-0.1.0 (c (n "smol-jsonrpc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "055k7x38ava6dj1kczja4l47bi3smx6r5rasb2s3y5dwzkxxp2ym") (f (quote (("std" "serde/std" "serde_json/std"))))))

(define-public crate-smol-jsonrpc-0.1.1 (c (n "smol-jsonrpc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "03clkijjzrga0fsjgpi70sziahh1z0hyp4jivmfwvscx9z2dpngr") (f (quote (("std" "serde/std" "serde_json/std"))))))

(define-public crate-smol-jsonrpc-0.2.0 (c (n "smol-jsonrpc") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "09lzqdva1brx7bdz3gjb5f98g0210rl34fy6pwp3i8dbqi11cf3k") (f (quote (("std" "serde/std" "serde_json/std"))))))

