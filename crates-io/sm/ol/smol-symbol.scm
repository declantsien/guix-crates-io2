(define-module (crates-io sm ol smol-symbol) #:use-module (crates-io))

(define-public crate-smol-symbol-0.1.5 (c (n "smol-symbol") (v "0.1.5") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "smol-symbol-macros") (r "^0.1.5") (d #t) (k 0)))) (h "0ppwg5vfs6ch927mg9nbpcm8l6idacv5izqp2kidyd46r12vs4xi") (f (quote (("generate-readme") ("default"))))))

(define-public crate-smol-symbol-0.1.6 (c (n "smol-symbol") (v "0.1.6") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "smol-symbol-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0z5nr1qzl42kr5vy01i6bx2gcsbh7mdnrpsgkmnm2zv4aix1x14b") (f (quote (("generate-readme") ("default"))))))

(define-public crate-smol-symbol-0.1.7 (c (n "smol-symbol") (v "0.1.7") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "smol-symbol-macros") (r "^0.1.7") (d #t) (k 0)))) (h "0d4aby8103myhhha21kk8nhgmp5s2pziwlb1virmpxcaxkszxyd0") (f (quote (("generate-readme") ("default"))))))

