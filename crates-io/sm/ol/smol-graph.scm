(define-module (crates-io sm ol smol-graph) #:use-module (crates-io))

(define-public crate-smol-graph-0.1.0 (c (n "smol-graph") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hjbpqy811li9x3k3vwz8yh3wi4b9wg45zhvmbj54i9s2bicv288")))

(define-public crate-smol-graph-0.1.1 (c (n "smol-graph") (v "0.1.1") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qkh2ipzbrysfrak6n3779f3il0lgqx2016ni6pws8xq2h2j6dbi")))

(define-public crate-smol-graph-0.1.2 (c (n "smol-graph") (v "0.1.2") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ghm0wh5xwixfp7cw7nnx0zk4y58r9wa1kn62sva0ym57ig677ax")))

(define-public crate-smol-graph-0.2.0 (c (n "smol-graph") (v "0.2.0") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0blvs07ngz60rsa9jdn8kk3q8a6awfqjjrhbrfjn7w6x5yk20x4m")))

(define-public crate-smol-graph-0.2.1 (c (n "smol-graph") (v "0.2.1") (d (list (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bib4klwgv1ijvhvhw2qi5sj3kpglbg0inim5nppix4qacbcf28c")))

