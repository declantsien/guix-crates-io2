(define-module (crates-io sm ol smoldb_derive) #:use-module (crates-io))

(define-public crate-smoldb_derive-0.1.0 (c (n "smoldb_derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "smoldb_traits") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("extra-traits" "clone-impls"))) (d #t) (k 0)))) (h "1vyww1zkc4gc696i8vjwkngiax782z5kmr8a0qspkx7zxvgy507c")))

