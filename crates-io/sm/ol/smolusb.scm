(define-module (crates-io sm ol smolusb) #:use-module (crates-io))

(define-public crate-smolusb-0.0.0 (c (n "smolusb") (v "0.0.0") (d (list (d (n "log") (r "=0.4.17") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (k 0)))) (h "13lmn17sx79xcw74c5cpwakbdm2zrwsfa7arkfk3bxq12i6vxw2x") (f (quote (("nightly") ("default") ("chonky_events")))) (y #t) (r "1.68")))

(define-public crate-smolusb-0.0.1 (c (n "smolusb") (v "0.0.1") (d (list (d (n "log") (r "=0.4.17") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (k 0)))) (h "1228cz6d3hfp3gcphwz2rql5whvrcl1q73w0i0am9rvlyhmlprjb") (f (quote (("nightly") ("default") ("chonky_events")))) (r "1.68")))

