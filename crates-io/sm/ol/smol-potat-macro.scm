(define-module (crates-io sm ol smol-potat-macro) #:use-module (crates-io))

(define-public crate-smol-potat-macro-0.2.0 (c (n "smol-potat-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lx3rl8ja6zdwj229vrjykb6rfwmb3d704s6viybwjqw48fn79yv") (f (quote (("tokio02"))))))

(define-public crate-smol-potat-macro-0.3.0 (c (n "smol-potat-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cyks28p6dwg3f7wrx50xqkqpci42n4c5xqphy0jzf0p9fff8lww") (f (quote (("tokio02"))))))

(define-public crate-smol-potat-macro-0.5.0 (c (n "smol-potat-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w80pmzqq6wwlx781nfdi9chw3yh92l6rz5hkkgcfg6l1j28n5mf") (f (quote (("auto"))))))

(define-public crate-smol-potat-macro-0.6.0 (c (n "smol-potat-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cirpy1309cr3n6zbmia66miyidih88sinpanj2r61hqk89dhz3b") (f (quote (("auto"))))))

