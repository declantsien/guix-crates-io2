(define-module (crates-io sm ol smol-timeout) #:use-module (crates-io))

(define-public crate-smol-timeout-0.1.0 (c (n "smol-timeout") (v "0.1.0") (d (list (d (n "pin-project") (r "~0.4") (d #t) (k 0)) (d (n "smol") (r "~0.1") (d #t) (k 0)))) (h "14mk3paa59fprg4n1vdclcx2jd01hqryxz6w3lbyi00by30ihj02")))

(define-public crate-smol-timeout-0.2.0 (c (n "smol-timeout") (v "0.2.0") (d (list (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 0)))) (h "1pj19n1rg69lj2sb7nfkfqng7w9i36m0p0cl6xwln04r9fds4hfc")))

(define-public crate-smol-timeout-0.3.0 (c (n "smol-timeout") (v "0.3.0") (d (list (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 0)))) (h "19f0mr9by7q01zq242ih0w50g0bz0f5q1d20ypdzvfkic6mgv7qa")))

(define-public crate-smol-timeout-0.4.0 (c (n "smol-timeout") (v "0.4.0") (d (list (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 0)))) (h "1v49i8q3krir8sx1q6zmbwdjams1am2d6299wxx81zb3sk5mnc1g")))

(define-public crate-smol-timeout-0.5.0 (c (n "smol-timeout") (v "0.5.0") (d (list (d (n "async-io") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)))) (h "0mqpjzn3r8yanj1qkdl8v3zm3lzpkpknzc08sz8kqmvq00anw57r")))

(define-public crate-smol-timeout-0.6.0 (c (n "smol-timeout") (v "0.6.0") (d (list (d (n "async-io") (r "^1.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.8") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)))) (h "0qa9x6lbyyym7kia1d7wck9z684q1bl7ji164snnn5kc5iz7fzc4")))

