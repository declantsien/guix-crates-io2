(define-module (crates-io sm ol smol-base-x) #:use-module (crates-io))

(define-public crate-smol-base-x-0.1.0 (c (n "smol-base-x") (v "0.1.0") (d (list (d (n "const-str") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "match-lookup") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0sy1hsfi06kgakj69qfdwq2n2n3kkjsv33gpp33142sbs8gp0s4g") (f (quote (("unstable" "const-str" "match-lookup") ("default"))))))

