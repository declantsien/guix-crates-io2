(define-module (crates-io sm ol smoldb_traits) #:use-module (crates-io))

(define-public crate-smoldb_traits-0.1.0 (c (n "smoldb_traits") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "00pl3gvs1g69ifidyq1ysmp18srkhk7y306gksi4i74z218bfpg2")))

