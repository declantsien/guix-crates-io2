(define-module (crates-io sm ol smolsocket) #:use-module (crates-io))

(define-public crate-smolsocket-0.1.0 (c (n "smolsocket") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "smoltcp") (r "^0.6.0") (k 0)))) (h "1qcsjzwsj484gp68i0gz4993fya3d4r5qqm3qn43cl632hfkiy6r") (f (quote (("verbose") ("std" "smoltcp/std") ("proto-ipv6" "smoltcp/proto-ipv6") ("proto-ipv4" "smoltcp/proto-ipv4") ("default" "std" "proto-ipv4" "proto-ipv6"))))))

(define-public crate-smolsocket-0.1.1 (c (n "smolsocket") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "smoltcp") (r "^0.6.0") (k 0)))) (h "0cwlvmx1la3az0ddpgh9qkm9pd3i1aaikaravhzrrlyvk39mycz3") (f (quote (("verbose") ("std" "smoltcp/std") ("proto-ipv6" "smoltcp/proto-ipv6") ("proto-ipv4" "smoltcp/proto-ipv4") ("default" "std" "proto-ipv4" "proto-ipv6"))))))

(define-public crate-smolsocket-0.3.0 (c (n "smolsocket") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "smoltcp") (r "^0.6.0") (k 0)))) (h "06mngc0dg6c8pm397q3dpw6nxvrc3cf7n6ha5h5af32vi2xkiwi6") (f (quote (("verbose") ("std" "smoltcp/std") ("proto-ipv6" "smoltcp/proto-ipv6") ("proto-ipv4" "smoltcp/proto-ipv4") ("default" "std" "proto-ipv4" "proto-ipv6"))))))

