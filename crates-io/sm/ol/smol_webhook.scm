(define-module (crates-io sm ol smol_webhook) #:use-module (crates-io))

(define-public crate-smol_webhook-0.1.0 (c (n "smol_webhook") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "02302yshnca24ifhhhnfs2h8xwsaz47c1sn3r3hibrhb5rw1sm5k")))

