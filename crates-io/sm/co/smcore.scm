(define-module (crates-io sm co smcore) #:use-module (crates-io))

(define-public crate-smcore-0.1.0 (c (n "smcore") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smdton") (r "^0.1.0") (d #t) (k 0)))) (h "0r6n1gsp1drh20baksifdxshxz6v1vr8glcwjjqi3m1blw9nl8n0") (y #t)))

(define-public crate-smcore-0.1.1 (c (n "smcore") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smdton") (r "^0.1.1") (d #t) (k 0)))) (h "1hbffwd7wng8hn30837a17pd970wnvgg03863089yjvsyskbw8v2")))

(define-public crate-smcore-0.1.2 (c (n "smcore") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "smdton") (r "^0.1.2") (d #t) (k 0)))) (h "14msy5zibh35i8bkqdsvqp5v5dj7jp3aa4ywg7l9y8ypayiaqhp4")))

