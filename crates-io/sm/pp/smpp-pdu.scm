(define-module (crates-io sm pp smpp-pdu) #:use-module (crates-io))

(define-public crate-smpp-pdu-0.1.2 (c (n "smpp-pdu") (v "0.1.2") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r ">=1.0.1") (f (quote ("io-util" "macros" "rt" "sync" "test-util"))) (d #t) (k 0)))) (h "06zwwncmjabvnn75874wgxbamz7jq22iqqa3ag2b74nplg9icr8n")))

(define-public crate-smpp-pdu-0.1.3 (c (n "smpp-pdu") (v "0.1.3") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r ">=1.0.1") (f (quote ("io-util" "macros" "rt" "sync" "test-util"))) (d #t) (k 0)))) (h "1bxk4jizjr0ak5q8ycszn8x7mzh3j12shbkfxax60w631x5229z3")))

(define-public crate-smpp-pdu-0.1.4 (c (n "smpp-pdu") (v "0.1.4") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r ">=1.0.1") (f (quote ("io-util" "macros" "rt" "sync" "test-util"))) (d #t) (k 0)))) (h "1nm6l68ikj6q757z66nvg6fj79l8hyvid345zixqbyh9q27js0cx")))

