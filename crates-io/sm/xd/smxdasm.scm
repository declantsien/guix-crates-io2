(define-module (crates-io sm xd smxdasm) #:use-module (crates-io))

(define-public crate-smxdasm-0.1.0 (c (n "smxdasm") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "1xi6f29fnd2gyxisvd40cpkzdcnyp196qlivfncclzdaxjjk4y74")))

(define-public crate-smxdasm-0.2.0 (c (n "smxdasm") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "0wg91pa1vg547g39jffrrrsxlfx52ffmc7vnn06ll3r231221fz4")))

