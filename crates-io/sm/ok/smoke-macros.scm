(define-module (crates-io sm ok smoke-macros) #:use-module (crates-io))

(define-public crate-smoke-macros-0.1.0 (c (n "smoke-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "033y013xsrp3lfsa9mn1q4g1l5qi4dicvqs191wrd9kw62qqin2n")))

