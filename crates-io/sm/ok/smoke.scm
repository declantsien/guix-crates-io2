(define-module (crates-io sm ok smoke) #:use-module (crates-io))

(define-public crate-smoke-0.1.0 (c (n "smoke") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1l1l1l7xv3zjz6hff4fbq1z2234vynk8h6jnlvlvgkxdbkjdrmnp") (f (quote (("random" "getrandom") ("default" "random"))))))

(define-public crate-smoke-0.2.0 (c (n "smoke") (v "0.2.0") (h "0qh1yzz6flw4j995n0ijmlzkh1szssnx91id41mdvl6vyzx90s5v") (f (quote (("default"))))))

(define-public crate-smoke-0.2.1 (c (n "smoke") (v "0.2.1") (h "0969jdp47s1xx8ylfnw22sfbsqmzw7hwfyqvbr7sbnz4j7jjd6i8") (f (quote (("default"))))))

(define-public crate-smoke-0.3.0 (c (n "smoke") (v "0.3.0") (h "1dsqp3r5w0kcxm4ls3bff2kn1mzqpc9ci3725cydpxrasx52w4dx") (f (quote (("default"))))))

(define-public crate-smoke-0.3.1 (c (n "smoke") (v "0.3.1") (h "1nkb72mbnmrq4j1xrg0zjvisfsjxghk07vgnmwnrbnpwmhxb5gfy") (f (quote (("default"))))))

