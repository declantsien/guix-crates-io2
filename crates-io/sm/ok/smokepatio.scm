(define-module (crates-io sm ok smokepatio) #:use-module (crates-io))

(define-public crate-smokepatio-0.1.0 (c (n "smokepatio") (v "0.1.0") (d (list (d (n "embedded-io") (r "^0.6") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12fhd3wkpc39iri3x3wbvc6kfzmz8zx6j544glwvrgkqxszghvk2") (f (quote (("std" "embedded-io/std") ("async" "embedded-io-async"))))))

(define-public crate-smokepatio-0.2.0 (c (n "smokepatio") (v "0.2.0") (d (list (d (n "embedded-io") (r "^0.6") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vs1417f0s327vlhxbnpbb59gzrbqdrki4zk9lrw8wfi0z4yh5il") (f (quote (("std" "embedded-io/std") ("async" "embedded-io-async"))))))

