(define-module (crates-io sm bp smbpndk-model) #:use-module (crates-io))

(define-public crate-smbpndk-model-0.3.1 (c (n "smbpndk-model") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0k7f22014wzar8glq2p5an4hvyl5qp0rxnv463mrld4shhry64w2")))

(define-public crate-smbpndk-model-0.3.2 (c (n "smbpndk-model") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1mfld94jrcs215fwbwvdlhwyw8rvs8bjp5vbrcnz0sh1mm136qch")))

(define-public crate-smbpndk-model-0.3.3 (c (n "smbpndk-model") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "108kga4bfjq4b73jcknayf4gsh9p3mh6rvf3k4pbc19hij1623gd")))

(define-public crate-smbpndk-model-0.3.4 (c (n "smbpndk-model") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1jblry563gfz1cqf0g8zqczy4bg9cx0y9zj9ay29g00cgw8q9p3f")))

(define-public crate-smbpndk-model-0.3.5 (c (n "smbpndk-model") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0ml8i415n4sznk7jf0snpz0ysy75i6wwyh5522dh1g9qz62nrpwn")))

(define-public crate-smbpndk-model-0.3.6 (c (n "smbpndk-model") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "11wbrvwq0cv6grz4bfcb1vq30mvd4v551m73rik58vy3p4dhz8gn")))

(define-public crate-smbpndk-model-0.3.7 (c (n "smbpndk-model") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0qmahw4m128m0cls041zlx0k1lvr44qr7j150vdmng23s82bzbbx")))

