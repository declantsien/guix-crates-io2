(define-module (crates-io sm pt smptera-format-identifers-rust) #:use-module (crates-io))

(define-public crate-smptera-format-identifers-rust-0.1.0 (c (n "smptera-format-identifers-rust") (v "0.1.0") (d (list (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)))) (h "085iybmr0i0yrfbl99xch5hiiz2lqn6ndfkd6qpcx8y2l27jw64d") (y #t)))

(define-public crate-smptera-format-identifers-rust-0.2.0 (c (n "smptera-format-identifers-rust") (v "0.2.0") (d (list (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)))) (h "08z4k719b47dshxqmfdcfk21xrq96w0xrlgf1qaxa7rphq6hz0hq") (y #t)))

