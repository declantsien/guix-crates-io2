(define-module (crates-io sm pt smptera-format-identifiers-rust) #:use-module (crates-io))

(define-public crate-smptera-format-identifiers-rust-0.2.0 (c (n "smptera-format-identifiers-rust") (v "0.2.0") (d (list (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)))) (h "1ccqa596fnqaqdds5vcxzf699pz7yp0j5zmzk0ckcsx6j78b5b1g")))

(define-public crate-smptera-format-identifiers-rust-0.3.0 (c (n "smptera-format-identifiers-rust") (v "0.3.0") (d (list (d (n "four-cc") (r "^0.1.0") (d #t) (k 0)))) (h "0bkzw8dmasbwpvndv2avhsdv85s8xcnab0qzjqcdbazk938vadmq")))

(define-public crate-smptera-format-identifiers-rust-0.4.0 (c (n "smptera-format-identifiers-rust") (v "0.4.0") (d (list (d (n "four-cc") (r "^0.3.0") (d #t) (k 0)))) (h "0l27i023i5a3r1975587a0k1pjv9c8qd0sbz6n97hfmgbxan56dp")))

