(define-module (crates-io sm pt smpte2022-1-packet) #:use-module (crates-io))

(define-public crate-smpte2022-1-packet-0.1.0 (c (n "smpte2022-1-packet") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "04pab72x6rmvxqnscjpp2ydw0mpv2lf5llb6gpq45x310538qipk")))

(define-public crate-smpte2022-1-packet-0.2.0 (c (n "smpte2022-1-packet") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "18z2vm3fx75ld89zbl01g5gyjs91xik9ipik5xyl32k1k5vzhzln")))

(define-public crate-smpte2022-1-packet-0.3.0 (c (n "smpte2022-1-packet") (v "0.3.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rtp-rs") (r "^0.3.0") (d #t) (k 0)))) (h "010r737nir6gzdl7d015a06jv1hggvq14yawfdg6fq3qr0qbnksp")))

(define-public crate-smpte2022-1-packet-0.4.0 (c (n "smpte2022-1-packet") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rtp-rs") (r "^0.3.0") (d #t) (k 0)))) (h "08fdxw4rncawgv4w489i1l7c0anw9m6c1h41qj34agq20cbhzdrq")))

(define-public crate-smpte2022-1-packet-0.5.0 (c (n "smpte2022-1-packet") (v "0.5.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "rtp-rs") (r "^0.5.0") (d #t) (k 0)))) (h "06kwqxx54yicyn4gjzvisgng0ja794niqipbsy0ad9v0cjs5gkxp")))

