(define-module (crates-io sm ve smve_macros) #:use-module (crates-io))

(define-public crate-smve_macros-0.1.0 (c (n "smve_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1g5lhj6g8i74girgm8pbvx3vxz3gajndqqs75993mnxbfqqqg95q")))

(define-public crate-smve_macros-0.1.1 (c (n "smve_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "13m5dwjv94x4gq0plfjdrvwx9hva60px1w40i7pcs5zpndcbzhvz")))

