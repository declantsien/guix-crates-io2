(define-module (crates-io sm #{23}# sm2335egh) #:use-module (crates-io))

(define-public crate-sm2335egh-0.1.0 (c (n "sm2335egh") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)))) (h "11nza8as1qcsrgx1xb6mrg83fz8449vs4kc787rmzdd1xksfvl36")))

(define-public crate-sm2335egh-0.1.1 (c (n "sm2335egh") (v "0.1.1") (d (list (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)))) (h "1m0nh9hkzldzzad7sw2qcd1ly2yzcmn681dww1cisdcdg898pi7x")))

(define-public crate-sm2335egh-0.2.0 (c (n "sm2335egh") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "02b76604l2jj2rsdvz1mzgf20qxvrkbfjzlp6cvzaz45jskd10jl")))

