(define-module (crates-io sm bi smbios) #:use-module (crates-io))

(define-public crate-smbios-0.1.0 (c (n "smbios") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "0xpsya7bwmzggj4q3lvihrq0mfwd1z8x2cg7nnxrlyywag0vn39b") (y #t)))

(define-public crate-smbios-0.1.1 (c (n "smbios") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "13gvyk94rvwjxm9x7y60qqknkv3mjssixdnp28smhyv9q2rdz04v") (y #t)))

(define-public crate-smbios-0.1.2 (c (n "smbios") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1vn8f4za3cz177kkaak8b58f000sxsbgal1z9qmbyvdf30kvyszj")))

(define-public crate-smbios-0.1.3 (c (n "smbios") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1m78994d7p5nq7dqmfzgmcs5mip8gbhfbr4pbg78nkfsbmfmmyaj")))

(define-public crate-smbios-0.1.4 (c (n "smbios") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1jaz9bs7hbww9iqxm230fs6m85wzvr3pahkqqzsb5jdqwfhrg5g1")))

(define-public crate-smbios-0.1.5 (c (n "smbios") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1h315kp265ixq7j0p85gdj1x67cljnc7p0rhj0k2kdc3ncb1szqj")))

(define-public crate-smbios-0.1.6 (c (n "smbios") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "1aw4c10gink7ss7j6cgb9m4y4k3pfpqgfq2zb94dkiddhlc73gvg")))

(define-public crate-smbios-0.1.7 (c (n "smbios") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4.10") (d #t) (k 0)))) (h "037djj28lfgdlbz9mm96m4k8sy44jwnaywb80rdh60b82dah4ly5")))

