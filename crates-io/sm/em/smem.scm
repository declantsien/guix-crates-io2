(define-module (crates-io sm em smem) #:use-module (crates-io))

(define-public crate-smem-0.1.0 (c (n "smem") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wwx29ljnnbi0g5ri0jpm7bj38cq2hg7gc45m68xd1g1z5xfvzib")))

