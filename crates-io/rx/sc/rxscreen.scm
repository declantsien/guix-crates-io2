(define-module (crates-io rx sc rxscreen) #:use-module (crates-io))

(define-public crate-rxscreen-0.1.0 (c (n "rxscreen") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)))) (h "1vsmmb4cxrsali2xk4d88w3ip0w9ffxvqbnqdx075mwlc40i6jhk") (f (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1.2 (c (n "rxscreen") (v "0.1.2") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)))) (h "1gjrwvymbvk84mpa4wikdl3manbh53vn06ql20wxf2vlvavlanr7") (f (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1.3 (c (n "rxscreen") (v "0.1.3") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)))) (h "1azl35vmrkisbgfc8lgscy2xpapilds76c6qzdrr98v08krp4x29") (f (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1.4 (c (n "rxscreen") (v "0.1.4") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jdfvr1hr9br90z1c7j6dhfnsvj7ssdr5pmkc5y7lr0ayi8nnx4l") (f (quote (("xrandr") ("shm") ("save" "image"))))))

(define-public crate-rxscreen-0.1.5 (c (n "rxscreen") (v "0.1.5") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cm5mcmvkcik3iff6hi0g8lapl650kn4wh7gdaahnk0ljhgwdgyy") (f (quote (("xrandr") ("shm")))) (s 2) (e (quote (("save" "dep:image"))))))

(define-public crate-rxscreen-0.1.6 (c (n "rxscreen") (v "0.1.6") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qmkivvblm7qa32r6vfsblvx4h2377ci60ajj4ka4la24z7jz7zn") (f (quote (("xrandr") ("shm")))) (s 2) (e (quote (("save" "dep:image"))))))

(define-public crate-rxscreen-0.1.7 (c (n "rxscreen") (v "0.1.7") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15pwv6x7bb7zv3swm799kk2zcb4rjis4h38mnxr84fjhhczv3fmg") (f (quote (("xrandr") ("shm") ("mouse")))) (s 2) (e (quote (("save" "dep:image"))))))

