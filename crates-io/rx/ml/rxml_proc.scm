(define-module (crates-io rx ml rxml_proc) #:use-module (crates-io))

(define-public crate-rxml_proc-0.4.0-alpha.0 (c (n "rxml_proc") (v "0.4.0-alpha.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1n39n9hhxza9q3yygfgbgyd1hb1wfy3kjqjyi2h76y727srdnpqa")))

(define-public crate-rxml_proc-0.4.0 (c (n "rxml_proc") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c1p3b43gdf656xrscj4yayckry54w2bbdb3ladm5m320rpx9bw7")))

(define-public crate-rxml_proc-0.5.0 (c (n "rxml_proc") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c04x9zv5fj0iqm7x4jj89khim1i99fr1bj5gksjasc19i1ja1bp")))

(define-public crate-rxml_proc-0.6.0 (c (n "rxml_proc") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "194mnk59aslib31g7lhd7k017rfb4cl488i19p2d56s9rvmdkkcl")))

(define-public crate-rxml_proc-0.7.0 (c (n "rxml_proc") (v "0.7.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bqbpv5vsj0aw91f0cn11gr6vqz2yj0a5d759yv0jwmvbak05z5i")))

(define-public crate-rxml_proc-0.8.0 (c (n "rxml_proc") (v "0.8.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vkshsaspny9kxqlzxykl0v4pxfvvnky81in55518sm4j57zamzk")))

(define-public crate-rxml_proc-0.8.1 (c (n "rxml_proc") (v "0.8.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1q0b61hhhj2pr9wvn44cvchllcwwc2fxppzxx5hc9cff5yfmla2r")))

(define-public crate-rxml_proc-0.8.2 (c (n "rxml_proc") (v "0.8.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11fjpdmhb4n0d9ixn1c5rn26y9cw7x7p3ny8x2g0qxijnngynzdl")))

(define-public crate-rxml_proc-0.9.0 (c (n "rxml_proc") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hjv7f64z8a6hlrvn2y6hzkwp9zf5zkip18ir7xrj0vxk4cygz1s")))

(define-public crate-rxml_proc-0.9.1 (c (n "rxml_proc") (v "0.9.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.9.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0iqjx2w6gxbjkdw8w4w2l1baw8jnqp4hsh25fi1mwvi0c085icsb")))

(define-public crate-rxml_proc-0.10.0 (c (n "rxml_proc") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rxml_validation") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gl1hki8q9by4x1zgvkj9bmdqbrcaykhzk5x2ywqxhszivm2zbfx")))

