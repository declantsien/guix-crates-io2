(define-module (crates-io rx ml rxml_validation) #:use-module (crates-io))

(define-public crate-rxml_validation-0.4.0-alpha.0 (c (n "rxml_validation") (v "0.4.0-alpha.0") (h "12jwhimg38hhfhgjxbd3dmmww8iklbr5dqgldwdhhwliak18aswh")))

(define-public crate-rxml_validation-0.4.0 (c (n "rxml_validation") (v "0.4.0") (h "0ndhl9fdpmj7f4b7vv1fcmd1ymspn7kikjsh8n2yiwgkwn708fl2")))

(define-public crate-rxml_validation-0.5.0 (c (n "rxml_validation") (v "0.5.0") (h "19g5qpv9lv9h191pk09h5n9356kbw0cglw58yd0hbfmdgapgkfif")))

(define-public crate-rxml_validation-0.6.0 (c (n "rxml_validation") (v "0.6.0") (h "1rs0gspbf32k6jzbfbsxxzs2bvgjhijj2pasv32pzj91b73hgzxi")))

(define-public crate-rxml_validation-0.7.0 (c (n "rxml_validation") (v "0.7.0") (h "0sqknj3m1sa08ssc6kipflhmjpfff3zsb361ps2zpnapxmfvcvp1")))

(define-public crate-rxml_validation-0.8.0 (c (n "rxml_validation") (v "0.8.0") (h "1c2ay4g8h24bqcpmvbxyj39lby3vhfzklqnczdwxffsmqvbixdm8")))

(define-public crate-rxml_validation-0.8.1 (c (n "rxml_validation") (v "0.8.1") (h "16ljpc7ln03984w8f349kyfzbib8j3lwc4c4gj362c5r9gzksqx8")))

(define-public crate-rxml_validation-0.8.2 (c (n "rxml_validation") (v "0.8.2") (h "1vh01l5bcyw9qpxmjqj6zrmx8xgj6lvxh74m3zxw4rls7xs7kg2k")))

(define-public crate-rxml_validation-0.9.0 (c (n "rxml_validation") (v "0.9.0") (h "1ss50iwcpcs79mm6a793rcvbqa67w94pddyqrx4w3d0bnvaaq6n3")))

(define-public crate-rxml_validation-0.9.1 (c (n "rxml_validation") (v "0.9.1") (h "0c3mpm6idgls57dlgvx8sbhlbqbdkmnsvlb6l4cjy86f1qsrg892")))

(define-public crate-rxml_validation-0.10.0 (c (n "rxml_validation") (v "0.10.0") (h "0kxxplvhgj9ls0s3bzlcvqr5ld1jsbp6hxs0l8i0iazg8vzxsqhm") (f (quote (("std") ("default" "std"))))))

