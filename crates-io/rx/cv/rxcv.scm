(define-module (crates-io rx cv rxcv) #:use-module (crates-io))

(define-public crate-rxcv-0.1.0-alpha.1 (c (n "rxcv") (v "0.1.0-alpha.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q2x8icyzhcnmsl4n5r2az99z647gjvmh48icsbshsgghl7skx6p")))

(define-public crate-rxcv-0.1.0-alpha.2 (c (n "rxcv") (v "0.1.0-alpha.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0np3qsllacnydihpyws1k5k5bazksjjkikfn4q4k5j7w1j9xdhyj")))

(define-public crate-rxcv-0.1.0-alpha.3 (c (n "rxcv") (v "0.1.0-alpha.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zpcyd7czqs6cmp0x1z21s0mpmv03xqkk4g4jkxq8vyzzfvqk68y")))

(define-public crate-rxcv-0.1.0-alpha.4 (c (n "rxcv") (v "0.1.0-alpha.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ip7hpj8wwh6s2dx5qvfimwdmgzds234kdg8vyr5k0q81307xc3w")))

(define-public crate-rxcv-0.1.0-alpha.5 (c (n "rxcv") (v "0.1.0-alpha.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fs27gy69s48650kh91cbbk54yfcaiikbmjvk0mcpcz5pc04jpms")))

