(define-module (crates-io rx ql rxqlite-notification) #:use-module (crates-io))

(define-public crate-rxqlite-notification-0.1.0 (c (n "rxqlite-notification") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h1fjn73a7z4dyhq9w2va4fvd2dkm2ywpawk2pz8ivb6n4wawyfi") (y #t)))

(define-public crate-rxqlite-notification-0.1.1 (c (n "rxqlite-notification") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nq3qzw5dcdahh8wrqn1dlk68k83hfny6ms5rgx9dc2c4kwwcqsy")))

