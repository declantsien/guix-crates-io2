(define-module (crates-io rx ql rxqlite-common) #:use-module (crates-io))

(define-public crate-rxqlite-common-0.1.0 (c (n "rxqlite-common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xm11wdwfmyqnq01746h1v13q3vz1rhj2hsy2092j0lmz35za3lv") (y #t)))

(define-public crate-rxqlite-common-0.1.1 (c (n "rxqlite-common") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w0wkvj2rss3r1x4vfipisb9fiykd39747vla9kc9z02nb52i8ay") (y #t)))

(define-public crate-rxqlite-common-0.1.2 (c (n "rxqlite-common") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1akx22sl7byzx6sizjzbyjsy3bqzid1zq0z1rad7xxcl48j1q4p1") (y #t)))

(define-public crate-rxqlite-common-0.1.3 (c (n "rxqlite-common") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p6igk830j8kdw30s45aid371594wbxp2iyjas9271xgl51rc0v0") (y #t)))

(define-public crate-rxqlite-common-0.1.4 (c (n "rxqlite-common") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "either") (r "^1.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03ip6cz8fn68ig6igwg8nvmy3q559rcvdp6x1mryn90c3klv9www")))

