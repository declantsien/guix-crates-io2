(define-module (crates-io rx ql rxqlite-tests-common) #:use-module (crates-io))

(define-public crate-rxqlite-tests-common-0.1.0 (c (n "rxqlite-tests-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.5") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1a2vsk1wb1wfsz51mlzsk6fpimnsqd8hiw45q8gnwm1k409ig0qw") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.1 (c (n "rxqlite-tests-common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.1") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1c7xvbx6820j11cafwb9jnr7f1xdq5f7xscn0j926x9k8pl5nj24") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.2 (c (n "rxqlite-tests-common") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.1") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "0j4y2wlfyg0qhx7z848xk7nkm2rxdxx78a1br7aradn1kp6ym70j") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.3 (c (n "rxqlite-tests-common") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.1") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "0dy6kn8702dwz04jsy102v8wk48z0kmbjw8g5n64halxbchdj73i") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.4 (c (n "rxqlite-tests-common") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.1") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1vhkgzga8alcxw8mygx83gslqa8z4mjlfqyrnmg2917g6k8qc6ld") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.5 (c (n "rxqlite-tests-common") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.2") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "19mq1x5pkslm9myibbj428xws6m5gq1mw15avgvdi3dcvdmyhjg5") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.6 (c (n "rxqlite-tests-common") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (f (quote ("signal"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.2") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1aigyw9v0hn6i69jgqcmlihzx18hi0fcl1yfqdj9igrbnbvh6bnn") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.7 (c (n "rxqlite-tests-common") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (f (quote ("signal"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.3") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1qv2nrv3v979dzgain640sg8gkp208zcb355c0kfmib97p08b16a") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.8 (c (n "rxqlite-tests-common") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (f (quote ("signal"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.3") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "1r89dqfbxabs9lw24v0ikn3787ijgkxg1wx83l9cyplra12s90wc") (y #t)))

(define-public crate-rxqlite-tests-common-0.1.9 (c (n "rxqlite-tests-common") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (f (quote ("signal"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rcgen") (r "^0.12") (d #t) (k 0)) (d (n "rxqlite-common") (r "^0.1.3") (d #t) (k 0)) (d (n "state") (r "^0.6") (f (quote ("tls"))) (d #t) (k 0)))) (h "0x9dd16cdpr42xiz8x9ixzinq3b9ms1hg8xavp07ip3a4519hmd8")))

