(define-module (crates-io rx pi rxpipes) #:use-module (crates-io))

(define-public crate-rxpipes-1.0.0 (c (n "rxpipes") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mdgfp1awrxv4x3yyi8ydzv678mlh4mhmi0arqs5ngq0lybcl1ys") (f (quote (("default" "alternate-screen") ("alternate-screen"))))))

(define-public crate-rxpipes-1.1.0 (c (n "rxpipes") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "05z52qk21xaizspqcv04szkcfgxjy104zzwiwnjrmpvwq2shx77r") (f (quote (("default" "alternate-screen") ("alternate-screen"))))))

