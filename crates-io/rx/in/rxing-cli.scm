(define-module (crates-io rx in rxing-cli) #:use-module (crates-io))

(define-public crate-rxing-cli-0.1.0 (c (n "rxing-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "^0.2.7") (d #t) (k 0)))) (h "02395jj4hbmwccl2hlsh067cj27f3qgbkv2i24nqjx8vv4plz7xj")))

(define-public crate-rxing-cli-0.1.1 (c (n "rxing-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "^0.2.8") (d #t) (k 0)))) (h "0gf4dwxbqyg58g13ngz90d374689jbf3173wxdq30cr3rvh98sky")))

(define-public crate-rxing-cli-0.1.2 (c (n "rxing-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "^0.2.9") (d #t) (k 0)))) (h "0gnl130pdp32ks8hmj2w9l465hpjkcqm8d62njlbp9rfzz0l2ya7")))

(define-public crate-rxing-cli-0.1.3 (c (n "rxing-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "^0.2.10") (d #t) (k 0)))) (h "0b3rrsw9hpv3ynv92945hqy9rakhzh7dgysk2vg5wapniywingv2")))

(define-public crate-rxing-cli-0.1.4 (c (n "rxing-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "^0.2.12") (d #t) (k 0)))) (h "05vf3q9xqvqca97qc385zm7qgbgpiklbyxih6sm11x7sr5qm1n1d")))

(define-public crate-rxing-cli-0.1.5 (c (n "rxing-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.2.13") (d #t) (k 0)))) (h "1ji38ljbwkyan0hdvivsrv183jx3yfipg3xh5k8wl0gqg1s7pl76")))

(define-public crate-rxing-cli-0.1.6 (c (n "rxing-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.2.14") (d #t) (k 0)))) (h "17bqbbn3sffzm7i9jf866s2wbib2sk76akbx7q3fsalrxsjdrz90")))

(define-public crate-rxing-cli-0.1.7 (c (n "rxing-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.2.15") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0r3yq0wbx1r8198y2aas3sgwljvn17dawbnvny0v425q0dqr0ars")))

(define-public crate-rxing-cli-0.1.8 (c (n "rxing-cli") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.2.19") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1sf0bsxsyx8d46f2jhvn8qmas7s8pdf952v9lfnlw766yjq2zyhc")))

(define-public crate-rxing-cli-0.1.9 (c (n "rxing-cli") (v "0.1.9") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.2.21") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "122ppbgh3jaxc704ygkjg8kfy2psxmda9gggxk60chvd0n77cvq4")))

(define-public crate-rxing-cli-0.1.10 (c (n "rxing-cli") (v "0.1.10") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.3.0") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1dc9rv172l0ncfl6v24y9ali1yw1nbwnqiqgjj4y4kffmb167i4s")))

(define-public crate-rxing-cli-0.1.11 (c (n "rxing-cli") (v "0.1.11") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.3.1") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0v83hgksc9mb0jb8fbfdnmwjj1rar9f6rilxf4wxjj36v4zydnix")))

(define-public crate-rxing-cli-0.1.12 (c (n "rxing-cli") (v "0.1.12") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.3.2") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0k8fjycn8rzr04590fvaw5db8w6jjjnxzy8a42bypgzm831s9628")))

(define-public crate-rxing-cli-0.1.13 (c (n "rxing-cli") (v "0.1.13") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.4.0") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0rxjvdfhg77nz3wpm9fz3xa5q9n6lnf0414r7gkgzrryv12hggy4")))

(define-public crate-rxing-cli-0.1.14 (c (n "rxing-cli") (v "0.1.14") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.4.0") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0yb3c16w2asxgx9i2s81zzz9wz3kicg8w96f07kljs8fz6sg9k6a")))

(define-public crate-rxing-cli-0.1.15 (c (n "rxing-cli") (v "0.1.15") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.4.6") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0y2nyn93s515s0rsi73mgnbdc929c0damfw7k5z989vbrxd6anqm")))

(define-public crate-rxing-cli-0.1.16 (c (n "rxing-cli") (v "0.1.16") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.4.9") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1l024alv44fc5wjdgwhxrmnkdrmbflpa5xi9nrs32rfn3i59xgny")))

(define-public crate-rxing-cli-0.1.17 (c (n "rxing-cli") (v "0.1.17") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.0") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "19xivafjkssvmy2mz2vhf3480k4hqg763fkh94k07ipvzvk1hy9m")))

(define-public crate-rxing-cli-0.1.18 (c (n "rxing-cli") (v "0.1.18") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.3") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1z2r0x0cn2f796slvxcj36yzfs46bx53i27vrdh2vaqzfi64rs4c")))

(define-public crate-rxing-cli-0.1.19 (c (n "rxing-cli") (v "0.1.19") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.5") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0lf60w8xw1hb57hpb4f9a4m1hqsrdwa5hl64wmz9dia8haxm8fa1")))

(define-public crate-rxing-cli-0.1.20 (c (n "rxing-cli") (v "0.1.20") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.7") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "0ig4q7g53a9hbsrv5j8rpqj9175fzhd3239b52qvdfcvky7fw1k6")))

(define-public crate-rxing-cli-0.1.21 (c (n "rxing-cli") (v "0.1.21") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.8") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1zgn91540gll3hr45g1cavyjhdmyj5paqj1drzga897ywvm28dv8")))

(define-public crate-rxing-cli-0.1.22 (c (n "rxing-cli") (v "0.1.22") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rxing") (r "~0.5.9") (f (quote ("image" "svg_read" "svg_write"))) (d #t) (k 0)))) (h "1xr9wch7wgzmb0mi9lwdlsg3jrha1wffvry5gqrshsfcm2jd2iyl")))

