(define-module (crates-io rx in rxing-one-d-proc-derive) #:use-module (crates-io))

(define-public crate-rxing-one-d-proc-derive-0.1.0 (c (n "rxing-one-d-proc-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vfhq1d608zm4cmvgan4bkmqkac5qbbp472sgf24xzrn8casqb75")))

(define-public crate-rxing-one-d-proc-derive-0.2.0 (c (n "rxing-one-d-proc-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l2ay6mxjcir7477g68ri405fhapm0l6i7i6zgcdxk37c6jfsgdr")))

(define-public crate-rxing-one-d-proc-derive-0.3.0 (c (n "rxing-one-d-proc-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17fsaciva5iv31lgzfxzcmmvpk030qq6zcs6pqkvdsbnw1fn2c73")))

(define-public crate-rxing-one-d-proc-derive-0.3.1 (c (n "rxing-one-d-proc-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kv7p2pk4q9vs8i47wk19dl5xd0j73cm2399clbmkfxa6hl9pccp")))

(define-public crate-rxing-one-d-proc-derive-0.4.0 (c (n "rxing-one-d-proc-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1194h3ifbv1f43dvaw1qk7vdb2jn3fvhdnqsnp2a5bp5gkv86ix7")))

(define-public crate-rxing-one-d-proc-derive-0.5.0 (c (n "rxing-one-d-proc-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07yiksi543fqky7i6nahpxibr7ih2imyxfayxabvhfd7ljzaq2hi")))

(define-public crate-rxing-one-d-proc-derive-0.5.1 (c (n "rxing-one-d-proc-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0w65bscyzrff4f55py0ziqsvxdilihxiqqavh5q1ivi4iqzrgpsj")))

(define-public crate-rxing-one-d-proc-derive-0.5.3 (c (n "rxing-one-d-proc-derive") (v "0.5.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wjlp5qvy1m1a16wrc7ql1qbq9a39dl6h0r1ainx1vkklx6jmjar")))

