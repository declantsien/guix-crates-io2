(define-module (crates-io rx pr rxprog) #:use-module (crates-io))

(define-public crate-rxprog-1.0.0 (c (n "rxprog") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "ihex") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "srec") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0qbq1rpdcrfy3alg4dzgargsfycf8llz6qign1lhaaxswibsagi8") (f (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

(define-public crate-rxprog-1.0.1 (c (n "rxprog") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "ihex") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "srec") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "14pc5i9liqbzaxvwr5nkqv27rhmzs71sy3ws0hv157s5i72w40am") (f (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

(define-public crate-rxprog-1.0.2 (c (n "rxprog") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "ihex") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "srec") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0spsvx7rmz025imkc2q8zx4a301d6c79lcmm1k712fq0dbi4srcs") (f (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

