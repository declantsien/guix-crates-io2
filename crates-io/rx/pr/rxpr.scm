(define-module (crates-io rx pr rxpr) #:use-module (crates-io))

(define-public crate-rxpr-0.1.0 (c (n "rxpr") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1mhf5pc1b3w8ky3d2n78v2raqk4lwgh96bxz93zwrkkx3razdsh9")))

(define-public crate-rxpr-0.1.1-dev.1 (c (n "rxpr") (v "0.1.1-dev.1") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "15yjs8h5x5k2820c9kjk5gqfvh7ccgws56zhbdz7incyiw3ghmgh")))

(define-public crate-rxpr-0.1.1-dev.2 (c (n "rxpr") (v "0.1.1-dev.2") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1is7hqsr3k6c4fnvlfsdsdfy2ha4qkh6cwxk05l752vgvhrphha7")))

(define-public crate-rxpr-0.1.1 (c (n "rxpr") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "06g8x9f7l1p1bi9skglfvln17nc8m3r8hmsmszqwsrvn4g36zrq9")))

(define-public crate-rxpr-0.1.2 (c (n "rxpr") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ipvz1m5x4d81gp0kjkiwwxz62byj7rcwf843n2sf9kfz3f5gylw")))

(define-public crate-rxpr-1.0.0 (c (n "rxpr") (v "1.0.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "17jpqaa40fzw6s6b006fq1r5dnkzr09a53ninfpvw7s398iqcd1v")))

