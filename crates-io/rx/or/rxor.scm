(define-module (crates-io rx or rxor) #:use-module (crates-io))

(define-public crate-rxor-0.1.0 (c (n "rxor") (v "0.1.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1qlkky25z666z6bx1ff1xc7k362ahq61amxxcfw1c9ihd7fza19a")))

(define-public crate-rxor-0.2.0 (c (n "rxor") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "0kdgsmynn68zby69sqi71f48x4ackz77p6lvi0f6k9g5xdq4dpvz")))

