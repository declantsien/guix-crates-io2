(define-module (crates-io rx #{50}# rx509) #:use-module (crates-io))

(define-public crate-rx509-0.2.0 (c (n "rx509") (v "0.2.0") (h "1ys2bhy50rvflsmfi7npjgmw1rzx8a2ab9zk7lbn7wi9ywzpl5ww")))

(define-public crate-rx509-0.2.1 (c (n "rx509") (v "0.2.1") (h "0wvy8cl59z8lsimwzr63vc85qi3f6n6rcspzxly3nq2adv9agd6k")))

