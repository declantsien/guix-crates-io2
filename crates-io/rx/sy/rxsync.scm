(define-module (crates-io rx sy rxsync) #:use-module (crates-io))

(define-public crate-rxsync-0.1.0 (c (n "rxsync") (v "0.1.0") (d (list (d (n "adler") (r "^1.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ssh2") (r "^0.9.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1s7z22r4bkr0nihkjvdcar2cxclas6427bj3fn7k8kixj1cygrcn")))

