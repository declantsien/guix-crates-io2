(define-module (crates-io rx ca rxcalc) #:use-module (crates-io))

(define-public crate-rxcalc-1.0.0 (c (n "rxcalc") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0wip46zpai3s5mlyn1m06xlnv9268z1qs0xilkqwi0g6h5niwyb1") (y #t)))

(define-public crate-rxcalc-1.0.1 (c (n "rxcalc") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "11pma1gj311c9cjnxxnckyl197my17qg7wqpidw5264h5kz2kxas") (y #t)))

(define-public crate-rxcalc-1.0.2 (c (n "rxcalc") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0g6w3p5qq6k7xwn3mcd34is4v727y6mmshdbma318d1kzc075kq2") (y #t)))

(define-public crate-rxcalc-1.0.3 (c (n "rxcalc") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1jfsahb8bif5ljw15r405rrp1hcknwnsgqr9rginxpqbzh89zkhn") (y #t)))

(define-public crate-rxcalc-1.0.4 (c (n "rxcalc") (v "1.0.4") (h "1q63r74606c9484b5dg1xbdf29ax6zqrkgjya9ds39xvpr9ww3hn") (y #t)))

(define-public crate-rxcalc-1.0.5 (c (n "rxcalc") (v "1.0.5") (h "1wmcyx2pci54s662pf72i78i47yj3i3kxjph9h2ls4kacyafr682")))

