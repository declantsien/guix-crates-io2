(define-module (crates-io rx pe rxpect) #:use-module (crates-io))

(define-public crate-rxpect-0.1.0 (c (n "rxpect") (v "0.1.0") (h "1b050m8wqg9fd5fszcj52q8wipwcfq0f913k6x0mkvlv9sqqcqwq")))

(define-public crate-rxpect-0.1.1 (c (n "rxpect") (v "0.1.1") (h "1w86c1mivyjpy56qk2f4pz00rlr8q8gjvwjwyz28625x53add8pg")))

