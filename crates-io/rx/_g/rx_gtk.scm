(define-module (crates-io rx _g rx_gtk) #:use-module (crates-io))

(define-public crate-rx_gtk-0.1.0 (c (n "rx_gtk") (v "0.1.0") (d (list (d (n "gdk") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)) (d (n "rxrs") (r "^0.1.0-alpha2") (d #t) (k 0)))) (h "1am6bzzvm290h5cv0lfad226fg9jcf8facf02r3hh956wm1lsj4z")))

(define-public crate-rx_gtk-0.1.1 (c (n "rx_gtk") (v "0.1.1") (d (list (d (n "gdk") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)) (d (n "rxrs") (r "^0.1.0-alpha3") (d #t) (k 0)))) (h "0by0r3zc983vp89134wmibs3rk5yagm6mqp7hmrkh82k8nb93fj5")))

