(define-module (crates-io fw da fwdansi) #:use-module (crates-io))

(define-public crate-fwdansi-1.0.0 (c (n "fwdansi") (v "1.0.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0c3lkbc4m413ainw0szxq1j8vwn85wrl33vrpxcqmnqmvzdy1bz7") (y #t)))

(define-public crate-fwdansi-1.0.1 (c (n "fwdansi") (v "1.0.1") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1hqhhq4bxnmip8yh90jv9pfy0k4ll7gn684nxzzkg3gng984rp9l")))

(define-public crate-fwdansi-1.1.0 (c (n "fwdansi") (v "1.1.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "027jz2x5fbi6rskic8sd6xx0mn03a7dnhwkpyz8hamg8gxwgbh88")))

