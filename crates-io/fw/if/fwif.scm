(define-module (crates-io fw if fwif) #:use-module (crates-io))

(define-public crate-fwif-0.1.0 (c (n "fwif") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "19adx4gvygg93kz6gkb6zyxmm0np019szbqy1fxq61249y8fs2ll")))

