(define-module (crates-io fw uf fwuffgrep) #:use-module (crates-io))

(define-public crate-fwuffgrep-1.0.0 (c (n "fwuffgrep") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "164av5byw8jb57ljmwd8snblrr3xn1xcazn7zmcfyfkw6hkvjwj7")))

