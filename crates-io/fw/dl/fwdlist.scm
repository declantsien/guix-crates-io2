(define-module (crates-io fw dl fwdlist) #:use-module (crates-io))

(define-public crate-fwdlist-0.1.0 (c (n "fwdlist") (v "0.1.0") (h "18bf6ymsb06fa259bwcym1inyh60p7jc3dvlsfnvw7j535dh87mb")))

(define-public crate-fwdlist-0.1.1 (c (n "fwdlist") (v "0.1.1") (h "18nj2yv0nim1i1w47dgshalppg8ly893ay60kh1b6rfyif203cym")))

(define-public crate-fwdlist-0.2.0 (c (n "fwdlist") (v "0.2.0") (h "10hif65cixj488b302mich2a8qkpay7216gz66zj2vnyxg934711")))

(define-public crate-fwdlist-0.3.0 (c (n "fwdlist") (v "0.3.0") (h "0dlaklg4k94lizniy2sk3whv2990njg95hlkg5cw0q6lixhllviw") (f (quote (("bench"))))))

