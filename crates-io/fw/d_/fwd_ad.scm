(define-module (crates-io fw d_ fwd_ad) #:use-module (crates-io))

(define-public crate-fwd_ad-0.1.0 (c (n "fwd_ad") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0d1i8qp1g7g6p8h95iwsnzpfmqzcwzbhzi6v067dwka472x3vdx9") (f (quote (("implicit-clone") ("default") ("bench" "criterion"))))))

(define-public crate-fwd_ad-0.2.0 (c (n "fwd_ad") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1zkvsdjb6rnk7lqiihrzfmcx991f6k0ig1666gr6q2rsgdbkp249") (f (quote (("implicit-clone") ("default") ("bench" "criterion"))))))

