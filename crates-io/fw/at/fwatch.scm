(define-module (crates-io fw at fwatch) #:use-module (crates-io))

(define-public crate-fwatch-0.1.0 (c (n "fwatch") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1385s5jz22g8khi2kxwi6l7l3d9y4kdh3ll8hscikbqnbzdc3bir")))

(define-public crate-fwatch-0.1.1 (c (n "fwatch") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "162pakiqm1pyw52p8nsn1x5diwzgb6axb04sw0sz36b7skcqqy2x")))

(define-public crate-fwatch-0.1.2 (c (n "fwatch") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1nsp43fbbi39fhxdqcpjr8mg3bykmcyafc56dw8j2p517c0pmcsy")))

(define-public crate-fwatch-0.1.3 (c (n "fwatch") (v "0.1.3") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0jiwvyi3bir0gshkmblp9pnk7ay7p5cpdhg0vb192ffd9jg2bpdr")))

(define-public crate-fwatch-0.1.4 (c (n "fwatch") (v "0.1.4") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1y5156niz0pxn796hvqzh31421l3q1qzb1cx4hys0lfcv3cmxx5s")))

(define-public crate-fwatch-0.1.5 (c (n "fwatch") (v "0.1.5") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1p4nj3bcwx8pvpwgmjk15ri7vhz28lwcmkcdvp2cj5jypixak320")))

