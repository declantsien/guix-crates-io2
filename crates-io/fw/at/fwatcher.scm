(define-module (crates-io fw at fwatcher) #:use-module (crates-io))

(define-public crate-fwatcher-0.0.1 (c (n "fwatcher") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0vizla8gnrhxgy4xdcvw17mlw0ijfkzgqgsx07lgs7xcda04gpfy") (y #t)))

(define-public crate-fwatcher-0.0.2 (c (n "fwatcher") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0m5msqi4ch795a38anziihg64azn9l3d5b4a7yck9phrdjllrd41") (y #t)))

(define-public crate-fwatcher-0.0.3 (c (n "fwatcher") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1msh8yn1vzb2srky5r3q1ylp52ywfiqayzywh9qj9pjpjc8bq263") (y #t)))

(define-public crate-fwatcher-0.0.4 (c (n "fwatcher") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0q0fzc2c25h1g96bhmi6ja1j47pdhh3ffzlhjr0wfpxnidfaf9hi") (y #t)))

(define-public crate-fwatcher-0.1.0 (c (n "fwatcher") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0701gpjadaznsj76xb6pp01j88rzx0cvmq8vg6wa1h6vrz1bh3r6") (y #t)))

(define-public crate-fwatcher-0.2.0 (c (n "fwatcher") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1ssq2d8varzd9rqfzn55hjk4l1il7ywqqmqdqcvvkxgk8qzxgv2b")))

(define-public crate-fwatcher-0.3.0 (c (n "fwatcher") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "171qp53rhynfzpc28g1jg558252jbs4wcif0d2pgppi7bjhbnkds")))

(define-public crate-fwatcher-0.3.1 (c (n "fwatcher") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1vq0rfay2f61m6d3c2fzqkvmhc0ad9zizfqwi4c6i1v9q1053dnb")))

(define-public crate-fwatcher-0.3.2 (c (n "fwatcher") (v "0.3.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "05wf7nax91lz1iz47i2gs87zyg7pnkmbn21w1rrp9v2a357fk796")))

(define-public crate-fwatcher-0.4.0 (c (n "fwatcher") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0sfdmlna7cm1h4c8717kbh0c4xpv04k4h6d81axc5sz15id1ai2i")))

(define-public crate-fwatcher-0.4.1 (c (n "fwatcher") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1fdrzn7lanmlqmy033q14gm7ngw32p7309k7692azmgxnw4mzq40")))

(define-public crate-fwatcher-0.4.2 (c (n "fwatcher") (v "0.4.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "16n016zlrj8wxcvaf7i745izd182yd95brsf3d8568pgvfwa1fk9")))

