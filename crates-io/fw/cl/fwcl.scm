(define-module (crates-io fw cl fwcl) #:use-module (crates-io))

(define-public crate-fwcl-0.1.0 (c (n "fwcl") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0fvmsp95kqk4x6c5kh2lfz24y4m9czwb0rz6dbbbbab9aivcr5x9")))

(define-public crate-fwcl-0.1.1 (c (n "fwcl") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0s00bakc1rym9l2ad6paizaq4ky8k22yj9nlsnpf011ihd7mfg2y")))

