(define-module (crates-io fw dt fwdt) #:use-module (crates-io))

(define-public crate-fwdt-0.1.0 (c (n "fwdt") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1jzai0yfjjj5vxy5s8ypvjh3h0cmd9rrz8h1436jaycrp5a60ik9")))

(define-public crate-fwdt-0.1.1 (c (n "fwdt") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "023cr81ahpqamqca84j1lap820b87jvfh55v0y29qyd3yqh8gk8z")))

