(define-module (crates-io fw al fwalker) #:use-module (crates-io))

(define-public crate-fwalker-0.1.0 (c (n "fwalker") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14pniswkc1jgqrw7sk7mg6f7vn54prb8vbkdsxcz9sni9lhl4mx4")))

(define-public crate-fwalker-0.2.0 (c (n "fwalker") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06qwzyd6ngsk70cygyrr4n1qmpn3sa6xjfvw4p8qz9h487rhj3wd")))

(define-public crate-fwalker-0.3.0 (c (n "fwalker") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.14") (d #t) (k 0)))) (h "056b7wvz343pfa8hagyadmhfjxfizq0zjlhxrpjrp4y5b3qym0i1")))

(define-public crate-fwalker-0.3.1 (c (n "fwalker") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "0cvnvrbs7smiagsprcdny6pqqbmhpj2zkaily49sl9lgvc9xg07m")))

(define-public crate-fwalker-0.3.2 (c (n "fwalker") (v "0.3.2") (d (list (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r ">=0.15.0, <0.16.0") (d #t) (k 0)))) (h "0f6wz6qjghi481z5gqvssbhqf2c1svxgmk54r73ggaqvk27zk8a9")))

(define-public crate-fwalker-0.3.3 (c (n "fwalker") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "0h78hmbsy0d14awgdhdf6qy68wn1gsxg2gvlmyk2hqr2622y9bwh")))

(define-public crate-fwalker-0.3.4 (c (n "fwalker") (v "0.3.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "1wbwgaw6zy9f59vqawizhn92vd1vdjm5v6h8jmg5lvw0bvqv2i6p")))

(define-public crate-fwalker-0.4.0 (c (n "fwalker") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15") (d #t) (k 0)))) (h "1kxb33glcpp2vh9n28n5lqmq2p8rmdmvsjybbqb8r862i7xz0mnh")))

(define-public crate-fwalker-0.4.1 (c (n "fwalker") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16") (d #t) (k 0)))) (h "17q5p6pnfb9rr4zsll2l0q9g2v2y62dlnqfh6pbhg3jq7b941ag4")))

