(define-module (crates-io ts tp tstpmove) #:use-module (crates-io))

(define-public crate-tstpmove-1.0.0 (c (n "tstpmove") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ys11hl8wn5w837q5awv1ykgabfc83mwylvv3c461ypqlivg52vk")))

(define-public crate-tstpmove-1.2.1 (c (n "tstpmove") (v "1.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nv830lvafnac1hrsvzkdrq6q3vvn86fckbr2aycha0pzv26xgf4")))

(define-public crate-tstpmove-1.2.2 (c (n "tstpmove") (v "1.2.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fa23qc3qswf26pwc6g67ym3p4rv2ck4w6z3vplb1ci8n8a8iqk")))

