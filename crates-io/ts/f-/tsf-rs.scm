(define-module (crates-io ts f- tsf-rs) #:use-module (crates-io))

(define-public crate-tsf-rs-0.1.0 (c (n "tsf-rs") (v "0.1.0") (d (list (d (n "sma-rs") (r "^0.1.4") (d #t) (k 0)) (d (n "ta-common") (r "^0.1.2") (d #t) (k 0)))) (h "1kci8ziw4d42s56rxqw6fr40p514165wmz1xnad4j22shfhsmgwc")))

(define-public crate-tsf-rs-0.1.1 (c (n "tsf-rs") (v "0.1.1") (d (list (d (n "sma-rs") (r "^0.1.4") (d #t) (k 0)) (d (n "ta-common") (r "^0.1.2") (d #t) (k 0)))) (h "07sc7917ypf7famy70wg5xvf2mslffd190sv8y5vv2c3r583i9kp")))

