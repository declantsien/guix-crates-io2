(define-module (crates-io ts f- tsf-sys) #:use-module (crates-io))

(define-public crate-tsf-sys-0.1.0 (c (n "tsf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "13sllimb0h47kxw4b13x8qb64w3vqxpp05ixz9144i027c8yw9gl")))

(define-public crate-tsf-sys-0.1.1 (c (n "tsf-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0m4c568mph9m1r5nshmxzfg26a7w8krbq5d3nc5333b4wqinfgli") (l "tsf")))

(define-public crate-tsf-sys-0.2.0 (c (n "tsf-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.69") (d #t) (k 1)))) (h "0cgnnz797wl35cn0h4cflpfj4yzynna1qx8y156lgsbqxix6sj2z") (l "tsf")))

