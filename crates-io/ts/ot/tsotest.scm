(define-module (crates-io ts ot tsotest) #:use-module (crates-io))

(define-public crate-tsotest-0.1.0 (c (n "tsotest") (v "0.1.0") (h "1g5d7x5djap0s9n61hrmaf5xj8bi2ypzfgxj1dn8cw5m7pi87sgp") (y #t)))

(define-public crate-tsotest-0.2.0 (c (n "tsotest") (v "0.2.0") (d (list (d (n "sysctl") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1jjsdi4pyhjibhz9s2888f1kyny7jcrnsva61802hbbd52h1myx8") (f (quote (("default") ("apple-tso" "sysctl"))))))

