(define-module (crates-io ts pr tsproto-structs) #:use-module (crates-io))

(define-public crate-tsproto-structs-0.1.0 (c (n "tsproto-structs") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0nanpj5i201684wmc09i5hbs0sycp41x65s767mllhngg00m9209")))

(define-public crate-tsproto-structs-0.2.0 (c (n "tsproto-structs") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15s1axkjbv70klb0hjsbh42yig36kggsq31615fa3lslla8ajvfp")))

