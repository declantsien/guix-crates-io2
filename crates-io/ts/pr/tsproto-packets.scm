(define-module (crates-io ts pr tsproto-packets) #:use-module (crates-io))

(define-public crate-tsproto-packets-0.1.0 (c (n "tsproto-packets") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "omnom") (r "^3") (d #t) (k 0)) (d (n "rental") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "166dmp4sajgkxba1xlz3h0vkspfdl3axr0y8mb39hy23gb6m2m29")))

