(define-module (crates-io ts pr tsproto-commands) #:use-module (crates-io))

(define-public crate-tsproto-commands-0.1.0 (c (n "tsproto-commands") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "t4rust-derive") (r "^0.1.3") (d #t) (k 1)) (d (n "tsproto") (r "= 0.1.0") (d #t) (k 0)) (d (n "tsproto-structs") (r "= 0.1.0") (d #t) (k 1)))) (h "0siawqfj7h7k4ig9hcnbpb7al28mivwwi159wxqsw3940qvdyd53")))

