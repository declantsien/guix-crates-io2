(define-module (crates-io ts -f ts-fmt-lite) #:use-module (crates-io))

(define-public crate-ts-fmt-lite-0.1.0 (c (n "ts-fmt-lite") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.7.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0kyvpn1q3i4sgqalqsv65xbaqw6dd6g123h09syffl4asb093qzb") (f (quote (("naive-wrap") ("default" "naive-wrap"))))))

(define-public crate-ts-fmt-lite-0.1.1 (c (n "ts-fmt-lite") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.7.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0m0rz8x0mnd6zbcp2h8jl3hsijanis2kllfjy4smzlii3gydlda0") (f (quote (("naive-wrap") ("default" "naive-wrap"))))))

