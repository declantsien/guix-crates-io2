(define-module (crates-io ts in tsinfo) #:use-module (crates-io))

(define-public crate-tsinfo-0.1.0 (c (n "tsinfo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q0mcrnfa532bn9rc5i21gd7xj479d5n1991i2wgqwjkmir2i7pb")))

