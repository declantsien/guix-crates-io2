(define-module (crates-io ts z- tsz-macro) #:use-module (crates-io))

(define-public crate-tsz-macro-0.1.1 (c (n "tsz-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "098j8p85gpw5adpxpdds55w0m00k9r40jpw6hhm5180ayz9nxxhd")))

(define-public crate-tsz-macro-0.1.2 (c (n "tsz-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "1by8pqc5j7r4gr9jlj4b0qgw9gfhfqn518s0jqpyww0rly018w3i")))

(define-public crate-tsz-macro-1.0.0 (c (n "tsz-macro") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "1mj22i9awj8hk9kwv0125zz1dsh53h3vbg7bp1s11w9xqfarc91d")))

(define-public crate-tsz-macro-1.0.1 (c (n "tsz-macro") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "1yyl95c2pf0csk9phvllgk2rj289vzph9c87d6qk1in6wggkrdxs")))

(define-public crate-tsz-macro-1.0.3 (c (n "tsz-macro") (v "1.0.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "05ch8k0mzf58p5ikkgh8rv40srvh1j7jmsx289419xpnvzc0bcqi")))

(define-public crate-tsz-macro-1.1.0 (c (n "tsz-macro") (v "1.1.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0nji9943nz1n41is6wygwfkk1y62kiz1cbfkgy09hirmvpv4hw62")))

(define-public crate-tsz-macro-1.1.3 (c (n "tsz-macro") (v "1.1.3") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "05dj8zzlzxlib0m4c7f9mig7yhx5f5z26kaamv07a6xmn3iigm0y")))

(define-public crate-tsz-macro-1.1.4 (c (n "tsz-macro") (v "1.1.4") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "01wgvx9zh5f99zi8pqx707i8bcqv5ip7b7ag3i946i3kjvkkcliv")))

(define-public crate-tsz-macro-1.1.5 (c (n "tsz-macro") (v "1.1.5") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1ilc1vf9kwnw4qm15kdhga4ky45wwda9jlilczb0xy9n48gnzr9w")))

