(define-module (crates-io ts z- tsz-compress) #:use-module (crates-io))

(define-public crate-tsz-compress-0.1.1 (c (n "tsz-compress") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.91") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1zhc4giqs9ggs32xk11bzh057a5m9dn0wnj57azmdkzpjpcpkcxy")))

(define-public crate-tsz-compress-0.1.2 (c (n "tsz-compress") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0plvlwv5qkag3bb2ngg8ihmjdciwncjwswd10bx76nizggridb2v")))

(define-public crate-tsz-compress-0.1.3 (c (n "tsz-compress") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0q8b8wbdggyqad9zrvncs58hp5lqcs1l1wgcyxapn71jq97kqz9g")))

(define-public crate-tsz-compress-0.1.4 (c (n "tsz-compress") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^0.1.2") (d #t) (k 0)))) (h "13snp7xj59229q8ng715d9vrlq92zr50r28pvr6bcl8rxcnqixy7")))

(define-public crate-tsz-compress-1.0.0 (c (n "tsz-compress") (v "1.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^1.0.0") (d #t) (k 0)))) (h "140mbc0g7sq0yyxcgncv6k6xapcvqyg4pf1vbbk9vjw42sn60wgp")))

(define-public crate-tsz-compress-1.0.1 (c (n "tsz-compress") (v "1.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "tsz-macro") (r "^1.0.1") (d #t) (k 0)))) (h "1d619k31g7vpdmzgg3sl3svrc9v9riyrx1dfznzfc0vwwxndngn8")))

(define-public crate-tsz-compress-1.0.2 (c (n "tsz-compress") (v "1.0.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.0.1") (d #t) (k 0)))) (h "06rkxm6v3sf2phcajg2qv0h0dgvdm5kjd3yzai1p218dbym21qbi")))

(define-public crate-tsz-compress-1.0.3 (c (n "tsz-compress") (v "1.0.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.0.3") (d #t) (k 0)))) (h "17nhsi4mdjpm038x1q14bhh5j4c6b725i3i28f7zbrbhd14vswlq")))

(define-public crate-tsz-compress-1.0.4 (c (n "tsz-compress") (v "1.0.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.0.3") (d #t) (k 0)))) (h "1z8zrhibszmvmykykj9jlg04pzcr89j4w14vnn16ryjvpk9ms1w8")))

(define-public crate-tsz-compress-1.0.5 (c (n "tsz-compress") (v "1.0.5") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.0.3") (d #t) (k 0)))) (h "1k3j08yxkni118z2zzgmhkqy9i2gqp3m5ps4b1l0spg8bprryagn")))

(define-public crate-tsz-compress-1.1.0 (c (n "tsz-compress") (v "1.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.0") (d #t) (k 0)))) (h "0s5b4c9nvl05j7yv5pzhkvjkf7rb24casqv52ch04v8yvr9dnc8z") (f (quote (("std") ("default"))))))

(define-public crate-tsz-compress-1.1.1 (c (n "tsz-compress") (v "1.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.0") (d #t) (k 0)))) (h "1mfksw8q2i15xk5z9gxhby67ffpmj7wrcyzhns7m3zb5frqmpcqm") (f (quote (("std") ("default"))))))

(define-public crate-tsz-compress-1.1.2 (c (n "tsz-compress") (v "1.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.0") (d #t) (k 0)))) (h "188xmp7y776f1aq9cpr5idsjbch7j7ia80l6cacqvhsb3c66jd7w") (f (quote (("std") ("default"))))))

(define-public crate-tsz-compress-1.1.3 (c (n "tsz-compress") (v "1.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.3") (d #t) (k 0)))) (h "1rc2lr4p7ndp1sxjkqjzj4lpy373vhllz8knck79awv1n186imv9") (f (quote (("std") ("default"))))))

(define-public crate-tsz-compress-1.1.4 (c (n "tsz-compress") (v "1.1.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.4") (d #t) (k 0)))) (h "0xwsjdbicv8zr1dcb68fil3i4h6vklxhh96v6wjfqyklg7rljxjx") (f (quote (("std") ("default"))))))

(define-public crate-tsz-compress-1.1.5 (c (n "tsz-compress") (v "1.1.5") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tsz-macro") (r "^1.1.5") (d #t) (k 0)))) (h "19zc9b1ayf53pgc5a1rnn69fz4p1izmk9n54vn02m8qq64f5yjnr") (f (quote (("std") ("default"))))))

