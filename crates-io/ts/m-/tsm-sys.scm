(define-module (crates-io ts m- tsm-sys) #:use-module (crates-io))

(define-public crate-tsm-sys-0.1.0 (c (n "tsm-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "^0.2.10") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^0.1.8") (d #t) (k 2)))) (h "134vzpn67rlmzppzp6gq8z34gi7scvxhg2a462y2qxjmpn4jlgri")))

