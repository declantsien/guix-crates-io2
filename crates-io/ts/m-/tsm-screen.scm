(define-module (crates-io ts m- tsm-screen) #:use-module (crates-io))

(define-public crate-tsm-screen-0.1.0 (c (n "tsm-screen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0z9qm9dcimcfd3kypk2ry4xzrad3x1p6qn9x6r61j907hyc15z13")))

(define-public crate-tsm-screen-0.1.1 (c (n "tsm-screen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0lrrzglqh9ify3mbwbbp4p6sxszz1sfj0b7l2iw5nf40lpgj2m0f")))

(define-public crate-tsm-screen-0.1.2 (c (n "tsm-screen") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "089f2xz0v7brs4g29pfhngd5mrx849d6hjyzlw71jgyv0fwmw140")))

