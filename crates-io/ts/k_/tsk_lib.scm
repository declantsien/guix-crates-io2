(define-module (crates-io ts k_ tsk_lib) #:use-module (crates-io))

(define-public crate-tsk_lib-0.0.0 (c (n "tsk_lib") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x4ww7fvz5bfy5swrrr90hibzsfcy90x8k8d1ln0mjv9nds4jmr9")))

