(define-module (crates-io ts li tslite) #:use-module (crates-io))

(define-public crate-tslite-0.1.0 (c (n "tslite") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qfljsj380hg712l7lpilcqh7ic6ldmwzfhqlv37wvb8faxy6944")))

(define-public crate-tslite-0.1.1 (c (n "tslite") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "096jzymn9n6zl04lp8hs0wqbsa025hkww3i85yxbg3q9j0wmmkms")))

(define-public crate-tslite-0.1.2 (c (n "tslite") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1kpl0as3axwa9isk0b7y2h67lqjc6lbvsw0y2n97dykvwilz6p5j")))

(define-public crate-tslite-0.1.3 (c (n "tslite") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j6k3l0zbnac4295597blpgvizxc97p5bp5q9gn2vj0mn377k627")))

(define-public crate-tslite-0.1.5 (c (n "tslite") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1bb3aq0qq5n1yn0kdcsygyl30hxyfb2k99cpwx8lgp5dc7zr2011")))

