(define-module (crates-io ts co tsconfig) #:use-module (crates-io))

(define-public crate-tsconfig-0.1.0 (c (n "tsconfig") (v "0.1.0") (d (list (d (n "json_comments") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0j1apql7n842297a0ni8hr9bx26jhglrzcj7k4s2h7jwfywsc4w6")))

(define-public crate-tsconfig-0.2.0 (c (n "tsconfig") (v "0.2.0") (d (list (d (n "json_comments") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0as64saxiq4p70kb5k5caf8ckzdl3x8xha5n7qs7xz8kdclfv2bw")))

(define-public crate-tsconfig-0.2.1 (c (n "tsconfig") (v "0.2.1") (d (list (d (n "json_comments") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hrg77d51fmlc6yjzdlkpblawncmlqf546krbvmsc8ij7rj0zf9z")))

