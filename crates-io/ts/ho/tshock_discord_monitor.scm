(define-module (crates-io ts ho tshock_discord_monitor) #:use-module (crates-io))

(define-public crate-tshock_discord_monitor-0.1.3 (c (n "tshock_discord_monitor") (v "0.1.3") (d (list (d (n "backoff") (r "^0.2.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "config") (r "^0.10.1") (f (quote ("toml"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.9.0-rc.0") (f (quote ("builder" "cache" "collector" "client" "gateway" "http" "model" "utils" "native_tls_backend"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hi8yaf9wqixlfhb2zb9jq119y9madl9r0jqar3hmj43w6h9x6bc")))

