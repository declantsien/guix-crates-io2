(define-module (crates-io ts _d ts_deplint) #:use-module (crates-io))

(define-public crate-ts_deplint-0.0.1 (c (n "ts_deplint") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0bz0z97d4kslimx1i6awq14j3qi5cpm042qgvcsjpi2mgy91y7hy")))

(define-public crate-ts_deplint-0.0.2 (c (n "ts_deplint") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "004z9jd2szdib318s4cdcz0kw0mj26nj7yhmv2mwmzyqfw6wzk2z")))

(define-public crate-ts_deplint-0.0.3 (c (n "ts_deplint") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1zjq11k6llgq647zglja8ax29lzn15ww65lhyw76iqa8i20nc4gs")))

(define-public crate-ts_deplint-0.0.4 (c (n "ts_deplint") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "01jcv0a2yyic92pyacxhcbzqgisbilqvj1vl7qdmz2jgriyqa8ra")))

(define-public crate-ts_deplint-0.0.5 (c (n "ts_deplint") (v "0.0.5") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03jnx09q3syjx7c8k1l0p68440hhcm80xsdlzxfpz60lzcpffhzm")))

