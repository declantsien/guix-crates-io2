(define-module (crates-io ts to tstore-cli) #:use-module (crates-io))

(define-public crate-tstore-cli-0.1.0 (c (n "tstore-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("multipart" "gzip" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14ql7jafix9p165gdnkjwm0n7gyk8wxnj76m6wbrzifcp4lr6rh8")))

