(define-module (crates-io ts to tstorage) #:use-module (crates-io))

(define-public crate-tstorage-0.1.0 (c (n "tstorage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tsz") (r "^0.1.4") (d #t) (k 0)))) (h "1xbcjfvi78fwn4z64djpsk4chz71za1yi3xx9gw5q4c6dk2pqjwh") (r "1.56")))

