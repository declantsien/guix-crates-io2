(define-module (crates-io ts #{2r}# ts2rs) #:use-module (crates-io))

(define-public crate-ts2rs-0.1.0 (c (n "ts2rs") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "189zh135v71b1npj1rw85mvmavxmsdv03kmwrjvmg4hc4cxahdz9")))

(define-public crate-ts2rs-0.1.1 (c (n "ts2rs") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "14g4srmnv0vm1jdq55gcphg68jgm3l8cj474gjp5c31bpg039m72")))

(define-public crate-ts2rs-0.1.2 (c (n "ts2rs") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "084sli91611ij73afiq5gnhrzdx518ww4gd0lp8pcbwq65kh90j6")))

(define-public crate-ts2rs-0.1.3 (c (n "ts2rs") (v "0.1.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0dmminip3f7f4y85068zn2ffikcdrycpiif7hsw9h5zdq75x7l4g")))

(define-public crate-ts2rs-0.2.0 (c (n "ts2rs") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "1jbfqrvd5v3z0wa8bkdy7vipiqbcjarfhkkrmsjlhbr5n8bk9n2j") (f (quote (("serde"))))))

(define-public crate-ts2rs-0.2.1 (c (n "ts2rs") (v "0.2.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "1lbns5xr9nraw37zslir15wg24gbzyhnpjrr6xbvir7dl30da2wr") (f (quote (("serde"))))))

(define-public crate-ts2rs-0.2.2 (c (n "ts2rs") (v "0.2.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "1q16mgmyygbdg617j0h3jcy6id6ni3fazw4d69vw3bpqsryi6ciw") (f (quote (("serde"))))))

