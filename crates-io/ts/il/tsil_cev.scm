(define-module (crates-io ts il tsil_cev) #:use-module (crates-io))

(define-public crate-tsil_cev-0.1.0 (c (n "tsil_cev") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "148xq1samsw60jsy7qnjxbqrjbmy4f4r092h9x93p4w77bb98fgb")))

(define-public crate-tsil_cev-0.2.0 (c (n "tsil_cev") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1n9baj1fjiwrghyb371nyplplnqpikldqjlvax5dfhrlzh4smkg0")))

(define-public crate-tsil_cev-0.3.0 (c (n "tsil_cev") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rjin3zvjsx78mlw445gwqskz5jf1qyfly9gwdpfrlqq3g1bpcnh")))

(define-public crate-tsil_cev-1.0.0 (c (n "tsil_cev") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1smvval6n32x39zrlqxp6dy7c08z84xnv2l88pqkhfcgyn7xfi5a")))

(define-public crate-tsil_cev-1.1.0 (c (n "tsil_cev") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g5yd703giqkvmpfry8w712bm64id44k6kqwza6cnkyfapv8zk15")))

