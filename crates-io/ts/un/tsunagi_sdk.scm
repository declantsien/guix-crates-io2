(define-module (crates-io ts un tsunagi_sdk) #:use-module (crates-io))

(define-public crate-tsunagi_sdk-0.1.0 (c (n "tsunagi_sdk") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1xxgk9gy60dwrpbki48xc2zjn32642rkjrkg9klchcc7pqqnnzn4")))

