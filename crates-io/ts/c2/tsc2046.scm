(define-module (crates-io ts c2 tsc2046) #:use-module (crates-io))

(define-public crate-tsc2046-0.1.0 (c (n "tsc2046") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)))) (h "1qkx2n53xmxvxcxgm0x2gvyfalkcl94wadc9n50pfnfp3nc1anyg")))

