(define-module (crates-io ts uk tsukuyomi-askama) #:use-module (crates-io))

(define-public crate-tsukuyomi-askama-0.1.0 (c (n "tsukuyomi-askama") (v "0.1.0") (d (list (d (n "askama") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.4.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "14xn5gy7xxq3yq37f7wjaxxqqd3qmwk8caiwxznvzw3qw3mihhbx")))

(define-public crate-tsukuyomi-askama-0.2.0 (c (n "tsukuyomi-askama") (v "0.2.0") (d (list (d (n "askama") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.5.0") (d #t) (k 0)) (d (n "tsukuyomi-server") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "0aj9av9vf58i8f8snc0my7a4w8i6kdmvjbzkawp2vrfc8l6n8hbs")))

(define-public crate-tsukuyomi-askama-0.2.1 (c (n "tsukuyomi-askama") (v "0.2.1") (d (list (d (n "askama") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.5.2") (d #t) (k 0)) (d (n "tsukuyomi-server") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "05p1akhnnqyfhsnzk8hirlgcjxz0bj8gkwfa9wx3m2bj413x87dz")))

