(define-module (crates-io ts uk tsukuyomi-macros) #:use-module (crates-io))

(define-public crate-tsukuyomi-macros-0.4.0 (c (n "tsukuyomi-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsukuyomi-internal") (r "^0.4.0") (d #t) (k 0)))) (h "1bzfcpqkcwlkbdf5gqy5d6jggsgd2dk4lv4n5m4qxaqn9rgdph4r")))

(define-public crate-tsukuyomi-macros-0.4.2 (c (n "tsukuyomi-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsukuyomi-internal") (r "= 0.4.2") (d #t) (k 0)))) (h "0ma35lpb531k3p7n3m93q3zk61x2vd7l45yy8lp28qvd42kgn90d")))

(define-public crate-tsukuyomi-macros-0.4.3 (c (n "tsukuyomi-macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsukuyomi-internal") (r "= 0.4.3") (d #t) (k 0)))) (h "1jfmy0jmaids24xwbgdki6x3qqmiyrfqhx80y0b0vc3paz7s2gma")))

(define-public crate-tsukuyomi-macros-0.5.0 (c (n "tsukuyomi-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wwv0xi879yxkhsy8cm3khmxvrsxkasx5db1ka7g1gzp8j0k4zcg")))

(define-public crate-tsukuyomi-macros-0.5.1 (c (n "tsukuyomi-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.5") (d #t) (k 2)))) (h "1kc6jl7zwj3bfrndb3a25hysdhs63y1wy5bly3m276n6z0kbjxg5")))

(define-public crate-tsukuyomi-macros-0.5.2 (c (n "tsukuyomi-macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.5") (d #t) (k 2)))) (h "0vp6d7pvd4bpclc8pil1w7cwh1v6sap8s1g13ad2r0rx1lmvabfm")))

