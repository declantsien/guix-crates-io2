(define-module (crates-io ts uk tsukuyomi-internal) #:use-module (crates-io))

(define-public crate-tsukuyomi-internal-0.4.0 (c (n "tsukuyomi-internal") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1kgg2l5y3y8im7ad7y66k0z0002jaa30whwa0yjrla8bdngi0zb4") (f (quote (("localmap"))))))

(define-public crate-tsukuyomi-internal-0.4.2 (c (n "tsukuyomi-internal") (v "0.4.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1y6k1ivqy0906fg821613knd7gsvg1n9zic1dgl73nz6bbprjm66") (f (quote (("localmap"))))))

(define-public crate-tsukuyomi-internal-0.4.3 (c (n "tsukuyomi-internal") (v "0.4.3") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "0jlq9dj1977g7bl569ak9a9qpjh4h0vi9zyacqq0g28gxir7awk0") (f (quote (("localmap"))))))

