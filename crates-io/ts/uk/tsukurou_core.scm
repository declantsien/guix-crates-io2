(define-module (crates-io ts uk tsukurou_core) #:use-module (crates-io))

(define-public crate-tsukurou_core-0.0.0 (c (n "tsukurou_core") (v "0.0.0") (h "0q9knirhd7hlpbj11bwib03q3syy3k6dbxjix2yv0z3fcyxysi2g")))

(define-public crate-tsukurou_core-0.0.1 (c (n "tsukurou_core") (v "0.0.1") (h "0894dmz5mcx031wm3wvg803p6z9zvkj9phbp7vdhdccnxgnc5zxf")))

