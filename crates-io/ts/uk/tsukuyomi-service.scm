(define-module (crates-io ts uk tsukuyomi-service) #:use-module (crates-io))

(define-public crate-tsukuyomi-service-0.1.0 (c (n "tsukuyomi-service") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "0n6yjs4s18pdha0iv5i90nb0x11z8rmgkwwxnvs8pz5rx1dinrvm")))

