(define-module (crates-io ts uk tsukuyomi-cors) #:use-module (crates-io))

(define-public crate-tsukuyomi-cors-0.1.0 (c (n "tsukuyomi-cors") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.4.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "0l4lmf5b44k24qb5bdnnxkkq79fyys7lsjh1vrhvgyjl9885p734")))

(define-public crate-tsukuyomi-cors-0.2.0 (c (n "tsukuyomi-cors") (v "0.2.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "tsukuyomi") (r "^0.5.0") (d #t) (k 0)) (d (n "tsukuyomi-server") (r "^0.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "0f2dmimj81jykznr27za1163ybgy83wv6876nxzdfjsfswv4ppkh")))

