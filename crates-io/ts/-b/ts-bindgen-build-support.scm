(define-module (crates-io ts -b ts-bindgen-build-support) #:use-module (crates-io))

(define-public crate-ts-bindgen-build-support-0.2.0 (c (n "ts-bindgen-build-support") (v "0.2.0") (d (list (d (n "cargo-whatfeatures") (r "^0.9.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.53") (d #t) (k 0)))) (h "1ykm0yqi88y3krszbwrdhj3fwzf7bykpy4b4w8blmylb1kz2yp8d")))

(define-public crate-ts-bindgen-build-support-0.4.0 (c (n "ts-bindgen-build-support") (v "0.4.0") (d (list (d (n "cargo-whatfeatures") (r "^0.9.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.53") (d #t) (k 0)))) (h "1ys2ks7avsyl7hda7c6s2xjp4dx5jrm9k1nkdvg5sqsvgpk9ji13")))

(define-public crate-ts-bindgen-build-support-0.5.0 (c (n "ts-bindgen-build-support") (v "0.5.0") (d (list (d (n "cargo-whatfeatures") (r "^0.9.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.53") (d #t) (k 0)))) (h "0lz33yxxgrcjdpdfzp1xdqhk8q149pbk9iz0r4g1859c5gj9cpjb")))

