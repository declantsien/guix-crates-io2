(define-module (crates-io ts -b ts-builder) #:use-module (crates-io))

(define-public crate-ts-builder-0.3.0 (c (n "ts-builder") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde" "std" "clock"))) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive" "std"))) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("std"))) (k 0)))) (h "0167w5vi6692wa1vi9a7x46q6ss4prrj1czpkq54pc52yzm1nmk9")))

