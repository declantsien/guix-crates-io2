(define-module (crates-io ts -b ts-bindgen-rt) #:use-module (crates-io))

(define-public crate-ts-bindgen-rt-0.1.0 (c (n "ts-bindgen-rt") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "1wv5lvgicbk99f4s4b9c2y1py3yv1rvgx2p3acr8c9am61h78cfz")))

(define-public crate-ts-bindgen-rt-0.2.0 (c (n "ts-bindgen-rt") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "120j39qnpj7g4ha36hxl8gn0i43padjh8hnz49y3nk7rrk66qs3w")))

(define-public crate-ts-bindgen-rt-0.4.0 (c (n "ts-bindgen-rt") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "1mlbjj37y59kv6g2aizzh7ngn8gmn3zvwpxskbc8ngfaw9k5ik6v")))

(define-public crate-ts-bindgen-rt-0.5.0 (c (n "ts-bindgen-rt") (v "0.5.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)))) (h "0n3s8frq5ipw7ynpjdqf6hwhrmxqax44454js67nca6ijf001c03")))

