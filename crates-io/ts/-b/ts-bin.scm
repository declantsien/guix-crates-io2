(define-module (crates-io ts -b ts-bin) #:use-module (crates-io))

(define-public crate-ts-bin-1.0.0 (c (n "ts-bin") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)))) (h "0iab8rvi6zqspj8yh281x5h08ipahzwryvdq2fn1klhrzsv4jc4w")))

