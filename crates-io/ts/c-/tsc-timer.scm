(define-module (crates-io ts c- tsc-timer) #:use-module (crates-io))

(define-public crate-tsc-timer-0.1.0 (c (n "tsc-timer") (v "0.1.0") (h "1hl1238my75hxjnv6c0nlc2wrs6f4vkm5va8yq3bavlgaqy3wghv")))

(define-public crate-tsc-timer-0.1.1 (c (n "tsc-timer") (v "0.1.1") (h "0al1qqlapl26qvv2n1lgvzmipadlrbz4g8i1qv9fhsc4yqqafzqg")))

(define-public crate-tsc-timer-0.1.2 (c (n "tsc-timer") (v "0.1.2") (h "1c8cs2a870bh81pwnkf7aby44bk930r653w540715f4kz65amwcs")))

(define-public crate-tsc-timer-0.1.3 (c (n "tsc-timer") (v "0.1.3") (h "17y671xb7basj372is5l7k7l69iljkcw992jgpfqbb5fb2mapap3")))

(define-public crate-tsc-timer-0.1.4 (c (n "tsc-timer") (v "0.1.4") (h "0d26m2k5zb5z6928gy72v7rnznzr1hb7rwy734spajisnwsw7ns5")))

(define-public crate-tsc-timer-0.1.5 (c (n "tsc-timer") (v "0.1.5") (h "1ipjyc54wqa58ak4dnpa0n6dfkcvs1zlyim5jl172b237fqgadbs")))

