(define-module (crates-io ts c- tsc-trace) #:use-module (crates-io))

(define-public crate-tsc-trace-0.4.0 (c (n "tsc-trace") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wmqdrbg3whih2jgrynnjnmbpa4valw8g5rsvd2f14l9ss6x9cya") (f (quote (("off") ("lfence") ("default") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

(define-public crate-tsc-trace-0.5.0 (c (n "tsc-trace") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1arxzfbm48jlw0101cmanjr8lgzkvmqw9xfih49flpxyqjkcznpl") (f (quote (("off") ("lfence") ("default") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

(define-public crate-tsc-trace-0.6.0 (c (n "tsc-trace") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "126dll8qqi0plyx4w579nfblhpliyc0r9y7wjb5d6mw2p55fy6np") (f (quote (("off") ("lfence") ("default") ("const_array") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

