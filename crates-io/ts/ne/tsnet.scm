(define-module (crates-io ts ne tsnet) #:use-module (crates-io))

(define-public crate-tsnet-0.1.0 (c (n "tsnet") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.25") (f (quote ("server"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "net"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "1hz1skmqjahhm466cal3yq2gj7alrs3i1n631202hj7pq338a15k") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:hyper"))))))

