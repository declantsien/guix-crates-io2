(define-module (crates-io ts db tsdb) #:use-module (crates-io))

(define-public crate-tsdb-0.1.0 (c (n "tsdb") (v "0.1.0") (h "0yxw69gi2px7cdy4xf9ia7yfwss7q78n0qj775s3spxrpn7wlfw0")))

(define-public crate-tsdb-0.2.0 (c (n "tsdb") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7.1") (d #t) (k 0)))) (h "182wwha30h6imaf44q6kcbwdg15akp469r3hjy3nbjrwpmpscppj")))

(define-public crate-tsdb-0.2.1 (c (n "tsdb") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7.1") (d #t) (k 0)))) (h "1djlzjmh98siqla79z0nq8nvf22snq6b0v9b07x0jhbwy8wwyyym")))

(define-public crate-tsdb-0.3.0 (c (n "tsdb") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7.1") (d #t) (k 0)))) (h "1qgswfmn574mlnbwl3b2qiwgjczm648hc1yn5wlb0m9l37b5jc5x")))

(define-public crate-tsdb-0.3.1 (c (n "tsdb") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)))) (h "1549c724aaw9zyz1gjpxikwgnsnj73psinip96vi7i6mbmvvk55q")))

(define-public crate-tsdb-0.3.2 (c (n "tsdb") (v "0.3.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)))) (h "0qp0azzqnms9yx0psdqcpan327kp9wydy7sizrky0fiss9hgdggx")))

(define-public crate-tsdb-0.3.3 (c (n "tsdb") (v "0.3.3") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)))) (h "16xrhjfv0lmp5l7d6svzb553xy2wp3a6slrckxrr50phqry6zxlb")))

