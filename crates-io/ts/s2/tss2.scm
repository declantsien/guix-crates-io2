(define-module (crates-io ts s2 tss2) #:use-module (crates-io))

(define-public crate-tss2-0.1.0 (c (n "tss2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0l9i9vzqxkdzikbc4cvyzav619ihhda6bf605b10a33i2zliazg2") (f (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1.1 (c (n "tss2") (v "0.1.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0n8s8xybwj5pdzaq3z0r0nqkn9ij353krsl419g4kszxwwkllqw7") (f (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1.2 (c (n "tss2") (v "0.1.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "19sckj5qf8z46jmsa4gm5zrqfccfb2qx2qcfpni9nx20k0rd9nkb") (f (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1.3 (c (n "tss2") (v "0.1.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0sayvswcs57i8h9qxmlcl6615rnsidjfqni3jyy3q5drbqk5d30n") (f (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

