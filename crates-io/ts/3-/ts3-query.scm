(define-module (crates-io ts #{3-}# ts3-query) #:use-module (crates-io))

(define-public crate-ts3-query-0.1.0 (c (n "ts3-query") (v "0.1.0") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "13rm9aa5abksm7qrfh2k6gzbzi8zgp1pg4hyl1kf8rlvshdm100n")))

(define-public crate-ts3-query-0.1.1 (c (n "ts3-query") (v "0.1.1") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "18c9cnf301d0sw02pkis0l6xzc9vwp73i4k8jyivm178bsq09q39")))

(define-public crate-ts3-query-0.1.2 (c (n "ts3-query") (v "0.1.2") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1ll99j3p47zma0dygihqiy4zg2bygyrkf2qa2maglf7k71ibzvbb")))

(define-public crate-ts3-query-0.1.3 (c (n "ts3-query") (v "0.1.3") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "0i74mbdzcqbijbcsgpfclp1qvjagpcn9pg04m60lihzy1na211vc")))

(define-public crate-ts3-query-0.1.4 (c (n "ts3-query") (v "0.1.4") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "18dj1axmqp3b3q6bmipxnmayn8s9a20q687c4hfmd3zqp4walhy6")))

(define-public crate-ts3-query-0.1.5 (c (n "ts3-query") (v "0.1.5") (d (list (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1safqi6h2inn62vxq53c6fisbdvcdhkd29jkr98r3kw9l1awkn9n")))

(define-public crate-ts3-query-0.1.6 (c (n "ts3-query") (v "0.1.6") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "131cyv67gvk0iaf0b23vf99d354nak037h3kay8ix7c0lxjbvwfs") (f (quote (("debug_response") ("backtrace" "snafu/backtraces")))) (y #t)))

(define-public crate-ts3-query-0.2.0 (c (n "ts3-query") (v "0.2.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1m8khmgj4vy1cxh7dn56njh2nirwawc85qpvg99h5m68qsfkl362") (f (quote (("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2.1 (c (n "ts3-query") (v "0.2.1") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1qf6m1b374agbnwj9a7ggx897gx7z79mvyhkdxi93q0dphyy1sc9") (f (quote (("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2.2 (c (n "ts3-query") (v "0.2.2") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1mw27jjqpf8ah591cpmd1zqix80jvsnws7w9zqkfn8q8ixzazjal") (f (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2.3 (c (n "ts3-query") (v "0.2.3") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1kmamy49y5hwafz7yppx2m1k54qqryw2jz7zk9m662cmxrybfsm1") (f (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3.0 (c (n "ts3-query") (v "0.3.0") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1ify9yiidm8rkz6rr5ps0xs25pw2cbvss3k8bpzqhri5gzifvsra") (f (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3.1 (c (n "ts3-query") (v "0.3.1") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0z97szwqzzhfv84khjmajys2l0qklldra23k5bn4w0g1kn5n5yfd") (f (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3.2 (c (n "ts3-query") (v "0.3.2") (d (list (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1hk20r2a5mzk3mb9sai0vnw1977985zwhwx321dsc78pjgrny85n") (f (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

