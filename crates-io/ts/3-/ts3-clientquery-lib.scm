(define-module (crates-io ts #{3-}# ts3-clientquery-lib) #:use-module (crates-io))

(define-public crate-ts3-clientquery-lib-0.0.1-alpha (c (n "ts3-clientquery-lib") (v "0.0.1-alpha") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde-teamspeak-querystring") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("net" "time" "io-util"))) (d #t) (k 0)))) (h "0r8mg3bb3qjy1pqvny6dqmvrkgga01v679mznlsa9blrav9i6f90") (f (quote (("default"))))))

(define-public crate-ts3-clientquery-lib-0.0.1-beta (c (n "ts3-clientquery-lib") (v "0.0.1-beta") (d (list (d (n "log") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde-teamspeak-querystring") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("net" "time" "io-util"))) (d #t) (k 0)))) (h "19b40mb9h948zhjnjkg6gv1anbi003xxdp2gc69rfra5vj1bvpj3") (f (quote (("server") ("default") ("client") ("all" "log" "server" "client"))))))

