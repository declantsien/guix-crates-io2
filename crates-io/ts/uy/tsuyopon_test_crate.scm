(define-module (crates-io ts uy tsuyopon_test_crate) #:use-module (crates-io))

(define-public crate-tsuyopon_test_crate-0.0.1 (c (n "tsuyopon_test_crate") (v "0.0.1") (h "1qmmzsj5wijyxrrizagm7v1a19dhykw3jz0w0ncgx780b5npk3mi") (y #t)))

(define-public crate-tsuyopon_test_crate-0.0.2 (c (n "tsuyopon_test_crate") (v "0.0.2") (h "1zslk9p1nx0dzgrp1915d5ml659zcwnxqz4p856w72mhhfyfxprl") (y #t)))

(define-public crate-tsuyopon_test_crate-0.0.3 (c (n "tsuyopon_test_crate") (v "0.0.3") (h "1b90vrx74jfflrcszn3300m19vx7d0q3f7bmf06niv873500zhmw")))

