(define-module (crates-io ts _o ts_observing_environment) #:use-module (crates-io))

(define-public crate-ts_observing_environment-0.1.0 (c (n "ts_observing_environment") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)))) (h "0fgpm5irf1vq084f0gzkig0x5k5p7zn9ij21ahkm3lcwkqhgdkyn") (y #t)))

(define-public crate-ts_observing_environment-0.2.0-alpha.1 (c (n "ts_observing_environment") (v "0.2.0-alpha.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)))) (h "1zpq16l0iq95l12skz9y5y626lp0hfgq4v6hicbw6c4zq61dg51v")))

(define-public crate-ts_observing_environment-0.2.0-alpha.2 (c (n "ts_observing_environment") (v "0.2.0-alpha.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)))) (h "0hw03k1gg8w5cwsx21ja17yrra987ag3m4yhripmlf4y4m9rl066")))

