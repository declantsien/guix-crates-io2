(define-module (crates-io ts _o ts_opentelemetry_semantic_conventions) #:use-module (crates-io))

(define-public crate-ts_opentelemetry_semantic_conventions-0.12.0-beta.1 (c (n "ts_opentelemetry_semantic_conventions") (v "0.12.0-beta.1") (d (list (d (n "ts_opentelemetry") (r "^0.20.0-beta.1") (k 0)))) (h "0kl9b7ds1cajg7in0k91xrnf0y21jwgyh2rxgch3c1navc8gmy1b") (r "1.60")))

