(define-module (crates-io ts -m ts-macros) #:use-module (crates-io))

(define-public crate-ts-macros-0.1.0 (c (n "ts-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "1ivgrcz64r38aiq443ljjrkyj9354xfmb9l94nw1czrgzza375jj")))

(define-public crate-ts-macros-0.2.0 (c (n "ts-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "0mnp880h8prgbx4yrgqrj8pqphmpz48b28ibx0cpqpf7m8wha7wy")))

(define-public crate-ts-macros-0.3.0 (c (n "ts-macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "09n2spicb87kic46lvv2z40m5z06p2rp7xxs2pjl90apbg90mc35")))

(define-public crate-ts-macros-0.4.0 (c (n "ts-macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "1hflry7ylnkf4hha92mx6d69j56d5vvh970vpcah5bdy1c48rvn5")))

(define-public crate-ts-macros-0.4.1 (c (n "ts-macros") (v "0.4.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "0yjmj1bzj5qcq0cpk512ywv721qyzm2mbwbhh3gwbfbq0ddb9xay")))

