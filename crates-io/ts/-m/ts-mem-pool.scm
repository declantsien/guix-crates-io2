(define-module (crates-io ts -m ts-mem-pool) #:use-module (crates-io))

(define-public crate-ts-mem-pool-0.1.0 (c (n "ts-mem-pool") (v "0.1.0") (h "0hn9hzwis0cgs61kr8hzampwg6klhhg3nysa1g599znqm7kvvlyq")))

(define-public crate-ts-mem-pool-0.1.1 (c (n "ts-mem-pool") (v "0.1.1") (h "09x31r57r4il20870imw37hk18d3x9xqg81r1bbv35cnl1icqsf5")))

(define-public crate-ts-mem-pool-0.1.2 (c (n "ts-mem-pool") (v "0.1.2") (h "09qsza3fq61rwa24szd0sq3bfmb3pf1dgyiralh3ccanzzk2cdix")))

(define-public crate-ts-mem-pool-0.1.3 (c (n "ts-mem-pool") (v "0.1.3") (h "09x5mrg85gyy4hnwp819xsvi97w11nvjm42gklajmj2v8hvng8ia")))

