(define-module (crates-io ts -r ts-rs-macros-serde-json) #:use-module (crates-io))

(define-public crate-ts-rs-macros-serde-json-7.0.1 (c (n "ts-rs-macros-serde-json") (v "7.0.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "1add7bwwhk10avs7f3vxlwacgjxqpqk4div2idfpqzmg1swxvk20") (f (quote (("serde-compat" "termcolor"))))))

