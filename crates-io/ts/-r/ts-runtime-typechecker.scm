(define-module (crates-io ts -r ts-runtime-typechecker) #:use-module (crates-io))

(define-public crate-ts-runtime-typechecker-0.1.6 (c (n "ts-runtime-typechecker") (v "0.1.6") (d (list (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1gl7j64a4axdfjmf74f5vajbfmxdvqzsdx5lmj4lnczlwqnfqd2n")))

