(define-module (crates-io ts s- tss-esapi-sys) #:use-module (crates-io))

(define-public crate-tss-esapi-sys-0.1.0 (c (n "tss-esapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "152w4b794y4x622psr3llihxgryq7k7c0b5g695j6vbf98j108s7") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

(define-public crate-tss-esapi-sys-0.1.1 (c (n "tss-esapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0ybj8gig849n195kgyqbcfslwkcb11kjlhs2pwgn44gp7gbrg1qs") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

(define-public crate-tss-esapi-sys-0.2.0 (c (n "tss-esapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "038y74152yprbmdnl5ppm9qncpn8lks2mbj0awp5p01wi5pf80kd") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

(define-public crate-tss-esapi-sys-0.3.0 (c (n "tss-esapi-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1w2zak11x76hi65s0irwsqwb564l8aw8p8fg8p8r9m649s8kfbqf") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

(define-public crate-tss-esapi-sys-0.4.0 (c (n "tss-esapi-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1gnf5izxh2yb44ghha7jfpqpf915614415py21295q3269avxf77") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

(define-public crate-tss-esapi-sys-0.5.0 (c (n "tss-esapi-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1dfmrbbm2834hzimvj78rhya59mv7wfhnrrfz3aw8bhwb29d2p2k") (f (quote (("generate-bindings" "bindgen")))) (l "tss2-esys")))

