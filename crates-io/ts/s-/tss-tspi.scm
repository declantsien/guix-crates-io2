(define-module (crates-io ts s- tss-tspi) #:use-module (crates-io))

(define-public crate-tss-tspi-0.0.1 (c (n "tss-tspi") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.1.1") (d #t) (k 2)))) (h "0yp24czgzlkl7kccap62iiv4cpwqa2fxzf0py18pgigi4wd2j2ik")))

