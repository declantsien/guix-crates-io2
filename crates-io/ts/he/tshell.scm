(define-module (crates-io ts he tshell) #:use-module (crates-io))

(define-public crate-tshell-0.1.0 (c (n "tshell") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0ci5vfv396wn46y4pdb7jwh9xwghi2nr0jk6c751hyqlcqj4by5m") (y #t)))

(define-public crate-tshell-0.1.1 (c (n "tshell") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1vi33668fxv5w9n03v4768b79fcl5gwigrxhw8cyjfv5g63707s3")))

(define-public crate-tshell-0.1.2 (c (n "tshell") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0bfwydjm8j0c0183ffdv69k25ywcjhmib8aka0pbiv887zszhibl")))

(define-public crate-tshell-0.1.3 (c (n "tshell") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0fizkngnflw4nk4v137w459x6gbhxs0bgqcrc7kpqaxgylwa6cx7")))

(define-public crate-tshell-0.2.1 (c (n "tshell") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "16v7bvi1km42h9vsyx820bb45vk497785s84628czv95q2xqcqfd")))

(define-public crate-tshell-0.2.2 (c (n "tshell") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0463xvzsnqaam9whcvpgpb16fi3gv3rhn3szrdx5lrqrz7lqmk58")))

