(define-module (crates-io ts md tsmd) #:use-module (crates-io))

(define-public crate-tsmd-0.1.0 (c (n "tsmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)))) (h "05q5bmbx9wpml1n09sg2b427w2jpf1krmshxrqwl7ap95k1lj2nd")))

(define-public crate-tsmd-0.1.1 (c (n "tsmd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)))) (h "0qwhyybfk20n78ax2bixh0vk4ha1dyrkamzi6c5grwkzxkg316qr")))

(define-public crate-tsmd-0.1.2 (c (n "tsmd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "fs"))) (d #t) (k 0)))) (h "17dfqcincr300v66b380fbhsklkriisafbhlxbrkl8wi9xvl3f31")))

