(define-module (crates-io ts fi tsfile) #:use-module (crates-io))

(define-public crate-tsfile-0.0.1 (c (n "tsfile") (v "0.0.1") (d (list (d (n "bit-set") (r "^0.5.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "15rfdak60jaspfb7g4nbfng2pvi773r2a8a08cjg0kq79s2m00xf")))

(define-public crate-tsfile-0.0.2 (c (n "tsfile") (v "0.0.2") (d (list (d (n "bit-set") (r "^0.5.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "snap") (r "^1.0.5") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "1xa8195hk3cmzs79flw10jf8cp81zz6w208zjik12msgv9h5jdmy")))

