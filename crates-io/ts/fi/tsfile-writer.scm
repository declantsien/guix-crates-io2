(define-module (crates-io ts fi tsfile-writer) #:use-module (crates-io))

(define-public crate-tsfile-writer-0.1.0 (c (n "tsfile-writer") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01j25d0dcrljasn34cy20pzx5fszkbdrbzbaifxa6igrb6kq3195")))

(define-public crate-tsfile-writer-0.1.1 (c (n "tsfile-writer") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09czbg43pk2lcp0fxiv5xsxrl7hbmgiwi8avq8as23xw1fk4g748")))

(define-public crate-tsfile-writer-0.1.2 (c (n "tsfile-writer") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0hlnydf36rwcb0ipp4m0m9h714c15mmz3zjanf4lr6fd5ywjw5x6")))

(define-public crate-tsfile-writer-0.1.3 (c (n "tsfile-writer") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "snap") (r "^1.0.5") (d #t) (k 0)))) (h "0ngs7jzqp3lmshxpabxn26qf7gr5f3wgb47wvwwg318mw89qg07r")))

(define-public crate-tsfile-writer-0.2.0 (c (n "tsfile-writer") (v "0.2.0") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.5") (d #t) (k 0)) (d (n "thrift") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "0aq5cv5g7dpclb3zvwpfyddm5qkwrzkw86ncfkbby9j28b4c8h85") (f (quote (("sync_sender" "thrift" "pnet" "uuid" "sha2" "hex" "crc32fast"))))))

(define-public crate-tsfile-writer-0.2.1 (c (n "tsfile-writer") (v "0.2.1") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.5") (d #t) (k 0)) (d (n "thrift") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.0") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "02hgabcl2j6pdw9aikgriw09qa0jl4ijixwjl26ki6mrw60n11iy") (f (quote (("sync_sender" "thrift" "pnet" "uuid" "sha2" "hex" "crc32fast"))))))

