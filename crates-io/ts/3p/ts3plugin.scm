(define-module (crates-io ts #{3p}# ts3plugin) #:use-module (crates-io))

(define-public crate-ts3plugin-0.1.0 (c (n "ts3plugin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)) (d (n "ts3plugin-sys") (r "^0.2") (d #t) (k 0)))) (h "10yw57wiik1vdzckg3l5912qchh96skf6pbfvc2aif0l7y22kjvd") (f (quote (("default"))))))

(define-public crate-ts3plugin-0.2.0 (c (n "ts3plugin") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)) (d (n "ts3plugin-sys") (r "^0.3") (d #t) (k 0)))) (h "126w6kvxjm5gv6qkrxirr1n174d3nkymlifm8kshn4f4q76sc8sy") (f (quote (("default")))) (y #t)))

(define-public crate-ts3plugin-0.2.1 (c (n "ts3plugin") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.6") (d #t) (k 1)) (d (n "skeptic") (r "^0.6") (d #t) (k 2)) (d (n "ts3plugin-sys") (r "^0.3") (d #t) (k 0)))) (h "0avv4mi0px0ard50r07vqnxdggccpvwlvg1snpj32grp1bmnrh4r") (f (quote (("default"))))))

