(define-module (crates-io ts #{3p}# ts3plugin-sys) #:use-module (crates-io))

(define-public crate-ts3plugin-sys-0.1.0 (c (n "ts3plugin-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qd7x2y9xl07670s6nhpjrk6a387x395ixfqyg9c61mcjjjhg8by")))

(define-public crate-ts3plugin-sys-0.2.0 (c (n "ts3plugin-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01wx2fg0hfflv61xbs28dpy9s2wgfby0czi19650g1n4482dmplk")))

(define-public crate-ts3plugin-sys-0.3.0 (c (n "ts3plugin-sys") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "0pd54d4m2gyr2npzvch781n0aah6i2rxswqw6f7lz1rjyj7p3xbw")))

(define-public crate-ts3plugin-sys-0.4.0 (c (n "ts3plugin-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1lkjkqijb8zph6b1x04ippls7bnb6k66p80pvwa9kifm4q38cayl")))

