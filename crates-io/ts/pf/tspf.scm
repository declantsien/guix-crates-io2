(define-module (crates-io ts pf tspf) #:use-module (crates-io))

(define-public crate-tspf-0.1.0 (c (n "tspf") (v "0.1.0") (h "0gxdr9gc9l4kzni113kmc1yzri7wax28qkrn2m57asgvpdv0zgha")))

(define-public crate-tspf-0.1.1 (c (n "tspf") (v "0.1.1") (h "1yqgq6ggfh9l9wr5ydfnjnlxhq9xgdfwn4kg43llxgfdf1yfr156")))

(define-public crate-tspf-0.2.0 (c (n "tspf") (v "0.2.0") (h "0avnh6x9bn4ic22dn26rsbpsm2spic4mny95x9zvkdd6fr5swdm5")))

(define-public crate-tspf-0.3.0 (c (n "tspf") (v "0.3.0") (d (list (d (n "getset") (r "^0.1.1") (d #t) (k 0)))) (h "02nkb2884r5ql0bkaphgdxy340p5n35zi8ihk1pjqq21hmhadag1")))

(define-public crate-tspf-0.3.1 (c (n "tspf") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)))) (h "1vxg6znh7swi70msjhng6fcaxf7l2k7hzqcqwmqf1iijcl39wdfd")))

