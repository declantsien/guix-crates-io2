(define-module (crates-io ts _s ts_static) #:use-module (crates-io))

(define-public crate-ts_static-0.1.0 (c (n "ts_static") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "0nz6s2dav30zkyf4zam4j9n1qpnc0y2b7z9vnydhwfabk5q21963")))

(define-public crate-ts_static-0.1.1 (c (n "ts_static") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "0k0l6wj6lvfpzwb3mbayqdz806ywlaaa5hcasrdvq01mb02wp51r")))

(define-public crate-ts_static-0.1.2 (c (n "ts_static") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "16br3mzdzlbdpc7j1isakxcxnccfk88aaskd4fsbsciainm5lfkq")))

(define-public crate-ts_static-0.1.3 (c (n "ts_static") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "07pj9acjrj95jjmcz8f9k771rnpvbyr7d34f7hnwlvsd59my9x4d")))

(define-public crate-ts_static-0.1.4 (c (n "ts_static") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "0jwcajfgjfjmbi8gl78kpgq0jl28dflr00nklg2zaq3agwdhymrh")))

(define-public crate-ts_static-0.1.5 (c (n "ts_static") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "1dgsv55ipc3pl5n9n7vk7bybqcynjslqc463vkw7f1ak4pybzzfh")))

(define-public crate-ts_static-0.2.0 (c (n "ts_static") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "04m9n1p16xsp3k143xki2b4ahqckf1f20w43fsk2f9m7mxd7qylp")))

(define-public crate-ts_static-0.3.0 (c (n "ts_static") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "0y375pq3fqhhprv82d3k7mva1xifdic7w0dj7vfxw1i059yb2hcp")))

