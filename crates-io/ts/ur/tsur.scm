(define-module (crates-io ts ur tsur) #:use-module (crates-io))

(define-public crate-tsur-0.1.0 (c (n "tsur") (v "0.1.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)))) (h "1sqvzaxmyasmmyxsl8ss5bmfk7kxz1zdpyab9s1nhpxf68a0nhb3")))

(define-public crate-tsur-0.1.1 (c (n "tsur") (v "0.1.1") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)))) (h "0k1dsaync74bjz0fmx928xrni9gaq02dcab2ik6h5mwxbh3g285c")))

(define-public crate-tsur-0.1.2 (c (n "tsur") (v "0.1.2") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)))) (h "1yknphlgfsv5zkkpaiby7i5nnz6lrzm3k56988ikd9acn79rk7n0")))

(define-public crate-tsur-0.1.3 (c (n "tsur") (v "0.1.3") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)))) (h "1skksz23z2fzxgvh78a1hd4ys86b4fjywd1f63vh7kp6wsi30m71")))

(define-public crate-tsur-0.1.4 (c (n "tsur") (v "0.1.4") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)))) (h "1amshs5s0b41mqr9j0ashlbwmnc04aqc95sy2ricih98032f7kf5")))

(define-public crate-tsur-0.1.6 (c (n "tsur") (v "0.1.6") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)))) (h "0nzds9wxqdbllyby387xah2igimzr08jmxg3cl0yh7bpvyjgadzr")))

