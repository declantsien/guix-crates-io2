(define-module (crates-io ts ki tskit-derive) #:use-module (crates-io))

(define-public crate-tskit-derive-0.1.0 (c (n "tskit-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "135adi66b99x769xblybx1jgd8sh4zgw5si5qr366i7jzk58vhcn")))

(define-public crate-tskit-derive-0.2.0 (c (n "tskit-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gzk4l4j62gmdp8nn1xrqfzqjl1brlwimpvpswy3s5zf12424hmi")))

