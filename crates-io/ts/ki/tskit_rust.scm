(define-module (crates-io ts ki tskit_rust) #:use-module (crates-io))

(define-public crate-tskit_rust-0.1.0 (c (n "tskit_rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "056rbzhw007wy0q0va21jcqm2j9ikjzs9gyn9ppfdg1vbk3jap75")))

