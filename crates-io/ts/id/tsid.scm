(define-module (crates-io ts id tsid) #:use-module (crates-io))

(define-public crate-tsid-0.0.1 (c (n "tsid") (v "0.0.1") (h "0vd74650fnssyn45xgyrmd1sirxrafjj3pb5h9ygzf448810p78c")))

(define-public crate-tsid-0.0.2 (c (n "tsid") (v "0.0.2") (h "119mrda7v20vvxnsrwrcabxzal664xjj6dhgz5zi0cwqnrwj0a91")))

(define-public crate-tsid-0.0.3 (c (n "tsid") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0lns5pgwq38jgn9is99a4n6hc41wfsxyhs04zf78lahrk5sfpds2")))

(define-public crate-tsid-0.0.4 (c (n "tsid") (v "0.0.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0njhfy00d1xzqc94pm06xg38j74k6q6bsj8d8d00wgl5pgbc60h6") (y #t)))

(define-public crate-tsid-0.0.5 (c (n "tsid") (v "0.0.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "067z79pjzmnzm3k5p8pc665x3rvqmjh9fw335nxl9ww5vqp56x05") (y #t)))

(define-public crate-tsid-0.0.6 (c (n "tsid") (v "0.0.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "113q1lfc54ssfl5g8cc53j8iswrnx801irca5qdqkc24w0xp15kb") (f (quote (("display") ("default" "display" "debug") ("debug" "display"))))))

(define-public crate-tsid-0.1.0 (c (n "tsid") (v "0.1.0") (d (list (d (n "bson") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.7.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1pfaigq6w13q03j0c8prahzxnx7630l9sm0rh9n2bxjxfvv2yz69") (f (quote (("serde_as_string" "serde") ("display") ("default" "display" "debug" "bson" "serde" "bson_as_string" "serde_as_string") ("debug" "display") ("bson_as_string" "bson")))) (s 2) (e (quote (("serde" "dep:serde" "display") ("bson" "dep:bson" "display"))))))

(define-public crate-tsid-0.1.1 (c (n "tsid") (v "0.1.1") (d (list (d (n "bson") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.7.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0hid07wf1kkjazfxlrdrmb15jdxjz1niyhy7b5vz4gifc8av15v9") (f (quote (("serde_as_string" "serde") ("display") ("default" "display" "debug") ("debug" "display") ("bson_as_string" "bson")))) (s 2) (e (quote (("serde" "dep:serde" "display") ("bson" "dep:bson" "display"))))))

(define-public crate-tsid-0.1.2 (c (n "tsid") (v "0.1.2") (d (list (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.7.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1y90c47adap37a533was23yhv4l04j8asp6171k7biny4y2mvnlj") (f (quote (("serde_as_string" "serde") ("default" "debug") ("debug") ("bson_as_string" "bson"))))))

(define-public crate-tsid-0.2.0 (c (n "tsid") (v "0.2.0") (d (list (d (n "bson") (r "^2") (o #t) (d #t) (k 0)) (d (n "bson") (r "^2.7.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09r46z7amcplihajr896q33fqbdlkcsy7z0jjww772mkkas4i9ch") (f (quote (("serde_as_string" "serde") ("default" "debug") ("debug") ("bson_as_string" "bson")))) (r "1.67.1")))

