(define-module (crates-io ts yn tsync) #:use-module (crates-io))

(define-public crate-tsync-1.0.0 (c (n "tsync") (v "1.0.0") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bwrrpzgh9w79vzhj6r1vv6j1484k310hwi9wafplmpwk2jav2q6")))

(define-public crate-tsync-1.0.1 (c (n "tsync") (v "1.0.1") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xnqjpjx7sspf50bfwhn7yc30md0vnxkhpvsd6p52mv6dyrxpggd")))

(define-public crate-tsync-1.0.2 (c (n "tsync") (v "1.0.2") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zn3d47c00m0i2qilyj6r5kjcsbmnb7jgwgz5jix7xb5hcmy2h04")))

(define-public crate-tsync-1.0.3 (c (n "tsync") (v "1.0.3") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qcgxv5cz0g5l4w69z6qnzd8vvf4dn6ra4nspx2k3gsq1wxbz1c0")))

(define-public crate-tsync-1.0.4 (c (n "tsync") (v "1.0.4") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07bpz2gy3x1nr7h0g57q77f741sn19kzlipslnbn0abmgpgn6b2k")))

(define-public crate-tsync-1.1.0 (c (n "tsync") (v "1.1.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jn6yvb5yw0x38ks528k4ary8ckyf5cmvsyv88j6lbymg91z6wgj")))

(define-public crate-tsync-1.2.0 (c (n "tsync") (v "1.2.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0g1m1q3qg5325rvj6mfidq2rcm69a83k7sjxaca4kkm4d43mr9p5")))

(define-public crate-tsync-1.2.1 (c (n "tsync") (v "1.2.1") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1n2z614lxghkqp4p14xlabnk973prgm5bv1b3vczgjvw54907azh")))

(define-public crate-tsync-1.3.0 (c (n "tsync") (v "1.3.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13b5rzp8na8s368dgr0sczw2wawla9s8q0ssach5fn1l4vl6hyk1")))

(define-public crate-tsync-1.4.0 (c (n "tsync") (v "1.4.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1p2mp299grg8nzjmr7iwssf7sdg59z62l31x6nwsayb0a5w92yrf")))

(define-public crate-tsync-1.4.1 (c (n "tsync") (v "1.4.1") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "028k57q397z2l7yw9jlspz09hn629vn87vwdfxg4g49w7f7x9xbr")))

(define-public crate-tsync-1.5.0 (c (n "tsync") (v "1.5.0") (d (list (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "08259zg4ca9s9xj1fps6p9lc9fx6hry6ma60kzdwdnj432lhcsbr")))

(define-public crate-tsync-1.6.0 (c (n "tsync") (v "1.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13m9dzrvpxwnhkbcy3vsj2mbc8bxmlnbrbm81xyvz9dnm5zf4sds")))

(define-public crate-tsync-1.6.1 (c (n "tsync") (v "1.6.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1p26w26mpwbdiraxz66bva3yvslqv1apfnhjp9cq88z4bz2agm7w")))

(define-public crate-tsync-1.6.2 (c (n "tsync") (v "1.6.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1dwcfs5pniyycfmwp94491i9ki3874rqrcpdbnqbb9041iwxq87k")))

(define-public crate-tsync-1.6.3 (c (n "tsync") (v "1.6.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "01pa7rk5dlyycz6s6jrjblcrbwfm36bx6yxd923gzbq3a00q1xnd")))

(define-public crate-tsync-1.7.0 (c (n "tsync") (v "1.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0jvzdv2hminjrnbn18xz789pll43n3hn70n09l8ka8lr13c9h3sl")))

(define-public crate-tsync-2.0.0 (c (n "tsync") (v "2.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "04bfr0hil2hddww75crinph79xwsw53gfbjbk7yd402cmivz5spq")))

(define-public crate-tsync-2.0.1 (c (n "tsync") (v "2.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1qbiy3ra4v01jxb6w43lvbscn5q05ivsaw8r1fpaifnrp7qghdga")))

(define-public crate-tsync-2.1.0 (c (n "tsync") (v "2.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "state") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0lc05xzqp7hn9z868l657w45kygkcq3lqamglc187l83rvn92kav")))

(define-public crate-tsync-2.2.0 (c (n "tsync") (v "2.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "state") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tsync-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0ivr46v1b8rppl75ay9f8y6p9nznixyfssfnsvdmvncdbqdh9r8s")))

