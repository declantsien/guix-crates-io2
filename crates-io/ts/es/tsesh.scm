(define-module (crates-io ts es tsesh) #:use-module (crates-io))

(define-public crate-tsesh-0.1.2 (c (n "tsesh") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "02mfvdf2npqy3dnzyyg3i1wbfi624jaibgyh7v0n34vr9qnvm0q8")))

