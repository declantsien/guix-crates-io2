(define-module (crates-io ts tr tstr_proc_macros) #:use-module (crates-io))

(define-public crate-tstr_proc_macros-0.1.0 (c (n "tstr_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("parsing"))) (o #t) (k 0)))) (h "19198zivdrvm7036appaqzbg5ygqf975qy4climp1crdky3z00rh") (f (quote (("syn_" "syn" "proc_macro2_") ("proc_macro2_" "proc-macro2") ("default") ("const_generics"))))))

(define-public crate-tstr_proc_macros-0.2.0 (c (n "tstr_proc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("parsing"))) (o #t) (k 0)))) (h "0ashgx9i94qpjax7lypfdzg3jgrin80raslcjn3dyjga873bbz5s") (f (quote (("syn_" "syn" "proc_macro2_") ("proc_macro2_" "proc-macro2") ("min_const_generics") ("default") ("const_generics" "min_const_generics"))))))

(define-public crate-tstr_proc_macros-0.2.1 (c (n "tstr_proc_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("parsing"))) (o #t) (k 0)))) (h "18l2bhylp0y0jydhck9f282lw20rp6rzwq4x6xliczxmy5j5gckf") (f (quote (("syn_" "syn" "proc_macro2_") ("proc_macro2_" "proc-macro2") ("min_const_generics") ("default") ("const_generics" "min_const_generics"))))))

(define-public crate-tstr_proc_macros-0.2.2 (c (n "tstr_proc_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("parsing"))) (o #t) (k 0)))) (h "0yklq0k0s3c4y0k5f0qm13lw7nvz5z97x3yhmyw1if0cdc3250g7") (f (quote (("syn_" "syn" "proc_macro2_") ("proc_macro2_" "proc-macro2") ("min_const_generics") ("default") ("const_generics" "min_const_generics"))))))

