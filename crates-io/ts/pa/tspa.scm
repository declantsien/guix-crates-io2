(define-module (crates-io ts pa tspa) #:use-module (crates-io))

(define-public crate-tspa-0.1.0 (c (n "tspa") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0-alpha.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)))) (h "0k0s6xplprn46vv8q3yq0r2n3j58s11qis3bizjw0ra5imbxkxqn") (y #t)))

(define-public crate-tspa-0.1.1 (c (n "tspa") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0-alpha.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)))) (h "1bp91d1g6qk20rg7nf1fyrlb3hb4f1f2a96ggvwdpqz0hxngnbfv")))

