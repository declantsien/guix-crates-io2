(define-module (crates-io ts l2 tsl256x) #:use-module (crates-io))

(define-public crate-tsl256x-0.1.0 (c (n "tsl256x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1926hcbix6b54n6rd3qv4j08l6f6374xpw09a90cb8bk5dcl1hhy")))

