(define-module (crates-io ts l2 tsl2591) #:use-module (crates-io))

(define-public crate-tsl2591-0.1.0 (c (n "tsl2591") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1g7kkaigq19cvmqzrzxwmz71xi6z679bdnl27x1lracdlgwah879")))

(define-public crate-tsl2591-0.2.0 (c (n "tsl2591") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1xv0v6i11xv7wqpahiasl5lcq037ziivayjx19rbyzy6dxqv5yqb")))

