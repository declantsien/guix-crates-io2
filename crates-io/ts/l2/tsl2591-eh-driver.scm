(define-module (crates-io ts l2 tsl2591-eh-driver) #:use-module (crates-io))

(define-public crate-tsl2591-eh-driver-0.3.0 (c (n "tsl2591-eh-driver") (v "0.3.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.10") (d #t) (k 0)))) (h "12287jvrgj9q0dacvs1h4cnw7hzbn0h2ryxzqn4pg4dv7347nlhm")))

(define-public crate-tsl2591-eh-driver-0.4.0 (c (n "tsl2591-eh-driver") (v "0.4.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.10") (d #t) (k 0)))) (h "0gd5dw69rhq80z11x42p18vqvd5h902h990dl95jjdrqvfvqh2a1")))

(define-public crate-tsl2591-eh-driver-0.5.0 (c (n "tsl2591-eh-driver") (v "0.5.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1l0s5fgqxa67f6s2i06fxc6cz3z5myv5i09hb1x9jbr90mhc5sc1")))

