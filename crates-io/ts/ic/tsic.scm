(define-module (crates-io ts ic tsic) #:use-module (crates-io))

(define-public crate-tsic-0.1.0 (c (n "tsic") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "10svjvnzz009zg976545np1kr8cr08crqgarnlj9vqjrcwd40258")))

(define-public crate-tsic-0.2.0 (c (n "tsic") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0bfhvz7mask6z09kjxbaa9ydn9xd14q6mjxxwwrd0lscrxhak113")))

(define-public crate-tsic-0.2.1 (c (n "tsic") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "19ms5fpwyw84h4x8d0q8ypnmwiqadyv5gkc7i7xldyh3cwjla0fc")))

