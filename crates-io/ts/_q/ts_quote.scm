(define-module (crates-io ts _q ts_quote) #:use-module (crates-io))

(define-public crate-ts_quote-0.1.0 (c (n "ts_quote") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0dzvndnmk8514zv7gknixcnignly2cqac9z0c29vyi5clqr1wa23")))

(define-public crate-ts_quote-0.1.1 (c (n "ts_quote") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.1") (d #t) (k 0)))) (h "01xjd5l4ynyybw188wg4pnq1fsjx4jj34j10chlrgmcjpa47n141")))

(define-public crate-ts_quote-0.1.2 (c (n "ts_quote") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.2") (d #t) (k 0)))) (h "12jac1np2v5lnx2yx20hli9xd5v6n7imlbkm5rridma4qvjfkjgk")))

(define-public crate-ts_quote-0.1.3 (c (n "ts_quote") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0072bdvvkfdjyva06jmbyh6n8km3w2cvcx6kd898n4xbb25ixmw9")))

(define-public crate-ts_quote-0.2.0 (c (n "ts_quote") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1kwqjqw3sg9bzagbcw2gbhjchan5qrz8y7n4nvrdanp922nkvgxw")))

(define-public crate-ts_quote-0.2.1 (c (n "ts_quote") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "deno_ast") (r "^0.31.6") (d #t) (k 0)) (d (n "dprint-plugin-typescript") (r "^0.88.3") (d #t) (k 0)) (d (n "ts_quote_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0mgcy46g0l538ai76p4rxhww5hhkrj5mzfq24hx5q48g2gaif73d")))

