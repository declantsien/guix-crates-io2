(define-module (crates-io ts if tsify-next-macros) #:use-module (crates-io))

(define-public crate-tsify-next-macros-0.5.0 (c (n "tsify-next-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0h1706cxim9q4gq32qgbhb9mcjbgzriibymj79nd9niclrq3h6cw") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-next-macros-0.5.1 (c (n "tsify-next-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0fbs27064zbkwjirqyj2acb9qbgnlb0g2wp0ahpb97dz468y2i74") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-next-macros-0.5.2 (c (n "tsify-next-macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "095m67gf8q80kn6qm8aqaxxv9427zhvwb78a5az9wxgl0r66rx36") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-next-macros-0.5.3 (c (n "tsify-next-macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0glmfjzyqiirkp7zfzyzw2qcx99mxpa5xmbp8xahswn6mj01nnv1") (f (quote (("wasm-bindgen") ("json") ("js"))))))

