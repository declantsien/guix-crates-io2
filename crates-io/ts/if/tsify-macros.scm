(define-module (crates-io ts if tsify-macros) #:use-module (crates-io))

(define-public crate-tsify-macros-0.1.0 (c (n "tsify-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "1aky6cljw145xfc18pq1ks6cjw2zsrr51llplvmzp9ya4nafjvmn") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.1.1 (c (n "tsify-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "1vjcafsmibvbywcgmkq9wayagmfhnqq6zzkzq6a3p7xmglk4wrv5") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.2.0 (c (n "tsify-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1c2l03y1fj3kvrlnxda6k56f16bjyfj3h9r82r13zjk1m47zs7lw") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.3.0 (c (n "tsify-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ycam3559b1fq7p32qqdjr2q9cjqllbqjs839vbwz4jq8jqrzvww") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.3.1 (c (n "tsify-macros") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sxnwlql7g386hw4cvcc3p2maad8v3h9v9ixa29f6qym07qdh0z0") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.4.0 (c (n "tsify-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06a17zw81adzf4zbdnzrza5iqmcyfb4pf0x8ahaj1l84xdxc0s15") (f (quote (("wasm-bindgen-impl"))))))

(define-public crate-tsify-macros-0.4.1 (c (n "tsify-macros") (v "0.4.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p3sw9wcgyl7zgm8fpgwgngfrdp6fc7laa5cygxb3z0xbzmd6vib") (f (quote (("wasm-bindgen"))))))

(define-public crate-tsify-macros-0.4.2 (c (n "tsify-macros") (v "0.4.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08762wsap5gvp9d2hmirizy13qan5bxa81c7l35ar3vhw04iwcbj") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-macros-0.4.3 (c (n "tsify-macros") (v "0.4.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lm1b7mm4w3l17ybdq60f75i69p7yjz0zqwcr31biimic91badyn") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-macros-0.4.4 (c (n "tsify-macros") (v "0.4.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wv3z9v21z3m4w206h3s43jf2imx4yvhwjv76j3dsgjyjnhg57df") (f (quote (("wasm-bindgen") ("json") ("js"))))))

(define-public crate-tsify-macros-0.4.5 (c (n "tsify-macros") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0irvzmypvc1gmxm29bfl995dk423az4b8in2qazmjgjbjpqb153s") (f (quote (("wasm-bindgen") ("json") ("js"))))))

