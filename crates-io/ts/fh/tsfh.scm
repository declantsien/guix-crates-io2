(define-module (crates-io ts fh tsfh) #:use-module (crates-io))

(define-public crate-tsfh-0.75.0 (c (n "tsfh") (v "0.75.0") (d (list (d (n "embed-resource") (r "^2.2.0") (d #t) (t "cfg(windows)") (k 1)))) (h "1xq3zxwjvy150hs73zhas9kj0gw8p38bam3knckxvih3vkdmv5yj") (y #t) (r "1.69")))

