(define-module (crates-io ts #{3_}# ts3_derive) #:use-module (crates-io))

(define-public crate-ts3_derive-0.1.0 (c (n "ts3_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (d #t) (k 0)))) (h "12f0yzl95hq7gidgczn4q331b3l799kfrdfnk861m3i75lksbcmw")))

(define-public crate-ts3_derive-0.1.2 (c (n "ts3_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (d #t) (k 0)))) (h "110py7dhjbkhpjfr4didn4chqfvpijcnydrdflm53j1xi40y2wkp")))

(define-public crate-ts3_derive-0.2.0 (c (n "ts3_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (d #t) (k 0)))) (h "05cwqwa2smx0f0q0mandb3yfxr49qc0b06a29y0kfljkmhf16dci")))

(define-public crate-ts3_derive-0.4.0 (c (n "ts3_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (d #t) (k 0)))) (h "0fdzaz4zpbpsls6chblnyn2y4l7yn0jxx218av2ypxkwvfgy4hfd")))

