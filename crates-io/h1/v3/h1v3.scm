(define-module (crates-io h1 v3 h1v3) #:use-module (crates-io))

(define-public crate-h1v3-0.1.0 (c (n "h1v3") (v "0.1.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ggez") (r "^0.7") (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "h1v3-logic") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "067nx3fnbavcfbplmzkglx63lz9w2c25q0c05rz7r5c8m2whmix9") (y #t) (r "1.56")))

(define-public crate-h1v3-0.2.0 (c (n "h1v3") (v "0.2.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ggez") (r "^0.7") (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "h1v3-logic") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08c7rz8vih8mjxil7211qv3dk58vq9pcmxj0xsdlbx554hfj5kf9") (y #t) (r "1.56")))

(define-public crate-h1v3-0.3.0 (c (n "h1v3") (v "0.3.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "ggez") (r "^0.7") (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "h1v3-logic") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1547742sczpjkp7cpqfx5ivhjwn9anhyvk19fd1gcag9jsvm47mk") (y #t)))

