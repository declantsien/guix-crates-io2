(define-module (crates-io h1 v3 h1v3-logic) #:use-module (crates-io))

(define-public crate-h1v3-logic-0.1.0 (c (n "h1v3-logic") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ilvlcnq0ffa21sp7xvf68gbfkavrjg7v5w3lca6blak3rh5v8cg") (y #t) (r "1.56")))

(define-public crate-h1v3-logic-0.2.0 (c (n "h1v3-logic") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1f22jvh7nrsz8fq2cmg8hwi6ndpzl09gq0rczj5x3l8j6d10cphq") (y #t) (r "1.56")))

(define-public crate-h1v3-logic-0.3.0 (c (n "h1v3-logic") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0awlf7gkwziilphh2rgimrfxsjayhqb2b1lgbkqd7gana5ccqzx4") (y #t) (r "1.56")))

