(define-module (crates-io j2 #{53}# j2534) #:use-module (crates-io))

(define-public crate-j2534-0.1.0 (c (n "j2534") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "1yc1qsvz4spzp6fkg8la6dk9jb13vw6g7jdd1fq0ri5w6d28wipx")))

(define-public crate-j2534-0.1.1 (c (n "j2534") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0x6ba1d3x9529803gk1836n2a5iqp0ggh1gr7rixwqn6jvhxmy6l")))

(define-public crate-j2534-0.1.2 (c (n "j2534") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1529kydppvsvprjz8fcxy5sqkniyhl26c8in08yn1lfxm6lzbxxa")))

(define-public crate-j2534-0.2.0 (c (n "j2534") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "0zdjq484ck15chh87g8gd17iym2x267j5saxm2pxp4mh3hcvnzzr") (y #t)))

(define-public crate-j2534-0.2.1 (c (n "j2534") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "03nmc76gqjsr3vpiflvcsbp5m3mij8yacskln043gdx26zgsc49a") (y #t)))

(define-public crate-j2534-0.2.2 (c (n "j2534") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "14zny8f2wz1jla1f61j1iciqkjlx54g9y5dy9yn7nb6wpnq630b2")))

(define-public crate-j2534-0.2.3 (c (n "j2534") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "1c23597235fgqi3lc0jbbxp5h1khfi4g2hni997ggln2d3p7p3m4")))

(define-public crate-j2534-0.2.4 (c (n "j2534") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (k 0)))) (h "15azsv7sz4kqs3ndcf58m5kx4sf4rkgyaf9g92qk2iw307vmm8ql")))

(define-public crate-j2534-0.3.0 (c (n "j2534") (v "0.3.0") (d (list (d (n "bitflags") (r ">=1.2.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "libloading") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "winreg") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "07wmk53ar47iks13ph8izrlcz8rifv7i9pywq4bmrcrzbdzhc1vh")))

(define-public crate-j2534-0.3.1 (c (n "j2534") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winreg") (r "^0.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0lpxs5ls34lcwfrqh01w55dzasxg9154ai8hbvs61hm3llnfd739")))

