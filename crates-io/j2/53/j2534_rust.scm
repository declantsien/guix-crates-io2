(define-module (crates-io j2 #{53}# j2534_rust) #:use-module (crates-io))

(define-public crate-j2534_rust-1.0.0 (c (n "j2534_rust") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "118lwa72yggky6r16gckyzb3y1i4csa9x545j1him7vffp06j4sq")))

(define-public crate-j2534_rust-1.0.1 (c (n "j2534_rust") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fj7rgq07i9rzsjj4w4l07chcax7bl4sdvzb2msgbys98dy4mn36")))

(define-public crate-j2534_rust-1.5.0 (c (n "j2534_rust") (v "1.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0i5p1xjhb20syrz0b8skmjrign20384w7vzjxm8bma2d3gw5z6yv")))

