(define-module (crates-io j2 -g j2-gba-tool) #:use-module (crates-io))

(define-public crate-j2-gba-tool-0.1.0 (c (n "j2-gba-tool") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "image") (r "^0.21.1") (d #t) (k 0)))) (h "02ln3r3pllyci7fmzwl0pz0k4h42qjyb8rgm071xa0ysyx4bncbi")))

