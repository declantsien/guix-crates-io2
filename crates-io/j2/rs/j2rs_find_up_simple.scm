(define-module (crates-io j2 rs j2rs_find_up_simple) #:use-module (crates-io))

(define-public crate-j2rs_find_up_simple-0.0.1 (c (n "j2rs_find_up_simple") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lets_find_up") (r "^0.0.3") (d #t) (k 2)) (d (n "sugar_path") (r "^0.0.12") (d #t) (k 2)))) (h "1kvq9ccyllp2zm8ci38myhy75lw0zrwrcgdrd1mcygkxlmh4gl02")))

(define-public crate-j2rs_find_up_simple-0.0.2 (c (n "j2rs_find_up_simple") (v "0.0.2") (d (list (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "lets_find_up") (r "^0.0.3") (d #t) (k 2)) (d (n "sugar_path") (r "^0.0.12") (d #t) (k 2)))) (h "1yiwaw25z4yrbwjx7mga827lbc3dj086d8amcw76dfca43w7y6mf")))

