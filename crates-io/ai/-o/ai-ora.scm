(define-module (crates-io ai -o ai-ora) #:use-module (crates-io))

(define-public crate-ai-ora-0.1.0 (c (n "ai-ora") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (f (quote ("cosmwasm_1_3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.5") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "cw2") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dkc5b2mmf8qgvkkfiljxq5v85g3nlghgmrkd716c0n62v5vwhb4") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

