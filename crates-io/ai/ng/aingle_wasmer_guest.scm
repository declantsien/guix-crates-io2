(define-module (crates-io ai ng aingle_wasmer_guest) #:use-module (crates-io))

(define-public crate-aingle_wasmer_guest-0.0.1 (c (n "aingle_wasmer_guest") (v "0.0.1") (d (list (d (n "aingle_middleware_bytes") (r "=0.0.3") (d #t) (k 0)) (d (n "aingle_wasmer_common") (r "=0.0.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mvyy253qhngv0wmv31j4pnj2k9cl9dq1k1gxxqvkr2g2a4w9p73")))

