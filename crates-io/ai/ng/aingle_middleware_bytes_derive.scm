(define-module (crates-io ai ng aingle_middleware_bytes_derive) #:use-module (crates-io))

(define-public crate-aingle_middleware_bytes_derive-0.0.1 (c (n "aingle_middleware_bytes_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gbsiyx9n9i861cb935jphi75vp2hncnlwd0gkn1017fvhkszvlc")))

(define-public crate-aingle_middleware_bytes_derive-0.0.2 (c (n "aingle_middleware_bytes_derive") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1iq09dyb09xwzyzrflsfi44f3cyj5r1pzr85g629b1hs0wrc622h")))

(define-public crate-aingle_middleware_bytes_derive-0.0.3 (c (n "aingle_middleware_bytes_derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fjcpbrh22p6dyj5z2jdq8vl4a6zpsaf3s1jrxndgmyhvqb1xaq5")))

