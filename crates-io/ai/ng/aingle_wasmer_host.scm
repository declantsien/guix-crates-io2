(define-module (crates-io ai ng aingle_wasmer_host) #:use-module (crates-io))

(define-public crate-aingle_wasmer_host-0.0.1 (c (n "aingle_wasmer_host") (v "0.0.1") (d (list (d (n "aingle_middleware_bytes") (r "=0.0.3") (d #t) (k 0)) (d (n "aingle_wasmer_common") (r "=0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "=0.16.2") (d #t) (k 0)) (d (n "wasmer-runtime-core") (r "=0.16.2") (d #t) (k 0)))) (h "1xmliw3iywcjzr04jlrbrk9zm75lc86cbs76z7mf7yvbd5zqz2xa")))

