(define-module (crates-io ai ng aingle_wasmer_common) #:use-module (crates-io))

(define-public crate-aingle_wasmer_common-0.0.1 (c (n "aingle_wasmer_common") (v "0.0.1") (d (list (d (n "aingle_middleware_bytes") (r "=0.0.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "1hym3m4d0n4xjdmryyiy5k8x704ycmn9f9s2b2id59a287lhawgj")))

