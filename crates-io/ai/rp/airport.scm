(define-module (crates-io ai rp airport) #:use-module (crates-io))

(define-public crate-airport-0.1.0 (c (n "airport") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jgdfagicrzav5vz1l0rq2bx647d61jzv3lhwgbdpv8dyvxb7nra")))

(define-public crate-airport-0.1.1 (c (n "airport") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dhvww10dhablipaw0ngadizz6p0fpd5vyjf8gaxsb1337wq2455")))

(define-public crate-airport-0.1.2 (c (n "airport") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04y7kmk1gf7cp76hffjlqzzlmgz5rg82rjaybbcfxw44plkls6rb")))

