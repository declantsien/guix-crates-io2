(define-module (crates-io ai rp airports) #:use-module (crates-io))

(define-public crate-airports-0.0.1 (c (n "airports") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1h98vlp9libn4xfyvnmbaw7sfwayrkbqz7wi9mb7fbsch098b73m")))

(define-public crate-airports-0.0.2 (c (n "airports") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1d39avjal6671vylaricqj01fkczrzr8943f069dcwzwvwiinv30")))

