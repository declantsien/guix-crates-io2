(define-module (crates-io ai nt aint) #:use-module (crates-io))

(define-public crate-aint-0.1.0 (c (n "aint") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18rqlyd9kpqqjmvnr2gyqmzbamk7vhmx083xj5gjqn95n365z935") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("num" "dep:num-traits" "dep:num-integer"))))))

