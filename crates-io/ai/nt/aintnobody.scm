(define-module (crates-io ai nt aintnobody) #:use-module (crates-io))

(define-public crate-aintnobody-0.0.0 (c (n "aintnobody") (v "0.0.0") (d (list (d (n "rodio") (r "^0.17.3") (f (quote ("minimp3"))) (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "14i31dldmn7crfpwwqqxik62nwmdby189bx0dxydzzmx9bh83nb1")))

(define-public crate-aintnobody-1.0.1 (c (n "aintnobody") (v "1.0.1") (d (list (d (n "rodio") (r "^0.17.3") (f (quote ("minimp3"))) (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1vics7adlxh3mrwmwwbbkdvqh3x5y05h4qf2ajd0cabps2zgrs6b")))

(define-public crate-aintnobody-1.1.1 (c (n "aintnobody") (v "1.1.1") (d (list (d (n "rodio") (r "^0.17.3") (f (quote ("minimp3"))) (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1z8hkkjkr151qjc8d7w2rzjsql4z2n46dghq4pfzza7lxhd5ciab")))

