(define-module (crates-io ai nc ainconv) #:use-module (crates-io))

(define-public crate-ainconv-0.1.0 (c (n "ainconv") (v "0.1.0") (h "0nb2fw8gfb4fzb1gvndmjmpq4b94w7x9if75j75i5gkcq2rnizjv")))

(define-public crate-ainconv-0.1.1 (c (n "ainconv") (v "0.1.1") (h "009bql7lq8yqli27mj2gns5mqpyg97n0yp5s50rhjgwpna46zl57")))

