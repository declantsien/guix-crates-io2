(define-module (crates-io ai vm aivm_train) #:use-module (crates-io))

(define-public crate-aivm_train-0.1.0 (c (n "aivm_train") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0rh05c3rcrn1v0m5p2wxmbrj8jf8pi71dl3i159asvk0lp7p6xv0")))

