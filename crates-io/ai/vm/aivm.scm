(define-module (crates-io ai vm aivm) #:use-module (crates-io))

(define-public crate-aivm-0.1.0 (c (n "aivm") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0b8z6zrmm0hsafx2n8limswavjg9223fy4whpx1881xs9isvkadj")))

(define-public crate-aivm-0.2.0 (c (n "aivm") (v "0.2.0") (d (list (d (n "cranelift") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-module") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-native") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13lfvv47w4xmjbih0xbxmi9ak41w61m70al54zmy969cyp1rrs1f") (f (quote (("jit-cranelift" "cranelift" "cranelift-jit" "cranelift-module" "cranelift-native"))))))

(define-public crate-aivm-0.3.0 (c (n "aivm") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "cranelift") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-module") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "cranelift-native") (r "^0.80") (o #t) (d #t) (k 0)) (d (n "dynasmrt") (r "^1") (o #t) (d #t) (k 0)))) (h "0wr0kk3q5f89pgkrh5hiknhzh45bm5hxqzzyk3ahplc8xdxz4jxk") (f (quote (("jit" "bitvec" "arrayvec" "dynasmrt")))) (s 2) (e (quote (("cranelift" "dep:cranelift" "cranelift-jit" "cranelift-module" "cranelift-native"))))))

