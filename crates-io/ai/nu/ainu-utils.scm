(define-module (crates-io ai nu ainu-utils) #:use-module (crates-io))

(define-public crate-ainu-utils-0.1.0 (c (n "ainu-utils") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "11cdr8vsjj9s6mmhqr1qnkclz82k4idnpbwvm4dgjfsrx1iq487s")))

(define-public crate-ainu-utils-0.2.0 (c (n "ainu-utils") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0jxvb0sy2hirkkm35qsgs4brcawnyia5a3abb2y7xj390ig4dndg")))

