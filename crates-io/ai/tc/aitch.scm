(define-module (crates-io ai tc aitch) #:use-module (crates-io))

(define-public crate-aitch-0.1.0 (c (n "aitch") (v "0.1.0") (h "1s8hc91mjs678b0jax8zn3zs66k7v51d9ipnzrgwkpqa8qnd0nlr")))

(define-public crate-aitch-0.1.1 (c (n "aitch") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tiny_http") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1") (o #t) (d #t) (k 0)))) (h "052rsgr15286byws8p0bzgzvs49qvnlw9wi11f81hk65m8a4f3ny") (f (quote (("server-tiny-http" "tiny_http" "tokio-threadpool") ("server-hyper" "hyper") ("json" "serde" "serde_json") ("default" "json" "server-hyper" "server-tiny-http" "mime_guess"))))))

