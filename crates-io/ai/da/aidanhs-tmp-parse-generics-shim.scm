(define-module (crates-io ai da aidanhs-tmp-parse-generics-shim) #:use-module (crates-io))

(define-public crate-aidanhs-tmp-parse-generics-shim-0.1.3 (c (n "aidanhs-tmp-parse-generics-shim") (v "0.1.3") (d (list (d (n "parse-generics-poc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 2)))) (h "19n318dx6z0fww9ymh0mp4r0sdxx82sm781m986g1xybxin4x6cv") (f (quote (("use-parse-generics-poc" "parse-generics-poc"))))))

