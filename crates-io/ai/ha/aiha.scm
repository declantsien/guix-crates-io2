(define-module (crates-io ai ha aiha) #:use-module (crates-io))

(define-public crate-aiha-0.1.0 (c (n "aiha") (v "0.1.0") (d (list (d (n "rust-gpu-tools") (r "^0.7.1") (d #t) (k 0)))) (h "0xxnr6ap1qb6ynz8s5p1sivx2580ymk4yi15ycxzngs2dpzkq56p") (y #t)))

(define-public crate-aiha-0.0.1 (c (n "aiha") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.9.0") (d #t) (k 0)))) (h "1i9k7v147104ix4dffivjhg504fzzdmy01lcvnxmvc3im0s9pwf7")))

(define-public crate-aiha-0.0.2 (c (n "aiha") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.9.0") (d #t) (k 0)))) (h "1q79ang1a9f5g4mmd4q6sc2a6jd6bck8rvdc3arsym3gslzf24wp")))

(define-public crate-aiha-0.0.3 (c (n "aiha") (v "0.0.3") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.9.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1czl3qd5xr5g2vgys4f1fis08adg1h8r1jgigypfhirz39yh182q")))

(define-public crate-aiha-0.0.4 (c (n "aiha") (v "0.0.4") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "nvml-wrapper") (r "^0.9.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fwzai0h170ivy5pwp3p4nsjdnllfj0idh8hlp86ay5wbqq8r9sv")))

