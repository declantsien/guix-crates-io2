(define-module (crates-io ai -g ai-graph) #:use-module (crates-io))

(define-public crate-ai-graph-0.0.1 (c (n "ai-graph") (v "0.0.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "14xaqflz6alil1c6k3a1s56hx02m0sz4klk2grpy8yydyb5bv55s") (y #t)))

(define-public crate-ai-graph-0.0.2 (c (n "ai-graph") (v "0.0.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "07bnf8rwnk2db7gmkd3sc4r3fh0jh3jfmz9q14nlvxp85lagfql0") (y #t)))

(define-public crate-ai-graph-0.0.3 (c (n "ai-graph") (v "0.0.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1307ask378q74z00pkdz98m28ksi3rfs917rzkhnq6g14bagnblb") (y #t)))

(define-public crate-ai-graph-0.0.4 (c (n "ai-graph") (v "0.0.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0wncpmdhb327v9igpnbqy9pdy3k92qhbqzv881lrbpvhpa7l386b") (y #t)))

(define-public crate-ai-graph-0.0.5 (c (n "ai-graph") (v "0.0.5") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0ydwwj8ahaxlfvr0cjz4mfsbb6nwlrqrqa67mzd7lhpw5pl7br02") (y #t)))

(define-public crate-ai-graph-0.0.8 (c (n "ai-graph") (v "0.0.8") (d (list (d (n "dont_disappear") (r "^1") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1hf807s8m2r1kwgabpmwkl77mibksny4p2c35x994y098j3v05yq") (y #t)))

(define-public crate-ai-graph-0.0.9 (c (n "ai-graph") (v "0.0.9") (d (list (d (n "dont_disappear") (r "^1") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1py0458403sgardca9mnj8mhlsqa8ribd5pz3bfa5dpqr9h23mba") (y #t)))

(define-public crate-ai-graph-0.0.10 (c (n "ai-graph") (v "0.0.10") (d (list (d (n "dont_disappear") (r "^1") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1g8qm4cp2pqk9m8wq5smng22wqx7hzqmdaih08146rvxp3wczv91") (y #t)))

(define-public crate-ai-graph-0.0.11 (c (n "ai-graph") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^1") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "132qsk48cfvplj081hqbv63viynvb0p063zvrxsf9j4i38hq3f4a") (y #t)))

(define-public crate-ai-graph-0.0.12 (c (n "ai-graph") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "01bnlrqn3iy5pjagzcqpyihsag4cj9q0mw954h3bgv1jp2jnvzlz") (y #t)))

(define-public crate-ai-graph-0.0.13 (c (n "ai-graph") (v "0.0.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0j8y9625dlca4ycssf8j18pmx3l9ky2bymkvhp4avds96pnjk705") (y #t)))

(define-public crate-ai-graph-0.0.14 (c (n "ai-graph") (v "0.0.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "06p47nv6mi58fmnn9chqrz88aacx55hsk834ai068nvcmkj7kn1n") (y #t)))

(define-public crate-ai-graph-0.0.15 (c (n "ai-graph") (v "0.0.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "10qsszcmznmc4kcknfh4y43nbrwdcqxvwv86yy58wd2ck9r5nms2") (y #t)))

(define-public crate-ai-graph-0.0.16 (c (n "ai-graph") (v "0.0.16") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1h2y9krs32jwf4rkw6iw568576vnmkl7ilssmjf9j6n16c6vahnr") (y #t)))

(define-public crate-ai-graph-0.0.17 (c (n "ai-graph") (v "0.0.17") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0d70km73zwraji4hdi9my62ng679mmnk28hr9wvyqyg1b9d8450i") (y #t)))

(define-public crate-ai-graph-0.0.18 (c (n "ai-graph") (v "0.0.18") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1mj23k32qil7n6sgh9yjqzgkb4dwri8m7lk5jf66nfd3g5zlgz84") (y #t)))

