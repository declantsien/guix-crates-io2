(define-module (crates-io ai #{21}# ai21) #:use-module (crates-io))

(define-public crate-ai21-0.1.0 (c (n "ai21") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "07g0g9q94glqfbclkbqwijq98bgdx3q7vzqsbby5hcvnakn25dnw")))

(define-public crate-ai21-0.1.1 (c (n "ai21") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0id9ys4xahvcmg8disa8k2azxgmxsk9pj1y6bw4k4qfh6m6l7rhd")))

(define-public crate-ai21-0.1.1-ohno (c (n "ai21") (v "0.1.1-ohno") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1yw0q2cn3cz9pczw7ipx0c9l2syhqpw3lgl3zv8hk3qwaigjbzin")))

(define-public crate-ai21-0.1.2 (c (n "ai21") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0k7k9lp00h9dswf96p40gacvsvvmp77y180c0c4rsca3pk4yrcba")))

(define-public crate-ai21-0.1.3 (c (n "ai21") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "0zh1p8ff69q8jpz0mp1g2mxkalg8wirlwx6j5kk0szvia40yhrii")))

(define-public crate-ai21-0.1.4 (c (n "ai21") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "1c6fak6fx56ck3y13fm8b6kr321j1khhdwihpphp9dhf515cfp7x")))

