(define-module (crates-io ai -w ai-write) #:use-module (crates-io))

(define-public crate-ai-write-0.1.0 (c (n "ai-write") (v "0.1.0") (d (list (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f05411k8k5wc2x7li0swqafb8xy57iby524rs4v7xvrrdn6vs5c")))

(define-public crate-ai-write-0.1.1 (c (n "ai-write") (v "0.1.1") (d (list (d (n "openai_api_rust") (r "^0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yz3jwrvzrl0psa4ijdj8qb84zj7fp6ppgc76ygajjy9q4abbvi6")))

