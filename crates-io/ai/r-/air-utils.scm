(define-module (crates-io ai r- air-utils) #:use-module (crates-io))

(define-public crate-air-utils-0.1.0 (c (n "air-utils") (v "0.1.0") (h "1d2cpqsiqkv9n2v5f7l23n1440yhn6bzl0dy0ann14fvig2jxpqr")))

(define-public crate-air-utils-0.1.1 (c (n "air-utils") (v "0.1.1") (h "129snqa2lc12rdivhf1fyjc3v7kr5qcs5g2z9j1h5ilgm8n91r97")))

(define-public crate-air-utils-0.2.0 (c (n "air-utils") (v "0.2.0") (h "1sqir087zl580phjy7mly2h889ya4rnv7ixgnhif3srzm9mkby7y")))

(define-public crate-air-utils-0.3.0 (c (n "air-utils") (v "0.3.0") (h "0raip18j0bkmg94fgx2l21fkzhglxnb6srlc3hwid8rwcr3by19p")))

