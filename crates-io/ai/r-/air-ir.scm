(define-module (crates-io ai r- air-ir) #:use-module (crates-io))

(define-public crate-air-ir-0.1.0 (c (n "air-ir") (v "0.1.0") (d (list (d (n "parser") (r "^0.1.0") (d #t) (k 0) (p "air-parser")))) (h "0hhl9zz36pz3kmws141g7lva9x0vshm164n3526andxa4aspvq2i") (r "1.65")))

(define-public crate-air-ir-0.2.0 (c (n "air-ir") (v "0.2.0") (d (list (d (n "air-script-core") (r "^0.2.0") (d #t) (k 0) (p "air-script-core")) (d (n "parser") (r "^0.2.0") (d #t) (k 0) (p "air-parser")))) (h "18pmgx8h0zh7vrh2b6vcpjpl790awi5hlxjfbhgxfyw476szbii4") (r "1.65")))

(define-public crate-air-ir-0.3.0 (c (n "air-ir") (v "0.3.0") (d (list (d (n "air-parser") (r "^0.3") (d #t) (k 0) (p "air-parser")) (d (n "air-pass") (r "^0.1") (d #t) (k 0) (p "air-pass")) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "miden-diagnostics") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yssixs27c1aj14jih1d5995lxlj7jq3z59wv4l9k8x1x4r5rf69") (r "1.67")))

