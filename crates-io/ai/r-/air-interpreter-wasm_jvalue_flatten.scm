(define-module (crates-io ai r- air-interpreter-wasm_jvalue_flatten) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.0 (c (n "air-interpreter-wasm_jvalue_flatten") (v "0.0.0") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1jfxli38f30lzmssmwkfnq9ng43pnb9ydznby8jx71mpjmwmpg2l")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.1 (c (n "air-interpreter-wasm_jvalue_flatten") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0wrr2ppdvx334mz7ndrbn62dkza9aasnx5r04m1sbdfhbz30zn36")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.2 (c (n "air-interpreter-wasm_jvalue_flatten") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1rhkglis5bbzy8zv2f24pishdim8pcv6hzjmwy79h7w4hvywrwfz")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.3 (c (n "air-interpreter-wasm_jvalue_flatten") (v "0.0.3") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0yc48wlxm5s43sv62fqz3cpjvkfd0bzbi136x9m8mbmkxncwh8f4")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.5 (c (n "air-interpreter-wasm_jvalue_flatten") (v "0.0.5") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1pmzdcr4d06aknjpdybh0y42jznshclnrwppzs298269qsr39v2a")))

