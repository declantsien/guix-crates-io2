(define-module (crates-io ai r- air-beautifier) #:use-module (crates-io))

(define-public crate-air-beautifier-0.1.3 (c (n "air-beautifier") (v "0.1.3") (d (list (d (n "aquavm-air-parser") (r "^0.7.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0jvg3h3cbya0p7yjas5i3kzif1cfj09252svaxgak1sdkj7y23c4")))

(define-public crate-air-beautifier-0.1.4 (c (n "air-beautifier") (v "0.1.4") (d (list (d (n "aquavm-air-parser") (r "^0.7.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1g6545d5bfr4p49zc9qlfyz93bc8hd1m82c32q4g102g057dmi98")))

(define-public crate-air-beautifier-0.2.0 (c (n "air-beautifier") (v "0.2.0") (d (list (d (n "aquavm-air-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "12gvv3krqhmlng06sx2za2hlydn7yljhh6kshi9blwiqzajxif55")))

(define-public crate-air-beautifier-0.2.1 (c (n "air-beautifier") (v "0.2.1") (d (list (d (n "aquavm-air-parser") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "06r2jw0w27mqkb7arlz3q24yvzd6z5l01mk0ag19swdmg7c2305c")))

(define-public crate-air-beautifier-0.2.2 (c (n "air-beautifier") (v "0.2.2") (d (list (d (n "aquavm-air-parser") (r "^0.8.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "16l4mbrckfd4319v64whpg9539g9w2mks3p7fs07bakh59bzrfp7")))

(define-public crate-air-beautifier-0.3.0 (c (n "air-beautifier") (v "0.3.0") (d (list (d (n "aquavm-air-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0w6zjhnxpfr526j3qi8cm0ibv664vzf2daxk0vc6y7xrhm9f5y6l")))

(define-public crate-air-beautifier-0.3.1 (c (n "air-beautifier") (v "0.3.1") (d (list (d (n "aquavm-air-parser") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1vy8ac6vwq228zd3kb49bxl4fcqqrrx0p7d7b5qkia2v7gyywwmc")))

(define-public crate-air-beautifier-0.4.0 (c (n "air-beautifier") (v "0.4.0") (d (list (d (n "aquavm-air-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09p28g4rijv1hgpksryp9c8bw24m7mcwnznzrf23p4mb4kpjckn4")))

(define-public crate-air-beautifier-0.4.1 (c (n "air-beautifier") (v "0.4.1") (d (list (d (n "aquavm-air-parser") (r "^0.11.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1bxmx7acc80wd97315wz7zh1b9qy5cx9rpjksk9bv6xfp56q8wv6")))

(define-public crate-air-beautifier-0.4.3 (c (n "air-beautifier") (v "0.4.3") (d (list (d (n "aquavm-air-parser") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0z6hvpc55jyfcdap9n75n1izai64gszr3c01w3vhp8lm35gwm3vp")))

