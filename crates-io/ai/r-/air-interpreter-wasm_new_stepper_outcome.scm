(define-module (crates-io ai r- air-interpreter-wasm_new_stepper_outcome) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.7 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.7") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1mm5zs6blwbbx1fq30xzg892a6nw06p5wrakar0lmppgif189sgx")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.8 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.8") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1p7l9sk3rgfqvqs78pbj7icfap0l7imjsn7jqlrzbb56s2rk1jn4")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.9 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.9") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0bjarl4z1wqrp2zng71ap1n8c31qx6yz5r5xwdpn0azq1l25zdj4")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.10 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.10") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1100a7v4dwxriivgrkbzl75mkhc503nrrhji59ivlpsww33i8cbk")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.11 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.11") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1zzf829b6l4nb5mbar8af0x3z3fx6x63x8jb12h1ard7lgfwd4fl")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.12 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.12") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0rn5bj4wn0vxiqiap9xb2bqr4kp8mr00qsmd27jxa26j8rs4s5za")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.13 (c (n "air-interpreter-wasm_new_stepper_outcome") (v "0.0.13") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0ckc0bm4lnl5kwc7v0mv045k9lba7l6r391xdzln29fc910vl35k")))

