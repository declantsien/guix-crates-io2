(define-module (crates-io ai r- air-interpreter-wasm_fold_iterable) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.1 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0hi604sc3313dc3aybvy452saz74grzlhal4cky7biy10wj0pv6h")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.2 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0q3hmd6nnqmkwkhyv72qr1q35czla7f6k3lcibsnjc72ibhxkd6w")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.3 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.3") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "165liqyry7qiqspxxav2p99fhgz2vxhkc3qjj19k83vwmjdzcg6p")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.4 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.4") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "09zzd358nz08h9swqxgw93v7r27735im0b2gq48rvqdmgl3608f4")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.5 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.5") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0q22s7q6g69yi0gq0iyar9h87wc5s8hzn5f0y8z3il3zbzw5nl07")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.6 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.6") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "13qcbxr7kzaga0pahzrvj8xl8z6x6w7gh43x2djbk24z0hsj4n3j")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.7 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.7") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1fqpv1irpqidmqwd9fz69pd0vwdd7wdq4dahgfykpbfkxyfpza1y")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.8 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.8") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0qp6hr88llzjrjwg3qj9as2ggy0ixys62xp2s05mf4bgy9ikp93v")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.9 (c (n "air-interpreter-wasm_fold_iterable") (v "0.0.9") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "01vl7p0grz2hxysa1vg2xyhaz3z4411619lly1ibdgrx90553311")))

