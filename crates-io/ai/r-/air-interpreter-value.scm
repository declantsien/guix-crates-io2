(define-module (crates-io ai r- air-interpreter-value) #:use-module (crates-io))

(define-public crate-air-interpreter-value-0.1.0 (c (n "air-interpreter-value") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0a854c5y5aamaq93fv8iwc2ysim523hi3q90a616sjfaym9qw5g3") (f (quote (("preserve_order" "indexmap"))))))

