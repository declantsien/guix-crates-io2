(define-module (crates-io ai r- air-interpreter-wasm_wasm_log) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_wasm_log-0.0.1 (c (n "air-interpreter-wasm_wasm_log") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "190zyrfs7k3jd1jk37lzd17qajivd9whyzih7cbbaw6w5sqgbid3")))

(define-public crate-air-interpreter-wasm_wasm_log-0.0.2 (c (n "air-interpreter-wasm_wasm_log") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "03kjicsq62iqc7ad50wqd3b59xj628flqkyigngzh5mzg5sb0ab6")))

