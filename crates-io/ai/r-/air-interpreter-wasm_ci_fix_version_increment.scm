(define-module (crates-io ai r- air-interpreter-wasm_ci_fix_version_increment) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_ci_fix_version_increment-0.0.1 (c (n "air-interpreter-wasm_ci_fix_version_increment") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "09x85hc8mv5sc5ahhv1znrcprmz6h3jgvjzvdk7hf87f7376s0c4")))

