(define-module (crates-io ai r- air-interpreter-wasm_new_it_types) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.1 (c (n "air-interpreter-wasm_new_it_types") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "044vlx17gwn8b6j8w366z8ckf88fk9zy92m6292js14vv7rnl1v8")))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.2 (c (n "air-interpreter-wasm_new_it_types") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0nl2yy17id9gjkppvny8qfxb2ifjhlpir1q1fc859548lh5njxch")))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.3 (c (n "air-interpreter-wasm_new_it_types") (v "0.0.3") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1fh4cfsq5g43xp7ijb201386r11iv4w3wzjwm4kziawnzfszs8d6")))

