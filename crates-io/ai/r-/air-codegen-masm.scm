(define-module (crates-io ai r- air-codegen-masm) #:use-module (crates-io))

(define-public crate-air-codegen-masm-0.1.0 (c (n "air-codegen-masm") (v "0.1.0") (d (list (d (n "air-ir") (r "^0.3") (d #t) (k 0) (p "air-ir")) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "miden-assembly") (r "^0.6") (k 2) (p "miden-assembly")) (d (n "miden-core") (r "^0.6") (k 0) (p "miden-core")) (d (n "miden-diagnostics") (r "^0.1") (d #t) (k 2)) (d (n "miden-processor") (r "^0.6") (f (quote ("internals"))) (k 2) (p "miden-processor")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winter-air") (r "^0.6") (k 2) (p "winter-air")) (d (n "winter-math") (r "^0.6") (k 0) (p "winter-math")))) (h "1khhmd9gxddyqf01ca9b3nrn0nb9fdms91dnkyvgzzx0m0kwphar") (r "1.67")))

