(define-module (crates-io ai r- air-lambda-parser) #:use-module (crates-io))

(define-public crate-air-lambda-parser-0.1.0 (c (n "air-lambda-parser") (v "0.1.0") (d (list (d (n "air-lambda-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "codespan") (r "^0.11.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19.8") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("rc" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)))) (h "1wb9jjbydjffnqv2pdni5a7mmaw9r8ql12qq366l292ggv887zb0")))

