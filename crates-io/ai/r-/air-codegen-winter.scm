(define-module (crates-io ai r- air-codegen-winter) #:use-module (crates-io))

(define-public crate-air-codegen-winter-0.1.0 (c (n "air-codegen-winter") (v "0.1.0") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "ir") (r "^0.1.0") (d #t) (k 0) (p "air-ir")))) (h "0781wgfgpk3vnn08713w3pb1lbfgrz81frcy3iv894ffa9jnf00w") (r "1.65")))

(define-public crate-air-codegen-winter-0.2.0 (c (n "air-codegen-winter") (v "0.2.0") (d (list (d (n "air-script-core") (r "^0.2.0") (d #t) (k 0) (p "air-script-core")) (d (n "codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "ir") (r "^0.2.0") (d #t) (k 0) (p "air-ir")))) (h "0rb4agmi19cf1bki0ybbms7rx6c5gpx47py28kfirvxi9ddgxfwp") (r "1.65")))

(define-public crate-air-codegen-winter-0.3.0 (c (n "air-codegen-winter") (v "0.3.0") (d (list (d (n "air-ir") (r "^0.3") (d #t) (k 0) (p "air-ir")) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "codegen") (r "^0.2") (d #t) (k 0)))) (h "0i565b3vwdmpb8rjfj7c5n97x635ik6h1v6pzg73myilhdk3px99") (r "1.67")))

