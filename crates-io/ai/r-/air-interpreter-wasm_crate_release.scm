(define-module (crates-io ai r- air-interpreter-wasm_crate_release) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_crate_release-0.0.14 (c (n "air-interpreter-wasm_crate_release") (v "0.0.14") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1xhgd6fcmyqhwfs7ay4mfr1rmx4midgyb04dk3181hanwgqadr06")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.15 (c (n "air-interpreter-wasm_crate_release") (v "0.0.15") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "13fzn290fdkq41sj69qviwq8a10izdhsm2v0disj790fyl1xn7bc")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.17 (c (n "air-interpreter-wasm_crate_release") (v "0.0.17") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "12higw51ljsz56ink3g1kxm76py76ks45154byycvf9rsn1xqkfq")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.18 (c (n "air-interpreter-wasm_crate_release") (v "0.0.18") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0f9kwk9dzv1ywm2miwykprhcxylmy3qsk7qyn6lc6n6hrkpzw8rk")))

