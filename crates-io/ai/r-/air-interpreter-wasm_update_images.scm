(define-module (crates-io ai r- air-interpreter-wasm_update_images) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_update_images-0.0.0 (c (n "air-interpreter-wasm_update_images") (v "0.0.0") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0iaqi4rh4a056ssvl21hyyic88rbanv7ghhqh5l8v84fvqfv4ypv")))

(define-public crate-air-interpreter-wasm_update_images-0.0.1 (c (n "air-interpreter-wasm_update_images") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "061iyrb4kvl1aq3if50dbaf62d0dd2q3g1zl43sw87cggi2s0ck7")))

