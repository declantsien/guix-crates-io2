(define-module (crates-io ai r- air-execution-info-collector) #:use-module (crates-io))

(define-public crate-air-execution-info-collector-0.7.3 (c (n "air-execution-info-collector") (v "0.7.3") (d (list (d (n "aquavm-air-parser") (r "^0.7.4") (d #t) (k 0)))) (h "095p44nr539nym4jc31akr309kjrhgnmma593hzwq8g0a0p4z4hc")))

(define-public crate-air-execution-info-collector-0.7.4 (c (n "air-execution-info-collector") (v "0.7.4") (d (list (d (n "aquavm-air-parser") (r "^0.7.4") (d #t) (k 0)))) (h "1yylx97g0bljbfp6wm4rfpq7kdq1f82n45b20hk9g5mz0pka3psq")))

(define-public crate-air-execution-info-collector-0.7.5 (c (n "air-execution-info-collector") (v "0.7.5") (d (list (d (n "aquavm-air-parser") (r "^0.7.5") (d #t) (k 0)))) (h "1wplv9xkc11rcblmbm8ylqq5f53xkhxznpp63r5dlvzv7rj59b8n")))

(define-public crate-air-execution-info-collector-0.7.6 (c (n "air-execution-info-collector") (v "0.7.6") (d (list (d (n "aquavm-air-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0brlx9iz4jpgfn49vfcaxxy53ck129dasnc12f7c9f6pgvc2i1i3")))

(define-public crate-air-execution-info-collector-0.7.7 (c (n "air-execution-info-collector") (v "0.7.7") (d (list (d (n "aquavm-air-parser") (r "^0.8.1") (d #t) (k 0)))) (h "12rgh3nmym5077zx18yvcn3mj8i4b6c78g85sxhfy3nlvpb0lnhb")))

(define-public crate-air-execution-info-collector-0.7.8 (c (n "air-execution-info-collector") (v "0.7.8") (d (list (d (n "aquavm-air-parser") (r "^0.8.2") (d #t) (k 0)))) (h "1jmm6brkzq481s15342kj3qlqfbljpavlvx0sfd3s8ssbcx02c7d")))

(define-public crate-air-execution-info-collector-0.7.9 (c (n "air-execution-info-collector") (v "0.7.9") (d (list (d (n "aquavm-air-parser") (r "^0.9.0") (d #t) (k 0)))) (h "0ksrd5fmhw4qqsr6538ygm4kmy3iivhs4mpy1gpyl81bmq566y9h")))

(define-public crate-air-execution-info-collector-0.7.10 (c (n "air-execution-info-collector") (v "0.7.10") (d (list (d (n "aquavm-air-parser") (r "^0.10.0") (d #t) (k 0)))) (h "05awnhmffi33jx2il30nz7hyq45bb177gq81kj4my0x4lwq9lnzn")))

(define-public crate-air-execution-info-collector-0.7.11 (c (n "air-execution-info-collector") (v "0.7.11") (d (list (d (n "aquavm-air-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1hpny45g2injghzm8p0hisl5qy5f0hrd690saczya30a23jakbnr")))

(define-public crate-air-execution-info-collector-0.7.12 (c (n "air-execution-info-collector") (v "0.7.12") (d (list (d (n "aquavm-air-parser") (r "^0.11.1") (d #t) (k 0)))) (h "0a9vvjgsyr94zvzsvcs4fcb3wf8az8rrbgmaz7v342mblhadajyd")))

(define-public crate-air-execution-info-collector-0.7.13 (c (n "air-execution-info-collector") (v "0.7.13") (d (list (d (n "aquavm-air-parser") (r "^0.11.2") (d #t) (k 0)))) (h "165zqzj0xvamjc5wc4n938x21nsw8fyk7810i9446xgpkgj2sbff")))

(define-public crate-air-execution-info-collector-0.7.14 (c (n "air-execution-info-collector") (v "0.7.14") (d (list (d (n "aquavm-air-parser") (r "^0.12.0") (d #t) (k 0)))) (h "0rbbk8rj9jihc6qz43z34b2zlpsz9y93jmx68fjpdq1ig997sz93")))

