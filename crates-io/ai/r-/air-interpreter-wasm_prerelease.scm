(define-module (crates-io ai r- air-interpreter-wasm_prerelease) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_prerelease-0.0.0 (c (n "air-interpreter-wasm_prerelease") (v "0.0.0") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1vvrl3ffznw1n1mdwp9lfp76yli64rdf4v3s63jv3w0ai1l89r2w")))

(define-public crate-air-interpreter-wasm_prerelease-0.0.1 (c (n "air-interpreter-wasm_prerelease") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1kqk0z1gszbhypp9aikdw5kdyv9adk4r9ag9s65p5d49sn8jpr3g")))

