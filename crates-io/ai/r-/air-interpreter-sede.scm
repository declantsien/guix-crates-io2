(define-module (crates-io ai r- air-interpreter-sede) #:use-module (crates-io))

(define-public crate-air-interpreter-sede-0.1.0 (c (n "air-interpreter-sede") (v "0.1.0") (d (list (d (n "marine-rs-sdk") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.8.0") (f (quote ("std"))) (k 0)))) (h "0qc2nb3ryadj8yzgd4c0ap0hhynm3jk2lsv4hdh6lpwgafiiqfs3") (f (quote (("serde_json") ("msgpack" "rmp-serde") ("json" "serde_json") ("default")))) (s 2) (e (quote (("rmp-serde" "dep:rmp-serde") ("marine" "dep:marine-rs-sdk"))))))

