(define-module (crates-io ai r- air-interpreter-wasm_get_rid_current_peer_id) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.1 (c (n "air-interpreter-wasm_get_rid_current_peer_id") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1j3p01pzn4ajxrbyf239ppmwpmwh51wy81x3ggwg6xp4b0gw1v7s")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.2 (c (n "air-interpreter-wasm_get_rid_current_peer_id") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0vpvyjzvm2kyf2srydn3v1d480sgb95wbvrxjwpg7wpxj6sp18fa")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.3 (c (n "air-interpreter-wasm_get_rid_current_peer_id") (v "0.0.3") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "17jvvbppkssgx323wgb9a2ar210h7a3vmv4cc5hm1da5zjhkibr5")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.4 (c (n "air-interpreter-wasm_get_rid_current_peer_id") (v "0.0.4") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "14qgpl95fzz2ss6662rihwa9220kfa88p0pvnyy152m7yagqdd2l")))

