(define-module (crates-io ai r- air-parser) #:use-module (crates-io))

(define-public crate-air-parser-0.1.0 (c (n "air-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hmhhwdknjycpw0id54j97rdfrhnhkjmabx86bbkw630w7jwfkhd") (r "1.65")))

(define-public crate-air-parser-0.2.0 (c (n "air-parser") (v "0.2.0") (d (list (d (n "air-script-core") (r "^0.2.0") (d #t) (k 0) (p "air-script-core")) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kg533fkvkiwnqxypf9c7g3b367rq9frrs5zmm1g9ggaq5gcjp7w") (r "1.65")))

(define-public crate-air-parser-0.3.0 (c (n "air-parser") (v "0.3.0") (d (list (d (n "air-pass") (r "^0.1") (d #t) (k 0) (p "air-pass")) (d (n "lalrpop") (r "^0.20") (k 1)) (d (n "lalrpop-util") (r "^0.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "miden-diagnostics") (r "^0.1") (d #t) (k 0)) (d (n "miden-parsing") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0an3ba9v3j38x1hngz2v24pz7csmfmi00jjd0azw5ky0ynvmw9nf") (r "1.67")))

