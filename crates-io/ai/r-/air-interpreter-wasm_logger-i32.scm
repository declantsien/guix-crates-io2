(define-module (crates-io ai r- air-interpreter-wasm_logger-i32) #:use-module (crates-io))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.0 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.0") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1v547ahn1z4hiqg537wwi4v0q40lawgr44wjw72s5gljd75nh2wf")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.1 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.1") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0hlkl2vgy9gdxa6vpfaq2lhr9r9n81p3sjp63jmna4cgma5dgf7p")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.2 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.2") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0ys0jpb8wzngzs6icxgcj3zig5pbzqpx91r8k0y2qd8imbqgf4vp")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.3 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.3") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1x4jbbqjv4kdk4jqbhaf4wph0zg6l3h14ak5np3ryf6i5f0mcbvx")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.4 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.4") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "0pjcxfzm07jkbxkxb12xyfshn2pp3d69gy7594mmdqbb1y9a7l8b")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.5 (c (n "air-interpreter-wasm_logger-i32") (v "0.0.5") (d (list (d (n "built") (r "^0.4.4") (d #t) (k 1)))) (h "1s93vrqck2jfsyplgxlh4j4nrx2k3q8g6v16481c12r4qrynmkw6")))

