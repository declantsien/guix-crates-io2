(define-module (crates-io ai ct aict) #:use-module (crates-io))

(define-public crate-aict-0.1.0 (c (n "aict") (v "0.1.0") (h "1nrp671az4npqj4vanj51dwy0fc128m5jad69fg9kb6rwdqd24hl")))

(define-public crate-aict-0.2.0 (c (n "aict") (v "0.2.0") (h "0kh74fmrk3m2hc6m70pcms39bfpx0qvqgcfzh8rh21j4d28gzhd7")))

