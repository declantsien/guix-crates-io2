(define-module (crates-io ai rs airs-types) #:use-module (crates-io))

(define-public crate-airs-types-0.0.0 (c (n "airs-types") (v "0.0.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a9v2yg4s5vaaygyrik2kvh02i8ag57xdiibhpq7hd3nap5hbryb") (f (quote (("default" "image" "ndarray"))))))

