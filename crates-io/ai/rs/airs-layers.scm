(define-module (crates-io ai rs airs-layers) #:use-module (crates-io))

(define-public crate-airs-layers-0.0.0 (c (n "airs-layers") (v "0.0.0") (d (list (d (n "airs-types") (r "^0.0.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "0wbi3bkdq3k825zgiyfphr5fllb5508g9nnr5vlnbpai3qcrcfa5") (f (quote (("default"))))))

