(define-module (crates-io ai rs airspy) #:use-module (crates-io))

(define-public crate-airspy-0.1.0 (c (n "airspy") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0hifd6qyi12yjy6ygsg2xqlylbryan767d596da7hm1v9d9bxvm3")))

(define-public crate-airspy-0.2.0 (c (n "airspy") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ggfkh5rg0i4qq9hhr96wm9dlwfhh7iyjndl835njl29xx792qr2")))

(define-public crate-airspy-0.3.0 (c (n "airspy") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "08ypz0ixprnzpd6gkxbm462w6ackhkxzx1j2rc528j0zs7r8lbpr")))

(define-public crate-airspy-0.3.1 (c (n "airspy") (v "0.3.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0y0b88pd93wfhjg8wh9n3wzhgkqn2lxc3nl60fpaq02p9if3mfr0")))

(define-public crate-airspy-0.4.0 (c (n "airspy") (v "0.4.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "125gz28jnl7lqpgszfw5ydl7r4q9r4iav6nqb8a6z48sij75vk9q")))

