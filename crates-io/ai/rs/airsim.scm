(define-module (crates-io ai rs airsim) #:use-module (crates-io))

(define-public crate-airsim-0.1.0 (c (n "airsim") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rmp-rpc") (r "^0.3.0") (d #t) (k 0)) (d (n "rmpv") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "17zabd4d4zr0n7dmysa6jp80gi8r0pl7vgbji13mxkm4m6glhl2c")))

(define-public crate-airsim-0.2.0 (c (n "airsim") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "glutin") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "rmp-rpc") (r "^0.3.0") (d #t) (k 0)) (d (n "rmpv") (r "^0.4.4") (d #t) (k 0)))) (h "0rji227rfmqi0vy27lwkbmnvw0s26pd6y0ac2rmvkrd32gzks4ak") (f (quote (("keyboard" "glutin"))))))

