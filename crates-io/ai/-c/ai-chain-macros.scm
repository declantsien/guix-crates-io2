(define-module (crates-io ai -c ai-chain-macros) #:use-module (crates-io))

(define-public crate-ai-chain-macros-0.13.0 (c (n "ai-chain-macros") (v "0.13.0") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1gryp1d04jsvq03f71fg28jcjsa5x4gmal3ivpsij22j0838vn0k")))

(define-public crate-ai-chain-macros-0.14.1 (c (n "ai-chain-macros") (v "0.14.1") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0hq1p2ahdf1iqpjcg4zg95kaxd48kprz2yff6plvcn648dh2fdjl")))

(define-public crate-ai-chain-macros-0.14.2 (c (n "ai-chain-macros") (v "0.14.2") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0x8lfk98kjsbh5wb74wbda2g84lxwpf2q3cwg6pjh46lsg9ar7wx")))

