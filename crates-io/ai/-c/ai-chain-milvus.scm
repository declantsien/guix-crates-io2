(define-module (crates-io ai -c ai-chain-milvus) #:use-module (crates-io))

(define-public crate-ai-chain-milvus-0.14.2 (c (n "ai-chain-milvus") (v "0.14.2") (d (list (d (n "ai-chain") (r "^0.14.2") (k 0)) (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "milvus-sdk-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "1aa6924vxiva3478sy8cm73ba23gpl3y5fn5jklpmclg13p60nw8")))

