(define-module (crates-io ai -c ai-cli-assistant) #:use-module (crates-io))

(define-public crate-ai-cli-assistant-0.1.0 (c (n "ai-cli-assistant") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "tokio-macros"))) (d #t) (k 0)))) (h "1zmgxrgnf3fm870jfgyjhr7spmnvdssbr0yzcpvpff6gpnmdjdw6")))

(define-public crate-ai-cli-assistant-0.1.1 (c (n "ai-cli-assistant") (v "0.1.1") (d (list (d (n "async-openai") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "tokio-macros"))) (d #t) (k 0)))) (h "04lpc1axflgh2grx3m39ggx76szbnjphx288s8sbncljfnsp4w6n")))

(define-public crate-ai-cli-assistant-0.1.2 (c (n "ai-cli-assistant") (v "0.1.2") (d (list (d (n "async-openai") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "tokio-macros"))) (d #t) (k 0)))) (h "1r9iihdyb3rw07prvjygj36dlj71fidbhj55azjipm0br840sgww")))

(define-public crate-ai-cli-assistant-0.1.3 (c (n "ai-cli-assistant") (v "0.1.3") (d (list (d (n "async-openai") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "tokio-macros"))) (d #t) (k 0)))) (h "1h9xqhahhxan5grpikqzwv9fqfl9z6ybq6b0x5pyi5i5a2xqj53f")))

(define-public crate-ai-cli-assistant-0.1.4 (c (n "ai-cli-assistant") (v "0.1.4") (d (list (d (n "async-openai") (r "^0.14.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "tokio-macros"))) (d #t) (k 0)))) (h "180zha558zahd4m5rwxhmyiss7fgvn7v8kkf157pmnjn04j9nkfk")))

