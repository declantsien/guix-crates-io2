(define-module (crates-io ai rt airtable-env) #:use-module (crates-io))

(define-public crate-airtable-env-0.1.0 (c (n "airtable-env") (v "0.1.0") (d (list (d (n "airtable-api") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rzlj2njr6wv188lwa86pr0d9h2dywak2b1jr29xiqakvkavm1pd")))

(define-public crate-airtable-env-0.1.1 (c (n "airtable-env") (v "0.1.1") (d (list (d (n "airtable-api") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgkg4zgcq8wcg2xsd1s1ra3msqcclrgs0c72hn2rwqjv05jqlgs")))

(define-public crate-airtable-env-0.1.2 (c (n "airtable-env") (v "0.1.2") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "084p5ilc14l9zqnbmmch4f49llzdpbkywigldlg7fxn6l9ciffiv")))

(define-public crate-airtable-env-0.1.3 (c (n "airtable-env") (v "0.1.3") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0l78wc7qcizl25rw9vjbx6qznx7b2wkhnqw561gggdhmirn1vask")))

(define-public crate-airtable-env-0.1.4 (c (n "airtable-env") (v "0.1.4") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0zxj1ryxxfwl1a3rgm0a3icyh7nj48wmh6rxyvv3bi51ddb3mf7p")))

(define-public crate-airtable-env-0.1.5 (c (n "airtable-env") (v "0.1.5") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0mf74n5n2d3xv1mzpyjmjqf0blijp9rj809giyfm1d3yq1v6q2zi")))

(define-public crate-airtable-env-0.1.6 (c (n "airtable-env") (v "0.1.6") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1h1l8fzqnrw1z924k7c9cg9di3xa225k4cxghbbm76x3122nnfp9")))

(define-public crate-airtable-env-0.1.7 (c (n "airtable-env") (v "0.1.7") (d (list (d (n "airtable-api") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwh3agz7rvkgln033sqdpqaddwcnap6m24qdrk751v8db9gwc5h")))

