(define-module (crates-io ai rt airtable) #:use-module (crates-io))

(define-public crate-airtable-0.1.0 (c (n "airtable") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "mockito") (r "^0.13") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q569ygadw159sjnlznr5z825yxnbgxcsidkxwkgriz21aj2jlj0")))

