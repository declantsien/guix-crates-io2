(define-module (crates-io ai ff aiff) #:use-module (crates-io))

(define-public crate-aiff-0.1.0 (c (n "aiff") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (f (quote ("i128"))) (d #t) (k 0)) (d (n "cpal") (r "^0.10.0") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.0.2") (d #t) (k 0)))) (h "12z28ydmln16in052jmajzdvqbjxqg8rv2sgvnjd0n4m2alh6vi7") (y #t)))

