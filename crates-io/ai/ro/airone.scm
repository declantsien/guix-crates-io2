(define-module (crates-io ai ro airone) #:use-module (crates-io))

(define-public crate-airone-0.1.1 (c (n "airone") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "19hnqf7lbl30hz2r5mixvvbjf9hrhcnkmhcw5pcgd4isr9979n7j") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.1.2 (c (n "airone") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "0hgfqfrqcjswq7m50b76kl751jinikdhbnfh2xr1msklcxiizdkh") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.0 (c (n "airone") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "0mbmk024vcadm4jr39kdg078ka674r3krxdm5rzjjihr1lrnz32v") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.1 (c (n "airone") (v "0.2.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1qf96376cyn7jdx5c9lr31xjma5sg7s1sqdk4qai3xydnym8fcni") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.2 (c (n "airone") (v "0.2.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "09akx5lhq4y7dya45vcx95irc2y49i8z30vvlnwagj1r59438hlz") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.3 (c (n "airone") (v "0.2.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1aihsl0mf5npglq9x95039wwk8s5486p2dby0vk2nxhg8csn8q7d") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.4 (c (n "airone") (v "0.2.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1rxk5jjh92y6qn6p5kmh98f8i6i30jbd7drjl9nnc0r66r9c9rnv") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.5 (c (n "airone") (v "0.2.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "0f4q25a38s93wg6w5lc3i62qpn9zdwxy8b9x6yckdi56f6qz564h") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.2.6 (c (n "airone") (v "0.2.6") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1ixbkhzrff5s81p4nr814w143lkpfpwbh22q27hdy3lxivbjhw28") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.3.0 (c (n "airone") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1xlh6063q7b8mi9gy862wn0bnwaga05q4lrgz4dv51abnhbb0myj") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.3.1 (c (n "airone") (v "0.3.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "0h4183wqzph8did5w0chgw5r7gxdynbcy5h2yviwdchj64q2ssiw") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.3.2 (c (n "airone") (v "0.3.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "05xi0j855m2imym406skca921s773372sxchr6m9jd60hi1na6na") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.3.3 (c (n "airone") (v "0.3.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "18ljw79bncj6w7cx0q20wbg55df7x19wr7jib1np78yh53ywmgif") (s 2) (e (quote (("rocket" "dep:rocket"))))))

(define-public crate-airone-0.4.0 (c (n "airone") (v "0.4.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)))) (h "1dpbf3r7jal6xp18cq3z0vx2kjyx0g8wxgqimppivxs3ib3hvnb9")))

(define-public crate-airone-0.4.1 (c (n "airone") (v "0.4.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0fh3qzlz9brx8rz51k67ps8dvhbj85xl2c94l0xj8w1h7yi7iygs")))

(define-public crate-airone-0.4.2 (c (n "airone") (v "0.4.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1c3sx4vn3kd70fqq6ram98pp2anl00h3bmhd23z8pfki0xlmbiq5")))

(define-public crate-airone-0.4.3 (c (n "airone") (v "0.4.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "11323dkcbx3xm4zngz242jm97dc8w21rsdl3aj7425x9cikm190b")))

(define-public crate-airone-0.5.0 (c (n "airone") (v "0.5.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1zkn9zzpn8ppbcqvxy8zrhwian7cv6awxp232fjf0yiw7jdf44b2")))

(define-public crate-airone-0.5.1 (c (n "airone") (v "0.5.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "13bc5c4wwhnbsx73hj5sf7kpwigag8f2ivvqirpmxil8lp1j1fga")))

(define-public crate-airone-0.6.0 (c (n "airone") (v "0.6.0") (d (list (d (n "airone_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "14akxi3f94i7l4hxmpyrzj25kym1s9f1zcanh2s7q0brlfn55im5")))

(define-public crate-airone-0.7.0 (c (n "airone") (v "0.7.0") (d (list (d (n "airone_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1l1g71vykf1qh3qygrqv8fnjnxq6yfr5m9kw43nnh78j3y6kg99x") (f (quote (("c" "airone_derive/c"))))))

(define-public crate-airone-0.7.1 (c (n "airone") (v "0.7.1") (d (list (d (n "airone_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1k53ww9bb62v8r61jjs6626i830h6wa7vwk3p2ycm6z3qa0bjqr0") (f (quote (("c" "airone_derive/c"))))))

(define-public crate-airone-0.7.2 (c (n "airone") (v "0.7.2") (d (list (d (n "airone_derive") (r "^0.7.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0g13mjj5vdayfgpvfl9xf3n28n80b1xhlgqm832i40xj6bl9wxzs") (f (quote (("c" "airone_derive/c"))))))

(define-public crate-airone-0.7.3 (c (n "airone") (v "0.7.3") (d (list (d (n "airone_derive") (r "^0.7.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0fxl2xqar77wf5gskbkjbg2x5fvv5kpab36iw76j6d6j2kl69wrq") (f (quote (("c" "airone_derive/c"))))))

(define-public crate-airone-0.7.4 (c (n "airone") (v "0.7.4") (d (list (d (n "airone_derive") (r "^0.7.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1c85bscwff3d418q82pr4z88vh3cq1qfjkybbva80vxla3628mc3") (f (quote (("c" "airone_derive/c"))))))

(define-public crate-airone-0.8.0 (c (n "airone") (v "0.8.0") (d (list (d (n "airone_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "110y22hf3fjyjym8i6m838cdjy7mxb91a4p7b4ci4nl1r8szj46p")))

(define-public crate-airone-0.8.1 (c (n "airone") (v "0.8.1") (d (list (d (n "airone_derive") (r "^0.8.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0aqli4z06cl7jzgd4l1ibbi8jj9q8r1ha7qpg9p8hcgc6kw0bci5")))

