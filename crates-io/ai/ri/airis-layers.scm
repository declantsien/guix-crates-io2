(define-module (crates-io ai ri airis-layers) #:use-module (crates-io))

(define-public crate-airis-layers-0.0.0 (c (n "airis-layers") (v "0.0.0") (d (list (d (n "airs-types") (r "^0.0.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)))) (h "02ckr8jgrdns7bpi1v2k01vcm61fymf6d3ra3s2zy612f4ri444h") (f (quote (("default"))))))

