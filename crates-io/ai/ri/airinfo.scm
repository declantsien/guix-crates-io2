(define-module (crates-io ai ri airinfo) #:use-module (crates-io))

(define-public crate-airinfo-0.1.0 (c (n "airinfo") (v "0.1.0") (d (list (d (n "btleplug") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09yy100fynqz5csdjk6abziss9sfh30q6whawmps2ipz1vmjvi7b")))

