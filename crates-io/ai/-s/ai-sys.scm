(define-module (crates-io ai -s ai-sys) #:use-module (crates-io))

(define-public crate-ai-sys-0.0.0 (c (n "ai-sys") (v "0.0.0") (h "04g1rmq2rmbn9l8dwj4qwaddgv32xbsr8dpy6x278ra8myfvd9qp") (r "1.46")))

(define-public crate-ai-sys-0.1.0 (c (n "ai-sys") (v "0.1.0") (h "1bin8yz0160pn5jglvh34cpdfwia2jcmn9rwm591yak6jhqc45l8")))

