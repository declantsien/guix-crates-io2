(define-module (crates-io ai dl aidl-cli) #:use-module (crates-io))

(define-public crate-aidl-cli-0.2.0 (c (n "aidl-cli") (v "0.2.0") (d (list (d (n "aidl-parser") (r "^0.9") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "109vwifvjs59awb48fcxk97q0kr5jj99xibqxg6qbdbhr02d2bq0")))

