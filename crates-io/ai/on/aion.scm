(define-module (crates-io ai on aion) #:use-module (crates-io))

(define-public crate-aion-0.1.0 (c (n "aion") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1m9x9l2qmlznr9h76lddikfk3qfsddq6x2fnxgqhlpq7808fsl9s")))

(define-public crate-aion-0.1.1 (c (n "aion") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0by2q1sv5bmz5ljqbh2wyymzsz5mkrh9pv1ghkh9qlzk280djscd")))

(define-public crate-aion-0.2.0 (c (n "aion") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1cnfff8wfp3lzk0rkvnmkq398wvspp297jid2kn4l0y9c0rrxikz")))

