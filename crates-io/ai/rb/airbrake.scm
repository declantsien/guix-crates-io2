(define-module (crates-io ai rb airbrake) #:use-module (crates-io))

(define-public crate-airbrake-0.1.0 (c (n "airbrake") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1zw8qb3m3rfyzgnaznzzyvy4kw2bng6a6wzpxaqsd69j7ky8gjd1")))

(define-public crate-airbrake-0.2.0 (c (n "airbrake") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1615mkj9s0c5kan6vgckxanjmzdgrpg6d1fih6nl68rfs087a1dj")))

