(define-module (crates-io ai -a ai-agent-macro) #:use-module (crates-io))

(define-public crate-ai-agent-macro-0.1.1 (c (n "ai-agent-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "133md8847h4xkkj1ki2hkjmkaba72pmi2k9rb2fd1y8dd7cmvvv0")))

