(define-module (crates-io ai p- aip-filtering) #:use-module (crates-io))

(define-public crate-aip-filtering-0.1.0 (c (n "aip-filtering") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest-ast") (r "^0.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0jzc38lag3m4ksa81lsrx12hzp9bycjnfn2bmyy4brxkv4az0l3b")))

