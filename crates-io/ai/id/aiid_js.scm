(define-module (crates-io ai id aiid_js) #:use-module (crates-io))

(define-public crate-aiid_js-0.0.1 (c (n "aiid_js") (v "0.0.1") (d (list (d (n "aiid") (r "=0.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.40") (d #t) (k 0)) (d (n "wee_alloc") (r "=0.4.4") (d #t) (k 0)))) (h "0xqvan1rpicgj9rf6dwpcw2j0pn55l3xnv2flwyr80hl242rcs9p")))

