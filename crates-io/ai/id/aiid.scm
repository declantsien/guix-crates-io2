(define-module (crates-io ai id aiid) #:use-module (crates-io))

(define-public crate-aiid-0.0.1 (c (n "aiid") (v "0.0.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "reed-solomon") (r "=0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05ywz95ilg5x1d17v9xfqqia6v2ak5acp9vip0bbaa6pvbprr92h")))

(define-public crate-aiid-0.0.2 (c (n "aiid") (v "0.0.2") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "reed-solomon") (r "=0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vzrzhi21525sf4x9m0n4js19nf0i9288pp7c61j4714h53z4gra")))

