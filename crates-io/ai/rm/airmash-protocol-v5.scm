(define-module (crates-io ai rm airmash-protocol-v5) #:use-module (crates-io))

(define-public crate-airmash-protocol-v5-0.0.1 (c (n "airmash-protocol-v5") (v "0.0.1") (d (list (d (n "airmash-protocol") (r "^0.2") (d #t) (k 0)))) (h "0c5dnnqchzms1pnlpbd4yk1d8dxcsf7rsmhv27g70h2qli0k7pfz")))

(define-public crate-airmash-protocol-v5-0.0.2 (c (n "airmash-protocol-v5") (v "0.0.2") (d (list (d (n "airmash-protocol") (r "^0.2") (d #t) (k 0)))) (h "03zilmhpx8qsagkqylf79d0z14whrzjcihajrsc0b8ccm0xnd5ma")))

(define-public crate-airmash-protocol-v5-0.0.3 (c (n "airmash-protocol-v5") (v "0.0.3") (d (list (d (n "airmash-protocol") (r "^0.2") (k 0)))) (h "1q5g0rvwnlkzvbninvgx6r01qwzsf93p3rcas3pkwwv2ay7fckqb")))

(define-public crate-airmash-protocol-v5-0.0.4 (c (n "airmash-protocol-v5") (v "0.0.4") (d (list (d (n "airmash-protocol") (r "^0.3") (k 0)))) (h "0naqjzbc30ykbbmmhljdcfclj1qyxmvpwaai6i5q6y1p919ds6f4")))

