(define-module (crates-io ai _f ai_functions) #:use-module (crates-io))

(define-public crate-ai_functions-0.1.0 (c (n "ai_functions") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1aa21hags950pl8vvn2yz1xmkk07kv7z03cff9q7f2yzpcxnfhd7")))

(define-public crate-ai_functions-0.1.1 (c (n "ai_functions") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06yisgp6ngswpbasdlxazx0nb7lipwarsvv7adn3lh07gqi4j5li")))

