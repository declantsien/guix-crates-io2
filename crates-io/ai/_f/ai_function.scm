(define-module (crates-io ai _f ai_function) #:use-module (crates-io))

(define-public crate-ai_function-0.1.0 (c (n "ai_function") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlj1zmnmhq1g6f865bn5ik56lhdhrnfvnp10px0c9yczkz80ddx")))

