(define-module (crates-io ai rl airline) #:use-module (crates-io))

(define-public crate-airline-0.1.0 (c (n "airline") (v "0.1.0") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1smpmcpf4rdlwa6hv5v0wcnsgxq43lgmizxncp829z8fa8dfbj4w")))

(define-public crate-airline-0.1.1 (c (n "airline") (v "0.1.1") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "01ym6vnix2x5snfaps5q79jdc5444gmmj2jkhcdxgmbdw7x36lcm")))

(define-public crate-airline-0.1.2 (c (n "airline") (v "0.1.2") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0x13nzlmh76ll4innnjkk4n8wpm2l5h0jnxhg90443gcns7h634m")))

(define-public crate-airline-0.1.3 (c (n "airline") (v "0.1.3") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lqha3y0nq0c2xlrvy1c9ygjy18r2g3xa979m3k6817224r4b26b")))

(define-public crate-airline-0.1.4 (c (n "airline") (v "0.1.4") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "06xd5x4w7c9bzy65zqv221dn7cclqb5sl3hhhc1j5ilbygr931pg")))

(define-public crate-airline-0.1.5 (c (n "airline") (v "0.1.5") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "06kg8ypm8pzwa5mnvl46gk23cvggp18qxyl02qi364pj1x7k0n53")))

(define-public crate-airline-0.1.6 (c (n "airline") (v "0.1.6") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "12r6hsb6i94rrkxsijg5vf6f4bjpr4145ragvldzpjhg7xa4n3b6")))

(define-public crate-airline-0.1.7 (c (n "airline") (v "0.1.7") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0vi2108pik249k4mzggn7b4ig2ab5p0zm86j8bdl3y782kixln8s")))

(define-public crate-airline-0.1.8 (c (n "airline") (v "0.1.8") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1npv5yfaqg4shmw7m7wy0m4srkh16g7sfpdx70cr23s5gld894z4")))

(define-public crate-airline-0.1.9 (c (n "airline") (v "0.1.9") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1anzvw1lkvlj7681kykn3klxs6q2pjg9jhx4f26w9niihjg1r6c3")))

(define-public crate-airline-0.1.10 (c (n "airline") (v "0.1.10") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1wnymji2q11mq4kbvm0flfwyhpiq7m99a60qzhpnp9a6j6280q7c")))

(define-public crate-airline-0.1.11 (c (n "airline") (v "0.1.11") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0amavrqcx08sk4gbz7f55m14j54h5jdd4k68p9ik216fxzsn67c6")))

(define-public crate-airline-0.1.12 (c (n "airline") (v "0.1.12") (d (list (d (n "airline-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1jlrv8cs5v3ywgllzdk4qxkqxl4ixq2ls646vfsshzsv0x3dnbp2")))

