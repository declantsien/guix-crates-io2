(define-module (crates-io ai rl airline-macros) #:use-module (crates-io))

(define-public crate-airline-macros-0.1.0 (c (n "airline-macros") (v "0.1.0") (h "0lxs7mi7mw0jssn0z7s3q4m64wklnqss6ad23wks0ncc8rd7xry6")))

(define-public crate-airline-macros-0.1.1 (c (n "airline-macros") (v "0.1.1") (h "0rz2r6155z910xphvs9d5qfk7mhsybfg9d756z1g5cgw5120p6ax")))

