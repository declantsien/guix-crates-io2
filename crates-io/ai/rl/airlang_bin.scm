(define-module (crates-io ai rl airlang_bin) #:use-module (crates-io))

(define-public crate-airlang_bin-0.0.3 (c (n "airlang_bin") (v "0.0.3") (d (list (d (n "airlang") (r "^0.0.3") (d #t) (k 0)))) (h "05srrnkhadlx57pjyxxvy7i2wa2i503jfg46kdihlfrdj35inj7r")))

(define-public crate-airlang_bin-0.0.4 (c (n "airlang_bin") (v "0.0.4") (d (list (d (n "airlang") (r "^0.0.4") (d #t) (k 0)))) (h "0mv977fxwmbgsklryjcqxldbqpk9hx04bcwq30ljfg476li5bg4h")))

(define-public crate-airlang_bin-0.0.5 (c (n "airlang_bin") (v "0.0.5") (d (list (d (n "airlang") (r "^0.0.5") (d #t) (k 0)))) (h "1zg1b9brqqbsfpb5m4z7r3m57zigb9adi2yc9gl930qq8nqpm6l2")))

(define-public crate-airlang_bin-0.0.6 (c (n "airlang_bin") (v "0.0.6") (d (list (d (n "airlang") (r "^0.0.6") (d #t) (k 0)))) (h "1hph7kgyywbdz1a8h8a7phcdw2a55m1fgknch67qlszmchwlv8k2")))

(define-public crate-airlang_bin-0.0.7 (c (n "airlang_bin") (v "0.0.7") (d (list (d (n "airlang") (r "^0.0.7") (d #t) (k 0)))) (h "1lwrl6qljzpszl9mkkw7ma5b5mr4ayvdgwg6j9bdg90gzhkah2hm")))

(define-public crate-airlang_bin-0.0.8 (c (n "airlang_bin") (v "0.0.8") (d (list (d (n "airlang") (r "^0.0.8") (d #t) (k 0)))) (h "1fwd4448dhznfzmwv529ny1w0nqr7cssa9cwjbiw3wwpbrpa4s0q")))

(define-public crate-airlang_bin-0.0.9 (c (n "airlang_bin") (v "0.0.9") (d (list (d (n "airlang") (r "^0.0.9") (d #t) (k 0)))) (h "19ymf2crkrk91m2qsgxrms00xv5xlywyliybnpangwp3xpiwskjr")))

(define-public crate-airlang_bin-0.0.10 (c (n "airlang_bin") (v "0.0.10") (d (list (d (n "airlang") (r "^0.0.10") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.0.10") (d #t) (k 0)))) (h "1ck25lapb186f12y5fxy1df3k3szbdq7vh2fhxpikf0spsnvafkr")))

(define-public crate-airlang_bin-0.0.11 (c (n "airlang_bin") (v "0.0.11") (d (list (d (n "airlang") (r "^0.0.11") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.0.11") (d #t) (k 0)))) (h "179jx9rfabmxa6a230dbas22v209x6hcca8463brl4rd72qv4089")))

(define-public crate-airlang_bin-0.0.12 (c (n "airlang_bin") (v "0.0.12") (d (list (d (n "airlang") (r "^0.0.12") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.0.12") (d #t) (k 0)))) (h "0hnvabrpl2ym6jiyzaby0wm8ghcc07sax7zxzjzh6q5ymssvr1gy") (r "1.60")))

(define-public crate-airlang_bin-0.0.13 (c (n "airlang_bin") (v "0.0.13") (d (list (d (n "airlang") (r "^0.0.13") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.0.13") (d #t) (k 0)))) (h "1z2mn4m78cd4gs1jf4cw89bmyb5sda9yq1wmfpjr0lwib2jv2m5y") (r "1.60")))

(define-public crate-airlang_bin-0.1.0 (c (n "airlang_bin") (v "0.1.0") (d (list (d (n "airlang") (r "^0.1.0") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "0i1wib7dv5172a8hm4rsvd5krall19cdlw7879zi89q4pdc8v6k0") (r "1.60")))

(define-public crate-airlang_bin-0.2.0 (c (n "airlang_bin") (v "0.2.0") (d (list (d (n "airlang") (r "^0.2.0") (d #t) (k 0)) (d (n "airlang_ext") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "0h3hnvmj79w43gwlg89ky1pd9nap04kskggkrb86xark5x2df8yw") (r "1.60")))

