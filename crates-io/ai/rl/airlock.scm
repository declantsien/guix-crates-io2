(define-module (crates-io ai rl airlock) #:use-module (crates-io))

(define-public crate-airlock-0.0.1 (c (n "airlock") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "16zfhsrwdgvz3imsmd1xzkq88phccmxai2jx9xzhvpjjmhwsx0a6") (y #t)))

(define-public crate-airlock-0.0.2 (c (n "airlock") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0n0ssxl1209767wvi9f08nzrjmwfwmfwxsmdl5aa2ib0hb75nls2") (f (quote (("std") ("default")))) (y #t) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.3 (c (n "airlock") (v "0.0.3") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "1ivxmiqx4zl5ajv6714c9x873577vkpvffb4q8ic94irli192xxz") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.4 (c (n "airlock") (v "0.0.4") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "14mi3zalmijc7h1cv49dbab284c9x2c9vcwn9icwlwnnf0qmxksh") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.5 (c (n "airlock") (v "0.0.5") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0lf6gkn2i32zp301f0kbl8wbxb00hi7a860gsc094lq26bs0grnh") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.6 (c (n "airlock") (v "0.0.6") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0ywm2aisd491a5carawx94c45l75mli183mk7qmp9gpjrwksp2c2") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.7 (c (n "airlock") (v "0.0.7") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "1inbdihsb8p7n0gxqfpah5by84afxngx80nnf2pcr4cqjyv47vzb") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.8 (c (n "airlock") (v "0.0.8") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "04wh15wg9x78s4g20721r56czszgzvrp5llwpwwsrlwplwjbz6jy") (f (quote (("std") ("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std"))))))

