(define-module (crates-io ai rl airlang_ext) #:use-module (crates-io))

(define-public crate-airlang_ext-0.0.10 (c (n "airlang_ext") (v "0.0.10") (d (list (d (n "airlang") (r "^0.0.10") (d #t) (k 0)))) (h "0zakm197z68w5dlx9bish4k29km3alriyamjk3bp8rl9m4ii09iq")))

(define-public crate-airlang_ext-0.0.11 (c (n "airlang_ext") (v "0.0.11") (d (list (d (n "airlang") (r "^0.0.11") (d #t) (k 0)))) (h "0b0gxm6h2cjc86md0h0dcfdczxfg483krkxp3i0icxbzz8j8n1dc")))

(define-public crate-airlang_ext-0.0.12 (c (n "airlang_ext") (v "0.0.12") (d (list (d (n "airlang") (r "^0.0.12") (d #t) (k 0)))) (h "0c2akgpc4xqf9ay2m4hz7gqy8205k0dzhiknb1pwzyr0kbrvcy5l") (r "1.60")))

(define-public crate-airlang_ext-0.0.13 (c (n "airlang_ext") (v "0.0.13") (d (list (d (n "airlang") (r "^0.0.13") (d #t) (k 0)))) (h "1949zfakazcfwq50n4ah1nxqz9q8dzsk9dwkz40yr5jk3b8rywhx") (r "1.60")))

(define-public crate-airlang_ext-0.1.0 (c (n "airlang_ext") (v "0.1.0") (d (list (d (n "airlang") (r "^0.1.0") (d #t) (k 0)))) (h "1b2j5dg02xf68xv4q9zr82z1fnzl5ifhq4yij0hpgd8qb5gsjyiq") (r "1.60")))

(define-public crate-airlang_ext-0.2.0 (c (n "airlang_ext") (v "0.2.0") (d (list (d (n "airlang") (r "^0.2.0") (d #t) (k 0)))) (h "10a35ghw7khv49qy4bc2pahgzrk33mhrpiqbg2bbhpmbf2hfarxh") (r "1.60")))

