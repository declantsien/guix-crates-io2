(define-module (crates-io ai -b ai-buddy-cli) #:use-module (crates-io))

(define-public crate-ai-buddy-cli-0.0.2 (c (n "ai-buddy-cli") (v "0.0.2") (d (list (d (n "ai-buddy") (r "^0.0.2") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display" "deref"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sjimalg84z45kfr9fmdkypdvxq0lv883m4b13pmi80hs8l1f9y4")))

(define-public crate-ai-buddy-cli-0.0.3 (c (n "ai-buddy-cli") (v "0.0.3") (d (list (d (n "ai-buddy") (r "^0.1.0") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display" "deref"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mhvarw5vci6vz3sqpwdxx1q28kja2csp448yawdzgbj0f87klvl")))

