(define-module (crates-io ai rd airdrop-merkle-tree-near-rs) #:use-module (crates-io))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.0 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "1jblpcwkq9j3384gihcg446f4jy0ylkrblcag3ir0wx56pfkrmmr")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.1 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.1") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "1va00qlwn9zwcbykwsjwsvjyknnz4k5kbha11qh6zax8kmx8adwx")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.2 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.2") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "1xxm0as4i8p1bmn7wrrmhpvqb78wl9ysla5bmfc5ixb6jadnidzg")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.3 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.3") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "13x1qgi9r5sc7996wnix65sbcwqbfphxqjgrdrd31l4s752facmj")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.4 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.4") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "01k93glr14459j9jbiapd9ml3ifb0sipl967dpxyxpw2dx8sfqad")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.5 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.5") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "19ljfbx8bgspx0wrwzqxj31ylkcp5kca2zid0w2qyfm85cq79jsm")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.6 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.6") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "17gsal6f5mx9qj9yq73yns0js3zli0w1z8p71zha97zycgfbbcj5")))

(define-public crate-airdrop-merkle-tree-near-rs-0.1.7 (c (n "airdrop-merkle-tree-near-rs") (v "0.1.7") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "086is2gn91zj1hsax82cvwapyjh67q8rywsi2vpmdni78cphc46z")))

