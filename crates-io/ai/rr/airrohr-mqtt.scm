(define-module (crates-io ai rr airrohr-mqtt) #:use-module (crates-io))

(define-public crate-airrohr-mqtt-0.1.0 (c (n "airrohr-mqtt") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.11") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bpyai2gp59rh2zvh8gbxy4vccm70mxjfjml2aa7l1rn1p8dnp03")))

