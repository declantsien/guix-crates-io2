(define-module (crates-io ai gr aigraph1) #:use-module (crates-io))

(define-public crate-aigraph1-0.1.0 (c (n "aigraph1") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "02nwx4ad64fbb831jrnb33pz8w034qz9r6z7vn715dcz5dxq90wy")))

