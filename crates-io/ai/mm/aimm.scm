(define-module (crates-io ai mm aimm) #:use-module (crates-io))

(define-public crate-aimm-0.1.0 (c (n "aimm") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "14f4hhsyjjsfm80hdj2mxafm6izix6k7q8rq29agb9wpsf7s3pvc")))

(define-public crate-aimm-0.1.1 (c (n "aimm") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "03qn176ygwlj3hgnzc51bx5igdhblg37nl62jc9i45p0vqflql8k")))

