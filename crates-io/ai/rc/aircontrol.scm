(define-module (crates-io ai rc aircontrol) #:use-module (crates-io))

(define-public crate-aircontrol-1.0.0 (c (n "aircontrol") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "hidapi") (r "^2.5.1") (d #t) (k 0)))) (h "1xav2z8amijqbpw65wbpf02sn921hlcqkfv6vhlkba3q8d22ykar")))

