(define-module (crates-io ai rc aircraft_icao_country) #:use-module (crates-io))

(define-public crate-aircraft_icao_country-1.0.0 (c (n "aircraft_icao_country") (v "1.0.0") (h "1h9cprw907iq8p4f5nzqp3swsm1fvq74kzwcvhc2djv3a6sbqgzz")))

(define-public crate-aircraft_icao_country-1.0.1 (c (n "aircraft_icao_country") (v "1.0.1") (h "1k9bxmc3i7nglh9fgyf2zb4slcmg2qm41a1jz1q5j8hhk3zlx5dl")))

(define-public crate-aircraft_icao_country-1.0.2 (c (n "aircraft_icao_country") (v "1.0.2") (h "1gbkvhl6q33gwj2ax48p857vmdi2yzj3hb6g05iy74kf8c9mg33k")))

(define-public crate-aircraft_icao_country-1.0.3 (c (n "aircraft_icao_country") (v "1.0.3") (h "15axgkxww6mzh09prk4l0yq79gcijxwp902kb1n8f6nxm94025zf")))

(define-public crate-aircraft_icao_country-1.0.4 (c (n "aircraft_icao_country") (v "1.0.4") (h "1s5cmc04biq0ks71184a03js89h4zd0w69cz1k5nzvpb2ghrghxi")))

