(define-module (crates-io ai ra airac) #:use-module (crates-io))

(define-public crate-airac-0.1.0 (c (n "airac") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "10i24gwf3hb5hja6fq1mkpx5gjf9il4ckqpxwvmak3n0mp175mp1")))

(define-public crate-airac-0.1.1 (c (n "airac") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1gf46cjb3hk4d9wizj17zq3x22rry94shijwv3n6i4524g26ndp6")))

