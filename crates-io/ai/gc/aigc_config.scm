(define-module (crates-io ai gc aigc_config) #:use-module (crates-io))

(define-public crate-aigc_config-5.1.2 (c (n "aigc_config") (v "5.1.2") (d (list (d (n "aigc_core") (r "^5.1.2") (d #t) (k 0)) (d (n "aigc_p2p") (r "^5.1.2") (d #t) (k 0)) (d (n "aigc_servers") (r "^5.1.2") (d #t) (k 0)) (d (n "aigc_util") (r "^5.1.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1900nhlf3878alsyjbgrkiddg31x3rjag04x99smkd0xrap551sb")))

