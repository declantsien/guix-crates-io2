(define-module (crates-io ai gc aigc_pool) #:use-module (crates-io))

(define-public crate-aigc_pool-5.1.2 (c (n "aigc_pool") (v "5.1.2") (d (list (d (n "aigc_chain") (r "^5.1.2") (d #t) (k 2)) (d (n "aigc_core") (r "^5.1.2") (d #t) (k 0)) (d (n "aigc_keychain") (r "^5.1.2") (d #t) (k 0)) (d (n "aigc_util") (r "^5.1.2") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1ild5s5fdq3dsa4jm8sypaip33fh1nr9495yjixd0mwl4p3m2sca")))

