(define-module (crates-io ai ki aikit) #:use-module (crates-io))

(define-public crate-aikit-0.0.0 (c (n "aikit") (v "0.0.0") (h "18z77zm8qqvg94nr87ylgjdx0qs3jbjplsl1y6bzh0n5qqdgzvpk")))

(define-public crate-aikit-0.0.1 (c (n "aikit") (v "0.0.1") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)))) (h "17wf0jf0ig2h6npk3ianxby4gy58dsfhvvc2wpniw6bkay42y141")))

(define-public crate-aikit-0.0.2 (c (n "aikit") (v "0.0.2") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)))) (h "1yk577giz86g8dkrggmp9fiqiwfc1ybij5szk3vszbgv2x0xnlkq")))

