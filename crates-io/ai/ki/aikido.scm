(define-module (crates-io ai ki aikido) #:use-module (crates-io))

(define-public crate-aikido-0.0.1 (c (n "aikido") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gitoxide") (r "^0.27.0") (d #t) (k 0)) (d (n "tera") (r "^1.19.0") (d #t) (k 0)))) (h "0xyk8gwwzffvcma5zlxnsdgxvv0j7spzqbygls3ymsj6hfmxyxll")))

