(define-module (crates-io ai ge aiger) #:use-module (crates-io))

(define-public crate-aiger-0.1.0 (c (n "aiger") (v "0.1.0") (h "1bdmr8kq15f0kp7b5khyxvx7qqh05xjzyjym4wc8239m6vvahnaj")))

(define-public crate-aiger-0.1.1 (c (n "aiger") (v "0.1.1") (h "0k0abnakz50zgv6w9ivsmpxmhzwn78q68xc8p36wm1z9cph8fjxn")))

(define-public crate-aiger-0.2.0 (c (n "aiger") (v "0.2.0") (h "0ilzd9pqd9ssrlb6fxfnaza014mvzdn7y69rbqhlc7qw0cd9hk7x")))

