(define-module (crates-io ai ge aigen_macros) #:use-module (crates-io))

(define-public crate-aigen_macros-0.1.0 (c (n "aigen_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1q06n6s4bdlrlqad0m9chp0g7cv3h2wrhfkn57snvm899ckj9qqa")))

