(define-module (crates-io ai r0 air001-hal) #:use-module (crates-io))

(define-public crate-air001-hal-0.1.0 (c (n "air001-hal") (v "0.1.0") (d (list (d (n "air001-pac") (r "^0.0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 0)))) (h "0xqng1hcnvyg7wfhbfvx5kypqda6nzk6smhbkgf33wrlkblp9i0p") (y #t)))

