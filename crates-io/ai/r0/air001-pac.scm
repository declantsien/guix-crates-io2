(define-module (crates-io ai r0 air001-pac) #:use-module (crates-io))

(define-public crate-air001-pac-0.0.1 (c (n "air001-pac") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0brd268hnvnkp35rc28nh52s6v08crvhqng11nwvqxzafkxbf4mm") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-air001-pac-0.0.2 (c (n "air001-pac") (v "0.0.2") (d (list (d (n "cortex-m") (r "^0.7") (f (quote ("critical-section-single-core"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0pl0m2x34p8gys9yhig8gy4szb3r2w66bi0kkq1kq9bz82868dzw") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt" "critical-section"))))))

