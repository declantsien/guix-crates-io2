(define-module (crates-io ai r0 air001) #:use-module (crates-io))

(define-public crate-air001-0.0.1 (c (n "air001") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1yhnm4pbydijiir26pln47c8ans8lmaw805fihvqvf0f6rsgbx4f") (f (quote (("default" "rt")))) (s 2) (e (quote (("rt" "dep:cortex-m-rt" "dep:critical-section" "cortex-m-rt/device"))))))

