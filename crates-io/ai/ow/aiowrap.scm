(define-module (crates-io ai ow aiowrap) #:use-module (crates-io))

(define-public crate-aiowrap-0.1.0 (c (n "aiowrap") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3") (d #t) (k 0)))) (h "1xsa0558989fswy1bg65gag0vgykz4id14a44qs0r3xjzwivyxbf")))

