(define-module (crates-io ai de aidecomment) #:use-module (crates-io))

(define-public crate-aidecomment-0.1.0 (c (n "aidecomment") (v "0.1.0") (d (list (d (n "aide") (r "^0.13.2") (f (quote ("axum"))) (k 2)) (d (n "axum") (r "^0.7.4") (f (quote ("macros"))) (k 2)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvn0kdql308l731prx08ab6p7ibrq6qm7r170zyl1b7zdg3i9qz")))

(define-public crate-aidecomment-0.1.1 (c (n "aidecomment") (v "0.1.1") (d (list (d (n "aide") (r "^0.13.2") (f (quote ("axum"))) (k 2)) (d (n "axum") (r "^0.7.4") (f (quote ("macros"))) (k 2)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "056lkhxy16r1ylbh6f063pm5h7ax5fprz6c5r8481kc8zkvjkw2v")))

