(define-module (crates-io ai de aide-axum-typed-multipart) #:use-module (crates-io))

(define-public crate-aide-axum-typed-multipart-0.13.0 (c (n "aide-axum-typed-multipart") (v "0.13.0") (d (list (d (n "aide") (r "^0.13") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "axum_typed_multipart") (r "^0.11.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0)))) (h "03m4hz5lzf4y3w9hgl3vmv9cifl3q7k0kyiizbfllx881a1mr3wv")))

