(define-module (crates-io ai o- aio-cargo-info) #:use-module (crates-io))

(define-public crate-aio-cargo-info-0.1.0 (c (n "aio-cargo-info") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0la51x4hq5s0ngfmbs7634j84xx4l99n6pk9asgy85i17fyf6j37")))

