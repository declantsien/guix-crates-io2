(define-module (crates-io ai o- aio-limited) #:use-module (crates-io))

(define-public crate-aio-limited-0.1.0 (c (n "aio-limited") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "000h0w97nx1x53f24qcq6f2nfly1rbfx7ln59pi8mh1zpi9b643z")))

(define-public crate-aio-limited-0.1.1 (c (n "aio-limited") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1crj3ryq1nnh36wpzay45r97220w9w51rwhjfp5rmnmjn1axzpf4")))

