(define-module (crates-io pj so pjson) #:use-module (crates-io))

(define-public crate-pjson-0.1.0 (c (n "pjson") (v "0.1.0") (h "0a88r7sjhhrpz2jqmdkvn8mjh20zafbh56976kbrhgmarmi2yrbz")))

(define-public crate-pjson-0.1.1 (c (n "pjson") (v "0.1.1") (h "0z1nhc4z99hxmiyzdh1rmlk8c5w11r7677y9nwrn3r26ig88cdjb")))

(define-public crate-pjson-0.1.2 (c (n "pjson") (v "0.1.2") (h "11i9jgcr5i71gc3fh2l0f31mzmgbr4i1jr383nii2yapq6j50ydp")))

(define-public crate-pjson-0.2.1 (c (n "pjson") (v "0.2.1") (h "14jn1lgqdkahzh00mca2aqvlzkvn2xiddavjcp5697by2k10f1fk")))

(define-public crate-pjson-0.2.2 (c (n "pjson") (v "0.2.2") (h "0sm3f30z6fqpa1ldj5zj5xaanna22babx35y4l589zndzb1cqah5")))

