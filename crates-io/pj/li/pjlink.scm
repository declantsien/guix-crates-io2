(define-module (crates-io pj li pjlink) #:use-module (crates-io))

(define-public crate-pjlink-0.1.0 (c (n "pjlink") (v "0.1.0") (d (list (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "1q6schldc7i6k3i2w4r2if569pqsag1a934kmrqw3syk8svb551c")))

(define-public crate-pjlink-0.1.1 (c (n "pjlink") (v "0.1.1") (d (list (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "0s51cni4n729bzlp5x2l2izkxll75cnnp96ssyfb9x650mn09ifs")))

(define-public crate-pjlink-0.2.0 (c (n "pjlink") (v "0.2.0") (d (list (d (n "md5") (r "^0.3.7") (d #t) (k 0)))) (h "1vrclfxz2ng1nr30zqq37w9vjcx0prcjpws4xvwmin63jhlxa2ml")))

