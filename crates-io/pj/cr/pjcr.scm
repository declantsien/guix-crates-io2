(define-module (crates-io pj cr pjcr) #:use-module (crates-io))

(define-public crate-pjcr-0.1.0 (c (n "pjcr") (v "0.1.0") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "166rwcc2pfbssn1psnh5j1mkrqw4s22w8r5hvz7fb7jyzq6hfxs0")))

(define-public crate-pjcr-0.2.0 (c (n "pjcr") (v "0.2.0") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ckcz4pycqpg42x6538lyhi87zjfxpjbs896fga8r32bsswkm88f")))

(define-public crate-pjcr-0.2.1 (c (n "pjcr") (v "0.2.1") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yqrg5d6yw3akbb5rw9nyaznypfjn1m2zm062nbrmg3ji59k1mr2")))

(define-public crate-pjcr-0.3.0 (c (n "pjcr") (v "0.3.0") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01as2wx88s86p1l593prnvwiy2gmz0xakgzjsdfig03y6siz4kgh")))

(define-public crate-pjcr-0.4.0-beta.0 (c (n "pjcr") (v "0.4.0-beta.0") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "078whgwhh9sz14x5i572gxsmw4qxfvqvxidwlc3qfwzkvrimj6x3")))

(define-public crate-pjcr-0.4.0 (c (n "pjcr") (v "0.4.0") (d (list (d (n "confy") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hch3w7xwpy3zrzr7waw6hn18flm7y5zlzbj1drki85785y9rx2v")))

