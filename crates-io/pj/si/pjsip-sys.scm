(define-module (crates-io pj si pjsip-sys) #:use-module (crates-io))

(define-public crate-pjsip-sys-0.0.1-alpha.0 (c (n "pjsip-sys") (v "0.0.1-alpha.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1pfh24c7h5jwhnvl00s2zxysffvirnd7b9sf7w349bvsjzkpp8gh") (y #t)))

