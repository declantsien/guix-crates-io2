(define-module (crates-io pj pr pjproject-sys) #:use-module (crates-io))

(define-public crate-pjproject-sys-1.0.0 (c (n "pjproject-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "169y96jhpvz2cjrmwc6d3vya65dnpzfha8609nz6ghpay5rbwli7")))

