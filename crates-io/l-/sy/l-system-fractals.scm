(define-module (crates-io l- sy l-system-fractals) #:use-module (crates-io))

(define-public crate-l-system-fractals-0.1.0 (c (n "l-system-fractals") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1w9y2zr8hrfw7a4h8gg8khrfl707s5g1hpc0ayfq2cqqzmi1jqsr")))

(define-public crate-l-system-fractals-0.1.1 (c (n "l-system-fractals") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1rw68xlxxprfa17hwha7qxi6c4s7lk5q0z9ppzrwdwnbab97q5k7")))

(define-public crate-l-system-fractals-0.1.2 (c (n "l-system-fractals") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1bpqpf48qymhjfzb64ikp8k2pcjbq9sgy7dlj6n98lxmjlcgybbb")))

