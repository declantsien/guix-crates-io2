(define-module (crates-io p4 ls p4ls) #:use-module (crates-io))

(define-public crate-p4ls-0.1.0 (c (n "p4ls") (v "0.1.0") (h "1rwfjsp0ygvap4p5pbm9pgm1ccjhrr8x0rxny2dxcb6sknglrmsf")))

(define-public crate-p4ls-0.1.1 (c (n "p4ls") (v "0.1.1") (h "0bmhwvssdyp4jcv2x74rafzcaa6rl3qqrag30955s1h9fdjf2p5d")))

