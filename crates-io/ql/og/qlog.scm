(define-module (crates-io ql og qlog) #:use-module (crates-io))

(define-public crate-qlog-0.1.0 (c (n "qlog") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "1db1sm1jj3z6g2pgg7qd51ngn102dc399mvb3xrsp8xngnjx3h97")))

(define-public crate-qlog-0.2.0 (c (n "qlog") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "0pq98km0nz22yc0nx1cz5a3pq42gvkggpxwajmqbkcary97dn7rs")))

(define-public crate-qlog-0.3.0 (c (n "qlog") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "1m0ad0mnldszskhfwwjxpx4q5gbc5cjd6dc5bs6lnrqvf0xr8rs7")))

(define-public crate-qlog-0.4.0 (c (n "qlog") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "1h9by0yzpnd92s53s6q71vl9qs27fj83m2ylk1qr1mj5054xaxw7")))

(define-public crate-qlog-0.5.0 (c (n "qlog") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "1qv7c15zbpjky8nsc9i0lnlyf7y5h7zfiy1k0rx3699vnbha3li7")))

(define-public crate-qlog-0.6.0 (c (n "qlog") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "0gz73jvh2ch4qr9nx607q7xzjyi251xar3fwjcks677h5a1l4n94")))

(define-public crate-qlog-0.7.0 (c (n "qlog") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "15nj07f0la6plj2csknvs50jb67yx0qvq3izl78kqqvmr8890hwd")))

(define-public crate-qlog-0.8.0 (c (n "qlog") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)))) (h "1zxwfawjdp3vw4m8i3plnr6zr8g13xc1nkd45yqlsglk43lkfsnw")))

(define-public crate-qlog-0.9.0 (c (n "qlog") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("serde"))) (d #t) (k 0)))) (h "12v4zdzzgf85ii62w7kgf6vqh7hr6rhhj5m4avi2n5cx36izf79j")))

(define-public crate-qlog-0.10.0 (c (n "qlog") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("serde"))) (d #t) (k 0)))) (h "1zw3lbim7xaqxgdnxjn9yhfch7jgksnyppfl923jpsby51qw3qqr")))

(define-public crate-qlog-0.11.0 (c (n "qlog") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("serde"))) (d #t) (k 0)))) (h "1in7zvpx1iwfpia36036qpcj6vz6b8dmn65x9w3nfph3484qyp3j")))

(define-public crate-qlog-0.12.0 (c (n "qlog") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (f (quote ("macros"))) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("serde"))) (d #t) (k 0)))) (h "03nd3wfcsym88qmznis79rs60bgvrcss35m72jbxkcv9ii1hf14w")))

(define-public crate-qlog-0.13.0 (c (n "qlog") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (f (quote ("macros"))) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pxpxmg4b5dw7dn3gqgq0lfzihmrsd73rfv74blkr4gs42wnapwv")))

