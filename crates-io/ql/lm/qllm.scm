(define-module (crates-io ql lm qllm) #:use-module (crates-io))

(define-public crate-qllm-0.1.0 (c (n "qllm") (v "0.1.0") (h "048qwc7hm5h22kkavbq34fg20p3vk2mxq3rab6iglh9jn64iv8a2")))

(define-public crate-qllm-0.2.0 (c (n "qllm") (v "0.2.0") (h "17c8bk8b67jhhrkv8rhmm827n787g5aasr9lpzx50v4hbdbdiqff")))

