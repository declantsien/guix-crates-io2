(define-module (crates-io ql ru qlrumap) #:use-module (crates-io))

(define-public crate-qlrumap-0.0.1 (c (n "qlrumap") (v "0.0.1") (d (list (d (n "ahash") (r "^0.7.6") (k 0)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)) (d (n "sn_fake_clock") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1a62zai9hv9cxj476g5g8r28w95xihg5xglfhcba4y36a8rzis8y") (f (quote (("fake-clock" "sn_fake_clock"))))))

(define-public crate-qlrumap-0.0.2 (c (n "qlrumap") (v "0.0.2") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (f (quote ("raw"))) (d #t) (k 0)) (d (n "sn_fake_clock") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0j0rbmhp0m36gm53zc8si0xlh6lpb78fci2myx6qn2cr51zaigr6") (f (quote (("fake-clock" "sn_fake_clock"))))))

