(define-module (crates-io l0 g- l0g-macros) #:use-module (crates-io))

(define-public crate-l0g-macros-1.0.0 (c (n "l0g-macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1sigpg2a405aqrl521zw3hm8vpzgbcyx5gkxc1mmndn1gfgarmgv") (f (quote (("log") ("defmt"))))))

