(define-module (crates-io k_ bo k_board) #:use-module (crates-io))

(define-public crate-k_board-1.0.0 (c (n "k_board") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0bf3p1vhzlg3clp6y1lh882wlpmipbx8j0m6fs0490ahx1x6iymy") (y #t)))

(define-public crate-k_board-1.1.0 (c (n "k_board") (v "1.1.0") (h "0b8dsgygz45dyfpb1d5n2xy4d6lz153ikq7sbax8kkccdd4m52px") (y #t)))

(define-public crate-k_board-1.1.1 (c (n "k_board") (v "1.1.1") (h "0lar8svx3x8s8qm2ypn5si75fbi90kki6k7dfrldp0rx5wmwxnh7") (y #t)))

(define-public crate-k_board-1.1.2 (c (n "k_board") (v "1.1.2") (h "025s4zj97ld7g3d642g5q5q7g004q5z458nq6qzzsh8vsx71j11z") (y #t)))

(define-public crate-k_board-1.1.3 (c (n "k_board") (v "1.1.3") (h "15ka2aah07g6xb8mn8vcm5fqzwvji6phlj4vawsj287cy5idc5qn")))

(define-public crate-k_board-1.1.4 (c (n "k_board") (v "1.1.4") (h "0wya5lvz42a4vwhymisxg9qmjzhaayf6ga2wg292gasprw3n9ih0")))

(define-public crate-k_board-1.1.5 (c (n "k_board") (v "1.1.5") (h "169aan1pc43lzmgkbp822j95pa4zfbp3d218lmbzqr69f4lfghdw")))

(define-public crate-k_board-1.1.6 (c (n "k_board") (v "1.1.6") (h "18hgfp6l1svvm7zcd5p3wiw8gwxba1kx5g53f3rsdlpw2w32zkm3")))

(define-public crate-k_board-1.1.7 (c (n "k_board") (v "1.1.7") (h "10p0r7yw8wssfnm8q5hwd6rgyhgrhvcygsb06kb8144ihylvvw33")))

(define-public crate-k_board-1.1.8 (c (n "k_board") (v "1.1.8") (h "1133fz9gvcn17k8v0dbrym05chfz1qlmn3b0z1ga2l9nl5bgpkmr")))

(define-public crate-k_board-1.1.9 (c (n "k_board") (v "1.1.9") (h "17a3xksv0wv6accpgpd70jr2prcjygk1nqixslm2wr7dwp9kz2hc")))

(define-public crate-k_board-1.1.10 (c (n "k_board") (v "1.1.10") (h "0jzx70xn1m8170f8ml7rr4afl72ii3bq422aidk2h3k6vkqlps5n")))

(define-public crate-k_board-1.1.11 (c (n "k_board") (v "1.1.11") (h "0pp6b5iimjj800sp8waikdddbg6i7ql7yyjkfp7a9n6lvg4s5l8h")))

(define-public crate-k_board-1.2.0 (c (n "k_board") (v "1.2.0") (h "0my9rznfc285qhqy1iivd8kjmzhdj2rzfmi5gp030fwqjnv4m7lh") (f (quote (("upper_letter") ("standar") ("numbers") ("lower_letter") ("f") ("ctrl_lower_letter") ("alt_lower_letter"))))))

(define-public crate-k_board-1.2.1 (c (n "k_board") (v "1.2.1") (h "0iqpk0r0g0ixsh4mnc0hd08bgcn87pgk77nkv82n400fmh8dbgwf") (f (quote (("upper_letter") ("standar") ("numbers") ("lower_letter") ("full") ("f") ("ctrl_lower_letter") ("alt_upper_letter") ("alt_lower_letter") ("alt_gr_letter"))))))

(define-public crate-k_board-1.2.2 (c (n "k_board") (v "1.2.2") (h "0rkn12gr3g9948b49yigb2aav5kqzsczy4nrp4s6ylklzw0x7qh5") (f (quote (("upper_letter") ("standar") ("numbers") ("lower_letter") ("full") ("f") ("ctrl_upper_letter") ("ctrl_standar") ("ctrl_numbers") ("ctrl_lower_letter") ("alt_upper_letter") ("alt_numbers") ("alt_lower_letter") ("alt_gr_upper_letter") ("alt_gr_numbers") ("alt_gr_lower_letter"))))))

(define-public crate-k_board-1.2.4 (c (n "k_board") (v "1.2.4") (h "0v8a51y7zzvakxk2wpw50kq555zgmnbymwl1n77cq8nxj6zb5vag") (f (quote (("upper_letter") ("standar") ("numbers") ("lower_letter") ("full") ("f") ("ctrl_upper_letter") ("ctrl_standar") ("ctrl_numbers") ("ctrl_lower_letter") ("alt_upper_letter") ("alt_numbers") ("alt_lower_letter") ("alt_gr_upper_letter") ("alt_gr_numbers") ("alt_gr_lower_letter"))))))

