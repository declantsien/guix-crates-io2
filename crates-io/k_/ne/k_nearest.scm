(define-module (crates-io k_ ne k_nearest) #:use-module (crates-io))

(define-public crate-k_nearest-0.1.0 (c (n "k_nearest") (v "0.1.0") (h "1wgs3bajr1ghqf3sicazwawliczkfg0wilmyxxwn28amjppjfpzg")))

(define-public crate-k_nearest-0.2.0 (c (n "k_nearest") (v "0.2.0") (h "04n98ralldmkhyhfy9b747bkbjgwh5s2v1g0zsi7xndfh6g630k9")))

