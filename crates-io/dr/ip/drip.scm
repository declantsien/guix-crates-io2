(define-module (crates-io dr ip drip) #:use-module (crates-io))

(define-public crate-drip-1.0.1 (c (n "drip") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.3") (d #t) (k 0)) (d (n "powershell_script") (r "^1.0.2") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "12dhc0l70956bbkkryibsssdfxfqkbm734aa92ayjbwll4xvlm2k") (y #t)))

(define-public crate-drip-1.0.2 (c (n "drip") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.3") (d #t) (k 0)) (d (n "powershell_script") (r "^1.0.2") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0bxidfpgc4s738jx8zam8hdjlkmdvgrskdx30g23m1xbwx5ja4jb") (y #t)))

