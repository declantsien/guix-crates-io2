(define-module (crates-io dr ip drips) #:use-module (crates-io))

(define-public crate-drips-0.1.0 (c (n "drips") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.7") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinach") (r "^2.1.0") (d #t) (k 0)))) (h "19a16pzj0pp5q3pjxh3bdf13sajfapfv48xdraprqw4yriydiar6")))

