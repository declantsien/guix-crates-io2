(define-module (crates-io dr m- drm-rs) #:use-module (crates-io))

(define-public crate-drm-rs-0.0.1 (c (n "drm-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "10yr5r5llcab2a8wxd9nb80gk7mz781q8z0fnixz17a1ya8ycq9x")))

(define-public crate-drm-rs-0.0.2 (c (n "drm-rs") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1rlx2xh6a3vra5gr8a5ap600nf1lhpqmhs43gb89l1zprhqpj41z")))

(define-public crate-drm-rs-0.0.3 (c (n "drm-rs") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0cpz5bsy55xfyb9q0gqc2rysb2ipv9grcbjhhaznj0m9njwp4mhw")))

(define-public crate-drm-rs-0.0.4 (c (n "drm-rs") (v "0.0.4") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0phsw9qlp6rarnqilmgf18jgzkd9nhj2395x8f79zpmp3lwcgvjc")))

(define-public crate-drm-rs-0.0.5 (c (n "drm-rs") (v "0.0.5") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1zd58g0jm4wh4f4xnbih3zj4msw0hzrri290c2xnrm3rddl0yy9b")))

(define-public crate-drm-rs-0.0.6 (c (n "drm-rs") (v "0.0.6") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1im6if0z0rmz4yp4bldr93k56098wmdslmfhdl794g0p6xbs8ba1")))

(define-public crate-drm-rs-0.0.7 (c (n "drm-rs") (v "0.0.7") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0qhmiwxy7gwk7g6k3qr37wvlfrwvsqzmmk3hjdcskc7z8d4w727f")))

(define-public crate-drm-rs-0.0.9 (c (n "drm-rs") (v "0.0.9") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "15ark22nw1x2cnfs53cd5xm0859q1r7h9b95yklccx2jnxb79a1b")))

(define-public crate-drm-rs-0.0.10 (c (n "drm-rs") (v "0.0.10") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1a95vzhpfw32xwp6bbrhlvz6sx7cyqssjcm49rj8x212mng2d784")))

(define-public crate-drm-rs-0.0.11 (c (n "drm-rs") (v "0.0.11") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0yvbngcharmlhwdqihgbsdqv9iz0lapvw3gfpdwmrc87sgjlcw0a")))

(define-public crate-drm-rs-0.0.12 (c (n "drm-rs") (v "0.0.12") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "01qcbi85rf92b66rhzrfdx3rwz97r7qmp0s5m7ih8zhpi67pz95g")))

(define-public crate-drm-rs-0.0.13 (c (n "drm-rs") (v "0.0.13") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0hji5mch8r31qd0zwh44gz9cw4fxb1vl4adm0fl5msq2pxb33c0c")))

(define-public crate-drm-rs-0.1.0 (c (n "drm-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "0971mdy1lvmwhs98nrpyrzwrm01h1cnjaim8b8k1bllw82v1ji9y") (y #t)))

(define-public crate-drm-rs-0.1.1 (c (n "drm-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1rk16al2fi9yyxcr4qj8kvnn5cyfyh2cv8l46p642lwjfvbpxgxy")))

(define-public crate-drm-rs-0.1.2 (c (n "drm-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0hirfkfqxzc21cca4f5fyf0i88j60g6bybk9gzd4rn97c08yvisy")))

(define-public crate-drm-rs-0.1.3 (c (n "drm-rs") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0d1clwrgmrv3cr42k06hrk0rs682yk1bs40h0m4cbd8pzvsbdfd6")))

