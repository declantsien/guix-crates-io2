(define-module (crates-io dr m- drm-fourcc) #:use-module (crates-io))

(define-public crate-drm-fourcc-1.0.0 (c (n "drm-fourcc") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (d #t) (k 1)))) (h "1mrh8shxrv12g591d01q591mwn1gdkfpfmd30lb3l6x8mc45gfwf")))

(define-public crate-drm-fourcc-1.0.1 (c (n "drm-fourcc") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (d #t) (k 1)))) (h "0bq2z3ipxqslxscbj3c33qcmm8yp3cvir21na7890b1szl7mbaax")))

(define-public crate-drm-fourcc-1.2.0 (c (n "drm-fourcc") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (d #t) (k 1)))) (h "0nzn6davwh1xwrpsv6dcagd163cav1rphl5yl1552fm7c7php5d7")))

(define-public crate-drm-fourcc-1.3.0 (c (n "drm-fourcc") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (d #t) (k 1)))) (h "1bygp1whffd76vskal2p0fg8rr207fkgyrf69b2sgxxrqilzi5l4")))

(define-public crate-drm-fourcc-1.3.2 (c (n "drm-fourcc") (v "1.3.2") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)))) (h "038jpqzavg2isvrv7pbq3bsrpin0xljx0w512xbm1704zygqkrbd") (f (quote (("default") ("build_bindings" "regex" "bindgen"))))))

(define-public crate-drm-fourcc-1.3.3 (c (n "drm-fourcc") (v "1.3.3") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xpmvmi1slwl8z7vlx3jnzaq2nsvbkbsawhbkbjdb85yb3ifzrpn") (f (quote (("default") ("build_bindings" "regex" "bindgen"))))))

(define-public crate-drm-fourcc-2.0.0 (c (n "drm-fourcc") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16519ma4f9l18kp8a9p94q21b7wll31rhr072s0hdc37ap1rcmqj") (f (quote (("default") ("build_bindings" "regex" "bindgen"))))))

(define-public crate-drm-fourcc-2.1.0 (c (n "drm-fourcc") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wmc7aiqs4jba05jbgf45vk902nkch6sj90sz1w7dh9jf1dniwx3") (f (quote (("default") ("build_bindings" "regex" "bindgen"))))))

(define-public crate-drm-fourcc-2.1.1 (c (n "drm-fourcc") (v "2.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kx0mlr0z9qjaa7a5bn6s9yz5hb97ps2y5yfxzzsn6k7sig3mgzb") (f (quote (("default") ("build_bindings" "regex" "bindgen"))))))

(define-public crate-drm-fourcc-2.2.0 (c (n "drm-fourcc") (v "2.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.4.3") (o #t) (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x76v9a0pkgym4n6cah4barnai9gsssm7gjzxskw2agwibdvrbqa") (f (quote (("std") ("default" "std") ("build_bindings" "regex" "bindgen"))))))

