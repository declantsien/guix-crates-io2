(define-module (crates-io dr m- drm-ffi) #:use-module (crates-io))

(define-public crate-drm-ffi-0.1.0 (c (n "drm-ffi") (v "0.1.0") (d (list (d (n "drm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "01sgkypz488iynyg4wgy3ywwfw3bhaqfn5m2x1v8yafrm5nwbj0c") (f (quote (("use_bindgen" "drm-sys/use_bindgen"))))))

(define-public crate-drm-ffi-0.2.0 (c (n "drm-ffi") (v "0.2.0") (d (list (d (n "drm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "1x875pfl5a3wd7qdl3b08zir87gqa1jks089cy92d21fvxh1fkz4") (f (quote (("use_bindgen" "drm-sys/use_bindgen"))))))

(define-public crate-drm-ffi-0.2.1 (c (n "drm-ffi") (v "0.2.1") (d (list (d (n "drm-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "0lh69wy41ic8rhm5a9gaxvvn01vkvcnq99lbs2dmxdrkvdzip38r") (f (quote (("use_bindgen" "drm-sys/use_bindgen"))))))

(define-public crate-drm-ffi-0.3.0 (c (n "drm-ffi") (v "0.3.0") (d (list (d (n "drm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("ioctl"))) (k 0)))) (h "19dwc6f69ifjwxj2yx4m1jxy9pqgaj7v3kbb7d47czx0bhgi6f75") (f (quote (("use_bindgen" "drm-sys/use_bindgen"))))))

(define-public crate-drm-ffi-0.4.0 (c (n "drm-ffi") (v "0.4.0") (d (list (d (n "drm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.0") (f (quote ("ioctl"))) (k 0)))) (h "1l4nmycddjm3426fllk4wk3ig93kbihbqaryyr013b32s9jhnca8") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.63")))

(define-public crate-drm-ffi-0.5.0 (c (n "drm-ffi") (v "0.5.0") (d (list (d (n "drm-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.0") (f (quote ("ioctl"))) (k 0)))) (h "09fmv3qg5vmk9g17a3j1gjx8vf8qrwrnpvzq3f57mqlhgcdlhlhk") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.63")))

(define-public crate-drm-ffi-0.6.0 (c (n "drm-ffi") (v "0.6.0") (d (list (d (n "drm-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)))) (h "0par8xcrpnz1h53yam4ai9jpqc9as337vclzsn4hw9xnqhciqzds") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.65")))

(define-public crate-drm-ffi-0.7.0 (c (n "drm-ffi") (v "0.7.0") (d (list (d (n "drm-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.22") (d #t) (k 0)))) (h "0x0ix9z0fb01x8b0z47vcg9r1w7m0bg017m1zyx0qaxz5v0xh392") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.65")))

(define-public crate-drm-ffi-0.7.1 (c (n "drm-ffi") (v "0.7.1") (d (list (d (n "drm-sys") (r "^0.6.1") (d #t) (k 0)) (d (n "rustix") (r "^0.38.22") (d #t) (k 0)))) (h "1iiardadqms7gaad50qq9624xzw0asfbnpyh5biq693r0n24ycs1") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.65")))

(define-public crate-drm-ffi-0.8.0 (c (n "drm-ffi") (v "0.8.0") (d (list (d (n "drm-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "rustix") (r "^0.38.22") (d #t) (k 0)))) (h "0lwd4zvimd4132cpay3vs41gf1sv3s4gx37a997wnz4bwhkqgjcp") (f (quote (("use_bindgen" "drm-sys/use_bindgen")))) (r "1.65")))

