(define-module (crates-io dr m- drm-sys) #:use-module (crates-io))

(define-public crate-drm-sys-0.0.0 (c (n "drm-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1smfmrk87xfdkmwszlf8z6p3hadmpqbrkikwr0rz8dy082mn891k")))

(define-public crate-drm-sys-0.0.1 (c (n "drm-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0xa835knxnzkffnl0wpf5jjy33rhrq2m4gb6d8jzx3gnm17h9vxi")))

(define-public crate-drm-sys-0.0.2 (c (n "drm-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0vqdys196pvxbawkzysw9sczic5ih90pz5xy4mpi3b06mfj8m3kp")))

(define-public crate-drm-sys-0.0.3 (c (n "drm-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.25") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "086q47wgz7ihhv8pqkz03a8k7zaacqwgi8g5rylqm2670g76phbm") (f (quote (("use_bindgen" "bindgen") ("default"))))))

(define-public crate-drm-sys-0.0.4 (c (n "drm-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.25") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "09xlc5njc6qnj0k36mxysjxbhid927ny3bnc4kbrqsg72zj7gzww") (f (quote (("use_bindgen" "bindgen") ("default"))))))

(define-public crate-drm-sys-0.0.5 (c (n "drm-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.25") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "09xnpb37p4n9wj1b89jjamvfy7242zsmjzrx5c6sknskgsqv3khk") (f (quote (("use_bindgen" "bindgen") ("default"))))))

(define-public crate-drm-sys-0.0.6 (c (n "drm-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.25") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1d71pi9sk2d05cigfpzirkcb74w7rw549w7lw42b9y9059l2cpvf") (f (quote (("use_bindgen" "bindgen") ("default"))))))

(define-public crate-drm-sys-0.0.7 (c (n "drm-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.25") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1rrwaa336js1hr4hcq658afyp2qmi2y3nd51xl67yl23isczr9n5") (f (quote (("use_bindgen" "bindgen") ("default"))))))

(define-public crate-drm-sys-0.0.8 (c (n "drm-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "185c7nlbnfay2057ibm6m4kxg275j7zfipyipvq1nlwvi2bjka0x") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("default"))))))

(define-public crate-drm-sys-0.0.9 (c (n "drm-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "17qbmw41jfjmdq7dcv4kb1l0n8zww5l03pj1f6qdnw4p22qj7qz5") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("default"))))))

(define-public crate-drm-sys-0.1.0 (c (n "drm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0y5pnq0ai120v8vsi1q678hydc6vz1b747ff1vjr7qivhbl5a619") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default"))))))

(define-public crate-drm-sys-0.1.1 (c (n "drm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1lh3adg0zix7np66g7gzahc8i0nr8gr88vvq0q268qjj2hwzp7i9") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default"))))))

(define-public crate-drm-sys-0.1.2 (c (n "drm-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0779rzp3njmckq1xl8ahwmykw0m2xnjb44gamlzw3wxjsg6mzx7m") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default"))))))

(define-public crate-drm-sys-0.2.0 (c (n "drm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0f538b1xd51lcqycflqfh2vxm9549qmf32arbmx158l17gvp7rdb") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default"))))))

(define-public crate-drm-sys-0.3.0 (c (n "drm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0xv3nhc40ql99fdgdxy7q6iwwydym0xcx8jq992dfkx6jv5d3v2j") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.63")))

(define-public crate-drm-sys-0.4.0 (c (n "drm-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.29") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0bg4f95lg8hb10las2cab55wghhmqh9hc7jc9hinsw3bkmkz2s8k") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.63")))

(define-public crate-drm-sys-0.5.0 (c (n "drm-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1bhmwzbraxclivn2h83ab7aqdcly82sy7w85az6mcah6d021qkrs") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.65")))

(define-public crate-drm-sys-0.6.0 (c (n "drm-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "linux-raw-sys") (r "^0.6") (f (quote ("general" "no_std"))) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0fshj0qkgw2i7dpwnj69fkli20k84xgwbhsxks5dm68cqqz2h5ai") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.65")))

(define-public crate-drm-sys-0.6.1 (c (n "drm-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "linux-raw-sys") (r "^0.6") (f (quote ("general" "no_std"))) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0xiir3qkl6ccw898zbhdnckxyjlgzws5xfh526qiiwcj3y4gy29d") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.65")))

(define-public crate-drm-sys-0.7.0 (c (n "drm-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(any(target_os = \"android\", target_os = \"linux\")))") (k 0)) (d (n "linux-raw-sys") (r "^0.6") (f (quote ("general" "no_std"))) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "11irvbzb0rbsm5zxgxsbaca8f6nvkl8kvwk3hwp6q6bf1gjdsfgx") (f (quote (("use_bindgen" "bindgen" "pkg-config") ("update_bindings" "use_bindgen") ("default")))) (r "1.65")))

