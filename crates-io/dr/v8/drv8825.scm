(define-module (crates-io dr v8 drv8825) #:use-module (crates-io))

(define-public crate-drv8825-0.3.0 (c (n "drv8825") (v "0.3.0") (d (list (d (n "step-dir") (r "^0.3.0") (d #t) (k 0)))) (h "0wg19vywps9ajzpxrywjs09qd6ms4jkvswjarg7kw9pwbvnq80gb")))

(define-public crate-drv8825-0.4.0 (c (n "drv8825") (v "0.4.0") (d (list (d (n "step-dir") (r "^0.4.0") (f (quote ("drv8825"))) (k 0)))) (h "14c6xm5jd0v3dab8qbbard9ff8zccdhw7jigy5rzlgksg2a7mwq6")))

(define-public crate-drv8825-0.4.1 (c (n "drv8825") (v "0.4.1") (d (list (d (n "step-dir") (r "^0.4.1") (f (quote ("drv8825"))) (k 0)))) (h "0rvzwxmrk0zb64xlac5pngfvipl4rgllihj6r0pixmz69ch4yq0g")))

(define-public crate-drv8825-0.5.0 (c (n "drv8825") (v "0.5.0") (d (list (d (n "stepper") (r "^0.5.0") (f (quote ("drv8825"))) (k 0)))) (h "0gkyzfg63iw4vz3xf6q5ag6bdf0nwki5vqivsjzf3pynw4x0cjal")))

(define-public crate-drv8825-0.6.0 (c (n "drv8825") (v "0.6.0") (d (list (d (n "stepper") (r "^0.6.0") (f (quote ("drv8825"))) (k 0)))) (h "0d012avdambbqfwj2nlpw92xfr5n09mwiir360l2j35vy532msf0")))

