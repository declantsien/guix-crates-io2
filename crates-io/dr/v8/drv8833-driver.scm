(define-module (crates-io dr v8 drv8833-driver) #:use-module (crates-io))

(define-public crate-drv8833-driver-0.1.0 (c (n "drv8833-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)))) (h "0agwxspi722asi8zgbwsgkzpvy56ji0wcm5if4l93bp05dlmwcgr")))

