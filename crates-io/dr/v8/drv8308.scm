(define-module (crates-io dr v8 drv8308) #:use-module (crates-io))

(define-public crate-drv8308-0.0.1 (c (n "drv8308") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.1") (d #t) (k 0)))) (h "0lb9lvj0w1hklf2clpdrapx8a43x9whkr1k1lsdyq5hapsfgycjn")))

