(define-module (crates-io dr ed dredge) #:use-module (crates-io))

(define-public crate-dredge-0.1.0 (c (n "dredge") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "rustbox") (r "^0.9") (d #t) (k 0)))) (h "001dq1pkbd9jrvy7lpblf3kxmbi0b5gl67lizf5m62dl2rdaazag")))

(define-public crate-dredge-0.1.1 (c (n "dredge") (v "0.1.1") (d (list (d (n "clap") (r "^2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "rustbox") (r "^0.9") (d #t) (k 0)))) (h "1lilshlx0djgsdw3k7h514l2g7bx1s75znmjp1208jizpqm6mgaw")))

