(define-module (crates-io dr ow drow) #:use-module (crates-io))

(define-public crate-drow-0.1.0 (c (n "drow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6.6") (d #t) (k 0)) (d (n "slog") (r "^2.0.6") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "1qmzklgs5s92vm4mmrlq4wllnjif62v3q3cxbw1mymsa5w397hiq") (y #t)))

(define-public crate-drow-0.1.1 (c (n "drow") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6.6") (d #t) (k 0)) (d (n "slog") (r "^2.0.6") (d #t) (k 0)) (d (n "sloggers") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "0lalg87rjnvcma1zvnahs9yw5rp13axfmibqnay0a6wmw605pfdk") (y #t)))

(define-public crate-drow-0.1.2 (c (n "drow") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (f (quote ("wrap_help" "color"))) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "git2") (r "^0.6.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0lhq3c4szapklpkb5dlciv4p522bs91m7fi22bfliqlwynhwffnx") (y #t)))

