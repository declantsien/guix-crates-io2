(define-module (crates-io dr oi droid) #:use-module (crates-io))

(define-public crate-droid-0.1.0 (c (n "droid") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "026zbw6ryi5f8nrnvv62b53sprz3dls1by0hz1ksbzq68yfqcad8")))

(define-public crate-droid-0.1.1 (c (n "droid") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17cf5q3l0038n10v045ak5qmwhapfv57rnzn0pza7yvc6mr7nfpb")))

(define-public crate-droid-0.1.2 (c (n "droid") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hpa6ywha2rjadfwq7dgx7f33khxy5dsswqy7m55pjpdh67w9b0m")))

(define-public crate-droid-0.1.3 (c (n "droid") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dghbx9hr4c1by11qx6vnf1dn63ypqk7gfmfpwxcnm93pbx6q712")))

(define-public crate-droid-0.1.4 (c (n "droid") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ird2y4n2smkhafji1hr1l1y005rb0imlmldygv7zjkccl30p5bz")))

(define-public crate-droid-0.1.5 (c (n "droid") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "13xzz40zmjpzhvhwfahwipadipic235cl9ir4llv9k6firv7v1w6")))

