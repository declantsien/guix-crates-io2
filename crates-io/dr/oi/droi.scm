(define-module (crates-io dr oi droi) #:use-module (crates-io))

(define-public crate-droi-0.1.0 (c (n "droi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0q07ip47w1dr62qqzbk61alndczpa2dh8dih107jpvs7wnv06ary")))

(define-public crate-droi-0.1.1 (c (n "droi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0zkp8ndrb9zw44k8nj31yaqvc0jdh9mgy0zx3bjambfz0i3z0gxh")))

(define-public crate-droi-0.2.0 (c (n "droi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "12n7abx5spv6gbcvs493z22a9894niml77b014y79qqkdd5fjjn1")))

(define-public crate-droi-0.2.1 (c (n "droi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1h37wgviiarva3h2b76afkj4724iknlxk54pblgp9k62srjm7zfx")))

(define-public crate-droi-0.2.2 (c (n "droi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "06bpgyfjb66p8rzgfvx2r3li1hrqpvll9nkqd9k681x8mnpq4vzf")))

(define-public crate-droi-0.2.4 (c (n "droi") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1014lw07il67nbaprvpw3nmqsaspasgf3n49qrvpw8ryr5hxcm3w")))

(define-public crate-droi-0.2.5 (c (n "droi") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1qq9f29zfm3lqh8lawv2p8w01nlm6l2kcl5yjrl32qa3gp3fv0i4")))

(define-public crate-droi-0.2.6 (c (n "droi") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0293z4w4pwrymwcrsdph95i32x5q6wvwb71b0ky33a3rxlmp4bxq")))

(define-public crate-droi-0.2.7 (c (n "droi") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1lci070varsjgbnxdccw3c0ymbml0yhsnifvadfn5yx8qv8j7a08")))

