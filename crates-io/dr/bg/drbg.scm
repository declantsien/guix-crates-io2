(define-module (crates-io dr bg drbg) #:use-module (crates-io))

(define-public crate-drbg-0.1.0 (c (n "drbg") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "02q6623k5i4fhiym7al08prnslmggbsk7xiyinbylzkvgzcrqc7k")))

(define-public crate-drbg-0.1.1 (c (n "drbg") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1pb1ni5mbq766dm2mla3dinib46lc18m9f98j47an0mqi9xwn4jd")))

(define-public crate-drbg-0.1.2 (c (n "drbg") (v "0.1.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "06nr57xnca7p0zxp9axlzdp4ql1abzl2cpj90qndrbwzkxd6a6di")))

(define-public crate-drbg-0.1.3 (c (n "drbg") (v "0.1.3") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "0igxcg1bnkc5casgkr1129s0fmgddqgfais31zbld3dgnx7cbn6r")))

(define-public crate-drbg-0.2.0 (c (n "drbg") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "0l493rdjnnncs1fqb4i50klxmdvn3z9wbi01n329kj8676r0yhkd")))

