(define-module (crates-io dr il drillhash) #:use-module (crates-io))

(define-public crate-drillhash-0.1.0 (c (n "drillhash") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.18.11") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cq75capzxj1y6q6165vhc9yzvzj7miapc3qlimbvyzdix6i5ybk") (f (quote (("solana" "solana-program") ("default" "blake3" "sha3"))))))

