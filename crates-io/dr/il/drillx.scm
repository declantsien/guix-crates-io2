(define-module (crates-io dr il drillx) #:use-module (crates-io))

(define-public crate-drillx-0.1.0 (c (n "drillx") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.18.11") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p9pkgcnm4fjfv0zk9v9fh9jgf7lymswijxnd173gsibxfm1v3y7") (f (quote (("solana" "solana-program") ("default" "blake3" "sha3") ("benchmark"))))))

