(define-module (crates-io dr an drand-client-rs) #:use-module (crates-io))

(define-public crate-drand-client-rs-0.1.0 (c (n "drand-client-rs") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.8.0") (f (quote ("experimental"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hq9ls6h55cfn1bdxllxxknv7g69kg52lsdqhmc4c7lq4zp7faj1")))

