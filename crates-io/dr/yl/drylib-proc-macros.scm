(define-module (crates-io dr yl drylib-proc-macros) #:use-module (crates-io))

(define-public crate-drylib-proc-macros-0.1.0 (c (n "drylib-proc-macros") (v "0.1.0") (h "1fsbb5bwz5d9x8ilk56nn7176ihwlydnpqs74xbx8569v0d2hakb") (f (quote (("muts") ("mutclones") ("default" "muts" "clones" "mutclones") ("clones-prefix-clone") ("clones-prefix-clon") ("clones-prefix-clo") ("clones-prefix-cl") ("clones-prefix-c") ("clones")))) (r "1.79.0")))

(define-public crate-drylib-proc-macros-0.1.1 (c (n "drylib-proc-macros") (v "0.1.1") (h "1j7jf6kndml5finicfqm3h3a0ck3x1lv0islwkv1g8ajhhqbm79p") (f (quote (("muts") ("mutclones") ("default" "muts" "clones" "mutclones") ("clones-prefix-clone") ("clones-prefix-clon") ("clones-prefix-clo") ("clones-prefix-cl") ("clones-prefix-c") ("clones")))) (r "1.79.0")))

(define-public crate-drylib-proc-macros-0.1.2 (c (n "drylib-proc-macros") (v "0.1.2") (h "1v95xhksvs8v4hg331355kc881bzfv342ny43vkf17waij29jd8j") (f (quote (("muts") ("mutclones") ("default" "muts" "clones" "mutclones") ("clones-prefix-clone") ("clones-prefix-clon") ("clones-prefix-clo") ("clones-prefix-cl") ("clones-prefix-c") ("clones")))) (r "1.79.0")))

