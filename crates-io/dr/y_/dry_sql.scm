(define-module (crates-io dr y_ dry_sql) #:use-module (crates-io))

(define-public crate-dry_sql-0.1.1 (c (n "dry_sql") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "0aidj50h9bgbyab5iw1z3jifdjwf2za1zs0pwv8qpj0y7w03snyk")))

(define-public crate-dry_sql-0.1.2 (c (n "dry_sql") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "0vfdmyr5sallsy6lrn9j6pgzlwpv7qxgr6kzz0q0y7yg91cpk1rk")))

(define-public crate-dry_sql-0.1.3 (c (n "dry_sql") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "0wcmypdhw1fdsb53j73233fxj4dqvz1cfkky70zpbqps3by2jl7l")))

(define-public crate-dry_sql-0.1.4 (c (n "dry_sql") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "0k4vr9bjf05vq3m4kjvbln4pj619mkzmrf1j069sjdvh0f7ds1pn")))

