(define-module (crates-io dr ea dreadfort) #:use-module (crates-io))

(define-public crate-dreadfort-0.1.0 (c (n "dreadfort") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "0byq8j55mj88bb7giniyphpdsxnsfkwacn985dpkxn1ld67xz4gn")))

