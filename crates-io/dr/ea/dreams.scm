(define-module (crates-io dr ea dreams) #:use-module (crates-io))

(define-public crate-dreams-0.0.1 (c (n "dreams") (v "0.0.1") (d (list (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "03f2srpmfpvjkx8dgv4gqjyxim7pcrnwdls90ppvqrf4faz6mw8r") (f (quote (("web_warp" "warp" "url")))) (y #t)))

