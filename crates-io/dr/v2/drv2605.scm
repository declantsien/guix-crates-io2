(define-module (crates-io dr v2 drv2605) #:use-module (crates-io))

(define-public crate-drv2605-0.1.0 (c (n "drv2605") (v "0.1.0") (d (list (d (n "bitfield") (r "~0.13") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 2)) (d (n "cortex-m-rt") (r "~0.5") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "jlink_rtt") (r "~0.1") (d #t) (k 2)) (d (n "metro_m0") (r "^0.1.0") (d #t) (k 2)) (d (n "panic_rtt") (r "~0.1") (d #t) (k 2)))) (h "11sym0pmrkn5yd41sm2hr2pag39rks6r6q7lrpig4wdgmv49lqjc") (f (quote (("use_semihosting"))))))

