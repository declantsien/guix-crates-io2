(define-module (crates-io dr v2 drv2605l) #:use-module (crates-io))

(define-public crate-drv2605l-0.1.0 (c (n "drv2605l") (v "0.1.0") (d (list (d (n "bitfield") (r "~0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "jlink_rtt") (r "^0.2.0") (d #t) (k 2)) (d (n "metro_m0") (r "^0.9.0") (d #t) (k 2)) (d (n "panic_rtt") (r "^0.3.0") (d #t) (k 2)))) (h "0z050bp2aw7lbm9hm2h5kq618cjay0klpdk49d1a88k2am50ff89")))

