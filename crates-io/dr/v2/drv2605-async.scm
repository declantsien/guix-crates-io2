(define-module (crates-io dr v2 drv2605-async) #:use-module (crates-io))

(define-public crate-drv2605-async-0.1.0 (c (n "drv2605-async") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.15") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "0ll18m2l25mwpq1mc83sag6pk03vn7jby44rag9qs8fl2i4xlq3g")))

