(define-module (crates-io dr iv driveby) #:use-module (crates-io))

(define-public crate-driveby-1.0.0 (c (n "driveby") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ympdnns78b4w67ly51rgzi8bqdlpnj1m826hwny8gj9mmrq70c8")))

(define-public crate-driveby-1.0.1 (c (n "driveby") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0s9azr0w3r9kva9pqx7d2k7vll6rv8x9wd6zm1740lzk8nbz2c70")))

