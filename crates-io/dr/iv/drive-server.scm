(define-module (crates-io dr iv drive-server) #:use-module (crates-io))

(define-public crate-drive-server-0.1.1 (c (n "drive-server") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "dns-message-parser") (r "^0.4.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.33") (d #t) (k 0)) (d (n "pnet") (r "^0.27.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zp7w8gv36m34dihxkf9q0s9d117wwhx3bhqs413mzqwakawifsl")))

(define-public crate-drive-server-0.1.2 (c (n "drive-server") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "dns-message-parser") (r "^0.4.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.33") (d #t) (k 0)) (d (n "pnet") (r "^0.27.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cywss21j7x66imwwnca2vl8g6ym0xn5j2ms9vldr4p7g2lwwv7y")))

