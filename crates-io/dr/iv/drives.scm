(define-module (crates-io dr iv drives) #:use-module (crates-io))

(define-public crate-drives-0.0.1 (c (n "drives") (v "0.0.1") (h "0ciyvrjck4546xsfn1gcpgrlj5nzfgg6rxpmqhycvlpa6bq4z9jk")))

(define-public crate-drives-0.0.2 (c (n "drives") (v "0.0.2") (d (list (d (n "mockall") (r "^0.11.3") (d #t) (k 2)))) (h "1i7qh0cxlw16l09i7vcwnva5pwmz30v3424lp69kqhhfcsjk69ym")))

(define-public crate-drives-0.1.0 (c (n "drives") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gww9rx4pyn1zvl3kwwairhwgn25nw9kwxrfxgsj4l6vhz3336hc")))

(define-public crate-drives-0.2.0 (c (n "drives") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1d8n1rhybx8ziih4vfrkw0c4yfsvwgl4ycympxzpl6riiyzxs88a")))

(define-public crate-drives-0.3.0 (c (n "drives") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02x1dy3cs02c4ai769jymvmbv4qdi2g09fq3jwzlam0rfj337ks2")))

(define-public crate-drives-0.3.1 (c (n "drives") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1fn0r089lvn6nbphr6zb57bgax1nv36as0gjikd536sn8pw3k5xs")))

(define-public crate-drives-0.4.0 (c (n "drives") (v "0.4.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k1xf4rqm8irhdnl8h2vz0n3jdghnwr67qyk5q0svqr2kh4idvsj")))

(define-public crate-drives-0.4.1 (c (n "drives") (v "0.4.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19vcxw46vfs5wxr68wshjzsvhsi1f3dgka391q1r0ygx5wccnxsz")))

(define-public crate-drives-0.5.0 (c (n "drives") (v "0.5.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16332x6k383hfl38yijm83m8bbclp5jh0a93lym54phk0jzpz7ah")))

(define-public crate-drives-0.6.0 (c (n "drives") (v "0.6.0") (d (list (d (n "gpt") (r "^3.1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lg5c9ssvs2ccm10k3krlgxb2sr4j2bmcy34ix20mja3h19vahdm")))

