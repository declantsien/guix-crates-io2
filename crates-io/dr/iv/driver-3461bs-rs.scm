(define-module (crates-io dr iv driver-3461bs-rs) #:use-module (crates-io))

(define-public crate-driver-3461bs-rs-0.1.0 (c (n "driver-3461bs-rs") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1zz3vzi19808aqrh3wfrvh220yzmyxxr8x86sr47q52ykp5zj0p0") (y #t)))

(define-public crate-driver-3461bs-rs-0.1.1 (c (n "driver-3461bs-rs") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0c9bqb7x0mbgl466qnr28l75w3z1m8sq1d7465gjsqc2midd08b4") (y #t)))

(define-public crate-driver-3461bs-rs-0.1.2 (c (n "driver-3461bs-rs") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0l3knrhff4fjr0c6yp2lkch5whzh82gpmqbx55adn23pbafgzjnc")))

