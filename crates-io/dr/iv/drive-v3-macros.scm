(define-module (crates-io dr iv drive-v3-macros) #:use-module (crates-io))

(define-public crate-drive-v3-macros-0.6.0 (c (n "drive-v3-macros") (v "0.6.0") (d (list (d (n "drive-v3") (r "^0.5.2") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "11pgvpi7qv55kixf9mb6xp82w6avqlsrbccgw0yi4q7b8q4b2qa1")))

(define-public crate-drive-v3-macros-0.6.1 (c (n "drive-v3-macros") (v "0.6.1") (d (list (d (n "drive-v3") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1v9xjzncxklf0kgz29va1dkqldkpr0mkglq16bhbgaph8aais2my")))

