(define-module (crates-io dr iv driver-74hc595) #:use-module (crates-io))

(define-public crate-driver-74hc595-0.1.0 (c (n "driver-74hc595") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "07y2kwm0b9nharjr5dvk83q2ldg77aavaa3cskm7r8vpj8lja5b4")))

