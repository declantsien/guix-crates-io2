(define-module (crates-io dr iv drive_74hc595) #:use-module (crates-io))

(define-public crate-drive_74hc595-0.1.0 (c (n "drive_74hc595") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "19w5w8jlsjfhqqpg1xddjj9qqv816amxbqrhmwv7zbhv5swjp82g")))

(define-public crate-drive_74hc595-0.2.0 (c (n "drive_74hc595") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "03mcarl3fs1q0gca3rzn4n8z129yvyj9dac839znpkdk1s03vv19")))

