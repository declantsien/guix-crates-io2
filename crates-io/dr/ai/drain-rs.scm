(define-module (crates-io dr ai drain-rs) #:use-module (crates-io))

(define-public crate-drain-rs-0.1.0 (c (n "drain-rs") (v "0.1.0") (d (list (d (n "grok") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lcgw7ndxqa4dqi6zpjipn05x7l9kg5426wrxswc7b5w5p2q5apw")))

(define-public crate-drain-rs-0.2.0 (c (n "drain-rs") (v "0.2.0") (d (list (d (n "grok") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1aiyh0ii8lwkqa6j05218l9vdisqv2n2nxq7qn1nv07nbs07g0h3")))

(define-public crate-drain-rs-0.3.0 (c (n "drain-rs") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)) (d (n "grok") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18dk6nndmqc9d04kwb0hirzyn58psypqpvabgk0g02vxic2fkxaf")))

