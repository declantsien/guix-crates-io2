(define-module (crates-io dr ai drain) #:use-module (crates-io))

(define-public crate-drain-0.0.0 (c (n "drain") (v "0.0.0") (h "0j8901xdvrbwz4r6307a5ag1q2z89kaxn8hkv4bwxfbf6z3mp4pn")))

(define-public crate-drain-0.0.1 (c (n "drain") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tower") (r "^0.4.7") (o #t) (k 0)))) (h "0n65db4nmp4nyiapzgxdk0qhwvb326rqwlglw8i1i9cv4yphzkms") (f (quote (("retain" "tower"))))))

(define-public crate-drain-0.1.0 (c (n "drain") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.15") (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tower") (r "^0.4.7") (o #t) (k 0)))) (h "062agrdmpdjqm9prdg7p44498zbmm8wncgavbpr0kwyza473h0cp") (f (quote (("retain" "tower"))))))

(define-public crate-drain-0.1.1 (c (n "drain") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.15") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tower") (r "^0.4.7") (o #t) (k 0)))) (h "0bb3sxdpv2cvn5mix80lhjv153j0lw3l4h8fvnsdkynf7yzhl6ig") (f (quote (("retain" "tower"))))))

(define-public crate-drain-0.1.2 (c (n "drain") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.15") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tower") (r "^0.4.7") (o #t) (k 0)))) (h "07mwv29y9mxxy7zdzn2d00vas7h08nj7kz8q6frzqp9bpll5044x") (f (quote (("retain" "tower"))))))

