(define-module (crates-io dr ai drainrs) #:use-module (crates-io))

(define-public crate-drainrs-0.1.0 (c (n "drainrs") (v "0.1.0") (d (list (d (n "indextree") (r "^4.5.0") (d #t) (k 0)) (d (n "json_in_type") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 2)))) (h "0llvmc8zj5v6j54igx9saqmpxmbivizdz6j0s1gk7zb6xl87dldh")))

