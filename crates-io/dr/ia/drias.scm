(define-module (crates-io dr ia drias) #:use-module (crates-io))

(define-public crate-drias-0.0.1 (c (n "drias") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0shc5fdgi05s1pnr9982ny2cii2xkf0hnrxdwr81sigxjn5gbdqp") (f (quote (("diag_ext" "diag") ("diag_emulate" "diag" "yansi") ("diag_api" "diag") ("diag") ("default" "diag" "diag_api" "diag_ext" "diag_emulate"))))))

(define-public crate-drias-0.0.2 (c (n "drias") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0h99xyidd7maap9zhm019pfkar7hy7vs6bjz1i82a38z0mn839ip") (f (quote (("diag_ext" "diag") ("diag_emulate" "diag" "yansi") ("diag_api" "diag") ("diag") ("default" "diag" "diag_api" "diag_ext" "diag_emulate"))))))

(define-public crate-drias-0.0.3 (c (n "drias") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "yansi") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "14yrm9agv655idb0lkgaywngxlsypzqgx0az8286fiv02yami0c3") (f (quote (("diag_ext" "diag") ("diag_emulate" "diag" "yansi") ("diag_api" "diag") ("diag") ("default" "diag" "diag_api" "diag_ext" "diag_emulate"))))))

