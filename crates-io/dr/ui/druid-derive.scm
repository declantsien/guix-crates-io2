(define-module (crates-io dr ui druid-derive) #:use-module (crates-io))

(define-public crate-druid-derive-0.1.1 (c (n "druid-derive") (v "0.1.1") (d (list (d (n "druid") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0nka0zr9an02pfh08kazqvb01kapincxwbxg19jbizhimi39i3gs")))

(define-public crate-druid-derive-0.1.2 (c (n "druid-derive") (v "0.1.2") (d (list (d (n "druid") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1l42k1s5ms507vwlpydnm9rjbqwayzgahzk511p6nk47ay4b9zp1")))

(define-public crate-druid-derive-0.3.0 (c (n "druid-derive") (v "0.3.0") (d (list (d (n "druid") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "082dx932y3zqy5hc6ciqjdax0jdacgbdcp89df2wmzv2gacdskq5")))

(define-public crate-druid-derive-0.3.1 (c (n "druid-derive") (v "0.3.1") (d (list (d (n "druid") (r "^0.6.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.8.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "0ilkxdxw7wwz1428glfndrx5mfrmmhfbkhnjpllldprq14xp0lsb")))

(define-public crate-druid-derive-0.4.0 (c (n "druid-derive") (v "0.4.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.8.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "062b9256796mainn97bcv5xsl8gxhnnpshfhw2a5hn5lwv7kzdap")))

(define-public crate-druid-derive-0.5.0 (c (n "druid-derive") (v "0.5.0") (d (list (d (n "druid") (r "^0.8.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wnf2gz69mdrqq7fzvnnyghqfk62n8p8ds1azzaf61qc9g0ly8zi")))

(define-public crate-druid-derive-0.5.1 (c (n "druid-derive") (v "0.5.1") (d (list (d (n "druid") (r "^0.8.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1jynqzdln1k4b4qjcrx9bgb78l8n4kxcjkbzrcn8m25ih926d3c0")))

