(define-module (crates-io dr ui druid-lens-compose) #:use-module (crates-io))

(define-public crate-druid-lens-compose-0.1.1 (c (n "druid-lens-compose") (v "0.1.1") (d (list (d (n "druid") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.51, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "157rz3ibm7ldvda5j8jbdc61i6gchs4655rlz77b4hi0x2w029yn")))

(define-public crate-druid-lens-compose-0.2.0 (c (n "druid-lens-compose") (v "0.2.0") (d (list (d (n "druid") (r "^0.6.0") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full"))) (d #t) (k 0)))) (h "1gdx0ydi708myxdrq4704rh1rx7ar2gs6ks2idnccdwv2ri7s1ri")))

