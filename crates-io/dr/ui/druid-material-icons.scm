(define-module (crates-io dr ui druid-material-icons) #:use-module (crates-io))

(define-public crate-druid-material-icons-0.1.0 (c (n "druid-material-icons") (v "0.1.0") (d (list (d (n "kurbo") (r "^0.8.3") (d #t) (k 0)))) (h "0pswd6jfzgcw10r1w1wqsfqkrq1ffv5sj9zjb7c7csw3fc04xja8")))

(define-public crate-druid-material-icons-0.2.0 (c (n "druid-material-icons") (v "0.2.0") (d (list (d (n "druid") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "kurbo") (r "^0.9") (d #t) (k 0)))) (h "1yc89976bf3cam56wl3aqd5jiailw1k98xa0msa0xrldn5myqjvb") (f (quote (("default" "druid"))))))

