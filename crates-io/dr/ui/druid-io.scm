(define-module (crates-io dr ui druid-io) #:use-module (crates-io))

(define-public crate-druid-io-0.1.0 (c (n "druid-io") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "rt-threaded" "blocking"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "022db4yy2f2nr4c6ic06v3qxv5rb4yv0qb7mj655px4yc0v93pvh")))

