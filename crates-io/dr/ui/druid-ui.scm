(define-module (crates-io dr ui druid-ui) #:use-module (crates-io))

(define-public crate-druid-ui-0.0.0 (c (n "druid-ui") (v "0.0.0") (h "1fwimcsdasbvxqm7i8c7n1v58l43f4xyd80kdb5k2x6a4vlym8la")))

(define-public crate-druid-ui-0.0.1 (c (n "druid-ui") (v "0.0.1") (d (list (d (n "druid") (r "^0.8") (f (quote ("image-all"))) (d #t) (k 0)) (d (n "druid-shell") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1ycq152qfpzxb7b222bgyk8z27j83shca6azi1p30nghz26k3pwi")))

