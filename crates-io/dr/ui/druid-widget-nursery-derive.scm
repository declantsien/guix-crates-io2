(define-module (crates-io dr ui druid-widget-nursery-derive) #:use-module (crates-io))

(define-public crate-druid-widget-nursery-derive-0.1.0 (c (n "druid-widget-nursery-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "02i0rv64xf6y9xdyhif0jrnniniz92nny2ffwk9xapjwaqh17bgr")))

