(define-module (crates-io dr aw draw-lr) #:use-module (crates-io))

(define-public crate-draw-lr-0.1.0 (c (n "draw-lr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01hh0qc319swi4c7ypca9mxwrqq779xwdlvvzcsrnm3h2x6a8w6c")))

