(define-module (crates-io dr aw drawbridge-byte) #:use-module (crates-io))

(define-public crate-drawbridge-byte-0.1.0 (c (n "drawbridge-byte") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("alloc"))) (o #t) (k 0)))) (h "07h90bpav9424b6wqchq9rf9aa078gf566y93ri2l22w3g1wdp36")))

(define-public crate-drawbridge-byte-0.2.0 (c (n "drawbridge-byte") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (k 0)))) (h "1hggz20y61bwz7jf0fzhnbn5nzbh7acrz2dwsr3asl3n3kkfmhlr") (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-drawbridge-byte-0.3.0 (c (n "drawbridge-byte") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (k 0)))) (h "04ggfdkaijhxmkzs9ksfccswm5pzw9sjzwx8h3k6sprzcmvj9bc4") (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-drawbridge-byte-0.4.0 (c (n "drawbridge-byte") (v "0.4.0") (d (list (d (n "base64") (r "^0.13.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (k 0)))) (h "14xckl5pi6f65zl11lfpgidqhdl0xf8xp5qibvb0iymzp0815i6y") (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-drawbridge-byte-0.4.2 (c (n "drawbridge-byte") (v "0.4.2") (d (list (d (n "base64") (r "^0.22.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (k 0)))) (h "1yf913gw9x6ww9vlzh44lfqvlmh7fs7038r4sk343ii41ixmqxcd") (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

