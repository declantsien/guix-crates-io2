(define-module (crates-io dr aw drawn) #:use-module (crates-io))

(define-public crate-drawn-0.1.0 (c (n "drawn") (v "0.1.0") (h "08znn3x320bs69v0w8hfl9jsphcf2rbvivcc10dvbng67msvyaha")))

(define-public crate-drawn-0.1.1 (c (n "drawn") (v "0.1.1") (h "1hm0xm67xdy6218ahvglgivcjsphrpnssm8qvb6vghbsi1gyjk88")))

(define-public crate-drawn-0.1.2 (c (n "drawn") (v "0.1.2") (d (list (d (n "sdl2") (r "0.32.*") (d #t) (k 0)))) (h "1ha3f77k64skr0b9b5i655z0bhzg3k3c65c26hxs0m8qp2km829p")))

(define-public crate-drawn-0.1.3 (c (n "drawn") (v "0.1.3") (d (list (d (n "sdl2") (r "0.35.*") (d #t) (k 0)))) (h "1fyym480zgkhhczh4jkzlkm19v2rc0dcg2c6spgrynqjw1xgrbm4")))

