(define-module (crates-io dr aw draw_box) #:use-module (crates-io))

(define-public crate-draw_box-0.1.0 (c (n "draw_box") (v "0.1.0") (h "0gq47sk8rzxgfw8wy487khms3w50yi5yd3q8hch560g4fldfnlak")))

(define-public crate-draw_box-0.2.0 (c (n "draw_box") (v "0.2.0") (h "0ckag4h6hn9yhq5rqrjnbm23gpa9hzvcqp80xxf587vr0qdc9l6a")))

(define-public crate-draw_box-0.2.1 (c (n "draw_box") (v "0.2.1") (h "0rd2bv1i50rlnr1nmprzhdlfrk06yri22g9w2504n9g62zh5kjzm")))

