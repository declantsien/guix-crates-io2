(define-module (crates-io dr aw drawing) #:use-module (crates-io))

(define-public crate-drawing-0.1.0 (c (n "drawing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.21") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z4xc68hprhn1vmmqf0p5zva7i4gh62c9pnjv6hnalc4vr19ygyl")))

(define-public crate-drawing-0.2.0 (c (n "drawing") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sq8zqy8h6xn8l9n6d2n6jf6gcmp6aqqjj0k6yzp0zmn7wpaizlq")))

(define-public crate-drawing-0.3.0 (c (n "drawing") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.29") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pgrhnjggn2s9q1k7zxlararyj09gcd58z6vzfiqv8ma2x0in8ag")))

(define-public crate-drawing-0.3.1 (c (n "drawing") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.29") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hly0l98nri6g1fw6v72rbwcshryw026q8n6bnj8mnh1ch010jgy")))

(define-public crate-drawing-0.3.2 (c (n "drawing") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.29") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ad5xwajl578y1b3wyxcdh8hldi1gkl1y0lq2qh9344izsvwv1cl")))

(define-public crate-drawing-0.3.3 (c (n "drawing") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.29") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "169blacandkwsf1knqdv42mbzrshc6rzgj8qk31icfbd572f15c0")))

(define-public crate-drawing-0.4.0 (c (n "drawing") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "clamped") (r "^1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.36") (d #t) (k 0)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cwwg283bn0wjldj16wh4a5rpzp8zrqcdshb0g7wsnkwf2nhf83a")))

