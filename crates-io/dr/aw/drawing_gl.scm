(define-module (crates-io dr aw drawing_gl) #:use-module (crates-io))

(define-public crate-drawing_gl-0.1.0 (c (n "drawing_gl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drawing") (r "^0.1") (d #t) (k 0)) (d (n "euclid") (r "^0.21") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "winit") (r "^0.22") (d #t) (k 0)))) (h "0y0zjfwv5gab4n84wy1c29qbibwax9m6z9l9qpdm0zfsj6kb94qi")))

(define-public crate-drawing_gl-0.2.0 (c (n "drawing_gl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drawing") (r "^0.2") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)))) (h "12y0llxcp59bah83g4vvyg0fdrhb6m6hxx8fmd37r5qphjjw8q9a")))

(define-public crate-drawing_gl-0.3.0 (c (n "drawing_gl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drawing") (r "^0.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)))) (h "10ycc4x2779j99j9g3k6wk741k6dqr1gqhyyaz731yrlfpv0yk0y")))

(define-public crate-drawing_gl-0.4.0 (c (n "drawing_gl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drawing") (r "^0.4") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)))) (h "1di02x0lb5zibrvfdjl8xrmlgafzrccsx1hql2mkhbry1kgydxf7")))

