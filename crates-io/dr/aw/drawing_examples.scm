(define-module (crates-io dr aw drawing_examples) #:use-module (crates-io))

(define-public crate-drawing_examples-0.4.0 (c (n "drawing_examples") (v "0.4.0") (d (list (d (n "cpu-time") (r "^1.0") (d #t) (k 0)) (d (n "drawing") (r "^0.4") (d #t) (k 0)) (d (n "drawing_gl") (r "^0.4") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "fui_system") (r "^0.13") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "rust-embed") (r "^8.4") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "07wz426dbsvxdj2dxwh81xl60cnv2c473hl2zvgy21i9z7md7307")))

