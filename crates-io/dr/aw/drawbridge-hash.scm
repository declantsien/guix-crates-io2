(define-module (crates-io dr aw drawbridge-hash) #:use-module (crates-io))

(define-public crate-drawbridge-hash-0.1.0 (c (n "drawbridge-hash") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default"))) (k 2)) (d (n "base64") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "futures") (r "^0.3.21") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std"))) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "1a46bi5fbajkz8a3w5v019wy90il7948yj2mbhwrw64b7mhp8795")))

