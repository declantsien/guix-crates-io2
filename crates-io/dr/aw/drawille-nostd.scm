(define-module (crates-io dr aw drawille-nostd) #:use-module (crates-io))

(define-public crate-drawille-nostd-0.1.0 (c (n "drawille-nostd") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (k 0)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)))) (h "028k76qb07dpjmdrdkzj5k7kc24w4s5s2x0ka9cxc5i9m68lxf73") (y #t)))

(define-public crate-drawille-nostd-0.1.1 (c (n "drawille-nostd") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)))) (h "05nj15kplajzamfpqvg7shxnsswdmylic1n6l49lzpcnqr2by59l") (y #t)))

(define-public crate-drawille-nostd-0.1.2 (c (n "drawille-nostd") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)))) (h "0dbsbq4245mdv4z4dy6cbmbr058n4ibyq9i95la29nblka3crkw8")))

