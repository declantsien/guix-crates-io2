(define-module (crates-io dr aw drawille) #:use-module (crates-io))

(define-public crate-drawille-0.2.0 (c (n "drawille") (v "0.2.0") (h "1hyazc3jw8cz291b9fn28q9wln6kgcdb0wx93g8zyw9px0irc9lm")))

(define-public crate-drawille-0.2.1 (c (n "drawille") (v "0.2.1") (h "1pf3dfgikgnk0cka9swlba5hk3k16wsvv2wp4qkap3ixwsh88cy5")))

(define-public crate-drawille-0.2.2 (c (n "drawille") (v "0.2.2") (h "0s21cai2vv01iivsppg5qd1ppzpp3ch7hhmrhbkan3773dwvxp6g")))

(define-public crate-drawille-0.2.3 (c (n "drawille") (v "0.2.3") (h "0d6n3h2f76amrw2lymdsxm7kg9g53ifmysignc5n3xbnkk0jgqc1")))

(define-public crate-drawille-0.3.0 (c (n "drawille") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0igq3l70qxpnd6py89ifvbp0j0szznrl01k2fa9xjs8y7wf4ckp6")))

