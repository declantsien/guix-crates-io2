(define-module (crates-io dr aw draw-music) #:use-module (crates-io))

(define-public crate-draw-music-0.0.1 (c (n "draw-music") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1h9whg8b43fvj1m9k1qcq4wj4ybz5wkzssxyh9n4ir5f8blrv2y2") (f (quote (("default" "console_error_panic_hook"))))))

