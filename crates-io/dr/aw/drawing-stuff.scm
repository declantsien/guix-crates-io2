(define-module (crates-io dr aw drawing-stuff) #:use-module (crates-io))

(define-public crate-drawing-stuff-0.1.0 (c (n "drawing-stuff") (v "0.1.0") (h "0i3bw11qc6aiy9lxbn1bwq3a7y2j4v6dxyi2png6zzmxkm96kcvx")))

(define-public crate-drawing-stuff-0.2.0 (c (n "drawing-stuff") (v "0.2.0") (h "1883ax9jac7nqzk6fq1q32d2f1vc29n4i55igirr3pbk3yfiyigb")))

(define-public crate-drawing-stuff-0.2.1 (c (n "drawing-stuff") (v "0.2.1") (h "1afpn0c03qivsijs2l05r1wqm5iib8slvzcry4jdab445dp0ghv6")))

