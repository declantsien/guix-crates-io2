(define-module (crates-io dr aw draw_queue) #:use-module (crates-io))

(define-public crate-draw_queue-0.1.0 (c (n "draw_queue") (v "0.1.0") (h "04cqw9wlaq38lfg555195gcq9rpckmhic8aap98f4sw7gwg7gfnq")))

(define-public crate-draw_queue-0.1.1 (c (n "draw_queue") (v "0.1.1") (h "1kanhpnldchvi8kzkgnp4kws33aqlavjc70shhbsv36w6w1y8w81")))

