(define-module (crates-io dr aw draw_state) #:use-module (crates-io))

(define-public crate-draw_state-0.0.1 (c (n "draw_state") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "03pcx8g8x525bx9zcg7xkij8kpflmrq9kpp7b7j0c6cpbqz3cq0h")))

(define-public crate-draw_state-0.0.2 (c (n "draw_state") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1wj64a8bd14p3ig302hrf5ymi1h0bybg22ik6zf6ip4s4451ycpq")))

(define-public crate-draw_state-0.0.5 (c (n "draw_state") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1nvmrwnd3p8miqmcnlpf8nmlr4j7d173i03w5iwsbmykn9vwlj05")))

(define-public crate-draw_state-0.0.7 (c (n "draw_state") (v "0.0.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1k2g1sdf2zagaa2dji77vcq5gfyavari5g90b1baj31if5bg6jlz")))

(define-public crate-draw_state-0.0.8 (c (n "draw_state") (v "0.0.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0lascrwrrafky7l0ri0xz6scs3gmw6kqfrcnvwaqbcypnxzpk7zq")))

(define-public crate-draw_state-0.1.0 (c (n "draw_state") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "19ycrk6q6xvnrrd6fl1pxr8jhlwgwqgf3ld4zbdjfrr1zhql7yxm")))

(define-public crate-draw_state-0.1.1 (c (n "draw_state") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)))) (h "0z2h60fm1jhfprg7s5ndx5r6l0myww93clk335dspd04kzcmzy9p")))

(define-public crate-draw_state-0.1.2 (c (n "draw_state") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)))) (h "1f9rxaqbv93c5ip42var1d8hiscbwsdb3f1hyml5fplfrwr7lljk")))

(define-public crate-draw_state-0.2.0 (c (n "draw_state") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)))) (h "1kh1zksmgdpqzshvfn0pclfk0zv4ih95xaixs4cmm0svi2xw8s2j")))

(define-public crate-draw_state-0.3.0 (c (n "draw_state") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)))) (h "04fy0lg9hj0qkgl36dh3zikjbammwfpi4n0j8nclqlfcc5ddrdih")))

(define-public crate-draw_state-0.4.0 (c (n "draw_state") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)))) (h "1a8abw232184c0zpg5d9s5xb9g07v7b3dq0d0aajrzl5xgqrllj9")))

(define-public crate-draw_state-0.5.0 (c (n "draw_state") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)))) (h "162pz3b4kbf4s48xxns65wnzjvjgrd75gwsljs2dmzdanyccac4m")))

(define-public crate-draw_state-0.6.0 (c (n "draw_state") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)))) (h "11c1ivczgxmivrbx451jg6hnc4nhganrvl6md17wh7kwigdgr5hm")))

(define-public crate-draw_state-0.7.0 (c (n "draw_state") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p739p0l4afbx3z3r5jszb33ws7rlaknmsdjknqbicn685mfgfxa") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-draw_state-0.7.1 (c (n "draw_state") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1affj11r9qy7yjyrkxk5djlbjh7bqhm2bzq15sgg4q4gm16fnyik") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-draw_state-0.8.0 (c (n "draw_state") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0lfng4fz9x7bwsmzv9r20ply10w0iid6vfcrhx292s6hw8vrbkrk")))

