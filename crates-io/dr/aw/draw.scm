(define-module (crates-io dr aw draw) #:use-module (crates-io))

(define-public crate-draw-0.1.0 (c (n "draw") (v "0.1.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.1") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1lwrfx7lwj8pnjw532py82pvia3l2kzgq5ccfbci79rc05spvps3")))

(define-public crate-draw-0.2.1 (c (n "draw") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "008jn6iffinpi5wjcvjh3v5gp0vcm24z29bdy6p584hczb9mfhzg")))

(define-public crate-draw-0.2.2 (c (n "draw") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0rzck5jfqli1qi3jmih4jrfsbjdcm2zvl3h9xlx81942pr98c9lm")))

(define-public crate-draw-0.2.3 (c (n "draw") (v "0.2.3") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "1waybi2k4zmsdb6l4mshcsf9l4j35cdc92qd5jhjgp2cnlhfxyns")))

(define-public crate-draw-0.2.4 (c (n "draw") (v "0.2.4") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0yhb9savfpx7smmnhlhx6wlmwfzcdk0046l6vwqj2rq25xk47wy4")))

(define-public crate-draw-0.2.5 (c (n "draw") (v "0.2.5") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0ijgl0db1vab6fy1qj4d0wnykycbnyabmn9jg4n7882rqd6psznh")))

(define-public crate-draw-0.2.6 (c (n "draw") (v "0.2.6") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "1bc9rlqbw6lfj7mmn0y5syfyj12x1z7id2x88y2r45cvj298fy8w")))

(define-public crate-draw-0.2.7 (c (n "draw") (v "0.2.7") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0x08dld03x1n78xf68idaiqa8y0xr79dcd15if6jdvbhgmc2ncsr")))

(define-public crate-draw-0.2.8 (c (n "draw") (v "0.2.8") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "1y07g4rz196dnsjiqlf8h4142pbdwxdlivz0b6xndzjhzi6bs9iv")))

(define-public crate-draw-0.2.9 (c (n "draw") (v "0.2.9") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0diivh6wl6m8ax5zzp7qnpa2hag21p4nmrydza8hhi0wp8kc9lxx")))

(define-public crate-draw-0.3.0 (c (n "draw") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "04pm7n60qxamc84vy5mrphg7n94dvlkq7n2dc9vrpxc86kgp513q")))

