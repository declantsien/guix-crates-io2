(define-module (crates-io dr es dresscode) #:use-module (crates-io))

(define-public crate-dresscode-0.1.0 (c (n "dresscode") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0ri35lrdni77xlsalcm0d5jhwh956gq23cm756p7zpfy1ghhpp9x")))

