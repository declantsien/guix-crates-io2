(define-module (crates-io dr ak drake) #:use-module (crates-io))

(define-public crate-drake-0.1.0 (c (n "drake") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "patricia_tree") (r "^0.6.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)) (d (n "tree-sitter-swift") (r "=0.3.6") (d #t) (k 0)))) (h "1vyxgw27psw1zs4ia9njinfrhrx9gdxnkywbb2wa0kl94shmc08z")))

(define-public crate-drake-0.2.0 (c (n "drake") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "patricia_tree") (r "^0.6.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.4") (d #t) (k 0)) (d (n "tree-sitter-swift") (r "=0.3.6") (d #t) (k 0)))) (h "15xfv0k1q34g1hqkz380fwc6gpdq88snmc0xdqfgm74s4q188qh1")))

