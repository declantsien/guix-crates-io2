(define-module (crates-io dr ak drakey-fuse) #:use-module (crates-io))

(define-public crate-drakey-fuse-0.4.0-alpha (c (n "drakey-fuse") (v "0.4.0-alpha") (d (list (d (n "drakey-fuse-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread-scoped") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0fcqp977hr8mxfc1n9s8vlmrv3c7vkbykcwpba1x6y0m8fwhmyax")))

(define-public crate-drakey-fuse-0.4.1-alpha (c (n "drakey-fuse") (v "0.4.1-alpha") (d (list (d (n "drakey-fuse-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread-scoped") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1isx1agkdphycv6lxri3s5cgkhxcdjhlw5isr82790mrcn1m9k99")))

(define-public crate-drakey-fuse-0.4.2-alpha (c (n "drakey-fuse") (v "0.4.2-alpha") (d (list (d (n "drakey-fuse-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread-scoped") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "063hkr2ix0c6vr7pwqh2p4r6dipbz5h2iqsrxzin9ls29smi8p10")))

