(define-module (crates-io dr ft drftr) #:use-module (crates-io))

(define-public crate-drftr-0.1.0 (c (n "drftr") (v "0.1.0") (d (list (d (n "poise") (r "^0.5.5") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1q71lk32v6mj4xvpcigshd6s97618995wi3cc4q824ckgrswi0xr")))

(define-public crate-drftr-0.1.1 (c (n "drftr") (v "0.1.1") (d (list (d (n "poise") (r "^0.5.5") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1zpmws4vsb3ajxy968zzn5lxxpaqvr5q7syjsy30kz20acychk91")))

