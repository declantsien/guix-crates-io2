(define-module (crates-io dr is driscoll) #:use-module (crates-io))

(define-public crate-driscoll-0.1.0 (c (n "driscoll") (v "0.1.0") (h "097k40xzhi41xw8f8870ghj3hs0llk5i9rlznfjgg8dg5kdzqlq2")))

(define-public crate-driscoll-0.1.1 (c (n "driscoll") (v "0.1.1") (h "1g50mwiir72jz5azk01viga80f8ngqnn5q594i8c9b44xss8drsq")))

(define-public crate-driscoll-0.1.2 (c (n "driscoll") (v "0.1.2") (h "027g95f87iz64gw2phw3fssmn0ykk8jgdwf8fcflv1mwz3daz639")))

