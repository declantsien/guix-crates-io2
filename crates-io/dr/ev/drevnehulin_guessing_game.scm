(define-module (crates-io dr ev drevnehulin_guessing_game) #:use-module (crates-io))

(define-public crate-drevnehulin_guessing_game-0.1.0 (c (n "drevnehulin_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15d2lj1xw7jamr69bhg2i93gvax889mc5pj8dn2r1lrgk1n6rdbl")))

(define-public crate-drevnehulin_guessing_game-0.1.1 (c (n "drevnehulin_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10b6d1xkh3bi92jlz3cc9nrwg17wa9c3h9pjxl0wsqb44r3798mf")))

