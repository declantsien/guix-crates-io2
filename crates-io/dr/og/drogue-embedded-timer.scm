(define-module (crates-io dr og drogue-embedded-timer) #:use-module (crates-io))

(define-public crate-drogue-embedded-timer-0.1.0 (c (n "drogue-embedded-timer") (v "0.1.0") (h "1pas7d028zkhg8ycvrn4y17srvp3wr3pji4d7k93jx2zxzq5yal3")))

(define-public crate-drogue-embedded-timer-0.2.0 (c (n "drogue-embedded-timer") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)))) (h "1jd7c9rhkgdcxnk9406xih9ls2fvinj1xsrwcpzlqifdy9i90q47")))

(define-public crate-drogue-embedded-timer-0.2.1 (c (n "drogue-embedded-timer") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)))) (h "12mkg7yhkc1kl3hhk6xhn46zm71draxh2vczch3gp4jwbp61dkmk")))

