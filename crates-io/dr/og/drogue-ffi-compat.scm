(define-module (crates-io dr og drogue-ffi-compat) #:use-module (crates-io))

(define-public crate-drogue-ffi-compat-0.1.0 (c (n "drogue-ffi-compat") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "04vwxbd30qfv3sza6wrk8y7dag4q4z0qw89da8pxjq8cs1vhp50w")))

