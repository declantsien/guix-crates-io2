(define-module (crates-io dr og drogue-tls-sys) #:use-module (crates-io))

(define-public crate-drogue-tls-sys-0.1.0 (c (n "drogue-tls-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "drogue-ffi-compat") (r "^0.1.0") (d #t) (k 0)))) (h "0v1s4x97hra6ysl8w0frj99ls6j8fpla4y4dwhx31qig00745gwl") (f (quote (("generate" "bindgen") ("default"))))))

