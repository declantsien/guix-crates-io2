(define-module (crates-io dr og drogue-mpu-6050) #:use-module (crates-io))

(define-public crate-drogue-mpu-6050-0.1.0 (c (n "drogue-mpu-6050") (v "0.1.0") (d (list (d (n "drogue-embedded-timer") (r "^0.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-time") (r "^0.10.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0zvj2bmgjszilmynq4v60ffsy0d3dybvxv7wr1s6ag5ir0paa90m")))

