(define-module (crates-io dr og drogue-ajour-protocol) #:use-module (crates-io))

(define-public crate-drogue-ajour-protocol-0.1.0 (c (n "drogue-ajour-protocol") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_cbor") (r "^0.11") (f (quote ("std"))) (d #t) (k 2)))) (h "08f5yn7261ml3n8ycm5l1mcnv05h0i4vpb07aq4i0h1y19xj7ys8") (f (quote (("std" "serde_bytes/std") ("default" "std"))))))

(define-public crate-drogue-ajour-protocol-0.2.0 (c (n "drogue-ajour-protocol") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_cbor") (r "^0.11") (f (quote ("std"))) (d #t) (k 2)))) (h "10nygww99vn9h6pq022b1p0k591aab6y0x4rw3zijyl1zbg8zkw1") (f (quote (("std" "serde_bytes/std") ("default" "std"))))))

(define-public crate-drogue-ajour-protocol-0.4.0 (c (n "drogue-ajour-protocol") (v "0.4.0") (d (list (d (n "heapless") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_cbor") (r "^0.11") (f (quote ("std"))) (d #t) (k 2)))) (h "02xfqjd4d7p0j45gd46sw6hzhab888msb040w5prqr13nlqhn5hy") (f (quote (("std" "serde_bytes/std") ("default" "std"))))))

(define-public crate-drogue-ajour-protocol-0.5.0 (c (n "drogue-ajour-protocol") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_cbor") (r "^0.11") (f (quote ("std"))) (d #t) (k 2)))) (h "1w45x7qljn2vfxdhs1hwaigb18rf1wz93krkklc650z4qyh093ki")))

(define-public crate-drogue-ajour-protocol-0.6.0 (c (n "drogue-ajour-protocol") (v "0.6.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_cbor") (r "^0.11") (f (quote ("std"))) (d #t) (k 2)))) (h "0nz6h0dspgvcm6zjjipxs1g4xkpq3hrh78q7jzbjkqsi0fv325qy") (s 2) (e (quote (("defmt" "dep:defmt"))))))

