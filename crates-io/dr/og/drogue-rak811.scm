(define-module (crates-io dr og drogue-rak811) #:use-module (crates-io))

(define-public crate-drogue-rak811-0.3.2 (c (n "drogue-rak811") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "moveslice") (r "^2.0.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (k 0)))) (h "0dk8dzmpab164sggzx236dmn2nkdn9kglnzc0iswr6gllnb17zv4") (y #t)))

(define-public crate-drogue-rak811-0.1.0 (c (n "drogue-rak811") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "moveslice") (r "^2.0.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (k 0)))) (h "1kc53857npnb09m0qiis6hd8rfx6lb9i0fjza3qs6988zk5md0bs") (y #t)))

(define-public crate-drogue-rak811-0.4.0 (c (n "drogue-rak811") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "moveslice") (r "^2.0.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (k 0)))) (h "09rqa6f53870qnchi513l6i9a5g5z7qk8liynh50jy6zdxvdbzcd")))

