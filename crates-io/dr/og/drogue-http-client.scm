(define-module (crates-io dr og drogue-http-client) #:use-module (crates-io))

(define-public crate-drogue-http-client-0.0.1 (c (n "drogue-http-client") (v "0.0.1") (d (list (d (n "drogue-network") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "httparse") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1cq6whc3jl974xzfs1zhxcayldn1g8pkc6n7zgr9ynm76dzrccv2")))

(define-public crate-drogue-http-client-0.0.2 (c (n "drogue-http-client") (v "0.0.2") (d (list (d (n "drogue-network") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "httparse") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "1ihb08cqy5nv1ydd2yc4m47bgyaqmjjbflz78q1ryq8vaa1ip73s")))

