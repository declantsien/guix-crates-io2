(define-module (crates-io dr og drogue-boot) #:use-module (crates-io))

(define-public crate-drogue-boot-0.1.0 (c (n "drogue-boot") (v "0.1.0") (h "19wqyjk3ywc3gwbm2p8k045d9brf28wjrhb5116nbis6f4f5zy19")))

(define-public crate-drogue-boot-0.1.1 (c (n "drogue-boot") (v "0.1.1") (h "0livfrqd3b2wmsh3qjd7xhk15dh49axvhfpsxfw4qav4k9363jmm")))

(define-public crate-drogue-boot-0.1.2 (c (n "drogue-boot") (v "0.1.2") (d (list (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (o #t) (d #t) (k 0)))) (h "17ng1g4j657czxm266r9vbg7jm8j422wc5rw030ljxl21s31pfqn") (f (quote (("rtt" "rtt-target"))))))

