(define-module (crates-io dr if driftwood) #:use-module (crates-io))

(define-public crate-driftwood-0.0.1 (c (n "driftwood") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1kqmwp7m1vamhsvxdhxn7878gcg8kz25ibnnb9wxwlnj26wgl6ad")))

(define-public crate-driftwood-0.0.3 (c (n "driftwood") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1ss1hppcq8ixg2bdcyykjb04l5n6g76fb45afsi61q3jvrihwdfq")))

(define-public crate-driftwood-0.0.4 (c (n "driftwood") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 0)))) (h "0s2axcvqgv781xzrjs6vcp9zr3cf1rycyf4i39rbv0fivrg6ffgw")))

(define-public crate-driftwood-0.0.5 (c (n "driftwood") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "181ck55m6l7ms6x1vvj7ddga7hsrg3krf7i2kb3iyrl5hxsqdxx1")))

(define-public crate-driftwood-0.0.6 (c (n "driftwood") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "15f9l3mr46zh2880g7fx5qxiy6lhjv2i64kma2n1y0168wx05cqk")))

(define-public crate-driftwood-0.0.7 (c (n "driftwood") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "size") (r "^0.4.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "1894wd24ffi5n9gnir2z312vzbih9w5v7wkp6yma6v45z27zk120")))

