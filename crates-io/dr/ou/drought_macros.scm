(define-module (crates-io dr ou drought_macros) #:use-module (crates-io))

(define-public crate-drought_macros-0.0.0 (c (n "drought_macros") (v "0.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "194bgvz96c9fl7n41jmy0iy2wsiicz4d0cfm3wyqrhc35x8n5nmk")))

(define-public crate-drought_macros-0.0.1 (c (n "drought_macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f5w1bb56a0hr68m5pjxnlwr1kki6syhv6bxns32bv32prqjrfhl")))

(define-public crate-drought_macros-0.0.2 (c (n "drought_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ki7z0crsb8qkzfhq7zcfhbz01y9zi2z9vk04xqfpaj3ff0sffv5")))

