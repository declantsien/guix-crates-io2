(define-module (crates-io dr ou drought) #:use-module (crates-io))

(define-public crate-drought-0.0.0 (c (n "drought") (v "0.0.0") (d (list (d (n "drought_macros") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "10mpvhhhrgb2hhg3zpr8qzshzq4l4lli28yk8997l6yx5l75pf6c") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:drought_macros"))))))

(define-public crate-drought-0.0.1 (c (n "drought") (v "0.0.1") (d (list (d (n "drought_macros") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0f2xd8wz14vk2fwwmcsnnlsd3sgp896xzp41978vsw7y15qppnm6") (f (quote (("default" "macro" "test")))) (s 2) (e (quote (("test" "dep:tokio") ("macro" "dep:drought_macros"))))))

