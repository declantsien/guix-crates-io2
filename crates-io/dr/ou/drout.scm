(define-module (crates-io dr ou drout) #:use-module (crates-io))

(define-public crate-drout-0.1.0 (c (n "drout") (v "0.1.0") (h "0ilkgd9r4h7nxdczxj566zl9y3w2c5p4y39ah0krz3b9mpi8ap4z") (y #t)))

(define-public crate-drout-0.1.1 (c (n "drout") (v "0.1.1") (h "1cs745k0id01603svgraj7px5a3ll29452ibxnp8zqp2dlbvhihj") (y #t)))

