(define-module (crates-io dr ng drng) #:use-module (crates-io))

(define-public crate-drng-0.1.0 (c (n "drng") (v "0.1.0") (h "1sxfrhypdjr3r7vg6apa04m65w4b9ysj07yh9irwi71c0w7n6k4h")))

(define-public crate-drng-0.1.1 (c (n "drng") (v "0.1.1") (h "1z6c287ks6h2b9p23wgs2535yr6ghv2bi11r7l3cka9lvf0iaqhf")))

(define-public crate-drng-0.1.2 (c (n "drng") (v "0.1.2") (h "0fxni79llwaycv7qxkrij3382fgs2ndyh22301wk1lb5s1sgig81")))

(define-public crate-drng-0.1.3 (c (n "drng") (v "0.1.3") (h "06mc6n446f2g0mrsh133ngb5yjbd9z6pfna2b2y1jr5xd5sc8phy")))

(define-public crate-drng-0.1.4 (c (n "drng") (v "0.1.4") (h "02ljfrsz6k9312g2f243q86v1q1clpqyvm4pjjap2w64zqfw9qnr")))

(define-public crate-drng-0.1.5 (c (n "drng") (v "0.1.5") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zr61an5mwbddymf6ripm2f9cwglp2kv0v2n57q0zjyi4bb5xwvb")))

(define-public crate-drng-0.1.6 (c (n "drng") (v "0.1.6") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "135rh3qqjyx81kdmbyacky9qnz5zng6dyfllcn14q0i8kh9jl6ak")))

(define-public crate-drng-0.2.0 (c (n "drng") (v "0.2.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05jhxd45gm2qjbbmd34iylk9mpk0h2b3cgj5knh4bpb1ac1s871y")))

