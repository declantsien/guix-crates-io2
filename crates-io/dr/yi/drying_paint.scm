(define-module (crates-io dr yi drying_paint) #:use-module (crates-io))

(define-public crate-drying_paint-0.1.0 (c (n "drying_paint") (v "0.1.0") (h "0sxbxcnbaaxbniv5jq0pjwqm9knpldggsyfvli5a21dn424lsxa0")))

(define-public crate-drying_paint-0.1.1 (c (n "drying_paint") (v "0.1.1") (h "1rxyvhhl9fkhrx6v2qb79kf772mbav83zzw642jlzn5gcii9vj17")))

(define-public crate-drying_paint-0.1.2 (c (n "drying_paint") (v "0.1.2") (h "0sz65nrdpw3xp9lrag5z9gvlyjmmx64kp4wfcky0iry1ak7f8pch")))

(define-public crate-drying_paint-0.1.3 (c (n "drying_paint") (v "0.1.3") (h "13n23hc85gciav9j0l2kc5ciy74q8hxqz8mcmnkkqxzzrrlz9cqj")))

(define-public crate-drying_paint-0.1.4 (c (n "drying_paint") (v "0.1.4") (h "0xwh4h1kd3pyvzzqcj3swcc80scqql81hwyln8m1msa0295pbs29")))

(define-public crate-drying_paint-0.2.0 (c (n "drying_paint") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0p0001hyb0wrvbyivq8sz84wx5smi446gmw9z25b87fs1hj7vwxw")))

(define-public crate-drying_paint-0.3.0 (c (n "drying_paint") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "059wxksmync3blla1g9dh2qdjxpxxfhfhnjvbwkh1f487ca0mk6p")))

(define-public crate-drying_paint-0.3.1 (c (n "drying_paint") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ly2kqygl2ki3fhmm0s4v8h5mq1ngb1353wzjjximrfrmbqb7mw1")))

(define-public crate-drying_paint-0.4.0 (c (n "drying_paint") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08yig5lqwq16xrb0wcaba9mbrczafzz9gpbqnq54vqw0zvklz6y1") (y #t)))

(define-public crate-drying_paint-0.4.1 (c (n "drying_paint") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04b5qj9cr9c1zzs75d1v2crmsdw6fxaj41vzyp215dzjzhl78fax") (y #t)))

(define-public crate-drying_paint-0.4.2 (c (n "drying_paint") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ipndd4vkm6h9bgqhmcbacf350k35h7n573v40wh5g4f5p3152sr") (y #t)))

(define-public crate-drying_paint-0.4.3 (c (n "drying_paint") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18n9x6ixizfysphayc6fiipxxwis0ab35lvvp93fr6i17nb7danp") (y #t)))

(define-public crate-drying_paint-0.4.4 (c (n "drying_paint") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09vivyfbjn97byv66mcwnmr6jxg5wk0rm4p3pzc7y9z1dqpm8ndd") (y #t)))

(define-public crate-drying_paint-0.4.5 (c (n "drying_paint") (v "0.4.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09p4rlly3ybrcyll97kyil86x0g4n7srx3j99ndd58kdqvm1fwgf") (y #t)))

(define-public crate-drying_paint-0.4.6 (c (n "drying_paint") (v "0.4.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17w7vwq19cbcs6b58vfpynz1cny10iyf7li3hnjq1pb4sbiq5rs7") (y #t)))

(define-public crate-drying_paint-0.4.7 (c (n "drying_paint") (v "0.4.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0klxc1nxwfi56dk5c8w7d9qf1wnh3lsilp2gvlpzjwgmcdw6qmlf") (y #t)))

(define-public crate-drying_paint-0.5.0 (c (n "drying_paint") (v "0.5.0") (h "1x10j8f2mrgq6vlwd1ax25zxw0qcyr38n94vlymyrz1vh5xnx5yz") (f (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5.1 (c (n "drying_paint") (v "0.5.1") (h "14cjhz3rbwkxgi9c0dk2ydsa80ksv83jwidv0cp5bm13mx6k75z8") (f (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5.2 (c (n "drying_paint") (v "0.5.2") (h "0kjbimcgwzs2b8s7g14hiq3xpmddd2j0wzk4jfj07xiw0gnnccnb") (f (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5.3 (c (n "drying_paint") (v "0.5.3") (h "1n3cnr8522bkwvnvsrvm48annznsafmp1fmifjjym08rxdz723l1") (f (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5.4 (c (n "drying_paint") (v "0.5.4") (h "1qfgj8is4fpl2399sr01znp3v5caaj3aq26nmsbq0fa63vi6fnxl") (f (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5.5 (c (n "drying_paint") (v "0.5.5") (h "1x352705vr45q04i26mhvx37lfz0p5jxbiv5srvawynrqi0xisyl") (f (quote (("std") ("default" "std"))))))

