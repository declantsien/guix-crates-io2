(define-module (crates-io dr os drossel-journal) #:use-module (crates-io))

(define-public crate-drossel-journal-0.1.0 (c (n "drossel-journal") (v "0.1.0") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)) (d (n "leveldb") (r "^0.2.0") (d #t) (k 0)))) (h "015nv5mlk9dybnizibyncnw54l1z9vk506c579nc3096ahm9c9v0")))

(define-public crate-drossel-journal-0.1.1 (c (n "drossel-journal") (v "0.1.1") (d (list (d (n "db-key") (r "^0.0.1") (d #t) (k 0)) (d (n "leveldb") (r "^0.2.0") (d #t) (k 0)))) (h "1psp6hjh3gn8lzk0gf9xcyyihmqwr5bl3nnyfhar1xdwniymlx46")))

(define-public crate-drossel-journal-0.1.2 (c (n "drossel-journal") (v "0.1.2") (d (list (d (n "db-key") (r "*") (d #t) (k 0)) (d (n "leveldb") (r "^0.6.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "0hpd3d0d190vzxpvrcgrdmh95fihyk7apkarnsv1418a7i69sqhf")))

