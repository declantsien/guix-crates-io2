(define-module (crates-io dr os drossel) #:use-module (crates-io))

(define-public crate-drossel-0.1.0 (c (n "drossel") (v "0.1.0") (d (list (d (n "drossel-journal") (r "^0.1.1") (d #t) (k 0)) (d (n "leveldb") (r "^0.2.0") (d #t) (k 0)) (d (n "strand") (r "^0.1.0") (d #t) (k 0)))) (h "12053fsdq240z17ll136kg9w4zfppb9qydbngszc1fyac6cxd8k3")))

