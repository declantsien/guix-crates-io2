(define-module (crates-io dr un drunken-diver) #:use-module (crates-io))

(define-public crate-drunken-diver-0.1.0 (c (n "drunken-diver") (v "0.1.0") (d (list (d (n "sha2") (r "^0.9.5") (d #t) (k 2)))) (h "0vsvzmh1hg7fpm5qiny7pa4086kv8j4ii85izzll0n6jbixmlisk") (y #t)))

(define-public crate-drunken-diver-0.1.1 (c (n "drunken-diver") (v "0.1.1") (d (list (d (n "sha2") (r "^0.9.5") (d #t) (k 2)))) (h "0pgm5whkzj1cz2gnxjl7k81y9bpqkyrgapi1xwswli1j9rxw7pv4")))

