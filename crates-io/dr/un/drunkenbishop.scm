(define-module (crates-io dr un drunkenbishop) #:use-module (crates-io))

(define-public crate-drunkenbishop-0.1.0 (c (n "drunkenbishop") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1i6sjd9ndbm0s12cm7l3ajrsmc88m29n5rqxyjyb665s7mca66zi") (f (quote (("default")))) (s 2) (e (quote (("hexparse" "dep:hex") ("hash" "dep:sha2"))))))

(define-public crate-drunkenbishop-0.1.1 (c (n "drunkenbishop") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1mckqcxlybg9fj2d5jj9vxfbih564kh7lgixgmxy5yd61f6q8vpg") (f (quote (("default")))) (s 2) (e (quote (("hexparse" "dep:hex") ("hash" "dep:sha2"))))))

