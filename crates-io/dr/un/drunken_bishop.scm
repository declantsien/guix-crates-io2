(define-module (crates-io dr un drunken_bishop) #:use-module (crates-io))

(define-public crate-drunken_bishop-0.1.0 (c (n "drunken_bishop") (v "0.1.0") (h "0fbi24pi2hkw8f9bi0in3hyi73bs478i8x147ryph365cdfmqzjf")))

(define-public crate-drunken_bishop-0.1.1 (c (n "drunken_bishop") (v "0.1.1") (h "1qdlk7ig0rqki825mf4yq01dzwi5c6258cldllkx2my5xplgqmh6")))

(define-public crate-drunken_bishop-0.1.2 (c (n "drunken_bishop") (v "0.1.2") (h "0iwbpl33mngjp9ffwpyn8r6pqdabsx0022d1fvg5d6sdjang1ysn")))

