(define-module (crates-io dr un drunx) #:use-module (crates-io))

(define-public crate-drunx-1.7.0 (c (n "drunx") (v "1.7.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0w43b179rva1l3ijmwaklm9ncanv9whhkkjyigb6z75h3jbgqf05")))

(define-public crate-drunx-1.7.1 (c (n "drunx") (v "1.7.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "1lipd2ipqjsn9i71shb5bi41w292dy31wbhvhfzysgs6q190gj7v")))

(define-public crate-drunx-1.7.2 (c (n "drunx") (v "1.7.2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0b7z96z5w93izd44lij40z1raw9wbgmjk47bjhzqj07b16z9xggn")))

