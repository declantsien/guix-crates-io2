(define-module (crates-io dr um drumatech) #:use-module (crates-io))

(define-public crate-drumatech-0.1.0 (c (n "drumatech") (v "0.1.0") (h "0592f5343mqwqbnsc3jdfkfrff8w59v8v4b90v7ql68wcfd0kwsl")))

(define-public crate-drumatech-0.1.1 (c (n "drumatech") (v "0.1.1") (h "0ys8v8rq8wldr6yfry8vdgn3assjpik5n5nxpwb2rxqxg2xgq69a")))

(define-public crate-drumatech-0.1.2 (c (n "drumatech") (v "0.1.2") (h "10i2a5ydhmbnb9x3yjmx7dl53mfq903m2imhqfmglb8pnx7zy3kr")))

(define-public crate-drumatech-0.1.3 (c (n "drumatech") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1lbqrv5v6bg0vzjnsi2ny4fl3m40crg091dx1dcwrpvxipg9c5wg")))

(define-public crate-drumatech-0.1.4 (c (n "drumatech") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "13xszh5j0a5z7p89f9mg97pgvxiwgnzhhb4991msgz8avhjncky7")))

(define-public crate-drumatech-0.1.5 (c (n "drumatech") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "01dc7lhl5gm9qgysqhw4y5496lp0l9pk27gw7pq8qxyrd9cm0c7j")))

(define-public crate-drumatech-0.1.7 (c (n "drumatech") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "11v5kc4pmk22v1qqq7pvgszpyjqa5qf7j205mcbiq2d0fp8x4clw")))

(define-public crate-drumatech-0.1.8 (c (n "drumatech") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1mja5mr3z0l371fl2q8hq7pc9xbazl3mfyk4i1djvzgwlqapzj1s")))

(define-public crate-drumatech-0.1.9 (c (n "drumatech") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1wc5d7b852rhgvq5q6h2337y64l1wnfzqyy8dl5xa5vh4wgyfky3")))

