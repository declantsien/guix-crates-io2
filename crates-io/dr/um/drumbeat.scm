(define-module (crates-io dr um drumbeat) #:use-module (crates-io))

(define-public crate-drumbeat-0.0.1 (c (n "drumbeat") (v "0.0.1") (h "1s0xi8rg978szzwhk91qlvi0c4qq9nqiga50qqsp8a4lb5y90fha")))

(define-public crate-drumbeat-0.0.2 (c (n "drumbeat") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "08zf7dbvfdw0izgwcw1q6xf2jg4asvw218j0i753h7yvxfiphcnr")))

(define-public crate-drumbeat-0.1.0 (c (n "drumbeat") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1iab9bnaigvwjc4djcjbs4vav9bng7jny2ikk7xwggnz1s8yx6r5")))

(define-public crate-drumbeat-0.1.1 (c (n "drumbeat") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0r56kk0gs69axa4qli0il636i2f0dvb34rvjadd10gswn4ivsgfl")))

