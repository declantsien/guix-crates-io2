(define-module (crates-io dr um drum) #:use-module (crates-io))

(define-public crate-drum-0.1.0 (c (n "drum") (v "0.1.0") (d (list (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1id6nrmfzbjrhcyp330kv5cip43ajqwl2wpl5d952qfv9ma1pa4h")))

(define-public crate-drum-0.2.0 (c (n "drum") (v "0.2.0") (d (list (d (n "bincode") (r "0.4.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "00ql83v31qc4f9mz7gfn17xxi51dq9g0lhj67x7afw0740wr0dnv")))

(define-public crate-drum-0.2.1 (c (n "drum") (v "0.2.1") (d (list (d (n "bincode") (r "0.4.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "tempfile") (r "1.1.*") (d #t) (k 2)))) (h "0xbj4ir6s3378qg0gywbssl2h1xmwn4bcq74rjqhhqfwndig2gab")))

(define-public crate-drum-0.4.2 (c (n "drum") (v "0.4.2") (d (list (d (n "bincode") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "0.6.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.6.*") (d #t) (k 0)) (d (n "tempfile") (r "2.0.*") (d #t) (k 2)))) (h "1d7nm0mclsycif1m1m9g9cs6kh9fj4yccr02d52sm3l0w5g5ssvf")))

(define-public crate-drum-0.4.3 (c (n "drum") (v "0.4.3") (d (list (d (n "bincode") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "0.6.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.6.*") (d #t) (k 0)) (d (n "tempfile") (r "2.0.*") (d #t) (k 2)))) (h "0z0llgvswszjhjyq2xi896rg14f4517219xnwvm3ffhbab597gwk")))

(define-public crate-drum-0.4.4 (c (n "drum") (v "0.4.4") (d (list (d (n "bincode") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "0.6.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.6.*") (d #t) (k 0)) (d (n "tempfile") (r "2.0.*") (d #t) (k 2)))) (h "0gj8wzfiy301lbc917bdm90mx4qfhh4drxcihmwxi758lmasy8sk")))

