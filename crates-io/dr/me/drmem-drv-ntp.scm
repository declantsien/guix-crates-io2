(define-module (crates-io dr me drmem-drv-ntp) #:use-module (crates-io))

(define-public crate-drmem-drv-ntp-0.1.0 (c (n "drmem-drv-ntp") (v "0.1.0") (d (list (d (n "drmem-api") (r "^0.1") (d #t) (k 0)) (d (n "drmem-config") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "net" "io-util"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "19681qfr3mcnnwa83467mybmn8fwvrj69jlmj0b9x29pwnnmwp86")))

(define-public crate-drmem-drv-ntp-0.2.0 (c (n "drmem-drv-ntp") (v "0.2.0") (d (list (d (n "drmem-api") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time" "sync" "net" "io-util"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "139pj72l9q8vsya4qs529c235xmqnz6j58hsg2xq645v5jl252hc")))

(define-public crate-drmem-drv-ntp-0.3.0 (c (n "drmem-drv-ntp") (v "0.3.0") (d (list (d (n "drmem-api") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "time" "sync" "io-util" "net" "io-util"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1d1ly2b58fi5y07m86h9d7paa2ji7546bn3h8pj4pjlaazv3ibj8")))

