(define-module (crates-io dr me drmem-config) #:use-module (crates-io))

(define-public crate-drmem-config-0.1.0 (c (n "drmem-config") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "drmem-api") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "time" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1k8lypp2asjyv6knbrqh5ckwpbl6k5izmgmlzmk6h3i5vn9wc6hn") (f (quote (("redis-backend"))))))

