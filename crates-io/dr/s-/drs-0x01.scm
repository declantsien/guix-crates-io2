(define-module (crates-io dr s- drs-0x01) #:use-module (crates-io))

(define-public crate-drs-0x01-0.1.0 (c (n "drs-0x01") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)))) (h "0n4lfad00rxjpjp6mnvzmb5gm7bpwl7gfz9y5a921xg74gzas1ss")))

(define-public crate-drs-0x01-0.1.1 (c (n "drs-0x01") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)))) (h "1hqmziab4pykld407q77ky57jspd8ad9xljq3p04b2fdas1m9yn3")))

(define-public crate-drs-0x01-0.1.2 (c (n "drs-0x01") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)))) (h "0bvqzrkg8bg0m1zfd14fyvzhb643qhajcff2lh36hffwg01if2hf")))

(define-public crate-drs-0x01-0.1.3 (c (n "drs-0x01") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)))) (h "0vpd1xgxjc3i7z38s0j7k56imfgbyqdk9rd3cqanvkwab6w8yz5s")))

(define-public crate-drs-0x01-0.1.4 (c (n "drs-0x01") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)))) (h "1j4x0swdwkfdzc6wqr5ass4wz76bnpgcpmlmajac7qw4pfyk68b6")))

(define-public crate-drs-0x01-0.1.5 (c (n "drs-0x01") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)))) (h "12v3yabld9wawvv9y4kvd6bkq1kjffrzp5gpdgwvd3gc3b0fs8wd")))

(define-public crate-drs-0x01-0.1.6 (c (n "drs-0x01") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)))) (h "0kvjn1fm3qz2aidblmxyrx065sm3rac7mfxzw71ds2sadmjzrsih")))

(define-public crate-drs-0x01-0.1.7 (c (n "drs-0x01") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)))) (h "0y5iplmlqnh7fi0y3av49bw9skn6vl5kq3xf7px82p3kvm90pcjq")))

(define-public crate-drs-0x01-0.2.0 (c (n "drs-0x01") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "00dkjip6j8hslf01w9wazdg97dsdq2cjjschrc2mp4hqxaigbknw")))

(define-public crate-drs-0x01-0.2.1 (c (n "drs-0x01") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)))) (h "08jcjwx1laas1x81p4kg97xpb65v94jixpyfcfwxdmqbm4kwd24m")))

(define-public crate-drs-0x01-0.2.2 (c (n "drs-0x01") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "try_from") (r "^0.3.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1bgllaz7gfx6d35rl7063vv37imnrpxrbxr0rfgxyjyj2rp50cv7")))

(define-public crate-drs-0x01-0.3.0 (c (n "drs-0x01") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.7") (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "try_from") (r "^0.3.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1908prkzqrlddz9shhx82fllbss6rfjjqflhpw0yllwpzddr1kcz")))

