(define-module (crates-io dr ol droll-cli) #:use-module (crates-io))

(define-public crate-droll-cli-0.1.0 (c (n "droll-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "droll") (r "^0.1.0") (d #t) (k 0)))) (h "19f3pqg9702gxc62a0g5v6ciz3xd58km7zsyihznf1j334rfiprs")))

