(define-module (crates-io dr ep drep) #:use-module (crates-io))

(define-public crate-drep-0.1.2 (c (n "drep") (v "0.1.2") (d (list (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "07xqcpg6jq4qdpn3bn58zbwq0c26ir8hhpyqxkv798jbwqy0c27p")))

(define-public crate-drep-0.1.3 (c (n "drep") (v "0.1.3") (d (list (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "000a0sgdlincq0fidfgf8n0qvm7f7qwsmh8mqkbs3wmrlh3q7nbc")))

