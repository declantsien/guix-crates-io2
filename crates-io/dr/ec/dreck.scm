(define-module (crates-io dr ec dreck) #:use-module (crates-io))

(define-public crate-dreck-0.1.0 (c (n "dreck") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1s5ml7kh262wihzypzkcld460pajj4vfqhmddlsc17v3k887rk9j")))

(define-public crate-dreck-0.1.1 (c (n "dreck") (v "0.1.1") (d (list (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "04lxycw3gjvbcr9c1im140zrv1cp7425km4szbkh0f8bx7va3xg8")))

