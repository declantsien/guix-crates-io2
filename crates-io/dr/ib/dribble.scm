(define-module (crates-io dr ib dribble) #:use-module (crates-io))

(define-public crate-dribble-0.1.0 (c (n "dribble") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0b502328v26h1kvq5dcqbdqpg4zpq3nfb7119hiy9j7gmji4pgs4") (f (quote (("unstable"))))))

(define-public crate-dribble-1.0.0 (c (n "dribble") (v "1.0.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "09kamynihi3i8n74pfmlbla059apb7asy36s7jamcxiawpgjqac8") (f (quote (("unstable"))))))

