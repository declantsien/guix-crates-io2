(define-module (crates-io dr sh drsh4dow_testing_mini_crate_that_do_nothing) #:use-module (crates-io))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1.0 (c (n "drsh4dow_testing_mini_crate_that_do_nothing") (v "0.1.0") (h "1i1clwlcf9agir90096qrhvxk9av8cd0z4x82ar0r7ky5km6ipga")))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1.1 (c (n "drsh4dow_testing_mini_crate_that_do_nothing") (v "0.1.1") (h "0x26zia9mfsdxjly240rknhpyim767ihnnjxn6zcs9vaqb9jzx7f")))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1.2 (c (n "drsh4dow_testing_mini_crate_that_do_nothing") (v "0.1.2") (h "1xjf30nhdig2g6nsx9wrhydnn58xv1fv01yd7cn95gr592bkbhm2")))

