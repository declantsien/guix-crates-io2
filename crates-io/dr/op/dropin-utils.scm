(define-module (crates-io dr op dropin-utils) #:use-module (crates-io))

(define-public crate-dropin-utils-0.5.3-1 (c (n "dropin-utils") (v "0.5.3-1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "1g7zhmgn4q9i98lw4g44lnqln45vx7s5c4nwc6sw3m37vwjlp9xh") (y #t)))

(define-public crate-dropin-utils-0.5.3-2 (c (n "dropin-utils") (v "0.5.3-2") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "0mbw3108q1hzil35da0z2lpb6wb96pynwsv8bif01hlrcn9380zf") (y #t)))

(define-public crate-dropin-utils-0.5.3-4 (c (n "dropin-utils") (v "0.5.3-4") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "0gdlr0r20clj122iv9ly3zd3x056y1255vqpwlykay8lvslpr45w") (y #t)))

