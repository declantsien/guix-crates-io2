(define-module (crates-io dr op drop_bomb) #:use-module (crates-io))

(define-public crate-drop_bomb-0.1.0 (c (n "drop_bomb") (v "0.1.0") (h "1vrwb6mi66hjsk7whaygckyv8rbm5vkf1fmcdfpspivv234vbpyr")))

(define-public crate-drop_bomb-0.1.1 (c (n "drop_bomb") (v "0.1.1") (h "022irnpmg3b844wg68nw2lhr32s6awadssgr0g82y9g7m48zjdic")))

(define-public crate-drop_bomb-0.1.2 (c (n "drop_bomb") (v "0.1.2") (h "0bvyz15v69pv61p262nk9fmidzvza2ggm3srps4xgxsjcz0c75y7")))

(define-public crate-drop_bomb-0.1.3 (c (n "drop_bomb") (v "0.1.3") (h "1rs8nfh9wa038x9f3fk0bvl689dr9fylk654ric9v8xndzjyjbn7")))

(define-public crate-drop_bomb-0.1.4 (c (n "drop_bomb") (v "0.1.4") (h "17qr9vnhy4izg492bm5fa74786b69vlr94kh1r9ri46jbx3nxck9")))

(define-public crate-drop_bomb-0.1.5 (c (n "drop_bomb") (v "0.1.5") (h "1qc59a53ngwxpnbvl8xidp2cmwrl671dhbzw7zijmjjaq0hqxnlv")))

(define-public crate-drop_bomb-0.1.6 (c (n "drop_bomb") (v "0.1.6") (h "1mqqz9fyn35j7zplnajskybrc03cxqj78q8j2867vypqcy3drzzx") (y #t)))

