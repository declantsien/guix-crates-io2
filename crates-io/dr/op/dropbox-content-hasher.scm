(define-module (crates-io dr op dropbox-content-hasher) #:use-module (crates-io))

(define-public crate-dropbox-content-hasher-0.2.0 (c (n "dropbox-content-hasher") (v "0.2.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1mymdw5pzdl21v129y8hfja6kza33wn2szwk0liyf3k6dnf42w4i")))

(define-public crate-dropbox-content-hasher-0.3.0 (c (n "dropbox-content-hasher") (v "0.3.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1w4b300qdnyn6j61pb1gil76wif6n5hsd0rjn9l4lzq7xnrqlwld")))

