(define-module (crates-io dr op drop-tracker) #:use-module (crates-io))

(define-public crate-drop-tracker-0.1.0 (c (n "drop-tracker") (v "0.1.0") (h "1br4s36qbcvlqglmkf53cnd4cxcd450pm2pbfy1mwd951agwkslc")))

(define-public crate-drop-tracker-0.1.1 (c (n "drop-tracker") (v "0.1.1") (h "1sk4wi4dnybbkh3mb3na4z7b9vq00nwz3k3q45m8p62zp5zsap8z")))

(define-public crate-drop-tracker-0.1.2 (c (n "drop-tracker") (v "0.1.2") (h "011dxw8bbik2fpbmqlr03y598pqqy16wlhafr92b0xrqhqyysrx2")))

(define-public crate-drop-tracker-0.1.3 (c (n "drop-tracker") (v "0.1.3") (h "02lhlsbxqqizg4h8rv496vxb0pi99cyd67v9z5dzp71srs4rridh")))

