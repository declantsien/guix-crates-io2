(define-module (crates-io dr op drop_ok) #:use-module (crates-io))

(define-public crate-drop_ok-1.0.0 (c (n "drop_ok") (v "1.0.0") (h "19lab4cd0bbqhawyg5y1fnq8hh4w4zlci1xhjzyxd78qlsmfb5hx")))

(define-public crate-drop_ok-1.0.1 (c (n "drop_ok") (v "1.0.1") (h "12g8jnm2m0m7mv97da5ap79xdxzwdp7bzcjna5i35k696ly6g4p8")))

(define-public crate-drop_ok-1.0.2 (c (n "drop_ok") (v "1.0.2") (h "17yh26i6zb9bipbzm6zz2kk8gj9bwvm2vj6jzkq4alck1hmxbc2a")))

