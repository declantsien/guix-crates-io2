(define-module (crates-io dr op drop-dir) #:use-module (crates-io))

(define-public crate-drop-dir-0.0.0 (c (n "drop-dir") (v "0.0.0") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0ipsiqsjiidn4b0q51jc3km5b9i3srzcblk4kb0w7ra5y33yfkza")))

(define-public crate-drop-dir-0.1.0 (c (n "drop-dir") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1j9cvg79jwpjhiffg9xbkdrmpcwk0njrp4xif16883lj0flj55sw")))

