(define-module (crates-io dr op drop_arena) #:use-module (crates-io))

(define-public crate-drop_arena-0.1.0 (c (n "drop_arena") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "0xj5jp3ldhn0z4bnklay4y107cjb4ir1si7p3jm6x3flazdwp670") (f (quote (("std") ("default" "std"))))))

(define-public crate-drop_arena-0.2.0 (c (n "drop_arena") (v "0.2.0") (d (list (d (n "consume_on_drop") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1zg3fhb4b1zllfc760aqqlp94n17whgzgb1hw0s0npvpxyjabywi") (f (quote (("std") ("default" "std"))))))

