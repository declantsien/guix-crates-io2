(define-module (crates-io dr op dropbear) #:use-module (crates-io))

(define-public crate-dropbear-0.1.0 (c (n "dropbear") (v "0.1.0") (d (list (d (n "futures") (r "~0.3.12") (d #t) (k 0)) (d (n "tokio") (r ">=0.2.24") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r ">=0.2.24") (f (quote ("sync" "macros" "rt"))) (d #t) (k 2)))) (h "0ckhki2ll6raymwy6vr7741gb8irzbq0y6p87wbj4cb5x47xr5ic")))

