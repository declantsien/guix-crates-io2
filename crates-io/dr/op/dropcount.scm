(define-module (crates-io dr op dropcount) #:use-module (crates-io))

(define-public crate-dropcount-0.1.0 (c (n "dropcount") (v "0.1.0") (h "09mf0zmy33gb4azlaxz93namj9h2pwrihv5gwgqmqcsl4pmbdpdc")))

(define-public crate-dropcount-0.1.1 (c (n "dropcount") (v "0.1.1") (h "1glg8cdyqracsj549nxj676pgf6skbzvg1y4imkar6qpkvvpr2b8")))

(define-public crate-dropcount-0.1.2 (c (n "dropcount") (v "0.1.2") (h "03kl4x176aniqvzsh6cywqfklmxs6di5f9nrjrrfkgy0jw3vh7h0")))

