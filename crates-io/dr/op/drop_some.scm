(define-module (crates-io dr op drop_some) #:use-module (crates-io))

(define-public crate-drop_some-1.0.0 (c (n "drop_some") (v "1.0.0") (h "0qw7kg9i5pl3ffjr5ws7yaqybbbv2czvg74zz95pxqx8slkqxaim")))

(define-public crate-drop_some-1.0.1 (c (n "drop_some") (v "1.0.1") (h "0k97fyn2vnkdzjvc40nhvpp1q16hdy7vpwchg84cxir48m51ip80")))

