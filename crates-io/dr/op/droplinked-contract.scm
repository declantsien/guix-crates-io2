(define-module (crates-io dr op droplinked-contract) #:use-module (crates-io))

(define-public crate-droplinked-contract-0.1.0 (c (n "droplinked-contract") (v "0.1.0") (d (list (d (n "base16") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^2.0.0") (d #t) (k 0)) (d (n "casper-types") (r "^2.0.0") (d #t) (k 0)) (d (n "ed25519") (r "^2.0.1") (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (k 0)))) (h "0w2f830pihhx2yqd5a2xb573c1pj7mz18bnd59hhnsf5gbaqkdsi")))

