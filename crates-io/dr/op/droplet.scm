(define-module (crates-io dr op droplet) #:use-module (crates-io))

(define-public crate-droplet-0.1.14 (c (n "droplet") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0hnx12vmslp4rr8ggl5z7cb57vcwran74nmpm346ymmdprr7ncr6")))

