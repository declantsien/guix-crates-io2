(define-module (crates-io dr op drop_guard) #:use-module (crates-io))

(define-public crate-drop_guard-0.0.0 (c (n "drop_guard") (v "0.0.0") (h "06hhdxqnfv0wiwhwcb9bcgn1r1p2k966hcg0sxwlagk01xjn60s6")))

(define-public crate-drop_guard-0.0.1 (c (n "drop_guard") (v "0.0.1") (d (list (d (n "threadpool") (r "^1.4") (d #t) (k 2)))) (h "04nq2m7hab2sxl72yr7awh66d1n9m9j7bmwqsa14i5cjx57w9glm")))

(define-public crate-drop_guard-0.0.2 (c (n "drop_guard") (v "0.0.2") (d (list (d (n "threadpool") (r "^1.4") (d #t) (k 2)))) (h "04l1x1vr5izwr53iw0faiydawzxwfrmar8aqh339bphmvvkvycq0")))

(define-public crate-drop_guard-0.1.0 (c (n "drop_guard") (v "0.1.0") (d (list (d (n "threadpool") (r "^1.4") (d #t) (k 2)))) (h "0vxmgwawpw6c9np91i10j3bnhr60iixkq6pwk5dcg9im7mynrszr")))

(define-public crate-drop_guard-0.2.0 (c (n "drop_guard") (v "0.2.0") (d (list (d (n "threadpool") (r "^1.4") (d #t) (k 2)))) (h "0z1xa53y0ayl4k0k141q5r5q3kb3vd55vr8ihjkvfl0dfh2mr6a2")))

(define-public crate-drop_guard-0.2.1 (c (n "drop_guard") (v "0.2.1") (d (list (d (n "threadpool") (r "^1.4") (d #t) (k 2)))) (h "1qlqj3mpvsn8a9rnyjx9gi4jax66j1p371nd0k2bgg4d8xrmvfvq")))

(define-public crate-drop_guard-0.3.0 (c (n "drop_guard") (v "0.3.0") (d (list (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "02fjcs86vr7ps28mgfqnflvpba271jmrldgdk9j6wgv8idyq2jic")))

