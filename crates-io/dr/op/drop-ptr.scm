(define-module (crates-io dr op drop-ptr) #:use-module (crates-io))

(define-public crate-drop-ptr-0.0.0 (c (n "drop-ptr") (v "0.0.0") (h "0gkh6z7cwvrc3s7lggjc4xn7ac8gccpx64cr9hlinds4wbn4mk77")))

(define-public crate-drop-ptr-0.1.0 (c (n "drop-ptr") (v "0.1.0") (h "039bzyc8j0j5x3i4b03piqw8yx90zjhvz0g3492qz1djzby846cz") (y #t)))

(define-public crate-drop-ptr-0.1.1 (c (n "drop-ptr") (v "0.1.1") (h "1vhhjg90jba41apk3lzc40yp6d2b7s45aa4wqf61pm0wnjhb9r1i")))

(define-public crate-drop-ptr-0.1.2 (c (n "drop-ptr") (v "0.1.2") (h "078y95rllhihijkmsgn7wxgqbq41xglbvn57axj8qj5wc7imlxii")))

