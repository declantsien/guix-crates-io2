(define-module (crates-io dr op dropin-bootstrap) #:use-module (crates-io))

(define-public crate-dropin-bootstrap-0.5.3-2 (c (n "dropin-bootstrap") (v "0.5.3-2") (d (list (d (n "dropin-utils") (r "^0.5.3-1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.11.0") (d #t) (k 0)))) (h "1pi4b3sb92dzy05i59bsc28rwl4piy15nq4drgfbcr480q3n0riy") (y #t)))

(define-public crate-dropin-bootstrap-0.5.3-4 (c (n "dropin-bootstrap") (v "0.5.3-4") (d (list (d (n "dropin-core") (r "^0.5.3-4") (d #t) (k 0)) (d (n "wasi") (r "^0.11.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.11.0") (d #t) (k 0)))) (h "1liggs0n2ivlvbn7rjsjgkmnijj67793z6vmlmxkmdym715f0217") (y #t)))

