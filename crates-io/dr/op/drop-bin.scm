(define-module (crates-io dr op drop-bin) #:use-module (crates-io))

(define-public crate-drop-bin-0.1.0 (c (n "drop-bin") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defer-drop") (r "^1.0.1") (d #t) (k 2)) (d (n "try-mutex") (r "^0.2.0") (d #t) (k 0)) (d (n "try-rwlock") (r "^0.1.0") (d #t) (k 0)))) (h "187157219771xy9jw013afbvlypmksx62dh3cs4k58snhx245fd3") (y #t)))

(define-public crate-drop-bin-0.1.1 (c (n "drop-bin") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defer-drop") (r "^1.0.1") (d #t) (k 2)) (d (n "try-mutex") (r "^0.2.0") (d #t) (k 0)) (d (n "try-rwlock") (r "^0.1.0") (d #t) (k 0)))) (h "0h53lr4sg2gsc8xr6zsqz3xc3v2lq6snlv25s0b300gbrnmxdc1k") (y #t)))

(define-public crate-drop-bin-0.2.0 (c (n "drop-bin") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defer-drop") (r "^1.0.1") (d #t) (k 2)) (d (n "try-mutex") (r "^0.2.0") (d #t) (k 0)) (d (n "try-rwlock") (r "^0.1.0") (d #t) (k 0)))) (h "07fgxjhv9xmf1ig7yr38rnlaap63dbncwj6ql37wc7qmv0ji74ny")))

(define-public crate-drop-bin-0.2.1 (c (n "drop-bin") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defer-drop") (r "^1.0.1") (d #t) (k 2)) (d (n "try-mutex") (r "^0.3.0") (d #t) (k 0)) (d (n "try-rwlock") (r "^0.1.0") (d #t) (k 0)))) (h "0ivpl3lp3w2xlrbppd8v43zjc79ssynib2w11ylfmcn9ivkvr811")))

(define-public crate-drop-bin-0.2.2 (c (n "drop-bin") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "defer-drop") (r "^1.0.1") (d #t) (k 2)) (d (n "try-mutex") (r "^0.3.0") (d #t) (k 0)) (d (n "try-rwlock") (r "^0.1.0") (d #t) (k 0)))) (h "0nnq421hvfa31qd6f2s0b23zia6pcnrxd3g9s9h75dsh6kkzkk7w")))

