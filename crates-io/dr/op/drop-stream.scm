(define-module (crates-io dr op drop-stream) #:use-module (crates-io))

(define-public crate-drop-stream-0.1.0 (c (n "drop-stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "09i5ngysdiva223c8v856ivy7znhs98y3d9ng49snwffg3ibprbk")))

(define-public crate-drop-stream-0.2.0 (c (n "drop-stream") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)))) (h "14kan12xfw03pqii0s04nizpsl31pj2k9131106547ibkk0dz3hr")))

(define-public crate-drop-stream-0.3.0 (c (n "drop-stream") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)))) (h "054cqxi983pd1m29cn2nr4gvj3j6yjmm5j7ak5j8jgq3l2b9w7w9")))

(define-public crate-drop-stream-0.3.1 (c (n "drop-stream") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)))) (h "1988h9a2zzn61zbhwcrc3bhdgi35is2qzzh6qqd6xj5ns4vhmpkm")))

