(define-module (crates-io dr op drop-root) #:use-module (crates-io))

(define-public crate-drop-root-0.1.0 (c (n "drop-root") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nn3qn0v09xqhkjjsfivy36v8xn7hydf4par73dmnilv8zahzjrz")))

(define-public crate-drop-root-0.1.1 (c (n "drop-root") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1r6bvsij034d01sjxdvbh7fp27s3yq8zsyisqhz8c17z8ya3qnlz")))

