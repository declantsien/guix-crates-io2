(define-module (crates-io dr op dropbox-dir) #:use-module (crates-io))

(define-public crate-dropbox-dir-1.0.0 (c (n "dropbox-dir") (v "1.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17dxkildc9xb8ay35g0igabknlds2a83p0d1nz2swkidzpvi7xb9")))

