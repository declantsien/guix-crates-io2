(define-module (crates-io dr op dropcheck) #:use-module (crates-io))

(define-public crate-dropcheck-0.1.0 (c (n "dropcheck") (v "0.1.0") (h "0330i6ya7f0qdfwp5rglpcr3j27i9y16r41yi1nyqibkmk9mq833")))

(define-public crate-dropcheck-0.1.1 (c (n "dropcheck") (v "0.1.1") (h "15fyqv4zh0gfapr7hhvz5j12x76vrzgnbxvxp634kid6llzrl658")))

