(define-module (crates-io dr op dropzone) #:use-module (crates-io))

(define-public crate-dropzone-0.0.1 (c (n "dropzone") (v "0.0.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "1kfcbx3ss595irvkndfjml4mqpmcgnr1zrcbsqx069smpwqm10ba")))

(define-public crate-dropzone-0.1.0 (c (n "dropzone") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "redis") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1ak22v6bpx0frq3zajpfh25sska6l530kp38g41c54p00xcfydg7")))

(define-public crate-dropzone-0.1.1 (c (n "dropzone") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "redis") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0ccd654lg1iq8q17hc8wwq0a63fb5l1yhxlqp6q4yp9qz9xhifa7")))

(define-public crate-dropzone-0.1.2 (c (n "dropzone") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "redis") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "113l1x356w8p3jfl27vhn1v49ip7gck15q5iilbajn4d1inl43dq")))

(define-public crate-dropzone-0.1.3 (c (n "dropzone") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "redis") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0nzh2jw372v8hd5llay9grryxmfalr4h6cacjyl8zw8m47r5yn6x")))

