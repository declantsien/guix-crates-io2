(define-module (crates-io dr op drop_tracer) #:use-module (crates-io))

(define-public crate-drop_tracer-0.1.0 (c (n "drop_tracer") (v "0.1.0") (d (list (d (n "test_panic") (r "^0.3") (d #t) (k 2)))) (h "0ndr2yad1kq3qpkbgp67857236pczii3mrvm6fv7d9x60gw3ldxg")))

(define-public crate-drop_tracer-0.1.1 (c (n "drop_tracer") (v "0.1.1") (d (list (d (n "test_panic") (r "^0.3") (d #t) (k 2)))) (h "00aq6r9h512mrmwivbf5md0z3gjqd7zlg9ypa4lsp9rrbz0b5bjs")))

