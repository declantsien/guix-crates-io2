(define-module (crates-io dr op drop-abort) #:use-module (crates-io))

(define-public crate-drop-abort-0.1.0 (c (n "drop-abort") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("executor"))) (k 2)) (d (n "futures-util") (r "^0.3.5") (k 0)))) (h "0dflr7j0ddvpg1iahjrx52zwgqly2fmpp88lx13pn726i31nhrcf")))

