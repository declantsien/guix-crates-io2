(define-module (crates-io dr op drop-take-derive) #:use-module (crates-io))

(define-public crate-drop-take-derive-0.1.0 (c (n "drop-take-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fzq1p8pwmdvyp6zy6ymx9zkd3cmxcf2205p69m9wp4aw0sg0wak") (y #t)))

