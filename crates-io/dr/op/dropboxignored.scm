(define-module (crates-io dr op dropboxignored) #:use-module (crates-io))

(define-public crate-dropboxignored-0.1.0 (c (n "dropboxignored") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (d #t) (k 0)))) (h "10gz8r7dxz4j9l913yc2f0xq9kssycn7sqxmi5jp7ab9iav70lj8")))

