(define-module (crates-io dr op dropin-pm) #:use-module (crates-io))

(define-public crate-dropin-pm-0.5.3-7 (c (n "dropin-pm") (v "0.5.3-7") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "dropin-utils") (r "^0.5.3-4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1a3p79fdsj2mlh3kzhd7fd6jqzlz78jdppgl8qv8njnvhzshnqhs") (y #t)))

