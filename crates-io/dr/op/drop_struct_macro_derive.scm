(define-module (crates-io dr op drop_struct_macro_derive) #:use-module (crates-io))

(define-public crate-drop_struct_macro_derive-0.1.0 (c (n "drop_struct_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10iwzbrdzrj8fw855qipclvbd23bwnh77l3cpyxcf1l0ajahvgd6")))

(define-public crate-drop_struct_macro_derive-0.1.1 (c (n "drop_struct_macro_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z4q0cz8jx9rls614birrjh44ry8n2zm713qayrkj1n2vc0msdng")))

(define-public crate-drop_struct_macro_derive-0.2.0 (c (n "drop_struct_macro_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hp8b0rrp6mbzsii44vssyjdazfr2xypjbflv893lfmfdvhyfir1")))

(define-public crate-drop_struct_macro_derive-0.3.0 (c (n "drop_struct_macro_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l3aippywc74rkz06hpwkfpplpqkjcr973g01d2cyg11mf8d6jhh")))

(define-public crate-drop_struct_macro_derive-0.4.0 (c (n "drop_struct_macro_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zs5nrpi8vdcwk3ww02h9ir1k2gh7q87zcxwqnyadlsv72rj61gk")))

(define-public crate-drop_struct_macro_derive-0.4.1 (c (n "drop_struct_macro_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1aj1dl3k3s09vmimr6wgjjy0zjlmd8fr9iqs91jc29a1nbl1rifz")))

(define-public crate-drop_struct_macro_derive-0.5.0 (c (n "drop_struct_macro_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pnmd4y5y0bx6zf3bd1njnzqkin5kkv9i8219lm9sf1cbccjhv66")))

