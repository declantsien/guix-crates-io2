(define-module (crates-io dr op droptest) #:use-module (crates-io))

(define-public crate-droptest-0.1.0 (c (n "droptest") (v "0.1.0") (h "0q24j5p2ldmwzqjfnkhv74vw829vrm02jfw9spym50v3xgsar3f2")))

(define-public crate-droptest-0.1.1 (c (n "droptest") (v "0.1.1") (h "1c2s5sjfqzp2ir1avawav36f3r25siwylkycnqfiabky2mwqyaj7")))

(define-public crate-droptest-0.2.0 (c (n "droptest") (v "0.2.0") (h "11zihlpz26y9zly8agnsyd78qf8qs3cwqg5dai6bzcvyf1p65cm3")))

(define-public crate-droptest-0.2.1 (c (n "droptest") (v "0.2.1") (h "1rzg7a6pjps4qg3psj31ijbj3p18i80dp5j4s26pbp9y344bd8zl")))

