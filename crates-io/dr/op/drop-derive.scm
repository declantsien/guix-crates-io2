(define-module (crates-io dr op drop-derive) #:use-module (crates-io))

(define-public crate-drop-derive-0.1.0 (c (n "drop-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0syn9b7xbicc7ixchmkv94qmsgdpamdd7if9rwxwfqqx5gj7bv0l")))

