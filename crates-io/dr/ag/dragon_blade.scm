(define-module (crates-io dr ag dragon_blade) #:use-module (crates-io))

(define-public crate-dragon_blade-1.0.0 (c (n "dragon_blade") (v "1.0.0") (h "0db71ql4y888xiv01smmj1f2hf7sybv09r39rskf03kqp3pp5qpb")))

(define-public crate-dragon_blade-2.0.0 (c (n "dragon_blade") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0ci2yhv2lqffph2z5hs45dlvnr1hgd3jc0syx69hwh7jk1yg3nj9")))

(define-public crate-dragon_blade-2.1.0 (c (n "dragon_blade") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0g59381p516cx2n94idzhksyyc8b0k5kbn1npqdl498pjhvm2dx9")))

