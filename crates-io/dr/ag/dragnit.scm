(define-module (crates-io dr ag dragnit) #:use-module (crates-io))

(define-public crate-dragnit-0.1.0 (c (n "dragnit") (v "0.1.0") (h "1pz3v72kk3pqi65ih4hx86hg8nzyz28k7ss35r37hywipiqdzf5j")))

(define-public crate-dragnit-0.1.1 (c (n "dragnit") (v "0.1.1") (h "06magyjww0s47619qhcvy6ibnbsj60rqzv6yr7m6ywbjfhf1vkc7")))

