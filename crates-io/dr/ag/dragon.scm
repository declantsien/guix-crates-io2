(define-module (crates-io dr ag dragon) #:use-module (crates-io))

(define-public crate-dragon-0.1.0 (c (n "dragon") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "07lr6f1dk97mz65fxghn3wr7pnf33fl3l5mqg6370lv48bw7nvh8")))

(define-public crate-dragon-0.1.2 (c (n "dragon") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)))) (h "0spngkmrh1dv2k8237gvgfdpkm6h61kl2rqfg4i7lki90khd86hf")))

(define-public crate-dragon-0.1.3 (c (n "dragon") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)))) (h "1dr1jgd900w5jvdmhmbswjrqq90xviqic6wpmcw0510si6zjz8q7")))

