(define-module (crates-io dr ag dragino_lgt92_tools) #:use-module (crates-io))

(define-public crate-dragino_lgt92_tools-0.1.0 (c (n "dragino_lgt92_tools") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "15zc8khmvbv68d695pgdnzdxlnjvixwqkg4fw1kr99x44zrjq924")))

(define-public crate-dragino_lgt92_tools-0.2.0 (c (n "dragino_lgt92_tools") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "0i7829y142psdbfv0j62ngv1p588jkxprnrayahb2fihziyghq77")))

(define-public crate-dragino_lgt92_tools-0.3.0 (c (n "dragino_lgt92_tools") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "05v0j7dijis2kbwk711vl0sifml08i5fiwgb7pmy1s3zvc7zwnz0")))

(define-public crate-dragino_lgt92_tools-0.4.0 (c (n "dragino_lgt92_tools") (v "0.4.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "00hdfg48mnzizxmqd4m8d4dzprdi3fr69x9k2drn5gl2pf01lvl4")))

(define-public crate-dragino_lgt92_tools-0.5.0 (c (n "dragino_lgt92_tools") (v "0.5.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "0zrxyqsvs3lpxnznvi0zcc8229lsq848sf4bx0l7n0iqfk98a04l")))

