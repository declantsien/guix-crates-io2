(define-module (crates-io dr ag dragula) #:use-module (crates-io))

(define-public crate-dragula-0.1.0 (c (n "dragula") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 2)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "DomTokenList" "Element" "HtmlElement" "HtmlCollection" "Window" "console"))) (d #t) (k 2)))) (h "1a07prz8ripmxq1mn3mz05mf81nc7qh6gibzv5wnj4jfs7mf3n2k") (f (quote (("default" "js-sys"))))))

