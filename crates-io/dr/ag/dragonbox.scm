(define-module (crates-io dr ag dragonbox) #:use-module (crates-io))

(define-public crate-dragonbox-0.1.0 (c (n "dragonbox") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0g59226fai85x8j3zr4f4wjsji8aw21rldhm4rmybp68lwd97xfa")))

(define-public crate-dragonbox-0.1.1 (c (n "dragonbox") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ism1w6n93npc3b1z5gnigqcvyapnygv07f08phfdxq63cvzx2n8") (r "1.51")))

(define-public crate-dragonbox-0.1.2 (c (n "dragonbox") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1mccwl1h0x3fnc76l0h4fvr7x7rgbc6jlbnw0g3i076gc783986n") (r "1.51")))

(define-public crate-dragonbox-0.1.3 (c (n "dragonbox") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0fyz83mxdv5hsk5s50p5l1jck81sxs57mf734pr0ky5qwqqzcp45") (r "1.51")))

(define-public crate-dragonbox-0.1.4 (c (n "dragonbox") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07k1i607r63pvrfkx3akfmdgirlb95g2zzvslk1k60xj2w1n84j9") (r "1.51")))

(define-public crate-dragonbox-0.1.5 (c (n "dragonbox") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "174dbwvi1bsg6147c1r7acjhn44z1xgy5nvkrha9rnynn6bqgkqm") (r "1.51")))

(define-public crate-dragonbox-0.1.6 (c (n "dragonbox") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xsrbwjja1nvbwgbkdmippilmzb0lr31isnkfix1kh5sc45nzwa2") (r "1.51")))

(define-public crate-dragonbox-0.1.7 (c (n "dragonbox") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1x31zfna5ay51sifyvzg15369f4a8f8mjgcbzw6d77kyar16pjyp") (r "1.51")))

(define-public crate-dragonbox-0.1.8 (c (n "dragonbox") (v "0.1.8") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xfl7jbk4jgdy2v42jddmjalyhhzgmnbacpdmqfs19nvvay0f9lf") (r "1.52")))

