(define-module (crates-io dr ag dragon_lair_utils) #:use-module (crates-io))

(define-public crate-dragon_lair_utils-0.1.0 (c (n "dragon_lair_utils") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casper_types_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "renvm-sig") (r "^0.1.1") (d #t) (k 0)))) (h "0ja6y84kbyv4p6zmnyr64bl74scjdnn4ij1kxvvv110jc3w9gcp9") (f (quote (("default" "casper-contract/std" "casper-types/std" "casper-contract/test-support"))))))

