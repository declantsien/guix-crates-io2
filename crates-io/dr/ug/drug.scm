(define-module (crates-io dr ug drug) #:use-module (crates-io))

(define-public crate-drug-0.0.1 (c (n "drug") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 2)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p53rs9lj733yjhcc2rzx0z6bkkwily04li31l4sz7lc7k49ck83")))

(define-public crate-drug-0.0.2 (c (n "drug") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 2)) (d (n "debug_stub_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 2)) (d (n "ndarray") (r "^0.11.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "145x1m0b10rf186zwli92s7bhjmvqbapv1x4n53dhmpa9pakl04i")))

