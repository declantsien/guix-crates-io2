(define-module (crates-io dr ug drug-extraction-core) #:use-module (crates-io))

(define-public crate-drug-extraction-core-0.1.0 (c (n "drug-extraction-core") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0dn25n0p6xmpmn8i8l71jnr9v745cih3jca0x2av1g35gqviik64")))

(define-public crate-drug-extraction-core-0.1.1 (c (n "drug-extraction-core") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "07vpdahfy64iywf8pwksg0i77zzvpm7q6pq5jjf0rfllpdm76i84")))

(define-public crate-drug-extraction-core-0.1.2 (c (n "drug-extraction-core") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0k8jdwgiy3pcy5mzmxp2v3nzrbwdipkimv27icx5163sxsixpgmd")))

