(define-module (crates-io dr au draumaz-butterfly) #:use-module (crates-io))

(define-public crate-draumaz-butterfly-0.16.0 (c (n "draumaz-butterfly") (v "0.16.0") (d (list (d (n "pancurses") (r "^0.17") (f (quote ("win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "savesys") (r "^3.5.9") (d #t) (k 0)))) (h "1q5hzj5p0r0njgvdjpgwdgzl1nzlk600ng805df7nc5asy56jysh")))

(define-public crate-draumaz-butterfly-0.16.1 (c (n "draumaz-butterfly") (v "0.16.1") (d (list (d (n "pancurses") (r "^0.17") (f (quote ("win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "savesys") (r "^3.5.9") (d #t) (k 0)))) (h "0plaq221ijb497rrm0akzaf47q80w0jc39jmq2wqcnbry52wx5qs")))

(define-public crate-draumaz-butterfly-0.16.2 (c (n "draumaz-butterfly") (v "0.16.2") (d (list (d (n "pancurses") (r "^0.17") (f (quote ("win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "savesys") (r "^3.5.9") (d #t) (k 0)))) (h "1gn1f5ylw8yx6j91wz8lxis2ps4n9bv6mj8mys8fz4kyjx7298n8")))

(define-public crate-draumaz-butterfly-0.16.3 (c (n "draumaz-butterfly") (v "0.16.3") (d (list (d (n "pancurses") (r "^0.17") (f (quote ("win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "savesys") (r "^3.5.9") (d #t) (k 0)))) (h "1apd1qf1k9sqgbxbvv5x42dagwsbwlm76w10371a79wr8yr2ill3")))

(define-public crate-draumaz-butterfly-0.16.4 (c (n "draumaz-butterfly") (v "0.16.4") (d (list (d (n "pancurses") (r "^0.17") (f (quote ("win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "savesys") (r "^3.5.9") (d #t) (k 0)))) (h "0z8a8xpy90l1fdjf3mfn9wvvh720niynnw4l79dich9klaw12hn8")))

