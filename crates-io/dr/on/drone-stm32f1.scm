(define-module (crates-io dr on drone-stm32f1) #:use-module (crates-io))

(define-public crate-drone-stm32f1-0.1.0 (c (n "drone-stm32f1") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.1") (d #t) (k 0)) (d (n "drone-test") (r "^0.1") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "1phygj2k6lqcyhw56hmak4585gw1mdbx34wgb3c4zadnb7mh22j3") (f (quote (("default"))))))

(define-public crate-drone-stm32f1-0.1.1 (c (n "drone-stm32f1") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.1.1") (d #t) (k 0)) (d (n "drone-test") (r "^0.1.1") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "1l0691q8y3pdfmbpdj0izlkzv1g7rh7qwb0xfwjm1c8gm48yg9g9") (f (quote (("default"))))))

(define-public crate-drone-stm32f1-0.1.2 (c (n "drone-stm32f1") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.1.2") (d #t) (k 0)) (d (n "drone-test") (r "^0.1.2") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "1ns1anv4gccikminry2ygrw2m5n162fi31k7kiba6gjhy69knrd6") (f (quote (("default"))))))

