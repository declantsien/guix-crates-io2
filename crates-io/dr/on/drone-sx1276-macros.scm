(define-module (crates-io dr on drone-sx1276-macros) #:use-module (crates-io))

(define-public crate-drone-sx1276-macros-0.1.0 (c (n "drone-sx1276-macros") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0sxsl62g6aw0f342287x4nqxcgai0c8pcdmmmi9hmigx8fyzd0pr")))

