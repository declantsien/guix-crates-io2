(define-module (crates-io dr on drone-tisl-map-pieces-10) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-10-0.0.1 (c (n "drone-tisl-map-pieces-10") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "10cbrf1mxvan2ynpjsrp9yssfz7psxnh1c7ka0scnyvyjjjd3qwv")))

(define-public crate-drone-tisl-map-pieces-10-0.13.0 (c (n "drone-tisl-map-pieces-10") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1cr8s24q4sddf1q7lvk36n0qmazjmn1a6slicwcq093njq7aj429")))

