(define-module (crates-io dr on drone-stm32-macros) #:use-module (crates-io))

(define-public crate-drone-stm32-macros-0.8.0 (c (n "drone-stm32-macros") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1wjc58nmbg752p3aa6sa2axxmxc928dbzin3vmn55ikb5k67jqnr")))

(define-public crate-drone-stm32-macros-0.8.1 (c (n "drone-stm32-macros") (v "0.8.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "03s0f86m462w8k88q7fgshkaw2iamla4yrsbgh5wivjldlscabjf")))

(define-public crate-drone-stm32-macros-0.8.2 (c (n "drone-stm32-macros") (v "0.8.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.8.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0471v85ibb7azc6jhnn6dghyi97vgd6i4l2897jp4qcc9hlxlj0g")))

(define-public crate-drone-stm32-macros-0.8.3 (c (n "drone-stm32-macros") (v "0.8.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.8.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "04lifl2k11q1s16w65y51rydvznv4fa2pq5mrd9abj3y2kr7a31b")))

