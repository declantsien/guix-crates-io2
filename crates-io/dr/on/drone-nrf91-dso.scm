(define-module (crates-io dr on drone-nrf91-dso) #:use-module (crates-io))

(define-public crate-drone-nrf91-dso-0.12.0 (c (n "drone-nrf91-dso") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map") (r "^0.12.0") (f (quote ("uarte"))) (d #t) (k 0)))) (h "0wfxlmm14yrgs4aymri36lxcskw6g4bakbgai32vmdp919yqllwf") (f (quote (("std" "drone-core/std" "drone-cortexm/std" "drone-nrf-map/std") ("default"))))))

(define-public crate-drone-nrf91-dso-0.12.1 (c (n "drone-nrf91-dso") (v "0.12.1") (d (list (d (n "drone-core") (r "^0.12.1") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.1") (d #t) (k 0)) (d (n "drone-nrf-map") (r "^0.12.0") (f (quote ("uarte"))) (d #t) (k 0)))) (h "0i1zpwalfxfkdl4gdvavigaxr75rxzyv5g2cwrs7l7ifrb256ldj") (f (quote (("std" "drone-core/std" "drone-cortexm/std" "drone-nrf-map/std") ("default"))))))

(define-public crate-drone-nrf91-dso-0.14.0 (c (n "drone-nrf91-dso") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map") (r "^0.14.0") (f (quote ("uarte"))) (d #t) (k 0)))) (h "00hyfkjn714iaw4x8pvmsiw9f03i0ml9n56klhzw622cm570n052") (f (quote (("std" "drone-core/std" "drone-cortexm/std" "drone-nrf-map/std") ("default"))))))

