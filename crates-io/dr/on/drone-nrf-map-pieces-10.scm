(define-module (crates-io dr on drone-nrf-map-pieces-10) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-10-0.11.0 (c (n "drone-nrf-map-pieces-10") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "1lcdpi5pckk5zd1y86rsw81729ki6bpljrmrc0lwzs8zc9k8krjf")))

(define-public crate-drone-nrf-map-pieces-10-0.12.0 (c (n "drone-nrf-map-pieces-10") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "1hn2hr0ml3ny3rcsymssc9vms4k2f7jjv2kj48qp7h68z8n1h7y7")))

(define-public crate-drone-nrf-map-pieces-10-0.13.0 (c (n "drone-nrf-map-pieces-10") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1gxv03nxijg2hqr5zrwyasb9rjb3x2jj7d524m1f82xfslaj6ry7")))

(define-public crate-drone-nrf-map-pieces-10-0.14.0 (c (n "drone-nrf-map-pieces-10") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "1yl0d8qbn3z5rxnqxcmfh658yi8c62y759i2pg447bl6wmc9ilfi")))

