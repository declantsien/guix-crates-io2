(define-module (crates-io dr on drone-nrf-map-svd) #:use-module (crates-io))

(define-public crate-drone-nrf-map-svd-0.11.0 (c (n "drone-nrf-map-svd") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.11.0") (d #t) (k 0)))) (h "08ccg6zk65g0kxkd0q6yhp83c2i3288bk4nj7sv3r790ixdpf49y")))

(define-public crate-drone-nrf-map-svd-0.12.0 (c (n "drone-nrf-map-svd") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.12.0") (d #t) (k 0)))) (h "1hg3kgvqq2hqrqafchmr4g7xf4piv5x4kg8y194719mbs2a1wg6m")))

(define-public crate-drone-nrf-map-svd-0.13.0 (c (n "drone-nrf-map-svd") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.13.0") (d #t) (k 0)))) (h "1p7zlqzjhpy46iywgki0kklincmrcssl5zq4pnsanzj3qsb43627")))

(define-public crate-drone-nrf-map-svd-0.14.0 (c (n "drone-nrf-map-svd") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.14.0") (d #t) (k 0)))) (h "1wavr3f92p62r19bw53faa6i5k9bxkdis1y54jg4q7xc65r3s65j")))

