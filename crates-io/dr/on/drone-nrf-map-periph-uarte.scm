(define-module (crates-io dr on drone-nrf-map-periph-uarte) #:use-module (crates-io))

(define-public crate-drone-nrf-map-periph-uarte-0.12.0 (c (n "drone-nrf-map-periph-uarte") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "= 0.12.0") (d #t) (k 0)))) (h "0plhs76jfkcgam5xvljbn8sjwcjaci15zvn8xbrqmjn4bga7jx40")))

(define-public crate-drone-nrf-map-periph-uarte-0.13.0 (c (n "drone-nrf-map-periph-uarte") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "0bvvyvks11bfrc6i60qcajdgxayyc2m1i8v4gcrznxzaalm8a8b9")))

(define-public crate-drone-nrf-map-periph-uarte-0.14.0 (c (n "drone-nrf-map-periph-uarte") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "=0.14.0") (d #t) (k 0)))) (h "11b1jsssi03xabm06lp20wpwz4abbappl3r9zrqyxd96h2mza4wj")))

