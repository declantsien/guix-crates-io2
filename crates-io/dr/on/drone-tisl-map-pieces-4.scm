(define-module (crates-io dr on drone-tisl-map-pieces-4) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-4-0.0.1 (c (n "drone-tisl-map-pieces-4") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "1d449n4d3ypdyc6x49q773nskp31nv3ishxnqqwnday3pzhixf69")))

(define-public crate-drone-tisl-map-pieces-4-0.13.0 (c (n "drone-tisl-map-pieces-4") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "08cd66lvv29c53jgnsh3k1gly8lpvky56scfz7r8q3dwiwwvxdji")))

