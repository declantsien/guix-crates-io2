(define-module (crates-io dr on drone-tisl-map-pieces-9) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-9-0.0.1 (c (n "drone-tisl-map-pieces-9") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "1kl66wwv8j5hda544c84ynq3ijlwpjb09dr71j8b4z1j666d4lvi")))

(define-public crate-drone-tisl-map-pieces-9-0.13.0 (c (n "drone-tisl-map-pieces-9") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "05xn7yc7iwfbq5mj16vy2s507y7cfv4pdxyc36v2zqx26v1saa1h")))

