(define-module (crates-io dr on drone-mirror-failure) #:use-module (crates-io))

(define-public crate-drone-mirror-failure-0.1.1 (c (n "drone-mirror-failure") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1y33i8w547vqzlr9hpk9a7x6jn90rdmrcjv4jm09kpdrik5cdc3x") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-drone-mirror-failure-0.1.2 (c (n "drone-mirror-failure") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0g3hjlcjr1didkcfdvpapw1z39j2bwzdqk6cmdfyw351wlsmxqjy") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

