(define-module (crates-io dr on drone-tisl-map-periph-uart) #:use-module (crates-io))

(define-public crate-drone-tisl-map-periph-uart-0.13.0 (c (n "drone-tisl-map-periph-uart") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-tisl-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "1wcp3kgwyk8b34wffrjzfaijyx9b65d6mfhmsdw6szcjk9a400va")))

