(define-module (crates-io dr on drone-tisl-map-pieces-11) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-11-0.0.1 (c (n "drone-tisl-map-pieces-11") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "0z8dj0bsdz1v5sz03wh45bcfw6rl9zhsm3hkqnwvzg8r3922s39y")))

(define-public crate-drone-tisl-map-pieces-11-0.13.0 (c (n "drone-tisl-map-pieces-11") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0z5dcdra32qa25iha8dpsm6l0bzhfij2kxyc3c8z73qn317yc90h")))

