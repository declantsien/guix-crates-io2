(define-module (crates-io dr on drone-sd-core) #:use-module (crates-io))

(define-public crate-drone-sd-core-0.1.0 (c (n "drone-sd-core") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "1xhs8rx3xbzk55dzgzig6wp74m1iqkznm8kzqb6l87vbin4g2sh4") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-sd-core-0.2.0 (c (n "drone-sd-core") (v "0.2.0") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "022ismnarwmy218f01pp77d3dqwx82ylv0cgivwp2ynxcp3g85hv") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-sd-core-0.2.1 (c (n "drone-sd-core") (v "0.2.1") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "025dv9g3zajrfcs68z62fw44z5w7a13x5bhi8vxy6mxc4k229r9f") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-sd-core-0.2.2 (c (n "drone-sd-core") (v "0.2.2") (d (list (d (n "drone-core") (r "^0.8.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)))) (h "06dqkwr46r7vfskh77yfkh2widzax6a41ya7spwf53k8kw70sjqn") (f (quote (("std" "drone-core/std" "futures-preview/std") ("default"))))))

