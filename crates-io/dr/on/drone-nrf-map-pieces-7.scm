(define-module (crates-io dr on drone-nrf-map-pieces-7) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-7-0.11.0 (c (n "drone-nrf-map-pieces-7") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "1ki9j0admjmr4i68mwdgnccv5p290h2801kp3k3ldqxsjwfnmjmd")))

(define-public crate-drone-nrf-map-pieces-7-0.12.0 (c (n "drone-nrf-map-pieces-7") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "0hkr5r4z2828i83dxs5n196bk6vfaphlvygsh15x6fhjbya6xvvw")))

(define-public crate-drone-nrf-map-pieces-7-0.13.0 (c (n "drone-nrf-map-pieces-7") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0k5if47z8zfs564fwd0q7wp3zpfnwljhr26q4dpb955n34k17pjg")))

(define-public crate-drone-nrf-map-pieces-7-0.14.0 (c (n "drone-nrf-map-pieces-7") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "02xxkfgambdv3h6gj182pgnz70w7wh9j77z7z0s9w6k931253mvb")))

