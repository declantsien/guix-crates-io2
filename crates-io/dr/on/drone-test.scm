(define-module (crates-io dr on drone-test) #:use-module (crates-io))

(define-public crate-drone-test-0.1.0 (c (n "drone-test") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "sc") (r "^0.2") (d #t) (k 0)))) (h "1pi1s1wfr3z0sjpv6x2l3qdp4c9vmb609kdvvbqjl5l0kzm9am1r") (f (quote (("panic_item") ("default"))))))

(define-public crate-drone-test-0.1.1 (c (n "drone-test") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "sc") (r "^0.2") (d #t) (k 0)))) (h "1is80mqp6z2vnpmwx80wvvw4pj56qqgpx447482xfxwmb7sf7j3g") (f (quote (("panic_item") ("default"))))))

(define-public crate-drone-test-0.1.2 (c (n "drone-test") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "sc") (r "^0.2") (d #t) (k 0)))) (h "0w17c4kx72z81xfdkc70yzndlc05kmlf67m94ah2i6w1yvkanvcn") (f (quote (("panic_item") ("default"))))))

