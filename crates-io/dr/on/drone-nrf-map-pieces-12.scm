(define-module (crates-io dr on drone-nrf-map-pieces-12) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-12-0.11.0 (c (n "drone-nrf-map-pieces-12") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "17c0rm5wj2kzyk1nfp1si1grrzif1lf77hn0c72wxj2f58yfli0p")))

(define-public crate-drone-nrf-map-pieces-12-0.12.0 (c (n "drone-nrf-map-pieces-12") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "1hjswbrnjmmff45s4dg5iccipib9934kiy8d6af66agdhg303ylx")))

(define-public crate-drone-nrf-map-pieces-12-0.13.0 (c (n "drone-nrf-map-pieces-12") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0fppp8iyhmr6ng0zdm7a39c92x0fnvdvfp9jpav9p067q22b8dz8")))

(define-public crate-drone-nrf-map-pieces-12-0.14.0 (c (n "drone-nrf-map-pieces-12") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "0ivqv9gkna8chdiwj08d81yhajwc3cqgw84jxkavcg9v9q2zs2gh")))

