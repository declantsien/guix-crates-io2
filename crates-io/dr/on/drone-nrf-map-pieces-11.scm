(define-module (crates-io dr on drone-nrf-map-pieces-11) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-11-0.11.0 (c (n "drone-nrf-map-pieces-11") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "0ihllx89jkm8ppz90zqcxhvi609pwy2xgz6ij3ykvdbkxsi8wy1l")))

(define-public crate-drone-nrf-map-pieces-11-0.12.0 (c (n "drone-nrf-map-pieces-11") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "0dxzahac9hn24abgds8w3dc7fqsxpjwqdq7bnz3kcxwsrakhqcil")))

(define-public crate-drone-nrf-map-pieces-11-0.13.0 (c (n "drone-nrf-map-pieces-11") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1r223j3rmd6krh8mlq053dbg5gx21zc2l1wbq8mkchpy0jqifzbg")))

(define-public crate-drone-nrf-map-pieces-11-0.14.0 (c (n "drone-nrf-map-pieces-11") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "153l83sx31wsvbyiqy41vaih0cl0m7p6rwjsqa38vay4jgxm813d")))

