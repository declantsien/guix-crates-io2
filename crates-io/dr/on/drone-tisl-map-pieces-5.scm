(define-module (crates-io dr on drone-tisl-map-pieces-5) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-5-0.0.1 (c (n "drone-tisl-map-pieces-5") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "0vdz9r8bhiifhd0f7vgm0p044jdrphf8fwrs6vpn3x0nbaynd1vr")))

(define-public crate-drone-tisl-map-pieces-5-0.13.0 (c (n "drone-tisl-map-pieces-5") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0ria98p7sm61g77hf8gri0gqfb09l72c9311jyx1qc74dl6ygpjj")))

