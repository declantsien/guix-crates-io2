(define-module (crates-io dr on drone-sd-stm32) #:use-module (crates-io))

(define-public crate-drone-sd-stm32-0.1.0 (c (n "drone-sd-stm32") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "169gck1r9psk23mv8p456i6ak2j6h5q9lw0b7iziz38a419i4wim")))

(define-public crate-drone-sd-stm32-0.2.0 (c (n "drone-sd-stm32") (v "0.2.0") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2.0") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "1kkc745pm0l0r423l192jn24gdlsk5z2g1v9hdx4djq9mwn1yszf")))

(define-public crate-drone-sd-stm32-0.2.1 (c (n "drone-sd-stm32") (v "0.2.1") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2.1") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "1rdj2j0q193ljnk1hdsdxifdb8ml84q432402ykz7k9nsc1wi46s")))

(define-public crate-drone-sd-stm32-0.2.2 (c (n "drone-sd-stm32") (v "0.2.2") (d (list (d (n "drone-core") (r "^0.8.3") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2.2") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8.3") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)))) (h "0rw1csd4xngwkldadizkmb8k6hicj35zw69yycq7riacxzym2pc1")))

