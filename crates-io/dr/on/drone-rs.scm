(define-module (crates-io dr on drone-rs) #:use-module (crates-io))

(define-public crate-drone-rs-0.1.0 (c (n "drone-rs") (v "0.1.0") (h "0i3flxffbhjz0106krf7m724rls51skfgmq7v5xg65jrspiidyks") (y #t)))

(define-public crate-drone-rs-0.0.1 (c (n "drone-rs") (v "0.0.1") (h "04209mdhs5ppkam9hq1lkbk3znzg76ckwfv9ld2bimp1x9bfkw85")))

