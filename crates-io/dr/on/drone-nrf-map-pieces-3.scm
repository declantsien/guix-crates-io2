(define-module (crates-io dr on drone-nrf-map-pieces-3) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-3-0.11.0 (c (n "drone-nrf-map-pieces-3") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "15dd5zvjdm83knqpc0sar6y1571db37bnrzm41scq3f0p2n0j4r2")))

(define-public crate-drone-nrf-map-pieces-3-0.12.0 (c (n "drone-nrf-map-pieces-3") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "0yhv4damrxhcl5dkfvdqzjqjn355qwd8cb7n59ng0ir4wlwn30hq")))

(define-public crate-drone-nrf-map-pieces-3-0.13.0 (c (n "drone-nrf-map-pieces-3") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1j7q7h1lh3kcni62yvc13m4van7cmjs9qdbqngkicihv5xz6gicc")))

(define-public crate-drone-nrf-map-pieces-3-0.14.0 (c (n "drone-nrf-map-pieces-3") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "0p3cwnm9ljy6r53ipk90i3y25kr1pb8ywlj4qa69y2y7z3g8lj3r")))

