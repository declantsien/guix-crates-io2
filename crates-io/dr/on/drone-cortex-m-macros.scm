(define-module (crates-io dr on drone-cortex-m-macros) #:use-module (crates-io))

(define-public crate-drone-cortex-m-macros-0.5.0 (c (n "drone-cortex-m-macros") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0qsd66ns71xp2j8h6kqkgp20f9anyq7ax3adm3983ky6yqrlz2cm")))

(define-public crate-drone-cortex-m-macros-0.5.1 (c (n "drone-cortex-m-macros") (v "0.5.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0693a9m3n35vysm6vfs06drqp9fphf6wlpf4fh0ix58cvq8p9g8z")))

(define-public crate-drone-cortex-m-macros-0.6.0 (c (n "drone-cortex-m-macros") (v "0.6.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1yb9cz6m4sw5lnzvvh3piixvbm1ckdkkkkbpb8f6rzyhm43ad51d")))

(define-public crate-drone-cortex-m-macros-0.7.0 (c (n "drone-cortex-m-macros") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1369i5b8l96c9cdzng4mziyldgdllk1v0jx9d7w1nfnb5p6rhi19")))

(define-public crate-drone-cortex-m-macros-0.9.0 (c (n "drone-cortex-m-macros") (v "0.9.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "14qzvv4nrs7bc4z77m5n8vibb6j2ispmkzajxhs779iajxi3knjc")))

(define-public crate-drone-cortex-m-macros-0.10.0 (c (n "drone-cortex-m-macros") (v "0.10.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1sfb3x40i62m7d9irayqybvabpr35ir7bx0636604nnqs50y4pf0")))

(define-public crate-drone-cortex-m-macros-0.10.1 (c (n "drone-cortex-m-macros") (v "0.10.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0p5jj2k6x9ziq34wsi8jv5ppw20rn3245afl5wqqxn8c7y6hm1ck")))

(define-public crate-drone-cortex-m-macros-0.11.0 (c (n "drone-cortex-m-macros") (v "0.11.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-config") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0anzq0bv9g1rpl93waf8marxj7p9jrnm7sr3bbnwrfw420vki5m3")))

(define-public crate-drone-cortex-m-macros-0.11.1 (c (n "drone-cortex-m-macros") (v "0.11.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-config") (r "^0.11.1") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0b0482dqdvql44a093lwmllvz99dw8qjb2q6brgq7xwpsblvx69j")))

