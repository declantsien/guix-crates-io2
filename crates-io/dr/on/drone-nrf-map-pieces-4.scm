(define-module (crates-io dr on drone-nrf-map-pieces-4) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-4-0.11.0 (c (n "drone-nrf-map-pieces-4") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "1ayg9ls5aq2yd763z0zbga5h6mwm8nq4aa59rqwa02crlnxhfapd")))

(define-public crate-drone-nrf-map-pieces-4-0.12.0 (c (n "drone-nrf-map-pieces-4") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "1hn56dafs8sxgy0ma97bzz84kzj93fv40vdrbsdm382sjw1pfk17")))

(define-public crate-drone-nrf-map-pieces-4-0.13.0 (c (n "drone-nrf-map-pieces-4") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0yr6xddxk15mfcrlr3msrdq1la712hmbwwhk668f2kmnvlyri25s")))

(define-public crate-drone-nrf-map-pieces-4-0.14.0 (c (n "drone-nrf-map-pieces-4") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "0259a9bnn7gkvlx59g905wfcbcbv8kdlbbb2i63wl1a6x3ivm3z1")))

