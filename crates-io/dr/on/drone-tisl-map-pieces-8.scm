(define-module (crates-io dr on drone-tisl-map-pieces-8) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-8-0.0.1 (c (n "drone-tisl-map-pieces-8") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "0l5p9pkacv0s3zigibmndvl2plil61fqd3nby6iknbppqsj5v15g")))

(define-public crate-drone-tisl-map-pieces-8-0.13.0 (c (n "drone-tisl-map-pieces-8") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1inx59zsrpaqjsza84sxdrxqd28f4j11ad9fs59r39gpy944fmlz")))

