(define-module (crates-io dr on drone-tisl-map-pieces-3) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-3-0.0.1 (c (n "drone-tisl-map-pieces-3") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "13nl6iymv0swv3jkmafws1q4b0mldbv0pipdvw531bh0lc4l1xgm")))

(define-public crate-drone-tisl-map-pieces-3-0.13.0 (c (n "drone-tisl-map-pieces-3") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "11ppsn0y8gs7vigvg0igx3a0w8s9azbh1kzn46h5szlhrk0d92gc")))

