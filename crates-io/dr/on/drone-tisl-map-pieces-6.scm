(define-module (crates-io dr on drone-tisl-map-pieces-6) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-6-0.0.1 (c (n "drone-tisl-map-pieces-6") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "0yfp5kypzk2nw6ycl9i0l2j4y3mb4pmyy6iny652hq0icdgdizff")))

(define-public crate-drone-tisl-map-pieces-6-0.13.0 (c (n "drone-tisl-map-pieces-6") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0z0ix5g6cqd92mmdpf9c37qj5ak6q251lkzj9dx1c8n0fgp6iclw")))

