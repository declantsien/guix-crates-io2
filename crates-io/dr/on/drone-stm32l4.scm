(define-module (crates-io dr on drone-stm32l4) #:use-module (crates-io))

(define-public crate-drone-stm32l4-0.1.1 (c (n "drone-stm32l4") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.1.1") (d #t) (k 0)) (d (n "drone-test") (r "^0.1.1") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "0ls3qrmj79zh169n24vg6panc44g14kxh66f0a3998ss2q2n7n9i") (f (quote (("default"))))))

(define-public crate-drone-stm32l4-0.1.2 (c (n "drone-stm32l4") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.1.2") (d #t) (k 0)) (d (n "drone-test") (r "^0.1.2") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "19jdc9wm8xdg9r78h7fbkn6599qc8jv9gxz61gz4n6r3c0j0fz0m") (f (quote (("default"))))))

