(define-module (crates-io dr on drone-macros) #:use-module (crates-io))

(define-public crate-drone-macros-0.5.0 (c (n "drone-macros") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1jd7z9ibdr9g1aglz07r8q7mr4gjjib7fmfygxjkcw8hrsx6gwc2")))

(define-public crate-drone-macros-0.5.1 (c (n "drone-macros") (v "0.5.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1qi3xyly281ywam6gs3v7cpk00249qqcswlfzb8qpxcnlv0zs6bl")))

(define-public crate-drone-macros-0.6.0 (c (n "drone-macros") (v "0.6.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "138xxgg8msah7jhh5iz6l2w9r84nyay3zr36j9dkgdbadxrzl16p")))

