(define-module (crates-io dr on drone-stm32-svd) #:use-module (crates-io))

(define-public crate-drone-stm32-svd-0.8.0 (c (n "drone-stm32-svd") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-mirror-failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "03piz2p1g77nf6h9l0d8rvi67z5lrp1yplrlwq45ysc5h31a4r6p")))

(define-public crate-drone-stm32-svd-0.8.1 (c (n "drone-stm32-svd") (v "0.8.1") (d (list (d (n "drone-mirror-failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0lf1aq3xzyvnmxqw93ax7g6m27j6mms6s4n5i4adf6mz0mnz3h3k")))

(define-public crate-drone-stm32-svd-0.8.2 (c (n "drone-stm32-svd") (v "0.8.2") (d (list (d (n "drone-mirror-failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1s8zcwpqwzw0xbhyi5dgh8by4nm94yiyjpzgwqi0ksazyfs4xl45")))

(define-public crate-drone-stm32-svd-0.8.3 (c (n "drone-stm32-svd") (v "0.8.3") (d (list (d (n "drone-mirror-failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0bhs025dkv2q1bbgpgs1j5fqbd40mqcq2yw7xp5xra7zpk5al3a9")))

