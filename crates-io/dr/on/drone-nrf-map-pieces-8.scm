(define-module (crates-io dr on drone-nrf-map-pieces-8) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-8-0.11.0 (c (n "drone-nrf-map-pieces-8") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "175xdc5xsdnzjzbkmby0rj7zydpq5kbdwcqgpabla27hbrfzn8xk")))

(define-public crate-drone-nrf-map-pieces-8-0.12.0 (c (n "drone-nrf-map-pieces-8") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "1jwzqnycfmzv07lwr6gd3fgh6mkydhgf3pwnsv1rg2kbv64cxz1k")))

(define-public crate-drone-nrf-map-pieces-8-0.13.0 (c (n "drone-nrf-map-pieces-8") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "03x8i9n8m7i8rmj48vcl010fwh77fdlf4kf8k6gif9zvf91j11qn")))

(define-public crate-drone-nrf-map-pieces-8-0.14.0 (c (n "drone-nrf-map-pieces-8") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "19xf2n4id34k4vysv8g85vn66a8af7jyivg9k8x0xw9hr2qzk5m6")))

