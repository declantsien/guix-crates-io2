(define-module (crates-io dr on drone-cortexm-macros) #:use-module (crates-io))

(define-public crate-drone-cortexm-macros-0.12.0 (c (n "drone-cortexm-macros") (v "0.12.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-config") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1p661fgfbmdlxvnq9vn6g15qmcsbhxfbkfxpbxa6wdd3mw9l4ps5")))

(define-public crate-drone-cortexm-macros-0.12.1 (c (n "drone-cortexm-macros") (v "0.12.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-config") (r "^0.12.1") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dsyl7ap43n2c3lm2djylqjvs7fjqn6bshdk18cr3jpf2liinvn3")))

(define-public crate-drone-cortexm-macros-0.12.2 (c (n "drone-cortexm-macros") (v "0.12.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "139m32rg3pvkf6zwxq9rng3l4vkqhqgnnpwxxkb46n9g0nl9rk0w")))

(define-public crate-drone-cortexm-macros-0.13.0 (c (n "drone-cortexm-macros") (v "0.13.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "154pyabjc4pd75zkh7b24rzvnn5d65s2d9x6da6x35glbml767kr")))

(define-public crate-drone-cortexm-macros-0.13.1 (c (n "drone-cortexm-macros") (v "0.13.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1cp11777kzib7fl382w6p895rgk7zfj10360vgsm6cy8fjy9pbqq")))

(define-public crate-drone-cortexm-macros-0.14.0 (c (n "drone-cortexm-macros") (v "0.14.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01gi1k8vsbxn7asng0vy54g3s0hbsdb748yir0zbs21rv4hk8ksr")))

(define-public crate-drone-cortexm-macros-0.14.1 (c (n "drone-cortexm-macros") (v "0.14.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ypx0dngzn5s9x6w76q4dj12gk2bs376lyfsa5bwlx936xaijirr")))

