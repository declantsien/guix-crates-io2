(define-module (crates-io dr on drone-sx1276-cortex-m) #:use-module (crates-io))

(define-public crate-drone-sx1276-cortex-m-0.1.0 (c (n "drone-sx1276-cortex-m") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.7") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "drone-sx1276") (r "^0.1.0") (d #t) (k 0)))) (h "0gmgr671ir9k78xpydzg866sljf165dmgr6kxinycr14lgjlgfmm")))

