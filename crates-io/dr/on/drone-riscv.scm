(define-module (crates-io dr on drone-riscv) #:use-module (crates-io))

(define-public crate-drone-riscv-0.14.0 (c (n "drone-riscv") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-riscv-macros") (r "=0.14.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "10lvy3q2zcckrgaw8vqwvijwa7a8wqkw91jx4capaf03pmrfn02b") (f (quote (("std" "drone-core/std" "futures/std") ("m-extension") ("default") ("c-extension") ("a-extension"))))))

