(define-module (crates-io dr on drone-tisl-map-pieces-7) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-7-0.0.1 (c (n "drone-tisl-map-pieces-7") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "19s8a2wrm1s6xnlgk7h7an00iql7l4lh2xckgsybsjc2ssf0splc")))

(define-public crate-drone-tisl-map-pieces-7-0.13.0 (c (n "drone-tisl-map-pieces-7") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1xq86jwizv0gj2awmj8rcv3420kjldakpnsnl4mp20wzv38wr47q")))

