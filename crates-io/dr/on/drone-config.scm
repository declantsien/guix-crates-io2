(define-module (crates-io dr on drone-config) #:use-module (crates-io))

(define-public crate-drone-config-0.9.0 (c (n "drone-config") (v "0.9.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1hx4xh7jl807pp1hkpbpyvlccx7dkwgiyzig0zg7l4rw3hrxcgsd")))

(define-public crate-drone-config-0.10.0 (c (n "drone-config") (v "0.10.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "18bgy7mwq6jg3f9vff9vbjn9hhr0f51k7i4zvx0amzdnwg2cxkrw")))

(define-public crate-drone-config-0.10.1 (c (n "drone-config") (v "0.10.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1bwp3hkyikqn51fv3rviss7rflgkma2jb5kq6ajjwzj746ym3frh")))

(define-public crate-drone-config-0.11.0 (c (n "drone-config") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06bvx0jxa1hl9mxmbvqizkfmfx2sgp20qc5wyzp73ajzm1fwba2j")))

(define-public crate-drone-config-0.11.1 (c (n "drone-config") (v "0.11.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0z1fzvn418yby8rg25dvaynlfr687ng90icsv8igvvfysaf27gbp")))

(define-public crate-drone-config-0.12.0 (c (n "drone-config") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1cwlnxkcbdxaarkx22grxl0364xrafa8bygjfifil7n3x7wd77kg")))

(define-public crate-drone-config-0.12.1 (c (n "drone-config") (v "0.12.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1r6ih3zw91bxrm8y4ci00yib01qjxvcmlflcnx4g29ygb1s6sr6n")))

(define-public crate-drone-config-0.12.2 (c (n "drone-config") (v "0.12.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1pycf7b5g74cz7z63j76iy2imkg6iahgyqwcvdr4fqg5vsdj6gjf")))

(define-public crate-drone-config-0.12.3 (c (n "drone-config") (v "0.12.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02335r5gk5cyibsn5w1cdvgxy37nsbpcv2vxpbg05s2mva4fcprh")))

(define-public crate-drone-config-0.12.4 (c (n "drone-config") (v "0.12.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1lhk159k4ifbjif085vapk8034c7kknapnlblapgchnhki8ff2kv")))

(define-public crate-drone-config-0.13.0 (c (n "drone-config") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bwn5k24kvp0nk26g7vy41glfv5qbpdg89cl0fj2hv1khjnjr7gx")))

(define-public crate-drone-config-0.13.1 (c (n "drone-config") (v "0.13.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "071lndrdviy3kn3dnncqlip9x1yi9iknbv5n2z6vpvlpqv3qh9fl")))

(define-public crate-drone-config-0.13.2 (c (n "drone-config") (v "0.13.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "07j3avpg19z1lzv4szbbpmvnrd1fm2bqmy8dyvd79m6w12qqdimc")))

(define-public crate-drone-config-0.14.0 (c (n "drone-config") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ly6gjlzi2nb9rcyj0sz1p02a1r2ddxs6m3rmn3690xmkdhl1hb0")))

