(define-module (crates-io dr on drone-svd) #:use-module (crates-io))

(define-public crate-drone-svd-0.11.0 (c (n "drone-svd") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)))) (h "0pda5n92062vfjvq58nnagkf5g4fd9crjhd49h71cdpzkrqvrdrn")))

(define-public crate-drone-svd-0.11.1 (c (n "drone-svd") (v "0.11.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)))) (h "16knp7xw2b9jrj2cvsx98ianlhvqd75a42jm37kqywwk76wwvzg3")))

(define-public crate-drone-svd-0.12.0 (c (n "drone-svd") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)))) (h "0p7rz7i4mrd3r2v0a3jcn08n3bzzlzlima9pkv1w13f9bhigg2qg")))

(define-public crate-drone-svd-0.13.0 (c (n "drone-svd") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)))) (h "12rsrly7abmbr29j94jj934sb9ir3w298bs56d4x699ski2b4v28")))

(define-public crate-drone-svd-0.14.0 (c (n "drone-svd") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)))) (h "0yzj65fyzfw6q23vfc977wi5r3x3xlr9z2q8fnr466391l31mb8h")))

