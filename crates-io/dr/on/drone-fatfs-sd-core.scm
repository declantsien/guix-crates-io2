(define-module (crates-io dr on drone-fatfs-sd-core) #:use-module (crates-io))

(define-public crate-drone-fatfs-sd-core-0.1.0 (c (n "drone-fatfs-sd-core") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "07l954lv2scgimzlkl67xjdggsc6p200gcawbp0mvnn5n52d7jka") (f (quote (("std" "futures/std") ("default"))))))

