(define-module (crates-io dr on drone-tisl-map-pieces-1) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-1-0.0.1 (c (n "drone-tisl-map-pieces-1") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "0zl7gn6gq0w24l227fp1j3n19iynprllcglj4bvhbgy0clrwalq1")))

(define-public crate-drone-tisl-map-pieces-1-0.13.0 (c (n "drone-tisl-map-pieces-1") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1xcxh6hlr1ndgmg3kvm3ls2g0xr7d9yrr5hsmi4p1llh40ciwd1b")))

