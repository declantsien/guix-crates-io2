(define-module (crates-io dr on drone-micropython-stm32) #:use-module (crates-io))

(define-public crate-drone-micropython-stm32-0.1.0 (c (n "drone-micropython-stm32") (v "0.1.0") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-micropython-core") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-micropython-raw") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "0cqly17fiydnzsihyvidmfnf914lnmlwllhkh4jjn4p1lspdf21y")))

(define-public crate-drone-micropython-stm32-0.1.1 (c (n "drone-micropython-stm32") (v "0.1.1") (d (list (d (n "drone-core") (r "^0.8.3") (d #t) (k 0)) (d (n "drone-micropython-core") (r "^0.1.1") (d #t) (k 0)) (d (n "drone-micropython-raw") (r "^0.1.1") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8.3") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)))) (h "079awvqp2zg0p01pw7fwjavqprwibm89dqndv8hds9wk1ahjj75k")))

