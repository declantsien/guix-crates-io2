(define-module (crates-io dr on drone-fatfs-core) #:use-module (crates-io))

(define-public crate-drone-fatfs-core-0.1.0 (c (n "drone-fatfs-core") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs-raw") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0a45vasnlnkby75bap6cvi2shimlkysrws4hk9m5yigansd6m6zd") (f (quote (("trim" "drone-fatfs-raw/trim") ("std" "futures/std") ("readonly" "drone-fatfs-raw/readonly") ("mkfs" "drone-fatfs-raw/mkfs") ("lfn" "drone-fatfs-raw/lfn") ("default" "mkfs" "trim" "lfn"))))))

