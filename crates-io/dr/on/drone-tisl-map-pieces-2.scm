(define-module (crates-io dr on drone-tisl-map-pieces-2) #:use-module (crates-io))

(define-public crate-drone-tisl-map-pieces-2-0.0.1 (c (n "drone-tisl-map-pieces-2") (v "0.0.1") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "= 0.0.1") (d #t) (k 1)))) (h "14bv5h4kkcrikrbxb2lplmghc75zpkbz9qj5vrwyqffv4mfgv6ks")))

(define-public crate-drone-tisl-map-pieces-2-0.13.0 (c (n "drone-tisl-map-pieces-2") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (f (quote ("bit-band"))) (d #t) (k 0)) (d (n "drone-tisl-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0dldbpvbdlmyn4sdqnvskh91a850d22n3visnri3q5zx4zjcrqxb")))

