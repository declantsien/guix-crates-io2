(define-module (crates-io dr on drone-tisl-map-svd) #:use-module (crates-io))

(define-public crate-drone-tisl-map-svd-0.0.1 (c (n "drone-tisl-map-svd") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.12.0") (d #t) (k 0)))) (h "0yshimzkyglwlzywpkfwq78kifw4320hsi64c7zsn17nxf2c0min")))

(define-public crate-drone-tisl-map-svd-0.13.0 (c (n "drone-tisl-map-svd") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.13.0") (d #t) (k 0)))) (h "1yv6sf635b6l36kwwwdz3swxgk1zbcwn5inha9lcd40lbf078xda")))

