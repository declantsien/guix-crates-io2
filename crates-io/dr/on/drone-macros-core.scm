(define-module (crates-io dr on drone-macros-core) #:use-module (crates-io))

(define-public crate-drone-macros-core-0.7.0 (c (n "drone-macros-core") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0x40rwb4hc76zdiba60ms0p4x0wa28gi3i762qa6hgv61zysd3q6")))

(define-public crate-drone-macros-core-0.8.0 (c (n "drone-macros-core") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1mjbdq6sr6d26alwrmvv8dkhkzaxglg6qnkvcqx51wdvz6xjjahs")))

(define-public crate-drone-macros-core-0.8.1 (c (n "drone-macros-core") (v "0.8.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0y7p7qsvygajdsb2nx3wcmwh4mi80pirla19d9w85gvqhrf1gw6h")))

(define-public crate-drone-macros-core-0.8.2 (c (n "drone-macros-core") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "13q301ilhqml1nki7yhlxjga8k4sha9nv0ilnk7x5mzn4hyr0rbm")))

(define-public crate-drone-macros-core-0.8.3 (c (n "drone-macros-core") (v "0.8.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "03r8x6l98adpjz19jkycjnyhja3aqb9s04pqqzxmb97z78f9mkz4")))

(define-public crate-drone-macros-core-0.9.0 (c (n "drone-macros-core") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bchfif66b8ssr6s031dqi23ai2l7cbs8x9f64zfpk45ywy61v4r")))

(define-public crate-drone-macros-core-0.10.0 (c (n "drone-macros-core") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pnxl8svbkmbss96yb0pl70k4w4zpnzmf6r3zy8qjsd0qrb8wy3f")))

(define-public crate-drone-macros-core-0.10.1 (c (n "drone-macros-core") (v "0.10.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01yd0g2siqykrkknp65ms046wkh0s9qxh5q3ijc6qks42v008a2n")))

(define-public crate-drone-macros-core-0.11.0 (c (n "drone-macros-core") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "166gmlm21shyzlc7fmp5c6dh2ffa9pn76hcc24rb87gc7l9bw8lz")))

(define-public crate-drone-macros-core-0.11.1 (c (n "drone-macros-core") (v "0.11.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a9xq56wakyfqqlxbpfg2ci4r0cy4rwjqr9a39f9hjvwgqvw75s2")))

(define-public crate-drone-macros-core-0.12.0 (c (n "drone-macros-core") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "169bzzscaqixxnlnzshcackvxl8z6g1ka8gpns41r7yns4b0vg8r")))

(define-public crate-drone-macros-core-0.12.1 (c (n "drone-macros-core") (v "0.12.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xn9za3vzqrcbjblf4l5iz8bp50mhija67g4pam1q5dz0pi4qk4h")))

(define-public crate-drone-macros-core-0.13.0 (c (n "drone-macros-core") (v "0.13.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qz28pjvkyf6dywkhz72374w4828ymsz2jhyd3gjfdqvknbs7qyf")))

(define-public crate-drone-macros-core-0.14.0 (c (n "drone-macros-core") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a5j4xhinvajmgiwcfqdgj1i23g7jxsyghwybji1d7rkr7r32ymh")))

(define-public crate-drone-macros-core-0.14.1 (c (n "drone-macros-core") (v "0.14.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gn07yzb6q9kifvas0wmc8w63x5rxxhcmy0lhspn7h2jhmp0i82r") (y #t)))

(define-public crate-drone-macros-core-0.14.2 (c (n "drone-macros-core") (v "0.14.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v4lw4vfgwn5395lbcwfafmb0wm1lyb9nkca6w2dmn4q8b505yp1")))

(define-public crate-drone-macros-core-0.14.3 (c (n "drone-macros-core") (v "0.14.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cbrk5b23f9xykqpc520mn7d3hmvbfa6m2pjka1ilgqnhnb4z5pw")))

