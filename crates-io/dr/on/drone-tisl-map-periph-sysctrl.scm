(define-module (crates-io dr on drone-tisl-map-periph-sysctrl) #:use-module (crates-io))

(define-public crate-drone-tisl-map-periph-sysctrl-0.13.0 (c (n "drone-tisl-map-periph-sysctrl") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-tisl-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "1v01769jgya4wynyqzfr9kx7yxbmfdw0zrgnasvwnapjdaywy0ss")))

