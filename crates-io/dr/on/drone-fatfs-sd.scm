(define-module (crates-io dr on drone-fatfs-sd) #:use-module (crates-io))

(define-public crate-drone-fatfs-sd-0.2.0 (c (n "drone-fatfs-sd") (v "0.2.0") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs") (r "^0.2.0") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "1ypy6l3iqd329xh93akwhc0y0a6b3w743m90zb5igwi1a0acasxp") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-fatfs-sd-0.2.1 (c (n "drone-fatfs-sd") (v "0.2.1") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs") (r "^0.2.1") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "12c3j5iy1g168w81fyflr5sbgycg61988nypi8abzq7ka0787xr2") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-fatfs-sd-0.2.2 (c (n "drone-fatfs-sd") (v "0.2.2") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs") (r "^0.2.2") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "0vrp034fdr7l2ji7qs10w2br17f2a2f22c38m2qck306y1j9vr1m") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-fatfs-sd-0.2.3 (c (n "drone-fatfs-sd") (v "0.2.3") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs") (r "^0.2.3") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)))) (h "1dja52phlvvh9wanmmpvk5z9qk1qxi1n3g7mrchinzf6fcna8m8i") (f (quote (("std" "drone-core/std" "futures-preview/std") ("default"))))))

