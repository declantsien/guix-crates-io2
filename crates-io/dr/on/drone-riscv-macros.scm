(define-module (crates-io dr on drone-riscv-macros) #:use-module (crates-io))

(define-public crate-drone-riscv-macros-0.14.0 (c (n "drone-riscv-macros") (v "0.14.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "drone-macros-core") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k13s4i0csj6dim87f4a6q6y90ki8vwa6qaacf0x2hxi43if860r")))

