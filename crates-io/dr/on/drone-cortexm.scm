(define-module (crates-io dr on drone-cortexm) #:use-module (crates-io))

(define-public crate-drone-cortexm-0.12.0 (c (n "drone-cortexm") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "= 0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "0f6q7c4yvib1raybpfic5k0yhiwkjklnrx1zgg8wfivhdc2b7ak3") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.12.1 (c (n "drone-cortexm") (v "0.12.1") (d (list (d (n "drone-core") (r "^0.12.1") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "= 0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "1psx0q3r3rlljiz31d0nx3q38ywywnsbqqkbpdg49l303s0gjvic") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.12.2 (c (n "drone-cortexm") (v "0.12.2") (d (list (d (n "drone-core") (r "^0.12.1") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "=0.12.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "0d7lcigiaw60w4w6hjnyf8i66ky2g783f33z577qra3di44vl9nk") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.13.0 (c (n "drone-cortexm") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "=0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "1i0bx9hsyspfwzfcm065l8k50xf73ismc7471dld413x98bqd5s0") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.13.1 (c (n "drone-cortexm") (v "0.13.1") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "=0.13.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "1xdg6q60nmfhqa6wp1zpanmaygzz3zwjljjfl5vrybkjnm7askc7") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.14.0 (c (n "drone-cortexm") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "=0.14.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "0afbajwn3wl713yvcnl2c2h33khjmmrd0qx8hdrkirrv3piqvbmd") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

(define-public crate-drone-cortexm-0.14.1 (c (n "drone-cortexm") (v "0.14.1") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm-macros") (r "=0.14.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "0k4r4jkjvkhhq4grfc687jhk827xncrxfackfsl99f5rg474fq63") (f (quote (("std" "drone-core/std" "futures/std") ("security-extension") ("memory-protection-unit") ("floating-point-unit") ("default") ("bit-band"))))))

