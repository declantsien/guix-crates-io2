(define-module (crates-io dr on drone-cortex-m-svd) #:use-module (crates-io))

(define-public crate-drone-cortex-m-svd-0.7.0 (c (n "drone-cortex-m-svd") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0aq2iq0vn0qymf01v05vkks714nmdhi97x1f9yz1s3qi1lnhyziq")))

