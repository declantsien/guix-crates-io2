(define-module (crates-io dr on drone-tisl-map-periph-gpio) #:use-module (crates-io))

(define-public crate-drone-tisl-map-periph-gpio-0.13.0 (c (n "drone-tisl-map-periph-gpio") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-tisl-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "0znjdrgq2kb6p5k6i69mcnqn151fnd86vs9pcg3ywf7x1ixr6w99")))

