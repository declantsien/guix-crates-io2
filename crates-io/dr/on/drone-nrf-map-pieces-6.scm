(define-module (crates-io dr on drone-nrf-map-pieces-6) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-6-0.11.0 (c (n "drone-nrf-map-pieces-6") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "072fvs15qva8h6b0ik0nw1wiqggq2mj6nhj5g6fmq50ir6p7fgvs")))

(define-public crate-drone-nrf-map-pieces-6-0.12.0 (c (n "drone-nrf-map-pieces-6") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "1x3xlrlrf81vg3bd1j8rdp7hqmdcs3jygybqrvp942h8idif19f8")))

(define-public crate-drone-nrf-map-pieces-6-0.13.0 (c (n "drone-nrf-map-pieces-6") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "1kxwrpjfyall5jcggr9z3vnbqzlh9lgv76c1crpkdcdjjxmglnvv")))

(define-public crate-drone-nrf-map-pieces-6-0.14.0 (c (n "drone-nrf-map-pieces-6") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "1lb4gk9c0zdlygsym4a1j55qri1z1sxgvay12s63kfnwn6vy6b1c")))

