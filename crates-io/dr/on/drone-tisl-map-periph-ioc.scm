(define-module (crates-io dr on drone-tisl-map-periph-ioc) #:use-module (crates-io))

(define-public crate-drone-tisl-map-periph-ioc-0.13.0 (c (n "drone-tisl-map-periph-ioc") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-tisl-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "0bvkdqp44bi82qhhkadx3qa55x6qqrhq15bjyfd2abhagq1cl0am")))

