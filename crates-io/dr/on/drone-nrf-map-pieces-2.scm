(define-module (crates-io dr on drone-nrf-map-pieces-2) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-2-0.11.0 (c (n "drone-nrf-map-pieces-2") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "0isfqw07i5l0riqbbhfyivrnagvhxmrfzfp5q7wx6z05169wy214")))

(define-public crate-drone-nrf-map-pieces-2-0.12.0 (c (n "drone-nrf-map-pieces-2") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "09ndscwnbics3aj1srwbhzsc4sjvymm0j2k7y679n1b21kw78hw8")))

(define-public crate-drone-nrf-map-pieces-2-0.13.0 (c (n "drone-nrf-map-pieces-2") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0g99ck4r2iab5rs4nxn0liz52hp69hsckhil6vq563w795mssngr")))

(define-public crate-drone-nrf-map-pieces-2-0.14.0 (c (n "drone-nrf-map-pieces-2") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "1y026bakb411k2c0ls58b7lkg4vp4l3ziaka8qfywx6h6aai26wn")))

