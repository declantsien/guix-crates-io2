(define-module (crates-io dr on drone-nrf-map) #:use-module (crates-io))

(define-public crate-drone-nrf-map-0.11.0 (c (n "drone-nrf-map") (v "0.11.0") (d (list (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "= 0.11.0") (d #t) (k 0)))) (h "1gg9kw8i089kabkg9qny0av4wm88w3mmnk7nx58lwmshcfdsa50j") (f (quote (("default"))))))

(define-public crate-drone-nrf-map-0.12.0 (c (n "drone-nrf-map") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-periph-uarte") (r "= 0.12.0") (o #t) (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "= 0.12.0") (d #t) (k 0)))) (h "0ck6wm3gwl2b17mmhlz1ccc0li858mbhx0gp9xw0hnjj0a95b4ql") (f (quote (("uarte" "drone-nrf-map-periph-uarte") ("std" "drone-core/std" "drone-cortexm/std") ("default") ("bit-band" "drone-nrf-map-pieces/bit-band"))))))

(define-public crate-drone-nrf-map-0.13.0 (c (n "drone-nrf-map") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-periph-uarte") (r "=0.13.0") (o #t) (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "=0.13.0") (d #t) (k 0)))) (h "1bvn02nah48pfdcs4bqjh6sl735b5lbdlbqvqbp3axp36dnnixrg") (f (quote (("uarte" "drone-nrf-map-periph-uarte") ("std" "drone-core/std" "drone-cortexm/std") ("default") ("bit-band" "drone-nrf-map-pieces/bit-band"))))))

(define-public crate-drone-nrf-map-0.14.0 (c (n "drone-nrf-map") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-periph-uarte") (r "=0.14.0") (o #t) (d #t) (k 0)) (d (n "drone-nrf-map-pieces") (r "=0.14.0") (d #t) (k 0)))) (h "0vgnya3cv3k3bqi4ipwfb266dbvm5hrnz91pwd9gxnzfqq67b529") (f (quote (("uarte" "drone-nrf-map-periph-uarte") ("std" "drone-core/std" "drone-cortexm/std") ("default") ("bit-band" "drone-nrf-map-pieces/bit-band"))))))

