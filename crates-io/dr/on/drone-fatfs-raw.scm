(define-module (crates-io dr on drone-fatfs-raw) #:use-module (crates-io))

(define-public crate-drone-fatfs-raw-0.1.0 (c (n "drone-fatfs-raw") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "drone-ctypes") (r "^0.8") (d #t) (k 0)))) (h "0xiyzw2pb5gxgj859734zc3jiyrgwpqa4610i8zznkcswn22p56q") (f (quote (("trim") ("readonly") ("mkfs") ("lfn") ("default"))))))

(define-public crate-drone-fatfs-raw-0.2.0 (c (n "drone-fatfs-raw") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "drone-ctypes") (r "^0.8") (d #t) (k 0)))) (h "0c55s2ax6ixd31r51wdmfbkawmwq6frgkis79k9qjcnb1jb7mb9r") (f (quote (("trim") ("readonly") ("mkfs") ("lfn") ("default"))))))

(define-public crate-drone-fatfs-raw-0.2.1 (c (n "drone-fatfs-raw") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "drone-ctypes") (r "^0.8") (d #t) (k 0)))) (h "037hs9zsnh1j8aspaal096wpc584c2wql351gzq1prhwygla9vzs") (f (quote (("trim") ("readonly") ("mkfs") ("lfn") ("default"))))))

(define-public crate-drone-fatfs-raw-0.2.2 (c (n "drone-fatfs-raw") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "drone-ctypes") (r "^0.8") (d #t) (k 0)))) (h "0davbhs5nwglnay4zfj9pdpkqdsnvl2yw7ff06305m3qa4wnyhb3") (f (quote (("trim") ("readonly") ("mkfs") ("lfn") ("default"))))))

(define-public crate-drone-fatfs-raw-0.2.3 (c (n "drone-fatfs-raw") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "drone-ctypes") (r "^0.8") (d #t) (k 0)))) (h "0civvpi79s5bb7ngmxvb07430s4aj137lqz6ymdzyrspjfgm3rmx") (f (quote (("trim") ("readonly") ("mkfs") ("lfn") ("default"))))))

