(define-module (crates-io dr on drone-nrf-map-pieces-5) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-5-0.11.0 (c (n "drone-nrf-map-pieces-5") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "06zqzf2wp5iniibyzigd4qv798350i788diqnlsbmxyv0g39bh4r")))

(define-public crate-drone-nrf-map-pieces-5-0.12.0 (c (n "drone-nrf-map-pieces-5") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "04ydhpl0kpsw1acaz9v3hgrx1dva787sb9cr8lq9bsbdy101ka4f")))

(define-public crate-drone-nrf-map-pieces-5-0.13.0 (c (n "drone-nrf-map-pieces-5") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0xswm2xagdqhz6sx4ijjv886sipfdzm2n0awqpv4wfgwd88azkwy")))

(define-public crate-drone-nrf-map-pieces-5-0.14.0 (c (n "drone-nrf-map-pieces-5") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "1i2ci50pbyh4fgjq7nawsi2177lkj2h98348ymjb5j0jks2kmg09")))

