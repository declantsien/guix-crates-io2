(define-module (crates-io dr on drone-micropython-core) #:use-module (crates-io))

(define-public crate-drone-micropython-core-0.1.0 (c (n "drone-micropython-core") (v "0.1.0") (d (list (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-micropython-raw") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)))) (h "1hvxsnmpi8h2gh3w5dc4hh758xjimllwsq49i58r9mdycapzfq2d") (f (quote (("std") ("io" "drone-micropython-raw/io") ("fileio" "io" "drone-micropython-raw/fileio") ("default"))))))

(define-public crate-drone-micropython-core-0.1.1 (c (n "drone-micropython-core") (v "0.1.1") (d (list (d (n "drone-core") (r "^0.8.3") (d #t) (k 0)) (d (n "drone-micropython-raw") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)))) (h "0a0s2j11bv8yw2mihg7imn5fmwbjl08amb9j1r91pwp66pj2i538") (f (quote (("std" "drone-core/std" "futures-preview/std") ("io" "drone-micropython-raw/io") ("fileio" "io" "drone-micropython-raw/fileio") ("default"))))))

