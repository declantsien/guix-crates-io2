(define-module (crates-io dr on drone-nrf-map-pieces-9) #:use-module (crates-io))

(define-public crate-drone-nrf-map-pieces-9-0.11.0 (c (n "drone-nrf-map-pieces-9") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-cortex-m") (r "^0.11.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.11.0") (d #t) (k 1)))) (h "1254qk42wrhgmcvclk47mm5w9kbmghn5ny9w8j0y7wpa8fx82l95")))

(define-public crate-drone-nrf-map-pieces-9-0.12.0 (c (n "drone-nrf-map-pieces-9") (v "0.12.0") (d (list (d (n "drone-core") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.12.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "= 0.12.0") (d #t) (k 1)))) (h "0k6h16ynaacs4xbikj710rarmz8p9q40198vq3y51fs323lmpkki")))

(define-public crate-drone-nrf-map-pieces-9-0.13.0 (c (n "drone-nrf-map-pieces-9") (v "0.13.0") (d (list (d (n "drone-core") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.13.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.13.0") (d #t) (k 1)))) (h "0i5a1mrfhgbfdpkf6mw7mh5dns9a8byk913d7gdfn7ciql0ik0qd")))

(define-public crate-drone-nrf-map-pieces-9-0.14.0 (c (n "drone-nrf-map-pieces-9") (v "0.14.0") (d (list (d (n "drone-core") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-cortexm") (r "^0.14.0") (d #t) (k 0)) (d (n "drone-nrf-map-svd") (r "=0.14.0") (d #t) (k 1)))) (h "0xrngsyqa2pgcvkvaf6m61vpjg7bgrdylfb6w0bh8rxlg6waxf2p")))

