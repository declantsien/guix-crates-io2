(define-module (crates-io dr on drone-stm32-map-svd) #:use-module (crates-io))

(define-public crate-drone-stm32-map-svd-0.9.0 (c (n "drone-stm32-map-svd") (v "0.9.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1iink7jlavmn55cc065vi32d31n7lk60j60khws3nqzyp86901mk")))

(define-public crate-drone-stm32-map-svd-0.10.0 (c (n "drone-stm32-map-svd") (v "0.10.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1gawh3yas9znyr67pvisx1mylvk70wjhp63bq55jxjqry66wiblk")))

(define-public crate-drone-stm32-map-svd-0.10.1 (c (n "drone-stm32-map-svd") (v "0.10.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2") (d #t) (k 0)))) (h "0d19yr2cm5flycqk670af7xqbxgghw950xcnfzfrrd96h504jpcq")))

(define-public crate-drone-stm32-map-svd-0.11.0 (c (n "drone-stm32-map-svd") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.11.0") (d #t) (k 0)))) (h "1cvxzk1dsl7kfsbyz46fapjxihm0ia5hxk905sj0srpaw5v6yrki")))

(define-public crate-drone-stm32-map-svd-0.11.1 (c (n "drone-stm32-map-svd") (v "0.11.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.11.1") (d #t) (k 0)))) (h "0j0cbw57sgjwmb1sc5w1ndn0cij4m7r4q6bi60s2dhsj27ya41zy")))

(define-public crate-drone-stm32-map-svd-0.11.2 (c (n "drone-stm32-map-svd") (v "0.11.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.11.1") (d #t) (k 0)))) (h "1f86w3kxn6z4n818mkc4bypzk182yrs06hdwka09q8wyx86yjkij")))

(define-public crate-drone-stm32-map-svd-0.12.0 (c (n "drone-stm32-map-svd") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.12.0") (d #t) (k 0)))) (h "1jasx8dh1si6k1bhy7j9p5afk2dm4b53qva318awfz9hb676wanz")))

(define-public crate-drone-stm32-map-svd-0.12.1 (c (n "drone-stm32-map-svd") (v "0.12.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.12.0") (d #t) (k 0)))) (h "1qd302lj93kwhd00511p0qqryc6x08a14i3fd5qa35a36a6zm8m6")))

(define-public crate-drone-stm32-map-svd-0.12.3 (c (n "drone-stm32-map-svd") (v "0.12.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.12.0") (d #t) (k 0)))) (h "1b7blmxj273hn528wz5gs7ff1jmml50fqvbyrccgv1krzywicj23")))

(define-public crate-drone-stm32-map-svd-0.13.0 (c (n "drone-stm32-map-svd") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.13.0") (d #t) (k 0)))) (h "0kxvcclsqziv5mikal6slk7sy4zyhmwd9bkqbb05q91kygyf2c9m")))

(define-public crate-drone-stm32-map-svd-0.14.0 (c (n "drone-stm32-map-svd") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "drone-svd") (r "^0.14.0") (d #t) (k 0)))) (h "0qp6l3xz4p2nghnyz75g7ycjbps81s5rd546x2saa9vdva24w86v")))

