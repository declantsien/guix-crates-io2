(define-module (crates-io dr on drone-core) #:use-module (crates-io))

(define-public crate-drone-core-0.1.0 (c (n "drone-core") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-test") (r "^0.1") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "0dc5099mmfp0pa91xdmj5zs4880d44h08f85182vlmrd07x10hym") (f (quote (("default"))))))

(define-public crate-drone-core-0.1.1 (c (n "drone-core") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-test") (r "^0.1.1") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "0pizks1rd92b089if72n01bl82zpbynm3j865l2qkaqbfgj92l3p") (f (quote (("default"))))))

(define-public crate-drone-core-0.1.2 (c (n "drone-core") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-test") (r "^0.1.2") (f (quote ("panic_item"))) (d #t) (k 2)))) (h "1p411vziwjv5z9rkkkfwn68j2f89zm59lhf42c36cggr74f9nais") (f (quote (("default"))))))

(define-public crate-drone-core-0.7.0 (c (n "drone-core") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "drone-core-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)))) (h "0yhwnzsqz3fi25nqd7rbzfqyxarz3npva06d0kh9d1c024ilygkd") (f (quote (("std" "futures/use_std") ("default"))))))

(define-public crate-drone-core-0.8.0 (c (n "drone-core") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "drone-core-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "^0.8.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)))) (h "0pafa53093zw8r8dz0vv0kgmnld7y5g40cmi7bdr7siam0asylgc") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.8.1 (c (n "drone-core") (v "0.8.1") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "drone-core-macros") (r "^0.8.1") (d #t) (k 0)) (d (n "drone-ctypes") (r "^0.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)))) (h "1s9xz19zw59zpnljggys5kkd9k27c58yrmnwhwhnaw12phzdwd8d") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.8.2 (c (n "drone-core") (v "0.8.2") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "drone-core-macros") (r "^0.8.2") (d #t) (k 0)) (d (n "drone-ctypes") (r "^0.8.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (k 0)) (d (n "futures") (r "^0.2") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)))) (h "1vwzcvr2kix10b66gqlmsp93dbmxwpli00spk5a2pa4j4vb58m0l") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.8.3 (c (n "drone-core") (v "0.8.3") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "drone-core-macros") (r "^0.8.3") (d #t) (k 0)) (d (n "drone-ctypes") (r "^0.8.3") (d #t) (k 0)) (d (n "failure") (r "= 0.1.1") (k 0)) (d (n "failure_derive") (r "= 0.1.1") (k 0)) (d (n "futures-preview") (r "^0.2") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)))) (h "1zjbclmmwj02g5p62wj1i02cimqp8wsbviaixcdm6nj756ii4hib") (f (quote (("std" "futures-preview/std") ("default"))))))

(define-public crate-drone-core-0.9.0 (c (n "drone-core") (v "0.9.0") (d (list (d (n "drone-core-macros") (r "= 0.9.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.9.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (k 0)))) (h "1689s6v55bqmsy8cchj2hdi3lpc39dwfrh62n8z856y7gizgagzd") (f (quote (("std" "futures-preview/std") ("default"))))))

(define-public crate-drone-core-0.10.0 (c (n "drone-core") (v "0.10.0") (d (list (d (n "drone-core-macros") (r "= 0.10.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.10.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (k 0)))) (h "1pwwzd00y0jj53l0j5hkl5ma1803sh06hf5fyfbj3hj7hfkwkm6l") (f (quote (("std" "futures-preview/std") ("default"))))))

(define-public crate-drone-core-0.10.1 (c (n "drone-core") (v "0.10.1") (d (list (d (n "drone-core-macros") (r "= 0.10.1") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.10.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (k 0)))) (h "0n81ivlf062x0ainxk7fp1hl22jzf4zsrlpbqwdval2zqn5cjf1p") (f (quote (("std" "futures-preview/std") ("default"))))))

(define-public crate-drone-core-0.11.0 (c (n "drone-core") (v "0.11.0") (d (list (d (n "drone-core-macros") (r "= 0.11.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.11.0") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (k 0)))) (h "015mhr90gpfnrnkz012gczjan6g4p4vwlhv2xppal6ic7xk7fs9y") (f (quote (("std" "futures-preview/std") ("default"))))))

(define-public crate-drone-core-0.11.1 (c (n "drone-core") (v "0.11.1") (d (list (d (n "drone-core-macros") (r "= 0.11.1") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.11.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (k 0)))) (h "04kv4jwvpbadnx5wfbschkvwjbyz5i551dm4pf7lyh4kj8lba492") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.12.0 (c (n "drone-core") (v "0.12.0") (d (list (d (n "drone-core-macros") (r "= 0.12.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)))) (h "03jsdhak7chsp9jy1jb78b416737wp7k3s99p6ivqaprnzjddnz2") (f (quote (("std" "futures/std") ("heaptrace") ("default"))))))

(define-public crate-drone-core-0.12.1 (c (n "drone-core") (v "0.12.1") (d (list (d (n "drone-core-macros") (r "= 0.12.1") (d #t) (k 0)) (d (n "drone-ctypes") (r "= 0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)))) (h "1mxdbcx2hjc62mnl76zx4mzld6fi9hk06ikla2wblpyf9sr3lwy9") (f (quote (("std" "futures/std") ("heaptrace") ("default"))))))

(define-public crate-drone-core-0.13.0 (c (n "drone-core") (v "0.13.0") (d (list (d (n "drone-core-macros") (r "=0.13.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "=0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0nycisd5aw8bzks416ml12s5b6h550vi82swy6498kibdd1a3b2a") (f (quote (("std" "futures/std") ("heaptrace") ("default"))))))

(define-public crate-drone-core-0.14.0 (c (n "drone-core") (v "0.14.0") (d (list (d (n "drone-core-macros") (r "=0.14.0") (d #t) (k 0)) (d (n "drone-ctypes") (r "=0.14.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0y8751fbqpnxzmypxkcyd3smvkfgp6az0ipgbd3n85cqhn0sfhgg") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.14.1 (c (n "drone-core") (v "0.14.1") (d (list (d (n "drone-core-macros") (r "=0.14.1") (d #t) (k 0)) (d (n "drone-ctypes") (r "=0.14.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0n76vfl0wvwb2gpqa798fd5gm4gsgmb5qp9znrqa5cki0wv3yan6") (f (quote (("std" "futures/std") ("default")))) (y #t)))

(define-public crate-drone-core-0.14.2 (c (n "drone-core") (v "0.14.2") (d (list (d (n "drone-core-macros") (r "=0.14.2") (d #t) (k 0)) (d (n "drone-ctypes") (r "=0.14.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0lgb1xka6ymskzi6m7y8462xi4zh9bgpm7b0mx27bcykyqmfclqz") (f (quote (("std" "futures/std") ("default"))))))

(define-public crate-drone-core-0.14.3 (c (n "drone-core") (v "0.14.3") (d (list (d (n "drone-core-macros") (r "=0.14.3") (d #t) (k 0)) (d (n "drone-ctypes") (r "=0.14.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("alloc" "async-await"))) (k 0)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0jidy6k9bz0f85zvxn120lcsz8shc8ldamgayqh50i573plc858q") (f (quote (("std" "futures/std") ("default"))))))

