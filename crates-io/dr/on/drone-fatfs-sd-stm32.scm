(define-module (crates-io dr on drone-fatfs-sd-stm32) #:use-module (crates-io))

(define-public crate-drone-fatfs-sd-stm32-0.1.0 (c (n "drone-fatfs-sd-stm32") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "drone-core") (r "^0.8") (d #t) (k 0)) (d (n "drone-fatfs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-fatfs-sd-core") (r "^0.1.0") (d #t) (k 0)) (d (n "drone-sd-core") (r "^0.1") (d #t) (k 0)) (d (n "drone-sd-stm32") (r "^0.1") (d #t) (k 0)) (d (n "drone-stm32") (r "^0.8") (d #t) (k 0)))) (h "17yp1b5sxkzxxy5hiqv2nszh977nry60wjspc8ylsm119fh6s9ky")))

