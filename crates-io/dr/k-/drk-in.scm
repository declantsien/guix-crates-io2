(define-module (crates-io dr k- drk-in) #:use-module (crates-io))

(define-public crate-drk-in-1.0.0 (c (n "drk-in") (v "1.0.0") (h "16c7cn6s5qj4a3fx7kq6n94wrmfn261rxh84d68hzjv0dw6mzgdr")))

(define-public crate-drk-in-1.0.1 (c (n "drk-in") (v "1.0.1") (h "13llz1kv9sg4sqvny66ppnp3qqsly2a7x9m9gk8kaycbh5hs5zyc")))

(define-public crate-drk-in-1.0.2 (c (n "drk-in") (v "1.0.2") (h "1kkhlgk1jrfl9fyvnkifjd86nrhj2k592gfr4lv5354cf23273i4")))

(define-public crate-drk-in-1.0.3 (c (n "drk-in") (v "1.0.3") (h "0xapwzh4whp83x3q0n5bcxa27f2v809c4m5nwhpfn3m9kdr45sci")))

(define-public crate-drk-in-1.0.4 (c (n "drk-in") (v "1.0.4") (h "16hilhkgijxzk4r4b8mb8r4w0bag5didr0ivs5xm8i793b1bdh0s")))

(define-public crate-drk-in-1.1.0 (c (n "drk-in") (v "1.1.0") (h "1lh26yh10wdzir3gpv72kr0r5gb037bkd5anq6k73dly3ffh6i4n")))

(define-public crate-drk-in-1.1.1 (c (n "drk-in") (v "1.1.1") (h "0dlhii948r40j8fj403g5z5xlk11l20jd80f346pnyvh9gkkzrm6")))

(define-public crate-drk-in-1.1.2 (c (n "drk-in") (v "1.1.2") (h "012kwb70pvk5iaarv9vxbp3rspkrlbvf2fbqrx17rh00ah8qqq3h")))

(define-public crate-drk-in-1.1.3 (c (n "drk-in") (v "1.1.3") (h "0if5ppy767iyvm8zvbbwzr8b06fvmw74bihjfz3ii6nmyxj1wy9y")))

(define-public crate-drk-in-1.1.4 (c (n "drk-in") (v "1.1.4") (h "1pgv9ah4y4qsvy1fpwzxrpj1mcf9qcyp50r74qk91m573bnx3f29")))

