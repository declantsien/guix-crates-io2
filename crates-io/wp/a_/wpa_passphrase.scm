(define-module (crates-io wp a_ wpa_passphrase) #:use-module (crates-io))

(define-public crate-wpa_passphrase-0.1.0 (c (n "wpa_passphrase") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11.0") (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "1b16ilb7cw27szr58zx5p308j4cz9b9yl57bp7snlkb3ry6z9csx")))

(define-public crate-wpa_passphrase-0.2.0 (c (n "wpa_passphrase") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11.0") (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)))) (h "079b1xv69c391vv90wy9azpi0cdc05gw44fr5wnlanxg30wq367g")))

(define-public crate-wpa_passphrase-0.3.0 (c (n "wpa_passphrase") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1rsn5n8n956rf4qsrvpigva4hpbnqcniqim0716yhb4r0mfhx85g")))

