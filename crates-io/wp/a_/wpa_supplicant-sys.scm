(define-module (crates-io wp a_ wpa_supplicant-sys) #:use-module (crates-io))

(define-public crate-wpa_supplicant-sys-0.1.0 (c (n "wpa_supplicant-sys") (v "0.1.0") (h "1flsqjbbkxdhyjjlqd5zam98424w1a0yn8m1brara505r382yakz") (y #t)))

(define-public crate-wpa_supplicant-sys-0.1.1 (c (n "wpa_supplicant-sys") (v "0.1.1") (h "0bvnj0crslr0rfl2x1x3gayq8rs6y22gdh94hy2navxw654hjj1i") (y #t)))

