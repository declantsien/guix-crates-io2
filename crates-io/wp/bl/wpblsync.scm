(define-module (crates-io wp bl wpblsync) #:use-module (crates-io))

(define-public crate-wpblsync-0.1.0 (c (n "wpblsync") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yv2bbmj57pl6mmfvflvi454xndgwd534q8klizkfha8ll3k9g2v")))

