(define-module (crates-io wp sl wpslugify) #:use-module (crates-io))

(define-public crate-wpslugify-0.1.0 (c (n "wpslugify") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1d1dzrnaxm7s6vh5dzbx19r25n1xdvwhwk2s8ic9n8l97vym7v7x")))

