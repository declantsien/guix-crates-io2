(define-module (crates-io wp ap wpaperctl) #:use-module (crates-io))

(define-public crate-wpaperctl-1.0.0 (c (n "wpaperctl") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive" "cargo"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.20") (d #t) (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "wpaperd-ipc") (r "^1.0.0") (d #t) (k 0)))) (h "1r0nmkznkbxpzrc178kqhkddh6qqlx90vhbhgika57kfs6y7m1za")))

