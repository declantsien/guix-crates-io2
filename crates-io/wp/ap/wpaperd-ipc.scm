(define-module (crates-io wp ap wpaperd-ipc) #:use-module (crates-io))

(define-public crate-wpaperd-ipc-1.0.0 (c (n "wpaperd-ipc") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "085s45iygwhlcq4840nxbq7pan8agndxdp8ld3kzwabcsaldwsfw")))

