(define-module (crates-io wp a- wpa-ctrl) #:use-module (crates-io))

(define-public crate-wpa-ctrl-0.1.0 (c (n "wpa-ctrl") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "0ipq4rwk8jd8ix29dq52amfp0x41cymnric7jk7g1i09nscfazh9")))

(define-public crate-wpa-ctrl-0.1.1 (c (n "wpa-ctrl") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "1kjf96dln1cranakj6w4zwsmf8qndi0z6b3rr665y39avjc8n35k") (y #t)))

(define-public crate-wpa-ctrl-0.1.2 (c (n "wpa-ctrl") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "0rszwhz17fgxx6glghcjbfp7g26bjv2s77v9z4d10n8f6qv9qqzw")))

(define-public crate-wpa-ctrl-0.1.3 (c (n "wpa-ctrl") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "1r6flp8mdgj1sqxg944vl88zbn3yvdjxmq6zaw1yf1zq3gr29hcr")))

(define-public crate-wpa-ctrl-0.2.0 (c (n "wpa-ctrl") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "06wjyb9vpyzcb9icqgzrr7mzay2w1fi6i71qlx24l4yqdsns8b0n")))

(define-public crate-wpa-ctrl-0.2.1 (c (n "wpa-ctrl") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "str-buf") (r "^3") (d #t) (k 0)))) (h "1czjchr8fkdk86w7v0h9baf9vyzn4iyndq57l6g4ljz24y2agg1q")))

