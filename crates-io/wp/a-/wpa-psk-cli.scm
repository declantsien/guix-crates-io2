(define-module (crates-io wp a- wpa-psk-cli) #:use-module (crates-io))

(define-public crate-wpa-psk-cli-0.1.0 (c (n "wpa-psk-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wpa-psk") (r "^0.2.0") (d #t) (k 0)))) (h "1si0zng9n57dv5c48w3y8hhjn0x70dvsmjmr8xd1cbjfz72hqs0g")))

(define-public crate-wpa-psk-cli-0.1.1 (c (n "wpa-psk-cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "wpa-psk") (r "^0.2.0") (d #t) (k 0)))) (h "09y4g2ras93py5igbiyg0jill6njh1yz7xkk869fkh6b3b66gk2h")))

(define-public crate-wpa-psk-cli-0.1.2 (c (n "wpa-psk-cli") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "wpa-psk") (r "^0.2.1") (d #t) (k 0)))) (h "1bpqppvlagm03f7x6rzdgi8fjsrag15bx2mxg989fv0qgwsyz5l3")))

(define-public crate-wpa-psk-cli-0.1.3 (c (n "wpa-psk-cli") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.2.7") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "pledge") (r "^0.4.2") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "wpa-psk") (r "^0.2.2") (d #t) (k 0)))) (h "1ikjykybx3swri9yy5nv58vrhnl6054zf4wghpcrnkrz2cssm7jg")))

(define-public crate-wpa-psk-cli-0.1.4 (c (n "wpa-psk-cli") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pledge") (r "^0.4.2") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "wpa-psk") (r "^0.2.2") (d #t) (k 0)))) (h "1y0ch0b2z1dh6r2an1bgjpg47876grakkb0f7143q81d3zrygrds")))

(define-public crate-wpa-psk-cli-0.1.5 (c (n "wpa-psk-cli") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pledge") (r "^0.4.2") (d #t) (t "cfg(target_os = \"openbsd\")") (k 0)) (d (n "wpa-psk") (r "^0.2.3") (d #t) (k 0)))) (h "0imk785jak2zsfv8q85gcg0zyll63g789adm5z65dgh9wv1nbz8r")))

