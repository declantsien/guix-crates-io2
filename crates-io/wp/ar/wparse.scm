(define-module (crates-io wp ar wparse) #:use-module (crates-io))

(define-public crate-wparse-0.2.0 (c (n "wparse") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ycdgdqc8bsq8j530fssbkzhi5444mbsip3hf9a56b5jid8rc393")))

(define-public crate-wparse-0.2.1 (c (n "wparse") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zlwb62i9587pglq7r70m5ad5sb2rdc87rri5ai5jjl574lx1hg0")))

