(define-module (crates-io wp re wpress-oxide) #:use-module (crates-io))

(define-public crate-wpress-oxide-1.1.0 (c (n "wpress-oxide") (v "1.1.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)))) (h "0dpri5i9gb9i8v2wlczmpbjimjagqi40qg0ywi8l63bp2sl5irnk")))

(define-public crate-wpress-oxide-1.1.1 (c (n "wpress-oxide") (v "1.1.1") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)))) (h "09im14kg6gchpac8w5qri3m8gbh94xjly8q8hlq772jzavqiw36b")))

(define-public crate-wpress-oxide-2.0.0 (c (n "wpress-oxide") (v "2.0.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0g4i9x9gifwh0kpahqmkh5w51ccm95m3r4ph3r3hdzvm4g2r15wg")))

(define-public crate-wpress-oxide-2.1.0 (c (n "wpress-oxide") (v "2.1.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ll4ri1ncvcndb59gfbd1j8z3gv70l7krg5j7dppynaxxlcnrhf5")))

(define-public crate-wpress-oxide-3.2.0 (c (n "wpress-oxide") (v "3.2.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1dsd22q8sxd3n7hh0qhj2p1a2jq8f129lzcp364mvi2mr0d0nc85")))

