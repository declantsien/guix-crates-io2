(define-module (crates-io wp _q wp_query_rs) #:use-module (crates-io))

(define-public crate-wp_query_rs-0.1.0 (c (n "wp_query_rs") (v "0.1.0") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "1p2ww6bylx1na6gswkr4byzfh7xzb6zqiyhr8kjxlyi1pvjyaaac") (y #t)))

(define-public crate-wp_query_rs-0.1.1 (c (n "wp_query_rs") (v "0.1.1") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "03l4nghkb4j2pn21fy7xxaqhq532y5p3k5bdz64d0iax6xhdgpn6") (y #t)))

(define-public crate-wp_query_rs-0.1.2 (c (n "wp_query_rs") (v "0.1.2") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0rvf3vxl4lhbvfvgjnr0wy9856drl0mivpyx2ca9gazg8pd1wnzp") (y #t)))

(define-public crate-wp_query_rs-0.1.3 (c (n "wp_query_rs") (v "0.1.3") (d (list (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "1s8q8y5ssk7ysa74h601sb3shcw7q2kyngqhhqxx344rh2nqml5i") (y #t)))

(define-public crate-wp_query_rs-0.1.4 (c (n "wp_query_rs") (v "0.1.4") (d (list (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0rpln9lhw1m7gc619j61sdipa42cad4vv7msqk83lv2swxjlbl7l") (y #t)))

(define-public crate-wp_query_rs-0.1.5 (c (n "wp_query_rs") (v "0.1.5") (d (list (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "13ggc6iqb6z2bphnj3z7f8j299wa1qqjhfnk86pw3mz2qnpbz2p7")))

(define-public crate-wp_query_rs-0.2.0 (c (n "wp_query_rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0bpwzfgnryv4dav54wf9rvj5gxwzvim5ni394hvwa64is5mzdnzc")))

(define-public crate-wp_query_rs-0.2.1 (c (n "wp_query_rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "11y4r75n34f1ndj6d4xiclkyb5j4pl994mz15rcpq4j0gyqbp8bm")))

(define-public crate-wp_query_rs-0.2.2 (c (n "wp_query_rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "01izfj8wbfr7dh0ds977g0lra4j9k6sdvj4b3lhq10ayiidlvh1p")))

(define-public crate-wp_query_rs-0.2.3 (c (n "wp_query_rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "10vxq30ma9yxi01ywrcx523xz5dmhx45wzzw7v93qcv5yiam76sp")))

(define-public crate-wp_query_rs-0.2.4 (c (n "wp_query_rs") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0n63ahaqsxsnaqy5y7wcndln9d7fly3385nnq1db18hdh5vzgq1k")))

(define-public crate-wp_query_rs-0.2.5 (c (n "wp_query_rs") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0gykwdhfrf78yaqcz45m939i9x53vs5r86k91sh8qi2xksdxb4id")))

(define-public crate-wp_query_rs-0.3.0 (c (n "wp_query_rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "04aryjg89i49cv2s3ig3pl5a1p4n5b7s2r68g6g5rak02b74zjj8")))

(define-public crate-wp_query_rs-0.3.1 (c (n "wp_query_rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "03yhyyhmj4nrzn259sw9sdmhamkq2wcw0cn3vbimzv0d642kl83a")))

(define-public crate-wp_query_rs-0.4.0 (c (n "wp_query_rs") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "005whqp32gfwsm657fvkl48024v6ljvdl4p6ap4dhddcm52mszln")))

(define-public crate-wp_query_rs-0.4.1 (c (n "wp_query_rs") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "ext-php-rs") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "sql_paginatorr") (r "^0.1.1") (d #t) (k 0)))) (h "0bdm4gv7ax4wvijpvakdipv233qkpy8f038x36ynamibnmbpnhnv") (f (quote (("rustls" "mysql/default-rustls") ("php" "ext-php-rs") ("native-tls" "mysql/native-tls"))))))

