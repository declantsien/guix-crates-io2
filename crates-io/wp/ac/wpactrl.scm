(define-module (crates-io wp ac wpactrl) #:use-module (crates-io))

(define-public crate-wpactrl-0.1.0 (c (n "wpactrl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1c3xzdnwp741l24zsww4fa317g0mzny1dv8gpx4ppz5361h11imj")))

(define-public crate-wpactrl-0.1.1 (c (n "wpactrl") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cfj5nsadqylkh5kni90ij1yxhnha2wnmldrcx5hj4yisbn2ss7c")))

(define-public crate-wpactrl-0.2.0 (c (n "wpactrl") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m8hh3fzi7bdal71f2xh14jwwmxkx5l5jx141ybai2kwswf9ihsd")))

(define-public crate-wpactrl-0.2.1 (c (n "wpactrl") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f3rj2c66ql24zqix5rs1pg4sm8ml1dc2anr6vj7mnc3palaz66p")))

(define-public crate-wpactrl-0.3.0 (c (n "wpactrl") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0msvfx3m5vrgx2xp5hkcvi1cydng5nb3cpjm9z3n696fn8x9xm8q")))

(define-public crate-wpactrl-0.3.1 (c (n "wpactrl") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "04swcf3afgrhbj7pcxkkcyfqc8a8rf2a59x3kjm78yfnhh5ar1zl")))

(define-public crate-wpactrl-0.4.0 (c (n "wpactrl") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.101") (k 0)) (d (n "log") (r "^0.4.14") (k 0)))) (h "00q36izr8qhvbbcdrbvpdgl664jnkvvpi11r591r2w2vl0vhsz4m")))

(define-public crate-wpactrl-0.5.0 (c (n "wpactrl") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.122") (k 0)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "serial_test") (r "^0.6.0") (k 2)))) (h "0777zch7blq0sy2xs2k3bcf3sr0j37wma402l2dajw5wzqk74x8b")))

(define-public crate-wpactrl-0.5.1 (c (n "wpactrl") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.122") (k 0)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "serial_test") (r "^0.6.0") (k 2)))) (h "0a70myr3r2yvyjvv62gvpgqn631yfjpw34v2drss2x2kjplkzvcg")))

