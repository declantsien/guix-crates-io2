(define-module (crates-io wp il wpilib) #:use-module (crates-io))

(define-public crate-wpilib-0.1.0 (c (n "wpilib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)))) (h "0xyvss1na1lz2qaczk1kl79p1lbxbjd35jd4zcbfa3azgvbp7q74")))

(define-public crate-wpilib-0.0.0 (c (n "wpilib") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1na8imzb6v7zljwzkvfhjwbfnm5sa9m2qv9w6x1k3visnrz70s79") (f (quote (("dev")))) (y #t)))

(define-public crate-wpilib-0.2.0 (c (n "wpilib") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1xgm1y1lq8vpd17bdrbsv1czgcc615zw7sqw31jjlpxkkhnawvzv") (f (quote (("dev"))))))

(define-public crate-wpilib-0.2.1 (c (n "wpilib") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0b4gyg0r3srg4nr8bx6f22s13b2f6zhllazafr5xkz0vps1ihzs9") (f (quote (("dev"))))))

(define-public crate-wpilib-0.2.2 (c (n "wpilib") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "01wl7zgklhgxvj9y30shclvqkz8b3d5cc3dy9j91wc6bz9racf3d") (f (quote (("dev"))))))

(define-public crate-wpilib-0.2.3 (c (n "wpilib") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "wpilib-sys") (r "^0.2.3") (d #t) (k 0)))) (h "1hy7dlpi57rw2l481rs0s278wsc6a6fwkbs93ym39j84phg3yyh7") (f (quote (("dev"))))))

(define-public crate-wpilib-0.3.0 (c (n "wpilib") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "wpilib-sys") (r "^0.3.0") (d #t) (k 0)))) (h "030rmzf33f5y2m40y5k1jy5kfwbgvfcc53svm0mdrwjwjlmxxaqf") (f (quote (("dev"))))))

(define-public crate-wpilib-0.3.1 (c (n "wpilib") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "wpilib-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0hi9qwhmhiv6rlybkpfs3mndhzxbd3n7sm2wcjxk27wrsrk4gi75") (f (quote (("dev"))))))

(define-public crate-wpilib-0.4.0 (c (n "wpilib") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "wpilib-sys") (r "^0.4.0") (d #t) (k 0)))) (h "02q7d2iy53b11cm63wiy5xhc4qlkzxa4ranfqf64sqnjhym8mp8j") (f (quote (("dev"))))))

