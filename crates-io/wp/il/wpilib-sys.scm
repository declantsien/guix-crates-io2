(define-module (crates-io wp il wpilib-sys) #:use-module (crates-io))

(define-public crate-wpilib-sys-0.1.0 (c (n "wpilib-sys") (v "0.1.0") (h "178ysj21zpfqhsq9k2h54q99c49ymbrjlx6jxy5zw5b66g9wfcbp")))

(define-public crate-wpilib-sys-0.2.3 (c (n "wpilib-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)))) (h "1lih5ag8fv57ykdi6aird12nkjyg7gbacaddplal10yhlfwxpl7d") (f (quote (("dev"))))))

(define-public crate-wpilib-sys-0.3.0 (c (n "wpilib-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)))) (h "0c66cbi2k5mnqzwnap9hl6yyknf4crv5zy22yd58k7x1ncp5sbgb") (f (quote (("dev"))))))

(define-public crate-wpilib-sys-0.3.1 (c (n "wpilib-sys") (v "0.3.1") (h "11gx0iw1j3p1c0p87v30n56b2zywlx9ncvlxrrakxwjkafp8ksaw") (f (quote (("dev"))))))

(define-public crate-wpilib-sys-0.4.0 (c (n "wpilib-sys") (v "0.4.0") (h "1w7xb465wa3kwq08g0qcgs1l3bg52asryfhqbp8nh6drqma83hma") (f (quote (("dev"))))))

