(define-module (crates-io wp il wpilib-hal) #:use-module (crates-io))

(define-public crate-wpilib-hal-0.1.1 (c (n "wpilib-hal") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "clippy") (r "^0.0.114") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "085nm5wgc5w8slc18y7ff2nf2xvry6nnlhkvmpawhbpdsvyfi6xh")))

