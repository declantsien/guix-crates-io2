(define-module (crates-io wp il wpilog) #:use-module (crates-io))

(define-public crate-wpilog-0.1.2 (c (n "wpilog") (v "0.1.2") (d (list (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "frc-value") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "single_value_channel") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)))) (h "0za9kjn4ydxcr0zhzanzlpy7svbs8z6fz4zkqvzcpvrxsbfd0r08") (f (quote (("default" "tracing")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

