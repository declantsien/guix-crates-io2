(define-module (crates-io wp t- wpt-interop) #:use-module (crates-io))

(define-public crate-wpt-interop-0.1.0 (c (n "wpt-interop") (v "0.1.0") (h "0glsgx87nq4q9giy4ykn9jlhwrigw3ck8wilp66061ll8dq03yh2")))

(define-public crate-wpt-interop-0.2.0 (c (n "wpt-interop") (v "0.2.0") (d (list (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1lfb9r5imnh7qsrmb3i21w5dr81i9c61f0jy6f1k0qaspp3fh64b")))

