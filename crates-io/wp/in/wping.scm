(define-module (crates-io wp in wping) #:use-module (crates-io))

(define-public crate-wping-0.2.0 (c (n "wping") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "pnet") (r "^0.16.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.1.1") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "0ngz35gn1k22ib6nq284w8ks6fxgg4pxc16p06ckfc3v5qiwx0k9")))

(define-public crate-wping-0.3.0 (c (n "wping") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "pnet") (r "^0.16.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.1.1") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "09r2yygiqiynapgl9nf08gayiajy74fw3n7bfvdlflwnrhmx42qf")))

