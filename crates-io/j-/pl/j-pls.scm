(define-module (crates-io j- pl j-pls) #:use-module (crates-io))

(define-public crate-j-pls-0.1.0 (c (n "j-pls") (v "0.1.0") (h "039bkbdznv6861gl08vwivkv76nfkfsq9llk5c57g3c9535iy9pl")))

(define-public crate-j-pls-0.1.1 (c (n "j-pls") (v "0.1.1") (d (list (d (n "owned_chars") (r "^0.3.2") (d #t) (k 0)))) (h "18x19kzwm4rwyk4j8sq6a974vab5j1kvaqd67w3xb873xss8w7xs")))

(define-public crate-j-pls-0.1.2 (c (n "j-pls") (v "0.1.2") (h "1rn4kj58q5yya6f0i9br717mbyn2222ng2lcg7zx55gqfhvqk4wm")))

(define-public crate-j-pls-0.1.3 (c (n "j-pls") (v "0.1.3") (h "1lywcg2ablvfr4ln8d0ribfksdsdmd9jfa9cs9smp79404b151bm")))

(define-public crate-j-pls-0.2.0 (c (n "j-pls") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1ic7gc43n8jrpw30hkiv9jgxf3ivjx7mwwwny9yxyi7pv3wnl3wp")))

(define-public crate-j-pls-0.2.1 (c (n "j-pls") (v "0.2.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1hq4lz5ala00qsw0d6xm59z2b1my28nph93d5fgazn54jsci4r1z")))

(define-public crate-j-pls-0.2.2 (c (n "j-pls") (v "0.2.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1b4kkj5h5ch1y6k3551g6zi1q0vkv0zqv8ziqdlxslka7ma2ifk0")))

(define-public crate-j-pls-0.2.3 (c (n "j-pls") (v "0.2.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "08jrd8q4z8jfhhlhzb1vdzf0gmfgjc8plvg6jwdqxsad518bdhva")))

(define-public crate-j-pls-0.3.0 (c (n "j-pls") (v "0.3.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "19l7pi2nj6f5jb02wq2b9pgbcx4g7hj663cz9bra8phkxb4rgwgc")))

(define-public crate-j-pls-0.4.0 (c (n "j-pls") (v "0.4.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0gbfqlcd57ymzmh4n6f3k630jyj3zac28p74wh7r11vp3rylznbx")))

(define-public crate-j-pls-0.4.1 (c (n "j-pls") (v "0.4.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1xpp9sysqr87707izmmxcghs351b5b25v8fza3c757h9297wbdqk")))

(define-public crate-j-pls-0.4.2 (c (n "j-pls") (v "0.4.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "global-static") (r "^0.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0w9hk8x42c5za395yb13pjcq4jjn76yynq9x6p4b16c6v3yhk94b")))

