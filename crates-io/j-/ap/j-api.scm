(define-module (crates-io j- ap j-api) #:use-module (crates-io))

(define-public crate-j-api-0.1.0 (c (n "j-api") (v "0.1.0") (d (list (d (n "j-api-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "198ys70nc9az2wlih315cp2xm8nbqn41vlpabl6248ykyagmlb9f") (f (quote (("derive" "j-api-derive"))))))

