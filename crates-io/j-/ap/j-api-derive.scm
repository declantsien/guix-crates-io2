(define-module (crates-io j- ap j-api-derive) #:use-module (crates-io))

(define-public crate-j-api-derive-0.1.0 (c (n "j-api-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nyw5vd26xahwzb08pdwqzjmsqv4bzyxxm16bd0ar2f1khh13cdx")))

