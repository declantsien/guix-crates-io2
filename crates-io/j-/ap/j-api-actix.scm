(define-module (crates-io j- ap j-api-actix) #:use-module (crates-io))

(define-public crate-j-api-actix-0.1.0 (c (n "j-api-actix") (v "0.1.0") (d (list (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "j-api") (r "^0.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18xd10fkg8z5sasprbbs84jas8cl1qfqpf6vn9c8jzbq1jv53iqc")))

