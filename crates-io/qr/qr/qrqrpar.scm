(define-module (crates-io qr qr qrqrpar) #:use-module (crates-io))

(define-public crate-qrqrpar-0.1.0 (c (n "qrqrpar") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0pjf6i0a66v11h90kjn04g3jrky0jy72kzw77x96llbc41hnk57w")))

(define-public crate-qrqrpar-0.1.2 (c (n "qrqrpar") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0mkw8znqq3cvd5fd7jlmjrakgz67nlam8hia43lq9fmgi7c4vh0x")))

(define-public crate-qrqrpar-0.1.3 (c (n "qrqrpar") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "051kw1fsj78hxm28yxapldmfmhx084ja8mk4zmickmgk0y7x1igs")))

(define-public crate-qrqrpar-0.1.4 (c (n "qrqrpar") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "027syc5hi66afdyybjs43q9cvm9l1lr59i4z8dy4ayf90kgv0iyh")))

(define-public crate-qrqrpar-0.1.5 (c (n "qrqrpar") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1zplgrlhva259zsjbafsn9jd0yb266ps9jpyl7k476fk62d68a1g")))

