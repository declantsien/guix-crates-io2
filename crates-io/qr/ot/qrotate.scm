(define-module (crates-io qr ot qrotate) #:use-module (crates-io))

(define-public crate-qrotate-0.2.0 (c (n "qrotate") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "numpy") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02qaw06j8zjbdi8q8blbbxxw9p636xbd695gndscm2vlbs5mrhyb") (f (quote (("default" "ndarray")))) (s 2) (e (quote (("python" "dep:ndarray" "dep:numpy" "dep:pyo3") ("ndarray" "dep:ndarray"))))))

