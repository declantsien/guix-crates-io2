(define-module (crates-io qr _c qr_cli) #:use-module (crates-io))

(define-public crate-qr_cli-0.1.0 (c (n "qr_cli") (v "0.1.0") (d (list (d (n "checked_int_cast") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (d #t) (k 0)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "1vm35wp4rp53xdv5bl2fm7rw5a0sgq8n4w20dgkzla5f2i2dzwmh")))

