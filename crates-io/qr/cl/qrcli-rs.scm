(define-module (crates-io qr cl qrcli-rs) #:use-module (crates-io))

(define-public crate-qrcli-rs-0.1.0 (c (n "qrcli-rs") (v "0.1.0") (d (list (d (n "qrcodegen") (r "^1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)))) (h "1wl3vyjch2768cly7jnlbi2hdmfhan5fjz2dqiyjvwdxn9nqcy6v")))

(define-public crate-qrcli-rs-0.2.0 (c (n "qrcli-rs") (v "0.2.0") (d (list (d (n "qrcodegen") (r "^1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)))) (h "1bw7rc6cq4386m9ra4ymdg60bnddlfm8qfsaqwzyd7c7aaq45phi")))

(define-public crate-qrcli-rs-0.2.1 (c (n "qrcli-rs") (v "0.2.1") (d (list (d (n "qrcodegen") (r "^1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)))) (h "056j2yk1ylrz47hdlbqwvccvscd00jvqgbcdr6rwbdcd73r3172r")))

(define-public crate-qrcli-rs-0.3.0 (c (n "qrcli-rs") (v "0.3.0") (d (list (d (n "qrcodegen") (r "^1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)))) (h "1k9fqqgq26rii243q5r16yydq2kk5f5c89sss8g3c2lqw8jk7h73")))

