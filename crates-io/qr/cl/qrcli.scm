(define-module (crates-io qr cl qrcli) #:use-module (crates-io))

(define-public crate-qrcli-1.0.0 (c (n "qrcli") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "qrcode-png") (r "^0.4.1") (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "1piyv5yyh7xigpxx0j0dhyfrvlgsyy4s1qavj5691zjg13bwch4w") (r "1.68")))

(define-public crate-qrcli-1.0.1 (c (n "qrcli") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "qrcode-png") (r "^0.4.1") (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "0k0x77q64zssk8v1mbc8v8wmpr1477ijqmsh6gf495s6piwnk4wl") (r "1.68")))

(define-public crate-qrcli-1.0.2 (c (n "qrcli") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "qrcode-png") (r "^0.4.1") (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "0rwrlrysl48p57vn25qw2q4a62vq2a0x3dpclpx2v5jhq5ahcb2g") (r "1.68")))

