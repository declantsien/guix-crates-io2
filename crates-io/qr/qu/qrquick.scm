(define-module (crates-io qr qu qrquick) #:use-module (crates-io))

(define-public crate-qrquick-0.1.0 (c (n "qrquick") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.7.0") (d #t) (k 0)))) (h "0nvnwgsglzp22yzm7z48fr6vsmpk25ppz7ja70pk1ls8w0zb2623")))

