(define-module (crates-io qr wc qrwcell) #:use-module (crates-io))

(define-public crate-qrwcell-0.1.0 (c (n "qrwcell") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "05pwzn9q9n3d3wqwh8hx2vkz907svqpvnzimghvs56ix65ss3a7h")))

(define-public crate-qrwcell-0.2.0 (c (n "qrwcell") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "15hlgq4ngzv1ysyd2bfnx2rzhzsnkhpv0pdz1klfnbbbmxf4p0h7")))

