(define-module (crates-io qr _t qr_terminal) #:use-module (crates-io))

(define-public crate-qr_terminal-0.0.1 (c (n "qr_terminal") (v "0.0.1") (d (list (d (n "qrcode") (r "^0.11") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)))) (h "1b147wkflbycy4y9p2dmxs83jwjlm96x3709n859jrzpczi7xnl8")))

