(define-module (crates-io qr s_ qrs_detector) #:use-module (crates-io))

(define-public crate-qrs_detector-0.1.0 (c (n "qrs_detector") (v "0.1.0") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)) (d (n "sliding_window") (r "^0.1.0") (d #t) (k 0)))) (h "13z8v43smsqrfbrid4draq4imgvbjv12wzifsqg0hn1g5543khyn")))

(define-public crate-qrs_detector-0.1.1 (c (n "qrs_detector") (v "0.1.1") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)) (d (n "sliding_window") (r "^0.1.0") (d #t) (k 0)))) (h "1pjhc95lvjj12gx8i8bgdpzrx2kksss7k5fr9k1700mfss11f5af")))

(define-public crate-qrs_detector-0.1.2 (c (n "qrs_detector") (v "0.1.2") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "micromath") (r "^1.0.0") (d #t) (k 0)) (d (n "sliding_window") (r "^0.1.0") (d #t) (k 0)))) (h "00yc6bqrhacfk7ar3b63bm9qjzkwrf5zdy9l4a6mg7kzqa518r5l")))

(define-public crate-qrs_detector-0.2.0 (c (n "qrs_detector") (v "0.2.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "micromath") (r "^1.0.0") (d #t) (k 0)) (d (n "sliding_window") (r "^0.1.0") (d #t) (k 0)))) (h "06pdzh5lw2wbp2hr4w3v54nzs2nckdrdapkc2y0l38fclyy6h8gn")))

