(define-module (crates-io qr po qrport) #:use-module (crates-io))

(define-public crate-qrport-0.1.0 (c (n "qrport") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.7") (d #t) (k 0)))) (h "1yxbz7pnhs6m501vdrd2aa77frq42ij06fx2w3klvyc73xvwcdpx")))

