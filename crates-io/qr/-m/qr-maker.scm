(define-module (crates-io qr -m qr-maker) #:use-module (crates-io))

(define-public crate-qr-maker-0.1.0 (c (n "qr-maker") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.4") (d #t) (k 0)))) (h "0nymy7js5wqbajgf0zlvp5nyaicy8706qpz105wbvyrlgfx98vj6")))

(define-public crate-qr-maker-0.1.1 (c (n "qr-maker") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.4") (d #t) (k 0)))) (h "1i4a63m2b4f365jrv6w85lkqvsxz7hjjkjj2hhj1gswd85g0kn8s")))

