(define-module (crates-io qr -r qr-reader) #:use-module (crates-io))

(define-public crate-qr-reader-0.1.0 (c (n "qr-reader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "0jnr60s142ifzjf5zmanqp4h8n1cli2aii6sqpp0f2wngcyrxka7")))

(define-public crate-qr-reader-0.2.0 (c (n "qr-reader") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "1lw2dkg1hjqbnb2lw31a8cp4sfnj3mq103lgg921lrkj1iwplvjn")))

