(define-module (crates-io qr pc qrpc-broker) #:use-module (crates-io))

(define-public crate-qrpc-broker-0.1.0 (c (n "qrpc-broker") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13") (d #t) (k 0)) (d (n "identity") (r "^0.6.0") (f (quote ("random"))) (d #t) (k 0) (p "ratman-identity")) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "qrpc-sdk") (r "^0.1") (f (quote ("internals"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (f (quote ("unix"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08v4w638nan3b4fa3mkg60d5n02hdfaf7v9a3v6kmxlax2mihw4g")))

