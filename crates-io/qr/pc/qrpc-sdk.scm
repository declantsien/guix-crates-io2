(define-module (crates-io qr pc qrpc-sdk) #:use-module (crates-io))

(define-public crate-qrpc-sdk-0.1.0 (c (n "qrpc-sdk") (v "0.1.0") (d (list (d (n "async-std") (r "=1.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13") (d #t) (k 0)) (d (n "capnpc") (r "^0.13") (d #t) (k 1)) (d (n "identity") (r "^0.5.0") (d #t) (k 0) (p "ratman-identity")) (d (n "socket2") (r "^0.3") (f (quote ("unix"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0w2cfrj0idl2xi0h23j1cpbc275n7zi31703ssp4ml0f2b13jz66") (f (quote (("internals"))))))

(define-public crate-qrpc-sdk-0.1.1 (c (n "qrpc-sdk") (v "0.1.1") (d (list (d (n "async-std") (r "^1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "capnp") (r "^0.13") (d #t) (k 0)) (d (n "capnpc") (r "^0.13") (d #t) (k 1)) (d (n "identity") (r "^0.6.0") (f (quote ("random"))) (d #t) (k 0) (p "ratman-identity")) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ws7vflp7pxdp88k3z0jafr5qb98vws4vqwaff5s4qml3y7m77dd") (f (quote (("internals"))))))

