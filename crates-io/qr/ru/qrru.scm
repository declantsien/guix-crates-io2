(define-module (crates-io qr ru qrru) #:use-module (crates-io))

(define-public crate-qrru-0.1.0 (c (n "qrru") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "1zv4f3m6rwnbxpl476l853j7nsqfklzd6pv8dgydysr561pfghal")))

(define-public crate-qrru-0.1.1 (c (n "qrru") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "1p5kkpkjjkai2h81p125yfn1blf7s8v9ffqlbjqzg54g8lpq8ii4")))

