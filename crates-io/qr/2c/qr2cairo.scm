(define-module (crates-io qr #{2c}# qr2cairo) #:use-module (crates-io))

(define-public crate-qr2cairo-0.1.0 (c (n "qr2cairo") (v "0.1.0") (d (list (d (n "cairo-rs") (r ">= 0.1.2, < 1.0.0") (k 0)) (d (n "cairo-rs") (r ">= 0.1.2, < 1.0.0") (f (quote ("pdf"))) (d #t) (k 2)) (d (n "qrcode") (r ">= 0.4.0, < 1.0.0") (k 0)))) (h "02jkn8iq13fxf508hxgjzz5l5hd9anxllhhin6km92d7bcw3pbxx")))

