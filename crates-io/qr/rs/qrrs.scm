(define-module (crates-io qr rs qrrs) #:use-module (crates-io))

(define-public crate-qrrs-0.1.0 (c (n "qrrs") (v "0.1.0") (d (list (d (n "bardecoder") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "qrcode") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0zznpwz04283ifnn2w1vwc6gz5dll1phsl7iwijh9a9vifb48nhy")))

(define-public crate-qrrs-0.1.1 (c (n "qrrs") (v "0.1.1") (d (list (d (n "bardecoder") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "qrcode") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "09wishshrrj110bnx6jvbb0y57gzrhc092hhswy7mfa9rz0jayxk")))

(define-public crate-qrrs-0.1.2 (c (n "qrrs") (v "0.1.2") (d (list (d (n "bardecoder") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "qrcode") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0h4s0a3yjrnpnp45q7x6q95rnkbdq0sc55mxkz5qjm77wbywcv86")))

(define-public crate-qrrs-0.1.3 (c (n "qrrs") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "0v59x9812hs8gdnz2r60zfx7dcjpwc845d42ik3al652ihhzdq0b")))

(define-public crate-qrrs-0.1.4 (c (n "qrrs") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "1m40pzvhzvi3z98acyv6l2jjladzlr8c8nkcgw52aps70q0kx0ih")))

(define-public crate-qrrs-0.1.5 (c (n "qrrs") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "0a2hyjkqwc8q4xln84lk64ksb1y22qm9ip66wv897br50l15y24h")))

(define-public crate-qrrs-0.1.6 (c (n "qrrs") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "09r1p278qvcbkmd5nzvnp8sn98l66a904w0cc2pgf8rad509l5n6")))

(define-public crate-qrrs-0.1.7 (c (n "qrrs") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "0zkli7vp2mwm2nq7czjfifiar02x3sqz507i94gsq7jj6b9n63vm")))

(define-public crate-qrrs-0.1.8 (c (n "qrrs") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rqrr") (r "^0.4.0") (d #t) (k 0)))) (h "0k3yrqhw7sha6ib19p898i4wn1shnr3iggng2bmh7v5ha9sm8k1i")))

(define-public crate-qrrs-0.1.9 (c (n "qrrs") (v "0.1.9") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo" "unicode" "wrap_help"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "qrencode") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roff") (r "^0.2.1") (d #t) (k 1)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "0pai9a44hpy8yjg8w1xzwhc3ijvf512f5iil57k4b5rgpzapxhp4")))

