(define-module (crates-io qr -a qr-api) #:use-module (crates-io))

(define-public crate-qr-api-0.1.0 (c (n "qr-api") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (d #t) (k 0)) (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (f (quote ("json"))) (k 0)) (d (n "rqrr") (r "^0.3.2") (d #t) (k 0)))) (h "03hd7sxin2c3ycazchnbq0rhb71813w9l0g6gf0qakrywirlwhn5")))

