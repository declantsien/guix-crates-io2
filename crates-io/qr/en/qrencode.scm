(define-module (crates-io qr en qrencode) #:use-module (crates-io))

(define-public crate-qrencode-0.14.0 (c (n "qrencode") (v "0.14.0") (d (list (d (n "checked_int_cast") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (o #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)))) (h "1hgp0lchnp3zx79j3799nm445rvqg7x62x2x7926ky22lqhv23d6") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

