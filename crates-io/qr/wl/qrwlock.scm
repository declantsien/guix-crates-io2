(define-module (crates-io qr wl qrwlock) #:use-module (crates-io))

(define-public crate-qrwlock-0.1.0 (c (n "qrwlock") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (f (quote ("ticket_mutex"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1xr270y9ylmiy6chg5b4cz2p2wir2hiyx92bvip0cyfa5pq5ibzs")))

(define-public crate-qrwlock-0.2.0 (c (n "qrwlock") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (f (quote ("ticket_mutex" "spin_mutex"))) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1gnc3wnb7xn74s3zhkqsq2pijxk49c73bsb5nh4m3wc65grw2ybl")))

