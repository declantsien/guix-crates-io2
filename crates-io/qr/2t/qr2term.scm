(define-module (crates-io qr #{2t}# qr2term) #:use-module (crates-io))

(define-public crate-qr2term-0.1.0 (c (n "qr2term") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.6") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.9") (k 0)))) (h "0agn6acfninccsli49br4a54yx2kyxr4302c966h8kp69b9nykqd")))

(define-public crate-qr2term-0.1.1 (c (n "qr2term") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.7") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.9") (k 0)))) (h "0j8kg2m7kqn9nn2q74x8lmf6if8drr78cms4iwprcn5hg847b09m")))

(define-public crate-qr2term-0.1.2 (c (n "qr2term") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.9") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.10") (k 0)))) (h "0h27ryilykbbssl0a7cal4hf85dkjkcbamz1kzfdwd70b5m7gpn1")))

(define-public crate-qr2term-0.1.3 (c (n "qr2term") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.10") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.10") (k 0)))) (h "0ziffas1nhgmamn8rvpc0x4n3dqyr9zn2pxslgnfjnlnwjc0k2qm")))

(define-public crate-qr2term-0.1.4 (c (n "qr2term") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.10") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.11") (k 0)))) (h "0jll0a33pixxsry2l385va6j6dbfh9jgnlbqnfj10vn3s0xcrfs5")))

(define-public crate-qr2term-0.1.5 (c (n "qr2term") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.13") (f (quote ("style"))) (k 0)) (d (n "qrcode") (r "^0.11") (k 0)))) (h "09da9q0jx2zhkzzkpnzp6q4jzxf0zmwv8vmc4w7vlik9izcb9vfx")))

(define-public crate-qr2term-0.1.7 (c (n "qr2term") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.14") (k 0)) (d (n "qrcode") (r "^0.11") (k 0)))) (h "1vi69w5irjx9ra4bfhmylixp4apv2fvxq4zrr5p11gl9r9h29zn1")))

(define-public crate-qr2term-0.1.8 (c (n "qr2term") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.16") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)))) (h "0r9i5717lv419vqv0gwd9cpz55qgva688yc6r2xp5s1bxz7s41vx")))

(define-public crate-qr2term-0.2.0 (c (n "qr2term") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)))) (h "1mnn4l8z9bmnvbgh9ky6v06nmx7kcmb1mj3ndxzijqsljq47xj8i")))

(define-public crate-qr2term-0.2.1 (c (n "qr2term") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.17") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1a3zxphycjic9w0zvj746mi2d6zga58jpsv9r0b97iz5p37x83c4")))

(define-public crate-qr2term-0.2.2 (c (n "qr2term") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.19") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0d3wkcngslixdwlag2m4spjppcfi19kgimhh5p2m6hadhy74fd4n")))

(define-public crate-qr2term-0.2.3 (c (n "qr2term") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.23") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0zaddmw5mkiaz02caz5f2s1pwhbwcndvrg9f9nhf8xv4b5nxd7hx")))

(define-public crate-qr2term-0.3.0 (c (n "qr2term") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.23") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1sdldrwq2wicl1b97g4fs9fh3ix4n1fa58fyw07d68bqmy7vn2nw")))

(define-public crate-qr2term-0.3.1 (c (n "qr2term") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.25") (k 0)) (d (n "qrcode") (r "^0.12") (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 2)))) (h "0k0fd77ipyfy961089c4sphiz6pyr2vi5nbs4h24nwfdnmviwajc") (r "1.56")))

