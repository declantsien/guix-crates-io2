(define-module (crates-io qr co qrcode-cli) #:use-module (crates-io))

(define-public crate-qrcode-cli-0.2.0 (c (n "qrcode-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "viuer") (r "^0.6") (d #t) (k 0)))) (h "100l14w7x1rknakrp6b6j3mbq948ncicvp4maij55hsgbwr9182c")))

(define-public crate-qrcode-cli-0.3.0 (c (n "qrcode-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0fmlfkbyhybqxy9cb1ph24nfqwbmjj54brljxj4a8qb2s98in08k")))

(define-public crate-qrcode-cli-0.4.0 (c (n "qrcode-cli") (v "0.4.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "01mhj4iv8fcfqxhqmxwwmwakdyia79nh5931sva0ykbpcjbkvm39")))

(define-public crate-qrcode-cli-0.5.0 (c (n "qrcode-cli") (v "0.5.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "15492k2ap6i7s5x88c5h1is7088kk81z2az3l8yd33pirnrir5y7")))

