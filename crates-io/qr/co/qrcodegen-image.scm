(define-module (crates-io qr co qrcodegen-image) #:use-module (crates-io))

(define-public crate-qrcodegen-image-1.0.0 (c (n "qrcodegen-image") (v "1.0.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "qrcodegen") (r "^1.8") (d #t) (k 0)))) (h "0f8ghpwy8hjw581j7gm81ndzmnil88rp5p9xivwy0c0x259zmg0g") (s 2) (e (quote (("base64" "dep:base64")))) (r "1.61")))

(define-public crate-qrcodegen-image-1.1.0 (c (n "qrcodegen-image") (v "1.1.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "qrcodegen") (r "^1.8") (d #t) (k 0)))) (h "0qazcg9am8dy1xlyvgsx18bsnxs4maqzinh6kg2pp5vjvls6lc5m") (s 2) (e (quote (("base64" "dep:base64")))) (r "1.61")))

(define-public crate-qrcodegen-image-1.2.0 (c (n "qrcodegen-image") (v "1.2.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "qrcodegen") (r "^1.8") (d #t) (k 0)))) (h "0gpb8d9sggdwj7faszjysnhyvydiz2ay4in86hapkp96842vxzm1") (s 2) (e (quote (("base64" "dep:base64")))) (r "1.61")))

(define-public crate-qrcodegen-image-1.3.0 (c (n "qrcodegen-image") (v "1.3.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "qrcodegen") (r "^1.8") (d #t) (k 0)))) (h "17x96aa9fnbq41pp6cjn6zchhkxnhbwpq4gc6s8092qbyg5hm6na") (s 2) (e (quote (("base64" "dep:base64")))) (r "1.61")))

(define-public crate-qrcodegen-image-1.4.0 (c (n "qrcodegen-image") (v "1.4.0") (d (list (d (n "base64") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "qrcodegen") (r "^1.8") (d #t) (k 0)))) (h "0djp9m2lvzzrnfydjyqxnxia9g05ngdn3mrdjjirzjclvdypr6pm") (s 2) (e (quote (("base64" "dep:base64")))) (r "1.61")))

