(define-module (crates-io qr co qrcode-png) #:use-module (crates-io))

(define-public crate-qrcode-png-0.0.1 (c (n "qrcode-png") (v "0.0.1") (d (list (d (n "png") (r "^0.16.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)))) (h "1k628cc69hjbqjaq75g3kjc8lrwc669h76vf3h6b3cpl0i6i5hyn") (y #t)))

(define-public crate-qrcode-png-0.0.2 (c (n "qrcode-png") (v "0.0.2") (d (list (d (n "png") (r "^0.16.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)))) (h "1c6mdr6dj17a3w12ra6m2d2hrxh1f8sns075xivrhy169ck9mhs8") (y #t)))

(define-public crate-qrcode-png-0.1.0 (c (n "qrcode-png") (v "0.1.0") (d (list (d (n "png") (r "^0.16.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)))) (h "11wlqjc3q106nc935xlf8sg6l00vxz77bgikk87kahb3zvi51i4w") (y #t)))

(define-public crate-qrcode-png-0.1.1 (c (n "qrcode-png") (v "0.1.1") (d (list (d (n "png") (r "^0.16.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)))) (h "0dfl2h19v0kqrxygjxyswcvr55r823nbb28w926lyldszfm37bqb") (y #t)))

(define-public crate-qrcode-png-0.2.0 (c (n "qrcode-png") (v "0.2.0") (d (list (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)))) (h "0934zjs3sa3nf7p6r0xc8s7qcdsg8z2z0cpdzirr0j2c166g9k2p") (y #t)))

(define-public crate-qrcode-png-0.2.1 (c (n "qrcode-png") (v "0.2.1") (d (list (d (n "png") (r "^0.17.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.7.0") (d #t) (k 0)))) (h "1bl3005qxjzycax4lsd4nblgkzcs30vz3apcws7w33z49b8jv3lj") (y #t)))

(define-public crate-qrcode-png-0.3.0 (c (n "qrcode-png") (v "0.3.0") (d (list (d (n "png") (r "^0.17.3") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.7.0") (d #t) (k 0)))) (h "1pqag4kcrrak31yiagsx4i1p68yi3lljix499w3ljzgc0bk4hbxw") (y #t)))

(define-public crate-qrcode-png-0.4.0 (c (n "qrcode-png") (v "0.4.0") (d (list (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.7.0") (d #t) (k 0)))) (h "1p3sfwvf7vrlxh8kh7spcxfivpyqddismfbg3cbxv8kddgd0cf5c")))

(define-public crate-qrcode-png-0.4.1 (c (n "qrcode-png") (v "0.4.1") (d (list (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.8.0") (d #t) (k 0)))) (h "0gdmnpxh0ylgw40wflws8g7gsa5ad44l0lp665kf7vyil7gxrngx")))

