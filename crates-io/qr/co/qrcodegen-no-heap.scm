(define-module (crates-io qr co qrcodegen-no-heap) #:use-module (crates-io))

(define-public crate-qrcodegen-no-heap-1.8.0 (c (n "qrcodegen-no-heap") (v "1.8.0") (h "03ixjm0hb1k6f4kyf4bn1va8qwldmyakchypkfmjgjdwrncm0jsi") (y #t)))

(define-public crate-qrcodegen-no-heap-1.8.1 (c (n "qrcodegen-no-heap") (v "1.8.1") (h "033gpbdhj976ma032bgw5gd0agwl4szxlww9yjjc98g8ifzw1qnh")))

