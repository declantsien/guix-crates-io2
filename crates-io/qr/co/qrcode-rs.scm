(define-module (crates-io qr co qrcode-rs) #:use-module (crates-io))

(define-public crate-qrcode-rs-0.1.0 (c (n "qrcode-rs") (v "0.1.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imagesize") (r "^0.10") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.8.0") (d #t) (k 0)))) (h "0pavky2i3b84ijf2pfz71y0vh6jw9727bwb8hny8h42j37c4wrv9")))

(define-public crate-qrcode-rs-0.1.1 (c (n "qrcode-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "02dplakh3ymi26x3x1pkpcn3nx75izyaqpc514rbr691iwcm8hvk") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-rs-0.1.2 (c (n "qrcode-rs") (v "0.1.2") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "0wgniq5qas1cbpfp6ypbipnk6j40ivgcbcsyw9623zv4dclra47n") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-rs-0.1.3 (c (n "qrcode-rs") (v "0.1.3") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "0rmhq972rh84wklypqxjsppc2w6jdzvxhav67s5z51ha89abx026") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-rs-0.1.4 (c (n "qrcode-rs") (v "0.1.4") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "0c5s4b4k9znhkips5m7yi7x7nhn5h55bm5g3mr2cbypvcsxa4mk9") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-rs-0.1.5 (c (n "qrcode-rs") (v "0.1.5") (d (list (d (n "image") (r "^0.25") (o #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 2)))) (h "0x7sdbp1sprmja6x0yfk801biwibi47kng0x37wc2mz3wq5vnari") (f (quote (("svg") ("default" "image" "svg") ("bench")))) (r "1.67.1")))

