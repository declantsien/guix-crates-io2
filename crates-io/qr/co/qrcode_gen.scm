(define-module (crates-io qr co qrcode_gen) #:use-module (crates-io))

(define-public crate-qrcode_gen-0.1.0 (c (n "qrcode_gen") (v "0.1.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1ay4vabp1fiq70p0785sf0vmmw62rsfq7zwjq9v73i6xnfg1f8sf") (f (quote (("svg") ("png") ("default" "image" "svg" "png") ("bench"))))))

(define-public crate-qrcode_gen-0.1.1 (c (n "qrcode_gen") (v "0.1.1") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "04w7n36c5p06zqqriqcypq87cc2qs3bg2z5z8fwhwhyrgd78bzgd") (f (quote (("svg") ("png") ("default" "image" "svg" "png") ("bench"))))))

