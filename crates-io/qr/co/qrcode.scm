(define-module (crates-io qr co qrcode) #:use-module (crates-io))

(define-public crate-qrcode-0.0.2 (c (n "qrcode") (v "0.0.2") (h "018i3bd3f97f4004qb75375fybah982w6vp879z9qknh7n780xn4")))

(define-public crate-qrcode-0.0.3 (c (n "qrcode") (v "0.0.3") (h "0kvia2gvrh6w06mybmrg8g36dcyx6mcffpjj2rrc07j8mi4yfgcp")))

(define-public crate-qrcode-0.1.0 (c (n "qrcode") (v "0.1.0") (h "0n0m70fl3sd94knk2rv0k0asnnkb37bdjbz3qa8z6ilzpgyr1c7k")))

(define-public crate-qrcode-0.1.1 (c (n "qrcode") (v "0.1.1") (h "0rh0px81lpqpckmycyj47wpph2plz6yx97my006iv549qgvxn91h")))

(define-public crate-qrcode-0.1.2 (c (n "qrcode") (v "0.1.2") (h "050myfmh4ns2wpvfchxsnfnzs341dzsgjj86gdhlxpaiarnvxk4f")))

(define-public crate-qrcode-0.1.3 (c (n "qrcode") (v "0.1.3") (h "1snp51klmj638csq9ajkrb7rdnydbpapzcpk3i1xlyf5g3df4178")))

(define-public crate-qrcode-0.1.6 (c (n "qrcode") (v "0.1.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "19lksc18csb273bb5x7w7gn54v48ngqajz36qjvg53fh4d6sx0fh") (f (quote (("bench"))))))

(define-public crate-qrcode-0.1.7 (c (n "qrcode") (v "0.1.7") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0ph1ys5nj0g9lpwkk1980rf44mmlkzc27yvfv3yh74s1wsw03n4m") (f (quote (("bench"))))))

(define-public crate-qrcode-0.1.8 (c (n "qrcode") (v "0.1.8") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "02r20653va09mpr9zw9m5hj9b9pml7z5cmvrzp7nywd9lzqh3qkd") (f (quote (("bench"))))))

(define-public crate-qrcode-0.2.0 (c (n "qrcode") (v "0.2.0") (d (list (d (n "image") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1k9pxdhhinakv2id39mkzqby49yl7siz24xqnbrzwv5jjmfjxwj9") (f (quote (("default" "image") ("bench"))))))

(define-public crate-qrcode-0.2.1 (c (n "qrcode") (v "0.2.1") (d (list (d (n "image") (r "^0.12.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.33") (d #t) (k 0)))) (h "19n1jr13k0nra6zhybqcw22caynl30sjg71fizgfvifbpp1psb3l") (f (quote (("default" "image") ("bench"))))))

(define-public crate-qrcode-0.3.0 (c (n "qrcode") (v "0.3.0") (d (list (d (n "image") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "16z07c9zdnql5j7iigx65zzn0v4xfwsmmynlqg6gxr5vjlsqqidg") (f (quote (("default" "image") ("bench"))))))

(define-public crate-qrcode-0.4.0 (c (n "qrcode") (v "0.4.0") (d (list (d (n "image") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0rc4azsab04zq7rxvx7fd1p31cp2da2krb0kmp3mkak7xijhgni7") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.5.0 (c (n "qrcode") (v "0.5.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1lvx8l6v0qinxgrnsd5v9f3yk8l3a17f9dpjrh5b6i5wwizxyn98") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.6.0 (c (n "qrcode") (v "0.6.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.18") (o #t) (d #t) (k 0)))) (h "04x6qcxzw58zlzzxv79h6nf4cyshy18pqp69r250al8ij1dl5rql") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.7.0 (c (n "qrcode") (v "0.7.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0p5mzh2jmpmnz9ji3pkic6dphs7lr1m9jbsxvc2hsa1w74jh7hym") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.8.0 (c (n "qrcode") (v "0.8.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0mxckfhjwn1pg2zgwqzv8ljn7s7xpkcy3af0bb0sfx02z0r4gd33") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.9.0 (c (n "qrcode") (v "0.9.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.20") (o #t) (d #t) (k 0)))) (h "09lvd7rgc05i0q7n0hf0jbfxywpajsy6knq05bgcwnfd2j33smdk") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.10.0 (c (n "qrcode") (v "0.10.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 2)))) (h "1436rz2vlsx9rga01z3hmxncy9719i3xi83w6l8sk8lyb4mix1l3") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.10.1 (c (n "qrcode") (v "0.10.1") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.21") (o #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 2)))) (h "1pi5nrvs8l1y33nah9wzfb69bn68lm3p90sq8wnsasv36502fffv") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.11.0 (c (n "qrcode") (v "0.11.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)))) (h "1rrxwdx43sdmxx0hk9ww60d4imln2qdgwdmjq36b20wcd2xmn0d0") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.11.1 (c (n "qrcode") (v "0.11.1") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)))) (h "0vb4ypv6ac659yhiamzgsagmxscji2b9759pa8384bpxakfr348b") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.11.2 (c (n "qrcode") (v "0.11.2") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)))) (h "12pb6fqfldc6jbzaa5hhr78ggmic29vgs2qa656x79cakgz9xc7m") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.12.0 (c (n "qrcode") (v "0.12.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "0zzmrwb44r17zn0hkpin0yldwxjdwya2nkvv23jwcc1nbx2z3lhn") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.13.0 (c (n "qrcode") (v "0.13.0") (d (list (d (n "image") (r "^0.24") (o #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "0gyfc3xv94ml441a788p2jk38rakl3vrqr7khs0zk6dizmni6vqn") (f (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qrcode-0.14.0 (c (n "qrcode") (v "0.14.0") (d (list (d (n "image") (r "^0.25") (o #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 2)))) (h "0v835gzkhjm7ij7prchbd9h3qnqisq0dnvj3m3szjvwna751krr3") (f (quote (("svg") ("default" "image" "svg") ("bench")))) (r "1.67.1")))

