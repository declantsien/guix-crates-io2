(define-module (crates-io qr co qrcode_scanner) #:use-module (crates-io))

(define-public crate-qrcode_scanner-0.1.0 (c (n "qrcode_scanner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bardecoder") (r "^0.5") (d #t) (k 0)) (d (n "ffimage") (r "^0.9") (d #t) (k 0)) (d (n "ffimage_yuv") (r "^0.9") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "v4l") (r "^0.14") (d #t) (k 0)))) (h "0nvd3mdn6dg3aslhgwjkv3n6bh1znxbkn6dgnncz9z3rwis3a1ra")))

(define-public crate-qrcode_scanner-0.1.1 (c (n "qrcode_scanner") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bardecoder") (r "^0.5") (d #t) (k 0)) (d (n "ffimage") (r "^0.9") (d #t) (k 0)) (d (n "ffimage_yuv") (r "^0.9") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "v4l") (r "^0.14") (d #t) (k 0)))) (h "0gspc9qg1zbbiq7ib8dzi88xw0ahlza5izw7lvissz6ddrkb5a13")))

(define-public crate-qrcode_scanner-0.2.0 (c (n "qrcode_scanner") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bardecoder") (r "^0.5") (d #t) (k 0)) (d (n "ffimage") (r "^0.9") (d #t) (k 0)) (d (n "ffimage_yuv") (r "^0.9") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "v4l") (r "^0.14") (d #t) (k 0)))) (h "1ys4f421bhbjd23f6pwflp75si33j51a93i2vwdxrgcfplqy4gj6")))

(define-public crate-qrcode_scanner-0.2.1 (c (n "qrcode_scanner") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bardecoder") (r "^0.5") (d #t) (k 0)) (d (n "ffimage") (r "^0.10") (d #t) (k 0)) (d (n "ffimage_yuv") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "v4l") (r "^0.14") (d #t) (k 0)))) (h "1vcvjizlciz2m2pijqgvs7kjpzdaip5s3n31i0ihrqmr3rw77349")))

(define-public crate-qrcode_scanner-0.3.1 (c (n "qrcode_scanner") (v "0.3.1") (d (list (d (n "ffimage") (r "^0.10") (d #t) (k 0)) (d (n "ffimage_yuv") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rxing") (r "^0.4") (d #t) (k 0)) (d (n "v4l") (r "^0.14") (d #t) (k 0)))) (h "0npjf99nd4s794dwzi2x5mmfmfxz8j78fqmq9w7ala4h8sc5a0iq")))

