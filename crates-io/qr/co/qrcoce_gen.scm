(define-module (crates-io qr co qrcoce_gen) #:use-module (crates-io))

(define-public crate-qrcoce_gen-0.1.0 (c (n "qrcoce_gen") (v "0.1.0") (d (list (d (n "checked_int_cast") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "00yskizmqzfgnkcxhk0imsf3w6rm90f7ycwwvg4pid6k8sgr7g53") (f (quote (("svg") ("png") ("default" "image" "svg" "png") ("bench")))) (y #t)))

