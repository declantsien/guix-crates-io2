(define-module (crates-io qr co qrcode53bytes) #:use-module (crates-io))

(define-public crate-qrcode53bytes-1.0.0 (c (n "qrcode53bytes") (v "1.0.0") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15z1pdnfsy5hjpwna86kyx49vcvba3sqgaz4j9w5x663nkin01wk") (y #t)))

(define-public crate-qrcode53bytes-1.1.0 (c (n "qrcode53bytes") (v "1.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0r3vi2rr6lk2i8rjgp477a6a4jvaxk73y33gk5pdj7bscwlg23yl")))

