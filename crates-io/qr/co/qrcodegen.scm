(define-module (crates-io qr co qrcodegen) #:use-module (crates-io))

(define-public crate-qrcodegen-1.2.1 (c (n "qrcodegen") (v "1.2.1") (h "0zxs045sz6f0af2kdwnddhvgr2fn1qc57hr34s774iqmiiw85m7h")))

(define-public crate-qrcodegen-1.3.0 (c (n "qrcodegen") (v "1.3.0") (h "1lqmvfbrm4hfxg6fw8ik3bj3m4kjzk8v84vwiz49r6zq723kmcr1")))

(define-public crate-qrcodegen-1.3.1 (c (n "qrcodegen") (v "1.3.1") (h "11nlzk5qy7ywbnrzk5gfnh7s9ybjq572vg6ckx5cv8kx0zy66fnq")))

(define-public crate-qrcodegen-1.4.0 (c (n "qrcodegen") (v "1.4.0") (h "00r2l1hx377vsdcxab8js2n3gdzsly1pcfy0172a7d3flkrp59yr")))

(define-public crate-qrcodegen-1.5.0 (c (n "qrcodegen") (v "1.5.0") (h "0aqndrz9x98fiv1pdla5s5x1zmnq829g61d23lv75dyhhyyciy5i")))

(define-public crate-qrcodegen-1.6.0 (c (n "qrcodegen") (v "1.6.0") (h "0v2b5cp558lybzqrbq4789086s0kq2z090b6wqrmbwa54f5s6knj")))

(define-public crate-qrcodegen-1.7.0 (c (n "qrcodegen") (v "1.7.0") (h "0hzkmv5p31g4vk76nfqn7q5qcdifwyaxi105sxyqkjnqxra6fphk")))

(define-public crate-qrcodegen-1.8.0 (c (n "qrcodegen") (v "1.8.0") (h "0hn1j12q31nzlkra42s20p0wh198bx8f7xc73mic3j9121xgqfa3")))

