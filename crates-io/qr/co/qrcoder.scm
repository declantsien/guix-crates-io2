(define-module (crates-io qr co qrcoder) #:use-module (crates-io))

(define-public crate-qrcoder-0.1.0 (c (n "qrcoder") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.9") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "03zyas26f5f7p8x7s4wgc00v2f0bjsl04kq0pqcnxf79020269hm")))

(define-public crate-qrcoder-0.1.1 (c (n "qrcoder") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.9") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "10vda4fyzpdphj2sn0vpgi9mwic6r8kzi1l8f433nfrmgmqqg0hy")))

(define-public crate-qrcoder-0.1.2 (c (n "qrcoder") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.9") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "11hv576mk14ha1rq40s97636m05lhfnrb25q1wwgpniz9hkcfrh4")))

(define-public crate-qrcoder-0.2.0 (c (n "qrcoder") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.9") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0bazcawgdfpvlf8fd6kwaaxc244fnk07b3n64zq5df3y9c2fvjjq")))

(define-public crate-qrcoder-0.2.1 (c (n "qrcoder") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.9") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0bzwfmnh7jrr5y47h3rnmg0751lkrjiprh68b44856iygk1nlf6d")))

