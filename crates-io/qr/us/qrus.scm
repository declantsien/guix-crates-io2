(define-module (crates-io qr us qrus) #:use-module (crates-io))

(define-public crate-qrus-0.1.0 (c (n "qrus") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "qrcode-png") (r "^0.4.1") (d #t) (k 0)) (d (n "rqrr") (r "^0.6.0") (d #t) (k 0)))) (h "16cqv777h99jbnkghaakn8whdp3gs3z51iik2d58i3hdcchf1q3i")))

