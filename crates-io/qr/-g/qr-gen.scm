(define-module (crates-io qr -g qr-gen) #:use-module (crates-io))

(define-public crate-qr-gen-1.0.0 (c (n "qr-gen") (v "1.0.0") (d (list (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)) (d (n "resvg") (r "^0.13.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.3") (d #t) (k 0)) (d (n "usvg") (r "^0.13.0") (d #t) (k 0)))) (h "1wrqnjj6clq49b8bayhvz1bk17rygkxfm9kn23bd38wmwknqzdsx")))

(define-public crate-qr-gen-1.0.1 (c (n "qr-gen") (v "1.0.1") (d (list (d (n "qrcodegen") (r "^1.6.0") (d #t) (k 0)) (d (n "resvg") (r "^0.13.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.3") (d #t) (k 0)) (d (n "usvg") (r "^0.13.0") (d #t) (k 0)))) (h "0s6pflsrw9m6xz4hhbny3qnnls5y766b86rsi2p33vabwpc2flgv")))

