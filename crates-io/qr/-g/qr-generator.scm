(define-module (crates-io qr -g qr-generator) #:use-module (crates-io))

(define-public crate-qr-generator-0.1.0 (c (n "qr-generator") (v "0.1.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.0") (d #t) (k 0)))) (h "1jkylqjqkj4fb4v1ibps8kzmjgsarbbfkj91a5d29m14wyf3dmcr")))

(define-public crate-qr-generator-0.1.1 (c (n "qr-generator") (v "0.1.1") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.0") (d #t) (k 0)))) (h "1z273ij5yvjw49k5clr2scd7y921mc8r31134ay8gxffpvgz6pys")))

(define-public crate-qr-generator-0.1.2 (c (n "qr-generator") (v "0.1.2") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.0") (d #t) (k 0)))) (h "07ngi3s9c0knsq7w5n2qf9nqgil1ir1b28hwpxbi8pvclk1kc3yw")))

(define-public crate-qr-generator-0.1.3 (c (n "qr-generator") (v "0.1.3") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "qrcode") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)))) (h "1isn0v4nghlwbcn9345h91h8yqzs5f7ld52yjzx8fsvjvgks539g")))

