(define-module (crates-io qr ep qrep) #:use-module (crates-io))

(define-public crate-qrep-0.1.0 (c (n "qrep") (v "0.1.0") (d (list (d (n "grep-matcher") (r "^0.1.1") (d #t) (k 0)) (d (n "grep-regex") (r "^0.1.1") (d #t) (k 0)) (d (n "grep-searcher") (r "^0.1.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_qt_binding_generator") (r "^0.2") (d #t) (k 1)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0p9w01vv9n4i495fp53in51sshmr6mxhkvcf4f1xhpkpwmdb006n") (l "qrep")))

