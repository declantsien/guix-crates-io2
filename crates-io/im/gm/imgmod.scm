(define-module (crates-io im gm imgmod) #:use-module (crates-io))

(define-public crate-imgmod-1.0.0 (c (n "imgmod") (v "1.0.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "04rwh1xs6rv052ww9lkj3hynl09f41cmmfbw990ddf1rmq3jv0q5")))

(define-public crate-imgmod-1.1.0 (c (n "imgmod") (v "1.1.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1bbry0nsfbkimqdbh7jm3k2qibfwr9b2nkx3fbx2bdwhr8pcafpx")))

(define-public crate-imgmod-1.2.0 (c (n "imgmod") (v "1.2.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1wx0lhh7k9h38gcgkk2izj2130vfh1pvhrs2kjp80bda0namm7pq")))

(define-public crate-imgmod-1.2.1 (c (n "imgmod") (v "1.2.1") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1xpak5m22xaywz5zvb4hhskfih73r4r52gg4glp2mq7jybqxc4di")))

