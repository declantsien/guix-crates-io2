(define-module (crates-io im pa impact-rs) #:use-module (crates-io))

(define-public crate-impact-rs-0.0.1 (c (n "impact-rs") (v "0.0.1") (d (list (d (n "fixed32") (r "^0.0.3") (d #t) (k 0)) (d (n "fixed32-math") (r "^0.0.2") (d #t) (k 0)))) (h "1zlcv9z6yb1hzc4a67q61sj2jm1wnf2kd451n65q798n96kcqshr")))

