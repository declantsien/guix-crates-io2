(define-module (crates-io im pa impartial-ord) #:use-module (crates-io))

(define-public crate-impartial-ord-1.0.0 (c (n "impartial-ord") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1iqawv9jxnh8yswxxxid11ip0bq5q676vzf51508cm5ww0yb98sv") (r "1.56.0")))

(define-public crate-impartial-ord-1.0.1 (c (n "impartial-ord") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "137g9q90hh8amjdfwv2c852a0wq6s96ayqcn6fpx63a1216abb5c") (r "1.56.0")))

(define-public crate-impartial-ord-1.0.2 (c (n "impartial-ord") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1m4kkkavbvs3qyv8qdsxg61yr0mnaz2fin6aipp3fv62s2yln1zv") (r "1.56.0")))

(define-public crate-impartial-ord-1.0.3 (c (n "impartial-ord") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "15ldr1w7dc9zbpxb9zn86pz61p38m81ibwkp1fpkcnimxxz3qlj4") (r "1.56.0")))

(define-public crate-impartial-ord-1.0.4 (c (n "impartial-ord") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0393l3jr63dcv3faa93iir6h282f4rs1rflyl79bz0qm59fny13h") (r "1.56.0")))

(define-public crate-impartial-ord-1.0.5 (c (n "impartial-ord") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1cjplr3pw12qrj2qcvhfnj3ajyahr0ajmdbh1sy0adbp206nm34i") (r "1.56.0")))

