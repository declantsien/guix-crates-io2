(define-module (crates-io im st imstr) #:use-module (crates-io))

(define-public crate-imstr-0.1.0-alpha.1 (c (n "imstr") (v "0.1.0-alpha.1") (d (list (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)))) (h "1066b1frish5fbrw6bs07g9dfp9ag9a0l2kkidwq7k076x3rk33c") (f (quote (("debug-internal")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-imstr-0.1.0 (c (n "imstr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)))) (h "0yjfg86nz0g5fa6alzb8f2h7lls1h8q9s8520cfcnc2dx60g2789") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-imstr-0.1.1 (c (n "imstr") (v "0.1.1") (d (list (d (n "peg-runtime") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "peg") (r "^0.8.1") (d #t) (k 2)))) (h "0kap7l5s4vaqia0ymly3a22p630bij72wshphl0d8b3p06hghjgq") (s 2) (e (quote (("serde" "dep:serde") ("peg" "dep:peg-runtime"))))))

(define-public crate-imstr-0.2.0 (c (n "imstr") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "peg-runtime") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "peg") (r "^0.8.1") (d #t) (k 2)))) (h "1zkqxi5p8hyg837qvnwclvb8qhl5smabw4zkfascdaafn3g42d3x") (s 2) (e (quote (("serde" "dep:serde") ("peg" "dep:peg-runtime") ("nom" "dep:nom"))))))

