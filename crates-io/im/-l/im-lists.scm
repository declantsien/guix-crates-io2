(define-module (crates-io im -l im-lists) #:use-module (crates-io))

(define-public crate-im-lists-0.1.0 (c (n "im-lists") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1j805zanlcgp3mi525f0xnxbbxawqpi18axkznvp7fzcmw30dbmh")))

(define-public crate-im-lists-0.2.0 (c (n "im-lists") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0si9wr2im6s4mkpy6pgr9iggnrczmw9k9rcjn782g72wqcp80njc")))

(define-public crate-im-lists-0.3.0 (c (n "im-lists") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0n3smxw89629d7482pb2vf4l21c8wxafaqzqj3ywwp41km8pc8k7")))

(define-public crate-im-lists-0.4.0 (c (n "im-lists") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "02j91qqbjd6f1vznnqn14m0wszdzjsyjwp0bvjg40rajknz3xy88")))

(define-public crate-im-lists-0.5.0 (c (n "im-lists") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0j9rly9yfas1113x7gdbwzw1hx2w1bwvz22xdwz5clgpk5iymqfv")))

(define-public crate-im-lists-0.6.0 (c (n "im-lists") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "08gx6xqqy6fnmdp41saxf7inb0m93ibb6mykfl95lmilwjjgn318")))

(define-public crate-im-lists-0.7.0 (c (n "im-lists") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1rpp8s509sr1fl9bwza85ipa333yz0j2bmfcwh8y9aslcrvnrkm8")))

(define-public crate-im-lists-0.7.1 (c (n "im-lists") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1627mzvr2q71q787b3v6g97iir89fmiahph8h8rr4i8nqkrfy1qc")))

(define-public crate-im-lists-0.8.0 (c (n "im-lists") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0qwb16ayx18ihyyanqlcc9j32i50fhd33z9kc0fhpz0zigxk6wiy")))

(define-public crate-im-lists-0.8.1 (c (n "im-lists") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "12myxlk86wn5gwzb6b6asvf1mz4n382dggasb3qim3hj3yhs53f3")))

