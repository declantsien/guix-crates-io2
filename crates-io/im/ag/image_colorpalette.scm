(define-module (crates-io im ag image_colorpalette) #:use-module (crates-io))

(define-public crate-image_colorpalette-0.1.0 (c (n "image_colorpalette") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "13bxznfrmcg1jnsvhkqyl7mvi54fxvda8y2f3qy2323aspl5rynz") (y #t)))

(define-public crate-image_colorpalette-0.1.1 (c (n "image_colorpalette") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "19yhfyf1da8mkyy1zzdkq79krhn437plwhjlcw33ki9jg307ddwp") (y #t)))

(define-public crate-image_colorpalette-0.1.2 (c (n "image_colorpalette") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "1576aqca3i8yxi6zzs3gsrlpdjslpwjpbcmdwhlpbv3h3mdwm6za") (y #t)))

(define-public crate-image_colorpalette-0.1.3 (c (n "image_colorpalette") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "0jjy6s4y671fl5jmk35fjw1xd74awx0dssg10kdkbwdkzy06wa8g")))

(define-public crate-image_colorpalette-0.1.4 (c (n "image_colorpalette") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "0shzr49jniy5s5ijvjby70qqczvb58r19grmz236z1s94v44ibca")))

(define-public crate-image_colorpalette-0.1.5 (c (n "image_colorpalette") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)))) (h "158y5ypsqn9dh2yr1aa8zkqlky8y9d2swip1zb0j8x7kmncg7xpx")))

