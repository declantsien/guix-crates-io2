(define-module (crates-io im ag image-utils) #:use-module (crates-io))

(define-public crate-image-utils-0.1.0 (c (n "image-utils") (v "0.1.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "06vn9v3bbl82957fwg6f1pfkv03kkwx9q4pzan7kvinp7a7n0mwl")))

(define-public crate-image-utils-0.1.1 (c (n "image-utils") (v "0.1.1") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "1248rb1cx22dkr3ifihsmv33zy1ccjlxybvaxmx3wrdx4pqq203x")))

(define-public crate-image-utils-0.1.2 (c (n "image-utils") (v "0.1.2") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1.4") (d #t) (k 0)))) (h "023yvpx6yj44lw1hqxsbi9pifs9k3y2hxi9gdfmwpqnpvr2anzpw")))

(define-public crate-image-utils-0.1.3 (c (n "image-utils") (v "0.1.3") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1.4") (d #t) (k 0)))) (h "0pvfbaa460dy0394l70qj9pl5kf8vzii62x7jn1d5lj7wbsidhrm")))

(define-public crate-image-utils-0.1.4 (c (n "image-utils") (v "0.1.4") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1.4") (d #t) (k 0)))) (h "1r8c72s0288h0clb13k5v13ad5zinw2fjlfzz8qqn0aw98vpqa3r")))

(define-public crate-image-utils-0.2.0 (c (n "image-utils") (v "0.2.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "07sm7c4kb5p61rbz0ws8j24s3k5ldcdx373sr468b2rg7aq046ny")))

