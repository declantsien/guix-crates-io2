(define-module (crates-io im ag image-moments) #:use-module (crates-io))

(define-public crate-image-moments-0.3.0 (c (n "image-moments") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "1j2qh5b6y5madj9k37rbgawagdsyiikbnxvs9cdwzdy206j8hsi8")))

(define-public crate-image-moments-0.3.1 (c (n "image-moments") (v "0.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "1scn5xxlp8ipn8b3nj9sdyh43ih4lsng9b381zp3pqj90xqyh40k")))

(define-public crate-image-moments-0.4.0 (c (n "image-moments") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "1k87y2gwyln8rs0p9929kwg9l2wjy291n14fyqr90bffcm5i5629")))

(define-public crate-image-moments-0.5.0 (c (n "image-moments") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "1ixhkhy325p8jsjym4c06f0kxrh8jkxiri0rd7sjnrvaxj0srs55")))

