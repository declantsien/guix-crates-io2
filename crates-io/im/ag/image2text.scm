(define-module (crates-io im ag image2text) #:use-module (crates-io))

(define-public crate-image2text-1.0.1 (c (n "image2text") (v "1.0.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "0fgjsnrbb71s29235zhir467lbf4sz2hxyliqbzbim76y22af9cj")))

