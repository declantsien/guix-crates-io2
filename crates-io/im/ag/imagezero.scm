(define-module (crates-io im ag imagezero) #:use-module (crates-io))

(define-public crate-imagezero-0.0.0 (c (n "imagezero") (v "0.0.0") (h "13ipq8gjdq354lbsmwq7blb3w47g9mkyb425xssrahdx5w25if3w")))

(define-public crate-imagezero-0.0.1 (c (n "imagezero") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1c5xg52kxq72qz2yqr5riq1bq906nsz3n2yc2wis9fwjdc4hn57l")))

