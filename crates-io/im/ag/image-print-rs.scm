(define-module (crates-io im ag image-print-rs) #:use-module (crates-io))

(define-public crate-image-print-rs-0.1.0 (c (n "image-print-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "viuer") (r "^0.6.1") (f (quote ("sixel"))) (d #t) (k 0)))) (h "11j3aa3hk2w9164abi2wgsfrpm05ndh8wqq5vv3d2lzcdbcb7b7z")))

(define-public crate-image-print-rs-0.1.1 (c (n "image-print-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "viuer") (r "^0.6.1") (f (quote ("sixel"))) (d #t) (k 0)))) (h "0bsz2wknmrxzvgvjd1h5biwyzqinjsd2fb1iybl29qbqngxw3i5i")))

