(define-module (crates-io im ag image-stream) #:use-module (crates-io))

(define-public crate-image-stream-0.1.0 (c (n "image-stream") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1k79ikdgqpr0x3wf25jrc7jrd5v0j9xkb51aah1yh08lfdm9kir2")))

