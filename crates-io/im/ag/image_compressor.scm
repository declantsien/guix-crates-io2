(define-module (crates-io im ag image_compressor) #:use-module (crates-io))

(define-public crate-image_compressor-0.1.0 (c (n "image_compressor") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "08wp9k47r7lj6y5fp6kbs8hmp7jv8xmpm5kbrzva6varrmj944ii")))

(define-public crate-image_compressor-0.1.1 (c (n "image_compressor") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "0xihsd8f1yfvdx4fvvs1bcgx9x0wfbqv14xj1fmwbqd78y9s0s0k")))

(define-public crate-image_compressor-1.0.0 (c (n "image_compressor") (v "1.0.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "0vpzp226w8md7fvmx617x8nfs0yrhz4fvgsh2hlb8bza8chl4f73")))

(define-public crate-image_compressor-1.1.0 (c (n "image_compressor") (v "1.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "06qazrr6hafjf16814jp7gsyrn1nxyhrzzw9ldzyr2pzvd0g9arw")))

(define-public crate-image_compressor-1.1.1 (c (n "image_compressor") (v "1.1.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "0m090r5bkmsaw6zydzrwlinhfr32vr0hj1fl6r37a1qq6gkv2jsc")))

(define-public crate-image_compressor-1.2.0 (c (n "image_compressor") (v "1.2.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "095g288j27nja4dqlpfkj62w4iqaib783sv1vvfxx4vyz37ixhx2")))

(define-public crate-image_compressor-1.2.1 (c (n "image_compressor") (v "1.2.1") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "1ckdm28ygwiccw99jmbynplkcg55yap0rllvkdyr6b70d6h9zg9s")))

(define-public crate-image_compressor-1.2.2 (c (n "image_compressor") (v "1.2.2") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "07wl5bb144j13v57rmkxv7w3prcbmpfjhdcq93b7xb7p395plz2g")))

(define-public crate-image_compressor-1.2.3 (c (n "image_compressor") (v "1.2.3") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "0jby0rivl6l7gzc1aw3lsd2bba69lyqnmsyi5v6mx6y52wpajzw5")))

(define-public crate-image_compressor-1.3.0 (c (n "image_compressor") (v "1.3.0") (d (list (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 0)))) (h "1r3kwj547p3qgdz2wd2ippss7cy94bym13315zhkp1f3xihfzn0n")))

(define-public crate-image_compressor-1.5.0 (c (n "image_compressor") (v "1.5.0") (d (list (d (n "colorgrad") (r "^0.6.2") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 2)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.10.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dij20h78g379z7gvgzipz8l3h99jiiqgv5p7qgvqq406hyy1jlw")))

