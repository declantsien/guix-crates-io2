(define-module (crates-io im ag imageinfo) #:use-module (crates-io))

(define-public crate-imageinfo-0.1.0 (c (n "imageinfo") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fnim5w3x5mdh4bahpfmqh6dpklwlf6d7a2gh3892rlrrffqaj4i")))

(define-public crate-imageinfo-0.2.0 (c (n "imageinfo") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sl9wsahld9b5a0ikhij9cnj38l9scy15p09s3mw5aqh0vxjf5pi")))

(define-public crate-imageinfo-0.3.0 (c (n "imageinfo") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1422g06gan6lq5jy7gv92park1mzg4ykwcdn09q1x7myh9q04z0n")))

(define-public crate-imageinfo-0.4.0 (c (n "imageinfo") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n5p30cz3mn5nlz3vfjb08rg2ai7iga7c0ci94sw2dz3b5ysjxn3")))

(define-public crate-imageinfo-0.5.0 (c (n "imageinfo") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0azjdnbjbnylm69pwv1l7ww03q3pjjn5adyjg8ghyjqalzy8zvwi")))

(define-public crate-imageinfo-0.6.0 (c (n "imageinfo") (v "0.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18dh7xw2wxhixs6zv5bgs0jxlzv38n81rh3p24szrkjkr0jdx720")))

(define-public crate-imageinfo-0.7.0 (c (n "imageinfo") (v "0.7.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0v0ca0lbbma0nmfcjgbk7f96nzj5g7xz4lc6vizyfq8fbr2jvg6v")))

(define-public crate-imageinfo-0.7.1 (c (n "imageinfo") (v "0.7.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z7wprg8w85gwfk6hvxrrgrk0ps5n496dyinvqzihv4ia2sidl97")))

(define-public crate-imageinfo-0.7.2 (c (n "imageinfo") (v "0.7.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02h3zxxqn1jyy81msvv6p5i6kqyv7fydbmjymbjbyr49c3bj8d9w")))

(define-public crate-imageinfo-0.7.3 (c (n "imageinfo") (v "0.7.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ik5l3p9pdcqn9y475x962sdk2z4h5sh18d8z3s6rpvfy43anf3k")))

(define-public crate-imageinfo-0.7.4 (c (n "imageinfo") (v "0.7.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "079mfdl29dblqfwwbxbw4pf02md91jg9gs5zvknyyd3c27rkx8jy")))

(define-public crate-imageinfo-0.7.5 (c (n "imageinfo") (v "0.7.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15n4bb36jwyr1rbr3pc8njfr36lpkxxbnnvw6iwl42n20ijzqksk")))

(define-public crate-imageinfo-0.7.6 (c (n "imageinfo") (v "0.7.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k2vdas48ilbxyhykix7ib42aq87arg92q3a5gxbznwx6mllbl4y")))

(define-public crate-imageinfo-0.7.7 (c (n "imageinfo") (v "0.7.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13afxbmxld8vwlh5bq1kci3h1f3pf3hrnx45q1i0j6h04kr1rkv2")))

(define-public crate-imageinfo-0.7.8 (c (n "imageinfo") (v "0.7.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dp1v0yhqiqg4p3fwch3pxcr0d32vq8q2qih5fwf8pj5yp1050f1")))

(define-public crate-imageinfo-0.7.9 (c (n "imageinfo") (v "0.7.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "028lsg02n0dpdpfkw5739x0q15qab4mdv02c428j5rda8ir5gs7p")))

(define-public crate-imageinfo-0.7.10 (c (n "imageinfo") (v "0.7.10") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wbyc2p2ph07w11sv8qvh745ljh715j9wj0wmgzcv62fs4zax0cg")))

(define-public crate-imageinfo-0.7.11 (c (n "imageinfo") (v "0.7.11") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bymr84nr4wl9ax12d2c4hrj07sm4r95nvynd7z368arpc2rpvv4")))

(define-public crate-imageinfo-0.7.12 (c (n "imageinfo") (v "0.7.12") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m7b0h9yk1zq3m1b6p5vy5qwz0bqvqk4ywj6glpk9s478838zcda")))

(define-public crate-imageinfo-0.7.13 (c (n "imageinfo") (v "0.7.13") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y21k5867s7bp2clnf5zh9jrk980jcq1k2ap9l9s4nmkany3kb2s")))

(define-public crate-imageinfo-0.7.14 (c (n "imageinfo") (v "0.7.14") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w6jvdnzzr9ab3r52s2fwgs7c25137p6c6rvag0bdfhdr2cmb4da")))

(define-public crate-imageinfo-0.7.15 (c (n "imageinfo") (v "0.7.15") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lc7fx96kk6wxd8iv0hmbqsrpcj16dxbr2fbfawihmiq8ks4a7gb")))

(define-public crate-imageinfo-0.7.16 (c (n "imageinfo") (v "0.7.16") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v6p6y5ivv04pyjczsclkdbq159x6jw0vg43xvmqwqrmsg9jc66s")))

