(define-module (crates-io im ag imagesize) #:use-module (crates-io))

(define-public crate-imagesize-0.1.0 (c (n "imagesize") (v "0.1.0") (h "0m7l67rfhdzsfzls1k1ym0cwa4594w4x6m00wrbgzdhwvpymhd45")))

(define-public crate-imagesize-0.1.1 (c (n "imagesize") (v "0.1.1") (h "12fasf9knxpjgprp1a2q6ppq11qpahp0kyiw4may01yb9n0cnbym")))

(define-public crate-imagesize-0.1.2 (c (n "imagesize") (v "0.1.2") (h "12n5xkaz06r452flp3a5pmb4g68vi11dsd8as0q2ygcxc7bv25xq")))

(define-public crate-imagesize-0.2.0 (c (n "imagesize") (v "0.2.0") (h "0jzlj9xh97krqk5rnl7kl1bx29j3kdwplhx0a4lb5xj3gw4s46p7")))

(define-public crate-imagesize-0.3.0 (c (n "imagesize") (v "0.3.0") (h "053kk3cd8yxnsy5z462ss2ydpf2450va6s6qn8lb7636cj40a2y7")))

(define-public crate-imagesize-0.3.1 (c (n "imagesize") (v "0.3.1") (h "14hdg9816wszc88sai3p7319c56zipdlb47mnnqf65i3pb2xsps7")))

(define-public crate-imagesize-0.3.2 (c (n "imagesize") (v "0.3.2") (h "151k1kdv277mlcwlb923qfqybfqp2j39j53ypqjypc0ngnqahkyc")))

(define-public crate-imagesize-0.3.3 (c (n "imagesize") (v "0.3.3") (h "10rn3fdnhxmn2gv2n8jx04gqz94ifnz23qqhd1qvqvs64v1l6m47")))

(define-public crate-imagesize-0.3.4 (c (n "imagesize") (v "0.3.4") (h "144jnd6zpc7ck1l35a7kj2p1mwab1ajvb5978axj3psf4gswg469")))

(define-public crate-imagesize-0.3.5 (c (n "imagesize") (v "0.3.5") (h "1dcrncyps9y80nfi4dv7wm6xjyakxmzjby2v3n8zd3h6c1c9z8xf")))

(define-public crate-imagesize-0.4.0 (c (n "imagesize") (v "0.4.0") (h "1a3ahfx3rwlgfarr7zs05lw0ygq123446qsw5z2kyd1rayhl15yb")))

(define-public crate-imagesize-0.5.0 (c (n "imagesize") (v "0.5.0") (h "1g79mj2yyzdc7724zvnl4rqk37g2cz1vpq40jl4cnypwvvqfm87f")))

(define-public crate-imagesize-0.5.1 (c (n "imagesize") (v "0.5.1") (h "189mkwwkfkh2z1b15jgld9bxafypvmkkywgfiahbc44kqrmbhi6c")))

(define-public crate-imagesize-0.5.2 (c (n "imagesize") (v "0.5.2") (h "0mrw2ai6g1h1j9ckpggjjirg733qybk6w5mwwjc74p4r6pxr8psp")))

(define-public crate-imagesize-0.6.0 (c (n "imagesize") (v "0.6.0") (h "1pzcjd8y09xmgdxfifwxziqy0x7z6hcggiasf460h6ywv7pq4xy3")))

(define-public crate-imagesize-0.7.0 (c (n "imagesize") (v "0.7.0") (h "0jm2mfaggzqh4dn7smc95w4fwbczv57c8p9f4zbvwcw4jv2z0p1p")))

(define-public crate-imagesize-0.7.1 (c (n "imagesize") (v "0.7.1") (h "18d4vyfyca3rjyiqzblfks7a4lznk953lvi9xlvkdbwh10p0l73k")))

(define-public crate-imagesize-0.8.0 (c (n "imagesize") (v "0.8.0") (h "1kql8ydpgprf0aazhyx4xv2hxg2fxx6sl12isgzg6z5nif6adgad")))

(define-public crate-imagesize-0.8.1 (c (n "imagesize") (v "0.8.1") (h "040vqqw3id742wqzwcy2iidi104w6xjwbwipw4nycylr9dj2z8d5")))

(define-public crate-imagesize-0.8.2 (c (n "imagesize") (v "0.8.2") (h "11bvhz9z9dz9pkn7clpsy26dj9k57drbzhj368n8x3v762ba927b")))

(define-public crate-imagesize-0.8.3 (c (n "imagesize") (v "0.8.3") (h "1whkh6p6rjlxnmvxni9snl1hmcr4wfvg0yzid8dk69zphh5immys")))

(define-public crate-imagesize-0.8.4 (c (n "imagesize") (v "0.8.4") (h "0hsm7zpl2r1wjc0dbqlb37zzaww1c125xnk1i5yd2pblrl2hwd7z")))

(define-public crate-imagesize-0.8.5 (c (n "imagesize") (v "0.8.5") (h "146a8203qzliicams2rwf33jp9i5ygbjpil05swm94fn1zs3x8py")))

(define-public crate-imagesize-0.8.6 (c (n "imagesize") (v "0.8.6") (h "13v1iiqdyrd59fm2hkkhz8lh6jjiwgvahsnmqnrk3qn2fvggaglp")))

(define-public crate-imagesize-0.8.7 (c (n "imagesize") (v "0.8.7") (h "19zshcm02pdv01x0xz2w41d270fiw1k0d4v35k4q67a0043m1hna")))

(define-public crate-imagesize-0.8.8 (c (n "imagesize") (v "0.8.8") (h "122h9xdc7vkj0alnw8q6dl9wbgw6bq89c6yblqkwc2jlkw9v49zx")))

(define-public crate-imagesize-0.9.0 (c (n "imagesize") (v "0.9.0") (h "0zcqxd7vcvlh39da0ady7nhgpqlvg2mfllbvl2pkhgkq9qpvdcq0")))

(define-public crate-imagesize-0.10.0 (c (n "imagesize") (v "0.10.0") (h "1vrzq7cvvy9n9qn6fpxqx9ryks0v0da6n1zbg8gj833an4ll2ilp")))

(define-public crate-imagesize-0.10.1 (c (n "imagesize") (v "0.10.1") (h "0lfrrjqk3pqjk6cyr051fbpg7cc1afaj5mlpr91w1zpvj8gdl6fz")))

(define-public crate-imagesize-0.11.0 (c (n "imagesize") (v "0.11.0") (h "1bww55igw5gwf6hvzawdw9hl3sjsasqs2m1jx2qjh5scandx8amp")))

(define-public crate-imagesize-0.12.0 (c (n "imagesize") (v "0.12.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "114jvqiyv13il1qghv2xm0xqrcjm68fh282hdlzdds6qfgsp7782")))

