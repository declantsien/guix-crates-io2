(define-module (crates-io im ag image-base64) #:use-module (crates-io))

(define-public crate-image-base64-0.1.0 (c (n "image-base64") (v "0.1.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0lnfxp1wm9wlpqxidi4idlw9lp6b689k7bggj9gc56vy7czyrglf")))

