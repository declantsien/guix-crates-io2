(define-module (crates-io im ag image-organizer) #:use-module (crates-io))

(define-public crate-image-organizer-0.1.0 (c (n "image-organizer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "061ba3xm6v8vdwg0bix7dbrcln3y8ilcimj1w3zpx72z1hhcadl7")))

(define-public crate-image-organizer-0.1.1 (c (n "image-organizer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "0m2swm17lk2bd1glj3ykmgk4r6n5qmvd5wsp9mndbgn996b1nw7a")))

(define-public crate-image-organizer-0.1.2 (c (n "image-organizer") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "02i9wg4nalqlb9nmbjqpl8nbq70rl5sdfwy73sdczmsyykh6f0z7")))

