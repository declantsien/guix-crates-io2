(define-module (crates-io im ag imag-counter) #:use-module (crates-io))

(define-public crate-imag-counter-0.2.0 (c (n "imag-counter") (v "0.2.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "libimagcounter") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagerror") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagutil") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)))) (h "1icywa1plrqfp6xn747fzcazvcy8m5kiwm4zafihkzhqd39cw93i")))

(define-public crate-imag-counter-0.3.0 (c (n "imag-counter") (v "0.3.0") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "libimagcounter") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagerror") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagutil") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)))) (h "0knqq0939bg2kxbg8yh92i1c2vch7vix1i4y9aig3mqd5hb8kz7l")))

(define-public crate-imag-counter-0.4.0 (c (n "imag-counter") (v "0.4.0") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "libimagcounter") (r "^0.4.0") (d #t) (k 0)) (d (n "libimagerror") (r "^0.4.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.4.0") (d #t) (k 0)) (d (n "libimagutil") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "version") (r "^2.0.1") (d #t) (k 0)))) (h "08injlyzqx991w69r959b23d2aq9gpnipb8lh61nxlynmvs3s5hb")))

