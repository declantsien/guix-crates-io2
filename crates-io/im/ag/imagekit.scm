(define-module (crates-io im ag imagekit) #:use-module (crates-io))

(define-public crate-imagekit-0.1.0-beta+1 (c (n "imagekit") (v "0.1.0-beta+1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "multipart" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1261g1swkwgxnxxgy3g1k1a8pfzg9m8m1hngrmybdbz81mxzvb4b")))

