(define-module (crates-io im ag image-meta) #:use-module (crates-io))

(define-public crate-image-meta-0.1.0 (c (n "image-meta") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1vqqx6ikd1g2r3yj13xbjvc9fhj8ddwfq36ci9yn4ji5pg5n225h")))

(define-public crate-image-meta-0.1.1 (c (n "image-meta") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "03dbwc8gh9h477wqakdsdi4rzqjvr370s6msp34m82ax4asafb8s")))

(define-public crate-image-meta-0.1.2 (c (n "image-meta") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1anf65f63p59v9dgvdl41mg1frpadldhhhzinjin489b0y5iv0xm")))

