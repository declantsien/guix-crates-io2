(define-module (crates-io im ag imagezero-sys) #:use-module (crates-io))

(define-public crate-imagezero-sys-0.1.1 (c (n "imagezero-sys") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "107x2q9kxg3dg4fv07lfs3zi7a6q4bhlpsdf5s8pp2ahrr128d1z")))

