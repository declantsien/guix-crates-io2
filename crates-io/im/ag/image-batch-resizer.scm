(define-module (crates-io im ag image-batch-resizer) #:use-module (crates-io))

(define-public crate-image-batch-resizer-0.1.0 (c (n "image-batch-resizer") (v "0.1.0") (d (list (d (n "failure") (r "= 0.1.1") (d #t) (k 0)) (d (n "image") (r "= 0.19.0") (d #t) (k 0)) (d (n "structopt") (r "= 0.2.7") (d #t) (k 0)) (d (n "structopt-derive") (r "= 0.2.7") (d #t) (k 0)) (d (n "vlog") (r "= 0.1.2") (d #t) (k 0)))) (h "0yzgl2qns088kw5ss0qcy7rj5cl3lij8p143nmgf4cfczqyxhxlf")))

(define-public crate-image-batch-resizer-0.2.0 (c (n "image-batch-resizer") (v "0.2.0") (d (list (d (n "failure") (r "= 0.1.1") (d #t) (k 0)) (d (n "glob") (r "= 0.2.11") (d #t) (k 0)) (d (n "image") (r "= 0.19.0") (d #t) (k 0)) (d (n "structopt") (r "= 0.2.7") (d #t) (k 0)) (d (n "structopt-derive") (r "= 0.2.7") (d #t) (k 0)) (d (n "vlog") (r "= 0.1.2") (d #t) (k 0)))) (h "0y5q6gpaah0bhwkgrgcd8qk42b8n00z8vpl3x0cvgd1zra5pf6z3")))

