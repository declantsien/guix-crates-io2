(define-module (crates-io im ag image2pdf) #:use-module (crates-io))

(define-public crate-image2pdf-0.1.0 (c (n "image2pdf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.5") (f (quote ("embedded_images"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "061sjx55anbdaavi4x0bahjny0cm5qwj5nsl63hj7g5naszyshsm")))

