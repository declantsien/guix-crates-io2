(define-module (crates-io im ag image-data) #:use-module (crates-io))

(define-public crate-image-data-0.1.0 (c (n "image-data") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0ggsyhxm2715lrsn79shi14ip6bqdn10pgx77bmz1n8x70xks9hf")))

(define-public crate-image-data-0.2.0 (c (n "image-data") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1cw1bm6295vp7awx07f4pz6hbijbik9hvfrkm7y40qv8ddrpzppv")))

