(define-module (crates-io im ag image-canvas) #:use-module (crates-io))

(define-public crate-image-canvas-0.0.1-a (c (n "image-canvas") (v "0.0.1-a") (h "1rynahxy2q0bma7648drzvqlkgmbibhf8lj35kpf284sp0lpjkkb")))

(define-public crate-image-canvas-0.0.1 (c (n "image-canvas") (v "0.0.1") (d (list (d (n "zerocopy") (r "^0.2.2") (d #t) (k 0)))) (h "0ma3b33ymj2282ical3735l5nhmxpyc81piav0lm3pivsi82x1ns")))

(define-public crate-image-canvas-0.0.2 (c (n "image-canvas") (v "0.0.2") (d (list (d (n "zerocopy") (r "^0.2.2") (d #t) (k 0)))) (h "0lf2i04iamkzmfpq13ck9ni4fykycxpni1a3sk9440x0sqa1cvnr")))

(define-public crate-image-canvas-0.0.3 (c (n "image-canvas") (v "0.0.3") (d (list (d (n "zerocopy") (r "^0.2.3") (d #t) (k 0)))) (h "0alfbfcwiphh2m2ri3byg0i5xp13zn1szxqsv10y37kryxw1cw05")))

(define-public crate-image-canvas-0.0.4 (c (n "image-canvas") (v "0.0.4") (d (list (d (n "zerocopy") (r "^0.2.4") (d #t) (k 0)))) (h "07fv1xjqsnw3ljhqs5lm6g20s31bjg5l7n0rs9hg3w2d763z8nl2")))

(define-public crate-image-canvas-0.0.5 (c (n "image-canvas") (v "0.0.5") (d (list (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "0d7nfi546cbbz2cnlxs3hmn86g6jbdxlp5qmdpq7ymgqcb264p6q")))

(define-public crate-image-canvas-0.0.6 (c (n "image-canvas") (v "0.0.6") (d (list (d (n "zerocopy") (r "^0.2.8") (d #t) (k 0)))) (h "1f4ymzgx859d1hhqa99mvlpll01czg3d5zqw1vvqypkh1qik3lr5")))

(define-public crate-image-canvas-0.1.0 (c (n "image-canvas") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)))) (h "1xbwnj3m4llylc4rm7lgnl65xkgz7w01c5bj5cp9lpbzjyjni4bd")))

(define-public crate-image-canvas-0.2.0 (c (n "image-canvas") (v "0.2.0") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "image-texel") (r "^0.1.0") (d #t) (k 0)))) (h "190ym2fjr22pfh95pffs4zn7mpcxdhda3256dnwb5l7c6sydp0hd")))

(define-public crate-image-canvas-0.2.1 (c (n "image-canvas") (v "0.2.1") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.1.1") (d #t) (k 0)))) (h "12raxj0xy0yac433cxzv21cp9xgddjd69rwg81nnrc93pvahhzzf")))

(define-public crate-image-canvas-0.2.2 (c (n "image-canvas") (v "0.2.2") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.1.1") (d #t) (k 0)))) (h "09plz534r7zc384igzpvvxk0711fa89xzv00740ibas582sf08x6")))

(define-public crate-image-canvas-0.2.3 (c (n "image-canvas") (v "0.2.3") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.1.1") (d #t) (k 0)))) (h "1p7dz46cpw6xirq997hi2bd75qzq0z0vzgs3xlszax3d9dgwm3s6")))

(define-public crate-image-canvas-0.2.4 (c (n "image-canvas") (v "0.2.4") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.1.2") (d #t) (k 0)))) (h "1n93n3yx39pwwqak08fq4gkjszxr3v54cyiq05xx2p2s0gq3kk83")))

(define-public crate-image-canvas-0.3.0 (c (n "image-canvas") (v "0.3.0") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.2.0") (d #t) (k 0)))) (h "1z6igr1mhmjdx2zkamh1hc9x992958l50kqim7qlv3xmm6y13bf2")))

(define-public crate-image-canvas-0.3.1 (c (n "image-canvas") (v "0.3.1") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.2.1") (d #t) (k 0)))) (h "11rgk0v5aign2lkliag8y90qjkxzgn6cp0h7x4qb8qbdxm8yffhm")))

(define-public crate-image-canvas-0.4.0 (c (n "image-canvas") (v "0.4.0") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.3.0") (d #t) (k 0)))) (h "1blbv3d9mbmqhw8ny80312128alyva83nar2fdsr6745ks62k4mv")))

(define-public crate-image-canvas-0.4.1 (c (n "image-canvas") (v "0.4.1") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.3.0") (d #t) (k 0)))) (h "1p0k27vvwxv18y2ndnflc86mvi9np4p6gmkxpjjgkk1ii3hacikr")))

(define-public crate-image-canvas-0.4.2 (c (n "image-canvas") (v "0.4.2") (d (list (d (n "brunch") (r "^0.2.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "image-texel") (r "^0.3.0") (d #t) (k 0)))) (h "1i5byc1xhk4jjrr2fnz5nk3zv1mr8g5z8qbj6m67np5w7r82yini")))

