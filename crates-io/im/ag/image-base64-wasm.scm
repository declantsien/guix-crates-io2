(define-module (crates-io im ag image-base64-wasm) #:use-module (crates-io))

(define-public crate-image-base64-wasm-0.2.0 (c (n "image-base64-wasm") (v "0.2.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "16apccwvj35qdqx5mshcwfnqsxq5yqw49jrz308lfxr9cyib0ydl")))

(define-public crate-image-base64-wasm-0.3.0 (c (n "image-base64-wasm") (v "0.3.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0jsikfgy108mf7pp883qjzbnch9scm262lhfbmrmf24f2fszhd32")))

(define-public crate-image-base64-wasm-0.4.0 (c (n "image-base64-wasm") (v "0.4.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1vmwshp22lvajrfvhcwmfl2blqqzj56d0ja7g6lkyvh8hrbnlqhh")))

(define-public crate-image-base64-wasm-0.5.0 (c (n "image-base64-wasm") (v "0.5.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0q9v42i839d40ab0v5655wd88p4gpnwm1mffj8g6w1d3pim2adxw")))

(define-public crate-image-base64-wasm-0.6.0 (c (n "image-base64-wasm") (v "0.6.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0jijb39w0mk6h05b00x5266gq2l2dgp21rab15qwiq1x2qclpbsg")))

