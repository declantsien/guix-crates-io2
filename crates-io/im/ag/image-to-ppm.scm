(define-module (crates-io im ag image-to-ppm) #:use-module (crates-io))

(define-public crate-image-to-ppm-0.1.0 (c (n "image-to-ppm") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "146dp2iymlgr4nirq0w4lkzr6fiihgg866v79l90hhda9r1892v5")))

(define-public crate-image-to-ppm-0.1.1 (c (n "image-to-ppm") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "1cz1rpqpwsdj919cm4lxjrzrr8f0lpa7mjsa1sj2g72xcv1albxb")))

