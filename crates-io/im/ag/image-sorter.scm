(define-module (crates-io im ag image-sorter) #:use-module (crates-io))

(define-public crate-image-sorter-0.1.0 (c (n "image-sorter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "08ak90pvnbp681gmga2aj1qkr4zlsmlpg3bqmjm4zg0gpwv2mlrx")))

(define-public crate-image-sorter-0.1.1 (c (n "image-sorter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "016j88cm2vlrjk588qdamdb59y75nlqyrnkvl8v4wx0vglilz4i4")))

(define-public crate-image-sorter-0.2.0 (c (n "image-sorter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "0bzfzb0vsr3xs4zmm41v6cym77qnfxhnkpxsyrprm2dmfdnicpzc")))

(define-public crate-image-sorter-0.3.0 (c (n "image-sorter") (v "0.3.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (k 0)) (d (n "subprocess") (r ">=0.2.6, <0.3.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "tui") (r ">=0.12.0, <0.13.0") (f (quote ("termion"))) (k 0)))) (h "1pxfxdwg9a5vw75h6zdwlnm6ggb0l1ccg65m0ynk8jn34848iddj")))

(define-public crate-image-sorter-0.3.1 (c (n "image-sorter") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "02kx5r8gg4bmawdqf149r0khfg0fhacllymifl1wz1z5yg1302jr")))

(define-public crate-image-sorter-0.3.2 (c (n "image-sorter") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "1y0z2nl1dx9wp1vkg3kn19xwzbpv4nw0q0cwvpjjfhzs3c13xdhc")))

(define-public crate-image-sorter-0.3.3 (c (n "image-sorter") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("termion"))) (k 0)))) (h "0b5qwj749q5hklfif0x5pcrzjm8mwb6gn7vjad478d0rldyyrvdf")))

(define-public crate-image-sorter-0.4.0 (c (n "image-sorter") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (f (quote ("termion"))) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1lhqwai24kl9amff8qvgpi5grvii1llfviwv3kldvfcn8w60jn09")))

(define-public crate-image-sorter-0.4.1 (c (n "image-sorter") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (f (quote ("termion"))) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1jai7x9aiilbw8m3hzxxksc48z6yrqk29mp4k70r9rlb783s4iwn")))

