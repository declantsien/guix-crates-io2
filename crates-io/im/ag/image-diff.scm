(define-module (crates-io im ag image-diff) #:use-module (crates-io))

(define-public crate-image-diff-0.1.0 (c (n "image-diff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "0ypzy3xx419fwx3sx4iw5f91k4pd74vi0h88vp5pafm2kls9sdi7")))

(define-public crate-image-diff-0.1.1 (c (n "image-diff") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "0mbx427qdss9qfacaim3aiawj43wsbj9sjqv5028i6r9y3vijjjm")))

(define-public crate-image-diff-0.1.11 (c (n "image-diff") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "03zxhgq4kxhcc96x8vkifnlxxyiibfcralrglzvnxhz4dhwf2dsc")))

(define-public crate-image-diff-0.1.12 (c (n "image-diff") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "0n4zx3skidgi4d4m51agq69qyvbjw4mj3mp00a67kjawnxwmahic")))

(define-public crate-image-diff-0.1.13 (c (n "image-diff") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "0gcd8rw4kv2nsqkh5i3349fzx17dsvak4gabifd9gpqs2rzkax8c")))

