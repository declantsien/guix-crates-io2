(define-module (crates-io im ag imagefmt) #:use-module (crates-io))

(define-public crate-imagefmt-0.1.0 (c (n "imagefmt") (v "0.1.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1k3a3f5mm299siyp1x0px0847pw72rykwy9zv1bvgdwl8z9srlv5")))

(define-public crate-imagefmt-0.2.0 (c (n "imagefmt") (v "0.2.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "16vjdhq70hvayif9va1qkfinz69hnx9bl528nl57infs91l3iva5")))

(define-public crate-imagefmt-0.3.0 (c (n "imagefmt") (v "0.3.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "18ngzcwi8xxpjnb8ahd17p0wfg074axxxp15g7ha9vssba6g7fr1")))

(define-public crate-imagefmt-0.3.1 (c (n "imagefmt") (v "0.3.1") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "13nbdlg9kbsn7vl57q4crnkjarjwzbnnjph9cfabgic4fiy4wakg")))

(define-public crate-imagefmt-0.3.2 (c (n "imagefmt") (v "0.3.2") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0fnd2vgll9fhbn2q6f4p2mkvi0g4yc13rcjak2yxnhad7dn52zas")))

(define-public crate-imagefmt-0.4.0 (c (n "imagefmt") (v "0.4.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0qnr0xkcp879ykix6c2agvidiyr1cpki922g9dw5cyd66wqf7pnv")))

(define-public crate-imagefmt-0.5.0 (c (n "imagefmt") (v "0.5.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1vvzkxwa1ds13wlxsamkbrw599clxz0jgdkn50xv4nkm0kcmwmv1") (f (quote (("tga") ("png") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-0.5.1 (c (n "imagefmt") (v "0.5.1") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "182as89329whnwimaqqqpx5ydpip8brpb6720hxp194v8gsnfak1") (f (quote (("tga") ("png") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.0.0 (c (n "imagefmt") (v "1.0.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1sggyl2d3p6chq016z7m8h2dqw3cq6zhi63zyj8h1djjd27q5gry") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.0.1 (c (n "imagefmt") (v "1.0.1") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1lhr2m1nnavxs2lgzv1sgqd039mcvjdbx63vnn386yi5n1hp87sj") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.0.2 (c (n "imagefmt") (v "1.0.2") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "061ffkiy26w0zpnhprqvz6s24j420hn1nvfn32cq9j4vc0n8sylh") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.0.3 (c (n "imagefmt") (v "1.0.3") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1zwgi881w1lxvlhj4rxdh4ryq6fvxq44wa6grng709ccd9xqa3rq") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.1.0 (c (n "imagefmt") (v "1.1.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1jpld62drvwpfxbvp9wa8sqd7rcn475l090wfzyzw4zravrimf2l") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-1.2.0 (c (n "imagefmt") (v "1.2.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "04zqq647gz1285ihjpq5yy5pfp2npd75pqq1wq275sbyakq20i1b") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-2.0.0 (c (n "imagefmt") (v "2.0.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "05g99xdrk92z4ws884l37vjbsqml65yk3h7jlhhsbb7b34b842ng") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-2.1.0 (c (n "imagefmt") (v "2.1.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1hssmy6vkhbx517gq8lrzaxf6lzsvc9hrhfwcpyhdgm12zc5dggx") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-2.1.1 (c (n "imagefmt") (v "2.1.1") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0123dpzacplh6ywrs93rxp8adinqa9lbwnc483mdjq7fca9z23cb") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-3.0.0 (c (n "imagefmt") (v "3.0.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "011gmn5mqwbw2x61ls99izzqmk95iq11jkljwgv7kghqkxxfrs39") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp")))) (y #t)))

(define-public crate-imagefmt-3.0.1 (c (n "imagefmt") (v "3.0.1") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1dmxhc18wm1b9nfnbrk3wh8cni7xx2zmkm9sr1z9knr9s7ii92h3") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

(define-public crate-imagefmt-4.0.0 (c (n "imagefmt") (v "4.0.0") (d (list (d (n "flate2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0brn5fpwnn7q2lynnfzx8hzqq600821q719cw9g4d3mgpnblmkvc") (f (quote (("tga") ("png" "flate2") ("jpeg") ("default" "png" "jpeg" "bmp" "tga") ("bmp"))))))

