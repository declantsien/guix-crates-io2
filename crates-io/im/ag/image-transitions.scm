(define-module (crates-io im ag image-transitions) #:use-module (crates-io))

(define-public crate-image-transitions-0.1.0 (c (n "image-transitions") (v "0.1.0") (d (list (d (n "cuda_builder") (r "^0.3.0") (d #t) (k 1)) (d (n "cust") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1pfbgg04shm4gb855zvkjz17rmwx95xna38qhi4p3ni5ladvbz25")))

(define-public crate-image-transitions-0.1.1 (c (n "image-transitions") (v "0.1.1") (d (list (d (n "cuda_builder") (r "^0.3.0") (d #t) (k 1)) (d (n "cust") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zfv16vd8wafr4l4fksw7f3yfvf07cv7yf58cc7fkr29cipl1nn1")))

(define-public crate-image-transitions-0.1.2 (c (n "image-transitions") (v "0.1.2") (d (list (d (n "cuda_builder") (r "^0.3.0") (d #t) (k 1)) (d (n "cust") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15s8dg5hqfh0iprqx4r8hbq0yphigz213ss1wvkqd69mzqz20wl3")))

(define-public crate-image-transitions-0.1.3 (c (n "image-transitions") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "cuda_builder") (r "^0.3.0") (d #t) (k 1)) (d (n "cust") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dq0ds01smrm4qah6jn93xl3r511icx0kmk4zp7pyg3vfsi0wjpx")))

