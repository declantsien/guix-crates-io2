(define-module (crates-io im ag image-watcher) #:use-module (crates-io))

(define-public crate-image-watcher-0.0.1 (c (n "image-watcher") (v "0.0.1") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "02hdgyimz4960sn3his63ixs93mklsgxcgprnvy4s5msbbhgbvf4")))

(define-public crate-image-watcher-0.0.2 (c (n "image-watcher") (v "0.0.2") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ba49ns8nkr05n6f05lasd8l8pnsm676bcgwhq2h7xfk4fgw979b")))

(define-public crate-image-watcher-0.0.3 (c (n "image-watcher") (v "0.0.3") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "03ii7ry5q79m4rgp29h8xspd46xc0aqf9v8zfvqrxwil2n8qzfpm")))

(define-public crate-image-watcher-0.0.4 (c (n "image-watcher") (v "0.0.4") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "093qcaqvn9zscvlpf8b17mv5wqwbkq7l1gh3y6dfshd23wxkd2gh")))

(define-public crate-image-watcher-0.0.5 (c (n "image-watcher") (v "0.0.5") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "02nc7k6h5508facj2d3ns2aivzq1h7ybyr8x3a9a3lfr4dwcdr0k")))

(define-public crate-image-watcher-0.0.6 (c (n "image-watcher") (v "0.0.6") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12j6ffh3r7f450n652h1ljrpvrksfxhrqnrgb7qzc6rhqfn9h8j1")))

(define-public crate-image-watcher-0.0.7 (c (n "image-watcher") (v "0.0.7") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1d9gkpf5p9d4nsm5ljydpd7677pr7nw149avwa4r2p6lp3wvfn3v")))

(define-public crate-image-watcher-0.0.9 (c (n "image-watcher") (v "0.0.9") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12hi4bvqlxkdxpf70krpj4d6bmjhr0glwfvgcq01v34sijamdnax")))

(define-public crate-image-watcher-0.0.10 (c (n "image-watcher") (v "0.0.10") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0sn6p881hs0smci3cdjnzq11dd0irswm883hvpyxwjlfknbj6kqd")))

(define-public crate-image-watcher-0.0.11 (c (n "image-watcher") (v "0.0.11") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^0.0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1n2x5b7g8dn4sprc977wqbrs47sk9q80vjzfi2dbfr0nqn95vjd1")))

(define-public crate-image-watcher-0.0.12 (c (n "image-watcher") (v "0.0.12") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^0.0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11sv39y95a364gjvdybb2a30s9yb884jf4b1qd4iprnmb9mdnihq")))

(define-public crate-image-watcher-0.0.13 (c (n "image-watcher") (v "0.0.13") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^0.0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "02k3z8h2js82pn0x69i7nmhh8q47r8b4gfrm79wsfazvlp10g94n")))

(define-public crate-image-watcher-0.0.16 (c (n "image-watcher") (v "0.0.16") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ngknqf4lwi3ixh3n4spypw4n9f5ppa14ws52f185jr4x7888p6j")))

(define-public crate-image-watcher-0.0.17 (c (n "image-watcher") (v "0.0.17") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0jrki9glim6najkwwbxzadkhbvk4vj8gmzg2wslfnj6vny4z7vkx")))

(define-public crate-image-watcher-0.0.18 (c (n "image-watcher") (v "0.0.18") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "100dnqfmrs6g5dqf21rfd2zgfppiyg3a4bqf2x5asqqhff6gf7zi")))

(define-public crate-image-watcher-0.0.19 (c (n "image-watcher") (v "0.0.19") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "09r5ph6x6c3jdvwdhgiky67dlf2lnjaq0q6y7i0y4blj77p4ymb8")))

(define-public crate-image-watcher-0.0.20 (c (n "image-watcher") (v "0.0.20") (d (list (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0xjmr288ic91ilnq2s75cgmlx4vq82sv4qij2if5casvsaskwjjz")))

(define-public crate-image-watcher-0.0.21 (c (n "image-watcher") (v "0.0.21") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "18qy88lvagm0wcc9sp1mp3a8bnriwd0f6m0rmgi3p1l02zs91wcl")))

(define-public crate-image-watcher-0.0.22 (c (n "image-watcher") (v "0.0.22") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0higj7073a6ai28s20jvxkw8aigp8q3k8lxvzfm9si4wi0p2jajd")))

(define-public crate-image-watcher-0.0.23 (c (n "image-watcher") (v "0.0.23") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "file-watcher") (r "0.0.*") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "read_input") (r "^0.8") (d #t) (k 0)) (d (n "set-error") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "07lm0vbvxfd4v194vy1s4vkyjinvk3lkg2pim8hda19c5v0h62sv")))

