(define-module (crates-io im ag image_colors) #:use-module (crates-io))

(define-public crate-image_colors-0.2.2 (c (n "image_colors") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dr77iikj1gc9mshv1nxjghgnsf02ifcqwbh33wbwid4n7040s5f")))

(define-public crate-image_colors-0.3.2 (c (n "image_colors") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jp2pijjlfr39y4037c54igrjiz6lvnk3gxw0csp9dyvflmn1jwn")))

(define-public crate-image_colors-0.4.2 (c (n "image_colors") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g8cinkcc0qaz5awxj64fsq7w0g09l03cr37v3mah9f0hvf2qkax")))

(define-public crate-image_colors-0.4.3 (c (n "image_colors") (v "0.4.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0g165hhfld1ar3sjs3s3v4826pfad1f9i6irv7za27qgwxy7gq4i")))

(define-public crate-image_colors-0.5.0 (c (n "image_colors") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0s3qj5x8nvabkfl3mi609li3854dg0z1pvfl8iz5cnjczzzx9xyq")))

(define-public crate-image_colors-0.5.1 (c (n "image_colors") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0cdl9w7pwjj9n2pnjbiima7sspgqfdryjhm4l98f5g2awzq7ibdl")))

