(define-module (crates-io im ag imager-rs) #:use-module (crates-io))

(define-public crate-imager-rs-0.1.0 (c (n "imager-rs") (v "0.1.0") (h "0pypha7bwjsvnmgx3cd2c19w5gp9isplhcg2k27dpba33hrxjpgl")))

(define-public crate-imager-rs-0.2.0 (c (n "imager-rs") (v "0.2.0") (h "14c7bxw91dgcspr23xmgsj6qvmm3pybfdcq5ikqi7dxcin5azqas")))

