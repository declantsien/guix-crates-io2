(define-module (crates-io im ag image-texel) #:use-module (crates-io))

(define-public crate-image-texel-0.1.0 (c (n "image-texel") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)))) (h "1z5zkwlv6kwvby16p2p28j267dshk8dmbd1cksl0kjsdf3l2nq8c")))

(define-public crate-image-texel-0.1.1 (c (n "image-texel") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)))) (h "0ya474i0ahh23pwvl64xqaiq3cpwac2syl9yb9snvg94gc9vbcwq")))

(define-public crate-image-texel-0.1.2 (c (n "image-texel") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "14i79w0d4jqlpdzd3hbkv06cn8mg0a0n07l3lbgcp3if8qqfma33") (r "1.61")))

(define-public crate-image-texel-0.2.0 (c (n "image-texel") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ym6i27l4d8njfcwk0vydxgqy42zr40mkw26n1pcs166fjn1lci3") (r "1.61")))

(define-public crate-image-texel-0.2.1 (c (n "image-texel") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "0nk99ijpqhsc4v88c2jhqx4920s54h5bvjgmgza220f74syac4vi") (r "1.61")))

(define-public crate-image-texel-0.3.0 (c (n "image-texel") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "077208jzc0lj7427p30r4ba6pplasf4r1rv0siigflhwl6dvk1i2") (r "1.61")))

(define-public crate-image-texel-0.3.1 (c (n "image-texel") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.3") (f (quote ("derive"))) (d #t) (k 2)))) (h "14flwb6dcdx1ya2ljb2fpfx0dmkmlc0nx9y11758fm68qm664sjs") (r "1.61")))

