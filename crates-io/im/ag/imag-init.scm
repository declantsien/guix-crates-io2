(define-module (crates-io im ag imag-init) #:use-module (crates-io))

(define-public crate-imag-init-0.6.0 (c (n "imag-init") (v "0.6.0") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1zy4z866rj42pa8ahyriyg6lg69ww2g3cak8wa72f0s51hpyyass")))

(define-public crate-imag-init-0.6.1 (c (n "imag-init") (v "0.6.1") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1mvdh9pyn96fz8cx324pqh73gh9qwg6w823g247xkgx9jaklzb0a")))

(define-public crate-imag-init-0.6.2 (c (n "imag-init") (v "0.6.2") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "17qk2z99bc5jir19qqw1f34wys7qg5h303wf1v7v6x8s7j82jdjx")))

(define-public crate-imag-init-0.6.3 (c (n "imag-init") (v "0.6.3") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1nsg8vmlbgyzqr2w4hi7ig2x8caqki8ipwy2cz24rvvnrlzppqly")))

(define-public crate-imag-init-0.6.4 (c (n "imag-init") (v "0.6.4") (d (list (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0wc046dqr3g0ka5k2m1mj7l9cj6a2ryy4jl2g7n0l07ddrlc47wh")))

(define-public crate-imag-init-0.7.0 (c (n "imag-init") (v "0.7.0") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.7.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.7.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0h1mf522v8cn3ysv895b471wjx4w1z9khy7f3qh1dlf4yfkz0gi0")))

(define-public crate-imag-init-0.7.1 (c (n "imag-init") (v "0.7.1") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.7.1") (d #t) (k 0)) (d (n "libimagrt") (r "^0.7.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0cgjh9f0a6rh1pm38y3qlsir50f71dnh0n02wm9589i6h2ra53b8")))

(define-public crate-imag-init-0.8.0 (c (n "imag-init") (v "0.8.0") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.8.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "15md3s2xazr6f1m1bad919bh8lf7jmkplsr6hxigibr96am1lz9s")))

(define-public crate-imag-init-0.9.0 (c (n "imag-init") (v "0.9.0") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.9.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "14550slbl25z31d8ssm44wjfnlh4y3f1d71c0zkrmb1dp09k7gkf")))

(define-public crate-imag-init-0.9.1 (c (n "imag-init") (v "0.9.1") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.9.1") (d #t) (k 0)) (d (n "libimagrt") (r "^0.9.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0wfym7gawv3z1ciscwq9053rcmn0bf2g4rbgld789ig2c1vss9vv")))

(define-public crate-imag-init-0.9.2 (c (n "imag-init") (v "0.9.2") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.9.1") (d #t) (k 0)) (d (n "libimagrt") (r "^0.9.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1j15q1ddcbdx7svff6b20kg5dx5g8yrdkspm4nj3pvps2ws8yvd7")))

(define-public crate-imag-init-0.9.3 (c (n "imag-init") (v "0.9.3") (d (list (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "libimagerror") (r "^0.9.3") (d #t) (k 0)) (d (n "libimagrt") (r "^0.9.3") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1p1ci4lz88nvg4d6nf9wfw3kgrmqsz9bhpjz8sf8n7l2ygbm80nv")))

(define-public crate-imag-init-0.10.0 (c (n "imag-init") (v "0.10.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libimagerror") (r "^0.10.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.10.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 2)))) (h "15nhm8fabvcs09hrhvb1cccj7lhbc56ha33ahla1y7a1crxmpp5g")))

(define-public crate-imag-init-0.10.1 (c (n "imag-init") (v "0.10.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libimagerror") (r "^0.10.1") (d #t) (k 0)) (d (n "libimagrt") (r "^0.10.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 2)))) (h "19dcy1w5fxqcr0na9nijha4d4g5m5qdg2g6d6yx7yygqp7bwfgng")))

