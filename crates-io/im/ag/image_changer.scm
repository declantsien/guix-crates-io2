(define-module (crates-io im ag image_changer) #:use-module (crates-io))

(define-public crate-image_changer-0.1.0 (c (n "image_changer") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)))) (h "0kb7880vb2xgsgx3z3ay8c98w7404b7y2wqwhjif6qbys1x6j1a6")))

(define-public crate-image_changer-0.2.0 (c (n "image_changer") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "19kajfv2pm4ag8vi97jg1b0zpxh51xbm2hs43a1inqfmm9639a02")))

(define-public crate-image_changer-0.3.0 (c (n "image_changer") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0hw492ijs4lprbpg8b9nwqj1g72aflwcq9g4syln4jf3658y9f1x")))

(define-public crate-image_changer-0.4.0 (c (n "image_changer") (v "0.4.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1hqw7pqhzsbif3vm9kgbp51crzzdqkljvx7ij9c030gqraqidhsm")))

(define-public crate-image_changer-0.5.0 (c (n "image_changer") (v "0.5.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0qqa7mj81k875qig1in3mi1acsv893r2i7x505y4pabnyxs0agq7")))

(define-public crate-image_changer-0.5.1 (c (n "image_changer") (v "0.5.1") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "0hh3svzprrg0dy9sip55sm35k1ck7bzq7rhk2y4594wvcslmzxdw")))

(define-public crate-image_changer-0.6.0 (c (n "image_changer") (v "0.6.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "08ap6vnpr4jwffq6nxmcqvj6h3p83g7zhr4cmm2bdl6xwcib09x2")))

(define-public crate-image_changer-0.6.1 (c (n "image_changer") (v "0.6.1") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "0s1zhqvy2blb7ysxsgi7p74cv5ghcpcgy7ib6x7hh0b1z28jk8zg")))

