(define-module (crates-io im ag image_to_space_engineers_lcd) #:use-module (crates-io))

(define-public crate-image_to_space_engineers_lcd-0.1.0 (c (n "image_to_space_engineers_lcd") (v "0.1.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "1vy4xz4lxwqma7vyf80kwyvzw1q4cm2gdaxa6412idjshxfbpw9q") (y #t)))

(define-public crate-image_to_space_engineers_lcd-0.1.1 (c (n "image_to_space_engineers_lcd") (v "0.1.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "0kb88igyjvk853drj0myqqrgw5bgszcawlz1dlnb93k8q8hhppxl") (y #t)))

(define-public crate-image_to_space_engineers_lcd-0.1.2 (c (n "image_to_space_engineers_lcd") (v "0.1.2") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "0131xs8vg36n4wkg0n1dwhi0rkfrbnhs6jfj7dbpmmjk954akka3") (y #t)))

(define-public crate-image_to_space_engineers_lcd-1.0.0 (c (n "image_to_space_engineers_lcd") (v "1.0.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "03bq4nk7b9w87bdgk99jakjh62f48qwbqsplzjrwws72dcpj4bcx")))

(define-public crate-image_to_space_engineers_lcd-1.0.1 (c (n "image_to_space_engineers_lcd") (v "1.0.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "0lgsgkmrsxsbsc8m21dc52fn2xa26qaxqr5730r9yccq25n0driv")))

(define-public crate-image_to_space_engineers_lcd-1.0.2 (c (n "image_to_space_engineers_lcd") (v "1.0.2") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "1p3kirffzs6fpwmv2m25jw7yk8j6lq3y8hib7j0daqbfi4k56g8j")))

