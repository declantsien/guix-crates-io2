(define-module (crates-io im ag imagequant-sys) #:use-module (crates-io))

(define-public crate-imagequant-sys-0.1.0 (c (n "imagequant-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "rgb") (r "^0.5.8") (d #t) (k 0)))) (h "1v1fvyxl7a2yj2z5shvf3bm1v11czrbxbh0irwma8swspn52yi37") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-0.1.1 (c (n "imagequant-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "rgb") (r "^0.5.8") (d #t) (k 0)))) (h "1x8nkaajvsjr48a94qrlpziwdq37lyj7jzpw8yi3ygq9ah4hnzgz") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.10.0 (c (n "imagequant-sys") (v "2.10.0") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "rgb") (r "^0.5.8") (d #t) (k 0)))) (h "0mivrk92h5061nklk27vmk6f021x5ic1fza1mjm422qvz43lpfbx") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.10.1 (c (n "imagequant-sys") (v "2.10.1") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "0bc6p8kcajldcrpzh92ha8rf0x0xs5idh2p9lbfkpcx9r5iy3r1z") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.10.2 (c (n "imagequant-sys") (v "2.10.2") (d (list (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "08cnr50xxmakq67y9x1dix4yj39gbs8lpbmg8cjnm8fwisjx0d9l") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.10.3 (c (n "imagequant-sys") (v "2.10.3") (d (list (d (n "gcc") (r "= 0.3.51") (d #t) (k 1)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "1rqydjih87snfvc4h8jmv9hkx48prpnqbqr1vnxk0d87p25l1aj7") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.0 (c (n "imagequant-sys") (v "2.11.0") (d (list (d (n "cc") (r "^1.0.1") (d #t) (k 1)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "05b46c5yvvv3kywyqwmfhcnclgnqa2gq2mvdhmq91djn1m9lvdz0") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.1 (c (n "imagequant-sys") (v "2.11.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1il1q2ykd9mxbqx84dzxh2vdss2bkn2japbwvmqwdxaij767hjbl") (f (quote (("sse") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.2 (c (n "imagequant-sys") (v "2.11.2") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0wylwn6apqinbxipp9wma7y0h903aqc5bxzc9mlg03fviwijmqp6") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.3 (c (n "imagequant-sys") (v "2.11.3") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0zk0z31in5i93iija8bricndqnlm720k302rz4hisi3n4dnwqd9p") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.4 (c (n "imagequant-sys") (v "2.11.4") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "12clikhv957qg9ywc747x7769yk855ws9s8jz5fdpqmlz5dvllv3") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.5 (c (n "imagequant-sys") (v "2.11.5") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "1vq871ap5a2a3yk1d0204wr9ihcf9y5iirpmcjp33f5k19pz9i3r") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.6 (c (n "imagequant-sys") (v "2.11.6") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "049ym1l39dgmw28k3fqc5x01zhj6jwmm80c31gnvx55rzrdhcwb2") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.7 (c (n "imagequant-sys") (v "2.11.7") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "0s1wff7c5byxhcp0b2bbim7n3by7sbczx6rx09r8xaki0gpnnckg") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.8 (c (n "imagequant-sys") (v "2.11.8") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "1zb5dixhwwcl7wn0vsmhrxap0nnx1qlsyy8rygcpki3saimllspk") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.11.9 (c (n "imagequant-sys") (v "2.11.9") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.5") (d #t) (k 0)))) (h "046ql83nk93p4bz1h6x9g7jb2881y99lqxv8v1pj0lgkmrlxp6z6") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t)))

(define-public crate-imagequant-sys-2.12.0 (c (n "imagequant-sys") (v "2.12.0") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.9") (d #t) (k 0)))) (h "1asqrg5jqajf5n88sx7f6yw8jikc2zqb5vs635vwrq410cdi1mwr") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-2.12.2 (c (n "imagequant-sys") (v "2.12.2") (d (list (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.9") (d #t) (k 0)))) (h "0n46jjhanwr999nj6c94icr068rjfgzv78nsvn4xyf70xnjrxrm0") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-2.12.5 (c (n "imagequant-sys") (v "2.12.5") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.13") (d #t) (k 0)))) (h "031rznqxdl5phghj0klbnh3s44naglyyqb1mqar8k6fq2y0y7nmz") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-2.12.7 (c (n "imagequant-sys") (v "2.12.7") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)) (d (n "openmp-sys") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.13") (d #t) (k 0)))) (h "0af3igf9z4zvz920sssmwj1vf45ny8w4mqyk3m63l8zw6xwxl5y7") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-2.13.0 (c (n "imagequant-sys") (v "2.13.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (d #t) (k 0)))) (h "17sdmrna1j2zy4m5i3hzn1n372v0mfww32h5g9rjvmz3d6hzr2dn") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-2.13.1 (c (n "imagequant-sys") (v "2.13.1") (d (list (d (n "cc") (r ">=1.0.58, <2.0.0") (d #t) (k 1)) (d (n "openmp-sys") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r ">=0.8.25, <0.9.0") (d #t) (k 0)))) (h "1fm96y7dnj5hbx9yszbxq26g4q3kx691dyvwn35nh6rlxdsnfjgy") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.0+sys2.13.1 (c (n "imagequant-sys") (v "3.0.0+sys2.13.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "095984zwbv3g1glhziisz41j76x50fkngp1nlp3hk62q3dv13cm3") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.1+sys2.13.2 (c (n "imagequant-sys") (v "3.0.1+sys2.13.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1vhz45x8vp2rl4ap8y2rwv24g1sipk94maf5hv5zlppadmvd6la1") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.2+sys2.14.0 (c (n "imagequant-sys") (v "3.0.2+sys2.14.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1gp026i80z6iwaiqify08cvavcc8fkdy6rwgbhand7qchpdxg2vi") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.3+sys2.14.0 (c (n "imagequant-sys") (v "3.0.3+sys2.14.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1zgq74lic3z1wwl31fp7r07pvlq5a6h07fk0x8vifjjcswgxz29n") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.4-alpha.1 (c (n "imagequant-sys") (v "3.0.4-alpha.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1ybqxxcvnjm4ya9ilipzj4hr0nl51ldv5wlb3fbbvcls75fnqc80") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.4-alpha.2 (c (n "imagequant-sys") (v "3.0.4-alpha.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1n04q6l1a5irsb3imz4d3anmk4x16mzzvs1q6q2z9wj1kb8rgh7n") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-3.0.6+sys2.15.0 (c (n "imagequant-sys") (v "3.0.6+sys2.15.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "068k250hzad77m5z35ha6hzx1kma7ksdrkl3pqrwx7l1qrx4xk1f") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-3.1.0+sys2.16.0 (c (n "imagequant-sys") (v "3.1.0+sys2.16.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "0w6hcp3pbi1348ix4x8sz9470615n18pvbnvx07bj5q9vvi5m69d") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-3.1.1+sys2.17.0 (c (n "imagequant-sys") (v "3.1.1+sys2.17.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.29") (d #t) (k 0)))) (h "114wlw83x9qfz4xm25p58xzra5jr0jsk7m04jj0a53kcr7pqxmdf") (f (quote (("sse") ("openmp-static" "openmp" "openmp-sys/static") ("openmp" "openmp-sys") ("default" "sse")))) (l "imagequant")))

(define-public crate-imagequant-sys-4.0.0-beta.3 (c (n "imagequant-sys") (v "4.0.0-beta.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0-beta.3") (f (quote ("_internal_c_ffi"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "1hshvczyyyjhwahjjqf2xv6mjnwa7ackcwinnn12gdrqvrha2206") (f (quote (("capi")))) (y #t)))

(define-public crate-imagequant-sys-4.0.0-beta.4 (c (n "imagequant-sys") (v "4.0.0-beta.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0-beta.4") (f (quote ("_internal_c_ffi"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0yfms8kcsikambil3lwlsky14fvz0npqvabr0xsb3gh8h6a7pc11") (f (quote (("capi")))) (y #t)))

(define-public crate-imagequant-sys-4.0.0-beta.7 (c (n "imagequant-sys") (v "4.0.0-beta.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0-beta.7") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "14n6ay86gy1pv7bpd4mzqi4pl1fxdhk1c9y21nrxa06mvzyj2lbd") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (y #t)))

(define-public crate-imagequant-sys-4.0.0-beta.8 (c (n "imagequant-sys") (v "4.0.0-beta.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0-beta.7") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "1lky9k42ica94mjggkn5wkfn8ap1k6pv2zjn5iq84yl91z8p8iyl") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-4.0.0-beta.9 (c (n "imagequant-sys") (v "4.0.0-beta.9") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0-beta.9") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "1ygn5vjchdk7zsqpyxpid4z3ybp4grgk0nmdy991rwgadw81znpx") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (y #t) (l "imagequant")))

(define-public crate-imagequant-sys-4.0.0 (c (n "imagequant-sys") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0jc3aqm04lsichdhgzfd5vyilfxr0mc0fyapx6l5hq423z5kdc59") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (l "imagequant")))

(define-public crate-imagequant-sys-4.0.1 (c (n "imagequant-sys") (v "4.0.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0hylp3k9r6vaq1kfl3da291442fh564wvixy51v5rnq3si0yryjs") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (l "imagequant") (r "1.60")))

(define-public crate-imagequant-sys-4.0.2 (c (n "imagequant-sys") (v "4.0.2") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "imagequant") (r "^4.2.1") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "04f0wsqh96259nmrgfw29y3hdl87knch2s8pxc7ivl3v0f4d4xni") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (l "imagequant") (r "1.63")))

(define-public crate-imagequant-sys-4.0.3 (c (n "imagequant-sys") (v "4.0.3") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "imagequant") (r "^4.2.1") (f (quote ("_internal_c_ffi"))) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0ljyj98ma3pcbpxgsp701zd75rlxbwr6nd25dzyfy301mmz3179r") (f (quote (("threads" "imagequant/threads") ("default" "imagequant/default") ("capi")))) (l "imagequant") (r "1.63")))

