(define-module (crates-io im ag image_ascii) #:use-module (crates-io))

(define-public crate-image_ascii-0.1.0 (c (n "image_ascii") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p4spmh19w7775hiravavj9pdc97qf820c8d8p7lgihp2qcwijrw")))

(define-public crate-image_ascii-0.1.1 (c (n "image_ascii") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0czb6jxzh9ibw7nqsmkc8d63an425wipi38djwf6lg2fzi3197wr")))

