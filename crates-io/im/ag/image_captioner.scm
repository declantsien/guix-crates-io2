(define-module (crates-io im ag image_captioner) #:use-module (crates-io))

(define-public crate-image_captioner-0.1.8 (c (n "image_captioner") (v "0.1.8") (d (list (d (n "maturin") (r "^1.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "002ya4rkljigis6l7qrl45nkkmrw55vdizhj98qcrwn5gyyc4782") (y #t)))

(define-public crate-image_captioner-0.1.9 (c (n "image_captioner") (v "0.1.9") (d (list (d (n "maturin") (r "^1.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "1jlahqk84zg97k4s0fj0jabdcqd7am9i7b2k34sgmx9kgm9van0q") (y #t)))

(define-public crate-image_captioner-0.1.10 (c (n "image_captioner") (v "0.1.10") (d (list (d (n "maturin") (r "^1.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "11r3la47wl9prn2xy3pjs6w11bfx57m9kn2fx99m231manla5xsq") (y #t)))

(define-public crate-image_captioner-0.1.11 (c (n "image_captioner") (v "0.1.11") (d (list (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "1iy1zfy27j41a8jx8s4knb16zbq0k8sbvqb2irz3jjmwd0bknziw") (y #t)))

(define-public crate-image_captioner-0.2.0 (c (n "image_captioner") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "1g4fllabdy8p1qkiwdk3y6wr2lyafwraya58gkl7r4188xk4pk1r") (y #t)))

(define-public crate-image_captioner-0.2.1 (c (n "image_captioner") (v "0.2.1") (d (list (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "09nrnfjms30lj2z26gw4c9ip1b812dcaimmwk70vq2amv14022vg") (y #t)))

(define-public crate-image_captioner-0.2.2 (c (n "image_captioner") (v "0.2.2") (d (list (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 1)))) (h "11xdip1rfm5154qqh4lgjx52avipsgc9smdkjfv69d3w052pwhdp")))

