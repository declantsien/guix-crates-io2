(define-module (crates-io im ag image-qoi) #:use-module (crates-io))

(define-public crate-image-qoi-0.1.0 (c (n "image-qoi") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)))) (h "1yhbhkijwfr3isk6avms43aj6hh47vqd43lwhg3ns4wggbvk7v67")))

(define-public crate-image-qoi-0.1.1 (c (n "image-qoi") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rapid-qoi") (r "^0.6") (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)))) (h "1dv6iimpcxj0czi8vz705nzqxcka7bip143b9b8f8sq7q1mrjl9z")))

