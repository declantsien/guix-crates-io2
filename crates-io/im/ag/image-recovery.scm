(define-module (crates-io im ag image-recovery) #:use-module (crates-io))

(define-public crate-image-recovery-0.1.0 (c (n "image-recovery") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("matrixmultiply-threading"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "19ivf4bvnw5z9zcbd1h4a8wdp7gd5s2ss1m3yf188f40lpnjl3gi")))

(define-public crate-image-recovery-0.2.10 (c (n "image-recovery") (v "0.2.10") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("matrixmultiply-threading"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dwqsxsn2lxj8c2i9bqs4m9hxriingh65zk62y7q9z8zfilfd86a")))

(define-public crate-image-recovery-0.3.0 (c (n "image-recovery") (v "0.3.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("matrixmultiply-threading"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1m3992wxkwi03670x6igpwv50sb4a2z6sscna6zsic62xsjrqw7j")))

(define-public crate-image-recovery-0.3.1 (c (n "image-recovery") (v "0.3.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("matrixmultiply-threading"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0crsjfa4fmp8x9m2jn1rbkk6hv492vdgmr745xigi0xmj7g9xbm2")))

