(define-module (crates-io im ag image_energy) #:use-module (crates-io))

(define-public crate-image_energy-0.1.0 (c (n "image_energy") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1drw0k7pli3h1rkbxc5ma1p4k3bjpb6lj5nw81m008c77rmwhn6k")))

(define-public crate-image_energy-0.1.1 (c (n "image_energy") (v "0.1.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "0gb9y78pkazzhgqpb125zl1h7v4wywhm2pl2xzrmx03gam7gq4gh")))

(define-public crate-image_energy-0.1.2 (c (n "image_energy") (v "0.1.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1m0l70rz31qyhm2wd05w1vibahn7a2i22s8lfl7hwlyf64f24njv")))

(define-public crate-image_energy-0.1.3 (c (n "image_energy") (v "0.1.3") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0ckma5ql7acpp9xhb3p05254a9lgb8yravkmab4k9i3kxfllxph7")))

