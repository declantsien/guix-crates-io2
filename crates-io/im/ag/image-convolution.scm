(define-module (crates-io im ag image-convolution) #:use-module (crates-io))

(define-public crate-image-convolution-0.0.0 (c (n "image-convolution") (v "0.0.0") (d (list (d (n "bytemuck") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std" "executor"))) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (d #t) (k 0)))) (h "1h3y27kp2mgjq941riiiy4xyxmnmh4bm54v1hcaqwdkh7i9ab2nq")))

(define-public crate-image-convolution-0.1.0 (c (n "image-convolution") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std" "executor"))) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)) (d (n "wgpu") (r "^0.8") (d #t) (k 0)))) (h "0i3a75gjwhh9cg4x97riwnf31nlpq3j0qdd75zlx617f1wwkm87n")))

