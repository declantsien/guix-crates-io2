(define-module (crates-io im ag image-prep) #:use-module (crates-io))

(define-public crate-image-prep-0.1.0 (c (n "image-prep") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg"))) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0hfvhsidfjxlhn2f49nbkbvfpw7ic6agb5xy30w9dlrjvivir608")))

