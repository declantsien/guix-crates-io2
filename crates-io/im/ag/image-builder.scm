(define-module (crates-io im ag image-builder) #:use-module (crates-io))

(define-public crate-image-builder-0.1.0 (c (n "image-builder") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "11b3fxxkvf6l1gdax3dhc15jpd3p9m8nv7lmfiwr192i0qp5zani")))

(define-public crate-image-builder-0.1.1 (c (n "image-builder") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1g2ixjqc76lj0yrnm3w4jf4n0imbf7kl3wz3x2w5zkb9iax2fpsl")))

(define-public crate-image-builder-0.1.2 (c (n "image-builder") (v "0.1.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1ajrr3l7wim9n4ngn3ml7njw2fss525zgs9cng8zxzsyj92jfwz3")))

(define-public crate-image-builder-0.2.0 (c (n "image-builder") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "03d5zb9v6lgbcy87vz97qwzpwmq8aka55j5iy85dkaqy5q4gk90j")))

(define-public crate-image-builder-0.3.0 (c (n "image-builder") (v "0.3.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0kva0din3s3gzvksrhd6yv8zi67hazvm8hgji9i8bw43wgnr192p")))

(define-public crate-image-builder-0.3.1 (c (n "image-builder") (v "0.3.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0f55bv09gpa2m9fdgcr4d4b2bhaqcdjfby6x32cbdrnjsrrhd2mj")))

(define-public crate-image-builder-0.3.2 (c (n "image-builder") (v "0.3.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "14x6bf8cwp7i921rpcad9cz98xqg8yr85c0ali93yd7q9az9nm8l")))

(define-public crate-image-builder-0.3.3 (c (n "image-builder") (v "0.3.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "128k3wiw4zm4z6vvjd6jk8lw5yidglqfcgayhbnha3mpa4b6ygkj")))

(define-public crate-image-builder-0.3.4 (c (n "image-builder") (v "0.3.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1silvqaw9cqc8nhgm6diyasfb93gc8snpkvk80iqjmlz9mxzkpim")))

(define-public crate-image-builder-0.4.0 (c (n "image-builder") (v "0.4.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0pfnbz01bb8a550c3jmrrw48hhxh22j4wc7faa5gsa5f17p7x4ml")))

(define-public crate-image-builder-0.5.0 (c (n "image-builder") (v "0.5.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1wpvbxsjd5mml77nbnxl8gfwa54dcz5vbw6fn25j2356p8w2jmv9")))

(define-public crate-image-builder-0.5.1 (c (n "image-builder") (v "0.5.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1zr1m7mnwa4w747pkz2j5ms3f9y18hj91zbp9r6gjncy05scl7kx")))

(define-public crate-image-builder-0.5.2 (c (n "image-builder") (v "0.5.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0clqw5782jqbx2cnb7i7fnrarzryvsravk8v8jygncdl85656ifc")))

(define-public crate-image-builder-1.0.0 (c (n "image-builder") (v "1.0.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0l0ly5snivpb3836jcp5yvrnimz591mbw0xlps4g4hb5h6gl7hlk")))

