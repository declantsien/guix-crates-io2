(define-module (crates-io im ag image_dct) #:use-module (crates-io))

(define-public crate-image_dct-0.1.0 (c (n "image_dct") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "rustdct") (r "^0.7.1") (d #t) (k 0)))) (h "11jb81c6cpp7lpgsrsgd15mwjf047cnpn9y30055ycqrs5iq1rx4")))

(define-public crate-image_dct-0.1.1 (c (n "image_dct") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "rustdct") (r "^0.7.1") (d #t) (k 0)))) (h "05f3ghzn2xzypp5cwp7ackxfkch73xv17zf2hzggqzf66lsan4v8")))

