(define-module (crates-io im ag image-tool) #:use-module (crates-io))

(define-public crate-image-tool-0.1.0 (c (n "image-tool") (v "0.1.0") (d (list (d (n "clap") (r "^2.34") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "1jxdzw5dwgwqxwgf4qp54b0j09h7yc7bfhpjdixlsvpzips4qrz6")))

