(define-module (crates-io im ag image-palette) #:use-module (crates-io))

(define-public crate-image-palette-0.1.0 (c (n "image-palette") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1ifj14mj66rpwd0lbv9agsm2868ijj4svksv2h9353yjqwk1mi35")))

(define-public crate-image-palette-0.1.1 (c (n "image-palette") (v "0.1.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0ip1ffhd1nkvx95sl95d96g75ykwz46qbg73b0xl0lqy4bym3yv1")))

