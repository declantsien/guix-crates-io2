(define-module (crates-io im ag image-webp) #:use-module (crates-io))

(define-public crate-image-webp-0.1.0 (c (n "image-webp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0drkpgmd618qrxgan2rsh6hyapj0kfyfx4s0xg74ibq4byi0fqds") (r "1.67.1")))

(define-public crate-image-webp-0.1.1 (c (n "image-webp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "png") (r "^0.17.12") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0z58998iq784sfz2rqz5lfqwjy8cwbwq1wjfq9xlib73r9fs513s") (r "1.67.1")))

(define-public crate-image-webp-0.1.2 (c (n "image-webp") (v "0.1.2") (d (list (d (n "byteorder-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "png") (r "^0.17.12") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0b97mpyy8drb2r07rjchd9k035g5bccczz87znfphk9wb22v0c6p") (r "1.67.1")))

