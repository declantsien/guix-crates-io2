(define-module (crates-io im ag image2tensor) #:use-module (crates-io))

(define-public crate-image2tensor-0.1.0 (c (n "image2tensor") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)))) (h "0q8f2lbcsidmqsmgfvkz2rq2laxii37w381is8mx434in7w6wizf")))

(define-public crate-image2tensor-0.1.1 (c (n "image2tensor") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)))) (h "1fl1zr9pklprvfypnyipmxchpbjgp6ybknrifz26z1sa9fnlbcnj")))

(define-public crate-image2tensor-0.2.0 (c (n "image2tensor") (v "0.2.0") (d (list (d (n "image") (r "^0.24.4") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)))) (h "1nrgyg3v39dysisras95qcvh159dds35g90555yb69pjiwd8fwmi")))

(define-public crate-image2tensor-0.3.0 (c (n "image2tensor") (v "0.3.0") (d (list (d (n "image") (r "^0.24") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)))) (h "0afjcqgmn2nciw16sgwb215d3h17qxd1wifxj8n549dnq19zkbs8")))

(define-public crate-image2tensor-0.3.1 (c (n "image2tensor") (v "0.3.1") (d (list (d (n "image") (r "^0.24") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)))) (h "1k7hijaiz5azbvriym2jid1y9f195ss2qhfix7j7xbjvrw2nylif")))

