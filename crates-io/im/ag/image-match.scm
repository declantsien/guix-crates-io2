(define-module (crates-io im ag image-match) #:use-module (crates-io))

(define-public crate-image-match-0.1.0 (c (n "image-match") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0g78ymhjpy3wdhv8ys83s60lx8kz44bjsrmawwz6xilac6hnqd8g") (f (quote (("img" "image" "num"))))))

(define-public crate-image-match-0.2.0 (c (n "image-match") (v "0.2.0") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0dcb6pvdvasa6cmjsrshh7lp6pfz3d388zj7yp5p2lj6457767j8") (f (quote (("img" "image"))))))

(define-public crate-image-match-0.2.1 (c (n "image-match") (v "0.2.1") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0vmxb7p4waz23k1f0y9mkaywivaq193jq8ix01bph4nbd9p13xms") (f (quote (("img" "image"))))))

(define-public crate-image-match-0.2.2 (c (n "image-match") (v "0.2.2") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0f72m1c8zh3cy8c4lymn33208dmqzia2rwg4fp7kn2b94xfmgqcd") (f (quote (("img" "image"))))))

(define-public crate-image-match-0.2.3 (c (n "image-match") (v "0.2.3") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1f809kd9hhqpvhfq31v7ffyky0hrjidagnixiyprv8cqzfyvykpq") (f (quote (("img" "image"))))))

