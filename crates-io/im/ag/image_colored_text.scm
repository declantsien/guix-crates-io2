(define-module (crates-io im ag image_colored_text) #:use-module (crates-io))

(define-public crate-image_colored_text-0.1.0 (c (n "image_colored_text") (v "0.1.0") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "07cmjag9yz1mj9sajkbx87qdzrc21scdwzk945a6bqmr5pllc5f6")))

(define-public crate-image_colored_text-0.1.1 (c (n "image_colored_text") (v "0.1.1") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "13h86bxk2l9l39jx1cnxdr7wwdrghqz0njv2zm3gmgh2i0x376bp")))

(define-public crate-image_colored_text-0.1.2 (c (n "image_colored_text") (v "0.1.2") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1b3gsfdvj4sqsrya2wjqd7ld80j6p7s3y992zasw5rs484k92rla")))

