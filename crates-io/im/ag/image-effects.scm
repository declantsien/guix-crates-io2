(define-module (crates-io im ag image-effects) #:use-module (crates-io))

(define-public crate-image-effects-0.1.0 (c (n "image-effects") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1v5srgpxxmnyxvlcjnb344438c1zbl2rr41b7z4c10gs9p0xnr0q")))

