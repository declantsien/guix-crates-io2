(define-module (crates-io im ag imag-create) #:use-module (crates-io))

(define-public crate-imag-create-0.10.0 (c (n "imag-create") (v "0.10.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color" "wrap_help"))) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.10.0") (d #t) (k 0)) (d (n "libimagrt") (r "^0.10.0") (d #t) (k 0)) (d (n "libimagstore") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fhc4il25vi6q7qdy66vn9yj9vd6c12a99g56p9wfmxnkg2qk8xi")))

(define-public crate-imag-create-0.10.1 (c (n "imag-create") (v "0.10.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color" "wrap_help"))) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.10.1") (d #t) (k 0)) (d (n "libimagrt") (r "^0.10.1") (d #t) (k 0)) (d (n "libimagstore") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06b178aqn2hcczsv9wgimm7168h8rkfnjs0n5nj6d851xwpjb52l")))

