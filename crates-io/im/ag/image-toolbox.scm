(define-module (crates-io im ag image-toolbox) #:use-module (crates-io))

(define-public crate-image-toolbox-0.1.0 (c (n "image-toolbox") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "math") (r "^0.0.2") (d #t) (k 0)))) (h "0mc32wjazvrvwhgp6hy9jfzarpjpxq18mmnhbrnf95w45pdyfc9p")))

(define-public crate-image-toolbox-0.1.1 (c (n "image-toolbox") (v "0.1.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "math") (r "^0.0.2") (d #t) (k 0)))) (h "1zl0m5n9zzwqx09zsyj8if1fxcspkk4b9mzwqa62r4vy4nwll7br")))

