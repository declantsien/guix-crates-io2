(define-module (crates-io im ag image-visualizer) #:use-module (crates-io))

(define-public crate-image-visualizer-0.1.0 (c (n "image-visualizer") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "visualizer") (r "^0.1.0") (d #t) (k 0)))) (h "0h056h8cp5m4k0n6h3qjijsihxh3qcsjpkzl58lf6s5gpvn7gqsg")))

(define-public crate-image-visualizer-0.1.1 (c (n "image-visualizer") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "visualizer") (r "^0.1.1") (d #t) (k 0)))) (h "1y27gww039x17vym9phri1d7mw1dym2270pdd50fkg030yiybi2b")))

