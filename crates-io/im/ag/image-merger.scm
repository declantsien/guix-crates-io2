(define-module (crates-io im ag image-merger) #:use-module (crates-io))

(define-public crate-image-merger-0.0.1 (c (n "image-merger") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1wn0n27w8nyzx76kgcsvj8wpvafcw5gzh1146h8ryp3f6ybzp4dy")))

(define-public crate-image-merger-1.0.0 (c (n "image-merger") (v "1.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0wkq5nq7im2wzr0i67bxmil9lp1x1h4z3h34cldakhbr32cgy82x")))

(define-public crate-image-merger-1.1.0 (c (n "image-merger") (v "1.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1zbad6k44dnahd8iz2f5r6x1jzvzjf7b3hhv0dalcdf4pidijhi0")))

