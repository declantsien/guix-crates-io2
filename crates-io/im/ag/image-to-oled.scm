(define-module (crates-io im ag image-to-oled) #:use-module (crates-io))

(define-public crate-image-to-oled-0.1.0 (c (n "image-to-oled") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "020xms89fn7mxc2cj8clbcyz082mblh3qzdz8nlcb0abw32z4r6f")))

(define-public crate-image-to-oled-0.2.0 (c (n "image-to-oled") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "01sk5z9l7cjp6jc0pbv70iikc2lg9hn9s4vgydbj65hhichl8644")))

(define-public crate-image-to-oled-0.3.0 (c (n "image-to-oled") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1d33vbcfjcpl04hcv35yzq14kk56lpcyblrbwnlprbl10gqh648c")))

(define-public crate-image-to-oled-0.4.0 (c (n "image-to-oled") (v "0.4.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1rs56vwkbqx5w6c5cvc6h60z50hx3z68qb723mqkyn1n5kygya9c")))

(define-public crate-image-to-oled-0.5.0 (c (n "image-to-oled") (v "0.5.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0r3h2bilmblzxhfw8l2qgwqb8bp7c8mdx6siwqj76nvrcys5r7j0")))

