(define-module (crates-io im ag image-generator) #:use-module (crates-io))

(define-public crate-image-generator-3.0.0 (c (n "image-generator") (v "3.0.0") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "116i1yq4pvx4ivh84igav2k4kqrr789ha0b2lbj6nidap0jvc3a4")))

(define-public crate-image-generator-3.0.1 (c (n "image-generator") (v "3.0.1") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vfv7hr2g7qpwm8z2kwgrhslzhrm6l5wf2pbwniwgw0fk846w8xb")))

(define-public crate-image-generator-3.0.2 (c (n "image-generator") (v "3.0.2") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1shzcc92ajd7ccx7jwq9iss08b79lvcv3709rrz0qq3xbwp45qcn")))

(define-public crate-image-generator-3.1.0 (c (n "image-generator") (v "3.1.0") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lykxl66zkc6zsn32j38vs0rbpmwfapmb9apxvm5hjg0g0ynvxxv")))

