(define-module (crates-io im ag image-dwt) #:use-module (crates-io))

(define-public crate-image-dwt-0.1.0 (c (n "image-dwt") (v "0.1.0") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "tiff") (r "^0.9.1") (d #t) (k 0)))) (h "0i2blflws651r6w6grs26pf3sk1j0snsc8wypxmmnfn3nc4x7v98")))

(define-public crate-image-dwt-0.2.0 (c (n "image-dwt") (v "0.2.0") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "tiff") (r "^0.9.1") (d #t) (k 0)))) (h "1pglp59j8paimpl6bpqmsxk987bnvd7871399xw93bbl79fv5j48")))

(define-public crate-image-dwt-0.2.1 (c (n "image-dwt") (v "0.2.1") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "tiff") (r "^0.9.1") (d #t) (k 0)))) (h "18jqq3qpjgn10vn9s3vz8f0mdhgnkqp3fp68l83bw9l74swr652c")))

(define-public crate-image-dwt-0.2.2 (c (n "image-dwt") (v "0.2.2") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "0apdmrmgskv2jksd6f038p3nkbxfkxfqjsnsxs0fy2vxxplcx6xn")))

(define-public crate-image-dwt-0.3.0 (c (n "image-dwt") (v "0.3.0") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "12w9bh4n83gfr8bpfz9c4y8g27a7ihzm0xnv9fjgdzd1klvyiwkf")))

(define-public crate-image-dwt-0.3.1 (c (n "image-dwt") (v "0.3.1") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "1l4mr9fwql1s3il0rnfbh9pgmh8yq06i6jlngqa0r5h4jdj58fg6")))

(define-public crate-image-dwt-0.3.2 (c (n "image-dwt") (v "0.3.2") (d (list (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "14xzs40r9nijd252j0pv2nbg0h9m9i629p2h6m8qfnxwh8kiys5y")))

(define-public crate-image-dwt-0.3.3 (c (n "image-dwt") (v "0.3.3") (d (list (d (n "convolve-image") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "0l31ss8axaisx3x9fa8qnq372mn5nlm3ak9n35lrvpcgfqcv43cv") (y #t)))

(define-public crate-image-dwt-0.4.0 (c (n "image-dwt") (v "0.4.0") (d (list (d (n "convolve-image") (r "^0.2.0") (f (quote ("ndarray"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "1xd7r0gbvir9zqw57ynpz79a5lsx1bnh6bdl8yz0p1m6gjlig79v")))

(define-public crate-image-dwt-0.4.1 (c (n "image-dwt") (v "0.4.1") (d (list (d (n "convolve-image") (r "^0.4.0") (f (quote ("ndarray" "image"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (f (quote ("tiff"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)))) (h "0ssjfs3kjad5mqdfi5lsvlck1bk6yrmg2qpn88sbgy6w85vgx9y1")))

