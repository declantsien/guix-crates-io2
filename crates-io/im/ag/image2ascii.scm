(define-module (crates-io im ag image2ascii) #:use-module (crates-io))

(define-public crate-image2ascii-0.1.0 (c (n "image2ascii") (v "0.1.0") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1nsrmv256bn3cb0r7sjccwc5ipcrfgr2q1l4szmgi3h1mxlf9vxd")))

(define-public crate-image2ascii-0.2.0 (c (n "image2ascii") (v "0.2.0") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0i9p70y5fb7bjymzq4bp68avdn30c5wv14fgh48klp8zg31h7cjg")))

(define-public crate-image2ascii-0.2.1 (c (n "image2ascii") (v "0.2.1") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "08f1gd3b949vm41g8mh56iypyqhx5rricywgq82lrhl4c5xl6hmz")))

