(define-module (crates-io im ag imagecli) #:use-module (crates-io))

(define-public crate-imagecli-0.1.0 (c (n "imagecli") (v "0.1.0") (d (list (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "imageproc") (r "^0.19.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "03iqwcmhapypgvirarw2pj5682r043rh0zayp5pn7m9jcsjx25rp")))

(define-public crate-imagecli-0.2.0 (c (n "imagecli") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "imageproc") (r "^0.19.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1x0h02iasdq0wzf73imxkrc0j7xq0dx3i34ah81ngmh95m2xn4yq")))

(define-public crate-imagecli-0.2.1 (c (n "imagecli") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "imageproc") (r "^0.19.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1k98ahq1kdg2psvxqpxlvfip7phsjsgjrpvrjvimlphf3l7blp9b")))

