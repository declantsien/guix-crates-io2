(define-module (crates-io im ag image-maker) #:use-module (crates-io))

(define-public crate-image-maker-0.1.0 (c (n "image-maker") (v "0.1.0") (d (list (d (n "easy-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "tianmu-fs") (r "^0.1.0") (d #t) (k 0)))) (h "0i0qywqcnwlws44dsn7grpql65f5ipm0i64k3dlf853yq24wjrph")))

(define-public crate-image-maker-0.1.1 (c (n "image-maker") (v "0.1.1") (d (list (d (n "easy-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "tianmu-fs") (r "^0.1.0") (d #t) (k 0)))) (h "0cl7jwrrldkawms5011mfgpnrdfh8py6vq3pr2klbhspz3001avk")))

(define-public crate-image-maker-0.1.2 (c (n "image-maker") (v "0.1.2") (d (list (d (n "easy-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "tianmu-fs") (r "^0.1.0") (d #t) (k 0)))) (h "0d85iw3lj6aidn6sc38zd6xybh574xjjjwmdcy3zj8nmyip6w0b7")))

(define-public crate-image-maker-0.1.3 (c (n "image-maker") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "easy-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "tianmu-fs") (r "^0.1.0") (d #t) (k 0)))) (h "1syjp8qg1z5gzhssyxp4gkh67najz9mmvjz05zz06w8bdbfs44s5")))

