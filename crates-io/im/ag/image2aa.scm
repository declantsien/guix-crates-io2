(define-module (crates-io im ag image2aa) #:use-module (crates-io))

(define-public crate-image2aa-0.1.3 (c (n "image2aa") (v "0.1.3") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)))) (h "00k8s4vqg388aim79wzyhi4ncw6dfmkbv07ind02bqdg1a7qpzjk")))

(define-public crate-image2aa-0.1.4 (c (n "image2aa") (v "0.1.4") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)))) (h "1nlp5zqfdsglr7wmxvvw4i20ijfl6f51k2dl5263f9w79cvg9nbz")))

