(define-module (crates-io im ag image-spec) #:use-module (crates-io))

(define-public crate-image-spec-0.1.0 (c (n "image-spec") (v "0.1.0") (h "053819g3r245fhcrk2l22b3h3kqjdjlkhwwq8sdsf6mm66vc4485")))

(define-public crate-image-spec-0.1.1 (c (n "image-spec") (v "0.1.1") (h "00wg3pzdcb9zbzkpx8hvvhk8dd0xrp4marrvjv1dzrhmi4d1cwqx")))

