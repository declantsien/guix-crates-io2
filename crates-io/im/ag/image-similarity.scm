(define-module (crates-io im ag image-similarity) #:use-module (crates-io))

(define-public crate-image-similarity-0.1.0 (c (n "image-similarity") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0gjjk609mfsl3fcwvxlxph7l74yzyps7qvqfaiykb3lh1h34im88")))

(define-public crate-image-similarity-0.1.1 (c (n "image-similarity") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0rynyf0ipsj02njw3qk8b8hms0zqfyl0s2kwqkdk00r4fd2k8m5n")))

(define-public crate-image-similarity-0.1.2 (c (n "image-similarity") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1ysg6fbbw3jx4xjjchslnn5c7gsapdxm9n27x96lw9mnkldinnjn")))

(define-public crate-image-similarity-0.1.3 (c (n "image-similarity") (v "0.1.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1bhhi52vqd0qx0bfnwmafmhzwf10krjlq9vgsq91al2j56xhqihw")))

(define-public crate-image-similarity-0.1.4 (c (n "image-similarity") (v "0.1.4") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "01glj9q897h9a4pd7jllnfklnyrss5a60hss41c885x4f470xjpn")))

(define-public crate-image-similarity-0.1.5 (c (n "image-similarity") (v "0.1.5") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0riic12mh4fskm5by1m0y2rrcqahlkk9apnzc4w4lmal27y1g63l")))

