(define-module (crates-io im ag image_buffer) #:use-module (crates-io))

(define-public crate-image_buffer-0.1.0 (c (n "image_buffer") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0q8wyaflrlr63xdw156isw5v9fl3js68fhz2jdwdnwlcyghpj3my")))

(define-public crate-image_buffer-0.2.0 (c (n "image_buffer") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "00qn2352l8hmlw9hyip82d8w6fakfh3k8lsnd02r4pxg5kjyhkdc")))

