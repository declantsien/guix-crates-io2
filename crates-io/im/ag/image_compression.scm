(define-module (crates-io im ag image_compression) #:use-module (crates-io))

(define-public crate-image_compression-0.1.0 (c (n "image_compression") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "073gk20375mq2fvx5ylq484gx91a96ng0kb7jlssaz8x3gkwz42z")))

(define-public crate-image_compression-0.1.1 (c (n "image_compression") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1400zhgird53343y29bi5sn3h4g8nd6g29srzb14mjy6xzfha4zq")))

