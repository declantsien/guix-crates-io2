(define-module (crates-io im ag image-go-fast) #:use-module (crates-io))

(define-public crate-image-go-fast-0.1.0 (c (n "image-go-fast") (v "0.1.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0rnwyczf9s9jcknwz1g9is4gfd3kh0zsiycpgmmqj9wvdpiby7r2")))

