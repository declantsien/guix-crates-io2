(define-module (crates-io im ag image-hdr) #:use-module (crates-io))

(define-public crate-image-hdr-0.1.0 (c (n "image-hdr") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0c2apafmvrj3s5vr7icx3a1qlk93gikl84x58kh4qj6zhwj4svcv")))

(define-public crate-image-hdr-0.1.1 (c (n "image-hdr") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1cilq47k2zg7zp3swf34z5wifdrwq4fpfdxhx18nrsbcp77s62vp")))

(define-public crate-image-hdr-0.1.2 (c (n "image-hdr") (v "0.1.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1jad0damk2vbphf20q6nvji7cj4srvvrchb9x1r8ix382jv3gvwv")))

(define-public crate-image-hdr-0.2.0 (c (n "image-hdr") (v "0.2.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0i6pn6a4cwdmbkm721xygfdclh2zydd1sfmsb96h2ljhw80zvvqv")))

(define-public crate-image-hdr-0.3.0 (c (n "image-hdr") (v "0.3.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0kcn8y98bwrvy9rycsdafhm0iycs5ycgi7i3icy8kclys5zg9a2j")))

(define-public crate-image-hdr-0.4.0 (c (n "image-hdr") (v "0.4.0") (d (list (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "rawloader") (r "^0.37") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "01yq3p55adcblkcv6b6lqbc1sdb8zwwk40znclxhkfmqfhk3nb8l")))

