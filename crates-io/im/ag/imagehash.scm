(define-module (crates-io im ag imagehash) #:use-module (crates-io))

(define-public crate-imagehash-0.1.0 (c (n "imagehash") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg"))) (k 2)))) (h "1wxi4k9kbf2rrv925hxs9xcmc9kpfbzzjr3a870y0fdxphj8qfz0") (r "1.61.0")))

(define-public crate-imagehash-0.2.0 (c (n "imagehash") (v "0.2.0") (d (list (d (n "image") (r "^0.24.7") (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg"))) (k 2)))) (h "0wx2b2b7dy43sv6n3s4mnw0hpqr9jmyk94w13y69dis4a853kl3r") (r "1.61.0")))

(define-public crate-imagehash-0.3.0 (c (n "imagehash") (v "0.3.0") (d (list (d (n "image") (r "^0.24.7") (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg"))) (k 2)))) (h "0424cbx08y8hai029rxpihb9nr4p5yq2j0wcwbi85ijvj5jknaaz") (r "1.61.0")))

