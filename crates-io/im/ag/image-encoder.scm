(define-module (crates-io im ag image-encoder) #:use-module (crates-io))

(define-public crate-image-encoder-0.1.0 (c (n "image-encoder") (v "0.1.0") (d (list (d (n "adler32") (r "^1.0.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 2)))) (h "13ivypxdijnswnjl7bb558ylv2ni12qqhr4vxgva43dd9h5585kp")))

