(define-module (crates-io im ag image2multires) #:use-module (crates-io))

(define-public crate-image2multires-0.1.0 (c (n "image2multires") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1hr59ccds5fsc1a1y40dwdmw603azyp2lzjia3qwg9m5zk78y5mx")))

(define-public crate-image2multires-0.1.1 (c (n "image2multires") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fn5jczm5ahhyn28k8nb8hh4jicv1by1k3kf3z5r7v9q5i89791v")))

