(define-module (crates-io im ag image-wrapper) #:use-module (crates-io))

(define-public crate-image-wrapper-0.1.0 (c (n "image-wrapper") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "pixels-graphics-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rk3cjwj1j2jd5hdv594lb6v7hcf077cn0bisvmzb8hhrd4xr9qd")))

