(define-module (crates-io im ag image-organiser) #:use-module (crates-io))

(define-public crate-image-organiser-0.2.0 (c (n "image-organiser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0ar02mjsgjh9zlrhzxpbyy80j5630yir3qq7b5qmq8m0nnxd49jl") (y #t)))

(define-public crate-image-organiser-0.3.0 (c (n "image-organiser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0i8kv5crjs7hbs0b251vs4wz0x9d00fsqvf2d88xv0w7mk5g23kq") (y #t)))

(define-public crate-image-organiser-0.3.1 (c (n "image-organiser") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "095rsrp073achiyh0b7l2ynn9fi9kfkcmz440c000piy9biar9v1") (y #t)))

(define-public crate-image-organiser-0.3.2 (c (n "image-organiser") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0w1w5646jwdaz2hyrnlqwnmpfy85qwfy3cb2qz1x6xg20acxhw2i") (y #t)))

(define-public crate-image-organiser-0.3.3 (c (n "image-organiser") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1s59qz3b99i57a9s400gbxq419r6g4w6ar1spxxhlaadlw3z4568") (y #t)))

(define-public crate-image-organiser-0.3.4 (c (n "image-organiser") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1vdnjpwy8hi29wijjbpc9yi5qw2gfv7hlaw8qrwl1fpx2rmcnnrm") (y #t)))

