(define-module (crates-io im co imcon) #:use-module (crates-io))

(define-public crate-imcon-0.1.1 (c (n "imcon") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "kmeans") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libheif-rs") (r "^0.15.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.5.0") (d #t) (k 0)))) (h "11n6691swlabv8l6264acrk5127kjch4101a6dbhs48smabzk7sa")))

