(define-module (crates-io im gr imgr) #:use-module (crates-io))

(define-public crate-imgr-0.1.0 (c (n "imgr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1vclsvvv63cf5n933hgy769d3srwwkbfwn81cvcxic6zr31z3vaz") (y #t)))

(define-public crate-imgr-0.1.1 (c (n "imgr") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0880y9ngsyz1p3h70y38z5gqka0cj91nvlqn8p7m2jmbkb9h6ccl") (y #t)))

(define-public crate-imgr-0.1.2 (c (n "imgr") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0hzf9wli7jfimcqgl87nczqj249b6hpmi4zxbnkaq75jj93lzr7i")))

(define-public crate-imgr-0.1.3 (c (n "imgr") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "005spjxgmjvbnk25sw27g8gdyxvcifc82iwg6gg8aqiqqv3ijsfy")))

(define-public crate-imgr-0.1.4 (c (n "imgr") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1c86ijbfif2b3ir9ka4cij96fq20dxl30wgyl8y5aczi5dklvx7h")))

(define-public crate-imgr-1.0.0 (c (n "imgr") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "11z60awqdgskic8znjq66djhn09r997ihcc3j22f8rzdk8bhaycl") (y #t)))

(define-public crate-imgr-1.0.1 (c (n "imgr") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0zds599dmhixjxn0rnnqv7x9xja39dz7z0dhpv9805acbsfc31jm")))

