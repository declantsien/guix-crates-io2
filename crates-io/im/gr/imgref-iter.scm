(define-module (crates-io im gr imgref-iter) #:use-module (crates-io))

(define-public crate-imgref-iter-0.1.0 (c (n "imgref-iter") (v "0.1.0") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "0p7a6g383kr8vm0d9k3qhccy7q1gm53si6fav654xd6klby5mpmv") (r "1.56")))

(define-public crate-imgref-iter-0.1.1 (c (n "imgref-iter") (v "0.1.1") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "0qh22p2r8gk7vfvskjbp5qdjjvsxpfn7cv2n1ak3m4ffhs7y3qg8") (r "1.56")))

(define-public crate-imgref-iter-0.1.2 (c (n "imgref-iter") (v "0.1.2") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "0zij5kwi02v1nm5c9almnx0vd5i5anx0mwrc5l0wrkw1yzm3jymk") (y #t) (r "1.56")))

(define-public crate-imgref-iter-0.2.0 (c (n "imgref-iter") (v "0.2.0") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "1gxnmxc9f8dd2459gmfskph0ybypggwafpwcx5acfc4yj1rsxv02") (r "1.56")))

(define-public crate-imgref-iter-0.3.0 (c (n "imgref-iter") (v "0.3.0") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "05rzqmcplwrn9a5dzji4sgakbmdfim0ja9mn5hvdg373jyd7npkx") (f (quote (("simd")))) (r "1.56")))

(define-public crate-imgref-iter-0.3.1 (c (n "imgref-iter") (v "0.3.1") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "1270j24xzfj9pk2zbyrvk1k8n0gc6vfjjmchhg0720xl2w2z3a0f") (f (quote (("simd")))) (r "1.56")))

(define-public crate-imgref-iter-0.3.2 (c (n "imgref-iter") (v "0.3.2") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "0ihxrwkdx0c0fndz5yc6zrs7x7lsd4g9yhs4wwf771s90dla4kqp") (f (quote (("simd")))) (r "1.56")))

(define-public crate-imgref-iter-0.3.3 (c (n "imgref-iter") (v "0.3.3") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "0bp61002jicyjxd4mhsrlfvfxf15vcd7d8nlqkijrn4sdi1x6a4y") (f (quote (("simd")))) (r "1.56")))

(define-public crate-imgref-iter-0.4.0 (c (n "imgref-iter") (v "0.4.0") (d (list (d (n "imgref") (r "^1.9.2") (d #t) (k 0)))) (h "1akn14lw5m3x5jfmgphrhwk97cnh8l29x97bvik8n5zfq0rjf4aq") (f (quote (("simd")))) (r "1.63")))

