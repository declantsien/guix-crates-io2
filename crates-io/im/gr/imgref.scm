(define-module (crates-io im gr imgref) #:use-module (crates-io))

(define-public crate-imgref-1.0.0 (c (n "imgref") (v "1.0.0") (h "0zxxjad0vlvrzbpv13xpcfa65v5qh9yzay7j8f4xzhpqmj3dxpbm") (y #t)))

(define-public crate-imgref-1.1.0 (c (n "imgref") (v "1.1.0") (h "0s0d0l9jrxmdb00cfph72l4almfrf6lgl7c0d5kkvz701lmdfh96") (y #t)))

(define-public crate-imgref-1.1.1 (c (n "imgref") (v "1.1.1") (h "1ndij2ryfk8ljkxglhk4cf9niilzsd9gzc53pxmzgv20zbrvk5vp") (y #t)))

(define-public crate-imgref-1.2.0 (c (n "imgref") (v "1.2.0") (h "14gi3y0293wkj8xc69sj6gdxj7hkcib0r8qjkkbxhn6ia3q3xzi9") (y #t)))

(define-public crate-imgref-1.2.1 (c (n "imgref") (v "1.2.1") (h "0xcr1ci1jmlsz5wb5kx4qcn6qd5qppbrwrc3ykdp2ci5zfasz6y2") (y #t)))

(define-public crate-imgref-1.2.2 (c (n "imgref") (v "1.2.2") (h "07b3rylaznsal4fjgi9j0azblk51h89fnyd1l08m65n3zxvkgzcz") (y #t)))

(define-public crate-imgref-1.3.0 (c (n "imgref") (v "1.3.0") (h "1f9gvx32b9skbq61n2v6hxvhc3m6x19armd8w94by0qp2c4d6lcg") (y #t)))

(define-public crate-imgref-1.3.1 (c (n "imgref") (v "1.3.1") (h "0mfjz44vivymacf92z1yi820w1j1sb26lqval0wkbi2zypchr0lk") (y #t)))

(define-public crate-imgref-1.3.2 (c (n "imgref") (v "1.3.2") (h "1fqk5cbsg24prpc2ka8k2za614ycy8zifyn9ljnzr59wa5w6c5h6") (y #t)))

(define-public crate-imgref-1.3.3 (c (n "imgref") (v "1.3.3") (h "1gi5kmag5ahbj2x3js50av81gs8zrssw0p43nk5qqihzdqjlpmxr") (y #t)))

(define-public crate-imgref-1.3.4 (c (n "imgref") (v "1.3.4") (h "0xpsk4v1h8n1pskr723w8bklsqhs0vhqan43x38wi77b90xzl794") (y #t)))

(define-public crate-imgref-1.3.5 (c (n "imgref") (v "1.3.5") (h "1n6jrs60phiyd4gza4bjhgd4dg547j6swhmygrcnxcy0hlxjxn64") (y #t)))

(define-public crate-imgref-1.4.0 (c (n "imgref") (v "1.4.0") (h "04m207csvhg7vgslmnrd2r5h4ym89ry7flsxwz429ya2vgcmspx4") (y #t)))

(define-public crate-imgref-1.4.1 (c (n "imgref") (v "1.4.1") (h "0ax8bh8xms2gdp9vl9g1mlqjk36b5dgdbsyx17a83smvf2q8x8as") (y #t)))

(define-public crate-imgref-1.4.2 (c (n "imgref") (v "1.4.2") (h "1ccbgzs5xm5pnam4xbad1702xnfdg0nnbhx3d7nz1i475jiqpnrq") (y #t)))

(define-public crate-imgref-1.4.3 (c (n "imgref") (v "1.4.3") (h "14jzy3bjad6prz0mcrz48pwvi14jab0ps8ajnfrhsxwqjby8yds8") (y #t)))

(define-public crate-imgref-1.5.0 (c (n "imgref") (v "1.5.0") (h "0pwbr8nm8ydzmjhbg3n6zjk3mlfrgjva07brcp9cl4qjq08vbb8p") (y #t)))

(define-public crate-imgref-1.6.0 (c (n "imgref") (v "1.6.0") (h "0m5si8n4ls3hidnqqrq0v5q6hbnqgwlmxcnk83zinffzpccya53a") (y #t)))

(define-public crate-imgref-1.6.1 (c (n "imgref") (v "1.6.1") (h "1f2m9lq769248bwbyzda714a8xcgk5bdpdsi20xcnavikiw449g8")))

(define-public crate-imgref-1.7.0 (c (n "imgref") (v "1.7.0") (h "1bpwn4f3076fqjmh037y5i7bk0i80kf2rdfrzyrva2brpr1phxnq")))

(define-public crate-imgref-1.7.1 (c (n "imgref") (v "1.7.1") (h "19dd5xss3nd40avv8az2kzicpxx71c2akiqznr616hki30w9vj07")))

(define-public crate-imgref-1.8.0 (c (n "imgref") (v "1.8.0") (h "0jh584fb1khp451lvf2i6jmir8jl09yr7i5wc1xc69mjrhzzzn0k")))

(define-public crate-imgref-1.9.0 (c (n "imgref") (v "1.9.0") (h "0i81gjb69y10qij8ydvkd4v17x99xv0sfkcayg93595sjhkh4vpr")))

(define-public crate-imgref-1.9.1 (c (n "imgref") (v "1.9.1") (h "0ra3wl5s22ffcx18inmlryqykh7qwzr0k2gdw1i84bwkdkdw1l21")))

(define-public crate-imgref-1.9.2 (c (n "imgref") (v "1.9.2") (h "1mf7h1xvb7zpshlm05xgaad2iszlqazwzslbkfch9ihdipa8qskx")))

(define-public crate-imgref-1.9.3 (c (n "imgref") (v "1.9.3") (h "19jkx0bnd8w7vzb5gkxnah0m28b4pjy7k4wbp498bhjri0k7631j")))

(define-public crate-imgref-1.9.4 (c (n "imgref") (v "1.9.4") (h "0b3czpz206z4nvpq7yq0v58bwjmqjwjmkr302hbzpp4523glkkxj")))

(define-public crate-imgref-1.10.0 (c (n "imgref") (v "1.10.0") (h "0p03zlszq904z4pc21932ph6p08z2w5482v4r54l83zh6kil9nch") (f (quote (("deprecated") ("default" "deprecated"))))))

(define-public crate-imgref-1.10.1 (c (n "imgref") (v "1.10.1") (h "09l18s80crfn7g8ank3v44g43xns4pg7f6hpaz3sfna1bwsxmzj4") (f (quote (("deprecated") ("default" "deprecated"))))))

