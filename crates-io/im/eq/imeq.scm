(define-module (crates-io im eq imeq) #:use-module (crates-io))

(define-public crate-imeq-0.0.1 (c (n "imeq") (v "0.0.1") (d (list (d (n "clap") (r "^2.34.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "05cxfxl885s2h4l4l0pvlkk8py7ckfp2zx5jsfzi4l1rmpwn750m")))

(define-public crate-imeq-0.0.2 (c (n "imeq") (v "0.0.2") (d (list (d (n "clap") (r "^2.34.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0anwvn6i2vxpp346m2jg5kgzr78in9wq7w2snnkg84390rrpgzp3")))

(define-public crate-imeq-0.1.0 (c (n "imeq") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0zcfys7y1w05bzgc4d14zpmici0yprnfnvbgv1nqv84l7mw6g7ij")))

