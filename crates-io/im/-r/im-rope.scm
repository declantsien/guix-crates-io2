(define-module (crates-io im -r im-rope) #:use-module (crates-io))

(define-public crate-im-rope-0.1.0 (c (n "im-rope") (v "0.1.0") (d (list (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2") (d #t) (k 2)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "static-cow") (r "^0.2") (d #t) (k 0)))) (h "1nnmgj8mmjz9wq8ff6xgl5vg050vmnybb317fydq4spj4cz7mcp5") (f (quote (("default" "serde" "proptest")))) (s 2) (e (quote (("serde" "dep:serde" "im/serde") ("proptest" "dep:proptest" "im/proptest")))) (r "1.65")))

