(define-module (crates-io im dl imdl-indicatif) #:use-module (crates-io))

(define-public crate-imdl-indicatif-0.14.0 (c (n "imdl-indicatif") (v "0.14.0") (d (list (d (n "console") (r ">= 0.9.1, < 1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0a4v6gwhnki4z9kdxn92filfms565cxhrp709cbqawa8j1h2rmbc") (f (quote (("with_rayon" "rayon") ("default"))))))

