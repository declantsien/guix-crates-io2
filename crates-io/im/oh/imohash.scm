(define-module (crates-io im oh imohash) #:use-module (crates-io))

(define-public crate-imohash-0.1.0 (c (n "imohash") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)))) (h "0qbhnhhnkylz6ci0h6r3cd5fxyqzszlmir55g44pprrsbb2d1vaf")))

