(define-module (crates-io im mo immortal_axum_macro) #:use-module (crates-io))

(define-public crate-immortal_axum_macro-0.1.0 (c (n "immortal_axum_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1br5w0b5xcgrq51b5szsh96cs5hw85kknj14ijb226vy5qp6229p")))

