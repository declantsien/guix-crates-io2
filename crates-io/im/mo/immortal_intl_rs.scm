(define-module (crates-io im mo immortal_intl_rs) #:use-module (crates-io))

(define-public crate-immortal_intl_rs-0.1.0 (c (n "immortal_intl_rs") (v "0.1.0") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "string_template") (r "^0.2.1") (d #t) (k 0)))) (h "033aixgap62y41kayd912kg3xx3qan9jlljqdsxhz54cirwcqkva")))

