(define-module (crates-io im mo immortal) #:use-module (crates-io))

(define-public crate-immortal-0.1.0 (c (n "immortal") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0p9488rp8h5zkwq1dgc7r00mx5f2dywnqcv7akgrryr0wmvb0ak7")))

