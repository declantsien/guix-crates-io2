(define-module (crates-io im mo immo) #:use-module (crates-io))

(define-public crate-immo-0.1.0 (c (n "immo") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "13kny6bqzld6scx4adkipaljkzdpxwl8nch70nv4svc99cc8l61g")))

(define-public crate-immo-0.1.1 (c (n "immo") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "07xkffj2c756bbafa06ra8dvj4jsbj3pwj2qbwvmahh3fad80qxk")))

(define-public crate-immo-0.1.2 (c (n "immo") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "1h42lcasy0vqlsgjal3dn8npgc7gkkhvrxd1r5xs0xsmv5nzj45n")))

