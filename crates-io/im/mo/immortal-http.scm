(define-module (crates-io im mo immortal-http) #:use-module (crates-io))

(define-public crate-immortal-http-0.1.0 (c (n "immortal-http") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.2") (d #t) (k 0)))) (h "19wxrznbavrzyq6qqnpcy3505k3r2az8v6w72667614c2l986mdh") (y #t)))

(define-public crate-immortal-http-0.1.1 (c (n "immortal-http") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.2") (d #t) (k 0)))) (h "1rpzhl9lp15ngz8mqgzg3il86iq2d0miswjxz3fxqqmrb8imwb6h") (y #t)))

(define-public crate-immortal-http-0.1.2 (c (n "immortal-http") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.2") (d #t) (k 0)))) (h "04mhc5ghlc1nnqkmydr0yrfzj0jjgkza7klxcrcgchg30yrhvmps") (y #t)))

(define-public crate-immortal-http-0.1.3 (c (n "immortal-http") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.4") (d #t) (k 0)))) (h "05f3khj2h24v24vb9h5hxcif9bdha7i3xm5nxarwa3vics6wsvdv") (y #t)))

(define-public crate-immortal-http-0.1.4 (c (n "immortal-http") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.4") (d #t) (k 0)))) (h "1n07p53v72b48hmlq3pgci8ba2wci66vy5ia2znrfsifihawb71j") (y #t)))

(define-public crate-immortal-http-0.1.5 (c (n "immortal-http") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.4") (d #t) (k 0)))) (h "17gpw3f1xc7zv3gphm5kw7j61kxkk90pjh3sbcah2gx4kq86jvyk")))

(define-public crate-immortal-http-0.1.6 (c (n "immortal-http") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.4") (d #t) (k 0)))) (h "015aq16iswgw069dcib459r15npv2zm13g29gvbjg4r9q4n70fpr")))

(define-public crate-immortal-http-0.1.7 (c (n "immortal-http") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scoped-thread-pool") (r "^1.0.4") (d #t) (k 0)))) (h "0iqq4z22pn246wiqkq85cnyflcq8rn20qa4y1habwk9r9wl1jl65")))

