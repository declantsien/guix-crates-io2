(define-module (crates-io im es imessage-database) #:use-module (crates-io))

(define-public crate-imessage-database-0.0.0 (c (n "imessage-database") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1l6k8c8ahwqy7mfpg731hriw62wrs4nf34hfly1hnvv8rdamlwnh")))

(define-public crate-imessage-database-0.1.0 (c (n "imessage-database") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "0n8cxwx4ln7w0pf3p2v8620ryq4acidj38kqwxr9avc208mb16hb")))

(define-public crate-imessage-database-0.1.1 (c (n "imessage-database") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "0a5krji0szykg3ccs5h84sdcwdgwk42arc0kp7jp1kalyphfwdzw")))

(define-public crate-imessage-database-0.1.2 (c (n "imessage-database") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "0jhsbg4d47gh31bsz85lpfy795ic1dhjnnflxik59wdlfmi3c4yw")))

(define-public crate-imessage-database-0.1.3 (c (n "imessage-database") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1haawiykgphba1szwcz9hgd4zmw051dcc7f0vm5yrvw54gf3srfd")))

(define-public crate-imessage-database-0.1.4 (c (n "imessage-database") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1ywrgpkiy3gd6bpyr0s3zlldjf3l15zm121fy458ylgz7b6pranb")))

(define-public crate-imessage-database-0.1.5 (c (n "imessage-database") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "038i3kppckzc4nnjzhhjipfnqxxp9i0ahx1887gg1hvnw5jf7xlh")))

(define-public crate-imessage-database-0.1.6 (c (n "imessage-database") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "18bf5n50n8q568rb2blw3439lraa1r4y6rmyj1vi98dibd3wimxq")))

(define-public crate-imessage-database-0.1.7 (c (n "imessage-database") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "00890vqrqmljpq2kn3r2fcx9qppaq9ga0wk7x32qqp6adad25f7p")))

(define-public crate-imessage-database-0.1.8 (c (n "imessage-database") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1nark44sgwzrh7zi054s0my3hmrqv29bzghs0648g48faig6g6x5")))

(define-public crate-imessage-database-0.1.9 (c (n "imessage-database") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1kcw11i05divpklzxza50gc2by0h2qfy58nj8m5sa9qin2382g6j")))

(define-public crate-imessage-database-1.0.0 (c (n "imessage-database") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob"))) (d #t) (k 0)))) (h "1qpmhdgrjsadsklsmibifj6xxzrzap78jjja51vzvpd085gdqx34")))

(define-public crate-imessage-database-1.1.0 (c (n "imessage-database") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "0pdycsb31h978kzxc0pj8sayqpq7smid4546g6in6rdxv9ks8w8q")))

(define-public crate-imessage-database-1.2.0 (c (n "imessage-database") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "1vpk9ig9fkl14hgm91c0ijp31f8gsqy0v9hk17hcbnynp1pk3155")))

(define-public crate-imessage-database-1.3.0 (c (n "imessage-database") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)))) (h "0gdw8ldmxj9wpaldrxsyskgmn2hd69dl8i3ja6a78fi9qrxjlnnm")))

(define-public crate-imessage-database-1.4.0 (c (n "imessage-database") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "069y8hfaa1njhw762wz0scgi46bz7y793qmblcw90mm8dvy6qykq")))

(define-public crate-imessage-database-1.5.0 (c (n "imessage-database") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0s8m4pmjwsglyzn40fid9mdclyqk1877i024y6188kk414gp1ymn")))

(define-public crate-imessage-database-1.6.0 (c (n "imessage-database") (v "1.6.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "11nmrjxjbz2bm6h3a7a3mj0wdy8xbvzja2z0crvxd720gj81v4mn")))

(define-public crate-imessage-database-1.7.0 (c (n "imessage-database") (v "1.7.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "12kf011bm14diakwh7m3y8d9v94cy39qdvi95lyy3a9fb2bri4rw")))

(define-public crate-imessage-database-1.8.0 (c (n "imessage-database") (v "1.8.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("blob" "bundled"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "01mimp55fz7kyi9szd2k31vx9baff3ff3ykd9kyb4fhsqdzisa7q")))

