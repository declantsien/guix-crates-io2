(define-module (crates-io im pu impulse) #:use-module (crates-io))

(define-public crate-impulse-0.1.0 (c (n "impulse") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.10") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "01ilfz3fqqzsv4wzh3lq5z31k3frn4bpm979b5il7yqbx8gqk99l") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-impulse-0.1.1 (c (n "impulse") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.10") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0dkqqi6748dix11wf7k7rrbsis4xy4x1d8c4chga1yc0pv7v5ah0") (f (quote (("default" "console_error_panic_hook"))))))

