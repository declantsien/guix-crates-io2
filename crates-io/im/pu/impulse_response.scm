(define-module (crates-io im pu impulse_response) #:use-module (crates-io))

(define-public crate-impulse_response-0.1.0 (c (n "impulse_response") (v "0.1.0") (d (list (d (n "numpy") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.10") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "19rs38lsm9r0zm1jfhrjdg3ifssyrnc4jd72rpkzh0ymz81wcr6r") (f (quote (("python" "pyo3" "numpy") ("default"))))))

(define-public crate-impulse_response-0.2.0 (c (n "impulse_response") (v "0.2.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0kvk32wcb6gs88mg7n7bjzsksxhhhqyghmpga5wgxyj6ljnlnvcv")))

(define-public crate-impulse_response-0.3.0 (c (n "impulse_response") (v "0.3.0") (d (list (d (n "hdrhistogram") (r "^7.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1ia24m6z4ln573h8960cql41y6ahsrnjjbacc8wjfm4pf7n8nnpr")))

(define-public crate-impulse_response-0.3.1 (c (n "impulse_response") (v "0.3.1") (d (list (d (n "hdrhistogram") (r "^7.1.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0csshy2c07jgj8lfgax6qm9l0r91l89axq27kq1w57ly0qy57sxh")))

(define-public crate-impulse_response-0.4.0 (c (n "impulse_response") (v "0.4.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "15z6z0hwkqap6ikl249h2jghzrwhs2h0s4jwcdjxq0z0r7iiq5wh")))

(define-public crate-impulse_response-0.5.0 (c (n "impulse_response") (v "0.5.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1pcbhndnp53kmkcryiykzjy1wqsyra1jz94hkv0xyqdqlhwbrf5f")))

(define-public crate-impulse_response-0.6.0 (c (n "impulse_response") (v "0.6.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0fsk81rkgrb42lbm859syvcicza9gxn1jbd9m578bdqcwham571y")))

(define-public crate-impulse_response-0.6.1 (c (n "impulse_response") (v "0.6.1") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0pidaqscl8m16xbaph8xlkk8i174yvn2qbn1hr1b0va0y200f4rc")))

