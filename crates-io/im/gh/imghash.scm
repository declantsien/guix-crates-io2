(define-module (crates-io im gh imghash) #:use-module (crates-io))

(define-public crate-imghash-1.0.0 (c (n "imghash") (v "1.0.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "0cy1k3v61c4yzn279mnhv10sbbmqqdgw8ql35hmak4ap6xr7y8sr")))

(define-public crate-imghash-1.1.0 (c (n "imghash") (v "1.1.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "0w82xlqfj95swf8qnriswv41qqxfpqzh5lzdjk4ll5bbyj22p595")))

(define-public crate-imghash-1.1.1 (c (n "imghash") (v "1.1.1") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "0yxmzg5bld0s0bqh0k12s2im4caknmyn21z47ds9mpi65qngkxhq")))

(define-public crate-imghash-1.2.0 (c (n "imghash") (v "1.2.0") (d (list (d (n "image") (r "^0.24.8") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0prdiqddi40iidn8vrl29cmpwnkimqv68rwj4yifjffl5rx7dakc")))

