(define-module (crates-io im gh imghdr) #:use-module (crates-io))

(define-public crate-imghdr-0.1.0 (c (n "imghdr") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "12gmykzsqhxrj717w39zj862qj0yrjqqzl7xga6p8y5w572l88qh")))

(define-public crate-imghdr-0.1.1 (c (n "imghdr") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "03psf7f3g38lv9laxsi680ibw3v5pq2cfmvhdbg6b1x06vs2h48d")))

(define-public crate-imghdr-0.1.2 (c (n "imghdr") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "1m55q0kfi8h24a31yfspm6dlkbg1958gr458wym52n5pf25kdhi2")))

(define-public crate-imghdr-0.1.3 (c (n "imghdr") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "1f3652kjjwjczpychhlbz4g9licmi6raybj4shrpnxg89i4v80p0")))

(define-public crate-imghdr-0.1.4 (c (n "imghdr") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "04qv18vqclnsssj3dgnq4rgl77ymhqrmkafylci34lmmb8gx5cgk")))

(define-public crate-imghdr-0.2.0 (c (n "imghdr") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "0w9f8pm7imvj31gibck85yngkx0hwfw34cfhvp9yx82dm543kk0q")))

(define-public crate-imghdr-0.3.0 (c (n "imghdr") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "1f0sir90h82j7gs765ikhv3zqxv8cskbinnwydiwvnz93vq0rghq")))

(define-public crate-imghdr-0.4.0 (c (n "imghdr") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)))) (h "0m1q7868yb2xdgiz9j1is0z9wxrvnl55s6mvk5c8jvn9543h53ia")))

(define-public crate-imghdr-0.5.0 (c (n "imghdr") (v "0.5.0") (h "0hcb7bpwfjdw76ip2dy8rzgfnl0rf0qywdsr42zcmf9bx62dji4q")))

(define-public crate-imghdr-0.6.0 (c (n "imghdr") (v "0.6.0") (h "06mm80q1mzykkzsny6nclb4l5q33f2wd5hb3vvsbq68y2my6xszj")))

(define-public crate-imghdr-0.7.0 (c (n "imghdr") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0a14y3bs1c2vdqmlv8wa6q5bfl04gbjdyx9kc20sqxjmv4x5zcy8") (f (quote (("std") ("default" "std"))))))

