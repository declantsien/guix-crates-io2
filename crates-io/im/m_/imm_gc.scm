(define-module (crates-io im m_ imm_gc) #:use-module (crates-io))

(define-public crate-imm_gc-0.2.0 (c (n "imm_gc") (v "0.2.0") (d (list (d (n "imm_gc_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "05cq5jx9hgcvra7ja05sxyqcx6ymi8qa2m1kinrjjw5xypcpyj49") (f (quote (("trace") ("derive" "imm_gc_derive"))))))

(define-public crate-imm_gc-0.2.1 (c (n "imm_gc") (v "0.2.1") (d (list (d (n "imm_gc_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1l103ddr2z9l3z37h7zljchnpdcvjj1zyf3y3j56lwqjis9d08lq") (f (quote (("trace") ("derive" "imm_gc_derive"))))))

