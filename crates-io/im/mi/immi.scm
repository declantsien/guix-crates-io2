(define-module (crates-io im mi immi) #:use-module (crates-io))

(define-public crate-immi-0.1.0 (c (n "immi") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gf0fvq0z0a73b3ns0da1683wam161a63bfah2yqvj01y1m2wv3p")))

(define-public crate-immi-0.1.1 (c (n "immi") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0l7xw9xdc3xkpl95zvfz8sa83drgs0600905m067cj18my9081r6")))

(define-public crate-immi-0.1.2 (c (n "immi") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12m5l59qn35f0mxs7f8pfjsp05ia4qb2r5hp3m7q7w523mlln0mf")))

(define-public crate-immi-0.1.3 (c (n "immi") (v "0.1.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0hb5kh74pmafmn9zcg87rqm59b5fswb2xl4bihhnqvh4k5xhh35f")))

(define-public crate-immi-0.2.0 (c (n "immi") (v "0.2.0") (h "1mhq07vmxj8q5gwbqg05jk93k1zvxf03gs16m3iifpwz1pwn07g9")))

(define-public crate-immi-0.3.0 (c (n "immi") (v "0.3.0") (h "0kxdr727x5rwjr0imf3wp1hf8yc5jaba02b4wzm576f1fbi9jacd")))

(define-public crate-immi-1.0.0 (c (n "immi") (v "1.0.0") (h "09ykbkqlf4awinvnn51d6a4bsrbkw0dakb8diqbm9v4szfmg18wm")))

(define-public crate-immi-1.0.1 (c (n "immi") (v "1.0.1") (h "0a3xqvazxcnihl6cb1lx0j127p3mlqwpww31bd7zb48brbwpl94n")))

(define-public crate-immi-1.0.2 (c (n "immi") (v "1.0.2") (h "1kk2i6mh8rpiiv3ym6j9znzagm5f2f4qcaki1ljgw30wlpxqk1fp")))

(define-public crate-immi-1.0.3 (c (n "immi") (v "1.0.3") (h "15qc6895cpify98s07adcmsa2ywblv9xpw0hbl9fc15dxw4d5ll7")))

(define-public crate-immi-1.0.4 (c (n "immi") (v "1.0.4") (h "19lgm33ydm8ibpgry6pxvivzq4ipgqh75nq1q817kpxvfwkd0w3f")))

