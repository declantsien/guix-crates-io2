(define-module (crates-io im ba imbak) #:use-module (crates-io))

(define-public crate-imbak-0.1.0 (c (n "imbak") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "imap") (r "^2.4.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)))) (h "0xcf737bk823mizjyr5yl7vbiyab5k3qjf7yv6yry9f2mpq2z3ll")))

(define-public crate-imbak-0.1.1 (c (n "imbak") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "imap") (r "^2.4.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)))) (h "0h5bb8d6hjpx4xjkx4jgqjpzp4lgcskqvn36ls26i54ir56vsrhk")))

