(define-module (crates-io im g- img-parts) #:use-module (crates-io))

(define-public crate-img-parts-0.1.0 (c (n "img-parts") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "1nk0c54wnbkna69ldb6i8gnvpbh5awhdf9mxll9zmfvb60a9907m")))

(define-public crate-img-parts-0.1.1 (c (n "img-parts") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)))) (h "1wv30w2gr2v2r4kqxjazg3b1hfk8jkrza38z3wzhsd3jrhc9gg7r")))

(define-public crate-img-parts-0.2.0 (c (n "img-parts") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5.3") (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "15lrq745knkcjzznvvhc6bmi9qx5lqhd862nf8ijpljblyhs8gkh") (f (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2.1 (c (n "img-parts") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5.3") (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "0z656jnv2fdnlq7rm4i8b5kybhz12lf9sz8ib0wpzjnc6a9vqihm") (f (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2.2 (c (n "img-parts") (v "0.2.2") (d (list (d (n "bytes") (r "^0.5.3") (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "0nblzdxmzdmx3ck9n9i9slqkq1lpqagg7xdlbpn8c1gd4rg0plj3") (f (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2.3 (c (n "img-parts") (v "0.2.3") (d (list (d (n "bytes") (r "^0.5.3") (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)))) (h "1rg4jhkza04lhb6gndzgvr7vb72l8wvp97zwdzaddaqp40fqf472") (f (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.3.0 (c (n "img-parts") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)))) (h "1w01cxdpps5iz5f1cdiydiisx4lg66jjgvbg8qsgr9criljmi4xi") (f (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

