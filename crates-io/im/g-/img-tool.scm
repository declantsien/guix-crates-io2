(define-module (crates-io im g- img-tool) #:use-module (crates-io))

(define-public crate-img-tool-0.1.0 (c (n "img-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "ico") (r "^0.3.0") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.2") (d #t) (k 0)))) (h "1sj57bh1m4zkvyn29w4gcdwqf0f9ibf4jpbhch2k0mm8d7minhsl")))

