(define-module (crates-io im g- img-qoi) #:use-module (crates-io))

(define-public crate-img-qoi-0.1.0 (c (n "img-qoi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02a9pqp1a5p1biwxvabh4wx737f9arqr5y1igabq3zx8lvjh78r3")))

