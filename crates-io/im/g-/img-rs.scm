(define-module (crates-io im g- img-rs) #:use-module (crates-io))

(define-public crate-img-rs-1.0.8 (c (n "img-rs") (v "1.0.8") (h "12cdhvb4n1gsxwj8qidl5df137k85rzf5k8lf44kfvccs826gmrc")))

(define-public crate-img-rs-2.27.0 (c (n "img-rs") (v "2.27.0") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vf52bvr98cyblafp4y5si6b3iyphnb26mpan0655pk7nqdc44fb") (f (quote (("internal-bindgen-on-build" "bindgen"))))))

