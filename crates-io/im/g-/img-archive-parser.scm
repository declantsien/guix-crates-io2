(define-module (crates-io im g- img-archive-parser) #:use-module (crates-io))

(define-public crate-img-archive-parser-0.1.0 (c (n "img-archive-parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "13g3zqhbga9wz30wjr6j1q72kv5pflfya18jx7bylzcdvphhadyl")))

(define-public crate-img-archive-parser-0.1.1 (c (n "img-archive-parser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "011vgchjxknl48hr0a6m9n6al8nyknmx18wis5ks7grp6mii13nx")))

