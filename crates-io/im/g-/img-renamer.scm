(define-module (crates-io im g- img-renamer) #:use-module (crates-io))

(define-public crate-img-renamer-1.0.0 (c (n "img-renamer") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vfs") (r "^0.4.0") (d #t) (k 2)))) (h "0fs27crii8klkbhjc0l7abvy8lgnsknarc2y8x2ma9hfs7w154fr")))

(define-public crate-img-renamer-1.0.1 (c (n "img-renamer") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vfs") (r "^0.4.0") (d #t) (k 2)))) (h "15vp3cjj7n8jdsfhc65x7nx537m3v34kszgn3k3mgqb6ba6g8c53")))

(define-public crate-img-renamer-1.0.2 (c (n "img-renamer") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vfs") (r "^0.4.0") (d #t) (k 2)))) (h "1axc2zgyggfp91jn6s83xzfw2s8d9bi9q75s5kzn8vqcx7ki0fwy")))

(define-public crate-img-renamer-2.0.0 (c (n "img-renamer") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vfs") (r "^0.4.0") (d #t) (k 2)))) (h "06wqcksfdsiin5npfrgmy3s9bfgw83jsin4b335zvrf46xly6h2i")))

(define-public crate-img-renamer-2.0.1 (c (n "img-renamer") (v "2.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vfs") (r "^0.4.0") (d #t) (k 2)))) (h "05lh2ip8y5ybzr0aw5f5igg35j6wwgfm11mgp0afv6207r4z2s7z")))

