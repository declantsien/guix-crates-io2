(define-module (crates-io im g- img-to-ascii) #:use-module (crates-io))

(define-public crate-img-to-ascii-0.1.0 (c (n "img-to-ascii") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0gr0df8zfn90cg0mchn1v7fkra7gxlqlcza1hzd0imdw5ja1wmyg")))

(define-public crate-img-to-ascii-0.1.1 (c (n "img-to-ascii") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0fhg02my3pi0735dfj527r37r11bz0zhsnzr94nzrcsar4f56hfh")))

(define-public crate-img-to-ascii-0.1.2 (c (n "img-to-ascii") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1i7a5yhamp9nf70qav2f1wd2ri05pqld108b2i9jpc4ngbs6kh1n")))

(define-public crate-img-to-ascii-0.1.3 (c (n "img-to-ascii") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1xc8gjcnmvrr6yq5z4fjgzr8q6bsm8jk407gqasbwa3630sb5c6y")))

(define-public crate-img-to-ascii-1.0.0 (c (n "img-to-ascii") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0z0kzb3c22b39rkb90r0aplxr5pgi652l4a5lv4bz0mvpd966mj7")))

