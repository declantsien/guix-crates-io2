(define-module (crates-io im -n im-native-dialog) #:use-module (crates-io))

(define-public crate-im-native-dialog-0.1.0 (c (n "im-native-dialog") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0irsda6jjrhyfrnix56bam33r0filxx9jf8h48wjifm20fziss49")))

(define-public crate-im-native-dialog-0.1.1 (c (n "im-native-dialog") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "051656xky70qk6pp9fdzcr9c7xgz0hmwpdc9nim2zwblm3rdww18")))

(define-public crate-im-native-dialog-0.2.0 (c (n "im-native-dialog") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-dialog") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "027qgh0cmzc8xw6v5nipz606qxqdxhwi7s0ny7rm5kikgrz71cxf")))

(define-public crate-im-native-dialog-0.3.0 (c (n "im-native-dialog") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12r26v5jpv1ssqizzidgslbpcjnchxrgp8drik7p79pds1j3iah3")))

