(define-module (crates-io im gk imgk-app) #:use-module (crates-io))

(define-public crate-imgk-app-0.0.1 (c (n "imgk-app") (v "0.0.1") (h "01r4n63j8mhlzl5cssmg2mssjncs8rg0m68i24kip9mlh2c3izin")))

(define-public crate-imgk-app-0.0.2 (c (n "imgk-app") (v "0.0.2") (h "0b06c87jjzwh29095jqznjm7pr5h2k96b82x450dlf42981k6wf6")))

