(define-module (crates-io im gf imgflipparser) #:use-module (crates-io))

(define-public crate-imgflipparser-0.1.0 (c (n "imgflipparser") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0af78737g3b9zir6cvp2sxp1zg7pmn1qm5sznb9khxxswiwr75lr")))

(define-public crate-imgflipparser-0.1.1 (c (n "imgflipparser") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0jxynd5r7ylj6kzxscvf98vni198xanmkq6y1nglgi3240a92anz")))

(define-public crate-imgflipparser-0.1.2 (c (n "imgflipparser") (v "0.1.2") (d (list (d (n "actix-web") (r "^3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "141c7pmxxn5nybr612y9zb2s744a2ls8zgp25xlmfi710018j0az") (f (quote (("webapi" "actix-web"))))))

(define-public crate-imgflipparser-0.1.3 (c (n "imgflipparser") (v "0.1.3") (d (list (d (n "actix-web") (r "^3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1ykdhn0766kqm2vz0pf2cvfrnmy3ar326722jqs12w66pa6lda8y") (f (quote (("webapi" "actix-web"))))))

(define-public crate-imgflipparser-0.1.4 (c (n "imgflipparser") (v "0.1.4") (d (list (d (n "actix-web") (r "^3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "06j1x5cbj668x9n8271znlpczkc71cqq1lcb0ja9wvrwk1vxa5jm") (f (quote (("webapi" "actix-web"))))))

