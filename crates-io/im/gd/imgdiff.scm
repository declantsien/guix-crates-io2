(define-module (crates-io im gd imgdiff) #:use-module (crates-io))

(define-public crate-imgdiff-0.1.0 (c (n "imgdiff") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0gy9492p624gic6psbwi9f035nxvyc5g1vv95jrg5wca8wljmzyj")))

(define-public crate-imgdiff-0.1.1 (c (n "imgdiff") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0gfr1q65sm1knxn5jayg5i9zkzmv9gvsxq3igzi8p2indamcirzx")))

