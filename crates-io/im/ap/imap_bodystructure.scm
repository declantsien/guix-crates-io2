(define-module (crates-io im ap imap_bodystructure) #:use-module (crates-io))

(define-public crate-imap_bodystructure-0.1.0 (c (n "imap_bodystructure") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1vfgqa859xns4m8n0mdzi2hd0jyhjhnrjlwp8n23wj5j4r8d0vp4")))

(define-public crate-imap_bodystructure-0.1.1 (c (n "imap_bodystructure") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0bpp5p0iqkzczwa7awpa8acs0fipgyg3729yrykr64sfhvhf3ng3")))

(define-public crate-imap_bodystructure-0.1.2 (c (n "imap_bodystructure") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1cg8y01lbk3fj4lfvsswbp74wak2aqdq7mzc40l92vqdakvxz3a0")))

(define-public crate-imap_bodystructure-0.1.3 (c (n "imap_bodystructure") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "11a59wv8pykzgdawiwcvpfdkabz8l29bdmy3ximc5kljnz5xlw1k")))

(define-public crate-imap_bodystructure-0.2.0 (c (n "imap_bodystructure") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1jdmdwjipfswxqqkp15hwncl9pnkjb1nmiylqiqafc33ymy4sman")))

(define-public crate-imap_bodystructure-0.2.1 (c (n "imap_bodystructure") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1i5z7gilvijq3xqbgrwpif2xh9r97z9p7daxydiwss40w5ssav4y")))

(define-public crate-imap_bodystructure-0.2.2 (c (n "imap_bodystructure") (v "0.2.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0cwi0vzwx26jjl6d8w452p31k5b1rgzg2rnc7kmz316vbwmhiqiv")))

(define-public crate-imap_bodystructure-0.2.3 (c (n "imap_bodystructure") (v "0.2.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0dj9ml0163qb04s1iwnnb5sj7g1g0jac34kadfqshv97jj6zbxc5")))

(define-public crate-imap_bodystructure-0.2.4 (c (n "imap_bodystructure") (v "0.2.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "13j4annngynlmpzidl3aqhll1xn48dr2rq8cjzhxhf5vbnziwpdp")))

(define-public crate-imap_bodystructure-0.2.5 (c (n "imap_bodystructure") (v "0.2.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0r9653j1lf3abn4sf4si7q090ap2irhmh62273xb798la13fj4yp")))

(define-public crate-imap_bodystructure-0.3.0 (c (n "imap_bodystructure") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "09445fin4k9bbd8jwj9ldkijz88ncw6pw3jrq0vmnq3z9q1wp2gq")))

(define-public crate-imap_bodystructure-0.3.1 (c (n "imap_bodystructure") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "15ls6s34d083i3g05pmipazqjyvsm05mfalkgayx2njr2ixz2kh7")))

(define-public crate-imap_bodystructure-0.3.2 (c (n "imap_bodystructure") (v "0.3.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "01fm9zdc3lvr30d3r1a96ak83xrfvf7ick01f97h2pa6h9bzchb7")))

(define-public crate-imap_bodystructure-0.3.3 (c (n "imap_bodystructure") (v "0.3.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "168zafd7dv2j1vpsv5k8vywfhcbr4inzrn8sjpddqmkpdrxgmkdh")))

(define-public crate-imap_bodystructure-0.3.4 (c (n "imap_bodystructure") (v "0.3.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "087k6k3nh346ph9i2hh65z2igklph0a1yhxvk3sm68r7vwqw6vcm")))

(define-public crate-imap_bodystructure-0.3.5 (c (n "imap_bodystructure") (v "0.3.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0hag0ln5mxbqcj4pjdck575cyafaxj9r2jg1md04h0ax1n4pzwd0")))

(define-public crate-imap_bodystructure-0.3.6 (c (n "imap_bodystructure") (v "0.3.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0qgjqk8w2pw5r5sp4d1wh6cmlpmf0dd02qnd7s4gcw9s7gdn94bg")))

(define-public crate-imap_bodystructure-0.3.7 (c (n "imap_bodystructure") (v "0.3.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1rsi9n90q5p4w95c81cqkfxzhnhq6409wy17p5fjss0r10n2ph7j")))

