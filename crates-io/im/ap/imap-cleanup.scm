(define-module (crates-io im ap imap-cleanup) #:use-module (crates-io))

(define-public crate-imap-cleanup-0.1.0 (c (n "imap-cleanup") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "imap") (r "^2.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)))) (h "0mdpzwc8jrpf9wi4pzm62ghv3m46vdx1cgq039fy8c0kg9mjks62")))

