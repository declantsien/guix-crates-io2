(define-module (crates-io im ap imapserver-cli) #:use-module (crates-io))

(define-public crate-IMAPServer-cli-0.1.0 (c (n "IMAPServer-cli") (v "0.1.0") (d (list (d (n "IMAPServer-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("compat"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "1v3ll4y55fvf5lfq4ghsa7fik5g668m4h3ih27l2s6wqanmqkjg7")))

