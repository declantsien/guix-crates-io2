(define-module (crates-io im ap imap-proto) #:use-module (crates-io))

(define-public crate-imap-proto-0.1.0 (c (n "imap-proto") (v "0.1.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "18dc6s5s65z4qcrza0f760v21sbjc24czizwzizci5xk7nsvhp8y")))

(define-public crate-imap-proto-0.2.0 (c (n "imap-proto") (v "0.2.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "0v5b2nh7p60b14gvifywcwwqq9fxiwh24yfnhhlbxyzkpdam8mmw")))

(define-public crate-imap-proto-0.3.0 (c (n "imap-proto") (v "0.3.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "1q19xl8rrbsjpxsrv9r1vmyhpismzd6fy15cdj7h0h40wb251fn5")))

(define-public crate-imap-proto-0.4.0 (c (n "imap-proto") (v "0.4.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "1xsnffrlzgf6kmc47fsdfkvypmm6r10smsw069kdl388h42h0m6a")))

(define-public crate-imap-proto-0.4.1 (c (n "imap-proto") (v "0.4.1") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "0ljrhqchr1v09azm7fniligc4g57ppgb358f14r9bq2q22ny2cw7")))

(define-public crate-imap-proto-0.5.0 (c (n "imap-proto") (v "0.5.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0sr0wgznbg4s6f941bmj1vbd44ilv07nqlvlprdqi61na70pb5gl")))

(define-public crate-imap-proto-0.6.0 (c (n "imap-proto") (v "0.6.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0lgkr5y8hmp9wpnbykm31x0yhdyg7752g6ra3fgma0gp79lw3n78")))

(define-public crate-imap-proto-0.7.0 (c (n "imap-proto") (v "0.7.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "16ikj7axizv8f5zvxkl6c4mx1kc4ap673c1i6n4jiw7sc4fppry4")))

(define-public crate-imap-proto-0.8.0 (c (n "imap-proto") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1f67l1irz4n1jpdk6231b2hg2iv3h4kw81kfbhhlwmsnvzaq9qq3")))

(define-public crate-imap-proto-0.8.1 (c (n "imap-proto") (v "0.8.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "11yp9699j73y4ws4nmgwq8cxsyfqxparcmaq1mwmc7qaf0c84n4s")))

(define-public crate-imap-proto-0.9.0 (c (n "imap-proto") (v "0.9.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "12f5jsbk7sx1znqlcjhxg7k6zpw370yrkghajl5gii94kd9cm4hv")))

(define-public crate-imap-proto-0.9.1 (c (n "imap-proto") (v "0.9.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1xzpmxhd4br5z70s77c1qxl2h8qdxzcyvnp1vw8q3zlakxl0ssg0")))

(define-public crate-imap-proto-0.10.0 (c (n "imap-proto") (v "0.10.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1wnl2ii2c34i4cqzvmhl60mjslrydb0j6iqdlnjvr9wbs6kmgfhi")))

(define-public crate-imap-proto-0.10.1 (c (n "imap-proto") (v "0.10.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1d8vwg6m637jp73vhj1564d7wn1gk9mlgjw8jc5bzjb57r7ikb7q")))

(define-public crate-imap-proto-0.10.2 (c (n "imap-proto") (v "0.10.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "0by2vz8v2m5sdj0xgr9fxxw35zjkg7ah3l9z1gbpb2dcspqxx9hn")))

(define-public crate-imap-proto-0.11.0 (c (n "imap-proto") (v "0.11.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "10ii8q7a733y0xmi4qpn3db0dbcmjjpn5ydn1q0rn3xqwngbk49h")))

(define-public crate-imap-proto-0.12.0 (c (n "imap-proto") (v "0.12.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "0hrsrywhjvdd9c7zn0acm6skbw0bp35hk8g5d6adpwwg44927xvl")))

(define-public crate-imap-proto-0.12.1 (c (n "imap-proto") (v "0.12.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "0i0flfb4cn8wgmf778ng16iw4hgmj20w9s1lpmrllvc1wq75g706")))

(define-public crate-imap-proto-0.12.2 (c (n "imap-proto") (v "0.12.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "112w3qp62hc56qq0pp1m0rsq3c2bkcl7n88hhs8hy9y3221wda09")))

(define-public crate-imap-proto-0.13.0 (c (n "imap-proto") (v "0.13.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "15jbn35f2g09f1iiydvyw68dkk4a88jmcc4g4g1i3rk7lhykx8v8")))

(define-public crate-imap-proto-0.13.1 (c (n "imap-proto") (v "0.13.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "0vhz1rsw2wp32gbab8k28p65b05972dcd8wpybgn11gfxkwniv7s") (y #t)))

(define-public crate-imap-proto-0.14.0 (c (n "imap-proto") (v "0.14.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "1isgz9h0p0rfrk9w02yqjc3cfyv0rzsfsaabawr1vm6ykfsb12s4")))

(define-public crate-imap-proto-0.14.1 (c (n "imap-proto") (v "0.14.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "1dph0ma1869063p0f5d3vx3gds4706zcb1fgcfi863vpsi5my106")))

(define-public crate-imap-proto-0.14.2 (c (n "imap-proto") (v "0.14.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "1mcimab72d6bydvv8nbk3xid8i5xlbkz9p5lrjiywhcmg0gk6r3w")))

(define-public crate-imap-proto-0.14.3 (c (n "imap-proto") (v "0.14.3") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "1fxgxz5v759hwvvqfil6cy3kwz1x8r8lxq4aay70gdpgg5mb9n9s")))

(define-public crate-imap-proto-0.15.0 (c (n "imap-proto") (v "0.15.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "142nik55x4h7jw8hkb608fii6b7gbbrqkqlyh24k3znm3g366vyk")))

(define-public crate-imap-proto-0.16.0 (c (n "imap-proto") (v "0.16.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0nxd10n3zswpq3kg8jfa82b9l19r223yvbc7z4pvw6g4q245sam9")))

(define-public crate-imap-proto-0.16.1 (c (n "imap-proto") (v "0.16.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1njg07daxjb71s2pwjbw0rhzh43xgshdi5yvrm486h2sdl4ahmpj")))

(define-public crate-imap-proto-0.16.2 (c (n "imap-proto") (v "0.16.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1q1k75bxqr7ai5lwm6ysayi4n6zjqlbdc0hhm05b464l2xiinfzp")))

(define-public crate-imap-proto-0.16.3 (c (n "imap-proto") (v "0.16.3") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1iqy8aq8w9mb5gc88k6dhakv1isdp1ia1i4n6fg0a5llwv32ap1h") (r "1.58")))

(define-public crate-imap-proto-0.16.4 (c (n "imap-proto") (v "0.16.4") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0q93r7v246hyc9hxqy0lqm9j30hjaaknn282k0fcpj42d3b0rrr2") (r "1.61")))

(define-public crate-imap-proto-0.16.5 (c (n "imap-proto") (v "0.16.5") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0ml8bmfjm5wiza2gznav4jhakv37gkxjcsl2rsg6yas64samsmfy") (r "1.61")))

