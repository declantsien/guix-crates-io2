(define-module (crates-io im _i im_interval_tree) #:use-module (crates-io))

(define-public crate-im_interval_tree-0.1.0 (c (n "im_interval_tree") (v "0.1.0") (h "0dgqazm8n9zlfdkkizj387ap6f98mk1if8g5wckz59xi8mjw7hy2")))

(define-public crate-im_interval_tree-0.1.1 (c (n "im_interval_tree") (v "0.1.1") (h "1ac8xb607hanckbpsvmrm7mqv7g95i3wqf6dkix1h5m1qzm76lbp")))

