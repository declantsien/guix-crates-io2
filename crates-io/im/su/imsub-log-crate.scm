(define-module (crates-io im su imsub-log-crate) #:use-module (crates-io))

(define-public crate-imsub-log-crate-0.1.0 (c (n "imsub-log-crate") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "lapin") (r "^2") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "13lgy1hwpdl5g9w1qp6p2klbigi11nnfqldvf6hkb768qjf4agz4") (f (quote (("with-serde" "protobuf/with-serde") ("default" "with-serde"))))))

