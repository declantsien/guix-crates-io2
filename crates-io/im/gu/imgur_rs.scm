(define-module (crates-io im gu imgur_rs) #:use-module (crates-io))

(define-public crate-imgur_rs-0.1.0 (c (n "imgur_rs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0vwcfpnsgjd4d7flflhcj0vp9s6jl6xhpm1k5gws0241z9sjj6aw")))

(define-public crate-imgur_rs-0.1.1 (c (n "imgur_rs") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1454cqncjmfaj5yllqs8bmylsmn8y1ssd47f3ixl555zs9h38hz0")))

