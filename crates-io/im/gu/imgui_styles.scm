(define-module (crates-io im gu imgui_styles) #:use-module (crates-io))

(define-public crate-imgui_styles-0.1.0 (c (n "imgui_styles") (v "0.1.0") (d (list (d (n "imgui") (r "^0.11") (f (quote ("docking"))) (d #t) (k 0)))) (h "050rxx40v554fxnm8wmqj25id80layrz1zbdlpl9jpm0bp1g8cmy")))

(define-public crate-imgui_styles-0.2.0 (c (n "imgui_styles") (v "0.2.0") (d (list (d (n "imgui") (r "^0.11") (f (quote ("docking"))) (d #t) (k 0)))) (h "1b5xy92fgdh9w0na1ajiam5qp53scz8nwg941jzvi4ri605qmhr9")))

(define-public crate-imgui_styles-0.2.1 (c (n "imgui_styles") (v "0.2.1") (d (list (d (n "imgui") (r "^0.11") (f (quote ("docking"))) (d #t) (k 0)))) (h "0r6hh8yn3bc96d05w2vi76pv4sg69ld8zclmsfm6gz0cgla8d5kk")))

