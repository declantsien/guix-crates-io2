(define-module (crates-io im gu imgui-inspect) #:use-module (crates-io))

(define-public crate-imgui-inspect-0.1.0 (c (n "imgui-inspect") (v "0.1.0") (d (list (d (n "imgui") (r "^0.1.0-pre") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1njkzridsa6md9glrd36bhsj0ai7cbllhi6avdkdxqasfsmix630")))

(define-public crate-imgui-inspect-0.2.0 (c (n "imgui-inspect") (v "0.2.0") (d (list (d (n "imgui") (r "^0.1.0-pre") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1q587kzs4asxnbjhc7zjh1p3b43clfgw371cs75a7cv2aa2f55fi")))

(define-public crate-imgui-inspect-0.3.0 (c (n "imgui-inspect") (v "0.3.0") (d (list (d (n "imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0qnffg01fpsibri2w05rx0pp868lnnbwqsfazdxpj2j711rfp3y2")))

(define-public crate-imgui-inspect-0.3.1 (c (n "imgui-inspect") (v "0.3.1") (d (list (d (n "imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0srwjs6xx9nkcqnaaalr4jmpwvvk2sfw65xahrfi288m81fm9mk8")))

(define-public crate-imgui-inspect-0.3.2 (c (n "imgui-inspect") (v "0.3.2") (d (list (d (n "imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0g2rqg1aa9av5d0kacq9fxyccca6nng3gp3qlvhqxvv1f60frc34")))

(define-public crate-imgui-inspect-0.3.3 (c (n "imgui-inspect") (v "0.3.3") (d (list (d (n "imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0mqcljvxp5bq20d407g13zy839qw3rkygv4q5vrjccx931xlwmbn")))

(define-public crate-imgui-inspect-0.3.4 (c (n "imgui-inspect") (v "0.3.4") (d (list (d (n "imgui") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3.0") (d #t) (k 0)))) (h "18clx4gq1r3f0if1qmldgd1pwx38ikwb938d9wcvbycw9y16lcqv")))

(define-public crate-imgui-inspect-0.4.0 (c (n "imgui-inspect") (v "0.4.0") (d (list (d (n "imgui") (r "^0.3") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.3") (d #t) (k 0)))) (h "0gjxmnz9zm3vb5pp87046fnp0mbl31d7vzn2m5adfz51aq0h61fq")))

(define-public crate-imgui-inspect-0.5.0 (c (n "imgui-inspect") (v "0.5.0") (d (list (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.5") (d #t) (k 0)))) (h "1l0lkdpvdcinh4lswn6n91v8sf8zkyyv1gblqdgcmcp7gj9i8wyz")))

(define-public crate-imgui-inspect-0.6.0 (c (n "imgui-inspect") (v "0.6.0") (d (list (d (n "imgui") (r "^0.5") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.6") (d #t) (k 0)))) (h "08n2h8qfmidnr4fjch5lj707kc5y2lscjq7dac7jf8c7wmjj96g1")))

(define-public crate-imgui-inspect-0.7.0 (c (n "imgui-inspect") (v "0.7.0") (d (list (d (n "imgui") (r "^0.6") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.7") (d #t) (k 0)))) (h "1wcw8gq3v32l5r1h306bjlqgm4vccbvm29iycwzpixbbgr94h8l8")))

(define-public crate-imgui-inspect-0.7.1 (c (n "imgui-inspect") (v "0.7.1") (d (list (d (n "imgui") (r "^0.6") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.7.1") (d #t) (k 0)))) (h "0ss6rr47g2nfax9v403003idf1szvrxy67wcwvyq1i7l25b542jf")))

(define-public crate-imgui-inspect-0.8.0 (c (n "imgui-inspect") (v "0.8.0") (d (list (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-inspect-derive") (r "^0.8.0") (d #t) (k 0)))) (h "1z34in8axzb4gwnml3gy86dkyzjn5diqkzh3srbvqnbsws006v40")))

