(define-module (crates-io im gu imgui-opengl-renderer) #:use-module (crates-io))

(define-public crate-imgui-opengl-renderer-0.1.0 (c (n "imgui-opengl-renderer") (v "0.1.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "0c4fw24sd46z5pqkblppyad22j12b48iqcgv322cmln0997qlmri")))

(define-public crate-imgui-opengl-renderer-0.2.0 (c (n "imgui-opengl-renderer") (v "0.2.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-sdl2") (r "^0.1.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "07vg7f3dnajabqhryb3hsgv3ig0slb9r9hsapk7v9hxpdvvc5gbp")))

(define-public crate-imgui-opengl-renderer-0.2.1 (c (n "imgui-opengl-renderer") (v "0.2.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-sdl2") (r "^0.2.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "07b426qq5hbycqhxpbanvangg78w31fzjzq9rnxy2lm7pj41chd0")))

(define-public crate-imgui-opengl-renderer-0.2.2 (c (n "imgui-opengl-renderer") (v "0.2.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-sdl2") (r "^0.2.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "0165wxl5rizad9sakbm0p7gf2jsf8mc35xijiy780niax6gsbzqg")))

(define-public crate-imgui-opengl-renderer-0.2.3 (c (n "imgui-opengl-renderer") (v "0.2.3") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.20") (d #t) (k 0)) (d (n "imgui-sdl2") (r "^0.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "18rbcia1vl9hchqgy8nscw7pykmkxffd4bf23bfvfafix380diad")))

(define-public crate-imgui-opengl-renderer-0.3.0 (c (n "imgui-opengl-renderer") (v "0.3.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)))) (h "1r5n2knwqggflkj23hv6j0aqpfw8vig66xc4a6qv54w19jz4p81l")))

(define-public crate-imgui-opengl-renderer-0.3.1 (c (n "imgui-opengl-renderer") (v "0.3.1") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)))) (h "0avqn7n9dfsax4xzig0r9bspin2sl4fmfg22vscq2n5rp0mdfmig")))

(define-public crate-imgui-opengl-renderer-0.3.2 (c (n "imgui-opengl-renderer") (v "0.3.2") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)))) (h "0xzfd492cs9cwvndc5cl0iy47gx33iqxfzp0phflr3pk0zx6an9m")))

(define-public crate-imgui-opengl-renderer-0.4.0 (c (n "imgui-opengl-renderer") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)))) (h "07gjp03c0yfjcnv6ma8syy05vhplsx9zxirqi3l01r3lh8fxmf64")))

(define-public crate-imgui-opengl-renderer-0.5.0 (c (n "imgui-opengl-renderer") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.1") (d #t) (k 0)))) (h "12qy3925481agrgp7kn1krl0v7lz86vp1fwjav3j6zizax3pqzsd")))

(define-public crate-imgui-opengl-renderer-0.6.0 (c (n "imgui-opengl-renderer") (v "0.6.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.2") (d #t) (k 0)))) (h "0ks5h8jfsl7fy5ih4xdif6i2dssifsn7ilfc6n5y0nfxgfxzpgyb")))

(define-public crate-imgui-opengl-renderer-0.6.1 (c (n "imgui-opengl-renderer") (v "0.6.1") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.2") (d #t) (k 0)))) (h "1hq7hqcs5wzccq5x2xc4cgpvdajv2h58k8vb42xlil511427dl04")))

(define-public crate-imgui-opengl-renderer-0.7.0 (c (n "imgui-opengl-renderer") (v "0.7.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.3.0") (d #t) (k 0)))) (h "16ax39k83pfv0cc9jv9xh9shvvgb7jp41426m1sbz37vwpvnf1gy")))

(define-public crate-imgui-opengl-renderer-0.8.0 (c (n "imgui-opengl-renderer") (v "0.8.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.4.0") (d #t) (k 0)))) (h "0mhyjy72bz2wdfcaqfr8hvyd22m8mngjjvdz4k7rpzpirgxc42v1")))

(define-public crate-imgui-opengl-renderer-0.9.0 (c (n "imgui-opengl-renderer") (v "0.9.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.5.0") (d #t) (k 0)))) (h "1ams794jm69h8vlms83297gzn9zb4cifyvb8vldasdpzz2a2qwci")))

(define-public crate-imgui-opengl-renderer-0.10.0 (c (n "imgui-opengl-renderer") (v "0.10.0") (d (list (d (n "gl_generator") (r "^0.9.0") (d #t) (k 1)) (d (n "imgui") (r "^0.6.0") (d #t) (k 0)))) (h "1zl4cx2zl7mhrja2kwvlhval23v6s5jc1ldz1r123i83qr7la80c")))

(define-public crate-imgui-opengl-renderer-0.11.0 (c (n "imgui-opengl-renderer") (v "0.11.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r "^0.7.0") (d #t) (k 0)))) (h "14j9k7a0gkpraiinayqghs10s66y9qxvadc5nr30hfn9yvl7nb2n")))

(define-public crate-imgui-opengl-renderer-0.11.1 (c (n "imgui-opengl-renderer") (v "0.11.1") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r ">=0.7.0, <0.9.0") (d #t) (k 0)))) (h "12ib45axkx8yjaf3n436sv822nqkjkcra9abvfphn1ijvp3w4y6v")))

(define-public crate-imgui-opengl-renderer-0.12.0 (c (n "imgui-opengl-renderer") (v "0.12.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "1jnnn11ll11hsq2ry74d3lj5s8n38sdj2c7ff9ddd1wpphzb3hsi")))

(define-public crate-imgui-opengl-renderer-0.12.1 (c (n "imgui-opengl-renderer") (v "0.12.1") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r ">=0.9.0, <0.12.0") (d #t) (k 0)))) (h "1jpaz3k4vrgcgw05r591nn1whkc9wkjfwhkrwwpw99gliqdc21z0")))

