(define-module (crates-io im gu imgui) #:use-module (crates-io))

(define-public crate-imgui-0.0.1 (c (n "imgui") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.8") (o #t) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("glutin"))) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "12vi82mpk97vfjcc47ijifxbqkd6j8agk09qbcd4a3ng5860dma9") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.2 (c (n "imgui") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.8") (o #t) (k 0)) (d (n "glium") (r "^0.8") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "16kbg8qm6plpz2iks8kznw4mqwf1ammhwgk3gd46i60c2qm18vlf") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.3 (c (n "imgui") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.9") (o #t) (k 0)) (d (n "glium") (r "^0.9") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "10j5v5yjcppkjk9lcvvbpfv2p0y1h23z3gcgfbbmryrjvrxff315") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.4 (c (n "imgui") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.10") (o #t) (k 0)) (d (n "glium") (r "^0.10") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "09wpfn5n5z93af435ihivzx1ix3s3xiqk6icy4jgxljlicir8cj3") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.5 (c (n "imgui") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.12") (o #t) (k 0)) (d (n "glium") (r "^0.12") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1x02xyhc4pk8vv284r7v3bmc7kkphmvgwqv5pdiqkgva4r0fjg6j") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.6 (c (n "imgui") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.13") (o #t) (k 0)) (d (n "glium") (r "^0.13") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0vk4nlhvizh0dp6z534g509iwsvjf5cspq1g51xmqyvc9jyf8gpx") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.7 (c (n "imgui") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.13") (o #t) (k 0)) (d (n "glium") (r "^0.13") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "08aq3fnr70b29zg5dk5iyf3wsk46i0568b9qr2p7l48shfqnvcm4") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.8 (c (n "imgui") (v "0.0.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.14") (o #t) (k 0)) (d (n "glium") (r "^0.14") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0g68370s4svsa1kclnzmf1800yx7ydw138pq5i2izaif5rpjwcdi") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.9 (c (n "imgui") (v "0.0.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.14") (o #t) (k 0)) (d (n "glium") (r "^0.14") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0swp8a55x30l1cavfk17rsfc07h7yzkdpfjfry20h7nmimp7gv3f") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.10 (c (n "imgui") (v "0.0.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.15") (o #t) (k 0)) (d (n "glium") (r "^0.15") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n41hwl5pljw242w52l1fb67wkdsqz5570zgif5v3zzkyxg5z253") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.11 (c (n "imgui") (v "0.0.11") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.16") (o #t) (k 0)) (d (n "glium") (r "^0.16") (f (quote ("glutin"))) (k 2)) (d (n "imgui-sys") (r "^0.0.11") (d #t) (k 0)))) (h "1dzrclc9h54vsjz9wdvf11qpkppmd25spn0xrj24fi5abfid5bq1") (f (quote (("default" "glium"))))))

(define-public crate-imgui-0.0.13 (c (n "imgui") (v "0.0.13") (d (list (d (n "imgui-sys") (r "^0.0.13") (d #t) (k 0)))) (h "14gk5fsxxd7agckq59cr1ws1km2w7b188x3kypv2riq4gmfwf9g9")))

(define-public crate-imgui-0.0.14 (c (n "imgui") (v "0.0.14") (d (list (d (n "imgui-sys") (r "^0.0.14") (d #t) (k 0)))) (h "1sdl2czrsfrzsjjvc8c89ikh9ys2za5kr5mfacr3m510dlcqqw75")))

(define-public crate-imgui-0.0.15 (c (n "imgui") (v "0.0.15") (d (list (d (n "imgui-sys") (r "^0.0.15") (d #t) (k 0)))) (h "135nqa5byfxs1fjis5aqlw4xaqymsvimaalsdmlyxcg20pzkmbi6")))

(define-public crate-imgui-0.0.16 (c (n "imgui") (v "0.0.16") (d (list (d (n "imgui-sys") (r "^0.0.16") (d #t) (k 0)))) (h "1rissgbjsxapxrnc1xqzkkwv7rgdikayyd0w001xpadjxgv216b1")))

(define-public crate-imgui-0.0.17 (c (n "imgui") (v "0.0.17") (d (list (d (n "imgui-sys") (r "^0.0.17") (d #t) (k 0)))) (h "17dsd5cgnk2ljx17z06pavgzmxx7sisx7xlmrd15wpw3yrhjshw4")))

(define-public crate-imgui-0.0.18 (c (n "imgui") (v "0.0.18") (d (list (d (n "imgui-sys") (r "^0.0.18") (d #t) (k 0)))) (h "05s6x39wp9cw91914cq1cn7bh83539ld3qndayfb81ik4v9wvv4w")))

(define-public crate-imgui-0.0.19 (c (n "imgui") (v "0.0.19") (d (list (d (n "imgui-sys") (r "^0.0.19") (d #t) (k 0)))) (h "0nqci2rj7ksny8vzlqw6bacz6wf5qzqxyh7d05r8i6jm011bsnf8")))

(define-public crate-imgui-0.0.20 (c (n "imgui") (v "0.0.20") (d (list (d (n "imgui-sys") (r "^0.0.20") (d #t) (k 0)))) (h "13wnm5pvcs2k93b3gbrf0nrq8vppql9yqhvhhnn1yahqib2rpkr2")))

(define-public crate-imgui-0.0.21 (c (n "imgui") (v "0.0.21") (d (list (d (n "imgui-sys") (r "^0.0.21") (d #t) (k 0)))) (h "07qn9s80yh84chnpy95pxxynx1kfrd27jnsn02h42whqqv6lpkj2")))

(define-public crate-imgui-0.0.22 (c (n "imgui") (v "0.0.22") (d (list (d (n "imgui-sys") (r "^0.0.22") (d #t) (k 0)))) (h "1c31drhlln2f28hppzcr74z1k9c8sg3h2hzywmpshlk4s2gis4md")))

(define-public crate-imgui-0.0.23 (c (n "imgui") (v "0.0.23") (d (list (d (n "imgui-sys") (r "^0.0.23") (d #t) (k 0)))) (h "073c11dz7fh9p883l99gbbk9z3bfa8n4k39860q8dq7zq6wzrw8c")))

(define-public crate-imgui-0.1.0-rc.1 (c (n "imgui") (v "0.1.0-rc.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.25") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.1.0-rc.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)))) (h "06l0vk75f7nir04akizh4b9d7kfjc37lk8g3vdx02d7pidh3zh19")))

(define-public crate-imgui-0.1.0-rc.2 (c (n "imgui") (v "0.1.0-rc.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.25") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)))) (h "14rkw9w6ba8yk57n24aiicwrs7k5ikmnih7cq7y9qjsraly536jx")))

(define-public crate-imgui-0.1.0 (c (n "imgui") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.25") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)))) (h "1iwxiprpcpavqqzk0fdmxikwqq4ywyicsylbvvr62cc4wai3gqdh")))

(define-public crate-imgui-0.2.0 (c (n "imgui") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.25") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1195q1i21wcxp72gyabz2pqn5r1v5icamibhp114g97j6ch1mi7d")))

(define-public crate-imgui-0.2.1 (c (n "imgui") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.25") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "00kplr7bk9jx14dq93frrqwbzxjns9pcv6yc150fj0y53911n2p7")))

(define-public crate-imgui-0.3.0 (c (n "imgui") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.26") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0fm2vxyb9gwgrlza3xagpsc1zarrwcxw4m6r9nmlsbcqwb72hbca")))

(define-public crate-imgui-0.4.0 (c (n "imgui") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.27") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0w05ap7vzykcj86rvj7aw9pf6sbdhfp0b1z2gh9pi3j9gv62pnwz") (f (quote (("wasm" "imgui-sys/wasm"))))))

(define-public crate-imgui-0.5.0 (c (n "imgui") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.27") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "061mqvkjq05rpc8ja4qzasjzyp9s010bi8yqakfya01cbgaz9fmi") (f (quote (("wasm" "imgui-sys/wasm"))))))

(define-public crate-imgui-0.6.0 (c (n "imgui") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.28") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0ksyjj86y2wcbs0gmdkbm80absrxl0vbaf3sqj96si087wxgf557") (f (quote (("wasm" "imgui-sys/wasm"))))))

(define-public crate-imgui-0.6.1 (c (n "imgui") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r ">=0.28, <0.30") (o #t) (k 0)) (d (n "imgui-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0bq3h21jil9jkjwxpgv1kg5k6h2xmxpbrl5fcw29d0mq7dyhg84j") (f (quote (("wasm" "imgui-sys/wasm"))))))

(define-public crate-imgui-0.7.0 (c (n "imgui") (v "0.7.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1ci2wz04j71b6yp5y0ar21jhcw11rvqwldynqlhn72166dpczkr4") (f (quote (("wasm" "imgui-sys/wasm"))))))

(define-public crate-imgui-0.8.0 (c (n "imgui") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1d0r67h9qm9dxpn087cac5ppvybkh53yxg69bvi624bvvhil1p2f") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("min-const-generics") ("freetype" "imgui-sys/freetype") ("default" "min-const-generics"))))))

(define-public crate-imgui-0.8.1 (c (n "imgui") (v "0.8.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1ak2s2dydfa78v7hk8k05ky76jjbnkdgcp95b4986cyfh3mikw6f") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("min-const-generics") ("freetype" "imgui-sys/freetype") ("default" "min-const-generics"))))))

(define-public crate-imgui-0.8.2 (c (n "imgui") (v "0.8.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0b5q0pxzqfpyqspcy0f0si21cc7g2dggfpx5pyqnspb4shrkk8z6") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("min-const-generics") ("freetype" "imgui-sys/freetype") ("default" "min-const-generics"))))))

(define-public crate-imgui-0.9.0 (c (n "imgui") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1k68hr82sfkxdzgkfz9vh6hnwqi09cfwzwd2n6667vifk37nkv8z") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("freetype" "imgui-sys/freetype") ("docking" "imgui-sys/docking"))))))

(define-public crate-imgui-0.10.0 (c (n "imgui") (v "0.10.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1g11074q3s23igmhqfdgs1mb54lpvn7gdab38scrqz94j351ksic") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("freetype" "imgui-sys/freetype") ("docking" "imgui-sys/docking"))))))

(define-public crate-imgui-0.11.0 (c (n "imgui") (v "0.11.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1davyx8cyabz49dywmf8rp9bn9kbykm7p40jbyhlrmpw1rynfb8j") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("freetype" "imgui-sys/freetype") ("docking" "imgui-sys/docking"))))))

(define-public crate-imgui-0.12.0 (c (n "imgui") (v "0.12.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0sanpr51gx9a11w5f88hipijrq0l728hds164a0ia1fgrsjszpca") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("freetype" "imgui-sys/freetype") ("docking" "imgui-sys/docking"))))))

