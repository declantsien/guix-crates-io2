(define-module (crates-io im gu imgui_file_explorer) #:use-module (crates-io))

(define-public crate-imgui_file_explorer-0.0.3 (c (n "imgui_file_explorer") (v "0.0.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 2)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.0.21") (d #t) (k 2)))) (h "086j8813628hmblr53kdmvlrxfyyxr8vsqd2fzy4vy1bjfz460vw")))

