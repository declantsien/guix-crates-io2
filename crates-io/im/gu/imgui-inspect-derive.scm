(define-module (crates-io im gu imgui-inspect-derive) #:use-module (crates-io))

(define-public crate-imgui-inspect-derive-0.1.0 (c (n "imgui-inspect-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07g3ab0ndvv162kkw3znzf7xhl36firbawb0yzy8nmr8j37gh2iz")))

(define-public crate-imgui-inspect-derive-0.2.0 (c (n "imgui-inspect-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i3313sd59akg0hl4gdclxfkl4b3wn8vrri41ya3a9lhm2cgj3rk") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.3.0 (c (n "imgui-inspect-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "176fy966z2zhhp30vmpwkq9fpb03hywix6sl32mfxvs3z36g4mmp") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.3.1 (c (n "imgui-inspect-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f5v79d6i2rgic3j9qwj41f6p0qfpvvvznrc2v4yrk3m3gapdd3s") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.4.0 (c (n "imgui-inspect-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d7b51wc5x2gxshjfx4b7q5v8xv46fw1w05gz26lf1hr5k8nxc7i") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.5.0 (c (n "imgui-inspect-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ggpgiyi10mjq1d4nyjsz8ribqfn87fs3942xmxdzdhkj8j581px") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.6.0 (c (n "imgui-inspect-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lpmzj6fwz3nzfa8w7nb1j7qz1q9mq1px0vgsm27ivg7f905yrih") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.7.0 (c (n "imgui-inspect-derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0id16whxgr9q0zmq1h66vpy8dd1yfy3hwyvrhx5zxqqk8gw8adq0") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.7.1 (c (n "imgui-inspect-derive") (v "0.7.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13nln4rj3rhk2diwig9xiwzf211fk9fccf2hrcfjd66f9xcxfxmf") (f (quote (("generate_code") ("default" "generate_code"))))))

(define-public crate-imgui-inspect-derive-0.8.0 (c (n "imgui-inspect-derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l639x7m1pamrwb79ciy39wamlagsyfw984qvjg0jnn4ljwhgikl") (f (quote (("generate_code") ("default" "generate_code"))))))

