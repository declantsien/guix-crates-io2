(define-module (crates-io im gu imgui-glium-renderer) #:use-module (crates-io))

(define-public crate-imgui-glium-renderer-0.0.13 (c (n "imgui-glium-renderer") (v "0.0.13") (d (list (d (n "glium") (r "^0.16") (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "imgui") (r "^0.0.13") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.13") (f (quote ("glium"))) (d #t) (k 0)))) (h "1rjg2xi4y63jsjgf80jc5ly0jdx3p2pxg7cvrp9mw6bl2xvgf6qk")))

(define-public crate-imgui-glium-renderer-0.0.14 (c (n "imgui-glium-renderer") (v "0.0.14") (d (list (d (n "glium") (r "^0.16") (k 0)) (d (n "imgui") (r "^0.0.14") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.14") (f (quote ("glium"))) (d #t) (k 0)))) (h "08hh62xxysda8fpyr81gfdwclsq5d7m0a4b8dxfxd9ch2y2dblhs")))

(define-public crate-imgui-glium-renderer-0.0.15 (c (n "imgui-glium-renderer") (v "0.0.15") (d (list (d (n "glium") (r "^0.17") (k 0)) (d (n "imgui") (r "^0.0.15") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.15") (f (quote ("glium"))) (d #t) (k 0)))) (h "14kkhckw7c2d7sjny3mchmihn5j53ppps8hvn55v77hr5i9vh0ky")))

(define-public crate-imgui-glium-renderer-0.0.16 (c (n "imgui-glium-renderer") (v "0.0.16") (d (list (d (n "glium") (r "^0.18") (k 0)) (d (n "imgui") (r "^0.0.16") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.16") (f (quote ("glium"))) (d #t) (k 0)))) (h "0cpjsygsgp9y9q134smisr4gwzj16h6wpisd4mjaliq1jznygdid")))

(define-public crate-imgui-glium-renderer-0.0.17 (c (n "imgui-glium-renderer") (v "0.0.17") (d (list (d (n "glium") (r "^0.18") (k 0)) (d (n "imgui") (r "^0.0.17") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.17") (f (quote ("glium"))) (d #t) (k 0)))) (h "161r20pk8zaff764cb1mz6khrpr4jmxyv7r0agznyi6f5h7whna4")))

(define-public crate-imgui-glium-renderer-0.0.18 (c (n "imgui-glium-renderer") (v "0.0.18") (d (list (d (n "glium") (r "^0.19") (k 0)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.18") (f (quote ("glium"))) (d #t) (k 0)))) (h "064416n2msh887kr1gbwbhy6wkidn2amxs4gr1wn86w3yjarlmw2")))

(define-public crate-imgui-glium-renderer-0.0.19 (c (n "imgui-glium-renderer") (v "0.0.19") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "imgui") (r "^0.0.19") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.19") (f (quote ("glium"))) (d #t) (k 0)))) (h "0pca8rb1p0wa6mb74nqfdcjvcj02h9g411vpf9aiwvihnblzrzs2")))

(define-public crate-imgui-glium-renderer-0.0.20 (c (n "imgui-glium-renderer") (v "0.0.20") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "imgui") (r "^0.0.20") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.20") (f (quote ("glium"))) (d #t) (k 0)))) (h "09yj401w3ya9jgixyph1f4zmmarxm34mvlz3hmwa4vw6l7ai502s")))

(define-public crate-imgui-glium-renderer-0.0.21 (c (n "imgui-glium-renderer") (v "0.0.21") (d (list (d (n "glium") (r "^0.22") (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.21") (f (quote ("glium"))) (d #t) (k 0)))) (h "1slycgk4bahnnjnazfrrwy4nkaq3dbs2zgba3qphsirwlh7lzry6")))

(define-public crate-imgui-glium-renderer-0.0.22 (c (n "imgui-glium-renderer") (v "0.0.22") (d (list (d (n "glium") (r "^0.23") (k 0)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.22") (f (quote ("glium"))) (d #t) (k 0)))) (h "0bs9ipzlj25bsz1rzgky80j7xmlmz3mncam5ylf0y99x0v3anja1")))

(define-public crate-imgui-glium-renderer-0.0.23 (c (n "imgui-glium-renderer") (v "0.0.23") (d (list (d (n "glium") (r "^0.23") (k 0)) (d (n "imgui") (r "^0.0.23") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.23") (f (quote ("glium"))) (d #t) (k 0)))) (h "11l16fscpwcbjj5jalfn2wljmiavg9p6mxlggndwmgayxk1fqa29")))

(define-public crate-imgui-glium-renderer-0.1.0-rc.1 (c (n "imgui-glium-renderer") (v "0.1.0-rc.1") (d (list (d (n "glium") (r "^0.25") (k 0)) (d (n "imgui") (r "^0.1.0-rc.1") (f (quote ("glium"))) (d #t) (k 0)))) (h "12j4kn6hmal0c3rgncwbc5zxy8gwmfdcjxm6xaf16ijmqbkxjmzw")))

(define-public crate-imgui-glium-renderer-0.1.0-rc.2 (c (n "imgui-glium-renderer") (v "0.1.0-rc.2") (d (list (d (n "glium") (r "^0.25") (k 0)) (d (n "imgui") (r "^0.1.0-rc.2") (f (quote ("glium"))) (d #t) (k 0)))) (h "11fncm896261djpanf5j4dzjmpgcslzwmnr9f76qkb35ghzbrqc7")))

(define-public crate-imgui-glium-renderer-0.1.0 (c (n "imgui-glium-renderer") (v "0.1.0") (d (list (d (n "glium") (r "^0.25") (k 0)) (d (n "imgui") (r "^0.1.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "1liilvn1yxnlka0ls54f9rfyqdls94k7674m0va5cnfcb8d750wf")))

(define-public crate-imgui-glium-renderer-0.2.0 (c (n "imgui-glium-renderer") (v "0.2.0") (d (list (d (n "glium") (r "^0.25") (k 0)) (d (n "imgui") (r "^0.2.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "0cvll0dm79d11dxdbr7axv4h65gc7n6sw4y4hvpv4g0s24nfiiah")))

(define-public crate-imgui-glium-renderer-0.3.0 (c (n "imgui-glium-renderer") (v "0.3.0") (d (list (d (n "glium") (r "^0.26") (k 0)) (d (n "imgui") (r "^0.3.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "1sc7q6jb0v70979q4s3l71ckc85jl94kzssgl3gvwzdiil9p61ym")))

(define-public crate-imgui-glium-renderer-0.4.0 (c (n "imgui-glium-renderer") (v "0.4.0") (d (list (d (n "glium") (r "^0.27") (k 0)) (d (n "imgui") (r "^0.4.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "11pmchfl8kabs33vcsphhddbby3ggr6v2rbjjzim9qxk7p5lfgn7")))

(define-public crate-imgui-glium-renderer-0.5.0 (c (n "imgui-glium-renderer") (v "0.5.0") (d (list (d (n "glium") (r "^0.27") (k 0)) (d (n "imgui") (r "^0.5.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "1zp9zn3rvx2343w3nzkdmrv5rj32jy4x49rb6ph791r8vzf9f9ww")))

(define-public crate-imgui-glium-renderer-0.6.0 (c (n "imgui-glium-renderer") (v "0.6.0") (d (list (d (n "glium") (r "^0.28") (k 0)) (d (n "imgui") (r "^0.6.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "1vi7gaypdf7fxahvph5d76cf32hi3kcvk692hdmgahqfy06n4cp8")))

(define-public crate-imgui-glium-renderer-0.6.1 (c (n "imgui-glium-renderer") (v "0.6.1") (d (list (d (n "glium") (r ">=0.28, <0.30") (k 0)) (d (n "imgui") (r "^0.6.1") (f (quote ("glium"))) (d #t) (k 0)))) (h "1jh4rbpcgq76ywavaf3xzz72npcmgkdxwppa21vhrda3fdq5a1sf")))

(define-public crate-imgui-glium-renderer-0.7.0 (c (n "imgui-glium-renderer") (v "0.7.0") (d (list (d (n "glium") (r ">=0.28, <0.30") (k 0)) (d (n "imgui") (r "^0.7.0") (d #t) (k 0)))) (h "1wv22yjlm3d390j9srs22518xkav8841hx044g7hjwhd1i8mqacc")))

(define-public crate-imgui-glium-renderer-0.8.0 (c (n "imgui-glium-renderer") (v "0.8.0") (d (list (d (n "glium") (r ">=0.28, <0.31") (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)))) (h "0b6sj8q1ywlzahy8mkr640y27yb59anks34gqpj0byx1m8cxk46w")))

(define-public crate-imgui-glium-renderer-0.8.1 (c (n "imgui-glium-renderer") (v "0.8.1") (d (list (d (n "glium") (r ">=0.28, <0.31") (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)))) (h "0lhvcl3ijjar907qh6pi7aagy10xfrhbjwzyk4g5gl6lv6p1yb3s")))

(define-public crate-imgui-glium-renderer-0.8.2 (c (n "imgui-glium-renderer") (v "0.8.2") (d (list (d (n "glium") (r ">=0.28, <0.31") (k 0)) (d (n "imgui") (r "^0.8.2") (d #t) (k 0)))) (h "1khjryb48ckpgqfq1wjy3xs09gaiikq82719zp3bv8xxw80wdwjs")))

(define-public crate-imgui-glium-renderer-0.9.0 (c (n "imgui-glium-renderer") (v "0.9.0") (d (list (d (n "glium") (r "^0.32.1") (k 0)) (d (n "imgui") (r "^0.9.0") (d #t) (k 0)))) (h "1z34kpkav70h33jqx3svfihhy5mghxmfdmmq1ij7vwvfsc6m93i6")))

(define-public crate-imgui-glium-renderer-0.10.0 (c (n "imgui-glium-renderer") (v "0.10.0") (d (list (d (n "glium") (r "^0.32.1") (k 0)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)))) (h "1b7mg8pmgxf3nczscmrqkwp0gd5awkzpx837f6pja8ghv4yqh99y")))

(define-public crate-imgui-glium-renderer-0.11.0 (c (n "imgui-glium-renderer") (v "0.11.0") (d (list (d (n "glium") (r "^0.32.1") (k 0)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)))) (h "03gvi7003mjn6fc3v2jf4zkfni0dsdsv683pqy71as720hn4k00y")))

(define-public crate-imgui-glium-renderer-0.12.0 (c (n "imgui-glium-renderer") (v "0.12.0") (d (list (d (n "glium") (r "^0.34.0") (k 0)) (d (n "glium") (r "^0.34.0") (f (quote ("glutin_backend"))) (k 2)) (d (n "glutin") (r "^0.31.1") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.4.2") (d #t) (k 2)) (d (n "imgui") (r "^0.12.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 2)) (d (n "winit") (r "^0.29.3") (f (quote ("rwh_05"))) (d #t) (k 2)))) (h "0y1a3wccrk6p5bss8ia7lzbbf4m6zshy97x1cn56szasl18vvlnr")))

