(define-module (crates-io im gu imgui-sfml-support) #:use-module (crates-io))

(define-public crate-imgui-sfml-support-0.1.0 (c (n "imgui-sfml-support") (v "0.1.0") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 0)) (d (n "sfml") (r "^0.21.0") (d #t) (k 0)))) (h "1wca3p9m6q22lw5gzn5ccza22ccllqx58qd36cg42j6842vwlkpf") (y #t)))

(define-public crate-imgui-sfml-support-0.1.1 (c (n "imgui-sfml-support") (v "0.1.1") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 0)) (d (n "sfml") (r "^0.21.0") (d #t) (k 0)))) (h "0ldg27izbgn190l5qlxswl1njwngf1qr5jisvys37kh0k2mv1k4i") (y #t)))

(define-public crate-imgui-sfml-support-0.1.2 (c (n "imgui-sfml-support") (v "0.1.2") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 0)) (d (n "sfml") (r "^0.21.0") (d #t) (k 0)))) (h "029cmwh69s9kmfblr3snr5bggab6wv9z0sd3fa7sq8187hjdjghy") (y #t)))

(define-public crate-imgui-sfml-support-0.1.3 (c (n "imgui-sfml-support") (v "0.1.3") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 0)) (d (n "sfml") (r "^0.21.0") (d #t) (k 0)))) (h "0andhg3q4wn622mc6d1wnr19abzd720c0ssh2ms3kddnf2p7ipgz")))

