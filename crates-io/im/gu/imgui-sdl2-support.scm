(define-module (crates-io im gu imgui-sdl2-support) #:use-module (crates-io))

(define-public crate-imgui-sdl2-support-0.9.0 (c (n "imgui-sdl2-support") (v "0.9.0") (d (list (d (n "glow") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.9.0") (d #t) (k 0)) (d (n "imgui-glow-renderer") (r "^0.9.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("bundled" "static-link"))) (d #t) (k 2)))) (h "102mkzqvwdhnq06cyf4zlxgv47yksrpr43b75lsdhx1a6kky3kn2")))

(define-public crate-imgui-sdl2-support-0.10.0 (c (n "imgui-sdl2-support") (v "0.10.0") (d (list (d (n "glow") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.10.0") (d #t) (k 0)) (d (n "imgui-glow-renderer") (r "^0.10.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("bundled" "static-link"))) (d #t) (k 2)))) (h "0awnak7qs05wv49v6h7dadx6xgfa6rfj1sd1yr318s1h219xmy1c")))

(define-public crate-imgui-sdl2-support-0.11.0 (c (n "imgui-sdl2-support") (v "0.11.0") (d (list (d (n "glow") (r "^0.12.0") (d #t) (k 2)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imgui-glow-renderer") (r "^0.11.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("bundled" "static-link"))) (d #t) (k 2)))) (h "0dfidkshk69vr7avkcpdghj9j6vqlnachjmf3lxxq9cvc66kcf6h")))

(define-public crate-imgui-sdl2-support-0.12.0 (c (n "imgui-sdl2-support") (v "0.12.0") (d (list (d (n "glow") (r "^0.13.1") (d #t) (k 2)) (d (n "imgui") (r "^0.12.0") (d #t) (k 0)) (d (n "imgui-glow-renderer") (r "^0.12.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("bundled" "static-link"))) (d #t) (k 2)))) (h "11rzd2sa1pdc3pw83cwzlphhxr3v94diafif36y02589s463mbxb")))

