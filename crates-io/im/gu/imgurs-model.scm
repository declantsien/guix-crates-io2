(define-module (crates-io im gu imgurs-model) #:use-module (crates-io))

(define-public crate-imgurs-model-0.2.0 (c (n "imgurs-model") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde" "macros" "serde-human-readable" "serde-well-known"))) (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1g73gz67igakigxhrmq3l3ms2mv96nwg4qifb1d4fnrz04vdjaiw") (f (quote (("from_env") ("default"))))))

(define-public crate-imgurs-model-0.1.0 (c (n "imgurs-model") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde" "macros" "serde-human-readable" "serde-well-known"))) (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1681vkjhbdilhsb3davg6b6j1z5qv20k2lv6kn66mq0a8psxyygr") (f (quote (("from_env") ("default"))))))

