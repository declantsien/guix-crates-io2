(define-module (crates-io im gu imgui-memory-editor-sys) #:use-module (crates-io))

(define-public crate-imgui-memory-editor-sys-0.1.0 (c (n "imgui-memory-editor-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "1i15rg7jcgg1cnpa6yrqsnsbw4hr3phw7zxp4vbqn7wqdlqkxgnb")))

(define-public crate-imgui-memory-editor-sys-0.1.1 (c (n "imgui-memory-editor-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "1zc2ax6qyp659hrsanayrwpm6ih239qmsn1ji2l3glj338n38lz6")))

(define-public crate-imgui-memory-editor-sys-0.2.0 (c (n "imgui-memory-editor-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0krf6x5gyg5bfh8k8qycbw6brbb8i63jwa11m28mimjkdlbj6byj") (l "imgui_memory_editor")))

(define-public crate-imgui-memory-editor-sys-0.3.0 (c (n "imgui-memory-editor-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1akfcbbwsfdkyvphxk65fd6kr7bl1syv5lm81x6nn52sk7z8xy3m") (l "imgui_memory_editor")))

