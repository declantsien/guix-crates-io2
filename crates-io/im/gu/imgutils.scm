(define-module (crates-io im gu imgutils) #:use-module (crates-io))

(define-public crate-imgutils-0.1.0 (c (n "imgutils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1sa62y41gwcq9kkx0b2bybiwvgnpqccc1ma96h0rig3mnz3w3l1j") (y #t) (r "1.74")))

(define-public crate-imgutils-0.1.1 (c (n "imgutils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "0dy4w16dnc3sy4sp75vqcv59c4nvd00k85gpbr0pb83jifdhvc9c") (r "1.74")))

(define-public crate-imgutils-0.1.2 (c (n "imgutils") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "09abflb93593x9qy7g08k8klfrqgksdywrbgmm2sq7v32hnzy1d1") (r "1.74")))

(define-public crate-imgutils-0.1.3 (c (n "imgutils") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "0wvvk9sj2cgkx1wz0rpsj8yg4qxhn8ys75wvchpfw509vw5g4zzw") (r "1.74")))

