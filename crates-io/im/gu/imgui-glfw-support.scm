(define-module (crates-io im gu imgui-glfw-support) #:use-module (crates-io))

(define-public crate-imgui-glfw-support-0.1.0 (c (n "imgui-glfw-support") (v "0.1.0") (d (list (d (n "glfw") (r "^0.36") (d #t) (k 0)) (d (n "imgui") (r "^0.3") (d #t) (k 0)) (d (n "imgui") (r "^0.3") (d #t) (k 2)) (d (n "imgui-wgpu") (r "^0.5") (d #t) (k 2)) (d (n "wgpu") (r "^0.4") (d #t) (k 2)))) (h "00j3rb646kvbvvs4cz410cx5hmna26kipj61d35y62mqljgwwwb8") (f (quote (("vulkan" "glfw/vulkan") ("image" "glfw/image"))))))

(define-public crate-imgui-glfw-support-0.2.0 (c (n "imgui-glfw-support") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "glfw") (r "^0.37") (d #t) (k 0)) (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui") (r "^0.4") (d #t) (k 2)) (d (n "imgui-wgpu") (r "^0.7") (d #t) (k 2)) (d (n "wgpu") (r "^0.5") (d #t) (k 2)))) (h "0rwdwkvz10a70wa4id2fhd51mxvk33ldawvxyqyl5s7mf263p3bj") (f (quote (("vulkan" "glfw/vulkan") ("image" "glfw/image"))))))

(define-public crate-imgui-glfw-support-0.3.0 (c (n "imgui-glfw-support") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "glfw") (r "^0.39.1") (d #t) (k 0)) (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui") (r "^0.4") (d #t) (k 2)) (d (n "imgui-wgpu") (r "^0.7") (d #t) (k 2)) (d (n "wgpu") (r "^0.5") (d #t) (k 2)))) (h "0lscxhs12zp0d8b24kiqalj73xj7ln515pjblbx4z1jyaa0fifix") (f (quote (("vulkan" "glfw/vulkan") ("image" "glfw/image"))))))

(define-public crate-imgui-glfw-support-0.4.0 (c (n "imgui-glfw-support") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "glfw") (r "^0.40") (d #t) (k 0)) (d (n "imgui") (r "^0.5") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.10") (d #t) (k 2)) (d (n "wgpu") (r "^0.6") (d #t) (k 2)))) (h "0mzmix89lyc2ffkskbg84wjq0yxf3cdhbw9rvcnyg3438nll224d") (f (quote (("vulkan" "glfw/vulkan") ("image" "glfw/image"))))))

(define-public crate-imgui-glfw-support-0.4.1 (c (n "imgui-glfw-support") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "glfw") (r "^0.40") (d #t) (k 0)) (d (n "imgui") (r "^0.5") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.10") (d #t) (k 2)) (d (n "wgpu") (r "^0.6") (d #t) (k 2)))) (h "1cnba9k4y7jawdml6rjvn99c6cc6jhavkriyvg8jr9199b8x3j0r") (f (quote (("vulkan" "glfw/vulkan") ("image" "glfw/image"))))))

