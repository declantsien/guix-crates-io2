(define-module (crates-io im gu imgui-miniquad-render) #:use-module (crates-io))

(define-public crate-imgui-miniquad-render-0.1.0 (c (n "imgui-miniquad-render") (v "0.1.0") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "^0.2.3") (d #t) (k 0)))) (h "05q1sl70vf4p1dqc4yna6xcila391p5y8dww8qvg4vqxvyiz8nv0")))

(define-public crate-imgui-miniquad-render-0.1.1 (c (n "imgui-miniquad-render") (v "0.1.1") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "^0.2.3") (d #t) (k 0)))) (h "1h5szizsgw2ca8wmvhzdm7cj2nbsfz1mvcfs95w1w6cihgd97smj")))

(define-public crate-imgui-miniquad-render-0.1.2 (c (n "imgui-miniquad-render") (v "0.1.2") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "^0.2.4") (d #t) (k 0)))) (h "0cbgik2a5xccjv11yzpbh5czq2rznj7d0cg85i722nxxlbjdsinm")))

(define-public crate-imgui-miniquad-render-0.1.3 (c (n "imgui-miniquad-render") (v "0.1.3") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "^0.2.30") (d #t) (k 0)))) (h "0nsmnx2mf2g8q9789y8rbn3dsvx8jx888p3ns2yygzr1r3hqwcin")))

(define-public crate-imgui-miniquad-render-0.1.4 (c (n "imgui-miniquad-render") (v "0.1.4") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "= 0.2.33") (d #t) (k 0)))) (h "0b3bdk30wr47icqmzi46dksr136wnpwaji94zbxh74g9jkylarcy")))

(define-public crate-imgui-miniquad-render-0.1.5 (c (n "imgui-miniquad-render") (v "0.1.5") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "= 0.2.35") (d #t) (k 0)))) (h "1w7859l41lyj9kcd07v4yc0r0m4ivj9m1zx5g9z1sayknc6b2wk1")))

(define-public crate-imgui-miniquad-render-0.1.6 (c (n "imgui-miniquad-render") (v "0.1.6") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "= 0.2.35") (d #t) (k 0)))) (h "0w9c1qjrl5mkqahz2b52jlpayyy4pggqjxqxpj72xg3i4pksyirb")))

(define-public crate-imgui-miniquad-render-0.1.7 (c (n "imgui-miniquad-render") (v "0.1.7") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.2.1") (d #t) (k 0)) (d (n "miniquad") (r "=0.2.35") (d #t) (k 0)))) (h "1j8sy1dfd5s3yhp8jfg5sfxwjx74hrvr6l3v7in06463rkhp1mab")))

(define-public crate-imgui-miniquad-render-0.1.8 (c (n "imgui-miniquad-render") (v "0.1.8") (d (list (d (n "glam") (r "^0.8") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "imgui") (r "^0.5.0") (d #t) (k 0)) (d (n "miniquad") (r "^0.3.0-alpha.20") (d #t) (k 0)))) (h "08cifvh4rla6bqxajsrhc6592pl8ak2pldrpnimh88ibr7xv1jhp")))

