(define-module (crates-io im gu imgui-cxx) #:use-module (crates-io))

(define-public crate-imgui-cxx-0.1.0 (c (n "imgui-cxx") (v "0.1.0") (d (list (d (n "cargo-cxx") (r "^0.1") (d #t) (k 1)))) (h "10plmsrkww3idnx6m7ljw9wx0z38fhcbjnzr07kfd3rmj0jvmc3f")))

(define-public crate-imgui-cxx-0.1.1 (c (n "imgui-cxx") (v "0.1.1") (d (list (d (n "cargo-cxx") (r "^0.1") (d #t) (k 1)))) (h "08s253l22d9l4fklp9xcmf2kb8bnw4l9kh3yg65anx9f3yzhna4q")))

