(define-module (crates-io im gu imgui-terminal) #:use-module (crates-io))

(define-public crate-imgui-terminal-0.1.0 (c (n "imgui-terminal") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "imgui") (r "^0.7.0") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0p8lzj53kz9mmb8yz3abiqg1byp5m5cry8z5pllm56dvrz3mzva5")))

