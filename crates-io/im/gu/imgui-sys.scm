(define-module (crates-io im gu imgui-sys) #:use-module (crates-io))

(define-public crate-imgui-sys-0.0.2 (c (n "imgui-sys") (v "0.0.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.8") (o #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "16z8shx3mbrfhr3vmqz69nvf7kaqhpmzf4pcj7lzpg55jh00c7kz") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.3 (c (n "imgui-sys") (v "0.0.3") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.9") (o #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0cwcyr1xrmjjl3p65spcaam6nh0ddhpi7bjkz2n0zgjg04ld4l2m") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.4 (c (n "imgui-sys") (v "0.0.4") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.10") (o #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0cb91r7whrcwp772wl5l4nz2pm59fs1faxk8rb0gybp4mckhkjgc") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.5 (c (n "imgui-sys") (v "0.0.5") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.12") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xzm4cnxkgaqqxkfkr3wb70w8f58pgd9ixdd8q3km06i5bi8qm42") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.6 (c (n "imgui-sys") (v "0.0.6") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.13") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "169sxmy2wh4dccp9xsbpixb90f2zkky1h2vzrj8yjjqszxp2pfsh") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.7 (c (n "imgui-sys") (v "0.0.7") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.13") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14zdcl7d7d0vmwzbdlb3nghcfrddwvzm7mixxif6ma0w5jdx4s7i") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.8 (c (n "imgui-sys") (v "0.0.8") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.14") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "172kavjp82420pv3cpy30dyddrshaklw22v5b0z4a25295ffiwpr") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.9 (c (n "imgui-sys") (v "0.0.9") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.14") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z5b3vrwmwrajcf7aqvs9sgkkpk0n0drz2yrvhwz3lk1bqrfbipx") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.10 (c (n "imgui-sys") (v "0.0.10") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.15") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bwa4i22f2niyryykz1kq2g9bb0scbdprwkbzld6c9vp9z1wgvyq") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.11 (c (n "imgui-sys") (v "0.0.11") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.16") (o #t) (k 0)))) (h "0kgv38d5mg5hihzqawlfzxq9ikn9x9yv1g0javxvbba0mqwc1p74") (f (quote (("default" "glium"))))))

(define-public crate-imgui-sys-0.0.12 (c (n "imgui-sys") (v "0.0.12") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.16") (o #t) (k 0)))) (h "0bf1x0n43k9pwn3df5j3y4gw0rjr36ivwvfmcg5yfy9684mw4l0p") (y #t)))

(define-public crate-imgui-sys-0.0.13 (c (n "imgui-sys") (v "0.0.13") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glium") (r "^0.16") (o #t) (k 0)))) (h "06m1vdgnxbvbkpan3j28f4pn07jgqhx82z6d72w9mcj8ic5nxpzg")))

(define-public crate-imgui-sys-0.0.14 (c (n "imgui-sys") (v "0.0.14") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "gfx") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.16") (o #t) (k 0)))) (h "07i1yl6hb19nr7z4rl48d8ir0np8b2lhi1q726x242k4w2z88kpl")))

(define-public crate-imgui-sys-0.0.15 (c (n "imgui-sys") (v "0.0.15") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "gfx") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.17") (o #t) (k 0)))) (h "1gjr4gpdg0db8hcnjfarmksh7g7j9fa5c4bb59ppjipri3j006zw")))

(define-public crate-imgui-sys-0.0.16 (c (n "imgui-sys") (v "0.0.16") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.18") (o #t) (k 0)))) (h "0djmrf569fpknhgzkkhxqfyy892fv7n1qqj5gkxa2b2kw34bfr60")))

(define-public crate-imgui-sys-0.0.17 (c (n "imgui-sys") (v "0.0.17") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.18") (o #t) (k 0)))) (h "1mnls7cg93aag7ay729ybkiq7h1s0czkhfpnv47jvkf8mwczcp7c")))

(define-public crate-imgui-sys-0.0.18 (c (n "imgui-sys") (v "0.0.18") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.19") (o #t) (k 0)))) (h "1dx4w8kvk5hyf44y6j25pdg9ps4y140wrq3na67qinjcd41sbbj7")))

(define-public crate-imgui-sys-0.0.19 (c (n "imgui-sys") (v "0.0.19") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.22") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ick2y88z40054vwwmi6wk382kyiyalyxrd58410ia3x6wwzv60v")))

(define-public crate-imgui-sys-0.0.20 (c (n "imgui-sys") (v "0.0.20") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.22") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03waivj7qhfm894dm8c3b28d7mjb31kxgky3xl9270xxls2np3fb")))

(define-public crate-imgui-sys-0.0.21 (c (n "imgui-sys") (v "0.0.21") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.22") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14sg9qywmk191cc9az7h2g5hmr81a40pj9iav5zd9dipfr0m6z1w")))

(define-public crate-imgui-sys-0.0.22 (c (n "imgui-sys") (v "0.0.22") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.23") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0daq85fp19arr8mjvb64q55q4pbhx0fgfcl3437a9a248ywdmhyc")))

(define-public crate-imgui-sys-0.0.23 (c (n "imgui-sys") (v "0.0.23") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gfx") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.23") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wc7pq1p1lnh4594kz4mcfv1c8sk4a8ay6pv7kd2jrqdj6kzrps3")))

(define-public crate-imgui-sys-0.1.0-rc.1 (c (n "imgui-sys") (v "0.1.0-rc.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wvc5cd75dyndkv3321zksf49fmwqhbg2r7nqylx7kqz4mw91khb")))

(define-public crate-imgui-sys-0.1.0-rc.2 (c (n "imgui-sys") (v "0.1.0-rc.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p7v3vqjnca17xwnykyvyx2xzfvfm0a0k8by3hwy92jxbp76ac6s")))

(define-public crate-imgui-sys-0.1.0 (c (n "imgui-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zyma3wxsl34hnlbas69khka66lj4p8is1pmk9vmhmpqvqks9vz5")))

(define-public crate-imgui-sys-0.2.0 (c (n "imgui-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1p02lsx76r8afa0vvli4h01x6jyjsssxv59rxm9bivk418pv4l03")))

(define-public crate-imgui-sys-0.3.0 (c (n "imgui-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0mwj498h7rzkva6ns3imbx4p342iqi6yvw7223nd5v361wz6ka11")))

(define-public crate-imgui-sys-0.4.0 (c (n "imgui-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1nj9d3zfwbwiqpvhrrqar4m2ccd4c00d333hpckfvl2dsrqrdgkj") (f (quote (("wasm") ("default"))))))

(define-public crate-imgui-sys-0.5.0 (c (n "imgui-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "157q8vf4gq6h9nqjrcag9yp5g721qrdxlxrrp5m2f3mapm2ncs06") (f (quote (("wasm") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.6.0 (c (n "imgui-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hbgi14fwzi063b9jlf7bi1vkn9r9z066b5kzcm34r4ava9vc8h5") (f (quote (("wasm") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.7.0 (c (n "imgui-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)))) (h "0c6kv0299hliqhyjyg7vs08wrpq8kpqqqiligssh5gvqdfz01jl5") (f (quote (("wasm") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.8.0 (c (n "imgui-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0ns5si4ra5sizmsxvhxx0bqjrp0ysxzgrpkhp4066261mvmxa509") (f (quote (("wasm") ("freetype" "pkg-config") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.8.1 (c (n "imgui-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "138fc7kbkvgkpg3rl44f0ylsiwz2yg88nd0p4cn50fm1h638qihn") (f (quote (("wasm") ("freetype" "pkg-config") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.8.2 (c (n "imgui-sys") (v "0.8.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1xzxfj0098khwypabv35wpr7ic93s4bw77727lhf70c2plw56vr8") (f (quote (("wasm") ("freetype" "pkg-config") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.9.0 (c (n "imgui-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0wcvb010din286hndsbgk0p1fv92f3w33q9c4qz5g2k3y7w4pz1f") (f (quote (("wasm") ("freetype" "pkg-config") ("docking") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.10.0 (c (n "imgui-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (o #t) (d #t) (k 1)))) (h "1nqa4msiqip1vs8ciw4avlq6xzmk328571zrcnqr7hmq8fzylb31") (f (quote (("wasm") ("use-vcpkg" "vcpkg") ("freetype" "pkg-config") ("docking") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.11.0 (c (n "imgui-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (o #t) (d #t) (k 1)))) (h "1gq3kjsvdxxbglcvpaf6zd04zv7291pkfvqy7ajmi06bawm2g1fp") (f (quote (("wasm") ("use-vcpkg" "vcpkg") ("freetype" "pkg-config") ("docking") ("default")))) (l "imgui")))

(define-public crate-imgui-sys-0.12.0 (c (n "imgui-sys") (v "0.12.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (o #t) (d #t) (k 1)))) (h "016qrhrmdia4bwzamggfgbc405ijwa1i9azlp3l9h0xnykwr7lga") (f (quote (("wasm") ("use-vcpkg" "vcpkg") ("freetype" "pkg-config") ("docking") ("default")))) (l "imgui")))

