(define-module (crates-io im gu imgui-glfw-rs) #:use-module (crates-io))

(define-public crate-imgui-glfw-rs-0.2.1 (c (n "imgui-glfw-rs") (v "0.2.1") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.25.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "0bp2cd9gzd6xh2mp1r4jkx17bqn4x4nqif5blmx38mfybcgbwjlc") (y #t)))

(define-public crate-imgui-glfw-rs-0.2.2 (c (n "imgui-glfw-rs") (v "0.2.2") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.25.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "1yxb4fr9ajn5fl16fnc6kcg7jgpskl6gw67b01s5bxvn41j65v51")))

(define-public crate-imgui-glfw-rs-0.2.3 (c (n "imgui-glfw-rs") (v "0.2.3") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.25.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "0w1nycm6769655vz5cnr72f51pxf2db96afs9il298sailxbcx1y")))

(define-public crate-imgui-glfw-rs-0.2.4 (c (n "imgui-glfw-rs") (v "0.2.4") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.26.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "01f2kaipphmpx5x5d1g1zv30pbfp3n67a7cjrwnd6m6ddr3zqgx3")))

(define-public crate-imgui-glfw-rs-0.2.5 (c (n "imgui-glfw-rs") (v "0.2.5") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.26.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "01pkdzmzzhanijj72va7k24xngcjc7zx8ghmkvgp1mqgxvgv9msw")))

(define-public crate-imgui-glfw-rs-0.2.6 (c (n "imgui-glfw-rs") (v "0.2.6") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.26.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3.1") (d #t) (k 2)))) (h "0gjpddhwi53gd2vk9r44kah73p637dybch5bsykxrrrq86cjyxiy")))

(define-public crate-imgui-glfw-rs-0.2.7 (c (n "imgui-glfw-rs") (v "0.2.7") (d (list (d (n "gl") (r "^0.11.0") (d #t) (k 2)) (d (n "glfw") (r "^0.26.1") (d #t) (k 0)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4.0") (d #t) (k 2)))) (h "17x6zc073ihry0qrsv5qhw5s8y1jdw2dh49fi0hdymdsbdigf0gj")))

(define-public crate-imgui-glfw-rs-0.3.0 (c (n "imgui-glfw-rs") (v "0.3.0") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 2)) (d (n "glfw") (r "^0.28.0") (d #t) (k 0)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4.0") (d #t) (k 2)))) (h "1fzwx9fikfyvb79dfggg5jxcypsnxxiv34hjnjsdad8810648hwb")))

(define-public crate-imgui-glfw-rs-0.4.0 (c (n "imgui-glfw-rs") (v "0.4.0") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 2)) (d (n "glfw") (r "^0.31.0") (d #t) (k 0)) (d (n "imgui") (r "^0.1.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.5.0") (d #t) (k 0)))) (h "1z7pv23r3czm3ikq0smpkd2pq34383ilc563hh6l971f24al5nf1")))

(define-public crate-imgui-glfw-rs-0.4.1 (c (n "imgui-glfw-rs") (v "0.4.1") (d (list (d (n "gl") (r "^0.13.0") (d #t) (k 2)) (d (n "glfw") (r "^0.31.0") (d #t) (k 0)) (d (n "imgui") (r "^0.1.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.5.0") (d #t) (k 0)))) (h "1vff0wpf5qp127xddgwj64477d4h7s52ma7izqmiraqfmajydxr0")))

