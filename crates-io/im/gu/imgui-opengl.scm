(define-module (crates-io im gu imgui-opengl) #:use-module (crates-io))

(define-public crate-imgui-opengl-0.0.0 (c (n "imgui-opengl") (v "0.0.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r "^0.9.0") (d #t) (k 0)))) (h "1mqfv9hprac36qf1s6dqd376x3cp86gmmsvyvfsn13g44kqn1x17")))

(define-public crate-imgui-opengl-0.1.0 (c (n "imgui-opengl") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14.0") (d #t) (k 1)) (d (n "imgui") (r "^0.11.0") (d #t) (k 0)))) (h "1y2jiksyls6m1skplb8vyv82lz3015p8f38a85dqys32n3n8swpi")))

