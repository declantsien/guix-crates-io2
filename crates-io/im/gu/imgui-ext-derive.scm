(define-module (crates-io im gu imgui-ext-derive) #:use-module (crates-io))

(define-public crate-imgui-ext-derive-0.1.0-rc0 (c (n "imgui-ext-derive") (v "0.1.0-rc0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0agxnhfzi5xsbwpjxbs9pwpdiiyqhm8hs4zvr917vf39jvjgjy70")))

(define-public crate-imgui-ext-derive-0.1.0-rc1 (c (n "imgui-ext-derive") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yvza5ima5ffrm0jx8l7ab3hkgp64nv9p86x2mip3fczm74836d6")))

(define-public crate-imgui-ext-derive-0.1.0-rc2 (c (n "imgui-ext-derive") (v "0.1.0-rc2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jm8s6slr9ci9hibs4b4pmdki3j2bfy0y5prxgxpqrdcf0k8sb37")))

(define-public crate-imgui-ext-derive-0.1.0-rc3 (c (n "imgui-ext-derive") (v "0.1.0-rc3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "13c6aw7xalfp70ivwgfd51zgziv7nzvc86zb2y7g7fb2g2q129rl")))

(define-public crate-imgui-ext-derive-0.1.0 (c (n "imgui-ext-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hw1mm3af8g8xixv8iaac96pdchpmhnl79c9am10dwzr1154hgfi")))

(define-public crate-imgui-ext-derive-0.1.1 (c (n "imgui-ext-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iy01khs7zhbhy46wm531isgnsl6q4nj5lk79ns6r3yaf6qakgia")))

(define-public crate-imgui-ext-derive-0.1.2 (c (n "imgui-ext-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vwvlpf1vy1vidpvfcp70lhj5im8w0w1md15xqk2k4i3ma6m6p0h")))

(define-public crate-imgui-ext-derive-0.1.3 (c (n "imgui-ext-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qr1b6qkg4vvay9lz3h0hk2101g1dzhssbdcgkg909ldgm2a9ljg")))

(define-public crate-imgui-ext-derive-0.1.4 (c (n "imgui-ext-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bc44l4l4sxwbd8zs92y6k9gzr1ffppng71avp8q4wi819hpk404")))

(define-public crate-imgui-ext-derive-0.1.5 (c (n "imgui-ext-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sby7bzissbgzrmpdp19nd6j0v76lddkn5184q5d6i5kgqzxag75")))

(define-public crate-imgui-ext-derive-0.1.6 (c (n "imgui-ext-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "04m4rrlpirg72jfjgnnb81k82pcdf417z6id76sk6a31mggc6db6")))

(define-public crate-imgui-ext-derive-0.1.7 (c (n "imgui-ext-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nqp1idzgz3c33flcdbwnlxv8rwybz8ap4m25sph8qlw9l9vs302")))

(define-public crate-imgui-ext-derive-0.1.8 (c (n "imgui-ext-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "114nglzb1vh61rmrjigg1m5g6whm3i587ig4gfr9b869awhmkg6h")))

(define-public crate-imgui-ext-derive-0.2.0 (c (n "imgui-ext-derive") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cm3nzim06c5qqav7bjlzqnbkdgd7b36pi9siwbd0kfmplsi8imh")))

(define-public crate-imgui-ext-derive-0.2.1 (c (n "imgui-ext-derive") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "09qfglhnn8nyza5ixv4yp7wj0b7lapfqbda81ksgzhi85jfckjcg")))

(define-public crate-imgui-ext-derive-0.3.0 (c (n "imgui-ext-derive") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x97n8bvfxnrhgcj5r86b1g09p12v6c78rliydwrxhpkvaa491zz")))

