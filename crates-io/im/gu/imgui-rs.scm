(define-module (crates-io im gu imgui-rs) #:use-module (crates-io))

(define-public crate-imgui-rs-1.47.0 (c (n "imgui-rs") (v "1.47.0") (d (list (d (n "gcc") (r "^0.3.20") (d #t) (k 1)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.12") (d #t) (k 2)))) (h "1n4pv56wifidas85sbn0krghvjrh3mz89l6rpbbas2hl1lfaxpwc")))

