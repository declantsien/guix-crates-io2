(define-module (crates-io im gu imgui-gfx-hal) #:use-module (crates-io))

(define-public crate-imgui-gfx-hal-0.1.0 (c (n "imgui-gfx-hal") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-backend-vulkan") (r "^0.1.0") (d #t) (k 2)) (d (n "gfx-hal") (r "~0.1.0") (d #t) (k 0)) (d (n "imgui") (r "~0.0.20") (d #t) (k 0)) (d (n "memoffset") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 2)))) (h "0ssm22f77wb316hwyfdkqjpwvn68m6rp5wf8q3y92sdgv4svbbr1")))

(define-public crate-imgui-gfx-hal-0.1.1 (c (n "imgui-gfx-hal") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-backend-vulkan") (r "^0.1.0") (d #t) (k 2)) (d (n "gfx-hal") (r "~0.1.0") (d #t) (k 0)) (d (n "imgui") (r "~0.0.20") (d #t) (k 0)) (d (n "memoffset") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 2)))) (h "0ip2sa7hmqrnbjr813jn8v1yjm5jsjx21w7hlzyn2iw3brzi3igb")))

(define-public crate-imgui-gfx-hal-0.1.2 (c (n "imgui-gfx-hal") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx-backend-vulkan") (r "^0.1.0") (d #t) (k 2)) (d (n "gfx-hal") (r "~0.1.0") (d #t) (k 0)) (d (n "imgui") (r "~0.0.20") (d #t) (k 0)) (d (n "memoffset") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 2)))) (h "1lj2hph12yv2rblmxfi1l4l9mq79s7hbdi6w9jib4x6l6wsgxi0k")))

