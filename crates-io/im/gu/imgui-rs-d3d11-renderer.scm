(define-module (crates-io im gu imgui-rs-d3d11-renderer) #:use-module (crates-io))

(define-public crate-imgui-rs-d3d11-renderer-0.1.0 (c (n "imgui-rs-d3d11-renderer") (v "0.1.0") (h "189zr8qg26wihyz2nyirwm94wfyxqc7rrj3a3nayzkc9rq1j5b0f") (y #t)))

(define-public crate-imgui-rs-d3d11-renderer-0.0.1 (c (n "imgui-rs-d3d11-renderer") (v "0.0.1") (h "1sb8p03wsh4w5r27kvj5mz51lhqqswwl6ap8y8vvc49b4h9ia0b1")))

(define-public crate-imgui-rs-d3d11-renderer-0.0.3 (c (n "imgui-rs-d3d11-renderer") (v "0.0.3") (h "1labbvcih1zd6b8v8s9akyn61rqi36wwlbwrf0pk34xs77xy9svv")))

