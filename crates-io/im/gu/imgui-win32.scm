(define-module (crates-io im gu imgui-win32) #:use-module (crates-io))

(define-public crate-imgui-win32-0.1.0 (c (n "imgui-win32") (v "0.1.0") (d (list (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "winuser" "wincon" "windef"))) (d #t) (k 0)))) (h "1qbd68vxj5pmj0s9jwpkyffaq54piws5zzkgjayarjvlipb9cqis")))

(define-public crate-imgui-win32-0.2.0 (c (n "imgui-win32") (v "0.2.0") (d (list (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "windows") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0axsfkvabr5imwdh1w61ld21nnvk11h66pzmgh4307j8hcgvk74j")))

(define-public crate-imgui-win32-0.2.1 (c (n "imgui-win32") (v "0.2.1") (d (list (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "01qa6mknvpg5v2gzci34pdxjjvs22qy5arbsl9089mzhi8h83ycx")))

(define-public crate-imgui-win32-0.2.2 (c (n "imgui-win32") (v "0.2.2") (d (list (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1vzwihphx62sjys658g58lhcm9c0h2f2wx5w5llblqlyxl9l95aj")))

