(define-module (crates-io im gu imgui-sdl2) #:use-module (crates-io))

(define-public crate-imgui-sdl2-0.1.0 (c (n "imgui-sdl2") (v "0.1.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.1.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "0bfl7m45hgbz9di4dd0p9sg29x1h2di3jjvp8x378xkxfdihn906")))

(define-public crate-imgui-sdl2-0.2.0 (c (n "imgui-sdl2") (v "0.2.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.1.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "13l2gwxl237v922089bmm9payp7w04iggds9x58dcv1fnz36wany")))

(define-public crate-imgui-sdl2-0.2.1 (c (n "imgui-sdl2") (v "0.2.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.1.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "0700hdzg3yj10hs5142gvagmspdmpq17xa51bbqkm416lm78n9c0")))

(define-public crate-imgui-sdl2-0.2.2 (c (n "imgui-sdl2") (v "0.2.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.1.0") (d #t) (k 2)) (d (n "sdl2") (r "^0") (d #t) (k 0)))) (h "1h4i9nqnjl93ww5ryd0y696ppngs2sf6wr2yapx8isy60i4wnr6r")))

(define-public crate-imgui-sdl2-0.2.3 (c (n "imgui-sdl2") (v "0.2.3") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.20") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.2") (d #t) (k 2)) (d (n "sdl2") (r "^0") (d #t) (k 0)))) (h "1bv15h3hj9r831c58q8j9a17cfbpah80lqmw59ncym0n8kyxnxm6")))

(define-public crate-imgui-sdl2-0.3.0 (c (n "imgui-sdl2") (v "0.3.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3") (d #t) (k 2)) (d (n "sdl2") (r "^0") (d #t) (k 0)))) (h "1q000sx6nbi9fnklnh0kl0iy5dy6ifppw5xb6vvf24z55kqwc8ah")))

(define-public crate-imgui-sdl2-0.3.1 (c (n "imgui-sdl2") (v "0.3.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "1rlxkjaz514zs9hzv6pmsfkri0qhjllbmrkimwi7jv1jx9sd09pr")))

(define-public crate-imgui-sdl2-0.4.0 (c (n "imgui-sdl2") (v "0.4.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.3") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "17awxx5c953d7xszcipjfbh6x0v9y06g359ca3k43aaykr8mz56i")))

(define-public crate-imgui-sdl2-0.5.0 (c (n "imgui-sdl2") (v "0.5.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "1sb63ana2sd9yk4nd4p5iyp2qc7w3fj0506yki9h7vvc3avjpp1c")))

(define-public crate-imgui-sdl2-0.6.0 (c (n "imgui-sdl2") (v "0.6.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.1") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.5") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "1gpah82pg1d6mzc6l8x7w208kmp1nlsaasd7xj7r41a4jkgbb1z2")))

(define-public crate-imgui-sdl2-0.7.0 (c (n "imgui-sdl2") (v "0.7.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.2") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.6") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "09gdmif8snq3d2i71916m6xg0khir2431n240w3xxqgylgl93kxf")))

(define-public crate-imgui-sdl2-0.8.0 (c (n "imgui-sdl2") (v "0.8.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.2") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.6") (d #t) (k 2)) (d (n "sdl2") (r "^0.33") (d #t) (k 0)))) (h "0aa6m41rgakc9qyv69j9gqs9cpa9wbvp8ivkxhiygzh4lg81kwc0")))

(define-public crate-imgui-sdl2-0.9.0 (c (n "imgui-sdl2") (v "0.9.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.3") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.7") (d #t) (k 2)) (d (n "sdl2") (r "^0.33") (d #t) (k 0)))) (h "0w5z4wi0hg9rzx4yiirml37h43jmlyna36x3ds7mk312rl71n0lf")))

(define-public crate-imgui-sdl2-0.10.0 (c (n "imgui-sdl2") (v "0.10.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.8") (d #t) (k 2)) (d (n "sdl2") (r "^0.33") (d #t) (k 0)))) (h "0q2rj8gfrg36ar7dijr661cq89m0gi2hr828zkfk1m51q025yfgc")))

(define-public crate-imgui-sdl2-0.11.0 (c (n "imgui-sdl2") (v "0.11.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.8") (d #t) (k 2)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "1kfg6fjjbzf1fwkrhawxyp9cgxg8aqwpzrvy94nm4s4sa32bc5d2")))

(define-public crate-imgui-sdl2-0.12.0 (c (n "imgui-sdl2") (v "0.12.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.5") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.9") (d #t) (k 2)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "0fivbi5bmyh4lf6cn62yc8ldaywx6ja8bqyrkrq9kkf66lqgw807")))

(define-public crate-imgui-sdl2-0.13.0 (c (n "imgui-sdl2") (v "0.13.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.6") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.10") (d #t) (k 2)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "02y2jzqajnyw9whygdhy2v1d4ipgp6fgwyh51cchv9mcqxssbrai")))

(define-public crate-imgui-sdl2-0.14.0 (c (n "imgui-sdl2") (v "0.14.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "0lrahdb2myq9sd4alqf4pykfi8n35nn8mhxclm25mgi99pgsyvc3")))

(define-public crate-imgui-sdl2-0.14.1 (c (n "imgui-sdl2") (v "0.14.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.11") (d #t) (k 2)) (d (n "sdl2") (r ">=0.34, <0.36") (d #t) (k 0)))) (h "0cjjfgspzryfv9mdm7283difi15dcy2f00q3sacmr6r3q0lhcfb4")))

(define-public crate-imgui-sdl2-0.15.0 (c (n "imgui-sdl2") (v "0.15.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r "^0.8") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.11.1") (d #t) (k 2)) (d (n "sdl2") (r ">=0.34, <0.36") (d #t) (k 0)))) (h "1f6zaxvkajxlbyp6zby9yc87plw12gzq7k10shcnhyaiv5bn4ngi")))

(define-public crate-imgui-sdl2-0.15.1 (c (n "imgui-sdl2") (v "0.15.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r ">=0.8.0, <0.10.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.11.1") (d #t) (k 2)) (d (n "sdl2") (r ">=0.34, <0.36") (d #t) (k 0)))) (h "1z2jcgf51xpgn1rr81n0bdbk02932vmfn0mlciviy4brpgy42bkc")))

(define-public crate-imgui-sdl2-0.15.2 (c (n "imgui-sdl2") (v "0.15.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r ">=0.8.0, <0.12.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 2)) (d (n "sdl2") (r ">=0.34, <0.36") (d #t) (k 0)))) (h "02vg15p6lv0a2j44jmmq8j05vgd7xdmzqrj6g81k4x6qr48nxw1c")))

(define-public crate-imgui-sdl2-0.15.3 (c (n "imgui-sdl2") (v "0.15.3") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "imgui") (r ">=0.8.0, <0.12.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.12.1") (d #t) (k 2)) (d (n "sdl2") (r ">=0.34, <0.37") (d #t) (k 0)))) (h "0g282a2wr22ywpi5nr4hb7ag29qirpfcn3iwd98wsmv0qxf7n8dx")))

