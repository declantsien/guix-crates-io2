(define-module (crates-io im gu imgui-log) #:use-module (crates-io))

(define-public crate-imgui-log-0.1.0 (c (n "imgui-log") (v "0.1.0") (d (list (d (n "amethyst") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "amethyst-imgui") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "imgui") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0k6zm0lmdf4gm3dw25cp4vdx9889qbam4v67syyaxbqvc331z4pi") (f (quote (("amethyst-system" "amethyst" "amethyst-imgui"))))))

