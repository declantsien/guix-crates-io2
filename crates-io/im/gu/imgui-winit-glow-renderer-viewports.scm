(define-module (crates-io im gu imgui-winit-glow-renderer-viewports) #:use-module (crates-io))

(define-public crate-imgui-winit-glow-renderer-viewports-0.11.0 (c (n "imgui-winit-glow-renderer-viewports") (v "0.11.0") (d (list (d (n "glow") (r "^0.12.0") (d #t) (k 0)) (d (n "glutin") (r "^0.30.3") (d #t) (k 0)) (d (n "glutin-winit") (r "^0.2.1") (d #t) (k 0)) (d (n "imgui") (r "^0.11.0") (f (quote ("docking"))) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "0rbcl6sndp5hdvg5sw07zcvqz8m1bz1j99djigz7db7k2ymyc125")))

(define-public crate-imgui-winit-glow-renderer-viewports-0.12.0 (c (n "imgui-winit-glow-renderer-viewports") (v "0.12.0") (d (list (d (n "glow") (r "^0.13.1") (d #t) (k 0)) (d (n "glutin") (r "^0.31.1") (d #t) (k 0)) (d (n "glutin-winit") (r "^0.4.2") (d #t) (k 0)) (d (n "imgui") (r "^0.12.0") (f (quote ("docking"))) (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (d #t) (k 0)))) (h "1szffl2s62kax04jwn6s0zkwby8lc9bryxi1pd7hv42f9m2wdwxk")))

