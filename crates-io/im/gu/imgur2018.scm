(define-module (crates-io im gu imgur2018) #:use-module (crates-io))

(define-public crate-imgur2018-0.1.0 (c (n "imgur2018") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qzwl2fv2wsvgcal4lc21kgdg0fkpb4pd9qhaj0bya4fgklaxwgw")))

(define-public crate-imgur2018-0.1.1 (c (n "imgur2018") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0afigmbyal3lyl7fv64gmjnfq5wgb1vbingjzks3mxawjdglirl8")))

