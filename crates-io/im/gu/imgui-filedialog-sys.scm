(define-module (crates-io im gu imgui-filedialog-sys) #:use-module (crates-io))

(define-public crate-imgui-filedialog-sys-0.1.0 (c (n "imgui-filedialog-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.7") (d #t) (k 0)))) (h "1fa7wpmdixnn2rgxy1cg1y3dda4a0jsagbsfhn8f6fv5wr5vq7zn") (l "ImGuiFileDialog")))

(define-public crate-imgui-filedialog-sys-0.1.1 (c (n "imgui-filedialog-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.7") (d #t) (k 0)))) (h "1cl68hpi686nb1bg7bfxb6wyvmwiyh9qslhrayfwawrdzrh6ryb5") (l "ImGuiFileDialog")))

