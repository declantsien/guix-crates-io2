(define-module (crates-io im gu imgur_openapi) #:use-module (crates-io))

(define-public crate-imgur_openapi-0.4.0 (c (n "imgur_openapi") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "10blz2h1iwbma08fphqwaj8m60bfndyvrvpbg6jglanhh9nkpw8a")))

