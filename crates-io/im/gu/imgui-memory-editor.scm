(define-module (crates-io im gu imgui-memory-editor) #:use-module (crates-io))

(define-public crate-imgui-memory-editor-0.1.0 (c (n "imgui-memory-editor") (v "0.1.0") (d (list (d (n "imgui") (r "^0.3.0") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0h1rc1hymvy24x7zn23h25fc1qsn77jvv5d1xlllg6fzxr3lkyp8")))

(define-public crate-imgui-memory-editor-0.1.1 (c (n "imgui-memory-editor") (v "0.1.1") (d (list (d (n "imgui") (r "^0.3.0") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "15xgw8xyc8xh4vig42iwzr9098z8chzsg72rpqaxjyi6mk8s2z54")))

(define-public crate-imgui-memory-editor-0.1.2 (c (n "imgui-memory-editor") (v "0.1.2") (d (list (d (n "imgui") (r "^0.3.0") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0m3x8m4fkhdfzkh66yrndn0z74mg2nkjgpyylncvv06h0m6qi4vm")))

(define-public crate-imgui-memory-editor-0.1.3 (c (n "imgui-memory-editor") (v "0.1.3") (d (list (d (n "imgui") (r "^0.4.0") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1g12rzhp2mrpm0g0d67cp8j0gd92hy4sxr1s368872b7z2fqkzhj")))

(define-public crate-imgui-memory-editor-0.2.0 (c (n "imgui-memory-editor") (v "0.2.0") (d (list (d (n "imgui") (r "^0.6.1") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1cw6ki068221iyphpg7c75ivswfrd44320s7kd51715qchyaccm4")))

(define-public crate-imgui-memory-editor-0.2.1 (c (n "imgui-memory-editor") (v "0.2.1") (d (list (d (n "imgui") (r "^0.6.1") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1j8sblbqgi9b02r9vpcqm038kb1rqy3s33r8a8fwmrqizjxvf726")))

(define-public crate-imgui-memory-editor-0.3.0 (c (n "imgui-memory-editor") (v "0.3.0") (d (list (d (n "imgui") (r "^0.7.0") (d #t) (k 0)) (d (n "imgui-memory-editor-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0lw9nl0r7h3ik999z8cxfd6vr19amsysp8yza1g2w1122w79v0w7")))

