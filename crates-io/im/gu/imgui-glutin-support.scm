(define-module (crates-io im gu imgui-glutin-support) #:use-module (crates-io))

(define-public crate-imgui-glutin-support-0.0.21 (c (n "imgui-glutin-support") (v "0.0.21") (d (list (d (n "glutin") (r ">= 0.17, <= 0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)))) (h "08n8a0x5qr5wwrxw4q8xvjr249b0ypbsnb3amr8byzhq2qfpvxhf")))

