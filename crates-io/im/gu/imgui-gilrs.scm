(define-module (crates-io im gu imgui-gilrs) #:use-module (crates-io))

(define-public crate-imgui-gilrs-0.1.0 (c (n "imgui-gilrs") (v "0.1.0") (d (list (d (n "gilrs") (r "^0.10") (d #t) (k 0)) (d (n "gilrs") (r "^0.10") (d #t) (k 2)) (d (n "imgui") (r "^0.11") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.24") (d #t) (k 2)) (d (n "imgui-winit-support") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "wgpu") (r "^0.17") (d #t) (k 2)) (d (n "winit") (r "^0.27") (o #t) (d #t) (k 0)))) (h "0k2jl7qqdj5vpr50ysf8ypyikpizngxamgjbha83k0w0hfyfh6zp") (s 2) (e (quote (("winit" "dep:winit" "imgui-winit-support"))))))

