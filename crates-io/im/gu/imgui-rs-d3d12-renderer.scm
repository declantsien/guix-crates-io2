(define-module (crates-io im gu imgui-rs-d3d12-renderer) #:use-module (crates-io))

(define-public crate-imgui-rs-d3d12-renderer-0.1.0 (c (n "imgui-rs-d3d12-renderer") (v "0.1.0") (h "1x3n9sgksqk316bpzq8wijnxl3izrk6j68sd33xzvd3d1rc4zqc8") (y #t)))

(define-public crate-imgui-rs-d3d12-renderer-0.0.2 (c (n "imgui-rs-d3d12-renderer") (v "0.0.2") (h "03g0phjd931vhj32jpnxqq3z6qnwvd19rr4k101b7nq29z3aavn6")))

(define-public crate-imgui-rs-d3d12-renderer-0.0.3 (c (n "imgui-rs-d3d12-renderer") (v "0.0.3") (h "1jky0dbiv3lqzflkbygajm8qw7pmx1mcf553ls82m9slff9n2js2")))

