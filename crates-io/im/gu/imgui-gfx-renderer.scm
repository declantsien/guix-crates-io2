(define-module (crates-io im gu imgui-gfx-renderer) #:use-module (crates-io))

(define-public crate-imgui-gfx-renderer-0.0.14 (c (n "imgui-gfx-renderer") (v "0.0.14") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "imgui") (r "^0.0.14") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.14") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0qrcmjvvnjgpw429kq0d5k5mjpx041f1v7jbagnj9x85hv332mkm")))

(define-public crate-imgui-gfx-renderer-0.0.15 (c (n "imgui-gfx-renderer") (v "0.0.15") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "imgui") (r "^0.0.15") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.15") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0i4g45sbr185zyp1jki6x7dsvdk19p2vkjb3ipqq16qsyawh7rqq")))

(define-public crate-imgui-gfx-renderer-0.0.16 (c (n "imgui-gfx-renderer") (v "0.0.16") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "imgui") (r "^0.0.16") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.16") (f (quote ("gfx"))) (d #t) (k 0)))) (h "12w6axgh6l5954alrdi2hsyscl397ly3fzaq7x77w5bdwbh8gb92")))

(define-public crate-imgui-gfx-renderer-0.0.17 (c (n "imgui-gfx-renderer") (v "0.0.17") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "imgui") (r "^0.0.17") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.17") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0nxib0f37jmz6k1w82cmr9jz7fkg1azly5mf63cpf4k7ydmb9pxj")))

(define-public crate-imgui-gfx-renderer-0.0.18 (c (n "imgui-gfx-renderer") (v "0.0.18") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "imgui") (r "^0.0.18") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.18") (f (quote ("gfx"))) (d #t) (k 0)))) (h "126ignmpnc7506dyx9qqdmsd6k6wxr4bb1qiljgk1jbgjyw048vg")))

(define-public crate-imgui-gfx-renderer-0.0.19 (c (n "imgui-gfx-renderer") (v "0.0.19") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "imgui") (r "^0.0.19") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.19") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0j8gnkl7vh9c79h5ics8h3kn33f39mx44pmvz8s2vp1z916ry2vk")))

(define-public crate-imgui-gfx-renderer-0.0.20 (c (n "imgui-gfx-renderer") (v "0.0.20") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "imgui") (r "^0.0.20") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.20") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0yavfvzsy42av4pgkchjh45libbfcl5ap0z99pa0igifc82zjj5r")))

(define-public crate-imgui-gfx-renderer-0.0.21 (c (n "imgui-gfx-renderer") (v "0.0.21") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.21") (f (quote ("gfx"))) (d #t) (k 0)))) (h "1v324m6j8i8x3hz0bn1zy0xyn9y86yadswa6ycv6xnyvphj39vpv")))

(define-public crate-imgui-gfx-renderer-0.0.22 (c (n "imgui-gfx-renderer") (v "0.0.22") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.22") (f (quote ("gfx"))) (d #t) (k 0)))) (h "03v45ss4ylfi2j4bwkksfkv6zqmqfmdapw4r8c2vikkbr1fgbmcr")))

(define-public crate-imgui-gfx-renderer-0.0.23 (c (n "imgui-gfx-renderer") (v "0.0.23") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.0.23") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.0.23") (f (quote ("gfx"))) (d #t) (k 0)))) (h "0rs16kc64i1angs5la8pkhwdqcp9ax51919y8hjpxj8r5ba2rwaj")))

(define-public crate-imgui-gfx-renderer-0.1.0-rc.1 (c (n "imgui-gfx-renderer") (v "0.1.0-rc.1") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.1.0-rc.1") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1j389vsg6gv8cv8xfi8rzpzav48psj054cgz2k5l1n3wjaq8ayy1") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.1.0-rc.2 (c (n "imgui-gfx-renderer") (v "0.1.0-rc.2") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.1.0-rc.2") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "19r7sy632f8qjx08v4xc8042isr2vfmi74yh0n13w9l1h2xifd7n") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.1.0 (c (n "imgui-gfx-renderer") (v "0.1.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.1.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "09p5k24kliarz06v3vaci0j3xr1prkra5612i2iifjzqgsga9rh9") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.2.0 (c (n "imgui-gfx-renderer") (v "0.2.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.2.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1299k453dffsl56amay8ipskqzgswk8asmsc7pjsrqqqxx7jhdrn") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.3.0 (c (n "imgui-gfx-renderer") (v "0.3.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.3.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1v5jlhbssqnhf5l1zyz7yn9gbgbhw0q1qkb3ff1c2sx08irawvjh") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.4.0 (c (n "imgui-gfx-renderer") (v "0.4.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.4.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "156z4bmn79wd661fwg0nxk55b689ndv5bi6ir2y51r1ffdjz8r8q") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.5.0 (c (n "imgui-gfx-renderer") (v "0.5.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.5.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "16zz5q59pcd03i0ncvfyn04ym1s7sichiza1hhaggw14mzz27i8j") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.6.0 (c (n "imgui-gfx-renderer") (v "0.6.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.6.0") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "165w8f27w1gd47cbqm124y07piay8b5hmgy51yjiydjviwzj47b5") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.6.1 (c (n "imgui-gfx-renderer") (v "0.6.1") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.6.1") (f (quote ("gfx"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1dhg0nhn31yiv1my3jikwm4cpjjl0jqyj13qpwrwr1ajv0kldfhj") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.7.0 (c (n "imgui-gfx-renderer") (v "0.7.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.7.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "0zb89a1kgl76mwjs2nbhd2x7mql3p7ykzrn167vkxivzn1150nic") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.8.0 (c (n "imgui-gfx-renderer") (v "0.8.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "108qkjs6ad66dx3hbvqcxkjwjw9l4cy0pl72agnmyvjb37k96ahc") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.8.1 (c (n "imgui-gfx-renderer") (v "0.8.1") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "00d2prcg34znxajgfdv5d44gba3i62aawgcjk8wx8zdrh8sr245i") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

(define-public crate-imgui-gfx-renderer-0.8.2 (c (n "imgui-gfx-renderer") (v "0.8.2") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3dcompiler"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1aryq2ck77q2wxm9bbwxila2rfspz7g0bhmvnn0xilgghc284q8s") (f (quote (("opengl") ("directx") ("default" "opengl"))))))

