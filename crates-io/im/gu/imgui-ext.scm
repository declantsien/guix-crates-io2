(define-module (crates-io im gu imgui-ext) #:use-module (crates-io))

(define-public crate-imgui-ext-0.1.0-rc0 (c (n "imgui-ext") (v "0.1.0-rc0") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.0-rc0") (d #t) (k 0)))) (h "0mcpcsd6mdl7m6xlr8ywzpdnav8lnvyh9h1m4y18859253zms86p")))

(define-public crate-imgui-ext-0.1.0-rc1 (c (n "imgui-ext") (v "0.1.0-rc1") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.0-rc1") (d #t) (k 0)))) (h "0gcsvs21nv6hr2agkrwx894g05zwcmza318yc6irl4vnvz698hx3")))

(define-public crate-imgui-ext-0.1.0-rc2 (c (n "imgui-ext") (v "0.1.0-rc2") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.0-rc2") (d #t) (k 0)))) (h "1aalipmjms31sfldilwqfjs3c93vp110dds9zq98iag2b1crn95p")))

(define-public crate-imgui-ext-0.1.0-rc3 (c (n "imgui-ext") (v "0.1.0-rc3") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.0-rc3") (d #t) (k 0)))) (h "1wy7cckgri75w57cr45cb0y5fwy92vnz3975g68xrabpdrv765sj")))

(define-public crate-imgui-ext-0.1.0 (c (n "imgui-ext") (v "0.1.0") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.0") (d #t) (k 0)))) (h "16mk19jlpr6d2j0yrylc2xwl4i30n797awgrzj5v4qxrhlcfd73f")))

(define-public crate-imgui-ext-0.1.1 (c (n "imgui-ext") (v "0.1.1") (d (list (d (n "imgui") (r "^0.0") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0lj6y38w6fiiwkf4vj3dl9w7nwwgxys7nakk0a93m2xp7pnzq1mr")))

(define-public crate-imgui-ext-0.1.2 (c (n "imgui-ext") (v "0.1.2") (d (list (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1ljxfbj0w3m1g9x9qgipgac3ci5jh2cwaspjk5hf09k85prn0k7y")))

(define-public crate-imgui-ext-0.1.3 (c (n "imgui-ext") (v "0.1.3") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.3") (d #t) (k 0)))) (h "1p71gvh23fagpikv59dcbfkm4lfgqj34piyyd78hd2lhhfpfhqaa")))

(define-public crate-imgui-ext-0.1.4 (c (n "imgui-ext") (v "0.1.4") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.4") (d #t) (k 0)))) (h "0cld6211v5qsai125bzqpms6rsfbkw52mwrs621012j67pvn6cs3")))

(define-public crate-imgui-ext-0.1.5 (c (n "imgui-ext") (v "0.1.5") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0amglsjv7cw8a86vfy3zziadn3z61qzyq7m6aigk36bnvk3vc8l3")))

(define-public crate-imgui-ext-0.1.6 (c (n "imgui-ext") (v "0.1.6") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.6") (d #t) (k 0)))) (h "1pvhncxfy56j2pzk811yjc8fm2zc5h2csmjq1fbs5swgyzlcspjv")))

(define-public crate-imgui-ext-0.1.7 (c (n "imgui-ext") (v "0.1.7") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.7") (d #t) (k 0)))) (h "1ljvb8phqbdvyk9nid4wi26gxp924yzshpl8417m9p3wpsqipl1g")))

(define-public crate-imgui-ext-0.1.8 (c (n "imgui-ext") (v "0.1.8") (d (list (d (n "imgui") (r "^0.0.22") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.1.8") (d #t) (k 0)))) (h "14789czr9w2b4csifffgqi02sbqcph3rvq2q83zdqfyjy8ykmn72")))

(define-public crate-imgui-ext-0.2.0 (c (n "imgui-ext") (v "0.2.0") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.23") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4.0") (d #t) (k 2)) (d (n "imgui-sdl2") (r "^0.5.0") (d #t) (k 2)) (d (n "na") (r "^0.18.0") (d #t) (k 2) (p "nalgebra")) (d (n "sdl2") (r "^0.32.1") (f (quote ("bundled"))) (d #t) (k 2)))) (h "062r0z7wdja8cpng88kx4j4dnwj5gc60pvbah4s6ir848gibx9jm") (f (quote (("matrix") ("default" "matrix"))))))

(define-public crate-imgui-ext-0.2.1 (c (n "imgui-ext") (v "0.2.1") (d (list (d (n "gl") (r "^0.12.0") (d #t) (k 2)) (d (n "imgui") (r "^0.0.23") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4.0") (d #t) (k 2)) (d (n "imgui-sdl2") (r "^0.5.0") (d #t) (k 2)) (d (n "na") (r "^0.18.0") (d #t) (k 2) (p "nalgebra")) (d (n "sdl2") (r "^0.32.1") (f (quote ("bundled"))) (d #t) (k 2)))) (h "092d7b6c7v1h7sxr3gvx8d6r7yisr8wgmnjd7x6gqk1ka4ls43jk") (f (quote (("matrix") ("default" "matrix"))))))

(define-public crate-imgui-ext-0.3.0 (c (n "imgui-ext") (v "0.3.0") (d (list (d (n "gl") (r "^0.13.0") (d #t) (k 2)) (d (n "imgui") (r "^0.1") (d #t) (k 0)) (d (n "imgui-ext-derive") (r "^0.3") (d #t) (k 0)) (d (n "imgui-opengl-renderer") (r "^0.4.0") (d #t) (k 2)) (d (n "imgui-sdl2") (r "^0.5.0") (d #t) (k 2)) (d (n "na") (r "^0.18.0") (d #t) (k 2) (p "nalgebra")) (d (n "sdl2") (r "^0.32.1") (f (quote ("bundled"))) (d #t) (k 2)))) (h "1ma8l4chmwpzqqgwi9cxp1rq7cssqlkglpmjs04703wf8xjc93nk") (f (quote (("mint") ("matrix") ("default" "matrix"))))))

