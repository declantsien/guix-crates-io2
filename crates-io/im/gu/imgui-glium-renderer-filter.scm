(define-module (crates-io im gu imgui-glium-renderer-filter) #:use-module (crates-io))

(define-public crate-imgui-glium-renderer-filter-0.4.0 (c (n "imgui-glium-renderer-filter") (v "0.4.0") (d (list (d (n "glium") (r "^0.27") (k 0)) (d (n "imgui") (r "^0.4.0") (f (quote ("glium"))) (d #t) (k 0)))) (h "1097i1v4grcnlj1n34x2d3is79qpr7p53jh2qfjxgh37wmc81zv1")))

