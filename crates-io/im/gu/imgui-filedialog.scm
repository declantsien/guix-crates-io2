(define-module (crates-io im gu imgui-filedialog) #:use-module (crates-io))

(define-public crate-imgui-filedialog-0.1.0 (c (n "imgui-filedialog") (v "0.1.0") (d (list (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-filedialog-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x2khcqarbp9zg3vmqkp5l6r1jafw6lxzgdv9npny8ca7qpdrrd1")))

(define-public crate-imgui-filedialog-0.1.1 (c (n "imgui-filedialog") (v "0.1.1") (d (list (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-filedialog-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0n63avyvq2w0sfa9r8j3s4h3jwqlaarydj0p8hyxvycb2qyzfwd3")))

(define-public crate-imgui-filedialog-0.2.0 (c (n "imgui-filedialog") (v "0.2.0") (d (list (d (n "imgui") (r "^0.7") (d #t) (k 0)) (d (n "imgui-filedialog-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0vw0yclhhya9r9mj7dn9h8hfg5gx7srbpi3l92ip4r1azxg6l4q6")))

