(define-module (crates-io im gu imgui-winit) #:use-module (crates-io))

(define-public crate-imgui-winit-0.1.0 (c (n "imgui-winit") (v "0.1.0") (d (list (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "winit") (r "^0.17") (d #t) (k 0)))) (h "1cdv698jb7vwga63x99da40gnp8mg3fgw1hdlsw72j2p7ihpd2ls") (y #t)))

(define-public crate-imgui-winit-0.1.1 (c (n "imgui-winit") (v "0.1.1") (d (list (d (n "imgui") (r "^0.0.21") (d #t) (k 0)) (d (n "winit") (r "^0.17") (d #t) (k 0)))) (h "0s6msazz31lfmjmlp657s8ncraxxnhiwqfghifzm4biac94wbwvb")))

(define-public crate-imgui-winit-0.1.2 (c (n "imgui-winit") (v "0.1.2") (d (list (d (n "imgui") (r ">= 0.0.21") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "1870bwjasgzwy062vrqc1s6z20jw8ajj6cax7nn72f82qdjqns70")))

