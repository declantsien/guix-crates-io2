(define-module (crates-io im so imsosorrybtw) #:use-module (crates-io))

(define-public crate-imsosorrybtw-0.1.0 (c (n "imsosorrybtw") (v "0.1.0") (h "0s3f4dgy0ka8i2d1vqwxfr3fxzks50x7ykkddrsbyp670czxs6vx")))

(define-public crate-imsosorrybtw-0.2.0 (c (n "imsosorrybtw") (v "0.2.0") (h "14j9lw476h5yzanwh97f5cpw6bxixas8qs8lpc6r39n8i1azngwx")))

(define-public crate-imsosorrybtw-0.3.0 (c (n "imsosorrybtw") (v "0.3.0") (h "1fdbc2sh8pzr60c02qs32vs7r3x6frj0r1pj1yvrwbrcjn987fhi")))

