(define-module (crates-io im gi imgix) #:use-module (crates-io))

(define-public crate-imgix-0.1.0 (c (n "imgix") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "069z8gbik2d8iw7q43ahrk2cairfrcg24xy16a59igla1kfpws72")))

(define-public crate-imgix-0.1.1 (c (n "imgix") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1kn3lgvw9b6ji9nk9w2n1i2iy4s9c4yb5pphfj4cy45v8mshcgp7")))

(define-public crate-imgix-0.1.2 (c (n "imgix") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0jml63i08km5lq3g8h9majd9p5h2lr2byywbplfh5pdsd4lkqlyd")))

(define-public crate-imgix-0.1.3 (c (n "imgix") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1d6sf9yzp2hrvdccxp9xwv1v66cxvz8lpsf3c87dpqwprabz9fib")))

