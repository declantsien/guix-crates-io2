(define-module (crates-io im di imdisable) #:use-module (crates-io))

(define-public crate-imdisable-0.1.2 (c (n "imdisable") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "device_query") (r "^1.1.3") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_UI_Input_Ime" "Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0z3w29387ady1i5f945a8zv1xqsgl4i4955sasf5lp4naq69i1r0")))

(define-public crate-imdisable-0.1.3 (c (n "imdisable") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "device_query") (r "^1.1.3") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_UI_Input_Ime" "Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "19sg3snmf9chsbrvffgvjwgjfclfffmzfjj5yis29gm3wwac92dq")))

(define-public crate-imdisable-0.1.4 (c (n "imdisable") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "device_query") (r "^1.1.3") (d #t) (k 0)) (d (n "inputbot") (r "^0.6.0") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_UI_Input_Ime" "Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yhnx0imk4mkysvn9pia2sjyfp3xpxnvkdw03nqacg17wmjb6ygn")))

