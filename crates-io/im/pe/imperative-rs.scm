(define-module (crates-io im pe imperative-rs) #:use-module (crates-io))

(define-public crate-imperative-rs-0.1.0 (c (n "imperative-rs") (v "0.1.0") (d (list (d (n "imperative-rs-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0qygg8yay5qp54ns8q7nkgliaymg0rbxx4b53agqvm9s7j11axf2")))

(define-public crate-imperative-rs-0.1.1 (c (n "imperative-rs") (v "0.1.1") (d (list (d (n "imperative-rs-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1s1cgmw3gvvpl9kimyi5djrfkjmv2zs7claj35mb2dj51s5m7k1y")))

(define-public crate-imperative-rs-0.1.2 (c (n "imperative-rs") (v "0.1.2") (d (list (d (n "imperative-rs-derive") (r "^0.1.2") (d #t) (k 0)))) (h "175gz7dcw1xbpfczf5yppb6llhff0dv09zs8kncb8gavxp79d28r")))

(define-public crate-imperative-rs-0.1.3 (c (n "imperative-rs") (v "0.1.3") (d (list (d (n "imperative-rs-derive") (r "^0.1.3") (d #t) (k 0)))) (h "1biydaf6a1cij8rp5g66birlxwa7h9mkqipn1kmsmnzgs9ws20i5")))

(define-public crate-imperative-rs-0.1.4 (c (n "imperative-rs") (v "0.1.4") (d (list (d (n "imperative-rs-derive") (r "^0.1.4") (d #t) (k 0)))) (h "1ffnaqykl9nmkxv1kxq6d0zc4q2da3cv4f582h2xff9mr5wvj89r")))

(define-public crate-imperative-rs-0.2.0 (c (n "imperative-rs") (v "0.2.0") (d (list (d (n "imperative-rs-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0iz1lk3rxyxwy9i97611h8rhjx2rb6lplxw63i2x7jg32887v0sz")))

(define-public crate-imperative-rs-0.3.0 (c (n "imperative-rs") (v "0.3.0") (d (list (d (n "imperative-rs-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0056773pz7cr6vnp1i33kdbkkp3136nwngj8cyc9dgsczaspw9jz")))

(define-public crate-imperative-rs-0.3.1 (c (n "imperative-rs") (v "0.3.1") (d (list (d (n "imperative-rs-derive") (r "^0.3.1") (d #t) (k 0)))) (h "18waa1p87f37cjh5vmnh0ppmdlvkqivvmxklkf8rw5pym2iz1m7y")))

