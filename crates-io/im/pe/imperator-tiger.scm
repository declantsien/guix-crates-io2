(define-module (crates-io im pe imperator-tiger) #:use-module (crates-io))

(define-public crate-imperator-tiger-0.9.3 (c (n "imperator-tiger") (v "0.9.3") (d (list (d (n "ansiterm") (r "^0.12.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiger-lib") (r "^0.9.3") (f (quote ("imperator"))) (k 0)))) (h "1213m3p9y12np2sfa7g97nan9lmb1wbxfsx2vvhv6kgc90ji32m5") (r "1.70")))

(define-public crate-imperator-tiger-0.9.4 (c (n "imperator-tiger") (v "0.9.4") (d (list (d (n "ansiterm") (r "^0.12.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiger-lib") (r "^0.9.4") (f (quote ("imperator"))) (k 0)))) (h "1sg1lq6xq91prnh8hq6m3iiikvqq3g3i910zpk1jb5p8am3xvvkc") (r "1.70")))

(define-public crate-imperator-tiger-0.9.5 (c (n "imperator-tiger") (v "0.9.5") (d (list (d (n "ansiterm") (r "^0.12.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiger-lib") (r "^0.9.5") (f (quote ("imperator"))) (k 0)))) (h "0xpfhlnpjhlp7m82i1d0g74wa0rn423w8p1whfwh4k7if2wmlbv8") (r "1.70")))

(define-public crate-imperator-tiger-0.9.6 (c (n "imperator-tiger") (v "0.9.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tiger-bin-shared") (r "^0.9.6") (f (quote ("imperator"))) (k 0)))) (h "0qn65dffxcky4bz0s6jjzvwmr34sqk4ab5gf993kq4n1jaywaiqy") (r "1.70")))

