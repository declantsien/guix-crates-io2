(define-module (crates-io im pe imperium) #:use-module (crates-io))

(define-public crate-imperium-0.1.0 (c (n "imperium") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1k3z4wffvbq06h2qb74sf8n57fphs1k5aibg6d78jdg6ncwyb4bi")))

(define-public crate-imperium-0.1.1 (c (n "imperium") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0ny54ifgzsvarhdj3x6xrg6j3997ywm6h0rsqgpav8xh5af367bq")))

(define-public crate-imperium-0.1.2 (c (n "imperium") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0dmzsnr1ajgrwlcy05yy5ckd1b3p2bs6glbdvs9vds6xwjlx26f7")))

(define-public crate-imperium-0.1.3 (c (n "imperium") (v "0.1.3") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "11a292x9f0az8i7il01mxy2nlxk9sha9kb40kik7kr9mipn0m8l3")))

(define-public crate-imperium-0.1.4 (c (n "imperium") (v "0.1.4") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1d85aqpp9vsnkykizp0yarskhs733vxr1k2msksj1yx5hrb4ah1z")))

(define-public crate-imperium-0.1.5 (c (n "imperium") (v "0.1.5") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "198wi8zj2y7l83vl45qvq08325jp63a1agvqkl6fgwx5jqmqk3rg")))

(define-public crate-imperium-0.1.6 (c (n "imperium") (v "0.1.6") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0m7w443znyrrcxkdmc22258hrvs55fqlba1f4lzxml95930k5y9i")))

(define-public crate-imperium-0.1.61 (c (n "imperium") (v "0.1.61") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "09karfdaz4rckgkg3aj8nxgyikjgr9zl2yrclx5aip5bl4d3m4iq")))

(define-public crate-imperium-0.1.62 (c (n "imperium") (v "0.1.62") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0pqp6706pdhi13hqw1j7ci82090mdzzxsy0301hrc4r27g359s98")))

(define-public crate-imperium-0.1.63 (c (n "imperium") (v "0.1.63") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1qn3csr7vaf1vk31f5v0qck0fr08mwpnn5irg2cfl92sjhcryarw")))

(define-public crate-imperium-0.1.64 (c (n "imperium") (v "0.1.64") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1280jnn3233bip7q10kdmmdjjzg0f1w4mvpyafmrf6fabg0av776")))

(define-public crate-imperium-0.1.65 (c (n "imperium") (v "0.1.65") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0aj6yk4c4s7ykwrncy6a1zmanzf7cq9hyyyq0iwjg1s7pqk6hrnn")))

(define-public crate-imperium-0.1.66 (c (n "imperium") (v "0.1.66") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "047pclh2ack69w4jmlgk9msrifm2fggqbqf05yhnwfa6h22ih075")))

(define-public crate-imperium-0.1.7 (c (n "imperium") (v "0.1.7") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1ajhv17f1l9ayqpk7z48q5fh8dlaf0my3mwldqxy9gsfxk4b1az9")))

(define-public crate-imperium-0.2.0 (c (n "imperium") (v "0.2.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0x3afmx5wz0ygksf90jz7djszwva9wlkalnisrpjjjfxa427zv0n")))

(define-public crate-imperium-0.2.1 (c (n "imperium") (v "0.2.1") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1abi5nmg6nr1cr5afayfdccdcars1y6r1ylv4gkbh30r1qn1rzw4")))

(define-public crate-imperium-0.2.2 (c (n "imperium") (v "0.2.2") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0sp9n8hs7g7rgysjdmpg78ddpw51nm63lp5ank7pwk8vxw5wxg72")))

(define-public crate-imperium-0.2.3 (c (n "imperium") (v "0.2.3") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1jn6jxyrisxlgxj6bpkdnvir2smpdc7mmkmb38m61nzxr9sx4mzn")))

(define-public crate-imperium-0.2.4 (c (n "imperium") (v "0.2.4") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0iw1sz0xf9q58bk570ngy60v8gygiv9byzgpdbnmw2pnrsf4s8fj")))

(define-public crate-imperium-0.2.5 (c (n "imperium") (v "0.2.5") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0vrrz6z3l4n9wn9ddya4cq5x2h1hbr2wrzdj36ydp1y957bpay0m")))

(define-public crate-imperium-0.2.6 (c (n "imperium") (v "0.2.6") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0b85rj4h484xvdn8rxsh3agw5n952bzh93940cfkvmb76zsdwrhj")))

(define-public crate-imperium-0.2.7 (c (n "imperium") (v "0.2.7") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "07svgj1ks2jxvz2b7jxiamahwgxjghmmb77726qcs5cnk1cbc6j7")))

(define-public crate-imperium-0.3.0 (c (n "imperium") (v "0.3.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0qy3dviniz1djabsfdqgwbzirkp8nkxaf98wq8p1p5k6mk1cpxvy")))

(define-public crate-imperium-0.3.1 (c (n "imperium") (v "0.3.1") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0prqmk8q88y7wqpsqm5fxypl01x5wrl49mx3hsjd49lb7k469axw")))

(define-public crate-imperium-0.3.2 (c (n "imperium") (v "0.3.2") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1a80098yy30p1zcrwjkbbl69k2q6ddhdgbcygik8i56gzpzvf7f5")))

(define-public crate-imperium-0.3.3 (c (n "imperium") (v "0.3.3") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0a3aivzvs0nspda9xaf110mh58x0cy46wkxafgc47zfzn7y23svx")))

(define-public crate-imperium-0.3.4 (c (n "imperium") (v "0.3.4") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "03kp27c2h119nw61p3jbbnw23lxf3mrmihy9wn3q9zzkw6416ixw")))

(define-public crate-imperium-0.3.6 (c (n "imperium") (v "0.3.6") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "16h0dbnrjm7ny0lyirxzcxzdhfj145mwcawj9lbwkjjabzgjn9hd")))

(define-public crate-imperium-0.4.0 (c (n "imperium") (v "0.4.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "13iic1lkgdciq2hpdgickbdgc38jpkxsklxls7fwc68calpf8x3g")))

(define-public crate-imperium-0.5.0 (c (n "imperium") (v "0.5.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "194mwd257v0cb0hbyk5lmdg39kyml8wgvw548maw8gzgk1f4lyjj")))

(define-public crate-imperium-0.6.0 (c (n "imperium") (v "0.6.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0q0idlfn593kq850q763shg7z8k8kc2k56nk81x94vn5yh0fbijq")))

(define-public crate-imperium-0.7.0 (c (n "imperium") (v "0.7.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "00nwr4ljrjrgb3sdha3jdppwbzpjzjwa995lnra390pd68lnzbn4")))

(define-public crate-imperium-0.7.1 (c (n "imperium") (v "0.7.1") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0xjcgl9rlrc68knq5gl3jbvd6gmr22b0i0rk3p7bam8nk8g1r74g")))

(define-public crate-imperium-0.7.2 (c (n "imperium") (v "0.7.2") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "01b17gb5qzb5rg27wadks98vy6k0imkvfbkx5n6ydp2yls056s9i")))

(define-public crate-imperium-0.8.0 (c (n "imperium") (v "0.8.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "07nrxgdlf4ik0zqasz5cwg8r0nf8kvyakqgx0szd52ngqygakinb")))

(define-public crate-imperium-0.9.0 (c (n "imperium") (v "0.9.0") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0qg75x1y6i8yqy3cygv3g2hbp7wp8adpr1zppk9b5zmja767imwf")))

(define-public crate-imperium-0.9.1 (c (n "imperium") (v "0.9.1") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "04dmjwnq9qa1x0d8dn3fmzdpmmkmhlhpv0rchm80clycyrnc209a")))

(define-public crate-imperium-0.9.2 (c (n "imperium") (v "0.9.2") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0vggc7fzbyfj0qjimah0ai061a28mmw27czbhqdysvm3xpg116mi")))

(define-public crate-imperium-0.9.3 (c (n "imperium") (v "0.9.3") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1g03zickq7m4wb0708aikir4kism7dr1kzp9syp8ksd82mbvv4kc")))

(define-public crate-imperium-0.9.4 (c (n "imperium") (v "0.9.4") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0qqzqygw6s64lj2fkmcb5rdqfdi9njpl3r3zgl69wn8x8z28rjh5")))

(define-public crate-imperium-0.9.5 (c (n "imperium") (v "0.9.5") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1rrwh3wd77jyzd7wh072hb41xf2awpxxkxn9c96pgm87dkc0mn46")))

(define-public crate-imperium-0.9.6 (c (n "imperium") (v "0.9.6") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "14gnh0zy1spxqdqq2ly6q9h89n0vnjifx5n1fr58zyln3hqwzzjc")))

(define-public crate-imperium-0.9.7 (c (n "imperium") (v "0.9.7") (d (list (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0hf4wgjx6j2065r1qdbp0jy8rl2xv55i893j1v992nrad6fdfz99")))

