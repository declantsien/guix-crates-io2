(define-module (crates-io im pe impersonate) #:use-module (crates-io))

(define-public crate-impersonate-0.0.1 (c (n "impersonate") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14s76415wxxp11dya9i8q4113asnc07g359a1q6lh1wv4kdb2wn7")))

(define-public crate-impersonate-0.0.2 (c (n "impersonate") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1v9g5ilfhqcp5zqhgn8miqqanpwk8fqv5mzz2p3bq9i18cb1k595")))

(define-public crate-impersonate-0.0.3 (c (n "impersonate") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0anj5s772j4pjsabs9zr1jaz3l2zx8b74i972zg1cdr2v6yjhwkr")))

