(define-module (crates-io im pe imperative-rs-derive) #:use-module (crates-io))

(define-public crate-imperative-rs-derive-0.1.0 (c (n "imperative-rs-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kr1ij8yk5ld395f3aqg8vdf3fnffcmqi6gcb4ahqxgsg4pqsya2")))

(define-public crate-imperative-rs-derive-0.1.1 (c (n "imperative-rs-derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m97p0in4jc5i7mwdw48567c69y4wyhzv16z5al5pczlivvx1bk1")))

(define-public crate-imperative-rs-derive-0.1.2 (c (n "imperative-rs-derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fa7cs6sf2p1k3hyjq51w9pm3wl0l6n1sxmnnvfk32h00vk871fn")))

(define-public crate-imperative-rs-derive-0.1.3 (c (n "imperative-rs-derive") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r15ql8ciskp4xd3qxm529rqmx4akhi4fwyvxy87g71y550cwnra")))

(define-public crate-imperative-rs-derive-0.1.4 (c (n "imperative-rs-derive") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p0pkxg9mip1p8ngs1fppww1shkvlgj0gw405v47rs4ygyg2v988")))

(define-public crate-imperative-rs-derive-0.2.0 (c (n "imperative-rs-derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lx8cbva6gvzxvbxvgbmva8xvy3p52qdfynb04bxdsn0njzq5x8p")))

(define-public crate-imperative-rs-derive-0.3.0 (c (n "imperative-rs-derive") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d5icwmvy1crq4zlfmp8gcidaky451pbbzv9in02l4j8zxf0fy51")))

(define-public crate-imperative-rs-derive-0.3.1 (c (n "imperative-rs-derive") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1axa3idvn38qfphks3g9p3n5kakjmbjgmbqbmhavgci4ibyl5f02")))

