(define-module (crates-io im pe imperative) #:use-module (crates-io))

(define-public crate-imperative-1.0.0 (c (n "imperative") (v "1.0.0") (d (list (d (n "multimap") (r "^0.5.0") (d #t) (k 1)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "rust-stemmers") (r "^1.1.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.1.0") (d #t) (k 1)))) (h "049h1qbqfs4d0zgxsfw038kgd7655p5p6rywhf8c6y7j01mim0dr")))

(define-public crate-imperative-1.0.1 (c (n "imperative") (v "1.0.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.1.0") (d #t) (k 0)))) (h "0snpc1yshrqmflhnvm3ymlglmyya2jn6frc6n0djsj4b34g8p14r")))

(define-public crate-imperative-1.0.2 (c (n "imperative") (v "1.0.2") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.1.0") (d #t) (k 0)))) (h "071v7klvk9rrwc4drq33l1pf7kcrps3d66pn55amy7lls3ilrxkr")))

(define-public crate-imperative-1.0.3 (c (n "imperative") (v "1.0.3") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "multimap") (r "^0.8.3") (d #t) (k 2)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 2)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.3") (f (quote ("path"))) (d #t) (k 2)))) (h "09yd5lnr4kqandk8bf4ipdxn1kb7pbc6kz4drimys3q4vfh6rn7y") (r "1.60.0")))

(define-public crate-imperative-1.0.4 (c (n "imperative") (v "1.0.4") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "multimap") (r "^0.8.3") (d #t) (k 2)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 2)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.3") (f (quote ("path"))) (d #t) (k 2)))) (h "1633df2nvwxigzwprm0308nkpjkhp4klg5pibldry3gyy8xi54lz") (r "1.60.0")))

(define-public crate-imperative-1.0.5 (c (n "imperative") (v "1.0.5") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "multimap") (r "^0.9.0") (d #t) (k 2)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 2)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.11") (f (quote ("path"))) (d #t) (k 2)))) (h "1h1gpliabdzsr40f7fbqk25kz5lmqzy42ab5dnmcsf6mjs17jw4b") (r "1.68.0")))

