(define-module (crates-io im no imnodes) #:use-module (crates-io))

(define-public crate-imnodes-0.1.0 (c (n "imnodes") (v "0.1.0") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imnodes-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1lndv3rhxnx66fxlnx1xl2s5ph7djix0xnh7kng8xdyrlz5hxa7l") (f (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2.0 (c (n "imnodes") (v "0.2.0") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imnodes-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0k1xfpclrsbmqvvyrf8clp5y9gps0a4h9ialz561lljb40qi0hfd") (f (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2.1 (c (n "imnodes") (v "0.2.1") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imnodes-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0lw5mrmvfblka5w13kdjk4icz9my4wz676s60832yxfv33p4n1vz") (f (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2.2 (c (n "imnodes") (v "0.2.2") (d (list (d (n "imgui") (r "^0.11.0") (d #t) (k 0)) (d (n "imnodes-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0f9x7b1mll28i359jfd1fh2v4bl2hb2afvn871hc6f2p1cncw180") (f (quote (("include_low_level_bindings"))))))

