(define-module (crates-io im no imnodes-sys) #:use-module (crates-io))

(define-public crate-imnodes-sys-0.1.0 (c (n "imnodes-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.11.0") (d #t) (k 0)))) (h "17p4ndi4adw1rbdxp9b96661nxf1ri3wsr8ip6r6svk2ppzk65pd") (l "imnodes")))

(define-public crate-imnodes-sys-0.2.0 (c (n "imnodes-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1x34b6g9fhd2zjjjpas92y14fs5icc8zdrwg39pcjng0c7mir9l0") (l "imnodes")))

(define-public crate-imnodes-sys-0.2.1 (c (n "imnodes-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1zqj9wzggasamyfjya4d58avdh4j1nvgnmp0ahf1p0ql90r3i22d") (l "imnodes")))

(define-public crate-imnodes-sys-0.2.2 (c (n "imnodes-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "imgui-sys") (r "^0.11.0") (d #t) (k 0)))) (h "12iyxl5fvxiinhhv4ws4h10gwahlw1r2qw11jxv4bcypfg4727jx") (l "imnodes")))

