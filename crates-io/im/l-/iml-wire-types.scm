(define-module (crates-io im l- iml-wire-types) #:use-module (crates-io))

(define-public crate-iml-wire-types-0.1.0 (c (n "iml-wire-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0q2anxw88m29619fm7ifhnxp2daiyp5ynahkiicw87221l063cr7")))

(define-public crate-iml-wire-types-0.1.1 (c (n "iml-wire-types") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0i8zjh4xrfh6f168yzkp92pm5v0lzayfxmjamn6b09mvl51arasc")))

(define-public crate-iml-wire-types-0.1.2 (c (n "iml-wire-types") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1bm6gjs389s6n2jq06wyh1lnmmvkiybn7rghwnjvy8xxsndaiyvh")))

(define-public crate-iml-wire-types-0.1.3 (c (n "iml-wire-types") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09iq8s0y88p0x7k0nwy0xs7gcxpf67gbhj9rqm17px3pg77xwrpj")))

(define-public crate-iml-wire-types-0.1.4 (c (n "iml-wire-types") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jj45ly68xv49w3mmr4rmb0m76ih0aqfd0m7w5w53h5mhz1jkh4g")))

(define-public crate-iml-wire-types-0.1.5 (c (n "iml-wire-types") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10x4vrdcgra4krg3bfsgy45b4n60h4ay8i349ms07l6sdn95icd1")))

(define-public crate-iml-wire-types-0.1.6 (c (n "iml-wire-types") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m64rw3qac5hl5cd3hlpss60hfnjgxhz2s2lgyw7y76rf74ca03s")))

(define-public crate-iml-wire-types-0.1.7 (c (n "iml-wire-types") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "006lw8394xdxlfasdrj4d0nbkpkl26qkblcb6vaf79vzwzdylnig")))

(define-public crate-iml-wire-types-0.1.8 (c (n "iml-wire-types") (v "0.1.8") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zac58n44h7dadv93d2myf3948p1l1mw8781zhsdrqis8m4b8j0k")))

(define-public crate-iml-wire-types-0.1.9 (c (n "iml-wire-types") (v "0.1.9") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sz4d2dpg2xv6d7mnj56r1rqq7vs1vzbxbnfdhiy03zbl8c329b7")))

(define-public crate-iml-wire-types-0.1.10 (c (n "iml-wire-types") (v "0.1.10") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wd46gvp8lpz9w5awhq6dlnrcdhkhzq1zxr7c42fw0zb4ayhklqq")))

(define-public crate-iml-wire-types-0.1.11 (c (n "iml-wire-types") (v "0.1.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.4.0-rc.3") (o #t) (d #t) (k 0)))) (h "112v0pg982shvznr32rh1iwqhd2mibyn7y300amqximn2l12a9ii") (f (quote (("postgres-interop" "tokio-postgres"))))))

(define-public crate-iml-wire-types-0.2.0 (c (n "iml-wire-types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.4.0-rc.3") (o #t) (d #t) (k 0)))) (h "19b1yvpszq9aph15a4zjlzijyyqv9idag34p6yh85fpqn4f6w8hj") (f (quote (("postgres-interop" "tokio-postgres"))))))

(define-public crate-iml-wire-types-0.2.1 (c (n "iml-wire-types") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "im") (r "^14.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "iml-api-utils") (r "^0.2") (d #t) (k 0)) (d (n "postgres-types") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1bbgvxgbm04zy90wpi5nfajsfnrkp37i2z59ing62irnjpd6mk2d") (f (quote (("postgres-interop" "tokio-postgres" "postgres-types" "bytes"))))))

