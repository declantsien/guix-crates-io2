(define-module (crates-io im l- iml-agent) #:use-module (crates-io))

(define-public crate-iml-agent-0.1.0-alpha.1 (c (n "iml-agent") (v "0.1.0-alpha.1") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ywssbns9wjzak3ygv808z1h21jgkjg674i4ls1chfc72n93x8ap")))

