(define-module (crates-io im u- imu-fusion) #:use-module (crates-io))

(define-public crate-imu-fusion-0.2.1 (c (n "imu-fusion") (v "0.2.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)))) (h "1v4133jzb171gw53jm3qab7ld4cmqzlxwglyb6p7z8bcx2lv8ps0") (f (quote (("fusion-use-normal-sqrt"))))))

(define-public crate-imu-fusion-0.2.4 (c (n "imu-fusion") (v "0.2.4") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm-force"))) (k 0)))) (h "1941xqaxcj880b3kv9szf500qdx91445ksjvlb6lyydxck19n2lb") (f (quote (("fusion-use-normal-sqrt"))))))

