(define-module (crates-io im p_ imp_int) #:use-module (crates-io))

(define-public crate-imp_int-0.2.0 (c (n "imp_int") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0mdigiy4f9f2ms56xnpb8ql20gmp4aizi3c9gqs0fmrigj097mdi") (y #t)))

(define-public crate-imp_int-0.3.0 (c (n "imp_int") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "131252ydj1bn9v4gxk5za28z7kkrpczb4p73vbmj64kvjsm1kqyj") (f (quote (("bigint")))) (y #t)))

(define-public crate-imp_int-0.3.1 (c (n "imp_int") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0r3wshcnannflxn7xysfm4r2hg3hhal0fjsa41qhmgwcg4jpj7nq") (y #t) (s 2) (e (quote (("bigint" "dep:num-bigint"))))))

