(define-module (crates-io im p_ imp_ast) #:use-module (crates-io))

(define-public crate-imp_ast-0.2.0 (c (n "imp_ast") (v "0.2.0") (d (list (d (n "int") (r "^0.2.0") (d #t) (k 0) (p "imp_int")) (d (n "lexer") (r "^0.2.1") (d #t) (k 0) (p "imp_lexer")) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "parser") (r "^0.2.2") (d #t) (k 0) (p "imp_parser")) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tree") (r "^0.2.0") (d #t) (k 0) (p "imp_tree")))) (h "197g0ism1nmb1wj59vfqx6zg5xn4s4gfcw802ki7qr6rcc01zcqm") (y #t)))

