(define-module (crates-io im p_ imp_lexer) #:use-module (crates-io))

(define-public crate-imp_lexer-0.2.0 (c (n "imp_lexer") (v "0.2.0") (d (list (d (n "int") (r "^0.2.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0s0c6lwl8wfb3shj3g4jxpa48sm267yczn5a5h0j702r9mwijbn9") (y #t)))

(define-public crate-imp_lexer-0.2.1 (c (n "imp_lexer") (v "0.2.1") (d (list (d (n "int") (r "^0.2.0") (d #t) (k 2) (p "imp_int")) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "00bfs36f04ivb0kfi1zjzig5inm76agxxqjh3vhyq5m9wci446mb") (y #t)))

