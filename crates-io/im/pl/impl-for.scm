(define-module (crates-io im pl impl-for) #:use-module (crates-io))

(define-public crate-impl-for-0.1.0 (c (n "impl-for") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "replace-types") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1abnfhwajwifmrdwvyyb9wqzzl4s6fh7r0a57ljhqa2k1lqani74")))

(define-public crate-impl-for-0.1.1 (c (n "impl-for") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "replace-types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02imkc78y2kva9f3g30pzlsb6srcbq73wp4klsb700q09gsmkng4")))

(define-public crate-impl-for-0.2.0 (c (n "impl-for") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "replace-types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jcgdwpq2fpb1l8j8y0gblhpid4xh6b92l30z9sqsx4zx392qvz8")))

(define-public crate-impl-for-0.2.1 (c (n "impl-for") (v "0.2.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "replace-types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1s6j2ss2iv1i3lp4c3w3lz6h77mqy5sk9xwi5ld258y1zmgl4v8p")))

