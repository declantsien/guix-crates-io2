(define-module (crates-io im pl impl-serde) #:use-module (crates-io))

(define-public crate-impl-serde-0.1.0 (c (n "impl-serde") (v "0.1.0") (d (list (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "191da21v5m5xprpc0h67dsrg3hywvplisv1l6ar3rv4bn5h7m21x")))

(define-public crate-impl-serde-0.1.1 (c (n "impl-serde") (v "0.1.1") (d (list (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0mibnpvcn0f3p3c7sg8bqkph8fg0gfyy0fnyw468w5flx6fhfn2i")))

(define-public crate-impl-serde-0.2.0 (c (n "impl-serde") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18dlc2gngcsnhsbgldpsqc3gy4hhd17nrxr3lhp5af6pjx5vw9kx")))

(define-public crate-impl-serde-0.2.1 (c (n "impl-serde") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "uint") (r "^0.8.1") (d #t) (k 2)))) (h "0fik4y3mc8irs9i7128byc0dav7iha50sczbmq77m95ci1hymcdv")))

(define-public crate-impl-serde-0.2.2 (c (n "impl-serde") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "uint") (r "^0.8.1") (d #t) (k 2)))) (h "1i2q6grzgia16jg7y5jn1idfr2ddq9nxhcviiy67ihx6vaaxqqx2")))

(define-public crate-impl-serde-0.2.3 (c (n "impl-serde") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.8.1") (d #t) (k 2)))) (h "1f39mh0w640rgg7rh7d7xm03ff55s26zfb6sjnlzazwwx7kwmqsq")))

(define-public crate-impl-serde-0.3.0 (c (n "impl-serde") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.8.1") (d #t) (k 2)))) (h "1y4zn9g0brbxqbjag5ayjnbr2yvvkgzz8qdxmcfgpw42n6lrxgjv")))

(define-public crate-impl-serde-0.3.1 (c (n "impl-serde") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.8.3") (d #t) (k 2)))) (h "0vyz1n3vqp39npnb9fv3if61gql0zxmgcp6fbyjhf5wknv9a8z5l") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-impl-serde-0.3.2 (c (n "impl-serde") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.9.0") (d #t) (k 2)))) (h "0p2zy8ikdxd28s3vb22nwqgnwjn8gx920sr2svdn93j3yd1g0la5") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-impl-serde-0.4.0 (c (n "impl-serde") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint") (r "^0.9.0") (d #t) (k 2)))) (h "1k9mr5wfypgz95azk0k9bgsdb66kd5ia7fjkr2q3vbi8f338zj7b") (f (quote (("std" "serde/std") ("default" "std")))) (r "1.56.1")))

