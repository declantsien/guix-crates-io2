(define-module (crates-io im pl implicit-await) #:use-module (crates-io))

(define-public crate-implicit-await-0.1.0 (c (n "implicit-await") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("nightly" "async-await"))) (d #t) (k 2)) (d (n "implicit-await-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1n5jkryclr0ryjcacyyhdzj2jl11gfss2f4rrpppk5v4v92qmdq2") (f (quote (("std") ("default" "std"))))))

