(define-module (crates-io im pl impl-converter-helper) #:use-module (crates-io))

(define-public crate-impl-converter-helper-0.1.0 (c (n "impl-converter-helper") (v "0.1.0") (d (list (d (n "warned") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "0958v5v3qpn1m05gdmxsdpvmlcl2bv13g2wl10klm8ac2knla2fg")))

(define-public crate-impl-converter-helper-0.1.1 (c (n "impl-converter-helper") (v "0.1.1") (d (list (d (n "warned") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "0ibh1x5v6qslfyczc10ii8wnsx0x0f2a66mdpkig8kjika0y16gh")))

(define-public crate-impl-converter-helper-0.1.2 (c (n "impl-converter-helper") (v "0.1.2") (d (list (d (n "warned") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "1l57flr5pyihxkinr4qf77d6zpxjqwyax5wkpr8i5mrggivbmzwb")))

