(define-module (crates-io im pl impl-vol) #:use-module (crates-io))

(define-public crate-impl-vol-0.0.1 (c (n "impl-vol") (v "0.0.1") (h "1gi871n3xdywwrrdbphrdzymxd4pg53jzppc2xp97jagvxx8l691") (y #t)))

(define-public crate-impl-vol-0.0.3 (c (n "impl-vol") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fl09ddfz6vm2nkyh3ndfwmvjddcr0p6f224jaygpfasdlavahaw") (f (quote (("default" "ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT" "ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT") ("ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("DO_NOT_OPTIMISE_NORMALISED_BLACK_IN_REGIONS_3_AND_4_FOR_CODYS_FUNCTIONS")))) (y #t)))

(define-public crate-impl-vol-0.0.4 (c (n "impl-vol") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q6p9afyin204z8y0402x6x3kifpmr3z7fnqpmfcpf7pha6saaap") (f (quote (("default" "ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT" "ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT") ("ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("DO_NOT_OPTIMISE_NORMALISED_BLACK_IN_REGIONS_3_AND_4_FOR_CODYS_FUNCTIONS")))) (y #t)))

(define-public crate-impl-vol-0.0.5 (c (n "impl-vol") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rp4laggpbj6vcan22rh4rw1psgi9d247pzhrabpd9whrap779jx") (f (quote (("default" "ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT" "ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT") ("ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("DO_NOT_OPTIMISE_NORMALISED_BLACK_IN_REGIONS_3_AND_4_FOR_CODYS_FUNCTIONS")))) (y #t)))

(define-public crate-impl-vol-0.0.6 (c (n "impl-vol") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1x4blc30jp8wma6213v77yxjxj8n2k2qnw9fg2cwvrqsgmj3pcd8") (f (quote (("default" "ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT" "ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT") ("ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("DO_NOT_OPTIMISE_NORMALISED_BLACK_IN_REGIONS_3_AND_4_FOR_CODYS_FUNCTIONS")))) (y #t)))

(define-public crate-impl-vol-0.0.7 (c (n "impl-vol") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j3rbkv42spc1i2dr2rarlv3isgq6c3a0ggj3yq1zfnd4jn4sf84") (f (quote (("default" "ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT" "ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("ENABLE_SWITCHING_THE_OUTPUT_TO_ITERATION_COUNT") ("ENABLE_CHANGING_THE_HOUSEHOLDER_METHOD_ORDER") ("DO_NOT_OPTIMISE_NORMALISED_BLACK_IN_REGIONS_3_AND_4_FOR_CODYS_FUNCTIONS")))) (y #t)))

(define-public crate-impl-vol-0.0.8 (c (n "impl-vol") (v "0.0.8") (h "0yhimk82j44ascy5qmgkl5ckqq6m4f20l83h07j0ylacqz18kxp3") (y #t)))

(define-public crate-impl-vol-0.0.9 (c (n "impl-vol") (v "0.0.9") (h "0y3aww3bbfjlsli3a6iwjfsjcxdxyb08dbvs4w0l7f4i06xjnh56") (y #t)))

