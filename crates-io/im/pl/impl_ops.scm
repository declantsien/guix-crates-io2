(define-module (crates-io im pl impl_ops) #:use-module (crates-io))

(define-public crate-impl_ops-0.1.0 (c (n "impl_ops") (v "0.1.0") (h "0b2q3b8afz6di1mkbp1za4vdc6fixn1wa3idr6syra2p0wssfwzp")))

(define-public crate-impl_ops-0.1.1 (c (n "impl_ops") (v "0.1.1") (h "1krdy8l11i810az9ahf74awj23wk0064l3x8gazcyg6x71gpmych")))

