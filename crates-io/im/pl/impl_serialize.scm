(define-module (crates-io im pl impl_serialize) #:use-module (crates-io))

(define-public crate-impl_serialize-1.0.0 (c (n "impl_serialize") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1f8qbd9kl2biz07njyclbxb6qm37833zrvxadf9zk4bmvyqx0j9z") (y #t)))

(define-public crate-impl_serialize-2.0.0 (c (n "impl_serialize") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1a726dh597245lp531hanj00f0xdiz1603dyk81y175q2a62vam5")))

(define-public crate-impl_serialize-2.0.1 (c (n "impl_serialize") (v "2.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1h1vrk6hr1dvhi6jnimzmppq7kc14bpcd3ag9mqrlfm62yv6fbdn")))

(define-public crate-impl_serialize-3.0.0 (c (n "impl_serialize") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "unhygienic2") (r "^0.1.0") (d #t) (k 0)))) (h "00z0b8cinjz3d91rz3jv1hd4hms5glvk0h6laylwhav8a65q24jq")))

(define-public crate-impl_serialize-3.1.0 (c (n "impl_serialize") (v "3.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "unhygienic2") (r "^0.1.0") (d #t) (k 0)))) (h "0xqdkbpq9vnxnph1k6fsl7xjm7h5xv9s5kdn0zgjl1jkgm6if46b")))

(define-public crate-impl_serialize-3.1.2 (c (n "impl_serialize") (v "3.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "unhygienic2") (r "^0.1.0") (d #t) (k 0)))) (h "0d0rs3nivblfcjlsd69i8v3c1f6y85w1w458gg67axv15s68c15h")))

(define-public crate-impl_serialize-3.1.3 (c (n "impl_serialize") (v "3.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "unhygienic2") (r "^0.1.0") (d #t) (k 0)))) (h "0khdy2ms47nckm6a7a1q2plgcz916dj0yfdfbmkdklk7k9a07dry")))

(define-public crate-impl_serialize-3.1.4 (c (n "impl_serialize") (v "3.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "unhygienic2") (r "^0.1.0") (d #t) (k 0)))) (h "0q0czhdrmb7szm0scj9xgfdg67lxjap1d667mg79nq8nm44y8sjd")))

