(define-module (crates-io im pl impl_table) #:use-module (crates-io))

(define-public crate-impl_table-0.1.0 (c (n "impl_table") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)))) (h "1x69k5a8qy7483i2lfq7v07gxf93dv5z6jkwr571fvzdjh94rg83")))

(define-public crate-impl_table-0.1.1 (c (n "impl_table") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0g46r77gjja4hl9577kz2hfdd1yzv19035k01ak4fzb7s0capp9w")))

(define-public crate-impl_table-0.1.2 (c (n "impl_table") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (f (quote ("chrono"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07ak4dddr0n7mcsipfz2mc7mifv48gmfxgw3zpnry9rkk1kga3b5")))

(define-public crate-impl_table-0.1.3 (c (n "impl_table") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (f (quote ("chrono"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0h78r0jz4m61cy64abdvfjq885mz7sv843hnrv5cqbcl0rw1ifbm")))

