(define-module (crates-io im pl impls_index_meta) #:use-module (crates-io))

(define-public crate-impls_index_meta-0.1.0 (c (n "impls_index_meta") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)) (d (n "werror") (r "~0.1") (d #t) (k 0)))) (h "0b4jgf4b4pf9k9bzf16v2a9fd6c2ky7gj0lr08a4d7hj7ymjrzz3")))

(define-public crate-impls_index_meta-0.1.1 (c (n "impls_index_meta") (v "0.1.1") (d (list (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)))) (h "1pm0681kwy2jnx6xmavys69p56f42dblmhlj77vaqyyvdw5b1c46") (f (quote (("full") ("default"))))))

(define-public crate-impls_index_meta-0.2.0 (c (n "impls_index_meta") (v "0.2.0") (d (list (d (n "macro_tools") (r "^0.2.0") (k 0)))) (h "1p6165jyivj87l094fpmff2fp7vc1ak9ysln6l4jkzjnki8l2mn7") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-impls_index_meta-0.3.0 (c (n "impls_index_meta") (v "0.3.0") (d (list (d (n "macro_tools") (r "~0.4.0") (k 0)))) (h "18bhmcniaa7fpyxlxs1c20pghb2629w2p9gbllxldkr4gizw4333") (f (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-impls_index_meta-0.4.0 (c (n "impls_index_meta") (v "0.4.0") (d (list (d (n "macro_tools") (r "~0.19.0") (k 0)))) (h "108vqxqz4w7nbinjlni14ngd01c3vwd1yl9h57zw4nfc303848zr") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-impls_index_meta-0.5.0 (c (n "impls_index_meta") (v "0.5.0") (d (list (d (n "macro_tools") (r "~0.20.0") (k 0)))) (h "1qnz3xkdrcjwkmv8ippi1pnw1dfsx8lcnas4wzr531vdmandjllm") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-impls_index_meta-0.6.0 (c (n "impls_index_meta") (v "0.6.0") (d (list (d (n "macro_tools") (r "~0.21.0") (k 0)))) (h "1m25xx2an67q2nins4mrddn7v9n87dndb2crlswdyj4ld5x1cbvy") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

(define-public crate-impls_index_meta-0.7.0 (c (n "impls_index_meta") (v "0.7.0") (d (list (d (n "macro_tools") (r "~0.24.0") (k 0)))) (h "1li4xzz1h4illjcv4yjnqiv3bdw283bqyhfwp7sbhymdk91s91v7") (f (quote (("full" "enabled") ("enabled" "macro_tools/enabled") ("default" "enabled"))))))

