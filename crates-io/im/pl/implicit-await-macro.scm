(define-module (crates-io im pl implicit-await-macro) #:use-module (crates-io))

(define-public crate-implicit-await-macro-0.1.0 (c (n "implicit-await-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0a534r0v95vcvjlj3a8xkg9k4yl5h2z5d5l5pahx4989wq4sli6a")))

