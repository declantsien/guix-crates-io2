(define-module (crates-io im pl impl-trait-for-tuples) #:use-module (crates-io))

(define-public crate-impl-trait-for-tuples-0.1.0 (c (n "impl-trait-for-tuples") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "1krl031yw1aghz01dc5r1cgg4nj31i92xq87s179fgxa2nyi74is")))

(define-public crate-impl-trait-for-tuples-0.1.1 (c (n "impl-trait-for-tuples") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "12mcn98m3h7cm2803frikg8983ybykaqh1yqqdiyh21hn56g83bv")))

(define-public crate-impl-trait-for-tuples-0.1.2 (c (n "impl-trait-for-tuples") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "13s27rwzp0rg0zzpg907lbhhpy5xm9mslsmv77s4i28gg5rb6iv9")))

(define-public crate-impl-trait-for-tuples-0.1.3 (c (n "impl-trait-for-tuples") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "178dj87q4hk2mbkmm25gc102hsh5c74d82grf470lx738855bxby")))

(define-public crate-impl-trait-for-tuples-0.2.0 (c (n "impl-trait-for-tuples") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "00s9adswsvk3v0hijzdarhf8gjjhwllv336kp3darvjgyznahrbg")))

(define-public crate-impl-trait-for-tuples-0.2.1 (c (n "impl-trait-for-tuples") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "1vii634v1zvb680h28md42xpdrj1j1d50ix3dga95fxkql8cpnnm")))

(define-public crate-impl-trait-for-tuples-0.2.2 (c (n "impl-trait-for-tuples") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1swmfdzfcfhnyvpm8irr5pvq8vpf8wfbdj91g6jzww8b6gvakmqi")))

