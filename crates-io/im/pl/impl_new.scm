(define-module (crates-io im pl impl_new) #:use-module (crates-io))

(define-public crate-impl_new-0.1.0 (c (n "impl_new") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gza555n46acz36yv2qggnz1fgvdv0hga26pvi0bbdk98msab01j") (y #t) (r "1.56.1")))

(define-public crate-impl_new-0.1.1 (c (n "impl_new") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d3hql2z6n97f6mg024nzm2ik27f47nl2k0hjf2ilwzy35hp5ic4") (y #t) (r "1.56.1")))

(define-public crate-impl_new-0.2.0 (c (n "impl_new") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "15pc4zhsnid03xqmckdvcv3czfsy6gggc27bq8jpm2hr3kv9jn5y") (y #t) (r "1.56.1")))

(define-public crate-impl_new-0.2.1 (c (n "impl_new") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yhq270iiks6nhmm4v7kvhrcs3vd9vi5w7kzgdg7fqn0nxxkcjh2") (y #t) (r "1.56.1")))

(define-public crate-impl_new-0.2.2 (c (n "impl_new") (v "0.2.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hl4kb9vasmnpbw4zlgzr5jbrplq78lps5lxzm42gk2piq2bsv9h") (y #t) (r "1.56.1")))

