(define-module (crates-io im pl implied-vol) #:use-module (crates-io))

(define-public crate-implied-vol-0.1.0 (c (n "implied-vol") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13a44wdrnzy72j7ncj5ikiy1yk3jnhs6i1n99hfs3rn1dbzywwhi") (y #t)))

(define-public crate-implied-vol-0.2.0 (c (n "implied-vol") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "080sy7k52v9q48hlhdrfwkvzm60fjy0jm0sbw7mpym9z3ih3kl7d") (y #t)))

(define-public crate-implied-vol-0.2.1 (c (n "implied-vol") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0yl0ysxizs3bl6jxh9pvjias9rgadn6asi0d1ybvb9xqv4d596jw")))

