(define-module (crates-io im pl implicit-clone) #:use-module (crates-io))

(define-public crate-implicit-clone-0.1.0 (c (n "implicit-clone") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "1rxnpwpk6n9azy8blmcjna42qbirlmwbvbjzs06x24xpn73v38zv") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.1.1 (c (n "implicit-clone") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "0k8mi28h6ip5wvf2fix33iaf9smf57d30ywfhyl8wpnzafdd7ls5") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.1.2 (c (n "implicit-clone") (v "0.1.2") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "1pbbn0yy2imaln8i7yayrzfhpc9argyawm06dwjz2avsd6fmppjb") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.1.3 (c (n "implicit-clone") (v "0.1.3") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "0hdhl1dvdnkcn5v74nd8c3dy7k42dswqpkxw5fvx4v9wvigpcpzp") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.2.0 (c (n "implicit-clone") (v "0.2.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "1m5nm8dnsdwmsdgdmv711gs81iqza8mhdgv367w4fipys2szd3rg") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.3.0 (c (n "implicit-clone") (v "0.3.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "035dn90z80byx968l14rvsgr18n3f47idgaz5dx3av5y5d2qbykm") (f (quote (("map" "indexmap")))) (y #t)))

(define-public crate-implicit-clone-0.3.1 (c (n "implicit-clone") (v "0.3.1") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "0fkarfl1y9dzzcfjmp8kvisd4rh5g1vv7ym5hww1s38xxxv1nixh") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.3.2 (c (n "implicit-clone") (v "0.3.2") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "0xq65wib96y8qslg3aisx3yz2dl952syvn5b8hcl4zchscqfcdx9") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.3.3 (c (n "implicit-clone") (v "0.3.3") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "17vizia4ik6wqgjdydwwqihkmyb0w2h2ncavpjz6389z4k1x1lwf") (f (quote (("map" "indexmap")))) (y #t)))

(define-public crate-implicit-clone-0.3.4 (c (n "implicit-clone") (v "0.3.4") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "1r6plhfhvm23vfsgk34nh30czy646a77ms2jqkmik58g84mv3734") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.3.5 (c (n "implicit-clone") (v "0.3.5") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)))) (h "1xwf6vj07y8yb16hm5v31chxg8mv90g8rlaw305k4p27f0p11z20") (f (quote (("map" "indexmap"))))))

(define-public crate-implicit-clone-0.3.6 (c (n "implicit-clone") (v "0.3.6") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0pzn8rmwbs08jgblizxzfdplpksnjy3qfrvcqzrz355vhzcwnvkw") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde"))))))

(define-public crate-implicit-clone-0.3.7 (c (n "implicit-clone") (v "0.3.7") (d (list (d (n "indexmap") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "00lgsfigggpgh59030vhqz9c8zrsx9f8m732wh14572ldzdhy6cc") (f (quote (("map" "indexmap")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.3.8 (c (n "implicit-clone") (v "0.3.8") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1jlkkwwy21rq1dp830w7lhsixnx6jj5cwz616395nfns4s4dv42l") (f (quote (("map" "indexmap")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.0 (c (n "implicit-clone") (v "0.4.0") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ps124nbn6dp6i1qncc1j2dq8rqw0qazykk0qrqmx517acwlrj3k") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.1 (c (n "implicit-clone") (v "0.4.1") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1l9d5fp8g6758sk8rp7vvcn1m55mv4pdbs2rn5yrxz8p1007fgdg") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.2 (c (n "implicit-clone") (v "0.4.2") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0awaf54358x7r717m5jmysk20rpdsmw8n27zqg1k6dr94qv3mm5d") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.3 (c (n "implicit-clone") (v "0.4.3") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0flw6gc8ddsimr96zp1qb2l2hzlh9hcv316035fnngikby4lmh8v") (f (quote (("map" "indexmap")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.4 (c (n "implicit-clone") (v "0.4.4") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1xajh6kdn76r529ri593lf5iqd986c2v23jh7y3yc425jcr6r65j") (f (quote (("map" "indexmap")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.5 (c (n "implicit-clone") (v "0.4.5") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "17pgnd1g3x7gdbr3kxhx9j46wyjwyvy9vjyc3y5r0xi2kpa576kq") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.6 (c (n "implicit-clone") (v "0.4.6") (d (list (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1p8rjf38xyk5mk0wwj88zf1lljsb8y7ai5zn8rwqwxxayvbgwl3s") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.7 (c (n "implicit-clone") (v "0.4.7") (d (list (d (n "implicit-clone-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "099cr9xkbmrm6a5lm01fzsczvfbyqcy2mgzq9q9bz6jghs549i8n") (f (quote (("map" "indexmap") ("derive" "implicit-clone-derive") ("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.4.8 (c (n "implicit-clone") (v "0.4.8") (d (list (d (n "implicit-clone-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1fxrp5vga78i40v444ga2v4nkib8ii8mmh4rwcmaa0plrdas41pw") (f (quote (("map" "indexmap") ("derive" "implicit-clone-derive") ("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

(define-public crate-implicit-clone-0.3.9 (c (n "implicit-clone") (v "0.3.9") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lxzpz48392nwyf47dqlsj4n3q61zskfziyafd3v5k1hghg21mng") (f (quote (("map" "indexmap")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde"))))))

(define-public crate-implicit-clone-0.4.9 (c (n "implicit-clone") (v "0.4.9") (d (list (d (n "implicit-clone-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "indexmap") (r ">=1, <=2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "113agd9bqk7c0s2rqgarzkfp3wgbzl3q59mp6sv72nkv3iwsmagq") (f (quote (("map" "indexmap") ("derive" "implicit-clone-derive") ("default" "derive")))) (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde")))) (r "1.64")))

