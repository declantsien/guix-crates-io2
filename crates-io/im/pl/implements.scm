(define-module (crates-io im pl implements) #:use-module (crates-io))

(define-public crate-implements-0.0.0 (c (n "implements") (v "0.0.0") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "03xr5cjhvd4q3a8qgyfsirgz5abkwaafxn7bqrjinxrf0czm2fps")))

(define-public crate-implements-0.0.1 (c (n "implements") (v "0.0.1") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1vra3w9i6g8hfryd008bm72dq5ajhhdiy40qj6jdd963g868a91a")))

(define-public crate-implements-0.0.2 (c (n "implements") (v "0.0.2") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1vy8xrnv79ffd6giivkxydgrfq4vgniv7z2ddfzgiamr7w2y0zkh")))

(define-public crate-implements-0.0.3 (c (n "implements") (v "0.0.3") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "16yyznb6hriv9fq3lswndzi17xmmnl2gd8gf5nj3dkslnr2511lv")))

(define-public crate-implements-0.0.4 (c (n "implements") (v "0.0.4") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "160r0nr2ygvvl45lp98jqpy8yv6cl7d6ki2kach5a1hfkggfxnwp")))

(define-public crate-implements-0.0.5 (c (n "implements") (v "0.0.5") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1bmfxckr1fj9r2xfyr7wk2cmwila57galpmfh9p9wvk6mr5pws0b")))

(define-public crate-implements-0.0.6 (c (n "implements") (v "0.0.6") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1mqk9ikn6dl9a54n9wms0cvzrcdcqqvqfvfs4rz58yrq770krmjx")))

(define-public crate-implements-0.0.7 (c (n "implements") (v "0.0.7") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1q8y5iz0k8v803ymn3y6495vrngz91npik1b00rqr2m02mqsjxhx")))

(define-public crate-implements-0.1.0 (c (n "implements") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "19byl1y6j0sf47p2zjsvya9b8qqlifmvsalwcxjgkv7hsk84niy6")))

(define-public crate-implements-0.1.1 (c (n "implements") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1hdxr4ynssn36spaab3ic603qy8x5z81kc12iaq6h7z8zgkdcp2v")))

(define-public crate-implements-0.2.0 (c (n "implements") (v "0.2.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "1pkwhjlly7960cs8gl0h787ysv1zy08iwg3zqqw8m0c4lhrbddgk") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.3.0 (c (n "implements") (v "0.3.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "1hd4l8yaipq77zaxg7713b3z7xwskr5ancws9ra45zl7yrdx3473") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.4.0 (c (n "implements") (v "0.4.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "08y60rk0808yzrq67i7avc06qk6c8ljpsx9pi8jr3c82hm5h13x7") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.5.0 (c (n "implements") (v "0.5.0") (d (list (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "1hxnf8sv2pwyw98dwyc4vzh8s2idrqng4697q530g7mc0skl58lk") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.6.0 (c (n "implements") (v "0.6.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "17mkr07kwg48z68k9nx46abzrzrxh83617njba3cwbk85bqz6sgr") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.7.0 (c (n "implements") (v "0.7.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1i8hll4x69hhcb34a4cswcb523102ainjglj0gq8aqsr0n6hpqxm") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-implements-0.8.0 (c (n "implements") (v "0.8.0") (d (list (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "04ywrbnam3dzkkw1dmy3njbvd03hp4fpwrnlkz8rc2sl6qvfmn5i") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

