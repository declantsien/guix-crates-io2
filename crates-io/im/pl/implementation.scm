(define-module (crates-io im pl implementation) #:use-module (crates-io))

(define-public crate-implementation-0.1.0 (c (n "implementation") (v "0.1.0") (h "19mr7a0ln29l72hwj9p09lxgd1n772hicf4vbkbfmskr4jh3nc5z")))

(define-public crate-implementation-0.1.1 (c (n "implementation") (v "0.1.1") (h "0mfzfl6bhnhilwci947rxvlyx4g32wzx28x7nqzgbhcs0ra15yq9")))

(define-public crate-implementation-0.1.2 (c (n "implementation") (v "0.1.2") (h "0mwi5mz48jpdcr0sg40w86wmxq4s7lrmb2qb655qy5b9mxrmqbrn")))

(define-public crate-implementation-0.1.3 (c (n "implementation") (v "0.1.3") (h "14fvx1baarssbvxnv86xhpsfssgsdw72bmxxn0pdl74ays65pkgr")))

(define-public crate-implementation-0.1.4 (c (n "implementation") (v "0.1.4") (h "16x0dyb60j9jwpqscghnzc8q4xch8qc78gg6wg2h529g9q85ss7s")))

