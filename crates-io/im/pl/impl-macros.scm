(define-module (crates-io im pl impl-macros) #:use-module (crates-io))

(define-public crate-impl-macros-0.1.0 (c (n "impl-macros") (v "0.1.0") (h "0klpsqaga50b50cjlial99vb1avg915v6laqxgc58w56dqqcqxbs") (f (quote (("std") ("default" "std"))))))

(define-public crate-impl-macros-0.1.1 (c (n "impl-macros") (v "0.1.1") (h "1sywcji13drs58nifqpdn8glqxkf1g1kar3afgmzmlnw4s3ivhs0") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

