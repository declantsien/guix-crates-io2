(define-module (crates-io im pl implode) #:use-module (crates-io))

(define-public crate-implode-0.1.0 (c (n "implode") (v "0.1.0") (h "0a0yw8w75gsipf5ynwk9yhpm2fdllrqnd0pyym7n3d06v4c3d64w")))

(define-public crate-implode-0.1.1 (c (n "implode") (v "0.1.1") (h "0539nm46midvr7ildf792y3992cc17qdrsq98053a32hx09fssmy")))

