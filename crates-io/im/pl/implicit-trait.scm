(define-module (crates-io im pl implicit-trait) #:use-module (crates-io))

(define-public crate-implicit-trait-0.1.0 (c (n "implicit-trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "1a249zw72c45s1pzzxcrr45ivscpzm5yjjnazgzrqqdlvvp8cv1s")))

(define-public crate-implicit-trait-0.2.0 (c (n "implicit-trait") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "0kklbvb6h4aig4la5hs0mcgw25wwxqh1ik99pyxq4qs32q7gyvf7")))

(define-public crate-implicit-trait-0.2.1 (c (n "implicit-trait") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "1qf6cghxbdkx9b39gi25p14mspkamj6614k6j7kmmc854d01ssll")))

(define-public crate-implicit-trait-0.2.2 (c (n "implicit-trait") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "17ky9din2i77hmgqn3bx0nw6s13n1gzh5bz65py7zvr8j6dvyl60")))

(define-public crate-implicit-trait-0.3.0 (c (n "implicit-trait") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.77") (d #t) (k 2)))) (h "0jfmj4jar60qfapzw8r915ncx2s9q31z7rb5mcqkfgq7z70zbvh5")))

