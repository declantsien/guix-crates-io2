(define-module (crates-io im pl impl-codec) #:use-module (crates-io))

(define-public crate-impl-codec-0.1.0 (c (n "impl-codec") (v "0.1.0") (d (list (d (n "parity-codec") (r "^2.1") (k 0)))) (h "08kx0960p8a50zjrajdjrjilz6pwr4xvsslz96vaj8x7xla4is1r") (f (quote (("std" "parity-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.1.1 (c (n "impl-codec") (v "0.1.1") (d (list (d (n "parity-codec") (r "^2.1") (k 0)))) (h "1wkrdhh9yyvp7vx8c0b22c0av276z2wpzk9hxl7cb4c2ha6md24w") (f (quote (("std" "parity-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.2.0 (c (n "impl-codec") (v "0.2.0") (d (list (d (n "parity-codec") (r "^3.0") (k 0)))) (h "0fvrkz2761ny183mhj1j2wqpj1w9rada0nrbdgifmyrr6s10s1fj") (f (quote (("std" "parity-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.3.0 (c (n "impl-codec") (v "0.3.0") (d (list (d (n "parity-codec") (r "^4.0") (k 0)))) (h "0wakqp85adn8338f7993bcywgbi965nvj05893c6v4dwczr8zvb2") (f (quote (("std" "parity-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.4.0 (c (n "impl-codec") (v "0.4.0") (d (list (d (n "parity-scale-codec") (r "^1.0.3") (k 0)))) (h "07q35wara3f4ka1hfxrmvasjkgbv5d46vrv1f504pqmmsarl3i3q") (f (quote (("std" "parity-scale-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.4.1 (c (n "impl-codec") (v "0.4.1") (d (list (d (n "parity-scale-codec") (r "^1.0.3") (k 0)))) (h "1lr1dw4w7a8c7k2z1si1fbwy4ybdxc2pnbmkagym0k2ja5i0i81z") (f (quote (("std" "parity-scale-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.4.2 (c (n "impl-codec") (v "0.4.2") (d (list (d (n "parity-scale-codec") (r "^1.0.6") (k 0)))) (h "0lxzqr0mda4mbz31q8i2m4d5ch8083ck5mgslan0wyq63f91mr8v") (f (quote (("std" "parity-scale-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.5.0 (c (n "impl-codec") (v "0.5.0") (d (list (d (n "parity-scale-codec") (r "^2.0.0") (k 0)))) (h "1vbpm2kmxla0zflaip3vfgj4hxqhcz6fsy7ynxfdvsws6px0w5yz") (f (quote (("std" "parity-scale-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.5.1 (c (n "impl-codec") (v "0.5.1") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("max-encoded-len"))) (k 0)))) (h "0hy4svffnw9idy9ipp0hkmbzk97fl583akqwyqmvbqy8qgzbs7hn") (f (quote (("std" "parity-scale-codec/std") ("default" "std"))))))

(define-public crate-impl-codec-0.6.0 (c (n "impl-codec") (v "0.6.0") (d (list (d (n "parity-scale-codec") (r "^3.0.0") (f (quote ("max-encoded-len"))) (k 0)))) (h "0bvzlxap996zrai9shwcnzris117r1gx2dizgxhiark27402fsms") (f (quote (("std" "parity-scale-codec/std") ("default" "std")))) (r "1.56.1")))

