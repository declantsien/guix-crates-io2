(define-module (crates-io im pl impl_inheritance) #:use-module (crates-io))

(define-public crate-impl_inheritance-0.0.1 (c (n "impl_inheritance") (v "0.0.1") (d (list (d (n "impl_inheritance_macros") (r "^0.0.1") (d #t) (k 0)))) (h "031fl5kb03bz1ml6401bfp1wm1dw1j5d8midymrzk7zw1fk95n4z")))

(define-public crate-impl_inheritance-0.0.2 (c (n "impl_inheritance") (v "0.0.2") (d (list (d (n "impl_inheritance_macros") (r "^0.0.2") (d #t) (k 0)))) (h "16mm27k29mnd1wb6qgzpf1681dzz9pfkphgpvi5ibznlc65gna4v")))

