(define-module (crates-io im pl impl-tools) #:use-module (crates-io))

(define-public crate-impl-tools-0.1.0 (c (n "impl-tools") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0adv3m6x5f6n1zbljignmdbrmvnpjf47nkl0zm0m6znr6llgm15v")))

(define-public crate-impl-tools-0.2.0 (c (n "impl-tools") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1r1j4jrw6w4chw7qncm9b7y929qh7k1yzi5sczzc6pmqwl37941w")))

(define-public crate-impl-tools-0.3.0 (c (n "impl-tools") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1s0lw5caas1bi549h4n8yj2hsf7z521jia2gdyxmvz2hnc6hwk82")))

(define-public crate-impl-tools-0.3.1 (c (n "impl-tools") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0as2bdfgjvdkagmz3fyxqvriny4ldk8rcm9zhjbw4cff76p52a16")))

(define-public crate-impl-tools-0.3.2 (c (n "impl-tools") (v "0.3.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "008bf1x121v6dqwdjvw87bqgaw6djh6ja0pkbh58jym6wnli4hg6")))

(define-public crate-impl-tools-0.4.0 (c (n "impl-tools") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "022k97s2fnbyp4jnlmhb31yhgm2fixi9k96z4vpc57x6sa2xbbdb")))

(define-public crate-impl-tools-0.4.1 (c (n "impl-tools") (v "0.4.1") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0539i7xqbn2w7bc09fh683m4spcr2srvc5dsrzhg9j6yrqhzjw7n")))

(define-public crate-impl-tools-0.4.2 (c (n "impl-tools") (v "0.4.2") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0za7ix9h14jd93j6ypl4mvvf43787ff7hvwp0pjijqa7sggkk50j")))

(define-public crate-impl-tools-0.4.3 (c (n "impl-tools") (v "0.4.3") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "13a2sd5pync990ckz361gjrc86pidpjvslq66vyybqcxfvqil5wa")))

(define-public crate-impl-tools-0.4.4 (c (n "impl-tools") (v "0.4.4") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "13k5456zkcbqv1awfapvqgvq4fcn0wxpqyvxj4apv0p9r0hfadvl")))

(define-public crate-impl-tools-0.5.0 (c (n "impl-tools") (v "0.5.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0p667cmlymws70wvmb3sdh4rny06d7n9sapy1jljhywvys7ykmqi")))

(define-public crate-impl-tools-0.5.1 (c (n "impl-tools") (v "0.5.1") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "145sqmaqfczdvfg6qwkdds28hnzr9pdrv6781nm7b51qysgcvc9v")))

(define-public crate-impl-tools-0.5.2 (c (n "impl-tools") (v "0.5.2") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0sa1vxm11r080qzcbp1sns662jg67hwx386887gdzixkcb407i7v")))

(define-public crate-impl-tools-0.6.0 (c (n "impl-tools") (v "0.6.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "1g9nilg9v9jxhlvkmnj8yfhhsillxys0ayw2c2qa31657nfkzn4h")))

(define-public crate-impl-tools-0.6.1 (c (n "impl-tools") (v "0.6.1") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0hpqxdxkhc0gbab63p17vv8vybm3h7zpg27378y83mgk2fz6xmd4")))

(define-public crate-impl-tools-0.6.2 (c (n "impl-tools") (v "0.6.2") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0jjh3wc2gv3hsy3pavgw51rhbf38jxck5q8xizxp1h23w0i992xk")))

(define-public crate-impl-tools-0.8.0 (c (n "impl-tools") (v "0.8.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "19vdq1v906cc79gqz1a3j39d91l1lpm1fwxx6a4z4fjnzb07bpib")))

(define-public crate-impl-tools-0.9.0 (c (n "impl-tools") (v "0.9.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "1dq2bazr5a9mbdg0sz3f4g5fcyfxmxv6qn7d0il2i679aiyncjqp")))

(define-public crate-impl-tools-0.9.1 (c (n "impl-tools") (v "0.9.1") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0qig701fg5myz8rai5jwmxi9gcvwj3al9qqjc3lr6pncs8cxhb6k")))

(define-public crate-impl-tools-0.10.0 (c (n "impl-tools") (v "0.10.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "impl-tools-lib") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 2)))) (h "0p4nmna2ia70syyk02606ryva2p5i33q6a16vagskwc121dk0b6q")))

