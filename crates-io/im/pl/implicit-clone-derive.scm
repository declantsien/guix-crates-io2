(define-module (crates-io im pl implicit-clone-derive) #:use-module (crates-io))

(define-public crate-implicit-clone-derive-0.1.0 (c (n "implicit-clone-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0w6d13rj5yzhcqjxnq6rqz5iv1jvkh8a2gb2ncdhr7ni0axf8jl2") (r "1.64")))

(define-public crate-implicit-clone-derive-0.1.1 (c (n "implicit-clone-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0fsfj6n56mg92f3899gcdck1dqlsmgyd52k0n2xhhj53p5g6h4ck") (r "1.64")))

