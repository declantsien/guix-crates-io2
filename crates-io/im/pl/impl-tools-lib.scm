(define-module (crates-io im pl impl-tools-lib) #:use-module (crates-io))

(define-public crate-impl-tools-lib-0.3.0 (c (n "impl-tools-lib") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1731np5gkc5dl30gwk57xg419p5xi4zr7ks16ij00hs9mcjppxz5")))

(define-public crate-impl-tools-lib-0.4.0 (c (n "impl-tools-lib") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0hz070icnj6viaw6fiyv5m9ywk20z965a99bq63gvai3k9ikff1a")))

(define-public crate-impl-tools-lib-0.4.2 (c (n "impl-tools-lib") (v "0.4.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xj04vxxci2kxxbllpb29knvv9xr47fzknsb2w3p84jvh56x5p9m")))

(define-public crate-impl-tools-lib-0.4.3 (c (n "impl-tools-lib") (v "0.4.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dqyabixpl15278ss49g8a679yya3k57ddgmjswrj1icjq2b1bmv")))

(define-public crate-impl-tools-lib-0.4.4 (c (n "impl-tools-lib") (v "0.4.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1imv67s5z6lvrxpc63r377khwf68w9gjssxhjqvcb4d7321c6ryi")))

(define-public crate-impl-tools-lib-0.5.0 (c (n "impl-tools-lib") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bsvgkma23b64arjqb57fypwc3y9gd1jxfj88prisvv4lilzvbcq")))

(define-public crate-impl-tools-lib-0.5.1 (c (n "impl-tools-lib") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0q1bk5vmazcki6q67pbj2jpg9mlc9a0ivkv6szizdcn69wbgcizl")))

(define-public crate-impl-tools-lib-0.5.2 (c (n "impl-tools-lib") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1vsk7811p6xraiax9pv9sv5ky5hcca88pninrggdv4ipp22zvghv")))

(define-public crate-impl-tools-lib-0.6.0 (c (n "impl-tools-lib") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "10z7h89syp1y8r5rssbhwm1qly1519ihdynwiwcbqhlar0vlim9v")))

(define-public crate-impl-tools-lib-0.7.0 (c (n "impl-tools-lib") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0a83mzls2lxhmnsxqj2kvfy2m5p58cz3bm7isiciq47wjax1d4fh")))

(define-public crate-impl-tools-lib-0.7.1 (c (n "impl-tools-lib") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1h50fm58my1297yrbf4ykmfsipi6l78nxlcr3vzara0nkzvwfwnq")))

(define-public crate-impl-tools-lib-0.8.0 (c (n "impl-tools-lib") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0yx7f73jpyv1rljx890vaax3zxh4aybbqwfzyjc8g8gspnqg7zag")))

(define-public crate-impl-tools-lib-0.9.0 (c (n "impl-tools-lib") (v "0.9.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "08z0dq2fvrdn4b6ppk0zp5n1a040p5azlll4mz7w7qgc4q4qikpz")))

(define-public crate-impl-tools-lib-0.9.1 (c (n "impl-tools-lib") (v "0.9.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0yqbrbv5j57n8w5bhh22spmwrnjwliky97ak1mva5d5wjgpzjp57")))

(define-public crate-impl-tools-lib-0.10.0 (c (n "impl-tools-lib") (v "0.10.0") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "120s6gxdkfzsrkvrvyi3698q3mffvid5iim05xqb1akfi1nr9lw5")))

