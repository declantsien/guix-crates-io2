(define-module (crates-io im pl impl-vol-sys) #:use-module (crates-io))

(define-public crate-impl-vol-sys-0.0.1 (c (n "impl-vol-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "18na7kdbasn99g7z9ifg01qadsv5blncgjma1vnxwlz4vbndk73n") (y #t)))

(define-public crate-impl-vol-sys-0.0.2 (c (n "impl-vol-sys") (v "0.0.2") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "0bghkjgqnrf0icdhd95j5svz7pa1i5gn70kawvbfxbv7z0mlbg5c") (y #t)))

(define-public crate-impl-vol-sys-0.0.3 (c (n "impl-vol-sys") (v "0.0.3") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "19c8axzwwps91rm4lc2yv7g2h0n5lap0zsx6nxdimsz9j8k0494n") (y #t)))

(define-public crate-impl-vol-sys-0.0.4 (c (n "impl-vol-sys") (v "0.0.4") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "0vjvbww58l0kkbngrllb6fv991wjhdw52kfg3fhj2f3y0xyvx2ws") (y #t)))

(define-public crate-impl-vol-sys-0.0.5 (c (n "impl-vol-sys") (v "0.0.5") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "1vzz2siq3360r8fz197n6cma571kd6505mlsgbr73yqhpdlzchg8") (y #t)))

(define-public crate-impl-vol-sys-0.0.6 (c (n "impl-vol-sys") (v "0.0.6") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)))) (h "02xn7ww8hwcm4v95a52dlbcibrqnrfvwkah6qav0rqi0d7599bfp") (y #t)))

(define-public crate-impl-vol-sys-0.9.0 (c (n "impl-vol-sys") (v "0.9.0") (d (list (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.91") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0ak9pi327vfdqdxivh3kv869q96rr5fnxp5s6q0y7ikgi1vv5y4a") (y #t)))

(define-public crate-impl-vol-sys-0.9.1 (c (n "impl-vol-sys") (v "0.9.1") (d (list (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.91") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0d2qksvnv55f7n72fxygxr2n1c3nmanlyr7zkszmj5yvgdh09rdq") (y #t)))

(define-public crate-impl-vol-sys-0.9.2 (c (n "impl-vol-sys") (v "0.9.2") (d (list (d (n "cxx") (r "^1.0.91") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.91") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0w4jpzyr6l1rv5xfn9k8ci79009a6dimbsl1g0x58hrxsbn2aazi") (y #t)))

(define-public crate-impl-vol-sys-0.9.3 (c (n "impl-vol-sys") (v "0.9.3") (d (list (d (n "cxx") (r "^1.0.94") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.94") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "0vf1dcs0wlr70hy7r529j20yifi00q8qgnql49pxmhl3wbdmbvpf") (y #t)))

(define-public crate-impl-vol-sys-0.9.4 (c (n "impl-vol-sys") (v "0.9.4") (d (list (d (n "cxx") (r "^1.0.100") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.100") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0cxpvsm9jnn31q15adjdgp0hfibrgr15i58hdqi38wwb69dc1sbg") (y #t)))

(define-public crate-impl-vol-sys-0.9.5 (c (n "impl-vol-sys") (v "0.9.5") (d (list (d (n "cxx") (r "^1.0.100") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.100") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0r101p9498bin9hy9wxfa41f7vdkcynw0cilnzjcw488hgcpc0qb") (y #t)))

