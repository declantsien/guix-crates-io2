(define-module (crates-io im pl impls_index) #:use-module (crates-io))

(define-public crate-impls_index-0.1.0 (c (n "impls_index") (v "0.1.0") (d (list (d (n "impls_index_meta") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1qfykazh7m2viyqvqiip1iph522j2vsiyi58xcdpv4s3b6979c0p")))

(define-public crate-impls_index-0.1.1 (c (n "impls_index") (v "0.1.1") (d (list (d (n "impls_index_meta") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1wl81rsb613sqhnclimp5dm4x686dsgn0f5p088s3ylwrm7aiz98")))

(define-public crate-impls_index-0.1.2 (c (n "impls_index") (v "0.1.2") (d (list (d (n "impls_index_meta") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1yql4vkn9z5x6rdr6zg2241lwxxkbwa7br23ixi3w2banz39yw4z") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

(define-public crate-impls_index-0.1.3 (c (n "impls_index") (v "0.1.3") (d (list (d (n "impls_index_meta") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "002yagsmvz1rycvcbr1ximyzcacl64irgm32vdpq3nz3v73m198z") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-impls_index-0.2.0 (c (n "impls_index") (v "0.2.0") (d (list (d (n "impls_index_meta") (r "^0.2.0") (d #t) (k 0)) (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "13aahkh897ld2w4hgca00gzrpr69h7y5abhvcszs5k2prj4s6dx1") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-impls_index-0.3.0 (c (n "impls_index") (v "0.3.0") (d (list (d (n "impls_index_meta") (r "~0.3.0") (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0y7dp41hq30bdnhri7big35zi6fqhskmrznb5470pv15syivczyj") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-impls_index-0.4.0 (c (n "impls_index") (v "0.4.0") (d (list (d (n "impls_index_meta") (r "~0.4.0") (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0adln2g1y8fzf1a413hif79fb4l4j1zw3yay5md22jhac13b57zn") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-impls_index-0.5.0 (c (n "impls_index") (v "0.5.0") (d (list (d (n "impls_index_meta") (r "~0.5.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1lwmvy3kdxr25jf5vc1vqhzcxvdn96np957hpkymdx4a11bb3s97") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "impls_index_meta/enabled") ("default" "enabled"))))))

(define-public crate-impls_index-0.6.0 (c (n "impls_index") (v "0.6.0") (d (list (d (n "impls_index_meta") (r "~0.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "07piwwd1g0ab9gcii4l21n7ar6chjxkbmkqj0x8zqbz5h9cjy904") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "impls_index_meta/enabled") ("default" "enabled"))))))

(define-public crate-impls_index-0.7.0 (c (n "impls_index") (v "0.7.0") (d (list (d (n "impls_index_meta") (r "~0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "0qm4firlgqfpcl995mgkfwvgkqdw9yn4qn4hb0mm1bwdw9ydm89c") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled" "impls_index_meta/enabled") ("default" "enabled"))))))

