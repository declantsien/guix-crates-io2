(define-module (crates-io im pl impl-rlp) #:use-module (crates-io))

(define-public crate-impl-rlp-0.1.0 (c (n "impl-rlp") (v "0.1.0") (d (list (d (n "rlp") (r "^0.3") (d #t) (k 0)))) (h "1d3f1nl9436gdj6n23l2ff44pybx9arbjirirq5ivhxqm5bwkbag")))

(define-public crate-impl-rlp-0.1.1 (c (n "impl-rlp") (v "0.1.1") (d (list (d (n "rlp") (r "^0.3") (k 0)))) (h "1zd5cn6zfsawg8lsyd7yynmij33cqqhmf1cx7ph89hijhqzh5hbc")))

(define-public crate-impl-rlp-0.2.0 (c (n "impl-rlp") (v "0.2.0") (d (list (d (n "rlp") (r "^0.4") (d #t) (k 0)))) (h "0frh06s6648k85xd3rybli76nzfngs9acc2bmv2gq4jzrxirk6zk")))

(define-public crate-impl-rlp-0.2.1 (c (n "impl-rlp") (v "0.2.1") (d (list (d (n "rlp") (r "^0.4") (k 0)))) (h "1igkn0dc78wh0ynd03rqajzqhf1ki2i0jfvfycrj7d9h33qp4ylg") (f (quote (("std" "rlp/std") ("default" "std"))))))

(define-public crate-impl-rlp-0.3.0 (c (n "impl-rlp") (v "0.3.0") (d (list (d (n "rlp") (r "^0.5") (k 0)))) (h "021869d5s47ili9kmhm9y80qpsbf0wwdap14qzfpb84pjbw210pj") (f (quote (("std" "rlp/std") ("default" "std"))))))

