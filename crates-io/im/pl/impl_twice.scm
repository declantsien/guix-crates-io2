(define-module (crates-io im pl impl_twice) #:use-module (crates-io))

(define-public crate-impl_twice-0.0.1 (c (n "impl_twice") (v "0.0.1") (h "00mjwszp0s92dqw258grgp9v3ys6rl5wvhgk55klm3nr647rzsa8")))

(define-public crate-impl_twice-0.0.2 (c (n "impl_twice") (v "0.0.2") (h "06nb920piaaq1rdwawva5fl9a6244jby7sd1gj5wvgn9qgs6wwjk")))

(define-public crate-impl_twice-0.0.3 (c (n "impl_twice") (v "0.0.3") (h "1a8xv286y6vkhx378yacz6f0b0sshj75wardgwm8k7h9sjiiy2nn")))

