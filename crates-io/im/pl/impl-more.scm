(define-module (crates-io im pl impl-more) #:use-module (crates-io))

(define-public crate-impl-more-0.0.1 (c (n "impl-more") (v "0.0.1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "06g1z7cnbs5p4241kp108kbfmyibvvcah6pfnkjfha8jl45ggi8f") (r "1.56")))

(define-public crate-impl-more-0.0.2 (c (n "impl-more") (v "0.0.2") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "04zb8j52bynh2r30vlr7gdbcln025l535nrn9sl95v607y752pab") (r "1.56")))

(define-public crate-impl-more-0.0.4 (c (n "impl-more") (v "0.0.4") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "15v5bf7yc0j3xx9fgghqynnc9lgbdgzjgjm80cmh8z3rzz0xxl2p") (r "1.56")))

(define-public crate-impl-more-0.1.0 (c (n "impl-more") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0rvnnwqqd858g8qkgcncjxkkfr9sp4zsnb9rfddcx791zzgv95y9") (r "1.56")))

(define-public crate-impl-more-0.1.1 (c (n "impl-more") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0kmxnrqkzq8lv7zjrgns64vwslhaf0pkwnqm5sk88ckpi0h44k6i") (r "1.56")))

(define-public crate-impl-more-0.1.2 (c (n "impl-more") (v "0.1.2") (h "0kgkbi04zac4984fjg9sj1vbmzhv7ipgdqbpbhb08vrvl67jnvil") (r "1.56")))

(define-public crate-impl-more-0.1.3 (c (n "impl-more") (v "0.1.3") (h "1rv5cc2qqa175k23mrjfyz2f58wpsb7fp3adz0w9r5rdpf3qnqwr") (r "1.56")))

(define-public crate-impl-more-0.1.4 (c (n "impl-more") (v "0.1.4") (h "08a7vka18abrsbnfpsxg7913jr4m2zbd3pn8mwcdrsviib5zz8m6") (y #t) (r "1.56")))

(define-public crate-impl-more-0.1.5 (c (n "impl-more") (v "0.1.5") (h "1njpbkx42bfr98hi274hxwywzvcm931jadpl3wpr8nrbj4iz75r6") (r "1.56")))

(define-public crate-impl-more-0.1.6 (c (n "impl-more") (v "0.1.6") (h "0bdv06br4p766rcgihhjwqyz8fcz31xyaq14rr53vfh3kifafv10") (r "1.56")))

