(define-module (crates-io im pl impl_variadics) #:use-module (crates-io))

(define-public crate-impl_variadics-0.1.0 (c (n "impl_variadics") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00vqmlwkm38n578bs0z386rbibmjgxjys9kfi1w25bwg3yw1fmkv")))

(define-public crate-impl_variadics-0.1.1 (c (n "impl_variadics") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b89v2i2lykq9rwgldb3v9bbrrs05nh9qs7lz1l5zz30ddlq1s26")))

(define-public crate-impl_variadics-0.1.2 (c (n "impl_variadics") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01x4sdz92h68562w9gnkgk55f45qddb80lrncr2qxfw92v5f2l8h")))

(define-public crate-impl_variadics-0.2.0 (c (n "impl_variadics") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y926xbcjxqlk0g8a4hp3vhhk22790jxclgg7x63ckpmysabb1i0") (y #t)))

(define-public crate-impl_variadics-0.3.0 (c (n "impl_variadics") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k58jk17yjlv0ky1lncrp4fl3nj2f2g9ij8sgqk50vadwl7gbc2h")))

