(define-module (crates-io im pl impl_index) #:use-module (crates-io))

(define-public crate-impl_index-1.0.0 (c (n "impl_index") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16wf0kc3r0jvvkqqjg9laav263rxacs7w4v1nc9rdzrxxgsciq9g") (r "1.60.0")))

