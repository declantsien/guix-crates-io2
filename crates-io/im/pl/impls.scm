(define-module (crates-io im pl impls) #:use-module (crates-io))

(define-public crate-impls-1.0.0 (c (n "impls") (v "1.0.0") (h "1pm69yihgnzwvallnmhvhl3s40dc162g0pg9a7af08298zmwxhmv")))

(define-public crate-impls-1.0.1 (c (n "impls") (v "1.0.1") (h "1ij073ygxxwsk4v22kax0i5bnls1f9mzw4d2qyv0scps4kc3hh2k")))

(define-public crate-impls-1.0.2 (c (n "impls") (v "1.0.2") (h "0yfj2wdazd7z2c87i0klfl8diidch8js1sff8rywaszksj1g0klb")))

(define-public crate-impls-1.0.3 (c (n "impls") (v "1.0.3") (h "1g2i16vn99kmzfaag6df9bjg3k9p2p1jc3qdm5hqhlvhpmdn8iks")))

