(define-module (crates-io im pl impl_inheritance_macros) #:use-module (crates-io))

(define-public crate-impl_inheritance_macros-0.0.1 (c (n "impl_inheritance_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0vfs0yf8yz9z8xgzsx6hmy3didsmf7zyxqpsmfgll9pw0jy6h4rz")))

(define-public crate-impl_inheritance_macros-0.0.2 (c (n "impl_inheritance_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1xfvbsffn3cn6fqxh2hpjrrdqsxsxlwp7wb80sf0dsc0l3gyd840")))

