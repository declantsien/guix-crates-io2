(define-module (crates-io im pl impl-template) #:use-module (crates-io))

(define-public crate-impl-template-1.0.0-alpha (c (n "impl-template") (v "1.0.0-alpha") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j8ajc6a6dfi814gyzvrv5cisks54r4hfcbjbs0kf915r755h1g2")))

