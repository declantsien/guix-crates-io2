(define-module (crates-io im pl impl-service) #:use-module (crates-io))

(define-public crate-impl-service-0.1.0 (c (n "impl-service") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "fold" "parsing" "printing" "extra-traits"))) (k 0)))) (h "1bal1c7nirp00nj206ncn80scyb62lxal6fsh0jz83rwv8k8i9k8")))

(define-public crate-impl-service-0.1.1 (c (n "impl-service") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "0wmxzhmmkx5hd9mk9669xx2klyiic9sn6dqcrpwckb94s2vfiys3")))

(define-public crate-impl-service-0.1.2 (c (n "impl-service") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "0a56lfy9q2akj8s2zjvqxmn279fvd436gx4665x08fh1kd3kpv4x")))

(define-public crate-impl-service-0.1.3 (c (n "impl-service") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "0d9sa6jkgvk468bgqhyam1h3p21zx8q65fg99p8rabj8648khq52")))

(define-public crate-impl-service-0.1.4 (c (n "impl-service") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "0khhhjmwp8lr593jmpkyzxnqxlj54cz8xzclzsd9pmnpj9nm09wb")))

(define-public crate-impl-service-0.1.5 (c (n "impl-service") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.9") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "08zgq8rpw5jcicpl82f73wncrx7jvhbci8n4ifrnr5ih93ynlqp9")))

(define-public crate-impl-service-0.1.6 (c (n "impl-service") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.9") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "fold" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)))) (h "00jzrg3s61ba73cn4r8znjasqwia8l5g2kna0l2js6q7djq0yrij")))

