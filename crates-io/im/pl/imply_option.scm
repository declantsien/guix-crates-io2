(define-module (crates-io im pl imply_option) #:use-module (crates-io))

(define-public crate-imply_option-0.1.0 (c (n "imply_option") (v "0.1.0") (h "1cxz2fxgic7s0wgdr99dawwfgpy42k6inimm1a0fnvsd1bav7sjk")))

(define-public crate-imply_option-0.1.1 (c (n "imply_option") (v "0.1.1") (h "1cyshl15a1gsmj69jmsy4j2a9ixr86frkk1gmssa0a3jkxppn7fa")))

(define-public crate-imply_option-0.1.2 (c (n "imply_option") (v "0.1.2") (h "1xj7z8d232j3wvas5ckd71fphf4gvs98gvnmvqsv525gxrsdkkmk")))

