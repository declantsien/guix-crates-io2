(define-module (crates-io im pl implib) #:use-module (crates-io))

(define-public crate-implib-0.0.0 (c (n "implib") (v "0.0.0") (d (list (d (n "ar") (r "^0.9.0") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "object") (r "^0.28.4") (f (quote ("pe"))) (k 0)))) (h "1g14r008k6a0yz0i9xclfqfxh1m6mfqkx82nhm2j7zz0jhk3bcsl")))

(define-public crate-implib-0.1.0 (c (n "implib") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "object") (r "^0.28.4") (f (quote ("pe"))) (k 0)))) (h "1kybw67fn657m6k5wrl3yngxb5yiv53aa2lhdawr7xk4jdbj8wph")))

(define-public crate-implib-0.2.0 (c (n "implib") (v "0.2.0") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "object") (r "^0.28.4") (f (quote ("pe"))) (k 0)))) (h "09fgkrfvj5hhncihdw4xqfapjf3p6flllnvhjybc3qzaii2akqdl")))

(define-public crate-implib-0.2.1 (c (n "implib") (v "0.2.1") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "object") (r "^0.28.4") (f (quote ("pe"))) (k 0)))) (h "1vl0wqywl0ypix01ia3i4i2ac5yyxbkgcp9fzc6bapdmgvvnim0i")))

(define-public crate-implib-0.3.0 (c (n "implib") (v "0.3.0") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "object") (r "^0.28.4") (f (quote ("pe"))) (k 0)))) (h "1ans1ba4kqh8aw6wr4dgvni2zsf9cbfac00hwh4n1hch0gvrss4f") (f (quote (("msvc") ("gnu" "object/write_std") ("default" "msvc" "gnu"))))))

(define-public crate-implib-0.3.1 (c (n "implib") (v "0.3.1") (d (list (d (n "memoffset") (r "^0.8.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("pe"))) (k 0)))) (h "1yahfnkmwgwzzhaxjqhhbnymdm2zrbbwzbq24azpq108r0v79wl5") (f (quote (("msvc") ("gnu" "object/write_std") ("default" "msvc" "gnu"))))))

(define-public crate-implib-0.3.2 (c (n "implib") (v "0.3.2") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 0)) (d (n "object") (r "^0.32.1") (f (quote ("pe"))) (k 0)))) (h "0qcjggmpf39g2ycac0b7i6j4cg59dndzd0r2h8xzjpbz1b8zk60d") (f (quote (("msvc") ("gnu" "object/write_std") ("default" "msvc" "gnu"))))))

