(define-module (crates-io im pl impl_serde_serialize_error) #:use-module (crates-io))

(define-public crate-impl_serde_serialize_error-1.0.0 (c (n "impl_serde_serialize_error") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "109r4j8makhqjqhbvzd8rj14i2q37hgi3i5vkaki8c2g7il2xxy6") (y #t)))

(define-public crate-impl_serde_serialize_error-1.0.1 (c (n "impl_serde_serialize_error") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1sc06i769mp7pxi8wvwkfw7awlchhfgls571v4n5wh4fl39smrm4") (y #t)))

(define-public crate-impl_serde_serialize_error-1.0.2 (c (n "impl_serde_serialize_error") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0dvgddpnr6qh9whb53ivggfxy4svls8ypjpmncqmg6zzr9gppgj0") (y #t)))

(define-public crate-impl_serde_serialize_error-1.0.3 (c (n "impl_serde_serialize_error") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1d3fzlsw4vli1lwignidk8is1szzcpm6za0k4vy8z9324jk1z7vm") (y #t)))

(define-public crate-impl_serde_serialize_error-1.0.4 (c (n "impl_serde_serialize_error") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0c6dg97snyr42rzsm4qsq8fvvnbxkqhqq7wjjx43zxr6xnmdccd8")))

