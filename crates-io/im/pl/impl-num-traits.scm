(define-module (crates-io im pl impl-num-traits) #:use-module (crates-io))

(define-public crate-impl-num-traits-0.1.0 (c (n "impl-num-traits") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint") (r "^0.9.0") (k 0)))) (h "0jkva5rw7bz4hm4d757jga3rm8k1h5f4hs22rgl06fsj7awi09lx")))

(define-public crate-impl-num-traits-0.1.1 (c (n "impl-num-traits") (v "0.1.1") (d (list (d (n "integer-sqrt") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint") (r "^0.9.1") (k 0)))) (h "0rjlic3z684l37qm7zywmxhhllnf982y3ilyslyxb6jiddyhbdiq")))

(define-public crate-impl-num-traits-0.1.2 (c (n "impl-num-traits") (v "0.1.2") (d (list (d (n "integer-sqrt") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint") (r "^0.9.5") (k 0)))) (h "05rzxp7zvvfphigpgk621an3a6akxj5sw6dzsh1zyfw77zql25lm") (r "1.56.1")))

