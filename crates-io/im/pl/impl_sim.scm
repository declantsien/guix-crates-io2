(define-module (crates-io im pl impl_sim) #:use-module (crates-io))

(define-public crate-impl_sim-0.1.0 (c (n "impl_sim") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cj2qk44ighav1wzjping9m33nkyi2ngjwd3wv1c0ih6cnkhpqnk") (y #t)))

(define-public crate-impl_sim-0.2.0 (c (n "impl_sim") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zjxvzri6fjc9rq1jxc2jfxx0af4i9mda78l5rai33fn0b0kkl5y") (y #t)))

(define-public crate-impl_sim-0.3.0 (c (n "impl_sim") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08vzx90s2f287m9w9ljkdz1fjinir6g03mdy31wiy6xvin6z7i77")))

