(define-module (crates-io im pl implicit) #:use-module (crates-io))

(define-public crate-implicit-0.1.0 (c (n "implicit") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "flame") (r "^0.1.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4.11") (d #t) (k 0)) (d (n "lux") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "0bkplad6ysmy23zkvs2h7n9zsljgnc3kb3p7fzcki8ifkj2l4zqq")))

