(define-module (crates-io im pl implot) #:use-module (crates-io))

(define-public crate-implot-0.1.0 (c (n "implot") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0dr68xdfl4djqsvyqilv9jr8d1nz71p2dwdb1fldqbyp6g6i9f1i")))

(define-public crate-implot-0.2.0 (c (n "implot") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "imgui") (r "^0.5.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "038yrj0w96sqn8fwdg02mwcm0dfwq46j1vqmd7ikh8957if898v5")))

(define-public crate-implot-0.3.0 (c (n "implot") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "imgui") (r "^0.6.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "0wd0v9jp2jx48ixzwvsp1jdxq2x2frb7k2l6m9a46l7wjg2hj669")))

(define-public crate-implot-0.4.0 (c (n "implot") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "imgui") (r "=0.6.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "0b14zsfrd735jn5bdl8s20nn4yhpnpxnj1h07dgcl3x4s38716mx")))

(define-public crate-implot-0.5.0 (c (n "implot") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "imgui") (r "=0.6.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "1cfzg613ypjiiky1hng36xfzin560gflicvbv8d73qh5szkj42ca")))

(define-public crate-implot-0.6.0 (c (n "implot") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "imgui") (r "=0.7.0") (d #t) (k 0)) (d (n "implot-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 0)))) (h "0dwalzadcchak3b61wr2r1zrk6nfv0b9aaz2qn9j8zwz5amjgd6z")))

