(define-module (crates-io im pl impl-enum) #:use-module (crates-io))

(define-public crate-impl-enum-0.1.0 (c (n "impl-enum") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qqn7674hxk3bdq96h02y66qcd960mfdxj4q58sxflnixa8ll7qg")))

(define-public crate-impl-enum-0.2.0 (c (n "impl-enum") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fx5x0wnb46jkbp9lqzhq0byia1ygh925ycc3shsy5p01ncnkxnw")))

(define-public crate-impl-enum-0.3.0 (c (n "impl-enum") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0qdmp25xnwm9jq6s1w9fbd90q7g096m7mq3pfz00bv7rcz8vm45b") (f (quote (("with_methods") ("default" "with_methods" "as_dyn") ("as_dyn")))) (r "1.56")))

(define-public crate-impl-enum-0.3.1 (c (n "impl-enum") (v "0.3.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1wvi8c9ksinvlwfj5a2g8mblimn9wlk8qk62m1z4bz5x27v8vm0g") (f (quote (("with_methods") ("default" "with_methods" "as_dyn") ("as_dyn")))) (r "1.56")))

