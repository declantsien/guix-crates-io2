(define-module (crates-io im pl impl_trait) #:use-module (crates-io))

(define-public crate-impl_trait-0.1.0 (c (n "impl_trait") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nz3dywly1d46cll7brrgzj3ksj17sggyi6gjnpcgyidcapjdp44")))

(define-public crate-impl_trait-0.1.1 (c (n "impl_trait") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1msjl782s095pan0fzlazhk1f4i7ljnvshzg9q5fmfiyrb40d7a6")))

(define-public crate-impl_trait-0.1.2 (c (n "impl_trait") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1hnjrcg8nbnzczziwrnl7rhkizap7d6hh7159ssprk2w3vflyqnw")))

(define-public crate-impl_trait-0.1.3 (c (n "impl_trait") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "075xdb7k7dxrha5d147dp2jc9lpqjz69p3yhxxg8ag987914qr0v")))

(define-public crate-impl_trait-0.1.4 (c (n "impl_trait") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qp2d8hzia2n9yxrg14p72brnjhfcxc1ps4p414k3c457bja2zzi")))

(define-public crate-impl_trait-0.1.5 (c (n "impl_trait") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ahx9flh7i2za2ibdfg1a2hizaifv3wbk6kh9pqabv9xnnfpjimh")))

(define-public crate-impl_trait-0.1.6 (c (n "impl_trait") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17dl7kbja8l85lmqd4v8ax1gmnxvaxjhwcbn2djsqr0mkbjrkjdc")))

(define-public crate-impl_trait-0.1.7 (c (n "impl_trait") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1crjbcz9whix5qp5gfvnfxf5rrv58n1z2v1w3w7ajp6hnq7fq44c")))

