(define-module (crates-io im ge imgen) #:use-module (crates-io))

(define-public crate-imgen-0.1.0 (c (n "imgen") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)))) (h "035hhp9amax6n7i89qwf39afdmi50ys2fn03l7qw4230sy96m9lg")))

(define-public crate-imgen-0.1.1 (c (n "imgen") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)))) (h "12r4i6v2mr4s1v26zf6bb55b0n5x0hkaxr44cs0dbb63zh2mrncb")))

(define-public crate-imgen-0.1.2 (c (n "imgen") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)))) (h "0bz5m8yz6j28a5598vimcv13vpg210lzbjsdw5d75jqch3125pn8")))

