(define-module (crates-io im ge imge) #:use-module (crates-io))

(define-public crate-imge-0.1.0 (c (n "imge") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "drives") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "048blzkwln4pm4l7mh4v07wwajianbgwn61j3y329xpx1whrfpy6")))

