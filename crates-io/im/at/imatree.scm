(define-module (crates-io im at imatree) #:use-module (crates-io))

(define-public crate-imatree-0.1.0 (c (n "imatree") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "text-to-png") (r "^0.2.0") (d #t) (k 0)))) (h "181a92bdnsfj5r513xbw2jr4m6gjnrr9q42b9awfhh7958kcy8by")))

