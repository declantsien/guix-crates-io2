(define-module (crates-io im at imath-sys) #:use-module (crates-io))

(define-public crate-imath-sys-0.1.0 (c (n "imath-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)))) (h "0vxfasnnvb20gj5igbndznnpa53z8w9lq2dy52a1735hxq8by5vh") (y #t) (l "imath")))

