(define-module (crates-io im at imath-traits) #:use-module (crates-io))

(define-public crate-imath-traits-0.1.0 (c (n "imath-traits") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0gahi4nk3br9i41xy22iqm5j7m4j6nmyx49gdx949p9hk3i7ixga")))

(define-public crate-imath-traits-0.2.0 (c (n "imath-traits") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1dwszagfrliihavqqqx03q9b1lyr4g16kz76ac7ixcf6b9zmfqrp")))

(define-public crate-imath-traits-0.3.0 (c (n "imath-traits") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0p778bwsyq4yfy75lx3ccvb5mphdplang8fapsgfzzf8nnxjvj2r")))

(define-public crate-imath-traits-0.4.0 (c (n "imath-traits") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1b8iay2m2nv2c3w6rxmm502vxrrs5sxvcwrrplb5r6gzqj0anna6")))

(define-public crate-imath-traits-0.5.0 (c (n "imath-traits") (v "0.5.0") (d (list (d (n "cgmath") (r "<=0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "<=0.21") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "<=0.29") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "<=0.15") (o #t) (d #t) (k 0)))) (h "1ifsxhxw64h1143mp8nmvdzn2qznvi611c3jghmhjxn4c3nj909g")))

(define-public crate-imath-traits-0.6.0 (c (n "imath-traits") (v "0.6.0") (d (list (d (n "cgmath") (r "<=0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "<=0.21") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "<=0.29") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "<=0.15") (o #t) (d #t) (k 0)))) (h "1vr8m7vja2cx85m7vbrxncy7ndk2ack0wv233d7ml8vrz64mr42c")))

(define-public crate-imath-traits-0.7.0 (c (n "imath-traits") (v "0.7.0") (d (list (d (n "cgmath") (r "<=0.18") (o #t) (d #t) (k 0)) (d (n "glam") (r "<=0.21") (o #t) (d #t) (k 0)) (d (n "half") (r "^1.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "<=0.29") (o #t) (d #t) (k 0)) (d (n "nalgebra-glm") (r "<=0.15") (o #t) (d #t) (k 0)))) (h "0rvp2blfafs7nxlhlfi2ix2b9kgybhksbrl50zvz72c1gllpzbcf")))

