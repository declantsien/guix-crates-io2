(define-module (crates-io im li imlib2) #:use-module (crates-io))

(define-public crate-imlib2-0.1.0 (c (n "imlib2") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "imlib2-sys") (r "^0.1") (d #t) (k 0)) (d (n "leanshot_xlib") (r "^0.1") (d #t) (k 0)))) (h "0av6yqljbzf49wqizyk36i7mj9fnmb2r74c898i5bjy83braazjw")))

(define-public crate-imlib2-0.1.1 (c (n "imlib2") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "imlib2-sys") (r "^0.1") (d #t) (k 0)) (d (n "leanshot_xlib") (r "^0.1") (d #t) (k 0)))) (h "14y4zvx6q6n10qg1ihkfqff82jncphxlp2spf5cp1pr86b6hmysa")))

