(define-module (crates-io im xr imxrt1062-aipstz1) #:use-module (crates-io))

(define-public crate-imxrt1062-aipstz1-0.1.0 (c (n "imxrt1062-aipstz1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09ffqr9j9hf86032gl2rgirxwij34zh15lw2r1d7jgjxnhp1zm3s")))

(define-public crate-imxrt1062-aipstz1-0.1.1 (c (n "imxrt1062-aipstz1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pr9zznybzqlrdmy748mg5mpkv5bawqdlj1y8cxfgac3xq3vyjp9")))

