(define-module (crates-io im xr imxrt1062-iomuxc) #:use-module (crates-io))

(define-public crate-imxrt1062-iomuxc-0.1.0 (c (n "imxrt1062-iomuxc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ixq2llc5gch76g09qf7dxfmgc2zvsc740jdc8838wb6pwv1rcb5")))

(define-public crate-imxrt1062-iomuxc-0.1.1 (c (n "imxrt1062-iomuxc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vvxqfjczy8wnxxq8ki4gxlsp6hzi7y67m04in3snacis8gq7v2i")))

