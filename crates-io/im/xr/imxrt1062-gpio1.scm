(define-module (crates-io im xr imxrt1062-gpio1) #:use-module (crates-io))

(define-public crate-imxrt1062-gpio1-0.1.0 (c (n "imxrt1062-gpio1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zin0863kc493j4rgah7hh8r9yl37dgcki48jm55s5k1dpxj2dgp")))

(define-public crate-imxrt1062-gpio1-0.1.1 (c (n "imxrt1062-gpio1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fn3wi8hy9wk8p01zmg81jmpvc8dz1fcyzdjhiqn7y61cfjgkfc1")))

