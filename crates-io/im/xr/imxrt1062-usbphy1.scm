(define-module (crates-io im xr imxrt1062-usbphy1) #:use-module (crates-io))

(define-public crate-imxrt1062-usbphy1-0.1.0 (c (n "imxrt1062-usbphy1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ybd0015n7y8jk089s11hlicsd3839h05hbz64pd02zvns6sgawk")))

(define-public crate-imxrt1062-usbphy1-0.1.1 (c (n "imxrt1062-usbphy1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0x4sip5avjg4v565lndpmjba95dw5hmvivhpkpjdhyi0k6l7v3mw")))

