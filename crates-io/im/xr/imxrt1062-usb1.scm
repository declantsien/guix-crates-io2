(define-module (crates-io im xr imxrt1062-usb1) #:use-module (crates-io))

(define-public crate-imxrt1062-usb1-0.1.0 (c (n "imxrt1062-usb1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "18a3gfzv6kr7wkibwxcmga1vrqpnsapiakgmr11al7v19mhn9q61")))

(define-public crate-imxrt1062-usb1-0.1.1 (c (n "imxrt1062-usb1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0my64116bmlk3n9acnl9q3qd9ikfcrcwznr78lg08wbs0a4drshw")))

