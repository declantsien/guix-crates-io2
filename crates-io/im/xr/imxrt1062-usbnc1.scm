(define-module (crates-io im xr imxrt1062-usbnc1) #:use-module (crates-io))

(define-public crate-imxrt1062-usbnc1-0.1.0 (c (n "imxrt1062-usbnc1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05x94w94k1nkxc1df61w7z8msnh6dnpzbqsy5hws8gx106ny4k84")))

(define-public crate-imxrt1062-usbnc1-0.1.1 (c (n "imxrt1062-usbnc1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1iy5pg544sljl88wialqb0kpr8svyqk9w16c3ig407m7y383my3y")))

