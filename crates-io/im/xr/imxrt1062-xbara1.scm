(define-module (crates-io im xr imxrt1062-xbara1) #:use-module (crates-io))

(define-public crate-imxrt1062-xbara1-0.1.0 (c (n "imxrt1062-xbara1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0abvgdwah1fixxlbrgkqjlx16r242kr2paw82a6pjdr8071w5ycv")))

(define-public crate-imxrt1062-xbara1-0.1.1 (c (n "imxrt1062-xbara1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1glgs291644ar2rym5yb1wqd2dzkk98in3vwkw8jd921bfx6j3g7")))

