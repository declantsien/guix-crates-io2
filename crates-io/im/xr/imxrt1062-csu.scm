(define-module (crates-io im xr imxrt1062-csu) #:use-module (crates-io))

(define-public crate-imxrt1062-csu-0.1.0 (c (n "imxrt1062-csu") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1dgban06ii3cj5wa9c5vd03d3zn1bg93b89hp7m3nwz66qlpqvyp")))

(define-public crate-imxrt1062-csu-0.1.1 (c (n "imxrt1062-csu") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gdkgbmmdkjf5j7gdx1jss8d75va80p3an4kj1gfdjwzfn79qhzf")))

