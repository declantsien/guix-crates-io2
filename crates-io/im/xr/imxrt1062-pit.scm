(define-module (crates-io im xr imxrt1062-pit) #:use-module (crates-io))

(define-public crate-imxrt1062-pit-0.1.0 (c (n "imxrt1062-pit") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0mglslyh1nymgn8y87rs37ci0gn2kzxipfmd14i6lv375j7vfcs5")))

(define-public crate-imxrt1062-pit-0.1.1 (c (n "imxrt1062-pit") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pcb6lwwpm8fj9fk72q0pxmmyd2zjz1s8m7i2wc5g2svkm86vzvl")))

