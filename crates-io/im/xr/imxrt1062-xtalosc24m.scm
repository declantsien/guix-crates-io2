(define-module (crates-io im xr imxrt1062-xtalosc24m) #:use-module (crates-io))

(define-public crate-imxrt1062-xtalosc24m-0.1.0 (c (n "imxrt1062-xtalosc24m") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "10dzj5qaifs1qxj6cvh5i7d968q2d4hb19w5wq299vsd1cnz5wln")))

(define-public crate-imxrt1062-xtalosc24m-0.1.1 (c (n "imxrt1062-xtalosc24m") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0wdkilfv324riyaica0ipkv5kbvaasx3hqz4bi009z10dzipm24j")))

