(define-module (crates-io im xr imxrt1062-flexspi) #:use-module (crates-io))

(define-public crate-imxrt1062-flexspi-0.1.0 (c (n "imxrt1062-flexspi") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1q5bqcm6icn8yv32wbvnq83b2yzq6mkiv1sa8dk0n7zxp9cf9f3l")))

(define-public crate-imxrt1062-flexspi-0.1.1 (c (n "imxrt1062-flexspi") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0glyjhmwbk1ys9jv8rf59j3ziraadbzlgg397l5686mpwc9bk2kx")))

