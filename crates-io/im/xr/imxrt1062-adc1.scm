(define-module (crates-io im xr imxrt1062-adc1) #:use-module (crates-io))

(define-public crate-imxrt1062-adc1-0.1.0 (c (n "imxrt1062-adc1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1xlip7jd9zag91pimpkzq7xq5dmlw458v4plx829jpkkrcxa4ngm")))

(define-public crate-imxrt1062-adc1-0.1.1 (c (n "imxrt1062-adc1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0bnyngd6c658pv92jd1zm6j94xp57mahxhy64p556akn5jdswbs1")))

