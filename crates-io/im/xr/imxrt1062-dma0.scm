(define-module (crates-io im xr imxrt1062-dma0) #:use-module (crates-io))

(define-public crate-imxrt1062-dma0-0.1.0 (c (n "imxrt1062-dma0") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lx6ghq1bv5ay3c55322l0sbmcy7hzcv05m2vpmmzg2x84c4rjzd")))

(define-public crate-imxrt1062-dma0-0.1.1 (c (n "imxrt1062-dma0") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1k3y0p678c7mm4ascp2yn53fdl2vhga4d1h8bxg6llpafw16068a")))

