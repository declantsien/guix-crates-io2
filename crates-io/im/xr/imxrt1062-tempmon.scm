(define-module (crates-io im xr imxrt1062-tempmon) #:use-module (crates-io))

(define-public crate-imxrt1062-tempmon-0.1.0 (c (n "imxrt1062-tempmon") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0h4z8q91nj0wbzkqz59c6pxgrzk9w1saq6b5mib5xnv3vf9bf1l8")))

(define-public crate-imxrt1062-tempmon-0.1.1 (c (n "imxrt1062-tempmon") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05kx20xglngjkbxpb2lmhc9yb4aq8rdhkiilkjr0f81738w2j030")))

