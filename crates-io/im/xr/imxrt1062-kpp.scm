(define-module (crates-io im xr imxrt1062-kpp) #:use-module (crates-io))

(define-public crate-imxrt1062-kpp-0.1.0 (c (n "imxrt1062-kpp") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0kw23231ff7c6nn3sg6cr9afhg5dmdi5yv5xl97a87fxg7yasnrd")))

(define-public crate-imxrt1062-kpp-0.1.1 (c (n "imxrt1062-kpp") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1xnjsp8qys9243xh0a7sy4npzgdjfx1sj56ps5dnisg4m4qjbvy3")))

