(define-module (crates-io im xr imxrt1062-pgc) #:use-module (crates-io))

(define-public crate-imxrt1062-pgc-0.1.0 (c (n "imxrt1062-pgc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ia9fgyfj88jw3gy8m90szrhsh33c16gp1w7vwx75akf1r8cvkfx")))

(define-public crate-imxrt1062-pgc-0.1.1 (c (n "imxrt1062-pgc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0995wjcnhplnn5a9043xv1fdfd1ccyipnk2w94gb99x4nbz1qzl4")))

