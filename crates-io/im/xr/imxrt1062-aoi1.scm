(define-module (crates-io im xr imxrt1062-aoi1) #:use-module (crates-io))

(define-public crate-imxrt1062-aoi1-0.1.0 (c (n "imxrt1062-aoi1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1blbmf7psqds27sy77nwbqp8l8zsxh6s9a2hg522w1aq97axb9x9")))

(define-public crate-imxrt1062-aoi1-0.1.1 (c (n "imxrt1062-aoi1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "13lkcfd0zn1xq3zlx5g6mdv2drsvrg1gjhaihlj6iyhmw1g2ard7")))

