(define-module (crates-io im xr imxrt1062-usdhc1) #:use-module (crates-io))

(define-public crate-imxrt1062-usdhc1-0.1.0 (c (n "imxrt1062-usdhc1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1cb6g7vj3nwfk0sn3al11ljkg0qcvcrpbbigvgxmffadzrdnscji")))

(define-public crate-imxrt1062-usdhc1-0.1.1 (c (n "imxrt1062-usdhc1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0l4vf6miv9bwivqhw3l69alh35br20mfzgcrkmvaqybp4hzbrgyr")))

