(define-module (crates-io im xr imxrt1062-rtwdog) #:use-module (crates-io))

(define-public crate-imxrt1062-rtwdog-0.1.0 (c (n "imxrt1062-rtwdog") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01lqpd88d3aqg2f0kvbxkaa44412n8sx3pcf4fjk39ggwrgsyjpp")))

(define-public crate-imxrt1062-rtwdog-0.1.1 (c (n "imxrt1062-rtwdog") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1k7k2zi8pfr77s3q2kdgpix1p2hbrjqp915k8by3jjsy7gyi5h9q")))

