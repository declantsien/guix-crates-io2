(define-module (crates-io im xr imxrt1062-hal) #:use-module (crates-io))

(define-public crate-imxrt1062-hal-0.1.0 (c (n "imxrt1062-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "imxrt1062-pac") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0sj59272i20zr3k54h9471dxkicrjcyaqzvnv239m6m1vmvc4hxk") (f (quote (("default" "embedded-hal/unproven"))))))

(define-public crate-imxrt1062-hal-0.2.1 (c (n "imxrt1062-hal") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "imxrt1062-pac") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1r5mfhfaix8mbzah800wdxfrldf4xbjzyh9zd83z77994c834ah4") (f (quote (("default" "embedded-hal/unproven"))))))

