(define-module (crates-io im xr imxrt1062-system-control) #:use-module (crates-io))

(define-public crate-imxrt1062-system-control-0.1.0 (c (n "imxrt1062-system-control") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1v0qx85qvl7r6916cz242a183hn281swb5wk2pzvyb5vy7idbimc")))

(define-public crate-imxrt1062-system-control-0.1.1 (c (n "imxrt1062-system-control") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1miakw4sm005kbb6520x9wg2ks6hlbhhw2darl1sipfgi2zim8d4")))

