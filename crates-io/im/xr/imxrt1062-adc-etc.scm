(define-module (crates-io im xr imxrt1062-adc-etc) #:use-module (crates-io))

(define-public crate-imxrt1062-adc-etc-0.1.0 (c (n "imxrt1062-adc-etc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ngz184b1bqw43jy79im9x12r4amkgr02c24faxai7vixv3x4yx6")))

(define-public crate-imxrt1062-adc-etc-0.1.1 (c (n "imxrt1062-adc-etc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0v3sjp1jwsy4wjc7p7vigk2mxxdxbxixb1zzi2w19019qdxdgmpc")))

