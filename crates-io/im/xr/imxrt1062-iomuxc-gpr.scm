(define-module (crates-io im xr imxrt1062-iomuxc-gpr) #:use-module (crates-io))

(define-public crate-imxrt1062-iomuxc-gpr-0.1.0 (c (n "imxrt1062-iomuxc-gpr") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1djvvr3n12ijwb9vlfksihnzc908fhksl3f9fv0nkbhchw75qfzk")))

(define-public crate-imxrt1062-iomuxc-gpr-0.1.1 (c (n "imxrt1062-iomuxc-gpr") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "065nxpbzwj87gmnqwwfx760q7i4qi84g8n4js0zq9q9xrs8ancc9")))

