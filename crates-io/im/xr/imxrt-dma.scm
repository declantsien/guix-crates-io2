(define-module (crates-io im xr imxrt-dma) #:use-module (crates-io))

(define-public crate-imxrt-dma-0.1.0 (c (n "imxrt-dma") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)))) (h "18h6jdylc19rd8k5n1xvw5k1w8rxy7ywdb7gaqm8xil01c7wjh69")))

(define-public crate-imxrt-dma-0.1.1 (c (n "imxrt-dma") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)))) (h "190jch6lfkyhyxlxvqc0w6yv5qx3djhwpqwb8z1qi76arycxrr0b")))

