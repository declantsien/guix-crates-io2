(define-module (crates-io im xr imxrt1062-enc1) #:use-module (crates-io))

(define-public crate-imxrt1062-enc1-0.1.0 (c (n "imxrt1062-enc1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xmbznw75rxbgbicdpwka9fflmwznc5p88fw5m2mchzldm6sxlpd")))

(define-public crate-imxrt1062-enc1-0.1.1 (c (n "imxrt1062-enc1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1hhs7fdq1cdslnn666y5bd6xba69lycq93qjnriccrswggwvi45n")))

