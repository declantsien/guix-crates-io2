(define-module (crates-io im xr imxrt1062-ccm-analog) #:use-module (crates-io))

(define-public crate-imxrt1062-ccm-analog-0.1.0 (c (n "imxrt1062-ccm-analog") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03vy8cj4wvzvrpcjqapg110ash4n2wxk94pivx03sv1kzygdhxrn")))

(define-public crate-imxrt1062-ccm-analog-0.1.1 (c (n "imxrt1062-ccm-analog") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "06yz3hscj57ngnld4m3cvlfqlprzix3bxs6wcn7b0iv5v5fj4hys")))

