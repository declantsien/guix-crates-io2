(define-module (crates-io im xr imxrt1062-can1) #:use-module (crates-io))

(define-public crate-imxrt1062-can1-0.1.0 (c (n "imxrt1062-can1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "06kvik17j0v9i5ygpb5b93d7wanm724g168m7p31i46b1gb979ig")))

(define-public crate-imxrt1062-can1-0.1.1 (c (n "imxrt1062-can1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xi89b2cd9k9syxj2flspsk5r2mvgzcjji4gg289rbk45ac9yhx6")))

