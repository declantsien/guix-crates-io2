(define-module (crates-io im xr imxrt1062-sai1) #:use-module (crates-io))

(define-public crate-imxrt1062-sai1-0.1.0 (c (n "imxrt1062-sai1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0dcy71a7bm7dsrgn3q70rsxpj5q8n3hiq2wax4v60x687xpdh1y8")))

(define-public crate-imxrt1062-sai1-0.1.1 (c (n "imxrt1062-sai1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1dkcs850r6fdsvsh5xb6jpfw6lc7ppry2q55w7nxlrpnrfpc64wd")))

