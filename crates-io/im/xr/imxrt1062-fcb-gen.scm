(define-module (crates-io im xr imxrt1062-fcb-gen) #:use-module (crates-io))

(define-public crate-imxrt1062-fcb-gen-0.1.0 (c (n "imxrt1062-fcb-gen") (v "0.1.0") (h "1f6jb849z6p9h4l6k3qpr5nry2jvx15z4x7xsb9p0xh3is1gaxy3")))

(define-public crate-imxrt1062-fcb-gen-0.1.1 (c (n "imxrt1062-fcb-gen") (v "0.1.1") (h "1a1z5pb72r5jls1bc02xr81a8jq8i6v7ff65h2lx7drv2q1i1ig5")))

