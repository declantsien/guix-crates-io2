(define-module (crates-io im xr imxrt1062-ocotp) #:use-module (crates-io))

(define-public crate-imxrt1062-ocotp-0.1.0 (c (n "imxrt1062-ocotp") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0b6k4dggfzyjlpdpbdg3nbf23xapqishz8d03kdq5facydgvyyrn")))

(define-public crate-imxrt1062-ocotp-0.1.1 (c (n "imxrt1062-ocotp") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rf5cmzgjh3y66gh5a8387rkhr3v8r6696j7ga8l9pm726rw8qbh")))

