(define-module (crates-io im xr imxrt1062-enet) #:use-module (crates-io))

(define-public crate-imxrt1062-enet-0.1.0 (c (n "imxrt1062-enet") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0zfmd9ivy7f4ry68pa5zlm3kvxy6hpmqdn5f6sxcld5rdka2kxzw")))

(define-public crate-imxrt1062-enet-0.1.1 (c (n "imxrt1062-enet") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0gdqi8pgfb7xnkq2z1kwz7vyfikyrgrkqs2p3arkvf2i6376mr5m")))

