(define-module (crates-io im xr imxrt1062-pxp) #:use-module (crates-io))

(define-public crate-imxrt1062-pxp-0.1.0 (c (n "imxrt1062-pxp") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "018kjbl36ydwj5ij195c5vb773l0bbi968zhi27bg6a7vzkbvkvf")))

(define-public crate-imxrt1062-pxp-0.1.1 (c (n "imxrt1062-pxp") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zj39r65avbvzjvvsgxqh4g1himh9vpn19ckln6hxnxsjr3c8zji")))

