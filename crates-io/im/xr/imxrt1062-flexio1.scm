(define-module (crates-io im xr imxrt1062-flexio1) #:use-module (crates-io))

(define-public crate-imxrt1062-flexio1-0.1.0 (c (n "imxrt1062-flexio1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1k6b9jjqd1jwgmrdkg5aymrdbmhkwzb4mha5kf8yw7gw634jws9b")))

(define-public crate-imxrt1062-flexio1-0.1.1 (c (n "imxrt1062-flexio1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0y1fvrk0dmw79c3ssdmvvq419874rlld4ads1dyhzvwwc1pbsrfx")))

