(define-module (crates-io im xr imxrt1062-wdog1) #:use-module (crates-io))

(define-public crate-imxrt1062-wdog1-0.1.0 (c (n "imxrt1062-wdog1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19mq2b3bs29fzgh8606yafji7i67fs5rfql8xa1sbf477xsmgn5l")))

(define-public crate-imxrt1062-wdog1-0.1.1 (c (n "imxrt1062-wdog1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0k1dshyzal4gi6i41719vmc56pimdh0axbbqjlsfx5jyjs8d38l0")))

