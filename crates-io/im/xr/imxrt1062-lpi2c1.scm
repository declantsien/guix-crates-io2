(define-module (crates-io im xr imxrt1062-lpi2c1) #:use-module (crates-io))

(define-public crate-imxrt1062-lpi2c1-0.1.0 (c (n "imxrt1062-lpi2c1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1nx3q2p1krci29r6sgnsm0gn8z7q9ga1rrhy7cjknyk8laf4vyka")))

(define-public crate-imxrt1062-lpi2c1-0.1.1 (c (n "imxrt1062-lpi2c1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0f3q6j65r9yspbj5xwp4npl1h97d2xdgh8w6fzsbvvqrpl232f81")))

