(define-module (crates-io im xr imxrt1062-pwm1) #:use-module (crates-io))

(define-public crate-imxrt1062-pwm1-0.1.0 (c (n "imxrt1062-pwm1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "157sb5316hi6wrvwn5m7q757jd8z2sdyfy2cmyxhx2glhsgzv3kl")))

(define-public crate-imxrt1062-pwm1-0.1.1 (c (n "imxrt1062-pwm1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1y4qfix3r0hg2d7mxdhaxf2y95281yfmhmgm0yf8yyavs6nl2270")))

