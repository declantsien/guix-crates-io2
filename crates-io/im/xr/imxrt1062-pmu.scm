(define-module (crates-io im xr imxrt1062-pmu) #:use-module (crates-io))

(define-public crate-imxrt1062-pmu-0.1.0 (c (n "imxrt1062-pmu") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rzk0rl2x9vkdgv1kqaz6zyshfw89i4062w9kwyl629l5p20iwxc")))

(define-public crate-imxrt1062-pmu-0.1.1 (c (n "imxrt1062-pmu") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00q27lskmh24bcrhcz6cy818fb08x92lhk66pfcabd0vk8mfqdhv")))

