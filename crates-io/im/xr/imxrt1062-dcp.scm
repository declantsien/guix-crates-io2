(define-module (crates-io im xr imxrt1062-dcp) #:use-module (crates-io))

(define-public crate-imxrt1062-dcp-0.1.0 (c (n "imxrt1062-dcp") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "040fj0nh3f7l1qc2p2k3wa9gnqs7nh63jzsnjdd3l6kxhjmg98sr")))

(define-public crate-imxrt1062-dcp-0.1.1 (c (n "imxrt1062-dcp") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16wnx28zf3ikc8w0p8mm1jkzg4f5194h4az2ypbavhkjz6hip1ca")))

