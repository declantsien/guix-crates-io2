(define-module (crates-io im xr imxrt1062-trng) #:use-module (crates-io))

(define-public crate-imxrt1062-trng-0.1.0 (c (n "imxrt1062-trng") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0m60sv1kv7lqz14l4yl06pkaqgdgszg7z2rvjwvz7b0niwlp32ih")))

(define-public crate-imxrt1062-trng-0.1.1 (c (n "imxrt1062-trng") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0gxy95iy8c6y6x9ldc3j8rxwm301dcfdm966kgxlw578n4my9d79")))

