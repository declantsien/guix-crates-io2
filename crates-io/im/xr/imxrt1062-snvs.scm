(define-module (crates-io im xr imxrt1062-snvs) #:use-module (crates-io))

(define-public crate-imxrt1062-snvs-0.1.0 (c (n "imxrt1062-snvs") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "15ayk7ah486miv9xvhk86xry6m8vgzdawl2x3blsh8l99r60gwi8")))

(define-public crate-imxrt1062-snvs-0.1.1 (c (n "imxrt1062-snvs") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0g07w42ii5qaay3i7h0db7jj5515wif50zzcd6y07qs2nk9q79ns")))

