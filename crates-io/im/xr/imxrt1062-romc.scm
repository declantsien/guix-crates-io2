(define-module (crates-io im xr imxrt1062-romc) #:use-module (crates-io))

(define-public crate-imxrt1062-romc-0.1.0 (c (n "imxrt1062-romc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "18l477ggz788dwvdb6z5dzz327hj9w58vryvs8y4yz98c60jx3gb")))

(define-public crate-imxrt1062-romc-0.1.1 (c (n "imxrt1062-romc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19hcvss36m8r98njvxgzid1a5fihydsiqylkbf5svj0r27ra6jmg")))

