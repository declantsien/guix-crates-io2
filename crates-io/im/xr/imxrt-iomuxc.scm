(define-module (crates-io im xr imxrt-iomuxc) #:use-module (crates-io))

(define-public crate-imxrt-iomuxc-0.1.0 (c (n "imxrt-iomuxc") (v "0.1.0") (d (list (d (n "imxrt-iomuxc-build") (r "^0.1.0") (d #t) (k 1)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1fmvbk7b677v5vkx468pm9vws0pd6mpn9imjc066mhvax5wp84pz") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.1.1 (c (n "imxrt-iomuxc") (v "0.1.1") (d (list (d (n "imxrt-iomuxc-build") (r "^0.1.0") (d #t) (k 1)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "049m4j8p2iagq50h6xz6nj0rfr5yz8cg9zalxy8rc8vhl83mhxbb") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.1.2 (c (n "imxrt-iomuxc") (v "0.1.2") (d (list (d (n "imxrt-iomuxc-build") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "typenum") (r ">=1.12.0, <2.0.0") (d #t) (k 0)))) (h "1jiw1f8nrffmhd37y4ldhp5z4yxy1cccnw28w1300rr8mcn1l95h") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.1.3 (c (n "imxrt-iomuxc") (v "0.1.3") (d (list (d (n "imxrt-iomuxc-build") (r "^0.1.0") (d #t) (k 1)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "123rsgfmplx3y29nqavgwx683cdvnf58dkigbpic1pa4ypgs1rh8") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.1.4 (c (n "imxrt-iomuxc") (v "0.1.4") (d (list (d (n "imxrt-iomuxc-build") (r "^0.1.0") (d #t) (k 1)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1qpqqnc0lvk77y4lkkpzk492lqkajar4xmgg10w9vg3dx5aw6za4") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.1.5 (c (n "imxrt-iomuxc") (v "0.1.5") (d (list (d (n "imxrt-iomuxc-build") (r "^0.1.0") (d #t) (k 1)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "00bfmy9mrg3n14n0ya970sbdk34ivh6r9bgp1976npbhwyzsxvpp") (f (quote (("imxrt106x"))))))

(define-public crate-imxrt-iomuxc-0.2.0 (c (n "imxrt-iomuxc") (v "0.2.0") (h "189qfk9nmdk13gf88my3wldfqjqr76dlkshqnwxj3cq8a8zqgpv1") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.1 (c (n "imxrt-iomuxc") (v "0.2.1") (h "1aq56a3na08mgmks218fnhlvfxj52b31xs5a1lm7fkhydxdfhqfg") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.2 (c (n "imxrt-iomuxc") (v "0.2.2") (h "0z5cczbffypawmw52wbhr9ys57zghnrc4n9hwfhwh41hrdpyc86q") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.3 (c (n "imxrt-iomuxc") (v "0.2.3") (h "0q1zf4bbwwlx3aa39ac5hid4d6rqggd34b1z03limswp0s0nbhmw") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.4 (c (n "imxrt-iomuxc") (v "0.2.4") (h "0glfmgzx7w8dhv1dcpg19s98abzfs8a43sli3vm0p47wpgmknff0") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.5 (c (n "imxrt-iomuxc") (v "0.2.5") (h "1lfx1909saqq32nhpwk6whk2rfsmj6f7320hwppg4b446jss7ijy") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-iomuxc-0.2.6 (c (n "imxrt-iomuxc") (v "0.2.6") (h "0jilj7hqmvb6w67z5xf7vab4ihynk7fcbidp52gjyhpai8hjz5jj") (f (quote (("imxrt1170") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

