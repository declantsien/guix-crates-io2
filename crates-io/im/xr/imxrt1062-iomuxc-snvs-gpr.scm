(define-module (crates-io im xr imxrt1062-iomuxc-snvs-gpr) #:use-module (crates-io))

(define-public crate-imxrt1062-iomuxc-snvs-gpr-0.1.0 (c (n "imxrt1062-iomuxc-snvs-gpr") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lqppb7vhjr2zfayx98qswcb9akpf49mvm72jcza6mg5lds21d02")))

(define-public crate-imxrt1062-iomuxc-snvs-gpr-0.1.1 (c (n "imxrt1062-iomuxc-snvs-gpr") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xzm7rn3kd89fc7y4pr2l9x39m29j40jrjd4pm6bvn4hbw68rmiy")))

