(define-module (crates-io im xr imxrt1062-spdif) #:use-module (crates-io))

(define-public crate-imxrt1062-spdif-0.1.0 (c (n "imxrt1062-spdif") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09kdxp5h7lfzdpnd43dyamrgj9girk9psl78zibhw2vxznpjf090")))

(define-public crate-imxrt1062-spdif-0.1.1 (c (n "imxrt1062-spdif") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0syf9wi2vnnbxglnzpdwncpsg23x196dyhfqg0qk3478y4agpmzw")))

