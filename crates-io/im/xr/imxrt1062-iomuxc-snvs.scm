(define-module (crates-io im xr imxrt1062-iomuxc-snvs) #:use-module (crates-io))

(define-public crate-imxrt1062-iomuxc-snvs-0.1.0 (c (n "imxrt1062-iomuxc-snvs") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "12hb887ma1fi9ccy2d4r5qwzw5av3hj8xaqnvbbra093iwnhwlq7")))

(define-public crate-imxrt1062-iomuxc-snvs-0.1.1 (c (n "imxrt1062-iomuxc-snvs") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "08nvvr7y0wkks2fn05lq813z5wjnw04cml7j9vzsb9b3rr99lylg")))

