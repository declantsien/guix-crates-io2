(define-module (crates-io im xr imxrt1062-cmp1) #:use-module (crates-io))

(define-public crate-imxrt1062-cmp1-0.1.0 (c (n "imxrt1062-cmp1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lxng55hny6qj7jmbqi59xcd575svfwx05v408hc9gpk7sivfria")))

(define-public crate-imxrt1062-cmp1-0.1.1 (c (n "imxrt1062-cmp1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1v53lbi922fwyyzvv4j2dbxc6zm7v83iwydscg96cq8fy0jfh1sr")))

