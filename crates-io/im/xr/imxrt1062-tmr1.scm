(define-module (crates-io im xr imxrt1062-tmr1) #:use-module (crates-io))

(define-public crate-imxrt1062-tmr1-0.1.0 (c (n "imxrt1062-tmr1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pgspshnm8v0g7wjzhc7yz3hs32agc0ikbv2bff0amks8662fhgd")))

(define-public crate-imxrt1062-tmr1-0.1.1 (c (n "imxrt1062-tmr1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1wqd47rk1pmzx5a3xvq6p9caclkwxsg746d6psvak89wp9bsz40g")))

