(define-module (crates-io im xr imxrt1062-csi) #:use-module (crates-io))

(define-public crate-imxrt1062-csi-0.1.0 (c (n "imxrt1062-csi") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1yfkr9kb5x5rgdpq02s4s3zhvic3nia2wpm5x46yjxn8canvdiim")))

(define-public crate-imxrt1062-csi-0.1.1 (c (n "imxrt1062-csi") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1jj7rdfa9klrs8bhmq7lmn5cw48c8w2zijmikr0c76nbxmd2a13q")))

