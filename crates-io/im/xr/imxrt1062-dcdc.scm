(define-module (crates-io im xr imxrt1062-dcdc) #:use-module (crates-io))

(define-public crate-imxrt1062-dcdc-0.1.0 (c (n "imxrt1062-dcdc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1s01b48315rjd76z30jj3fkmknq277gckyh7pbv643zsqki1msxh")))

(define-public crate-imxrt1062-dcdc-0.1.1 (c (n "imxrt1062-dcdc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gbg6q01pm1vvqph7g00qh5ibg8kz2niy1j9d5rgpww0zzcldblb")))

