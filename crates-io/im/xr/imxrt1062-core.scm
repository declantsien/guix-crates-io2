(define-module (crates-io im xr imxrt1062-core) #:use-module (crates-io))

(define-public crate-imxrt1062-core-0.1.0 (c (n "imxrt1062-core") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1arc2i3wqsx75xd226hcflk2dmjmnfy1xvhdwy11vkpyl2qm889n")))

(define-public crate-imxrt1062-core-0.1.1 (c (n "imxrt1062-core") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fp6p0z72nbplvcxpnjqi6vzsp551619aq5dbnn1hs8944h3fzj6")))

