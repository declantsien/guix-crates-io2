(define-module (crates-io im xr imxrt1062-ewm) #:use-module (crates-io))

(define-public crate-imxrt1062-ewm-0.1.0 (c (n "imxrt1062-ewm") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1wzpmgwfjnd1c83vx8x8sk4v0lf80xv0xnwwym3fpahc3lzbgjf6")))

(define-public crate-imxrt1062-ewm-0.1.1 (c (n "imxrt1062-ewm") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0m7s2b7qd4743yph4yw6as3zcwj1i510xnrwnyqrkfhs3j3dqdxq")))

