(define-module (crates-io im xr imxrt-boot-gen) #:use-module (crates-io))

(define-public crate-imxrt-boot-gen-0.1.0 (c (n "imxrt-boot-gen") (v "0.1.0") (h "0myg3am79gs64djs4px2l92ckx9a98vrmnkhyq350bk7g0n7549r") (f (quote (("imxrt1064") ("imxrt1062") ("imxrt1061") ("imxrt1011"))))))

(define-public crate-imxrt-boot-gen-0.2.0 (c (n "imxrt-boot-gen") (v "0.2.0") (h "0c862259mmw74gyr32sdsg9prqyd9yf6gf5xa2kn12jn5d5cs8d9") (f (quote (("imxrt1064") ("imxrt1060") ("imxrt1010"))))))

(define-public crate-imxrt-boot-gen-0.2.1 (c (n "imxrt-boot-gen") (v "0.2.1") (h "0gbwhalgzqvsc9nmhass2y164wzqkq65vxqwvnz4mvqyd2syndyr") (f (quote (("imxrt1064") ("imxrt1060") ("imxrt1010"))))))

(define-public crate-imxrt-boot-gen-0.3.0 (c (n "imxrt-boot-gen") (v "0.3.0") (h "1rsfii5p0sml88n41s3w1d14gb16djkr9ssxl1g3p1zs05ac5h00") (f (quote (("imxrt1170") ("imxrt1064") ("imxrt1060") ("imxrt1010"))))))

(define-public crate-imxrt-boot-gen-0.3.1 (c (n "imxrt-boot-gen") (v "0.3.1") (h "06gyymad4f0rf8ywsl35jcb49l0qccxrjv9dcvw5i7k1lh2cn2z9") (f (quote (("imxrt1170") ("imxrt1064") ("imxrt1060") ("imxrt1020") ("imxrt1010"))))))

(define-public crate-imxrt-boot-gen-0.3.2 (c (n "imxrt-boot-gen") (v "0.3.2") (h "0xbsx26d2cmxfb26yn4g0yi9lrz1n6kp80zf2yjlb2d7918bhl4m") (f (quote (("imxrt1170") ("imxrt1064") ("imxrt1060") ("imxrt1050") ("imxrt1020") ("imxrt1010"))))))

