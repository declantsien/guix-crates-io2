(define-module (crates-io im xr imxrt1062-lpuart1) #:use-module (crates-io))

(define-public crate-imxrt1062-lpuart1-0.1.0 (c (n "imxrt1062-lpuart1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0j03vmadhjpk1w2vg0a8q3jiylhy84wgf35r91m70xv649cya9r6")))

(define-public crate-imxrt1062-lpuart1-0.1.1 (c (n "imxrt1062-lpuart1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "13a839rsc8plba8zkzpylbh9clcyxmwcf2fg0fx30i3cs6w38l81")))

