(define-module (crates-io im xr imxrt1062-usb-analog) #:use-module (crates-io))

(define-public crate-imxrt1062-usb-analog-0.1.0 (c (n "imxrt1062-usb-analog") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0c849wpx0l1h6y90hq7znavmnc0qnhzpw002k1z1nyijyakcfc0y")))

(define-public crate-imxrt1062-usb-analog-0.1.1 (c (n "imxrt1062-usb-analog") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "019r820zskfr7gk9cqf8varbfrrlzafyz01qcq817v5kvgbkp0i6")))

