(define-module (crates-io im xr imxrt1062-gpt1) #:use-module (crates-io))

(define-public crate-imxrt1062-gpt1-0.1.0 (c (n "imxrt1062-gpt1") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1glfdv70lrkl54s0s5w1izbwqxpis8painfvc1c53awdaghysd24")))

(define-public crate-imxrt1062-gpt1-0.1.1 (c (n "imxrt1062-gpt1") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "12aj5npza6hndkkla0148mlffpr2alr6spwdfi22fxqq2wnp04p4")))

