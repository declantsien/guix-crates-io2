(define-module (crates-io im xr imxrt-usbd) #:use-module (crates-io))

(define-public crate-imxrt-usbd-0.1.0 (c (n "imxrt-usbd") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "imxrt-ral") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0m4mkd6cbag25i8m3z0ym0jmb6ln3z3hcfabnn16xv5cd6xm1v7f") (f (quote (("__log" "log"))))))

(define-public crate-imxrt-usbd-0.2.0 (c (n "imxrt-usbd") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "17d01hl4f2p3fclz776vmx9rk7sfj2dh04rb6vhjw5qbnbhvfzqn") (f (quote (("__log" "log"))))))

(define-public crate-imxrt-usbd-0.2.1 (c (n "imxrt-usbd") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "01b4d97w8a1a1bvq1skn1q7203yj7zfcb3d9cjxpw982nsbwvari") (f (quote (("__log" "log"))))))

(define-public crate-imxrt-usbd-0.2.2 (c (n "imxrt-usbd") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "10rbjiinm8awz6rh1syxb4601yqcgr14ihpx6b8amyliwcqjq8f1") (f (quote (("__log" "log"))))))

(define-public crate-imxrt-usbd-0.3.0 (c (n "imxrt-usbd") (v "0.3.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "ral-registers") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)))) (h "16ijnvf206h1fjglr1pkv6d6yggg36p8y3g9b8yr1kfh8qv5zq01") (s 2) (e (quote (("defmt-03" "dep:defmt-03" "usb-device/defmt"))))))

