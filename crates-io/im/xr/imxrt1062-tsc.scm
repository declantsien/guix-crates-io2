(define-module (crates-io im xr imxrt1062-tsc) #:use-module (crates-io))

(define-public crate-imxrt1062-tsc-0.1.0 (c (n "imxrt1062-tsc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0wbxg20fhk82jkx80d7rj6zhkw6dy7ji96xsli07yc9fp588xyzq")))

(define-public crate-imxrt1062-tsc-0.1.1 (c (n "imxrt1062-tsc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pczjmc0g0dghyk8nm027rw8srp02fc4r40bwrppsridymcdngb8")))

