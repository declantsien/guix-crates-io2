(define-module (crates-io im xr imxrt1062-gpc) #:use-module (crates-io))

(define-public crate-imxrt1062-gpc-0.1.0 (c (n "imxrt1062-gpc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "043hycnbic72dzbavgajcfykrm3pfav06hrg5h16fvln91vsrm3y")))

(define-public crate-imxrt1062-gpc-0.1.1 (c (n "imxrt1062-gpc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0vhzh6xj8qwspn39ql086j0zsrzp1vlhynqycgjakcymm27qicxf")))

