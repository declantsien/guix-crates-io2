(define-module (crates-io im xr imxrt1062-ccm) #:use-module (crates-io))

(define-public crate-imxrt1062-ccm-0.1.0 (c (n "imxrt1062-ccm") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1g7zilbwvrzz88v9ibdhay6pdih5jxaa8j7shsxi82kj28230mgn")))

(define-public crate-imxrt1062-ccm-0.1.1 (c (n "imxrt1062-ccm") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09b9fskbdjs4l75zlkh3vackp499635sf4acn4am81fzaizpilbh")))

