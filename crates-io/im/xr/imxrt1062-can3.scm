(define-module (crates-io im xr imxrt1062-can3) #:use-module (crates-io))

(define-public crate-imxrt1062-can3-0.1.0 (c (n "imxrt1062-can3") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19j73lpbmlwg5blnnsnfmzn8vmww0xifik1nnag4rrgcfkl4mx08")))

(define-public crate-imxrt1062-can3-0.1.1 (c (n "imxrt1062-can3") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xchwp3bnrv2nddq2f8xwb2dspyjnf46a2zhrlr30pwrg7zkmviv")))

