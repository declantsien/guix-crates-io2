(define-module (crates-io im xr imxrt1062-lcdif) #:use-module (crates-io))

(define-public crate-imxrt1062-lcdif-0.1.0 (c (n "imxrt1062-lcdif") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19360pdzzlj7zzfg7yqjd2wnff37yxqv5x12qcpmhz2282lsxrg1")))

(define-public crate-imxrt1062-lcdif-0.1.1 (c (n "imxrt1062-lcdif") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1fm45bfddi55inzgsszmkifzqpqq55dyk7s5931vd822qbxhbb9i")))

