(define-module (crates-io im xr imxrt1062-semc) #:use-module (crates-io))

(define-public crate-imxrt1062-semc-0.1.0 (c (n "imxrt1062-semc") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "10psy1qvv9r02lapwx9pnxa7czi9chizkhvmx17jpv6zxdc4i89y")))

(define-public crate-imxrt1062-semc-0.1.1 (c (n "imxrt1062-semc") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vcqwynx1p3aap86p2mdqwph9szi3m065slmaqallrv5cr10v07p")))

