(define-module (crates-io im xr imxrt-dcd) #:use-module (crates-io))

(define-public crate-imxrt-dcd-0.1.0 (c (n "imxrt-dcd") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "047rv3q9ljapwnvga2qgyk264y5hqzb57mw1jxk276sjrvyxysik") (y #t)))

(define-public crate-imxrt-dcd-1.0.0 (c (n "imxrt-dcd") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "13gm8zqci6ijg3d60qm41sxar98kgalazfz65vx1a37p9wc274mi")))

(define-public crate-imxrt-dcd-1.0.1 (c (n "imxrt-dcd") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0n07k4f60aplycsxchcp4p1k23fjdym257ql81bnl6sw1i7ja6x1")))

(define-public crate-imxrt-dcd-1.1.0 (c (n "imxrt-dcd") (v "1.1.0") (d (list (d (n "imxrt-ral") (r "^0.5.3") (f (quote ("imxrt1062"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0fda53pkdj5sy94jbahm0w610wfafa4m3vi0s59babxxa5b1irs6") (f (quote (("ral") ("default" "ral"))))))

