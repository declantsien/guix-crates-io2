(define-module (crates-io im xr imxrt1062-bee) #:use-module (crates-io))

(define-public crate-imxrt1062-bee-0.1.0 (c (n "imxrt1062-bee") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rnd31lja6ii07w2da2k2c51dqfg5h4jq9d9qwi209q3s8qg6c7g")))

(define-public crate-imxrt1062-bee-0.1.1 (c (n "imxrt1062-bee") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14pi159q5dy0a0pbpm23hh5yn0yf3hi7qk7l7lxgjcfgqynlnjlk")))

