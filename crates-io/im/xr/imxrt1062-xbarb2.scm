(define-module (crates-io im xr imxrt1062-xbarb2) #:use-module (crates-io))

(define-public crate-imxrt1062-xbarb2-0.1.0 (c (n "imxrt1062-xbarb2") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fbdy4v87za16maxwmh0a0vs5r3ljfrz6xac3sdvilngc065ahl8")))

(define-public crate-imxrt1062-xbarb2-0.1.1 (c (n "imxrt1062-xbarb2") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16iq6qbhrfgcsymbzq9040dccbnn68yw4g811snjm8wlf68nhkk7")))

