(define-module (crates-io im xr imxrt1062-flexram) #:use-module (crates-io))

(define-public crate-imxrt1062-flexram-0.1.0 (c (n "imxrt1062-flexram") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lm964mkx5y1yp486wd8fy8v9dx6q31dr2nxwnarp21vnzbma0hb")))

(define-public crate-imxrt1062-flexram-0.1.1 (c (n "imxrt1062-flexram") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19qs33czzyq7v0qia1g7jyvr0mxjkj1nnvrwl7qg7prhpd6braz1")))

