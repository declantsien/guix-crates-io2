(define-module (crates-io im xr imxrt1062-src) #:use-module (crates-io))

(define-public crate-imxrt1062-src-0.1.0 (c (n "imxrt1062-src") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1yvhs7bgkv7378z9c6853mmc9k3d2np6bs5xvmc4y1mn7d5719b0")))

(define-public crate-imxrt1062-src-0.1.1 (c (n "imxrt1062-src") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1kas59a4fdyxm84jsffni4lab3j5rygfir5pdpzc9j6m83ji8bdj")))

