(define-module (crates-io im -p im-pathtree) #:use-module (crates-io))

(define-public crate-im-pathtree-0.0.1 (c (n "im-pathtree") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1q7616qbk6d7yzfi5af1m0f1cn009q7iwb5g4p2jdmnai9p12gi3")))

(define-public crate-im-pathtree-0.0.2 (c (n "im-pathtree") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1jzzj8fyl43gkp59km8niyp0bqd7smd8sfwc2n3xn9zsga6x4hcq")))

(define-public crate-im-pathtree-0.0.3 (c (n "im-pathtree") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "15mkqdqcnr5m4fzf1dy7qk95f1ymfmgx8gnk0w58s8l7fsxj0327")))

(define-public crate-im-pathtree-0.0.4 (c (n "im-pathtree") (v "0.0.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "17clsrgids25yyvzf51xm9x8032gslzac18a2mkg0q70kr5r5av2")))

(define-public crate-im-pathtree-0.0.5 (c (n "im-pathtree") (v "0.0.5") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0b8mw87p9x0kayrdyz44z70gzxa9avm9pschdxa5c51xx64vss51")))

(define-public crate-im-pathtree-0.0.6 (c (n "im-pathtree") (v "0.0.6") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0krqy3lhzg3l04j1bh750f69lg0vmhgg0yimg7dwgc1h006kgj9i")))

(define-public crate-im-pathtree-0.0.7 (c (n "im-pathtree") (v "0.0.7") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "08c37r7vfis3xi1vyqz8hbqqdx79jh7j159k5p7q6hfsn3jkcwxv") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.8 (c (n "im-pathtree") (v "0.0.8") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1dfd31fplb1ynmqkv3ynrb3f23wmzh81am0c0c65zkg3iww597x9") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.9 (c (n "im-pathtree") (v "0.0.9") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0107j9i9f3kw54wylmqxsfhbxdawhwpx1jprlwks15agyn0p6w3k") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.10 (c (n "im-pathtree") (v "0.0.10") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "07ph46i81x6k9zf2axlq7fa5gr7bmx8ry3w8h3ya2bpljg3m1xfk") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.11 (c (n "im-pathtree") (v "0.0.11") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "089rgrv5ypk3q06n5dd8mhqz8d6bwri3kf5yhs7zg6yzy9w2fvwn") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.12 (c (n "im-pathtree") (v "0.0.12") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1j1l7x8jaq5iih0231ymkmx53yilb8yq6sccbxam38kgp6jl2qgz") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.13 (c (n "im-pathtree") (v "0.0.13") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1z5557ca2ayg66ri3cjdpnxjm7pln884drz306b6rh2g0skrnibk") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.14 (c (n "im-pathtree") (v "0.0.14") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xj2v3gw24gdzncsfzb99kdnasm5f0dxiwlfz1kl1gj7qw1y1fm7") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.15 (c (n "im-pathtree") (v "0.0.15") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1z9iwyabfrnb3cyp3xzc1cx8jhfc00l6myi61ipq2f7zzxcj3775") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.16 (c (n "im-pathtree") (v "0.0.16") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0glda6yxp5i3vfif91xa763qln7qws9h5izgaw0i0fk19vxfb0v8") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.17 (c (n "im-pathtree") (v "0.0.17") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0dibcjvjv3iyrix6rmjq4dwvisprisplzc6z779627c3qykip86h") (f (quote (("default" "im")))) (y #t) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.18 (c (n "im-pathtree") (v "0.0.18") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09yia9saaxw1ib5hncd8jb4l6gqf56z7wwxvx057x67s2434km9r") (f (quote (("default" "im")))) (y #t) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.19 (c (n "im-pathtree") (v "0.0.19") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1126bzb30i6v3li39bz1hjr218fl0f0h385adqcmaah9vhka1wxn") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.20 (c (n "im-pathtree") (v "0.0.20") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1r1jf9jakpcid9ziay20l61n8ib2dcg7pmjnis7i3v7s9cm5rp75") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.21 (c (n "im-pathtree") (v "0.0.21") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0695b0iy6fymgx4gy1a8i2l5xkrd6mzhq7h15f3bmhxv9l73j63x") (f (quote (("default" "im")))) (y #t) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.0.22 (c (n "im-pathtree") (v "0.0.22") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0f6vzcz8a8nkwxv1vp0xpfr5gqdld0vwndpi0c0vb0is8rxq00gc") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.1.0 (c (n "im-pathtree") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0vd9sqp58jfb55rl3c60sn4gbpflhh80lrxl1kayx3jdkk2wk2gh") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.1.1 (c (n "im-pathtree") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0vkhf3m7hj6900z2x7znbd078mvrjp2mxaqwrlwdab8myb78ig57") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.1.2 (c (n "im-pathtree") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ca1124lq5m5wbwqpki301yig4vbc8xzcj3nclj93s9rw29xwciq") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.1.3 (c (n "im-pathtree") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1w3dkdvwdllknk1kw2lr9zw3z1d2818x80g032wagjw408yfh8xc") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

(define-public crate-im-pathtree-0.2.0 (c (n "im-pathtree") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "186dapy2jfhqqqxl2sfly86xjph5a123pqqcgcz4c1kg195cb8bj") (f (quote (("default" "im")))) (s 2) (e (quote (("im" "dep:im"))))))

