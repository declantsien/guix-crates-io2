(define-module (crates-io im gl imglife) #:use-module (crates-io))

(define-public crate-imglife-1.0.0 (c (n "imglife") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "08zqffyq3y5fid02ixcfk31x5iayg447mhxgiywapsh2q0mh2jh6")))

(define-public crate-imglife-1.0.1 (c (n "imglife") (v "1.0.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1s4rc22q91b7dxnm9p290ihnbfawyxywwgxm6gs913rihi3h5afn")))

(define-public crate-imglife-1.0.2 (c (n "imglife") (v "1.0.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0lsivc09jlnb6ilvpbji054fsj7dmy0adc7jjkwfzdz124zsj301")))

(define-public crate-imglife-1.0.3 (c (n "imglife") (v "1.0.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "16ahlccwfda5j9di04cm7r2qf7mpnxv52n8f3sm7ckv2gswxkwl4")))

(define-public crate-imglife-1.0.4 (c (n "imglife") (v "1.0.4") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1gvzxdall1gn9wiiygzqg5dfgg59wwyi4kzajjbn7pxfv19w6sm6")))

(define-public crate-imglife-1.0.5 (c (n "imglife") (v "1.0.5") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "12ljnvl96vhwwrjgp05h1lg87q8fwm7d9ibv0kp649vd42qlix9j")))

(define-public crate-imglife-1.0.6 (c (n "imglife") (v "1.0.6") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1vcy0qi0mkk11fphjm57h65px1iz6cq84hnp8x2rs5ichmgdk35p")))

(define-public crate-imglife-1.0.7 (c (n "imglife") (v "1.0.7") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1zydwwi2ii14j5ym8i8ajvqywl4rx2ya0q6az7c34f9p60gdika5")))

