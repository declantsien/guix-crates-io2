(define-module (crates-io im bu imbue) #:use-module (crates-io))

(define-public crate-imbue-0.1.0 (c (n "imbue") (v "0.1.0") (h "14rr4xmppx03l1yjp9h5mgwjkvb8al0q18cq89y3la66r4bqpl65")))

(define-public crate-imbue-0.1.1 (c (n "imbue") (v "0.1.1") (h "085w2p0xyjx9ixh586rik286sx6hj4w4kyxxa2pvbjqvyz37nb7f")))

