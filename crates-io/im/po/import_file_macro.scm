(define-module (crates-io im po import_file_macro) #:use-module (crates-io))

(define-public crate-import_file_macro-0.1.0 (c (n "import_file_macro") (v "0.1.0") (d (list (d (n "comptime") (r "^0.1.1") (d #t) (k 0)) (d (n "hex-string") (r "^0.1.0") (d #t) (k 0)))) (h "0ny6yrm2qhhsj89nph2h0flr1c2z7zy3mi2bpxld7g7mq483wf2d")))

(define-public crate-import_file_macro-0.1.1 (c (n "import_file_macro") (v "0.1.1") (d (list (d (n "comptime") (r "^0.1.1") (d #t) (k 0)) (d (n "hex-string") (r "^0.1.0") (d #t) (k 0)))) (h "1ms4cmy9bp71l6xxmhnwccw64k7psqdshkladqr8wg6wxlbsn0mb")))

