(define-module (crates-io im po impostor_cli) #:use-module (crates-io))

(define-public crate-impostor_cli-0.1.0 (c (n "impostor_cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "impostor_compiler_axum") (r "^0.1.0") (d #t) (k 0)) (d (n "impostor_core") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l8734iv8cwdlfiq2cgnh08fxdyx9iy1ma6h4rb75agjvl85cf5f")))

