(define-module (crates-io im po imposter-pass) #:use-module (crates-io))

(define-public crate-imposter-pass-0.2.1 (c (n "imposter-pass") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qaw09apws05ax4s592psjp13r2yhs5vi2wh8j1mmixff61vrpig")))

(define-public crate-imposter-pass-0.3.0 (c (n "imposter-pass") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jsv57j45jz810r6x32qidknwpq1pfd9bf87b2zfr6z1byhrqiq5")))

