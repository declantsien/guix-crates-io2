(define-module (crates-io im po impostor_core) #:use-module (crates-io))

(define-public crate-impostor_core-0.1.0 (c (n "impostor_core") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "08q2zz0yvxassq0j93i8jx3s8652l4zahs36r61qg0mbb7kcx0aw")))

