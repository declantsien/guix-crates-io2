(define-module (crates-io im po import-modules) #:use-module (crates-io))

(define-public crate-import-modules-0.1.0 (c (n "import-modules") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "11rhlrazrkkmgz85vm63a3jf4jjdnrxm81cfyxa3kli7b2djyvyq")))

(define-public crate-import-modules-0.1.1 (c (n "import-modules") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)))) (h "0yc0b1r08ymmhbrbi1kwrp3m467mlsfq6n3p2bqrqfmnzyniny9m")))

(define-public crate-import-modules-0.1.2 (c (n "import-modules") (v "0.1.2") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)))) (h "0iyammj60ifq7x408zmh70b3zwsza4rp85dnjnjd1vq7fpy4fiz6")))

(define-public crate-import-modules-0.1.3 (c (n "import-modules") (v "0.1.3") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)))) (h "1663gi7ccq0xqxhyn847g1mx0yrbzni8v7bd6p9vp2nib4a3jry0")))

(define-public crate-import-modules-0.1.4 (c (n "import-modules") (v "0.1.4") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)))) (h "0kwhhxpc8ccsj739sjvhnf2cxbdhpd3xabwg8spxla4x5ra5nbbp")))

(define-public crate-import-modules-0.1.5 (c (n "import-modules") (v "0.1.5") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)))) (h "1vp5qjvi9kms689lq6m2bd3sj2qyk284yxflxb2v6wdmq9igps7p")))

(define-public crate-import-modules-1.0.0 (c (n "import-modules") (v "1.0.0") (d (list (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ql33z6ky4ymcxbb3nnb2236g0xxghd1pacwp4ch3xf5lckxw58n")))

