(define-module (crates-io im po impose) #:use-module (crates-io))

(define-public crate-impose-0.1.0 (c (n "impose") (v "0.1.0") (d (list (d (n "rodio") (r "^0.5") (d #t) (k 0)))) (h "1zqci9mj4yxc08bbsr2v5qn3p03dm561ba5pyr0ha2sjrqbn8p70")))

(define-public crate-impose-0.2.0 (c (n "impose") (v "0.2.0") (d (list (d (n "rodio") (r "^0.6") (d #t) (k 0)))) (h "1ya54wl00s92971yfkj6xs1gdc284g4xcanl5qnyh46zzjya748z")))

(define-public crate-impose-0.3.0 (c (n "impose") (v "0.3.0") (d (list (d (n "rodio") (r "^0.8") (d #t) (k 0)))) (h "148hxijb7fk9f7q59g5rp1sd762wjy6adwlqblw0zd4h495b0xh3")))

