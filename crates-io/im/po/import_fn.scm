(define-module (crates-io im po import_fn) #:use-module (crates-io))

(define-public crate-import_fn-0.1.0 (c (n "import_fn") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1b3l7akwf65cd1l7cnv981cyk1zdlaszw5c66wf4zh6f95blhqnk")))

(define-public crate-import_fn-0.1.1 (c (n "import_fn") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "080zq8piqr0wnpjr90nisb2ni96skfnna3nnn23mbr5cr04c2z32")))

(define-public crate-import_fn-0.1.2 (c (n "import_fn") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0aby2l3qxyzswgxgb7hyql8jhrb8s8dsvjzyspy0mwq8r1wxyrmm")))

