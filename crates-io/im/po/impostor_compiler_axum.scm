(define-module (crates-io im po impostor_compiler_axum) #:use-module (crates-io))

(define-public crate-impostor_compiler_axum-0.1.0 (c (n "impostor_compiler_axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "axum-extra") (r "^0.9.0") (f (quote ("cookie"))) (d #t) (k 0)) (d (n "impostor_core") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "1n4gfgr51mhrrz1dqfw7h0cv0n6qp5mcnz6axnswwa55js3yqqxp")))

