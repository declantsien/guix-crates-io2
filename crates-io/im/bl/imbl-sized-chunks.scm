(define-module (crates-io im bl imbl-sized-chunks) #:use-module (crates-io))

(define-public crate-imbl-sized-chunks-0.1.0 (c (n "imbl-sized-chunks") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "03c2scj57gq03lc9bwdyp778vr1pp1vmps4wmhvbwh3yhvqz6zyc") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-imbl-sized-chunks-0.1.1 (c (n "imbl-sized-chunks") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0xhhmb7aldl92hxkmsx10n59zxsa0hw4bvykc6jmq72lnah7x5g6") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

(define-public crate-imbl-sized-chunks-0.1.2 (c (n "imbl-sized-chunks") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0qzdw55na2w6fd44p7y9rh05nxa98gzpaigmwg57sy7db3xhch0l") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

