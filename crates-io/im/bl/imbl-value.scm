(define-module (crates-io im bl imbl-value) #:use-module (crates-io))

(define-public crate-imbl-value-0.1.0 (c (n "imbl-value") (v "0.1.0") (d (list (d (n "imbl") (r "2.*") (f (quote ("serde" "small-chunks"))) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "treediff") (r "4.*") (o #t) (d #t) (k 0)) (d (n "yasi") (r "^0.1") (d #t) (k 0)))) (h "0359ddri9h524ssdyzbyri6x6pf6z6x94mhrnsm4db6ivy63svcb") (f (quote (("default"))))))

