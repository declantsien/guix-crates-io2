(define-module (crates-io im sz imsz) #:use-module (crates-io))

(define-public crate-imsz-0.2.0 (c (n "imsz") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xbk40v1m7i9bh93ys2ik981hpx176b5pbwyrfy991vhigx4n94w")))

(define-public crate-imsz-0.2.1 (c (n "imsz") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 2)))) (h "088m10anz2c0dbx3yzwlgiwx2pliykmlrai42pwd5345nala86qs") (r "1.59")))

(define-public crate-imsz-0.2.2 (c (n "imsz") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zrz6nnfn2ps3zqyhrawncgbhldvgvjf3w8x4jxbql47pjp9x93n") (r "1.59")))

(define-public crate-imsz-0.3.1 (c (n "imsz") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 2)))) (h "0amyhszlzskzvsw8zdqvy5hhimn5i4fab75ax6m3nks6cc7h4nsx") (r "1.59")))

