(define-module (crates-io im ex imex) #:use-module (crates-io))

(define-public crate-imex-0.1.0 (c (n "imex") (v "0.1.0") (h "1ggnljn5syw3kw1i9dxz5ldpljacv6hk94d72nrir6a4kydxc9w6")))

(define-public crate-imex-0.2.0 (c (n "imex") (v "0.2.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "0bpdrhj8y39gg0rx8pv7f3rhyhs1lvchyf4mwv9k1aykf0yh3h8n")))

(define-public crate-imex-0.2.1 (c (n "imex") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "1c7pi4zlx1rpwj412fs39f8lg27szv5w2l4ng5zymymk94ywsk30")))

