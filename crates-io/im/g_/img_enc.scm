(define-module (crates-io im g_ img_enc) #:use-module (crates-io))

(define-public crate-img_enc-0.1.0 (c (n "img_enc") (v "0.1.0") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "03vcjvg7vhrqirgib7vpyaskia9gjcz7wa4h2zjw4r0knldxdrwh")))

(define-public crate-img_enc-0.1.1 (c (n "img_enc") (v "0.1.1") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1v3z1icq2vlk7ilvnmbs6fiprbr7dv747r885ydr5c1wcg9dxxqv")))

(define-public crate-img_enc-0.1.2 (c (n "img_enc") (v "0.1.2") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0ajiziy8bq4s25z9mrh1h5d3khmykvf0bcx5b93nrs8p3rq0z0qn")))

