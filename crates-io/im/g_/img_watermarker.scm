(define-module (crates-io im g_ img_watermarker) #:use-module (crates-io))

(define-public crate-img_watermarker-0.0.0 (c (n "img_watermarker") (v "0.0.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)))) (h "1npmkydfq4blhf6xarym6hrcyz3s03ihdzfkq3llghq0l3dnrwq3")))

(define-public crate-img_watermarker-0.0.1 (c (n "img_watermarker") (v "0.0.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)))) (h "15pah36jvpkagjfzvs1hp3krlhpby0x8m7j8jpvxw3fj9gh6mn74")))

