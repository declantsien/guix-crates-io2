(define-module (crates-io im g_ img_hash) #:use-module (crates-io))

(define-public crate-img_hash-0.0.2 (c (n "img_hash") (v "0.0.2") (d (list (d (n "image") (r "*") (d #t) (k 0)))) (h "028r89khww8n38rb8vs0a99fqsmixakzfr8ya2kszgbp33vx02bj") (y #t)))

(define-public crate-img_hash-0.5.0 (c (n "img_hash") (v "0.5.0") (d (list (d (n "image") (r "*") (o #t) (d #t) (k 0)) (d (n "image") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "03mpcc8zm0gnli88kzh48j5f12c59adqszwp1qx30y9wkqcnc2fc") (f (quote (("rust-image" "image"))))))

(define-public crate-img_hash-1.0.0 (c (n "img_hash") (v "1.0.0") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)) (d (n "image") (r "*") (o #t) (d #t) (k 0)) (d (n "image") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "stream-dct") (r "*") (f (quote ("cos-approx"))) (d #t) (k 0)))) (h "1idgqzqcha05b089vfca2dmyha1ymmabqrjdhdm2y6advfyi5933") (f (quote (("rust-image" "image") ("bench"))))))

(define-public crate-img_hash-2.0.0 (c (n "img_hash") (v "2.0.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (o #t) (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1k8l6gyfrl9xldy80zaq30vb98ziiigg4343bpnmj72z5zm8x2vv") (f (quote (("rust-image" "image") ("default" "rust-image") ("bench"))))))

(define-public crate-img_hash-2.0.1 (c (n "img_hash") (v "2.0.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (o #t) (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0knxqg36xn3x7f1xhcrp9ziwpmygd2d1apjsrsmb6xc54jspns8z") (f (quote (("rust-image" "image") ("default" "rust-image") ("bench"))))))

(define-public crate-img_hash-2.0.2 (c (n "img_hash") (v "2.0.2") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (o #t) (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.13") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "18fk3hj0zq5mp4pgrxxl1pzgg5ipbhfiaa3fxrf1lvlxqqadivhr") (f (quote (("rust-image" "image") ("default" "rust-image") ("bench"))))))

(define-public crate-img_hash-2.1.0 (c (n "img_hash") (v "2.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.19") (o #t) (d #t) (k 0)) (d (n "image") (r ">= 0.10, <= 0.19") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1packnls4dcb3vlqlys35ddywhf6qjf0vas8ah3q2ajai065aiwk") (f (quote (("rust-image" "image") ("default" "rust-image") ("bench"))))))

(define-public crate-img_hash-3.0.0 (c (n "img_hash") (v "3.0.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "image") (r ">= 0.21, < 0.23") (d #t) (k 0)) (d (n "rustdct") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.1") (d #t) (k 0)))) (h "0vcrrkdgsadpc8f834lccjqj9wsz5ir0g5xwi12sjszq813rc2cx") (f (quote (("nightly"))))))

(define-public crate-img_hash-3.1.0 (c (n "img_hash") (v "3.1.0") (d (list (d (n "base64") (r ">= 0.10, < 0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r ">= 0.21, < 0.24") (d #t) (k 0)) (d (n "rustdct") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "0ysjlf60rzi6s75y0bgifzy4c4k99y93hghlcdf1cypki2ycbxld") (f (quote (("nightly"))))))

(define-public crate-img_hash-3.1.1 (c (n "img_hash") (v "3.1.1") (d (list (d (n "base64") (r ">=0.10, <0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r ">=0.21, <0.24") (k 0)) (d (n "rustdct") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "0v8czrih23q9gnnzzz3yf8wnwl0glsm0zcnmkimw6g2v2m5pl1aj") (f (quote (("nightly"))))))

(define-public crate-img_hash-3.1.2 (c (n "img_hash") (v "3.1.2") (d (list (d (n "base64") (r ">=0.10, <0.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r ">=0.21, <0.24") (k 0)) (d (n "rustdct") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "0mjz308biif6wc77ijwgcx680ckn9nykyg1w22zwihd5i07rhkp8") (f (quote (("nightly"))))))

(define-public crate-img_hash-3.2.0 (c (n "img_hash") (v "3.2.0") (d (list (d (n "base64") (r ">=0.10, <0.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r ">=0.21, <0.24") (k 0)) (d (n "rustdct") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "0j8blz1riwl7v13w6nm2v3fsbgs7nwr0f8aw7lvfsr2gzk3fm92y") (f (quote (("nightly"))))))

