(define-module (crates-io im g_ img_hash_median) #:use-module (crates-io))

(define-public crate-img_hash_median-4.0.0 (c (n "img_hash_median") (v "4.0.0") (d (list (d (n "base64") (r ">=0.10, <0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r ">=0.21, <0.24") (d #t) (k 0)) (d (n "rustdct") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "0lsv971x7qfhban2zyzqzh9s5gh3a2icbj8pa0m6zvq5d6xhwphj") (f (quote (("nightly"))))))

