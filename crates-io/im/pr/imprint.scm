(define-module (crates-io im pr imprint) #:use-module (crates-io))

(define-public crate-imprint-0.1.0 (c (n "imprint") (v "0.1.0") (h "17mjp42dv8axhvwh9r36q6a5j2xj2amfqs4hkhdsjfl66gpsk6mz")))

(define-public crate-imprint-0.2.0 (c (n "imprint") (v "0.2.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.32") (d #t) (k 0)))) (h "00wp3mwr0jv001bmn3x9rsgwyin8f5x8k7cawb6119wjfsrafin2")))

