(define-module (crates-io im pr impral) #:use-module (crates-io))

(define-public crate-impral-0.1.0 (c (n "impral") (v "0.1.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)) (d (n "tagged-box") (r "^0.1.1") (f (quote ("57bits"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1csk6c1vkm6cbm3948xdrinzdrb1nlqbiv598mbr7wipn1j9i5c4") (y #t)))

(define-public crate-impral-0.1.1 (c (n "impral") (v "0.1.1") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)) (d (n "tagged-box") (r "^0.1.1") (f (quote ("57bits"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0kz5lqw3k5l429983kwg0wqdkl5cf188sk5m7802l7z6gz9spgaz")))

(define-public crate-impral-0.1.2 (c (n "impral") (v "0.1.2") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)) (d (n "tagged-box") (r "^0.1.1") (f (quote ("57bits"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0l9cqdpz7r5m4y40j9waazk111zj2jzvbm104fz8dvn4a2ind3pg")))

(define-public crate-impral-0.1.3 (c (n "impral") (v "0.1.3") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)) (d (n "tagged-box") (r "^0.1.1") (f (quote ("57bits"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "194533dhxwwah0zxv1zqm87ca146d2jk204ih66wqvrajskfwrzf")))

(define-public crate-impral-0.1.4 (c (n "impral") (v "0.1.4") (d (list (d (n "nanval") (r "^0.1.1") (d #t) (k 0)) (d (n "peekmore") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "tagged-box") (r "^0.1.1") (f (quote ("57bits"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0xinagbsgcqbv9nfcas7g9n94siffsf5vgyng03npp5q8r31ci16")))

(define-public crate-impral-0.1.5 (c (n "impral") (v "0.1.5") (d (list (d (n "peekmore") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07wjhjmhs08gk36bzzb0g03yvab779vbjn1ia1nph1sd011zg370")))

(define-public crate-impral-0.1.6 (c (n "impral") (v "0.1.6") (d (list (d (n "peekmore") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (d #t) (k 0)))) (h "1rg5ffdqbk7855kmihcblsd5wnagsff3gd83i6jwixync587ynvs")))

