(define-module (crates-io im pr impromptu) #:use-module (crates-io))

(define-public crate-impromptu-0.0.1 (c (n "impromptu") (v "0.0.1") (d (list (d (n "cached") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 0)) (d (n "git2") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1xnhyysmka7c6a1q2f7si2z7a8p4jyjb1fgdsjzlp1hv97v262si")))

