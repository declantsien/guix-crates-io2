(define-module (crates-io im pr improved_slice_patterns) #:use-module (crates-io))

(define-public crate-improved_slice_patterns-1.0.0 (c (n "improved_slice_patterns") (v "1.0.0") (h "1c6j175xhb8rb05sbjw8vaqkazijl5b5hsg7g15jk01c5m0iyjzw")))

(define-public crate-improved_slice_patterns-1.0.1 (c (n "improved_slice_patterns") (v "1.0.1") (h "0w3vxc8jp9z1hi4j9wv6ykj71mwdhrhzm1716ik445l8w79vrnk3")))

(define-public crate-improved_slice_patterns-2.0.0 (c (n "improved_slice_patterns") (v "2.0.0") (h "02gi9aigf7dkf5a9zfz9wqk093i4h8sx62br4viiyr2kd2k47i2y")))

(define-public crate-improved_slice_patterns-2.0.1 (c (n "improved_slice_patterns") (v "2.0.1") (h "11ig5s23ygdh66gfznjr0qscpdr4g5ikv8a04lghmxmz9lbrd2s1")))

