(define-module (crates-io im ei imei) #:use-module (crates-io))

(define-public crate-imei-1.0.0 (c (n "imei") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "09n7zg2zb4dv1lmacji7l9fqhh5c8g5di100qyimjh5ja30ikkg0")))

(define-public crate-imei-1.0.1 (c (n "imei") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1w1xfz7h8rfp5msffdpaff0iz2jaz7v465ybfrxwn5lq5hsll681")))

(define-public crate-imei-1.0.2 (c (n "imei") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "02ps8g049k315q8dijdgvj62crnhk1l7z3ax5zr6kl61r6a5bbdz")))

(define-public crate-imei-1.1.0 (c (n "imei") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1mj1csj5b0hw2wqcnwvzm64986sl2jja4y5385qjhyh3pyzalmdi") (f (quote (("std") ("default" "std"))))))

(define-public crate-imei-1.1.1 (c (n "imei") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0faqr6d1nzrz6b2z5llv77sxjcmdg3hyrpaci0hd99mximbnxf19") (f (quote (("std") ("default" "std" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

