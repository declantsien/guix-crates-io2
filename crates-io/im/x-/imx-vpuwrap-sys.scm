(define-module (crates-io im x- imx-vpuwrap-sys) #:use-module (crates-io))

(define-public crate-imx-vpuwrap-sys-0.1.0 (c (n "imx-vpuwrap-sys") (v "0.1.0") (h "0k3jamq0wy7jbjiwvlbf5s2g4x1ik6254sclkgmign84qnq35vxm")))

(define-public crate-imx-vpuwrap-sys-0.1.1 (c (n "imx-vpuwrap-sys") (v "0.1.1") (h "0q9jfyaw2q1mcip7r20ygp81cc6v31hcwwix2sb7z0xhr712knhi")))

