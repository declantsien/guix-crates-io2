(define-module (crates-io im x- imx-vpuwrap-rs) #:use-module (crates-io))

(define-public crate-imx-vpuwrap-rs-0.1.0 (c (n "imx-vpuwrap-rs") (v "0.1.0") (d (list (d (n "imx-vpuwrap-safe") (r "^0.1.0") (d #t) (k 0)))) (h "15ppsgi6g3n4qpipp7rhw2hl6ji6xdykiz6i4h90lj5ccizib9rd")))

(define-public crate-imx-vpuwrap-rs-0.1.1 (c (n "imx-vpuwrap-rs") (v "0.1.1") (d (list (d (n "imx-vpuwrap-safe") (r "^0.1.1") (d #t) (k 0)))) (h "0405g8lc7n7by94d86vl5i1rjwl4dggv4qpgkcb8sba89m6sphjl")))

