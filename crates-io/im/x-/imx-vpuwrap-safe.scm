(define-module (crates-io im x- imx-vpuwrap-safe) #:use-module (crates-io))

(define-public crate-imx-vpuwrap-safe-0.1.0 (c (n "imx-vpuwrap-safe") (v "0.1.0") (d (list (d (n "imx-vpuwrap-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0zs8jgf717pp4gxyv3r93737zs8xgvca8rf5zp6d33r8kjjbhcj8")))

(define-public crate-imx-vpuwrap-safe-0.1.1 (c (n "imx-vpuwrap-safe") (v "0.1.1") (d (list (d (n "imx-vpuwrap-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0fkz5xlpvawlv9w6xcbk18grd7h6fc5i6qzdhy39i0gnk2dy36ls")))

