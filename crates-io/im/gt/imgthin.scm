(define-module (crates-io im gt imgthin) #:use-module (crates-io))

(define-public crate-imgthin-0.1.0 (c (n "imgthin") (v "0.1.0") (h "11jqwq6cvg2lsikz0i7l9qbq8k2hphayggaajf24hdzdws9n5hlq") (f (quote (("improved_ysc_whh") ("default"))))))

(define-public crate-imgthin-0.1.1 (c (n "imgthin") (v "0.1.1") (h "1nq01ynm4i5xb8b31vnp2rcc2qbzqljf9gd15rnwg3771wpnmwyx") (f (quote (("improved_ysc_whh") ("default"))))))

