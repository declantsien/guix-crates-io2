(define-module (crates-io im gt imgtiger) #:use-module (crates-io))

(define-public crate-imgtiger-1.0.0 (c (n "imgtiger") (v "1.0.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1dqmwl6c1j0kbwc670r7pr8af5xsjh5k15913vx52jfaah53mwx2")))

(define-public crate-imgtiger-1.0.2 (c (n "imgtiger") (v "1.0.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1rwx9a7id4wgvi5nwvn6f7370j9bh1mpl4mrgrllmrnr7swgwppl")))

(define-public crate-imgtiger-1.0.3 (c (n "imgtiger") (v "1.0.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1jg63bcbvrv28167gsky2l53iqb93spw09j93fjg0cjl5kghihfr")))

