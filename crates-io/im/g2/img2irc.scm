(define-module (crates-io im g2 img2irc) #:use-module (crates-io))

(define-public crate-img2irc-0.1.0 (c (n "img2irc") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oklab") (r "^1.0.0") (d #t) (k 0)))) (h "035i3l8zw8d8nl03vpxbwxz1wbhblch166ys2za9xjzwqd2yy0n0")))

(define-public crate-img2irc-0.1.1 (c (n "img2irc") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oklab") (r "^1.0.0") (d #t) (k 0)))) (h "0bw9r37mpxhvjmb9iwdrsgri6xzldfjr7l5p3bl029afjk2xi1b3")))

(define-public crate-img2irc-0.2.0 (c (n "img2irc") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oklab") (r "^1.0.0") (d #t) (k 0)))) (h "1dipfwxzfqzyd2b6h21g5zlzf8yhs4ric85hqa4jcnl89wmn3v9p")))

(define-public crate-img2irc-0.3.0 (c (n "img2irc") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oklab") (r "^1.0.0") (d #t) (k 0)))) (h "1zkq92hkfmccmvzkki6in5rvpdqv733c9n0mz0678a49pylznpc7")))

