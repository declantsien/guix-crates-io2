(define-module (crates-io im g2 img2txt-rs) #:use-module (crates-io))

(define-public crate-img2txt-rs-1.0.0 (c (n "img2txt-rs") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.30") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1qi61vxp1nsilf98yzfvy4mjfcn0iby7h0jqs9cl612ki7klpyv8")))

