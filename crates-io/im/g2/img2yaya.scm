(define-module (crates-io im g2 img2yaya) #:use-module (crates-io))

(define-public crate-img2yaya-0.1.0 (c (n "img2yaya") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1v779s1gr8c2x2m1wfdk8blcqyklp3x3js1kx20d2innmii25i94")))

(define-public crate-img2yaya-0.1.1 (c (n "img2yaya") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1nnsfpacz25l88if6iwal89fdr9wlwfhynxbgclrrrpfvssgp0df")))

