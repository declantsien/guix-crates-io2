(define-module (crates-io im g2 img2raw) #:use-module (crates-io))

(define-public crate-img2raw-0.1.0 (c (n "img2raw") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "half") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "10vfivi1frjz6ianl480pm3gx4j83w5a83ymm5yi0brdrmssap04")))

(define-public crate-img2raw-0.1.1 (c (n "img2raw") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "half") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "1rq9ssdb7rmnj8wrxa3fcv6jgn3sfgd1sd07skjv1m89vi1aysq4")))

(define-public crate-img2raw-0.2.1 (c (n "img2raw") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "half") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "1nbs446zhfh0zwlkbblfp278ij5zmvx2zgxqny1rv8jxdxikg0zd")))

(define-public crate-img2raw-0.2.2 (c (n "img2raw") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "half") (r "^1.3") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "0xa9y2k8z2wmmd389160j8ykwfi242ncq61bpakr2wmxiiq23wnb")))

(define-public crate-img2raw-0.3.0 (c (n "img2raw") (v "0.3.0") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "03g9dhvzd01fkf7s4n7pjsvhy05jmml8ywfmjvjn6cdarjrcx7xa") (f (quote (("default" "zerocopy"))))))

(define-public crate-img2raw-0.3.1 (c (n "img2raw") (v "0.3.1") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15lbfzgzqpr3g42mshw90fx5bghq099zk0c1ryi4ww18y889ig16") (f (quote (("default" "zerocopy"))))))

(define-public crate-img2raw-0.3.3 (c (n "img2raw") (v "0.3.3") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0arb20csbvz7fbwzcd2l7h666892gb2a51n0n7jpv9jasyxgrwha") (f (quote (("default" "zerocopy"))))))

(define-public crate-img2raw-0.3.4 (c (n "img2raw") (v "0.3.4") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11yyrjm6hjdccv72dkv7rfdpjykdl390km132llzkpwnk4gpjbk1") (f (quote (("default" "zerocopy"))))))

(define-public crate-img2raw-0.4.0 (c (n "img2raw") (v "0.4.0") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0sbv3vh459darchz68zk2vxjkqzba1pspm9qxz67ai9m4sbfs5zz") (f (quote (("default" "zerocopy"))))))

(define-public crate-img2raw-0.3.5 (c (n "img2raw") (v "0.3.5") (d (list (d (n "zerocopy") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1br1dqv2v7lm6x74jkhcw27mhh0gb0d4x9y6f8x63ygah74bvibk") (f (quote (("default" "zerocopy"))))))

