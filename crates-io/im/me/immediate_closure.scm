(define-module (crates-io im me immediate_closure) #:use-module (crates-io))

(define-public crate-immediate_closure-0.1.0 (c (n "immediate_closure") (v "0.1.0") (h "1wc7n2mng30mf457gycb0kmmcal4mqp9qpz12c9w0a7ms12k6n67") (y #t)))

(define-public crate-immediate_closure-0.1.1 (c (n "immediate_closure") (v "0.1.1") (h "1nzdnvahs3yyh1wnvlinfl207k4hbiy1mxvw5nrckm68wffa1vav")))

