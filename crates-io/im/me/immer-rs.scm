(define-module (crates-io im me immer-rs) #:use-module (crates-io))

(define-public crate-immer-rs-0.1.0 (c (n "immer-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wa159apm0d9a4fv9ml7wwmg6j45qdpgnaanpyzyk26rs2wrci1f")))

(define-public crate-immer-rs-0.1.1 (c (n "immer-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zdp9bv7j7rmf9v5p09cx3vrmk8y6sg4ray5z6cf0d4zw02irc8k") (y #t)))

(define-public crate-immer-rs-0.1.2 (c (n "immer-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "096qbpmmm4h1dlc6i7kq3dvf5hvs2qafaf5bl0bgz87r4zxlxj56")))

