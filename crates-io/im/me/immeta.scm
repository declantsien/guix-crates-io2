(define-module (crates-io im me immeta) #:use-module (crates-io))

(define-public crate-immeta-0.1.0 (c (n "immeta") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0fwmyap9bm58lalchp7xyi443nx4rrbavajrabdw048sb9lihrvi")))

(define-public crate-immeta-0.2.0 (c (n "immeta") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "0jd7awr43n4v30fvkcgwnl4wazkjy29y5cahjjiq3hddrn6a8wc3")))

(define-public crate-immeta-0.2.1 (c (n "immeta") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "1gwdzzczr6vcplg5y1b0alx79plb411lcc89ds03znhyqc99qvkk")))

(define-public crate-immeta-0.2.2 (c (n "immeta") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "1lwn5iam72wy517l80kqjqwb7fydsr90zkiv288rjl9179cc3wmq")))

(define-public crate-immeta-0.2.3 (c (n "immeta") (v "0.2.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "0i5mn8z808mfs9lbbb8c78pqfz7hx68rl7ddkysps26c282lap2y")))

(define-public crate-immeta-0.2.4 (c (n "immeta") (v "0.2.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "0x5zan5fdzr8ivvlmwrfzh36i36aa2xmxpgwyfq0cndi8bs7azl7")))

(define-public crate-immeta-0.3.0 (c (n "immeta") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "1rshprppj9cssjrwc6xw27f4j6nmqfjfcj3yqybpzir4cil682dw")))

(define-public crate-immeta-0.3.1 (c (n "immeta") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "0qvdfb2c4hvbicrhbjdy3x4k8hbhr48d2dcjqxsr44g0rybqx1hv")))

(define-public crate-immeta-0.3.2 (c (n "immeta") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "06xc3yfpsafplhqldq6qqfwhh6klyj8vl07pmagb46kf04daxc1x")))

(define-public crate-immeta-0.3.3 (c (n "immeta") (v "0.3.3") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0fbpfj300r6pmqnnycmp88i83bp1489lkkjyhg4p1a9bzbq3sw79")))

(define-public crate-immeta-0.3.4 (c (n "immeta") (v "0.3.4") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1xsyhid8m1w85d7h8ngppwy7wipibr4w1dy5gwfajya9ssqyqxiy")))

(define-public crate-immeta-0.3.5 (c (n "immeta") (v "0.3.5") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "00ima8miyaqv18ys8i3yzrghs1acs7i5c30h08zzw6r2793614hb")))

(define-public crate-immeta-0.3.6 (c (n "immeta") (v "0.3.6") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0dw3fysc3yji8bdanjmsiia913f0nbr1bkkig22kqcn7zdbsbahs")))

(define-public crate-immeta-0.4.0 (c (n "immeta") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12ri5ad781x51wqljwn7n0r3qna5xphy45xmv7i0vmpsk0yalwbk")))

