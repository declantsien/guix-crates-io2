(define-module (crates-io im vi imview) #:use-module (crates-io))

(define-public crate-imview-0.1.0 (c (n "imview") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "pallete") (r "^1.0.0") (d #t) (k 0)))) (h "0s1xxscgkk4rlhbydgf52g7ffxq9s4l9yyvv0pmmb1kiq4yhlddi")))

