(define-module (crates-io im bo imboard) #:use-module (crates-io))

(define-public crate-imboard-0.1.0 (c (n "imboard") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0ky2ml6is9377a5rdv7p88h0rcdlqbjbwq0hy0r4n4i9wv6qd0mp")))

(define-public crate-imboard-0.1.1 (c (n "imboard") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0dn1v9s7zwlpqgjcc5687qs9fpha8r1sh6zqalbrcz3rq74h8i6z")))

(define-public crate-imboard-0.1.2 (c (n "imboard") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "03j8kq7sg5w3xf9gwll44816jcl2b3s25jn0jcg5gzs833l74xid")))

(define-public crate-imboard-0.2.0 (c (n "imboard") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4hs13fckakdb4h2lfvzdf4xya870ivs8bcxs897nvkdkqqxlnc")))

(define-public crate-imboard-0.2.1 (c (n "imboard") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0cy5947ldjd7s1cnc2rjn9lfv5lfw47vp6fbfdr77ymnr2b5qwg9")))

(define-public crate-imboard-0.2.2 (c (n "imboard") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "03qzd07lgsg3vyfmm5xi54kf7n1vrmly3z7a431aldibi2cnk0zj")))

(define-public crate-imboard-0.2.3 (c (n "imboard") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1y0ld9867bm8qgmr5i5i4111mnqwb0x4ngnncjiilcywr7qma3yf")))

(define-public crate-imboard-0.2.4 (c (n "imboard") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0ki0jn8xwlv6qzvv23lvj13ywi760qfgj7m5cmxw0rh0b7x5vxz2")))

(define-public crate-imboard-0.2.5 (c (n "imboard") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1qa9rwmp3k68gqgsmlp41ymwiqdd1kkva0yrnw92rkq245np2bfp")))

(define-public crate-imboard-0.2.6 (c (n "imboard") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0417zbcj2bzfy3bpsxmyzcfzd22lwxq3l11l4lali0h7qkk52s4g")))

