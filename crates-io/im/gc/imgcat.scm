(define-module (crates-io im gc imgcat) #:use-module (crates-io))

(define-public crate-imgcat-0.1.0 (c (n "imgcat") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0z1bppcq34vy0nivjxipxz7dsgbzddnj7i6s6yndl7vwbw95s7ch")))

