(define-module (crates-io im gc imgcatr) #:use-module (crates-io))

(define-public crate-imgcatr-0.1.0 (c (n "imgcatr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16l4yafqds128vfql7v96qshs73pabcvsb1143y302ji55r9f9jl")))

(define-public crate-imgcatr-0.1.1 (c (n "imgcatr") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wlhh93v1kyd3bhapj0pb1b5vcis9abqxhw9d9vqbclkfh58b8gk")))

(define-public crate-imgcatr-0.1.2 (c (n "imgcatr") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1pici3l2kcrmqjxd02x6nnf38yhw3641jzckab38iww99iv71wqk")))

(define-public crate-imgcatr-0.1.4 (c (n "imgcatr") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0czlhylb4qllmrn55mh1f2cfgc6li3fv58vxcyjsn5qzhvw9i7cl")))

