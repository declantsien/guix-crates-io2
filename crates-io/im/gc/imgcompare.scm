(define-module (crates-io im gc imgcompare) #:use-module (crates-io))

(define-public crate-imgcompare-0.1.0 (c (n "imgcompare") (v "0.1.0") (d (list (d (n "image") (r "^0.14") (d #t) (k 0)))) (h "0aickhjbh709bks7wv3385dykyvjkm985yx3w83d1d4h70n2dyp7")))

(define-public crate-imgcompare-0.1.1 (c (n "imgcompare") (v "0.1.1") (d (list (d (n "image") (r "^0.14") (d #t) (k 0)))) (h "1mq5m6g1vps98shbbvsv0nhm6mh3rif7jkvk05zwygh7lpvlywsr")))

