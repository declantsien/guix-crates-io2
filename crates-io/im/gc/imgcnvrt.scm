(define-module (crates-io im gc imgcnvrt) #:use-module (crates-io))

(define-public crate-imgcnvrt-0.1.0 (c (n "imgcnvrt") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)))) (h "05fbhwvw02kwb4xbgdm7i9jmbnxyzr0gd8zb8m290kzrpzrksmqr")))

(define-public crate-imgcnvrt-0.2.0 (c (n "imgcnvrt") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)))) (h "1rjjxf1bhwf6ap0ilr1kvmf608kdn66qq04ma3rzmq7v87jdyapf")))

