(define-module (crates-io im gp imgproc) #:use-module (crates-io))

(define-public crate-imgproc-0.1.0 (c (n "imgproc") (v "0.1.0") (d (list (d (n "yuv-sys") (r "^0.1.0") (d #t) (k 0)))) (h "14xyn66yfsnjfm1gg4q5vcc8flhwz27bj8vv6iy7354wm9dyj00k")))

(define-public crate-imgproc-0.1.1 (c (n "imgproc") (v "0.1.1") (d (list (d (n "yuv-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1r0jmk6p62vh492ib4yyw5xw2vz7xpg99cqqbwik9krvlxir909y")))

(define-public crate-imgproc-0.3.0 (c (n "imgproc") (v "0.3.0") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "02i3rqfgz4r7dwrhymb7f1paw8sdkndx0ix38x1svkw4fx6r5v12")))

(define-public crate-imgproc-0.3.1 (c (n "imgproc") (v "0.3.1") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1dajgyiq8p5hp90lkjd50s7y5mxdfr1yq0azxnmwjgk652pljl86")))

(define-public crate-imgproc-0.3.2 (c (n "imgproc") (v "0.3.2") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "137njxawgzps0f3mw81zmba02f5sm83zn83q7ka5c9ig9hlrvm3z")))

(define-public crate-imgproc-0.3.3 (c (n "imgproc") (v "0.3.3") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "09v7y4ja96dq0bwfx8zqifjyyykxdmrdgxlz3khmawz6x62rc4qj")))

(define-public crate-imgproc-0.3.4 (c (n "imgproc") (v "0.3.4") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1j20ynrikzmiax0pjvzzjv79pi5zhvfpss22y2l1gvsrixpcx6qf")))

(define-public crate-imgproc-0.3.5 (c (n "imgproc") (v "0.3.5") (d (list (d (n "yuv-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0yywxzvx6km9xadnwccvk0fac55z69sy7jkdij12nlhbwgsrqflx")))

(define-public crate-imgproc-0.3.6 (c (n "imgproc") (v "0.3.6") (d (list (d (n "yuv-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0hd3pk879hgzgvzcbc4r9iigg9sqbzpdakd37spyc5qxzq37156f")))

(define-public crate-imgproc-0.3.7 (c (n "imgproc") (v "0.3.7") (d (list (d (n "yuv-sys") (r "^0.3.2") (d #t) (k 0)))) (h "1b3a6831mq4vavhz1q7wcblwp6gwmyv441d8rdjqkwvglsl5q3nw")))

(define-public crate-imgproc-0.3.8 (c (n "imgproc") (v "0.3.8") (d (list (d (n "yuv-sys") (r "^0.3.3") (d #t) (k 0)))) (h "0p4sg2h7x4hx9yqq0xfmx8rij35xb3kpp808bjfyailk1w4nhvw3")))

(define-public crate-imgproc-0.3.9 (c (n "imgproc") (v "0.3.9") (d (list (d (n "yuv-sys") (r "^0.3.4") (d #t) (k 0)))) (h "14ah425qvy4jwkqfayyqqjk2q2wk23v2jlbrcvigj8ndzhvyimmb")))

(define-public crate-imgproc-0.3.10 (c (n "imgproc") (v "0.3.10") (d (list (d (n "yuv-sys") (r "^0.3.5") (d #t) (k 0)))) (h "16d02lm6q4wj78dh9qbr9f4prbymh9nqgllhwakcjb6cwqry1y5i")))

(define-public crate-imgproc-0.3.11 (c (n "imgproc") (v "0.3.11") (d (list (d (n "yuv-sys") (r "^0.3.6") (d #t) (k 0)))) (h "14chpjnjmvvqhihlc86jk0px976x7bay2mhyr61bb248094j3qnc")))

