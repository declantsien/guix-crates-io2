(define-module (crates-io im gp imgproc-rs) #:use-module (crates-io))

(define-public crate-imgproc-rs-0.1.0 (c (n "imgproc-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "1j3401i40fl0d6m6fc1xdhyavcab3gg4mwkc5rqcbsfdys36s9wk")))

(define-public crate-imgproc-rs-0.1.1 (c (n "imgproc-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "16azcgnrykj9xcxrkvf6f741hm8ars162j2jw15m01qvi95w6n5x")))

(define-public crate-imgproc-rs-0.2.0 (c (n "imgproc-rs") (v "0.2.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "17wvzbl0hxfqlxqay69mm7xa11dqqxg9dqd6c39hsv50jlzp047g")))

(define-public crate-imgproc-rs-0.2.1 (c (n "imgproc-rs") (v "0.2.1") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "1xvs1s342aa2jn4xjzj43xly33qswnaassymvmlkriydgx41ii93") (f (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.2.2 (c (n "imgproc-rs") (v "0.2.2") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "18fm4k01lqyp3gh4xfib321jba3n8iwkfw991y985ykfff9xmrvz") (f (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.2.3 (c (n "imgproc-rs") (v "0.2.3") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "0ikccj65fk2z0l46mznapk2vpy1gqvd80c3xw871dkmpvl95kdy9") (f (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.3.0 (c (n "imgproc-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "0n9ilk17ihdzq4n7m137zg4spcz5xbgw5bgqy1abr32b9hdw17jy") (f (quote (("simd") ("parallel" "rayon") ("default" "simd"))))))

