(define-module (crates-io im mu immutable-seq) #:use-module (crates-io))

(define-public crate-immutable-seq-0.1.0 (c (n "immutable-seq") (v "0.1.0") (d (list (d (n "debug_unreachable") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "0yr3x7hp41ivjrwg57wra827j8viw5bvc3jdbb3511vf6ljcj7kw")))

(define-public crate-immutable-seq-0.1.1 (c (n "immutable-seq") (v "0.1.1") (d (list (d (n "debug_unreachable") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "05irm8b3ip6rf4w7bfsd1ssj06499dgn12l7dmnhka3jcp8bfkkl")))

(define-public crate-immutable-seq-0.1.2 (c (n "immutable-seq") (v "0.1.2") (d (list (d (n "debug_unreachable") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "1h9hjkgz6jnvp7ivgxlshrn7lvhk4d0np8x31y39w7mrmx4yi2qx")))

