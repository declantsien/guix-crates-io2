(define-module (crates-io im mu immutable_arena) #:use-module (crates-io))

(define-public crate-immutable_arena-0.1.0 (c (n "immutable_arena") (v "0.1.0") (d (list (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "07gq4qy1mfqg5b25nvbw18jlqb8s2jbg79l3vsl2lmfcsvzq9cnw")))

(define-public crate-immutable_arena-0.1.1 (c (n "immutable_arena") (v "0.1.1") (d (list (d (n "typed-arena") (r "^1.2.0") (d #t) (k 0)))) (h "0kkkik5xf5kf64r47adyqcvp8rgj1cfz2f0rdh9z4ympq7y9p6mw")))

