(define-module (crates-io im mu immutable_string) #:use-module (crates-io))

(define-public crate-immutable_string-0.1.0 (c (n "immutable_string") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "weak-table") (r "^0.3.0") (d #t) (k 0)))) (h "1v290kqcn9hjwanv336hagjykja6bnihq78nrgh94yhl76xngscm")))

(define-public crate-immutable_string-0.1.1 (c (n "immutable_string") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "weak-table") (r "^0.3.0") (d #t) (k 0)))) (h "16iq8jchzk5f31k1545b3r52ca4nrkkacsr49qapq0jsjnniiy9k")))

