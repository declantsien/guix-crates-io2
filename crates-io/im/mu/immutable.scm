(define-module (crates-io im mu immutable) #:use-module (crates-io))

(define-public crate-immutable-0.1.0 (c (n "immutable") (v "0.1.0") (h "08nbhfgk820zq47awnvasrimmink9wsmdi6sq1ls3l4p1nrw374d")))

(define-public crate-immutable-0.1.1 (c (n "immutable") (v "0.1.1") (h "1brf4v60x0q83hnrzwdy9g35wk0rn8q9zlpbyxh1ycbdaj8m4fxf")))

