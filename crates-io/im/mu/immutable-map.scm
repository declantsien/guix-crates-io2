(define-module (crates-io im mu immutable-map) #:use-module (crates-io))

(define-public crate-immutable-map-0.1.0 (c (n "immutable-map") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "007albp8nd30hd9lv558n67y0sqzzzzq8az9spack92ym93kwxfh")))

(define-public crate-immutable-map-0.1.1 (c (n "immutable-map") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rjgh3ag15nhiy1xji7idrs39ljwyhpwm5s7y67k8cwfpwvrd5vq")))

(define-public crate-immutable-map-0.1.2 (c (n "immutable-map") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "05j1bzkn1081y3ldmg641mf8mnpic7rkwjws5ydk4g1kzbgkb546")))

