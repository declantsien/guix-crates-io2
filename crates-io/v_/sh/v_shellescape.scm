(define-module (crates-io v_ sh v_shellescape) #:use-module (crates-io))

(define-public crate-v_shellescape-0.3.2 (c (n "v_shellescape") (v "0.3.2") (d (list (d (n "v_escape") (r "^0.4") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0472a6rmg8lpafsdlsz8841k28k1qr3diwj6i1aha3ab3gza3wlb")))

(define-public crate-v_shellescape-0.3.3 (c (n "v_shellescape") (v "0.3.3") (d (list (d (n "v_escape") (r "^0.7") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 1)))) (h "1ak8x43dv7rq3wvj6rjji0bfy4w57id2sjssl7rsv8x87c5b3ds1")))

(define-public crate-v_shellescape-0.3.4 (c (n "v_shellescape") (v "0.3.4") (d (list (d (n "v_escape") (r "^0.7") (d #t) (k 0)))) (h "0bpzn0bdgi8gf66fm6h1favr2zhbxq6a71y068kff2awpj3vb8yx")))

(define-public crate-v_shellescape-0.3.5 (c (n "v_shellescape") (v "0.3.5") (d (list (d (n "v_escape") (r "^0.7") (d #t) (k 0)))) (h "1sxr6waz0bgmsnv44drxw29q4r1wlyx5kdlm0npvn4ynrqr8cz25")))

