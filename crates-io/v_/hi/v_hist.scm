(define-module (crates-io v_ hi v_hist) #:use-module (crates-io))

(define-public crate-v_hist-0.1.0 (c (n "v_hist") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "174wrii50clah713jvps2qxw72n8d607cbxfih8vphqvr2yq1vvg")))

(define-public crate-v_hist-0.1.1 (c (n "v_hist") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1240nhmh63c36w1m7y5jfrszxap59ca8nn8qlzs4ip2j1bph9brg")))

(define-public crate-v_hist-0.1.2 (c (n "v_hist") (v "0.1.2") (d (list (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1c8wdl4zvq83jikiky45r2phc2qw9kd96sbigfixsb07gqna9r8i")))

(define-public crate-v_hist-0.1.3 (c (n "v_hist") (v "0.1.3") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vvkgk39v6bpz9miyswkc8q9s04pvmga8730ph54ga9hqffhb0jc")))

