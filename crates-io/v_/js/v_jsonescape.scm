(define-module (crates-io v_ js v_jsonescape) #:use-module (crates-io))

(define-public crate-v_jsonescape-0.1.0 (c (n "v_jsonescape") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.11") (d #t) (k 0)))) (h "0xn26gf8j0g95rlir0yhgwrj6mpqmspl6vdc4q96274p6ldbzg16")))

(define-public crate-v_jsonescape-0.2.0 (c (n "v_jsonescape") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.12") (k 0)))) (h "19ni5frwyzf04j5ggwmlhx0pvv4n11lh92wpxxwv12hlhgmghchn") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2.1 (c (n "v_jsonescape") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.12") (k 0)))) (h "0a0idwg1ppd7xwn73ay4q9f08h5bkkdrrmxnsjf62c1arb5537kw") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2.2 (c (n "v_jsonescape") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "0z51kg32ky8l2zcspcikg3jg4h93lir9212wjzq6awij60zszf91") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2.3 (c (n "v_jsonescape") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "020zs2jcbzfwcry6z3xd5xl9163y913bphkiym1gs2978pq2sym6") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2.4 (c (n "v_jsonescape") (v "0.2.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "121yzyx4yg6vnz0rf5g4y2lgqmcaybxgx7a0g690rrm7f5c88h6c") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.3.0 (c (n "v_jsonescape") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.14") (k 0)))) (h "0ljcnra58yk2c0yrwk1wjq6ckx48rhm90da32v0qig1slaabl48l") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.4.0 (c (n "v_jsonescape") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.15") (k 0)))) (h "1s94y37viysb3sd9nzmf8yanhgc5sqqrs46phr24x6fpysv0nfks") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.5.0 (c (n "v_jsonescape") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.16.0") (k 0)))) (h "1d8asbsaqxwarjzjgwwqwdhjkkzdqrqb3p7j3hd36im3wgb53spg") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.5.1 (c (n "v_jsonescape") (v "0.5.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.16.1") (k 0)))) (h "04x17s15swi6vligp8qh532a5abcw0y6822ly3825fpsvph4b743") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.6.0 (c (n "v_jsonescape") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.17") (d #t) (k 0)))) (h "1dpaqmq99vanz0lcc0c2n00h5q4w0zmr63wcyj6a248gns3zrdcp")))

(define-public crate-v_jsonescape-0.6.1 (c (n "v_jsonescape") (v "0.6.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.18") (d #t) (k 0)))) (h "1z92gyd35aj9rgcc266b2mjcs7nlq0a436zsfkgxz58v6nqz3h39")))

(define-public crate-v_jsonescape-0.7.0 (c (n "v_jsonescape") (v "0.7.0") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0bzhaqzifscj80d9c7xs4708pa77czdcfkmsrm6cn4ymbkvaa427") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.1 (c (n "v_jsonescape") (v "0.7.1") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0ayhsx05388n93hiz7zgx4l2j8kx7ld293w3ybnhm9s2mgc6qfjc") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.2 (c (n "v_jsonescape") (v "0.7.2") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "14vj1wvmkw82xjwhlbazkk8fl6j5q3wdhry80m31hqyjqhc4l8sm") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.3 (c (n "v_jsonescape") (v "0.7.3") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "11ywb100cs68cxzd4xn2yp4s4q8hj24wj4wr5h4p18m5lxy98zia") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.5 (c (n "v_jsonescape") (v "0.7.5") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0avr8mjzymdfa23p9ixs0xaw8rrvssp7ynapv2l4d3nnz4ydvl7z") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.6 (c (n "v_jsonescape") (v "0.7.6") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0zqq753vy547igpxc1d0c8dkz0yvjp26430v0n9mjvpg6z104c4z") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.7 (c (n "v_jsonescape") (v "0.7.7") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1awwqi8p7czhsx21ydzij3a6y3idw4zccyzsd9hjxcsp76glmfsn") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7.8 (c (n "v_jsonescape") (v "0.7.8") (d (list (d (n "buf-min") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1s0hpfkpwf7agr34fnn4qlqxh9hxy5qnh6i3qd40r8ab8v61k0my") (f (quote (("bytes-buf" "buf-min"))))))

