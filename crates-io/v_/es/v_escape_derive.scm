(define-module (crates-io v_ es v_escape_derive) #:use-module (crates-io))

(define-public crate-v_escape_derive-0.1.0 (c (n "v_escape_derive") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1c1misnlcgg01mlc72bb647ndxb5l0r9f3cqgnjb0zwysvphhhq5")))

(define-public crate-v_escape_derive-0.2.0 (c (n "v_escape_derive") (v "0.2.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "06b72iwbdl65c8kiaabzxq3vrr0yd6cypgr3rma8ig9ciis6p7hy")))

(define-public crate-v_escape_derive-0.2.1 (c (n "v_escape_derive") (v "0.2.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0k7lrg99qjgvr58m5sijggg297jbq173kr9immxfyzs87g399nbw")))

(define-public crate-v_escape_derive-0.3.0 (c (n "v_escape_derive") (v "0.3.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0bm15zilj099i7cnnxdz7k2vhxcrkzdbam7npqd69nqmjmcgyj5m")))

(define-public crate-v_escape_derive-0.4.0 (c (n "v_escape_derive") (v "0.4.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ga0g997lq2sgndkslvmcky1v28icqaj1xk3z512pcg603gm1i7b")))

(define-public crate-v_escape_derive-0.4.1 (c (n "v_escape_derive") (v "0.4.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ww5jb6ispj6nbj9yc0winlj9bby7wxvqpap4kiv0nfn45lgawdn")))

(define-public crate-v_escape_derive-0.5.0 (c (n "v_escape_derive") (v "0.5.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ycb63g3m2fsfgb7qkxa3axicpkca9318lw2wqgg9jkjmdwf74j5")))

(define-public crate-v_escape_derive-0.5.1 (c (n "v_escape_derive") (v "0.5.1") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ycg9z4sgys0zhzmrlky4q94151da541813nlryaxfbjmiwqf5v8")))

(define-public crate-v_escape_derive-0.5.2 (c (n "v_escape_derive") (v "0.5.2") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qcf4y5r46q4xl7gjd5sm6y30sr1j0h6jk6lh1d8p8q5fdjbhwrk")))

(define-public crate-v_escape_derive-0.5.3 (c (n "v_escape_derive") (v "0.5.3") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08wfllrnmikmvy5qrzk83pi45r3d8l0c079s4q0qb6bm9gzrcs1h")))

(define-public crate-v_escape_derive-0.5.4 (c (n "v_escape_derive") (v "0.5.4") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rblrmaw3k2amyd25ih30y3sszrm8g3m9ilag95siscl5dap2m48")))

(define-public crate-v_escape_derive-0.5.5 (c (n "v_escape_derive") (v "0.5.5") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0xi271ic4jkqwh8m89pllj67v0m380qjcd5zi127nkf1s58drckm")))

(define-public crate-v_escape_derive-0.5.6 (c (n "v_escape_derive") (v "0.5.6") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1bk7qhh2wrv6pk7ir51f2mxzhzcj79ypl24b316vdi9zpha2mjn2")))

(define-public crate-v_escape_derive-0.6.0 (c (n "v_escape_derive") (v "0.6.0") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1p3sypmnbbh7njsr5484v41vzbx0lar55fys5z9j40p81745j8xc")))

(define-public crate-v_escape_derive-0.7.0 (c (n "v_escape_derive") (v "0.7.0") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1kdy946d24m79zj70s8n7d4cxzkbwczwjq3kbaggk523cp8ipjnk")))

(define-public crate-v_escape_derive-0.7.1 (c (n "v_escape_derive") (v "0.7.1") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0mk97n48gyjrysgzndwrgjx3ajfl28c1qfg668jc61fvhl0x4qx8")))

(define-public crate-v_escape_derive-0.8.0 (c (n "v_escape_derive") (v "0.8.0") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0s6f31q042xqq69wmxpf1hhpgd35mgsk9d7dbqlgs7qlfd6za7xy") (y #t)))

(define-public crate-v_escape_derive-0.8.1 (c (n "v_escape_derive") (v "0.8.1") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1k949s8dr0nlln3d2nkv85hwxz4zxvg6y3v142dszydil3yczrya")))

(define-public crate-v_escape_derive-0.8.2 (c (n "v_escape_derive") (v "0.8.2") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "054qyyllgrcrj40vmzk162q2x5m0vwnff457b39746x5c0v0dmi3")))

(define-public crate-v_escape_derive-0.8.3 (c (n "v_escape_derive") (v "0.8.3") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hl60w3svn5g8k3kb2m5dqpi79l8fskn5xqzdkn7d2v7x0fkjjmq")))

(define-public crate-v_escape_derive-0.8.4 (c (n "v_escape_derive") (v "0.8.4") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s1n9xbqxall20pa6950rcj5s3p6r4hdn1gfdh0fgvplfc9asq68")))

(define-public crate-v_escape_derive-0.8.5 (c (n "v_escape_derive") (v "0.8.5") (d (list (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "133c55mansr4a76iz6zm0ikffc3998d9c7459fa69cpq1906k5zj")))

(define-public crate-v_escape_derive-0.9.0 (c (n "v_escape_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1vychbyg4vnxcd49nm9yipm7r9wv23lwxhv0gzz2q0kd809blfx5")))

(define-public crate-v_escape_derive-0.9.1 (c (n "v_escape_derive") (v "0.9.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02cq71waq0f2i9mqjbjc34vfc3bqx4gfky25cpfnsbj7jkqirs4g")))

