(define-module (crates-io v_ es v_escape) #:use-module (crates-io))

(define-public crate-v_escape-0.1.0 (c (n "v_escape") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1.4") (d #t) (k 1)))) (h "0pgq9xr013fwmiwakr5bx6s1hrygkkfm5aq52nj5sxwl688lkchz") (y #t)))

(define-public crate-v_escape-0.1.1 (c (n "v_escape") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1.4") (d #t) (k 1)))) (h "1z1qvwj8zaifkfq9rfyh85rk8pxw6dkd98swrrn82s6xba551xrm") (y #t)))

(define-public crate-v_escape-0.1.2 (c (n "v_escape") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "19h9fmbgg3nahmay4pq904n81nv6cdsafdyy3wh5wrgap4m6myzy") (y #t)))

(define-public crate-v_escape-0.1.3 (c (n "v_escape") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1mx6x93396rvi9vhvgj6nffv1rjbvdckgf2gx1fslymgi8ngbpnp") (y #t)))

(define-public crate-v_escape-0.1.4 (c (n "v_escape") (v "0.1.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "06m1nk4jdni3x1h3hziq6vfxavf1mg9jgjsnxr5yls7hrgkrgp6x") (y #t)))

(define-public crate-v_escape-0.2.0 (c (n "v_escape") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0lz6hgab9bcpamq5m1s2265p1811hsls2kmbh9541bngfhw22172") (y #t)))

(define-public crate-v_escape-0.3.0 (c (n "v_escape") (v "0.3.0") (d (list (d (n "v_escape_derive") (r "^0.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1mjq268b4r5zzhg6z7vzzbsrdd64awzi581681a2kffs1mb6cysl")))

(define-public crate-v_escape-0.3.1 (c (n "v_escape") (v "0.3.1") (d (list (d (n "v_escape_derive") (r "^0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1hg5clf0fjjlrajzdavakf1yfyvh8k4c7lm35jvfwwnpzrm3jjrp")))

(define-public crate-v_escape-0.3.2 (c (n "v_escape") (v "0.3.2") (d (list (d (n "v_escape_derive") (r "^0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0518kbh9g7l0adv8j03i36izmc04l2yyi7qs584lqvxqxn40ddf8")))

(define-public crate-v_escape-0.4.0 (c (n "v_escape") (v "0.4.0") (d (list (d (n "v_escape_derive") (r "^0.3") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "02g81rfdq24cdl0zj6fhmcb4hvyqib3hlqbrlk04j4s0zb9q912p")))

(define-public crate-v_escape-0.5.0 (c (n "v_escape") (v "0.5.0") (d (list (d (n "v_escape_derive") (r "^0.4") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "18cmn85jlnv7cjwpqswsf9qycr7hqhgshcsavlsih2dp61d5cxv1")))

(define-public crate-v_escape-0.5.1 (c (n "v_escape") (v "0.5.1") (d (list (d (n "v_escape_derive") (r "^0.4") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0v2vq22h0pcyskkx93xmgs1a7fz2vzakhqlsi9f5hmarw5z6636z")))

(define-public crate-v_escape-0.6.0 (c (n "v_escape") (v "0.6.0") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0xbd8yjk5j5f3x1i3r4vy3cpf3aaq7yagpykdz5pv7ghvcgpw61q")))

(define-public crate-v_escape-0.7.0 (c (n "v_escape") (v "0.7.0") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1wvy3kpdhhk8jkznj2kzsgzpmrxgvj6gji43706r465x4r350g4p")))

(define-public crate-v_escape-0.7.1 (c (n "v_escape") (v "0.7.1") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1vx9ffrw4jswqk56fac625bw4xryryb9ggl851pcy3zqs5bzg3lf")))

(define-public crate-v_escape-0.7.2 (c (n "v_escape") (v "0.7.2") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0balygndhngwsra8iagbc9b9x25s337nlj253cy1kygfg0dm0rc8")))

(define-public crate-v_escape-0.7.3 (c (n "v_escape") (v "0.7.3") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "09zqm3p8g0sp4qgkspvgihwf8ahg3bli0h36vpqmsccdkwnk5832")))

(define-public crate-v_escape-0.7.4 (c (n "v_escape") (v "0.7.4") (d (list (d (n "v_escape_derive") (r "^0.5") (d #t) (k 0)))) (h "19kdgk3iqzd295h9x7x7sbb5is3phw9v6zwyxcyqdl5m0wf102v6")))

(define-public crate-v_escape-0.8.0 (c (n "v_escape") (v "0.8.0") (d (list (d (n "v_escape_derive") (r "^0.6") (d #t) (k 0)))) (h "1q12hv6vdwimq0f2wfrkslv8fdd5pkc33xaf0ym8n62k27gymajh")))

(define-public crate-v_escape-0.9.0 (c (n "v_escape") (v "0.9.0") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "0nx1yajkql45k09xp0wicvxzh0zpghbchyxxhqr1by5m2qp929bj")))

(define-public crate-v_escape-0.9.1 (c (n "v_escape") (v "0.9.1") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "0mk0blgcdkvypk8bx7cfg2hh4vvv1apllqj7bahmhi1w825jrn7k")))

(define-public crate-v_escape-0.9.2 (c (n "v_escape") (v "0.9.2") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "1ip42q9rnimc6sf2mcaw6w9xy4ikkv972msgq1c06f4352vlgbbc")))

(define-public crate-v_escape-0.9.3 (c (n "v_escape") (v "0.9.3") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "1681y5si57i12kzg5rb836cagxyy7qw3pj9qyzhmzc0sinp0ddrr")))

(define-public crate-v_escape-0.9.4 (c (n "v_escape") (v "0.9.4") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "0036hhz5j51mbc3i3w4iqrnwc1fcnrg1n0rr3rv7rs2kpz0y544v")))

(define-public crate-v_escape-0.10.0 (c (n "v_escape") (v "0.10.0") (d (list (d (n "v_escape_derive") (r "^0.7") (d #t) (k 0)))) (h "0l176f2m0vr7kamq3lf1a0ax6n1k8n3cndrw4rd7357dhh0vh6bz")))

(define-public crate-v_escape-0.11.0 (c (n "v_escape") (v "0.11.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "19df06mnx3s2863yrd262n36xqqngbhr27m0xj8fgdvl6szghx1w") (y #t)))

(define-public crate-v_escape-0.11.1 (c (n "v_escape") (v "0.11.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "0vmlik3qgziaydzv4xhr0xpz8j1zhdbjdaqz74x7hsn14yal0prs")))

(define-public crate-v_escape-0.11.2 (c (n "v_escape") (v "0.11.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "08ppd9jwn4z7sikwy2q3qjd1n74gizm5l8rdk77znhffqn6p8yxs") (y #t)))

(define-public crate-v_escape-0.11.3 (c (n "v_escape") (v "0.11.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "0x5pcdsgb120jhqnnjyhxf42xxrgk0y1dzwcijq20n6bmac9061j")))

(define-public crate-v_escape-0.12.0 (c (n "v_escape") (v "0.12.0") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "0900dd5hnl1by0zf6pigskqh0h7x4n0xcrj2sizik0k98b75hqdn") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.12.1 (c (n "v_escape") (v "0.12.1") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "1lz8h19x9yq1slzaadclscdbcqgvr3jh48k4bsnxa4h4dyjmqbbv") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.12.2 (c (n "v_escape") (v "0.12.2") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "1l7cw6kl9pz4yv9vmr9713jf0saw3iws3ad39klh6sgz62j7rb7g") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.13.0 (c (n "v_escape") (v "0.13.0") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "09dm70l6izshxy3fyzr5hfdqdr71sn2k0l8s480avkrx1pqczx05") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.13.1 (c (n "v_escape") (v "0.13.1") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "1crywxzxvjv7v0465har14yzqgc1700y56krd5lmbs3a7v418qsv") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.13.2 (c (n "v_escape") (v "0.13.2") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "0pj7s2q1kmq0bd6fxmyfpqnq0z7nxxgid42gwzj89sw6693l96h3") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.13.3 (c (n "v_escape") (v "0.13.3") (d (list (d (n "buf-min") (r "^0.1") (d #t) (k 0)) (d (n "buf-min") (r "^0.1") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.6") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "1c5qbx99s0viz9k80av01nv0sdjfmfw2bfc4ykxmfbfk0wj2d28x") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf")))) (y #t)))

(define-public crate-v_escape-0.14.0 (c (n "v_escape") (v "0.14.0") (d (list (d (n "buf-min") (r "^0.2") (d #t) (k 0)) (d (n "buf-min") (r "^0.2") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.6") (d #t) (k 2)) (d (n "v_escape_derive") (r "^0.8") (d #t) (k 0)))) (h "0f5ipp9gldcifsv8wvzs6brxwh85339krdbm9028is8yka2byj8i") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf")))) (y #t)))

(define-public crate-v_escape-0.14.1 (c (n "v_escape") (v "0.14.1") (d (list (d (n "buf-min") (r "^0.2") (d #t) (k 0)) (d (n "buf-min") (r "^0.2") (f (quote ("bytes-buf"))) (d #t) (k 2)) (d (n "bytes") (r "^0.6") (d #t) (k 2)) (d (n "v_escape_derive") (r "~0.8.4") (d #t) (k 0)))) (h "1ypsi0whykphbb24lxqpbb72lrnkwkd1dv5s1j885f3qqrrrxjnc") (f (quote (("default" "bytes-buf") ("bytes-buf" "buf-min/bytes-buf"))))))

(define-public crate-v_escape-0.15.0 (c (n "v_escape") (v "0.15.0") (d (list (d (n "buf-min") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "buf-min") (r "^0.4") (f (quote ("bytes-buf-tokio3"))) (d #t) (k 2)) (d (n "bytes") (r "^0.6") (d #t) (k 2)) (d (n "v_escape_derive") (r "~0.8.4") (d #t) (k 0)))) (h "1m6r4mv49xllqg1j1c65k3w73x36rfaafbix86lpichxmdgspq7k") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "buf-min/bytes-buf-tokio3") ("bytes-buf-tokio2" "buf-min/bytes-buf-tokio2"))))))

(define-public crate-v_escape-0.16.0 (c (n "v_escape") (v "0.16.0") (d (list (d (n "buf-min") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "buf-min") (r "^0.5.0") (f (quote ("bytes-buf-tokio3"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "v_escape_derive") (r "~0.8.4") (d #t) (k 0)))) (h "04yv3sagkbvqm36p9q5z8x2lkc5w1qspgni63ylghzk24kdryn4v") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "buf-min/bytes-buf-tokio3") ("bytes-buf-tokio2" "buf-min/bytes-buf-tokio2"))))))

(define-public crate-v_escape-0.16.1 (c (n "v_escape") (v "0.16.1") (d (list (d (n "buf-min") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "buf-min") (r "^0.6.0") (f (quote ("bytes-buf-tokio3"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "v_escape_derive") (r "~0.8.4") (d #t) (k 0)))) (h "0lx86f4lslyvjh41ckw2i0d9dqm4xp0wygrp03rhxrwqj3q02xxm") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "buf-min/bytes-buf-tokio3") ("bytes-buf-tokio2" "buf-min/bytes-buf-tokio2"))))))

(define-public crate-v_escape-0.17.0 (c (n "v_escape") (v "0.17.0") (d (list (d (n "buf-min") (r "^0.6.0") (k 0)) (d (n "v_escape_derive") (r "~0.8.5") (d #t) (k 0)))) (h "1q1vlvqjgcd2wqcl8y87rvnhl0w9lvfrk5nzc3ga352ba3paqsgq")))

(define-public crate-v_escape-0.18.0 (c (n "v_escape") (v "0.18.0") (d (list (d (n "buf-min") (r "^0.6.0") (k 0)) (d (n "v_escape_derive") (r "^0.9") (d #t) (k 0)))) (h "16qzqjbbzkmgd5zz087855crw67ak6b40kv1an1bk84cbqqrglkr")))

