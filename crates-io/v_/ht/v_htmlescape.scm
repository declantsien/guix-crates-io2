(define-module (crates-io v_ ht v_htmlescape) #:use-module (crates-io))

(define-public crate-v_htmlescape-0.1.0 (c (n "v_htmlescape") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "19hjl3g5kjndq22dxz1lal851swas3xn265hwji2igx3fzwingdq")))

(define-public crate-v_htmlescape-0.1.1 (c (n "v_htmlescape") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1ifi082kjmdgdqqz5kzznq2gwlvmfaaq13nxsvn7p3amgp5sd02i")))

(define-public crate-v_htmlescape-0.1.2 (c (n "v_htmlescape") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1ai0229wnigmksiqkqmkpvlvad6mlli9hj2yg3ha9anw871p69h1")))

(define-public crate-v_htmlescape-0.3.0 (c (n "v_htmlescape") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.3") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1iyljaik0qkxhgnn26zbnq708lx9afh5kryl7wv7n3llf4sp2gmg")))

(define-public crate-v_htmlescape-0.3.1 (c (n "v_htmlescape") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.3.1") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0cx393kfhw65sidhhy4sg2djbwz3wp5wybkm4i47sb7z8aknr901")))

(define-public crate-v_htmlescape-0.3.2 (c (n "v_htmlescape") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.3.2") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0zxjkxv22gwnjwic39ffr2d7l70v55xhh09zaam969n8gn0sw302")))

(define-public crate-v_htmlescape-0.4.0 (c (n "v_htmlescape") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0ng96xgxarxly8aldljyps0xk78r7z4kjgvlbbrpiwsqvh4052qn")))

(define-public crate-v_htmlescape-0.4.1 (c (n "v_htmlescape") (v "0.4.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0jsjhkdggiw2ainpkl3hyk7bj5x1fmc32yv6a4h7hbkz8vwhy0vj")))

(define-public crate-v_htmlescape-0.4.2 (c (n "v_htmlescape") (v "0.4.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 1)))) (h "0ynlh2dmd78wjc2h4sbq351c02p8r0dhp481m57g3rq9906i9hwv")))

(define-public crate-v_htmlescape-0.4.3 (c (n "v_htmlescape") (v "0.4.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 1)))) (h "1gp1h782pirwycr5hy6hwjfndfjkp7a1i8k1rz4gjdnxi3xf1fvz")))

(define-public crate-v_htmlescape-0.4.4 (c (n "v_htmlescape") (v "0.4.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 0)))) (h "0ywyhsydwzkpj19sbwsdcw8pz5fskv89sm4v548prhncgnh55m3a")))

(define-public crate-v_htmlescape-0.4.5 (c (n "v_htmlescape") (v "0.4.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.7") (d #t) (k 0)))) (h "0hbyx7r922q0czpb4f39g9kmdg3qmid7vfmn9x8lgw4c1nf96gp3")))

(define-public crate-v_htmlescape-0.5.0 (c (n "v_htmlescape") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.8") (d #t) (k 0)))) (h "190ncs1jq666ykj6md4yk2mk5affwjb620dr1ai623h7w47sg6l7")))

(define-public crate-v_htmlescape-0.6.0 (c (n "v_htmlescape") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.9") (d #t) (k 0)))) (h "1m6b4kwqdwjimw9wh3p4kmlk0k9zmx65a3r4ixq36jj58n06456z")))

(define-public crate-v_htmlescape-0.6.1 (c (n "v_htmlescape") (v "0.6.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.9") (d #t) (k 0)))) (h "1kkxbpbkr5yav0s2iz9i7wqdqj5m0pc36ghy3z80y7w9pn5yf4qq")))

(define-public crate-v_htmlescape-0.7.0 (c (n "v_htmlescape") (v "0.7.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.9") (d #t) (k 0)))) (h "0plnk4yjy73b7ki2bnr4wxv1569y1h6gn4ihw2pplsyxzc8g8yah")))

(define-public crate-v_htmlescape-0.8.0 (c (n "v_htmlescape") (v "0.8.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.10") (d #t) (k 0)))) (h "13iiq7xk85jifmyq83kyvdli38f2wsb2jz69n9mcrlj5vxqk7ylx")))

(define-public crate-v_htmlescape-0.9.0 (c (n "v_htmlescape") (v "0.9.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.11") (d #t) (k 0)))) (h "058ipf1wbzl86l9plvd1bqcjybc6mwz6pi1ai18i5lisrdw1rllc") (y #t)))

(define-public crate-v_htmlescape-0.9.1 (c (n "v_htmlescape") (v "0.9.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.11") (d #t) (k 0)))) (h "1wj9vj0ff86zymv9y37pj22hqdhhn3bz8w733p1fv0mxsakiv61p")))

(define-public crate-v_htmlescape-0.10.0 (c (n "v_htmlescape") (v "0.10.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.12") (k 0)))) (h "0af6spma8jbdq99hhj2ygc2nvcjpng7pnl7fnlkqbxxjki92bzgm") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.10.1 (c (n "v_htmlescape") (v "0.10.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.12") (k 0)))) (h "0ry6bkly3z5z3wssfrfxcas7sb6k2x1z5d5cvqp9mikaf9ps1z43") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.10.2 (c (n "v_htmlescape") (v "0.10.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "1msvvll45xazi3hddkjiv72chydhlpihpzwrjbs70q586j27nj6w") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.10.3 (c (n "v_htmlescape") (v "0.10.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "0jrb6wd7d33hki80pynxcpqs4ww6dcviar8gbbv5gk6c024igva2") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.10.4 (c (n "v_htmlescape") (v "0.10.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.13") (k 0)))) (h "10lldadc97cxlkc1r7ph13gzj4k53gh3kkrbnk0hvkyp7siw5mqi") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.11.0 (c (n "v_htmlescape") (v "0.11.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.14") (k 0)))) (h "00kl42xlq60m95hx5zxvbcrq4y1f4h6z42znk94yigla4h1wj06v") (f (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_htmlescape-0.12.0 (c (n "v_htmlescape") (v "0.12.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.15") (k 0)))) (h "0wj52yaqg9dsjgpfk12854gwcjvnv682b7cwk34pyvxd23v8m6hz") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_htmlescape-0.13.0 (c (n "v_htmlescape") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.16.0") (k 0)))) (h "10kcjbrwjficmqbmiiyy5s1f54bl473w94qrq38mzgnqzav69hzn") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_htmlescape-0.13.1 (c (n "v_htmlescape") (v "0.13.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.16.1") (k 0)))) (h "0ndy433p71mmcy84i8r90272g6c0wjy9ic04das53kc4q0ldj296") (f (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_htmlescape-0.14.0 (c (n "v_htmlescape") (v "0.14.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.17") (d #t) (k 0)))) (h "15sfvi46jbbkcl1r6q0igg250pq6l8rg629np5a23nc1z4z2g3m6")))

(define-public crate-v_htmlescape-0.14.1 (c (n "v_htmlescape") (v "0.14.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "v_escape") (r "^0.18") (d #t) (k 0)))) (h "0y04m2giyzbhcj0jp9wvkqla1mixs0xfc7mhzhassjfmphr2gcq4")))

(define-public crate-v_htmlescape-0.15.0 (c (n "v_htmlescape") (v "0.15.0") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1pwfap3rfrfbs25vxmzs0ivk5s7yp78kzw9f21s3mx6jdp01zp1g") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.1 (c (n "v_htmlescape") (v "0.15.1") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "020639cxw7r04wcnh5lignn3pn08kpfw8mlvzg1rxkyyhyga8q6k") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.2 (c (n "v_htmlescape") (v "0.15.2") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "05ml6chf7s1kbw1qf065cl66cd6nacphwx4c0ddihikc9qpb4ky8") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.3 (c (n "v_htmlescape") (v "0.15.3") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1xbmlsmx4qpsj9x2snymsrhncvd3xj84sr8wky85f9gm4ijkaxry") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.5 (c (n "v_htmlescape") (v "0.15.5") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1irci7x1nbjx422lf5h3i4lrfc431d38m4zhsplfdwhhbhm6vhgs") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.6 (c (n "v_htmlescape") (v "0.15.6") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1zjgx03vg75ckamk6i7ca7ickwn735lz10vp5x17v8jyfbm9fx79") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.7 (c (n "v_htmlescape") (v "0.15.7") (d (list (d (n "buf-min") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0fazphxa84plpyga1pjxlpbd5n33daakl677c3nqk19rs8vbh0i6") (f (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_htmlescape-0.15.8 (c (n "v_htmlescape") (v "0.15.8") (d (list (d (n "buf-min") (r "^0.7") (o #t) (d #t) (k 0)))) (h "135inp4x7cc32k0hzrymlz1baf0rj0ah5h82nrpa9w0hqpxmg0jf") (f (quote (("bytes-buf" "buf-min"))))))

