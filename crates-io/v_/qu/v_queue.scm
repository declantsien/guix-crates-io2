(define-module (crates-io v_ qu v_queue) #:use-module (crates-io))

(define-public crate-v_queue-0.1.0 (c (n "v_queue") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "1ydxxzqfdl9r86da4xacw2arzh4m96qi3wxbyygh20m1bf05hhns")))

(define-public crate-v_queue-0.1.1 (c (n "v_queue") (v "0.1.1") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "0n1n3jxw2r5rk3iya9zgkw57kplrdwl7l6xcdqpyb550h54jw4q5")))

(define-public crate-v_queue-0.1.2 (c (n "v_queue") (v "0.1.2") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "0rrjm6gjgzx0dbzphrs5csyr81drd3xdz35mfxkks86wg8jg33yz")))

(define-public crate-v_queue-0.1.3 (c (n "v_queue") (v "0.1.3") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "05d06lklhgahvm8hhjgzv6ayvsnalya08j3pawjcilny09ll04ax")))

(define-public crate-v_queue-0.1.4 (c (n "v_queue") (v "0.1.4") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "021f39qj2vzawv8azkc49sm0rjcxglv2pqwdfw7zn303nzkygyrh")))

(define-public crate-v_queue-0.1.5 (c (n "v_queue") (v "0.1.5") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "071jjfk6nf2jnycs9pq5h854pf9kppjbd6gpbb4cjzdzz2pnm7mz")))

(define-public crate-v_queue-0.1.6 (c (n "v_queue") (v "0.1.6") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "063myi6akmnr6hxrd9mcaswzq9xv67jvspr07b2fk4v9py4hl8d1")))

(define-public crate-v_queue-0.1.7 (c (n "v_queue") (v "0.1.7") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.0") (d #t) (k 0)))) (h "10fwn0pm8xjx7ic1za8pb9nqrqjyc4rs6z2iiqqb2r0n9zjfcz12")))

(define-public crate-v_queue-0.1.8 (c (n "v_queue") (v "0.1.8") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.19.2") (d #t) (k 0)))) (h "1p9k8kvqhh3h6hbyapsf0kfhncqbq5gasqj5hvz3s05dwq64qw34")))

(define-public crate-v_queue-0.1.9 (c (n "v_queue") (v "0.1.9") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.19.2") (d #t) (k 0)))) (h "14fpgmm7acvqvimz9fdyfwdxpm3bkiwbc6dmwz8ak3wazv706fq4")))

(define-public crate-v_queue-0.1.10 (c (n "v_queue") (v "0.1.10") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "1jsplsy4srmkyn9g0qai45mjjl6zm9cwa91g8amfjm9n3gf5ykjj")))

(define-public crate-v_queue-0.1.11 (c (n "v_queue") (v "0.1.11") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "1060z9dqf4h7ajir65sv5wrs62svarqqd2k79nwc037wnjdakmhh")))

(define-public crate-v_queue-0.2.0 (c (n "v_queue") (v "0.2.0") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "1m39lv6az8k2l6mzx04ff4nmfv1i9z6w5qspwwszcm43z81nc1lv")))

(define-public crate-v_queue-0.2.1 (c (n "v_queue") (v "0.2.1") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "0bxii9gpr9ji94rbm691yxmim81r1jscx8y3b4s2ks6bd16g9cy9")))

(define-public crate-v_queue-0.2.2 (c (n "v_queue") (v "0.2.2") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "1cdw5psrwk11jbap9gvs9bdj2gmwsnks6ih9p9w36rrwf8xfcdmk")))

(define-public crate-v_queue-0.2.3 (c (n "v_queue") (v "0.2.3") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "0iykp89cqzx5qf1br9yypv7av5hw0dfwwq2jffgxldx501al6hfx")))

(define-public crate-v_queue-0.2.4 (c (n "v_queue") (v "0.2.4") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "05ava0a4bif0qn4qvdq6hb7w17r7x20xihp7vb34l814p06ivqj7")))

(define-public crate-v_queue-0.2.5 (c (n "v_queue") (v "0.2.5") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2.6") (d #t) (k 0)))) (h "0636ja7ywbq94azxhrrkhr3w7blimh6xaccm8l3lnjsnyxsf836s")))

