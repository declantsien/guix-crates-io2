(define-module (crates-io v_ az v_az_lmdb) #:use-module (crates-io))

(define-public crate-v_az_lmdb-0.1.1 (c (n "v_az_lmdb") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lmdb-rs-m") (r "^0.7.7") (d #t) (k 0)) (d (n "v_authorization") (r "^0.1.0") (d #t) (k 0)))) (h "0lpn99a2sphhyddh82z1qcd5lqqvzz4krq0nl3gdr18wvj5x92nh")))

