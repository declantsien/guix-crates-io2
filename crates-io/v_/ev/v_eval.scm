(define-module (crates-io v_ ev v_eval) #:use-module (crates-io))

(define-public crate-v_eval-0.0.1 (c (n "v_eval") (v "0.0.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "09zjca6nz8vi5na2acnzr8wy3ws04s0ldmpklzq283kac6x6rscc")))

(define-public crate-v_eval-0.0.2 (c (n "v_eval") (v "0.0.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "12hmkfxjb4c60qgqd29agc6jhlz4qhihwq70pvn4xlaqc9cnjhhl")))

(define-public crate-v_eval-0.1.0 (c (n "v_eval") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0q3fgagbrl1nxnwj6vr0c0k7h72kjhzyr0khcw9brli5pl4kkis9")))

(define-public crate-v_eval-0.2.0 (c (n "v_eval") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "15r939jpn0ba4rmc8dbhrrd93c5xa4nfn09l5643v18ps5r2gdr1")))

(define-public crate-v_eval-0.3.0 (c (n "v_eval") (v "0.3.0") (d (list (d (n "quote-impersonated") (r "^0.1") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ki3p0rrmnia3rpxydk4db41740zfw6zjwm21vfjazi5n4hjd3dz")))

(define-public crate-v_eval-0.4.0 (c (n "v_eval") (v "0.4.0") (d (list (d (n "quote-impersonated") (r "^0.1") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0q9iblnbhyrszr9hilkj6r5vgx9fa1faqv8r15w41r1bmfsv7r8r")))

(define-public crate-v_eval-0.4.1 (c (n "v_eval") (v "0.4.1") (d (list (d (n "quote-impersonated") (r "^0.1") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1c63xl96rcfsdph45spnq6spcmrg8q8knyngijmgzx75nfm4p1fq")))

(define-public crate-v_eval-0.5.0 (c (n "v_eval") (v "0.5.0") (d (list (d (n "quote-impersonated") (r "^0.1") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1mrzpjh6jw0nybfnipvj90npkfkiis03p3d29h5gwilz75l7x0kj")))

(define-public crate-v_eval-0.5.1 (c (n "v_eval") (v "0.5.1") (d (list (d (n "quote-impersonated") (r "^0.1") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0brgc74x3pnz8kcmf14lr5mb1gmwkhd61fmir72y1n9j6mmv1wvf")))

(define-public crate-v_eval-0.5.2 (c (n "v_eval") (v "0.5.2") (d (list (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "10w2v17gb5kk4g7hgyjwq1anmbdhbkyxmlsay27bi8yg3fvq7628")))

(define-public crate-v_eval-0.5.3 (c (n "v_eval") (v "0.5.3") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn-impersonated") (r "^0.1") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1hmrfq39n6pjbfv5qr37i0sbwy22k4s5ckcppd7m2v35vb6ik962")))

(define-public crate-v_eval-0.6.0 (c (n "v_eval") (v "0.6.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1nf91j1771kwh17wcjpspyivnsybm9h3i6ny1n6h7swpsycvbn0d")))

