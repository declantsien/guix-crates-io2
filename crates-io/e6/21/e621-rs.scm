(define-module (crates-io e6 #{21}# e621-rs) #:use-module (crates-io))

(define-public crate-e621-rs-0.1.0 (c (n "e621-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01lyljqj8hsr81s3sc0xq2i7kkhks3cwrfsvdf0jg6dz1grlsbc8") (y #t)))

(define-public crate-e621-rs-0.1.1 (c (n "e621-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13hjmafv1029gjf953ivcjfaqzk1sfiiig5rqdabx659hijlj3q6") (y #t)))

(define-public crate-e621-rs-0.1.2 (c (n "e621-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1822ndf6mxnn1x4f3akmilrx70p73z33mlh4128wsk1n50vld7c5") (y #t)))

(define-public crate-e621-rs-0.1.3 (c (n "e621-rs") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q20vccaq13ysy8fc5q2kfy84q8zzhf7pipzd4ii8vz20fwlv9ci") (y #t)))

