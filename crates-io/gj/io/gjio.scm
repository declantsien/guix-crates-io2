(define-module (crates-io gj io gjio) #:use-module (crates-io))

(define-public crate-gjio-0.1.0 (c (n "gjio") (v "0.1.0") (d (list (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "miow") (r "^0.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0w2rsmalfyb4nhdqwna0hlgq095wwbflwblfrkc35m2kznc67dsy")))

(define-public crate-gjio-0.1.1 (c (n "gjio") (v "0.1.1") (d (list (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "miow") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.5") (d #t) (t "cfg(unix)") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gjlsfwhaqx65q9xaga3hhcqc9a9lc6cxw18wv1lvh1n1kzzgkk4")))

(define-public crate-gjio-0.1.2 (c (n "gjio") (v "0.1.2") (d (list (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "miow") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.5") (d #t) (t "cfg(unix)") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1q86ximcw61rp53fjjg2lyd8scnvjs80iif2766l0rzbcq5mnzc8")))

(define-public crate-gjio-0.1.3 (c (n "gjio") (v "0.1.3") (d (list (d (n "gj") (r "^0.2") (d #t) (k 0)) (d (n "miow") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "net2") (r "^0.2.23") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.5") (d #t) (t "cfg(unix)") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1jfbpwm49sjmyj7znskw4ri871g4hr0x86nsvmfa8w0wq496wi4z")))

