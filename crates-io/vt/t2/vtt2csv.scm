(define-module (crates-io vt t2 vtt2csv) #:use-module (crates-io))

(define-public crate-vtt2csv-0.1.0 (c (n "vtt2csv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "05yhk9bi96ccj3vf3risk0dm3h74i5ny81wgywapi3xl9cl1hn1y")))

(define-public crate-vtt2csv-0.1.1 (c (n "vtt2csv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1srfl9h3p4214iigab3zwcgj5qrzmfw2k59rdynab0akqrf556ni")))

