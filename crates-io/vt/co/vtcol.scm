(define-module (crates-io vt co vtcol) #:use-module (crates-io))

(define-public crate-vtcol-0.42.0 (c (n "vtcol") (v "0.42.0") (h "0vnghzpg4mpbszfwilbcd1fr8fckccx1ifr2hxb9afqd0qq0fm9c")))

(define-public crate-vtcol-0.42.1 (c (n "vtcol") (v "0.42.1") (h "05a44khkqqsa3xzczmfl37h3ljl0nynxxr6fy0fgsa8avgww9qg9")))

(define-public crate-vtcol-0.42.2 (c (n "vtcol") (v "0.42.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15ds9s5im2f9s079q4d1k04lwr8gxdj2q4x8lgfnyys9k918k7bj")))

