(define-module (crates-io vt ri vtrig) #:use-module (crates-io))

(define-public crate-vtrig-1.0.0 (c (n "vtrig") (v "1.0.0") (h "02ijbqgzdfkanxi5shirb6lb8hmccj699c066is3wvlknwz1rpi6")))

(define-public crate-vtrig-1.0.1 (c (n "vtrig") (v "1.0.1") (h "0jvwbzn4lhc7wllzi7274grmmrf9jc5a33xx8scd68nxswasly45")))

