(define-module (crates-io vt fl vtflib-sys) #:use-module (crates-io))

(define-public crate-vtflib-sys-0.1.0 (c (n "vtflib-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0d60lkmimrlz5kal7knk6yp828v3ac43zamc6h4lhjsj7fdpbwfl") (f (quote (("static")))) (l "VTFLib")))

(define-public crate-vtflib-sys-0.1.1 (c (n "vtflib-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1jcsjbri2xajz69v4mckmv0dinlydsyakz3ifzyngkw69wigqibl") (f (quote (("static")))) (l "VTFLib")))

(define-public crate-vtflib-sys-0.1.2 (c (n "vtflib-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0sv7y8cfpapznl3jhdikvh3hibfflrdj00br8jq1k7yr42w3md7h") (f (quote (("static")))) (l "VTFLib")))

(define-public crate-vtflib-sys-0.1.3 (c (n "vtflib-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0s99i69b0gdcd5ji83ajmc2hlzn1x77hi6w5m7fv1gz0259ydclf") (f (quote (("static")))) (l "VTFLib")))

(define-public crate-vtflib-sys-0.1.4 (c (n "vtflib-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1x6f8s8rwam6a7g7idwac5x9ci9xfscimb8gd1q1drdsrq2w8np6") (f (quote (("static")))) (l "VTFLib")))

(define-public crate-vtflib-sys-0.1.5 (c (n "vtflib-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0x1pf53xzj7n47b1rsvi1fs6paipj356hf2yspkylif9b76f7lvv") (f (quote (("static")))) (l "VTFLib")))

