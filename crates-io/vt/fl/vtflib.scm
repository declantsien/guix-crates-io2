(define-module (crates-io vt fl vtflib) #:use-module (crates-io))

(define-public crate-vtflib-0.1.0 (c (n "vtflib") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "vtflib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1l4r6vwq39nq70160pi4w09w8wjv6k0csjwcgvlrncywk60q2jb9") (f (quote (("static" "vtflib-sys/static")))) (y #t)))

(define-public crate-vtflib-0.1.1 (c (n "vtflib") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "vtflib-sys") (r "^0.1.1") (d #t) (k 0)))) (h "07ahk9aqcd8fdz0rmwc0xj6ay0qvwfhnh87ijbvrlx4977fgdpky") (f (quote (("static" "vtflib-sys/static")))) (y #t)))

(define-public crate-vtflib-0.2.0 (c (n "vtflib") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "vtflib-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0kpg156xp93jpvgrc6iffg7610laknx930gx3m0yk7ai9pq3cqz8") (f (quote (("static" "vtflib-sys/static"))))))

(define-public crate-vtflib-0.2.1 (c (n "vtflib") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "vtflib-sys") (r "^0.1.1") (d #t) (k 0)))) (h "19sq3yh2bvb8hzvkq2n181nwgxndv1krl1frbzbmgvfvx5pbinyv") (f (quote (("static" "vtflib-sys/static"))))))

