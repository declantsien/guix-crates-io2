(define-module (crates-io vt eb vtebench) #:use-module (crates-io))

(define-public crate-vtebench-0.1.0 (c (n "vtebench") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)) (d (n "terminfo") (r "^0.4") (d #t) (k 0)))) (h "182gcnxqj3pkbhh71ja1qafby1i2zxxaaln8498h284sgfpgc7ld")))

(define-public crate-vtebench-0.1.1 (c (n "vtebench") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)) (d (n "terminfo") (r "^0.4") (d #t) (k 0)))) (h "0wpw48s095lp214smpxkzdzqm2pxj67c26vnxbch1sh23wfjx42m")))

(define-public crate-vtebench-0.2.0 (c (n "vtebench") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4.9") (d #t) (k 0)) (d (n "terminfo") (r "^0.7.3") (d #t) (k 0)))) (h "1m9246qkb36fd03pmqwmwb5xy0zmcnc1yqqhwr8rkag44w0djyq5")))

(define-public crate-vtebench-0.3.1 (c (n "vtebench") (v "0.3.1") (d (list (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0mrpcmqg2kh8sw537n8pr9n1s2y4xs3mrhi8ihhiw5yjpscii3xc")))

