(define-module (crates-io vt mk vtmk) #:use-module (crates-io))

(define-public crate-VTMK-0.1.0 (c (n "VTMK") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("yaml" "env" "color" "suggestions" "unicode"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)) (d (n "zstd-safe") (r "^4.1.1") (d #t) (k 0)))) (h "0dlxazag2dj1gs99aij0q7zch27wigymvbg405l72cf420njlhyx")))

