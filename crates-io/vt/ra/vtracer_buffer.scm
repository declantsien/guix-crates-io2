(define-module (crates-io vt ra vtracer_buffer) #:use-module (crates-io))

(define-public crate-vtracer_buffer-0.6.3 (c (n "vtracer_buffer") (v "0.6.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.4") (d #t) (k 0)))) (h "0h77ijckygzg0akqnn34nwn8sbx76fljdp6r6wwv0i8bvhlddll5") (f (quote (("python-binding" "pyo3"))))))

(define-public crate-vtracer_buffer-0.6.4 (c (n "vtracer_buffer") (v "0.6.4") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.4") (d #t) (k 0)))) (h "13vpk3rrbqxj011wizw4wxx1mvn86m7p03b2bgxkd43s2qkz3sfg") (f (quote (("python-binding" "pyo3"))))))

(define-public crate-vtracer_buffer-0.6.5 (c (n "vtracer_buffer") (v "0.6.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.4") (d #t) (k 0)))) (h "0sdyixp5lk1ia1kj2g6v5s8ak3zaby4q0f1bajnpw8b7dx8iaisl") (f (quote (("python-binding" "pyo3"))))))

