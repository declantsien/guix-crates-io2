(define-module (crates-io vt ra vtracer) #:use-module (crates-io))

(define-public crate-vtracer-0.1.0 (c (n "vtracer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.2.0") (d #t) (k 0)))) (h "0c6sz9dsvv0iadinrh0z785mpqqmzg3m9gdi4mxhplfxzdbkrcb9")))

(define-public crate-vtracer-0.1.1 (c (n "vtracer") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.2.0") (d #t) (k 0)))) (h "0mbi7ljqrgwwf8ck3m848cp87ccrx2015wfhr9rnjxi6ky0wjixn")))

(define-public crate-vtracer-0.2.0 (c (n "vtracer") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.3.0") (d #t) (k 0)))) (h "0i866dvbs4lfvd4v0wwxihmikyi5v130da4n67lb6c8ldabfps1n")))

(define-public crate-vtracer-0.2.1 (c (n "vtracer") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "0rkb01daw2zgkbgkvxxz95fir8nak94z23gryrha0i6qvyzsy610")))

(define-public crate-vtracer-0.3.0 (c (n "vtracer") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "15hna7c5frj7bwxdn6i9mf65a96gfc1aibikn7y1b29krvwa0rnn")))

(define-public crate-vtracer-0.4.0 (c (n "vtracer") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.6.0") (d #t) (k 0)))) (h "10x20rl1c90qgf1xqf93l36vq3dj7hj10w80izx902kkh6shqpk7")))

(define-public crate-vtracer-0.5.0 (c (n "vtracer") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.0") (d #t) (k 0)))) (h "1fajfqd7g2j3y3g5g3g8gii65xd3w5ijx7cwrlvcgrksi1br1b62")))

(define-public crate-vtracer-0.6.1 (c (n "vtracer") (v "0.6.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.1") (d #t) (k 0)))) (h "1k56ilh6pkigjsh89957rlc5l8md17m33lss0lfdszzbya43dgxm") (f (quote (("python-binding" "pyo3"))))))

(define-public crate-vtracer-0.6.2 (c (n "vtracer") (v "0.6.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.1") (d #t) (k 0)))) (h "1w4vxxfq2fgfn6d0d2c1ldqk7xlal8rz01dlpizy517z92bbmv68") (f (quote (("python-binding" "pyo3"))))))

(define-public crate-vtracer-0.6.3 (c (n "vtracer") (v "0.6.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.1") (d #t) (k 0)))) (h "1qjw09y330vxa1icplm0lxv3bngddj3kbriad3ha17sbf35rsbzn") (f (quote (("python-binding" "pyo3"))))))

