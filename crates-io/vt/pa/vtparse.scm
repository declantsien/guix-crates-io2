(define-module (crates-io vt pa vtparse) #:use-module (crates-io))

(define-public crate-vtparse-0.1.0 (c (n "vtparse") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1vlpallzkiyrbxiimd3d84pis0w2m8cgaairjgsjpn3dib57023a")))

(define-public crate-vtparse-0.2.0 (c (n "vtparse") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "133jgg28bqz25m01jwnd9shgii5fvccx6sqbyrcxfa2a6wgmgf6j")))

(define-public crate-vtparse-0.2.1 (c (n "vtparse") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1zx8fbw7pa64id4f5cbzxmr5f0id83b5rcssq3254mdasz2awvsp")))

(define-public crate-vtparse-0.2.2 (c (n "vtparse") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "0k6ir49syjmy7z1kkmj39p3b60hncllxshbw1r9g53mn1ljb279s")))

(define-public crate-vtparse-0.4.0 (c (n "vtparse") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.2") (d #t) (k 0)))) (h "1j4k9882b4lzk538i3vnr2f75r2vv99n3hv0jm3162cab6vsbr6r")))

(define-public crate-vtparse-0.5.0 (c (n "vtparse") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.2") (d #t) (k 0)))) (h "0hkiygwd1px6s2vrb30v048zplm5pyvx2wy73ggham3ba2zvx83q")))

(define-public crate-vtparse-0.6.0 (c (n "vtparse") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.2") (d #t) (k 0)))) (h "00alva5ifbliidyszsqyw468awxfnmxwcihcvm1izpjd9hqwjhcg")))

(define-public crate-vtparse-0.6.1 (c (n "vtparse") (v "0.6.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "utf8parse") (r "^0.2") (d #t) (k 0)))) (h "0p4cyil2b1mf8sqngjmpjcxa1jzd777cv27l93fq8b30f8wr1kin")))

(define-public crate-vtparse-0.6.2 (c (n "vtparse") (v "0.6.2") (d (list (d (n "k9") (r "^0.11") (d #t) (k 2)) (d (n "utf8parse") (r "^0.2") (d #t) (k 0)))) (h "1l5yz9650zhkaffxn28cvfys7plcw2wd6drajyf41pshn37jm6vd")))

