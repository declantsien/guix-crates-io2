(define-module (crates-io vt e_ vte_generate_state_changes) #:use-module (crates-io))

(define-public crate-vte_generate_state_changes-0.1.0 (c (n "vte_generate_state_changes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1d809ick2gpd47hb160vlcg7cz9033i6s6jxbfw9prn08qlq2gdi")))

(define-public crate-vte_generate_state_changes-0.1.1 (c (n "vte_generate_state_changes") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "1zs5q766q7jmc80c5c80gpzy4qpg5lnydf94mgdzrpy7h5q82myj")))

