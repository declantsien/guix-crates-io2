(define-module (crates-io vt d_ vtd_xml) #:use-module (crates-io))

(define-public crate-vtd_xml-0.2.0 (c (n "vtd_xml") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.0") (d #t) (k 0)))) (h "09v01py20cld8iaym5jfww2i5hybpfw8yv0n601qqf3krw6zp1ql")))

(define-public crate-vtd_xml-0.2.1 (c (n "vtd_xml") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0bq30vygw822ss4i7601whbv9g5m9nkk7s99z3dh4v3pd2dyrzih")))

(define-public crate-vtd_xml-0.2.2 (c (n "vtd_xml") (v "0.2.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0vzygrri8cvvik3ldfi1g5g59z5ifiwi3a5qbs2w4rzqzvihdl8s")))

(define-public crate-vtd_xml-0.3.0 (c (n "vtd_xml") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1mqcn1sbiidij6zhawiqq9f2zc5lnkv4y026qw5zcfqanghr8l6n")))

(define-public crate-vtd_xml-0.3.1 (c (n "vtd_xml") (v "0.3.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0ihsdkn0fv6wlqvzcma1vw83m5k7iq1hhh5jhd4wvrb9cp3bdx20")))

(define-public crate-vtd_xml-0.3.2 (c (n "vtd_xml") (v "0.3.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "1aag4zb5jhy8775hcmcvq9lpaszfa3jj9i84pf2yaix258fsjq2g")))

(define-public crate-vtd_xml-0.3.3 (c (n "vtd_xml") (v "0.3.3") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1ijnv70073ss1kh3w8qlc5njsyl0yqyyjafzy7isj3qlbnv6ydn6") (l "vtdxml")))

(define-public crate-vtd_xml-0.3.4 (c (n "vtd_xml") (v "0.3.4") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "gstuff") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "08grwl56gnhm3737k4lh2f5aqz52d68p3dnzwcwhz9c2md7h92fr") (l "vtdxml")))

