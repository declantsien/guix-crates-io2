(define-module (crates-io vt ab vtable) #:use-module (crates-io))

(define-public crate-vtable-0.1.0 (c (n "vtable") (v "0.1.0") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "vtable-macro") (r "^0.1") (d #t) (k 0)))) (h "0a1dr1zv372cs27pd9ypaz559d0127bh644463yfw9sfrrpbk3x6")))

(define-public crate-vtable-0.1.1 (c (n "vtable") (v "0.1.1") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "vtable-macro") (r "=0.1.1") (d #t) (k 0)))) (h "1pwqpbwhgzz5gk87pjjknw1gw2xpi0ypb61hgcr7wsrdkcmdqln4")))

(define-public crate-vtable-0.1.2 (c (n "vtable") (v "0.1.2") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "vtable-macro") (r "=0.1.2") (d #t) (k 0)))) (h "1xlf1fcrawrrvygbbq84ablf7ibhnq5n6bcpxqmaawy3i05k9bsn")))

(define-public crate-vtable-0.1.3 (c (n "vtable") (v "0.1.3") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "vtable-macro") (r "=0.1.3") (d #t) (k 0)))) (h "00yjg7iw43dziicz2bmp30qpfd1jafzy3hvc6kvws89ii022ccs6")))

(define-public crate-vtable-0.1.4 (c (n "vtable") (v "0.1.4") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "vtable-macro") (r "=0.1.4") (d #t) (k 0)))) (h "0jlxkvs3gfihbhyssam6s7cqzkk3kcwq1ag57lwzckby55cr03q0")))

(define-public crate-vtable-0.1.5 (c (n "vtable") (v "0.1.5") (d (list (d (n "atomic-polyfill") (r "^0.1.5") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.5") (d #t) (k 0)))) (h "0sx4vi6lzmbcknryrwqyiqdpl4b601axg7xy41pix54xidzhp4nw")))

(define-public crate-vtable-0.1.6 (c (n "vtable") (v "0.1.6") (d (list (d (n "atomic-polyfill") (r "^0.1.5") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.6") (d #t) (k 0)))) (h "1sf9ixwqcfblzkshlw4bnjayshsbwjjb49v2bxrrhbzqfs3cpcan")))

(define-public crate-vtable-0.1.7 (c (n "vtable") (v "0.1.7") (d (list (d (n "atomic-polyfill") (r "^0.1.5") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.7") (d #t) (k 0)))) (h "06xydbnhi76aks4l6qa3znzvvlj2nqrinrlg2yxwg5s23gwi076c")))

(define-public crate-vtable-0.1.8 (c (n "vtable") (v "0.1.8") (d (list (d (n "atomic-polyfill") (r "^0.1.5") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.8") (d #t) (k 0)))) (h "09bzfnpwqvlnka00gpfkjvdfd83404pnxz0ra507md2blwqnhqrd")))

(define-public crate-vtable-0.1.9 (c (n "vtable") (v "0.1.9") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.9") (d #t) (k 0)))) (h "1pslqjn1dp9af5r6x30pjv53q7b2y94knwvdfr4wx0d8fr56rciz")))

(define-public crate-vtable-0.1.10 (c (n "vtable") (v "0.1.10") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)) (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.10") (d #t) (k 0)))) (h "0s02falz92lvqvyvja0c2ljjsi6dapaz33xqkw3swvnwf7fzzsph")))

(define-public crate-vtable-0.1.11 (c (n "vtable") (v "0.1.11") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.10") (d #t) (k 0)))) (h "177x67vfp1zaj91vrrw7a53pr1q100ydrlv131x7fqc54c37ak4z")))

(define-public crate-vtable-0.1.12 (c (n "vtable") (v "0.1.12") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.1.12") (d #t) (k 0)))) (h "03bzyrk6pgdv4l81vk0zpwgxi7mv4gybvqn1wcqy7b99sf0kkdyd")))

(define-public crate-vtable-0.2.0 (c (n "vtable") (v "0.2.0") (d (list (d (n "const-field-offset") (r "^0.1") (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)) (d (n "vtable-macro") (r "=0.2.0") (d #t) (k 0)))) (h "0qq94c4rgwq54rii18nbk2q197xmjj5jd0kjd3qvqdczv6lx971p")))

