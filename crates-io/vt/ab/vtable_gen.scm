(define-module (crates-io vt ab vtable_gen) #:use-module (crates-io))

(define-public crate-vtable_gen-0.1.0 (c (n "vtable_gen") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p0wqx1jdflssb3cncf0ga44r9qnfikxy4msrhjh21jgmc191a77")))

(define-public crate-vtable_gen-0.1.1 (c (n "vtable_gen") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1frpzkqfiy42zd22g3ajgqpvlspaq03fias2y6lyaa5iz3hn8j06")))

(define-public crate-vtable_gen-0.1.2 (c (n "vtable_gen") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04p0l6872iqyxyc3zadrazi1cnlf9awzzbppiv4k0b8q7bpiizkw")))

(define-public crate-vtable_gen-0.1.3 (c (n "vtable_gen") (v "0.1.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13p497k62sgnkxwj4mdz4ss8650vq324fw84x6vhs0ni0ih90nva") (y #t)))

(define-public crate-vtable_gen-0.1.4 (c (n "vtable_gen") (v "0.1.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l0sp4d76fgfmv751imvincyn2rk8xyxhsv39v0kkbihjj2vciva")))

(define-public crate-vtable_gen-0.1.5 (c (n "vtable_gen") (v "0.1.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r33p7sfqh8d5gy66bnpp05phsxdw4bkz2c903dlnsf5xlw9i8zs")))

(define-public crate-vtable_gen-0.1.6 (c (n "vtable_gen") (v "0.1.6") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hsxxh6dqg92rfzjv86yhxh1m1vrmhw17a881d3gmalz23cd5cxr")))

(define-public crate-vtable_gen-0.1.7 (c (n "vtable_gen") (v "0.1.7") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvw17rbzg0552m9f2xy10dm0rqd1lyhhfs9ayd7wwgirsj2hqp0")))

(define-public crate-vtable_gen-0.2.0 (c (n "vtable_gen") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x582vh68brljsi2wwf4l4f3grlq3s5w56nc4j5jr8qm4j2pycgf")))

(define-public crate-vtable_gen-0.2.1 (c (n "vtable_gen") (v "0.2.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rg6pgygpikwgrnhxw5ia91dy86ka8mjdc67c4d8cgdaf604mx7q")))

(define-public crate-vtable_gen-0.3.0 (c (n "vtable_gen") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d3805xxvkr9csn9ns2nx183xgx2aqpsq2c1b6cv6caxrq96dfr8")))

