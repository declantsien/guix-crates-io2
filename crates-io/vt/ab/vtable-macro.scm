(define-module (crates-io vt ab vtable-macro) #:use-module (crates-io))

(define-public crate-vtable-macro-0.1.0 (c (n "vtable-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w0vycmsd46q7j2q4n772kx4gspjqjkal1sy66ijfb7mxgpnk3n4")))

(define-public crate-vtable-macro-0.1.1 (c (n "vtable-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhyclimhdc8hz7nkq4k6da1qirs55pvlfv4p50dxzayay1apwwn")))

(define-public crate-vtable-macro-0.1.2 (c (n "vtable-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07swjxpafqfqry90hpyxd9h2ap7mpx2ijdaxgiwsszfhigcljba1")))

(define-public crate-vtable-macro-0.1.3 (c (n "vtable-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11x04iyl444r2nsbilffi0l66pp8kg02hsw7iv7i0d53whibgqym")))

(define-public crate-vtable-macro-0.1.4 (c (n "vtable-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sjfj1g2klpril0rgdjjmh6p2inrx0z3aq2ax6zqx3yin7iv2f0q")))

(define-public crate-vtable-macro-0.1.5 (c (n "vtable-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nk6v2xy8pjsrk7jp6vkxig4frcsxbwx4zckpy0zfi17c9jxcn0c")))

(define-public crate-vtable-macro-0.1.6 (c (n "vtable-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04p9jrsg00f758s4kbnxfhg0mjcfpzixgylr6bbhd3kj2al9pqsp")))

(define-public crate-vtable-macro-0.1.7 (c (n "vtable-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01cr70kl3dhdj5g9daw4xv057b4lf2bygwqgf2zsxvi9jhdym4ic")))

(define-public crate-vtable-macro-0.1.8 (c (n "vtable-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1byj3k5npbms3vi37irr7g6xirnys3a6bm9i3jmmf5gfdgks0zzs")))

(define-public crate-vtable-macro-0.1.9 (c (n "vtable-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gccgixx3721ra4q05d528kzwb30a5z3shk1d2qwrbdpk906zfb3")))

(define-public crate-vtable-macro-0.1.10 (c (n "vtable-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xiikg9sv94kz8vbwg90wwrxrxj8y0ccavslynnq8hlfvgn8wavb")))

(define-public crate-vtable-macro-0.1.12 (c (n "vtable-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "113k9qaah4144rp6qm1ra67mgnd6ks26vgajcimr0j98dkg29g48")))

(define-public crate-vtable-macro-0.2.0 (c (n "vtable-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rb6mbiq2v1ym5g7n9jwl8b98m761khafpynx5hbrls3r1gbihb8")))

