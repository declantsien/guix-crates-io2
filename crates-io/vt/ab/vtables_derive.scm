(define-module (crates-io vt ab vtables_derive) #:use-module (crates-io))

(define-public crate-vtables_derive-0.1.0 (c (n "vtables_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vtables") (r "^0.1") (d #t) (k 2)))) (h "0i4vcmjc5hkjm5vlgw0agyaf6bny2xi37zrj5n7jbv3b17nffj50")))

