(define-module (crates-io vt x- vtx-bin) #:use-module (crates-io))

(define-public crate-vtx-bin-0.1.0 (c (n "vtx-bin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vtx") (r "^0.1") (d #t) (k 0)) (d (n "wav") (r "^1") (d #t) (k 0)))) (h "11m3hhkjk0w2npxqrwvgyypf8vrvnwq0g9qbg2h7ys74228w3kx2")))

(define-public crate-vtx-bin-0.1.1 (c (n "vtx-bin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vtx") (r "^0.1") (d #t) (k 0)) (d (n "wav") (r "^1") (d #t) (k 0)))) (h "0yy5mriyrf84bd0hamwmcm4nirfb5dshqym8pv0j8k3gzkvkhyb9")))

(define-public crate-vtx-bin-0.1.2 (c (n "vtx-bin") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vtx") (r "^0.1") (d #t) (k 0)) (d (n "wav") (r "^1") (d #t) (k 0)))) (h "067brziniwy7y77w7pdrg89g77lf94zzn0hb6d2d3pb82wbgmgcp")))

(define-public crate-vtx-bin-0.15.0 (c (n "vtx-bin") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vtx") (r "^0.15") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 0)))) (h "05dv2l1yy24a0mw070s8wi8i9vhz15jq12lxhhy3wiyd2fqrnxna")))

(define-public crate-vtx-bin-0.16.0 (c (n "vtx-bin") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vtx") (r "^0.16.0") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 0)))) (h "0qss60xm8560xr3161npa2fgbydiaknbspf1phc4ikddrrba8pg6")))

