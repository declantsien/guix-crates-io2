(define-module (crates-io vt il vtil-parser) #:use-module (crates-io))

(define-public crate-vtil-parser-0.1.0 (c (n "vtil-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "09fm9q8gjq6fbsmslcn29l922kszlksfafcpsmpfxlfny7m4znml")))

(define-public crate-vtil-parser-0.1.1 (c (n "vtil-parser") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "0m7b015h7sqcijl40jf5v57gi2ikjd29z0pnd4s3nqiddjx3xpc0")))

(define-public crate-vtil-parser-0.2.0 (c (n "vtil-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "12876jgsyl11akfdsmw4g6ga1pgmbwhflwfv34afs6an1d17k699")))

(define-public crate-vtil-parser-0.2.1 (c (n "vtil-parser") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)))) (h "0m2r5790n0xc47smr0ycxv3rb57v1iwq1xvphw36kij7ayh29p0z")))

(define-public crate-vtil-parser-0.3.0 (c (n "vtil-parser") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.9.2") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h427wnyxad6n9b0flc3zy45kw7q15a4h7klw66g1pkkn9gy2h4i")))

(define-public crate-vtil-parser-0.4.0 (c (n "vtil-parser") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c66gpfxj77xs036fbj9g4cjgw0srwlq4h1vcdn5y2lxllgm8sjw")))

(define-public crate-vtil-parser-0.5.0 (c (n "vtil-parser") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16y1cl8977skvw9wm1xk9mvigbzfhdivpk31f54jkcrb49wi3fx8")))

(define-public crate-vtil-parser-0.6.0 (c (n "vtil-parser") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17ih2vsq7mk92ii7k42kmll0sqixh0pzlm08msxmwkn09yc5q8y3") (f (quote (("serde-1" "serde" "indexmap/serde-1"))))))

(define-public crate-vtil-parser-0.6.1 (c (n "vtil-parser") (v "0.6.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ac68yqax6qbn3cbnj2im5llcnsp7bfxhgkfn35352fqakazjbh8") (f (quote (("serde-1" "serde" "indexmap/serde-1"))))))

(define-public crate-vtil-parser-0.7.0 (c (n "vtil-parser") (v "0.7.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "scroll") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rh5z3lgjqy5h6w9ssjijdbpji59fgxp5fjv421cwlyanng6nhfv") (f (quote (("serde-1" "serde" "indexmap/serde-1"))))))

