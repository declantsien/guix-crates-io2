(define-module (crates-io f_ cl f_cli) #:use-module (crates-io))

(define-public crate-f_cli-0.1.0 (c (n "f_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "derive" "cargo"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.1") (f (quote ("debug-embed"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "update-informer") (r "^1.0.0") (d #t) (k 0)))) (h "0a2lfcsv50whsz6zz37xf36jhv4gvxbxcpdqfnsms2h6f33kl4f6") (y #t)))

(define-public crate-f_cli-0.1.1 (c (n "f_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("wrap_help" "derive" "cargo"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.1") (f (quote ("debug-embed"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "update-informer") (r "^1.0.0") (d #t) (k 0)))) (h "0qas79hn1ar9jmf9a6wlg0ilsb2hjpl240xxdpwqbsqbigbp7ham")))

