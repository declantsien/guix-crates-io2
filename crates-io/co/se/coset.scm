(define-module (crates-io co se coset) #:use-module (crates-io))

(define-public crate-coset-0.1.0 (c (n "coset") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (f (quote ("std" "tags"))) (d #t) (k 0)))) (h "0n4skknpkda6alnsfp9kh85v8879g1cxw0r1q2ln8vk1zgd0fjgr")))

(define-public crate-coset-0.1.1 (c (n "coset") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (f (quote ("std" "tags"))) (d #t) (k 0)))) (h "0m6ycljr5a6gdbn6263yig7picpd938mrmdhjsjjmbz04fjixczs")))

(define-public crate-coset-0.1.2 (c (n "coset") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (f (quote ("std" "tags"))) (d #t) (k 0)))) (h "0sh5wq710s5860c198phip1pnabpqjcahsvjf6yr9gw9gc7hrnzm")))

(define-public crate-coset-0.2.0 (c (n "coset") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)))) (h "0mrlsysdyygamjcr0hfca2n95fmfp2lq234kv2ls5zdl1jysybmq")))

(define-public crate-coset-0.3.0 (c (n "coset") (v "0.3.0") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "0z33mdxncr2jfv51ydqnhfgm5j51k45sm2xxnym3r7d7qs4gbhc7")))

(define-public crate-coset-0.3.1 (c (n "coset") (v "0.3.1") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "16g3k6swlb0fhnhp3sj8z5pizkxc25fxnq2v359rgvg6rd0pgc3k")))

(define-public crate-coset-0.3.2 (c (n "coset") (v "0.3.2") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "1jya5im9a93jmb76lx1mz1ckddjj3jf733njxx7i994vhkw3zg2m")))

(define-public crate-coset-0.3.3 (c (n "coset") (v "0.3.3") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "12wkrfkyg08f0mm6z7h9v6c0zby0sgv8ry6xp42ywkvggc7v6k30")))

(define-public crate-coset-0.3.4 (c (n "coset") (v "0.3.4") (d (list (d (n "ciborium") (r "^0.2.0") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "158glv25450qwwhh0jhky2myqzssk5hcv7wflilmrwnfabla8rf7") (f (quote (("std") ("default"))))))

(define-public crate-coset-0.3.5 (c (n "coset") (v "0.3.5") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "1m8i7hqvaaspyrd1pqzkxj0sisiz696sx76pas453d68qnxi9hlr") (f (quote (("std") ("default"))))))

(define-public crate-coset-0.3.6 (c (n "coset") (v "0.3.6") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "1ndbj4nd5f9iq6c9krsi4kwj5iic76zhb2l42j38nhaqxs4gdivp") (f (quote (("std") ("default"))))))

(define-public crate-coset-0.3.7 (c (n "coset") (v "0.3.7") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "ciborium-io") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "0g2fdsa9pycb2y4jl95lkx6nr8mm3q2i6ac1gsjdm1hz1j2sv2pz") (f (quote (("std") ("default"))))))

