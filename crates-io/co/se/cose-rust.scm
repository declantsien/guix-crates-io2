(define-module (crates-io co se cose-rust) #:use-module (crates-io))

(define-public crate-cose-rust-0.1.0 (c (n "cose-rust") (v "0.1.0") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "14l89frbz9cs190sf3cgzbkxy4glma6k18v49d0fq2h7lh173yvf") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.1 (c (n "cose-rust") (v "0.1.1") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "19hkbgl6bad0ym4wf2cr3vj8ivws735cw2n82hizzpx9hmrhz9b3") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.2 (c (n "cose-rust") (v "0.1.2") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "110880a3vrxprsmj43qg09mcpg2c9ji9dilwli1srjj2gqkypdh9") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.3 (c (n "cose-rust") (v "0.1.3") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "0wfprp6gbxikr9w1vxvymycs1px7jhj02hvvh8c8xmwkis2hk94k") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.4 (c (n "cose-rust") (v "0.1.4") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "1y7hskdwsmy0gg9pjmr150fmlv8qs24rrnfwjnc4z0j98swyzhsa") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.5 (c (n "cose-rust") (v "0.1.5") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "16wp5nkmj57i09zr4ham6bg7pc9ygbz4baqmfgndk3y2lnfa304m") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.6 (c (n "cose-rust") (v "0.1.6") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "1h9hjc676fb4ma6qg71mpx7bxryds7rh7zazzgadilf0b05j1apq") (f (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1.7 (c (n "cose-rust") (v "0.1.7") (d (list (d (n "cbor-codec") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "115bwfc1d0b24l1x764npl5k7k212xflk8bzbdsr7kkjkccb88gj") (f (quote (("json" "hex" "serde_json"))))))

