(define-module (crates-io co se cose-rust-plus) #:use-module (crates-io))

(define-public crate-cose-rust-plus-0.2.0 (c (n "cose-rust-plus") (v "0.2.0") (d (list (d (n "cbor-codec-plus") (r "^0.8.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1k1rciw68wr9kxnn4bax72p9fgh1p5sp2q229nid1l6rylhjdwy7") (f (quote (("json" "hex" "serde_json"))))))

