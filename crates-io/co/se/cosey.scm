(define-module (crates-io co se cosey) #:use-module (crates-io))

(define-public crate-cosey-0.1.0-alpha.0 (c (n "cosey") (v "0.1.0-alpha.0") (d (list (d (n "heapless-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1aybcn100g00g075bqhig4i56gwf47x3d9hfx4ib62hqfby9z7pa")))

(define-public crate-cosey-0.1.0 (c (n "cosey") (v "0.1.0") (d (list (d (n "heapless-bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0wzlji41i4lycz3vpzc70bkwkpakjbc5r4j576c6dp550i85fh9g")))

(define-public crate-cosey-0.2.0 (c (n "cosey") (v "0.2.0") (d (list (d (n "heapless-bytes") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1y3l9kmks23mx4yqg7fkxlqhccyr6pnnizlyf8ib1yf8c6z0xrys")))

(define-public crate-cosey-0.3.0 (c (n "cosey") (v "0.3.0") (d (list (d (n "heapless-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1apxmjmml1bf67j3m3knjf1zrz3m5xapzfpm0879g6xnb0n3qx5v")))

