(define-module (crates-io co se cose) #:use-module (crates-io))

(define-public crate-cose-0.1.0 (c (n "cose") (v "0.1.0") (h "1azjqyr5kf1q33fdmcxs67xv3w1myp6sigbr15m905d6zp2rgi44")))

(define-public crate-cose-0.1.1 (c (n "cose") (v "0.1.1") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (k 2)))) (h "0wdp9084s4m1zafj638xi65l2nw37j6a8ic91zi8bm2gav9qn8s2") (f (quote (("default"))))))

(define-public crate-cose-0.1.2 (c (n "cose") (v "0.1.2") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (k 2)))) (h "0sv4wnkaydbj6r85ar6jr3s59fgsfhs9l6cfy1bsb3zk55k8247c") (f (quote (("default"))))))

(define-public crate-cose-0.1.3 (c (n "cose") (v "0.1.3") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (k 2)))) (h "0pvcf6fjpdnsdwf06ckxdfvbhlayr12fw9884qjs7kvg3qyzxw2m") (f (quote (("default"))))))

(define-public crate-cose-0.1.4 (c (n "cose") (v "0.1.4") (d (list (d (n "moz_cbor") (r "^0.1.0") (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (d #t) (k 2)))) (h "03armrxfyfshgypgpypa1wi09kjpxl7pvmk31yvy8fhx2p5jdykj") (f (quote (("default"))))))

