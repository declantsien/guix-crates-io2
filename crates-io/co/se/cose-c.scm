(define-module (crates-io co se cose-c) #:use-module (crates-io))

(define-public crate-cose-c-0.1.0 (c (n "cose-c") (v "0.1.0") (d (list (d (n "cose") (r "^0.1") (d #t) (k 0)))) (h "05yzbivrwhzr7rcr89xr8z0k3pf2gaavz1ia2cj4k6cqlbms12dk")))

(define-public crate-cose-c-0.1.1 (c (n "cose-c") (v "0.1.1") (d (list (d (n "cose") (r "^0.1.2") (d #t) (k 0)))) (h "0bzwn0kff85bgqqlv1glqwmp7dvg9ddbhsckxx0kbsf2bsw8pk07")))

(define-public crate-cose-c-0.1.2 (c (n "cose-c") (v "0.1.2") (d (list (d (n "cose") (r "^0.1.2") (d #t) (k 0)))) (h "0awm0q6fifafb2788zcp1m1sva7dal1llf6rkcr3hcq4lg9kaqml")))

(define-public crate-cose-c-0.1.3 (c (n "cose-c") (v "0.1.3") (d (list (d (n "cose") (r "^0.1.2") (d #t) (k 0)))) (h "1p5rn7bdlwgjia25gvw0anppbam2jw9hb47pr3wxzn139kh43aqx")))

(define-public crate-cose-c-0.1.4 (c (n "cose-c") (v "0.1.4") (d (list (d (n "cose") (r "^0.1.3") (d #t) (k 0)))) (h "16s1vl7hzrcjbwgbijajw1bfflwv2pxks2i5d7bw2k8rdnd9ayj8")))

(define-public crate-cose-c-0.1.5 (c (n "cose-c") (v "0.1.5") (d (list (d (n "cose") (r "^0.1.4") (d #t) (k 0)))) (h "154szzzjznjlym9zlxx7jnvicfjlg951x9nc9wa6b9qcmcan0wj9")))

