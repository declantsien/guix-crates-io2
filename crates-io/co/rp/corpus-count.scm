(define-module (crates-io co rp corpus-count) #:use-module (crates-io))

(define-public crate-corpus-count-0.1.0 (c (n "corpus-count") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "1ihdlzsiv66nnkkmbygs54xccjpzwcx6rgrfjfpg72n3fh86ksxr")))

(define-public crate-corpus-count-0.1.1 (c (n "corpus-count") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "0mf9brpqrl2bn3sy4v7n2kns3zga4rwcdb0h5rk6vz1hcmfsq5fy")))

