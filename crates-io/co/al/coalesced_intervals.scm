(define-module (crates-io co al coalesced_intervals) #:use-module (crates-io))

(define-public crate-coalesced_intervals-0.1.0 (c (n "coalesced_intervals") (v "0.1.0") (d (list (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "0xck86jsqf80ld3rm9xvf1baknzq2s72j5m3ssfn0g2na4mj5ky4")))

(define-public crate-coalesced_intervals-0.1.1 (c (n "coalesced_intervals") (v "0.1.1") (d (list (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "0nh7mqvw543iq5hyvdg6zjrxrqjzzq3l882zws81hw3fkq6dwdmd")))

(define-public crate-coalesced_intervals-0.1.2 (c (n "coalesced_intervals") (v "0.1.2") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "1jp1zvh9dc65gpcx32rza6a0dcb6imcyg90k0zkdyz05sjysk776")))

(define-public crate-coalesced_intervals-0.1.3 (c (n "coalesced_intervals") (v "0.1.3") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "1hwqhxa4mnjwali9scy8aqk8wcvmpikc77knn930mkf83fnisg6k")))

(define-public crate-coalesced_intervals-0.1.4 (c (n "coalesced_intervals") (v "0.1.4") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "1m62z7x1v3hyc8957zvq79k99arkzrk7w5b5f1mij5i1k1vmynhy")))

(define-public crate-coalesced_intervals-0.1.5 (c (n "coalesced_intervals") (v "0.1.5") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "env_logger") (r "~0.11") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "03d7jrnmb8v9yzmi3inmq2vy9vgnchgf7y5s9kidf9b8wgd7p81j")))

