(define-module (crates-io co al coalesce) #:use-module (crates-io))

(define-public crate-coalesce-0.1.0 (c (n "coalesce") (v "0.1.0") (h "1yms8ga7j1y1sg89wjpapyq3cadlxyp3r5qnd8jm84bk5ls3g2p7")))

(define-public crate-coalesce-0.1.1 (c (n "coalesce") (v "0.1.1") (h "01ygqwzx9yzy3jykbp47r2hchg87nbcivf9xbfymb1m5pa9jzx77")))

