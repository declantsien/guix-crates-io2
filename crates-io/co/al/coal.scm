(define-module (crates-io co al coal) #:use-module (crates-io))

(define-public crate-coal-0.1.0 (c (n "coal") (v "0.1.0") (h "1zkzyp946476pr9961hl94rv7g53yqyqkmz0rzg5md576yhmfpsj")))

(define-public crate-coal-1.0.0 (c (n "coal") (v "1.0.0") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-files") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "grass") (r "^0.10.4") (d #t) (k 0)) (d (n "html-minifier") (r "^3.0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "05p1952fx3q1nczn5ivsfqy1yxiflgnj40iywjfzi9znfq1jpj4b")))

