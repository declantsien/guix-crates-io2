(define-module (crates-io co al coalescing_buffer) #:use-module (crates-io))

(define-public crate-coalescing_buffer-0.0.1 (c (n "coalescing_buffer") (v "0.0.1") (h "1m37b4d24ryyc2knpywdag01mpr4489whwx8hy7yj9xs5amdhgs4")))

(define-public crate-coalescing_buffer-0.0.2 (c (n "coalescing_buffer") (v "0.0.2") (h "1jy4xm05y44n2n8vm2ylr4rjlcnlxgwd4y5620zami8b6xwa4gr8")))

(define-public crate-coalescing_buffer-0.0.3 (c (n "coalescing_buffer") (v "0.0.3") (h "0qwvhr2nwyhi90bnys6fdkb5lcdzdzzg2bbgcb346x17i6m94hn5")))

(define-public crate-coalescing_buffer-0.1.0 (c (n "coalescing_buffer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)))) (h "1p12g3klrmdhscfpv60sq8njxg469500c01j367qnwcwkb208i1s")))

