(define-module (crates-io co i- coi-actix-web-derive) #:use-module (crates-io))

(define-public crate-coi-actix-web-derive-0.1.0 (c (n "coi-actix-web-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0jw2z9p6njllknrks1wdabm0gnvgf4pmril779yhz39z5fxr08vg")))

(define-public crate-coi-actix-web-derive-0.2.0 (c (n "coi-actix-web-derive") (v "0.2.0") (d (list (d (n "actix-web") (r "^2.0") (d #t) (k 2)) (d (n "coi") (r "^0.9.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ji64fx9y8spf50724yrzjji3ljrljazjyfjlxc8p51270dsi22h")))

(define-public crate-coi-actix-web-derive-0.2.1 (c (n "coi-actix-web-derive") (v "0.2.1") (d (list (d (n "actix-web") (r "^2.0") (d #t) (k 2)) (d (n "coi") (r "^0.10.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cc0z2wzcmvz2klr3akvj8w36ag77pclip5zzmmr0dr12jgm80sz")))

(define-public crate-coi-actix-web-derive-0.3.0 (c (n "coi-actix-web-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "actix-web") (r "^4.2.1") (d #t) (k 2)) (d (n "coi") (r "^0.10.0") (d #t) (k 2)))) (h "12p8x946f6xfag793f3kyhg71scmxqzj5dfxxxkglp4hhg6vnnvc")))

(define-public crate-coi-actix-web-derive-0.4.0 (c (n "coi-actix-web-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "actix-web") (r "^4.2.1") (d #t) (k 2)) (d (n "coi") (r "^0.10.1") (d #t) (k 2)))) (h "037iwx1865bjig62ggwll15v95zcchd9qvqjbi5y6qw39ax1h3wg")))

