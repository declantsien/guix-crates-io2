(define-module (crates-io co i- coi-rocket) #:use-module (crates-io))

(define-public crate-coi-rocket-0.1.0 (c (n "coi-rocket") (v "0.1.0") (d (list (d (n "coi") (r "^0.9.2") (d #t) (k 0)) (d (n "coi-rocket-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)))) (h "13dghxxdzy55f8q86qi51fp9cvz9sp6s3lg83yh078nnh09490dm") (f (quote (("default") ("debug" "coi/debug"))))))

(define-public crate-coi-rocket-0.2.0 (c (n "coi-rocket") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "coi") (r "^0.10.2") (d #t) (k 0)) (d (n "coi-rocket-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "1w9psh576zamp98j19zrmg5m05vdal9rzr56kci9lzaln42h4x9v") (f (quote (("default") ("debug" "coi/debug"))))))

(define-public crate-coi-rocket-0.2.1 (c (n "coi-rocket") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "coi") (r "^0.10.2") (d #t) (k 0)) (d (n "coi-rocket-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "0na7j6pjgq2qcxvzwks8rdf3265y5m4v65jf6x4vvy34pl8rf0gv") (f (quote (("default") ("debug" "coi/debug"))))))

(define-public crate-coi-rocket-0.2.2 (c (n "coi-rocket") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "coi") (r "^0.10.3") (d #t) (k 0)) (d (n "coi-rocket-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "0zjhjxdy5h70pjmii8pzwv69ym0f4l6nm25ji109h6kq7xxd7b22") (f (quote (("default") ("debug" "coi/debug"))))))

