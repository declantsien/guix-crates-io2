(define-module (crates-io co i- coi-rocket-derive) #:use-module (crates-io))

(define-public crate-coi-rocket-derive-0.1.0 (c (n "coi-rocket-derive") (v "0.1.0") (d (list (d (n "coi") (r "^0.9.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qv4pn2ak1sgr035ys9f3q4cw2y3im63zzg205wg0l6g8ngwbi88")))

(define-public crate-coi-rocket-derive-0.2.0 (c (n "coi-rocket-derive") (v "0.2.0") (d (list (d (n "coi") (r "^0.10.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03svkaml8xhai79z1m7fpqk86srybjq8hg413h7fji7d9xl0sx0s")))

(define-public crate-coi-rocket-derive-0.2.1 (c (n "coi-rocket-derive") (v "0.2.1") (d (list (d (n "coi") (r "^0.10.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0vkv058q9lsr8n0yx1a2zgk7ffkf9smnfrnp79v73dy1lyivizhc")))

