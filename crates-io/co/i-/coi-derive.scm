(define-module (crates-io co i- coi-derive) #:use-module (crates-io))

(define-public crate-coi-derive-0.1.0 (c (n "coi-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "16fwm1jp7nbxyy9w066a400p0wrf94cd6yy96gpcr31rk3k9fir9")))

(define-public crate-coi-derive-0.1.1 (c (n "coi-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1nxwh9sy2gz0s39srdvqjdcma3adk1f264ckrn3fdgi90yzwp8mk")))

(define-public crate-coi-derive-0.2.0 (c (n "coi-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "17347fvjj3p7bh4f1vl0xpbhfjj2iivldgn6k7x3dns7wbg6zm5h")))

(define-public crate-coi-derive-0.2.1 (c (n "coi-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "144wwjz1kzjlnk9kzsl972wrvpwck4xmpzwbjqyjy2h7854293w7")))

(define-public crate-coi-derive-0.3.0 (c (n "coi-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "08133ph97p28w7dp92q7sama97rwkq5mrk02xdqm18kyhg87k1ri")))

(define-public crate-coi-derive-0.3.1 (c (n "coi-derive") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "073vc5hs89z7asdid22gf1s6l14f795irf7d12n9aa0qlpy6srhv")))

(define-public crate-coi-derive-0.3.2 (c (n "coi-derive") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1xashhr95cybrp8i0w4bpqzh7bgx26kf3mqg91vnal96b4rm1i6h")))

(define-public crate-coi-derive-0.4.0 (c (n "coi-derive") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhb650gfjfx67p6076k2gpmd7kkkn5iiyda0rx8fzbvv8w53jf7") (f (quote (("default" "async") ("async"))))))

(define-public crate-coi-derive-0.4.1 (c (n "coi-derive") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1z7jnsrr6l3lh8inqq52xf5zmpfjv0y0x01clqs6c1h6rpng9i2g") (f (quote (("default" "async") ("async"))))))

(define-public crate-coi-derive-0.5.0 (c (n "coi-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "16y53magmz6383mpj5nvwnx5w705s5vn0msblh5sar940cn1hx65") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.6.0 (c (n "coi-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0w28j73lbk26sr0364hdj5hcz3q420z3v92jjh5wyjjr6i2d7jrn") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.7.0 (c (n "coi-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsqm1yrp0qhfqvm2vm97s394zpzack2sbk2wp2iha7imjllhxr0") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.8.0 (c (n "coi-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0ga590dqjy6ydj5sscdq7dwhn353x94b0qf3l7ipwzggafvnb3yp") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.9.0 (c (n "coi-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q81ss1bz5ysdyybnxkn04arp74n64adg2x6yy1c6sfwgpc44pnz") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.10.0 (c (n "coi-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lj6ibyfwxfvlrklgc8hnxiyldj3mjdx2gi6nbir7arjxwk901h0") (f (quote (("default") ("debug"))))))

(define-public crate-coi-derive-0.10.1 (c (n "coi-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0dxdiif8lq8xk014wpfacj4rk04fvris3m1g0vvnkpc0jbilg7jj") (f (quote (("default") ("debug"))))))

