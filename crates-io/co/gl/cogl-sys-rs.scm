(define-module (crates-io co gl cogl-sys-rs) #:use-module (crates-io))

(define-public crate-cogl-sys-rs-0.1.0 (c (n "cogl-sys-rs") (v "0.1.0") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^1.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vbd144brpy31m7khzx9wx5q6k4yavvdf760k63kip1hb129pz27") (y #t) (l "\"cogl\"")))

(define-public crate-cogl-sys-rs-0.1.1 (c (n "cogl-sys-rs") (v "0.1.1") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 2)) (d (n "system-deps") (r "^1.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "17vcriqg07dawhpd03sjrkkyk47jd70xv62b7y2p54ywsspm7z4p") (f (quote (("dox")))) (y #t) (l "\"cogl\"")))

(define-public crate-cogl-sys-rs-0.1.2 (c (n "cogl-sys-rs") (v "0.1.2") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 2)) (d (n "system-deps") (r "^1.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1zrjvwl5q8w7h14j3fkhfrbnbblbwrdycvr3j45f2i2717g0qapc") (f (quote (("dox")))) (y #t) (l "\"cogl\"")))

(define-public crate-cogl-sys-rs-0.1.3 (c (n "cogl-sys-rs") (v "0.1.3") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 2)) (d (n "system-deps") (r "^1.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0c2b4psznr3498g3zj916qqz2q9dq9djvfhljffid26d99l2z446") (f (quote (("dox")))) (y #t) (l "\"cogl\"")))

(define-public crate-cogl-sys-rs-0.1.4 (c (n "cogl-sys-rs") (v "0.1.4") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 2)) (d (n "system-deps") (r "^1.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "045x4lfrzxmmzqrj2h0zmk4miynqwqv6arl6dir7cpvxqbmzml26") (f (quote (("dox")))) (l "\"cogl\"")))

