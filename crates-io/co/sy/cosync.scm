(define-module (crates-io co sy cosync) #:use-module (crates-io))

(define-public crate-cosync-0.1.0 (c (n "cosync") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15cs9nzn4wiiq6pbki3ih7g6b5amszrzhcjxin7i92ig2ijkmddi")))

(define-public crate-cosync-0.2.0 (c (n "cosync") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y0rwzvmbf27pgxw7qjybs04klgvm646i40ba4vhv30bxswby11z")))

(define-public crate-cosync-0.2.1 (c (n "cosync") (v "0.2.1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0anc30qdk630iir72qbxgx1z06i3x2cnw8xnz6ciis4fynd68irx")))

