(define-module (crates-io co mp composure_adapter_cloudflare) #:use-module (crates-io))

(define-public crate-composure_adapter_cloudflare-0.0.1 (c (n "composure_adapter_cloudflare") (v "0.0.1") (d (list (d (n "composure") (r "^0.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "worker") (r "^0.0.16") (d #t) (k 0)))) (h "063nvkvh2v1j8qry78vixqhlggyyy6mm0lyqik78vwfzz6p7sbg6")))

(define-public crate-composure_adapter_cloudflare-0.0.2 (c (n "composure_adapter_cloudflare") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "composure") (r "^0.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "worker") (r "^0.0.16") (d #t) (k 0)))) (h "1zcs6iyqxr4mljf164ik5d5vmxdx95zpl1pm3hj8jsz30x0c6dlv")))

