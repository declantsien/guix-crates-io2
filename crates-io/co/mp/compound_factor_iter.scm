(define-module (crates-io co mp compound_factor_iter) #:use-module (crates-io))

(define-public crate-compound_factor_iter-0.1.0 (c (n "compound_factor_iter") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "17cphx64zgk333as3dz4hcjgi533by3hk4fw9f58zfssl7d0msi7") (f (quote (("letter_distribution" "rand" "rand_pcg"))))))

(define-public crate-compound_factor_iter-0.1.1 (c (n "compound_factor_iter") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1kjqgqhhf94mkkrl3xw6nlacm9m6kyxm36983ls4n08sskd69v9c") (f (quote (("letter_distribution" "rand" "rand_pcg"))))))

