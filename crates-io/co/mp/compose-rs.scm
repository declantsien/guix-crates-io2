(define-module (crates-io co mp compose-rs) #:use-module (crates-io))

(define-public crate-compose-rs-0.0.1 (c (n "compose-rs") (v "0.0.1") (d (list (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "relative-path") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1zpm4yh72c3ggwbq1in7ml71qmzr3agrrj92n2d8hr2gn8b2g84m")))

(define-public crate-compose-rs-0.0.2 (c (n "compose-rs") (v "0.0.2") (d (list (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "relative-path") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0zhwr24r0h8hay93dqp6qnjy4kzpvv965dbj1xqsmhyb45xla800")))

(define-public crate-compose-rs-0.0.3 (c (n "compose-rs") (v "0.0.3") (d (list (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "relative-path") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1gfggm7qjxd8qrf4hif733r4ijvg7hfwnyhq76wx16zkn0s3kpd0")))

