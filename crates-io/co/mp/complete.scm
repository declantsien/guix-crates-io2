(define-module (crates-io co mp complete) #:use-module (crates-io))

(define-public crate-complete-0.0.0 (c (n "complete") (v "0.0.0") (h "0f9gzjizivd6bd4jhgl4hwq7z6qihcckg582ds2dvgxalrarxs25")))

(define-public crate-complete-0.0.1 (c (n "complete") (v "0.0.1") (h "1xr1yrpx8czy1gx1pxml2wbsc8z62146rk75rdklqzzd4ivdv9lh")))

