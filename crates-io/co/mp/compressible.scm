(define-module (crates-io co mp compressible) #:use-module (crates-io))

(define-public crate-compressible-0.1.0 (c (n "compressible") (v "0.1.0") (d (list (d (n "mime") (r "^0.3.16") (d #t) (k 0)))) (h "14ywz7w3n65dnq260mj36fgv10gwap70zcrxgw2ndi7x7vfk9app")))

(define-public crate-compressible-0.2.0 (c (n "compressible") (v "0.2.0") (d (list (d (n "mime") (r "^0.3.16") (d #t) (k 0)))) (h "1lwkyibacvc4hd6pskmvgqhaas6kcdaby8q8qkfprhx7m3x56y7y")))

