(define-module (crates-io co mp compiledfiles) #:use-module (crates-io))

(define-public crate-compiledfiles-0.1.0 (c (n "compiledfiles") (v "0.1.0") (d (list (d (n "gimli") (r "^0.19.0") (d #t) (k 0)) (d (n "object") (r "^0.16.0") (d #t) (k 0)) (d (n "pdb") (r "^0.5.0") (d #t) (k 0)))) (h "1d2df51dgwkqcf25653bcvq32vfhn1s56vl7h9csls0jq3gw4bnv")))

(define-public crate-compiledfiles-0.2.0 (c (n "compiledfiles") (v "0.2.0") (d (list (d (n "gimli") (r "^0.19.0") (d #t) (k 0)) (d (n "object") (r "^0.16.0") (d #t) (k 0)) (d (n "pdb") (r "^0.5.0") (d #t) (k 0)))) (h "0r4c6sadylwpf0fmznvcgp0rv9sjzk6cxyhixgjsvqsrrm89hc0m")))

(define-public crate-compiledfiles-0.3.0 (c (n "compiledfiles") (v "0.3.0") (d (list (d (n "gimli") (r "^0.19.0") (d #t) (k 0)) (d (n "object") (r "^0.17.0") (d #t) (k 0)) (d (n "pdb") (r "^0.5.0") (d #t) (k 0)))) (h "169dp2gmkw9821n6r57al7b1ascdw5fyvl8wrigwqv3hkaian3zi")))

(define-public crate-compiledfiles-0.4.0 (c (n "compiledfiles") (v "0.4.0") (d (list (d (n "gimli") (r "^0.20.0") (d #t) (k 0)) (d (n "object") (r "^0.17.0") (d #t) (k 0)) (d (n "pdb") (r "^0.6.0") (d #t) (k 0)))) (h "0g7g9pgnb6nhchd8s58shvsqp12l831wxnnccn4rddd0w3bkah87")))

(define-public crate-compiledfiles-0.5.0 (c (n "compiledfiles") (v "0.5.0") (d (list (d (n "gimli") (r "^0.21.0") (d #t) (k 0)) (d (n "object") (r "^0.19.0") (d #t) (k 0)) (d (n "pdb") (r "^0.6.0") (d #t) (k 0)))) (h "0wdqqi34kllsyb4gn64l5slckzdn0l524p5z7ky6apgj2rzsgcs6")))

(define-public crate-compiledfiles-0.6.0 (c (n "compiledfiles") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gimli") (r "^0.23.0") (d #t) (k 0)) (d (n "object") (r "^0.23.0") (d #t) (k 0)) (d (n "pdb") (r "^0.7.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "151qfkqc3rnb2hzp8miq4v6fygzbc4dmrlyilw91hl8rzqy2gyik")))

(define-public crate-compiledfiles-0.7.0 (c (n "compiledfiles") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "gimli") (r "^0.26.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "pdb") (r "^0.7.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pdrf91ladvhvhs8hspyjb0wp3ksfnmbq4xzd48lfdsnys2y7g1i")))

(define-public crate-compiledfiles-0.8.0 (c (n "compiledfiles") (v "0.8.0") (d (list (d (n "gimli") (r "^0.27.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (d #t) (k 0)) (d (n "pdb") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0nglxaf7g7s2m1hamycr0sgsx8jp8q51z3g81jyygj6d6p9w7zgf")))

