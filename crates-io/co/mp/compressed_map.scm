(define-module (crates-io co mp compressed_map) #:use-module (crates-io))

(define-public crate-compressed_map-0.1.0 (c (n "compressed_map") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-RC.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.21.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "0nc67mpp8gmilcq1rprdwgzam8h5dqzyyq9khq2rqdcgci1q60kb") (f (quote (("threading") ("headers") ("default" "cffi") ("cffi"))))))

