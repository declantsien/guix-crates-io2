(define-module (crates-io co mp compare-files) #:use-module (crates-io))

(define-public crate-compare-files-0.1.0 (c (n "compare-files") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "09nj2nrjwi2nhzxyvl3wskkjc9nvmznjsikgbzarlkky5gkbryab")))

(define-public crate-compare-files-0.1.1 (c (n "compare-files") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "0jq5ji3q1n68zvqx1ag79wqps7k356si9fr4gcw4a3mskmxbzvbj")))

(define-public crate-compare-files-0.1.2 (c (n "compare-files") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)))) (h "0zsy6yddzixmrbp37jibknba2c6lh9lm2gdmj9r5acq1r4j32rs6")))

(define-public crate-compare-files-0.1.3 (c (n "compare-files") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)))) (h "1cnqz1k93wjjcm0ilbfa29h0kr4d5bll8s6s64ra775446ay7d9d")))

