(define-module (crates-io co mp compatible-with-derive) #:use-module (crates-io))

(define-public crate-compatible-with-derive-0.1.0 (c (n "compatible-with-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ljywfwdkfakd5fkm2r4wbcc0cypkhdvdd5axlfzv93amw8p8ihh")))

