(define-module (crates-io co mp compare) #:use-module (crates-io))

(define-public crate-compare-0.0.1 (c (n "compare") (v "0.0.1") (h "01l3d9vr0nf1fk4yw1sbgw32xrn48kd54slh7dmwk5qq7746xpcg")))

(define-public crate-compare-0.0.2 (c (n "compare") (v "0.0.2") (h "168iddcwjhqkk6k2q037khkbyjj1fvx2x9l3ypna4hn5dz57lx0l")))

(define-public crate-compare-0.0.3 (c (n "compare") (v "0.0.3") (h "1a98c2g1zasb4lfhkihqxyss2s16vs349fy6filk2cb6v9cnc2f7")))

(define-public crate-compare-0.0.4 (c (n "compare") (v "0.0.4") (h "0hrawnzzfk4ckl68136lv9z9g3hrx98wg50gms55aq6zgq8mivdz")))

(define-public crate-compare-0.0.5 (c (n "compare") (v "0.0.5") (h "06c2a1hhfd6dhrpskb9m4b5rp4zs8l9fhzwg0nlxysi436znsw05")))

(define-public crate-compare-0.0.6 (c (n "compare") (v "0.0.6") (h "19wbyirhcgl3c01r889fy3msy7c01jb1bzfnmi28naiw23v9a07a")))

(define-public crate-compare-0.1.0 (c (n "compare") (v "0.1.0") (h "1lv84g7l04vc1g54z5sigz330xklhkljwl165vz7xi1fvga3608j")))

