(define-module (crates-io co mp compact-calendar) #:use-module (crates-io))

(define-public crate-compact-calendar-0.5.6 (c (n "compact-calendar") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "013abrdmans8zlw535r4qfs9q34bxf7sb8rmljsc2hgfpw766bmw")))

(define-public crate-compact-calendar-0.6.0 (c (n "compact-calendar") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1i67jqadiijlyd38fn5jv9hyg735f6i4z25bfxzn1rw2fhfqc3yn")))

(define-public crate-compact-calendar-0.6.1 (c (n "compact-calendar") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "11g429xk584h1bbyd7d5pfhdswpzyhhm2ca6lzdzwnj5kh4r60zl")))

(define-public crate-compact-calendar-0.6.2 (c (n "compact-calendar") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "182izrf020357gs08i6ap8xhh9y4ragxdxd5wdx8wh05d1fdyycz")))

(define-public crate-compact-calendar-0.6.3 (c (n "compact-calendar") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1n03bk9r80j4dchz7acxkva9yqdhqzyi93mvqm7ih95a29ckwbpy")))

(define-public crate-compact-calendar-0.6.4 (c (n "compact-calendar") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1f0j1jg7ws82gl8bxx0yq5n9dpjpl35kyq0mizpng2hl9s15hq3r")))

(define-public crate-compact-calendar-0.6.5 (c (n "compact-calendar") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1b534ah5dqlv5kf4chidl2rcrmvb7zq40vrc8337bjd2sbyqdfj6")))

(define-public crate-compact-calendar-0.6.6 (c (n "compact-calendar") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "01vz7dpzlhni0r01jhlcgy9q3wxjkrxp5gn9y14kx093zh5731pw")))

(define-public crate-compact-calendar-0.6.7 (c (n "compact-calendar") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0x8aws4zwr0wf04l2fh78q01rlb17mfdskcgsajjycwxg5rb3mg2")))

(define-public crate-compact-calendar-0.6.8 (c (n "compact-calendar") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1gnpbinxnv6qgwg5fpfyly8397pb72jkr7pwmbli9f7d6llfigrj")))

(define-public crate-compact-calendar-0.6.9 (c (n "compact-calendar") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0l7kp3ijdglwjgp980mlska9s5wx5lla57ki4wwhd311m8xxh4z3")))

(define-public crate-compact-calendar-0.6.10 (c (n "compact-calendar") (v "0.6.10") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "051lck95i6sq2d80zssswz2bwqqnnh5p8drgza8i2amxl0cavlnk")))

(define-public crate-compact-calendar-0.6.11 (c (n "compact-calendar") (v "0.6.11") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0lb69chaw8h3dkp8gry462ygx16ndrq9bpjnnax168mg89z1pw9x")))

(define-public crate-compact-calendar-0.6.12 (c (n "compact-calendar") (v "0.6.12") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1rll9ggchadp8kv3wjz9nk9d2ynvd25mrdw85i8pmz6cdfbyb830")))

(define-public crate-compact-calendar-0.6.13 (c (n "compact-calendar") (v "0.6.13") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0ms2nqvgmcpdj6ks9lvz6y8n8s0g0yxq9b4m8b1hm69kkra65fzj")))

(define-public crate-compact-calendar-0.6.14 (c (n "compact-calendar") (v "0.6.14") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "1bnhdcqf7hmalz81hl8nk5bl1spd95lj4i5r0v7cwf526dzcmn2k")))

(define-public crate-compact-calendar-0.6.15 (c (n "compact-calendar") (v "0.6.15") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "01k6vsxhrlgiaww2k8n6gh0ik1j220pv5bls9zxs6zwa65nawgz5")))

(define-public crate-compact-calendar-0.6.18 (c (n "compact-calendar") (v "0.6.18") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "14kzqdgz1izq8hz258gir7r0icif87mc79q08js496az17p2mvlj")))

