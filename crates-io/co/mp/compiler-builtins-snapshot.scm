(define-module (crates-io co mp compiler-builtins-snapshot) #:use-module (crates-io))

(define-public crate-compiler-builtins-snapshot-0.0.20161003+954e3b70959ba8c6df6fc564a5f758f518b44286 (c (n "compiler-builtins-snapshot") (v "0.0.20161003+954e3b70959ba8c6df6fc564a5f758f518b44286") (h "0m7nbb9x6g63pkj54wvv1cswqajpf6848dayijsd0hxag48id8mm") (y #t)))

(define-public crate-compiler-builtins-snapshot-0.0.20161004+954e3b70959ba8c6df6fc564a5f758f518b44286 (c (n "compiler-builtins-snapshot") (v "0.0.20161004+954e3b70959ba8c6df6fc564a5f758f518b44286") (d (list (d (n "rlibc") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1g9af1c0ag3b2qc6zricwslpxxf07fawhhr23dn053pm77cswnsd") (f (quote (("memcpy" "rlibc"))))))

(define-public crate-compiler-builtins-snapshot-0.0.20161008+c56faf22abb39724008148d58f12bcd43b6d236b (c (n "compiler-builtins-snapshot") (v "0.0.20161008+c56faf22abb39724008148d58f12bcd43b6d236b") (d (list (d (n "rlibc") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1xlw42bwgw528vd3rziz95cl081m5ki2i6agwfcffw6dgnpsg0fa") (f (quote (("memcpy" "rlibc"))))))

(define-public crate-compiler-builtins-snapshot-0.1.20170126+764557f0b669bb0e28817c5ae70c9d718685e35a (c (n "compiler-builtins-snapshot") (v "0.1.20170126+764557f0b669bb0e28817c5ae70c9d718685e35a") (d (list (d (n "gcc") (r "^0.3.36") (d #t) (k 1)) (d (n "rustc-cfg") (r "^0.3.0") (d #t) (k 1)))) (h "0nzi691cmf5j8qb4x8njppxs0pia2bhxpa5q4rvms0sibgpygmhj") (f (quote (("mem") ("default" "compiler-builtins") ("compiler-builtins") ("c")))) (y #t)))

(define-public crate-compiler-builtins-snapshot-0.1.20170127+764557f0b669bb0e28817c5ae70c9d718685e35a (c (n "compiler-builtins-snapshot") (v "0.1.20170127+764557f0b669bb0e28817c5ae70c9d718685e35a") (d (list (d (n "gcc") (r "^0.3.36") (d #t) (k 1)) (d (n "rustc-cfg") (r "^0.3.0") (d #t) (k 1)))) (h "09fhg6kwgf4z32vf1ixaw5jz58lj5b2v281aw9p4cw4289f4fsfd") (f (quote (("mem") ("default" "compiler-builtins") ("compiler-builtins") ("c"))))))

