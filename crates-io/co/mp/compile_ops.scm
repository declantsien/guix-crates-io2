(define-module (crates-io co mp compile_ops) #:use-module (crates-io))

(define-public crate-compile_ops-0.1.0 (c (n "compile_ops") (v "0.1.0") (h "14293h3m7r5fsksah8c1vrz5wqfzgms9m3zlxj78k4yk1yi2j0my")))

(define-public crate-compile_ops-0.1.1 (c (n "compile_ops") (v "0.1.1") (h "0hzwcj1mgs83djkaslcbsszyicix7d1aq5ckfbkxp1rigk12ff3p")))

(define-public crate-compile_ops-0.1.2 (c (n "compile_ops") (v "0.1.2") (h "0avfybzmq9g8wwl3xjdcxm2czlpbb4ryvksjicxpwdys4ylql3y4")))

(define-public crate-compile_ops-0.1.3 (c (n "compile_ops") (v "0.1.3") (h "07y763dmf6mwxk7rg10a7ncjq8af6d446546z7ggg315lsrpsl52")))

