(define-module (crates-io co mp computus) #:use-module (crates-io))

(define-public crate-computus-0.1.0 (c (n "computus") (v "0.1.0") (h "0jadcc0qain3fy9sq8bi9nicn9izk8x6y7qqi1b811dl0ixh5j2j")))

(define-public crate-computus-0.1.1 (c (n "computus") (v "0.1.1") (h "1b30iv8919bpvjds3zqi6s9lfjqzxc90qpbiq2drn1fqqjl7wvrd")))

(define-public crate-computus-1.0.0 (c (n "computus") (v "1.0.0") (h "14378xgqmzln76pbmiqa0s909xk9zk0l7nv4yh5fbcc9yhfs5snr")))

(define-public crate-computus-1.1.0 (c (n "computus") (v "1.1.0") (d (list (d (n "chrono") (r ">=0.2.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.5.0") (d #t) (k 2)))) (h "0kc0br9klgxba05ky4c78jqpj6sxpdfwqcqq0jzhndzr6lv58c9h")))

