(define-module (crates-io co mp compound_duration) #:use-module (crates-io))

(define-public crate-compound_duration-1.0.0 (c (n "compound_duration") (v "1.0.0") (h "07pkn4vmcmvkxzwmbb4kzqz0apdw1sd7mb0lc9h771vz9fn597x1")))

(define-public crate-compound_duration-1.0.1 (c (n "compound_duration") (v "1.0.1") (h "159px2hsbv9d9m8d7jk7p6vmkdsi7kgp9ihjqb9gf5my0ai3yc0d")))

(define-public crate-compound_duration-1.0.2 (c (n "compound_duration") (v "1.0.2") (h "0v697qif28cmy81ak12wqq5gdxyxbhfjhvi9wiv4ib10wwany9cc")))

(define-public crate-compound_duration-1.0.3 (c (n "compound_duration") (v "1.0.3") (h "1pacqrnsgm7qyk9jc72rd6g4dx2d0pc2x37lkilzh21jvj7nn3v7")))

(define-public crate-compound_duration-1.1.0 (c (n "compound_duration") (v "1.1.0") (h "1z09raar1www7h1a2h5rgn6d7ybqgw48rgpb7lcfphjdsyfip44i")))

(define-public crate-compound_duration-1.2.0 (c (n "compound_duration") (v "1.2.0") (h "0k367k67cx6pc6qhc8c0zqc18xpkyiz4zdfmml6nvvf42vc07j0l")))

(define-public crate-compound_duration-1.2.1 (c (n "compound_duration") (v "1.2.1") (h "0rb5kasd7044xxb73wz2qqxv6iwawp9gipvqhya8yybafh7a679r")))

