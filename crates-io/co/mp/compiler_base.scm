(define-module (crates-io co mp compiler_base) #:use-module (crates-io))

(define-public crate-compiler_base-0.1.0 (c (n "compiler_base") (v "0.1.0") (h "1abx0shn6qci0cqysdgpqh6qrvv8zyp94xz5y94c4nnpw95qn8fc") (y #t)))

(define-public crate-compiler_base-0.1.1 (c (n "compiler_base") (v "0.1.1") (h "0f54vkjlwasscnsdh3fa3aqqwsk4qxji70jiygga7b8cf8anlipm") (y #t)))

(define-public crate-compiler_base-0.1.2 (c (n "compiler_base") (v "0.1.2") (h "06g443xjdlgp2i6lhls3c77j4zb8g6cnj9awqqvjrl35yf9kh1pl") (y #t)))

(define-public crate-compiler_base-0.1.3 (c (n "compiler_base") (v "0.1.3") (h "00gpik0hqw2s901jsgfs2xi840x3dj0y92paqg272yjdkjq0bnyv") (y #t)))

(define-public crate-compiler_base-0.1.4 (c (n "compiler_base") (v "0.1.4") (h "19x3xlxsiwrg9056dkqsnwj51k57avx7jq3p1ygp0p7ick5b072r")))

