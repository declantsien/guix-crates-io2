(define-module (crates-io co mp compiled-nn-bindings) #:use-module (crates-io))

(define-public crate-compiled-nn-bindings-0.1.0 (c (n "compiled-nn-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0lyhx5qx2cd043wnqhg8jz652zsldc4j5k0a7rabdcnc1i0jd6fg")))

(define-public crate-compiled-nn-bindings-0.2.0 (c (n "compiled-nn-bindings") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0s5l71ybm1d79mp34vr7laschyv0fvwjn1avxzzz990yig962xxr")))

(define-public crate-compiled-nn-bindings-0.3.0 (c (n "compiled-nn-bindings") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0zsrmmhq382bmv8bwijzmk2p60qikql9nia017kah2d1b20nifhb")))

(define-public crate-compiled-nn-bindings-0.5.0 (c (n "compiled-nn-bindings") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0pdwmgf0gs924xayqnzgzby5gk7j4fbbp37i2z1xqqh1ph59hh0g")))

(define-public crate-compiled-nn-bindings-0.6.0 (c (n "compiled-nn-bindings") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1cbjkmdjziddx18gnspldnfwl1nq4gcwxpchx0r4g76czb34vvj4")))

(define-public crate-compiled-nn-bindings-0.7.0 (c (n "compiled-nn-bindings") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0zld3d6hagasjz219l3z90l0wj6kr619djqvax5vnqc4ic7j6x7w")))

(define-public crate-compiled-nn-bindings-0.8.0 (c (n "compiled-nn-bindings") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "16cy7nq10y3snjr44pyahs3xygfzylr4ig1i9vnz33fpkp01vbri")))

(define-public crate-compiled-nn-bindings-0.9.0 (c (n "compiled-nn-bindings") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0zgvzm5dda96zb1kmm6cb855qajsam6a16h6sibdzjp4l1gijgm2")))

(define-public crate-compiled-nn-bindings-0.10.0 (c (n "compiled-nn-bindings") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0xy4z7smc8vzl22xrzyzwxa4pwrlarax3yhkkz4f0id6hxa7mjqs")))

(define-public crate-compiled-nn-bindings-0.11.0 (c (n "compiled-nn-bindings") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0g61wq0imlihqs4hi599wpld7imnj2x41pln9dbq07fh7x1kxm89")))

(define-public crate-compiled-nn-bindings-0.12.0 (c (n "compiled-nn-bindings") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0q28zpwgb2mr1snch6wgv0j0wf28cga15ank4v7qn846ivh3nnq1")))

