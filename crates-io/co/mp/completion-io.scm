(define-module (crates-io co mp completion-io) #:use-module (crates-io))

(define-public crate-completion-io-0.1.0 (c (n "completion-io") (v "0.1.0") (d (list (d (n "completion-core") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 2)))) (h "1jvmmp6pww7xx1ckif7r3ff1nyv08p5flicwgl4iw7jvxkgz0ziw")))

(define-public crate-completion-io-0.2.0 (c (n "completion-io") (v "0.2.0") (d (list (d (n "completion-core") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 2)))) (h "1nz6qc8ryszvd5654h1q33q6s5fj0dbksqnkn69fka3634abw9s5")))

