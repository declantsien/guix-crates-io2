(define-module (crates-io co mp compactmap) #:use-module (crates-io))

(define-public crate-compactmap-0.1.0 (c (n "compactmap") (v "0.1.0") (h "0bjjwkprc6bdf7v5lkb2h94ajn5ph3lch7vy6nm4ggk37mg54hy0")))

(define-public crate-compactmap-0.2.0 (c (n "compactmap") (v "0.2.0") (h "1b3mbyri54jrhizqfnplzymcychq73ndg9jkvyy5jx21li5q1v8d")))

(define-public crate-compactmap-0.2.1 (c (n "compactmap") (v "0.2.1") (h "1lzrr3rmkzigmwhx28rv1n45aiq4r6wjq575aymxzqmqghj0f0zr")))

(define-public crate-compactmap-0.3.0 (c (n "compactmap") (v "0.3.0") (h "0a0wili4qngmqpqwy8vwmbdn7wx8i2rggmmng82wk7iha6m2y60q")))

(define-public crate-compactmap-0.3.1 (c (n "compactmap") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gnp22cl2lwkwnaz0qyv5k2gnlnlbg4n6kl5jz9lkkxjj57rp7mr") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.2 (c (n "compactmap") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b6r7q2rnhg5fkb6c2ala7sjmh5cf0c26maba54lb5v4cbri9xy1") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.3 (c (n "compactmap") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jwkpz2rgcz9l8wn1qgvbd43cilqa8bx7y254jkdp5p7p3bygfrp") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.4 (c (n "compactmap") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fg3dq1sl6idbsi39v4450angkgpc4qwlabgda02xdq53wb0klbp") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.5 (c (n "compactmap") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s88x2yzdp910ayvxxvliqf8y9sbh3cb84nsgvd4zr08ff66hr92") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.6 (c (n "compactmap") (v "0.3.6") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 2)))) (h "1szn4jba18575ymzafkhk5wlhh30lya7acdi82gqmwkz60a056fc") (f (quote (("serde_ser_len"))))))

(define-public crate-compactmap-0.3.7 (c (n "compactmap") (v "0.3.7") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 2)))) (h "0bfdmij5pfh9qc0rshjby86mcfnc8ncimkm27gdag67qk64cfgvk") (f (quote (("serde_ser_len"))))))

