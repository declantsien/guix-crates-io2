(define-module (crates-io co mp compile-time-crc32-impl) #:use-module (crates-io))

(define-public crate-compile-time-crc32-impl-0.1.0 (c (n "compile-time-crc32-impl") (v "0.1.0") (d (list (d (n "crc") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gc2xwsggl0i1w9v0h1vlhviji85dmihp527wy98cnpwnsbpiihc")))

