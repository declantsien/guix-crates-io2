(define-module (crates-io co mp compile-time-create-file) #:use-module (crates-io))

(define-public crate-compile-time-create-file-0.0.1 (c (n "compile-time-create-file") (v "0.0.1") (d (list (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "0gg6zynw5jy6rhxdriq100jcxbmix3cgkmdiqic7i6pkpvim4dpd")))

(define-public crate-compile-time-create-file-0.0.2 (c (n "compile-time-create-file") (v "0.0.2") (d (list (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "19mmyyk0ikrr6j3gcngggdipzm5z93zvxlf40drk4wh0a921f420")))

(define-public crate-compile-time-create-file-0.1.0 (c (n "compile-time-create-file") (v "0.1.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "1rpvzhlfqh9jg72ny0f26jxcf2vcgdq5slpngcmk1f2jqq3s3b2y")))

