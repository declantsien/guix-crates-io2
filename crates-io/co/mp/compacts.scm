(define-module (crates-io co mp compacts) #:use-module (crates-io))

(define-public crate-compacts-0.1.0 (c (n "compacts") (v "0.1.0") (d (list (d (n "compacts-bits") (r "^0.1") (d #t) (k 0)) (d (n "compacts-dict") (r "^0.1") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.1") (d #t) (k 0)))) (h "09p9fy298fx0xpm8yxbsya44c9ppmfxglg2bx94y0458i0zrjvmv")))

(define-public crate-compacts-0.1.1 (c (n "compacts") (v "0.1.1") (d (list (d (n "compacts-bits") (r "^0.1.1") (d #t) (k 0)) (d (n "compacts-dict") (r "^0.1.0") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.1.0") (d #t) (k 0)))) (h "1605b8z4kjzykka5bp048y0962nibh4aj6a0xl1162i6iqh6kl7v")))

(define-public crate-compacts-0.2.0 (c (n "compacts") (v "0.2.0") (d (list (d (n "compacts-bits") (r "^0.2") (d #t) (k 0)) (d (n "compacts-dict") (r "^0.2") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)))) (h "19ilz6rb2m25h9b6v0x7nvd0vpj5w55njdc3bywpbgib60rwjd61")))

(define-public crate-compacts-0.2.1 (c (n "compacts") (v "0.2.1") (d (list (d (n "compacts-bits") (r "^0.2") (d #t) (k 0)) (d (n "compacts-dict") (r "^0.2") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)))) (h "09k19akwypd8z03kjwd1xkpisshss0gy4zzxnq5n30mc3mwnncml")))

(define-public crate-compacts-0.3.0 (c (n "compacts") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1g6bdrg0agp781fyj662f50w6kkccbvpk768vnf4dr4hcalrv6b8")))

(define-public crate-compacts-0.3.1 (c (n "compacts") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1r5a26ri8n0i288mv3ag7pdkgchgky866sczk3mljgh050pxhiq3")))

(define-public crate-compacts-0.5.0 (c (n "compacts") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0753is6i744wcnz6cy9rj7aab9i8ak37mxviip63afb4kx3waf9r")))

(define-public crate-compacts-0.5.1 (c (n "compacts") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07y5hvdaakaiim9n0xaavz06gwca5820dbnl5c4n86z0wyk8vrry")))

(define-public crate-compacts-0.6.0 (c (n "compacts") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0hqxivvb3w0n37wslxbnj3slyg4zh6a4fqjkiazfgq1ffq99xb3y")))

(define-public crate-compacts-0.6.1 (c (n "compacts") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "097ar8xihdf34lj948r00rz8wl5nl3pr00nkkj9wygizasd1s9zk") (y #t)))

(define-public crate-compacts-0.6.2 (c (n "compacts") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "0l3l76js2xi6m7aack95lh6y5bvjaw8hk62v7m18dj2r060bi0rb")))

(define-public crate-compacts-0.6.3 (c (n "compacts") (v "0.6.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "1f77zh9s33wb955jlfmi4bch1m4ishbx1cxyfzcz72nsny6d66b1")))

(define-public crate-compacts-0.6.4 (c (n "compacts") (v "0.6.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "0jmpi2bxmji8zm2qic8mqzb6l3x4260jwi5zlnr5ckw8iipl99fb")))

(define-public crate-compacts-0.6.5 (c (n "compacts") (v "0.6.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "119i5dndmlbkkcapq1j9wclswyw6rd5wjln4rfx364wz65aw05ax")))

(define-public crate-compacts-0.7.0 (c (n "compacts") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "1alfyjilw3v9r16y7nkdkv63jwvfqh84zryw1v9jl83vp9jqk6hp")))

(define-public crate-compacts-0.7.1 (c (n "compacts") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "1b5hd5bp4cap40sfvgcf7kycb1k0l9pfkbjhd0b2l1qmknq1283a")))

(define-public crate-compacts-0.7.2 (c (n "compacts") (v "0.7.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "03lmsxfpkdlf29gshwb85wfc9237ay9f35dvivfgjh0n1j20zkwi")))

(define-public crate-compacts-0.8.0 (c (n "compacts") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "1ljfqvc3g1x8mlhb4bdrg5v6yk72bnmx1ws63rxrr85ql935kqfb") (y #t)))

(define-public crate-compacts-0.8.1 (c (n "compacts") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "1id178nsvppsqx2dczn9snsgkh647wxlmgxk2jm449w1xp66skz1") (y #t)))

(define-public crate-compacts-0.9.0 (c (n "compacts") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "snap") (r "^0.2") (d #t) (k 2)) (d (n "zstd") (r "^0.4") (d #t) (k 2)))) (h "0530i6yk1znjldhhxm54pacp4vg2a21in7qz3yc1qxlpgafihzw9")))

