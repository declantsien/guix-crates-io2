(define-module (crates-io co mp compy) #:use-module (crates-io))

(define-public crate-compy-0.0.1 (c (n "compy") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hostfile") (r "^0.2.0") (d #t) (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "pgs-files") (r "^0.0.7") (d #t) (k 0)) (d (n "servicefile") (r "^0.3.0") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "02lwn58aqi5r8y9p7dwmkvs1pr2ygv3wrsa3x2qhhrsilprs3f36")))

