(define-module (crates-io co mp compute-file-server) #:use-module (crates-io))

(define-public crate-compute-file-server-1.1.0 (c (n "compute-file-server") (v "1.1.0") (d (list (d (n "fastly") (r "^0.8.7") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-range") (r "^0.1.5") (d #t) (k 0)) (d (n "httpdate") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0hdn5sxhm8chgddcmjkfm13c0nyhfmr8yq5aaqgknljcya7vz3qc")))

