(define-module (crates-io co mp compose-rt) #:use-module (crates-io))

(define-public crate-compose-rt-0.1.0 (c (n "compose-rt") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x58kylhlnhsq8vj4fpzsn6i7sjrzmb61nic5lgssmz3hi7bjiqx")))

(define-public crate-compose-rt-0.1.1 (c (n "compose-rt") (v "0.1.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xp4ajynbjwfl5vsi5diimdy3i6f55n056r2hhglvbmrd41d7p9z")))

(define-public crate-compose-rt-0.2.0 (c (n "compose-rt") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kqrm0s57xiqi132ramklb7pz358s99z3mp1dkcl76j9yf0qv25a")))

(define-public crate-compose-rt-0.3.0 (c (n "compose-rt") (v "0.3.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "083rblvppmywm5k9lh9kx4c441bxi6cqk4b2y7ahlxgwg4aimabi")))

(define-public crate-compose-rt-0.4.0 (c (n "compose-rt") (v "0.4.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12cj9dm7rxg30ayi4vc1v3zlalsk2xg2vjjdn5gcxlyrmrcfk338")))

(define-public crate-compose-rt-0.5.0 (c (n "compose-rt") (v "0.5.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bvdixnllnkvjhvw1g6q8x4c0slhl6gaq9mq6rkiaasy0npkb43h")))

(define-public crate-compose-rt-0.6.0 (c (n "compose-rt") (v "0.6.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b64m7c32kcz9n30rrwi866w37229shwsqsh5b68s9zbzi8nnkiz")))

(define-public crate-compose-rt-0.7.0 (c (n "compose-rt") (v "0.7.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z1lxijgr7bf1yy1pccc0xvl2lg7jxyj5mbdg8bb69l1sw0sl2zk")))

(define-public crate-compose-rt-0.8.0 (c (n "compose-rt") (v "0.8.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00cknk6ld1jgn2ywbhhirhxdkfai7dac5nq9md9zdrclklr5s2j3")))

(define-public crate-compose-rt-0.9.0 (c (n "compose-rt") (v "0.9.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x0hiipc9vnnv826hw0bshqn0hapkfixf63rw55vp76gfb5p92y0")))

(define-public crate-compose-rt-0.9.1 (c (n "compose-rt") (v "0.9.1") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "045j9wdzwc0340v37pja5jb8azk35igvhwrqi1ghhvp1cldgccbk")))

(define-public crate-compose-rt-0.10.0 (c (n "compose-rt") (v "0.10.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y4ikhw8zzj92z6gy6qsz5idvprjfhi7qwchqwhpgw0ccfyvkwr5")))

(define-public crate-compose-rt-0.11.0 (c (n "compose-rt") (v "0.11.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ad2lmjrsgm9lxb4g549a3j0q1kxbs6iy1vizwhpnk82j101hin7")))

(define-public crate-compose-rt-0.12.0 (c (n "compose-rt") (v "0.12.0") (d (list (d (n "compose-derive") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (d #t) (k 0)))) (h "05rrwjp40lpqxxf973kqmfpbikbcangkpirkihflnpx57i3rwqhb")))

