(define-module (crates-io co mp completion-core) #:use-module (crates-io))

(define-public crate-completion-core-0.1.0 (c (n "completion-core") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.12") (k 0)))) (h "1r9vw7i9s2d3k4cr89l18xhjmflprd5z2w134cnqncl251alfps6") (f (quote (("std" "alloc" "futures-core/std") ("default" "std") ("alloc" "futures-core/alloc"))))))

(define-public crate-completion-core-0.2.0 (c (n "completion-core") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3.12") (k 0)))) (h "1cn8v274g1z0yyy9hgxcr68f1j1pf2gq748cwn5kycdh421w9c3b") (f (quote (("std" "alloc" "futures-core/std") ("default" "std") ("alloc" "futures-core/alloc"))))))

