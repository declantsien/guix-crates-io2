(define-module (crates-io co mp compiler_error) #:use-module (crates-io))

(define-public crate-compiler_error-0.1.0 (c (n "compiler_error") (v "0.1.0") (h "08na11wkv2bx1wi38kpvblh653y46v2gryighaslm72l58crw4f7")))

(define-public crate-compiler_error-0.1.1 (c (n "compiler_error") (v "0.1.1") (h "0irh7c0gznk2k6mj3cmqw7x4pg59lppmy1y8d6k5xc926rnmz5zg")))

