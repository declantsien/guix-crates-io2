(define-module (crates-io co mp compiler_base_span) #:use-module (crates-io))

(define-public crate-compiler_base_span-0.1.0 (c (n "compiler_base_span") (v "0.1.0") (d (list (d (n "rustc_span") (r "^0.0.0") (d #t) (k 0)))) (h "001p6jhkaqw752r69kjgy94x2nl0axavs1c388a2ysgf54rnkylw") (y #t)))

(define-public crate-compiler_base_span-0.0.1 (c (n "compiler_base_span") (v "0.0.1") (d (list (d (n "rustc_span") (r "^0.0.1") (d #t) (k 0)))) (h "18sa6mqw73q2h8bi1d6iv4kg1cfgcl3a7qzjy6j82256ag5cf5hn")))

(define-public crate-compiler_base_span-0.1.1 (c (n "compiler_base_span") (v "0.1.1") (d (list (d (n "rustc_span") (r "^0.1.0") (d #t) (k 0)))) (h "0p8i3s3b9nkhlvmabcpb28l8by8yg412d4kp1cj83x84ip786196")))

(define-public crate-compiler_base_span-0.1.2 (c (n "compiler_base_span") (v "0.1.2") (d (list (d (n "rustc_span") (r "^0.1.1") (d #t) (k 0)))) (h "0rg57myvnqyx1ibcqkx0a9brfqx1glw2pkqyzc6aghr7qsjszs7n")))

(define-public crate-compiler_base_span-0.1.3 (c (n "compiler_base_span") (v "0.1.3") (d (list (d (n "rustc_span") (r "^0.1.2") (d #t) (k 0)))) (h "0bnbx5bmg4wl3n3arw4msmiwmscn7igq6lny3r21id54vwmawam4")))

