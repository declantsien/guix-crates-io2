(define-module (crates-io co mp complexity) #:use-module (crates-io))

(define-public crate-complexity-0.0.0 (c (n "complexity") (v "0.0.0") (h "1ga32s18lx248mj1h0r2p1z78la7x0n35cqf40q5amr5vjaj54b4") (y #t)))

(define-public crate-complexity-0.1.0 (c (n "complexity") (v "0.1.0") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "147l2q4pq1jylk9vbk332fxr8q8ik7m11xq5fa132gwnxh3w3rby")))

(define-public crate-complexity-0.1.1 (c (n "complexity") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 2)) (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "structopt") (r "^0.3.13") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1s0bxanvgycw7rrqcf7ryyww6pc3h0p1pgxw9jx2dzd21q88crb4")))

(define-public crate-complexity-0.2.0 (c (n "complexity") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 2)) (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "structopt") (r "^0.3.13") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "01zbmk336zfrb93vdvzb235hjva8aygvsvnvxbvmfbld3dvm4rd3")))

