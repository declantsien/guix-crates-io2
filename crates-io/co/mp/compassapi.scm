(define-module (crates-io co mp compassapi) #:use-module (crates-io))

(define-public crate-compassapi-0.1.0 (c (n "compassapi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gjfff71bc4dwgy7yih9zn3h608q7yymdw2ixrvfqf6j4zcknmfl")))

(define-public crate-compassapi-0.1.1 (c (n "compassapi") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "090zrivc95bx0lh058qhbbkvvbrj1giz374a4plkxf1zilk0bv13")))

(define-public crate-compassapi-0.1.2 (c (n "compassapi") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zkyyfd9k37yq1k51x473py4456caz3gzp4xv501n30d0hsnqhl1")))

(define-public crate-compassapi-0.1.3 (c (n "compassapi") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n57hzg79i87j377ll3pp0z71sqywzgpacly0b3sqkj8jany5vr1")))

(define-public crate-compassapi-0.1.4 (c (n "compassapi") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "021af3jhb7h7nyyzljj0wwdskdn4d4qmsr8pn79ndvg9b0qc2rxp")))

(define-public crate-compassapi-0.1.5 (c (n "compassapi") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w8dv2rnd5j5z34isdpy0dphg86alrnngpwy6dhkf6a6zwrdq7pw")))

