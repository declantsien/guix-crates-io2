(define-module (crates-io co mp complexible) #:use-module (crates-io))

(define-public crate-complexible-0.2.0 (c (n "complexible") (v "0.2.0") (h "1sy5rr2bvmkdlblkwrhddqbn3fjyyzm7cr1yw57kn6a1pwsqm6wh") (y #t)))

(define-public crate-complexible-0.2.1 (c (n "complexible") (v "0.2.1") (h "16p4zgb6r7cb7xfilf1wg8rsxl1wqkacl0zlgc8bhqy4c7jfggvx") (y #t)))

(define-public crate-complexible-0.2.2 (c (n "complexible") (v "0.2.2") (h "1zfazqf722bc1b505vvjlzrpfq5c36ad43hs6msa5pz6kmw2rks7")))

(define-public crate-complexible-0.2.3 (c (n "complexible") (v "0.2.3") (h "0i95i7hgb5w850pg2f3pks6bfq3jkvrivr0vcx7gd99qblrdx056")))

(define-public crate-complexible-0.2.4 (c (n "complexible") (v "0.2.4") (h "16808g3svplxca1w55smfyqbzihphlmvmjyhf80x0kh6kkaqz16q")))

