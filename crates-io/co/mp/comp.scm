(define-module (crates-io co mp comp) #:use-module (crates-io))

(define-public crate-comp-0.1.2 (c (n "comp") (v "0.1.2") (h "0z0yy1z44ajgimq6cy0jp06w6l9s653ih041h7d61ldrs7qqwnk7") (y #t)))

(define-public crate-comp-0.1.3 (c (n "comp") (v "0.1.3") (h "078jikkyf3gf9d53bz1pg2arv5sabx3fvamdlhdpdqqq02iwxrpm") (y #t)))

(define-public crate-comp-0.2.1 (c (n "comp") (v "0.2.1") (h "0j892ysqbh5v9i4a32p7487fvz6n72j1sa8fmq6nz8kikzicmdmp")))

