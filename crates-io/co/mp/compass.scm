(define-module (crates-io co mp compass) #:use-module (crates-io))

(define-public crate-compass-0.0.1 (c (n "compass") (v "0.0.1") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18wzrz4c85nyz0ldgazcbvlp6m6gmh655c8f9n6lkhs33l3jfbjb")))

