(define-module (crates-io co mp complex_algebra) #:use-module (crates-io))

(define-public crate-complex_algebra-0.1.0 (c (n "complex_algebra") (v "0.1.0") (h "0gvfm6df1vz72s50r1zd95v3dpx16bppci1kggrsv7dk9b0r5a6q")))

(define-public crate-complex_algebra-0.1.1 (c (n "complex_algebra") (v "0.1.1") (h "0xi3vi1ay0gncx6s9kik7kwrb335krfqf9v94f2a17d6rasp5n2w")))

(define-public crate-complex_algebra-0.1.2 (c (n "complex_algebra") (v "0.1.2") (h "1vjznr362bm5n7x6rnpri2ngxjvm0pass9gifxwzw05hqxrlixhg")))

(define-public crate-complex_algebra-0.1.3 (c (n "complex_algebra") (v "0.1.3") (h "142aii2df4m7kysps1cnd2pfxyaaqvrgp8k0v6g0b0akl5y223y7")))

(define-public crate-complex_algebra-0.1.4 (c (n "complex_algebra") (v "0.1.4") (h "1f9rqpw2k0nyswa5bvy14kmmin7mnb69l8rba74jhki46jmibmz6")))

(define-public crate-complex_algebra-0.1.5 (c (n "complex_algebra") (v "0.1.5") (h "1ljj3wkv9qa1xi5r378bmmv202773wrgqllm9ax4dhzcpyp3anhl")))

(define-public crate-complex_algebra-0.1.6 (c (n "complex_algebra") (v "0.1.6") (h "115jqzdjg5f9wlz2vgg7qm27fjyyii9aqqx130lp44ah22ri1z3x")))

(define-public crate-complex_algebra-0.1.7 (c (n "complex_algebra") (v "0.1.7") (h "0micfjn9k6cjjpqmnv1rn30976hqgrkbzp4z2012hnvyxsk3r7as")))

(define-public crate-complex_algebra-0.1.8 (c (n "complex_algebra") (v "0.1.8") (h "0v7fyrjhqxa7i92irbvbawwcrbpbam31lc9fny4m8pqp9xjxmcwd")))

