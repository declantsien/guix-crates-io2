(define-module (crates-io co mp compact-encoding) #:use-module (crates-io))

(define-public crate-compact-encoding-1.0.0 (c (n "compact-encoding") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1qr0k1p1adk0csn215y3d22z947jbq24rvpibwvl0warhksh31sh")))

(define-public crate-compact-encoding-1.1.0 (c (n "compact-encoding") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0kz8c8m4i4kj8qrplpn6h6hsr855k7lkvy9hgph1qznqk14bxmdj")))

