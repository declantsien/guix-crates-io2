(define-module (crates-io co mp compiled-uuid) #:use-module (crates-io))

(define-public crate-compiled-uuid-0.1.0 (c (n "compiled-uuid") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1zmiaqny6d543a5sms2mkjp74nyqy6hx1j74fl0hac2nc39n8mcg")))

(define-public crate-compiled-uuid-0.1.1 (c (n "compiled-uuid") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1l2h951wq4asg554nyf6wg221mbw0zc01ilpsxj3xcjab4rbybpd")))

(define-public crate-compiled-uuid-0.1.2 (c (n "compiled-uuid") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "042n07frdzndk202fg8q4rwkf5zqkl1y9kx0lsmawf5vrc2g9j8q")))

