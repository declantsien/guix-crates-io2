(define-module (crates-io co mp completable_future) #:use-module (crates-io))

(define-public crate-completable_future-0.2.0 (c (n "completable_future") (v "0.2.0") (d (list (d (n "futures") (r "^0.2.1") (d #t) (k 0)))) (h "0phm14j3rppz0dx6fhhw24spi1vnfgkywkcnbpqx7zma2lsjjlag")))

(define-public crate-completable_future-0.1.0 (c (n "completable_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio") (r "^0.1.6") (d #t) (k 2)))) (h "0rqir5gn5vr9ii5dwx2yh8ifwv78k4wjk239afxq5qii5wafvfb1")))

