(define-module (crates-io co mp compacts-bits) #:use-module (crates-io))

(define-public crate-compacts-bits-0.1.0 (c (n "compacts-bits") (v "0.1.0") (d (list (d (n "compacts-prim") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "karabiner") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0pnkxi0kwihip0cswkv0vxrxqwgqsmxf8l06x0z49il1sb1qijs3")))

(define-public crate-compacts-bits-0.1.1 (c (n "compacts-bits") (v "0.1.1") (d (list (d (n "compacts-prim") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "karabiner") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0q8dvzy3rycdah2kh8vl0v880wn5dmz29177x16l2j2xxnyjrq86")))

(define-public crate-compacts-bits-0.2.0 (c (n "compacts-bits") (v "0.2.0") (d (list (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "karabiner") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14f7c61s0x80f1s26ik2kzz186w7b0a5n4vqdlgc1fgd57zqbg8v")))

(define-public crate-compacts-bits-0.2.1 (c (n "compacts-bits") (v "0.2.1") (d (list (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "karabiner") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vrnmzsna0z1vznp118bv4vwka1ywl605s1m2l9z83sw0vcx2b4l")))

