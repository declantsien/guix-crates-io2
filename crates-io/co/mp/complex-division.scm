(define-module (crates-io co mp complex-division) #:use-module (crates-io))

(define-public crate-complex-division-1.0.0 (c (n "complex-division") (v "1.0.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1l3kjpx6gk9a2yh2y5nww1liizh3751ihq0inbjxfxg97szpnwv9")))

(define-public crate-complex-division-1.0.1 (c (n "complex-division") (v "1.0.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "19q7yazmpi9ddakw50l3sam20gam7qx9fiwj7pg5kklcq5zbdn5x")))

