(define-module (crates-io co mp composite_types) #:use-module (crates-io))

(define-public crate-composite_types-0.1.0 (c (n "composite_types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "18m56mpqkda3pcxnpfzr7w4r3yl9ks1v8z85ilhibn24kd036klm") (r "1.71.0")))

(define-public crate-composite_types-0.1.1 (c (n "composite_types") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0vs9x3n33w0zi6v0izg17w4hj5g9f2ykdfvs3jl4vmv89k30xqj8") (r "1.71.0")))

