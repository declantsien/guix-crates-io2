(define-module (crates-io co mp complot) #:use-module (crates-io))

(define-public crate-complot-0.1.0 (c (n "complot") (v "0.1.0") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0kyk4kh34xn9mhhb4lrpjfxm65yibfc90lf76wis62l2wdfwxjjn")))

(define-public crate-complot-0.1.1 (c (n "complot") (v "0.1.1") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "07wbl5kqp5841nxk8x8fs2klpin71w3xywp1npc2cw67n8syih1k")))

(define-public crate-complot-0.2.0 (c (n "complot") (v "0.2.0") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "03vws4d65wbqfqbr67v6blf9w7l3kgfgf8axqqvdqbvykg4wc14p")))

(define-public crate-complot-0.3.0 (c (n "complot") (v "0.3.0") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0v9g87k9n4m96jbjyw85vzzqahb2qpkamhh7k75yi0hbvwfg3qx0")))

(define-public crate-complot-0.3.1 (c (n "complot") (v "0.3.1") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0qyjclmppqllvh6xfk25jqz71dm1xklcbw0ljrfqy57sisrgljav") (f (quote (("svg") ("png"))))))

(define-public crate-complot-0.3.2 (c (n "complot") (v "0.3.2") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "07fjpdf1xximkln2v6rhbx0d3z4sqsggdffvs055pwmya3gsrr4j") (f (quote (("svg") ("png") ("default" "png"))))))

(define-public crate-complot-0.3.3 (c (n "complot") (v "0.3.3") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "1g6hny90lcza6g4c70bhjq2ad8flpw4w1aq8ajiy0v7q4mkf5iwg") (f (quote (("svg") ("png") ("default" "png"))))))

(define-public crate-complot-0.3.4 (c (n "complot") (v "0.3.4") (d (list (d (n "colorous") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)))) (h "0kx8fq1dy1awmjhgp59x6phd3rvblfyhpizc54g91hjabilg2dqf") (f (quote (("svg") ("png") ("default" "png"))))))

