(define-module (crates-io co mp complex-bessel-rs) #:use-module (crates-io))

(define-public crate-complex-bessel-rs-0.0.1 (c (n "complex-bessel-rs") (v "0.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "09wz6bf874iavvnw9zv3f6kjcw1mcpw9xg9r4kxjzjlgzk7350lj")))

(define-public crate-complex-bessel-rs-0.0.2 (c (n "complex-bessel-rs") (v "0.0.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1pjaqmka8zv7b7ykawwsxn6vqi3w8il6spxn973ah8r77n0jgpvj")))

(define-public crate-complex-bessel-rs-0.0.3 (c (n "complex-bessel-rs") (v "0.0.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1xmy2c73n1v3gdpgj0fjf6sf9zf0rf4w0wy0mdr30925sqdcjsi3")))

(define-public crate-complex-bessel-rs-0.0.4 (c (n "complex-bessel-rs") (v "0.0.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1w539walwbb8kk5qswxid9di2i41cpcz2hnwnxsr5ddk6lyhcf05")))

(define-public crate-complex-bessel-rs-0.0.5 (c (n "complex-bessel-rs") (v "0.0.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "05n89z00gnpmav2nki2x8ra8h62zn0z0wgnxl9kvl5schk34l685")))

(define-public crate-complex-bessel-rs-1.0.0 (c (n "complex-bessel-rs") (v "1.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0lyz0b7a6ljfxlpwaxgqp2mb4v37rqz8fvsrz7jyi0h9kizw7gml")))

(define-public crate-complex-bessel-rs-1.1.0 (c (n "complex-bessel-rs") (v "1.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0i0axhnvswcfsqv5hh73qzmc3bv2hnhl43jlcsi4pakk60b8yl5h")))

(define-public crate-complex-bessel-rs-1.2.0 (c (n "complex-bessel-rs") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1x5y6vhxhn2cazghpk5y0mpa53lwpiqpvvlq3s10sf48blv4lk4c")))

