(define-module (crates-io co mp components) #:use-module (crates-io))

(define-public crate-components-0.1.0 (c (n "components") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("oleauto" "combaseapi" "objbase" "windef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d383377w6khg09185fmriv5rj5aavidwdhcx358a3a19zi0r6fb")))

