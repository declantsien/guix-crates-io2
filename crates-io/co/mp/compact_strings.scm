(define-module (crates-io co mp compact_strings) #:use-module (crates-io))

(define-public crate-compact_strings-0.1.0 (c (n "compact_strings") (v "0.1.0") (h "1gxl4rb9zv6v7h0kmkqg5s3ax0z6fgb2pmnjh86kmjs0dgfc5gv2") (y #t) (r "1.56.1")))

(define-public crate-compact_strings-1.0.0 (c (n "compact_strings") (v "1.0.0") (h "1hnf2gh3rdixbiaim2q28j7j2bb418v60xridjy31fwhq9szqjjm") (y #t) (r "1.56.1")))

(define-public crate-compact_strings-1.1.0 (c (n "compact_strings") (v "1.1.0") (h "1fk51sqv4lzv9n7acw435n22r9cr5kpxmawkj0kad2xlva21g0i2") (y #t) (r "1.56.1")))

(define-public crate-compact_strings-1.2.2 (c (n "compact_strings") (v "1.2.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1237iksgxzqq3i4s07ys253i71lzi6nvq715z33v2dlmw9dniifh") (y #t) (r "1.56.1")))

(define-public crate-compact_strings-2.0.0 (c (n "compact_strings") (v "2.0.0") (h "1ga7nvq3kijimy2cp1j942g0pvbgb7iivk74m2hb9akh7yscci8y") (y #t) (r "1.56.1")))

(define-public crate-compact_strings-3.0.0 (c (n "compact_strings") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0xq1bfq32dcqzrjk041brwawjh96knddlfxpkf37pqapp3xpm8jw") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.0.0 (c (n "compact_strings") (v "4.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1hk4sazw6wzaq8lmgs5zwfrfq94zhlzw3mgyqgvcq9ar136p0m7g") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.0.1 (c (n "compact_strings") (v "4.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1bjifbrnv8s62yri1d1qrpjjmdlhyv6i33qs1xr24gqr19rwjpnp") (f (quote (("no_unsafe") ("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.0.2 (c (n "compact_strings") (v "4.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0kgrah8d3drvkqfqnw7jb2scis1dybwgscp1fc4ygdhxlknb2xn1") (f (quote (("no_unsafe") ("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.1.0 (c (n "compact_strings") (v "4.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1z9ps5h947816ilrvybgkp57gc9vyrhqn07n5jd2cqjy7y0c08kh") (f (quote (("no_unsafe") ("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.1.1 (c (n "compact_strings") (v "4.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "05pg3faxnmqwa15ny1xsxxj9sf7jdfl4vf0p3d3sxz0wskrbfwdy") (f (quote (("no_unsafe") ("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-compact_strings-4.1.2 (c (n "compact_strings") (v "4.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "13ns2sma37ksq9fvnsykhvlq80nx3jzhcs0ysmdv4bmnsff73sj2") (f (quote (("no_unsafe") ("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

