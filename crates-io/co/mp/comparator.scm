(define-module (crates-io co mp comparator) #:use-module (crates-io))

(define-public crate-comparator-0.1.0 (c (n "comparator") (v "0.1.0") (h "1vk1xs4cq8hy7c79laqwm4ppx64a25pkk0s7ir93mpgpigb00697") (f (quote (("std") ("default" "std"))))))

(define-public crate-comparator-0.1.1 (c (n "comparator") (v "0.1.1") (h "0rgp3p75b8ixss09059dadiz0qbvzzfmsbm66k5grb6lf0vqpq9m") (f (quote (("std") ("default" "std"))))))

(define-public crate-comparator-0.2.0 (c (n "comparator") (v "0.2.0") (h "0whkaaxnqxcifx674sn87ahiah9wrzyjnriki7h8br4d9yz6kg29") (f (quote (("std") ("default" "std"))))))

(define-public crate-comparator-0.2.1 (c (n "comparator") (v "0.2.1") (h "1xk96465h71n691nyjvp5nkwa69b9a6m8rzamxb17irba0z8j25d") (f (quote (("std") ("default" "std"))))))

(define-public crate-comparator-0.3.0 (c (n "comparator") (v "0.3.0") (h "1pclamivrn73q7nhqkm2h2f29fpzagiv312fra5vr1snshcfrsab") (f (quote (("std") ("default" "std"))))))

