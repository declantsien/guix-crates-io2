(define-module (crates-io co mp compact-genome) #:use-module (crates-io))

(define-public crate-compact-genome-0.1.0-alpha.3 (c (n "compact-genome") (v "0.1.0-alpha.3") (h "15915cvs54a8658z2z608snbykjsmsm1yvf9p123jcncmna4nvl3")))

(define-public crate-compact-genome-0.1.0-alpha.4 (c (n "compact-genome") (v "0.1.0-alpha.4") (h "02a3435rcdavcqbwkp0cvsiid4acx4kpx107q3arbfb7r2ls3n6d")))

(define-public crate-compact-genome-0.1.0-alpha.5 (c (n "compact-genome") (v "0.1.0-alpha.5") (h "1mf36xzdpg8mw744zij8ny8igmi9b0rgspjpi391vvr6fdmq6lq6")))

(define-public crate-compact-genome-0.1.0-alpha.6 (c (n "compact-genome") (v "0.1.0-alpha.6") (h "1py889a6rz1nmd8zaijkfcdnyvnckhi5f4fccqlf55kkwrr4ka69")))

(define-public crate-compact-genome-0.1.0-rc.1 (c (n "compact-genome") (v "0.1.0-rc.1") (h "1sa2pmx0bfql0f3vvwclgvh7hnwmqnm5zgdr09ysyxzakwdh94n4")))

(define-public crate-compact-genome-0.1.0 (c (n "compact-genome") (v "0.1.0") (h "0wsgza8flqcypwl2jd92zarvni2fpsysd4s2kj8aknzlpjkbd7lz")))

(define-public crate-compact-genome-0.2.0 (c (n "compact-genome") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "1lmyvabr4d67zc37r3kw2wv7273qpdaf0v9bvg3jayyl5hi5dn5f")))

(define-public crate-compact-genome-0.3.0 (c (n "compact-genome") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.0") (d #t) (k 0)))) (h "13jx19ma725xq9nygw7ps0qxh0yzpnbm66bczmwj704zy7jjjw1c")))

(define-public crate-compact-genome-0.3.1 (c (n "compact-genome") (v "0.3.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.1") (d #t) (k 0)))) (h "0wy36n8zfh1yp961g9q15dvqxsih97v95scjzmf2qzf5djzbhy7w")))

(define-public crate-compact-genome-0.3.1-alpha.1 (c (n "compact-genome") (v "0.3.1-alpha.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.3.1-alpha.0") (d #t) (k 0)))) (h "0qbwn9lx67pm8cfs0iyrs6fyzrn0bp5dvvf34ymvkdlr29sdwkqn")))

(define-public crate-compact-genome-0.4.0 (c (n "compact-genome") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0ywahjgd6whgn5l683d3i1d59vjskmyqmgc61934907i7zf265cf")))

(define-public crate-compact-genome-0.4.1 (c (n "compact-genome") (v "0.4.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1i522r00l8sniy828kg0p3x6fzmk555z2gp84scm733d75qpwjbi")))

(define-public crate-compact-genome-0.4.2 (c (n "compact-genome") (v "0.4.2") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "08iyr595fag5yfy00lsjba1rp25khnra0jh155dpnw8d26jb9lfs")))

(define-public crate-compact-genome-0.4.3 (c (n "compact-genome") (v "0.4.3") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1pa89zccbjbvxmgg1saqi73km9j53dg01a9n5zdfsxxp39jzh116")))

(define-public crate-compact-genome-0.4.4 (c (n "compact-genome") (v "0.4.4") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "0qjp55vq832rmmgakylcfimxdnl0sm9z5lq9w15dy9ly5yc0mmlm")))

(define-public crate-compact-genome-0.4.5 (c (n "compact-genome") (v "0.4.5") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "18vip7r7d4sy383nf36i10cl3jwdgd1hj0v97gc808lbcf7ca3a5")))

(define-public crate-compact-genome-0.4.6 (c (n "compact-genome") (v "0.4.6") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1jmn5xmfhranxkakgsdn4y8qic1d5q0cs55167b8b8aghbr83lj3")))

(define-public crate-compact-genome-0.4.9 (c (n "compact-genome") (v "0.4.9") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "1ji9548pixrzi3cjdkqvg1mk79ysr09ph11l5lzk9sqkjp2yjwh2")))

(define-public crate-compact-genome-0.4.10 (c (n "compact-genome") (v "0.4.10") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "traitsequence") (r "^0.4.0") (d #t) (k 0)))) (h "06530b66w8jm2yhyx6nxa4qhsa41xf2jx6pkm333dbkjdbdskf1m")))

(define-public crate-compact-genome-0.6.0 (c (n "compact-genome") (v "0.6.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^0.6.0") (d #t) (k 0)))) (h "18q4i53viwhfb50h5l5l224s5cyvg50xz11zf1wc58ih63lv1dqz")))

(define-public crate-compact-genome-0.7.0 (c (n "compact-genome") (v "0.7.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^0.6.0") (d #t) (k 0)))) (h "04i7k48r3mnvnjyvq93lk7wk2hfb1cpgfi8v2jzlg9g0qv8wvzvp") (r "1.58.1")))

(define-public crate-compact-genome-0.8.0 (c (n "compact-genome") (v "0.8.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^0.10.0") (d #t) (k 0)))) (h "1365bl87mm68sy70bzkxbmfsqxs28h0j4qhgqp95hspfr8dciflj") (r "1.58.1")))

(define-public crate-compact-genome-1.0.0 (c (n "compact-genome") (v "1.0.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.0.0") (d #t) (k 0)))) (h "0hfmh1ly77q2r3n0f3jzz8ncfi5acmpm5dsq80zic47fjxvpz2ak") (r "1.58.1")))

(define-public crate-compact-genome-1.1.0 (c (n "compact-genome") (v "1.1.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "19spkq314pj6qr1a3pjjs6l1n3z9737b12lklb0vh6fj9vr43rl4") (r "1.58.1")))

(define-public crate-compact-genome-1.2.0 (c (n "compact-genome") (v "1.2.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "0npzblpq5g8gndra9m26wxiphvcba7gvqiqbja4hyy9d3303mcby") (y #t) (r "1.58.1")))

(define-public crate-compact-genome-1.2.1 (c (n "compact-genome") (v "1.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "0dm7nmv71zwqr71yxb524fgb409jj7mxkrjk0a2d7kj03wkdpgwn") (r "1.58.1")))

(define-public crate-compact-genome-1.3.0 (c (n "compact-genome") (v "1.3.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "0nn2b9nsr1ypj77bsh45q1ywkql0npghl85w13dj5bsq0v2z6fln") (r "1.58.1")))

(define-public crate-compact-genome-1.3.1 (c (n "compact-genome") (v "1.3.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^1.1.0") (d #t) (k 0)))) (h "0pd5fvshxvc7pyf4vyrsdvin1ckj168d0k63462sgb0yzbj9mnz1") (r "1.59.0")))

(define-public crate-compact-genome-2.0.0 (c (n "compact-genome") (v "2.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "traitsequence") (r "^2.0.0") (d #t) (k 0)))) (h "1f20jwr88b8zvsijnmdm42zpw57fh17zbsihyjd30cpwjsamnsz7") (r "1.59.0")))

(define-public crate-compact-genome-2.1.0 (c (n "compact-genome") (v "2.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "traitsequence") (r "^2.0.0") (d #t) (k 0)))) (h "00z875k0fa921vd8gh0gcsbii7m89q2m3rizz6anxv9jmaqniaj4") (r "1.59.0")))

(define-public crate-compact-genome-2.1.1 (c (n "compact-genome") (v "2.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "traitsequence") (r "^2.0.0") (d #t) (k 0)))) (h "1qnpkf90w8vvrr5lxf9g0xgjl34cx9brck0bsyrn29b4svgzb6vv") (r "1.65.0")))

