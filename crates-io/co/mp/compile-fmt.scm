(define-module (crates-io co mp compile-fmt) #:use-module (crates-io))

(define-public crate-compile-fmt-0.1.0 (c (n "compile-fmt") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0038iy8dgfpp6y8xbvjj5cm4zyzfikmiwhnn71z2rra2xm3r1mmy") (r "1.65")))

