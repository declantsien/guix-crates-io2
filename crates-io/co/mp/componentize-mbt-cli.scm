(define-module (crates-io co mp componentize-mbt-cli) #:use-module (crates-io))

(define-public crate-componentize-mbt-cli-0.1.0 (c (n "componentize-mbt-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "componentize-mbt") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.16.0") (d #t) (k 0)) (d (n "wit-bindgen-mbt") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "12nwidqlff89pkisx1ncp2z0ly904axlp1d8ikch8jgzlad2fl1h")))

(define-public crate-componentize-mbt-cli-0.2.0 (c (n "componentize-mbt-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "componentize-mbt") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.16.0") (d #t) (k 0)) (d (n "wit-bindgen-mbt") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "0sff4qppmc6w2q7f13y6yhhdlhkgpaih9hll2zw24hy7niw26041")))

