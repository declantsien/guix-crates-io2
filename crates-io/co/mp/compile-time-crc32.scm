(define-module (crates-io co mp compile-time-crc32) #:use-module (crates-io))

(define-public crate-compile-time-crc32-0.1.0 (c (n "compile-time-crc32") (v "0.1.0") (d (list (d (n "compile-time-crc32-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1h4qiil6dcb2lvl48qvk1y582hfv74g2bkr3c294pwcc9cpl4q37")))

(define-public crate-compile-time-crc32-0.1.1 (c (n "compile-time-crc32") (v "0.1.1") (d (list (d (n "compile-time-crc32-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1vvimf9y49ykwgpdsc74yfqb0snd52q12lqlczn63c880yc47vfw")))

(define-public crate-compile-time-crc32-0.1.2 (c (n "compile-time-crc32") (v "0.1.2") (d (list (d (n "compile-time-crc32-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0sm3x4cfxjg7bylvsm024hmbdz2b50biff8c991dp1pvnzcsd7r7")))

