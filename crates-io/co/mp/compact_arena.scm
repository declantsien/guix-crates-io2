(define-module (crates-io co mp compact_arena) #:use-module (crates-io))

(define-public crate-compact_arena-0.1.0 (c (n "compact_arena") (v "0.1.0") (h "1dh6d6y9ld9dqj9ndpy80ihmpjyqz41azp5qr2asjs83p3428smn") (f (quote (("uninit")))) (y #t)))

(define-public crate-compact_arena-0.1.1 (c (n "compact_arena") (v "0.1.1") (h "10w8l4xg6fiy66ryx2khij3lffmfb104162nx9wj1h09xkxd6z9m") (f (quote (("uninit")))) (y #t)))

(define-public crate-compact_arena-0.2.0 (c (n "compact_arena") (v "0.2.0") (h "0z5awwnbl79jn6whbpwkfzhznpdd8zbgp3733h80jm11wq6vb76p") (f (quote (("uninit")))) (y #t)))

(define-public crate-compact_arena-0.2.1 (c (n "compact_arena") (v "0.2.1") (h "0csfwvr2blxi9l9vhn09iglcyjql06p05zz672nm781brb56473s") (f (quote (("uninit")))) (y #t)))

(define-public crate-compact_arena-0.3.0 (c (n "compact_arena") (v "0.3.0") (h "0vv0v7hp71fyxrkd966v6ny2awm4vpal7z0idvbqpscrblavff27") (f (quote (("uninit") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-compact_arena-0.3.1 (c (n "compact_arena") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 2)))) (h "18p5f7ak942jg5zfjhl34xmcig30rnrv4zj44ajrfpn59xq4lg9f") (f (quote (("uninit") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-compact_arena-0.3.2 (c (n "compact_arena") (v "0.3.2") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 2)))) (h "1y937b7rabfb9imy0ic0jfys27vsvbrkmq9gdq4jb6yvi6c1shxq") (f (quote (("uninit") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-compact_arena-0.4.0 (c (n "compact_arena") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 2)))) (h "0hb1mhal7lzv1lm8hh4c6702sys7ga44jlfvsmsm01wjxmdqrc2a") (f (quote (("uninit") ("default" "alloc") ("alloc"))))))

(define-public crate-compact_arena-0.4.1 (c (n "compact_arena") (v "0.4.1") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("stable"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)))) (h "027gwvrls18k9rzn3nz8i2swh93q1acq1vys8fz8nq30wgzwchjj") (f (quote (("uninit") ("default" "alloc") ("alloc"))))))

