(define-module (crates-io co mp compiled-nn) #:use-module (crates-io))

(define-public crate-compiled-nn-0.1.0 (c (n "compiled-nn") (v "0.1.0") (d (list (d (n "compiled-nn-bindings") (r "^0.1.0") (d #t) (k 0)))) (h "0dbjs88rzd95hil5nr1n73dbvf9b72a98cwpxvb6bs4da270y14y")))

(define-public crate-compiled-nn-0.2.0 (c (n "compiled-nn") (v "0.2.0") (d (list (d (n "compiled-nn-bindings") (r "^0.2.0") (d #t) (k 0)))) (h "00n7fq279wqbrkgkj44hcjqr9pb1kfj4wcg7ji56amvzmzwq3i1n")))

(define-public crate-compiled-nn-0.3.0 (c (n "compiled-nn") (v "0.3.0") (d (list (d (n "compiled-nn-bindings") (r "^0.3.0") (d #t) (k 0)))) (h "1c03v5ac7fxah73kn3f58hvk0acwx6355r4z8vf6xsww3avnwpbb")))

(define-public crate-compiled-nn-0.5.0 (c (n "compiled-nn") (v "0.5.0") (d (list (d (n "compiled-nn-bindings") (r "^0.5.0") (d #t) (k 0)))) (h "0x804caqb6xsnsicnk32idavwzzvw0ijq2whmpinf1fqpnwir7ja")))

(define-public crate-compiled-nn-0.6.0 (c (n "compiled-nn") (v "0.6.0") (d (list (d (n "compiled-nn-bindings") (r "^0.6.0") (d #t) (k 0)))) (h "0wp8mw41x2m3aqlqhpy74akxqzxqgpv3h7hf5rgai7aki80bv3lw")))

(define-public crate-compiled-nn-0.7.0 (c (n "compiled-nn") (v "0.7.0") (d (list (d (n "compiled-nn-bindings") (r "^0.7.0") (d #t) (k 0)))) (h "0kndwg8rg14sbldxrl5wxpn7vdqm4b5q44g0bjhvzh9f42vhbhpn")))

(define-public crate-compiled-nn-0.8.0 (c (n "compiled-nn") (v "0.8.0") (d (list (d (n "compiled-nn-bindings") (r "^0.8.0") (d #t) (k 0)))) (h "191391whkhwwfyyxk9iyn4bjgpcr4a811vwirxdnwhiwb7yp0lzc")))

(define-public crate-compiled-nn-0.9.0 (c (n "compiled-nn") (v "0.9.0") (d (list (d (n "compiled-nn-bindings") (r "^0.9.0") (d #t) (k 0)))) (h "0jwm8wqqrs7w33z1jj6j22r7ka7g8c042lpzq113vnn0ra1x9v7g")))

(define-public crate-compiled-nn-0.10.0 (c (n "compiled-nn") (v "0.10.0") (d (list (d (n "compiled-nn-bindings") (r "^0.10.0") (d #t) (k 0)))) (h "0axvgp6cd4kc500l96cyzcggnbdndybcr6g6fl663k2n6i44jlln")))

(define-public crate-compiled-nn-0.11.0 (c (n "compiled-nn") (v "0.11.0") (d (list (d (n "compiled-nn-bindings") (r "^0.11.0") (d #t) (k 0)))) (h "1nnajfmc7vgpcp08qikdzfhyckvq5qqq87ikrhwmd07yvqrk4iry")))

(define-public crate-compiled-nn-0.12.0 (c (n "compiled-nn") (v "0.12.0") (d (list (d (n "compiled-nn-bindings") (r "^0.12.0") (d #t) (k 0)))) (h "0kfww5wabrv41b4hrgrjpw8yly8sgy8wi75c9kldac40pc5a958r")))

