(define-module (crates-io co mp compiler-interrupts) #:use-module (crates-io))

(define-public crate-compiler-interrupts-1.0.0 (c (n "compiler-interrupts") (v "1.0.0") (h "09wm7c2zmkzvsvv4b6ms5m8ra5dm9iq6arv5pwpg5m2b44xdqdf1")))

(define-public crate-compiler-interrupts-1.0.1 (c (n "compiler-interrupts") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "nanorand") (r "^0.6") (d #t) (k 2)) (d (n "nix") (r "^0.22") (d #t) (k 2)))) (h "0i70kc68apclg2b6bkxjskrfs5hhvr7x3jd5c5cry0blrfv587bn")))

