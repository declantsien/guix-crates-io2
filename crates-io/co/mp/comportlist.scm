(define-module (crates-io co mp comportlist) #:use-module (crates-io))

(define-public crate-comportlist-0.1.0 (c (n "comportlist") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "07li99hghmlx3xamhc69g0f3qrk7fr4zi1njsxr0cy00a67443s9") (r "1.65")))

(define-public crate-comportlist-0.1.1 (c (n "comportlist") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "113p3mpx1kwpi0m45417qkk3my4cyw6c72fdh2gqllh1rf469d4y") (r "1.61")))

(define-public crate-comportlist-0.1.2 (c (n "comportlist") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "1864fqx4w44di35jmscc6gk73rwszxxq79nisfygrfrnzr5469dw") (r "1.61")))

(define-public crate-comportlist-0.1.3 (c (n "comportlist") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "012k9vcdj95az91afhplz58n6paql7jysi7f6bn40jd6zain159d") (r "1.61")))

(define-public crate-comportlist-0.1.4 (c (n "comportlist") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "1h4n7knfcdq79m2w5lgj3l198qp95b376nnnnlm2q2rd7sh7qypl") (r "1.61")))

(define-public crate-comportlist-1.0.0 (c (n "comportlist") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "0mhwjh6dh55djr4pxigpsn150s0ka7i6vhvivbv50kvqpfgh1rdl") (r "1.61")))

(define-public crate-comportlist-1.1.0 (c (n "comportlist") (v "1.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "commapi" "errhandlingapi" "fileapi" "guiddef" "handleapi" "minwinbase" "minwindef" "ntdef" "setupapi" "winbase" "winerror" "winnt" "ioapiset" "synchapi"))) (d #t) (k 0)))) (h "0gn3jc94zdihmp6hrkffdp0bjapzf7dflcv6waqms06dgsnzgmlk") (y #t) (r "1.61")))

(define-public crate-comportlist-1.2.1 (c (n "comportlist") (v "1.2.1") (d (list (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("cguid" "errhandlingapi" "setupapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "1lbj8j01sn0qgr8hflgqp4yd5hhcc9zhqqfdxyvbifw2pqgawqhp") (r "1.61")))

