(define-module (crates-io co mp compiler-test-derive) #:use-module (crates-io))

(define-public crate-compiler-test-derive-2.4.3 (c (n "compiler-test-derive") (v "2.4.3") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "1g9vz5phij2vh1vnqgcgfhvlhdfzbwznwkqbqc3xdl2xj2w59nhi")))

(define-public crate-compiler-test-derive-2.4.1 (c (n "compiler-test-derive") (v "2.4.1") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "0gd5xggnpd24vxbn71ym5wryc2690wqaxz72b6krwifd0h25yb0f")))

