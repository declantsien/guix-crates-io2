(define-module (crates-io co mp composing) #:use-module (crates-io))

(define-public crate-composing-0.1.0 (c (n "composing") (v "0.1.0") (h "1rba4agin42splkd051bicxbj08rfvi8bdlay1fa2zyab3jwmia2")))

(define-public crate-composing-0.1.1 (c (n "composing") (v "0.1.1") (h "0is0sixk4wfyqm5258ny5c2f8yjln8p13gl396xri5g3i0qim0rf")))

(define-public crate-composing-0.2.0 (c (n "composing") (v "0.2.0") (h "0a3ihpm4y06zprnbs39ya9p9im7w6k018cr6328vmm62xrm50573") (f (quote (("std") ("default" "std"))))))

