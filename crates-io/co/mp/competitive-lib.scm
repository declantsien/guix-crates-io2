(define-module (crates-io co mp competitive-lib) #:use-module (crates-io))

(define-public crate-competitive-lib-0.1.0 (c (n "competitive-lib") (v "0.1.0") (h "04sjlq0il0k3c7m8xmsn6x0k75v3c75p9s0l5fk67nzckwk7b3jc") (y #t)))

(define-public crate-competitive-lib-0.1.1 (c (n "competitive-lib") (v "0.1.1") (h "0f50dwqfv9qr7n2a92j7wfynv9i1s9r5jy8gns2wjk4j26qpx7y1") (y #t)))

