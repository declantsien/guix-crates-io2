(define-module (crates-io co mp compile-time-run-macro) #:use-module (crates-io))

(define-public crate-compile-time-run-macro-0.1.0 (c (n "compile-time-run-macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0gjychdclcdzkf1102pc3ghgxvfdfxxd1h3z5nr80qyzzdyj0r15")))

(define-public crate-compile-time-run-macro-0.2.0 (c (n "compile-time-run-macro") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0lsmdiakhw42bzkza5ci0b7f2qa0922avsglrivyqk2hlfxn9fnv")))

(define-public crate-compile-time-run-macro-0.2.1 (c (n "compile-time-run-macro") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1cnp10xlb1bk9xdx3m4b1s48bbdqys1sqic8v51xwyv8hb8dhhyg")))

(define-public crate-compile-time-run-macro-0.2.2 (c (n "compile-time-run-macro") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00w4cnm8d0522cprb8lip54imnkrn8djzz8qcr1aclwwikfhz2qg")))

(define-public crate-compile-time-run-macro-0.2.4 (c (n "compile-time-run-macro") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08skmbzmlgxibkpppzl36x3pw3zvfb8329mnnd242136xzr78gva")))

(define-public crate-compile-time-run-macro-0.2.5 (c (n "compile-time-run-macro") (v "0.2.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ylx415p0zfj7d79fnd5wzsc21z4in42mnfyjl6pfmz8mvg38rar")))

(define-public crate-compile-time-run-macro-0.2.6 (c (n "compile-time-run-macro") (v "0.2.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1rfh8b859135wyzgbqjif2b687mzvjkz0f9pxn49jw39as2knvq8")))

(define-public crate-compile-time-run-macro-0.2.7 (c (n "compile-time-run-macro") (v "0.2.7") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p50xfdians5dr87bfdhiikpg1sc5pfq6kbapcs24w9gg86h4pnq")))

(define-public crate-compile-time-run-macro-0.2.8 (c (n "compile-time-run-macro") (v "0.2.8") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12nmk4b5nqfkkq8xx8pyqdwb7jm43w033vdjb4mf4rmrzndzqkf4")))

