(define-module (crates-io co mp comprez) #:use-module (crates-io))

(define-public crate-comprez-0.0.1 (c (n "comprez") (v "0.0.1") (h "1kshazkdrh85c2fc2mzigpr1avqqaxiwv4gpxjdr36lsr1jg6z6a") (y #t)))

(define-public crate-comprez-0.0.11 (c (n "comprez") (v "0.0.11") (h "04zq0hzwi82vliw2srbc037ypckl6asl0ms5n28fk814h6z2pm22") (y #t)))

(define-public crate-comprez-0.0.2 (c (n "comprez") (v "0.0.2") (h "0qw9v20w0a1cswblz1s6r07cw10qbsk3jxpz2vaix51yx1vbkali") (y #t)))

(define-public crate-comprez-0.1.0 (c (n "comprez") (v "0.1.0") (h "1jcrw42icnq2f0sipixdp6fj0qvx2rpxir53i2lwzvl9wij6gi64")))

(define-public crate-comprez-0.1.1 (c (n "comprez") (v "0.1.1") (h "1jnp9dwy65h3ianp2l19ldipiggblwxfbgpcxznkjxxg4ri0xkw3")))

(define-public crate-comprez-0.2.0 (c (n "comprez") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0vzqlmv3gbjm64m7lba9bfrgiz6idfizchwdzkcqxp8aw3cc1dmx")))

(define-public crate-comprez-0.2.1 (c (n "comprez") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "015wz087fkx6m56l3pfwvds62q34jq057ndsb3r993w18av2yg4p")))

(define-public crate-comprez-0.2.5 (c (n "comprez") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1jzw9jrmbpm0z2a5fmcj4rgscsxk8ncqfi8sq7z5fnrrcl0xm38y")))

(define-public crate-comprez-0.2.6 (c (n "comprez") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "07h00glslrpcfipyfrdpa444kvjqxjq9y27zihw306a21i08di9q")))

(define-public crate-comprez-0.2.7 (c (n "comprez") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "01dfhk0b22qdwwjakc7vmfk5302l2qyfflh31m4hp4h8277hpvva")))

