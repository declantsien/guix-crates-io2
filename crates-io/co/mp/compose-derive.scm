(define-module (crates-io co mp compose-derive) #:use-module (crates-io))

(define-public crate-compose-derive-0.1.0 (c (n "compose-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ca3c5jisyjz89y57z37wms7pg1149g5i1pvvrai0mhyg6p58cvp")))

