(define-module (crates-io co mp compile_type_eq) #:use-module (crates-io))

(define-public crate-compile_type_eq-0.1.0 (c (n "compile_type_eq") (v "0.1.0") (h "1m7r4xansj10bm47kllx0zl6zy3bwxms8g7byyibajcp901kk7i1")))

(define-public crate-compile_type_eq-0.1.1 (c (n "compile_type_eq") (v "0.1.1") (h "0p4agxn7iinqp6g8w711b8s5l1b5c7nlqx32wb7jpd38a7v9pks7")))

