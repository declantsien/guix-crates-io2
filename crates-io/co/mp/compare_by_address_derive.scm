(define-module (crates-io co mp compare_by_address_derive) #:use-module (crates-io))

(define-public crate-compare_by_address_derive-0.1.0 (c (n "compare_by_address_derive") (v "0.1.0") (d (list (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dh7zid3sb5wpnyczr7g6kjy75ig8b8wbiqigdibzmp3fq6zp42g")))

(define-public crate-compare_by_address_derive-0.1.1 (c (n "compare_by_address_derive") (v "0.1.1") (d (list (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09651bahnnd4vfzfabmwfqfk1sn1bl3vr18l874c7sxm0hfxc7yn")))

