(define-module (crates-io co mp compile-time-run) #:use-module (crates-io))

(define-public crate-compile-time-run-0.1.0 (c (n "compile-time-run") (v "0.1.0") (d (list (d (n "compile-time-run-macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1jxx3xwq7vf240pi4kqaz6dq11iiar2azq2sk7k36xn02acpcxgk")))

(define-public crate-compile-time-run-0.2.0 (c (n "compile-time-run") (v "0.2.0") (d (list (d (n "compile-time-run-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0hpcxxpdi722mqpcnq13lj2v5v1vv8y0mrplvdva9nl6r0350zpb")))

(define-public crate-compile-time-run-0.2.2 (c (n "compile-time-run") (v "0.2.2") (d (list (d (n "compile-time-run-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1y228g6f6nqg2gp6hh77svi0kxvvahi4jbrlaby15b6i7ywmyrxg")))

(define-public crate-compile-time-run-0.2.3 (c (n "compile-time-run") (v "0.2.3") (d (list (d (n "compile-time-run-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10f4z82sm8i5q48vpfnbqljzkcclsfps3cr9i2683sm87kbckda6")))

(define-public crate-compile-time-run-0.2.4 (c (n "compile-time-run") (v "0.2.4") (d (list (d (n "compile-time-run-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1dq6y5wb6isgzlj2nzc7pmifiaz79h2s471v3lzz8ydnbgz87a85")))

(define-public crate-compile-time-run-0.2.5 (c (n "compile-time-run") (v "0.2.5") (d (list (d (n "compile-time-run-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0lmc90rzf1c5h0svixbzazf1ip212mf5mhc1nq1lyavpwzpmisx0")))

(define-public crate-compile-time-run-0.2.6 (c (n "compile-time-run") (v "0.2.6") (d (list (d (n "compile-time-run-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0xpmmn7li95qjda9ja5rg8y7valaqdn8icml4qxnrff07d6nlb10")))

(define-public crate-compile-time-run-0.2.7 (c (n "compile-time-run") (v "0.2.7") (d (list (d (n "compile-time-run-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0vnm9jl8imw0rppbqphmjzjzn7q6qdnc98k8z8nxr06hny2jnwyj")))

(define-public crate-compile-time-run-0.2.8 (c (n "compile-time-run") (v "0.2.8") (d (list (d (n "compile-time-run-macro") (r "= 0.2.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "11k2msl7g0f10959xpw9faic3acb7cajp845sl4679wd1rs766jj")))

(define-public crate-compile-time-run-0.2.9 (c (n "compile-time-run") (v "0.2.9") (d (list (d (n "assert2") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "038nb0fdd3lzckblyzqy3r0ay25sx0ddczjv0fqa0708gc56306l")))

(define-public crate-compile-time-run-0.2.10 (c (n "compile-time-run") (v "0.2.10") (d (list (d (n "assert2") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nm8z590z9pcxwwrhrk0b1lviyahsv3b5991isysxkp0q2hpwvzk")))

(define-public crate-compile-time-run-0.2.11 (c (n "compile-time-run") (v "0.2.11") (d (list (d (n "assert2") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "189hjkm1sxxswaywwpls162f9m01km1d98glcai1j7dsvwaz0d5p")))

(define-public crate-compile-time-run-0.2.12 (c (n "compile-time-run") (v "0.2.12") (d (list (d (n "assert2") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zv39vza9ppzdnmwb6hybsca1m66v1fi4r9h92d06769lzxszda3")))

