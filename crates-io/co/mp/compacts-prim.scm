(define-module (crates-io co mp compacts-prim) #:use-module (crates-io))

(define-public crate-compacts-prim-0.1.0 (c (n "compacts-prim") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "15gq3hrmhmgg852hka5hnf4qblk1302ra98kj18alc8vj0a282g3")))

(define-public crate-compacts-prim-0.2.0 (c (n "compacts-prim") (v "0.2.0") (h "13zrm3d8xn8i6pl72w9178azs726bwlrgw7largif7hsb3fa4z5p")))

(define-public crate-compacts-prim-0.2.1 (c (n "compacts-prim") (v "0.2.1") (h "1a4kvk60rcsjca44irms25ngnv990niin0zc09jy04fqj36nz6vf")))

