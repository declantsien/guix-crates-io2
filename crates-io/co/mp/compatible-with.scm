(define-module (crates-io co mp compatible-with) #:use-module (crates-io))

(define-public crate-compatible-with-0.1.0 (c (n "compatible-with") (v "0.1.0") (d (list (d (n "compatible-with-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1dbwvihdc2rv7cp1mf3dnnzabvvxmnbkx4yh567my9g1z8y7xdgh")))

(define-public crate-compatible-with-0.1.1 (c (n "compatible-with") (v "0.1.1") (d (list (d (n "compatible-with-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0g4amp2xp48hv4k4hbaskf871rccxpa4nr855601zs1lzyq7cyz3")))

